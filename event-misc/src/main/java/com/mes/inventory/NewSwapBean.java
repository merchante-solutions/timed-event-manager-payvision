/*@lineinfo:filename=NewSwapBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/NewSwapBean.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/20/04 12:40p $
  Version            : $Revision: 21 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.ops.InventoryBase;
import sqlj.runtime.ResultSetIterator;

public class NewSwapBean extends InventoryBase
{
  public static final int RECEIVED_IN_STOCK       = 0;
  public static final int ENCRYPTED_IN_STOCK      = 3;

  public static final int SWAP_INCOMPLETE         = mesConstants.CALLTAG_INCOMPLETE;
  public static final int SWAP_COMPLETE           = mesConstants.CALLTAG_COMPLETE;

  public static final int SWAP_IN_NOTE            = 0;
  public static final int SWAP_OUT_NOTE           = 1;
 

  //always the same 
  private String   merchantNum                    = "";
  private String   swapNum                        = "";
  private int      swapStatus                     = 0;
  private String   callTag                        = "";

  private String   serialNumOutList               = "";
  private String   serialNumOutMan                = "";

  private String   serialNumInMan                 = "";
  private String   serialNumInList                = "";
  private String   partNum                        = "";
  private String   owner                          = "";
  private String   ownerDesc                      = "";

  //use once already submitted
  private String   serialNumOut                   = "";
  private String   descriptionOut                 = "";
  private String   serialNumIn                    = "";
  private String   descriptionIn                  = "";
  
  //holds current statues once submitted
  private String   statusIn                       = "";
  private String   statusOut                      = "";
  private String   statusOutDesc                  = "";
  
  private String   shippingCostIn                 = "";
  private String   shippingCostOut                = "";
  
  private String   notesIn                        = "";
  private String   notesOut                       = "";

  private boolean  inSubmitted                    = false;
  private boolean  outSubmitted                   = false;

  public  Vector   availableEquipDesc             = new Vector();
  public  Vector   availableEquipSerialNum        = new Vector();

  public  Vector   merchEquipDesc                 = new Vector();
  public  Vector   merchEquipSerialNum            = new Vector();

  public  Vector   equipDesc                      = new Vector();
  public  Vector   equipModel                     = new Vector();

  public  Vector   statusDescIn                   = new Vector();
  public  Vector   statusCodeIn                   = new Vector();

  public  Vector   statusDescOut                  = new Vector();
  public  Vector   statusCodeOut                  = new Vector();

  public  Vector   ownerIn                        = new Vector();
  public  Vector   ownerCodeIn                    = new Vector();

  public  Vector   inNote                         = new Vector();
  public  Vector   inDate                         = new Vector();
  public  Vector   inTime                         = new Vector();
  public  Vector   inUser                         = new Vector();

  public  Vector   outNote                        = new Vector();
  public  Vector   outDate                        = new Vector();
  public  Vector   outTime                        = new Vector();
  public  Vector   outUser                        = new Vector();


  public NewSwapBean()
  {
    ResultSet         rs        = null;
    ResultSetIterator it        = null;
    boolean           isStale   = false;
    try
    {
      if(isConnectionStale())
      {
        isStale = true;
        connect();
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:133^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_descriptor,
//                  equip_model
//          from    equipment
//          where   equiptype_code not in (7, 8, 9)  and
//                  used_in_inventory = 'Y'
//          order by equipmfgr_mfr_code, equip_descriptor
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_descriptor,\n                equip_model\n        from    equipment\n        where   equiptype_code not in (7, 8, 9)  and\n                used_in_inventory = 'Y'\n        order by equipmfgr_mfr_code, equip_descriptor";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.NewSwapBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.NewSwapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:141^7*/
      
      rs = it.getResultSet();

      equipDesc.add("select product desc");
      equipModel.add("");
      
      while(rs.next())
      {
        equipDesc.add(rs.getString("equip_descriptor"));
        equipModel.add(rs.getString("equip_model"));
      }
      
      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:157^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    equip_status
//          where   equip_status_id not in (1, 2, 5, 7, 20, 26, 27, 28)
//          order by equip_status_order
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    equip_status\n        where   equip_status_id not in (1, 2, 5, 7, 20, 26, 27, 28)\n        order by equip_status_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.inventory.NewSwapBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.inventory.NewSwapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:163^7*/
      
      rs = it.getResultSet();
      
      statusDescIn.add("select status");
      statusCodeIn.add("");
      
      while(rs.next())
      {
        statusDescIn.add(rs.getString("equip_status_desc"));
        statusCodeIn.add(rs.getString("equip_status_id"));
      }
      
      rs.close();
      it.close();

      
      /*@lineinfo:generated-code*//*@lineinfo:180^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    equip_status
//          where   equip_status_id in (1, 2, 5, 7, 20, 26, 27, 28)
//          order by equip_status_order
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    equip_status\n        where   equip_status_id in (1, 2, 5, 7, 20, 26, 27, 28)\n        order by equip_status_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.inventory.NewSwapBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.inventory.NewSwapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:186^7*/
      
      rs = it.getResultSet();

      statusDescOut.add("select status");
      statusCodeOut.add("");
      
      while(rs.next())
      {
        statusDescOut.add(rs.getString("equip_status_desc"));
        statusCodeOut.add(rs.getString("equip_status_id"));
      }
      
      rs.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:202^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_type_code,
//                  inventory_owner_name
//          from    app_type
//          where   separate_inventory = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_type_code,\n                inventory_owner_name\n        from    app_type\n        where   separate_inventory = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.inventory.NewSwapBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.inventory.NewSwapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:208^7*/
      
      rs = it.getResultSet();

      ownerIn.add("select owner");
      ownerCodeIn.add("");
      
      while(rs.next())
      {
        ownerIn.add(rs.getString("inventory_owner_name"));  
        ownerCodeIn.add(rs.getString("app_type_code"));
      }
      
      rs.close();
      it.close();

    }  
    catch(Exception e)
    {
      logEntry( "constructor()", e.toString());
      addError("constructor: " + e.toString());
    }
    finally
    {
      if(isStale)
      {
        cleanUp();
      }
    }
  }
  
  public void situateOwner(String merchNum)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:248^9*/

//  ************************************************************
//  #sql [Ctx] { select  merchant_type, 
//                    merchant_type_desc  
//            
//            from    merchant_types
//            where   merchant_number = :merchNum                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merchant_type, \n                  merchant_type_desc  \n           \n          from    merchant_types\n          where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.inventory.NewSwapBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   owner = (String)__sJT_rs.getString(1);
   ownerDesc = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:256^9*/
      }
      catch( java.sql.SQLException sqle )   // no data found set default
      {
        ownerDesc = "MES";
        owner     = "0";
      }        

      // load the data for this owner
      /*@lineinfo:generated-code*//*@lineinfo:265^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_serial_number                     as serial_number,
//                  clas.ec_class_name || ' - ' || 
//                  equip.equip_descriptor || ' - ' || 
//                  ei.ei_serial_number                     as equip_desc
//          from    equipment         equip,
//                  equip_inventory   ei,
//                  equip_class       clas
//          where   ei.ei_class = clas.ec_class_id 
//                  and ei.ei_part_number = equip.equip_model  
//                  and ei.ei_lrb = 0                          
//                  and ei.ei_status in (0,3)
//                  and ei.ei_owner = :owner                      
//          order by ei.ei_class, equip.equipmfgr_mfr_code, equip.equip_descriptor
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_serial_number                     as serial_number,\n                clas.ec_class_name || ' - ' || \n                equip.equip_descriptor || ' - ' || \n                ei.ei_serial_number                     as equip_desc\n        from    equipment         equip,\n                equip_inventory   ei,\n                equip_class       clas\n        where   ei.ei_class = clas.ec_class_id \n                and ei.ei_part_number = equip.equip_model  \n                and ei.ei_lrb = 0                          \n                and ei.ei_status in (0,3)\n                and ei.ei_owner =  :1                       \n        order by ei.ei_class, equip.equipmfgr_mfr_code, equip.equip_descriptor";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.inventory.NewSwapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,owner);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.inventory.NewSwapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:280^7*/
      rs = it.getResultSet();

      // add the default
      availableEquipDesc.add("select one");
      availableEquipSerialNum.add("");
      
      while(rs.next())
      {
        availableEquipDesc.add( rs.getString("equip_desc") );
        availableEquipSerialNum.add(rs.getString("serial_number"));
      }
      rs.close();
      it.close();
    }  
    catch(Exception e)
    {
      logEntry("situateOwner()", e.toString());
      addError("situateOwner: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  public String startNewSwap(String merchNum)
  {
    this.merchantNum = merchNum; //add merchant number to variable

    long                controlSeq  = 10000000000L;
    long                primaryKey  = 0L;
    
    if(!isBlank(this.swapNum))
    {
      return "";
    }

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:320^7*/

//  ************************************************************
//  #sql [Ctx] { select swap_sequence.nextval  from dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select swap_sequence.nextval   from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.inventory.NewSwapBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   primaryKey = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:323^7*/

      // get a new control number
      Calendar      cal         = new GregorianCalendar();

      // add the year
      controlSeq += ((cal.get(Calendar.YEAR) - 2000) * 100000000L);

      // add the julian day
      controlSeq += (cal.get(Calendar.DAY_OF_YEAR) * 100000L);

      // add the primarykey
      controlSeq += primaryKey;
      
      this.swapNum = Long.toString(controlSeq);
    }
    catch(Exception e)
    {
      logEntry( "createReferenceNum()", e.toString());
      this.swapNum = "";
    }
    return this.swapNum;
  }


  public void getMerchantEquipment()
  {
    ResultSetIterator it        = null;
    ResultSet         rs        = null;
    StringBuffer      qs        = new StringBuffer();
    boolean           wasStale  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      // empty the vector
      merchEquipDesc.removeAllElements(); 
      
      merchEquipDesc.add("select one");
      merchEquipSerialNum.add("");
      
      /*@lineinfo:generated-code*//*@lineinfo:369^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_serial_number,
//                  ei.ei_part_number,
//                  eq.equip_descriptor
//          from    equipment       eq, 
//                  equip_inventory ei
//          where   ei.ei_part_number     = eq.equip_model and
//                  ei.ei_merchant_number = :merchantNum 
//          order by eq.equip_descriptor
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_serial_number,\n                ei.ei_part_number,\n                eq.equip_descriptor\n        from    equipment       eq, \n                equip_inventory ei\n        where   ei.ei_part_number     = eq.equip_model and\n                ei.ei_merchant_number =  :1  \n        order by eq.equip_descriptor";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.inventory.NewSwapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.inventory.NewSwapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:379^7*/
      rs = it.getResultSet();

      while(rs.next())
      {
        merchEquipDesc.add(rs.getString("equip_descriptor") + " - " + rs.getString("ei_serial_number"));
        merchEquipSerialNum.add(rs.getString("ei_serial_number") + "@" + rs.getString("ei_part_number"));
      }
      rs.close();
      it.close();
    }  
    catch(Exception e)
    {
      logEntry( "getMerchantEquipment()", e.toString());
      addError("getMerchantEquipment: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
      if(wasStale)
      {
        cleanUp();
      }
    }
  }

  public void getNoteInfo(String swap)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:412^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  esn.note_type             as note_type,
//                  esn.note                  as note, 
//                  esn.note_date             as note_date,
//                  u.login_name              as login_name
//          from    equip_swap_notes  esn, 
//                  users             u
//          where   swap_ref_num = :swap and 
//                  esn.note_user = u.user_id 
//          order by esn.note_date DESC
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  esn.note_type             as note_type,\n                esn.note                  as note, \n                esn.note_date             as note_date,\n                u.login_name              as login_name\n        from    equip_swap_notes  esn, \n                users             u\n        where   swap_ref_num =  :1  and \n                esn.note_user = u.user_id \n        order by esn.note_date DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.inventory.NewSwapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,swap);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.inventory.NewSwapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:423^7*/
      rs = it.getResultSet();
 
      while(rs.next())
      {
        switch(rs.getInt("note_type"))
        {
          case SWAP_IN_NOTE:
            inNote.add(replaceNewLine(rs.getString("note"))); 
            inDate.add(java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("note_date"))); 
            inTime.add(java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("note_date")));
            inUser.add(rs.getString("login_name")); 
          break;
          case SWAP_OUT_NOTE:
            outNote.add(replaceNewLine(rs.getString("note"))); 
            outDate.add(java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("note_date")));
            outTime.add(java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("note_date")));
            outUser.add(rs.getString("login_name")); 
          break;
        }
      }
      rs.close();
      it.close();
    }  
    catch(Exception e)
    {
      logEntry( "getNoteInfo()", e.toString());
      addError("getNoteInfo: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public String getSwapInfo(String swap)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "";

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:466^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    equip_swap    es
//          where   es.swap_ref_num = :swap
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    equip_swap    es\n        where   es.swap_ref_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.inventory.NewSwapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,swap);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.inventory.NewSwapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:471^7*/
      rs = it.getResultSet();

      if(rs.next())
      {
        result = rs.getString("merchant_num");
        
        this.inSubmitted  = !isBlank(rs.getString("serial_num_in"));
        if(isInSubmitted())
        {
          this.serialNumIn = rs.getString("serial_num_in");
          getSubmittedInData();
        }
        
        this.outSubmitted = !isBlank(rs.getString("serial_num_out"));
        if(isOutSubmitted())
        {
          this.serialNumOut = rs.getString("serial_num_out");
          getSubmittedOutData();
        }
      
        this.shippingCostIn  = isBlank(rs.getString("shipping_cost_in")) ? "" : rs.getString("shipping_cost_in");
        this.shippingCostOut = isBlank(rs.getString("shipping_cost_out")) ? "" : rs.getString("shipping_cost_out");
      
        this.swapStatus       = rs.getInt("swap_status");
        this.swapNum          = rs.getString("swap_ref_num");
        this.merchantNum      = rs.getString("merchant_num");
        this.callTag          = isBlank(rs.getString("call_tag_num")) ? "" : rs.getString("call_tag_num");
      }
      
      rs.close();
      it.close();
    }  
    catch(Exception e)
    {
      logEntry( "getSwapInfo()", e.toString());
      addError("getSwapInfo: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {} 
    }
    return(result);
  }

  public void getSubmittedInData()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:520^7*/

//  ************************************************************
//  #sql [Ctx] { select  eq.equip_descriptor,
//                  ei.ei_status 
//          
//          from    equipment         eq,
//                  equip_inventory   ei
//          where   eq.equip_model = ei.ei_part_number and
//                  ei.ei_serial_number = :serialNumIn
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  eq.equip_descriptor,\n                ei.ei_status \n         \n        from    equipment         eq,\n                equip_inventory   ei\n        where   eq.equip_model = ei.ei_part_number and\n                ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.inventory.NewSwapBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,serialNumIn);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   descriptionIn = (String)__sJT_rs.getString(1);
   statusIn = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:530^7*/
      
      //@ is this necessary?
      if( descriptionIn == null )
      {
        descriptionIn = "";
      }
      if ( statusIn == null )
      {
        statusIn = "";
      }
    }  
    catch(Exception e)
    {
      logEntry("getSubmittedInData()", e.toString());
      addError("getSubmittedInData: " + e.toString());
    }
  }

  public void getSubmittedOutData()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:553^7*/

//  ************************************************************
//  #sql [Ctx] { select  eq.equip_descriptor, 
//                  es.equip_status_desc 
//          
//          from    equipment         eq,
//                  equip_inventory   ei,
//                  equip_status      es
//          where   eq.equip_model = ei.ei_part_number and 
//                  es.equip_status_id = ei.ei_status and 
//                  ei.ei_serial_number = :serialNumOut
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  eq.equip_descriptor, \n                es.equip_status_desc \n         \n        from    equipment         eq,\n                equip_inventory   ei,\n                equip_status      es\n        where   eq.equip_model = ei.ei_part_number and \n                es.equip_status_id = ei.ei_status and \n                ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.inventory.NewSwapBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,serialNumOut);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   descriptionOut = (String)__sJT_rs.getString(1);
   statusOutDesc = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:565^7*/
      
      //@ is this necessary?
      if ( descriptionOut == null )
      {
        descriptionOut = "";
      }
      if ( statusOutDesc == null )
      {
        statusOutDesc = "";
      }
    }  
    catch(Exception e)
    {
      logEntry("getSubmittedOutData()", e.toString());
      addError("getSubmittedOutData: " + e.toString());
    }
  }

  public boolean validate()
  {
    long              merchantId      = 0;
    
    try
    {
      merchantId = Long.parseLong(this.merchantNum);
    }
    catch( NumberFormatException e )
    {
    }
    
    if( !isMerchant(merchantId) )
    {
      logEntry( "validate()", "Specified merchant '" + merchantNum + "' does not exist" );
      addError( "Validation failed because the specified merchant '" + merchantNum + "' does not exist." );
    }
    return( !hasErrors() );
  }


  public boolean validateOutUpdateData()
  {
    
    if(!isBlank(this.shippingCostOut) && !validNumber(this.shippingCostOut))
    {
      addError("Please provide a valid shipping cost for deployed equipment");
    }
    
    return(!hasErrors());
  }

  public boolean validateOutData()
  {
    if(isBlank(this.serialNumOutMan) && isBlank(this.serialNumOutList))
    {
      addError("Either select an In Item from the list OR provide a serial number");
    }
    else if(!isBlank(this.serialNumOutMan) && !isBlank(this.serialNumOutList))
    {
      addError("Either select an In Item from the list OR provide a serial number");
    }    
  
    if(isBlank(this.statusOut))
    {
      addError("Please select the status of the deployed equipment");
    }
    if(!isBlank(this.shippingCostOut) && !validNumber(this.shippingCostOut))
    {
      addError("Please provide a valid shipping cost for deployed equipment");
    }
    
    //make sure it exists in inventory
    if(!hasErrors())
    {
      this.serialNumOut = isBlank(this.serialNumOutMan) ? this.serialNumOutList : this.serialNumOutMan;
      if(!equipmentExists(this.serialNumOut))
      {
        addError("Equipment with serial # " + this.serialNumOut + " does not exist in the inventory");
      }
    }
    
    //make sure equipment is not already deployed 
    if(!hasErrors())
    {
      String tempMerch = getDeployedMerch(this.serialNumOut);
      
      if(!isBlank(tempMerch))
      {
        addError("Equipment with serial # " + this.serialNumOut + " is currently deployed to merchant # " + tempMerch);
      }
    }
    
    //make sure it has a status of instock...
    if(!hasErrors())
    {
      String tempStatus = getStatusDesc(this.serialNumOut);

      if(!isBlank(tempStatus))
      {
        addError("Equipment with serial # " + this.serialNumOut + " currently has a status of " + tempStatus);
      }
    }
    
    //make sure it belongs to the same client
    if(!hasErrors())
    {
      String tempOwner = getEquipOwner(this.serialNumOut,this.owner);
      
      if(!isBlank(tempOwner))
      {
        addError("Equipment with serial # " + this.serialNumOut + " exists in inventory as being owned by " + tempOwner);
      }
    }


    return(!hasErrors());
  }

  private String getStatusDesc(String serNum)
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    String                result  = "";

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:691^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_status              as ei_status,
//                  stat.equip_status_desc    as equip_status_desc 
//          from    equip_inventory     ei, 
//                  equip_status        stat 
//          where   ei.ei_status = stat.equip_status_id and
//                  ei.ei_serial_number = :serNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_status              as ei_status,\n                stat.equip_status_desc    as equip_status_desc \n        from    equip_inventory     ei, \n                equip_status        stat \n        where   ei.ei_status = stat.equip_status_id and\n                ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.inventory.NewSwapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.inventory.NewSwapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:699^7*/      
      rs = it.getResultSet();

      if(rs.next())
      {
        if(rs.getInt("ei_status") != RECEIVED_IN_STOCK && rs.getInt("ei_status") != ENCRYPTED_IN_STOCK)
        {
          result = isBlank(rs.getString("equip_status_desc")) ? "" : rs.getString("equip_status_desc");
        }
        else
        {
          result = "";
        }
      }
      else
      {
        result = "";
      }
      rs.close();
      it.close();
    }  
    catch(Exception e)
    {
      logEntry( "getStatusDesc()", e.toString());
      addError("getStatusDesc: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) { }
    }
    return(result);
  }

  private boolean equipmentExists(String serNum)
  {
    int       itemCount     = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:738^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(ei_serial_number) 
//          from    equip_inventory 
//          where   ei_serial_number = :serNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ei_serial_number)  \n        from    equip_inventory \n        where   ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.inventory.NewSwapBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,serNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:743^7*/
    }  
    catch(Exception e)
    {
      logEntry( "equipmentExists()", e.toString());
      addError("equipmentExists: " + e.toString());
    }
    return(itemCount > 0);
  }


  public boolean validateNoteIn()
  {
    if(isBlank(this.notesIn))
    {
      addError("Please provide an In Item Note");
    }
    else if(this.notesIn.length() > 500)
    {
      addError("In Item Note must be less than 500 characters. Currently it's " + this.notesIn.length() + " characters long.");
    }
    return(!hasErrors());
  }

  public boolean validateNoteOut()
  {
    if(isBlank(this.notesOut))
    {
      addError("Please provide an Out Item Note");
    }
    else if(this.notesOut.length() > 500)
    {
      addError("Out Item Note must be less than 500 characters. Currently it's " + this.notesOut.length() + " characters long.");
    }
    return(!hasErrors());
  }

  public boolean validateInUpdateData()
  {
    
    if(isBlank(this.statusIn))
    {
      addError("Please select the status of the equipment upon return");
    }
    if(!isBlank(this.shippingCostIn) && !validNumber(this.shippingCostIn))
    {
      addError("Please provide a valid shipping cost for returned equipment");
    }
    
    return(!hasErrors());
  }

  public boolean validateInData()
  {
    if(isBlank(this.serialNumInMan) && isBlank(this.serialNumInList))
    {
      addError("Either select an In Item from the list OR provide a serial number and product description");
    }
    else if(!isBlank(this.serialNumInMan) && !isBlank(this.serialNumInList))
    {
      addError("Either select an In Item from the list OR provide a serial number and product description");
    }    
    else if(!isBlank(this.serialNumInMan) && isBlank(partNum))
    {
      addError("If entering a serial #, also select the description of the item from the pull down list");
    }
    
    //else if(!isBlank(this.serialNumInMan) && isBlank(owner))
    //{
    //  addError("If entering a serial #, also select the owner of the item from the pull down list");
    //}
    
    if(isBlank(this.statusIn))
    {
      addError("Please select the status of the equipment upon return");
    }
    if(!isBlank(this.shippingCostIn) && !validNumber(this.shippingCostIn))
    {
      addError("Please provide a valid shipping cost for returned equipment");
    }
    
    if(!hasErrors())
    {
      this.serialNumIn = isBlank(this.serialNumInMan) ? this.serialNumInList : this.serialNumInMan;
      String tempMerch = getDeployedMerch(this.serialNumIn);
      
      if(!isBlank(tempMerch) && !this.merchantNum.equals(tempMerch))
      {
        addError("Equipment with serial # " + this.serialNumIn + " is deployed to merchant # " + tempMerch);
      }
    }
    
    if(!hasErrors() && !isBlank(this.partNum))
    {
      String tempDesc = getEquipDesc(this.serialNumIn,this.partNum);
      
      if(!isBlank(tempDesc))
      {
        addError("Equipment with serial # " + this.serialNumIn + " exists in inventory as " + tempDesc);
      }
    }
    
//***********************************************************************
//if they try to enter an item into the database and it is owned by the wrong dude.. then it cant be entered
    if(!hasErrors() && !isBlank(this.owner))
    {
      String tempOwner = getEquipOwner(this.serialNumIn,this.owner);
      
      if(!isBlank(tempOwner))
      {
        addError("Equipment with serial # " + this.serialNumIn + " exists in inventory as being owned by " + tempOwner);
      }
    }
//************************************************************************
    
    return(!hasErrors());
  }

  private String getEquipOwner(String serNum, String own)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "";

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:869^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_owner,
//                  at.inventory_owner_name
//          from    equip_inventory     ei, 
//                  app_type            at 
//          where   ei.ei_owner = at.app_type_code and 
//                  ei.ei_serial_number = :serNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_owner,\n                at.inventory_owner_name\n        from    equip_inventory     ei, \n                app_type            at \n        where   ei.ei_owner = at.app_type_code and \n                ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.inventory.NewSwapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.inventory.NewSwapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:877^7*/
      rs = it.getResultSet();

      if(rs.next())
      {
        String tempOwner = isBlank(rs.getString("ei_owner")) ? "" : rs.getString("ei_owner");

        if(!tempOwner.equals(own))
        {
          result = isBlank(rs.getString("inventory_owner_name")) ? "" : rs.getString("inventory_owner_name");
        }
        else
        {
          result = "";
        }
      }
      else
      {
        result = "";
      }
      rs.close();
      it.close();
    }  
    catch(Exception e)
    {
      logEntry( "getEquipOwner()", e.toString());
      addError("getEquipOwner: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
    return(result);
  }



  private String getEquipDesc(String serNum,String parNum)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "";

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:922^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_part_number,
//                  equip.equip_descriptor 
//          from    equip_inventory ei, 
//                  equipment       equip 
//          where   ei.ei_part_number = equip.equip_model and 
//                  ei.ei_serial_number = :serNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_part_number,\n                equip.equip_descriptor \n        from    equip_inventory ei, \n                equipment       equip \n        where   ei.ei_part_number = equip.equip_model and \n                ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.inventory.NewSwapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.inventory.NewSwapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:930^7*/
      rs = it.getResultSet();

      if( rs.next() )
      {
        String tempModel = isBlank(rs.getString("ei_part_number")) ? "" : rs.getString("ei_part_number");
        if(!tempModel.equals(parNum))
        {
          result = isBlank(rs.getString("equip_descriptor")) ? "" : rs.getString("equip_descriptor");
        }
        else
        {
          result = "";
        }
      }
      else
      {
        result = "";
      }
      rs.close();
      it.close();
    }  
    catch(Exception e)
    {
      logEntry( "getEquipDesc()", e.toString());
      addError("getEquipDesc: " + e.toString());
    }
    finally
    {
      try{it.close();}catch(Exception e){}
    }
    
    return(result);
  }

  private String getDeployedMerch(String serNum)
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    String            result  = "";

    try
    {
      ps = getPreparedStatement("select ei_merchant_number from equip_inventory where ei_serial_number = ? ");
      ps.setString(1, serNum);    

      rs = ps.executeQuery();

      if(rs.next())
      {
        result = isBlank(rs.getString("ei_merchant_number")) ? "" : rs.getString("ei_merchant_number");
      }
      else
      {
        result = "";
      }
      ps.close();
      rs.close();
    }  
    catch(Exception e)
    {
      logEntry( "getDeployedMerch()", e.toString());
      addError("getDeployedMerch: " + e.toString());
    }
    return result;
  }


  public boolean validNumber(String num)
  {
    boolean valid = false;
    try
    {
      double temp = Double.parseDouble(num);
      valid       = true;
    }
    catch(Exception e)
    {
      valid       = false;
    }
    return valid;
  }
    
  private String decodeStatus(String status)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    String            result  = "";

    try
    {
      ps = getPreparedStatement("select equip_status_desc from equip_status where equip_status_id = ? ");
      ps.setString(1, status);
      rs = ps.executeQuery();
 
      if(rs.next())
      {
        result = isBlank(rs.getString("equip_status_desc")) ? "" : rs.getString("equip_status_desc");
      }
 
      rs.close();
      ps.close();
    }  
    catch(Exception e)
    {
      logEntry( "decodeStatus()", e.toString());
      addError("decodeStatus: " + e.toString());
    }
    return result;
  }

  private String decodeOwner(String own)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    String            result  = "";

    try
    {
      ps = getPreparedStatement("select inventory_owner_name from app_type where app_type_code = ? ");
      ps.setString(1, own);
      rs = ps.executeQuery();
 
      if(rs.next())
      {
        result = isBlank(rs.getString("inventory_owner_name")) ? "" : rs.getString("inventory_owner_name");
      }
 
      rs.close();
      ps.close();
    }  
    catch(Exception e)
    {
      logEntry( "decodeOwner()", e.toString());
      addError("decodeOwner: " + e.toString());
    }
    return result;
  }

  private void addToTracking()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    try
    {
      
      ps = getPreparedStatement("select * from equip_merchant_tracking where action = ? and ref_num_serial_num = ? ");
      ps.setInt(1, mesConstants.ACTION_CODE_SWAP);    
      ps.setString(2,this.swapNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        //if it already exists we dont need to put in another entry
      }
      else
      {
        rs.close();
        ps.close();

        qs.setLength(0);
        qs.append("insert into equip_merchant_tracking ( ");
        qs.append("merchant_number, ");
        qs.append("action_date, ");
        qs.append("action, ");
        qs.append("ref_num_serial_num ) ");
        qs.append(" values(?,sysdate,?,?)");

        ps = getPreparedStatement(qs.toString());
      
        ps.setString(1, this.merchantNum);
        ps.setInt(2,    mesConstants.ACTION_CODE_SWAP); //code for swap action
        ps.setString(3, this.swapNum);//swap ref num
       
        ps.executeUpdate();
        ps.close();
      }
    }  
    catch(Exception e)
    {
      logEntry( "addToTracking()", e.toString());
      addError("addToTracking: " + e.toString());
    }
  }

  public void submitInData(long userId)
  {
    int               itemCount   = 0;
    String            action      = "";

    try
    {
      if( doesExist( this.partNum, this.serialNumIn ) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1128^9*/

//  ************************************************************
//  #sql [Ctx] { update equip_inventory 
//            set   ei_status           = :this.statusIn,
//                  ei_class            = 2,    -- set to used
//                  ei_lrb              = 0,    -- set to not deployed
//                  ei_deployed_date    = null, -- get rid of deployed date
//                  ei_merchant_number  = null, -- get rid of merchant number
//                  ei_lrb_price        = null  -- get rid of lend price
//            where ei_part_number = :this.partNum and
//                  ei_serial_number = :this.serialNumIn
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update equip_inventory \n          set   ei_status           =  :1 ,\n                ei_class            = 2,    -- set to used\n                ei_lrb              = 0,    -- set to not deployed\n                ei_deployed_date    = null, -- get rid of deployed date\n                ei_merchant_number  = null, -- get rid of merchant number\n                ei_lrb_price        = null  -- get rid of lend price\n          where ei_part_number =  :2  and\n                ei_serial_number =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.inventory.NewSwapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.statusIn);
   __sJT_st.setString(2,this.partNum);
   __sJT_st.setString(3,this.serialNumIn);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1139^9*/
        action = "Item returned from merchant # " + this.merchantNum + " in swap ref # " + this.swapNum;
        action += "\n Status of returning item - " + decodeStatus(this.statusIn);
      }
      else  // does not exist in inventory, add it
      {
        /*@lineinfo:generated-code*//*@lineinfo:1145^9*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_inventory 
//            (
//              ei_status,
//              ei_class,               -- set to used, 2
//              ei_original_class,      -- set to new, 1
//              ei_lrb,                 -- set to not deployed, 0
//              ei_received_date,       -- set to today
//              ei_transaction_id,      -- set to zero, 0
//              ei_part_number,       
//              ei_serial_number,
//              ei_owner
//            ) 
//            values 
//            (
//              :this.statusIn,
//              2,
//              1,
//              0,
//              sysdate,
//              0,
//              :this.partNum,
//              :this.serialNumIn,
//              :this.owner
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_inventory \n          (\n            ei_status,\n            ei_class,               -- set to used, 2\n            ei_original_class,      -- set to new, 1\n            ei_lrb,                 -- set to not deployed, 0\n            ei_received_date,       -- set to today\n            ei_transaction_id,      -- set to zero, 0\n            ei_part_number,       \n            ei_serial_number,\n            ei_owner\n          ) \n          values \n          (\n             :1 ,\n            2,\n            1,\n            0,\n            sysdate,\n            0,\n             :2 ,\n             :3 ,\n             :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.inventory.NewSwapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.statusIn);
   __sJT_st.setString(2,this.partNum);
   __sJT_st.setString(3,this.serialNumIn);
   __sJT_st.setString(4,this.owner);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1171^9*/
        action = "Item added to inventory from merchant # " + this.merchantNum + " in swap ref # " + this.swapNum;
        action += "\n Status of item upon entry - " + decodeStatus(this.statusIn);
        action += "\n Owner of item upon entry - " + decodeOwner(this.owner);
      }
      addToHistory(this.partNum, this.serialNumIn, userId, Integer.parseInt(this.statusIn), action );
      
      //add to swap table...
      /*@lineinfo:generated-code*//*@lineinfo:1179^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(swap_ref_num) 
//          from    equip_swap 
//          where   swap_ref_num = :this.swapNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(swap_ref_num)  \n        from    equip_swap \n        where   swap_ref_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.inventory.NewSwapBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,this.swapNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1184^7*/
      
      if( itemCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1188^9*/

//  ************************************************************
//  #sql [Ctx] { update equip_swap 
//            set   serial_num_in     = :this.serialNumIn,
//                  swap_status       = :this.swapStatus,
//                  call_tag_num      = :this.callTag,
//                  date_received     = sysdate,
//                  shipping_cost_in  = :isBlank(this.shippingCostIn) ? null : this.shippingCostIn
//            where swap_ref_num = :this.swapNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1802 = isBlank(this.shippingCostIn) ? null : this.shippingCostIn;
   String theSqlTS = "update equip_swap \n          set   serial_num_in     =  :1 ,\n                swap_status       =  :2 ,\n                call_tag_num      =  :3 ,\n                date_received     = sysdate,\n                shipping_cost_in  =  :4 \n          where swap_ref_num =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.inventory.NewSwapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.serialNumIn);
   __sJT_st.setInt(2,this.swapStatus);
   __sJT_st.setString(3,this.callTag);
   __sJT_st.setString(4,__sJT_1802);
   __sJT_st.setString(5,this.swapNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1197^9*/        
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1201^9*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_swap 
//            ( 
//              swap_ref_num,
//              merchant_num,
//              serial_num_in,
//              date_received,
//              shipping_cost_in, 
//              call_tag_num, 
//              swap_status
//            ) 
//            values 
//            (
//              :this.swapNum,
//              :this.merchantNum,
//              :this.serialNumIn,
//              sysdate,
//              :this.shippingCostIn,
//              :this.callTag,
//              :this.swapStatus
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_swap \n          ( \n            swap_ref_num,\n            merchant_num,\n            serial_num_in,\n            date_received,\n            shipping_cost_in, \n            call_tag_num, \n            swap_status\n          ) \n          values \n          (\n             :1 ,\n             :2 ,\n             :3 ,\n            sysdate,\n             :4 ,\n             :5 ,\n             :6 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.inventory.NewSwapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.swapNum);
   __sJT_st.setString(2,this.merchantNum);
   __sJT_st.setString(3,this.serialNumIn);
   __sJT_st.setString(4,this.shippingCostIn);
   __sJT_st.setString(5,this.callTag);
   __sJT_st.setInt(6,this.swapStatus);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1223^9*/
      
        addToTracking();
      
      }
    }  
    catch(Exception e)
    {
      logEntry( "submitInData()", e.toString());
      addError("submitInData: " + e.toString());
    }
  }

  public void submitUpdateInData(long userId)
  {
    PreparedStatement ps            = null;
    ResultSet         rs            = null;
    StringBuffer      qs            = new StringBuffer();
    String            action        = "";
    boolean           updateStatus  = false;

    try
    {
      ps = getPreparedStatement("select stat.equip_status_desc,ei.ei_status from equip_inventory ei, equip_status stat where ei.ei_status = stat.equip_status_id and ei.ei_serial_number = ? ");
      ps.setString(1, this.serialNumIn);    
      rs = ps.executeQuery();

      if(rs.next())
      {
        if(!this.statusIn.equals(rs.getString("ei_status")))  //if status changes add change to history
        {
          qs.append("update equip_inventory set ");
          qs.append("ei_status = ? ");
          qs.append("where ei_serial_number = ? ");
        
          ps = getPreparedStatement(qs.toString());
        
          ps.setString(1,this.statusIn);
          ps.setString(2,this.serialNumIn);

          action = "Status changed from " + rs.getString("equip_status_desc") + " to " + decodeStatus(this.statusIn);
    
          ps.executeUpdate();
          ps.close();
  
          addToHistory(this.partNum, this.serialNumIn, userId, Integer.parseInt(statusIn), action );
        }
      }
      else
      {
        addError("In item Status update failed");
      }
      
    
      //update in shipping in swap table... if not blank
      if(!isBlank(this.shippingCostIn))
      {
        updateStatus = true;

        ps = getPreparedStatement("select swap_ref_num from equip_swap where swap_ref_num = ? ");
        ps.setString(1, this.swapNum);    
        rs = ps.executeQuery();

        qs.setLength(0);

        if(rs.next())
        {
          qs.append("update equip_swap set ");
          qs.append("shipping_cost_in = ?, "); 
          qs.append("swap_status = ? "); 
          qs.append("where swap_ref_num = ? ");
        
          ps = getPreparedStatement(qs.toString());
        
          ps.setString(1,this.shippingCostIn);
          ps.setInt(2,this.swapStatus);
          ps.setString(3,this.swapNum);
    
          ps.executeUpdate();
          ps.close();
        }
        else
        {
          addError("In item shipping cost update failed");
        }
      }

      //update call tag... if not blank
      if(!isBlank(this.callTag))
      {
        updateStatus = true;

        ps = getPreparedStatement("select swap_ref_num from equip_swap where swap_ref_num = ? ");
        ps.setString(1, this.swapNum);    
        rs = ps.executeQuery();

        qs.setLength(0);

        if(rs.next())
        {
          qs.append("update equip_swap set ");
          qs.append("call_tag_num = ?, "); 
          qs.append("swap_status = ? "); 
          qs.append("where swap_ref_num = ? ");
        
          ps = getPreparedStatement(qs.toString());
        
          ps.setString(1,this.callTag);
          ps.setInt(2,this.swapStatus);
          ps.setString(3,this.swapNum);
    
          ps.executeUpdate();
          ps.close();
        }
        else
        {
          addError("Call Tag update failed");
        }
      }

      //update swap status
      if(!updateStatus)
      {
        ps = getPreparedStatement("select swap_ref_num from equip_swap where swap_ref_num = ? ");
        ps.setString(1, this.swapNum);    
        rs = ps.executeQuery();

        qs.setLength(0);

        if(rs.next())
        {
          qs.append("update equip_swap set ");
          qs.append("swap_status = ? "); 
          qs.append("where swap_ref_num = ? ");
        
          ps = getPreparedStatement(qs.toString());
        
          ps.setInt(1,this.swapStatus);
          ps.setString(2,this.swapNum);

          ps.executeUpdate();
          ps.close();
        }
        else
        {
          addError("Swap status update failed");
        }
      }
    }  
    catch(Exception e)
    {
      logEntry( "submitUpdateInData()", e.toString());
      addError("submitUpdateInData: " + e.toString());
    }
  }

  private void addToTracking2() //this only shows that a deployment was made in a swap
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
 
    try
    {
      qs.setLength(0);
      qs.append("insert into equip_merchant_tracking ( ");
      qs.append("merchant_number, ");
      qs.append("action_date, ");
      qs.append("action, ");
      qs.append("action_desc, ");
      qs.append("ref_num_serial_num ) ");
      qs.append(" values(?,sysdate,?,?,?)");
      
      ps = getPreparedStatement(qs.toString());
      
      ps.setString(1, this.merchantNum);
      ps.setInt(2,    mesConstants.ACTION_CODE_DEPLOYMENT_SWAP); //code for swap deployment action
      ps.setString(3, this.statusOut);//tells what kinds of lend type... 
      ps.setString(4, this.serialNumOut);
     
      ps.executeUpdate();
      ps.close();
      
    }  
    catch(Exception e)
    {
      logEntry( "addToTracking2()", e.toString());
      addError("addToTracking2: " + e.toString());
    }
  }


  public void submitOutData(long userId)
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    StringBuffer      qs      = new StringBuffer();
    String            action  = "";

    try
    {
      qs.append("update equip_inventory set ");
      qs.append("ei_status              = ?, ");
      qs.append("ei_lrb                 = ?, ");             
      qs.append("ei_deployed_date       = sysdate, "); 
      qs.append("ei_merchant_number     = ? "); 
      qs.append("where ei_serial_number = ? ");
       
      ps = getPreparedStatement(qs.toString());
        
      ps.setString(1,this.statusOut);
      ps.setString(2,this.statusOut);
      ps.setString(3,this.merchantNum);
      ps.setString(4,this.serialNumOut);
      
      if(ps.executeUpdate() != 1)
      {
        addError("Error: Unable to deploy out item");
      }
      else
      {
        action = decodeStatus(this.statusOut) + " to merchant #: " + this.merchantNum + " in swap ref # " + this.swapNum;
        addToHistory(this.partNum, this.serialNumOut, userId, Integer.parseInt(statusOut), action );
        addToTracking();
        addToTracking2();
      }
      ps.close();
    
      //add to swap table...
      ps = getPreparedStatement("select swap_ref_num from equip_swap where swap_ref_num = ? ");
      ps.setString(1, this.swapNum);    
      rs = ps.executeQuery();

      qs.setLength(0);

      if(rs.next())
      {
        qs.append("update equip_swap set ");
        qs.append("serial_num_out    = ?, ");
        qs.append("swap_status    = ?, ");

        if(!isBlank(this.callTag))
        {
          qs.append("call_tag_num = ?, "); 
        }

        if(!isBlank(this.shippingCostOut))
        {
          qs.append("date_deployed     = sysdate, "); 
          qs.append("shipping_cost_out = ? "); 
        }
        else
        {
          qs.append("date_deployed    = sysdate "); 
        }
        qs.append("where swap_ref_num = ? ");
        
        ps = getPreparedStatement(qs.toString());
        
        ps.setString(1,this.serialNumOut);
        ps.setInt(2,this.swapStatus);

        if(!isBlank(this.callTag))
        {
          ps.setString(3,this.callTag);
          if(!isBlank(this.shippingCostOut))
          {
            ps.setString(4,this.shippingCostOut);
            ps.setString(5,this.swapNum);
          }
          else
          {
            ps.setString(4,this.swapNum);
          }
        }
        else
        {
          if(!isBlank(this.shippingCostOut))
          {
            ps.setString(3,this.shippingCostOut);
            ps.setString(4,this.swapNum);
          }
          else
          {
            ps.setString(3,this.swapNum);
          }
        }
      }
      else
      {
        qs.append("insert into equip_swap ( ");
        qs.append("swap_ref_num, ");
        qs.append("merchant_num, ");
        qs.append("serial_num_out, ");
        qs.append("date_deployed, ");
        qs.append("shipping_cost_out, call_tag_num, swap_status) values (?,?,?,sysdate,?,?,?) ");
        
        ps = getPreparedStatement(qs.toString());
        
        ps.setString(1,this.swapNum);
        ps.setString(2,this.merchantNum);
        ps.setString(3,this.serialNumOut);
        ps.setString(4,this.shippingCostOut);
        ps.setString(5,this.callTag);
        ps.setInt(6,this.swapStatus);
      }
      
      ps.executeUpdate();
      ps.close();
    }  
    catch(Exception e)
    {
      logEntry( "submitOutData()", e.toString());
      addError("submitOutData: " + e.toString());
    }
  }

  public void submitUpdateOutData(long userId)
  {
    PreparedStatement ps            = null;
    ResultSet         rs            = null;
    StringBuffer      qs            = new StringBuffer();
    boolean           updateStatus  = false;

    try
    {
    
      //update out shipping in swap table... if not blank
      if(!isBlank(this.shippingCostOut))
      {
        updateStatus = true;

        ps = getPreparedStatement("select swap_ref_num from equip_swap where swap_ref_num = ? ");
        ps.setString(1, this.swapNum);    
        rs = ps.executeQuery();

        qs.setLength(0);

        if(rs.next())
        {
          qs.append("update equip_swap set ");
          qs.append("shipping_cost_out = ?, ");
          qs.append("swap_status = ? "); 
          qs.append("where swap_ref_num = ? ");
        
          ps = getPreparedStatement(qs.toString());
        
          ps.setString(1,this.shippingCostOut);
          ps.setInt(2,this.swapStatus);
          ps.setString(3,this.swapNum);

          ps.executeUpdate();
          ps.close();
        }
        else
        {
          addError("Out item shipping cost update failed");
        }
      }

      //update call tag num in swap table... if not blank
      if(!isBlank(this.callTag))
      {
        updateStatus = true;

        ps = getPreparedStatement("select swap_ref_num from equip_swap where swap_ref_num = ? ");
        ps.setString(1, this.swapNum);    
        rs = ps.executeQuery();

        qs.setLength(0);

        if(rs.next())
        {
          qs.append("update equip_swap set ");
          qs.append("call_tag_num = ?, "); 
          qs.append("swap_status = ? "); 
          qs.append("where swap_ref_num = ? ");
        
          ps = getPreparedStatement(qs.toString());
        
          ps.setString(1,this.callTag);
          ps.setInt(2,this.swapStatus);
          ps.setString(3,this.swapNum);

          ps.executeUpdate();
          ps.close();
        }
        else
        {
          addError("Call Tag update failed");
        }
      }

      //update swap status
      if(!updateStatus)
      {

        ps = getPreparedStatement("select swap_ref_num from equip_swap where swap_ref_num = ? ");
        ps.setString(1, this.swapNum);    
        rs = ps.executeQuery();

        qs.setLength(0);

        if(rs.next())
        {
          qs.append("update equip_swap set ");
          qs.append("swap_status = ? "); 
          qs.append("where swap_ref_num = ? ");
        
          ps = getPreparedStatement(qs.toString());
        
          ps.setInt(1,this.swapStatus);
          ps.setString(2,this.swapNum);

          ps.executeUpdate();
          ps.close();
        }
        else
        {
          addError("Swap status update failed");
        }
      }
    }  
    catch(Exception e)
    {
      logEntry( "submitUpdateOutData()", e.toString());
      addError("submitUpdateOutData: " + e.toString());
    }
  }

  public void submitInNote(long userId)
  {
    PreparedStatement ps            = null;
    ResultSet         rs            = null;
    StringBuffer      qs            = new StringBuffer();
    boolean           updateStatus  = false;

    try
    {
      
      qs.append("insert into equip_swap_notes ( ");
      qs.append("swap_ref_num, ");
      qs.append("note_date, ");
      qs.append("note_user, ");
      qs.append("note_type, ");
      qs.append("note) values (?,sysdate,?,?,?) ");
    
      ps = getPreparedStatement(qs.toString());
    
      ps.setString(1,this.swapNum);
      ps.setLong(2,userId);
      ps.setInt(3,SWAP_IN_NOTE);
      ps.setString(4,this.notesIn);
      
      ps.executeUpdate();
      ps.close();
      
      //update call tag num in swap table... if not blank
      if(!isBlank(this.callTag))
      {
        updateStatus = true;

        ps = getPreparedStatement("select swap_ref_num from equip_swap where swap_ref_num = ? ");
        ps.setString(1, this.swapNum);    
        rs = ps.executeQuery();

        qs.setLength(0);

        if(rs.next())
        {
          qs.append("update equip_swap set ");
          qs.append("call_tag_num = ?, "); 
          qs.append("swap_status = ? "); 
          qs.append("where swap_ref_num = ? ");
        
          ps = getPreparedStatement(qs.toString());
        
          ps.setString(1,this.callTag);
          ps.setInt(2,this.swapStatus);
          ps.setString(3,this.swapNum);

          ps.executeUpdate();
          ps.close();
        }
        else
        {
          //insert new record in swap table
          qs.append("insert into equip_swap ( ");
          qs.append("swap_ref_num, ");
          qs.append("merchant_num, ");
          qs.append("call_tag_num, swap_status) values (?,?,?,?) ");
    
          ps = getPreparedStatement(qs.toString());
    
          ps.setString(1,this.swapNum);
          ps.setString(2,this.merchantNum);
          ps.setString(3,this.callTag);
          ps.setInt(4,this.swapStatus);
          ps.executeUpdate();
          ps.close();

          //add to tracking table
          addToTracking();
        }
      }

      //update swap status
      if(!updateStatus)
      {

        ps = getPreparedStatement("select swap_ref_num from equip_swap where swap_ref_num = ? ");
        ps.setString(1, this.swapNum);    
        rs = ps.executeQuery();

        qs.setLength(0);

        if(rs.next())
        {
          qs.append("update equip_swap set ");
          qs.append("swap_status = ? "); 
          qs.append("where swap_ref_num = ? ");
        
          ps = getPreparedStatement(qs.toString());
        
          ps.setInt(1,this.swapStatus);
          ps.setString(2,this.swapNum);

          ps.executeUpdate();
          ps.close();
        }
        else
        {
          //insert new record in swap table
          qs.append("insert into equip_swap ( ");
          qs.append("swap_ref_num, ");
          qs.append("merchant_num, ");
          qs.append("swap_status) values (?,?,?) ");
    
          ps = getPreparedStatement(qs.toString());
    
          ps.setString(1,this.swapNum);
          ps.setString(2,this.merchantNum);
          ps.setInt(3,this.swapStatus);
          ps.executeUpdate();
          ps.close();

          //add to tracking table
          addToTracking();
        }
      }
    }  
    catch(Exception e)
    {
      logEntry( "submitInNote()", e.toString());
      addError("submitInNote: " + e.toString());
    }
  }

  public void submitOutNote(long userId)
  {
    PreparedStatement ps            = null;
    ResultSet         rs            = null;
    StringBuffer      qs            = new StringBuffer();
    boolean           updateStatus  = false;

    try
    {
      
      qs.append("insert into equip_swap_notes ( ");
      qs.append("swap_ref_num, ");
      qs.append("note_date, ");
      qs.append("note_user, ");
      qs.append("note_type, ");
      qs.append("note) values (?,sysdate,?,?,?) ");
    
      ps = getPreparedStatement(qs.toString());
    
      ps.setString(1,this.swapNum);
      ps.setLong(2,userId);
      ps.setInt(3,SWAP_OUT_NOTE);
      ps.setString(4,this.notesOut);
      
      ps.executeUpdate();
      ps.close();
      
      //update call tag num in swap table... if not blank
      if(!isBlank(this.callTag))
      {
        updateStatus = true;

        ps = getPreparedStatement("select swap_ref_num from equip_swap where swap_ref_num = ? ");
        ps.setString(1, this.swapNum);    
        rs = ps.executeQuery();

        qs.setLength(0);

        if(rs.next())
        {
          qs.append("update equip_swap set ");
          qs.append("call_tag_num = ?, "); 
          qs.append("swap_status = ? "); 
          qs.append("where swap_ref_num = ? ");
        
          ps = getPreparedStatement(qs.toString());
        
          ps.setString(1,this.callTag);
          ps.setInt(2,this.swapStatus);
          ps.setString(3,this.swapNum);

          ps.executeUpdate();
          ps.close();
        }
        else
        {
          //insert new record in swap table
          qs.append("insert into equip_swap ( ");
          qs.append("swap_ref_num, ");
          qs.append("merchant_num, ");
          qs.append("call_tag_num, swap_status) values (?,?,?,?) ");
    
          ps = getPreparedStatement(qs.toString());
    
          ps.setString(1,this.swapNum);
          ps.setString(2,this.merchantNum);
          ps.setString(3,this.callTag);
          ps.setInt(4,this.swapStatus);
          ps.executeUpdate();
          ps.close();

          //add to tracking table
          addToTracking();
        }
      }

      //update swap status
      if(!updateStatus)
      {

        ps = getPreparedStatement("select swap_ref_num from equip_swap where swap_ref_num = ? ");
        ps.setString(1, this.swapNum);    
        rs = ps.executeQuery();

        qs.setLength(0);

        if(rs.next())
        {
          qs.append("update equip_swap set ");
          qs.append("swap_status = ? "); 
          qs.append("where swap_ref_num = ? ");
        
          ps = getPreparedStatement(qs.toString());
        
          ps.setInt(1,this.swapStatus);
          ps.setString(2,this.swapNum);

          ps.executeUpdate();
          ps.close();
        }
        else
        {
          //insert new record in swap table
          qs.append("insert into equip_swap ( ");
          qs.append("swap_ref_num, ");
          qs.append("merchant_num, ");
          qs.append("swap_status) values (?,?,?) ");
    
          ps = getPreparedStatement(qs.toString());
    
          ps.setString(1,this.swapNum);
          ps.setString(2,this.merchantNum);
          ps.setInt(3,this.swapStatus);
          ps.executeUpdate();
          ps.close();

          //add to tracking table
          addToTracking();
        }
      }
    }  
    catch(Exception e)
    {
      logEntry( "submitOutNote()", e.toString());
      addError("submitOutNote: " + e.toString());
    }
  }

  public String replaceNewLine(String newLine)
  {
    if(isBlank(newLine))
    {
      return "";
    }

    String body = newLine;
    try
    {
      StringBuffer    bodyBuff = null;
      int             index = 0;
      
      index = body.indexOf('\n');
      
      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"<BR>");
        body = bodyBuff.toString();
        index = body.indexOf('\n', index);
      }
    }
    catch(Exception e)
    {
      System.out.println("error will robinson");
    }
    return body;
  }


  public void setMerchantNum(String merchantNum)
  {
    this.merchantNum = merchantNum;
  }

  public String getMerchantNum()
  {
    return this.merchantNum;
  }

  public void setCallTag(String callTag)
  {
    this.callTag = callTag;
  }

  public String getCallTag()
  {
    return this.callTag;
  }


  public void setSwapStatus(String swapStatus)
  {
    try
    {
      this.swapStatus = Integer.parseInt(swapStatus);
    }
    catch(Exception e)
    {
      this.swapStatus = 0;
    }
  }

  public int getSwapStatus()
  {
    return this.swapStatus;
  }
 

  public void setSerialNumIn(String serialNumIn)
  {
    this.serialNumIn = serialNumIn;
  }

  public void setSerialNumOut(String serialNumOut)
  {
    this.serialNumOut = serialNumOut;
  }

  public String getSerialNumOut()
  {
    return this.serialNumOut;
  }

  public String getSerialNumIn()
  {
    return this.serialNumIn;
  }

  public void setStatusIn(String statusIn)
  {
    this.statusIn = statusIn;
  }

  public String getStatusIn()
  {
    return this.statusIn;
  }

  public void setStatusOut(String statusOut)
  {
    this.statusOut = statusOut;
  }

  public String getStatusOut()
  {
    return this.statusOut;
  }

  public String getStatusOutDesc()
  {
    return this.statusOutDesc;
  }

  public void setPartNum(String partNum)
  {
    this.partNum = partNum;
  }

  public String getPartNum()
  {
    return this.partNum;
  }

  public void setOwner(String owner)
  {
    this.owner = owner;
  }

  public String getOwner()
  {
    return this.owner;
  }

  public void setOwnerDesc(String ownerDesc)
  {
    this.ownerDesc = ownerDesc;
  }

  public String getOwnerDesc()
  {
    return this.ownerDesc;
  }

  public void setNotesIn(String notesIn)
  {
    this.notesIn = notesIn;
  }

  public void setNotesOut(String notesOut)
  {
    this.notesOut = notesOut;
  }

  public String getDescriptionIn()
  {
    return this.descriptionIn;
  }

  public String getDescriptionOut()
  {
    return this.descriptionOut;
  }

  public void setSerialNumOutMan(String serialNumOutMan)
  {
    this.serialNumOutMan = serialNumOutMan;
  }

  public String getSerialNumOutMan()
  {
    return this.serialNumOutMan;
  }

  public void setSerialNumInMan(String serialNumInMan)
  {
    this.serialNumInMan = serialNumInMan;
  }

  public String getSerialNumInMan()
  {
    return this.serialNumInMan;
  }

  public void setSerialNumOutList(String serialNumOutList)
  {
    this.serialNumOutList = serialNumOutList;
  }

  public String getSerialNumOutList()
  {
    return this.serialNumOutList;
  }

  public void setSerialNumInList(String input)
  {
    int idx               = input.indexOf("@");
    this.serialNumInList  = input.substring(0,idx);
    this.partNum          = input.substring(idx+1);
  }

  public String getSerialNumInList()
  {
    return (this.serialNumInList + this.partNum);
  }

  public boolean  isInSubmitted()
  {
    return this.inSubmitted;
  }
  public boolean  isOutSubmitted()
  {
    return this.outSubmitted;
  }

  public void setShippingCostIn(String shippingCostIn)
  {
    this.shippingCostIn = shippingCostIn;
  }

  public String getShippingCostIn()
  {
    return this.shippingCostIn;
  }

  public void setShippingCostOut(String shippingCostOut)
  {
    this.shippingCostOut = shippingCostOut;
  }

  public String getShippingCostOut()
  {
    return this.shippingCostOut;
  }

  public void setSwapNum(String swapNum)
  {
    this.swapNum = swapNum;
  }

  public String getSwapNum()
  {
    return this.swapNum;
  }

}/*@lineinfo:generated-code*/