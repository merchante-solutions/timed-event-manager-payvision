/*@lineinfo:filename=RepairSearchBeanNew*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/RepairSearchBeanNew.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/23/04 3:11p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.inventory;

import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class RepairSearchBeanNew extends DateSQLJBean
{
  public class RepairData
  {
    public String repairRefNum;
    public String serialNum;
    public String product;
    public String repairDate;
    public String repairDateSorted;
    public String callTagNum;
    public String repairCompany;
    public String repairStatus;

    public RepairData(ResultSet rs)
    {
      try
      {
        repairRefNum        = processStringField(rs.getString("repair_ref_num"));
        serialNum           = processStringField(rs.getString("serial_num"));
        product             = processStringField(rs.getString("product"));
        repairDate          = DateTimeFormatter.getFormattedDate(rs.getTimestamp("repair_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT);
        repairDateSorted    = DateTimeFormatter.getFormattedDate(rs.getTimestamp("repair_date"), "yyyyMMddHHmmss");
        callTagNum          = processStringField(rs.getString("call_tag_num"));
        repairCompany       = processStringField(rs.getString("repair_company"));
        repairStatus        = processStringField(rs.getString("repair_status"));
      }
      catch(Exception e)
      {
        System.out.println("repairDate Constructor: " + e.toString());
      }
    }
    
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }

  }
  
  public class RepairDataComparator
    implements Comparator
  {
    public final static int   SB_REPAIR_DATE          = 0;
    public final static int   SB_REPAIR_REF_NUM       = 1;
    public final static int   SB_PRODUCT              = 2;
    public final static int   SB_SERIAL_NUM           = 3;
    public final static int   SB_CALL_TAG             = 4;
    public final static int   SB_REPAIR_COMPANY       = 5;
    public final static int   SB_REPAIR_STATUS        = 6;

    private int               sortBy;
    
    private boolean           sortAscending       = false;
    
    public RepairDataComparator()
    {
      this.sortBy = SB_REPAIR_DATE;
    }
    
    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }
    
    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_REPAIR_DATE && sortBy <= SB_REPAIR_STATUS)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }
        
        this.sortBy = sortBy;
      }
    }
  
    int compare(RepairData o1, RepairData o2)
    {
      int result    = 0;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_REPAIR_DATE:
            compareString1 = o1.repairDateSorted + o1.repairRefNum;
            compareString2 = o2.repairDateSorted + o2.repairRefNum;
            break;
          
          case SB_REPAIR_REF_NUM:
            compareString1 = o1.repairRefNum + o1.repairDateSorted;
            compareString2 = o2.repairRefNum + o2.repairDateSorted;
            break;
            
          case SB_PRODUCT:
            compareString1 = o1.product + o1.repairDateSorted;
            compareString2 = o2.product + o2.repairDateSorted;
            break;
          
          case SB_SERIAL_NUM:
            compareString1 = o1.serialNum + o1.repairDateSorted;
            compareString2 = o2.serialNum + o2.repairDateSorted;
            break;
            
          case SB_CALL_TAG:
            compareString1 = o1.callTagNum + o1.repairDateSorted;
            compareString2 = o2.callTagNum + o2.repairDateSorted;
            break;
            
          case SB_REPAIR_COMPANY:
            compareString1 = o1.repairCompany + o1.repairDateSorted;
            compareString2 = o2.repairCompany + o2.repairDateSorted;
            break;
            
          case SB_REPAIR_STATUS:
            compareString1 = o1.repairStatus + o1.repairDateSorted;
            compareString2 = o2.repairStatus + o2.repairDateSorted;
            break;
          
          default:
            break;
        }
        
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(RepairData o1, RepairData o2)
    {
      boolean result    = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_REPAIR_DATE:
            compareString1 = o1.repairDateSorted + o1.repairRefNum;
            compareString2 = o2.repairDateSorted + o2.repairRefNum;
            break;
          
          case SB_REPAIR_REF_NUM:
            compareString1 = o1.repairRefNum + o1.repairDateSorted;
            compareString2 = o2.repairRefNum + o2.repairDateSorted;
            break;
            
          case SB_PRODUCT:
            compareString1 = o1.product + o1.repairDateSorted;
            compareString2 = o2.product + o2.repairDateSorted;
            break;
          
          case SB_SERIAL_NUM:
            compareString1 = o1.serialNum + o1.repairDateSorted;
            compareString2 = o2.serialNum + o2.repairDateSorted;
            break;
            
          case SB_CALL_TAG:
            compareString1 = o1.callTagNum + o1.repairDateSorted;
            compareString2 = o2.callTagNum + o2.repairDateSorted;
            break;
            
          case SB_REPAIR_COMPANY:
            compareString1 = o1.repairCompany + o1.repairDateSorted;
            compareString2 = o2.repairCompany + o2.repairDateSorted;
            break;
            
          case SB_REPAIR_STATUS:
            compareString1 = o1.repairStatus + o1.repairDateSorted;
            compareString2 = o2.repairStatus + o2.repairDateSorted;
            break;
          
          default:
            break;
        }

        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((RepairData)o1, (RepairData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((RepairData)o1, (RepairData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }

  public static final int REPAIR_STATUS_ALL             = -1;
  public static final int REPAIR_STATUS_INCOMPLETE      = 0;
  public static final int REPAIR_STATUS_COMPLETE        = 1;

  public static final int REPAIR_COMPANY_ALL                = -1;
  public static final int REPAIR_COMPANY_POSPORTAL          = 11;
  public static final int REPAIR_COMPANY_VMS                = 16;
  public static final int REPAIR_COMPANY_OTHER              = 8;
  public static final int REPAIR_COMPANY_HYPERCOM           = 21;
  public static final int REPAIR_COMPANY_GCF                = 22;
  public static final int REPAIR_COMPANY_ALLIANCE           = 23;
  public static final int REPAIR_COMPANY_DISCOVER_ALLIANCE  = 25;
  public static final int REPAIR_COMPANY_ENCRYPTION_VMS     = 24;
  public static final int REPAIR_COMPANY_CDE_SERVICES       = 29;

  private String              lookupValue           = "";
  private String              lastLookupValue       = "";
  
  private int                 action                = mesConstants.LU_ACTION_INVALID;
  private String              actionDescription     = "Invalid Action";
  
  private RepairDataComparator rdc                  = new RepairDataComparator();
  private Vector              lookupResults         = new Vector();
  private TreeSet             sortedResults         = null;
  
  private boolean             submitted             = false;
  private boolean             refresh               = false;
  private boolean             edit                  = false;
  
  //search criteria
  private String              startNew              = "";
  private String              lastStartNew          = "";

  private int                 repairCompany         = -1;
  private int                 lastRepairCompany     = -1;

  private int                 repairStatus          = -1;
  private int                 lastRepairStatus      = -1;

  private String              product               = "-1";
  private String              lastProduct           = "-1";

  public  Vector              productModel          = new Vector();
  public  Vector              productModelDesc      = new Vector();

  public void RepairSearchBeanNew()
  {
    fillDropDowns();
    initDate(-1, 0);
  }
  
  public void forceRefresh(boolean refresh)
  {
    this.refresh = refresh;
  }
  
  public boolean hasLookupChanged()
  {
    boolean result = false;
    
    if( refresh                               ||
        startNew        != lastStartNew       ||
        repairCompany   != lastRepairCompany  ||
        repairStatus    != lastRepairStatus   ||
        product         != lastProduct        ||
        ! product.equals(lastProduct)         ||
        ! lookupValue.equals(lastLookupValue)   
      )
    {
      result = true;
    }
    
    return result;
  }
  
  /*
  ** METHOD fillDropDowns
  **
  ** Fills the drop down boxes
  */  
  public void fillDropDowns()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
    
      productModelDesc.clear();
      productModel.clear();
    

      // equip models
      productModelDesc.add("All Equip");
      productModel.add("-1");

      /*@lineinfo:generated-code*//*@lineinfo:390^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   equip_model,
//                   equip_descriptor
//          from     equipment
//          where    equiptype_code not in (7,8,9) and
//                   used_in_inventory = 'Y'
//          order by equip_descriptor asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   equip_model,\n                 equip_descriptor\n        from     equipment\n        where    equiptype_code not in (7,8,9) and\n                 used_in_inventory = 'Y'\n        order by equip_descriptor asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.RepairSearchBeanNew",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.RepairSearchBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:398^7*/
    
      rs = it.getResultSet();
    
      while(rs.next())
      {
        productModelDesc.add(rs.getString("equip_descriptor"));
        productModel.add(rs.getString("equip_model"));
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("fillDropDowns()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CALL_TRACKING);
    }
  }
  
  /*
  ** METHOD public void getData()
  **
  */
  public void getData()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      connect();
      
      if (hasLookupChanged())
      {
        // set up longLookup value (numeric version of lookup value)
        long longLookup;
        try
        {
          longLookup = Long.parseLong(lookupValue.trim());
        }
        catch (Exception e) 
        {
          longLookup = -1;
        }
      
        // add wildcards to stringLookup so we can match partial strings
        String stringLookup = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup = "passall";
        }
        else
        {
          stringLookup = "%" + lookupValue.toUpperCase() + "%";
        }

        // no wildcards to stringLookup
        String stringLookup2 = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup2 = "passall";
        }
        else
        {
          stringLookup2 = lookupValue.toUpperCase();
        }

        /*@lineinfo:generated-code*//*@lineinfo:484^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  er.repair_ref_num                                   repair_ref_num,
//                    er.serial_num                                       serial_num,
//                    es.equip_status_desc                                repair_company,
//                    decode(er.repair_status,1,'Complete','Incomplete')  repair_status,
//                    er.call_tag_num                                     call_tag_num,
//                    er.date_repair_order_initiated                      repair_date,
//                    eq.equip_descriptor                                 product
//            from    equip_repair                    er,
//                    equip_status                    es,
//                    equipment                       eq
//            where   er.part_num       = eq.equip_model      and 
//                    er.repair_company = es.equip_status_id  and
//                    (-1   = :repairCompany or er.repair_company = :repairCompany) and
//                    (-1   = :repairStatus  or er.repair_status  = :repairStatus)  and
//                    ('-1' = :product       or er.part_num       = :product)       and
//                    (
//                      'passall'                 = :stringLookup   or
//                      upper(er.serial_num)      = :stringLookup2  or
//                      upper(er.call_tag_num)    = :stringLookup2  or
//                      er.repair_ref_num         = :longLookup     --or
//                      --er.repair_ref_num         in (select to_number(ref_num_serial_num) from equip_merchant_tracking where merchant_number = :longLookup and action = 3 and ref_num_serial_num is not null)
//                    )          
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  er.repair_ref_num                                   repair_ref_num,\n                  er.serial_num                                       serial_num,\n                  es.equip_status_desc                                repair_company,\n                  decode(er.repair_status,1,'Complete','Incomplete')  repair_status,\n                  er.call_tag_num                                     call_tag_num,\n                  er.date_repair_order_initiated                      repair_date,\n                  eq.equip_descriptor                                 product\n          from    equip_repair                    er,\n                  equip_status                    es,\n                  equipment                       eq\n          where   er.part_num       = eq.equip_model      and \n                  er.repair_company = es.equip_status_id  and\n                  (-1   =  :1  or er.repair_company =  :2 ) and\n                  (-1   =  :3   or er.repair_status  =  :4 )  and\n                  ('-1' =  :5        or er.part_num       =  :6 )       and\n                  (\n                    'passall'                 =  :7    or\n                    upper(er.serial_num)      =  :8   or\n                    upper(er.call_tag_num)    =  :9   or\n                    er.repair_ref_num         =  :10      --or\n                    --er.repair_ref_num         in (select to_number(ref_num_serial_num) from equip_merchant_tracking where merchant_number = :longLookup and action = 3 and ref_num_serial_num is not null)\n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.inventory.RepairSearchBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,repairCompany);
   __sJT_st.setInt(2,repairCompany);
   __sJT_st.setInt(3,repairStatus);
   __sJT_st.setInt(4,repairStatus);
   __sJT_st.setString(5,product);
   __sJT_st.setString(6,product);
   __sJT_st.setString(7,stringLookup);
   __sJT_st.setString(8,stringLookup2);
   __sJT_st.setString(9,stringLookup2);
   __sJT_st.setLong(10,longLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.inventory.RepairSearchBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:508^9*/
        
        lookupResults.clear();
      
        rs = it.getResultSet();
        
        while(rs.next())
        {
          // add all results to the lookupResults
          lookupResults.add(new RepairData(rs));
        }
      
        rs.close();
        it.close();
        
        lastStartNew        = startNew; 
        lastRepairCompany   = repairCompany;
        lastRepairStatus    = repairStatus;
        lastProduct         = product;
        lastLookupValue     = lookupValue;
      }
      
      // now create the sorted tree
      if(sortedResults != null)
      {
        sortedResults.clear();
      }
      
      sortedResults = null;
      
      sortedResults = new TreeSet(rdc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }
  
  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public String getActionDescription()
  {
    return this.actionDescription;
  }
  
  /*
  ** ACCESSORS
  */
  public String getLookupValue()
  {
    return lookupValue;
  }
  public void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }
  
  public int getAction()
  {
    return this.action;
  }
  public void setAction(int action)
  {
    this.actionDescription = "Repair Search";
  }
  public void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }
    
    setAction(actionNum);
  }
  
  public void setSortBy(String sortBy)
  {
    try
    {
      rdc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }
 
  public void setProduct(String product)
  {
    this.product = product;
  }
 
  public String getProduct()
  {
    return this.product;
  }

  public void setRepairCompany(String repairCompany)
  {
    try
    {
      this.repairCompany = Integer.parseInt(repairCompany);
    }
    catch(Exception e)
    {
    }
  }
 
  public String getRepairCompany()
  {
    return Integer.toString(this.repairCompany);
  }

  public void setRepairStatus(String repairStatus)
  {
    try
    {
      this.repairStatus = Integer.parseInt(repairStatus);
    }
    catch(Exception e)
    {
    }
  }
 
  public String getRepairStatus()
  {
    return Integer.toString(this.repairStatus);
  }


  public String encode(String url)
  {
    String body = url;
    try
    {
      StringBuffer    bodyBuff = null;
      int             index = 0;
      
      index = body.indexOf(' ');
      
      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"%20");
        body = bodyBuff.toString();
        index = body.indexOf(' ', index);
      }
    }
    catch(Exception e)
    {
      System.out.println("error will robinson %20");
    }
    return body;
  }
 
  public void setSubmitted(String submitted)
  {
    this.submitted = true;
  }

  public void setStartNew(String startNew)
  {
    this.startNew = startNew;
  }
 
  public boolean isSubmitted()
  {
    return this.submitted;
  }
}/*@lineinfo:generated-code*/