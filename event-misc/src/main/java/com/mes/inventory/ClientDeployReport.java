/*@lineinfo:filename=ClientDeployReport*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/ClientDeployReport.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/23/04 3:07p $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.inventory;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class ClientDeployReport extends DateSQLJBean
{
  public class EquipmentData
  {
    private String      serialNumber;
    private String      partNumber;
    private String      movedFromCode;
    private String      movedToCode;
    private String      statusCode;
    private String      equipType;
    private String      equipDesc;
    private String      cost;
    private Timestamp   transferDateSorted;
    private String      transferDate;
    private String      movedFrom;
    private String      movedTo;
    private String      transferCondition;

    public EquipmentData()
    {
      serialNumber        = "";
      partNumber          = "";
      movedFromCode       = "";
      movedToCode         = "";
      statusCode          = "";
      equipType           = "";
      equipDesc           = "";
      cost                = "";
      transferDateSorted  = null;
      transferDate        = "";
      movedFrom           = "";
      movedTo             = "";
      transferCondition   = "";
    }
    
    public EquipmentData( String     serialNumber,
                          String     partNumber,
                          String     movedFromCode,
                          String     movedToCode,
                          String     statusCode,
                          String     equipType,
                          String     equipDesc,
                          String     cost,
                          Timestamp  transferDateSorted,
                          String     transferDate,
                          String     movedFrom,
                          String     movedTo,
                          String     transferCondition)
    {
      setSerialNumber       (serialNumber);
      setPartNumber         (partNumber);
      setMovedFromCode      (movedFromCode);
      setMovedToCode        (movedToCode);
      setStatusCode         (statusCode);
      setEquipType          (equipType);
      setEquipDesc          (equipDesc);
      setCost               (cost);
      setTransferDateSorted (transferDateSorted);
      setTransferDate       (transferDate);
      setMovedFrom          (movedFrom);
      setMovedTo            (movedTo);
      setTransferCondition  (transferCondition);
    }
    
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }
    
    public void setSerialNumber(String serialNumber)
    {
      this.serialNumber = processStringField(serialNumber);
    }
    public String getSerialNumber()
    {
      return this.serialNumber;
    }

    public void setPartNumber(String partNumber)
    {
      this.partNumber = processStringField(partNumber);
    }
    public String getPartNumber()
    {
      return this.partNumber;
    }

    public void setMovedFromCode(String movedFromCode)
    {
      this.movedFromCode = processStringField(movedFromCode);
    }
    public String getMovedFromCode()
    {
      return this.movedFromCode;
    }

    public void setMovedToCode(String movedToCode)
    {
      this.movedToCode = processStringField(movedToCode);
    }
    public String getMovedToCode()
    {
      return this.movedToCode;
    }

    public void setStatusCode(String statusCode)
    {
      this.statusCode = processStringField(statusCode);
    }
    public String getStatusCode()
    {
      return this.statusCode;
    }

    public void setEquipType(String equipType)
    {
      this.equipType = processStringField(equipType);
    }
    public String getEquipType()
    {
      return this.equipType;
    }

    public void setEquipDesc(String equipDesc)
    {
      this.equipDesc = processStringField(equipDesc);
    }
    public String getEquipDesc()
    {
      return this.equipDesc;
    }

    public void setCost(String cost)
    {
      this.cost = processStringField(cost);
    }
    public String getCost()
    {
      return this.cost;
    }

    public void setTransferDateSorted(Timestamp transferDateSorted)
    {
      this.transferDateSorted = transferDateSorted;
    }
    public String getTransferDateSorted()
    {
      return DateTimeFormatter.getFormattedDate(transferDateSorted, "yyyyMMddHHmmss");
    }

    public void setTransferDate(String transferDate)
    {
      this.transferDate = processStringField(transferDate);
    }
    public String getTransferDate()
    {
      return this.transferDate;
    }

    public void setMovedFrom(String movedFrom)
    {
      this.movedFrom = processStringField(movedFrom);
    }
    public String getMovedFrom()
    {
      return this.movedFrom;
    }

    public void setMovedTo(String movedTo)
    {
      this.movedTo = processStringField(movedTo);
    }
    public String getMovedTo()
    {
      return this.movedTo;
    }

    public void setTransferCondition(String transferCondition)
    {
      this.transferCondition = processStringField(transferCondition);
    }
    public String getTransferCondition()
    {
      return this.transferCondition;
    }
  }

  public class EquipDataComparator
    implements Comparator
  {
    public final static int   SB_SERIAL_NUMBER        = 0;
    public final static int   SB_EQUIP_TYPE           = 1;
    public final static int   SB_EQUIP_DESC           = 2;
    public final static int   SB_TRANSFER_DATE        = 3;
    public final static int   SB_MOVED_FROM           = 4;
    public final static int   SB_MOVED_TO             = 5;
    public final static int   SB_TRANSFER_CONDITION   = 6;
    public final static int   SB_COST                 = 7;

    private int               sortBy;
    
    private boolean           sortAscending       = false;
    
    public EquipDataComparator()
    {
      this.sortBy = SB_TRANSFER_DATE;
    }
    
    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }
    
    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_SERIAL_NUMBER && sortBy <= SB_COST)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }
        
        this.sortBy = sortBy;
      }
    }
  
    int compare(EquipmentData o1, EquipmentData o2)
    {
      int result    = 0;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {

          case SB_SERIAL_NUMBER:
            compareString1 = o1.getSerialNumber()       + o1.getTransferDateSorted();
            compareString2 = o2.getSerialNumber()       + o2.getTransferDateSorted();
            break;
          
          case SB_EQUIP_TYPE:
            compareString1 = o1.getEquipType()          + o1.getTransferDateSorted();
            compareString2 = o2.getEquipType()          + o2.getTransferDateSorted();
            break;

          case SB_EQUIP_DESC:
            compareString1 = o1.getEquipDesc()          + o1.getTransferDateSorted();
            compareString2 = o2.getEquipDesc()          + o2.getTransferDateSorted();
            break;
            
          case SB_TRANSFER_DATE:
            compareString1 = o1.getTransferDateSorted() + o1.getSerialNumber();
            compareString2 = o2.getTransferDateSorted() + o2.getSerialNumber();
            break;
          
          case SB_MOVED_FROM:
            compareString1 = o1.getMovedFrom()          + o1.getTransferDateSorted();
            compareString2 = o2.getMovedFrom()          + o2.getTransferDateSorted();
            break;
            
          case SB_MOVED_TO:
            compareString1 = o1.getMovedTo()            + o1.getTransferDateSorted();
            compareString2 = o2.getMovedTo()            + o2.getTransferDateSorted();
            break;
            
          case SB_TRANSFER_CONDITION:
            compareString1 = o1.getTransferCondition()  + o1.getTransferDateSorted();
            compareString2 = o2.getTransferCondition()  + o2.getTransferDateSorted();
            break;

          case SB_COST:
            compareString1 = o1.getCost()  + o1.getTransferDateSorted();
            compareString2 = o2.getCost()  + o2.getTransferDateSorted();
            break;

          default:
            break;
        }
        
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(EquipmentData o1, EquipmentData o2)
    {
      boolean result    = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_SERIAL_NUMBER:
            compareString1 = o1.getSerialNumber()       + o1.getTransferDateSorted();
            compareString2 = o2.getSerialNumber()       + o2.getTransferDateSorted();
            break;
          
          case SB_EQUIP_TYPE:
            compareString1 = o1.getEquipType()          + o1.getTransferDateSorted();
            compareString2 = o2.getEquipType()          + o2.getTransferDateSorted();
            break;

          case SB_EQUIP_DESC:
            compareString1 = o1.getEquipDesc()          + o1.getTransferDateSorted();
            compareString2 = o2.getEquipDesc()          + o2.getTransferDateSorted();
            break;
            
          case SB_TRANSFER_DATE:
            compareString1 = o1.getTransferDateSorted() + o1.getSerialNumber();
            compareString2 = o2.getTransferDateSorted() + o2.getSerialNumber();
            break;
          
          case SB_MOVED_FROM:
            compareString1 = o1.getMovedFrom()          + o1.getTransferDateSorted();
            compareString2 = o2.getMovedFrom()          + o2.getTransferDateSorted();
            break;
            
          case SB_MOVED_TO:
            compareString1 = o1.getMovedTo()            + o1.getTransferDateSorted();
            compareString2 = o2.getMovedTo()            + o2.getTransferDateSorted();
            break;
            
          case SB_TRANSFER_CONDITION:
            compareString1 = o1.getTransferCondition()  + o1.getTransferDateSorted();
            compareString2 = o2.getTransferCondition()  + o2.getTransferDateSorted();
            break;

          case SB_COST:
            compareString1 = o1.getCost()  + o1.getTransferDateSorted();
            compareString2 = o2.getCost()  + o2.getTransferDateSorted();
            break;

          default:
            break;
        }

        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((EquipmentData)o1, (EquipmentData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((EquipmentData)o1, (EquipmentData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }

  private String              lookupValue         = "";
  private String              lastLookupValue     = "";
  
  private int                 action              = mesConstants.LU_ACTION_INVALID;
  private String              actionDescription   = "Invalid Action";
  
  private EquipDataComparator edc                 = new EquipDataComparator();
  private Vector              lookupResults       = new Vector();
  private TreeSet             sortedResults       = null;
  
  private boolean             submitted           = false;
  private boolean             refresh             = false;
  
  //search criteria
  private int                 owner               = -1;
  private int                 lastOwner           = -1;
  
  private String              equipType           = "-1";
  private String              lastEquipType       = "-1";

  private String              equipModel          = "-1";
  private String              lastEquipModel      = "-1";

  private int                 lastFromMonth       = -1;
  private int                 lastFromDay         = -1;
  private int                 lastFromYear        = -1;
  private int                 lastToMonth         = -1;
  private int                 lastToDay           = -1;
  private int                 lastToYear          = -1;
  
  public  Vector              clients              = new Vector();
  public  Vector              clientValues         = new Vector();

  public  Vector              equipTypes           = new Vector();
  public  Vector              equipTypeValues      = new Vector();

  public  Vector              equipModels          = new Vector();
  public  Vector              equipModelValues     = new Vector();

  public ClientDeployReport()
  {
    fillDropDowns();
    initDate(-1, 0);
  }
  
  public synchronized void forceRefresh(boolean refresh)
  {
    this.refresh = refresh;
  }
  
  private boolean hasLookupChanged()
  {
    boolean result = false;
    
    if( refresh                               ||
        owner           != lastOwner          ||
        fromMonth       != lastFromMonth      ||
        fromDay         != lastFromDay        ||
        fromYear        != lastFromYear       ||
        toMonth         != lastToMonth        ||
        toDay           != lastToDay          ||
        toYear          != lastToYear         ||
        ! equipType.equals(lastEquipType)     ||
        ! equipModel.equals(lastEquipModel)   ||
        ! lookupValue.equals(lastLookupValue)   
      )
    {
      result = true;
    }
    
    return result;
  }
  
  /*
  ** METHOD fillDropDowns
  **
  ** Fills the drop down boxes
  */  
  public synchronized void fillDropDowns()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      if(clients.size() == 0)
      {
        clients.clear();
        clientValues.clear();
        equipTypes.clear();
        equipTypeValues.clear();
        equipModels.clear();
        equipModelValues.clear();
      
        // clients
        clients.add("All Clients");
        clientValues.add("-1");
        /*@lineinfo:generated-code*//*@lineinfo:552^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  inventory_owner_name, app_type_code 
//            from    app_type
//            where   SEPARATE_INVENTORY = 'Y'and app_type_code != :mesConstants.APP_TYPE_MES
//            order by inventory_owner_name asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  inventory_owner_name, app_type_code \n          from    app_type\n          where   SEPARATE_INVENTORY = 'Y'and app_type_code !=  :1 \n          order by inventory_owner_name asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.ClientDeployReport",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.APP_TYPE_MES);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.ClientDeployReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:558^9*/
      
        rs = it.getResultSet();
        while(rs.next())
        {
          clients.add(rs.getString("inventory_owner_name"));
          clientValues.add(rs.getString("app_type_code"));
        }
      
        rs.close();
        it.close();
      
        // equip types
        equipTypes.add("All Equip Types");
        equipTypeValues.add("-1");

        /*@lineinfo:generated-code*//*@lineinfo:574^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    equiptype
//            where   equiptype_code not in (7,8,9)
//            order by equiptype_description asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    equiptype\n          where   equiptype_code not in (7,8,9)\n          order by equiptype_description asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.inventory.ClientDeployReport",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.inventory.ClientDeployReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:580^9*/
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          equipTypes.add(rs.getString("equiptype_description"));
          equipTypeValues.add(rs.getString("equiptype_code"));
        }
        
        rs.close();
        it.close();

        // equip models
        equipModels.add("All Equip Desc");
        equipModelValues.add("-1");

        /*@lineinfo:generated-code*//*@lineinfo:597^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_model,
//                    equip_descriptor
//            from    equipment
//            where   equiptype_code not in (7,8,9) and
//                    used_in_inventory = 'Y'
//            order by equip_descriptor asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_model,\n                  equip_descriptor\n          from    equipment\n          where   equiptype_code not in (7,8,9) and\n                  used_in_inventory = 'Y'\n          order by equip_descriptor asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.inventory.ClientDeployReport",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.inventory.ClientDeployReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:605^9*/
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          equipModels.add(rs.getString("equip_descriptor"));
          equipModelValues.add(rs.getString("equip_model"));
        }
        
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("fillDropDowns()", e.toString());
      cleanUp();
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private boolean equipNotDeployed(String serialNum, String partNum)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             result  = false;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:640^7*/

//  ************************************************************
//  #sql [Ctx] it = { select * 
//          from    equip_inventory
//          where   ei_serial_number = :serialNum and
//                  ei_part_number   = :partNum   and
//                  ei_deployed_date is null      and
//                  ei_merchant_number is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select * \n        from    equip_inventory\n        where   ei_serial_number =  :1  and\n                ei_part_number   =  :2    and\n                ei_deployed_date is null      and\n                ei_merchant_number is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.inventory.ClientDeployReport",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serialNum);
   __sJT_st.setString(2,partNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.inventory.ClientDeployReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:648^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }
    
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("equipNotDeployed()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }

    return result;
  }

  /*
  ** METHOD cancelTransfer
  **
  ** Fills the drop down boxes
  */  
  public synchronized boolean cancelTransfer(String serialNum, String partNum, String fromClient, String fromClientDesc, String toClient, String toClientDesc, String status, String userId)
  {
    boolean result = false;

    try
    {
      connect();
      
      if(equipNotDeployed(serialNum,partNum))
      {
        //set cancel flag in equip_client_deployment so it doesnt show up in report
        /*@lineinfo:generated-code*//*@lineinfo:689^9*/

//  ************************************************************
//  #sql [Ctx] { update equip_client_deployment
//            set    cancel_date   = sysdate
//            where  serial_number = :serialNum  and
//                   part_number   = :partNum    and
//                   from_client   = :fromClient and
//                   to_client     = :toClient   
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update equip_client_deployment\n          set    cancel_date   = sysdate\n          where  serial_number =  :1   and\n                 part_number   =  :2     and\n                 from_client   =  :3  and\n                 to_client     =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.inventory.ClientDeployReport",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serialNum);
   __sJT_st.setString(2,partNum);
   __sJT_st.setString(3,fromClient);
   __sJT_st.setString(4,toClient);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:697^9*/
      
        //change the equipment owner in equip_inventory so that it goes back into the client it use to be
        /*@lineinfo:generated-code*//*@lineinfo:700^9*/

//  ************************************************************
//  #sql [Ctx] { update equip_inventory
//            set    ei_owner         = :fromClient
//            where  ei_serial_number = :serialNum  and
//                   ei_part_number   = :partNum    and
//                   ei_owner         = :toClient   
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update equip_inventory\n          set    ei_owner         =  :1 \n          where  ei_serial_number =  :2   and\n                 ei_part_number   =  :3     and\n                 ei_owner         =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.inventory.ClientDeployReport",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,fromClient);
   __sJT_st.setString(2,serialNum);
   __sJT_st.setString(3,partNum);
   __sJT_st.setString(4,toClient);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:707^9*/

        String actionDesc = "Equipment transfer from " + fromClientDesc + " to " + toClientDesc + " cancelled.";
      
        //add to history
        /*@lineinfo:generated-code*//*@lineinfo:712^9*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_history 
//            (
//              part_number, 
//              eh_serial_number,
//              eh_user,
//              eh_date,
//              eh_action_performed,
//              eh_note,
//              equip_status,
//              owner
//            ) 
//            values
//            (
//              :partNum,
//              :serialNum,
//              :userId,
//              sysdate,
//              :actionDesc,
//              '',
//              :status,
//              :toClient
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_history \n          (\n            part_number, \n            eh_serial_number,\n            eh_user,\n            eh_date,\n            eh_action_performed,\n            eh_note,\n            equip_status,\n            owner\n          ) \n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n            sysdate,\n             :4 ,\n            '',\n             :5 ,\n             :6 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.inventory.ClientDeployReport",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,partNum);
   __sJT_st.setString(2,serialNum);
   __sJT_st.setString(3,userId);
   __sJT_st.setString(4,actionDesc);
   __sJT_st.setString(5,status);
   __sJT_st.setString(6,toClient);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:736^9*/

        result = true;
      }
    }
    catch(Exception e)
    {
      logEntry("cancel_transfer()", e.toString());
      result = false;
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public synchronized void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CALL_TRACKING);
    }
  }
  
  /*
  ** METHOD public void getData()
  **
  */
  public synchronized void getData()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      connect();
      
      if (hasLookupChanged())
      {
        // set up longLookup value (numeric version of lookup value)
        long longLookup;
        try
        {
          longLookup = Long.parseLong(lookupValue.trim());
        }
        catch (Exception e) 
        {
          longLookup = -1;
        }
      
        // add wildcards to stringLookup so we can match partial strings
        String stringLookup = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup = "passall";
        }
        else
        {
          stringLookup = "%" + lookupValue.toUpperCase() + "%";
        }

        // no wildcards to stringLookup
        String stringLookup2 = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup2 = "passall";
        }
        else
        {
          stringLookup2 = lookupValue.toUpperCase();
        }

        
        Date fromDate = getSqlFromDate();
        Date toDate   = getSqlToDate(); 
        
        /*@lineinfo:generated-code*//*@lineinfo:819^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_serial_number             serial_number,
//                    ei.ei_part_number               part_number,
//                    ei.ei_unit_cost                 unit_cost,
//                    ei.ei_status                    status,
//                    et.equiptype_description        equip_type,
//                    eq.equip_descriptor             equip_desc,
//                    ecd.move_date                   transfer_date,
//                    at1.inventory_owner_name        moved_from,
//                    at1.app_type_code               moved_from_code,
//                    at2.inventory_owner_name        moved_to,
//                    at2.app_type_code               moved_to_code,
//                    ec.ec_class_name                transfer_condition
//            from    equipment                       eq,
//                    equiptype                       et,
//                    equip_inventory                 ei,
//                    equip_class                     ec,
//                    app_type                        at1,
//                    app_type                        at2,
//                    equip_client_deployment         ecd
//            where   trunc(ecd.move_date) between :fromDate and :toDate        and
//                    ecd.serial_number     = ei.ei_serial_number               and
//                    ei.EI_PART_NUMBER     = eq.equip_model                    and
//                    ecd.condition         = ec.ec_class_id                    and
//                    ecd.from_client       = at1.app_type_code                 and 
//                    ecd.to_client         = at2.app_type_code                 and
//                    eq.equiptype_code     = et.equiptype_code                 and 
//                    (-1   = :owner      or ecd.to_client      = :owner)       and
//                    ('-1' = :equipType  or eq.equiptype_code  = :equipType)   and
//                    ('-1' = :equipModel or eq.equip_model     = :equipModel)  and
//                    ecd.cancel_date is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_serial_number             serial_number,\n                  ei.ei_part_number               part_number,\n                  ei.ei_unit_cost                 unit_cost,\n                  ei.ei_status                    status,\n                  et.equiptype_description        equip_type,\n                  eq.equip_descriptor             equip_desc,\n                  ecd.move_date                   transfer_date,\n                  at1.inventory_owner_name        moved_from,\n                  at1.app_type_code               moved_from_code,\n                  at2.inventory_owner_name        moved_to,\n                  at2.app_type_code               moved_to_code,\n                  ec.ec_class_name                transfer_condition\n          from    equipment                       eq,\n                  equiptype                       et,\n                  equip_inventory                 ei,\n                  equip_class                     ec,\n                  app_type                        at1,\n                  app_type                        at2,\n                  equip_client_deployment         ecd\n          where   trunc(ecd.move_date) between  :1  and  :2         and\n                  ecd.serial_number     = ei.ei_serial_number               and\n                  ei.EI_PART_NUMBER     = eq.equip_model                    and\n                  ecd.condition         = ec.ec_class_id                    and\n                  ecd.from_client       = at1.app_type_code                 and \n                  ecd.to_client         = at2.app_type_code                 and\n                  eq.equiptype_code     = et.equiptype_code                 and \n                  (-1   =  :3       or ecd.to_client      =  :4 )       and\n                  ('-1' =  :5   or eq.equiptype_code  =  :6 )   and\n                  ('-1' =  :7  or eq.equip_model     =  :8 )  and\n                  ecd.cancel_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.inventory.ClientDeployReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromDate);
   __sJT_st.setDate(2,toDate);
   __sJT_st.setInt(3,owner);
   __sJT_st.setInt(4,owner);
   __sJT_st.setString(5,equipType);
   __sJT_st.setString(6,equipType);
   __sJT_st.setString(7,equipModel);
   __sJT_st.setString(8,equipModel);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.inventory.ClientDeployReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:851^9*/
        
        lookupResults.clear();
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          lookupResults.add(new EquipmentData(rs.getString("serial_number"),  
                                              rs.getString("part_number"),
                                              rs.getString("moved_from_code"), 
                                              rs.getString("moved_to_code"), 
                                              rs.getString("status"),
                                              rs.getString("equip_type"), 
                                              rs.getString("equip_desc"),
                                              rs.getString("unit_cost"),
                                              rs.getTimestamp("transfer_date"),
                                              DateTimeFormatter.getFormattedDate(rs.getTimestamp("transfer_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT),
                                              rs.getString("moved_from"), 
                                              rs.getString("moved_to"), 
                                              rs.getString("transfer_condition")));
        }
      
        rs.close();
        it.close();
        
        lastOwner           = owner;
        lastEquipType       = equipType;
        lastEquipModel      = equipModel;
        lastFromMonth       = fromMonth;
        lastFromDay         = fromDay;
        lastFromYear        = fromYear;
        lastToMonth         = toMonth;
        lastToDay           = toDay;
        lastToYear          = toYear;
        lastLookupValue     = lookupValue;
      }
      
      // now create the sorted tree
      if(sortedResults != null)
      {
        sortedResults.clear();
      }
      
      sortedResults = null;
      
      sortedResults = new TreeSet(edc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public synchronized Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }
  
  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public synchronized String getActionDescription()
  {
    return this.actionDescription;
  }
  
  /*
  ** ACCESSORS
  */
  public synchronized String getLookupValue()
  {
    return lookupValue;
  }
  public synchronized void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }
  
  public synchronized int getAction()
  {
    return this.action;
  }
  public synchronized void setAction(int action)
  {
    this.actionDescription = "Transferred Equipment Report";
  }
  public void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }
    
    setAction(actionNum);
  }
  
  public synchronized void setSortBy(String sortBy)
  {
    try
    {
      edc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }
 
  public synchronized void setEquipType(String equipType)
  {
    this.equipType = equipType;
  }
 
  public synchronized String getEquipType()
  {
    return this.equipType;
  }

  public synchronized void setEquipModel(String equipModel)
  {
    this.equipModel = equipModel;
  }
 
  public String getEquipModel()
  {
    return this.equipModel;
  }

  
  public synchronized void setClient(String owner)
  {
    try
    {
      this.owner = Integer.parseInt(owner);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getClient()
  {
    return Integer.toString(this.owner);
  }
 
  public synchronized void setSubmitted(String submitted)
  {
    this.submitted = true;
  }
 
  public synchronized boolean isSubmitted()
  {
    return this.submitted;
  }
}/*@lineinfo:generated-code*/