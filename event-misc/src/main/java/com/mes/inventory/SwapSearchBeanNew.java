/*@lineinfo:filename=SwapSearchBeanNew*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/SwapSearchBeanNew.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/23/04 3:11p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.inventory;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class SwapSearchBeanNew extends DateSQLJBean
  implements Serializable
{
  public class SwapData
    implements Serializable
  {
    public String swapRefNum;
    public String merchantNum;
    public String merchantDba;
    public String swapDate;
    public String swapDateSorted;
    public String callTagNum;
    public String swapStatus;

    public SwapData(ResultSet rs)
    {
      try
      {
        swapRefNum          = processStringField(rs.getString("swap_ref_num"));
        merchantNum         = processStringField(rs.getString("merchant_num"));
        merchantDba         = processStringField(rs.getString("merchant_dba"));
        swapDate            = DateTimeFormatter.getFormattedDate(rs.getTimestamp("swap_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT);
        swapDateSorted      = DateTimeFormatter.getFormattedDate(rs.getTimestamp("swap_date"), "yyyyMMddHHmmss");
        callTagNum          = processStringField(rs.getString("call_tag_num"));
        swapStatus          = processStringField(rs.getString("swap_status"));
      }
      catch(Exception e)
      {
        System.out.println("swapDate Constructor: " + e.toString());
      }
    }
    
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }

  }

  public class SwapDataComparator
    implements Comparator, Serializable
  {
    public final static int   SB_SWAP_DATE            = 0;
    public final static int   SB_SWAP_REF_NUM         = 1;
    public final static int   SB_MERCHANT_NUMBER      = 2;
    public final static int   SB_MERCHANT_DBA         = 3;
    public final static int   SB_CALL_TAG             = 4;
    public final static int   SB_SWAP_STATUS          = 5;

    private int               sortBy;
    
    private boolean           sortAscending       = false;
    
    public SwapDataComparator()
    {
      this.sortBy = SB_SWAP_DATE;
    }
    
    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }
    
    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_SWAP_DATE && sortBy <= SB_SWAP_STATUS)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }
        
        this.sortBy = sortBy;
      }
    }
  
    int compare(SwapData o1, SwapData o2)
    {
      int result    = 0;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_SWAP_DATE:
            compareString1 = o1.swapDateSorted + o1.swapRefNum + o1.swapRefNum;
            compareString2 = o2.swapDateSorted + o2.swapRefNum + o2.swapRefNum;
            break;
          
          case SB_SWAP_REF_NUM:
            compareString1 = o1.swapRefNum + o1.swapDateSorted;
            compareString2 = o2.swapRefNum + o2.swapDateSorted;
            break;
            
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.merchantNum + o1.swapDateSorted + o1.swapRefNum;
            compareString2 = o2.merchantNum + o2.swapDateSorted + o2.swapRefNum;
            break;
          
          case SB_MERCHANT_DBA:
            compareString1 = o1.merchantDba + o1.swapDateSorted + o1.swapRefNum;
            compareString2 = o2.merchantDba + o2.swapDateSorted + o2.swapRefNum;
            break;
            
          case SB_CALL_TAG:
            compareString1 = o1.callTagNum + o1.swapDateSorted + o1.swapRefNum;
            compareString2 = o2.callTagNum + o2.swapDateSorted + o2.swapRefNum;
            break;
            
          case SB_SWAP_STATUS:
            compareString1 = o1.swapStatus + o1.swapDateSorted + o1.swapRefNum;
            compareString2 = o2.swapStatus + o2.swapDateSorted + o2.swapRefNum;
            break;
          
          default:
            break;
        }
        
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(SwapData o1, SwapData o2)
    {
      boolean result    = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_SWAP_DATE:
            compareString1 = o1.swapDateSorted + o1.swapRefNum + o1.swapRefNum;
            compareString2 = o2.swapDateSorted + o2.swapRefNum + o2.swapRefNum;
            break;
          
          case SB_SWAP_REF_NUM:
            compareString1 = o1.swapRefNum + o1.swapDateSorted;
            compareString2 = o2.swapRefNum + o2.swapDateSorted;
            break;
            
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.merchantNum + o1.swapDateSorted + o1.swapRefNum;
            compareString2 = o2.merchantNum + o2.swapDateSorted + o2.swapRefNum;
            break;
          
          case SB_MERCHANT_DBA:
            compareString1 = o1.merchantDba + o1.swapDateSorted + o1.swapRefNum;
            compareString2 = o2.merchantDba + o2.swapDateSorted + o2.swapRefNum;
            break;
            
          case SB_CALL_TAG:
            compareString1 = o1.callTagNum + o1.swapDateSorted + o1.swapRefNum;
            compareString2 = o2.callTagNum + o2.swapDateSorted + o2.swapRefNum;
            break;
            
          case SB_SWAP_STATUS:
            compareString1 = o1.swapStatus + o1.swapDateSorted + o1.swapRefNum;
            compareString2 = o2.swapStatus + o2.swapDateSorted + o2.swapRefNum;
            break;
          
          default:
            break;
        }

        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((SwapData)o1, (SwapData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((SwapData)o1, (SwapData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }

  public static final int SWAP_STATUS_ALL         = -1;
  public static final int SWAP_STATUS_INCOMPLETE  = 0;
  public static final int SWAP_STATUS_COMPLETE    = 1;

  private String              lookupValue           = "";
  private String              lastLookupValue       = "";
  
  private int                 action                = mesConstants.LU_ACTION_INVALID;
  private String              actionDescription     = "Invalid Action";
  
  private SwapDataComparator  sdc                   = new SwapDataComparator();
  private Vector              lookupResults         = new Vector();
  private TreeSet             sortedResults         = null;
  
  private boolean             submitted             = false;
  private boolean             refresh               = false;
  private boolean             edit                  = false;
  
  //search criteria
  private String              startNew              = "";
  private String              lastStartNew          = "";

  private int                 swapStatus            = -1;
  private int                 lastSwapStatus        = -1;

  public SwapSearchBeanNew()
  {
    fillDropDowns();
    initDate(-1, 0);
  }
  
  public synchronized void forceRefresh(boolean refresh)
  {
    this.refresh = refresh;
  }
  
  public synchronized boolean hasLookupChanged()
  {
    boolean result = false;
    
    if( refresh                               ||
        startNew        != lastStartNew       ||
        swapStatus      != lastSwapStatus     ||
        ! lookupValue.equals(lastLookupValue)   
      )
    {
      result = true;
    }
    
    return result;
  }
  
  /*
  ** METHOD fillDropDowns
  **
  ** Fills the drop down boxes
  */  
  public void fillDropDowns()
  {
  }
  
  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public synchronized void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CALL_TRACKING);
    }
  }
  
  /*
  ** METHOD public void getData()
  **
  */
  public synchronized void getData()
  {
    ResultSetIterator   it = null;
    ResultSet           rs = null;
    
    try
    {
      connect();
      
      if (hasLookupChanged())
      {
        // set up longLookup value (numeric version of lookup value)
        long longLookup;
        try
        {
          longLookup = Long.parseLong(lookupValue.trim());
        }
        catch (Exception e) 
        {
          longLookup = -1;
        }
      
        // add wildcards to stringLookup so we can match partial strings
        String stringLookup = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup = "passall";
        }
        else
        {
          stringLookup = "%" + lookupValue.toUpperCase() + "%";
        }

        // no wildcards to stringLookup
        String stringLookup2 = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup2 = "passall";
        }
        else
        {
          stringLookup2 = lookupValue.toUpperCase();
        }

        /*@lineinfo:generated-code*//*@lineinfo:406^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  es.swap_ref_num                                     swap_ref_num,
//                    es.merchant_num                                     merchant_num,
//                    mt.merchant_name                                    merchant_dba,
//                    decode(es.swap_status,1,'Complete','Incomplete')    swap_status,
//                    es.call_tag_num                                     call_tag_num,
//                    emt.action_date                                     swap_date
//            from    equip_swap                      es,
//                    equip_merchant_tracking         emt,
//                    merchant_types                  mt
//            where   es.swap_ref_num = emt.ref_num_serial_num  and 
//                    emt.action      = 2                       and
//                    es.merchant_num = mt.merchant_number      and
//                    (-1   = :swapStatus  or es.swap_status  = :swapStatus)  and
//                    (
//                      'passall'                 = :stringLookup     or
//                      upper(mt.merchant_name)   like :stringLookup  or
//                      upper(es.call_tag_num)    = :stringLookup2    or
//                      es.merchant_num           = :longLookup       or
//                      es.swap_ref_num           = :longLookup
//                    )          
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  es.swap_ref_num                                     swap_ref_num,\n                  es.merchant_num                                     merchant_num,\n                  mt.merchant_name                                    merchant_dba,\n                  decode(es.swap_status,1,'Complete','Incomplete')    swap_status,\n                  es.call_tag_num                                     call_tag_num,\n                  emt.action_date                                     swap_date\n          from    equip_swap                      es,\n                  equip_merchant_tracking         emt,\n                  merchant_types                  mt\n          where   es.swap_ref_num = emt.ref_num_serial_num  and \n                  emt.action      = 2                       and\n                  es.merchant_num = mt.merchant_number      and\n                  (-1   =  :1   or es.swap_status  =  :2 )  and\n                  (\n                    'passall'                 =  :3      or\n                    upper(mt.merchant_name)   like  :4   or\n                    upper(es.call_tag_num)    =  :5     or\n                    es.merchant_num           =  :6        or\n                    es.swap_ref_num           =  :7 \n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.SwapSearchBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,swapStatus);
   __sJT_st.setInt(2,swapStatus);
   __sJT_st.setString(3,stringLookup);
   __sJT_st.setString(4,stringLookup);
   __sJT_st.setString(5,stringLookup2);
   __sJT_st.setLong(6,longLookup);
   __sJT_st.setLong(7,longLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.SwapSearchBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:428^9*/
        
        lookupResults.clear();
      
        rs = it.getResultSet();
        
        while(rs.next())
        {
          // add all results to the lookupResults
          lookupResults.add(new SwapData(rs));
        }
      
        rs.close();
        it.close();
        
        lastStartNew        = startNew; 
        lastSwapStatus      = swapStatus;
        lastLookupValue     = lookupValue;
      }
      
      // now create the sorted tree
      if(sortedResults != null)
      {
        sortedResults.clear();
      }
      
      sortedResults = null;
      
      sortedResults = new TreeSet(sdc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public synchronized Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }
  
  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public String getActionDescription()
  {
    return this.actionDescription;
  }
  
  /*
  ** ACCESSORS
  */
  public String getLookupValue()
  {
    return lookupValue;
  }
  public synchronized void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }
  
  public int getAction()
  {
    return this.action;
  }
  public synchronized void setAction(int action)
  {
    this.actionDescription = "Swap Search";
  }
  public synchronized void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }
    
    setAction(actionNum);
  }
  
  public synchronized void setSortBy(String sortBy)
  {
    try
    {
      sdc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }
 
  public synchronized void setSwapStatus(String swapStatus)
  {
    try
    {
      this.swapStatus = Integer.parseInt(swapStatus);
    }
    catch(Exception e)
    {
    }
  }
 
  public String getSwapStatus()
  {
    return Integer.toString(this.swapStatus);
  }


  public synchronized String encode(String url)
  {
    String body = url;
    try
    {
      StringBuffer    bodyBuff = null;
      int             index = 0;
      
      index = body.indexOf(' ');
      
      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"%20");
        body = bodyBuff.toString();
        index = body.indexOf(' ', index);
      }
    }
    catch(Exception e)
    {
      System.out.println("error will robinson %20");
    }
    return body;
  }
 
  public synchronized void setSubmitted(String submitted)
  {
    this.submitted = true;
  }

  public synchronized void setStartNew(String startNew)
  {
    this.startNew = startNew;
  }
 
  public boolean isSubmitted()
  {
    return this.submitted;
  }

}/*@lineinfo:generated-code*/