package com.mes.inventory;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
// log4j
import org.apache.log4j.Logger;

public class FileUploadBean {

  /**
   * Log4J Logger
   */
  static Logger log = Logger.getLogger(FileUploadBean.class);

  private String savePath = "C:\\\\upload\\";
  private String filepath, filename, contentType;
  private EndOfDayUpload shipFile = null;
  private Dictionary fields;
  private boolean uploaded = false;

  public String getFilename()
  {
    return filename;
  }

  public String getContentType()
  {
    return contentType;
  }

  public String getFilepath()
  {
    return filepath;
  }

  public void setSavePath(String savePath)
  {
    this.savePath = savePath;
  }


  public String getFieldValue(String fieldName)
  {
    if (fields == null || fieldName == null)
      return null;
    return (String) fields.get(fieldName);
  }

  private void setFilename(String s) {
    if (s==null)
      return;

    int pos = s.indexOf("filename=\"");
    if (pos != -1) {
      filepath = s.substring(pos+10, s.length()-1);
      // Windows browsers include the full path on the client
      // But Linux/Unix and Mac browsers only send the filename
      // test if this is from a Windows browser
      pos = filepath.lastIndexOf("\\");
      if (pos != -1)
        filename = filepath.substring(pos + 1);
      else
        filename = filepath;

      //set the name so that notes will reflect shipper
      shipFile.setShipper(filename);
    }
  }

  private void setContentType(String s) {
    if (s==null)
      return;

    int pos = s.indexOf(": ");
    if (pos != -1)
      contentType = s.substring(pos+2, s.length());
  }

  public void doUpload(HttpServletRequest request) throws IOException
  {
    log.debug("in doUpload");

    shipFile = new EndOfDayUpload();

    ServletInputStream in = request.getInputStream();

    byte[] line = new byte[128];

    int i = in.readLine(line, 0, 128);

    if (i < 3)
    {
      log.debug("I was < 3");
      return;
    }

    int boundaryLength = i - 2;

    String boundary = new String(line, 0, boundaryLength); //-2 discards the newline character

    log.debug("boundary = " + boundary);

    fields = new Hashtable();

    while (i != -1)
    {
      String newLine = new String(line, 0, i);

      if (newLine.startsWith("Content-Disposition: form-data; name=\""))
      {
        if (newLine.indexOf("filename=\"") != -1)
        {

          setFilename(new String(line, 0, i-2));

          log.debug("filename = " + filename);

          if (filename==null)
          {
            return;
          }

          //this is the file content
          i = in.readLine(line, 0, 128);

          setContentType(new String(line, 0, i-2));

          log.debug("content type = " + contentType);

          i = in.readLine(line, 0, 128);

          // blank line
          i = in.readLine(line, 0, 128);

          newLine = new String(line, 0, i);

          //PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter((savePath==null? "" : savePath) + filename)));

          while (i != -1 && !newLine.startsWith(boundary))
          {
            // the problem is the last line of the file content
            // contains the new line character.
            // So, we need to check if the current line is
            // the last line.
            i = in.readLine(line, 0, 128);
            if ((i==boundaryLength+2 || i==boundaryLength+4) && (new String(line, 0, i).startsWith(boundary))) // + 4 is eof
            {
              //pw.print(newLine.substring(0, newLine.length()-2));
              shipFile.setData(newLine.substring(0, newLine.length()-2));
            }
            else
            {
              //pw.print(newLine);
              shipFile.setData(newLine);
            }

            newLine = new String(line, 0, i);

          }

          //pw.close();

        }
        else
        {
          //this is a field
          // get the field name
          int pos = newLine.indexOf("name=\"");
          String fieldName = newLine.substring(pos+6, newLine.length()-3);
          //log.debug("fieldName:" + fieldName);

          // blank line
          i = in.readLine(line, 0, 128);
          i = in.readLine(line, 0, 128);
          newLine = new String(line, 0, i);
          StringBuffer fieldValue = new StringBuffer(128);
          while (i != -1 && !newLine.startsWith(boundary))
          {
            // The last line of the field
            // contains the new line character.
            // So, we need to check if the current line is
            // the last line.
            i = in.readLine(line, 0, 128);
            if ((i==boundaryLength+2 || i==boundaryLength+4) && (new String(line, 0, i).startsWith(boundary)))// + 4 is eof
            {
              fieldValue.append(newLine.substring(0, newLine.length()-2));
            }
            else
            {
              fieldValue.append(newLine);
            }

            newLine = new String(line, 0, i);
          }
          //log.debug("fieldValue:" + fieldValue.toString());
          fields.put(fieldName, fieldValue.toString());
        }
      }
      i = in.readLine(line, 0, 128);

    } // end while

    uploaded = shipFile.submitData();
    shipFile.cleanUp();
  }

  public boolean wasSuccessful()
  {
    return uploaded;
  }


}
