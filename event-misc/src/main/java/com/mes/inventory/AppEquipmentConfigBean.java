/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/AppEquipmentConfigBean.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-02-01 16:39:25 -0800 (Fri, 01 Feb 2008) $
  Version            : $Revision: 14544 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.TreeSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;

public class AppEquipmentConfigBean extends com.mes.screens.SequenceDataBean
{

  public  final static int    NEEDED_SECTION          = 1;

  private int                 numRecs                 = 0;
  private int                 owner                   = -1;
  private String              ownerName               = "";
  private boolean             addToList               = false;
  private boolean             invalidClient           = false;

  public  Vector              terminals               = new Vector();
  public  Vector              termPrinters            = new Vector();                           
  public  Vector              termPrintPins           = new Vector();
  public  Vector              printers                = new Vector();
  public  Vector              pinpads                 = new Vector();
  public  Vector              imprinters              = new Vector();
  public  Vector              peripherals             = new Vector();
  public  Vector              accessories             = new Vector();

  public  Vector              terminalsModel          = new Vector();
  public  Vector              termPrintersModel       = new Vector();                           
  public  Vector              termPrintPinsModel      = new Vector();
  public  Vector              printersModel           = new Vector();
  public  Vector              pinpadsModel            = new Vector();
  public  Vector              imprintersModel         = new Vector();
  public  Vector              peripheralsModel        = new Vector();
  public  Vector              accessoriesModel        = new Vector();

  private Vector              selectedEquip           = new Vector();
  private Vector              equipRecs               = new Vector();

  public  Vector              prodOpt                 = new Vector();
  public  Vector              prodOptCode             = new Vector();
  public  Vector              allClients              = new Vector();

  private HashMap             validateMap             = new HashMap();
  private HashMap             prodOptions             = new HashMap();
  private HashMap             equipDesc               = new HashMap();
  private HashMap             equipTypeDesc           = new HashMap();
  private HashMap             equipTypeCode           = new HashMap();


  private HashMap             configuredClients       = new HashMap();
  private HashMap             allClientsMap           = new HashMap();


  private EquipRecComparator  erc                     = new EquipRecComparator();
  private TreeSet             sortedResults           = null;


  public AppEquipmentConfigBean()
  {
    try
    {
      StringBuffer      qs      = new StringBuffer("");
      PreparedStatement ps      = null;
      ResultSet         rs      = null;
      
      qs.append("select   e.equip_model,                      ");
      qs.append("         e.equip_descriptor,                 ");
      qs.append("         e.equiptype_code,                   ");
      qs.append("         t.equiptype_description             ");
      qs.append("from     equipment e,                        ");
      qs.append("         equiptype t                         ");
      qs.append("where    e.used_in_inventory = 'Y' and       ");
      qs.append("         e.equiptype_code = t.equiptype_code ");
      qs.append("order by e.equiptype_code,e.equip_descriptor ");
      
      ps = getPreparedStatement(qs.toString());
      
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        switch(rs.getInt("equiptype_code"))
        {
          case mesConstants.APP_EQUIP_TYPE_TERMINAL:
            terminals.add(rs.getString("equip_descriptor"));
            terminalsModel.add(rs.getString("equip_model"));
            equipDesc.put(rs.getString("equip_model"),rs.getString("equip_descriptor"));
            equipTypeDesc.put(rs.getString("equip_model"),rs.getString("equiptype_description"));
            equipTypeCode.put(rs.getString("equip_model"),rs.getString("equiptype_code"));
          break;
          case mesConstants.APP_EQUIP_TYPE_PRINTER:
            printers.add(rs.getString("equip_descriptor"));
            printersModel.add(rs.getString("equip_model"));
            equipDesc.put(rs.getString("equip_model"),rs.getString("equip_descriptor"));
            equipTypeDesc.put(rs.getString("equip_model"),rs.getString("equiptype_description"));
            equipTypeCode.put(rs.getString("equip_model"),rs.getString("equiptype_code"));
          break;
          case mesConstants.APP_EQUIP_TYPE_PINPAD:
            pinpads.add(rs.getString("equip_descriptor"));
            pinpadsModel.add(rs.getString("equip_model"));
            equipDesc.put(rs.getString("equip_model"),rs.getString("equip_descriptor"));
            equipTypeDesc.put(rs.getString("equip_model"),rs.getString("equiptype_description"));
            equipTypeCode.put(rs.getString("equip_model"),rs.getString("equiptype_code"));
          break;
          case mesConstants.APP_EQUIP_TYPE_IMPRINTER:
            imprinters.add(rs.getString("equip_descriptor"));
            imprintersModel.add(rs.getString("equip_model"));
            equipDesc.put(rs.getString("equip_model"),rs.getString("equip_descriptor"));
            equipTypeDesc.put(rs.getString("equip_model"),rs.getString("equiptype_description"));
            equipTypeCode.put(rs.getString("equip_model"),rs.getString("equiptype_code"));
          break;
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINTER:
            termPrinters.add(rs.getString("equip_descriptor"));
            termPrintersModel.add(rs.getString("equip_model"));
            equipDesc.put(rs.getString("equip_model"),rs.getString("equip_descriptor"));
            equipTypeDesc.put(rs.getString("equip_model"),rs.getString("equiptype_description"));
            equipTypeCode.put(rs.getString("equip_model"),rs.getString("equiptype_code"));
          break;
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD:
            termPrintPins.add(rs.getString("equip_descriptor"));
            termPrintPinsModel.add(rs.getString("equip_model"));
            equipDesc.put(rs.getString("equip_model"),rs.getString("equip_descriptor"));
            equipTypeDesc.put(rs.getString("equip_model"),rs.getString("equiptype_description"));
            equipTypeCode.put(rs.getString("equip_model"),rs.getString("equiptype_code"));
          break;
          case mesConstants.APP_EQUIP_TYPE_PERIPHERAL:
            peripherals.add(rs.getString("equip_descriptor"));
            peripheralsModel.add(rs.getString("equip_model"));
            equipDesc.put(rs.getString("equip_model"),rs.getString("equip_descriptor"));
            equipTypeDesc.put(rs.getString("equip_model"),rs.getString("equiptype_description"));
            equipTypeCode.put(rs.getString("equip_model"),rs.getString("equiptype_code"));
          break;
          case mesConstants.APP_EQUIP_TYPE_ACCESSORIES:
            accessories.add(rs.getString("equip_descriptor"));
            accessoriesModel.add(rs.getString("equip_model"));
            equipDesc.put(rs.getString("equip_model"),rs.getString("equip_descriptor"));
            equipTypeDesc.put(rs.getString("equip_model"),rs.getString("equiptype_description"));
            equipTypeCode.put(rs.getString("equip_model"),rs.getString("equiptype_code"));
          break;
        }  
      }
      
      rs.close();
      ps.close();
    
      ps = getPreparedStatement("select * from prodoption");
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        prodOpt.add(rs.getString("prod_option_des"));
        prodOptCode.add(rs.getString("prod_option_id"));  
        prodOptions.put(rs.getString("prod_option_id"),rs.getString("prod_option_des"));
      }
    
      rs.close();
      ps.close();
    
      ps = getPreparedStatement("select DISTINCT app_type from equipment_application where equip_section = ? ");
      ps.setInt(1, NEEDED_SECTION);
      
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        configuredClients.put(rs.getString("app_type"), "USED");
      }
    
      rs.close();
      ps.close();

      ps = getPreparedStatement("select app_type_code,app_description as inventory_owner_name from app_type ");
      
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        allClients.add(rs.getString("app_type_code"));
        allClientsMap.put(rs.getString("app_type_code"), rs.getString("inventory_owner_name"));
      }
    
      rs.close();
      ps.close();


    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "constructor--filling grids: " + e.toString());
    }
  }
  
  public void updateData(HttpServletRequest aReq)
  {
    int     i           = 0;
    boolean addToVector = false;

    while(i < this.numRecs)
    {
      String tempClass = null;
      EquipRec tempRec = new EquipRec();

      if(isBlank(aReq.getParameter("delete" + i)))
      {    
        addToVector = true;
      }
      else
      {
        addToVector = false;
      }
      
      //this validates to make sure two identical items are not picked..
      if(addToVector & isSubmitted())
      {
        if(!isBlank(aReq.getParameter("partNum" + i)) && !isBlank(aReq.getParameter("prodOptId" + i)))
        {
          tempClass = (String)validateMap.get((aReq.getParameter("partNum" + i) + "*" + aReq.getParameter("prodOptId" + i)));
          if(isBlank(tempClass))
          {
            validateMap.put((aReq.getParameter("partNum" + i) + "*" + aReq.getParameter("prodOptId" + i)),"EXSISTS");
          }
          else
          {
            addError("You can not have two items with the same product description and the same product option.");
          }
        }
      }
      
      if(!isBlank(aReq.getParameter("partNum" + i)))
      {    
        tempRec.setPartNum(aReq.getParameter("partNum" + i));
      }
      if(!isBlank(aReq.getParameter("prodOptId" + i)))
      {    
        tempRec.setProdOptId(aReq.getParameter("prodOptId" + i));
      }

      if(!isBlank(aReq.getParameter("buyMin" + i)))
      {    
        tempRec.setBuyMin(aReq.getParameter("buyMin" + i));
      }
      if(!isBlank(aReq.getParameter("buyMax" + i)))
      {    
        tempRec.setBuyMax(aReq.getParameter("buyMax" + i));
      }

      if(!isBlank(aReq.getParameter("rentMin" + i)))
      {    
        tempRec.setRentMin(aReq.getParameter("rentMin" + i));
      }
      if(!isBlank(aReq.getParameter("rentMax" + i)))
      {    
        tempRec.setRentMax(aReq.getParameter("rentMax" + i));
      }

      if(!isBlank(aReq.getParameter("leaseMin" + i)))
      {    
        tempRec.setLeaseMin(aReq.getParameter("leaseMin" + i));
      }
      if(!isBlank(aReq.getParameter("leaseMax" + i)))
      {    
        tempRec.setLeaseMax(aReq.getParameter("leaseMax" + i));
      }

      i++;

      if(addToVector)
      {
        equipRecs.add(tempRec);
      }
    }

    // now create the sorted tree this will sort the records by equipment type
    if(sortedResults != null)
    {
      sortedResults.clear();
    }
      
    sortedResults = null;
      
    sortedResults = new TreeSet(erc);
    sortedResults.addAll(equipRecs);

  }

  public boolean validate()
  {
    int     i           = 0;

    while(i < this.numRecs)
    {
      EquipRec tempRec =  (EquipRec)equipRecs.elementAt(i);

      if(!isBlank(tempRec.getBuyMin()))
      {
        if(validCurr(tempRec.getBuyMin()))
        {
          tempRec.setBuyMin(getDigitsAndDecimals(formatCurr(getCurr(tempRec.getBuyMin()))));
          try
          {
            double tempDoub = Double.parseDouble(tempRec.getBuyMin());
            if(tempDoub > 9999.99)
            {
              addError("Buy minimum price for each " + decodeEquipDesc(tempRec.getPartNum()) + " must be less than $10,000.00");
            }
          }
          catch(Exception e)
          {}
        }
        else
        {
          addError("Please provide a valid buy minimum price for each " + decodeEquipDesc(tempRec.getPartNum()));
        }
      }

      if(!isBlank(tempRec.getBuyMax()))
      {
        if(validCurr(tempRec.getBuyMax()))
        {
          tempRec.setBuyMax(getDigitsAndDecimals(formatCurr(getCurr(tempRec.getBuyMax()))));
          try
          {
            double tempDoub = Double.parseDouble(tempRec.getBuyMax());
            if(tempDoub > 9999.99)
            {
              addError("Buy maximum price for each " + decodeEquipDesc(tempRec.getPartNum()) + " must be less than $10,000.00");
            }
          }
          catch(Exception e)
          {}
        }
        else
        {
          addError("Please provide a valid buy maximum price for each " + decodeEquipDesc(tempRec.getPartNum()));
        }
      }


      if(!isBlank(tempRec.getRentMin()))
      {
        if(validCurr(tempRec.getRentMin()))
        {
          tempRec.setRentMin(getDigitsAndDecimals(formatCurr(getCurr(tempRec.getRentMin()))));
          try
          {
            double tempDoub = Double.parseDouble(tempRec.getRentMin());
            if(tempDoub > 9999.99)
            {
              addError("Rent minimum price for each " + decodeEquipDesc(tempRec.getPartNum()) + " must be less than $10,000.00");
            }
          }
          catch(Exception e)
          {}
        }
        else
        {
          addError("Please provide a valid rent minimum price for each " + decodeEquipDesc(tempRec.getPartNum()));
        }
      }

      if(!isBlank(tempRec.getRentMax()))
      {
        if(validCurr(tempRec.getRentMax()))
        {
          tempRec.setRentMax(getDigitsAndDecimals(formatCurr(getCurr(tempRec.getRentMax()))));
          try
          {
            double tempDoub = Double.parseDouble(tempRec.getRentMax());
            if(tempDoub > 9999.99)
            {
              addError("Rent maximum price for each " + decodeEquipDesc(tempRec.getPartNum()) + " must be less than $10,000.00");
            }
          }
          catch(Exception e)
          {}
        }
        else
        {
          addError("Please provide a valid rent maximum price for each " + decodeEquipDesc(tempRec.getPartNum()));
        }
      }


      if(!isBlank(tempRec.getLeaseMin()))
      {
        if(validCurr(tempRec.getLeaseMin()))
        {
          tempRec.setLeaseMin(getDigitsAndDecimals(formatCurr(getCurr(tempRec.getLeaseMin()))));
          try
          {
            double tempDoub = Double.parseDouble(tempRec.getLeaseMin());
            if(tempDoub > 9999.99)
            {
              addError("Lease minimum price for each " + decodeEquipDesc(tempRec.getPartNum()) + " must be less than $10,000.00");
            }
          }
          catch(Exception e)
          {}
        }
        else
        {
          addError("Please provide a valid lease minimum price for each " + decodeEquipDesc(tempRec.getPartNum()));
        }
      }

      if(!isBlank(tempRec.getLeaseMax()))
      {
        if(validCurr(tempRec.getLeaseMax()))
        {
          tempRec.setLeaseMax(getDigitsAndDecimals(formatCurr(getCurr(tempRec.getLeaseMax()))));
          try
          {
            double tempDoub = Double.parseDouble(tempRec.getLeaseMax());
            if(tempDoub > 9999.99)
            {
              addError("Lease maximum price for each " + decodeEquipDesc(tempRec.getPartNum()) + " must be less than $10,000.00");
            }
          }
          catch(Exception e)
          {}
        }
        else
        {
          addError("Please provide a valid lease maximum price for each " + decodeEquipDesc(tempRec.getPartNum()));
        }
      }

      i++;
    }
    
    return(!hasErrors());
  }

  public void getEquipInfo()
  {
    for(int i=0; i<selectedEquip.size(); i++)
    {
      EquipRec tempRec = new EquipRec();

      tempRec.setPartNum((String)selectedEquip.elementAt(i));

      equipRecs.add(tempRec);
    }

    // now create the sorted tree this will sort the records by equipment type
    if(sortedResults != null)
    {
      sortedResults.clear();
    }
      
    sortedResults = null;
      
    sortedResults = new TreeSet(erc);
    sortedResults.addAll(equipRecs);
  }



  public String decodeEquipTypeDesc(String model)
  {
    String result = "";
    try
    {
      result = (String)equipTypeDesc.get(model);
    }
    catch(Exception e)
    {
    }
    return result;
  }
  
  public String decodeEquipTypeCode(String model)
  {
    String result = "";
    try
    {
      result = (String)equipTypeCode.get(model);
    }
    catch(Exception e)
    {
    }
    return result;
  }

  public String decodeProdOptions(int option)
  {
    String result = "";
    try
    {
      result = (String)prodOptions.get(Integer.toString(option));
    }
    catch(Exception e)
    {
    }
    return result;
  }


  public boolean isClientConfigured(String client)
  {
    boolean result = false;
    try
    {
      result = !isBlank((String)configuredClients.get(client));
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }

  public String decodeClientName(String client)
  {
    String result = "";
    try
    {
      result = (String)allClientsMap.get(client);
    }
    catch(Exception e)
    {
    }
    return result;
  }

  public String decodeEquipDesc(String model)
  {
    String result = "";
    try
    {
      result = (String)equipDesc.get(model);
    }
    catch(Exception e)
    {
    }
    return result;
  }


  public void submitData()
  {
    submitEquipmentList();
  }

  private boolean findOwnerName(String client)
  {
    StringBuffer      qs        = new StringBuffer("");
    PreparedStatement ps        = null;
    ResultSet         rs        = null;
    boolean           result    = false;

    try
    {
      qs.setLength(0);
      qs.append("select app_description,app_type_code ");
      qs.append("from app_type ");
      qs.append("where app_type_code = ? ");
      
      ps = getPreparedStatement(qs.toString());
      
      ps.setString(1, client);

      rs = ps.executeQuery();

      if(rs.next())
      {
        this.ownerName = rs.getString("app_description");
        this.owner     = rs.getInt("app_type_code");
        result         = true;
      }
      else
      {
        this.invalidClient = true;
      }
    }
    catch(Exception e)
    {
      result = false;
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "findOwnerName: " + e.toString());
      addError("findOwnerName: " + e.toString());
    }

    return result;
  }

  public void getEquipmentList(String client)
  {
    StringBuffer      qs        = new StringBuffer("");
    PreparedStatement ps        = null;
    ResultSet         rs        = null;
    EquipRec          tempRec   = null;
    
    try
    {
      if(isBlank(client))
      {
        this.invalidClient = true;
        return;
      }

      if(!findOwnerName(client))
      {
        this.invalidClient = true;
        return;
      }

      qs.setLength(0);
      qs.append("select * ");
      qs.append("from equipment_application ");
      qs.append("where app_type = ? and equip_section = ? ");
      
      ps = getPreparedStatement(qs.toString());
      
      ps.setString(1, client);
      ps.setInt(2,NEEDED_SECTION);

      rs = ps.executeQuery();

      String partNum          = "";
      String prodOptId        = "";
      String buyMin           = "";
      String buyMax           = "";
      String rentMin          = "";
      String rentMax          = "";
      String leaseMin         = "";
      String leaseMax         = "";

      while(rs.next())
      {
        partNum          = isBlank(rs.getString("equip_model"))     ? "" : rs.getString("equip_model");
        prodOptId        = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

        buyMin           = isBlank(rs.getString("equip_buy_min"))   ? "" : rs.getString("equip_buy_min");
        buyMax           = isBlank(rs.getString("equip_buy_max"))   ? "" : rs.getString("equip_buy_max");
        rentMin          = isBlank(rs.getString("equip_rent_min"))  ? "" : rs.getString("equip_rent_min");
        rentMax          = isBlank(rs.getString("equip_rent_max"))  ? "" : rs.getString("equip_rent_max");
        leaseMin         = isBlank(rs.getString("equip_lease_min")) ? "" : rs.getString("equip_lease_min");
        leaseMax         = isBlank(rs.getString("equip_lease_max")) ? "" : rs.getString("equip_lease_max");

        if(!isBlank(partNum))
        {
          tempRec =  new EquipRec();
          tempRec.setPartNum(   partNum   );
          tempRec.setProdOptId( prodOptId );      
          tempRec.setBuyMin(    buyMin    );
          tempRec.setBuyMax(    buyMax    );
          tempRec.setRentMin(   rentMin   );
          tempRec.setRentMax(   rentMax   );
          tempRec.setLeaseMin(  leaseMin  );
          tempRec.setLeaseMax(  leaseMax  );
          equipRecs.add(tempRec);
        }
          
      }  

      // now create the sorted tree this will sort the records by equipment type
      if(sortedResults != null)
      {
        sortedResults.clear();
      }
      
      sortedResults = null;
      
      sortedResults = new TreeSet(erc);
      sortedResults.addAll(equipRecs);
 
      ps.close();  
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getEquipmentList: " + e.toString());
      addError("getEquipmentList: " + e.toString());
    }
  }


  private void submitEquipmentList()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;
    
    try
    {
      ps = getPreparedStatement("delete from equipment_application where app_type = ? and equip_section = ? ");
      ps.setInt(1,this.owner);
      ps.setInt(2,NEEDED_SECTION);
      ps.executeUpdate();
      ps.close();
  
      qs.setLength(0);
      qs.append("insert into equipment_application (");
      qs.append("equip_model,       ");
      qs.append("prod_option_id,    ");
      qs.append("equip_description, ");
      qs.append("equip_type,        ");
      qs.append("equip_section,     ");
      qs.append("app_type,          ");
      qs.append("equip_buy_min,     ");
      qs.append("equip_buy_max,     ");
      qs.append("equip_rent_min,    ");
      qs.append("equip_rent_max,    ");
      qs.append("equip_lease_min,   ");
      qs.append("equip_lease_max,   ");
      qs.append("equip_sort_order   ");
      qs.append(") values(?,?,?,?,?,?,?,?,?,?,?,?,?)");
      
      ps = getPreparedStatement(qs.toString());

      Iterator it = getSortedResults();
      while(it != null && it.hasNext())
      {
        String desc = "";

        EquipRec er = (EquipRec)(it.next());

        ps.setString(1, er.getPartNum());
        
        if(er.getProdOptId() != -1)
        {
          ps.setInt(2,er.getProdOptId());
          desc = decodeProdOptions(er.getProdOptId());
        }
        else
        {
          ps.setString(2,"");
        }
        
        if(!isBlank(desc))
        {
          desc = (decodeEquipDesc(er.getPartNum())).trim() + "-" + desc.trim();
        }
        else
        {
          desc = (decodeEquipDesc(er.getPartNum())).trim();
        }

        ps.setString(3, desc);

        ps.setString(4,   decodeEquipTypeCode(er.getPartNum()));
        ps.setInt(5,      NEEDED_SECTION);
        ps.setInt(6,      this.owner);
        ps.setString(7,   er.getBuyMin());
        ps.setString(8,   er.getBuyMax());
        ps.setString(9,   er.getRentMin());
        ps.setString(10,  er.getRentMax());
        ps.setString(11,  er.getLeaseMin());
        ps.setString(12,  er.getLeaseMax());
        ps.setInt(13,     i);
        
        ps.executeUpdate();
        
        ++i; // increment sort order
      }  

      ps.close();  

    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitEquipmentList: " + e.toString());
      addError("submitEquipmentList: " + e.toString());
    }
  }
  

  private String getDigitsAndDecimals(String raw)
  {
    StringBuffer  digits      = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == '.')
        {
          digits.append(raw.charAt(i));
        }
      }
      
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDigits: " + e.toString());
    }
    
    return digits.toString();

  }

  public String formatCurr(String curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }

  public String getCurr(String num)
  {
    String result = "";
    try
    {
      if(validCurr(num))
      {
        if(!num.startsWith("$"))
        {
          num = "$" + num;
        }
        NumberFormat  nf       = NumberFormat.getCurrencyInstance(Locale.US);
        double        temp     = (nf.parse(num)).doubleValue();
                      result   = Double.toString(temp);   
      }
    }
    catch(Exception e)
    {}
    return result;
  }

  public boolean validCurr(String raw)
  {
    boolean result  = true;
    int     sign    = 0;
    int     decimal = 0;

    try
    {
      if(!raw.startsWith("$"))
      {
        raw = "$" + raw;
      }
      
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == ',') //let digits and commas slide
        {         
        }
        else if(raw.charAt(i) == '$')
        {
          sign++;
        }
        else if(raw.charAt(i) == '.')
        {
          decimal++;
        }
        else
        {
          result = false;
        }
      }
      
      if(sign > 1 || decimal > 1)
      {
        result = false;
      }
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }
  
  public int getListSize()
  {
    return equipRecs.size();
  }

  public void setNumRecs(String numRecs)
  {
    try
    {
      this.numRecs = Integer.parseInt(numRecs);
    }
    catch(Exception e)
    {
      this.numRecs = 0;
    }
  }

  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }

  public boolean addingToList()
  {
    return addToList;
  }

  public void setAddToList(String temp)
  {
    this.addToList = true;
  }

  public int getOwner()
  {
    return this.owner;
  }

  public void setOwner(String owner)
  {
    try
    {
      this.owner = Integer.parseInt(owner);
    }
    catch(Exception e)
    {
      this.owner = -1;
    }
  }

  public String getOwnerName()
  {
    return this.ownerName;
  }

  public void setOwnerName(String ownerName)
  {
    this.ownerName = ownerName;
  }

  public void setInvalidClient()
  {
    this.invalidClient = true;
  }

  public boolean isInvalidClient()
  {
    return this.invalidClient;
  }


  public void setTerminals(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }
  public void setTermPrinters(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }
  public void setTermPrintPins(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }
  public void setPrinters(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }
  public void setPinpads(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }
  public void setImprinters(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }
  public void setPeripherals(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }
  public void setAccessories(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }

/*
  public String getPartNum(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getPartNum());
  }

  public int getProdOptId(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getProdOptId());
  }
    
  public String getBuyMin(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getBuyMin());
  }
    
  public String getBuyMax(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getBuyMax());
  }

  public String getRentMin(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getRentMin());
  }
    
  public String getRentMax(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getRentMax());
  }
    
  public String getLeaseMin(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getLeaseMin());
  }
    
  public String getLeaseMax(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getLeaseMax());
  }
*/


  public class EquipRec
  {
    private String partNum        = "";
    private int    prodOptId      = -1;
    private String buyMin         = "";
    private String buyMax         = "";
    private String rentMin        = "";
    private String rentMax        = "";
    private String leaseMin       = "";
    private String leaseMax       = "";
    

    EquipRec()
    {}


    public void setPartNum(String partNum)
    {
      this.partNum = partNum;
    }
    
 
    public void setProdOptId(String prodOptId)
    {
      try
      {
        this.prodOptId = Integer.parseInt(prodOptId);
      }
      catch(Exception e)
      {
        this.prodOptId = -1;
      }
    }
    
    public void setBuyMin(String buyMin)
    {
      this.buyMin = buyMin;
    }
    
    public void setBuyMax(String buyMax)
    {
      this.buyMax = buyMax;
    }

    public void setRentMin(String rentMin)
    {
      this.rentMin = rentMin;
    }
    
    public void setRentMax(String rentMax)
    {
      this.rentMax = rentMax;
    }
    
    public void setLeaseMin(String leaseMin)
    {
      this.leaseMin = leaseMin;
    }
    
    public void setLeaseMax(String leaseMax)
    {
      this.leaseMax = leaseMax;
    }

    public String getPartNum()
    {
      return this.partNum;
    }
  
    public int getProdOptId()
    {
      return this.prodOptId;
    }
    
    public String getBuyMin()
    {
      return this.buyMin;
    }
    
    public String getBuyMax()
    {
      return this.buyMax;
    }

    public String getRentMin()
    {
      return this.rentMin;
    }
    
    public String getRentMax()
    {
      return this.rentMax;
    }
    
    public String getLeaseMin()
    {
      return this.leaseMin;
    }
    
    public String getLeaseMax()
    {
      return this.leaseMax;
    }
  
  
  }

  public class EquipRecComparator implements Comparator
  {
   
    public EquipRecComparator()
    {
    }
  
    int compare(EquipRec o1, EquipRec o2)
    {
      int result    = 0;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        compareString1 = decodeEquipTypeDesc(o1.getPartNum()) + "*" + o1.getPartNum() + "*" + o1.getProdOptId();
        compareString2 = decodeEquipTypeDesc(o2.getPartNum()) + "*" + o2.getPartNum() + "*" + o2.getProdOptId();
        result = compareString1.compareTo(compareString2);
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(EquipRec o1, EquipRec o2)
    {
      boolean result    = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        compareString1 = decodeEquipTypeDesc(o1.getPartNum()) + "*" + o1.getPartNum() + "*" + o1.getProdOptId();
        compareString2 = decodeEquipTypeDesc(o2.getPartNum()) + "*" + o2.getPartNum() + "*" + o2.getProdOptId();
        result = compareString1.equals(compareString2);
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((EquipRec)o1, (EquipRec)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((EquipRec)o1, (EquipRec)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }
}
