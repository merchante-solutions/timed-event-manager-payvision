/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/PurchaseOrderDetailsBean.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/14/03 4:01p $
  Version            : $Revision: 11 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

public class PurchaseOrderDetailsBean extends com.mes.screens.SequenceDataBean
{

  private String   referenceNum                        = "";
  private int      numRecs                             = 0;
  private boolean  updateNotes                         = false;
  private String   poNotes                             = "";
  private String   poNotesTemp                         = "";
  
  private String   owner                               = "";
    
  private boolean  editNotes                           = false;
  private boolean  wasEdited                           = false;
  private boolean  received                            = true;
  
  private Date     receiveDate                         = null;
  
  private Vector   selectedEquip                       = new Vector();
  private Vector   equipRecs                           = new Vector();

  public PurchaseOrderDetailsBean()
  {
  }

  public void getData(String referenceNum)
  {
    getEquipData(referenceNum);
    getNoteData(referenceNum);
  }

  //change this so that it adds up all the ordered and received.. since
  //we nolonger use the order quan and recieved quan columns in the db
  public void getEquipData(String referenceNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;
    EquipRec          tempRec = null;
     
    try
    {
     
      qs.append("select trans.*, class.ec_class_name, equip.equip_descriptor, type.equiptype_description, mfgr.equipmfgr_mfr_name ");
      qs.append("from equipment equip, equiptype type, equipmfgr mfgr, equip_transaction trans, equip_class class ");
      qs.append("where et_transaction_id = ? and trans.et_part_number = equip.equip_model and ");
      qs.append("equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code and ");
      qs.append("equip.equiptype_code = type.equiptype_code and ");
      qs.append("trans.et_class = class.ec_class_id ");
      qs.append("order by trans.et_part_number,trans.et_class ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setString(1,referenceNum);
      
      rs = ps.executeQuery();

      String prevPartNum      = "";
      String prevMfrName      = "";
      String prevEquipDesc    = "";
      String prevEquipType    = "";
      String prevClassDesc    = "";      
      String prevClass        = "";      
      String prevUnitPrice    = "";
      int    count            = 1;
      int    receivedCount    = 0;

      while(rs.next())
      {
        String tempPartNum    = isBlank(rs.getString("et_part_number")) ? "" : rs.getString("et_part_number");
        String tempClass      = isBlank(rs.getString("et_class"))       ? "" : rs.getString("et_class");


        if(!tempPartNum.equals(prevPartNum) || !tempClass.equals(prevClass))
        {
          if(!isBlank(prevPartNum))
          {
            tempRec =  new EquipRec();
            tempRec.setPartNum(     prevPartNum   );
            tempRec.setProductName( prevMfrName   );
            tempRec.setProductDesc( prevEquipDesc );
            tempRec.setProductType( prevEquipType );
            tempRec.setClassType(   prevClassDesc );      
            tempRec.setClassCode(   prevClass     );      
            tempRec.setUnitPrice(   prevUnitPrice );

            tempRec.setQuantity(          Integer.toString(count)         );
            tempRec.setQuantityReceived(  Integer.toString(receivedCount) );

            equipRecs.add(tempRec);
            
            //reset count only if added to vector
            count         = 1;
            receivedCount = 0;
          }
          
          //put in quanity we counted along with other stuff... into tempRec.. then we put new into prev
          prevPartNum      =     rs.getString("et_part_number");       
          prevClass        =     rs.getString("et_class");
          prevClassDesc    =     rs.getString("ec_class_name");
          prevUnitPrice    =     rs.getString("et_unit_price");
          prevMfrName      =     rs.getString("equipmfgr_mfr_name");
          prevEquipDesc    =     rs.getString("equip_descriptor");
          prevEquipType    =     rs.getString("equiptype_description");
        }
        else
        {
          ++count;
        }

        //this must go at end...
        if(!isBlank(rs.getString("et_serial_number")))
        {
          receivedCount++;
        }

      }  
 
      //add last one to vector..
      if(!isBlank(prevPartNum))
      {
        tempRec =  new EquipRec();
        tempRec.setPartNum(     prevPartNum   );
        tempRec.setProductName( prevMfrName   );
        tempRec.setProductDesc( prevEquipDesc );
        tempRec.setProductType( prevEquipType );
        tempRec.setClassType(   prevClassDesc );      
        tempRec.setClassCode(   prevClass     );      
        tempRec.setUnitPrice(   prevUnitPrice );

        tempRec.setQuantity(    Integer.toString(count) );
        tempRec.setQuantityReceived(  Integer.toString(receivedCount)  );
        equipRecs.add(tempRec);
      }

      rs.close();  
      ps.close();
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getEquipData: " + e.toString());
      addError("getEquipData: " + e.toString());
    }
  }  
  

  public void getNoteData(String referenceNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;

    try
    {
      qs.append("select ep_description,ep_receive_date,ep_owner from equip_purchase_order ");
      qs.append("where ep_transaction_id = ? ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setString(1,referenceNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.referenceNum = isBlank(referenceNum) ? "" : referenceNum;
        this.owner        = rs.getString("ep_owner");

        if(isBlank(this.poNotes) && !wasEdited())
        {
          this.poNotes    = isBlank(rs.getString("ep_description")) ? "" : rs.getString("ep_description");
        }
        
        if(!isBlank(this.poNotesTemp))
        {
          this.poNotes    = (isBlank(this.poNotes) ? "" : (this.poNotes + "\n")) + this.poNotesTemp;
        }

        if(rs.getDate("ep_receive_date") != null)
        {
          this.received = true;
        }
            
      }
      rs.close();
      ps.close();
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getOrderData: " + e.toString());
      addError("getOrderData: " + e.toString());
    }
  }  

  private boolean validateNotes()
  {
    if(this.poNotes.length() > 250)
    {
      addError("Notes must be limited to 250 characters.  Currently there are " + this.poNotes.length() + " characters");
      this.editNotes = true;
    }
    return(!hasErrors());
  }

  public void updatePoNotes()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    
    try
    {
      if(validateNotes())
      {
        qs.append("update equip_purchase_order set ");
        qs.append("ep_description = ? ");
        qs.append("where ep_transaction_id = ? ");
      
        ps = getPreparedStatement(qs.toString());
      
        ps.setString(1, this.poNotes);
        ps.setString(2, this.referenceNum);
        ps.executeUpdate();
        ps.close();
      }
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "updatePoNotes: " + e.toString());
      addError("updatePoNotes: " + e.toString());
    }
  }


/*
//not sure if this is being used.. 
//might just be carry over from cut and paste
  public void updateData(HttpServletRequest aReq)
  {
    int     i           = 0;
    boolean addToVector = false;

    while(i < this.numRecs)
    {
      EquipRec tempRec = new EquipRec();

      if(isBlank(aReq.getParameter("delete" + i)))
      {    
        addToVector = true;
      }
      else
      {
        addToVector = false;
      }
      
      if(!isBlank(aReq.getParameter("partNum" + i)))
      {    
        tempRec.setPartNum(aReq.getParameter("partNum" + i));
      }
      
      if(!isBlank(aReq.getParameter("productName" + i)))
      {    
        tempRec.setProductName(aReq.getParameter("productName" + i));
      }
      
      if(!isBlank(aReq.getParameter("productDesc" + i)))
      {    
        tempRec.setProductDesc(aReq.getParameter("productDesc" + i));
      }
      
      if(!isBlank(aReq.getParameter("productType" + i)))
      {    
        tempRec.setProductType(aReq.getParameter("productType" + i));
      }

      if(!isBlank(aReq.getParameter("classType" + i)))
      {    
        tempRec.setClassType(aReq.getParameter("classType" + i));
      }
      
      if(!isBlank(aReq.getParameter("quantity" + i)))
      {    
        tempRec.setQuantity(aReq.getParameter("quantity" + i));
      }
      
      if(!isBlank(aReq.getParameter("unitPrice" + i)))
      {    
        tempRec.setUnitPrice(aReq.getParameter("unitPrice" + i));
      }
      
      i++;
      
      if(addToVector)
      {
        equipRecs.add(tempRec);
      }
    
    }
  }
*/


/*  
  public boolean validate()
  {
    if(this.receiveDate == null)
    {
      addError("Please provide a valid Receive Date");
    }
    if(this.invoiceDate == null)
    {
      addError("Please provide a valid Invoice Date");
    }
    if(isBlank(this.invoiceNum))
    {
      addError("Please provide an Invoice Number");
    }
    if(!isBlank(this.shipCharge))
    {
      if(validCurr(this.shipCharge))
      {
        this.shipCharge = getDigitsAndDecimals(formatCurr(getCurr(this.shipCharge)));
      }
      else
      {
        addError("Please provide a valid shipping charge");
      }
    }
    return(!hasErrors());
  }

  public void submitData()
  {
    updatePurchaseOrder();
  }
  
  private void updatePurchaseOrder()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    
    try
    {
      qs.append("update equip_purchase_order set ");
      qs.append("ep_receive_date = ?, ");
      qs.append("ep_invoice_date = ?, ");
      qs.append("ep_invoice_number = ?, ");
      qs.append("ep_shipping_company = ?, ");
      qs.append("ep_ship_charge = ? ");
      qs.append("where ep_transaction_id = ? ");
      
      ps = getPreparedStatement(qs.toString());
      
      ps.setDate(1, new java.sql.Date(this.receiveDate.getTime()));
      ps.setDate(2, new java.sql.Date(this.invoiceDate.getTime()));
      ps.setString(3, this.invoiceNum);
      ps.setString(4, this.shipCompany);
      ps.setString(5, this.shipCharge);
      ps.setString(6, this.referenceNum);
      ps.executeUpdate();
      ps.close();
      setTransactionDate();
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "updatePurchaseOrder: " + e.toString());
      addError("updatePurchaseOrder: " + e.toString());
    }
  }
  
  private void setTransactionDate()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    
    try
    {
      qs.append("update equip_purchase_order set ");
      qs.append("ep_transaction_date = sysdate ");
      qs.append("where ep_transaction_id = ? and ep_transaction_date is null");
      
      ps = getPreparedStatement(qs.toString());
      
      ps.setString(1, this.referenceNum);
      ps.executeUpdate();
      ps.close();
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setTransactionDate: " + e.toString());
      addError("setTransactionDate: " + e.toString());
    }
  }
*/


  private String getDigitsAndDecimals(String raw)
  {
    StringBuffer  digits      = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == '.')
        {
          digits.append(raw.charAt(i));
        }
      }
      
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDigits: " + e.toString());
    }
    
    return digits.toString();

  }

  public String formatCurr(String curr)
  {
    if(isBlank(curr))
    {
      return "";
    }
    else if(curr.equals("-----"))
    {
      return curr;
    }

    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }

  public String getCurr(String num)
  {
    String result = "";
    try
    {
      if(validCurr(num))
      {
        if(!num.startsWith("$"))
        {
          num = "$" + num;
        }
        NumberFormat  nf       = NumberFormat.getCurrencyInstance(Locale.US);
        double        temp     = (nf.parse(num)).doubleValue();
                      result   = Double.toString(temp);   
      }
    }
    catch(Exception e)
    {}
    return result;
  }

  public boolean validCurr(String raw)
  {
    boolean result  = true;
    int     sign    = 0;
    int     decimal = 0;

    try
    {
      if(!raw.startsWith("$"))
      {
        raw = "$" + raw;
      }
      
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == ',') //let digits and commas slide
        {         
        }
        else if(raw.charAt(i) == '$')
        {
          sign++;
        }
        else if(raw.charAt(i) == '.')
        {
          decimal++;
        }
        else
        {
          result = false;
        }
      }
      
      if(sign > 1 || decimal > 1)
      {
        result = false;
      }
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }
  
  private boolean validDate(String date)
  {
    int index1  = date.indexOf('/');
    int index2  = date.lastIndexOf('/');
    String mon  = date.substring(0,index1);
    String day  = date.substring(index1+1,index2);
    String year = date.substring(index2+1);
    boolean result = true;
    try
    {
      int dayInt = Integer.parseInt(day);
      int monInt = Integer.parseInt(mon);
      int yearInt = Integer.parseInt(year);
      if(dayInt > 31 || dayInt < 1)
        result = false;
      if(monInt > 12 || monInt < 1)
        result = false;
      if(yearInt < 0)
        result = false;
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }
  
  public int getListSize()
  {
    return equipRecs.size();
  }
  public String getOwner()
  {
    return this.owner;
  }
  public boolean editNotes()
  {
    return this.editNotes;
  }
  public boolean wasEdited()
  {
    return this.wasEdited;
  }
  public void setEditNotes(String temp)
  {
    this.editNotes = true;
  }
  public void setWasEdited(String temp)
  {
    this.wasEdited = true;
  }
    
  public void setPoNotesTemp(String poNotesTemp)
  {
    this.poNotesTemp = poNotesTemp;
  }
  public String getPoNotesTemp()
  {
    return this.poNotesTemp;
  }

  public void setPoNotes(String poNotes)
  {
    this.poNotes = poNotes;
  }
  public String getPoNotes()
  {
    return this.poNotes;
  }
  public String getPoNotesBr()
  {
    String body = this.poNotes;
    try
    {
      StringBuffer    bodyBuff = null;
      int index = 0;
      index = body.indexOf('\n');
      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"<BR>");
        body = bodyBuff.toString();
        index = body.indexOf('\n', index);
      }
    }
    catch(Exception e)
    {
    }
    return body;
  }
  
  public void setNumRecs(String numRecs)
  {
    try
    {
      this.numRecs = Integer.parseInt(numRecs);
    }
    catch(Exception e)
    {
      this.numRecs = 0;
    }
  }
  public boolean received()
  {
    return this.received;
  }
  
  public boolean updateNotes()
  {
    return this.updateNotes;
  }
  
  public void setUpdateNotes(String temp)
  {
    this.updateNotes = true;
  }
  
  

  public String getReceiveDate()
  {
    DateFormat df   = new SimpleDateFormat("MM/dd/yy");
    String result = "-----";
    if(this.receiveDate != null)
    {
      result = df.format(this.receiveDate);
    }
    return result;
  }  


  public void setReceiveDate(String receiveDate)
  {
    String receiveDateStr = receiveDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(receiveDate.trim());
      try
      {
        receiveDate          = receiveDate.replace('/','0');
        long tempLong         = Long.parseLong(receiveDate.trim());
        if(validDate(receiveDateStr.trim()))
        {
          this.receiveDate = aDate;
        }
        else
        {
          this.receiveDate = null;
        }
      }
      catch(Exception e)
      {
        this.receiveDate    = null;
      }
    }
    catch(ParseException e)
    {
      this.receiveDate      = null;
    }
  }
    
  public void setReferenceNum(String referenceNum)
  {
    this.referenceNum = referenceNum;
  }

  public String getReferenceNum()
  {
    return this.referenceNum;
  }

/******************************************************************
Class that holds information for each piece of equipment ordered
*******************************************************************/  

  public class EquipRec
  {
    
    private int    quantity         = -1;
    private int    quantityReceived = -1;
  
    private String classType        = "";
    private String classCode        = "";
    private String unitPrice        = "";
    private String partNum          = "";
    private String productName      = "";
    private String productType      = "";
    private String productDesc      = "";

    EquipRec()
    {}

    public void setClassType(String classType)
    {
     this.classType = classType;
    }
    public void setClassCode(String classCode)
    {
     this.classCode = classCode;
    }

    public void setQuantity(String quantity)
    {
      try
      {
        if(quantity != null && !quantity.equals(""))
        {
          this.quantity = Integer.parseInt(quantity);
        }
      }
      catch(Exception e)
      {}
    }

    public void setQuantityReceived(String quantityReceived)
    {
      try
      {
        if(quantityReceived != null && !quantityReceived.equals(""))
        {
          this.quantityReceived = Integer.parseInt(quantityReceived);
        }
      }
      catch(Exception e)
      {}
    }

    public void setUnitPrice(String unitPrice)
    {
      this.unitPrice = unitPrice;
    }
    public void setPartNum(String partNum)
    {
      this.partNum = partNum;
    }
    public void setProductName(String productName)
    {
      this.productName = productName;
    }
    public void setProductType(String productType)
    {
      this.productType = productType;
    }
    public void setProductDesc(String productDesc)
    {
      this.productDesc = productDesc;
    }

    public String getClassType()
    {
      return this.classType;
    }
    public String getClassCode()
    {
      return this.classCode;
    }
  
    public int getQuantity()
    {
      return this.quantity;
    }
    public int getQuantityReceived()
    {
      return this.quantityReceived;
    }  
    public String getUnitPrice()
    {
      return this.unitPrice;
    }
    public String getPartNum()
    {
      return this.partNum;
    }
    public String getProductName()
    {
      return this.productName;
    }
    public String getProductType()
    {
      return this.productType;
    }
    public String getProductDesc()
    {
      return this.productDesc;
    }
  }

  //accessory methods that access the embedded class
  public String getClassType(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getClassType());
  }
  public String getClassCode(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getClassCode());
  }

  public int getQuantity(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getQuantity());
  }
  public int getQuantityReceived(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getQuantityReceived());
  }
  public String getUnitPrice(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getUnitPrice());
  }
  public String getPartNum(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getPartNum());
  }
  public String getProductName(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getProductName());
  }
  public String getProductType(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getProductType());
  }
  public String getProductDesc(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getProductDesc());
  }


}