/*@lineinfo:filename=MoveToMesBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/MoveToMesBean.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/15/03 4:15p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.util.StringTokenizer;
import com.mes.constants.mesConstants;
import com.mes.ops.InventoryBase;

public class MoveToMesBean extends InventoryBase
{
  private String  LastDescription                 = null;
  private String  LastInventory                   = null;
  private String  LastSerialNumber                = null;

  private String  PartNumber                      = null;
  private String  SerialNumber                    = null;

  public MoveToMesBean()
  {
  }

  public String getLastDescription()
  {
    return( LastDescription );
  }
  
  public String getLastInventory()
  {
    return( LastInventory );
  }

  public String getLastSerialNumber()
  {
    return( LastSerialNumber );
  }

  
  public void setSerialNumInList( String serialNumberStr )
  {
    if ( !isBlank(serialNumberStr) )
    {
      if ( isBlank( SerialNumber ) )
      {      
        StringTokenizer       tokens = new StringTokenizer(serialNumberStr,";");
    
        try
        {
          PartNumber    = tokens.nextToken();
          SerialNumber  = tokens.nextToken();
        }
        catch( Exception e )
        {
          logEntry("setSerialNumInList()",e.toString());
        }
      }
      else
      {
        addError("Either select an In Item from the list OR provide a serial number and product description.");
      }      
    }
  }
  
  public void setSerialNumInMan( String serialNumber )
  {
    if ( !isBlank( serialNumber ) )
    {
      if ( isBlank( SerialNumber ) )
      {
        SerialNumber = serialNumber;
      }
      else
      {
        addError("Either select an In Item from the list OR provide a serial number and product description.");
      }
    }      
  }

  public void submit(long userId)
  {
    String            action        = "";
    int               statusCode    = 0;
    int               owner         = 0;

    try
    {

      //get current owner (before move to mes) and status
      /*@lineinfo:generated-code*//*@lineinfo:122^7*/

//  ************************************************************
//  #sql [Ctx] { select  ei.ei_status,
//                  ei.ei_owner
//          
//          from    equip_inventory ei
//          where   ei.ei_part_number   = :PartNumber and
//                  ei.ei_serial_number = :SerialNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ei.ei_status,\n                ei.ei_owner\n         \n        from    equip_inventory ei\n        where   ei.ei_part_number   =  :1  and\n                ei.ei_serial_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.MoveToMesBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,PartNumber);
   __sJT_st.setString(2,SerialNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   statusCode = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   owner = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:131^7*/

      /*@lineinfo:generated-code*//*@lineinfo:133^7*/

//  ************************************************************
//  #sql [Ctx] { update  equip_inventory 
//          set     ei_owner          = :mesConstants.APP_TYPE_MES
//          where   ei_part_number    = :PartNumber and
//                  ei_serial_number  = :SerialNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_inventory \n        set     ei_owner          =  :1 \n        where   ei_part_number    =  :2  and\n                ei_serial_number  =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.inventory.MoveToMesBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.APP_TYPE_MES);
   __sJT_st.setString(2,PartNumber);
   __sJT_st.setString(3,SerialNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:139^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:141^7*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_client_deployment 
//          (  
//            move_date,                             
//            serial_number,                         
//            from_client,                           
//            to_client,                             
//            condition,
//            part_number
//          ) 
//          values 
//          (
//            sysdate,
//            :SerialNumber,
//            :owner,
//            :mesConstants.APP_TYPE_MES,
//            :getClass(PartNumber, SerialNumber),
//            :PartNumber
//          )             
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1801 = getClass(PartNumber, SerialNumber);
   String theSqlTS = "insert into equip_client_deployment \n        (  \n          move_date,                             \n          serial_number,                         \n          from_client,                           \n          to_client,                             \n          condition,\n          part_number\n        ) \n        values \n        (\n          sysdate,\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.inventory.MoveToMesBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,SerialNumber);
   __sJT_st.setInt(2,owner);
   __sJT_st.setInt(3,mesConstants.APP_TYPE_MES);
   __sJT_st.setInt(4,__sJT_1801);
   __sJT_st.setString(5,PartNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:161^7*/
      
      // store the last updated data
      LastSerialNumber  = SerialNumber;
      LastDescription   = getEquipmentDesc( PartNumber );
      LastInventory     = getOwnerDesc( owner );
      
      // add this event to the history
      action = "Item moved to MES Inventory from " + LastInventory + " Inventory";
      addToHistory( PartNumber, SerialNumber, userId, InventoryBase.ES_TRANSFERRED, action, null, owner);
      
      //action = "Item received into MES Inventory from " + LastInventory + " Inventory";
      //addToHistory( PartNumber, SerialNumber, userId, statusCode, action, null, mesConstants.APP_TYPE_MES );
      
      /*@lineinfo:generated-code*//*@lineinfo:175^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:178^7*/
    }  
    catch(Exception e)
    {
      logEntry("submit()", e.toString());
      addError("submit: " + e.toString());
    }
  }
  
  public boolean validate()
  {
    if ( isBlank( SerialNumber ) )
    {
      addError("Please select or entry a valid serial number.");
    }
    else if ( isBlank( PartNumber ) )
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:197^9*/

//  ************************************************************
//  #sql [Ctx] { select  ei.ei_part_number 
//            from    equip_inventory ei
//            where   ei.ei_serial_number = :SerialNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ei.ei_part_number  \n          from    equip_inventory ei\n          where   ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.inventory.MoveToMesBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,SerialNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   PartNumber = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:202^9*/
      }
      catch( java.sql.SQLException e )
      {
        addError("Specified serial number is invalid or duplicated.  Try selecting the specific item from the list.");
      }        
    }

    if(!hasErrors())
    {
      // if it doesnt exist in db or its not mes inventory.. 
      // or its deployed. then we cant do it.. 
      // it must not be deployed.. it must be in stock..
      if(!doesExist(PartNumber, SerialNumber))
      {
        addError("Equipment with serial # " + SerialNumber + " does not exist in our database.");
      }
    }

    if(!hasErrors())
    {
      if(isMesInventory(PartNumber, SerialNumber))
      {
        addError("Equipment with serial # " + SerialNumber + " already belongs to MES inventory.");
      }
    }

    if(!hasErrors())
    {
      if(!isInStock(PartNumber, SerialNumber))
      {
        addError("Equipment with serial # " + SerialNumber + " is currently deployed.");
      }
    }
    return(!hasErrors());
  }
}/*@lineinfo:generated-code*/