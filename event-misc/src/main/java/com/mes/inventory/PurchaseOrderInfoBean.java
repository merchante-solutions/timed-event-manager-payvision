/*@lineinfo:filename=PurchaseOrderInfoBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/PurchaseOrderInfoBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/14/03 4:01p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import sqlj.runtime.ResultSetIterator;

public class PurchaseOrderInfoBean extends com.mes.screens.SequenceDataBean
{

  private String   referenceNum                        = "";
  private String   supplier                            = "";
  private String   supplierCode                        = "";
  private String   owner                               = "";
  private String   ownerCode                           = "";
  private Date     orderDate                           = null;
  private Date     receiveDate                         = null;
  private Date     invoiceDate                         = null;
  private String   invoiceNum                          = "";
  private String   vendorNum                           = "";
  private String   terms                               = "";
  private String   shipCompany                         = "";
  private String   shipCharge                          = "";
  
  public PurchaseOrderInfoBean()
  {
  }

  public void getData(String referenceNum)
  {
    getOrderData(referenceNum);
  }

  public void getOrderData(String referenceNum)
  {
    ResultSetIterator it      = null;
    int               i       = 0;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:81^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pur.*, 
//                  sup.es_supplier_name, 
//                  at.INVENTORY_OWNER_NAME
//          from    equip_purchase_order pur, 
//                  equip_supplier sup, 
//                  app_type at
//          where   ep_transaction_id = :referenceNum and
//                  pur.ep_supplier_id = sup.es_supplier_id and 
//                  at.app_type_code = pur.ep_owner
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pur.*, \n                sup.es_supplier_name, \n                at.INVENTORY_OWNER_NAME\n        from    equip_purchase_order pur, \n                equip_supplier sup, \n                app_type at\n        where   ep_transaction_id =  :1  and\n                pur.ep_supplier_id = sup.es_supplier_id and \n                at.app_type_code = pur.ep_owner";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.PurchaseOrderInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,referenceNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.PurchaseOrderInfoBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:92^7*/
      
      ResultSet rs = it.getResultSet();
      
      if(rs.next())
      {
        this.referenceNum = referenceNum;
        this.supplier     = rs.getString("es_supplier_name");
        this.supplierCode = rs.getString("ep_supplier_id");
        this.owner        = rs.getString("INVENTORY_OWNER_NAME");
        this.ownerCode    = rs.getString("ep_owner");
        this.orderDate    = this.orderDate == null    ? rs.getDate("ep_order_date")         : this.orderDate;
        this.receiveDate  = this.receiveDate == null  ? rs.getDate("ep_receive_date")       : this.receiveDate;
        this.invoiceDate  = this.invoiceDate == null  ? rs.getDate("ep_invoice_date")       : this.invoiceDate;;
        this.invoiceNum   = isBlank(this.invoiceNum)  ? rs.getString("ep_invoice_number")   : this.invoiceNum;
        this.vendorNum    = isBlank(this.vendorNum)   ? rs.getString("ep_vendor_number")    : this.vendorNum;
        this.terms        = isBlank(this.terms)       ? rs.getString("ep_terms")            : this.terms;
        this.shipCompany  = isBlank(this.shipCompany) ? rs.getString("ep_shipping_company") : this.shipCompany;
        this.shipCharge   = isBlank(this.shipCharge)  ? rs.getString("ep_ship_charge")      : this.shipCharge;
      }
      
      it.close();
    }  
    catch(Exception e)
    {
      logEntry("getOrderData()", e.toString());
      addError("getOrderData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }  



  public boolean validate()
  {
    /*
    if(this.receiveDate == null)
    {
      addError("Please provide a valid Receive Date");
    }
    

    if(this.invoiceDate == null)
    {
      addError("Please provide a valid Invoice Date");
    }
    
    if(isBlank(this.invoiceNum))
    {
      addError("Please provide an Invoice Number");
    }
    */

    if(!isBlank(this.shipCharge))
    {
      if(validCurr(this.shipCharge))
      {
        this.shipCharge = getDigitsAndDecimals(formatCurr(getCurr(this.shipCharge)));
      }
      else
      {
        addError("Please provide a valid shipping charge");
      }
    }
    return(!hasErrors());
  }

  public void submitData()
  {
    updatePurchaseOrder();
  }
  
  private void updatePurchaseOrder()
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:173^7*/

//  ************************************************************
//  #sql [Ctx] { update  equip_purchase_order 
//          set     ep_invoice_date       = :this.invoiceDate,
//                  ep_vendor_number      = :this.vendorNum,
//                  ep_terms              = :this.terms,
//                  ep_invoice_number     = :this.invoiceNum,
//                  ep_shipping_company   = :this.shipCompany,
//                  ep_ship_charge        = :this.shipCharge 
//          where   ep_transaction_id     = :this.referenceNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_purchase_order \n        set     ep_invoice_date       =  :1 ,\n                ep_vendor_number      =  :2 ,\n                ep_terms              =  :3 ,\n                ep_invoice_number     =  :4 ,\n                ep_shipping_company   =  :5 ,\n                ep_ship_charge        =  :6  \n        where   ep_transaction_id     =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.inventory.PurchaseOrderInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,this.invoiceDate);
   __sJT_st.setString(2,this.vendorNum);
   __sJT_st.setString(3,this.terms);
   __sJT_st.setString(4,this.invoiceNum);
   __sJT_st.setString(5,this.shipCompany);
   __sJT_st.setString(6,this.shipCharge);
   __sJT_st.setString(7,this.referenceNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:183^7*/
      
      setTransactionDate();
    }  
    catch(Exception e)
    {
      logEntry("updatePurchaseOrder()", e.toString());
      addError("updatePurchaseOrder: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void setTransactionDate()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:202^7*/

//  ************************************************************
//  #sql [Ctx] { update  equip_purchase_order 
//          set     ep_transaction_date = sysdate
//          where   ep_transaction_id = :this.referenceNum and 
//                  ep_transaction_date is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_purchase_order \n        set     ep_transaction_date = sysdate\n        where   ep_transaction_id =  :1  and \n                ep_transaction_date is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.inventory.PurchaseOrderInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.referenceNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:208^7*/
    }  
    catch(Exception e)
    {
      logEntry("setTransactionDate()", e.toString());
      addError("setTransactionDate: " + e.toString());
    }
  }


  private String getDigitsAndDecimals(String raw)
  {
    StringBuffer  digits      = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == '.')
        {
          digits.append(raw.charAt(i));
        }
      }
      
    }
    catch(Exception e)
    {
      logEntry("getDigitsAndDecimals(" + raw + ")", e.toString());
    }
    
    return digits.toString();

  }

  public String formatCurr(String curr)
  {
    if(isBlank(curr))
    {
      return "";
    }
    else if(curr.equals("-----"))
    {
      return curr;
    }

    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }

  public String getCurr(String num)
  {
    String result = "";
    try
    {
      if(validCurr(num))
      {
        if(!num.startsWith("$"))
        {
          num = "$" + num;
        }
        NumberFormat  nf       = NumberFormat.getCurrencyInstance(Locale.US);
        double        temp     = (nf.parse(num)).doubleValue();
                      result   = Double.toString(temp);   
      }
    }
    catch(Exception e)
    {}
    return result;
  }

  public boolean validCurr(String raw)
  {
    boolean result  = true;
    int     sign    = 0;
    int     decimal = 0;

    try
    {
      if(!raw.startsWith("$"))
      {
        raw = "$" + raw;
      }
      
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == ',') //let digits and commas slide
        {         
        }
        else if(raw.charAt(i) == '$')
        {
          sign++;
        }
        else if(raw.charAt(i) == '.')
        {
          decimal++;
        }
        else
        {
          result = false;
        }
      }
      
      if(sign > 1 || decimal > 1)
      {
        result = false;
      }
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }
  
  private boolean validDate(String date)
  {
    int index1  = date.indexOf('/');
    int index2  = date.lastIndexOf('/');
    String mon  = date.substring(0,index1);
    String day  = date.substring(index1+1,index2);
    String year = date.substring(index2+1);
    boolean result = true;
    try
    {
      int dayInt = Integer.parseInt(day);
      int monInt = Integer.parseInt(mon);
      int yearInt = Integer.parseInt(year);
      if(dayInt > 31 || dayInt < 1)
        result = false;
      if(monInt > 12 || monInt < 1)
        result = false;
      if(yearInt < 0)
        result = false;
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }
  
 
  public String getSupplier()
  {
    return this.supplier;
  }
  public void setSupplier(String supplier)
  {
    this.supplier = supplier;
  }
  
  public String getSupplierCode()
  {
    return this.supplierCode;
  }
  public void setSupplierCode(String supplierCode)
  {
    this.supplierCode = supplierCode;
  }

  public String getOwner()
  {
    return this.owner;
  }
  public void setOwner(String owner)
  {
    this.owner = owner;
  }

  public String getOwnerCode()
  {
    return this.ownerCode;
  }
  public void setOwnerCode(String ownerCode)
  {
    this.ownerCode = ownerCode;
  }

  public String getOrderDate()
  {
    DateFormat df   = new SimpleDateFormat("MM/dd/yy");
    String result = "-----";
    if(this.orderDate != null)
    {
      result = df.format(this.orderDate);
    }
    return result;
  }    
  
  public String getReceiveDate()
  {
    DateFormat df   = new SimpleDateFormat("MM/dd/yy");
    String result = "-----";
    if(this.receiveDate != null)
    {
      result = df.format(this.receiveDate);
    }
    return result;
  }  
  
  public String getInvoiceDate()
  {
    DateFormat df   = new SimpleDateFormat("MM/dd/yy");
    String result = "-----";
    if(this.invoiceDate != null)
    {
      result = df.format(this.invoiceDate);
    }
    return result;
  }  

  public void setOrderDate(String orderDate)
  {
    String orderDateStr = orderDate.trim();
    java.util.Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    
    try
    {
      aDate = fmt.parse(orderDate.trim());
      try
      {
        orderDate             = orderDate.replace('/','0');
        long tempLong         = Long.parseLong(orderDate.trim());
        if(validDate(orderDateStr.trim()))
        {
          this.orderDate = new java.sql.Date(aDate.getTime());
        }
        else
        {
          this.orderDate = null;
        }
      }
      catch(Exception e)
      {
        this.orderDate    = null;
      }
    }
    catch(ParseException e)
    {
      this.orderDate      = null;
    }
  }

/*
  public void setReceiveDate(String receiveDate)
  {
    String receiveDateStr = receiveDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(receiveDate.trim());
      try
      {
        receiveDate          = receiveDate.replace('/','0');
        long tempLong         = Long.parseLong(receiveDate.trim());
        if(validDate(receiveDateStr.trim()))
        {
          this.receiveDate = aDate;
        }
        else
        {
          this.receiveDate = null;
        }
      }
      catch(Exception e)
      {
        this.receiveDate    = null;
      }
    }
    catch(ParseException e)
    {
      this.receiveDate      = null;
    }
  }
*/
  
  public void setInvoiceDate(String invoiceDate)
  {
    String invoiceDateStr = invoiceDate.trim();
    java.util.Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(invoiceDate.trim());
      try
      {
        invoiceDate          = invoiceDate.replace('/','0');
        long tempLong        = Long.parseLong(invoiceDate.trim());

        if(validDate(invoiceDateStr.trim()))
        {
          this.invoiceDate = new java.sql.Date(aDate.getTime());
        }
        else
        {
          this.invoiceDate = null;
        }
      }
      catch(Exception e)
      {
        this.invoiceDate    = null;
      }
    }
    catch(ParseException e)
    {
      this.invoiceDate      = null;
    }
  }    
  
  public void setInvoiceNum(String invoiceNum)
  {
    this.invoiceNum = invoiceNum;
  }  
  public String getInvoiceNum()
  {
    String result = "-----";
    if(!isBlank(this.invoiceNum))
    {
      result = this.invoiceNum;
    }
    return result;
  }

  public void setVendorNum(String vendorNum)
  {
    this.vendorNum = vendorNum;
  }  
  public String getVendorNum()
  {
    String result = "-----";
    if(!isBlank(this.vendorNum))
    {
      result = this.vendorNum;
    }
    return result;
  }

  public void setTerms(String terms)
  {
    this.terms = terms;
  }  
  public String getTerms()
  {
    String result = "-----";
    if(!isBlank(this.terms))
    {
      result = this.terms;
    }
    return result;
  }


  public void setShipCompany(String shipCompany)
  {
    this.shipCompany = shipCompany;
  }  
  public String getShipCompany()
  {
    String result = "-----";
    if(!isBlank(this.shipCompany))
    {
      result = this.shipCompany;
    }
    return result;
  }
  public void setShipCharge(String shipCharge)
  {
     this.shipCharge = shipCharge;
  }
  public String getShipCharge()
  {
    String result = "-----";
    if(!isBlank(this.shipCharge))
    {
      result = this.shipCharge;
    }
    return result;
  }
    
  public void setReferenceNum(String referenceNum)
  {
    this.referenceNum = referenceNum;
  }

  public String getReferenceNum()
  {
    return this.referenceNum;
  }

}/*@lineinfo:generated-code*/