/*@lineinfo:filename=ReceivedEquipReport*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/ReceivedEquipReport.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/23/04 3:10p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.inventory;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class ReceivedEquipReport extends DateSQLJBean
{
  public class EquipmentData
  {
    private String      serialNumber;
    private String      partNumber;
    private String      equipType;
    private String      equipDesc;
    private Timestamp   receivedDateSorted;
    private String      receivedDate;
    private String      invoiceNumber;
    private String      orderNumber;
    private String      unitCost;
    private String      shippingCharge;
    private String      owner;
    private String      receivedCondition;
    private String      currentStatus;
    private String      deploymentStatus;

    public EquipmentData()
    {
      serialNumber        = "";
      partNumber          = "";
      equipType           = "";
      equipDesc           = "";
      receivedDateSorted  = null;
      receivedDate        = "";
      invoiceNumber       = "";
      orderNumber         = "";
      unitCost            = "";
      shippingCharge      = "";
      owner               = "";
      receivedCondition   = "";
      currentStatus       = "";
      deploymentStatus    = "";
    }
    
    public EquipmentData( String     serialNumber,
                          String     partNumber,
                          String     equipType,
                          String     equipDesc,
                          Timestamp  receivedDateSorted,
                          String     receivedDate,
                          String     invoiceNumber,
                          String     orderNumber,
                          String     unitCost,
                          String     shippingCharge,
                          String     owner,
                          String     receivedCondition,
                          String     currentStatus,
                          String     deploymentStatus)
    {
      setSerialNumber       (serialNumber);
      setPartNumber         (partNumber);
      setEquipType          (equipType);
      setEquipDesc          (equipDesc);
      setReceivedDateSorted (receivedDateSorted);
      setReceivedDate       (receivedDate);
      setInvoiceNumber      (invoiceNumber);
      setOrderNumber        (orderNumber);
      setUnitCost           (unitCost);
      setShippingCharge     (shippingCharge);
      setOwner              (owner);
      setReceivedCondition  (receivedCondition);
      setCurrentStatus      (currentStatus);
      setDeploymentStatus   (deploymentStatus);
    }
    
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }
    
    public void setSerialNumber(String serialNumber)
    {
      this.serialNumber = processStringField(serialNumber);
    }
    public String getSerialNumber()
    {
      return this.serialNumber;
    }

    public void setPartNumber(String partNumber)
    {
      this.partNumber = processStringField(partNumber);
    }
    public String getPartNumber()
    {
      return this.partNumber;
    }

    public void setEquipType(String equipType)
    {
      this.equipType = processStringField(equipType);
    }
    public String getEquipType()
    {
      return this.equipType;
    }

    public void setEquipDesc(String equipDesc)
    {
      this.equipDesc = processStringField(equipDesc);
    }
    public String getEquipDesc()
    {
      return this.equipDesc;
    }

    public void setReceivedDateSorted(Timestamp receivedDateSorted)
    {
      this.receivedDateSorted = receivedDateSorted;
    }
    public String getReceivedDateSorted()
    {
      return DateTimeFormatter.getFormattedDate(receivedDateSorted, "yyyyMMddHHmmss");
    }

    public void setReceivedDate(String receivedDate)
    {
      this.receivedDate = processStringField(receivedDate);
    }
    public String getReceivedDate()
    {
      return this.receivedDate;
    }

    public void setInvoiceNumber(String invoiceNumber)
    {
      this.invoiceNumber = processStringField(invoiceNumber);
    }
    public String getInvoiceNumber()
    {
      return this.invoiceNumber;
    }

    public void setOrderNumber(String orderNumber)
    {
      this.orderNumber = processStringField(orderNumber);
    }
    public String getOrderNumber()
    {
      return this.orderNumber;
    }

    public void setUnitCost(String unitCost)
    {
      this.unitCost = processStringField(unitCost);
    }
    public String getUnitCost()
    {
      return this.unitCost;
    }

    public void setShippingCharge(String shippingCharge)
    {
      this.shippingCharge = processStringField(shippingCharge);
    }
    public String getShippingCharge()
    {
      return this.shippingCharge;
    }

    public void setOwner(String owner)
    {
      this.owner = processStringField(owner);
    }
    public String getOwner()
    {
      return this.owner;
    }

    public void setReceivedCondition(String receivedCondition)
    {
      this.receivedCondition = processStringField(receivedCondition);
    }
    public String getReceivedCondition()
    {
      return this.receivedCondition;
    }

    public void setCurrentStatus(String currentStatus)
    {
      this.currentStatus = processStringField(currentStatus);
    }
    public String getCurrentStatus()
    {
      return this.currentStatus;
    }

    public void setDeploymentStatus(String deploymentStatus)
    {
      this.deploymentStatus = processStringField(deploymentStatus);
    }
    public String getDeploymentStatus()
    {
      return this.deploymentStatus;
    }
  }

  public class EquipDataComparator
    implements Comparator
  {

    public final static int   SB_SERIAL_NUMBER        = 0;
    public final static int   SB_EQUIP_TYPE           = 1;
    public final static int   SB_EQUIP_DESC           = 2;
    public final static int   SB_RECEIVED_DATE        = 3;
    public final static int   SB_INVOICE_NUMBER       = 4;
    public final static int   SB_ORDER_NUMBER         = 5;
    public final static int   SB_UNIT_COST            = 6;
    public final static int   SB_SHIPPING_CHARGE      = 7;
    public final static int   SB_OWNER                = 8;
    public final static int   SB_RECEIVED_CONDITION   = 9;
    public final static int   SB_CURRENT_STATUS       = 10;
    public final static int   SB_DEPLOYMENT_STATUS    = 11;

    private int               sortBy;
    
    private boolean           sortAscending       = false;
    
    public EquipDataComparator()
    {
      this.sortBy = SB_RECEIVED_DATE;
    }
    
    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }
    
    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_SERIAL_NUMBER && sortBy <= SB_DEPLOYMENT_STATUS)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }
        
        this.sortBy = sortBy;
      }
    }
  
    int compare(EquipmentData o1, EquipmentData o2)
    {
      int result    = 0;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {

          case SB_SERIAL_NUMBER:
            compareString1 = o1.getSerialNumber()       + o1.getReceivedDateSorted();
            compareString2 = o2.getSerialNumber()       + o2.getReceivedDateSorted();
            break;
          
          case SB_EQUIP_TYPE:
            compareString1 = o1.getEquipType()          + o1.getReceivedDateSorted() + o1.getSerialNumber();
            compareString2 = o2.getEquipType()          + o2.getReceivedDateSorted() + o2.getSerialNumber();
            break;

          case SB_EQUIP_DESC:
            compareString1 = o1.getEquipDesc()          + o1.getReceivedDateSorted() + o1.getSerialNumber();
            compareString2 = o2.getEquipDesc()          + o2.getReceivedDateSorted() + o2.getSerialNumber();
            break;
            
          case SB_RECEIVED_DATE:
            compareString1 = o1.getReceivedDateSorted() + o1.getSerialNumber();
            compareString2 = o2.getReceivedDateSorted() + o2.getSerialNumber();
            break;
          
          case SB_INVOICE_NUMBER:
            compareString1 = o1.getInvoiceNumber()      + o1.getReceivedDateSorted() + o1.getSerialNumber();
            compareString2 = o2.getInvoiceNumber()      + o2.getReceivedDateSorted() + o2.getSerialNumber();
            break;
            
          case SB_ORDER_NUMBER:
            compareString1 = o1.getOrderNumber()        + o1.getReceivedDateSorted() + o1.getSerialNumber();
            compareString2 = o2.getOrderNumber()        + o2.getReceivedDateSorted() + o2.getSerialNumber();
            break;
            
          case SB_UNIT_COST:
            compareString1 = o1.getUnitCost()           + o1.getReceivedDateSorted() + o1.getSerialNumber();
            compareString2 = o2.getUnitCost()           + o2.getReceivedDateSorted() + o2.getSerialNumber();
            break;

          case SB_SHIPPING_CHARGE:
            compareString1 = o1.getShippingCharge()     + o1.getReceivedDateSorted() + o1.getSerialNumber();
            compareString2 = o2.getShippingCharge()     + o2.getReceivedDateSorted() + o2.getSerialNumber();
            break;
            
          case SB_OWNER:
            compareString1 = o1.getOwner()              + o1.getReceivedDateSorted() + o1.getSerialNumber();
            compareString2 = o2.getOwner()              + o2.getReceivedDateSorted() + o2.getSerialNumber();
            break;
            
          case SB_RECEIVED_CONDITION:
            compareString1 = o1.getReceivedCondition()  + o1.getReceivedDateSorted() + o1.getSerialNumber();
            compareString2 = o2.getReceivedCondition()  + o2.getReceivedDateSorted() + o2.getSerialNumber();
            break;
            
          case SB_CURRENT_STATUS:
            compareString1 = o1.getCurrentStatus()      + o1.getReceivedDateSorted() + o1.getSerialNumber();
            compareString2 = o2.getCurrentStatus()      + o2.getReceivedDateSorted() + o2.getSerialNumber();
            break;

          case SB_DEPLOYMENT_STATUS:
            compareString1 = o1.getDeploymentStatus()   + o1.getReceivedDateSorted() + o1.getSerialNumber();
            compareString2 = o2.getDeploymentStatus()   + o2.getReceivedDateSorted() + o2.getSerialNumber();
            break;

          default:
            break;
        }
        
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(EquipmentData o1, EquipmentData o2)
    {
      boolean result    = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_SERIAL_NUMBER:
            compareString1 = o1.getSerialNumber()       + o1.getReceivedDateSorted();
            compareString2 = o2.getSerialNumber()       + o2.getReceivedDateSorted();
            break;
          
          case SB_EQUIP_TYPE:
            compareString1 = o1.getEquipType()          + o1.getReceivedDateSorted();
            compareString2 = o2.getEquipType()          + o2.getReceivedDateSorted();
            break;

          case SB_EQUIP_DESC:
            compareString1 = o1.getEquipDesc()          + o1.getReceivedDateSorted();
            compareString2 = o2.getEquipDesc()          + o2.getReceivedDateSorted();
            break;
            
          case SB_RECEIVED_DATE:
            compareString1 = o1.getReceivedDateSorted() + o1.getSerialNumber();
            compareString2 = o2.getReceivedDateSorted() + o2.getSerialNumber();
            break;
          
          case SB_INVOICE_NUMBER:
            compareString1 = o1.getInvoiceNumber()      + o1.getReceivedDateSorted();
            compareString2 = o2.getInvoiceNumber()      + o2.getReceivedDateSorted();
            break;
            
          case SB_ORDER_NUMBER:
            compareString1 = o1.getOrderNumber()        + o1.getReceivedDateSorted();
            compareString2 = o2.getOrderNumber()        + o2.getReceivedDateSorted();
            break;
            
          case SB_UNIT_COST:
            compareString1 = o1.getUnitCost()           + o1.getReceivedDateSorted();
            compareString2 = o2.getUnitCost()           + o2.getReceivedDateSorted();
            break;

          case SB_SHIPPING_CHARGE:
            compareString1 = o1.getShippingCharge()     + o1.getReceivedDateSorted();
            compareString2 = o2.getShippingCharge()     + o2.getReceivedDateSorted();
            break;
            
          case SB_OWNER:
            compareString1 = o1.getOwner()              + o1.getReceivedDateSorted();
            compareString2 = o2.getOwner()              + o2.getReceivedDateSorted();
            break;
            
          case SB_RECEIVED_CONDITION:
            compareString1 = o1.getReceivedCondition()  + o1.getReceivedDateSorted();
            compareString2 = o2.getReceivedCondition()  + o2.getReceivedDateSorted();
            break;
            
          case SB_CURRENT_STATUS:
            compareString1 = o1.getCurrentStatus()      + o1.getReceivedDateSorted();
            compareString2 = o2.getCurrentStatus()      + o2.getReceivedDateSorted();
            break;

          case SB_DEPLOYMENT_STATUS:
            compareString1 = o1.getDeploymentStatus()   + o1.getReceivedDateSorted();
            compareString2 = o2.getDeploymentStatus()   + o2.getReceivedDateSorted();
            break;

          default:
            break;
        }

        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((EquipmentData)o1, (EquipmentData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((EquipmentData)o1, (EquipmentData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }

  private String              lookupValue         = "";
  private String              lastLookupValue     = "";
  
  private int                 action              = mesConstants.LU_ACTION_INVALID;
  private String              actionDescription   = "Invalid Action";
  
  private EquipDataComparator edc                 = new EquipDataComparator();
  private Vector              lookupResults       = new Vector();
  private TreeSet             sortedResults       = null;
  
  private boolean             submitted           = false;
  private boolean             refresh             = false;
  
  //search criteria
  private int                 owner               = -1;
  private int                 lastOwner           = -1;
  
  private String              equipType           = "-1";
  private String              lastEquipType       = "-1";

  private String              equipModel          = "-1";
  private String              lastEquipModel      = "-1";

  private int                 lastFromMonth       = -1;
  private int                 lastFromDay         = -1;
  private int                 lastFromYear        = -1;
  private int                 lastToMonth         = -1;
  private int                 lastToDay           = -1;
  private int                 lastToYear          = -1;
  
  public  Vector              clients              = new Vector();
  public  Vector              clientValues         = new Vector();

  public  Vector              equipTypes           = new Vector();
  public  Vector              equipTypeValues      = new Vector();

  public  Vector              equipModels          = new Vector();
  public  Vector              equipModelValues     = new Vector();

  public ReceivedEquipReport()
  {
    fillDropDowns();
    initDate(-1, 0);
  }
  
  public synchronized void forceRefresh(boolean refresh)
  {
    this.refresh = refresh;
  }
  
  private boolean hasLookupChanged()
  {
    boolean result = false;
    
    if( refresh                               ||
        owner           != lastOwner          ||
        fromMonth       != lastFromMonth      ||
        fromDay         != lastFromDay        ||
        fromYear        != lastFromYear       ||
        toMonth         != lastToMonth        ||
        toDay           != lastToDay          ||
        toYear          != lastToYear         ||
        ! equipType.equals(lastEquipType)     ||
        ! equipModel.equals(lastEquipModel)   ||
        ! lookupValue.equals(lastLookupValue)   
      )
    {
      result = true;
    }
    
    return result;
  }
  
  /*
  ** METHOD fillDropDowns
  **
  ** Fills the drop down boxes
  */  
  public synchronized void fillDropDowns()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      if(clients.size() == 0)
      {
        clients.clear();
        clientValues.clear();
        equipTypes.clear();
        equipTypeValues.clear();
        equipModels.clear();
        equipModelValues.clear();
      
        // clients
        clients.add("All Clients");
        clientValues.add("-1");
        /*@lineinfo:generated-code*//*@lineinfo:609^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  inventory_owner_name, app_type_code 
//            from    app_type
//            where   SEPARATE_INVENTORY = 'Y'
//            order by inventory_owner_name asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  inventory_owner_name, app_type_code \n          from    app_type\n          where   SEPARATE_INVENTORY = 'Y'\n          order by inventory_owner_name asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.ReceivedEquipReport",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.ReceivedEquipReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:615^9*/
      
        rs = it.getResultSet();
        while(rs.next())
        {
          clients.add(rs.getString("inventory_owner_name"));
          clientValues.add(rs.getString("app_type_code"));
        }
      
        rs.close();
        it.close();
      
        // equip types
        equipTypes.add("All Equip Types");
        equipTypeValues.add("-1");

        /*@lineinfo:generated-code*//*@lineinfo:631^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    equiptype
//            where   equiptype_code not in (7,8,9)
//            order by equiptype_description asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    equiptype\n          where   equiptype_code not in (7,8,9)\n          order by equiptype_description asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.inventory.ReceivedEquipReport",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.inventory.ReceivedEquipReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:637^9*/
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          equipTypes.add(rs.getString("equiptype_description"));
          equipTypeValues.add(rs.getString("equiptype_code"));
        }
        
        rs.close();
        it.close();

        // equip models
        equipModels.add("All Equip Desc");
        equipModelValues.add("-1");

        /*@lineinfo:generated-code*//*@lineinfo:654^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_model,equip_descriptor
//            from    equipment
//            where   equiptype_code not in (7,8,9) and
//                    used_in_inventory = 'Y'
//            order by equip_descriptor asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_model,equip_descriptor\n          from    equipment\n          where   equiptype_code not in (7,8,9) and\n                  used_in_inventory = 'Y'\n          order by equip_descriptor asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.inventory.ReceivedEquipReport",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.inventory.ReceivedEquipReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:661^9*/
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          equipModels.add(rs.getString("equip_descriptor"));
          equipModelValues.add(rs.getString("equip_model"));
        }
        
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("fillDropDowns()", e.toString());
      cleanUp();
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public synchronized void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CALL_TRACKING);
    }
  }
  
  /*
  ** METHOD public void getData()
  **
  */
  public synchronized void getData()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    try
    {
      connect();
      
      if (hasLookupChanged())
      {
        // set up longLookup value (numeric version of lookup value)
        long longLookup;
        try
        {
          longLookup = Long.parseLong(lookupValue.trim());
        }
        catch (Exception e) 
        {
          longLookup = -1;
        }
      
        // add wildcards to stringLookup so we can match partial strings
        String stringLookup = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup = "passall";
        }
        else
        {
          stringLookup = "%" + lookupValue.toUpperCase() + "%";
        }

        // no wildcards to stringLookup
        String stringLookup2 = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup2 = "passall";
        }
        else
        {
          stringLookup2 = lookupValue.toUpperCase();
        }

        Date fromDate = getSqlFromDate();
        Date toDate   = getSqlToDate(); 
        
        /*@lineinfo:generated-code*//*@lineinfo:751^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_serial_number             serial_number,
//                    ei.ei_part_number               part_number,
//                    ei.ei_received_date             received_date,
//                    epo.ep_invoice_number           invoice_number,
//                    ei.ei_transaction_id            transaction_id,
//                    ei.ei_unit_cost                 unit_cost,
//                    ei.ei_shipping_cost             shipping_charge,
//                    at.inventory_owner_name         owner,
//                    ec.ec_class_name                received_condition,
//                    es.equip_status_desc            current_status,
//                    elt.equiplendtype_description   deployment_status,
//                    et.equiptype_description        equip_type,
//                    eq.equip_descriptor             equip_desc
//            from    equipment                       eq,
//                    equiplendtype                   elt,
//                    equiptype                       et,
//                    equip_status                    es,
//                    equip_inventory                 ei,
//                    equip_class                     ec,
//                    app_type                        at,
//                    equip_purchase_order            epo
//            where   trunc(ei.ei_received_date) between :fromDate and :toDate and
//                    ei.ei_lrb             = elt.equiplendtype_code  and
//                    ei.EI_PART_NUMBER     = eq.equip_model          and
//                    ei.ei_original_class  = ec.ec_class_id          and
//                    ei.ei_owner           = at.app_type_code        and 
//                    eq.equiptype_code     = et.equiptype_code       and 
//                    ei.ei_status          = es.equip_status_id      and
//                    ei.ei_transaction_id  = epo.ep_transaction_id(+)and
//                    (-1 = :owner or ei.ei_owner = :owner)           and
//                    ('-1' = :equipType or eq.equiptype_code = :equipType) and
//                    ('-1' = :equipModel or eq.equip_model = :equipModel) 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_serial_number             serial_number,\n                  ei.ei_part_number               part_number,\n                  ei.ei_received_date             received_date,\n                  epo.ep_invoice_number           invoice_number,\n                  ei.ei_transaction_id            transaction_id,\n                  ei.ei_unit_cost                 unit_cost,\n                  ei.ei_shipping_cost             shipping_charge,\n                  at.inventory_owner_name         owner,\n                  ec.ec_class_name                received_condition,\n                  es.equip_status_desc            current_status,\n                  elt.equiplendtype_description   deployment_status,\n                  et.equiptype_description        equip_type,\n                  eq.equip_descriptor             equip_desc\n          from    equipment                       eq,\n                  equiplendtype                   elt,\n                  equiptype                       et,\n                  equip_status                    es,\n                  equip_inventory                 ei,\n                  equip_class                     ec,\n                  app_type                        at,\n                  equip_purchase_order            epo\n          where   trunc(ei.ei_received_date) between  :1  and  :2  and\n                  ei.ei_lrb             = elt.equiplendtype_code  and\n                  ei.EI_PART_NUMBER     = eq.equip_model          and\n                  ei.ei_original_class  = ec.ec_class_id          and\n                  ei.ei_owner           = at.app_type_code        and \n                  eq.equiptype_code     = et.equiptype_code       and \n                  ei.ei_status          = es.equip_status_id      and\n                  ei.ei_transaction_id  = epo.ep_transaction_id(+)and\n                  (-1 =  :3  or ei.ei_owner =  :4 )           and\n                  ('-1' =  :5  or eq.equiptype_code =  :6 ) and\n                  ('-1' =  :7  or eq.equip_model =  :8 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.inventory.ReceivedEquipReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromDate);
   __sJT_st.setDate(2,toDate);
   __sJT_st.setInt(3,owner);
   __sJT_st.setInt(4,owner);
   __sJT_st.setString(5,equipType);
   __sJT_st.setString(6,equipType);
   __sJT_st.setString(7,equipModel);
   __sJT_st.setString(8,equipModel);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.inventory.ReceivedEquipReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:785^9*/
        
        lookupResults.clear();
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          lookupResults.add(new EquipmentData(rs.getString("serial_number"),  
                                              rs.getString("part_number"),
                                              rs.getString("equip_type"), 
                                              rs.getString("equip_desc"),
                                              rs.getTimestamp("received_date"),
                                              DateTimeFormatter.getFormattedDate(rs.getTimestamp("received_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT),
                                              rs.getString("invoice_number"), 
                                              rs.getString("transaction_id"), 
                                              rs.getString("unit_cost"), 
                                              rs.getString("shipping_charge"), 
                                              rs.getString("owner"), 
                                              rs.getString("received_condition"),
                                              rs.getString("current_status"), 
                                              rs.getString("deployment_status")));
        }
      
        rs.close();
        it.close();
        
        lastOwner           = owner;
        lastEquipType       = equipType;
        lastEquipModel      = equipModel;
        lastFromMonth       = fromMonth;
        lastFromDay         = fromDay;
        lastFromYear        = fromYear;
        lastToMonth         = toMonth;
        lastToDay           = toDay;
        lastToYear          = toYear;
        lastLookupValue     = lookupValue;
      }
      
      // now create the sorted tree
      if(sortedResults != null)
      {
        sortedResults.clear();
      }
      
      sortedResults = null;
      
      sortedResults = new TreeSet(edc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public synchronized Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }
  
  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public synchronized String getActionDescription()
  {
    return this.actionDescription;
  }
  
  /*
  ** ACCESSORS
  */
  public synchronized String getLookupValue()
  {
    return lookupValue;
  }
  public synchronized void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }
  
  public synchronized int getAction()
  {
    return this.action;
  }
  public synchronized void setAction(int action)
  {
    this.actionDescription = "Received Equipment Report";
  }
  public void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }
    
    setAction(actionNum);
  }
  
  public synchronized void setSortBy(String sortBy)
  {
    try
    {
      edc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }
 
  public synchronized void setEquipType(String equipType)
  {
    this.equipType = equipType;
  }
 
  public synchronized String getEquipType()
  {
    return this.equipType;
  }

  public synchronized void setEquipModel(String equipModel)
  {
    this.equipModel = equipModel;
  }
 
  public synchronized String getEquipModel()
  {
    return this.equipModel;
  }
  
  public synchronized void setClient(String owner)
  {
    try
    {
      this.owner = Integer.parseInt(owner);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getClient()
  {
    return Integer.toString(this.owner);
  }
 
  public synchronized void setSubmitted(String submitted)
  {
    this.submitted = true;
  }
 
  public synchronized boolean isSubmitted()
  {
    return this.submitted;
  }
}/*@lineinfo:generated-code*/