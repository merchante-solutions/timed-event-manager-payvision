/*@lineinfo:filename=EquipSearch*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/EquipSearch.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/20/04 12:42p $
  Version            : $Revision: 12 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.inventory;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class EquipSearch extends DateSQLJBean
{
  public class EquipmentData
  {
    private String      serialNum;
    private String      client;
    private String      prodType;
    private String      prodName;
    private String      prodDesc;
    private String      prodCondition;
    private String      prodStatus;
    private String      cost;
    private String      deployDate;
    private Timestamp   deployDateSorted;
    private String      receiveDate;
    private Timestamp   receiveDateSorted;
    private String      dbaName;
    private String      merchantNumber;
    private String      associationNumber;
    private String      partNumber;
    private String      deployType;
    private String      deployPrice;

    public EquipmentData()
    {
      serialNum           = "";
      client              = "";
      prodType            = "";
      prodName            = "";
      prodDesc            = "";
      prodCondition       = "";
      prodStatus          = "";
      cost                = "";
      deployDate          = "";
      deployDateSorted    = null;
      receiveDate         = "";
      receiveDateSorted   = null;
      dbaName             = "";
      merchantNumber      = "";
      associationNumber   = "";
      partNumber          = "";
      deployType          = "";
      deployPrice         = "";
    }
    
    public EquipmentData( String    serialNum,        
                          String    client,           
                          String    prodType,
                          String    prodName,         
                          String    prodDesc,         
                          String    prodCondition,    
                          String    prodStatus,       
                          String    cost,
                          String    deployDate,       
                          Timestamp deployDateSorted, 
                          String    receiveDate,
                          Timestamp receiveDateSorted,
                          String    dbaName,          
                          String    merchantNumber,   
                          String    associationNumber,
                          String    partNumber,
                          String    deployType,       
                          String    deployPrice)

    {
      setSerialNum(serialNum);
      setClient(client);
      setProdType(prodType);
      setProdName(prodName);
      setProdDesc(prodDesc);
      setProdCondition(prodCondition);
      setProdStatus(prodStatus);
      setCost(cost);
      setDeployDate(deployDate);
      setDeployDateSorted(deployDateSorted);
      setReceiveDate(receiveDate);
      setReceiveDateSorted(receiveDateSorted);
      setDbaName(dbaName);
      setMerchantNumber(merchantNumber);
      setAssociationNumber(associationNumber);
      setPartNumber(partNumber);
      setDeployType(deployType);
      setDeployPrice(deployPrice);      
    }
    
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }
    
    public void setSerialNum(String serialNum)
    {
      this.serialNum = processStringField(serialNum);
    }
    public String getSerialNum()
    {
      return this.serialNum;
    }

    public void setClient(String client)
    {
      this.client = processStringField(client);
    }
    public String getClient()
    {
      return this.client;
    }

    public void setProdType(String prodType)
    {
      this.prodType = processStringField(prodType);
    }
    public String getProdType()
    {
      return this.prodType;
    }

    public void setProdName(String prodName)
    {
      this.prodName = processStringField(prodName);
    }
    public String getProdName()
    {
      return this.prodName;
    }

    public void setProdDesc(String prodDesc)
    {
      this.prodDesc = processStringField(prodDesc);
    }
    public String getProdDesc()
    {
      return this.prodDesc;
    }

    public void setProdCondition(String prodCondition)
    {
      this.prodCondition = processStringField(prodCondition);
    }
    public String getProdCondition()
    {
      return this.prodCondition;
    }

    public void setProdStatus(String prodStatus)
    {
      this.prodStatus = processStringField(prodStatus);
    }
    public String getProdStatus()
    {
      return this.prodStatus;
    }

    public void setCost(String cost)
    {
      this.cost = processStringField(cost);
    }
    public String getCost()
    {
      return this.cost;
    }

    public void setDeployDate(String deployDate)
    {
      this.deployDate = processStringField(deployDate);
    }
    public String getDeployDate()
    {
      return this.deployDate;
    }

    public void setDeployDateSorted(Timestamp deployDateSorted)
    {
      this.deployDateSorted = deployDateSorted;
    }
    public String getDeployDateSorted()
    {
      return DateTimeFormatter.getFormattedDate(deployDateSorted, "yyyyMMddHHmmss");
    }

    public void setReceiveDate(String receiveDate)
    {
      this.receiveDate = processStringField(receiveDate);
    }
    public String getReceiveDate()
    {
      return this.receiveDate;
    }

    public void setReceiveDateSorted(Timestamp receiveDateSorted)
    {
      this.receiveDateSorted = receiveDateSorted;
    }
    public String getReceiveDateSorted()
    {
      return DateTimeFormatter.getFormattedDate(receiveDateSorted, "yyyyMMddHHmmss");
    }

    public void setDbaName(String dbaName)
    {
      this.dbaName = processStringField(dbaName);
    }
    public String getDbaName()
    {
      return this.dbaName;
    }

    public void setMerchantNumber(String merchantNumber)
    {
      this.merchantNumber = processStringField(merchantNumber);
    }
    public String getMerchantNumber()
    {
      return this.merchantNumber;
    }

    public void setAssociationNumber(String associationNumber)
    {
      this.associationNumber = processStringField(associationNumber);
    }
    public String getAssociationNumber()
    {
      return this.associationNumber;
    }

    public void setDeployType(String deployType)
    {
      this.deployType = processStringField(deployType);
    }
    public String getDeployType()
    {
      return this.deployType;
    }
   
    public void setDeployPrice(String deployPrice)
    {
      this.deployPrice = processStringField(deployPrice);
    }
    public String getDeployPrice()
    {
      return this.deployPrice;
    }

    public void setPartNumber(String partNumber)
    {
      this.partNumber = processStringField(partNumber);
    }
    public String getPartNumber()
    {
      return this.partNumber;
    }
  }
  
  public class EquipDataComparator
    implements Comparator
  {
    public final static int   SB_MERCHANT_NUMBER      = 0;
    public final static int   SB_DEPLOY_DATE          = 1;
    public final static int   SB_DBA_NAME             = 2;
    public final static int   SB_CLIENT               = 3;
    public final static int   SB_SERIAL_NUMBER        = 4;
    public final static int   SB_DEPLOY_TYPE          = 5;
    public final static int   SB_PROD_NAME            = 6;
    public final static int   SB_PROD_DESC            = 7;
    public final static int   SB_PROD_CONDITION       = 8;
    public final static int   SB_PROD_STATUS          = 9;
    public final static int   SB_PROD_TYPE            = 10;
    public final static int   SB_DEPLOY_PRICE         = 11;
    public final static int   SB_ASSO_NUMBER          = 12;
    public final static int   SB_COST                 = 13;
    public final static int   SB_RECEIVE_DATE         = 14;

    private int               sortBy;
    
    private boolean           sortAscending       = false;
    
    public EquipDataComparator()
    {
      this.sortBy = SB_SERIAL_NUMBER;
    }
    
    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }
    
    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_MERCHANT_NUMBER && sortBy <= SB_RECEIVE_DATE)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }
        
        this.sortBy = sortBy;
      }
    }
  
    int compare(EquipmentData o1, EquipmentData o2)
    {
      int result    = 0;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.getMerchantNumber() + o1.getSerialNum();
            compareString2 = o2.getMerchantNumber() + o2.getSerialNum();
            break;
          
          case SB_ASSO_NUMBER:
            compareString1 = o1.getAssociationNumber() + o1.getSerialNum();
            compareString2 = o2.getAssociationNumber() + o2.getSerialNum();
            break;
            
          case SB_DEPLOY_DATE:
            compareString1 = o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getDeployDateSorted() + o2.getSerialNum();
            break;

          case SB_RECEIVE_DATE:
            compareString1 = o1.getReceiveDateSorted() + o1.getSerialNum();
            compareString2 = o2.getReceiveDateSorted() + o2.getSerialNum();
            break;
          
          case SB_DBA_NAME:
            compareString1 = o1.getDbaName() + o1.getSerialNum();
            compareString2 = o2.getDbaName() + o2.getSerialNum();
            break;
            
          case SB_CLIENT:
            compareString1 = o1.getClient() + o1.getSerialNum();
            compareString2 = o2.getClient() + o2.getSerialNum();
            break;
            
          case SB_SERIAL_NUMBER:
            compareString1 = o1.getSerialNum();
            compareString2 = o2.getSerialNum();
            break;
            
          case SB_DEPLOY_TYPE:
            compareString1 = o1.getDeployType() + o1.getSerialNum();
            compareString2 = o2.getDeployType() + o2.getSerialNum();
            break;
            
          case SB_PROD_NAME:
            compareString1 = o1.getProdName() + o1.getSerialNum();
            compareString2 = o2.getProdName() + o2.getSerialNum();
            break;
            
          case SB_PROD_DESC:
            compareString1 = o1.getProdDesc() + o1.getSerialNum();
            compareString2 = o2.getProdDesc() + o2.getSerialNum();
            break;
            
          case SB_PROD_CONDITION:
            compareString1 = o1.getProdCondition() + o1.getSerialNum();
            compareString2 = o2.getProdCondition() + o2.getSerialNum();
            break;

          case SB_PROD_STATUS:
            compareString1 = o1.getProdStatus() + o1.getSerialNum();
            compareString2 = o2.getProdStatus() + o2.getSerialNum();
            break;
            
          case SB_PROD_TYPE:
            compareString1 = o1.getProdType() + o1.getSerialNum();
            compareString2 = o2.getProdType() + o2.getSerialNum();
            break;
            
          case SB_DEPLOY_PRICE:
            compareString1 = o1.getDeployPrice() + o1.getSerialNum();
            compareString2 = o2.getDeployPrice() + o2.getSerialNum();
            break;

          case SB_COST:
            compareString1 = o1.getCost() + o1.getSerialNum();
            compareString2 = o2.getCost() + o2.getSerialNum();
          
          default:
            break;
        }
        
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(EquipmentData o1, EquipmentData o2)
    {
      boolean result    = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.getMerchantNumber() + o1.getSerialNum();
            compareString2 = o2.getMerchantNumber() + o2.getSerialNum();
            break;
          
          case SB_ASSO_NUMBER:
            compareString1 = o1.getAssociationNumber() + o1.getSerialNum();
            compareString2 = o2.getAssociationNumber() + o2.getSerialNum();
            break;
            
          case SB_DEPLOY_DATE:
            compareString1 = o1.getDeployDateSorted() + o1.getSerialNum();
            compareString2 = o2.getDeployDateSorted() + o2.getSerialNum();
            break;
          
          case SB_RECEIVE_DATE:
            compareString1 = o1.getReceiveDateSorted() + o1.getSerialNum();
            compareString2 = o2.getReceiveDateSorted() + o2.getSerialNum();
            break;

          case SB_DBA_NAME:
            compareString1 = o1.getDbaName() + o1.getSerialNum();
            compareString2 = o2.getDbaName() + o2.getSerialNum();
            break;
            
          case SB_CLIENT:
            compareString1 = o1.getClient() + o1.getSerialNum();
            compareString2 = o2.getClient() + o2.getSerialNum();
            break;
            
          case SB_SERIAL_NUMBER:
            compareString1 = o1.getSerialNum();
            compareString2 = o2.getSerialNum();
            break;
            
          case SB_DEPLOY_TYPE:
            compareString1 = o1.getDeployType() + o1.getSerialNum();
            compareString2 = o2.getDeployType() + o2.getSerialNum();
            break;
            
          case SB_PROD_NAME:
            compareString1 = o1.getProdName() + o1.getSerialNum();
            compareString2 = o2.getProdName() + o2.getSerialNum();
            break;
            
          case SB_PROD_DESC:
            compareString1 = o1.getProdDesc() + o1.getSerialNum();
            compareString2 = o2.getProdDesc() + o2.getSerialNum();
            break;
            
          case SB_PROD_CONDITION:
            compareString1 = o1.getProdCondition() + o1.getSerialNum();
            compareString2 = o2.getProdCondition() + o2.getSerialNum();
            break;

          case SB_PROD_STATUS:
            compareString1 = o1.getProdStatus() + o1.getSerialNum();
            compareString2 = o2.getProdStatus() + o2.getSerialNum();
            break;
            
          case SB_PROD_TYPE:
            compareString1 = o1.getProdType() + o1.getSerialNum();
            compareString2 = o2.getProdType() + o2.getSerialNum();
            break;
            
          case SB_DEPLOY_PRICE:
            compareString1 = o1.getDeployPrice() + o1.getSerialNum();
            compareString2 = o2.getDeployPrice() + o2.getSerialNum();
            break;

          case SB_COST:
            compareString1 = o1.getCost() + o1.getSerialNum();
            compareString2 = o2.getCost() + o2.getSerialNum();
          
          default:
            break;
        }

        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((EquipmentData)o1, (EquipmentData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((EquipmentData)o1, (EquipmentData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }
  
  private String              lookupValue           = "";
  private String              lastLookupValue       = "";
  
  private int                 action                = mesConstants.LU_ACTION_INVALID;
  private String              actionDescription     = "Invalid Action";
  
  private EquipDataComparator edc                   = new EquipDataComparator();
  private Vector              lookupResults         = new Vector();
  private TreeSet             sortedResults         = null;
  
  private boolean             submitted             = false;
  private boolean             refresh               = false;
  private boolean             edit                  = false;
  
  //search criteria
  private String              startNew              = "";
  private String              lastStartNew          = "";

  private int                 client                = -1;
  private int                 lastClient            = -1;

  private int                 condition             = -1;
  private int                 lastCondition         = -1;
  
  private int                 status                = -1;
  private int                 lastStatus            = -1;

  private int                 deploymentType        = -1;
  private int                 lastDeploymentType    = -1;
  
  private int                 equipName             = -1;
  private int                 lastEquipName         = -1;

  private int                 equipType             = -1;
  private int                 lastEquipType         = -1;

  private String              equipModel            = "-1";
  private String              lastEquipModel        = "-1";

  public  Vector              clients               = new Vector();
  public  Vector              clientValues          = new Vector();

  public  Vector              conditions            = new Vector();
  public  Vector              conditionValues       = new Vector();

  public  Vector              statuses              = new Vector();
  public  Vector              statusValues          = new Vector();

  public  Vector              deploymentTypes       = new Vector();
  public  Vector              deploymentTypeValues  = new Vector();

  public  Vector              equipNames            = new Vector();
  public  Vector              equipNameValues       = new Vector();

  public  Vector              equipTypes            = new Vector();
  public  Vector              equipTypeValues       = new Vector();

  public  Vector              equipModels           = new Vector();
  public  Vector              equipModelValues      = new Vector();

  public EquipSearch()
  {
    fillDropDowns();
    initDate(-1, 0);
  }
  
  public synchronized void forceRefresh(boolean refresh)
  {
    this.refresh = refresh;
  }
  
  private boolean hasLookupChanged()
  {
    boolean result = false;
    
    if( refresh                               ||
        startNew        != lastStartNew       ||
        client          != lastClient         ||
        condition       != lastCondition      ||
        status          != lastStatus         ||
        deploymentType  != lastDeploymentType ||
        equipName       != lastEquipName      ||
        equipType       != lastEquipType      ||
        ! equipModel.equals(lastEquipModel)   ||
        ! lookupValue.equals(lastLookupValue)   
      )
    {
      result = true;
    }
    
    return result;
  }
  
  /*
  ** METHOD fillDropDowns
  **
  ** Fills the drop down boxes
  */  
  public synchronized void fillDropDowns()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      if(clients.size() == 0)
      {
        clients.clear();
        clientValues.clear();
        conditions.clear();
        conditionValues.clear();
        statuses.clear();
        statusValues.clear();
        deploymentTypes.clear();
        deploymentTypeValues.clear();
        equipNames.clear();
        equipNameValues.clear();
        equipTypes.clear();
        equipTypeValues.clear();
        equipModels.clear();
        equipModelValues.clear();
      
        // clients
        clients.add("All Clients");
        clientValues.add("-1");
        /*@lineinfo:generated-code*//*@lineinfo:720^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  inventory_owner_name, app_type_code 
//            from    app_type
//            where   SEPARATE_INVENTORY = 'Y'
//            order by inventory_owner_name asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  inventory_owner_name, app_type_code \n          from    app_type\n          where   SEPARATE_INVENTORY = 'Y'\n          order by inventory_owner_name asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.EquipSearch",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.EquipSearch",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:726^9*/
      
        rs = it.getResultSet();
        while(rs.next())
        {
          clients.add(rs.getString("inventory_owner_name"));
          clientValues.add(rs.getString("app_type_code"));
        }
      
        rs.close();
        it.close();
      
        // condition
        conditions.add("All Conditions");
        conditionValues.add("-1");
        /*@lineinfo:generated-code*//*@lineinfo:741^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  * 
//            from    equip_class
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  * \n          from    equip_class";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.inventory.EquipSearch",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.inventory.EquipSearch",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:745^9*/
      
        rs = it.getResultSet();
        while(rs.next())
        {
          conditions.add(rs.getString("ec_class_name"));
          conditionValues.add(rs.getString("ec_class_id"));
        }
      
        rs.close();
        it.close();

        // status
        statuses.add("All Statuses");
        statusValues.add("-1");
        /*@lineinfo:generated-code*//*@lineinfo:760^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_status_id, equip_status_desc 
//            from    equip_status
//            order by equip_status_order asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_status_id, equip_status_desc \n          from    equip_status\n          order by equip_status_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.inventory.EquipSearch",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.inventory.EquipSearch",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:765^9*/
      
        rs = it.getResultSet();
        while(rs.next())
        {
          statuses.add(rs.getString("equip_status_desc"));
          statusValues.add(rs.getString("equip_status_id"));
        }
      
        rs.close();
        it.close();

        // lrb types
        deploymentTypes.add("All Lend Types");
        deploymentTypeValues.add("-1");
        deploymentTypes.add("All Not Deployed");
        deploymentTypeValues.add("0");
        deploymentTypes.add("All Deployed");
        deploymentTypeValues.add("99");
        /*@lineinfo:generated-code*//*@lineinfo:784^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    equiplendtype
//            where   equiplendtype_code in (1,2,5,6,7,20,26,27,28)
//            order by equiplendtype_description asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    equiplendtype\n          where   equiplendtype_code in (1,2,5,6,7,20,26,27,28)\n          order by equiplendtype_description asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.inventory.EquipSearch",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.inventory.EquipSearch",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:790^9*/
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          deploymentTypes.add(rs.getString("equiplendtype_description"));
          deploymentTypeValues.add(rs.getString("equiplendtype_code"));
        }
        
        rs.close();
        it.close();
        
        // equip types
        equipTypes.add("All Equip Types");
        equipTypeValues.add("-1");

        /*@lineinfo:generated-code*//*@lineinfo:807^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    equiptype
//            where   equiptype_code not in (7,8,9)
//            order by equiptype_description asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    equiptype\n          where   equiptype_code not in (7,8,9)\n          order by equiptype_description asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.inventory.EquipSearch",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.inventory.EquipSearch",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:813^9*/
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          equipTypes.add(rs.getString("equiptype_description"));
          equipTypeValues.add(rs.getString("equiptype_code"));
        }
        
        rs.close();
        it.close();

        // equip names
        equipNames.add("All Equip Mfg.");
        equipNameValues.add("-1");

        /*@lineinfo:generated-code*//*@lineinfo:830^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    equipmfgr
//            where   equipmfgr_mfr_code not in (9)
//            order by equipmfgr_mfr_name asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    equipmfgr\n          where   equipmfgr_mfr_code not in (9)\n          order by equipmfgr_mfr_name asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.inventory.EquipSearch",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.inventory.EquipSearch",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:836^9*/
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          equipNames.add(rs.getString("equipmfgr_mfr_name"));
          equipNameValues.add(rs.getString("equipmfgr_mfr_code"));
        }
        
        rs.close();
        it.close();

        // equip models
        equipModels.add("All Equip Desc");
        equipModelValues.add("-1");

        /*@lineinfo:generated-code*//*@lineinfo:853^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_model,equip_descriptor
//            from    equipment
//            where   equiptype_code not in (7,8,9) and
//                    used_in_inventory = 'Y'
//            order by equip_descriptor asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_model,equip_descriptor\n          from    equipment\n          where   equiptype_code not in (7,8,9) and\n                  used_in_inventory = 'Y'\n          order by equip_descriptor asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.inventory.EquipSearch",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.inventory.EquipSearch",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:860^9*/
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          equipModels.add(rs.getString("equip_descriptor"));
          equipModelValues.add(rs.getString("equip_model"));
        }
        
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("fillDropDowns()", e.toString());
      cleanUp();
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public synchronized void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CALL_TRACKING);
    }
  }
  
  /*
  ** METHOD public void getData()
  **
  */
  public synchronized void getData()
  {
    ResultSetIterator it    = null;
    ResultSet         rs    = null;
    try
    {
      connect();
      
      if (hasLookupChanged())
      {
        HashSet   foundEquip = new HashSet();
        
        // set up longLookup value (numeric version of lookup value)
        long longLookup;
        try
        {
          longLookup = Long.parseLong(lookupValue.trim());
        }
        catch (Exception e) 
        {
          longLookup = -1;
        }
      
        // add wildcards to stringLookup so we can match partial strings
        String stringLookup = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup = "passall";
        }
        else
        {
          stringLookup = "%" + lookupValue.toUpperCase() + "%";
        }

        // no wildcards to stringLookup
        String stringLookup2 = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup2 = "passall";
        }
        else
        {
          stringLookup2 = lookupValue.toUpperCase();
        }

        // first search the MIF       
        /*@lineinfo:generated-code*//*@lineinfo:950^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.EI_SERIAL_NUMBER             serial_number,
//                    ei.ei_received_date             receive_date,
//                    ei.ei_deployed_date             deploy_date,
//                    ei.ei_merchant_number           merchant_number,
//                    ei.ei_lrb_price                 deploy_price,
//                    ei.ei_unit_cost                 unit_cost,
//                    ei.ei_part_number               part_number,
//                    ec.ec_class_name                condition,
//                    mf.dba_name                     dba_name,
//                    mf.dmagent                      association_number,
//                    elt.equiplendtype_description   deploy_type,
//                    es.equip_status_desc            equip_status,
//                    em.equipmfgr_mfr_name           equip_name,
//                    et.equiptype_description        equip_type,
//                    eq.equip_descriptor             equip_desc,
//                    ap.inventory_owner_name         client_name
//            from    equip_inventory                 ei,
//                    mif                             mf,
//                    equip_status                    es,
//                    equipmfgr                       em,
//                    app_type                        ap,
//                    equipment                       eq,
//                    equiplendtype                   elt,
//                    equiptype                       et,
//                    equip_class                     ec
//            where   ei.ei_merchant_number = mf.merchant_number(+) and
//                    ei.ei_status = es.equip_status_id and
//                    ei.ei_lrb = elt.equiplendtype_code and
//                    ei.ei_owner = ap.app_type_code and
//                    ei.ei_class = ec.ec_class_id and
//                    ei.ei_part_number = eq.equip_model and
//                    eq.equiptype_code = et.equiptype_code and 
//                    eq.EQUIPMFGR_MFR_CODE = em.EQUIPMFGR_MFR_CODE and
//                    (-1 = :client or ei.ei_owner = :client) and
//                    (-1 = :condition or ei.ei_class = :condition) and
//                    (-1 = :status or ei.ei_status = :status) and
//                    (99 != :deploymentType or ei.ei_lrb != 0) and
//                    (-1 = :deploymentType or 99 = :deploymentType or ei.ei_lrb = :deploymentType) and
//                    (-1 = :equipType or eq.equiptype_code = :equipType) and
//                    (-1 = :equipName or eq.EQUIPMFGR_MFR_CODE = :equipName) and
//                    ('-1' = :equipModel or eq.equip_model = :equipModel) and
//                    (
//                      'passall'                 = :stringLookup or
//                      ei.ei_merchant_number     = :longLookup or
//                      ei.EI_TRANSACTION_ID      = :longLookup or
//                      mf.dmagent                = :longLookup or
//                      upper(ei.EI_SERIAL_NUMBER) = :stringLookup2 or 
//                      upper(mf.dba_name) like :stringLookup
//                    )          
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.EI_SERIAL_NUMBER             serial_number,\n                  ei.ei_received_date             receive_date,\n                  ei.ei_deployed_date             deploy_date,\n                  ei.ei_merchant_number           merchant_number,\n                  ei.ei_lrb_price                 deploy_price,\n                  ei.ei_unit_cost                 unit_cost,\n                  ei.ei_part_number               part_number,\n                  ec.ec_class_name                condition,\n                  mf.dba_name                     dba_name,\n                  mf.dmagent                      association_number,\n                  elt.equiplendtype_description   deploy_type,\n                  es.equip_status_desc            equip_status,\n                  em.equipmfgr_mfr_name           equip_name,\n                  et.equiptype_description        equip_type,\n                  eq.equip_descriptor             equip_desc,\n                  ap.inventory_owner_name         client_name\n          from    equip_inventory                 ei,\n                  mif                             mf,\n                  equip_status                    es,\n                  equipmfgr                       em,\n                  app_type                        ap,\n                  equipment                       eq,\n                  equiplendtype                   elt,\n                  equiptype                       et,\n                  equip_class                     ec\n          where   ei.ei_merchant_number = mf.merchant_number(+) and\n                  ei.ei_status = es.equip_status_id and\n                  ei.ei_lrb = elt.equiplendtype_code and\n                  ei.ei_owner = ap.app_type_code and\n                  ei.ei_class = ec.ec_class_id and\n                  ei.ei_part_number = eq.equip_model and\n                  eq.equiptype_code = et.equiptype_code and \n                  eq.EQUIPMFGR_MFR_CODE = em.EQUIPMFGR_MFR_CODE and\n                  (-1 =  :1  or ei.ei_owner =  :2 ) and\n                  (-1 =  :3  or ei.ei_class =  :4 ) and\n                  (-1 =  :5  or ei.ei_status =  :6 ) and\n                  (99 !=  :7  or ei.ei_lrb != 0) and\n                  (-1 =  :8  or 99 =  :9  or ei.ei_lrb =  :10 ) and\n                  (-1 =  :11  or eq.equiptype_code =  :12 ) and\n                  (-1 =  :13  or eq.EQUIPMFGR_MFR_CODE =  :14 ) and\n                  ('-1' =  :15  or eq.equip_model =  :16 ) and\n                  (\n                    'passall'                 =  :17  or\n                    ei.ei_merchant_number     =  :18  or\n                    ei.EI_TRANSACTION_ID      =  :19  or\n                    mf.dmagent                =  :20  or\n                    upper(ei.EI_SERIAL_NUMBER) =  :21  or \n                    upper(mf.dba_name) like  :22 \n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.inventory.EquipSearch",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,client);
   __sJT_st.setInt(2,client);
   __sJT_st.setInt(3,condition);
   __sJT_st.setInt(4,condition);
   __sJT_st.setInt(5,status);
   __sJT_st.setInt(6,status);
   __sJT_st.setInt(7,deploymentType);
   __sJT_st.setInt(8,deploymentType);
   __sJT_st.setInt(9,deploymentType);
   __sJT_st.setInt(10,deploymentType);
   __sJT_st.setInt(11,equipType);
   __sJT_st.setInt(12,equipType);
   __sJT_st.setInt(13,equipName);
   __sJT_st.setInt(14,equipName);
   __sJT_st.setString(15,equipModel);
   __sJT_st.setString(16,equipModel);
   __sJT_st.setString(17,stringLookup);
   __sJT_st.setLong(18,longLookup);
   __sJT_st.setLong(19,longLookup);
   __sJT_st.setLong(20,longLookup);
   __sJT_st.setString(21,stringLookup2);
   __sJT_st.setString(22,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.inventory.EquipSearch",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1001^9*/
        
        lookupResults.clear();
      
        rs = it.getResultSet();
        
        while(rs.next())
        {
          // add all results to the lookupResults
          lookupResults.add(new EquipmentData( rs.getString("serial_number"),  
                                               rs.getString("client_name"), 
                                               rs.getString("equip_type"), 
                                               rs.getString("equip_name"), 
                                               rs.getString("equip_desc"), 
                                               rs.getString("condition"), 
                                               rs.getString("equip_status"), 
                                               rs.getString("unit_cost"),
                                               DateTimeFormatter.getFormattedDate(rs.getTimestamp("deploy_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT), 
                                               rs.getTimestamp("deploy_date"), 
                                               DateTimeFormatter.getFormattedDate(rs.getTimestamp("receive_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT), 
                                               rs.getTimestamp("receive_date"), 
                                               rs.getString("dba_name"), 
                                               rs.getString("merchant_number"), 
                                               rs.getString("association_number"),
                                               rs.getString("part_number"),
                                               rs.getString("deploy_type"), 
                                               rs.getString("deploy_price")));
                                               
          // add the merchant number (unique) into the HashSet
          foundEquip.add(rs.getString("serial_number"));
        }
      
        rs.close();
        it.close();
        
        // now search the merchant table
        /*@lineinfo:generated-code*//*@lineinfo:1037^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.EI_SERIAL_NUMBER             serial_number,
//                    ei.ei_received_date             receive_date,
//                    ei.ei_deployed_date             deploy_date,
//                    ei.ei_merchant_number           merchant_number,
//                    ei.ei_lrb_price                 deploy_price,
//                    ei.ei_unit_cost                 unit_cost,
//                    ei.ei_part_number               part_number,
//                    ec.ec_class_name                condition,
//                    mr.merch_business_name          dba_name,
//                    mr.asso_number                  association_number,
//                    elt.equiplendtype_description   deploy_type,
//                    es.equip_status_desc            equip_status,
//                    em.equipmfgr_mfr_name           equip_name,
//                    et.equiptype_description        equip_type,
//                    eq.equip_descriptor             equip_desc,
//                    ap.inventory_owner_name         client_name
//            from    equip_inventory                 ei,
//                    merchant                        mr,
//                    equip_status                    es,
//                    equipmfgr                       em,
//                    app_type                        ap,
//                    equipment                       eq,
//                    equiplendtype                   elt,
//                    equiptype                       et,
//                    equip_class                     ec
//            where   ei.ei_merchant_number = mr.merch_number(+) and
//                    ei.ei_status = es.equip_status_id and
//                    ei.ei_lrb = elt.equiplendtype_code and
//                    ei.ei_owner = ap.app_type_code and
//                    ei.ei_class = ec.ec_class_id and
//                    ei.ei_part_number = eq.equip_model and
//                    eq.equiptype_code = et.equiptype_code and 
//                    eq.EQUIPMFGR_MFR_CODE = em.EQUIPMFGR_MFR_CODE and
//                    (-1 = :client or ei.ei_owner = :client) and
//                    (-1 = :condition or ei.ei_class = :condition) and
//                    (-1 = :status or ei.ei_status = :status) and
//                    (99 != :deploymentType or ei.ei_lrb != 0) and
//                    (-1 = :deploymentType or 99 = :deploymentType or ei.ei_lrb = :deploymentType) and
//                    (-1 = :equipType or eq.equiptype_code = :equipType) and
//                    (-1 = :equipName or eq.EQUIPMFGR_MFR_CODE = :equipName) and
//                    ('-1' = :equipModel or eq.equip_model = :equipModel) and
//                    (
//                      'passall'                 = :stringLookup or
//                      ei.ei_merchant_number     = :longLookup or
//                      ei.EI_TRANSACTION_ID      = :longLookup or
//                      mr.asso_number            = :longLookup or
//                      upper(ei.EI_SERIAL_NUMBER) = :stringLookup2 or 
//                      upper(mr.merch_business_name) like :stringLookup
//                    )          
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.EI_SERIAL_NUMBER             serial_number,\n                  ei.ei_received_date             receive_date,\n                  ei.ei_deployed_date             deploy_date,\n                  ei.ei_merchant_number           merchant_number,\n                  ei.ei_lrb_price                 deploy_price,\n                  ei.ei_unit_cost                 unit_cost,\n                  ei.ei_part_number               part_number,\n                  ec.ec_class_name                condition,\n                  mr.merch_business_name          dba_name,\n                  mr.asso_number                  association_number,\n                  elt.equiplendtype_description   deploy_type,\n                  es.equip_status_desc            equip_status,\n                  em.equipmfgr_mfr_name           equip_name,\n                  et.equiptype_description        equip_type,\n                  eq.equip_descriptor             equip_desc,\n                  ap.inventory_owner_name         client_name\n          from    equip_inventory                 ei,\n                  merchant                        mr,\n                  equip_status                    es,\n                  equipmfgr                       em,\n                  app_type                        ap,\n                  equipment                       eq,\n                  equiplendtype                   elt,\n                  equiptype                       et,\n                  equip_class                     ec\n          where   ei.ei_merchant_number = mr.merch_number(+) and\n                  ei.ei_status = es.equip_status_id and\n                  ei.ei_lrb = elt.equiplendtype_code and\n                  ei.ei_owner = ap.app_type_code and\n                  ei.ei_class = ec.ec_class_id and\n                  ei.ei_part_number = eq.equip_model and\n                  eq.equiptype_code = et.equiptype_code and \n                  eq.EQUIPMFGR_MFR_CODE = em.EQUIPMFGR_MFR_CODE and\n                  (-1 =  :1  or ei.ei_owner =  :2 ) and\n                  (-1 =  :3  or ei.ei_class =  :4 ) and\n                  (-1 =  :5  or ei.ei_status =  :6 ) and\n                  (99 !=  :7  or ei.ei_lrb != 0) and\n                  (-1 =  :8  or 99 =  :9  or ei.ei_lrb =  :10 ) and\n                  (-1 =  :11  or eq.equiptype_code =  :12 ) and\n                  (-1 =  :13  or eq.EQUIPMFGR_MFR_CODE =  :14 ) and\n                  ('-1' =  :15  or eq.equip_model =  :16 ) and\n                  (\n                    'passall'                 =  :17  or\n                    ei.ei_merchant_number     =  :18  or\n                    ei.EI_TRANSACTION_ID      =  :19  or\n                    mr.asso_number            =  :20  or\n                    upper(ei.EI_SERIAL_NUMBER) =  :21  or \n                    upper(mr.merch_business_name) like  :22 \n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.inventory.EquipSearch",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,client);
   __sJT_st.setInt(2,client);
   __sJT_st.setInt(3,condition);
   __sJT_st.setInt(4,condition);
   __sJT_st.setInt(5,status);
   __sJT_st.setInt(6,status);
   __sJT_st.setInt(7,deploymentType);
   __sJT_st.setInt(8,deploymentType);
   __sJT_st.setInt(9,deploymentType);
   __sJT_st.setInt(10,deploymentType);
   __sJT_st.setInt(11,equipType);
   __sJT_st.setInt(12,equipType);
   __sJT_st.setInt(13,equipName);
   __sJT_st.setInt(14,equipName);
   __sJT_st.setString(15,equipModel);
   __sJT_st.setString(16,equipModel);
   __sJT_st.setString(17,stringLookup);
   __sJT_st.setLong(18,longLookup);
   __sJT_st.setLong(19,longLookup);
   __sJT_st.setLong(20,longLookup);
   __sJT_st.setString(21,stringLookup2);
   __sJT_st.setString(22,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.inventory.EquipSearch",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1088^9*/
        
        rs = it.getResultSet();
        while(rs.next())
        {
          // only add to the lookup Results if the merchant number doesn't already exist
          if(! foundEquip.contains(rs.getString("serial_number")))
          {
            // add all results to the lookupResults
            lookupResults.add(new EquipmentData( rs.getString("serial_number"),  
                                                 rs.getString("client_name"), 
                                                 rs.getString("equip_type"), 
                                                 rs.getString("equip_name"), 
                                                 rs.getString("equip_desc"), 
                                                 rs.getString("condition"), 
                                                 rs.getString("equip_status"), 
                                                 rs.getString("unit_cost"),
                                                 DateTimeFormatter.getFormattedDate(rs.getTimestamp("deploy_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT), 
                                                 rs.getTimestamp("deploy_date"), 
                                                 DateTimeFormatter.getFormattedDate(rs.getTimestamp("receive_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT), 
                                                 rs.getTimestamp("receive_date"), 
                                                 rs.getString("dba_name"), 
                                                 rs.getString("merchant_number"), 
                                                 rs.getString("association_number"),
                                                 rs.getString("part_number"),
                                                 rs.getString("deploy_type"), 
                                                 rs.getString("deploy_price")));
          }
        }
      
        rs.close();
        it.close();

        lastStartNew        = startNew; 
        lastClient          = client;
        lastCondition       = condition;
        lastStatus          = status;
        lastDeploymentType  = deploymentType;
        lastEquipName       = equipName;
        lastEquipType       = equipType;
        lastEquipModel      = equipModel;
        lastLookupValue     = lookupValue;
      }
      
      // now create the sorted tree
      if(sortedResults != null)
      {
        sortedResults.clear();
      }
      
      sortedResults = null;
      
      sortedResults = new TreeSet(edc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public synchronized Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }
  
  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public synchronized String getActionDescription()
  {
    return this.actionDescription;
  }
  
  /*
  ** ACCESSORS
  */
  public synchronized String getLookupValue()
  {
    return lookupValue;
  }
  public synchronized void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }
  
  public synchronized int getAction()
  {
    return this.action;
  }
  public synchronized void setAction(int action)
  {
    this.actionDescription = "Equipment Search";
  }
  public void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }
    
    setAction(actionNum);
  }
  
  public synchronized void setSortBy(String sortBy)
  {
    try
    {
      edc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }
 
  public synchronized void setEquipType(String equipType)
  {
    try
    {
      this.equipType = Integer.parseInt(equipType);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getEquipType()
  {
    return Integer.toString(this.equipType);
  }

  public synchronized void setEquipName(String equipName)
  {
    try
    {
      this.equipName = Integer.parseInt(equipName);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getEquipName()
  {
    return Integer.toString(this.equipName);
  }

  public synchronized void setEquipModel(String equipModel)
  {
    this.equipModel = equipModel;
  }
 
  public synchronized String getEquipModel()
  {
    return this.equipModel;
  }

  public synchronized void setClient(String client)
  {
    try
    {
      this.client = Integer.parseInt(client);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getClient()
  {
    return Integer.toString(this.client);
  }
 
  public synchronized void setCondition(String condition)
  {
    try
    {
      this.condition = Integer.parseInt(condition);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getCondition()
  {
    return Integer.toString(this.condition);
  }


  public synchronized void setStatus(String status)
  {
    try
    {
      this.status = Integer.parseInt(status);
    }
    catch(Exception e)
    {
    }
  }
 
  public synchronized String getStatus()
  {
    return Integer.toString(this.status);
  }


  public synchronized void setDeploymentType(String deploymentType)
  {
    try
    {
      this.deploymentType = Integer.parseInt(deploymentType);
    }
    catch(Exception e)
    {
    }
  }

  public synchronized String encode(String url)
  {
    String body = url;
    try
    {
      StringBuffer    bodyBuff = null;
      int             index = 0;
      
      index = body.indexOf(' ');
      
      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"%20");
        body = bodyBuff.toString();
        index = body.indexOf(' ', index);
      }
    }
    catch(Exception e)
    {
      System.out.println("error will robinson %20");
    }
    return body;
  }
 
  public synchronized String getDeploymentType()
  {
    return Integer.toString(this.deploymentType);
  }

  public synchronized void setSubmitted(String submitted)
  {
    this.submitted = true;
  }

  public synchronized void setStartNew(String startNew)
  {
    this.startNew = startNew;
  }
 
  public synchronized boolean isSubmitted()
  {
    return this.submitted;
  }

  public synchronized void setEdit(boolean edit)
  {
    this.edit = edit;
  }
  public synchronized boolean isEdit()
  {
    return this.edit;
  }
}/*@lineinfo:generated-code*/