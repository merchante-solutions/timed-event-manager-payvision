/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/RepairSearchBean.java $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/23/04 3:11p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Vector;

public class RepairSearchBean extends com.mes.screens.SequenceDataBean
{

  public static final int REPAIR_REFERENCE_NUM      = 1;
  public static final int REPAIR_MERCHANT_NUM       = 2;
  public static final int REPAIR_SERIAL_NUM         = 3;
  public static final int REPAIR_CALLTAG_NUM        = 4;
  public static final int REPAIR_PART_NUM           = 5;
  public static final int SEARCH_NOFILTER           = -1;

  public static final int SPECIFIC_DETAIL_EQUAL     = 1;
  public static final int SPECIFIC_DETAIL_GREAT     = 2;

  public static final int REPAIR_STATUS_ALL         = -1;
  public static final int REPAIR_STATUS_INCOMPLETE  = 0;
  public static final int REPAIR_STATUS_COMPLETE    = 1;

  public static final int REPAIR_COMPANY_ALL        = -1;
  public static final int REPAIR_COMPANY_POSPORTAL  = 11;
  public static final int REPAIR_COMPANY_VMS        = 16;
  public static final int REPAIR_COMPANY_OTHER      = 8;


  // private data members
  private String        repairNumber        = "-1";
  private String        merchantNumber      = "-1";
  private String        serialNum           = "";
  private String        callTagNum          = "";
  private String        partNum             = "";

  private int           repairStatus        = REPAIR_STATUS_ALL;
  private int           repairCompany       = REPAIR_COMPANY_ALL;

  private int           specificDateDetail  = SPECIFIC_DETAIL_EQUAL;
  
  private Date          specificDate        = null;
  private Date          specificDateNext    = null;
  private String        specificDateStr     = "-1";
  
  private int           searchCriteria      = -1;
  private StringBuffer  qs                  = new StringBuffer("");
  private boolean       submit              = false;
  private boolean       hasErrors           = false;
  private Vector        errors              = new Vector();
  private ResultSet     rs                  = null;

  public  Vector        productModel        = new Vector();
  public  Vector        productModelDesc    = new Vector();  


  /*
  ** CONSTRUCTOR
  */
  public RepairSearchBean()
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      ps = getPreparedStatement("select equip_model,equip_descriptor from equipment where equiptype_code not in (7,8,9) and used_in_inventory = 'Y' order by equip_descriptor");
      rs = ps.executeQuery();

      productModel.add("");
      productModelDesc.add("select one");  
      
      while(rs.next())
      {
        productModel.add(rs.getString("equip_model"));
        productModelDesc.add(rs.getString("equip_descriptor"));  
      } 
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      addError("Constructor crashed!!!!");
    }
  }

  public ResultSet getResultSet()
  {
    PreparedStatement ps      = null;
    
    try
    {
      qs.setLength(0);
      
      qs.append("select DISTINCT * from equip_repair ");


      switch(this.searchCriteria)
      {
        
        case REPAIR_REFERENCE_NUM:
        
          qs.append("where repair_ref_num = ? ");
        
          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.repairNumber);
          break;
        
        case REPAIR_MERCHANT_NUM:
        
          qs.append("where repair_ref_num in (select ref_num_serial_num from equip_merchant_tracking where merchant_number = ? and action = 3) ");

          switch(this.repairStatus)
          {
            case REPAIR_STATUS_INCOMPLETE:
              qs.append("and repair_status = " + Integer.toString(REPAIR_STATUS_INCOMPLETE) + " ");
            break;
            case REPAIR_STATUS_COMPLETE:
              qs.append("and repair_status = " + Integer.toString(REPAIR_STATUS_COMPLETE) + " ");
            break;
          }

          switch(this.repairCompany)
          {
            case REPAIR_COMPANY_POSPORTAL:
              qs.append("and repair_company = " + Integer.toString(REPAIR_COMPANY_POSPORTAL) + " ");
            break;
            case REPAIR_COMPANY_VMS:
              qs.append("and repair_company = " + Integer.toString(REPAIR_COMPANY_VMS) + " ");
            break;
            case REPAIR_COMPANY_OTHER:
              qs.append("and repair_company = " + Integer.toString(REPAIR_COMPANY_OTHER) + " ");
            break;
          }


          qs.append("order by repair_ref_num ");
        
          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.merchantNumber);
          break;
/*
        case SEARCH_ORDER_DATE:
          if(this.orderSpecificDateDetail == SPECIFIC_DETAIL_GREAT)
          {
            qs.append("and ep.ep_order_date >= ? "); 
          }
          else
          {
            qs.append("and ep.ep_order_date between ? and ? ");
          }

          switch(this.swapStatus)
          {
            case SWAP_STATUS_INCOMPLETE:
              qs.append("and ep.ep_transaction_date is not null ");
            break;
            case SWAP_STATUS_COMPLETE:
              qs.append("and ep.ep_transaction_date is null ");
            break;
          }

          qs.append("order by ep.ep_order_date desc ");
          
          ps = getPreparedStatement(qs.toString());
          
          ps.setDate(1, new java.sql.Date(this.orderDate.getTime()));
          
          if(this.orderSpecificDateDetail != SPECIFIC_DETAIL_GREAT)
          {
            ps.setDate(2, new java.sql.Date(this.orderDateNext.getTime()));
          }
          break;
*/
        case REPAIR_SERIAL_NUM:
          qs.append("where serial_num = ? ");

          switch(this.repairStatus)
          {
            case REPAIR_STATUS_INCOMPLETE:
              qs.append("and repair_status = " + Integer.toString(REPAIR_STATUS_INCOMPLETE) + " ");
            break;
            case REPAIR_STATUS_COMPLETE:
              qs.append("and repair_status = " + Integer.toString(REPAIR_STATUS_COMPLETE) + " ");
            break;
          }

          switch(this.repairCompany)
          {
            case REPAIR_COMPANY_POSPORTAL:
              qs.append("and repair_company = " + Integer.toString(REPAIR_COMPANY_POSPORTAL) + " ");
            break;
            case REPAIR_COMPANY_VMS:
              qs.append("and repair_company = " + Integer.toString(REPAIR_COMPANY_VMS) + " ");
            break;
            case REPAIR_COMPANY_OTHER:
              qs.append("and repair_company = " + Integer.toString(REPAIR_COMPANY_OTHER) + " ");
            break;
          }

          qs.append("order by repair_ref_num ");

          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.serialNum);
          break;  
        
        case REPAIR_CALLTAG_NUM:
        
          qs.append("where call_tag_num = ? ");

          switch(this.repairStatus)
          {
            case REPAIR_STATUS_INCOMPLETE:
              qs.append("and repair_status = " + Integer.toString(REPAIR_STATUS_INCOMPLETE) + " ");
            break;
            case REPAIR_STATUS_COMPLETE:
              qs.append("and repair_status = " + Integer.toString(REPAIR_STATUS_COMPLETE) + " ");
            break;
          }

          switch(this.repairCompany)
          {
            case REPAIR_COMPANY_POSPORTAL:
              qs.append("and repair_company = " + Integer.toString(REPAIR_COMPANY_POSPORTAL) + " ");
            break;
            case REPAIR_COMPANY_VMS:
              qs.append("and repair_company = " + Integer.toString(REPAIR_COMPANY_VMS) + " ");
            break;
            case REPAIR_COMPANY_OTHER:
              qs.append("and repair_company = " + Integer.toString(REPAIR_COMPANY_OTHER) + " ");
            break;
          }

          qs.append("order by repair_ref_num ");
        
          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.callTagNum);
          break;
        
        case REPAIR_PART_NUM:

          qs.setLength(0);
          qs.append("select DISTINCT er.* from equip_repair er, equip_inventory ei where er.serial_num = ei.ei_serial_number(+) and (er.part_num = ? or ei.EI_PART_NUMBER = ?) ");

          switch(this.repairStatus)
          {
            case REPAIR_STATUS_INCOMPLETE:
              qs.append("and er.repair_status = " + Integer.toString(REPAIR_STATUS_INCOMPLETE) + " ");
            break;
            case REPAIR_STATUS_COMPLETE:
              qs.append("and er.repair_status = " + Integer.toString(REPAIR_STATUS_COMPLETE) + " ");
            break;
          }

          switch(this.repairCompany)
          {
            case REPAIR_COMPANY_POSPORTAL:
              qs.append("and er.repair_company = " + Integer.toString(REPAIR_COMPANY_POSPORTAL) + " ");
            break;
            case REPAIR_COMPANY_VMS:
              qs.append("and er.repair_company = " + Integer.toString(REPAIR_COMPANY_VMS) + " ");
            break;
            case REPAIR_COMPANY_OTHER:
              qs.append("and er.repair_company = " + Integer.toString(REPAIR_COMPANY_OTHER) + " ");
            break;
          }

          qs.append("order by repair_ref_num ");
        
          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.partNum);
          ps.setString(2,this.partNum);
          break;

        default:
          boolean usedWhere = false;

          switch(this.repairStatus)
          {
            case REPAIR_STATUS_INCOMPLETE:
              qs.append("where repair_status = " + Integer.toString(REPAIR_STATUS_INCOMPLETE) + " ");
              usedWhere = true;
            break;
            case REPAIR_STATUS_COMPLETE:
              qs.append("where repair_status = " + Integer.toString(REPAIR_STATUS_COMPLETE) + " ");
              usedWhere = true;
            break;
          }

          switch(this.repairCompany)
          {
            case REPAIR_COMPANY_POSPORTAL:
              if(usedWhere)
              {
                qs.append("and repair_company = " + Integer.toString(REPAIR_COMPANY_POSPORTAL) + " ");
              }
              else
              {
                qs.append("where repair_company = " + Integer.toString(REPAIR_COMPANY_POSPORTAL) + " ");
              }
            break;
            case REPAIR_COMPANY_VMS:
              if(usedWhere)
              {
                qs.append("and repair_company = " + Integer.toString(REPAIR_COMPANY_VMS) + " ");
              }
              else
              {
                qs.append("where repair_company = " + Integer.toString(REPAIR_COMPANY_VMS) + " ");
              }
            break;
            case REPAIR_COMPANY_OTHER:
              if(usedWhere)
              {
                qs.append("and repair_company = " + Integer.toString(REPAIR_COMPANY_OTHER) + " ");
              }
              else
              {
                qs.append("where repair_company = " + Integer.toString(REPAIR_COMPANY_OTHER) + " ");
              }
            break;
          }

          qs.append("order by repair_ref_num desc ");
          ps = getPreparedStatement(qs.toString());
          break;
      }
      
      this.rs = ps.executeQuery();
      
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
      addError("getData: " + e.toString());
    }
    
    return this.rs;
  }  

  public boolean validateData()
  {
    boolean result = false;
    String temp = "";
    switch(this.searchCriteria)
    {
      case REPAIR_REFERENCE_NUM:
        if(this.repairNumber.equals("-1"))
        {
          temp = "Please enter a valid repair reference number";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case REPAIR_MERCHANT_NUM:
        if(this.merchantNumber.equals("-1"))
        {
          temp = "Please enter a valid merchant number";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
/*
      case SEARCH_ORDER_DATE:
        if(this.orderDateStr.equals("-1") || !validDate(this.orderDateStr))
        {
          temp = "Please enter a valid order date in the following format:  mm/dd/yy";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        else
        {
          this.orderDateNext = getDateNext(this.orderDateStr);
        }
      break;
*/
      case REPAIR_SERIAL_NUM:
        if(isBlank(this.serialNum))
        {
          temp = "Please provide a Serial Number to see which repairs it was involved in";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case REPAIR_CALLTAG_NUM:
        if(isBlank(this.callTagNum))
        {
          temp = "Please provide a Call Tag Number to see which repair it was involved in";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case REPAIR_PART_NUM:
        if(isBlank(this.partNum))
        {
          temp = "Please select a Product to see which repairs it's type was involved in";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      default:
        this.hasErrors = false;
        break;
    }
    
    if(!this.hasErrors)
    {
      result = true;
    }
    
    return result;
  }
  private boolean validDate(String date)
  {
    int index1  = date.indexOf('/');
    int index2  = date.lastIndexOf('/');
    String mon  = date.substring(0,index1);
    String day  = date.substring(index1+1,index2);
    String year = date.substring(index2+1);
    boolean result = true;
    try
    {
      int dayInt = Integer.parseInt(day);
      int monInt = Integer.parseInt(mon);
      int yearInt = Integer.parseInt(year);
      if(dayInt > 31 || dayInt < 1)
        result = false;
      if(monInt > 12 || monInt < 1)
        result = false;
      if(yearInt < 0)
        result = false;
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }

  private Date getDateNext(String specificDate)
  {
    int index1  = specificDate.indexOf('/');
    int index2  = specificDate.lastIndexOf('/');
    String mon  = specificDate.substring(0,index1);
    String day  = specificDate.substring(index1+1,index2);
    String year = specificDate.substring(index2+1);
    Date result = null;
        
    try
    {
      int nextDay = Integer.parseInt(day);
      nextDay++;
      day             = String.valueOf(nextDay);
      String date     = mon + "/" + day + "/" + year;
      DateFormat fmt  = DateFormat.getDateInstance(DateFormat.SHORT);
      result          = fmt.parse(date);
    }
    catch(Exception e)
    {
      result = null;
    }
    return result;
  }

  public void setSpecificDate(String specificDate)
  {
    this.specificDateStr = specificDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(specificDate.trim());
      try
      {
        specificDate          = specificDate.replace('/','0');
        long tempLong         = Long.parseLong(specificDate.trim());
        this.specificDate     = aDate;
      }
      catch(Exception e)
      {
        this.specificDate    = null;
        this.specificDateStr = "-1";
      }
    }
    catch(ParseException e)
    {
      this.specificDate      = null;
      this.specificDateStr   = "-1";
    }
  }
 
  public String getSpecificDate()
  {
    String result = "";
    if(!this.specificDateStr.equals("-1"))
    {
      result = this.specificDateStr;
    }
    return result;
  }
 
  public void setSpecificDateDetail(String specificDateDetail)
  {
    try
    {
      this.specificDateDetail = Integer.parseInt(specificDateDetail); 
    }
    catch(Exception e)
    {
      this.specificDateDetail = -1;
    }
  }
  public int getSpecificDateDetail()
  {
    int result = 0;
    if(this.specificDateDetail != -1)
    {
      result = this.specificDateDetail;
    }
    return result;
  }


  public void setSearchCriteria(String searchCriteria)
  {
    try
    {
      this.searchCriteria = Integer.parseInt(searchCriteria); 
    }
    catch(Exception e)
    {
      this.searchCriteria = -1;
    }
  }
  public int getSearchCriteria()
  {
    return this.searchCriteria;
  }

  public void setRepairStatus(String repairStatus)
  {
    try
    {
      this.repairStatus = Integer.parseInt(repairStatus); 
    }
    catch(Exception e)
    {
      this.repairStatus = -1;
    }
  }
  public int getRepairStatus()
  {
    return this.repairStatus;
  }

  public void setRepairCompany(String repairCompany)
  {
    try
    {
      this.repairCompany = Integer.parseInt(repairCompany); 
    }
    catch(Exception e)
    {
      this.repairCompany = -1;
    }
  }
  public int getRepairCompany()
  {
    return this.repairCompany;
  }


  public void setMerchantNumber(String merchantNumber)
  {
    try
    {
      long temp = Long.parseLong(merchantNumber);
      this.merchantNumber = merchantNumber;
    }
    catch(Exception e)
    {
      this.merchantNumber = "-1";
    }
  }
  public String getMerchantNumber()
  {
    String result = "";
    if(!this.merchantNumber.equals("-1"))
    {
      result = this.merchantNumber;
    }
    return result;
  }

  public void setRepairNum(String repairNumber)
  {
    try
    {
      long temp = Long.parseLong(repairNumber);
      this.repairNumber = repairNumber;
    }
    catch(Exception e)
    {
      this.repairNumber = "-1";
    }
  }
  public String getRepairNum()
  {
    String result = "";
    if(!this.repairNumber.equals("-1"))
    {
      result = this.repairNumber;
    }
    return result;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum.trim();
  }
  public String getSerialNum()
  {
    return this.serialNum;
  }

  public void setCallTagNum(String callTagNum)
  {
    this.callTagNum = callTagNum.trim();
  }
  public String getCallTagNum()
  {
    return this.callTagNum;
  }

  public void setPartNum(String partNum)
  {
    this.partNum = partNum.trim();
  }
  public String getPartNum()
  {
    return this.partNum;
  }

  public void setSubmit(String submit)
  {
    this.submit = true;
  }
  public boolean isSubmitted()
  {
    return this.submit;
  }

  public boolean getHasErrors()
  {
    return this.hasErrors;
  }

  public Vector getErrors()
  {
    return errors;
  }
}
