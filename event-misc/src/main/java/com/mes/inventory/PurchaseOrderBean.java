/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/PurchaseOrderBean.java $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/23/04 4:42p $
  Version            : $Revision: 16 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;

public class PurchaseOrderBean extends com.mes.screens.SequenceDataBean
{

  private String   referenceNum                        = "";
  private int      numRecs                             = 0;
  private int      supplierId                          = -1;
  private int      owner                               = -1;
  private boolean  addToList                           = false;
  private String   poNotes                             = "";
  
  public  Vector   terminals                           = new Vector();
  public  Vector   termPrinters                        = new Vector();                           
  public  Vector   termPrintPins                       = new Vector();
  public  Vector   printers                            = new Vector();
  public  Vector   pinpads                             = new Vector();
  public  Vector   imprinters                          = new Vector();
  public  Vector   peripherals                         = new Vector();
  public  Vector   accessories                         = new Vector();

  public  Vector   terminalsModel                      = new Vector();
  public  Vector   termPrintersModel                   = new Vector();                           
  public  Vector   termPrintPinsModel                  = new Vector();
  public  Vector   printersModel                       = new Vector();
  public  Vector   pinpadsModel                        = new Vector();
  public  Vector   imprintersModel                     = new Vector();
  public  Vector   peripheralsModel                    = new Vector();
  public  Vector   accessoriesModel                    = new Vector();

  private Vector   selectedEquip                       = new Vector();
  private Vector   equipRecs                           = new Vector();

  public  Vector   classes                             = new Vector();
  public  Vector   classesCode                         = new Vector();

  public  Vector   supplier                            = new Vector();
  public  Vector   supplierCode                        = new Vector();

  public  Vector   ownerDesc                           = new Vector();
  public  Vector   ownerCode                           = new Vector();

  private HashMap  validateMap                         = new HashMap();
  
  public PurchaseOrderBean()
  {
    try
    {
      StringBuffer      qs      = new StringBuffer("");
      PreparedStatement ps      = null;
      ResultSet         rs      = null;
      
      qs.append("select equip_model,equip_descriptor,equiptype_code ");
      qs.append("from equipment where used_in_inventory = 'Y'       ");
      qs.append("order by equiptype_code,equip_descriptor           ");
      
      ps = getPreparedStatement(qs.toString());
      
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        switch(rs.getInt("equiptype_code"))
        {
          case mesConstants.APP_EQUIP_TYPE_TERMINAL:
            terminals.add(rs.getString("equip_descriptor"));
            terminalsModel.add(rs.getString("equip_model"));
          break;
          case mesConstants.APP_EQUIP_TYPE_PRINTER:
            printers.add(rs.getString("equip_descriptor"));
            printersModel.add(rs.getString("equip_model"));
          break;
          case mesConstants.APP_EQUIP_TYPE_PINPAD:
            pinpads.add(rs.getString("equip_descriptor"));
            pinpadsModel.add(rs.getString("equip_model"));
          break;
          case mesConstants.APP_EQUIP_TYPE_IMPRINTER:
            imprinters.add(rs.getString("equip_descriptor"));
            imprintersModel.add(rs.getString("equip_model"));
          break;
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINTER:
            termPrinters.add(rs.getString("equip_descriptor"));
            termPrintersModel.add(rs.getString("equip_model"));
          break;
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD:
            termPrintPins.add(rs.getString("equip_descriptor"));
            termPrintPinsModel.add(rs.getString("equip_model"));
          break;
          case mesConstants.APP_EQUIP_TYPE_PERIPHERAL:
            peripherals.add(rs.getString("equip_descriptor"));
            peripheralsModel.add(rs.getString("equip_model"));
          break;
          case mesConstants.APP_EQUIP_TYPE_ACCESSORIES:
            accessories.add(rs.getString("equip_descriptor"));
            accessoriesModel.add(rs.getString("equip_model"));
          break;
        }  
      }
      
      rs.close();
      ps.close();
    
      ps = getPreparedStatement("select * from equip_class");
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        classes.add(rs.getString("ec_class_name"));
        classesCode.add(rs.getString("ec_class_id"));  
      }
    
      rs.close();
      ps.close();
    
      ps = getPreparedStatement("select * from equip_supplier");
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        supplier.add(rs.getString("es_supplier_name"));
        supplierCode.add(rs.getString("es_supplier_id"));  
      }

      rs.close();
      ps.close();
    
      ps = getPreparedStatement("select app_type_code,inventory_owner_name from app_type where SEPARATE_INVENTORY = 'Y' ");
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        ownerDesc.add(rs.getString("inventory_owner_name"));  
        ownerCode.add(rs.getString("app_type_code"));
      }

      rs.close();
      ps.close();

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "constructor--filling grids: " + e.toString());
    }


  }

  public void createReferenceNum()
  {
    PreparedStatement   ps          = null;
    ResultSet           rs          = null;
    long                controlSeq  = 10000000000L;
    long                primaryKey  = 0L;
    
    if(!isBlank(this.referenceNum))
    {
      return;
    }
    try
    {

      // get a new sequence number
      ps = getPreparedStatement("select purchase_order_sequence.nextval from dual");
      rs = ps.executeQuery();

      if(rs.next())
      {
        primaryKey = rs.getLong("nextval");
      }

      rs.close();
      ps.close();

      // get a new control number
      Calendar      cal         = new GregorianCalendar();
      String        YYYY        = String.valueOf(cal.get(Calendar.YEAR));
      String        YY          = YYYY.substring(2);
      int           day         = cal.get(Calendar.DAY_OF_YEAR);

      // add the year
      controlSeq += ((cal.get(Calendar.YEAR) - 2000) * 100000000L);

      // add the julian day
      controlSeq += (cal.get(Calendar.DAY_OF_YEAR) * 100000L);

      // add the primarykey
      controlSeq += primaryKey;
      
      this.referenceNum = Long.toString(controlSeq);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "createReferenceNum: " + e.toString());
      this.referenceNum = Long.toString(primaryKey);
    }
  }
  
  
  
  
  public void updateData(HttpServletRequest aReq)
  {
    int     i           = 0;
    boolean addToVector = false;

    while(i < this.numRecs)
    {
      String tempClass = null;
      EquipRec tempRec = new EquipRec();

      if(isBlank(aReq.getParameter("delete" + i)))
      {    
        addToVector = true;
      }
      else
      {
        addToVector = false;
      }
      if(addToVector & isSubmitted())
      {
        if(!isBlank(aReq.getParameter("partNum" + i)) && !isBlank(aReq.getParameter("classType" + i)))
        {
          tempClass = (String)validateMap.get(aReq.getParameter("partNum" + i));
          if(isBlank(tempClass))
          {
            validateMap.put(aReq.getParameter("partNum" + i),aReq.getParameter("classType" + i));
          }
          else if(tempClass.equals(aReq.getParameter("classType" + i)))
          {
            addError("You can not have two items with the same product description and the same product condition.  Please specify quantity.");
          }
        }
      }
      
      if(!isBlank(aReq.getParameter("partNum" + i)))
      {    
        tempRec.setPartNum(aReq.getParameter("partNum" + i));
      }
      if(!isBlank(aReq.getParameter("productName" + i)))
      {    
        tempRec.setProductName(aReq.getParameter("productName" + i));
      }
      if(!isBlank(aReq.getParameter("productDesc" + i)))
      {    
        tempRec.setProductDesc(aReq.getParameter("productDesc" + i));
      }
      if(!isBlank(aReq.getParameter("productType" + i)))
      {    
        tempRec.setProductType(aReq.getParameter("productType" + i));
      }

      if(!isBlank(aReq.getParameter("classType" + i)))
      {    
        tempRec.setClassType(aReq.getParameter("classType" + i));
      }
      if(!isBlank(aReq.getParameter("quantity" + i)))
      {    
        tempRec.setQuantity(aReq.getParameter("quantity" + i));
      }
      if(!isBlank(aReq.getParameter("unitPrice" + i)))
      {    
        tempRec.setUnitPrice(aReq.getParameter("unitPrice" + i));
      }
      i++;
      if(addToVector)
      {
        equipRecs.add(tempRec);
      }
    }
  }

  public boolean validate()
  {
    int     i           = 0;

    if(this.supplierId == -1)
    {
      addError("Please select a supplier from the list of suppliers");
    }
    if(this.owner == -1)
    {
      addError("Please select the separate inventory this order belongs too");
    }
    if(this.poNotes.length() > 250)
    {
      addError("Notes must be limited to 250 characters.  Currently there are " + this.poNotes.length() + " characters");
    }
    while(i < this.numRecs)
    {
      EquipRec tempRec =  (EquipRec)equipRecs.elementAt(i);

      
      if(tempRec.getClassType() == -1)
      {    
        addError("Please select the condition of the " + tempRec.getProductDesc() + " you wish to order");
      }
      if(tempRec.getQuantity() == -1)
      {    
        addError("Please select the quantity of " + tempRec.getProductDesc() + " you wish to order");
      }
      if(isBlank(tempRec.getUnitPrice()))
      {    
        addError("Please provide the unit price of each " + tempRec.getProductDesc() + " you wish to order");
      }
      else
      {
        if(validCurr(tempRec.getUnitPrice()))
        {
          tempRec.setUnitPrice(getDigitsAndDecimals(formatCurr(getCurr(tempRec.getUnitPrice()))));
          try
          {
            double tempDoub = Double.parseDouble(tempRec.getUnitPrice());
            if(tempDoub > 9999.99)
            {
              addError("Unit price for each " + tempRec.getProductDesc() + " must be less than $10,000.00");
            }
          }
          catch(Exception e)
          {}
        }
        else
        {
          addError("Please provide a valid unit price for each " + tempRec.getProductDesc() + " you wish to order");
        }
      }

      i++;
    }
    
    return(!hasErrors());
  }

  public void getEquipInfo()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select equip.equip_descriptor, type.equiptype_description, mfgr.equipmfgr_mfr_name ");
      qs.append("from equipment equip, equiptype type, equipmfgr mfgr ");
      qs.append("where equip.equip_model = ? and ");
      qs.append("equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code and ");
      qs.append("equip.equiptype_code = type.equiptype_code ");
      ps = getPreparedStatement(qs.toString());
      
      for(int i=0; i<selectedEquip.size(); i++)
      {
        ps.setString(1,(String)selectedEquip.elementAt(i));
        rs = ps.executeQuery();
      
        EquipRec tempRec = new EquipRec();

        if(rs.next())
        {
          tempRec.setPartNum((String)selectedEquip.elementAt(i));
          tempRec.setProductName(rs.getString("equipmfgr_mfr_name"));
          tempRec.setProductDesc(rs.getString("equip_descriptor"));
          tempRec.setProductType(rs.getString("equiptype_description"));
        }

        equipRecs.add(tempRec);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getEquipmentInfo: " + e.toString());
      addError("getEquipmentInfo: " + e.toString());
    }
  }
  

  public void submitData()
  {
    submitPurchaseOrder();
  }

  public void getOrderInfo(String refNum)
  {
    StringBuffer      qs        = new StringBuffer("");
    PreparedStatement ps        = null;
    ResultSet         rs        = null;
    EquipRec          tempRec   = null;
    
    try
    {

      qs.setLength(0);
      qs.append("select * from equip_purchase_order where ep_transaction_id = ?");
      
      ps = getPreparedStatement(qs.toString());
      ps.setString(1,refNum);
      rs = ps.executeQuery();
  
      if(rs.next())
      {
        this.poNotes      = isBlank(rs.getString("ep_description")) ? "" : rs.getString("ep_description");
        this.supplierId   = rs.getInt("ep_supplier_id");      
        this.owner        = rs.getInt("ep_owner");
        this.referenceNum = rs.getString("ep_transaction_id");
      }
      else
      {
        addError("<b><font size=5>**************Purchase Order Not Found**************</font></b>");
        return;
      }
  
      ps.close();


      qs.setLength(0);
      qs.append("select et.*, equip.equip_descriptor, type.equiptype_description, mfgr.equipmfgr_mfr_name ");
      qs.append("from equip_transaction et, equipment equip, equiptype type, equipmfgr mfgr ");
      qs.append("where et.et_transaction_id = ? and et.et_part_number = equip.equip_model and ");
      qs.append("equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code and ");
      qs.append("equip.equiptype_code = type.equiptype_code order by et.et_part_number,et.et_class ");
      
      ps = getPreparedStatement(qs.toString());
      
      ps.setString(1, refNum);

      rs = ps.executeQuery();

      String prevPartNum      = "";
      String prevClass        = "";      
      String prevUnitPrice    = "";
      String prevMfrName      = "";
      String prevEquipDesc    = "";
      String prevEquipType    = "";
      int    count            = 0;

      while(rs.next())
      {
        String tempPartNum    = isBlank(rs.getString("et_part_number")) ? "" : rs.getString("et_part_number");
        String tempClass      = isBlank(rs.getString("et_class"))       ? "" : rs.getString("et_class");

        if(!tempPartNum.equals(prevPartNum) || !tempClass.equals(prevClass))
        {
          if(!isBlank(prevPartNum))
          {
            tempRec =  new EquipRec();
            tempRec.setPartNum(     prevPartNum             );
            tempRec.setClassType(   prevClass               );      
            tempRec.setUnitPrice(   prevUnitPrice           );
            tempRec.setQuantity(    Integer.toString(count) );
            tempRec.setProductName( prevMfrName             );
            tempRec.setProductDesc( prevEquipDesc           );
            tempRec.setProductType( prevEquipType           );
            equipRecs.add(tempRec);
          }
          
          //reset count
          count = 1;

          //put in quanity we counted along with other stuff... into tempRec.. then we put new into prev
          prevPartNum      =     rs.getString("et_part_number");       
          prevClass        =     rs.getString("et_class");
          prevUnitPrice    =     rs.getString("et_unit_price");
          prevMfrName      =     rs.getString("equipmfgr_mfr_name");
          prevEquipDesc    =     rs.getString("equip_descriptor");
          prevEquipType    =     rs.getString("equiptype_description");
        }
        else
        {
          ++count;
        }
      }  
 
      //add last one to vector..
      if(!isBlank(prevPartNum))
      {
        tempRec =  new EquipRec();
        tempRec.setPartNum(     prevPartNum             );
        tempRec.setClassType(   prevClass               );      
        tempRec.setUnitPrice(   prevUnitPrice           );
        tempRec.setQuantity(    Integer.toString(count) );
        tempRec.setProductName( prevMfrName             );
        tempRec.setProductDesc( prevEquipDesc           );
        tempRec.setProductType( prevEquipType           );
        equipRecs.add(tempRec);
      }
 
      
      ps.close();  
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getOrderInfo: " + e.toString());
      addError("getOrderInfo: " + e.toString());
    }
  }

  private void submitPurchaseOrder()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;
    
    try
    {
      ps = getPreparedStatement("select * from equip_purchase_order where ep_transaction_id = ?");
      ps.setString(1,this.referenceNum);
      rs = ps.executeQuery();

      if(rs.next())
      {
        qs.setLength(0);
        qs.append("update equip_purchase_order ");
        qs.append("set ep_change_date = sysdate, ");
        qs.append("ep_description = ?, ");
        qs.append("ep_supplier_id = ?, ");
        qs.append("ep_owner = ? ");
        qs.append("where ep_transaction_id = ?");
      }
      else
      {
        qs.setLength(0);
        qs.append("insert into equip_purchase_order (");
        qs.append("ep_order_date,  ");
        qs.append("ep_description, ");
        qs.append("ep_supplier_id, ");
        qs.append("ep_owner, ");
        qs.append("ep_transaction_id");
        qs.append(") values(sysdate,?,?,?,?)");    
      }

      ps = getPreparedStatement(qs.toString());

      ps.setString( 1,this.poNotes);
      ps.setInt(    2,this.supplierId);    
      ps.setInt(    3,this.owner);
      ps.setString( 4,this.referenceNum);  

      ps.executeUpdate();
      ps.close();
      rs.close();
  
      ps = getPreparedStatement("delete from equip_transaction where et_transaction_id = ? and et_serial_number is null");
      ps.setString(1,this.referenceNum);
      ps.executeUpdate();
      ps.close();
  
      qs.setLength(0);
      qs.append("insert into equip_transaction (");
      qs.append("et_transaction_id,");
      qs.append("et_part_number,");
      qs.append("et_class,");
      qs.append("et_unit_price, ");
      qs.append("et_place_holder ");
      qs.append(") values(?,?,?,?,?)");
      
      ps = getPreparedStatement(qs.toString());

      while(i < this.numRecs)
      {
        EquipRec tempRec =  (EquipRec)equipRecs.elementAt(i);
        ps.setString( 1,this.referenceNum);
        ps.setString( 2,tempRec.getPartNum());
        ps.setInt(    3,tempRec.getClassType());      
        ps.setString( 4,tempRec.getUnitPrice());
                
        for(int y=0; y<tempRec.getQuantity(); y++)
        {
          ps.setInt(5,(y + 1));
          ps.executeUpdate();
        }
        
        i++;
      }  
      ps.close();  

    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitPurchaseOrder: " + e.toString());
      addError("submitPurchaseOrder: " + e.toString());
    }
  }
  

  private String getDigitsAndDecimals(String raw)
  {
    StringBuffer  digits      = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == '.')
        {
          digits.append(raw.charAt(i));
        }
      }
      
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDigits: " + e.toString());
    }
    
    return digits.toString();

  }

  public String formatCurr(String curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }

  public String getCurr(String num)
  {
    String result = "";
    try
    {
      if(validCurr(num))
      {
        if(!num.startsWith("$"))
        {
          num = "$" + num;
        }
        NumberFormat  nf       = NumberFormat.getCurrencyInstance(Locale.US);
        double        temp     = (nf.parse(num)).doubleValue();
                      result   = Double.toString(temp);   
      }
    }
    catch(Exception e)
    {}
    return result;
  }

  public boolean validCurr(String raw)
  {
    boolean result  = true;
    int     sign    = 0;
    int     decimal = 0;

    try
    {
      if(!raw.startsWith("$"))
      {
        raw = "$" + raw;
      }
      
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == ',') //let digits and commas slide
        {         
        }
        else if(raw.charAt(i) == '$')
        {
          sign++;
        }
        else if(raw.charAt(i) == '.')
        {
          decimal++;
        }
        else
        {
          result = false;
        }
      }
      
      if(sign > 1 || decimal > 1)
      {
        result = false;
      }
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }
  
  public int getListSize()
  {
    return equipRecs.size();
  }
  public void setPoNotes(String poNotes)
  {
    this.poNotes = poNotes;
  }
  public String getPoNotes()
  {
    return this.poNotes;
  }
  public void setNumRecs(String numRecs)
  {
    try
    {
      this.numRecs = Integer.parseInt(numRecs);
    }
    catch(Exception e)
    {
      this.numRecs = 0;
    }
  }

  public boolean addingToList()
  {
    return addToList;
  }

  public void setAddToList(String temp)
  {
    this.addToList = true;
  }

  public void setReferenceNum(String referenceNum)
  {
    this.referenceNum = referenceNum;
  }

  public String getReferenceNum()
  {
    return this.referenceNum;
  }

  public int getSupplierId()
  {
    return this.supplierId;
  }

  public void setSupplierId(String supplierId)
  {
    try
    {
      this.supplierId = Integer.parseInt(supplierId);
    }
    catch(Exception e)
    {
      this.supplierId = -1;
    }
  }

  public int getOwner()
  {
    return this.owner;
  }

  public void setOwner(String owner)
  {
    try
    {
      this.owner = Integer.parseInt(owner);
    }
    catch(Exception e)
    {
      this.owner = -1;
    }
  }

  public void setTerminals(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }
  public void setTermPrinters(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }
  public void setTermPrintPins(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }
  public void setPrinters(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }
  public void setPinpads(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }
  public void setImprinters(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }
  public void setPeripherals(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }
  public void setAccessories(String equip)
  {
    if(!isBlank(equip))
    {
      selectedEquip.add(equip);   
    }
  }


  public int getClassType(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getClassType());
  }
  public int getQuantity(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getQuantity());
  }

  public String getUnitPrice(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getUnitPrice());
  }
  public String getPartNum(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getPartNum());
  }
  public String getProductName(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getProductName());
  }
  public String getProductType(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getProductType());
  }
  public String getProductDesc(int idx)
  {
    EquipRec equip = (EquipRec)equipRecs.elementAt(idx);
    return(equip.getProductDesc());
  }



  public class EquipRec
  {
    private int    classType      = -1;
    private int    quantity       = -1;

    private String unitPrice      = "";
    private String partNum        = "";
    private String productName    = "";
    private String productType    = "";
    private String productDesc    = "";

    EquipRec()
    {}

    public void setClassType(String classType)
    {
      try
      {
        if(classType != null && !classType.equals(""))
        {
          this.classType = Integer.parseInt(classType);
        }
      }
      catch(Exception e)
      {}
    }
    public void setQuantity(String quantity)
    {
      try
      {
        if(quantity != null && !quantity.equals(""))
        {
          this.quantity = Integer.parseInt(quantity);
        }
      }
      catch(Exception e)
      {}
    }

    public void setUnitPrice(String unitPrice)
    {
      this.unitPrice = unitPrice;
    }
    public void setPartNum(String partNum)
    {
      this.partNum = partNum;
    }
    public void setProductName(String productName)
    {
      this.productName = productName;
    }
    public void setProductType(String productType)
    {
      this.productType = productType;
    }
    public void setProductDesc(String productDesc)
    {
      this.productDesc = productDesc;
    }

    public int getClassType()
    {
      return this.classType;
    }
    public int getQuantity()
    {
      return this.quantity;
    }
    public String getUnitPrice()
    {
      return this.unitPrice;
    }
    public String getPartNum()
    {
      return this.partNum;
    }
    public String getProductName()
    {
      return this.productName;
    }
    public String getProductType()
    {
      return this.productType;
    }
    public String getProductDesc()
    {
      return this.productDesc;
    }
  }

}