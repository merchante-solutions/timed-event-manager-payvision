/*@lineinfo:filename=RemoveEquipmentBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/RemoveEquipmentBean.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 11/18/02 11:21a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.StringTokenizer;
import com.mes.ops.InventoryBase;
import sqlj.runtime.ResultSetIterator;

public class RemoveEquipmentBean extends InventoryBase
{
  public static final String  REMOVE_CATEGORY_LOST        = "Lost";
  public static final String  REMOVE_CATEGORY_OBSOLETE    = "Obsolete";
  public static final String  REMOVE_CATEGORY_DAMAGED     = "Damaged";
  public static final String  REMOVE_CATEGORY_CORRECTION  = "Inventory Correction";

  private String  LastDescription                 = null;
  private String  LastInventory                   = null;
  private String  LastSerialNumber                = null;
  private String  LastRemoveCategory              = null;

  private int     Owner                           = -1;
  
  private String  PartNumber                      = null;
  private String  SerialNumber                    = null;
  private String  RemoveCategory                  = null;

  public RemoveEquipmentBean()
  {
  }

  public String getLastDescription()
  {
    return( LastDescription );
  }
  
  public String getLastInventory()
  {
    return( LastInventory );
  }

  public String getLastSerialNumber()
  {
    return( LastSerialNumber );
  }
  
  public String getLastRemoveCategory()
  {
    return( LastRemoveCategory );
  }


  public void setOwner( String ownerStr )
  {
    try
    {
      if ( ! isBlank( ownerStr ) )
      {
        Owner = Integer.parseInt( ownerStr );
      }        
    }
    catch( NumberFormatException e )
    {
      Owner = -1;
    }
  }
  
  public void setRemoveCategory( String removeCat )
  {
    this.RemoveCategory = removeCat;
  }


  public void setSerialNumInList( String serialNumberStr )
  {
    if ( !isBlank(serialNumberStr) )
    {
      if ( isBlank( SerialNumber ) )
      {      
        StringTokenizer       tokens = new StringTokenizer(serialNumberStr,";");
    
        try
        {
          PartNumber    = tokens.nextToken();
          SerialNumber  = tokens.nextToken();
        }
        catch( Exception e )
        {
          logEntry("setSerialNumInList()",e.toString());
        }
      }
      else
      {
        addError("Either select an In Item from the list OR provide a serial number and product description.");
      }      
    }
  }
  
  public void setSerialNumInMan( String serialNumber )
  {
    if ( !isBlank( serialNumber ) )
    {
      if ( isBlank( SerialNumber ) )
      {
        SerialNumber = serialNumber;
      }
      else
      {
        addError("Either select an In Item from the list OR provide a serial number and product description.");
      }
    }      
  }

  public void submit(long userId)
  {
    ResultSetIterator   it            = null;
    ResultSet           rs            = null;

    String              action        = "";
    int                 statusCode    = 0;

    String      ei_part_number        = ""; 
    String      ei_serial_number      = "";    
    String      ei_unit_cost          = "";   
    String      ei_class              = "";   
    String      ei_status             = "";   
    String      ei_lrb                = "";   
    Timestamp   ei_received_date      = null;   
    Timestamp   ei_deployed_date      = null;   
    String      ei_invoice_number     = "";  
    String      ei_transaction_id     = ""; 
    String      ei_merchant_number    = ""; 
    String      ei_original_class     = ""; 
    String      ei_shipping_cost      = ""; 
    String      ei_lrb_price          = ""; 
    String      ei_owner              = ""; 
    String      ei_date_removed       = ""; 
    String      ei_remove_status      = ""; 

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:177^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  * 
//          from    equip_inventory
//          where   ei_part_number   = :PartNumber and
//                  ei_serial_number = :SerialNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  * \n        from    equip_inventory\n        where   ei_part_number   =  :1  and\n                ei_serial_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.RemoveEquipmentBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,PartNumber);
   __sJT_st.setString(2,SerialNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.RemoveEquipmentBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:183^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        ei_part_number     =   isBlank(rs.getString("EI_PART_NUMBER"))      ? "" : rs.getString("EI_PART_NUMBER"); 
        ei_serial_number   =   isBlank(rs.getString("EI_SERIAL_NUMBER"))    ? "" : rs.getString("EI_SERIAL_NUMBER"); 
        ei_unit_cost       =   isBlank(rs.getString("EI_UNIT_COST"))        ? "" : rs.getString("EI_UNIT_COST"); 
        ei_class           =   isBlank(rs.getString("EI_CLASS"))            ? "" : rs.getString("EI_CLASS");
        ei_status          =   isBlank(rs.getString("EI_STATUS"))           ? "" : rs.getString("EI_STATUS");
        ei_lrb             =   isBlank(rs.getString("EI_LRB"))              ? "" : rs.getString("EI_LRB");

        ei_received_date   =   rs.getTimestamp("EI_RECEIVED_DATE");
        ei_deployed_date   =   rs.getTimestamp("EI_DEPLOYED_DATE");

        ei_invoice_number  =   isBlank(rs.getString("EI_INVOICE_NUMBER"))   ? "" : rs.getString("EI_INVOICE_NUMBER");
        ei_transaction_id  =   isBlank(rs.getString("EI_TRANSACTION_ID"))   ? "" : rs.getString("EI_TRANSACTION_ID");
        ei_merchant_number =   isBlank(rs.getString("EI_MERCHANT_NUMBER"))  ? "" : rs.getString("EI_MERCHANT_NUMBER");
        ei_original_class  =   isBlank(rs.getString("EI_ORIGINAL_CLASS"))   ? "" : rs.getString("EI_ORIGINAL_CLASS");
        ei_shipping_cost   =   isBlank(rs.getString("EI_SHIPPING_COST"))    ? "" : rs.getString("EI_SHIPPING_COST");
        ei_lrb_price       =   isBlank(rs.getString("EI_LRB_PRICE"))        ? "" : rs.getString("EI_LRB_PRICE");
        ei_owner           =   isBlank(rs.getString("EI_OWNER"))            ? "" : rs.getString("EI_OWNER");
      }

      rs.close();
      it.close();
      


      
      /*@lineinfo:generated-code*//*@lineinfo:214^7*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_inventory_removed 
//          (  
//            EIR_PART_NUMBER,
//            EIR_SERIAL_NUMBER,
//            EIR_UNIT_COST,
//            EIR_CLASS,
//            EIR_STATUS,
//            EIR_LRB,
//            EIR_RECEIVED_DATE,
//            EIR_DEPLOYED_DATE,
//            EIR_INVOICE_NUMBER,
//            EIR_TRANSACTION_ID,
//            EIR_MERCHANT_NUMBER,
//            EIR_ORIGINAL_CLASS,
//            EIR_SHIPPING_COST,
//            EIR_LRB_PRICE,
//            EIR_OWNER,
//            EIR_DATE_REMOVED,
//            EIR_REMOVE_STATUS    
//          ) 
//          values 
//          (
//            :ei_part_number,
//            :ei_serial_number,
//            :ei_unit_cost,
//            :ei_class,
//            :ei_status,
//            :ei_lrb,
//            :ei_received_date,
//            :ei_deployed_date,
//            :ei_invoice_number,
//            :ei_transaction_id,
//            :ei_merchant_number,
//            :ei_original_class,
//            :ei_shipping_cost,
//            :ei_lrb_price,
//            :ei_owner,
//            sysdate,
//            :RemoveCategory          
//          )             
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_inventory_removed \n        (  \n          EIR_PART_NUMBER,\n          EIR_SERIAL_NUMBER,\n          EIR_UNIT_COST,\n          EIR_CLASS,\n          EIR_STATUS,\n          EIR_LRB,\n          EIR_RECEIVED_DATE,\n          EIR_DEPLOYED_DATE,\n          EIR_INVOICE_NUMBER,\n          EIR_TRANSACTION_ID,\n          EIR_MERCHANT_NUMBER,\n          EIR_ORIGINAL_CLASS,\n          EIR_SHIPPING_COST,\n          EIR_LRB_PRICE,\n          EIR_OWNER,\n          EIR_DATE_REMOVED,\n          EIR_REMOVE_STATUS    \n        ) \n        values \n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n          sysdate,\n           :16           \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.inventory.RemoveEquipmentBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,ei_part_number);
   __sJT_st.setString(2,ei_serial_number);
   __sJT_st.setString(3,ei_unit_cost);
   __sJT_st.setString(4,ei_class);
   __sJT_st.setString(5,ei_status);
   __sJT_st.setString(6,ei_lrb);
   __sJT_st.setTimestamp(7,ei_received_date);
   __sJT_st.setTimestamp(8,ei_deployed_date);
   __sJT_st.setString(9,ei_invoice_number);
   __sJT_st.setString(10,ei_transaction_id);
   __sJT_st.setString(11,ei_merchant_number);
   __sJT_st.setString(12,ei_original_class);
   __sJT_st.setString(13,ei_shipping_cost);
   __sJT_st.setString(14,ei_lrb_price);
   __sJT_st.setString(15,ei_owner);
   __sJT_st.setString(16,RemoveCategory);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:256^7*/
      

      /*@lineinfo:generated-code*//*@lineinfo:259^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    equip_inventory
//          where   ei_part_number   = :PartNumber and
//                  ei_serial_number = :SerialNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    equip_inventory\n        where   ei_part_number   =  :1  and\n                ei_serial_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.inventory.RemoveEquipmentBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,PartNumber);
   __sJT_st.setString(2,SerialNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:265^7*/



      // store the last updated data
      LastSerialNumber    = SerialNumber;
      LastDescription     = getEquipmentDesc( PartNumber );
      LastInventory       = getOwnerDesc( Owner );
      LastRemoveCategory  = RemoveCategory;

      // add this event to the history
      action = "Item removed from " + LastInventory + " inventory because it's " + RemoveCategory;
      addToHistory( PartNumber, SerialNumber, userId, InventoryBase.ES_TRANSFERRED, action, null, Owner );
      
     
      /*@lineinfo:generated-code*//*@lineinfo:280^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:283^7*/
    }  
    catch(Exception e)
    {
      logEntry("submit()", e.toString());
      addError("submit: " + e.toString());
    }
  }
  
  public boolean validate()
  {
    if ( isBlank( SerialNumber ) )
    {
      //addError("Please select or enter a valid serial number.");
      addError("Please select a piece of equipment.");
    }
    else if ( isBlank( PartNumber ) )
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:303^9*/

//  ************************************************************
//  #sql [Ctx] { select  ei.part_number 
//            from    equip_inventory ei
//            where   ei.ei_serial_number = :SerialNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ei.part_number  \n          from    equip_inventory ei\n          where   ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.inventory.RemoveEquipmentBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,SerialNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   PartNumber = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:308^9*/
      }
      catch( java.sql.SQLException e )
      {
        addError("Specified serial number is invalid or duplicated.  Try selecting the specific item from the list.");
      }        
    }

    if( Owner < 0 )
    {
      addError("Please specify which Client Inventory you want to remove this item from");
    }

    if( isBlank(RemoveCategory) )
    {
      addError("Please specify the condition of this item");
    }

    
    if(!hasErrors())
    {
      if(!doesExist(PartNumber, SerialNumber, Owner))
      {
        addError("Equipment with serial # " + SerialNumber + " does not exist in " + getOwnerDesc(Owner) + " Inventory.");
      }
    }

    if(!hasErrors())
    {
      if(!isInStock(PartNumber, SerialNumber, Owner))
      {
        addError("Equipment with serial # " + SerialNumber + " is currently deployed.");
      }
    }


/*
    not worried about whether its in mes inventory or not
    if(!hasErrors())
    {
      if(!isMesInventory(PartNumber, SerialNumber))
      {
        addError("Equipment with serial # " + SerialNumber + " does not belong to MES inventory.");
      }
    }
*/


    return(!hasErrors());
  }
}/*@lineinfo:generated-code*/