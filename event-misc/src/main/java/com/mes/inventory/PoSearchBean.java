/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/PoSearchBean.java $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 1/25/02 4:29p $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Vector;

public class PoSearchBean extends com.mes.screens.SequenceDataBean
{

  public static final int SEARCH_REFERENCE_NUM  = 1;
  public static final int SEARCH_INVOICE_NUM    = 2;
  public static final int SEARCH_ORDER_DATE     = 3;
  public static final int SEARCH_RECEIVE_DATE   = 4;
  public static final int SEARCH_INVOICE_DATE   = 5;
  public static final int SEARCH_SUPPLIER_ID    = 6;
  public static final int SEARCH_SERIAL_NUM     = 7;
  public static final int SEARCH_CHANGE_DATE    = 8;
  public static final int SEARCH_NOFILTER       = -1;

  public static final int SPECIFIC_DETAIL_EQUAL = 1;
  public static final int SPECIFIC_DETAIL_GREAT = 2;

  public static final int ORDER_ALL             = -1;
  public static final int ORDER_RECEIVED        = 1;
  public static final int ORDER_NOT_RECEIVED    = 2;
  public static final int ORDER_NOT_COMPLETE    = 3;

  public static final int OWNER_ALL             = -1;


  // private data members
  private boolean       changeOrder         = false;
  private String        referenceNumber     = "-1";
  private String        invoiceNumber       = "-1";
  private int           supplierId          = -1;
  private String        serialNum           = "";
  private boolean       searchSerialNum     = false;
  private int           specialSearch       = ORDER_ALL;
  private int           owner               = OWNER_ALL;

  private int           specificDateDetail        = SPECIFIC_DETAIL_EQUAL;
  private int           orderSpecificDateDetail   = SPECIFIC_DETAIL_EQUAL;
  private int           changeSpecificDateDetail  = SPECIFIC_DETAIL_EQUAL;
  private int           receiveSpecificDateDetail = SPECIFIC_DETAIL_EQUAL;
  private int           invoiceSpecificDateDetail = SPECIFIC_DETAIL_EQUAL;
  
  private Date          specificDate        = null;
  private Date          specificDateNext    = null;
  private String        specificDateStr     = "-1";
  
  private Date          orderDate           = null;
  private Date          orderDateNext       = null;
  private String        orderDateStr        = "-1";

  private Date          changeDate          = null;
  private Date          changeDateNext      = null;
  private String        changeDateStr       = "-1";

  private Date          receiveDate         = null;
  private Date          receiveDateNext     = null;
  private String        receiveDateStr      = "-1";

  private Date          invoiceDate         = null;
  private Date          invoiceDateNext     = null;
  private String        invoiceDateStr      = "-1";



  private Date          startDate           = null;
  private String        startDateStr        = "-1";
  
  private Date          endDate             = null;
  private String        endDateStr          = "-1";
  private Date          endDateNext         = null;
  
  private int           searchCriteria      = -1;
  private StringBuffer  qs                  = new StringBuffer("");
  private boolean       submit              = false;
  private boolean       hasErrors           = false;
  private Vector        errors              = new Vector();
  private ResultSet     rs                  = null;

  public Vector         suppliers           = new Vector();
  public Vector         supplierName        = new Vector();

  public Vector         ownerCode           = new Vector();
  public Vector         ownerDesc           = new Vector();

  /*
  ** CONSTRUCTOR
  */
  public PoSearchBean()
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
   
    try
    {
      ps = getPreparedStatement("select * from equip_supplier");
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        supplierName.add(rs.getString("es_supplier_name"));
        suppliers.add(rs.getString("es_supplier_id"));  
      } 

      rs.close();
      ps.close();

      ps = getPreparedStatement("select app_type_code,inventory_owner_name from app_type where SEPARATE_INVENTORY = 'Y' ");
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        ownerDesc.add(rs.getString("inventory_owner_name"));  
        ownerCode.add(rs.getString("app_type_code"));
      }

      rs.close();
      ps.close();

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "constructor: " + e.toString());
      addError("constructor: " + e.toString());
    }
  }

  

  public ResultSet getResultSet()
  {
    PreparedStatement ps      = null;
    
    try
    {
      qs.setLength(0);
      
      qs.append("select DISTINCT ep.ep_transaction_id, ");
      qs.append("       ep.ep_order_date, ");
      qs.append("       ep.ep_change_date, ");
      qs.append("       ep.ep_receive_date, ");
      qs.append("       ep.ep_invoice_number, ");
      qs.append("       ep.ep_invoice_date, ");
      qs.append("       es.es_supplier_name, ");
      qs.append("       at.inventory_owner_name ");
      qs.append("from   equip_purchase_order ep, equip_supplier es, equip_transaction et, app_type at ");
      qs.append("where  es.es_supplier_id = ep.ep_supplier_id and ep.ep_transaction_id = et.et_transaction_id and ep.ep_owner = at.app_type_code ");

      switch(this.searchCriteria)
      {
        case SEARCH_REFERENCE_NUM:
          qs.append("and ep.ep_transaction_id = ? ");
          
          if(isChangeOrder())
          {
            qs.append("and ep.ep_transaction_date is null ");
          }
  
          qs.append("order by ep.ep_order_date desc ");
        
          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.referenceNumber);
          break;
        case SEARCH_INVOICE_NUM:
          qs.append("and ep.ep_invoice_number = ? ");
          
          if(isChangeOrder())
          {
            qs.append("and ep.ep_transaction_date is null ");
          }

          qs.append("order by ep.ep_order_date desc ");
        
          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.invoiceNumber);
        
          break;
        case SEARCH_ORDER_DATE:
          if(this.orderSpecificDateDetail == SPECIFIC_DETAIL_GREAT)
          {
            qs.append("and ep.ep_order_date >= ? "); 
          }
          else
          {
            qs.append("and ep.ep_order_date between ? and ? ");
          }

          switch(this.specialSearch)
          {
            case ORDER_RECEIVED:
              qs.append("and ep.ep_transaction_date is not null ");
            break;
            case ORDER_NOT_RECEIVED:
              qs.append("and ep.ep_transaction_date is null ");
            break;
            case ORDER_NOT_COMPLETE:
              //qs.append("and ep.ep_transaction_date is not null and et.et_quantity_ordered != et.et_quantity_received ");
              qs.append("and ep.ep_transaction_date is not null and et.et_serial_number is null ");
            break;
          }

          if(this.owner != OWNER_ALL)
          {
            qs.append("and ep.ep_owner = " + Integer.toString(this.owner) + " ");
          }

          qs.append("order by ep.ep_order_date desc ");
          
          ps = getPreparedStatement(qs.toString());
          
          ps.setDate(1, new java.sql.Date(this.orderDate.getTime()));
          
          if(this.orderSpecificDateDetail != SPECIFIC_DETAIL_GREAT)
          {
            ps.setDate(2, new java.sql.Date(this.orderDateNext.getTime()));
          }
          break;
        case SEARCH_CHANGE_DATE:
          if(this.changeSpecificDateDetail == SPECIFIC_DETAIL_GREAT)
          {
            qs.append("and ep.ep_change_date >= ? "); 
          }
          else
          {
            qs.append("and ep.ep_change_date between ? and ? ");
          }

          switch(this.specialSearch)
          {
            case ORDER_RECEIVED:
              qs.append("and ep.ep_transaction_date is not null ");
            break;
            case ORDER_NOT_RECEIVED:
              qs.append("and ep.ep_transaction_date is null ");
            break;
            case ORDER_NOT_COMPLETE:
              //qs.append("and ep.ep_transaction_date is not null and et.et_quantity_ordered != et.et_quantity_received ");
              qs.append("and ep.ep_transaction_date is not null and et.et_serial_number is null ");
            break;
          }

          if(this.owner != OWNER_ALL)
          {
            qs.append("and ep.ep_owner = " + Integer.toString(this.owner) + " ");
          }

          qs.append("order by ep.ep_order_date desc ");
          
          ps = getPreparedStatement(qs.toString());
          
          ps.setDate(1, new java.sql.Date(this.changeDate.getTime()));
          
          if(this.changeSpecificDateDetail != SPECIFIC_DETAIL_GREAT)
          {
            ps.setDate(2, new java.sql.Date(this.changeDateNext.getTime()));
          }
          break;  
        case SEARCH_RECEIVE_DATE:
          if(this.receiveSpecificDateDetail == SPECIFIC_DETAIL_GREAT)
          {
            qs.append("and ep.ep_receive_date >= ? "); 
          }
          else
          {
            qs.append("and ep.ep_receive_date between ? and ? ");
          }

          switch(this.specialSearch)
          {
            case ORDER_RECEIVED:
              qs.append("and ep.ep_transaction_date is not null ");
            break;
            case ORDER_NOT_RECEIVED:
              qs.append("and ep.ep_transaction_date is null ");
            break;
            case ORDER_NOT_COMPLETE:
              //qs.append("and ep.ep_transaction_date is not null and et.et_quantity_ordered != et.et_quantity_received ");
              qs.append("and ep.ep_transaction_date is not null and et.et_serial_number is null ");
            break;
          }

          if(this.owner != OWNER_ALL)
          {
            qs.append("and ep.ep_owner = " + Integer.toString(this.owner) + " ");
          }

          qs.append("order by ep.ep_order_date desc ");
          
          ps = getPreparedStatement(qs.toString());
          
          ps.setDate(1, new java.sql.Date(this.receiveDate.getTime()));
          
          if(this.receiveSpecificDateDetail != SPECIFIC_DETAIL_GREAT)
          {
            ps.setDate(2, new java.sql.Date(this.receiveDateNext.getTime()));
          }
          break;
        case SEARCH_INVOICE_DATE:
          if(this.invoiceSpecificDateDetail == SPECIFIC_DETAIL_GREAT)
          {
            qs.append("and ep.ep_invoice_date >= ? "); 
          }
          else
          {
            qs.append("and ep.ep_invoice_date between ? and ? ");
          }

          switch(this.specialSearch)
          {
            case ORDER_RECEIVED:
              qs.append("and ep.ep_transaction_date is not null ");
            break;
            case ORDER_NOT_RECEIVED:
              qs.append("and ep.ep_transaction_date is null ");
            break;
            case ORDER_NOT_COMPLETE:
              //qs.append("and ep.ep_transaction_date is not null and et.et_quantity_ordered != et.et_quantity_received ");
              qs.append("and ep.ep_transaction_date is not null and et.et_serial_number is null ");
            break;
          }

          if(this.owner != OWNER_ALL)
          {
            qs.append("and ep.ep_owner = " + Integer.toString(this.owner) + " ");
          }

          qs.append("order by ep.ep_order_date desc ");
          
          ps = getPreparedStatement(qs.toString());
          
          ps.setDate(1, new java.sql.Date(this.invoiceDate.getTime()));
          
          if(this.invoiceSpecificDateDetail != SPECIFIC_DETAIL_GREAT)
          {
            ps.setDate(2, new java.sql.Date(this.invoiceDateNext.getTime()));
          }
          break;
        case SEARCH_SUPPLIER_ID:
          qs.append("and ep.ep_supplier_id = ? ");

          switch(this.specialSearch)
          {
            case ORDER_RECEIVED:
              qs.append("and ep.ep_transaction_date is not null ");
            break;
            case ORDER_NOT_RECEIVED:
              qs.append("and ep.ep_transaction_date is null ");
            break;
            case ORDER_NOT_COMPLETE:
              //qs.append("and ep.ep_transaction_date is not null and et.et_quantity_ordered != et.et_quantity_received ");
              qs.append("and ep.ep_transaction_date is not null and et.et_serial_number is null ");
            break;
          }

          if(this.owner != OWNER_ALL)
          {
            qs.append("and ep.ep_owner = " + Integer.toString(this.owner) + " ");
          }

          qs.append("order by ep.ep_order_date desc ");
        
          ps = getPreparedStatement(qs.toString());
          ps.setInt(1,this.supplierId);
          break;
        case SEARCH_SERIAL_NUM:
          
          this.searchSerialNum = true;
          
          qs.setLength(0);
          qs.append("select ei_transaction_id from equip_inventory where ei_serial_number = ? ");
          
          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.serialNum);
          
          rs = ps.executeQuery();
          
          if(rs.next())
          {
            this.referenceNumber = rs.getString("ei_transaction_id");
            this.searchCriteria  = SEARCH_REFERENCE_NUM;
            return getResultSet();
          }
          else
          {
            this.rs = null;
          }
          
          break;
        
        default:

          switch(this.specialSearch)
          {
            case ORDER_RECEIVED:
              qs.append("and ep.ep_transaction_date is not null ");
            break;
            case ORDER_NOT_RECEIVED:
              qs.append("and ep.ep_transaction_date is null ");
            break;
            case ORDER_NOT_COMPLETE:
              //qs.append("and ep.ep_transaction_date is not null and et.et_quantity_ordered != et.et_quantity_received ");
              qs.append("and ep.ep_transaction_date is not null and et.et_serial_number is null ");
            break;

          }
          if(this.owner != OWNER_ALL)
          {
            qs.append("and ep.ep_owner = " + Integer.toString(this.owner) + " ");
          }

          qs.append("order by ep.ep_order_date desc ");
          ps = getPreparedStatement(qs.toString());
          break;
      }
      if(this.searchCriteria != SEARCH_SERIAL_NUM)
      {
        this.rs = ps.executeQuery();
        if(this.searchSerialNum)
        {
          this.searchCriteria   = SEARCH_SERIAL_NUM;
          this.referenceNumber  = ""; 
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
      addError("getData: " + e.toString());
    }
    
    return this.rs;
  }  


  public boolean validateData()
  {
    boolean result = false;
    String temp = "";
    switch(this.searchCriteria)
    {
      case SEARCH_REFERENCE_NUM:
        if(this.referenceNumber.equals("-1"))
        {
          temp = "Please enter a valid reference number";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case SEARCH_INVOICE_NUM:
        if(this.invoiceNumber.equals("-1"))
        {
          temp = "Please enter a valid Invoice number";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case SEARCH_ORDER_DATE:
        if(this.orderDateStr.equals("-1") || !validDate(this.orderDateStr))
        {
          temp = "Please enter a valid order date in the following format:  mm/dd/yy";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        else
        {
          this.orderDateNext = getDateNext(this.orderDateStr);
        }
      break;
      case SEARCH_CHANGE_DATE:
        if(this.changeDateStr.equals("-1") || !validDate(this.changeDateStr))
        {
          temp = "Please enter a valid last change date in the following format:  mm/dd/yy";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        else
        {
          this.changeDateNext = getDateNext(this.changeDateStr);
        }
      break;
      case SEARCH_RECEIVE_DATE:
        if(this.receiveDateStr.equals("-1") || !validDate(this.receiveDateStr))
        {
          temp = "Please enter a valid received date in the following format:  mm/dd/yy";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        else
        {
          this.receiveDateNext = getDateNext(this.receiveDateStr);
        }
        break;
      case SEARCH_INVOICE_DATE:
        if(this.invoiceDateStr.equals("-1") || !validDate(this.invoiceDateStr))
        {
          temp = "Please enter a valid invoice date in the following format:  mm/dd/yy";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        else
        {
          this.invoiceDateNext = getDateNext(this.invoiceDateStr);
        }
        break;
      case SEARCH_SUPPLIER_ID:
        if(this.supplierId == -1)
        {
          temp = "Please select a supplier";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case SEARCH_SERIAL_NUM:
        if(isBlank(this.serialNum))
        {
          temp = "Please provide a Serial Number to see which purchase order it came in on";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      default:
        this.hasErrors = false;
        break;
    }
    
    if(!this.hasErrors)
    {
      result = true;
    }
    
    return result;
  }
  private boolean validDate(String date)
  {
    int index1  = date.indexOf('/');
    int index2  = date.lastIndexOf('/');
    String mon  = date.substring(0,index1);
    String day  = date.substring(index1+1,index2);
    String year = date.substring(index2+1);
    boolean result = true;
    try
    {
      int dayInt = Integer.parseInt(day);
      int monInt = Integer.parseInt(mon);
      int yearInt = Integer.parseInt(year);
      if(dayInt > 31 || dayInt < 1)
        result = false;
      if(monInt > 12 || monInt < 1)
        result = false;
      if(yearInt < 0)
        result = false;
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }

  private void getEndDateNext(String endDate)
  {
    int index1  = endDate.indexOf('/');
    int index2  = endDate.lastIndexOf('/');
    String mon  = endDate.substring(0,index1);
    String day  = endDate.substring(index1+1,index2);
    String year = endDate.substring(index2+1);
    
    try
    {
      int nextDay = Integer.parseInt(day);
      nextDay++;
      day                   = String.valueOf(nextDay);
      String date           = mon + "/" + day + "/" + year;
      DateFormat fmt        = DateFormat.getDateInstance(DateFormat.SHORT);
      this.endDateNext = fmt.parse(date);
    }
    catch(Exception e)
    {
      this.endDateNext = null;
    }
  }

  private Date getDateNext(String specificDate)
  {
    int index1  = specificDate.indexOf('/');
    int index2  = specificDate.lastIndexOf('/');
    String mon  = specificDate.substring(0,index1);
    String day  = specificDate.substring(index1+1,index2);
    String year = specificDate.substring(index2+1);
    Date result = null;
        
    try
    {
      int nextDay = Integer.parseInt(day);
      nextDay++;
      day             = String.valueOf(nextDay);
      String date     = mon + "/" + day + "/" + year;
      DateFormat fmt  = DateFormat.getDateInstance(DateFormat.SHORT);
      result          = fmt.parse(date);
    }
    catch(Exception e)
    {
      result = null;
    }
    return result;
  }

  public void setSpecificDate(String specificDate)
  {
    this.specificDateStr = specificDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(specificDate.trim());
      try
      {
        specificDate          = specificDate.replace('/','0');
        long tempLong         = Long.parseLong(specificDate.trim());
        this.specificDate     = aDate;
      }
      catch(Exception e)
      {
        this.specificDate    = null;
        this.specificDateStr = "-1";
      }
    }
    catch(ParseException e)
    {
      this.specificDate      = null;
      this.specificDateStr   = "-1";
    }
  }
 
  public String getSpecificDate()
  {
    String result = "";
    if(!this.specificDateStr.equals("-1"))
    {
      result = this.specificDateStr;
    }
    return result;
  }
 


  public void setOrderDate(String orderDate)
  {
    this.orderDateStr = orderDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(orderDate.trim());
      try
      {
        orderDate             = orderDate.replace('/','0');
        long tempLong         = Long.parseLong(orderDate.trim());
        this.orderDate        = aDate;
      }
      catch(Exception e)
      {
        this.orderDate    = null;
        this.orderDateStr = "-1";
      }
    }
    catch(ParseException e)
    {
      this.orderDate      = null;
      this.orderDateStr   = "-1";
    }
  }
 
  public String getOrderDate()
  {
    String result = "";
    if(!this.orderDateStr.equals("-1"))
    {
      result = this.orderDateStr;
    }
    return result;
  }

  public void setChangeDate(String changeDate)
  {
    this.changeDateStr = changeDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(changeDate.trim());
      try
      {
        changeDate            = changeDate.replace('/','0');
        long tempLong         = Long.parseLong(changeDate.trim());
        this.changeDate       = aDate;
      }
      catch(Exception e)
      {
        this.changeDate    = null;
        this.changeDateStr = "-1";
      }
    }
    catch(ParseException e)
    {
      this.changeDate      = null;
      this.changeDateStr   = "-1";
    }
  }
 
  public String getChangeDate()
  {
    String result = "";
    if(!this.changeDateStr.equals("-1"))
    {
      result = this.changeDateStr;
    }
    return result;
  }
  
  public void setReceiveDate(String receiveDate)
  {
    this.receiveDateStr = receiveDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(receiveDate.trim());
      try
      {
        receiveDate       = receiveDate.replace('/','0');
        long tempLong     = Long.parseLong(receiveDate.trim());
        this.receiveDate  = aDate;
      }
      catch(Exception e)
      {
        this.receiveDate    = null;
        this.receiveDateStr = "-1";
      }
    }
    catch(ParseException e)
    {
      this.receiveDate      = null;
      this.receiveDateStr   = "-1";
    }
  }
 
  public String getReceiveDate()
  {
    String result = "";
    if(!this.receiveDateStr.equals("-1"))
    {
      result = this.receiveDateStr;
    }
    return result;
  }

  public void setInvoiceDate(String invoiceDate)
  {
    this.invoiceDateStr = invoiceDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(invoiceDate.trim());
      try
      {
        invoiceDate       = invoiceDate.replace('/','0');
        long tempLong     = Long.parseLong(invoiceDate.trim());
        this.invoiceDate  = aDate;
      }
      catch(Exception e)
      {
        this.invoiceDate    = null;
        this.invoiceDateStr = "-1";
      }
    }
    catch(ParseException e)
    {
      this.invoiceDate      = null;
      this.invoiceDateStr   = "-1";
    }
  }
 
  public String getInvoiceDate()
  {
    String result = "";
    if(!this.invoiceDateStr.equals("-1"))
    {
      result = this.invoiceDateStr;
    }
    return result;
  }



 
  public void setStartDate(String startDate)
  {
    this.startDateStr = startDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(startDate.trim());
      try
      {
        startDate          = startDate.replace('/','0');
        long tempLong      = Long.parseLong(startDate.trim());
        this.startDate     = aDate;
      }
      catch(Exception e)
      {
        this.startDate    = null;
        this.startDateStr = "-1";
      }
    }
    catch(ParseException e)
    {
      this.startDate    = null;
      this.startDateStr = "-1";
    }
  }
 
  public String getStartDate()
  {
    String result = "";
    if(!this.startDateStr.equals("-1"))
    {
      result = this.startDateStr;
    }
    return result;
  }
  public void setEndDate(String endDate)
  {
    this.endDateStr = endDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(endDate.trim());
      try
      {
        endDate        = endDate.replace('/','0');
        long tempLong  = Long.parseLong(endDate.trim());
        this.endDate   = aDate;
      }
      catch(Exception e)
      {
        this.endDate    = null;
        this.endDateStr = "-2";
      }
    }
    catch(ParseException e)
    {
      this.endDate    = null;
      this.endDateStr = "-2";
    }
  }
 
  public String getEndDate()
  {
    String result = "";
    if(!this.endDateStr.equals("-1") && !this.endDateStr.equals("-2"))
    {
      result = this.endDateStr;
    }
    return result;
  }

  public void setSpecificDateDetail(String specificDateDetail)
  {
    try
    {
      this.specificDateDetail = Integer.parseInt(specificDateDetail); 
    }
    catch(Exception e)
    {
      this.specificDateDetail = -1;
    }
  }
  public int getSpecificDateDetail()
  {
    int result = 0;
    if(this.specificDateDetail != -1)
    {
      result = this.specificDateDetail;
    }
    return result;
  }


  public void setOrderSpecificDateDetail(String orderSpecificDateDetail)
  {
    try
    {
      this.orderSpecificDateDetail = Integer.parseInt(orderSpecificDateDetail); 
    }
    catch(Exception e)
    {
      this.orderSpecificDateDetail = -1;
    }
  }
  public int getOrderSpecificDateDetail()
  {
    int result = 0;
    if(this.orderSpecificDateDetail != -1)
    {
      result = this.orderSpecificDateDetail;
    }
    return result;
  }

  public void setChangeSpecificDateDetail(String changeSpecificDateDetail)
  {
    try
    {
      this.changeSpecificDateDetail = Integer.parseInt(changeSpecificDateDetail); 
    }
    catch(Exception e)
    {
      this.changeSpecificDateDetail = -1;
    }
  }
  public int getChangeSpecificDateDetail()
  {
    int result = 0;
    if(this.changeSpecificDateDetail != -1)
    {
      result = this.changeSpecificDateDetail;
    }
    return result;
  }
  
  public void setInvoiceSpecificDateDetail(String invoiceSpecificDateDetail)
  {
    try
    {
      this.invoiceSpecificDateDetail = Integer.parseInt(invoiceSpecificDateDetail); 
    }
    catch(Exception e)
    {
      this.invoiceSpecificDateDetail = -1;
    }
  }
  public int getInvoiceSpecificDateDetail()
  {
    int result = 0;
    if(this.invoiceSpecificDateDetail != -1)
    {
      result = this.invoiceSpecificDateDetail;
    }
    return result;
  }

  public void setReceiveSpecificDateDetail(String receiveSpecificDateDetail)
  {
    try
    {
      this.receiveSpecificDateDetail = Integer.parseInt(receiveSpecificDateDetail); 
    }
    catch(Exception e)
    {
      this.receiveSpecificDateDetail = -1;
    }
  }
  public int getReceiveSpecificDateDetail()
  {
    int result = 0;
    if(this.receiveSpecificDateDetail != -1)
    {
      result = this.receiveSpecificDateDetail;
    }
    return result;
  }
  

  public void setSearchCriteria(String searchCriteria)
  {
    try
    {
      this.searchCriteria = Integer.parseInt(searchCriteria); 
    }
    catch(Exception e)
    {
      this.searchCriteria = -1;
    }
  }
  public int getSearchCriteria()
  {
    return this.searchCriteria;
  }

  public void setSpecialSearch(String specialSearch)
  {
    try
    {
      this.specialSearch = Integer.parseInt(specialSearch); 
    }
    catch(Exception e)
    {
      this.specialSearch = -1;
    }
  }
  public int getSpecialSearch()
  {
    return this.specialSearch;
  }

  public void setOwner(String owner)
  {
    try
    {
      this.owner = Integer.parseInt(owner); 
    }
    catch(Exception e)
    {
      this.owner = -1;
    }
  }
  public int getOwner()
  {
    return this.owner;
  }



  public void setSubmit(String submit)
  {
    this.submit = true;
  }
  public boolean isSubmitted()
  {
    return this.submit;
  }

  public boolean getHasErrors()
  {
    return this.hasErrors;
  }

  public Vector getErrors()
  {
    return errors;
  }

  public void setInvoiceNum(String invoiceNumber)
  {
    this.invoiceNumber = invoiceNumber;
  }

  public void setSupplierId(String supplierId)
  {
    try
    {
      int temp = Integer.parseInt(supplierId);
      this.supplierId = temp;
    }
    catch(Exception e)
    {
      this.supplierId = -1;
    }
  }

  public void setReferenceNum(String referenceNumber)
  {
    try
    {
      long temp = Long.parseLong(referenceNumber);
      this.referenceNumber = referenceNumber;
    }
    catch(Exception e)
    {
      this.referenceNumber = "-1";
    }
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum.trim();
  }
  public String getSerialNum()
  {
    return this.serialNum;
  }

  public String getInvoiceNum()
  {
    String result = "";
    if(!this.invoiceNumber.equals("-1"))
    {
      result = this.invoiceNumber;
    }
    return result;
  }

  public int getSupplierId()
  {
    return this.supplierId;
  }
  public String getReferenceNum()
  {
    String result = "";
    if(!this.referenceNumber.equals("-1"))
    {
      result = this.referenceNumber;
    }
    return result;
  }
  public void setChangeOrder(String temp)
  {
    this.changeOrder = true;
  }
  public boolean isChangeOrder()
  {
    return this.changeOrder;
  }

}
