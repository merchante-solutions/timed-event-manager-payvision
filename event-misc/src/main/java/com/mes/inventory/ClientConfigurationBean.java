/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/ClientConfigurationBean.java $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 1/31/02 5:00p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
   property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

public class ClientConfigurationBean extends com.mes.screens.SequenceDataBean
{


  private String   swapGuidelines                    = "";
  private String   repairGuidelines                  = "";

  private String   clientCode                        = "";
  private String   clientName                        = "";

  private boolean  noPossibleClients                 = false;

  public  Vector   clients                           = new Vector();
  public  Vector   clientCodes                       = new Vector();

  public  Vector   possibleClients                   = new Vector();
  public  Vector   possibleClientCodes               = new Vector();


  public ClientConfigurationBean()
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    StringBuffer      qs      = new StringBuffer();

    try
    {

      ps = getPreparedStatement("select app_type_code,inventory_owner_name from app_type where SEPARATE_INVENTORY = 'Y' ");
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        clients.add(rs.getString("inventory_owner_name"));  
        clientCodes.add(rs.getString("app_type_code"));
      }

      rs.close();
      ps.close();

      ps = getPreparedStatement("select app_type_code,inventory_owner_name from app_type where SEPARATE_INVENTORY != 'Y' ");
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        possibleClients.add(rs.getString("inventory_owner_name"));  
        possibleClientCodes.add(rs.getString("app_type_code"));
      }

      rs.close();
      ps.close();
     
      if(possibleClientCodes.size() == 0)
      {
        noPossibleClients = true;
      }

    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "constructor: " + e.toString());
      addError("constructor: " + e.toString());
    }
  }
  
  public void getData(String clientCode)
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    StringBuffer      qs      = new StringBuffer();

    try
    {
      this.clientCode = clientCode; //set clientcode

      ps = getPreparedStatement("select ecg.*, at.inventory_owner_name from equip_client_guidelines ecg, app_type at where ecg.client_code = ? and ecg.client_code = at.app_type_code ");
      ps.setString(1, this.clientCode);

      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.swapGuidelines   = isBlank(rs.getString("swap_guidelines"))      ? "" : rs.getString("swap_guidelines");
        this.repairGuidelines = isBlank(rs.getString("repair_guidelines"))    ? "" : rs.getString("repair_guidelines");
        this.clientName       = isBlank(rs.getString("inventory_owner_name")) ? "" : rs.getString("inventory_owner_name");
      }
      else
      {
        rs.close();
        ps.close();
        ps = getPreparedStatement("select inventory_owner_name from app_type where app_type_code = ? ");
        ps.setString(1, this.clientCode);

        rs = ps.executeQuery();
    
        if(rs.next())
        {
          this.clientName = isBlank(rs.getString("inventory_owner_name")) ? "" : rs.getString("inventory_owner_name");
        }
      }

      rs.close();
      ps.close();
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
      addError("getData: " + e.toString());
    }
  }

  public void submitData()
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    StringBuffer      qs      = new StringBuffer();

    try
    {
      ps = getPreparedStatement("update app_type set SEPARATE_INVENTORY = 'Y', CLIENT_CODE = ? where app_type_code = ? ");
      ps.setString(1, this.clientCode);
      ps.setString(2, this.clientCode);

      ps.executeUpdate();
      
      ps.close();

      qs.setLength(0);
      qs.append("update equip_client_guidelines set ");
      qs.append("swap_guidelines   = ?, ");
      qs.append("repair_guidelines = ? ");
      qs.append("where client_code = ? ");

      ps = getPreparedStatement(qs.toString());
      ps.setString(1,this.swapGuidelines);
      ps.setString(2,this.repairGuidelines);
      ps.setString(3,this.clientCode);
  
      if(ps.executeUpdate() == 0)
      {
        qs.setLength(0);
        qs.append("insert into equip_client_guidelines (");
        qs.append("swap_guidelines, ");
        qs.append("repair_guidelines, ");
        qs.append("client_code) values (?,?,?) ");

        ps = getPreparedStatement(qs.toString());
        ps.setString(1,this.swapGuidelines);
        ps.setString(2,this.repairGuidelines);
        ps.setString(3,this.clientCode);
        
       ps.executeUpdate();
     }
  
      ps.close();
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
      addError("submitData: " + e.toString());
    }
  }

  public boolean validate()
  {

    if(this.swapGuidelines.length() > 1000)
    {
      addError("Swap Guidelines must be limited to 1000 characters.  Currently there are " + this.swapGuidelines.length() + " characters");
    }

    if(this.repairGuidelines.length() > 1000)
    {
      addError("Repair Guidelines must be limited to 1000 characters.  Currently there are " + this.repairGuidelines.length() + " characters");
    }

    return(!hasErrors());
  }

  public boolean isNoPossibleClients()
  {
    return this.noPossibleClients;
  }

  public void setClientCode(String clientCode)
  {
    this.clientCode = clientCode;
  }

  public String getClientCode()
  {
    return this.clientCode;
  }

  public String getClientName()
  {
    return this.clientName;
  }
  public void setClientName(String clientName)
  {
    this.clientName = clientName;
  }

  public String getSwapGuidelines()
  {
    return this.swapGuidelines;
  }
  public String getRepairGuidelines()
  {
    return this.repairGuidelines;
  }

  public void setSwapGuidelines(String swapGuidelines)
  {
    this.swapGuidelines = swapGuidelines;
  }
  public void setRepairGuidelines(String repairGuidelines)
  {
    this.repairGuidelines = repairGuidelines;
  }


}