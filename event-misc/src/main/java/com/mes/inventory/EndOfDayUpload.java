/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/EndOfDayUpload.java $

  Description:
    Base class for application data beans

    This class should maintain any data that is common to all of the
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.

  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 4/15/04 3:16p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.inventory;

import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.screens.SequenceDataBean;
import com.mes.support.StringUtilities;
import com.mes.tools.DBGrunt;

public class EndOfDayUpload extends SequenceDataBean
{

  /**
   * Log4J Logger
   */
  static Logger log = Logger.getLogger(EndOfDayUpload.class);

  //not reading stored files anymore.. reading directly from file in memory
  public static final String  UPLOAD_FILE = "../EXPORT.CSV"; //POINTS TO C DRIVE
  public static final boolean HAS_HEADER  = false;

  public static final String SL_CODE_E            = "E";
  public static final String SL_DESC_E            = "Express";
  public static final String SL_CODE_S            = "S";
  public static final String SL_DESC_S            = "Second Day";
  public static final String SL_CODE_N            = "N";
  public static final String SL_DESC_N            = "NAS";
  public static final String SL_CODE_G            = "G";
  public static final String SL_DESC_G            = "Ground";

  public static final String SL_CODE_1            = "1";
  public static final String SL_DESC_1            = "Priority Overnight";
  public static final String SL_CODE_3            = "3";
  public static final String SL_DESC_3            = "2 Day";
  public static final String SL_CODE_83           = "83";
  public static final String SL_DESC_83           = "Express Freight";
  public static final String SL_CODE_6            = "6";
  public static final String SL_DESC_6            = "First Overnight";
  public static final String SL_CODE_92           = "92";
  public static final String SL_DESC_92           = "Ground";
  public static final String SL_CODE_90           = "90";
  public static final String SL_DESC_90           = "Home Delivery";
  public static final String SL_CODE_70           = "70";
  public static final String SL_DESC_70           = "One Day Freight";
  public static final String SL_CODE_50           = "50";
  public static final String SL_CODE_5            = "5";
  public static final String SL_DESC_5_50         = "Standard Overnight";
  public static final String SL_CODE_20           = "20";
  public static final String SL_DESC_20           = "Express Saver";
  public static final String SL_CODE_80           = "80";
  public static final String SL_DESC_80           = "2 Day Freight";

/*
FEDEX
2Day  = 3
Express Freight = 83
Express Saver 20
First Overnight = 6
Ground = 92
Home Delivery = 90
Internal. Economy = 3
Intl. Economy Freight = 86
Intl. Priority Overnight = 1
One Day Freight = 70
Priority Overnight = 1
Standard Overnight = 50 or 5
2Day Freight = 80
*/

  public static final String  LOGIN_NAME          = "SYSTEM";
  public static final String  CALL_BACK           = "N";
  public static final int     SC_TYPE_DEPLOYMENT  = 14; //internal
  public static final int     SC_STATUS_CLOSED    = 2;

  private Vector accounts         = new Vector();   //for service_calls
  private ArrayList trackList     = new ArrayList();//for ship_uplaod_details
  private int    linesRead        = 0;
  private String SHIPPER          = "FedEx"; //default

  public EndOfDayUpload()
  {
    log.debug("test copy");
  }

  public void setShipper(String shipper)
  {
    if(shipper !=null)
    {
      if (shipper.indexOf("F")!=0)
      {
        SHIPPER = "Airborne";
      }
    }
    log.debug("SHIPPER = "+SHIPPER);
  }

  public boolean submitData()
  {
    /*
    for(int x=0; x< this.accounts.size(); x++)
    {
      AcctItem tempRec =  (AcctItem)accounts.elementAt(x);
      log.debug(tempRec.toString());
    }
    */

    if( insertServiceCalls() && insertShipDetails() )
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  private boolean insertServiceCalls()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    boolean           result  = true;

    try
    {
      qs.setLength(0);
      qs.append("insert into service_calls (      ");
      qs.append("merchant_number,                 ");
      qs.append("call_date,                       ");
      qs.append("type,                            ");
      qs.append("other_description,               ");
      qs.append("login_name,                      ");
      qs.append("call_back,                       ");
      qs.append("status,                          ");
      qs.append("notes,                           ");
      qs.append("billable, client_generated       ");
      qs.append(") values(?,sysdate,?,?,?,?,?,?,'N','N')  ");

      ps = getPreparedStatement(qs.toString());

      for(int x=0; x< this.accounts.size(); x++)
      {
        AcctItem tempRec =  (AcctItem)accounts.elementAt(x);

        ps.setString(1, tempRec.RECEIVER_KEY        );
        ps.setInt   (2, SC_TYPE_DEPLOYMENT          );
        ps.setString(3, ""                          ); //no description for other
        ps.setString(4, LOGIN_NAME                  );
        ps.setString(5, CALL_BACK                   );
        ps.setInt   (6, SC_STATUS_CLOSED            );
        ps.setString(7, tempRec.NOTE_MESSAGE        );

        ps.executeUpdate();
      }

      ps.close();
    }
    catch(Exception e)
    {
      log.debug("submit data: " + e.toString());
      result = false;
    }

    return result;
  }



  private boolean insertShipDetails()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    boolean           result  = true;

    try
    {
      qs.setLength(0);
      qs.append("insert into         ");
      qs.append("SHIP_UPLOAD_DETAIL  ");
      qs.append("(                   ");
      qs.append("merchant_number,    ");
      qs.append("description,        ");
      qs.append("ship_weight,        ");
      qs.append("service_level,      ");
      qs.append("pickup_date,        ");
      qs.append("tracking_number,    ");
      qs.append("type_key,           ");
      qs.append("process_id          ");
      qs.append(")                   ");
      qs.append("values              ");
      qs.append("(?,?,?,?,?,?,?,?)   ");

      ps = getPreparedStatement(qs.toString());

      for(int x=0; x< this.trackList.size(); x++)
      {
        AcctItem tempRec =  (AcctItem)trackList.get(x);
        log.debug(tempRec.toString());
        ps.setString(1, tempRec.RECEIVER_KEY      );
        ps.setString(2, tempRec.DESCRIPTION_LINE1 );
        ps.setString(3, tempRec.SHIPMENT_WEIGHT   );
        ps.setString(4, tempRec.SERVICE_LEVEL     );
        ps.setDate(5, tempRec.getPUDate()         );
        ps.setString(6, tempRec.SHIPMENT_NUMBER   );
        ps.setString(7, tempRec.TYPE_KEY          );
        ps.setString(8, tempRec.PROCESS_ID           );

        ps.executeUpdate();
      }

      ps.close();
    }
    catch(Exception e)
    {
      log.debug("submit data: " + e.toString());
      result = false;
    }

     return result;
  }



  public void setData(String data)
  {
    if(!isBlank(data))
    {
      parseLine(data, ++linesRead);
    }
  }


//941000055330,"W.Kt7p thermal/call tag",5.3,"1", 11162005, 725944502080, "acr 24652633"

//10053694,     T7PROLL/CALL TAG,        6,   E,  11142005, 33823237173, ACR 24592766


  private boolean parseLine(String line, int linenum)
  {

      log.debug("#"+linenum+": "+line);

      AcctItem        acct        = null;
      String[]        item        = new String[7];
      int             idx         = 0;
      StringBuffer    tempBuff    = new StringBuffer("");
      boolean         endOfField  = false;


      try
      {
        for(int l=0; l<line.length(); ++l)
        {
          if(line.charAt(l) == '"')
          {
            endOfField = !endOfField;
            continue;
          }

          if(line.charAt(l) == ',' && !endOfField)
          {
            item[idx] = ((tempBuff.toString()).trim()).toUpperCase();
            if(isBlank(item[idx]))
            {
              item[idx] = "";
            }
            idx++;
            tempBuff = new StringBuffer("");
          }
          else
          {
            tempBuff.append(line.charAt(l));
          }
        }

        item[idx] = isBlank(tempBuff.toString()) ? "" : ((tempBuff.toString()).trim()).toUpperCase(); //must get last field after last ','

        if(idx != 6)
        {
          log.debug("error occured at idx != 6 line # " + linenum);
        }

        log.debug("item[0] = "+item[0]);

        acct        = new AcctItem();

        acct.RECEIVER_KEY       = item[0].trim();
        acct.DESCRIPTION_LINE1  = item[1].trim();
        acct.SHIPMENT_WEIGHT    = item[2].trim();
        acct.SERVICE_LEVEL      = decodeServiceLevel(item[3].trim());
        acct.PICKUP_DATE        = formatPickupDate(item[4].trim());
        acct.SHIPMENT_NUMBER    = item[5].trim();
        acct.buildKeys(item[6].trim());
        acct.NOTE_MESSAGE       = createNoteMessage(acct);

        //TRACKING NUMBERS
        //add all items to the trackList, as we'll be using these to generate
        //mapping of track numbers to appropriate processes
        trackList.add(acct);

        //SERVICE CALLS
        //check to see if this merchant already had has an entry, if so just add the note message to the existing not message
        //so there is only one entry per tape file per merchant
        if(isValidNumber(item[0]))
        {
          boolean addToVector = true;

          for(int f=0; f<accounts.size(); f++)
          {
            if(acct.RECEIVER_KEY.equals(((AcctItem)accounts.elementAt(f)).RECEIVER_KEY))
            {
              ((AcctItem)accounts.elementAt(f)).NOTE_MESSAGE += (" ------ " + acct.NOTE_MESSAGE);
              addToVector = false;
              break;
            }
          }
          if(addToVector)
          {
            accounts.add(acct);
          }
        }

    }
    catch(Exception e)
    {
      log.debug("end of parseLine: " + e.toString());
    }

    return true;
  }

  private String decodeServiceLevel(String slCode)
  {
    String result = "";

    if(slCode.equals(SL_CODE_E))
    {
      result = SL_DESC_E;
    }
    else if(slCode.equals(SL_CODE_S))
    {
      result = SL_DESC_S;
    }
    else if(slCode.equals(SL_CODE_N))
    {
      result = SL_DESC_N;
    }
    else if(slCode.equals(SL_CODE_G))
    {
      result = SL_DESC_G;
    }
    else if(slCode.equals(SL_CODE_1))
    {
      result = SL_DESC_1;
    }
    else if(slCode.equals(SL_CODE_20))
    {
      result = SL_DESC_20;
    }
    else if(slCode.equals(SL_CODE_3))
    {
      result = SL_DESC_3;
    }
    else if(slCode.equals(SL_CODE_6))
    {
      result = SL_DESC_6;
    }
    else if(slCode.equals(SL_CODE_5) || slCode.equals(SL_CODE_50))
    {
      result = SL_DESC_5_50;
    }
    else if(slCode.equals(SL_CODE_70))
    {
      result = SL_DESC_70;
    }
    else if(slCode.equals(SL_CODE_80))
    {
      result = SL_DESC_80;
    }
    else if(slCode.equals(SL_CODE_83))
    {
      result = SL_DESC_83;
    }
    else if(slCode.equals(SL_CODE_90))
    {
      result = SL_DESC_90;
    }
    else if(slCode.equals(SL_CODE_92))
    {
      result = SL_DESC_92;
    }


    return result;
  }

  private String formatPickupDate(String puDate)
  {
    if(puDate.length() == 7)
    {
      puDate = "0" + puDate;
    }

    if(puDate.length() == 8)
    {
      puDate = puDate.substring(0,2) + "-" + puDate.substring(2,4) + "-" + puDate.substring(6);
    }

    return puDate;
  }

  private String createNoteMessage(AcctItem acct)
  {
    return (  //acct.TYPE_KEY +
              //acct.PROCESS_ID + "/" +
              acct.DESCRIPTION_LINE1 + " " +
              acct.SHIPMENT_WEIGHT + "lbs. shipped via " +
              SHIPPER + " " +
              acct.SERVICE_LEVEL + " on " +
              acct.PICKUP_DATE + " Airbill #: " +
              acct.SHIPMENT_NUMBER);
  }

  private boolean isValidNumber(String num)
  {
    boolean result = true;

    try
    {
      long test = Long.parseLong(num);
    }
    catch(Exception e)
    {
      result = false;
    }

    return result;
  }


  public class AcctItem
  {
    public String RECEIVER_KEY        = "";
    public String DESCRIPTION_LINE1   = "";
    public String SHIPMENT_WEIGHT     = "";
    public String SERVICE_LEVEL       = "";
    public String PICKUP_DATE         = "";
    public String SHIPMENT_NUMBER     = "";
    public String NOTE_MESSAGE        = "";
    public String TYPE_KEY            = "";
    public String PROCESS_ID          = "";

    /**
     * paramount in tying the incoming upload info to
     * a process, this method determines what was entered in
     * the 'RECEIVER_NOTES' field and assesses the type_key and
     * process_id (ACR#, merchant num etc,)
     */
    public void buildKeys(String info)
    {
      if( info != null)
      {
        int index = 0;
        char c;
        while (index < info.length() )
        {
          c = info.charAt(index);
          try
          {
            Long.parseLong("" + c);
            break;
          }
          catch (NumberFormatException nfe)
          {
            index++;
          }
        }

        if(index>0)
        {
          TYPE_KEY = info.substring(0,index).trim();
          PROCESS_ID = info.substring(index).trim();
        }
        else
        {
          TYPE_KEY = "WK";
          //process id remains empty - use RECEIVER_KEY
          //to map to MIF etc. for OLA
        }
      }
    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer(128);

        sb.append("-------------------------").append("\n");
        sb.append("RECEIVER_KEY = ").append(RECEIVER_KEY).append("\n");
        sb.append("DESCRIPTION_LINE1 = ").append(DESCRIPTION_LINE1).append("\n");
        sb.append("SHIPMENT_WEIGHT = ").append(SHIPMENT_WEIGHT).append("\n");
        sb.append("SERVICE_LEVEL = ").append(SERVICE_LEVEL).append("\n");
        sb.append("PICKUP_DATE = ").append(PICKUP_DATE).append("\n");
        sb.append("SHIPMENT_NUMBER = ").append(SHIPMENT_NUMBER).append("\n");
        sb.append("NOTE_MESSAGE = ").append(NOTE_MESSAGE).append("\n");
        sb.append("TYPE_KEY = ").append(TYPE_KEY).append("\n");
        sb.append("PROCESS_ID = ").append(PROCESS_ID).append("\n");
        sb.append("-------------------------\n");

        return sb.toString();
    }

    public java.sql.Date getPUDate()
    {
      java.util.Date utilDate;
      String newDateString="---";

      try
      {
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
        newDateString = StringUtilities.replace(PICKUP_DATE,"-","/");
        utilDate = df.parse(newDateString);
      }
      catch (Exception e)
      {
        log.debug("newDateString = "+newDateString);
        //e.printStackTrace();
        utilDate = DBGrunt.getCurrentDate();
      }

      return new java.sql.Date(utilDate.getTime());

    }
  }

}

