/*@lineinfo:filename=ReceivePurchaseOrderDetailsBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/ReceivePurchaseOrderDetailsBean.sqlj $

  Description:


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/22/02 5:20p $
  Version            : $Revision: 18 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.ops.InventoryBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class ReceivePurchaseOrderDetailsBean extends InventoryBase
{

  //always the same
  private long     referenceNumber                      = 0L;
  private String   partNumber                           = "";
  private String   productName                          = "";
  private String   productDesc                          = "";

  //hold original info from po
  private String    classType                           = "";
  private String    classTypeDesc                       = "";
  private double    unitCost                            = 0.0;
  private double    averageShipping                     = 0.0;
  private int       orderOwner                          = 0;

  //used for header...
  private String   supplier                             = "";
  private String   supplierCode                         = "";
  private Date     orderDate                            = null;
  private Date     receiveDate                          = null;
  private Date     invoiceDate                          = null;
  private String   invoiceNum                           = "";
  private String   shipCompany                          = "";
  private String   shipCharge                           = "";

  //keeps tract of whats been added and what needs to be added
  private Vector   receivedEquipment                    = new Vector();
  private Vector   newEquipment                         = new Vector();
  private int      numOrdered                           = 0;
  private int      numReceived                          = 0;
  private int      numNeeded                            = 0;

  //used for condition drop down
  public Vector   classes                               = new Vector();
  public Vector   classesCode                           = new Vector();

  public Vector   owner                                 = new Vector();
  public Vector   ownerCode                             = new Vector();

  private HashMap  validateMap                          = new HashMap();

  public ReceivePurchaseOrderDetailsBean()
  {
    ResultSetIterator     it      = null;
    try
    {
      if(isConnectionStale())
      {
        connect();
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:98^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ec_class_name,
//                  ec_class_id
//          from    equip_class
//          order by ec_class_id asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ec_class_name,\n                ec_class_id\n        from    equip_class\n        order by ec_class_id asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.ReceivePurchaseOrderDetailsBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.ReceivePurchaseOrderDetailsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:104^7*/
      
      ResultSet rs = it.getResultSet();

      while(rs.next())
      {
        classes.add(rs.getString("ec_class_name"));
        classesCode.add(rs.getString("ec_class_id"));
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:117^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_type_code,
//                  inventory_owner_name
//          from    app_type
//          where   separate_inventory = 'Y'
//          order by app_type_code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_type_code,\n                inventory_owner_name\n        from    app_type\n        where   separate_inventory = 'Y'\n        order by app_type_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.inventory.ReceivePurchaseOrderDetailsBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.inventory.ReceivePurchaseOrderDetailsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:124^7*/
      
      rs = it.getResultSet();

      while(rs.next())
      {
        owner.add(rs.getString("inventory_owner_name"));
        ownerCode.add(rs.getString("app_type_code"));
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("constructor--filling grids()", e.toString());
    }
  }

  //this info comes in from link.. num ordered specifies how many to show.. we replace this.. get it from db directly
  //do search on part num and og classtype...
  public void setData(HttpServletRequest request)
  {
    ResultSetIterator   it      = null;
    
    // get order owner from referenceNumber
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:152^7*/

//  ************************************************************
//  #sql [Ctx] { select  ep_owner
//          
//          from    equip_purchase_order
//          where   ep_transaction_id = :this.referenceNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ep_owner\n         \n        from    equip_purchase_order\n        where   ep_transaction_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.inventory.ReceivePurchaseOrderDetailsBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,this.referenceNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   this.orderOwner = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:158^7*/
    }
    catch(Exception e)
    {
      addError("Unable to get owner: " + e.toString());
    }

    // get unit cost from referenceNumber, partNumber, and classType
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:168^7*/

//  ************************************************************
//  #sql [Ctx] { select  distinct et_unit_price
//          
//          from    equip_transaction
//          where   et_transaction_id = :this.referenceNumber and
//                  et_part_number    = :this.partNumber and
//                  et_class          = :this.classType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct et_unit_price\n         \n        from    equip_transaction\n        where   et_transaction_id =  :1  and\n                et_part_number    =  :2  and\n                et_class          =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.inventory.ReceivePurchaseOrderDetailsBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,this.referenceNumber);
   __sJT_st.setString(2,this.partNumber);
   __sJT_st.setString(3,this.classType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   this.unitCost = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:176^7*/
    }
    catch(Exception e)
    {
      addError("Unable to get unit cost: " + e.toString());
    }
    
    try
    {
      // get product name and description and classtype
      /*@lineinfo:generated-code*//*@lineinfo:186^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  class.ec_class_name,
//                  equip.equip_descriptor,
//                  mfgr.equipmfgr_mfr_name
//          from    equipment equip,
//                  equipmfgr mfgr,
//                  equip_class class
//          where   equip.equip_model = :this.partNumber and
//                  class.ec_class_id = :this.classType and
//                  equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  class.ec_class_name,\n                equip.equip_descriptor,\n                mfgr.equipmfgr_mfr_name\n        from    equipment equip,\n                equipmfgr mfgr,\n                equip_class class\n        where   equip.equip_model =  :1  and\n                class.ec_class_id =  :2  and\n                equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.inventory.ReceivePurchaseOrderDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.partNumber);
   __sJT_st.setString(2,this.classType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.inventory.ReceivePurchaseOrderDetailsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^7*/
      
      ResultSet rs = it.getResultSet();

      if(rs.next())
      {
        this.productName    = rs.getString("equipmfgr_mfr_name");
        this.productDesc    = rs.getString("equip_descriptor");
        this.classTypeDesc  = rs.getString("ec_class_name");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      addError("Unable to set product name and description: " + e.toString());
    }
    
    // get the averate shipping cost
    getAveShippingCost(); // must get before we update data...
    
    // get what still needs to be assigned a serial number
    getQuantityData();
    
    // update vector of equipment with the current data from the database
    setEquipmentList(request);
  }

  private void getAveShippingCost()
  {
    ResultSetIterator it          = null;
    int               numOfItems  = 0;
    double            shipping    = 0.0;

    try
    {
      // get total number of items in this purchase order
      /*@lineinfo:generated-code*//*@lineinfo:235^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(et_transaction_id)
//          
//          from    equip_transaction
//          where   et_transaction_id = :this.referenceNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(et_transaction_id)\n         \n        from    equip_transaction\n        where   et_transaction_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.inventory.ReceivePurchaseOrderDetailsBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,this.referenceNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   numOfItems = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:241^7*/

      //
      /*@lineinfo:generated-code*//*@lineinfo:244^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ep_ship_charge
//          from    equip_purchase_order
//          where   ep_transaction_id = :this.referenceNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ep_ship_charge\n        from    equip_purchase_order\n        where   ep_transaction_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.inventory.ReceivePurchaseOrderDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,this.referenceNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.inventory.ReceivePurchaseOrderDetailsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:249^7*/

      ResultSet rs = it.getResultSet();

      if(rs.next())
      {
        shipping = rs.getDouble("ep_ship_charge");
      }

      if(shipping > 0.0 && numOfItems > 0)
      {
        this.averageShipping = shipping / (double)numOfItems;
      }
      else
      {
        this.averageShipping = 0.0;
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getAveShippingCost()", e.toString());
      addError("getAveShippingCost: " + e.toString());
    }
  }

  public void getData()
  {
    getReceivedData();
    getQuantityData();
  }

  private void getQuantityData()
  {
    ResultSetIterator it      = null;
    int               i       = 0;

    try
    {
      //get received inventory
      /*@lineinfo:generated-code*//*@lineinfo:291^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    equip_transaction
//          where   et_transaction_id = :this.referenceNumber and
//                  et_part_number    = :this.partNumber and
//                  et_class          = :this.classType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    equip_transaction\n        where   et_transaction_id =  :1  and\n                et_part_number    =  :2  and\n                et_class          =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.inventory.ReceivePurchaseOrderDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,this.referenceNumber);
   __sJT_st.setString(2,this.partNumber);
   __sJT_st.setString(3,this.classType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.inventory.ReceivePurchaseOrderDetailsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:298^7*/

      ResultSet rs = it.getResultSet();

      this.numReceived  = 0;
      this.numNeeded    = 0;
      this.numOrdered   = 0;

      while(rs.next())
      {
        ++this.numOrdered;

        if( isBlank(rs.getString("et_serial_number")))
        {
          ++this.numNeeded;
        }
        else
        {
          ++this.numReceived;
        }
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getQuantityData()", e.toString());
      addError("getQuantityData: " + e.toString());
    }
  }



  private void getReceivedData()
  {
    ResultSetIterator it      = null;
    int               i       = 0;

    try
    {
      //get received inventory
      /*@lineinfo:generated-code*//*@lineinfo:340^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_serial_number,
//                  ei.ei_unit_cost,
//                  ei.ei_received_date,
//                  ei.ei_shipping_cost,
//                  ec.ec_class_name,
//                  es.equip_status_desc,
//                  at.inventory_owner_name
//          from    equip_inventory ei,
//                  equip_class ec,
//                  equip_status es,
//                  app_type at
//          where   ei.ei_transaction_id = :this.referenceNumber and
//                  ei.ei_part_number = :this.partNumber and
//                  ei.ei_original_class = :this.classType and
//                  ei.ei_class = ec.ec_class_id and
//                  ei.ei_status = es.equip_status_id and
//                  ei.ei_owner = at.app_type_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_serial_number,\n                ei.ei_unit_cost,\n                ei.ei_received_date,\n                ei.ei_shipping_cost,\n                ec.ec_class_name,\n                es.equip_status_desc,\n                at.inventory_owner_name\n        from    equip_inventory ei,\n                equip_class ec,\n                equip_status es,\n                app_type at\n        where   ei.ei_transaction_id =  :1  and\n                ei.ei_part_number =  :2  and\n                ei.ei_original_class =  :3  and\n                ei.ei_class = ec.ec_class_id and\n                ei.ei_status = es.equip_status_id and\n                ei.ei_owner = at.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.inventory.ReceivePurchaseOrderDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,this.referenceNumber);
   __sJT_st.setString(2,this.partNumber);
   __sJT_st.setString(3,this.classType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.inventory.ReceivePurchaseOrderDetailsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:359^7*/

      ResultSet rs = it.getResultSet();

      while(rs.next())
      {
        EquipRec tempRec =  new EquipRec();
        tempRec.setSerialNumber(    rs.getString("ei_serial_number")  );
        tempRec.setClassDesc(       rs.getString("ec_class_name")     );
        tempRec.setStatusDesc(      rs.getString("equip_status_desc") );
        tempRec.setActualUnitCost(  rs.getDouble("ei_unit_cost")      );
        tempRec.setActualShipCost(  rs.getDouble("ei_shipping_cost")  );
        tempRec.setOwnerDesc(       rs.getString("inventory_owner_name"));
        tempRec.setDateReceived(    rs.getDate("ei_received_date")    );

        receivedEquipment.add(tempRec);
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getReceivedData()", e.toString());
      addError("getReceivedData: " + e.toString());
    }
  }

  private void setEquipmentList(HttpServletRequest request)
  {
    int      i           = 0;
    EquipRec tempRec     = null;
    
    newEquipment.clear();

    while(i < this.numNeeded)
    {
      if(isSubmitted())
      {
        tempRec = new EquipRec();
        
        if(! HttpHelper.getString(request, "serialNumber" + i, "xyx").equals("xyx"))
        {
          tempRec.setSerialNumber(HttpHelper.getString(request, "serialNumber" + i));
        }
        if(! HttpHelper.getString(request, "actualUnitCost" + i, "xyx").equals("xyx"))
        {
          tempRec.setActualUnitCost(HttpHelper.getString(request, "actualUnitCost" + i));
        }
        if(! HttpHelper.getString(request, "actualShipCost" + i, "xyx").equals("xyx"))
        {
          tempRec.setActualShipCost(HttpHelper.getString(request, "actualShipCost" + i));
        }
        if(! HttpHelper.getString(request, "actualClassType" + i, "xyx").equals("xyx"))
        {
          tempRec.setActualClassType(HttpHelper.getString(request, "actualClassType" + i));
        }
        if(! HttpHelper.getString(request, "owner" + i, "xyx").equals("xyx"))
        {
          tempRec.setOwner(HttpHelper.getString(request, "owner" + i));
        }
      }
      else
      {
        tempRec = new EquipRec(this.unitCost, this.classType, this.averageShipping, this.orderOwner);
      }

      ++i;
      
      newEquipment.add(tempRec);
    }
  }

  private boolean doesExist(String serNum)
  {
    int               itemCount   = 0;
    boolean           result      = true;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:439^7*/

//  ************************************************************
//  #sql [Ctx] { select count(ei_serial_number) 
//          from equip_inventory
//          where ei_serial_number = :serNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(ei_serial_number)  \n        from equip_inventory\n        where ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.inventory.ReceivePurchaseOrderDetailsBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,serNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:444^7*/
      if(itemCount > 0)
      {
        result = true;
      }
      else
      {
        result = false;
      }

    }
    catch(Exception e)
    {
      logEntry("submitEquipment()", e.toString());
      addError("submitEquipment: " + e.toString());
    }
    return result;
  }

  public boolean validate()
  {
    int i = 0;

    while(i < this.numNeeded)
    {
      EquipRec equip = (EquipRec)newEquipment.elementAt(i);

      if(!isBlank(equip.getSerialNumber()))
      {
        if(doesExist(equip.getSerialNumber()))
        {
          addError("Serial number " + equip.getSerialNumber() +  " already exists in database");
        }
        else
        {
          String tempClass = (String)validateMap.get(equip.getSerialNumber());
          if(isBlank(tempClass))
          {
            validateMap.put(equip.getSerialNumber(),"added");
          }
          else
          {
            addError("Each item must have a unique serial number");
          }
        }

        if(equip.getActualUnitCost() == -1)
        {
          addError("Please provide a valid Actual Unit Cost");
        }

        if(equip.getActualShipCost() == -1)
        {
          addError("Please provide a valid Actual Shipping Cost");
        }

        if(equip.getActualClassType() == -1)
        {
          addError("Please select an Actual Product Condition");
        }

        if(equip.getOwner() == -1)
        {
          addError("Please select an Owner so this item can be put into their separate inventory");
        }
      }

      i++;
    }

    return(!hasErrors());
  }

  public void submitData(long userLogin)
  {
    updateInventory(userLogin);
    updateReceiveDate();
  }

  /*
  ** This method update the Received Date field in the purchase order table.. once the first piece of equipment
  ** from an order is inputed into the database it time stamps the order as received if it hasnt already done so
  */

  private void updateReceiveDate()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;

    try
    {
      qs.append("update equip_purchase_order set ");
      qs.append("ep_receive_date = sysdate ");
      qs.append("where ep_transaction_id = ? and ep_receive_date is null ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1, this.referenceNumber);
      ps.executeUpdate();
      ps.close();
    }
    catch(Exception e)
    {
      logEntry("updateReceiveDate()", e.toString());
      addError("updateReceiveDate: " + e.toString());
    }
  }


  private void updateInventory(long userLogin)
  {
    ResultSetIterator   it      = null;
    int                 i       = 0;

    try
    {
      Vector tempVec = new Vector();

      while(i < newEquipment.size())
      {
        EquipRec  equip = (EquipRec)newEquipment.elementAt(i);

        if( ! isBlank(equip.getSerialNumber()))
        {
          if( ! doesExist(equip.getSerialNumber()))
          {
            // update equip transaction
            /*@lineinfo:generated-code*//*@lineinfo:571^13*/

//  ************************************************************
//  #sql [Ctx] { update  equip_transaction
//                set     et_serial_number  = :equip.getSerialNumber()
//                where   et_transaction_id = :this.referenceNumber and
//                        et_part_number    = :this.partNumber and
//                        et_class          = :this.classType and
//                        et_serial_number  is null and
//                        et_place_holder =
//                        (
//                          select  min(et_place_holder)
//                          from    equip_transaction
//                          where   et_transaction_id = :this.referenceNumber and
//                                  et_part_number    = :this.partNumber and
//                                  et_class          = :this.classType and
//                                  et_serial_number  is null
//                        )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1803 = equip.getSerialNumber();
   String theSqlTS = "update  equip_transaction\n              set     et_serial_number  =  :1 \n              where   et_transaction_id =  :2  and\n                      et_part_number    =  :3  and\n                      et_class          =  :4  and\n                      et_serial_number  is null and\n                      et_place_holder =\n                      (\n                        select  min(et_place_holder)\n                        from    equip_transaction\n                        where   et_transaction_id =  :5  and\n                                et_part_number    =  :6  and\n                                et_class          =  :7  and\n                                et_serial_number  is null\n                      )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.inventory.ReceivePurchaseOrderDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1803);
   __sJT_st.setLong(2,this.referenceNumber);
   __sJT_st.setString(3,this.partNumber);
   __sJT_st.setString(4,this.classType);
   __sJT_st.setLong(5,this.referenceNumber);
   __sJT_st.setString(6,this.partNumber);
   __sJT_st.setString(7,this.classType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:588^13*/

            // insert new equipment into inventory
            /*@lineinfo:generated-code*//*@lineinfo:591^13*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_inventory
//                (
//                  ei_part_number,
//                  ei_transaction_id,
//                  ei_unit_cost,
//                  ei_class,
//                  ei_original_class,
//                  ei_received_date,
//                  ei_invoice_number,
//                  ei_lrb,
//                  ei_status,
//                  ei_shipping_cost,
//                  ei_serial_number,
//                  ei_owner
//                )
//                values
//                (
//                  :this.partNumber,
//                  :this.referenceNumber,
//                  :equip.getActualUnitCost(),
//                  :equip.getActualClassType(),
//                  :this.classType,
//                  sysdate,
//                  :this.invoiceNum,
//                  :mesConstants.APP_EQUIP_IN_STOCK,
//                  :mesConstants.APP_EQUIP_IN_STOCK,
//                  :equip.getActualShipCost(),
//                  :equip.getSerialNumber(),
//                  :equip.getOwner()
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_1804 = equip.getActualUnitCost();
 int __sJT_1805 = equip.getActualClassType();
 double __sJT_1806 = equip.getActualShipCost();
 String __sJT_1807 = equip.getSerialNumber();
 int __sJT_1808 = equip.getOwner();
   String theSqlTS = "insert into equip_inventory\n              (\n                ei_part_number,\n                ei_transaction_id,\n                ei_unit_cost,\n                ei_class,\n                ei_original_class,\n                ei_received_date,\n                ei_invoice_number,\n                ei_lrb,\n                ei_status,\n                ei_shipping_cost,\n                ei_serial_number,\n                ei_owner\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                 :5 ,\n                sysdate,\n                 :6 ,\n                 :7 ,\n                 :8 ,\n                 :9 ,\n                 :10 ,\n                 :11 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.inventory.ReceivePurchaseOrderDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.partNumber);
   __sJT_st.setLong(2,this.referenceNumber);
   __sJT_st.setDouble(3,__sJT_1804);
   __sJT_st.setInt(4,__sJT_1805);
   __sJT_st.setString(5,this.classType);
   __sJT_st.setString(6,this.invoiceNum);
   __sJT_st.setInt(7,mesConstants.APP_EQUIP_IN_STOCK);
   __sJT_st.setInt(8,mesConstants.APP_EQUIP_IN_STOCK);
   __sJT_st.setDouble(9,__sJT_1806);
   __sJT_st.setString(10,__sJT_1807);
   __sJT_st.setInt(11,__sJT_1808);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:623^13*/
          }
          else
          {
            addError("Serial number " + equip.getSerialNumber() + " already exists in database");
          }
        }
        else
        {
          // keep this item in unreceived inventory queue
          tempVec.add(equip);
        }

        ++i;
      }

      newEquipment = tempVec;
    }
    catch(Exception e)
    {
      logEntry("updateInventory()", e.toString());
      addError("updateInventory: " + e.toString());
    }
  }

  public String formatCurr(String curr)
  {
    if(isBlank(curr))
    {
      return "";
    }
    else if(curr.equals("-----"))
    {
      return curr;
    }

    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }

  public String formatCurr(double curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(curr);
    }
    catch(Exception e)
    {}
    return result;
  }


  public int getListSize()
  {
    return receivedEquipment.size();
  }

  public void setNumOrdered(String num)
  {
    try
    {
      this.numOrdered = Integer.parseInt(num);
    }
    catch(Exception e)
    {
      this.numOrdered = 0;
    }
  }

  public int getNumOrdered()
  {
    return this.numOrdered;
  }

  public int getNumNeeded()
  {
    return this.numNeeded;
  }

  public void setNumNeeded(String num)
  {
    try
    {
      this.numNeeded = Integer.parseInt(num);
    }
    catch(Exception e)
    {
      this.numNeeded = 0;
    }
  }

  public String encode(String url)
  {
    String body = url;
    try
    {
      StringBuffer    bodyBuff = null;
      int             index = 0;

      index = body.indexOf(' ');

      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"%20");
        body = bodyBuff.toString();
        index = body.indexOf(' ', index);
      }
    }
    catch(Exception e)
    {
      System.out.println("error will robinson %20");
    }
    return body;
  }


  public void setReferenceNumber(String referenceNumber)
  {
    try
    {
      this.referenceNumber = Long.parseLong(referenceNumber);
    }
    catch(Exception e)
    {
      logEntry("setReferenceNumber(" + referenceNumber + ")", e.toString());
    }
  }
  public long getReferenceNumber()
  {
    return this.referenceNumber;
  }

  public String getPartNumber()
  {
    return this.partNumber;
  }
  public void setPartNumber(String partNumber)
  {
    this.partNumber = partNumber;
  }

  public String getClassType()
  {
    return this.classType;
  }
  public void setClassType(String classType)
  {
    this.classType = classType;
  }

  public int getOrderOwner()
  {
    return this.orderOwner;
  }
  public void setOrderOwner(String orderOwner)
  {
    try
    {
      this.orderOwner = Integer.parseInt(orderOwner);
    }
    catch(Exception e)
    {
      this.orderOwner = 0;
    }
  }

  public String getUnitCost()
  {
    return formatCurr(unitCost);
  }
  public void setUnitCost(String unitCost)
  {
    try
    {
      this.unitCost = Double.parseDouble(unitCost);
    }
    catch(Exception e)
    {
      this.unitCost = 0.0;
    }
  }

  public String getAverageShipping()
  {
    return formatCurr(this.averageShipping);
  }

  public void setAverageShipping(String averageShipping)
  {
    try
    {
      this.averageShipping = Double.parseDouble(averageShipping);
    }
    catch(Exception e)
    {
      this.averageShipping = 0.0;
    }
  }

  public String getProductName()
  {
    return this.productName;
  }
  public String getProductDesc()
  {
    return this.productDesc;
  }
  public String getClassTypeDesc()
  {
    return this.classTypeDesc;
  }

  public String getDateReceived(int idx)
  {
    EquipRec equip = (EquipRec)receivedEquipment.elementAt(idx);
    return(equip.getDateReceived());
  }
  public String getSerialNumber(int idx)
  {
    EquipRec equip = (EquipRec)receivedEquipment.elementAt(idx);
    return(equip.getSerialNumber());
  }
  public double getUnitCost(int idx)
  {
    EquipRec equip = (EquipRec)receivedEquipment.elementAt(idx);
    return(equip.getActualUnitCost());
  }
  public double getShipCost(int idx)
  {
    EquipRec equip = (EquipRec)receivedEquipment.elementAt(idx);
    return(equip.getActualShipCost());
  }
  public String getClassDesc(int idx)
  {
    EquipRec equip = (EquipRec)receivedEquipment.elementAt(idx);
    return(equip.getClassDesc());
  }
  public String getStatusDesc(int idx)
  {
    EquipRec equip = (EquipRec)receivedEquipment.elementAt(idx);
    return(equip.getStatusDesc());
  }
  public String getOwnerDesc(int idx)
  {
    EquipRec equip = (EquipRec)receivedEquipment.elementAt(idx);
    return(equip.getOwnerDesc());
  }

  public double getActualUnitCost(int idx)
  {
    EquipRec equip = (EquipRec)newEquipment.elementAt(idx);
    return(equip.getActualUnitCost());
  }
  public double getActualShipCost(int idx)
  {
    EquipRec equip = (EquipRec)newEquipment.elementAt(idx);
    return(equip.getActualShipCost());
  }

  public int getActualClassType(int idx)
  {
    EquipRec equip = (EquipRec)newEquipment.elementAt(idx);
    return(equip.getActualClassType());
  }
  public int getOwner(int idx)
  {
    EquipRec equip = (EquipRec)newEquipment.elementAt(idx);
    return(equip.getOwner());
  }
  public String getSerialNumNeeded(int idx)
  {
    EquipRec equip = (EquipRec)newEquipment.elementAt(idx);
    return(equip.getSerialNumber());
  }

  public class EquipRec
  {

    private String  serialNumber     = "";
    private String  dateReceived     = "";
    private String  classDesc        = "";
    private int     actualClassType  = -1;
    private String  statusDesc       = "";
    private String  ownerDesc        = "";
    private int     owner            = -1;
    private double  actualUnitCost   = 0.0;
    private double  actualShipCost   = 0.0;

    EquipRec()
    {}

    EquipRec(double unitCost, String classType, double aveShipCost, int owner)
    {
      setActualUnitCost(unitCost);
      setActualClassType(classType);
      setActualShipCost(aveShipCost);
      setOwner(owner);
    }

    public void setDateReceived(Date dateReceived)
    {
      this.dateReceived = DateTimeFormatter.getFormattedDate(dateReceived, "MM/dd/yy");
    }
    public void setSerialNumber(String serialNumber)
    {
      this.serialNumber = serialNumber;
    }
    public void setClassDesc(String classDesc)
    {
      this.classDesc = classDesc;
    }
    public void setStatusDesc(String statusDesc)
    {
      this.statusDesc = statusDesc;
    }
    public void setOwnerDesc(String ownerDesc)
    {
      this.ownerDesc = ownerDesc;
    }

    public void setActualUnitCost(double actualUnitCost)
    {
      this.actualUnitCost = actualUnitCost;
    }
    public void setActualUnitCost(String actualUnitCost)
    {
      try
      {
        setActualUnitCost(Double.parseDouble(actualUnitCost));
      }
      catch(Exception e)
      {
        this.actualUnitCost = 0.0;
      }
    }
    public void setActualShipCost(double actualShipCost)
    {
      this.actualShipCost = actualShipCost;
    }
    public void setActualShipCost(String actualShipCost)
    {
      try
      {
        setActualShipCost(Double.parseDouble(actualShipCost));
      }
      catch(Exception e)
      {
        this.actualShipCost = 0.0;
      }
    }


    public void setActualClassType(String actualClassType)
    {
      try
      {
        this.actualClassType = Integer.parseInt(actualClassType);
      }
      catch(Exception e)
      {
        this.actualClassType = -1;
      }
    }

    public void setOwner(int owner)
    {
      this.owner = owner;
    }
    public void setOwner(String owner)
    {
      try
      {
        setOwner(Integer.parseInt(owner));
      }
      catch(Exception e)
      {
        this.owner = -1;
      }
    }

    public String getClassDesc()
    {
      return this.classDesc;
    }
    public String getStatusDesc()
    {
      return this.statusDesc;
    }
    public String getOwnerDesc()
    {
      return this.ownerDesc;
    }
    public String getSerialNumber()
    {
      return this.serialNumber;
    }
    public String getDateReceived()
    {
      return this.dateReceived;
    }

    public double getActualUnitCost()
    {
      return this.actualUnitCost;
    }

    public double getActualShipCost()
    {
      return this.actualShipCost;
    }

    public int getActualClassType()
    {
      return this.actualClassType;
    }
    public int getOwner()
    {
      return this.owner;
    }
  }
}/*@lineinfo:generated-code*/