/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/EquipSearchBean.java $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/23/04 3:08p $
  Version            : $Revision: 10 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Vector;

public class EquipSearchBean extends com.mes.screens.SequenceDataBean
{

  public static final int SEARCH_SERIAL_NUM     = 1;
  public static final int SEARCH_PRODUCT_NAME   = 2;
  public static final int SEARCH_PRODUCT_TYPE   = 3;
  public static final int SEARCH_PRODUCT_DESC   = 4;
  public static final int SEARCH_MERCHANT_NUM   = 5;
  public static final int SEARCH_DEPLOYED_DATE  = 6;
  public static final int SEARCH_REFERENCE_NUM  = 7;
  public static final int SEARCH_NOFILTER       = -1;

  public static final int SPECIFIC_DETAIL_EQUAL = 1;
  public static final int SPECIFIC_DETAIL_GREAT = 2;

  public static final int ALL                   = -2;
  public static final int ALL_IN_STOCK          = com.mes.constants.mesConstants.APP_EQUIP_IN_STOCK;
  public static final int ALL_DEPLOYED          = -4;
  public static final int ALL_STATUS            = -1;
  public static final int ALL_OWNERS            = -1;

  // private data members
  private boolean      edit                 = false;

  private String       referenceNumber      = "-1";
  private String       serialNum            = "";
  private int          productName          = -1;
  private int          productType          = -1;
  private String       productDesc          = "";
  private String       merchantNum          = "-1";

  private int          productCond          = -1;
  private int          productLrb           = -1;
  private int          productStatus        = ALL_STATUS;
  private int          productOwner         = ALL_OWNERS;

  private int          specificDateDetail       = SPECIFIC_DETAIL_EQUAL;
  private int          deploySpecificDateDetail = SPECIFIC_DETAIL_EQUAL;
     
  private Date         deployDate           = null;
  private Date         deployDateNext       = null;
  private String       deployDateStr        = "-1";

  private Date         startDate            = null;
  private String       startDateStr         = "-1";
  
  private Date         endDate              = null;
  private String       endDateStr           = "-1";
  private Date         endDateNext          = null;
  
  private int          searchCriteria       = -1;
  private StringBuffer qs                   = new StringBuffer("");
  private boolean      submit               = false;
  private boolean      hasErrors            = false;
  private Vector       errors               = new Vector();
  private ResultSet    rs                   = null;

  public Vector        productNamev         = new Vector();
  public Vector        productNameDesc      = new Vector();

  public Vector        productTypev         = new Vector();
  public Vector        productTypeDesc      = new Vector();

  public Vector        productModel         = new Vector();
  public Vector        productModelDesc     = new Vector();
  
  public Vector        productCondv         = new Vector();
  public Vector        productCondDesc      = new Vector();

  public Vector        productLRB           = new Vector();
  public Vector        productLRBDesc       = new Vector();

  public Vector        productStatusCode    = new Vector();
  public Vector        productStatusDesc    = new Vector();

  public Vector        owner                = new Vector();  
  public Vector        ownerCode            = new Vector();

  /*
  ** CONSTRUCTOR
  */
  public EquipSearchBean()
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
   
    try
    {
      ps = getPreparedStatement("select * from equipmfgr where equipmfgr_mfr_code != 9 order by equipmfgr_mfr_name");
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        productNamev.add(rs.getString("equipmfgr_mfr_code"));
        productNameDesc.add(rs.getString("equipmfgr_mfr_name"));  
      } 
      rs.close();
      ps.close();

      ps = getPreparedStatement("select * from equiptype where equiptype_code != 7 and equiptype_code != 8 order by equiptype_description");
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        productTypev.add(rs.getString("equiptype_code"));
        productTypeDesc.add(rs.getString("equiptype_description"));  
      } 
      rs.close();
      ps.close();

      ps = getPreparedStatement("select equip_model,equip_descriptor from equipment where equiptype_code not in (7,8,9) and used_in_inventory = 'Y' order by equip_descriptor");
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        productModel.add(rs.getString("equip_model"));
        productModelDesc.add(rs.getString("equip_descriptor"));  
      } 
      rs.close();
      ps.close();

      ps = getPreparedStatement("select * from equip_status order by equip_status_order");
      rs = ps.executeQuery();
      
      productStatusDesc.add("All");
      productStatusCode.add(Integer.toString(ALL_STATUS));  

      while(rs.next())
      {
        productStatusDesc.add(rs.getString("equip_status_desc"));
        productStatusCode.add(rs.getString("equip_status_id"));  
      }

      rs.close();
      ps.close();

      ps = getPreparedStatement("select * from equip_class");
      rs = ps.executeQuery();
 
      productCondv.add(Integer.toString(ALL));
      productCondDesc.add("All");

      while(rs.next())
      {
        productCondv.add(rs.getString("ec_class_id"));
        productCondDesc.add(rs.getString("ec_class_name"));  
      } 
      rs.close();
      ps.close();

      ps = getPreparedStatement("select app_type_code,inventory_owner_name from app_type where SEPARATE_INVENTORY = 'Y' ");
      rs = ps.executeQuery();

      owner.add("All");
      ownerCode.add(Integer.toString(ALL_OWNERS));
      
      while(rs.next())
      {
        owner.add(rs.getString("inventory_owner_name"));  
        ownerCode.add(rs.getString("app_type_code"));
      }

      rs.close();
      ps.close();


      productLRB.add(Integer.toString(ALL));
      productLRBDesc.add("All");
      productLRB.add(Integer.toString(ALL_IN_STOCK));
      productLRBDesc.add("All Not Deployed");
      productLRB.add(Integer.toString(ALL_DEPLOYED));
      productLRBDesc.add("All Deployed");
      productLRB.add("1");
      productLRBDesc.add("Purchased");
      productLRB.add("2");
      productLRBDesc.add("Rented");
      productLRB.add("5");
      productLRBDesc.add("Leased");
      productLRB.add("7");
      productLRBDesc.add("Loan");
      productLRB.add("6");
      productLRBDesc.add("Unknown");

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "constructor: " + e.toString());
      addError("constructor: " + e.toString());
    }
  }

  

  public ResultSet getResultSet()
  {
    PreparedStatement ps      = null;
    
    try
    {
      qs.setLength(0);
      
      
      
      qs.append("select inv.*, miff.dmagent, class.ec_class_name, equip.equip_descriptor, type.equiptype_description, ");
      qs.append("mfgr.equipmfgr_mfr_name, lend.equiplendtype_description, status.equip_status_desc, appdep.lend_type, appdep.amount, at.inventory_owner_name ");
      qs.append("from equipment equip, equiptype type, equipmfgr mfgr, equip_inventory inv, view_app_deployed_equipment appdep, ");
      qs.append("equip_class class, equiplendtype lend, equip_status status, mif miff, app_type at ");

      qs.append("where inv.EI_MERCHANT_NUMBER = miff.merchANT_NUMber(+) and inv.EI_MERCHANT_NUMBER = appdep.MERCH_NUMBER(+) and inv.EI_PART_NUMBER = appdep.MODEL(+) and inv.ei_part_number = equip.equip_model and ");
      
      if(this.productCond != ALL)
      {
        qs.append("inv.ei_class = " + this.productCond + " and ");
      }
      qs.append("inv.ei_class = class.ec_class_id and ");
      
      if(this.productLrb != ALL && this.productLrb != ALL_DEPLOYED)
      {
        qs.append("inv.ei_lrb = " + this.productLrb + " and ");
      }
      else if(this.productLrb == ALL_DEPLOYED)
      {
        qs.append("inv.ei_lrb != " + ALL_IN_STOCK + " and ");
      }

      qs.append("inv.ei_lrb = lend.equiplendtype_code and ");

      if(this.productStatus != ALL_STATUS)
      {
        qs.append("inv.ei_status = " + this.productStatus + " and ");
      }


      qs.append("inv.ei_status = status.equip_status_id and ");

      if(this.productOwner != ALL_OWNERS)
      {
        qs.append("inv.ei_owner = " + this.productOwner + " and ");
      }

      qs.append("inv.ei_owner = at.app_type_code and ");

      qs.append("equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code and ");
      qs.append("equip.equiptype_code = type.equiptype_code ");
      

      //System.out.println(qs.toString());
      
      
      switch(this.searchCriteria)
      {
        case SEARCH_SERIAL_NUM:
          qs.append("and inv.ei_serial_number = ? ");
          qs.append("order by inv.ei_received_date desc ");
          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.serialNum);
          rs = ps.executeQuery();
          break;
        case SEARCH_PRODUCT_NAME:
          qs.append("and equip.equipmfgr_mfr_code = ? ");
          qs.append("order by inv.ei_received_date desc ");
          ps = getPreparedStatement(qs.toString());
          ps.setInt(1,this.productName);
          rs = ps.executeQuery();
          break;
        case SEARCH_PRODUCT_TYPE:
          qs.append("and equip.equiptype_code = ? ");
          qs.append("order by inv.ei_received_date desc ");
          ps = getPreparedStatement(qs.toString());
          ps.setInt(1,this.productType);
          rs = ps.executeQuery();
          break;
        case SEARCH_PRODUCT_DESC:
          qs.append("and equip.equip_model = ? ");
          qs.append("order by inv.ei_received_date desc ");
          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.productDesc);
          rs = ps.executeQuery();
          break;
        case SEARCH_MERCHANT_NUM:
          qs.append("and inv.ei_merchant_number = ? ");
          qs.append("order by inv.ei_received_date desc ");
          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.merchantNum);
          break;
        case SEARCH_DEPLOYED_DATE:
          if(this.deploySpecificDateDetail == SPECIFIC_DETAIL_GREAT)
          {
            qs.append("and inv.ei_deployed_date >= ? "); 
          }
          else
          {
            qs.append("and inv.ei_deployed_date between ? and ? ");
          }
          qs.append("order by inv.ei_received_date desc ");
          
          ps = getPreparedStatement(qs.toString());
          
          ps.setDate(1, new java.sql.Date(this.deployDate.getTime()));
          
          if(this.deploySpecificDateDetail != SPECIFIC_DETAIL_GREAT)
          {
            ps.setDate(2, new java.sql.Date(this.deployDateNext.getTime()));
          }
          break;
        case SEARCH_REFERENCE_NUM:
          qs.append("and inv.ei_transaction_id = ? ");
          qs.append("order by inv.ei_received_date desc ");
          ps = getPreparedStatement(qs.toString());
          ps.setString(1,this.referenceNumber);
          break;
        default:
          qs.append("order by inv.ei_received_date desc ");
          ps = getPreparedStatement(qs.toString());
          break;
      }
        this.rs = ps.executeQuery();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
      addError("getData: " + e.toString());
    }
    
    return this.rs;
  }  


  public boolean validateData()
  {
    boolean result = false;
    String temp = "";

    
    
    switch(this.searchCriteria)
    {
      case SEARCH_SERIAL_NUM:
        if(isBlank(this.serialNum))
        {
          temp = "Please provide a Serial Number";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case SEARCH_PRODUCT_NAME:
        if(this.productName == -1)
        {
          temp = "Please select a Product Name";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case SEARCH_PRODUCT_TYPE:
        if(this.productType == -1)
        {
          temp = "Please select a Product Type";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case SEARCH_PRODUCT_DESC:
        if(isBlank(this.productDesc))
        {
          temp = "Please select a Product Description";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case SEARCH_MERCHANT_NUM:
        if(this.merchantNum.equals("-1"))
        {
          temp = "Please enter a valid Merchant Number";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      case SEARCH_DEPLOYED_DATE:
        if(this.deployDateStr.equals("-1") || !validDate(this.deployDateStr))
        {
          temp = "Please enter a valid Deployed Date in the following format:  mm/dd/yy";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        else
        {
          this.deployDateNext = getDateNext(this.deployDateStr);
        }
      break;
      case SEARCH_REFERENCE_NUM:
        if(this.referenceNumber.equals("-1"))
        {
          temp = "Please enter a valid Reference Number";
          try
          {
            errors.add(temp);
          }
          catch(Exception e)
          {}
          this.hasErrors = true;
        }
        break;
      default:
        this.hasErrors = false;
        break;
    }
    
    
    if(!this.hasErrors)
    {
      result = true;
    }
    
    return result;
  }
  private boolean validDate(String date)
  {
    int index1  = date.indexOf('/');
    int index2  = date.lastIndexOf('/');
    String mon  = date.substring(0,index1);
    String day  = date.substring(index1+1,index2);
    String year = date.substring(index2+1);
    boolean result = true;
    try
    {
      int dayInt = Integer.parseInt(day);
      int monInt = Integer.parseInt(mon);
      int yearInt = Integer.parseInt(year);
      if(dayInt > 31 || dayInt < 1)
        result = false;
      if(monInt > 12 || monInt < 1)
        result = false;
      if(yearInt < 0)
        result = false;
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }

  private void getEndDateNext(String endDate)
  {
    int index1  = endDate.indexOf('/');
    int index2  = endDate.lastIndexOf('/');
    String mon  = endDate.substring(0,index1);
    String day  = endDate.substring(index1+1,index2);
    String year = endDate.substring(index2+1);
    
    try
    {
      int nextDay = Integer.parseInt(day);
      nextDay++;
      day                   = String.valueOf(nextDay);
      String date           = mon + "/" + day + "/" + year;
      DateFormat fmt        = DateFormat.getDateInstance(DateFormat.SHORT);
      this.endDateNext = fmt.parse(date);
    }
    catch(Exception e)
    {
      this.endDateNext = null;
    }
  }

  private Date getDateNext(String specificDate)
  {
    int index1  = specificDate.indexOf('/');
    int index2  = specificDate.lastIndexOf('/');
    String mon  = specificDate.substring(0,index1);
    String day  = specificDate.substring(index1+1,index2);
    String year = specificDate.substring(index2+1);
    Date result = null;
        
    try
    {
      int nextDay = Integer.parseInt(day);
      nextDay++;
      day             = String.valueOf(nextDay);
      String date     = mon + "/" + day + "/" + year;
      DateFormat fmt  = DateFormat.getDateInstance(DateFormat.SHORT);
      result          = fmt.parse(date);
    }
    catch(Exception e)
    {
      result = null;
    }
    return result;
  }

  public void setDeployDate(String deployDate)
  {
    this.deployDateStr = deployDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(deployDate.trim());
      try
      {
        deployDate             = deployDate.replace('/','0');
        long tempLong         = Long.parseLong(deployDate.trim());
        this.deployDate        = aDate;
      }
      catch(Exception e)
      {
        this.deployDate    = null;
        this.deployDateStr = "-1";
      }
    }
    catch(ParseException e)
    {
      this.deployDate      = null;
      this.deployDateStr   = "-1";
    }
  }
 
  public String getDeployDate()
  {
    String result = "";
    if(!this.deployDateStr.equals("-1"))
    {
      result = this.deployDateStr;
    }
    return result;
  }

  public void setStartDate(String startDate)
  {
    this.startDateStr = startDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(startDate.trim());
      try
      {
        startDate          = startDate.replace('/','0');
        long tempLong      = Long.parseLong(startDate.trim());
        this.startDate     = aDate;
      }
      catch(Exception e)
      {
        this.startDate    = null;
        this.startDateStr = "-1";
      }
    }
    catch(ParseException e)
    {
      this.startDate    = null;
      this.startDateStr = "-1";
    }
  }
 
  public String getStartDate()
  {
    String result = "";
    if(!this.startDateStr.equals("-1"))
    {
      result = this.startDateStr;
    }
    return result;
  }
  public void setEndDate(String endDate)
  {
    this.endDateStr = endDate.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(endDate.trim());
      try
      {
        endDate        = endDate.replace('/','0');
        long tempLong  = Long.parseLong(endDate.trim());
        this.endDate   = aDate;
      }
      catch(Exception e)
      {
        this.endDate    = null;
        this.endDateStr = "-2";
      }
    }
    catch(ParseException e)
    {
      this.endDate    = null;
      this.endDateStr = "-2";
    }
  }
 
  public String getEndDate()
  {
    String result = "";
    if(!this.endDateStr.equals("-1") && !this.endDateStr.equals("-2"))
    {
      result = this.endDateStr;
    }
    return result;
  }

  public void setSpecificDateDetail(String specificDateDetail)
  {
    try
    {
      this.specificDateDetail = Integer.parseInt(specificDateDetail); 
    }
    catch(Exception e)
    {
      this.specificDateDetail = -1;
    }
  }
  public int getSpecificDateDetail()
  {
    int result = 0;
    if(this.specificDateDetail != -1)
    {
      result = this.specificDateDetail;
    }
    return result;
  }

  public void setDeploySpecificDateDetail(String deploySpecificDateDetail)
  {
    try
    {
      this.deploySpecificDateDetail = Integer.parseInt(deploySpecificDateDetail); 
    }
    catch(Exception e)
    {
      this.deploySpecificDateDetail = -1;
    }
  }
  public int getDeploySpecificDateDetail()
  {
    int result = 0;
    if(this.deploySpecificDateDetail != -1)
    {
      result = this.deploySpecificDateDetail;
    }
    return result;
  }
  
  public void setSearchCriteria(String searchCriteria)
  {
    try
    {
      this.searchCriteria = Integer.parseInt(searchCriteria); 
    }
    catch(Exception e)
    {
      this.searchCriteria = -1;
    }
  }
  public int getSearchCriteria()
  {
    return this.searchCriteria;
  }

  public void setSubmit(String submit)
  {
    this.submit = true;
  }
  public boolean isSubmitted()
  {
    return this.submit;
  }

  public boolean getHasErrors()
  {
    return this.hasErrors;
  }

  public Vector getErrors()
  {
    return errors;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum.trim();
  }
  public String getSerialNum()
  {
    return this.serialNum;
  }

  public void setMerchantNum(String merchantNum)
  {
    try
    {
      long temp = Long.parseLong(merchantNum.trim());
      this.merchantNum = merchantNum.trim();
    }
    catch(Exception e)
    {
      this.merchantNum = "-1";
    }
  }

  public void setReferenceNum(String referenceNumber)
  {
    try
    {
      long temp = Long.parseLong(referenceNumber.trim());
      this.referenceNumber = referenceNumber.trim();
    }
    catch(Exception e)
    {
      this.referenceNumber = "-1";
    }
  }

  public String getReferenceNum()
  {
    String result = "";
    if(!this.referenceNumber.equals("-1"))
    {
      result = this.referenceNumber;
    }
    return result;
  }

  public void setProductName(String productName)
  {
    try
    {
      this.productName = Integer.parseInt(productName); 
    }
    catch(Exception e)
    {
      this.productName = -1;
    }
  }
  public void setProductType(String productType)
  {
    try
    {
      this.productType = Integer.parseInt(productType); 
    }
    catch(Exception e)
    {
      this.productType = -1;
    }
  }
  public void setProductDesc(String productDesc)
  {
    this.productDesc = productDesc.trim();  
  }

  public int getProductName()
  {
    return this.productName;
  }
  public int getProductType()
  {
    return this.productType;
  }
  public String getProductDesc()
  {
    return this.productDesc;
  }
  public String getMerchantNum()
  {
    String result = "";
    if(!this.merchantNum.equals("-1"))
    {
      result = this.merchantNum;
    }
    return result;
  }

  public void setProductCond(String productCond)
  {
    try
    {
      //@ System.out.println("in condition");
      
      this.productCond = Integer.parseInt(productCond); 
    }
    catch(Exception e)
    {
      System.out.println("condition crashed");
      this.productCond = -1;
    }
  }

  public int getProductCond()
  {
    return this.productCond;
  }
  public void setProductLrb(String productLrb)
  {
    try
    {
      //@ System.out.println("in lrb");
      
      this.productLrb = Integer.parseInt(productLrb); 
    }
    catch(Exception e)
    {
      //@ System.out.println("lrb crashed");
      this.productLrb = -1;
    }
  }
  public int getProductLrb()
  {
    return this.productLrb;
  }

  public void setProductStatus(String productStatus)
  {
    try
    {
      //@ System.out.println("in lrb");
      
      this.productStatus = Integer.parseInt(productStatus); 
    }
    catch(Exception e)
    {
      //@ System.out.println("lrb crashed");
      this.productStatus = -1;
    }
  }

  public int getProductStatus()
  {
    return this.productStatus;
  }

  public void setProductOwner(String productOwner)
  {
    try
    {
      //@ System.out.println("in owner");
      
      this.productOwner = Integer.parseInt(productOwner); 
    }
    catch(Exception e)
    {
      //@ System.out.println("owner crashed");
      this.productOwner = -1;
    }
  }

  public int getProductOwner()
  {
    return this.productOwner;
  }


  public boolean isEdit()
  {
    return this.edit;
  }
  public void setEdit(String temp)
  {
    this.edit = true;
  }
  
  public String encode(String url)
  {
    String body = url;
    try
    {
      StringBuffer    bodyBuff = null;
      int             index = 0;
      
      index = body.indexOf(' ');
      
      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"%20");
        body = bodyBuff.toString();
        index = body.indexOf(' ', index);
      }
    }
    catch(Exception e)
    {
      //@ System.out.println("error will robinson %20");
    }
    return body;

  }

}
