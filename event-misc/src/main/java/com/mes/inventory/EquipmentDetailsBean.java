/*@lineinfo:filename=EquipmentDetailsBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/EquipmentDetailsBean.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/23/04 4:56p $
  Version            : $Revision: 22 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.inventory;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class EquipmentDetailsBean extends com.mes.ops.InventoryBase
{
  private String   referenceNum                        = "";
  private String   serialNum                           = "";
  private String   serialNumNew                        = "";
  
  private String   unitCost                            = "";
  private String   unitCostNew                         = "";

  private String   shipCost                            = "";
  private String   shipCostNew                         = "";
  
  private Date     receiveDate                         = null;
  private Date     receiveDateNew                      = null;
  
  private String   classDesc                           = "";
  private int      classType                           = -1;
  private int      classTypeNew                        = -1;
  
  private String   statusDesc                          = "";
  private int      statusType                          = -1;
  private int      statusTypeNew                       = -1;

  private String   ownerDesc                           = "";
  private int      ownerType                           = -1;
  private int      ownerTypeNew                        = -1;
  
  private String   partNum                             = "";
  private String   partNumNew                          = "";


  private String   productName                         = "";
  private String   productDesc                         = "";
  private String   productType                         = "";
  
  private String   changeNote                          = "";

  private int      lrbCode                             = 0;
  private String   lrbDesc                             = "";
  private double   unitPrice                           = 0.0;
  private String   merchantNum                         = "";
  private String   merchantDba                         = "";
  private String   primaryKey                          = "";

  private Date     deployDate                          = null;
  
  public  Vector   historyRecs                         = new Vector();
  
  public  Vector   statuses                            = new Vector();
  public  Vector   statusCode                          = new Vector();  

//@  public  Vector   owner                               = new Vector();
//@  public  Vector   ownerCode                           = new Vector();  

  public  Vector   classes                             = new Vector();
  public  Vector   classesCode                         = new Vector();  

  public  Vector   models                              = new Vector();
  public  Vector   modelDesc                           = new Vector();  


  public  HashMap  classMap                            = new HashMap();
  public  HashMap  statusMap                           = new HashMap();
//@  public  HashMap  ownerMap                            = new HashMap();
//@  public  HashMap  modelMap                            = new HashMap();

  public class HistoryRec
  {
    private String Note       = "";
    private String NoteStatus = "";
    private String NoteDate   = "";
    private String NoteTime   = "";
    private String NoteUser   = "";
    
    public HistoryRec( ResultSet resultSet )
      throws java.sql.SQLException
    {
      Note         = resultSet.getString("eh_note");
      NoteStatus   = resultSet.getString("eh_action_performed");
      NoteDate     = DateTimeFormatter.getFormattedDate( resultSet.getTimestamp("eh_date"), "MM/dd/yyyy" );
      NoteTime     = DateTimeFormatter.getFormattedDate( resultSet.getTimestamp("eh_date"), "hh:mm:ss" );
      NoteUser     = resultSet.getString("login_name");
    }

    public void setNote(String note)
    {
      Note = note;
    }
    public void setNoteStatus(String noteStatus)
    {
      NoteStatus = noteStatus;
    }
    public void setNoteDate(String noteDate)
    {
      NoteDate = noteDate;
    }
    public void setNoteTime(String noteTime)
    {
      NoteTime = noteTime;
    }
    public void setNoteUser(String noteUser)
    {
      NoteUser = noteUser;
    }

    public String getNote()
    {
      return( Note );
    }
    public String getNoteStatus()
    {
      return( NoteStatus );
    }
    public String getNoteDate()
    {
      return( NoteDate );
    }
    public String getNoteTime()
    {
      return( NoteTime );
    }
    public String getNoteUser()
    {
      return( NoteUser );
    }
  }
    
  public EquipmentDetailsBean()
  {
  }
  
  public void initialize( )
  {
    ResultSetIterator       it  = null;
    ResultSet               rs  = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:177^7*/

//  ************************************************************
//  #sql [Ctx] it = { select * 
//          from equip_class
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select * \n        from equip_class";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.inventory.EquipmentDetailsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:181^7*/
      rs = it.getResultSet();
      
      while(rs.next())
      {
        classMap.put(rs.getString("ec_class_id"),rs.getString("ec_class_name"));
        classes.add(rs.getString("ec_class_name"));
        classesCode.add(rs.getString("ec_class_id"));  
      }
      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:193^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    equip_status_id,
//                    equip_status_desc
//          from      equip_status 
//          order by  equip_status_order
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    equip_status_id,\n                  equip_status_desc\n        from      equip_status \n        order by  equip_status_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.inventory.EquipmentDetailsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:199^7*/
      rs = it.getResultSet();
      
      while(rs.next())
      {
        statusMap.put(rs.getString("equip_status_id"),rs.getString("equip_status_desc"));
        statuses.add(rs.getString("equip_status_desc"));
        statusCode.add(rs.getString("equip_status_id"));  
      }
      rs.close();
      it.close();
   
      /*@lineinfo:generated-code*//*@lineinfo:211^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_model,
//                  equip_descriptor,
//                  equiptype_code
//          from    equipment
//          where   used_in_inventory = 'Y'
//          order by equiptype_code,equip_descriptor
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_model,\n                equip_descriptor,\n                equiptype_code\n        from    equipment\n        where   used_in_inventory = 'Y'\n        order by equiptype_code,equip_descriptor";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.inventory.EquipmentDetailsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:219^7*/
      rs = it.getResultSet();
     
      boolean firstterm         = true;
      boolean firstprinter      = true;
      boolean firstpin          = true;
      boolean firstimprinter    = true;
      boolean firsttermprinter  = true;
      boolean firsttermprintpin = true;
      boolean firstperiph       = true;
      boolean firstaccessory    = true;

      while(rs.next())
      {
//@        modelMap.put(rs.getString("equip_model"),rs.getString("equip_descriptor"));

        switch(rs.getInt("equiptype_code"))
        {
          case mesConstants.APP_EQUIP_TYPE_TERMINAL:
            if(firstterm)
            {
              modelDesc.add("****TERMINALS****");
              models.add("*");
              firstterm = false;
            }
            modelDesc.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;
            
          case mesConstants.APP_EQUIP_TYPE_PRINTER:
            if(firstprinter)
            {
              modelDesc.add("****PRINTERS****");
              models.add("*");
              firstprinter = false;
            }
            modelDesc.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;
            
          case mesConstants.APP_EQUIP_TYPE_PINPAD:
            if(firstpin)
            {
              modelDesc.add("****PINPADS****");
              models.add("*");
              firstpin = false;
            }
            modelDesc.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;
            
          case mesConstants.APP_EQUIP_TYPE_IMPRINTER:
            if(firstimprinter)
            {
              modelDesc.add("****IMPRINTERS****");
              models.add("*");
              firstimprinter = false;
            }
            modelDesc.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;
            
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINTER:
            if(firsttermprinter)
            {
              modelDesc.add("****TERMINAL/PRINTERS****");
              models.add("*");
              firsttermprinter = false;
            }
            modelDesc.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;
            
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD:
            if(firsttermprintpin)
            {
              modelDesc.add("****TERMINAL/PRINTER/PINPADS****");
              models.add("*");
              firsttermprintpin = false;
            }
            modelDesc.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;
            
          case mesConstants.APP_EQUIP_TYPE_PERIPHERAL:
            if(firstperiph)
            {
              modelDesc.add("****PERIPHERALS****");
              models.add("*");
              firstperiph = false;
            }
            modelDesc.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;

          case mesConstants.APP_EQUIP_TYPE_ACCESSORIES:
            if(firstaccessory)
            {
              modelDesc.add("****ACCESSORIES****");
              models.add("*");
              firstaccessory = false;
            }
            modelDesc.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
            break;
        }  
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("initialize()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  public void getData()
  {
    ResultSetIterator     it                = null;
    ResultSet             rs                = null;
    String                selectSerialNum   = null;
    boolean               wasStale          = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      //if serial number was changed... we must bring up record using new serial number
      if(isBlank(this.serialNumNew) || hasErrors())
      {
        selectSerialNum = this.serialNum;
      }
      else
      {
        selectSerialNum = this.serialNumNew;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:364^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.*,
//                  stat.equip_status_desc, 
//                  at.inventory_owner_name, 
//                  class.ec_class_name, 
//                  equip.equip_descriptor, 
//                  type.equiptype_description, 
//                  mfgr.equipmfgr_mfr_name, 
//                  lend.equiplendtype_description 
//          from    equip_inventory     ei, 
//                  equipment           equip, 
//                  equiptype           type, 
//                  equipmfgr           mfgr, 
//                  equip_class         class, 
//                  equiplendtype       lend, 
//                  equip_status        stat, 
//                  app_type            at 
//          where   ei.ei_part_number         = :partNum and
//                  ei.ei_serial_number       = :selectSerialNum and 
//                  ei_part_number            = equip.equip_model and 
//                  equip.equipmfgr_mfr_code  = mfgr.equipmfgr_mfr_code and 
//                  equip.equiptype_code      = type.equiptype_code and 
//                  lend.equiplendtype_code   = ei.ei_lrb and 
//                  stat.equip_status_id      = ei.ei_status and 
//                  ei.ei_class               = class.ec_class_id and 
//                  ei.ei_owner               = at.app_type_code 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.*,\n                stat.equip_status_desc, \n                at.inventory_owner_name, \n                class.ec_class_name, \n                equip.equip_descriptor, \n                type.equiptype_description, \n                mfgr.equipmfgr_mfr_name, \n                lend.equiplendtype_description \n        from    equip_inventory     ei, \n                equipment           equip, \n                equiptype           type, \n                equipmfgr           mfgr, \n                equip_class         class, \n                equiplendtype       lend, \n                equip_status        stat, \n                app_type            at \n        where   ei.ei_part_number         =  :1  and\n                ei.ei_serial_number       =  :2  and \n                ei_part_number            = equip.equip_model and \n                equip.equipmfgr_mfr_code  = mfgr.equipmfgr_mfr_code and \n                equip.equiptype_code      = type.equiptype_code and \n                lend.equiplendtype_code   = ei.ei_lrb and \n                stat.equip_status_id      = ei.ei_status and \n                ei.ei_class               = class.ec_class_id and \n                ei.ei_owner               = at.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,partNum);
   __sJT_st.setString(2,selectSerialNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.inventory.EquipmentDetailsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:391^7*/
      rs = it.getResultSet();
      
      if(rs.next())
      {
        this.serialNum       = rs.getString("ei_serial_number");
        this.unitCost        = isBlank(rs.getString("ei_unit_cost"))              ? "" : rs.getString("ei_unit_cost");
        this.shipCost        = isBlank(rs.getString("ei_shipping_cost"))          ? "" : rs.getString("ei_shipping_cost");
        this.statusType      = rs.getInt("ei_status");
        this.ownerType       = rs.getInt("ei_owner");
        this.classType       = rs.getInt("ei_class");
        this.receiveDate     = rs.getDate("ei_received_date");
        this.partNum         = rs.getString("ei_part_number");
        
        //if not submitted, display info from db.. (not sure why we do this)
        if(!isSubmitted()) 
        {
          this.serialNumNew    = this.serialNum;
          this.unitCostNew     = this.unitCost;
          this.shipCostNew     = this.shipCost;
          this.statusTypeNew   = this.statusType;
          this.ownerTypeNew    = this.ownerType;
          this.classTypeNew    = this.classType;
          this.receiveDateNew  = this.receiveDate;
          this.partNumNew      = this.partNum;
        }
        
        this.classDesc       = isBlank(rs.getString("ec_class_name"))             ? "" : rs.getString("ec_class_name");
        this.statusDesc      = isBlank(rs.getString("equip_status_desc"))         ? "" : rs.getString("equip_status_desc");
        this.ownerDesc       = isBlank(rs.getString("inventory_owner_name"))      ? "" : rs.getString("inventory_owner_name");
        this.referenceNum    = isBlank(rs.getString("ei_transaction_id"))         ? "" : rs.getString("ei_transaction_id");
        this.lrbDesc         = isBlank(rs.getString("equiplendtype_description")) ? "" : rs.getString("equiplendtype_description");
        this.productName     = isBlank(rs.getString("equipmfgr_mfr_name"))        ? "" : rs.getString("equipmfgr_mfr_name");
        this.productDesc     = isBlank(rs.getString("equip_descriptor"))          ? "" : rs.getString("equip_descriptor");
        this.productType     = isBlank(rs.getString("equiptype_description"))     ? "" : rs.getString("equiptype_description");
        this.merchantNum     = isBlank(rs.getString("ei_merchant_number"))        ? "" : rs.getString("ei_merchant_number");
        this.deployDate      = rs.getDate("ei_deployed_date");
        this.lrbCode         = rs.getInt("ei_lrb");
        this.unitPrice       = rs.getDouble("ei_lrb_price");
      }    
      rs.close();  
      it.close();
      
      //get merchant history
      /*@lineinfo:generated-code*//*@lineinfo:435^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  eh.*,
//                  u.login_name
//          from    equip_history   eh,
//                  users           u
//          where   eh.part_number = :partNum and
//                  eh.eh_serial_number = :serialNum and
//                  u.user_id = eh.eh_user
//          order by eh.eh_date desc                                 
//                          
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  eh.*,\n                u.login_name\n        from    equip_history   eh,\n                users           u\n        where   eh.part_number =  :1  and\n                eh.eh_serial_number =  :2  and\n                u.user_id = eh.eh_user\n        order by eh.eh_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,partNum);
   __sJT_st.setString(2,serialNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.inventory.EquipmentDetailsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:446^7*/
      rs = it.getResultSet();
  
      while(rs.next())
      {
        historyRecs.add( new HistoryRec(rs) );
      }    
      rs.close();  
      it.close();

      //get deployed info
      if(!isBlank(merchantNum))
      {
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:461^11*/

//  ************************************************************
//  #sql [Ctx] { select  merch_business_name
//              
//              from    merchant
//              where   merch_number = :merchantNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_business_name\n             \n            from    merchant\n            where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchantNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantDba = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:467^11*/
        }
        catch(Exception e)
        {
          /*@lineinfo:generated-code*//*@lineinfo:471^11*/

//  ************************************************************
//  #sql [Ctx] { select  dba_name
//              
//              from    mif
//              where   merchant_number = :merchantNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  dba_name\n             \n            from    mif\n            where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchantNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantDba = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:477^11*/
        }
      }
    }  
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
      addError("getData: " + e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
  }  
  

  public void updateData(HttpServletRequest aReq)
  {
  }
  
  public boolean validate()
  {
    if(isBlank(this.partNumNew) || this.partNumNew.equals("*"))
    {
      addError("Please select a Product Description for this item.");
    }

    if(isBlank(this.serialNumNew))
    {
      addError("Please provide a serial number for this item.");
    }
    else if(!this.serialNum.equals(this.serialNumNew) && doesExist(this.partNumNew, this.serialNumNew))
    {
      addError("Part number " + this.partNumNew + " with serial number " + this.serialNumNew + " already exists in database");
    }

    try
    {
      double tempd = Double.parseDouble(this.unitCostNew);
    }
    catch(Exception e)
    {
      addError("Please provide a valid unit cost.");
    }

    try
    {
      double tempd = Double.parseDouble(this.shipCostNew);
    }
    catch(Exception e)
    {
      addError("Please provide a valid shipping cost.");
    }

    if(this.classTypeNew == -1)
    {
      addError("Please select a product condition.");
    }
    if(this.statusTypeNew == -1)
    {
      addError("Please select a status.");
    }
    if(this.ownerTypeNew == -1)
    {
      addError("Please select an owner of this item.");
    }
    if(receiveDateNew == null)
    {
      addError("Please provide a valid received date.");
    }
    if(isBlank(this.changeNote))
    {
      addError("Must add Note/Location to describe change made or if adding general note.");
    }
    else if(this.changeNote.length() > 250)
    {
      addError("Note/Location must be less than 200 characters.  Currently it is " + this.changeNote.length() + " characters long.");
    }
    return(!hasErrors());
  }

  public void submitData(long userId)
  {
    updateInventory(userId);
  }

  private void updateInventory(long userId)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:570^7*/

//  ************************************************************
//  #sql [Ctx] { update equip_inventory 
//          set   ei_unit_cost           = :this.unitCostNew,
//                ei_class               = :this.classTypeNew,
//                ei_received_date       = :this.receiveDateNew,
//                ei_status              = :this.statusTypeNew,
//                ei_serial_number       = :this.serialNumNew,
//                ei_part_number         = :this.partNumNew,
//                ei_shipping_cost       = :this.shipCostNew,
//                ei_owner               = :this.ownerTypeNew 
//          where ei_part_number    = :this.partNum and
//                ei_serial_number  = :this.serialNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update equip_inventory \n        set   ei_unit_cost           =  :1 ,\n              ei_class               =  :2 ,\n              ei_received_date       =  :3 ,\n              ei_status              =  :4 ,\n              ei_serial_number       =  :5 ,\n              ei_part_number         =  :6 ,\n              ei_shipping_cost       =  :7 ,\n              ei_owner               =  :8  \n        where ei_part_number    =  :9  and\n              ei_serial_number  =  :10";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.unitCostNew);
   __sJT_st.setInt(2,this.classTypeNew);
   __sJT_st.setDate(3,this.receiveDateNew);
   __sJT_st.setInt(4,this.statusTypeNew);
   __sJT_st.setString(5,this.serialNumNew);
   __sJT_st.setString(6,this.partNumNew);
   __sJT_st.setString(7,this.shipCostNew);
   __sJT_st.setInt(8,this.ownerTypeNew);
   __sJT_st.setString(9,this.partNum);
   __sJT_st.setString(10,this.serialNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:583^7*/
      
      // if serial number or the part number changed then
      // it is necessary to update the transaction tables
      // associated with this entry in the database.
      if( (!this.serialNum.equals(this.serialNumNew)) ||
          (!this.partNum.equals(this.partNumNew)) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:591^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_history
//            set     eh_serial_number = :this.serialNumNew,
//                    part_number      = :this.partNumNew
//            where   part_number = :this.partNum and
//                    eh_serial_number = :this.serialNum                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_history\n          set     eh_serial_number =  :1 ,\n                  part_number      =  :2 \n          where   part_number =  :3  and\n                  eh_serial_number =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.serialNumNew);
   __sJT_st.setString(2,this.partNumNew);
   __sJT_st.setString(3,this.partNum);
   __sJT_st.setString(4,this.serialNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:598^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:600^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_transaction
//            set     et_serial_number = :this.serialNumNew,
//                    et_part_number      = :this.partNumNew
//            where   et_part_number = :this.partNum and
//                    et_serial_number = :this.serialNum                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_transaction\n          set     et_serial_number =  :1 ,\n                  et_part_number      =  :2 \n          where   et_part_number =  :3  and\n                  et_serial_number =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.serialNumNew);
   __sJT_st.setString(2,this.partNumNew);
   __sJT_st.setString(3,this.partNum);
   __sJT_st.setString(4,this.serialNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:607^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:609^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_swap
//            set     serial_num_in   = :this.serialNumNew,
//                    part_number_in  = :this.partNumNew
//            where   part_number_in  = :this.partNum and
//                    serial_num_in   = :this.serialNum                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_swap\n          set     serial_num_in   =  :1 ,\n                  part_number_in  =  :2 \n          where   part_number_in  =  :3  and\n                  serial_num_in   =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.serialNumNew);
   __sJT_st.setString(2,this.partNumNew);
   __sJT_st.setString(3,this.partNum);
   __sJT_st.setString(4,this.serialNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:616^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:618^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_swap
//            set     serial_num_out  = :this.serialNumNew,
//                    part_number_out = :this.partNumNew
//            where   part_number_out = :this.partNum and
//                    serial_num_out  = :this.serialNum                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_swap\n          set     serial_num_out  =  :1 ,\n                  part_number_out =  :2 \n          where   part_number_out =  :3  and\n                  serial_num_out  =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.serialNumNew);
   __sJT_st.setString(2,this.partNumNew);
   __sJT_st.setString(3,this.partNum);
   __sJT_st.setString(4,this.serialNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:625^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:627^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//            set     serial_num_in = :this.serialNumNew,
//                    part_number   = :this.partNumNew
//            where   part_number   = :this.partNum and
//                    serial_num_in = :this.serialNum                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n          set     serial_num_in =  :1 ,\n                  part_number   =  :2 \n          where   part_number   =  :3  and\n                  serial_num_in =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.serialNumNew);
   __sJT_st.setString(2,this.partNumNew);
   __sJT_st.setString(3,this.partNum);
   __sJT_st.setString(4,this.serialNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:634^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:636^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_exchange
//            set     serial_num_out  = :this.serialNumNew,
//                    part_number     = :this.partNumNew
//            where   part_number     = :this.partNum and
//                    serial_num_out  = :this.serialNum                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_exchange\n          set     serial_num_out  =  :1 ,\n                  part_number     =  :2 \n          where   part_number     =  :3  and\n                  serial_num_out  =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.serialNumNew);
   __sJT_st.setString(2,this.partNumNew);
   __sJT_st.setString(3,this.partNum);
   __sJT_st.setString(4,this.serialNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:643^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:645^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_repair
//            set     serial_num    = :this.serialNumNew,
//                    part_num      = :this.partNumNew
//            where   part_num      = :this.partNum and
//                    serial_num    = :this.serialNum                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_repair\n          set     serial_num    =  :1 ,\n                  part_num      =  :2 \n          where   part_num      =  :3  and\n                  serial_num    =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.serialNumNew);
   __sJT_st.setString(2,this.partNumNew);
   __sJT_st.setString(3,this.partNum);
   __sJT_st.setString(4,this.serialNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:652^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:654^9*/

//  ************************************************************
//  #sql [Ctx] { update equip_merchant_tracking 
//            set ref_num_serial_num = :this.serialNumNew,
//                part_number = :this.partNumNew
//            where ref_num_serial_num = :this.serialNum and 
//                  part_number = :this.partNum and
//                  action in 
//                    ( :mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP,
//                      :mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL,
//                      :mesConstants.ACTION_CODE_DEPLOYMENT_SWAP,
//                      :mesConstants.ACTION_CODE_DEPLOYMENT_PINPAD_EXCHANGE )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update equip_merchant_tracking \n          set ref_num_serial_num =  :1 ,\n              part_number =  :2 \n          where ref_num_serial_num =  :3  and \n                part_number =  :4  and\n                action in \n                  (  :5 ,\n                     :6 ,\n                     :7 ,\n                     :8  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.serialNumNew);
   __sJT_st.setString(2,this.partNumNew);
   __sJT_st.setString(3,this.serialNum);
   __sJT_st.setString(4,this.partNum);
   __sJT_st.setInt(5,mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP);
   __sJT_st.setInt(6,mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL);
   __sJT_st.setInt(7,mesConstants.ACTION_CODE_DEPLOYMENT_SWAP);
   __sJT_st.setInt(8,mesConstants.ACTION_CODE_DEPLOYMENT_PINPAD_EXCHANGE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:666^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:668^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_merchant_tracking
//            set     ref_num_serial_num  = :this.serialNumNew,
//                    part_number         = :this.partNumNew
//            where   part_number = :this.partNum and
//                    ref_num_serial_num = :this.serialNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_merchant_tracking\n          set     ref_num_serial_num  =  :1 ,\n                  part_number         =  :2 \n          where   part_number =  :3  and\n                  ref_num_serial_num =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.inventory.EquipmentDetailsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.serialNumNew);
   __sJT_st.setString(2,this.partNumNew);
   __sJT_st.setString(3,this.partNum);
   __sJT_st.setString(4,this.serialNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:675^9*/
      }
      
      // commit these changes to the database
      /*@lineinfo:generated-code*//*@lineinfo:679^7*/

//  ************************************************************
//  #sql [Ctx] { commit;
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:682^7*/
      
      addToHistory( partNumNew, serialNumNew, userId, statusTypeNew, getActionList(), changeNote );
      
      // if note was successfully added we want to 
      // clear out the note from the input box so
      this.changeNote = "";
    }  
    catch(Exception e)
    {
      logEntry("updateInventory()", e.toString());
      addError("updateInventory: " + e.toString());
    }
  }
  
  private String getActionList()
  {
    String result = "";
    
    //these if statements must be in this order...   well serial num must come first atleast
    if(!serialNum.equals(serialNumNew))
    {
      result = "Serial # changed from " + serialNum + " to " + serialNumNew;    
    }

    if(!partNum.equals(partNumNew))
    {
      if(isBlank(result))
      {
       result = "Product Description changed from " + getEquipmentDesc(partNum) + " to " + getEquipmentDesc(partNumNew);
      }
      else
      {
       result +=  "\n" + "Product Description changed from " + getEquipmentDesc(partNum) + " to " + getEquipmentDesc(partNumNew);
      }
    }

    if(statusType != statusTypeNew)
    {
      if(isBlank(result))
      {
        result = "Status changed from " + getStatusDesc(statusType) + " to " + getStatusDesc(statusTypeNew);    
      }
       else
       {
         result +=  "\n" + "Status changed from " + getStatusDesc(statusType) + " to " + getStatusDesc(statusTypeNew);
       }
    }

    if(ownerType != ownerTypeNew)
    {
     if(isBlank(result))
     {
       result = "Owner changed from " + getOwnerDesc(ownerType) + " to " + getOwnerDesc(ownerTypeNew);    
     }
     else
     {
       result +=  "\n" + "Owner changed from " + getOwnerDesc(ownerType) + " to " + getOwnerDesc(ownerTypeNew);
     }
    }
    
    if(classType != classTypeNew)
    {
     if(isBlank(result))
     {
       result = "Product Condition changed from " + decodeClass(Integer.toString(classType)) + " to " + decodeClass(Integer.toString(classTypeNew));
     }
     else
     {
       result +=  "\n" + "Product Condition changed from " + decodeClass(Integer.toString(classType)) + " to " + decodeClass(Integer.toString(classTypeNew));
     }
    }


    if(!unitCost.equals(unitCostNew))
    {
     if(isBlank(result))
     {
       result = "Unit Cost changed from " + unitCost + " to " + unitCostNew;    
     }
     else
     {
       result +=  "\n" + "Unit Cost changed from " + unitCost + " to " + unitCostNew;    
     }
    }

    if(!shipCost.equals(shipCostNew))
    {
     if(isBlank(result))
     {
       result = "Shipping Cost changed from " + shipCost + " to " + shipCostNew;    
     }
     else
     {
       result +=  "\n" + "Shipping Cost changed from " + shipCost + " to " + shipCostNew;    
     }
    }


    if(!getReceiveDate().equals(getReceiveDateNew()))
    {
     if(isBlank(result))
     {
       result = "Receive Date changed from " + getReceiveDate() + " to " + getReceiveDateNew();    
     }
     else
     {
       result +=  "\n" + "Receive Date changed from " + getReceiveDate() + " to " + getReceiveDateNew();    
     }
    }

    if(isBlank(result))
    {
      result = "Miscellaneous Comment Added";
    }
    return result;  
  }

  private String decodeClass(String classCode)
  {
    String result = "";
    if(isBlank(classCode))
    {
      return "UNKNOWN";
    }
    
    try
    {
      result = (String)classMap.get(classCode);
    }
    catch(Exception e)
    {
    }
    
    if(isBlank(result))
    {
      return classCode;
    }
    return result;
  }

  public String getDeployDate()
  {
    String        result      = null;
    
    result = DateTimeFormatter.getFormattedDate(this.deployDate,"MM/dd/yy");
    
    if ( isBlank(result) )
    {
      result = "------";
    }
    return( result );
  }    

  public String getReceiveDate()
  {
    String        result      = null;
    
    result = DateTimeFormatter.getFormattedDate(this.receiveDate,"MM/dd/yy");
    
    if ( isBlank(result) )
    {
      result = "------";
    }
    return( result );
  }    

  public void setReceiveDate(String receiveDate)
  {
    try
    {
      this.receiveDate = new java.sql.Date( (DateTimeFormatter.parseDate(receiveDate,"MM/dd/yy")).getTime() );
    }
    catch( Exception e )
    {
      this.receiveDate = null;
    }      
  }

  public String getReceiveDateNew()
  {
    String      retVal      = DateTimeFormatter.getFormattedDate(this.receiveDateNew,"MM/dd/yy");
    
    if ( isBlank(retVal) )
    {
      retVal = "------";
    }
    return( retVal );
  }    

  public void setReceiveDateNew(String receiveDateNew)
  {
    try
    {
      this.receiveDateNew = new java.sql.Date( (DateTimeFormatter.parseDate(receiveDateNew,"MM/dd/yy")).getTime() );
    }
    catch( Exception e )
    {
      this.receiveDateNew = null;
    }      
  }

  public int getNumNotes()
  {
    return( this.historyRecs.size() );
  }
  
  public String getChangeNote()
  {
    return( this.changeNote );
  }
  
  public void setChangeNote(String changeNote)
  {
    this.changeNote = changeNote;
  }

  public String getReferenceNum()
  {
    return this.referenceNum;
  }

  public String getSerialNum()
  {
    return this.serialNum;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }

  public String getSerialNumNew()
  {
    return this.serialNumNew;
  }

  public void setSerialNumNew(String serialNumNew)
  {
    this.serialNumNew = serialNumNew;
  }

  public String getPartNum()
  {
    return this.partNum;
  }

  public void setPartNum(String partNum)
  {
    this.partNum = partNum;
  }

  public String getPartNumNew()
  {
    return this.partNumNew;
  }

  public void setPartNumNew(String partNumNew)
  {
    this.partNumNew = partNumNew;
  }

  public String getUnitCost()
  {
    return this.unitCost;
  }
  
  public void setUnitCost(String unitCost)
  {
    this.unitCost = unitCost;
  }

  public String getUnitCostNew()
  {
    return this.unitCostNew;
  }
  
  public void setUnitCostNew(String unitCostNew)
  {
    this.unitCostNew = unitCostNew;
  }

  public String getShipCost()
  {
    return this.shipCost;
  }
  
  public void setShipCost(String shipCost)
  {
    this.shipCost = shipCost;
  }

  public String getShipCostNew()
  {
    return this.shipCostNew;
  }
  
  public void setShipCostNew(String shipCostNew)
  {
    this.shipCostNew = shipCostNew;
  }


  public String getUnitPrice()
  {
    String result = MesMath.toCurrency(this.unitPrice);
    if(isBlank(result))
    {
      result = "---";
    }
    return( result );
  }
  
  public String getClassDesc()
  {
    return this.classDesc;
  }
  
  public int  getClassType()
  {
    return this.classType;
  }
  
  public void  setClassType(String classType)
  {
    try
    {
      this.classType = Integer.parseInt(classType);
    }
    catch(Exception e)
    {
      this.classType = -1;
    }
  }

  public int  getClassTypeNew()
  {
    return this.classTypeNew;
  }
  
  public void  setClassTypeNew(String classTypeNew)
  {
    try
    {
      this.classTypeNew = Integer.parseInt(classTypeNew);
    }
    catch(Exception e)
    {
      this.classTypeNew = -1;
    }
  }


  public String getLrbDesc()
  {
    String result = "-----";
    if(!isBlank(this.lrbDesc))
    {
      result = this.lrbDesc;
    }
    return result;
  }

  public String getStatusDesc()
  {
    String result = "-----";
    if(!isBlank(this.statusDesc))
    {
      result = this.statusDesc;
    }
    return result;
  }

  public String getOwnerDesc()
  {
    String result = "-----";
    if(!isBlank(this.ownerDesc))
    {
      result = this.ownerDesc;
    }
    return result;
  }
  
  public int getStatusType()
  {
    return this.statusType;
  }

  public int getOwnerType()
  {
    return this.ownerType;
  }
  
  public void  setStatusType(String statusType)
  {
    try
    {
      this.statusType = Integer.parseInt(statusType);
    }
    catch(Exception e)
    {
      this.statusType = -1;
    }
  }
  public void setOwnerType(String ownerType)
  {
    try
    {
      this.ownerType = Integer.parseInt(ownerType);
    }
    catch(Exception e)
    {
      this.ownerType = -1;
    }
  }
  
  public int getStatusTypeNew()
  {
    return this.statusTypeNew;
  }

  public int getOwnerTypeNew()
  {
    return this.ownerTypeNew;
  }
  
  public void  setStatusTypeNew(String statusTypeNew)
  {
    try
    {
      this.statusTypeNew = Integer.parseInt(statusTypeNew);
    }
    catch(Exception e)
    {
      this.statusTypeNew = -1;
    }
  }

  public void  setOwnerTypeNew(String ownerTypeNew)
  {
    try
    {
      this.ownerTypeNew = Integer.parseInt(ownerTypeNew);
    }
    catch(Exception e)
    {
      this.ownerTypeNew = -1;
    }
  }

  public String getMerchantNum()
  {
    String result = "";
    
    if(!isBlank(this.merchantNum))
    {
      result = this.merchantNum;
    }
    
    return result;
  }

  public String getMerchantDba()
  {
    String result = "-----";
    if(!isBlank(this.merchantDba))
    {
      result = this.merchantDba;
    }
    return result;
  }

  public String getPrimaryKey()
  {
    return this.primaryKey;
  }

  public String getProductName()
  {
    String result = "-----";
    if(!isBlank(this.productName))
    {
      result = this.productName;
    }
    return result;
  }
  public String getProductDesc()
  {
    String result = "-----";
    if(!isBlank(this.productDesc))
    {
      result = this.productDesc;
    }
    return result;
  }
  public String getProductType()
  {
    String result = "-----";
    if(!isBlank(this.productType))
    {
      result = this.productType;
    }
    return result;
  }
  public String getNote(int idx)
  {
    HistoryRec hrec = (HistoryRec)historyRecs.elementAt(idx);
    return(replaceNewLine(hrec.getNote()));
  }
  public String getNoteStatus(int idx)
  {
    HistoryRec hrec = (HistoryRec)historyRecs.elementAt(idx);
    return(replaceNewLine(hrec.getNoteStatus()));
  }
  public String getNoteDate(int idx)
  {
    HistoryRec hrec = (HistoryRec)historyRecs.elementAt(idx);
    return(hrec.getNoteDate());
  }
  public String getNoteTime(int idx)
  {
    HistoryRec hrec = (HistoryRec)historyRecs.elementAt(idx);
    return(hrec.getNoteTime());
  }
  public String getNoteUser(int idx)
  {
    HistoryRec hrec = (HistoryRec)historyRecs.elementAt(idx);
    return(hrec.getNoteUser());
  }
}/*@lineinfo:generated-code*/