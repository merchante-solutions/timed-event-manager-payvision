/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/inventory/EquipmentUpload.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 9/24/01 11:37a $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.inventory;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.LineNumberReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class EquipmentUpload extends com.mes.screens.SequenceDataBean
{


  private Vector      equipment       = new Vector();
  private FileWriter  errorWriter     = null;
  private FileWriter  okWriter        = null;

  public EquipmentUpload()
  {
  }


  public boolean submitData()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    PreparedStatement ps1     = null;
    ResultSet         rs      = null;
    boolean           result  = true;
    int               i       = 0;
    
    try
    {
      qs.setLength(0);
      qs.append("insert into equip_inventory (");
      qs.append("ei_part_number,  ");
      qs.append("ei_serial_number, ");
      qs.append("ei_received_date, ");
      qs.append("ei_invoice_number, ");
      qs.append("ei_unit_cost, ");
      qs.append("ei_class, ");
      qs.append("ei_original_class, ");
      qs.append("ei_status, ");
      qs.append("ei_lrb, ");
      qs.append("ei_deployed_date, ");
      qs.append("ei_merchant_number, ");
      qs.append("ei_transaction_id ");
      qs.append(") values(?,?,?,?,?,?,?,?,?,?,?,?)");    
    
      ps = getPreparedStatement(qs.toString());
      
      for(int x=0; x< this.equipment.size(); x++)
      {
        EquipItem tempRec =  (EquipItem)equipment.elementAt(x);
        
        ps.setString(1,tempRec.getPartNumber()); //part number
        ps.setString(2,tempRec.getSerialNumber()); //serial number
        
        if(!isBlank(tempRec.getReceivedDate()))
        {
          ps.setDate(3, new java.sql.Date(makeDate(tempRec.getReceivedDate()).getTime()));
        }
        else
        {
          ps.setNull(3,java.sql.Types.DATE );
        }

        //ps.setString(4,"19999991"); //invoice number
        ps.setString(4,tempRec.getInvoiceNumber()); //invoice number
        
        ps.setString(5,tempRec.getUnitCost()); //unit cost
        ps.setString(6,tempRec.getCondition()); //class
        ps.setString(7,tempRec.getCondition()); //original class
        ps.setString(8,tempRec.getStatus()); //status
        ps.setString(9,tempRec.getLrb()); //lrb
        
        if(!isBlank(tempRec.getDeployedDate()))
        {
          ps.setDate(10, new java.sql.Date(makeDate(tempRec.getDeployedDate()).getTime()));
        }
        else
        {
          ps.setNull(10,java.sql.Types.DATE );
        }
        
        ps.setString(11,tempRec.getMerchantNumber());//merchant number
        ps.setInt(12,0);          //transaction id

        ps.executeUpdate();
      }

      ps.close();
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
      addError("submitData: " + e.toString());
      result = false;
    }
    return result;
  }



  public boolean getData()  
  {
    String            lineInfo  = "";
    boolean           result    = false;

    try 
    {
      errorWriter = new FileWriter(new File("../error_report.txt"));
      okWriter    = new FileWriter(new File("../ok_report.txt"));

      LineNumberReader in  = new LineNumberReader(new FileReader(new File("../equipment.csv")));
      int i = 2;

      if(in != null)
      { 
        lineInfo = in.readLine();
        while(lineInfo != null)
        {
          if(parseLine(lineInfo, i))
          {
            //System.out.println(lineInfo); // prints error records
          }

          lineInfo = in.readLine();
          i++;
        }
      }
      in.close();
      errorWriter.close();
      okWriter.close();
      result = true;
      System.out.println("number of lines read " + i);
    }
    catch(Exception e)
    {
      //com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
      System.out.println("getData: " + e.toString());
    }
    return result;
  }
  
  private boolean parseLine(String line, int linenum)
  {
      EquipItem       equip     = null;
      boolean         feeError  = false;
      String[]        item      = new String[13];
      //StringTokenizer tok       = new StringTokenizer(line, ",");
      int             i         = 0;
      StringBuffer    tempBuff  = null;
      Vector          errorVec  = new Vector();

      //while (tok.hasMoreTokens()) 
      //{
        //item[i] = (tok.nextToken()).trim();
        //i++;
      //}
      boolean ignoreComma = false;
      tempBuff = new StringBuffer("");
      for(int l=0; l<line.length(); ++l)
      {

        if(line.charAt(l) == '"')
        {
          ignoreComma = !ignoreComma;
          continue;
        }

        if(line.charAt(l) == ',' && !ignoreComma)
        {
          item[i] = tempBuff.toString();
          i++;
          tempBuff = new StringBuffer("");
        }
        else
        {
          tempBuff.append(line.charAt(l));
        }
      }
      //System.out.println("i ========= " + i);
      item[i] = tempBuff.toString(); //must get last field after last ','



      if(i != 12)
      {
        feeError = true;
        //System.out.println("error occured at i != 12 line # " + linenum);
        errorVec.add("error occured at i != 12 line # " + linenum);
      }
      else
      {
        //item[0]  = ; //manufacturer
        //item[3]  = ; //vendor
        //item[12] = ; //merchant name dba

        item[1]  = getModel(item[1]); //model
        if(isBlank(item[1]))
        {
          feeError = true;
          //System.out.println("error occured at model line # " + linenum);
          errorVec.add("error occured at model line # " + linenum);
        }

        if(isBlank(item[2]))  //serial number
        {
          feeError = true;
          //System.out.println("error occured at serial number line # " + linenum);
          errorVec.add("error occured at serial number line # " + linenum);
        }

      
        item[4] = checkInvoiceDate(item[4]); //invoice date  return current date if not date

        if(isBlank(item[5])) //invoice number
        {
          item[5] = "";
        }

        item[6]  = getCost(item[6]); //cost

        item[7]  = getCondition(item[7]); //condition
        if(isBlank(item[7]))  //CONDITION
        {
          feeError = true;
          //System.out.println("error occured at condition line # " + linenum);
          errorVec.add("error occured at condition line # " + linenum);
        }

        item[8]  = getStatus(item[8], item[9]); //status   also send lc to see if deployed-lease
        if(isBlank(item[8]))
        {
          feeError = true;
          //System.out.println("error occured at model line # " + linenum);
          errorVec.add("error occured at status line # " + linenum);
        }

      
        if(isBlank(item[9])) //lc
        {
          item[9] = "";
        }
      
        String item10 = isBlank(item[10]) ? "" : checkDeployDate(item[10]); //deploy date  return "" if not date

        if(!isBlank(item[10]) && isBlank(item10))
        {
          feeError = true;
          //System.out.println("error occured at deploy date line # " + linenum);
          errorVec.add("error occured at deploy date line # " + linenum);
        }
        else
        {
          item[10] = item10;
        }
        
        if(isBlank(item[11])) //merchant #
        {
          item[11] = "";
          if(!isBlank(item[10]))
          {
            feeError = true;
            //System.out.println("error occured at merchant number line # " + linenum);
            errorVec.add("error occured at merchant number line # " + linenum);
          }
        }
        else
        {
          if(!feeError)
          {
            feeError = !checkNumber(item[11]); //if return true (is number) then feeError gets false
            if(feeError)
            {
              //System.out.println("error occurred cause merchnum not num line # " + linenum);
              errorVec.add("error occurred cause merchnum not num line # " + linenum);
            }
          }
        }

      }
    try
    {
      if(feeError)
      {
        // write to error report file
        errorWriter.write("Line # " + linenum + " ");
        errorWriter.write(line);
        errorWriter.write("\n");
        for(int w = 0; w<errorVec.size(); w++)
        {
          errorWriter.write((String)errorVec.elementAt(w) + "\n");
        }
        errorWriter.write("\n");
      }
      else
      {
        
        equip = new EquipItem();
        equip.setPartNumber(item[1]);
        equip.setSerialNumber(item[2]);
        equip.setReceivedDate(item[4]);
        equip.setInvoiceNumber(item[5]);
        equip.setUnitCost(item[6]);
        equip.setCondition(item[7]);
        equip.setStatus(item[8]);
        equip.setDeployedDate(item[10]);
        equip.setMerchantNumber(item[11]);

        if(isBlank(item[10]))
        {
          equip.setLrb("0");
          //equip.setStatus("0");
        }
        else
        {
          if(isBlank(item[9]))
          {
            equip.setLrb("6");  //set up unknown.. cause those people be IS dumb
          }
          else
          {
            equip.setLrb("5"); //set to lease if lc?
          }

          //equip.setStatus("2");
        }

        equipment.add(equip);
        
        // add to accessory class to be added to database
        int k = 0;
        okWriter.write("Line # " + linenum + "\n");
        
        while(k < 13)
        {
          switch(k)
          {
            case 0:
              okWriter.write("Manufacturer = ");
            break;
            case 1:
              okWriter.write("Model = ");
            break;
            case 2:
              okWriter.write("Serial # = ");
            break;
            case 3:
              okWriter.write("Vendor = ");
            break;
            case 4:
              okWriter.write("Invoice Date = ");
            break;
            case 5:
              okWriter.write("Invoice # = ");
            break;
            case 6:
              okWriter.write("Cost = ");
            break;
            case 7:
              okWriter.write("Condition = ");
            break;
            case 8:
              okWriter.write("Status = ");
            break;
            case 9:
              okWriter.write("LC = ");
            break;
            case 10:
              okWriter.write("Deploy Date = ");
            break;
            case 11:
              okWriter.write("Merchant # = ");
            break;
            case 12:
              okWriter.write("DBA = ");
            break;
          }
          okWriter.write(item[k]);
          okWriter.write("\n");
          k++;
        }
        okWriter.write("\n");
      }
    }
    catch(Exception e)
    {
      System.out.println(e.toString());
    }
     return feeError;
  }
 
  private String getCondition(String cond)
  {
    String result = "";
    if(isBlank(cond))
    {
      return "";
    }
    else
    {
      cond = cond.toUpperCase();
    }


    if(cond.equals("USED"))
    {
      result = "2";
    }
    else if(cond.equals("NEW"))
    {
      result = "1";
    }
    else if(cond.equals("REFIRB"))
    {
      result = "3";
    }
    else if(cond.equals("REFURB"))
    {
      result = "3";
    }
    return result;
  } 

  private boolean checkNumber(String raw)
  {
    boolean       result      = true;
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          //true
        }
        else
        {
          result = false;
        }
      }
    }
    catch(Exception e)
    {
      result = false;
    }
    
    return result;

  }
  private String getCost(String raw)
  {
    String        result      = "";
    StringBuffer  digits      = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == '.')
        {
          digits.append(raw.charAt(i));
        }
      }
      
      result = digits.toString();
    }
    catch(Exception e)
    {
      result = "";
    }
    
    return result;
  }

  private String checkDeployDate(String tempDate)
  {
    Date       aDate;
    DateFormat fmt    = DateFormat.getDateInstance(DateFormat.SHORT);
    
    try
    {
      aDate = fmt.parse(tempDate);
      
      try
      {
        String specificDate   = tempDate.replace('/','0');
        long tempLong         = Long.parseLong(specificDate.trim());
      }
      catch(Exception e)
      {
        tempDate = "";
      }
    }
    catch(ParseException e)
    {
      tempDate = "";
    }
    return tempDate;
  }


  private Date makeDate(String tempDate)
  {
    Date       aDate  = null;
    DateFormat fmt    = DateFormat.getDateInstance(DateFormat.SHORT);
    
    try
    {
      aDate = fmt.parse(tempDate);
    }
    catch(ParseException e)
    {
      aDate = null;
    }
    return aDate;
  }


  private String checkInvoiceDate(String tempDate)
  {
    Date       aDate;
    DateFormat fmt    = DateFormat.getDateInstance(DateFormat.SHORT);
    String     todayDate = "";
    
    try
    {
      Calendar   cal        = Calendar.getInstance();
      DateFormat df         = new SimpleDateFormat("MM/dd/yyyy");
                 todayDate  = df.format(cal.getTime());

      aDate = fmt.parse(tempDate);
      
      try
      {
        String specificDate   = tempDate.replace('/','0');
        long tempLong         = Long.parseLong(specificDate.trim());
      }
      catch(Exception e)
      {
        tempDate = todayDate;
      }
    }
    catch(ParseException e)
    {
      tempDate = todayDate;
    }
    return tempDate;
  }


  private String getModel(String model)
  {
    String result = "";
    
    if(isBlank(model))
    {
      return "";
    }
    else
    {
      model = model.toUpperCase();
    }

    if(model.equals("TRANZ 380/128K"))
    {
      result = "VFTRANZ380128k";
    }
    else if(model.equals("TRANZ 330"))
    {
      result = "VFTRANZ330";
    }
    else if(model.equals("ZON JR. XL"))
    {
      result = "VFZONXL";
    }
    else if(model.equals("TRANZ 380/256K"))
    {
      result = "VFTRANZ380";
    }
    else if(model.equals("TRANZ 380X2/256K"))
    {
      result = "VFTRANZ380X2";
    }
    else if(model.equals("TRANZ 460/128K"))
    {
      result = "VFTRANZ460IP";
    }
    else if(model.equals("TRANZ 460/256K"))
    {
      result = "VFTRANZ460IP128";
    }
    else if(model.equals("OMNI 470"))
    {
      result = "VFOMNI470IPPP";
    }
    else if(model.equals("OMNI 3200"))
    {
      result = "VFOMNI3200IP";
    }
    else if(model.equals("PIN PAD 1000"))
    {
      result = "VF1000";
    }
    else if(model.equals("PIN PAD 101"))
    {
      result = "VF101";
    }
    else if(model.equals("PIN PAD 201"))
    {
      result = "VF301";
    }
    else if(model.equals("PIN PAD 2000"))
    {
      result = "VF2000";
    }
    else if(model.equals("P-250"))
    {
      result = "VFP250";
    }
    else if(model.equals("P-900 PRINTER"))
    {
      result = "VFR900";
    }
    else if(model.equals("CR 600 CHECKREADER"))
    {
      result = "VFCR600";
    }
    else if(model.equals("S-8 PIN PAD"))
    {
      result = "HPS8";
    }
    else if(model.equals("S-8 PIN PAD - LONG CORD"))
    {
      result = "HPS8DL";
    }
    else if(model.equals("T7E"))
    {
      result = "HPT7EIP";
    }
    else if(model.equals("P8S PRINTER"))
    {
      result = "HPP8SP";
    }
    else if(model.equals("T7P"))
    {
      result = "HPT7PIP";
    }
    else if(model.equals("T7P-U"))
    {
      result = "HPT7PRIP";
    }
    else if(model.equals("T7PT"))
    {
      result = "HPT7PTIP";
    }
    else if(model.equals("T77-S"))
    {
      result = "HPT77S";
    }
    else if(model.equals("T77-T"))
    {
      result = "HPT77T";
    }
    else if(model.equals("T77-F"))
    {
      result = "HPT77R";
    }
    else if(model.equals("NURIT 3010 WIRELESS"))
    {
      result = "NRT3010";
    }
    else if(model.equals("NURIT 2085"))
    {
      result = "NRT2085";
    }
    else if(model.equals("NURIT 2085 +"))
    {
      result = "NRT2085";
    }
    else if(model.equals("NURTI2085 + U"))
    {
      result = "NRT2085";
    }
    else if(model.equals("NURIT 202"))
    {
      result = "";
    }
    else if(model.equals("TALENTO 2UD") || model.equals("TALENTO T-ONE (2UD)"))
    {
      result = "DSTT1";
    }
    else if(model.equals("TALENTO 2U") || model.equals("TALENTO T-ONE (2U)"))
    {
      result = "DSTT1";
    }
    else if(model.equals("ARTEMA"))
    {
      result = "DSART";
    }
    else if(model.equals("TALENTO - T PAD"))
    {
      result = "DSTPAD";
    }
    return result;
  }


  private String getStatus(String status, String leasecom)
  {
    String result = "";
    
    if(isBlank(status))
    {
      return "";
    }
    else
    {
      status = status.toUpperCase();
    }



    if(status.equals("RECEIVED") || status.equals("RECIEVED"))
    {
      result = "0";
    }
    else if(status.equals("ENCRYPTED"))
    {
      result = "3";
    }
    else if(status.equals("DEPLOYED"))
    {
      if(!isBlank(leasecom))
      {
        result = "5";
      }
      else
      {
        result = "6";
      }
    }
    else if(status.equals("HELP DESK"))
    {
      result = "9";
    }
    else if(status.equals("TRAINING"))
    {
      result = "10";
    }
    else if(status.equals("ON LOAN"))
    {
      result = "11";
    }
    else if(status.equals("VMS") || status.equals("VITAL"))
    {
      result = "16";
    }
    else if(status.equals("DEFECTIVE"))
    {
      result = "12";
    }
    else if(status.equals("MISSING"))
    {
      result = "13";
    }
    else if(status.equals("LOST"))
    {
      result = "14";
    }
    else if(status.equals("OBSOLETE"))
    {
      result = "4";
    }
    
    return result;
  }




  public class EquipItem
  {
    private String partNumber     = "";
    private String serialNumber   = "";
    private String receivedDate   = "";
    private String invoiceNumber  = "";
    private String unitCost       = "";
    private String condition      = "";
    private String status         = "";
    private String lrb            = "";
    private String deployedDate   = "";
    private String merchantNumber = "";

    EquipItem()
    {}

    public void setPartNumber(String partNumber)
    {
      this.partNumber = partNumber;  
    }
    public void setSerialNumber(String serialNumber)
    {
      this.serialNumber = serialNumber;      
    }
    public void setReceivedDate(String receivedDate)
    {
      this.receivedDate = receivedDate;  
    }
    public void setInvoiceNumber(String invoiceNumber)
    {
      this.invoiceNumber = invoiceNumber;
    }
    public void setUnitCost(String unitCost)
    {
      this.unitCost = unitCost;
    }
    public void setCondition(String condition)
    {
      this.condition = condition;
    }
    public void setStatus(String status)
    {
      this.status = status;      
    }
    public void setLrb(String lrb)
    {
      this.lrb = lrb;
    }
    public void setDeployedDate(String deployedDate)
    {
      this.deployedDate = deployedDate;
    }
    public void setMerchantNumber(String merchantNumber)
    {
      this.merchantNumber = merchantNumber;      
    }


    public String getPartNumber()
    {
      return this.partNumber;  
    }
    public String getSerialNumber()
    {
      return this.serialNumber;      
    }
    public String getReceivedDate()
    {
      return this.receivedDate;  
    }
    public String getInvoiceNumber()
    {
      return this.invoiceNumber;
    }
    public String getUnitCost()
    {
      return this.unitCost;
    }
    public String getCondition()
    {
      return this.condition;
    }
    public String getStatus()
    {
      return this.status;      
    }
    public String getLrb()
    {
      return this.lrb;
    }
    public String getDeployedDate()
    {
      return this.deployedDate;
    }
    public String getMerchantNumber()
    {
      return this.merchantNumber;      
    }
  }



}
