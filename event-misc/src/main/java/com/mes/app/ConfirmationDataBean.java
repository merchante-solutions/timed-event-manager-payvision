/*@lineinfo:filename=ConfirmationDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/ConfirmationDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/27/03 4:34p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app;

import java.sql.ResultSet;
import com.mes.ops.InsertCreditQueue;
import sqlj.runtime.ResultSetIterator;

public class ConfirmationDataBean extends AppDataBeanBase
{

  private String owner1FirstName   = "";
  private String owner1LastName    = "";
  private String owner1Title       = "";
  private String controlNum        = "";


  public ConfirmationDataBean()
  {
  }
  
  public void init()
  {
    try
    {
      super.init();
      storeData();
      loadData();
    }
    catch( Exception e )
    {
      logEntry("init()",e.toString());
    }
  }

  public void storeData()
  {
    try
    {
      InsertCreditQueue icq = new InsertCreditQueue();
      icq.addApp(AppSeqNum, AppUserBean.getUserId());
      
      markPageComplete();         // mark this page done in screen_progress
    }
    catch( Exception e )
    {
      logEntry(("storeData() app_seq_num = " + AppSeqNum + " userId = " + AppUserBean.getUserId()), e.toString());
    }
    finally
    {}
  }


  public void loadData()
  {
    ResultSetIterator             it        = null;
    ResultSet                     resultSet = null;
    
    try
    {

      // this is here just incase we want to get merchant info to display on confirmation page.. 
      // such as dba and control number
      /*@lineinfo:generated-code*//*@lineinfo:96^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  BUSOWNER_FIRST_NAME,
//                  BUSOWNER_LAST_NAME,
//                  BUSOWNER_TITLE
//  
//          from    BUSINESSOWNER
//                  
//          where   app_seq_num  = :AppSeqNum  and 
//                  BUSOWNER_NUM = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  BUSOWNER_FIRST_NAME,\n                BUSOWNER_LAST_NAME,\n                BUSOWNER_TITLE\n\n        from    BUSINESSOWNER\n                \n        where   app_seq_num  =  :1   and \n                BUSOWNER_NUM = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.ConfirmationDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.ConfirmationDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:106^7*/

      resultSet = it.getResultSet();

      if(resultSet.next())
      {
        this.owner1FirstName  = resultSet.getString("BUSOWNER_FIRST_NAME");
        this.owner1LastName   = resultSet.getString("BUSOWNER_LAST_NAME");
        this.owner1Title      = resultSet.getString("BUSOWNER_TITLE");
      }

      resultSet.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:120^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merc_cntrl_number
//  
//          from    merchant
//                  
//          where   app_seq_num  = :AppSeqNum  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merc_cntrl_number\n\n        from    merchant\n                \n        where   app_seq_num  =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.ConfirmationDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.ConfirmationDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:127^7*/

      resultSet = it.getResultSet();

      if(resultSet.next())
      {
        this.controlNum   = resultSet.getString("merc_cntrl_number");
      }

      resultSet.close();
      it.close();


    }
    catch( Exception e )
    {
      logEntry("loadData()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
 
  public String getOwner1FirstName()
  {
    return this.owner1FirstName;
  }
  public String getOwner1LastName()
  {
    return this.owner1LastName;
  }
  public String getOwner1Title()
  {
    return this.owner1Title;
  }
  public String getControlNum()
  {
    return this.controlNum;
  }

}/*@lineinfo:generated-code*/