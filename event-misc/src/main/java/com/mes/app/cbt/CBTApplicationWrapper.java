/*@lineinfo:filename=CBTApplicationWrapper*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.app.cbt;

import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.app.AppBase;
import com.mes.constants.mesConstants;
import com.mes.user.UserBean;

public class CBTApplicationWrapper extends AppBase
{
  static Logger log = Logger.getLogger(CBTApplicationWrapper.class);
  
  private Business  bus;
  private Equipment eq;
  
  public long  appSeqNum       = 0L;
  
  {
    appType = mesConstants.APP_TYPE_CBT_NEW;
    curScreenId = 1;
  }
  
  public CBTApplicationWrapper()
  {
    super();
  }
  
  protected void createNewApp()
  {
    super.createNewApp(null);
    
    try
    {
      appSeq.setAppSeqNum(fields.getField("appSeqNum").asLong());
      appSeqNum = fields.getField("appSeqNum").asLong();
      log.debug("appSeqNum = " + appSeqNum);
      appSeq.getFirstScreen().markAsComplete();
      appSeq.getLastScreen().markAsComplete();
    }
    catch(Exception e)
    {
      logEntry("createNewApp()", e.toString());
    }
  }
  
  public void initializeApp()
  {
    try
    {
      // initialize user object
      log.debug("creating new user bean");
      user = new UserBean();
      log.debug("force validating user");
      user.forceValidate("nbsc_conversion");
      
      // create fields
      log.debug("creating fields");
      createFields(null);
      
      log.debug("post handle request");
      super.postHandleRequest(null);
      
      log.debug("creating new app");
      createNewApp();
      
      // add Options
//      log.debug("creating options fields");
//      Options opt = new Options();
//      opt.initFields();
//      add(opt);
      
      // add Business
      log.debug("creating business fields");
      bus = new Business();
      bus.setAutoCommit(false);
      bus.initFields();
//      add(bus);

      log.debug("creating equipment fields");
      eq = new Equipment();
      eq.setAutoCommit(false);
      eq.initFields();
    }
    catch(Exception e)
    {
      logEntry("initializeApp()", e.toString());
    }
    finally
    {
    }
  }
  
  public void setBusinessFields(ResultSet rs)
  {
    try
    {
      // set fields in all beans
      bus.setFields(rs);
    }
    catch(Exception e)
    {
      logEntry("setBusinessFields()", e.toString());
    }
  }
  
  public void setEquipmentFields(ResultSet rs)
  {
    try
    {
      // set fields in all beans
      eq.setFields(rs);
      
      eq.setData("trainingType", "S");
    }
    catch(Exception e)
    {
      logEntry("setEquipnentFields()", e.toString());
    }
  }
  
  public boolean storeBusinessData()
  {
    boolean result = false;
    try
    {
      bus.setData("appSeqNum", Long.toString(appSeqNum));
      result = bus.storeData();
    }
    catch(Exception e)
    {
      logEntry("storeBusinessData()", e.toString());
    }
    
    return( result );
  }
  
  public boolean storeEquipmentData()
  {
    boolean result = false;
    try
    {
      eq.setData("appSeqNum", Long.toString(appSeqNum));
      result = eq.storeData();
    }
    catch(Exception e)
    {
      logEntry("storeEquipmentData()", e.toString());
    }
    
    return( result );
  }
  
  public void deleteApplication()
  {
    try
    {
      connect();
      
      log.debug("deleting application: " + appSeqNum);
      /*@lineinfo:generated-code*//*@lineinfo:164^7*/

//  ************************************************************
//  #sql [Ctx] { call delete_application(:appSeqNum)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN delete_application( :1 )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.cbt.CBTApplicationWrapper",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:167^7*/
    }
    catch(Exception e)
    {
      log.error(e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/