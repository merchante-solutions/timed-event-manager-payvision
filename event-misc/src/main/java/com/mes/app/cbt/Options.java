/*@lineinfo:filename=Options*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/cbt/Options.sqlj $

  Description:

  Options

  Application options page.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-06-04 14:50:12 -0700 (Wed, 04 Jun 2008) $
  Version            : $Revision: 14930 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.cbt;

import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.AppBase;
import com.mes.constants.MesHierarchy;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.ConstantField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Options extends AppBase
{
  /*************************************************************************
  **
  **   Initialize
  **
  **************************************************************************/

  {
    appType = 31;
    curScreenId = 1;
  }
  
  private String fixedStyle = "formLabelTextBold";

  /*************************************************************************
  **
  **   Drop Down Tables
  **
  **************************************************************************/

  /*************************************************************************
  **
  **   Radio Button Lists
  **
  **************************************************************************/

  /*************************************************************************
  **
  **   Validations
  **
  **************************************************************************/

  public class SicValidation extends SQLJConnectionBase
    implements Validation
  {
    public boolean validate(String fieldData)
    {
      if (fieldData == null || fieldData.length() == 0)
      {
        return true;
      }
      
      boolean isValid = false;
      
      try
      {
        connect();
        
        int intSic = -1;

        try
        {        
          intSic = Integer.parseInt(fieldData);
        }
        catch (Exception e) {}

        ResultSetIterator it = null;        
        /*@lineinfo:generated-code*//*@lineinfo:103^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sic_code
//            from    sic_codes
//            where   sic_code = :intSic
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sic_code\n          from    sic_codes\n          where   sic_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.cbt.Options",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,intSic);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.cbt.Options",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:108^9*/
        
        isValid = it.getResultSet().next();
      }
      catch (Exception e)
      {
        String func = this.getClass().getName() + "::validate(fieldData = " 
          + fieldData + ")";
        String desc = e.toString();
        System.out.println(func + ": " + desc);
        logEntry(func,desc);
      }
      finally
      {
        cleanUp();
      }
      
      return isValid;
    }
        
    public String getErrorText()
    {
      return "Invalid Sic Code";
    }
  }
  
  /*************************************************************************
  **
  **   Custom Fields
  **
  **************************************************************************/

  /*************************************************************************
  **
  **   Field Bean
  **
  **************************************************************************/

  /*
  ** public class AssocOptions
  **
  ** Loads association options, generates fields for selecting assoc nums,
  ** provided means of managing these fields.
  */
  public class AssocOptions
  {
    public class Category
    {
      public String name;
      public String type;
      public Hashtable assocHash = new Hashtable();
    }
  
    public class Association
    {
      public String desc;
      public String name;
      public String id;
      public String num;
      public String catType;
    }
  
    private Hashtable   catHash       = new Hashtable();
    private Hashtable   allAssocHash  = new Hashtable();
    
    private FieldGroup  gAssocOptions = new FieldGroup("gAssocOptions");
    private Field[]     assocFields   = null;
    private String[][]  catList       = null;
    private long        amId          = -1L;

    public AssocOptions()
    {
    }

    /*
    ** protected void loadAssocData()
    **
    ** Loads the assocation categories and numbers for the user.  The data is
    ** stored in Category and Association objects pointed to by catHash.
    */
    protected void loadAssocData()
    {
      ResultSetIterator it = null;
    
      try
      {
        connect();
        
        // determine an id to use for looking up assoc mappings
        long appSeqNum = fields.getField("appSeqNum").asLong();
        
        // first look for an am id that is already assigned to this app
        if (appSeqNum > 0L)
        {
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:204^13*/

//  ************************************************************
//  #sql [Ctx] { select  id
//                
//                from    am_app
//                where   app_seq_num = :appSeqNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  id\n               \n              from    am_app\n              where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.cbt.Options",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:210^13*/
          }
          catch (Exception ie) {}
        }
          
        // if this app has not had assoc mapping assigned 
        // yet then get it from the user's id
        if (amId == -1L)
        {
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:221^13*/

//  ************************************************************
//  #sql [Ctx] { select  id
//                
//                from    am_user
//                where   login_name = :user.getLoginName()
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2694 = user.getLoginName();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  id\n               \n              from    am_user\n              where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.cbt.Options",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_2694);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:227^13*/
          }
          catch(Exception ie) {}
        }
               
        // query database for assoc choices
        /*@lineinfo:generated-code*//*@lineinfo:233^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    ci.id         cat_id,
//                      ci.item_num   cat_type,
//                      ci.name       cat_name,
//                      ai.id         assoc_id,
//                      ai.name       assoc_name,
//                      ai.item_num   assoc_num,
//                      ai.item_num || ' - ' || ai.name || ' (' || gi.name || ')'
//                                    descriptor
//            from      am_item     ci,
//                      am_item     ai,
//                      am_item     gi,
//                      t_hierarchy h1,
//                      t_hierarchy h2,
//                      t_hierarchy h3
//            where     h1.ancestor = :amId
//                      and h1.entity_type = :MesHierarchy.ET_AM_GROUP -- 6
//                      and h1.descendent = h2.ancestor
//                      and h2.entity_type = :MesHierarchy.ET_AM_CATEGORY -- 7
//                      and h2.relation = 1
//                      and h2.descendent = h3.ancestor
//                      and h3.entity_type = :MesHierarchy.ET_AM_ASSOC -- 8
//                      and h3.ancestor = ci.id
//                      and h3.descendent = ai.id
//                      and h2.ancestor = gi.id
//            order by  ci.item_num, ai.item_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    ci.id         cat_id,\n                    ci.item_num   cat_type,\n                    ci.name       cat_name,\n                    ai.id         assoc_id,\n                    ai.name       assoc_name,\n                    ai.item_num   assoc_num,\n                    ai.item_num || ' - ' || ai.name || ' (' || gi.name || ')'\n                                  descriptor\n          from      am_item     ci,\n                    am_item     ai,\n                    am_item     gi,\n                    t_hierarchy h1,\n                    t_hierarchy h2,\n                    t_hierarchy h3\n          where     h1.ancestor =  :1 \n                    and h1.entity_type =  :2  -- 6\n                    and h1.descendent = h2.ancestor\n                    and h2.entity_type =  :3  -- 7\n                    and h2.relation = 1\n                    and h2.descendent = h3.ancestor\n                    and h3.entity_type =  :4  -- 8\n                    and h3.ancestor = ci.id\n                    and h3.descendent = ai.id\n                    and h2.ancestor = gi.id\n          order by  ci.item_num, ai.item_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.cbt.Options",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,amId);
   __sJT_st.setInt(2,MesHierarchy.ET_AM_GROUP);
   __sJT_st.setInt(3,MesHierarchy.ET_AM_CATEGORY);
   __sJT_st.setInt(4,MesHierarchy.ET_AM_ASSOC);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.cbt.Options",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:260^9*/
      
        Category cat = null;

        // store the assoc choices in catHash
        ResultSet rs = it.getResultSet();
        while (rs.next())
        {
          String catType    = rs.getString("cat_type");
          String catName    = rs.getString("cat_name");
          String assocId    = rs.getString("assoc_id");
          String assocName  = rs.getString("assoc_name");
          String assocNum   = rs.getString("assoc_num");
          String descriptor = rs.getString("descriptor");
        
          // make sure a category exists for this row's category
          cat = (Category)catHash.get(catType);
          if (cat == null)
          {
            cat = new Category();
            cat.name = catName;
            cat.type = catType;
            catHash.put(catType,cat);
          }
        
          // add this row's association to it's category
          Association assoc = (Association)cat.assocHash.get(assocId);
          if (assoc == null)
          {
            assoc = new Association();
            assoc.desc = descriptor;
            assoc.name = assocName;
            assoc.num = assocNum;
            assoc.id = assocId;
            assoc.catType = catType;
            cat.assocHash.put(assocNum,assoc);
            allAssocHash.put(assocNum,assoc);
          }
        }
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + "::loadAssocData(): "
          + e.toString());
        logEntry("loadAssocData",e.toString());
      }
      finally
      {
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
    
    /*
    ** protected FieldGroup getAssocFields()
    **
    ** Creates the association category radio buttons and all corresponding
    ** association number fields (drop downs, fixed fields and open fields).
    **
    ** RETURNS: FieldGroup containing the generated fields (named "gAssocOptions").
    */
    public FieldGroup getFieldGroup()
    {    
      try
      {
        // load association numbers and categories from database
        loadAssocData();
    
        // create radio button list sized to found categories
        catList = new String[catHash.values().size() + 1][2];
      
        // create field array sized to found categories
        assocFields = new Field[catHash.values().size() + 1];
      
        // create fields based on category/association data
        int catIdx = 0;
        TreeMap catTree = new TreeMap();
        catTree.putAll(catHash);
        for (Iterator i = catTree.values().iterator(); i.hasNext(); ++catIdx)
        {
          // get a category, populate the cat list elements
          Category cat = (Category)i.next();
          catList[catIdx][0] = cat.name;
          catList[catIdx][1] = cat.type;
        
          String choiceName = "assocChoice" + cat.type;
        
          // make a free entry field for if no assoc num in category
          if (cat.assocHash.size() == 0)
          {
            assocFields[catIdx] = new NumberField(choiceName,6,4,true,0);
          }
          // make a fixed field for single association value
          else if (cat.assocHash.size() == 1)
          {
            Association assoc
              = (Association)((cat.assocHash.values().toArray())[0]);
            assocFields[catIdx]
              = new ConstantField(choiceName,assoc.num,fixedStyle);
          }
          // make a drop down for multiple association values
          else
          {
            // create drop down table populated with category's assoc nums
            DropDownTable assocTable = new DropDownTable();
            assocTable.addElement("","--");
          
            // sort the associations by assoc num
            TreeMap assocTree = new TreeMap(cat.assocHash);
            for (Iterator j = assocTree.values().iterator(); j.hasNext();)
            {
              Association assoc = (Association)j.next();
              assocTable.addElement(assoc.num,assoc.desc);
            }
          
            // build drop down field with table
            Field assocField = new DropDownField(choiceName,assocTable,true);
            assocFields[catIdx] = assocField;
          }
        }
      
        // create a category for other/unknown
        catList[catIdx][0] = "Other";
        catList[catIdx][1] = "-1";
        assocFields[catIdx] = new NumberField("assocChoice-1",6,4,true,0,100000,999999);
      
        // create category radio button field using catList
        Field assocCats = new RadioButtonField("assocCats",catList,false,
          "An association category must be selected");
        gAssocOptions.add(assocCats);
      
        // add assoc selection fields, add validations
        for (int i = 0; i < assocFields.length; ++i)
        {
          // always require assoc num (even if unknown/other selected)
          assocFields[i].addValidation(
              new ConditionalRequiredValidation(
                new FieldValueCondition(assocCats,catList[i][1])));

          // if drop down field, make it requied if corresponding
          // radio option is selected
          //if (assocFields[i] instanceof DropDownField)
          //{
          //  assocFields[i].addValidation(
          //      new ConditionalRequiredValidation(
          //        new FieldValueCondition(assocCats,catList[i][1])));
          //}

          gAssocOptions.add(assocFields[i]);
        }
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + "::getAssocFields(): "
          + e.toString());
        logEntry("getAssocFields",e.toString());
      }
      
      return gAssocOptions;
    }
    
    public void resetFields()
    {
      // reset fields if the field assoc options field group has been created
      if (gAssocOptions != null)
      {
        // get the association category setting
        String assocCat = gAssocOptions.getData("assocCats");
        if (assocCat.length() > 0)
        {
          // scan through the category array
          for (int i = 0; i < catList.length; ++i)
          {
            // if the category is not selected, reset it's corresponding
            // association options field
            if (!catList[i][1].equals(assocCat))
            {
              gAssocOptions.setData("assocChoice" + catList[i][1],"");
            }
          }
        }
      }
    }
    
    public void setAssocNum(String assocNum) throws Exception
    {
      // if this screen has been completed or a non-null assocNum
      // exists then the assoc category can be set
      if (appSeq.getCurrentScreen().isComplete() || assocNum != null)
      {
        // if assoc num is not null then set the category
        // and corresponding assoc num field
        if (assocNum != null)
        {
          // look for the assoc num in one of the specific categories
          Association assoc = (Association)allAssocHash.get(assocNum);
          if (assoc != null)
          {
            // set the category and the category's assoc option 
            // field to the assoc num
            fields.setData("assocCats",assoc.catType);
            fields.setData("assocChoice" + assoc.catType,assocNum);
          }
          // set to category OTHER/unknown since the assoc num
          // doesn't correspond with a specific assoc num option
          else
          {
            fields.setData("assocCats","-1");
            fields.setData("assocChoice-1",assocNum);
          }
        }
        // set to category other/UNKNOWN since screen has been
        // completed but the assoc num is not specified
        else
        {
          fields.setData("assocCats","-1");
        }
      }
    }
    
    public long getAmId()
    {
      return amId;
    }
  }
  
  protected AssocOptions assocOptions;
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      // setup info
      FieldGroup gSetup = new FieldGroup("gSetup");
      
      gSetup.add(new Field      ("repCode",     "Rep Code",4,6,true));

      // these are now hidden due to the new cb&t review queues
      gSetup.add(new HiddenField("creditScore"));
      gSetup.add(new HiddenField("sicCode"));

      //gSetup.getField("sicCode").addValidation(new SicValidation());
      
      fields.add(gSetup);
      
      // create the association options handler
      assocOptions = new AssocOptions();
      
      // have the handler create the association option fields, add them
      fields.add(assocOptions.getFieldGroup());
      
      // set html extra
      fields.setHtmlExtra("class=\"formText\"");

      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }

  /*************************************************************************
  **
  **   Helper Methods
  **
  **************************************************************************/

  /*************************************************************************
  **
  **   Submit
  **
  **************************************************************************/

  /*
  ** protected void submitAmApp(long appSeqNum) throws Exception
  **
  ** Stores the association map id in am_app.
  */
  protected void submitAmApp(long appSeqNum) throws Exception
  {
    // don't bother assigning to nonexistent app
    if (appSeqNum > 0L)
    {
      try
      {
        // determine if an id is already assigned to this app
        int count = 0;
        /*@lineinfo:generated-code*//*@lineinfo:553^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//            
//            from    am_app
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n           \n          from    am_app\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.cbt.Options",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:559^9*/
      
        // if no id assigned, store the current one in assocOptions
        if (count == 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:564^11*/

//  ************************************************************
//  #sql [Ctx] { insert into am_app
//              ( app_seq_num,
//                id )
//              values
//              ( :appSeqNum,
//                :assocOptions.getAmId() )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_2695 = assocOptions.getAmId();
   String theSqlTS = "insert into am_app\n            ( app_seq_num,\n              id )\n            values\n            (  :1 ,\n               :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.app.cbt.Options",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,__sJT_2695);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:572^11*/
        }
      }
      catch (Exception e)
      {
        String methodSig = this.getClass().getName() + "::submitAmApp()";
        throw new Exception(methodSig + ": " + e.toString());
      }
    }
  }

  /*
  ** protected void submitMerchant(long appSeqNum) throws Exception
  **
  ** Stores association number in the merchant table.
  */
  protected void submitMerchant(long appSeqNum) throws Exception
  {
    try
    {
      // determine the association number
      String assocCat = fields.getData("assocCats");
      String assocNum = fields.getData("assocChoice" + assocCat);
      
      assocOptions.resetFields();
      
      /*@lineinfo:generated-code*//*@lineinfo:598^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     asso_number       = :assocNum,
//                  merch_rep_code    = :fields.getData("repCode"),
//                  app_sic_code      = :fields.getData("sicCode")
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2696 = fields.getData("repCode");
 String __sJT_2697 = fields.getData("sicCode");
   String theSqlTS = "update  merchant\n        set     asso_number       =  :1 ,\n                merch_rep_code    =  :2 ,\n                app_sic_code      =  :3 \n        where   app_seq_num =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.app.cbt.Options",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,assocNum);
   __sJT_st.setString(2,__sJT_2696);
   __sJT_st.setString(3,__sJT_2697);
   __sJT_st.setLong(4,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:605^7*/
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitMerchant()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  /*
  ** protected boolean submitAppData()
  **
  ** Causes all data from the page to be stored in the database.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean submitAppData()
  {
    boolean submitOk = false;
    try
    {
      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        connect();
        submitAmApp(appSeqNum);
        submitMerchant(appSeqNum);
      }
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitAppData()",e.toString());
      System.out.println(this.getClass().getName() + "::submitAppData(): "
        + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return submitOk;
  }

  /*************************************************************************
  **
  **   Load
  **
  **************************************************************************/

  /*
  ** protected void loadMerchant(long appSeqNum) throws Exception
  **
  ** Loads association number from merchant, sets fields based on association.
  */
  protected void loadMerchant(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    try
    {
      // load data from merchant
      /*@lineinfo:generated-code*//*@lineinfo:667^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  asso_number       assoc_num,
//                  merch_rep_code    rep_code,
//                  app_sic_code      sic_code,
//                  cbt_credit_score||' ('||
//                    to_char(cbt_credit_score_date, 'mm/dd/yy')||
//                    ')'             credit_score
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  asso_number       assoc_num,\n                merch_rep_code    rep_code,\n                app_sic_code      sic_code,\n                cbt_credit_score||' ('||\n                  to_char(cbt_credit_score_date, 'mm/dd/yy')||\n                  ')'             credit_score\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.cbt.Options",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.app.cbt.Options",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:677^7*/
      
      rs = it.getResultSet();
      
      if (rs.next())
      {
        assocOptions.setAssocNum(rs.getString("assoc_num"));
        setFields(rs,false);
      }
      
      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadMerchant()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** protected boolean loadAppData()
  **
  ** Loads app data.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean loadAppData()
  {
    boolean loadOk = false;
    ResultSetIterator it = null;
    try
    {
      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        connect();
        loadMerchant(appSeqNum);
      }
      loadOk = true;
    }
    catch(Exception e)
    {
      logEntry("loadData()",e.toString());
      System.out.println(this.getClass().getName() + "::loadAppData(): "
        + e.toString());
      e.printStackTrace();
    }
    finally
    {
      cleanUp();
    }
    return loadOk;
  }
}/*@lineinfo:generated-code*/