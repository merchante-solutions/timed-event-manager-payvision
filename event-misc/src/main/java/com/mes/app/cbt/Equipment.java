/**************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/cbt/Equipment.java $

  Description:
  
  Equipment
  
  Banner Bank online application equipment page bean.


  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 2/17/04 9:55a $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.cbt;

import javax.servlet.http.HttpServletRequest;
//import sqlj.runtime.ResultSetIterator;
import com.mes.app.EquipmentBase;
import com.mes.constants.mesConstants;
import com.mes.forms.CheckboxField;

public class Equipment extends EquipmentBase
{
  {
    appType = 31;
    curScreenId = 3;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // create special field for 3 month payment plan
      fields.add(new CheckboxField("paymentPlan3Months", "3 mo installment", false));
      
      // reset all fields, including those just added to be of class formText
      fields.setHtmlExtra("class=\"formText\"");
      
      // set default shipping method to be overnight
      fields.setData("shippingMethod",
        Integer.toString(mesConstants.SHIPPING_METHOD_FEDEX_OVERNIGHT));
      
      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
      
      // make comm type required
      fields.getField("commType").makeRequired();
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
  
  protected boolean submitAppData()
  {
    boolean submitOk = false;
    try
    {
      // set paymentPlanMonths field if special checkbox is checked
      if(fields.getField("paymentPlan3Months").getData().equals("y"))
      {
        fields.getField("paymentPlanMonths").setData("3");
      }
      
      submitOk = super.submitAppData();
    }
    catch(Exception e)
    {
      logEntry("submitAppData()", e.toString());
    }
    
    return submitOk;
  }
  
  protected void loadAppSpecData()
  {
    try
    {
      super.loadAppSpecData();
      
      // set contents of checkbox based on value of paymentPlanMonths field
      if(fields.getField("paymentPlanMonths").getData().equals("3"))
      {
        fields.getField("paymentPlan3Months").setData("y");
      }
      else
      {
        fields.getField("paymentPlan3Months").setData("n");
      }
    }
    catch(Exception e)
    {
      logEntry("loadAppSpecData()", e.toString());
    }
  }
}
