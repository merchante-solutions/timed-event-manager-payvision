/*@lineinfo:filename=Pricing*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/cbt/Pricing.sqlj $

  Description:

  Pricing

  CB&T online app pricing page bean.  Extends PricingBase with CB&T custom
  pricing options.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-12 12:14:18 -0700 (Mon, 12 Mar 2007) $
  Version            : $Revision: 13531 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.cbt;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.PricingBase;
import com.mes.constants.mesConstants;
import com.mes.forms.CurrencyField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Pricing extends PricingBase
{
  {
    appType = 31;
    curScreenId = 4;
  }
  
  /*************************************************************************
  **
  **   Drop Down Tables
  **
  **************************************************************************/
  
  protected class BetSet1 extends DropDownTable
  {
    public BetSet1()
    {
      addElement("",  "select one");
      addElement("1", "Pass Through - Retail (Card Present - Swiped)");
      addElement("2", "Pass Through - MOTO (Card Not Present - Keyed)");
      addElement("3", "Pass Through - Hotel (Card Present - Swiped)");
    }
  }
  
  protected class BetSet5 extends DropDownTable
  {
    public BetSet5()
    {
      addElement("",  "select one");
      addElement("4", "Retail Punitive - 0.55%/1.35%");
      addElement("5", "Retail Punitive - 0.80%/1.35%");
      addElement("6", "Retail Punitive - 1.00%/1.35%");
      addElement("7", "Retail Punitive - 1.25%/1.35%");
      addElement("8", "Retail Punitive - 1.25%/1.60%");
    }
  }
  
      
  protected class BetSet6 extends DropDownTable
  {
    public BetSet6()
    {
      addElement("",  "select one");
      addElement("9", "MOTO/Lodging Punitive - 1.00%");
      addElement("10","MOTO/Lodging Punitive - 1.25%");
      addElement("11","MOTO/Lodging Punitive - 1.50%");
    }
  }
  
  protected class TerminationMonthsTable extends DropDownTable
  {
    public TerminationMonthsTable()
    {
      addElement("0", "0");
      addElement("12", "12");
      addElement("24", "24");
      addElement("36", "36");
    }
  }
  
  protected class TerminationFeeTable extends DropDownTable
  {
    public TerminationFeeTable()
    {
      addElement("0", "$0.00");
      addElement("180", "$180.00");
      addElement("360", "$360.00");
      addElement("540", "$540.00");
    }
  }
  
  /*************************************************************************
  **
  **   Radio Button Lists
  **
  **************************************************************************/
  
  protected static String[][]     betTypeList =
  {
    { "Pass Through All Interchange Downgrades",  "1" },
    { "Use Punitive Retail BETs",                 "5" },
    { "Use Punitive MOTO/Lodging BETs",           "6" }
  };
  
  protected String[][]            supplyBillingList =
  {
    { "Club (Charge Bank)",       "CLUB"  },
    { "NClub (Charge Merchant)",  "NCLUB" }
  };
  
  protected static String[][] terminationMonthsList =
  {
    { "0",  "0"  },
    { "12", "12" },
    { "24", "24" },
    { "36", "36" }
  };
  
  protected static String[][] terminationFeeList =
  {
    { "$0.00",    "0"   },
    { "$180.00",  "180" },
    { "$360.00",  "360" },
    { "$540.00",  "540" }
  };
  
  /*************************************************************************
  **
  **   Validations
  **
  **************************************************************************/
  
  protected class SetupKitValidation implements Validation
  {
    private Field kitField;
    private String errorText;
    
    public SetupKitValidation(Field kitField)
    {
      this.kitField = kitField;
    }
    
    public boolean validate(String fdata)
    {
      double feeAmount = 0;
      boolean isValid = true;
      
      try
      {
        feeAmount = Double.parseDouble(fdata);
      }
      catch (Exception e) {}
      
      if (feeAmount != 0)
      {
        if (kitField.getData().equals("0"))
        {
          isValid = false;
          errorText = "Select setup kit type or clear fee amount";
        }
      }
      
      return isValid;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }

  protected class UniqueFeeValidation implements Validation
  {
    private Field descField;
    private String errorText;
    
    public UniqueFeeValidation(Field descField)
    {
      this.descField = descField;
    }
    
    public boolean validate(String fdata)
    {
      double feeAmount = 0;
      boolean isValid = true;
      
      try
      {
        feeAmount = Double.parseDouble(fdata);
      }
      catch (Exception e) {}
      
      if (feeAmount != 0)
      {
        if (descField.isBlank())
        {
          isValid = false;
          errorText = "Description of unique fee required";
        }
      }
      else if (!descField.isBlank())
      {
        isValid = false;
        errorText = "Provide an amount or clear the unique fee description";
      }
      
      return isValid;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }
  
  /*************************************************************************
  **
  **   Custom Fields
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Upcharge Records
  **
  **************************************************************************/
  
  public static final int URT_ONE_TIME          = 1;
  public static final int URT_MONTHLY_RECURRING = 2;
  public static final int URT_ANNUAL_RECURRING  = 3;
  
  public static String recurTypeDescriptor[] 
    = { "unknown", "One Time", "Monthly Recurring", "Annual Recurring" };
  
  
  public class UpchargeItem
  {
    private long    bankMerchNum;
    private String  bankName;
    
    private long    merchMerchNum;
    
    private String  chargeType;
    private String  descriptor;
    private int     recurType;
    private int     quantity;
    
    private double  upchargeAmount;
    private double  merchChargeAmount;
    
    private Date    appCompletedDate;
    
    public UpchargeItem(ResultSet rs)
    {
      try
      {
        bankMerchNum      = rs.getLong  ("bank_merch_num");
        bankName          = rs.getString("bank_name");
        merchMerchNum     = rs.getLong  ("merch_merch_num");
        chargeType        = rs.getString("charge_type").toUpperCase();
        descriptor        = rs.getString("descriptor").toUpperCase();
        quantity          = rs.getInt   ("quantity");
        recurType         = rs.getInt   ("recur_type");
        upchargeAmount    = rs.getDouble("upcharge_amount");
        merchChargeAmount = rs.getDouble("merch_charge_amount");
        appCompletedDate  = rs.getDate  ("complete_date");
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + "::UpchargeItem(): "
          + e.toString());
        logEntry("UpchargeItem()",e.toString());
      }
    }

    public long   getBankMerchNum()       { return bankMerchNum;      }
    public String getBankName()           { return bankName;          }
    public long   getMerchMerchNum()      { return merchMerchNum;     }
    public String getChargeType()         { return chargeType;        }
    public String getDescriptor()         { return descriptor;        }
    public int    getRecurType()          { return recurType;         }
    public int    getQuantity()           { return quantity;          }
    public double getUpchargeAmount()     { return upchargeAmount;    }
    public double getMerchChargeAmount()  { return merchChargeAmount; }

    public double getChargeAmount()
    {
      // a negative upcharge amount indicates no upcharge data
      // was found for the item, probably it is equipment related
      // for now just return 0 upcharge amount
      if (upchargeAmount < 0)
      {
        return 0;
      }
      
      // return the difference between the set cb&t pricing for the item
      // and the amount being charged to the merchant
      // if the merchant is being charged more than the fixed price then
      // a negative amount (credit to the affiliate bank) is returned
      // if the merchant is being charged less than the fixed price then
      // a positive amount (charge to the bank) is returned
      return upchargeAmount - merchChargeAmount;
    }
    
    private Date startDate = null;
    
    /*
    ** private Date getStartDate()
    **
    ** Determines what start date to use based on the app creation date.  If
    ** the app has been submitted before the 15th of the current month then
    ** this month should be used for the start date.  If on or after the 15th
    ** then the following month should be used.
    ** 
    */
    private Date getStartDate()
    {
      if (startDate == null)
      {
        // get the month, day, year of the app creation date
        Calendar cal = Calendar.getInstance();
        cal.setTime(appCompletedDate);
      
        // if the create date is on or after the 15th, start charges
        // on following month, but set start date to be on the first
        // of the month
        boolean past14 = cal.get(Calendar.DAY_OF_MONTH) > 14;
        cal.set(Calendar.DAY_OF_MONTH,1);
        if (past14)
        {
          cal.roll(Calendar.MONTH,1);
        }
        
        // store the start date
        startDate = cal.getTime();
      }
      return startDate;
    }

    /*
    ** public String getStartDateStr()
    **
    ** Generates a date string to say when a charge record should start.
    **
    ** RETURNS: String containing the start date in the format MMYY.
    */
    public String getStartDateStr()
    {
      // generate a string indicating the appropriate start date in MMYY format
      SimpleDateFormat df = new SimpleDateFormat("MMyy");
      return df.format(getStartDate());
    }
    
    /*
    ** public String getExpireDateStr()
    **
    ** Determines what the expiration date should be.  This depends on the
    ** type of charge (one time or recurring).
    **
    ** RETURNS: string containing the date in MMDDYY format ("999999" for
    **          recurring charges that don't expire).
    */
    public String getExpireDateStr()
    {
      // default to "999999" (never expire)
      String dateStr = "999999";
      
      // calculate the actual expiration date if this 
      // is one time charge
      if (recurType == 1)
      {
        // set the expiration date to be the 15th of the
        // month following the start date
        Calendar cal = Calendar.getInstance();
        cal.setTime(getStartDate());
        cal.roll(Calendar.MONTH,1);
        cal.set(Calendar.DAY_OF_MONTH,15);
        SimpleDateFormat df = new SimpleDateFormat("MMddyy");
        dateStr = df.format(cal.getTime());
      }
      return dateStr;
    }
    
    public String getMonthFrequencyFlag(int monthIdx)
    {
      // monthly recurring charges occur every month
      if (recurType == 2)
      {
        return "Y";
      }
      
      // one time and annual recurring only 
      // occur on the month of setup
      Calendar cal = Calendar.getInstance();
      cal.setTime(getStartDate());
      return (monthIdx == cal.get(Calendar.MONTH) ? "Y" : "N");
    }
    
    public String getSummary()
    {
      StringBuffer summary = new StringBuffer();
      if (chargeType.equals("MIS"))
      {
        summary.append("Miscellaneous ");
      }
      else if (chargeType.equals("POS"))
      {
        summary.append("POS ");
      }
      else if (chargeType.equals("IMP"))
      {
        summary.append("Imprinter ");
      }
      summary.append(recurTypeDescriptor[recurType] + " Charge");
      return summary.toString();
    }
  }
  
  public boolean upchargeExempt()
  {
    boolean result = false;
    
    try
    {
      connect();
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:458^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(cue.hierarchy_node)
//          
//          from    cbt_upcharge_exempt cue,
//                  merchant mr,
//                  t_hierarchy th
//          where   mr.app_seq_num = :getData("appSeqNum") and
//                  mr.asso_number + 3858000000 = th.descendent and
//                  th.ancestor = cue.hierarchy_node and
//                  cue.is_exempt = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2698 = getData("appSeqNum");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cue.hierarchy_node)\n         \n        from    cbt_upcharge_exempt cue,\n                merchant mr,\n                t_hierarchy th\n        where   mr.app_seq_num =  :1  and\n                mr.asso_number + 3858000000 = th.descendent and\n                th.ancestor = cue.hierarchy_node and\n                cue.is_exempt = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.cbt.Pricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_2698);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:469^7*/
      
      result = (recCount > 0);
    }
    catch(Exception e)
    {
      logEntry("upchargeExempt()", e.toString());
      System.out.println("app.cbt.Pricing::upchargeExempt(): " + e.toString());
    }               
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  private void loadUcItems()
  {
    ResultSetIterator it = null;
    
    try
    {
      connect();
      
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      // determine the group number to lookup the bank upcharge merchant
      // account info and the merch num of the application
      int groupNum = -1;
      long merchNum = -1L;
      java.sql.Date completeDate = null;
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:503^9*/

//  ************************************************************
//  #sql [Ctx] { select  substr(h.ancestor,5,6),
//                    x.xa_date,
//                    nvl(m.merch_number,0)
//            
//            from    t_hierarchy       h,
//                    merchant          m,
//                    xml_applications  x
//            where   m.app_seq_num = :appSeqNum
//                    and m.asso_number + 3858000000 = h.descendent
//                    and h.relation = 1
//                    and x.app_seq_num = m.app_seq_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  substr(h.ancestor,5,6),\n                  x.xa_date,\n                  nvl(m.merch_number,0)\n           \n          from    t_hierarchy       h,\n                  merchant          m,\n                  xml_applications  x\n          where   m.app_seq_num =  :1 \n                  and m.asso_number + 3858000000 = h.descendent\n                  and h.relation = 1\n                  and x.app_seq_num = m.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.cbt.Pricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   groupNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   completeDate = (java.sql.Date)__sJT_rs.getDate(2);
   merchNum = __sJT_rs.getLong(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:518^9*/
      }
      catch (Exception e)
      {
        System.out.println("Failure retrieving groupNum or merchNum: " 
          + e.toString());
        System.out.println("(groupNum: " + groupNum + ", merchNum: "
          + merchNum + ")");
      }
      
      // proceed if the group num and merch num are available
      if (groupNum != -1 && merchNum != -1L)
      {
        // find upcharges for misc charges that vary from the
        // cb&t upcharge price amounts
        /*@lineinfo:generated-code*//*@lineinfo:533^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mc.misc_chrg_amount     merch_charge_amount,
//                    ua.amount               upcharge_amount,
//                    ua.descriptor           descriptor,
//                    ua.charge_type          charge_type,
//                    ua.recur_type           recur_type,
//                    1                       quantity,
//                    mn.bank_name            bank_name,
//                    mn.merch_num            bank_merch_num,
//                    :merchNum               merch_merch_num,
//                    :completeDate           complete_date
//            
//            from    miscchrg                mc,
//                    cbt_upchrg_amts         ua,
//                    cbt_upchrg_merch_nums   mn
//                    
//            where   mc.app_seq_num = :appSeqNum
//                    and mc.misc_code = ua.misc_code
//                    and mc.misc_chrg_amount - ua.amount <> 0
//                    and mn.group_num = :groupNum
//                    and ( mn.charge_type is null 
//                          or mn.charge_type = ua.charge_type )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mc.misc_chrg_amount     merch_charge_amount,\n                  ua.amount               upcharge_amount,\n                  ua.descriptor           descriptor,\n                  ua.charge_type          charge_type,\n                  ua.recur_type           recur_type,\n                  1                       quantity,\n                  mn.bank_name            bank_name,\n                  mn.merch_num            bank_merch_num,\n                   :1                merch_merch_num,\n                   :2            complete_date\n          \n          from    miscchrg                mc,\n                  cbt_upchrg_amts         ua,\n                  cbt_upchrg_merch_nums   mn\n                  \n          where   mc.app_seq_num =  :3 \n                  and mc.misc_code = ua.misc_code\n                  and mc.misc_chrg_amount - ua.amount <> 0\n                  and mn.group_num =  :4 \n                  and ( mn.charge_type is null \n                        or mn.charge_type = ua.charge_type )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.cbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,completeDate);
   __sJT_st.setLong(3,appSeqNum);
   __sJT_st.setInt(4,groupNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.cbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:556^9*/
        ResultSet rs = it.getResultSet();
        while (rs.next())
        {
          ucItems.add(new UpchargeItem(rs));
        }
        it.close();

        // find equipment related upcharges for equipment
        // priced different than the cb&t pricing
        /*@lineinfo:generated-code*//*@lineinfo:566^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  me.merchequip_amount    merch_charge_amount,
//                    nvl(ua.amount,-1)       upcharge_amount,
//                    nvl(ua.descriptor,me.equip_model)
//                                            descriptor,
//                    nvl(ua.charge_type,decode(me.equiptype_code,4,'IMP','POS'))
//                                            charge_type,
//                    nvl(ua.recur_type,decode(me.equiplendtype_code,1,1,2,2,-1))
//                                            recur_type,
//                    me.merchequip_equip_quantity
//                                            quantity,
//                    mn.bank_name            bank_name,
//                    mn.merch_num            bank_merch_num,
//                    :merchNum               merch_merch_num,
//                    :completeDate           complete_date
//                    
//            from    merchequipment          me,
//                    cbt_upchrg_amts         ua,
//                    cbt_upchrg_merch_nums   mn
//                    
//            where   me.app_seq_num = :appSeqNum
//                    and me.equip_model = ua.model(+)
//                    and me.equiplendtype_code = ua.lend_type(+)
//                    and me.equiplendtype_code in ( 1, 2 )
//                    and ( ua.model is null or ua.amount <> me.merchequip_amount)
//                    and mn.group_num = :groupNum
//                    and ( mn.charge_type is null 
//                          or mn.charge_type = nvl(ua.charge_type,decode(me.equiptype_code,4,'IMP','POS')) )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  me.merchequip_amount    merch_charge_amount,\n                  nvl(ua.amount,-1)       upcharge_amount,\n                  nvl(ua.descriptor,me.equip_model)\n                                          descriptor,\n                  nvl(ua.charge_type,decode(me.equiptype_code,4,'IMP','POS'))\n                                          charge_type,\n                  nvl(ua.recur_type,decode(me.equiplendtype_code,1,1,2,2,-1))\n                                          recur_type,\n                  me.merchequip_equip_quantity\n                                          quantity,\n                  mn.bank_name            bank_name,\n                  mn.merch_num            bank_merch_num,\n                   :1                merch_merch_num,\n                   :2            complete_date\n                  \n          from    merchequipment          me,\n                  cbt_upchrg_amts         ua,\n                  cbt_upchrg_merch_nums   mn\n                  \n          where   me.app_seq_num =  :3 \n                  and me.equip_model = ua.model(+)\n                  and me.equiplendtype_code = ua.lend_type(+)\n                  and me.equiplendtype_code in ( 1, 2 )\n                  and ( ua.model is null or ua.amount <> me.merchequip_amount)\n                  and mn.group_num =  :4 \n                  and ( mn.charge_type is null \n                        or mn.charge_type = nvl(ua.charge_type,decode(me.equiptype_code,4,'IMP','POS')) )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.cbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,completeDate);
   __sJT_st.setLong(3,appSeqNum);
   __sJT_st.setInt(4,groupNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.cbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:595^9*/
        rs = it.getResultSet();
        while (rs.next())
        {
          ucItems.add(new UpchargeItem(rs));
        }
      
        ucItemsLoaded = true;
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::loadUcItems(): "
        + e.toString());
      e.printStackTrace();
      logEntry("loadUcItems()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
  }
  
  private Vector ucItems = new Vector();
  
  private boolean ucItemsLoaded = false;
  
  public Vector getUcItems()
  {
    if (!ucItemsLoaded)
    {
      loadUcItems();
    }
    return ucItems;
  }
  
  /*************************************************************************
  **
  **   Field Bean
  **
  **************************************************************************/
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      connect();
      
      // initialize the wireless equipment object
      wirelessSet = new WirelessEquipmentSet(appSeqNum);
      add(wirelessSet);
      
      // v/mc pricing options
      
      // cb&t's only pricing plan option is 8
      fields.setData("pricingPlan","8");

      // setup cb&t bet options
      createBetSetFields();
      
      if (isPosType(mesConstants.POS_DIAL_AUTH))
      {
        fields.setData("betType","7");
      }
      
      // installation kit options and fees
      FieldGroup gOddFees = new FieldGroup  ("gOddFees");
      
      // supply billing type
      gOddFees.add(new RadioButtonField     ("supplyBilling", supplyBillingList,-1,false,"Required"));
      
      // unique fee
      gOddFees.add(new CurrencyField        ("uniqueFee",     8,6,true));
      gOddFees.add(new Field                ("uniqueFeeDesc", 30,20,true));
      
      gOddFees.getField("uniqueFee").addValidation(
        new UniqueFeeValidation(gOddFees.getField("uniqueFeeDesc")));
        
      fields.add(gOddFees);
      
      // change termination fee fields to be drop-down boxes
      fields.deleteField("terminationMonths");
      fields.deleteField("terminationFee");
      
      fields.add(new RadioButtonField("terminationMonths", terminationMonthsList, -1, false, "Required"));
      fields.add(new RadioButtonField("terminationFee",    terminationFeeList,    -1, false, "Required"));
//      fields.add(new DropDownField("terminationMonths", new TerminationMonthsTable(), true));
//      fields.add(new DropDownField("terminationFee",    new TerminationFeeTable(),    true));

      // use completely blown up v/mc pricing 
      setDetailedVmcPricing();
      
      // set the field style class
      fields.setHtmlExtra("class=\"formText\"");

      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      e.printStackTrace();
      logEntry("createFields()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
    
    // clear bet set selectors if the corresponding 
    String betType = fields.getData("betType");
    if (!betType.equals("1"))
    {
      fields.setData("betSet_1","");
    }
    if (!betType.equals("5"))
    {
      fields.setData("betSet_5","");
    }
    if (!betType.equals("6"))
    {
      fields.setData("betSet_6","");
    }
  }
  
  /*************************************************************************
  **
  **   Load
  **
  **************************************************************************/

  /*
  ** protected void loadSiteInspection(long appSeqNum)
  **
  ** Loads site inspection data.  Overrides base method so that supply
  ** billing and setup kit can be tacked on.
  */
  protected void loadSiteInspection(long appSeqNum)
  {
    ResultSetIterator         it          = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:749^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  siteinsp_comment              pricing_comments,
//                  siteinsp_name_flag            q_name_match,
//                  siteinsp_inv_sign_flag        q_signage,
//                  siteinsp_bus_hours_flag       q_hours_posted,
//                  siteinsp_inv_viewed_flag      q_inventory_review,
//                  siteinsp_inv_consistant_flag  q_inventory_consistent,
//                  siteinsp_vol_flag             q_inventory_adequate,
//                  siteinsp_no_of_emp            employee_count,
//                  siteinsp_inv_street           inventory_addr,
//                  siteinsp_inv_city             inventory_csz_city,
//                  siteinsp_inv_state            inventory_csz_state,
//                  siteinsp_inv_zip              inventory_csz_zip,
//                  siteinsp_bus_loc              location_type,
//                  siteinsp_bus_loc_comment      location_desc,
//                  siteinsp_bus_address          location_addr_type,
//                  siteinsp_bus_street           location_addr,
//                  siteinsp_bus_city             location_csz_city,
//                  siteinsp_bus_state            location_csz_state,
//                  siteinsp_bus_zip              location_csz_zip,
//                  siteinsp_inv_value            inventory_value,
//                  siteinsp_full_flag            fulfillment_house,
//                  siteinsp_full_name            fulfillment_name,
//                  siteinsp_full_street          fulfillment_addr,
//                  siteinsp_full_city            fulfillment_csz_city,
//                  siteinsp_full_state           fulfillment_csz_state,
//                  siteinsp_full_zip             fulfillment_csz_zip,
//                  siteinsp_soft_flag            security_software,
//                  siteinsp_soft_name            security_software_vendor,
//                  siteinsp_supply_billing       supply_billing,
//                  siteinsp_setupkit_type        setup_kit
//          from    siteinspection
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  siteinsp_comment              pricing_comments,\n                siteinsp_name_flag            q_name_match,\n                siteinsp_inv_sign_flag        q_signage,\n                siteinsp_bus_hours_flag       q_hours_posted,\n                siteinsp_inv_viewed_flag      q_inventory_review,\n                siteinsp_inv_consistant_flag  q_inventory_consistent,\n                siteinsp_vol_flag             q_inventory_adequate,\n                siteinsp_no_of_emp            employee_count,\n                siteinsp_inv_street           inventory_addr,\n                siteinsp_inv_city             inventory_csz_city,\n                siteinsp_inv_state            inventory_csz_state,\n                siteinsp_inv_zip              inventory_csz_zip,\n                siteinsp_bus_loc              location_type,\n                siteinsp_bus_loc_comment      location_desc,\n                siteinsp_bus_address          location_addr_type,\n                siteinsp_bus_street           location_addr,\n                siteinsp_bus_city             location_csz_city,\n                siteinsp_bus_state            location_csz_state,\n                siteinsp_bus_zip              location_csz_zip,\n                siteinsp_inv_value            inventory_value,\n                siteinsp_full_flag            fulfillment_house,\n                siteinsp_full_name            fulfillment_name,\n                siteinsp_full_street          fulfillment_addr,\n                siteinsp_full_city            fulfillment_csz_city,\n                siteinsp_full_state           fulfillment_csz_state,\n                siteinsp_full_zip             fulfillment_csz_zip,\n                siteinsp_soft_flag            security_software,\n                siteinsp_soft_name            security_software_vendor,\n                siteinsp_supply_billing       supply_billing,\n                siteinsp_setupkit_type        setup_kit\n        from    siteinspection\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.cbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.app.cbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:783^7*/
      setFields(it.getResultSet());
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::loadSiteInspection(): "
        + e.toString());
      logEntry("loadSiteInspection()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) { }
    }
  }

  /*
  ** protected void loadOddFees(long appSeqNum)
  **
  ** Load any fees not handled by the fee set.  Currently these are unique
  ** "unique fees" and the setup kid info.
  */
  protected void loadOddFees(long appSeqNum)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      // unique fee
      /*@lineinfo:generated-code*//*@lineinfo:812^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  misc_chrg_amount,
//                  misc_chrgbasis_descr
//          from    miscchrg
//          where   app_seq_num = :appSeqNum
//                  and misc_code = : mesConstants.APP_MISC_CHARGE_UNIQUE_FEE
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  misc_chrg_amount,\n                misc_chrgbasis_descr\n        from    miscchrg\n        where   app_seq_num =  :1 \n                and misc_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.cbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2, mesConstants.APP_MISC_CHARGE_UNIQUE_FEE);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.app.cbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:819^7*/
      rs = it.getResultSet();
      if (rs.next())
      {
        fields.setData("uniqueFee",rs.getString("misc_chrg_amount"));
        fields.setData("uniqueFeeDesc",rs.getString("misc_chrgbasis_descr"));
      }
      
      rs.close();
      it.close();
      
      // setup kit
      /*@lineinfo:generated-code*//*@lineinfo:831^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  misc_chrg_amount
//          from    miscchrg
//          where   app_seq_num = :appSeqNum
//                  and misc_code = : mesConstants.APP_MISC_CHARGE_SETUP_KIT_FEE
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  misc_chrg_amount\n        from    miscchrg\n        where   app_seq_num =  :1 \n                and misc_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.cbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2, mesConstants.APP_MISC_CHARGE_SETUP_KIT_FEE);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.app.cbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:837^7*/
      rs = it.getResultSet();
      if (rs.next())
      {
        fields.setData("setupKitFee",rs.getString("misc_chrg_amount"));
      }
      
      rs.close();
      it.close();
      
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::loadMiscCharges(): "
        + e.toString());
      logEntry("loadMiscCharges()",e.toString());
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  protected boolean loadAppData()
  {
    boolean loadOk = false;
    ResultSetIterator it = null;
    try
    {
      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        connect();

        loadOddFees(appSeqNum);
      }
      loadOk = super.loadAppData();
    }
    catch(Exception e)
    {
      logEntry("loadAppData()",e.toString());
      System.out.println(this.getClass().getName() + "::loadAppData(): "
        + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return loadOk;
  }

  /*************************************************************************
  **
  **   Submit
  **
  **************************************************************************/

  /*
  ** protected void submitSiteInspection(long appSeqNum)
  **
  ** Stores site inspection info.  Overrides base method so that supply
  ** billing and setup kit can be tacked on.
  */
  protected void submitSiteInspection(long appSeqNum)
  {
    try
    {
      // clear any old siteinspection record for this app
      /*@lineinfo:generated-code*//*@lineinfo:907^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    siteinspection
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    siteinspection\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.cbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:912^7*/

      // submit site inspection data
      /*@lineinfo:generated-code*//*@lineinfo:915^7*/

//  ************************************************************
//  #sql [Ctx] { insert into siteinspection
//          ( app_seq_num,
//            siteinsp_comment,
//            siteinsp_name_flag,
//            siteinsp_inv_sign_flag,
//            siteinsp_bus_hours_flag,
//            siteinsp_inv_viewed_flag,
//            siteinsp_inv_consistant_flag,
//            siteinsp_vol_flag,
//            siteinsp_full_flag,
//            siteinsp_soft_flag,
//            siteinsp_inv_street,
//            siteinsp_inv_city,
//            siteinsp_inv_state,
//            siteinsp_inv_zip,
//            siteinsp_full_street,
//            siteinsp_full_city,
//            siteinsp_full_state,
//            siteinsp_full_zip,
//            siteinsp_bus_street,
//            siteinsp_bus_city,
//            siteinsp_bus_state,
//            siteinsp_bus_zip,
//            siteinsp_inv_value,
//            siteinsp_full_name,
//            siteinsp_no_of_emp,
//            siteinsp_bus_loc,
//            siteinsp_bus_loc_comment,
//            siteinsp_bus_address,
//            siteinsp_soft_name,
//            siteinsp_supply_billing,
//            siteinsp_setupkit_type )
//          values
//          ( :appSeqNum,
//            :fields.getData("pricingComments"),
//            :fields.getData("qNameMatch").toUpperCase(),
//            :fields.getData("qSignage").toUpperCase(),
//            :fields.getData("qHoursPosted").toUpperCase(),
//            :fields.getData("qInventoryReview").toUpperCase(),
//            :fields.getData("qInventoryConsistent").toUpperCase(),
//            :fields.getData("qInventoryAdequate").toUpperCase(),
//            :fields.getData("fulfillmentHouse").toUpperCase(),
//            :fields.getData("securitySoftware").toUpperCase(),
//            :fields.getData("inventoryAddr"),
//            :fields.getData("inventoryCszCity"),
//            :fields.getData("inventoryCszState"),
//            :fields.getData("inventoryCszZip"),
//            :fields.getData("fulfillmentAddr"),
//            :fields.getData("fulfillmentCszCity"),
//            :fields.getData("fulfillmentCszState"),
//            :fields.getData("fulfillmentCszZip"),
//            :fields.getData("locationAddr"),
//            :fields.getData("locationCszCity"),
//            :fields.getData("locationCszState"),
//            :fields.getData("locationCszZip"),
//            :fields.getData("inventoryValue"),
//            :fields.getData("fulfillmentName"),
//            :fields.getData("employeeCount"),
//            :fields.getData("locationType"),
//            :fields.getData("locationDesc"),
//            :fields.getData("locationAddrType"),
//            :fields.getData("securitySoftwareVendor"),
//            :fields.getData("supplyBilling"),
//            :fields.getData("setupKit") )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2699 = fields.getData("pricingComments");
 String __sJT_2700 = fields.getData("qNameMatch").toUpperCase();
 String __sJT_2701 = fields.getData("qSignage").toUpperCase();
 String __sJT_2702 = fields.getData("qHoursPosted").toUpperCase();
 String __sJT_2703 = fields.getData("qInventoryReview").toUpperCase();
 String __sJT_2704 = fields.getData("qInventoryConsistent").toUpperCase();
 String __sJT_2705 = fields.getData("qInventoryAdequate").toUpperCase();
 String __sJT_2706 = fields.getData("fulfillmentHouse").toUpperCase();
 String __sJT_2707 = fields.getData("securitySoftware").toUpperCase();
 String __sJT_2708 = fields.getData("inventoryAddr");
 String __sJT_2709 = fields.getData("inventoryCszCity");
 String __sJT_2710 = fields.getData("inventoryCszState");
 String __sJT_2711 = fields.getData("inventoryCszZip");
 String __sJT_2712 = fields.getData("fulfillmentAddr");
 String __sJT_2713 = fields.getData("fulfillmentCszCity");
 String __sJT_2714 = fields.getData("fulfillmentCszState");
 String __sJT_2715 = fields.getData("fulfillmentCszZip");
 String __sJT_2716 = fields.getData("locationAddr");
 String __sJT_2717 = fields.getData("locationCszCity");
 String __sJT_2718 = fields.getData("locationCszState");
 String __sJT_2719 = fields.getData("locationCszZip");
 String __sJT_2720 = fields.getData("inventoryValue");
 String __sJT_2721 = fields.getData("fulfillmentName");
 String __sJT_2722 = fields.getData("employeeCount");
 String __sJT_2723 = fields.getData("locationType");
 String __sJT_2724 = fields.getData("locationDesc");
 String __sJT_2725 = fields.getData("locationAddrType");
 String __sJT_2726 = fields.getData("securitySoftwareVendor");
 String __sJT_2727 = fields.getData("supplyBilling");
 String __sJT_2728 = fields.getData("setupKit");
   String theSqlTS = "insert into siteinspection\n        ( app_seq_num,\n          siteinsp_comment,\n          siteinsp_name_flag,\n          siteinsp_inv_sign_flag,\n          siteinsp_bus_hours_flag,\n          siteinsp_inv_viewed_flag,\n          siteinsp_inv_consistant_flag,\n          siteinsp_vol_flag,\n          siteinsp_full_flag,\n          siteinsp_soft_flag,\n          siteinsp_inv_street,\n          siteinsp_inv_city,\n          siteinsp_inv_state,\n          siteinsp_inv_zip,\n          siteinsp_full_street,\n          siteinsp_full_city,\n          siteinsp_full_state,\n          siteinsp_full_zip,\n          siteinsp_bus_street,\n          siteinsp_bus_city,\n          siteinsp_bus_state,\n          siteinsp_bus_zip,\n          siteinsp_inv_value,\n          siteinsp_full_name,\n          siteinsp_no_of_emp,\n          siteinsp_bus_loc,\n          siteinsp_bus_loc_comment,\n          siteinsp_bus_address,\n          siteinsp_soft_name,\n          siteinsp_supply_billing,\n          siteinsp_setupkit_type )\n        values\n        (  :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n           :16 ,\n           :17 ,\n           :18 ,\n           :19 ,\n           :20 ,\n           :21 ,\n           :22 ,\n           :23 ,\n           :24 ,\n           :25 ,\n           :26 ,\n           :27 ,\n           :28 ,\n           :29 ,\n           :30 ,\n           :31  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.app.cbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_2699);
   __sJT_st.setString(3,__sJT_2700);
   __sJT_st.setString(4,__sJT_2701);
   __sJT_st.setString(5,__sJT_2702);
   __sJT_st.setString(6,__sJT_2703);
   __sJT_st.setString(7,__sJT_2704);
   __sJT_st.setString(8,__sJT_2705);
   __sJT_st.setString(9,__sJT_2706);
   __sJT_st.setString(10,__sJT_2707);
   __sJT_st.setString(11,__sJT_2708);
   __sJT_st.setString(12,__sJT_2709);
   __sJT_st.setString(13,__sJT_2710);
   __sJT_st.setString(14,__sJT_2711);
   __sJT_st.setString(15,__sJT_2712);
   __sJT_st.setString(16,__sJT_2713);
   __sJT_st.setString(17,__sJT_2714);
   __sJT_st.setString(18,__sJT_2715);
   __sJT_st.setString(19,__sJT_2716);
   __sJT_st.setString(20,__sJT_2717);
   __sJT_st.setString(21,__sJT_2718);
   __sJT_st.setString(22,__sJT_2719);
   __sJT_st.setString(23,__sJT_2720);
   __sJT_st.setString(24,__sJT_2721);
   __sJT_st.setString(25,__sJT_2722);
   __sJT_st.setString(26,__sJT_2723);
   __sJT_st.setString(27,__sJT_2724);
   __sJT_st.setString(28,__sJT_2725);
   __sJT_st.setString(29,__sJT_2726);
   __sJT_st.setString(30,__sJT_2727);
   __sJT_st.setString(31,__sJT_2728);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:981^7*/
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::submitSiteInspection(): "
        + e.toString());
      logEntry( "submitSiteInspection()", e.toString());
      addError( "submitSiteInspection: " + e.toString());
    }
  }

  /*
  ** protected void submitOddFees(long appSeqNum)
  **
  ** Stores any fees not handled by the fee set object.  Right now that
  ** is unique fees and setup kit information
  */
  protected void submitOddFees(long appSeqNum)
  {
    try
    {
      // clear involved fees
      /*@lineinfo:generated-code*//*@lineinfo:1003^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    miscchrg
//          where   app_seq_num = :appSeqNum
//                  and misc_code in
//                    ( :mesConstants.APP_MISC_CHARGE_UNIQUE_FEE,
//                      :mesConstants.APP_MISC_CHARGE_SETUP_KIT_FEE )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    miscchrg\n        where   app_seq_num =  :1 \n                and misc_code in\n                  (  :2 ,\n                     :3  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.cbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_UNIQUE_FEE);
   __sJT_st.setInt(3,mesConstants.APP_MISC_CHARGE_SETUP_KIT_FEE);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1011^7*/
      
      // unique fee
      Field uniqueFee = fields.getField("uniqueFee");
      if (uniqueFee != null && !uniqueFee.getData().equals(""))
      {
        /*@lineinfo:generated-code*//*@lineinfo:1017^9*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//            ( app_seq_num,
//              misc_code,
//              misc_chrg_amount,
//              misc_chrgbasis_descr )
//            values
//            ( :appSeqNum,
//              :mesConstants.APP_MISC_CHARGE_UNIQUE_FEE,
//              :uniqueFee.getData(),
//              :fields.getData("uniqueFeeDesc") )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2729 = uniqueFee.getData();
 String __sJT_2730 = fields.getData("uniqueFeeDesc");
   String theSqlTS = "insert into miscchrg\n          ( app_seq_num,\n            misc_code,\n            misc_chrg_amount,\n            misc_chrgbasis_descr )\n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.app.cbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_UNIQUE_FEE);
   __sJT_st.setString(3,__sJT_2729);
   __sJT_st.setString(4,__sJT_2730);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1029^9*/
      }
      
      // setup kit
      Field setupKit = fields.getField("setupKit");
      if (setupKit != null && !setupKit.getData().equals("0"))
      {
        String setupKitFee = fields.getData("setupKitFee");
        if (!setupKitFee.equals(""))
        {
          /*@lineinfo:generated-code*//*@lineinfo:1039^11*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//              ( app_seq_num,
//                misc_code,
//                misc_chrg_amount )
//              values
//              ( :appSeqNum,
//                :mesConstants.APP_MISC_CHARGE_SETUP_KIT_FEE,
//                :setupKitFee )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into miscchrg\n            ( app_seq_num,\n              misc_code,\n              misc_chrg_amount )\n            values\n            (  :1 ,\n               :2 ,\n               :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.app.cbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_SETUP_KIT_FEE);
   __sJT_st.setString(3,setupKitFee);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1049^11*/
        }
      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::submitMiscCharges(): "
        + e.toString());
      logEntry("submitMiscCharges()",e.toString());
      addError("submitMiscCharges: " + e.toString());
    }
  }

  protected boolean submitAppData()
  {
    boolean submitOk = false;
    try
    {
      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        connect();
      
        submitOddFees(appSeqNum);
      }
      submitOk = super.submitAppData();
    }
    catch(Exception e)
    {
      logEntry("submitAppData()",e.toString());
      System.out.println(this.getClass().getName() + "::submitAppData(): "
        + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return submitOk;
  }
}/*@lineinfo:generated-code*/