/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/cbt/Business.java $

  Description:

  Business

  CB&T online application merchant information page bean.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-01-04 12:10:39 -0800 (Fri, 04 Jan 2008) $
  Version            : $Revision: 14454 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.cbt;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.CheckboxField;
import com.mes.forms.DisabledCheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Business extends com.mes.app.BusinessBase
{
  {
    appType = 31;
    curScreenId = 2;
  }

  protected class TransitRoutingTable extends DropDownTable
  {
    public TransitRoutingTable()
    {
      addElement("","select one");
      addElement("323371076","323371076");
      addElement("123204110","123204110");
      addElement("125108191","125108191");
      addElement("125107765","125107765");
    }
  }

  protected class IndustryTypeTable extends DropDownTable
  {
    public IndustryTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail");
      addElement("2","Restaurant");
      addElement("3","Hotel");
      addElement("4","Motel");
      addElement("5","Internet");
      addElement("7","Direct Marketing");
      addElement("14","MOTO");
      addElement("9","Cash Advance");
      addElement("8","Other");
    }
  }

  protected String[][] posTypeList =
  {
    { "Electronic Draft Capture (EDC) Dial Terminals",
      Integer.toString(mesConstants.POS_DIAL_TERMINAL     ) },
    { "Internet Solution",
      Integer.toString(mesConstants.POS_INTERNET          ) },
    { "PC Charge Express for Windows",
      Integer.toString(mesConstants.POS_CHARGE_EXPRESS    ) },
    { "PC Charge Pro for Windows",
      Integer.toString(mesConstants.POS_CHARGE_PRO        ) },
    { "POS Partner 2000",
      Integer.toString(mesConstants.POS_PC                ) },
    { "DialPay (Telephone Authorization & Capture)",
      Integer.toString(mesConstants.POS_DIAL_AUTH         ) },
    { "Certified Third Party Software",
      Integer.toString(mesConstants.POS_OTHER             ) },
  };

  protected String[][] vmcAcceptList =
  {
    { "Consumer Debit/Prepaid Cards Only",  "2" },
    { "Credit/Business Cards Only",         "1" },
    { "Both",                               "0" }
  };

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      // make some fields required
      fields.getField("businessLegalName").makeRequired();
      fields.getField("taxpayerId").makeRequired();
      fields.getField("yearsOpen").makeRequired();

      // CB&T uses annual fields instead of monthly
      fields.getField("monthlySales").makeOptional();
      fields.getField("monthlyVMCSales").makeOptional();
      fields.getField("annualSales").makeRequired();
      fields.getField("annualVMCSales").makeRequired();
      fields.getField("highestTicket").makeRequired();

      // make some fields not required
      fields.getField("sourceOfInfo").makeOptional();

      // replace industry type drop down
      fields.add    (new DropDownField    ("industryType",      "Industry",new IndustryTypeTable(),false));

      // replace business description with 16 character limitted field
      fields.add    (new Field            ("businessDesc",      "Describe Business",16,16,false));

      // replace payment options
      FieldGroup gPayOpts = new FieldGroup("gPayOpts");

      gPayOpts.add  (new DisabledCheckboxField("vmcAccepted",   "Visa/MasterCard",true));
      gPayOpts.add  (new RadioButtonField ("vmcAcceptTypes",    vmcAcceptList,2,false,"Required"));

      gPayOpts.add  (new CheckboxField    ("debitAccepted",     "Debit with pin",false));
      gPayOpts.add  (new CheckboxField    ("ebtAccepted",       "EBT",false));
      gPayOpts.add  (new Field            ("fcsNumber",         "FCS #",25,25,true));
      gPayOpts.add  (new CheckboxField    ("amexAccepted",      "American Express",false));
      gPayOpts.add  (new CheckboxField    ("amexSplitDial",     "Split Dial",false));
      gPayOpts.add  (new CheckboxField    ("amexPip",           "PIP",false));
      gPayOpts.add  (new NumberField      ("amexEsaRate",       "Amex ESA Rate",5,5,true,2));
      gPayOpts.add  (new NumberField      ("amexAcctNum",       "Amex Acct #",16,25,true,0));
      gPayOpts.add  (new CheckboxField    ("discoverAccepted",  "Discover",false));
      gPayOpts.add  (new CheckboxField    ("needsDiscover",     "Needs new Discover Merchant #",false));
      gPayOpts.add  (new NumberField      ("discoverRapRate",   "Disc RAP Rate",5,5,true,2));
      gPayOpts.add  (new NumberField      ("discoverAcctNum",   "Disc Acct #",15,25,true,0));
      gPayOpts.add  (new CheckboxField    ("dinersAccepted",    "Diners Club",false));
      gPayOpts.add  (new NumberField      ("dinersAcctNum",     "Diners Acct #",10,25,true,0) );
      gPayOpts.add  (new CheckboxField    ("jcbAccepted",       "JCB",false));
      gPayOpts.add  (new NumberField      ("jcbAcctNum",        "JCB Acct #",25,25,true,0));
      gPayOpts.add  (new CheckboxField    ("checkAccepted",     "Checks",false));
      gPayOpts.add  (new DropDownField    ("checkProvider",     "Check Provider",new CheckProviderTable(),true));
      gPayOpts.add  (new Field            ("checkProviderOther","Provider Name",25,25,true));
      gPayOpts.add  (new Field            ("checkAcctNum",      "Acct # / Station #",25,25,true));
      gPayOpts.add  (new Field            ("checkTermId",       "Elec Check Terminal ID",25,25,true));
      gPayOpts.add  (new CheckboxField    ("valutecAccepted",   "Valutec Gift Card",false));
      gPayOpts.add  (new Field            ("valutecAcctNum",    "Valutec Merchant #",25,25,true));
      gPayOpts.add  (new NumberField      ("valutecTermId",     "Valutec Terminal ID",10,25,true,0));
      gPayOpts.add  (new CheckboxField    ("spsAccepted",       "Secure Payment Systems",false));
      gPayOpts.add  (new NumberField      ("spsAcctNum",        "SPS Acct #", 10, 10, true, 0));

      gPayOpts.getField("valutecTermId")
        .addValidation( new ValutecTermIdValidation( gPayOpts.getField("valutecAccepted") ) );
      gPayOpts.getField("valutecTermId").setMinLength(5);

      gPayOpts.getField("checkProvider")
        .addValidation( new CheckProviderValidation( gPayOpts.getField("checkAccepted"),
                                                     gPayOpts.getField("checkProviderOther") ) );

      // make bank account and transit confirmation fields optional (not used)
      fields.getField("confirmCheckingAccount").makeOptional();
      fields.getField("confirmTransitRouting").makeOptional();

      CardAcceptedValidation amexVal = new CardAcceptedValidation();
      amexVal.addField(gPayOpts.getField("amexAcctNum"));
      //amexVal.addField(gPayOpts.getField("amexSplitDial"));
      gPayOpts.getField("amexAccepted").addValidation(amexVal, "amexValidation");

      CardAcceptedValidation discVal = new CardAcceptedValidation();
      discVal.addField(gPayOpts.getField("discoverAcctNum"));
      discVal.addField(gPayOpts.getField("needsDiscover"));
      gPayOpts.getField("discoverAccepted").addValidation(discVal, "discoverValidation");

      CardAcceptedValidation dinersVal = new CardAcceptedValidation();
      dinersVal.addField(gPayOpts.getField("dinersAcctNum"));
      gPayOpts.getField("dinersAccepted").addValidation(dinersVal);

      CardAcceptedValidation jcbVal = new CardAcceptedValidation();
      jcbVal.addField(gPayOpts.getField("jcbAcctNum"));
      gPayOpts.getField("jcbAccepted").addValidation(jcbVal);

      CardAcceptedValidation ebtVal = new CardAcceptedValidation();
      ebtVal.addField(gPayOpts.getField("fcsNumber"));
      gPayOpts.getField("ebtAccepted").addValidation(ebtVal);

      CardAcceptedValidation spsVal = new CardAcceptedValidation();
      spsVal.addField(gPayOpts.getField("spsAcctNum"));
      gPayOpts.getField("spsAccepted").addValidation(spsVal);

      gPayOpts.getField("spsAcctNum").
        addValidation(new SPSAcctValidation(gPayOpts.getField("spsAccepted")));

      fields.add(gPayOpts);
/*
      // replace pos product section
      FieldGroup gProduct = new FieldGroup("gProduct");

      gProduct.add  (new RadioButtonField("productType",      posTypeList,-1,false,"Please specify a product type"));
      gProduct.add  (new DropDownField  ("internetType",      new InternetProductTable(),false));
      gProduct.add  (new CheckboxField  ("existingInternet", "Merchant has existing Internet Solution", false));
      gProduct.add  (new CheckboxField  ("existingVerisign",  "Merchant has an existing Verisign account",false));
      gProduct.add  (new Field          ("webUrl",            25,30,false));
      gProduct.add  (new Field          ("vitalProduct",      40,45,false));
      gProduct.add  (new NumberField    ("imprinterPlates",   2,3,true,0));
      gProduct.add  (new TextareaField  ("comments",          4000,2,80,true));
      gProduct.add  (new CheckboxField  ("existingPCChargeExpress", "Merchant has existing PC Charge Express for Windows", false));
      gProduct.add  (new CheckboxField  ("existingPCChargePro", "Merchant has existing PC Charge Pro for Windows", false));

      RadioButtonField productType =
        (RadioButtonField)gProduct.getField("productType");

      gProduct.getField("internetType").setOptionalCondition(
        new FieldValueCondition(productType,
          Integer.toString(mesConstants.POS_INTERNET)));
      gProduct.getField("webUrl").setOptionalCondition(
        new FieldValueCondition(productType,
          Integer.toString(mesConstants.POS_INTERNET)));
      gProduct.getField("vitalProduct").setOptionalCondition(
        new FieldValueCondition(productType,
          Integer.toString(mesConstants.POS_OTHER)));

      fields.add(gProduct);
*/

      // pos product version additions
      FieldGroup gProduct = (FieldGroup)getField("gProduct");

      //add version field
      gProduct.add  (new Field          ("pgVersion",      10,10,false));
      gProduct.add  (new Field          ("vVersion",      10,10,false));

      //make it required on POS_OTHER and POS_PAYMENT_GATEWAY
      RadioButtonField productType =
        (RadioButtonField)gProduct.getField("productType");

      gProduct.getField("vVersion").setOptionalCondition(
        new FieldValueCondition(productType,
          Integer.toString(mesConstants.POS_OTHER)));

      gProduct.getField("pgVersion").setOptionalCondition(
        new FieldValueCondition(productType,
          Integer.toString(mesConstants.POS_PAYMENT_GATEWAY)));


      // set field validations for extended pos partner fields
      createPosPartnerExtendedFields();

      // set up to use annual volume fields
      setAnnualVolume(true);

      // reset all fields, including those just added to be of class form
      fields.setHtmlExtra("class=\"formText\"");

      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
  
  protected void preHandleRequest(HttpServletRequest request)
  {
    super.preHandleRequest(request);
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);

    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }
    
    if( ! getData("needsDiscover").equals("") )
    {
      setData("discoverRapRate", Double.toString(calculateDiscoverRate()));
    }
  }

  //update version number if it exists,
  //after you've created the merch_pos entry in the super
  protected void submitProduct(long appSeqNum) throws Exception
  {
    super.submitProduct(appSeqNum);

    String version = fields.getField("vVersion").getData();

    if (version.equals(""))
    {
      version = fields.getField("pgVersion").getData();
    }

    if(version.length()>0)
    {

      try
      {
        // additional fields for
        /*@lineinfo:generated-code*//*@lineinfo:318^9*/

//  ************************************************************
//  #sql [Ctx] { update merch_pos
//            set version = :version
//            where app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update merch_pos\n          set version =  :1 \n          where app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.app.cbt.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,version);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:323^9*/
      }
      catch (Exception e)
      {
        String methodSig = this.getClass().getName() + "::submitProduct()";
        throw new Exception(methodSig + ": " + e.toString());
      }
    }

  }
  
  protected void loadCards(long appSeqNum) throws Exception
  {
    super.loadCards(appSeqNum);
    
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:345^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchpo_rate,
//                  merchpo_card_merch_number
//          from    merchpayoption
//          where   app_seq_num = :appSeqNum and
//                  cardtype_code = :mesConstants.APP_CT_DISCOVER
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merchpo_rate,\n                merchpo_card_merch_number\n        from    merchpayoption\n        where   app_seq_num =  :1  and\n                cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.cbt.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_DISCOVER);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.cbt.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:352^7*/
      
      rs = it.getResultSet();
      
      if( rs.next() && 
          (
            rs.getString("merchpo_card_merch_number") == null ||
            rs.getString("merchpo_card_merch_number").length() <= 1
          ) &&
          rs.getDouble("merchpo_rate") > 0 )
      {
        setData("needsDiscover", "y");
      }
    }
    catch(Exception e)
    {
      logEntry("loadCards(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  //load version number if it exists,
  //after you've loaded the prodcut info in the super
  protected void loadProduct(long appSeqNum) throws Exception
  {

    super.loadProduct(appSeqNum);

    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    try
    {
      // product type
      /*@lineinfo:generated-code*//*@lineinfo:390^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mp.pos_code   pos_code,
//                  mp.version    version
//          from    merch_pos     mp
//          where   mp.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mp.pos_code   pos_code,\n                mp.version    version\n        from    merch_pos     mp\n        where   mp.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.cbt.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.cbt.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:396^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        int pos_code = rs.getInt("pos_code");

        if(pos_code == mesConstants.APP_MPOS_OTHER_VITAL_PRODUCT)
        {
          // set internet type and product type
          fields.setData("vVersion", rs.getString("version"));
        }
        else if(pos_code == mesConstants.APP_MPOS_PAYMENT_GATEWAY_CP ||
                pos_code == mesConstants.APP_MPOS_PAYMENT_GATEWAY_CNP)
        {

          fields.setData("pgVersion", rs.getString("version"));

        }
      }
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadProduct()";
      //throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }


}/*@lineinfo:generated-code*/