/*@lineinfo:filename=TomDaileyAppDone*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/AppDone.sqlj $

  Description:
  
  AppDone
  
  Bean used by app completion page.  To allow it to be used by multiple
  screen sequences this bean will use the appSeqNum to determine what its
  app type and screen sequence id are.  It also causes the app to be
  inserted into the credit queue and automatically marks itself as complete.


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.tomd;

import javax.servlet.http.HttpServletRequest;
import com.mes.app.AppDone;
import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.net.MailMessage;
import com.mes.support.HttpHelper;

public class TomDaileyAppDone extends AppDone
{
  {
    appType = mesConstants.APP_TYPE_TOMD;
    curScreenId = 2;
    submitPartial = true;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    this.request = request;
  }
  
  private void sendEmail()
  {
    try
    {
      connect();
      
      String  businessName;
      String  controlNumber;
      StringBuffer body = new StringBuffer("");
      
      /*@lineinfo:generated-code*//*@lineinfo:66^7*/

//  ************************************************************
//  #sql [Ctx] { select  mr.merch_business_name,
//                  mr.merc_cntrl_number
//          
//          from    merchant mr
//          where   mr.app_seq_num = :getData("appSeqNum")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3192 = getData("appSeqNum");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mr.merch_business_name,\n                mr.merc_cntrl_number\n         \n        from    merchant mr\n        where   mr.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.tomd.TomDaileyAppDone",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3192);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   businessName = (String)__sJT_rs.getString(1);
   controlNumber = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:74^7*/
      
      MailMessage msg = new MailMessage();
      
      
      body.append("A new Tom Dailey referral application has been submitted.\n\n");
      body.append("  Control Number: " + controlNumber + "\n");
      body.append("  Business Name : " + businessName + "\n\n");
      
      body.append(HttpHelper.getServerURL(request) + "/jsp/setup/merchinfo4.jsp?primaryKey=" + getData("appSeqNum") + "\n\n");
      
      msg.setAddresses(MesEmails.MSG_ADDRS_TOMDAILEY_NOTIFY);
      msg.setSubject("New Tom Dailey Referral Application");
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("sendEmail(" + getData("appSeqNum") + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*
  ** protected boolean loadAppData()
  **
  ** There is no way to submit from the app completed page, so app_init.jsp
  ** will always be calling loadAppData when a user accesses this page.  Rather
  ** that load anything, this method will cause the app to be inserted into
  ** the credit queue.  It also marks this screen in the sequence as being
  ** complete.
  **
  ** RETURNS: true if no errors, else false.
  */
  protected boolean loadAppData()
  {
    long appSeqNum = fields.getField("appSeqNum").asInteger();
    boolean loadOk = false;
    try
    {
      // mark the sequence screen as complete
      appSeq.getCurrentScreen().markAsComplete();
      
      // send email to sales to notify them of new app
      sendEmail();
      
      loadOk = true;
    }
    catch( Exception e )
    {
      logEntry("loadAppData(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    return loadOk;
  }
}/*@lineinfo:generated-code*/