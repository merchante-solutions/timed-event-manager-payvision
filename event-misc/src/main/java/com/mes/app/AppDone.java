/*@lineinfo:filename=AppDone*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/AppDone.sqlj $

  Description:
  
  AppDone
  
  Bean used by app completion page.  To allow it to be used by multiple
  screen sequences this bean will use the appSeqNum to determine what its
  app type and screen sequence id are.  It also causes the app to be
  inserted into the credit queue and automatically marks itself as complete.


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 4/15/04 11:36a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.ops.InsertCreditQueue;
import sqlj.runtime.ResultSetIterator;

public class AppDone extends AppBase
{
  /*
  ** protected void createFields(HttpServletRequest request)
  **
  ** Determines app type and screen id based on the app seq num.  Unlike
  ** most page beans, app done is generic and may be used with multiple
  ** screen sequences.  The bean asssumes that it 
  */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      if (appSeqNum > 0L)
      {
        connect();
      
        // set app type 
        int appType = -1;
        /*@lineinfo:generated-code*//*@lineinfo:73^9*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//            
//            from    application
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n           \n          from    application\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.AppDone",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:79^9*/
        fields.setData("appType",Integer.toString(appType));
        
        // set cur screen id
        int curScreenId = -1;
        
        /*@lineinfo:generated-code*//*@lineinfo:85^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ss.screen_id
//            from    screen_sequence ss,
//                    org_app         oa
//            where   oa.app_type = :appType
//                    and oa.screen_sequence_id = ss.screen_sequence_id
//                    and ss.screen_classname = : this.getClass().getName()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1940 =  this.getClass().getName();
  try {
   String theSqlTS = "select  ss.screen_id\n          from    screen_sequence ss,\n                  org_app         oa\n          where   oa.app_type =  :1 \n                  and oa.screen_sequence_id = ss.screen_sequence_id\n                  and ss.screen_classname =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.AppDone",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   __sJT_st.setString(2,__sJT_1940);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.AppDone",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:93^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          fields.setData("curScreenId",rs.getString("screen_id"));
        }
        
        rs.close();
        it.close();
      }
      else
      {
        throw new Exception("Attempt to access confirmation page without a "
          + "valid app seq num.");
      }
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  /*
  ** protected boolean loadAppData()
  **
  ** There is no way to submit from the app completed page, so app_init.jsp
  ** will always be calling loadAppData when a user accesses this page.  Rather
  ** that load anything, this method will cause the app to be inserted into
  ** the credit queue.  It also marks this screen in the sequence as being
  ** complete.
  **
  ** RETURNS: true if no errors, else false.
  */
  protected boolean loadAppData()
  {
    long appSeqNum = fields.getField("appSeqNum").asInteger();
    boolean loadOk = false;
    try
    {
      // mark the sequence screen as complete
      appSeq.getCurrentScreen().markAsComplete();
      
      // insert the app into the credit queue
      (new InsertCreditQueue()).addApp(appSeqNum,user.getUserId());
      
      loadOk = true;
    }
    catch( Exception e )
    {
      logEntry("loadAppData(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    return loadOk;
  }
}/*@lineinfo:generated-code*/