/*@lineinfo:filename=PricingBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/app/PricingBase.sqlj $

  Description:
  
  Generic pricing page bean.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-12 12:14:18 -0700 (Mon, 12 Mar 2007) $
  Version            : $Revision: 13531 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.CityStateZipField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DiscountField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.SmallCurrencyField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class PricingBase extends AppBase
{
  protected FeeSet feeSet = null;
  
  protected boolean useDetailedVmcPricing = false;
  
  /*
  ** public class WirelessEquipmentSet extends FieldBean
  **
  ** A sub bean class containing wireless equipment information.
  */
  public class WirelessEquipmentSet extends FieldBean
  {
    private long appSeqNum;
    
    /*
    ** public class WirelessItem
    **
    ** Data structure representing a wireless item.  Contains accessors for
    ** getting at the equipment specifications and getting and setting the
    ** lli/esn identifiers that need to be gathered for each such item.
    */
    public class WirelessItem
    {
      private String  description;
      private String  model;
      private int     prodId;
      private int     lendType;
      private int     itemIndex;
    
      private Field   lliField = null;
      private Field   esnField = null;
    
    
    
      public WirelessItem(String description,
                          String model,
                          int prodId,
                          int lendType,
                          int itemIndex)
      {
        this.description  = description;
        this.model        = model;
        this.prodId       = prodId;
        this.lendType     = lendType;
        this.itemIndex    = itemIndex;
      
        // create the lli/esn fields
        String fieldName = model + "_" + lendType + "_" + itemIndex;
        lliField = new Field("lli_" + fieldName,15,15,false);
        esnField = new Field("esn_" + fieldName,15,15,false);
      }
    
      public String getDescription()    { return description; }
      public String getModel()          { return model;       }
      public int    getProdId()         { return prodId;      }
      public int    getLendType()       { return lendType;    }
      public int    getItemIndex()      { return itemIndex;   }
    
      public Field  getLliField()       { return lliField;    }
      public Field  getEsnField()       { return esnField;    }
    
      public String toString()
      {
        StringBuffer str = new StringBuffer();
      
        str.append("WirelessItem (");
        str.append(" description - " + description);
        str.append(", model - " + model);
        str.append(", prod id - " + prodId);
        str.append(", lend type - " + lendType);
        str.append(", item idx - " + itemIndex);
        str.append(", lli - " + lliField.getData());
        str.append(", esn - " + esnField.getData());
        str.append(" )");
      
        return str.toString();
      }
    }
  
    private Vector wirelessItems = null;
  
    /*
    ** private void init()
    **
    ** Loads any selected wireless equipment items into the wireless equipment
    ** vector.  Wireless equipment is defined as any equipment item with a 
    ** product id of 20 (Retail CDPD), 21 (Retail Motient w/Debit) or 22
    ** (Retail CDPD w/Debit).  Even if no equipment is found, the wireless
    ** vector is created (allowing getWirelessCount() to return 0).  Also
    ** generates lli/esn fields for each item and puts them in the base
    ** field group.
    */
    private void init()
    {
      ResultSetIterator it  = null;
      ResultSet         rs  = null;

      try
      {
        connect();
      
        // load wireless equipment items
        /*@lineinfo:generated-code*//*@lineinfo:148^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  me.equip_model                model,
//                    me.prod_option_id             prod_id,
//                    me.equiplendtype_code         lend_type,
//                    me.merchequip_equip_quantity  quantity,
//                    ea.equip_description          description
//                  
//            from    merchequipment                me,
//                    equipment_application         ea,
//                    application                   a
//                  
//            where   me.app_seq_num        = :appSeqNum
//                    and me.equip_model    = ea.equip_model
//                    and me.equiptype_code = ea.equip_type
//                    and me.prod_option_id = ea.prod_option_id
//                    and me.app_seq_num    = a.app_seq_num
//                    -- 3 = owned equipment
//                    and (     ( ea.app_type = -1 
//                                and me.equiplendtype_code = 3 ) 
//                          or  ( a.app_type = ea.app_type 
//                                and me.equiplendtype_code <> 3 ) )
//                    -- 20, 21, 22 are wireless prod options
//                    and me.prod_option_id in ( 20, 21, 22 ) 
//                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  me.equip_model                model,\n                  me.prod_option_id             prod_id,\n                  me.equiplendtype_code         lend_type,\n                  me.merchequip_equip_quantity  quantity,\n                  ea.equip_description          description\n                \n          from    merchequipment                me,\n                  equipment_application         ea,\n                  application                   a\n                \n          where   me.app_seq_num        =  :1 \n                  and me.equip_model    = ea.equip_model\n                  and me.equiptype_code = ea.equip_type\n                  and me.prod_option_id = ea.prod_option_id\n                  and me.app_seq_num    = a.app_seq_num\n                  -- 3 = owned equipment\n                  and (     ( ea.app_type = -1 \n                              and me.equiplendtype_code = 3 ) \n                        or  ( a.app_type = ea.app_type \n                              and me.equiplendtype_code <> 3 ) )\n                  -- 20, 21, 22 are wireless prod options\n                  and me.prod_option_id in ( 20, 21, 22 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:173^9*/
        
        // for each equip row, add as many items as specified by quantity
        wirelessItems = new Vector();
        rs = it.getResultSet();
        while (rs.next())
        {
          int quantity = rs.getInt("quantity");
          for (int i = 1; i <= quantity; ++i)
          {
            WirelessItem wi = new WirelessItem(rs.getString("description"),
                                               rs.getString("model"),
                                               rs.getInt("prod_id"),
                                               rs.getInt("lend_type"),
                                               i);
            wirelessItems.add(wi);
            fields.add(wi.getLliField());
            fields.add(wi.getEsnField());
          }
        }
      }
      catch (Exception e)
      {
        logEntry("init()",e.toString());
      }
      finally
      {
        try{ rs.close(); } catch(Exception e) {}
        try{ it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
    
    /*
    ** public WirelessEquipmentSet(long appSeqNum)
    **
    ** Constructor.
    **
    ** Initializes the wireless equipment set.
    */
    public WirelessEquipmentSet(long appSeqNum)
    {
      this.appSeqNum = appSeqNum;
      
      init();
    }
    
    /*
    ** public int getWirelessCount()
    **
    ** RETURNS: number of wireless equipment items in the wireless equipment 
    **          vector.
    */
    public int getWirelessCount()
    {
      if (wirelessItems != null)
      {
        return wirelessItems.size();
      }
      return 0;
    }
    
    /*
    ** public Vector getWirelessItems()
    **
    ** RETURNS: reference to the wireless items vector.
    */
    public Vector getWirelessItems()
    {
      return wirelessItems;
    }
    
    /*
    ** public WirelessItem getWirelessItem(String model, 
    **                                     int prodId, 
    **                                     int lendType,
    **                                     int itemIndex)
    **
    ** Searches the wireless items vector for an item matching the given
    ** parameters.
    **
    ** RETURNS: the corresponding wireless item if found, null if none found
    **          or the wirelesss items vector is null.
    */
    public WirelessItem getWirelessItem(String model, 
                                        int prodId, 
                                        int lendType,
                                        int itemIndex)
    {
      if (wirelessItems != null)
      {
        for (Iterator i = wirelessItems.iterator(); i.hasNext();)
        {
          WirelessItem wi = (WirelessItem)i.next();
          if (wi.getModel().equals(model) &&
              wi.getProdId() == prodId &&
              wi.getLendType() == lendType &&
              wi.getItemIndex() == itemIndex)
          {
            return wi;
          }
        }
      }
      return null;
    }
    
    /*
    ** public boolean autoLoad()
    **
    ** Loads wireless equipment records from equip_wireless table.
    **
    ** RETURNS: true if successful, else false.
    */
    public boolean autoLoad()
    {
      // nothing to do if there are no wireless items
      if (wirelessItems == null)
      {
        return true;
      }
      
      ResultSetIterator it      = null;
      boolean           loadOk  = false;
      
      try
      {
        connect();
        
        // load wireless equipment records
        /*@lineinfo:generated-code*//*@lineinfo:302^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  item_num        item_index,
//                    model           model,
//                    prod_option_id  prod_id,
//                    lend_type       lend_type,
//                    lli             lli,
//                    esn             esn
//                    
//            from    equip_wireless
//            
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  item_num        item_index,\n                  model           model,\n                  prod_option_id  prod_id,\n                  lend_type       lend_type,\n                  lli             lli,\n                  esn             esn\n                  \n          from    equip_wireless\n          \n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:314^9*/
        
        // load the data into the wireless items
        ResultSet rs = it.getResultSet();
        while (rs.next())
        {
          WirelessItem wi = getWirelessItem(rs.getString("model"),
                                            rs.getInt("prod_id"),
                                            rs.getInt("lend_type"),
                                            rs.getInt("item_index"));
          if (wi != null)
          {
            wi.getLliField().setData(rs.getString("lli"));
            wi.getEsnField().setData(rs.getString("esn"));
          }                                            
        }
        
        loadOk = true;
      }
      catch (Exception e)
      {
        logEntry("autoLoad()",e.toString());
      }
      finally
      {
        try { it.close(); } catch (Exception ie) {}
        cleanUp();
      }
      
      return loadOk;
    }
    
    /*
    ** public boolean autoSubmit()
    **
    ** Stores wireless equipment records in equip_wireless table.
    **
    ** RETURNS: true if successful, else false.
    */
    public boolean autoSubmit()
    {
      // nothing to do if there are no wireless items
      if (wirelessItems == null)
      {
        return true;
      }
      
      boolean submitOk = false;

      try
      {
        connect();
        
        // clear out old wireless records for this app seq num
        /*@lineinfo:generated-code*//*@lineinfo:368^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    equip_wireless
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    equip_wireless\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:373^9*/
        
        // submit current wireless information
        for (Iterator i = wirelessItems.iterator(); i.hasNext();)
        {
          WirelessItem wi = (WirelessItem)i.next();
          /*@lineinfo:generated-code*//*@lineinfo:379^11*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_wireless
//              values
//              ( :appSeqNum,
//                : wi.getItemIndex(),
//                : wi.getModel(),
//                : wi.getProdId(),
//                : wi.getLendType(),
//                : wi.getLliField().getData(),
//                : wi.getEsnField().getData() )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2295 =  wi.getItemIndex();
 String __sJT_2296 =  wi.getModel();
 int __sJT_2297 =  wi.getProdId();
 int __sJT_2298 =  wi.getLendType();
 String __sJT_2299 =  wi.getLliField().getData();
 String __sJT_2300 =  wi.getEsnField().getData();
   String theSqlTS = "insert into equip_wireless\n            values\n            (  :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n               :7  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,__sJT_2295);
   __sJT_st.setString(3,__sJT_2296);
   __sJT_st.setInt(4,__sJT_2297);
   __sJT_st.setInt(5,__sJT_2298);
   __sJT_st.setString(6,__sJT_2299);
   __sJT_st.setString(7,__sJT_2300);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:390^11*/
        }
        
        submitOk = true;
      }
      catch (Exception e)
      {
        logEntry("autoSubmit()",e.toString());
      }
      finally
      {
        cleanUp();
      }
      
      return submitOk;
    }
  }

  protected WirelessEquipmentSet wirelessSet = null;
  
  /*************************************************************************
  **
  **   Drop Down Tables
  **
  **************************************************************************/
  
  protected class LocationAddressTable extends DropDownTable
  {
    public LocationAddressTable( )
    {
      //@ should be fixed to use codes instead of text
      addElement("","select one");
      addElement("Business Address on Application", "Business Address on Application" );
      addElement("Mailing Address on Application", "Mailing Address on Application" );
      addElement("Other", "Other" );
    }
  }

  protected class LocationTypeTable extends DropDownTable
  {
    public LocationTypeTable( )
    {
      //@ should be fixed to use codes instead of text
      addElement("",                  "select one");
      addElement("Retail Storefront", "Retail Storefront" );
      addElement("Office Building",   "Office Building" );
      addElement("Industrial Park",   "Industrial Park" );
      addElement("Strip Mall",        "Strip Mall" );
      addElement("Residence",         "Residence" );
      addElement("Other",             "Other" );
    }
  }
  
  protected class BetSet extends DropDownTable
  {
    public BetSet(int betType)
    {
      ResultSetIterator it = null;
      
      try
      {
        connect();
        
        addElement("","select one");
        
        if(HttpHelper.isTestServer(request))
        {
          // utilize dev column to determine what to show
          /*@lineinfo:generated-code*//*@lineinfo:458^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  aib.combo_id                    combo_id,
//                      aib.label                       label,
//                      nvl(aib.required_rights, 'all') required_rights
//              from    appo_ic_bets  aib,
//                      application   app
//              where   aib.bet_type = :betType
//                      and aib.app_type = :appType
//                      and app.app_seq_num = :fields.getData("appSeqNum")
//                      and nvl(aib.dev, 'N') = 'Y'
//              order by aib.display_order
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2301 = fields.getData("appSeqNum");
  try {
   String theSqlTS = "select  aib.combo_id                    combo_id,\n                    aib.label                       label,\n                    nvl(aib.required_rights, 'all') required_rights\n            from    appo_ic_bets  aib,\n                    application   app\n            where   aib.bet_type =  :1 \n                    and aib.app_type =  :2 \n                    and app.app_seq_num =  :3 \n                    and nvl(aib.dev, 'N') = 'Y'\n            order by aib.display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,betType);
   __sJT_st.setInt(2,appType);
   __sJT_st.setString(3,__sJT_2301);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:470^11*/
        }
        else
        {
          // utilize date range to determine what to show
          /*@lineinfo:generated-code*//*@lineinfo:475^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  aib.combo_id                    combo_id,
//                      aib.label                       label,
//                      nvl(aib.required_rights, 'all') required_rights
//              from    appo_ic_bets  aib,
//                      application   app
//              where   aib.bet_type = :betType
//                      and aib.app_type = :appType
//                      and app.app_seq_num = :fields.getData("appSeqNum")
//                      and trunc(app.app_created_date) between aib.valid_from and aib.valid_to
//              order by aib.display_order
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2302 = fields.getData("appSeqNum");
  try {
   String theSqlTS = "select  aib.combo_id                    combo_id,\n                    aib.label                       label,\n                    nvl(aib.required_rights, 'all') required_rights\n            from    appo_ic_bets  aib,\n                    application   app\n            where   aib.bet_type =  :1 \n                    and aib.app_type =  :2 \n                    and app.app_seq_num =  :3 \n                    and trunc(app.app_created_date) between aib.valid_from and aib.valid_to\n            order by aib.display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,betType);
   __sJT_st.setInt(2,appType);
   __sJT_st.setString(3,__sJT_2302);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:487^11*/
        }
        ResultSet rs = it.getResultSet();
        while (rs.next())
        {
          // determine if user has necessary rights to access this pricing option
          boolean pricingAvailable = false;
        
          String rights = rs.getString("required_rights");
        
          if(rights.equals("all"))
          {
            pricingAvailable = true;
          }
          else
          {
            // tokenize rights and mash against user
            StringTokenizer rightTokens = new StringTokenizer(rs.getString("required_rights"), ",");
            while(rightTokens.hasMoreTokens())
            {
              try
              {
                if(user.hasRight(Integer.parseInt(rightTokens.nextToken())))
                {
                  pricingAvailable = true;
                }
              }
              catch(Exception e)
              {
              }
            }
          }
        
          if(pricingAvailable)
          {
            addElement(rs);
          }
        }
      }
      catch(Exception e)
      {
        logEntry("BetSet()",e.toString());
      }
      finally
      {
        try{ it.close(); } catch(Exception e) { }
        cleanUp();
      }
    }
  }
  
  /*************************************************************************
  **
  **   Radio Button Lists
  **
  **************************************************************************/
  
  protected static final String[][] yesNoRadioList = 
  {
    { "No",  "N" },
    { "Yes", "Y" }
  };
  
  // indexes into the QuestionFields string array
  public static final int Q_FIELD_NAME  = 0;
  public static final int Q_TEXT        = 1;

  protected static String[][] questionList =
  {
      // field name           // question
    { "qNameMatch",           "Did name posted at business match business name on application?" },
    { "qSignage",             "Did location appear to have appropriate signage?" },
    { "qHoursPosted",         "Were business hours posted?" },
    { "qInventoryReview",     "Was merchant's inventory viewed?" },
    { "qInventoryConsistent", "Was inventory consistent with merchant's type of business?" },
    { "qInventoryAdequate",   "Did inventory appear to be adequate to support the sales volume indicated on the application?" }
  };

  protected RadioButtonField[] questionFields 
    = new RadioButtonField[questionList.length];

  /*************************************************************************
  **
  **   Validations
  **
  **************************************************************************/
  
  protected class FulfillmentHouseValidation implements Validation
  {
    private String errorText = null;

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String fdata)
    {
      boolean isValid = true;
      
      if (fdata != null && fdata.toUpperCase().equals("Y"))
      {
        if (fields.getField("fulfillmentAddr").isBlank() ||
            fields.getField("fulfillmentCszCity").isBlank() ||
            fields.getField("fulfillmentCszState").isBlank() ||
            fields.getField("fulfillmentCszZip").isBlank())
        {
          errorText = "Please provide the full address of the Fulfillment House";
          isValid = false;
        }
      }
      
      return isValid;
    }
  }

  protected class LocationAddressValidation implements Validation
  {
    private String errorText;

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String fdata)
    {
      boolean isValid = true;
      
      try
      {
        if (fdata.equals("Other"))
        {
          if (fields.getField("locationAddr").isBlank() ||
              fields.getField("locationCszCity").isBlank() ||
              fields.getField("locationCszState").isBlank() ||
              fields.getField("locationCszZip").isBlank())
          {
            errorText = "When indicating 'Other' for address of location inspected provide the full address of the inspected location";
            isValid = false;
          }
        }
      }
      catch(Exception e)
      {
      }
      
      return isValid;
    }
  }

  protected class LocationTypeValidation implements Validation
  {
    Field locTypeField = null;

    public LocationTypeValidation(Field locTypeField)
    {
      this.locTypeField = locTypeField;
    }

    public String getErrorText()
    {
      return("Please provide a description of the location type when selecting other location type ");
    }

    public boolean validate( String fdata )
    {
      return ( !locTypeField.getData().equals("Other")
               || (fdata != null && fdata.length() > 0) );
    }
  }

  public class NoneOrAllValidation implements Validation
  {
    private Vector fields = new Vector();
    private String errorText;

    public NoneOrAllValidation(Vector fields,String errorText)
    {
      this.fields = fields;
      this.errorText = errorText;
    }

    public NoneOrAllValidation(String errorText)
    {
      this.fields = new Vector();
      this.errorText = errorText;
    }

    public void add(Field addField)
    {
      fields.add(addField);
    }

    public boolean validate(String fieldData)
    {
      int blankFields = 0;
      for (Iterator i = fields.iterator(); i.hasNext();)
      {
        Field field = (Field)i.next();
        if (field.isBlank())
        {
          ++blankFields;
        }
      }

      return (blankFields == 0 || blankFields == fields.size());
    }

    public String getErrorText()
    {
      return errorText;
    }
  }
  
  public class DetailedPricingValidation implements Validation
  {
    FieldGroup  pricingFields = null;
    
    public DetailedPricingValidation(FieldGroup pricingFields)
    {
      this.pricingFields = pricingFields;
    }
    
    public boolean validate(String fieldData)
    {
      boolean   isValid = false;
      
      try
      {
        boolean visaConsumer  = (! pricingFields.getData("visaDiscRate").equals("") ||
                                ! pricingFields.getData("visaPerItem").equals(""));
        boolean visaCheck     = (! pricingFields.getData("visaCheckDiscRate").equals("") ||
                                ! pricingFields.getData("visaCheckPerItem").equals(""));
        boolean visaBusiness  = (! pricingFields.getData("visaBusinessDiscRate").equals("") ||
                                ! pricingFields.getData("visaBusinessPerItem").equals(""));
        boolean mcConsumer    = (! pricingFields.getData("mcDiscRate").equals("") ||
                                ! pricingFields.getData("mcPerItem").equals(""));
        boolean mcCheck       = (! pricingFields.getData("mcCheckDiscRate").equals("") ||
                                ! pricingFields.getData("mcCheckPerItem").equals(""));
        boolean mcBusiness    = (! pricingFields.getData("mcBusinessDiscRate").equals("") ||
                                ! pricingFields.getData("mcBusinessPerItem").equals(""));
                        
        // acceptable scenario 1 - visa Consumer and nothing else
        if(visaConsumer && ! visaCheck && ! visaBusiness &&
            ! mcConsumer && ! mcCheck && ! mcBusiness)
        {
          isValid = true;
          
          // set discount rate and per item fields accordingly
          setData("mcDiscRate", getData("visaDiscRate"));
          setData("mcPerItem", getData("visaPerItem"));
        }
        
        // acceptable scenario 2 - visa and mastercard consumer and nothing else
        else if(visaConsumer && ! visaCheck && ! visaBusiness &&
            mcConsumer && ! mcCheck && ! mcBusiness)
        {
          isValid = true;
        }
        
        // acceptable scenario 3 - all visa and no mastercard
        else if(visaConsumer && visaCheck && visaBusiness &&
                ! mcConsumer && ! mcCheck && ! mcBusiness)
        {
          isValid = true;
          
          // set discount rate and per item fields accordingly
          setData("mcDiscRate", getData("visaDiscRate"));
          setData("mcPerItem", getData("visaPerItem"));
          setData("mcCheckDiscRate", getData("visaCheckDiscRate"));
          setData("mcCheckPerItem", getData("visaCheckPerItem"));
          setData("mcBusinessDiscRate", getData("visaBusinessDiscRate"));
          setData("mcBusinessPerItem", getData("visaBusinessPerItem"));
        }
        
        // acceptable scenario 4 - visa and visa check
        else if(visaConsumer && visaCheck && ! visaBusiness && 
                ! mcConsumer && ! mcCheck && ! mcBusiness)
        {
          isValid = true;
          setData("visaBusinessDiscRate", getData("visaDiscRate"));
          setData("visaBusinessPerItem", getData("visaPerItem"));
          setData("mcDiscRate", getData("visaDiscRate"));
          setData("mcPerItem", getData("visaPerItem"));
          setData("mcCheckDiscRate", getData("visaCheckDiscRate"));
          setData("mcCheckPerItem", getData("visaCheckPerItem"));
          setData("mcBusinessDiscRate", getData("visaDiscRate"));
          setData("mcBusinessPerItem", getData("visaPerItem"));
        }
          
        // acceptable scenario 4 - visa, visa check, and mc consumer
        else if(visaConsumer && visaCheck && ! visaBusiness &&
                mcConsumer && ! mcCheck && ! mcBusiness)
        {
          isValid = true;
          setData("visaBusinessDiscRate", getData("visaDiscRate"));
          setData("visaBusinessPerItem", getData("visaPerItem"));
          setData("mcCheckDiscRate", getData("mcDiscRate"));
          setData("mcCheckPerItem", getData("mcPerItem"));
          setData("mcBusinessDiscRate", getData("mcDiscRate"));
          setData("mcBusinessPerItem", getData("mcPerItem"));
        }
        
        // acceptable scenario 5 - visa, mc, and mc check
        else if(visaConsumer && ! visaCheck && ! visaBusiness &&
                mcConsumer && mcCheck && ! mcBusiness)
        {
          isValid = true;
          setData("visaCheckDiscRate", getData("visaDiscRate"));
          setData("visaCheckPerItem", getData("visaPerItem"));
          setData("visaBusinessDiscRate", getData("visaDiscRate"));
          setData("visaBusinessPerItem", getData("visaPerItem"));
          setData("mcBusinessDiscRate", getData("mcDiscRate"));
          setData("mcBusinessPerItem", getData("mcPerItem"));
        }
        
        // acceptable scenario 6 - everything
        else if(visaConsumer && visaCheck && visaBusiness &&
                mcConsumer && mcCheck && mcBusiness)
        {
          isValid = true;
        }
      }
      catch(Exception e)
      {
        logEntry("validate()", e.toString());
      }
      
      return isValid;
    }
    
    public String getErrorText()
    {
      return "Invalid pricing combination";
    }
  }
  
  /*************************************************************************
  **
  **   Custom Fields
  **
  **************************************************************************/
  
  /*
  ** * MapperField classes
  **
  ** These fields can be used by child classes to replace the generic
  ** single pricing type field set used to specify things like IC BET
  ** type, discount rate and per item amounts.  By default these mapper
  ** fields are not used (a set of place holder fields are created using
  ** a combination of hidden and generic field types).  When a child class
  ** needs to support multiple pricing types they would generate the mapper
  ** fields defined here to replace the generic place holder fields.  These
  ** mapper fields will then use the value of the field named "betType"
  ** to map to appropriate fields.  This allows the loader and submitter
  ** methods to support multiple pricing types without having to be rewritten
  ** for each app type.
  **
  ** Example: An app type supports pricing type "22" and "23" so a 
  **          radio button field named "betType" is created that allows 
  **          these values.  Mapper fields (BetSetMapperField, 
  **          DiscRateMapperField and PerItemMapperField) are created.
  **          These custom fields name themselves "betSet", "discRate"
  **          and "perItem" and when added replace the generic place holder
  **          fields with the same names.  These fields will try to map to 
  **          fields using the pricing type values in the betType field.  
  **          The following fields are created to handle the possible 
  **          mappings: "betSet_22", "betSet_23", "discRate_22", 
  **          "discRate_23", "perItem_22" and "perItem_23".  Now, when 
  **          loading and submitting the mapper fields will be used to
  **          load and retrieve the values according to the pricing type 
  **          that is given in the betType field.
  */

  public class PricingTypeMapperField extends Field
  {
    public PricingTypeMapperField(String name, String label)
    {
      super(name,label,0,0,true);
    }
    
    public String getData()
    {
      String betType = fields.getData("betType");
      if (betType != null && betType.length() > 0)
      {
        return fields.getData(getName() + "_" + betType);
      }
      return "";
    }
    
    protected String processData(String rawData)
    {
      String mapFieldName = getName() + "_" + fields.getData("betType");
      fields.setData(mapFieldName,rawData);
      return rawData;
    }
  }
    

  public class BetSetMapperField extends PricingTypeMapperField
  {
    public BetSetMapperField()
    {
      super("betSet","Bet Set");
    }
  }
     
  public class DiscRateMapperField extends PricingTypeMapperField
  {
    public DiscRateMapperField()
    {
      super("discRate","Rate");
    }
  }
  
  public class DiscRateCheckMapperField extends PricingTypeMapperField
  {
    public DiscRateCheckMapperField()
    {
      super("discRateCheck","Rate");
    }
  }
     
  public class PerItemMapperField extends PricingTypeMapperField
  {
    public PerItemMapperField()
    {
      super("perItem","Per Item");
    }
  }
  
  public class PerItemCheckMapperField extends PricingTypeMapperField
  {
    public PerItemCheckMapperField()
    {
      super("perItemCheck","Per Item");
    }
  }
  
  // new fields to support card-type level breakout
  public class VisaDiscRateMapperField extends PricingTypeMapperField
  {
    public VisaDiscRateMapperField()
    {
      super("visaDiscRate","Rate");
    }
  }
  public class VisaPerItemMapperField extends PricingTypeMapperField
  {
    public VisaPerItemMapperField()
    {
      super("visaPerItem","Per Item");
    }
  }
  public class VisaCheckDiscRateMapperField extends PricingTypeMapperField
  {
    public VisaCheckDiscRateMapperField()
    {
      super("visaCheckDiscRate","Rate");
    }
  }
  public class VisaCheckPerItemMapperField extends PricingTypeMapperField
  {
    public VisaCheckPerItemMapperField()
    {
      super("visaCheckPerItem","Per Item");
    }
  }
  public class VisaBusinessDiscRateMapperField extends PricingTypeMapperField
  {
    public VisaBusinessDiscRateMapperField()
    {
      super("visaBusinessDiscRate","Rate");
    }
  }
  public class VisaBusinessPerItemMapperField extends PricingTypeMapperField
  {
    public VisaBusinessPerItemMapperField()
    {
      super("visaBusinessPerItem","Per Item");
    }
  }

  public class MCDiscRateMapperField extends PricingTypeMapperField
  {
    public MCDiscRateMapperField()
    {
      super("mcDiscRate","Rate");
    }
  }
  public class MCPerItemMapperField extends PricingTypeMapperField
  {
    public MCPerItemMapperField()
    {
      super("mcPerItem","Per Item");
    }
  }
  public class MCCheckDiscRateMapperField extends PricingTypeMapperField
  {
    public MCCheckDiscRateMapperField()
    {
      super("mcCheckDiscRate","Rate");
    }
  }
  public class MCCheckPerItemMapperField extends PricingTypeMapperField
  {
    public MCCheckPerItemMapperField()
    {
      super("mcCheckPerItem","Per Item");
    }
  }
  public class MCBusinessDiscRateMapperField extends PricingTypeMapperField
  {
    public MCBusinessDiscRateMapperField()
    {
      super("mcBusinessDiscRate","Rate");
    }
  }
  public class MCBusinessPerItemMapperField extends PricingTypeMapperField
  {
    public MCBusinessPerItemMapperField()
    {
      super("mcBusinessPerItem","Per Item");
    }
  }
     
  /*************************************************************************
  **
  **   Field Bean
  **
  **************************************************************************/
  
  /*
  ** protected String[][] getBetTypeArray()
  **
  ** Creates a list of String value/descriptor pairs to represent the options 
  ** in a radio button field.  A set of bet set type/descriptive labels are 
  ** loaded from appo_ic_bet_types based on the appType (set by child class).
  **
  ** RETURNS: 2 dimensional array of Strings containing the name value pairs
  **          for a radio button field representing the appType bet set 
  **          options.
  */
  protected String[][] getBetTypeArray()
  {
    ResultSetIterator it = null;
    Vector betTypes = new Vector();
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1041^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pricing_type,
//                  label
//          from    appo_ic_bet_types
//          where   app_type = :appType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pricing_type,\n                label\n        from    appo_ic_bet_types\n        where   app_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1047^7*/
      ResultSet rs = it.getResultSet();
      while (rs.next())
      {
        betTypes.add(new String[] { rs.getString("label"),
                                    rs.getString("pricing_type") });
      }
    }
    catch(Exception e)
    {
      logEntry("getBetTypeArray()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) { }
      cleanUp();
    }
    
    return (String[][])betTypes.toArray(new String[0][0]);
  }
  
  /*
  ** protected Field getBetSetOptionsField(int betTypeId)
  **
  ** Given a betTypeId the bet set options that correspond with the type
  ** and the appType are loaded from appo_ic_bets.  Any set that has more
  ** than one option causes a drop down field to be generated containting
  ** the options, if only one (or zero) option is found for a type than
  ** a hidden field is created to hold the sole option (0 if no option found).
  **
  ** RETURNS: a Field representing the bet type's options.
  */
  protected Field getBetSetOptionsField(int betTypeId)
  {
    ResultSetIterator it  = null;
    Field optionField     = null;
    String fieldName      = "betSet_" + betTypeId;
    Field betType         = fields.getField("betType");
    
    try
    {
      connect();
      
      // load bet set options for this bet type and app type
      if(HttpHelper.isTestServer(null))
      {
        // utilize dev column to determine what to show
        /*@lineinfo:generated-code*//*@lineinfo:1094^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  aib.combo_id,
//                    aib.label,
//                    nvl(aib.required_rights, 'all') required_rights
//            from    appo_ic_bets  aib,
//                    application   app
//            where   aib.bet_type = :betTypeId
//                    and aib.app_type = :appType
//                    and app.app_seq_num = :fields.getData("appSeqNum")
//                    and nvl(aib.dev, 'N') = 'Y'
//            order by aib.display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2303 = fields.getData("appSeqNum");
  try {
   String theSqlTS = "select  aib.combo_id,\n                  aib.label,\n                  nvl(aib.required_rights, 'all') required_rights\n          from    appo_ic_bets  aib,\n                  application   app\n          where   aib.bet_type =  :1 \n                  and aib.app_type =  :2 \n                  and app.app_seq_num =  :3 \n                  and nvl(aib.dev, 'N') = 'Y'\n          order by aib.display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,betTypeId);
   __sJT_st.setInt(2,appType);
   __sJT_st.setString(3,__sJT_2303);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1106^9*/
      }
      else
      {
        // utilize date range to determine what to show
        /*@lineinfo:generated-code*//*@lineinfo:1111^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  aib.combo_id,
//                    aib.label,
//                    nvl(aib.required_rights, 'all') required_rights
//            from    appo_ic_bets  aib,
//                    application   app
//            where   aib.bet_type = :betTypeId
//                    and aib.app_type = :appType
//                    and app.app_seq_num = :fields.getData("appSeqNum")
//                    and trunc(app.app_created_date) between aib.valid_from and aib.valid_to
//            order by aib.display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2304 = fields.getData("appSeqNum");
  try {
   String theSqlTS = "select  aib.combo_id,\n                  aib.label,\n                  nvl(aib.required_rights, 'all') required_rights\n          from    appo_ic_bets  aib,\n                  application   app\n          where   aib.bet_type =  :1 \n                  and aib.app_type =  :2 \n                  and app.app_seq_num =  :3 \n                  and trunc(app.app_created_date) between aib.valid_from and aib.valid_to\n          order by aib.display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,betTypeId);
   __sJT_st.setInt(2,appType);
   __sJT_st.setString(3,__sJT_2304);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1123^9*/
      }
      
      ResultSet rs = it.getResultSet();
      Vector options = new Vector();
      while (rs.next())
      {
        // determine if user has necessary rights to access this pricing option
        boolean pricingAvailable = false;
        
        String rights = rs.getString("required_rights");
        
        if(rights.equals("all"))
        {
          pricingAvailable = true;
        }
        else
        {
          // tokenize rights and mash against user
          StringTokenizer rightTokens = new StringTokenizer(rs.getString("required_rights"), ",");
          while(rightTokens.hasMoreTokens())
          {
            try
            {
              if(user.hasRight(Integer.parseInt(rightTokens.nextToken())))
              {
                pricingAvailable = true;
              }
            }
            catch(Exception e)
            {
            }
          }
        }
        
        if(pricingAvailable)
        {
          options.add(new String[] {  rs.getString("combo_id"),
                                      rs.getString("label") });
        }
      }
      
      // if more than one option create a drop down
      if (options.size() > 1)
      {
        DropDownTable table = new DropDownTable();
        table.addElement("","select one");
        for (Iterator i = options.iterator(); i.hasNext();)
        {
          String[] pair = (String[])i.next();
          table.addElement(pair[0],pair[1]);
        }
        optionField = new DropDownField(fieldName,table,true);
        
        // make field required only if the corresponding bet type is selected
        String requiredStr = Integer.toString(betTypeId);
        class RequiredIfValidation implements Validation
        {
          private Field betField;
          private String requiredStr;
          
          public RequiredIfValidation(Field betField,String requiredStr)
          {
            this.betField = betField;
            this.requiredStr = requiredStr;
          }
          
          public boolean validate(String fdata)
          {
            if ((fdata == null || fdata.equals("")) &&
                betField.getData().equals(requiredStr))
            {
              return false;
            }
            return true;
          }
          
          public String getErrorText()
          {
            return "Field required";
          }
        }
        optionField.addValidation(
          new RequiredIfValidation(betType,requiredStr));
      }
      // otherwise create a hidden field for a single value
      else
      {
        optionField = new HiddenField(fieldName);
        
        // use any found single option
        if (options.size() == 1)
        {
          String[] pair = (String[])options.firstElement();
          optionField.setData(pair[0]);
        }
        // create single option of -1 if none defined
        else
        {
          optionField.setData("-1");
        }
      }
    }
    catch(Exception e)
    {
      logEntry("getBetSetOptionsField()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) { }
      cleanUp();
    }
    
    return optionField;
  }
  
  /*
  ** protected void createBetSetFields()
  **
  ** Creates a radio button field with values for each bet type defined for
  ** the appType in appo_ic_bet_types.  The values array is then iterated
  ** through and bet set fields are created to represent possible bet sets
  ** for each bet set type (as defined in appo_ic_bets).
  */
  protected void createBetSetFields()
  {
    // create bet set radio button field
    String[][] betTypeList = getBetTypeArray();
    Field betType = new RadioButtonField("betType",betTypeList,-1,false,
                          "A downgrade handling option is required");
    fields.add(betType);
    
    // create bet set option fields for each bet type
    for (int i = 0; i < betTypeList.length; ++i)
    {
      fields.add(getBetSetOptionsField(Integer.parseInt(betTypeList[i][1])));
    }

    // create bet set mapper field to 
    fields.add(new BetSetMapperField());
  }

  /*
  ** protected FeeSet createFeeSet(long appSeqNum)
  **
  ** Default FeeSet creator.  Can be overridden by child classes that need
  ** specialized fee handling.  See BB&T app for example.
  **
  ** RETURNS: generic FeeSet handler object.
  */
  protected FeeSet createFeeSet(long appSeqNum)
  {
    return new FeeSet(user,appSeqNum);
  }
      
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      // initialize the FeeSet object
      feeSet = createFeeSet(appSeqNum);
      add(feeSet);
      
      // v/mc pricing (these are defaults and will almost certainly need
      // to be overriden with mapper fields replacements and the like)
      FieldGroup gVmcPricing = new FieldGroup("gVmcPricing");

      gVmcPricing.add (new DiscountField      ("discRate",            false));
      gVmcPricing.add (new SmallCurrencyField ("perItem",             5,6,true));
      gVmcPricing.add (new HiddenField        ("pricingPlan"));
      gVmcPricing.add (new HiddenField        ("betType"));
      gVmcPricing.add (new HiddenField        ("betSet"));
      gVmcPricing.add (new DiscountField      ("discRateCheck",       true));
      gVmcPricing.add (new SmallCurrencyField ("perItemCheck",        5,6,true));

      fields.add(gVmcPricing);
      
      // pricing comments
      fields.add      (new TextareaField      ("pricingComments",     320,4,80,true));

      // site information
      FieldGroup gSiteInfo = new FieldGroup("gSiteInfo");
      
      gSiteInfo.add   (new DropDownField      ("locationType",        new LocationTypeTable(),true));
      gSiteInfo.add   (new Field              ("locationDesc",        100,30,true ));
      gSiteInfo.add   (new Field              ("locationAddr",        50,30,true ));
      gSiteInfo.add   (new CityStateZipField  ("locationCsz",         30,true));
      gSiteInfo.add   (new DropDownField      ("locationAddrType",    new LocationAddressTable(), true));
      gSiteInfo.add   (new NumberField        ("employeeCount",       6,3,true,0));

      for( int i = 0; i < questionList.length; ++i )
      {
        RadioButtonField yesNoField
          = new RadioButtonField(questionList[i][Q_FIELD_NAME],
                                 yesNoRadioList,-1,true,"Required");
        questionFields[i] = yesNoField;
        gSiteInfo.add(yesNoField);
      }

      gSiteInfo.getField("locationDesc").addValidation(
        new LocationTypeValidation(gSiteInfo.getField("locationType")));

      gSiteInfo.getField("locationAddrType").addValidation(
        new LocationAddressValidation());
        
      fields.add(gSiteInfo);

      // e-commerce/moto info
      FieldGroup gMotoInfo = new FieldGroup("gMotoInfo");
      
      gMotoInfo.add   (new Field              ("inventoryAddr",           50,30,true));
      gMotoInfo.add   (new CityStateZipField  ("inventoryCsz",            30,true));
      gMotoInfo.add   (new CurrencyField      ("inventoryValue",          12,12,true));
      gMotoInfo.add   (new Field              ("fulfillmentName",         100,30,true));
      gMotoInfo.add   (new Field              ("fulfillmentAddr",         50,30,true));
      gMotoInfo.add   (new CityStateZipField  ("fulfillmentCsz",          30,true));
      gMotoInfo.add   (new RadioButtonField   ("fulfillmentHouse",        yesNoRadioList,-1,true,"Required"));
      gMotoInfo.add   (new RadioButtonField   ("securitySoftware",        yesNoRadioList,-1,true,"Required"));
      gMotoInfo.add   (new Field              ("securitySoftwareVendor",  100,30,true));

      gMotoInfo.getField("fulfillmentHouse").addValidation(
        new FulfillmentHouseValidation());

      gMotoInfo.getField("securitySoftwareVendor").addValidation(
        new IfYesNotBlankValidation(fields.getField("securitySoftware"),
          "Please specify the name of the security software used at this location"));
          
      fields.add(gMotoInfo);
      
      // termination fee fields
      FieldGroup  gTermFees = new FieldGroup("gTermFees");
      gTermFees.add  (new NumberField  ("terminationMonths",   4, 4,true,0));
      gTermFees.add  (new CurrencyField("terminationFee",      5, 6,true));      
      
      // allow submit button
      fields.add      (new ButtonField        ("actionCalculate",         "Calculate"));
      fields.add      (new ButtonField        ("actionAllowSubmit",       "Allow me to submit"));

      // set the field style class
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch( Exception e )
    {
      logEntry("createFields()",e.toString());
    }
  }
  
  /*
  ** public FeeSet getFeeSet()
  **
  ** RETURNS: reference to the FeeSet object.
  */
  public FeeSet getFeeSet()
  {
    return feeSet;
  }
  
  public int getBusinessNature()
  {
    int     retVal      = -1;
    
    try
    {
      connect();
      
      long appSeqNum = fields.getField("appSeqNum").asLong();
    
      /*@lineinfo:generated-code*//*@lineinfo:1394^7*/

//  ************************************************************
//  #sql [Ctx] { select  mr.business_nature  
//          from    merchant    mr
//          where   mr.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mr.business_nature   \n        from    merchant    mr\n        where   mr.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.PricingBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1399^7*/
    }
    catch( Exception e )
    {
      logEntry("getBusinessNature()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  public int getProductType()
  {
    int     retVal      = -1;
    
    try
    {
      connect();
    
      long appSeqNum = fields.getField("appSeqNum").asLong();
    
      /*@lineinfo:generated-code*//*@lineinfo:1422^7*/

//  ************************************************************
//  #sql [Ctx] { select  pc.pos_type    
//          from    merch_pos     mp,
//                  pos_category  pc
//          where   mp.app_seq_num = :appSeqNum and
//                  pc.pos_code = mp.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pc.pos_type     \n        from    merch_pos     mp,\n                pos_category  pc\n        where   mp.app_seq_num =  :1  and\n                pc.pos_code = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.PricingBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1429^7*/
    }
    catch( Exception e )
    {
      logEntry("getProductType()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  /*
  ** public FeeSet getWirelessSet()
  **
  ** RETURNS: reference to the WirelessEquipmentSet object.
  */
  public WirelessEquipmentSet getWirelessSet()
  {
    return wirelessSet;
  }
  
  /*
  ** public int getWirelessCount()
  **
  ** RETURNS: wireless equipment count.
  */
  public int getWirelessCount()
  {
    return wirelessSet.getWirelessCount();
  }  

  /*
  ** public boolean hasWireless()
  **
  ** RETURNS: true if wireless equipment count greater than 0.
  */
  public boolean hasWireless()
  {
    return getWirelessCount() > 0;
  }
  
  public boolean acceptsCard( int ct )
  {
    int       recCount    = 0;
    
    try
    {
      connect();
      
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      /*@lineinfo:generated-code*//*@lineinfo:1482^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(mpo.app_seq_num)  
//          from    merchpayoption      mpo
//          where   mpo.app_seq_num = :appSeqNum and
//                  mpo.cardtype_code = :ct
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mpo.app_seq_num)   \n        from    merchpayoption      mpo\n        where   mpo.app_seq_num =  :1  and\n                mpo.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.app.PricingBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,ct);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1488^7*/
    }
    catch( Exception e )
    {
      logEntry("acceptsCard(" + String.valueOf(ct) + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( recCount != 0 );
  }

  /*
  ** public String[][] getQuestionList()
  **
  ** RETURNS: reference to the question list array.
  */
  public String[][] getQuestionList()
  {
    return questionList;
  }

  /*
  ** public RadioButtonField[] getQuestionFields()
  **
  ** RETURNS: reference to the question fields array.
  */
  public RadioButtonField[] getQuestionFields()
  {
    return questionFields;
  }

  /*
  ** public boolean allowedToSubmit()
  **
  ** RETURNS: true if user has completed this screen previously or if they
  **          have pressed the "allow me to submit" button already.
  */
  public boolean allowedToSubmit()
  {
    boolean allowed = false;
    try
    {
      allowed = appSeq.getCurrentScreen().isComplete()
            || !fields.getField("actionAllowSubmit").isBlank();
    }
    catch (Exception e) {}
    return allowed;
  }

  /*************************************************************************
  **
  **   Load
  **
  **************************************************************************/

  /*
  ** protected void loadVmcPricing(long appSeqNum)
  **
  ** Load v/mc specific pricing info.
  */
  protected void loadVmcPricing(long appSeqNum)
  {
    ResultSetIterator it  = null;

    try
    {
      // load the pricing type
      /*@lineinfo:generated-code*//*@lineinfo:1557^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bet_type_code pricing_type
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bet_type_code pricing_type\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1562^7*/
      setFields(it.getResultSet());

      // load v/mc pricing
      it.close();
      
      if(useDetailedVmcPricing)
      {
        // load visa consumer card pricing
        /*@lineinfo:generated-code*//*@lineinfo:1571^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tranchrg_discrate_type        pricing_plan,
//                    tranchrg_interchangefee_type  bet_type,
//                    tranchrg_disc_rate            visa_disc_rate,
//                    tranchrg_pass_thru            visa_per_item,
//                    tranchrg_interchangefee_fee   bet_set
//            from    tranchrg
//            where   app_seq_num       = :appSeqNum
//                    and cardtype_code = :mesConstants.APP_CT_VISA
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tranchrg_discrate_type        pricing_plan,\n                  tranchrg_interchangefee_type  bet_type,\n                  tranchrg_disc_rate            visa_disc_rate,\n                  tranchrg_pass_thru            visa_per_item,\n                  tranchrg_interchangefee_fee   bet_set\n          from    tranchrg\n          where   app_seq_num       =  :1 \n                  and cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1581^9*/
        setFields(it.getResultSet());
        
        it.close();
        
        // visa check card pricing
        /*@lineinfo:generated-code*//*@lineinfo:1587^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tranchrg_disc_rate            visa_check_disc_rate,
//                    tranchrg_pass_thru            visa_check_per_item
//            from    tranchrg
//            where   app_seq_num       = :appSeqNum
//                    and cardtype_code = :mesConstants.APP_CT_VISA_CHECK_CARD
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tranchrg_disc_rate            visa_check_disc_rate,\n                  tranchrg_pass_thru            visa_check_per_item\n          from    tranchrg\n          where   app_seq_num       =  :1 \n                  and cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA_CHECK_CARD);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1594^9*/
        setFields(it.getResultSet());
        
        it.close();
        
        // visa business card pricing
        /*@lineinfo:generated-code*//*@lineinfo:1600^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tranchrg_disc_rate            visa_business_disc_rate,
//                    tranchrg_pass_thru            visa_business_per_item
//            from    tranchrg
//            where   app_seq_num       = :appSeqNum
//                    and cardtype_code = :mesConstants.APP_CT_VISA_BUSINESS_CARD
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tranchrg_disc_rate            visa_business_disc_rate,\n                  tranchrg_pass_thru            visa_business_per_item\n          from    tranchrg\n          where   app_seq_num       =  :1 \n                  and cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA_BUSINESS_CARD);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1607^9*/
        setFields(it.getResultSet());
        
        it.close();
        
        // mc consumer card
        /*@lineinfo:generated-code*//*@lineinfo:1613^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tranchrg_disc_rate            mc_disc_rate,
//                    tranchrg_pass_thru            mc_per_item
//            from    tranchrg
//            where   app_seq_num       = :appSeqNum
//                    and cardtype_code = :mesConstants.APP_CT_MC
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tranchrg_disc_rate            mc_disc_rate,\n                  tranchrg_pass_thru            mc_per_item\n          from    tranchrg\n          where   app_seq_num       =  :1 \n                  and cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_MC);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1620^9*/
        setFields(it.getResultSet());
        
        it.close();
        
        // mc check card
        /*@lineinfo:generated-code*//*@lineinfo:1626^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tranchrg_disc_rate            mc_check_disc_rate,
//                    tranchrg_pass_thru            mc_check_per_item
//            from    tranchrg
//            where   app_seq_num       = :appSeqNum
//                    and cardtype_code = :mesConstants.APP_CT_MC_CHECK_CARD
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tranchrg_disc_rate            mc_check_disc_rate,\n                  tranchrg_pass_thru            mc_check_per_item\n          from    tranchrg\n          where   app_seq_num       =  :1 \n                  and cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_MC_CHECK_CARD);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1633^9*/
        setFields(it.getResultSet());
        
        it.close();
        
        // mc business card
        /*@lineinfo:generated-code*//*@lineinfo:1639^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tranchrg_disc_rate            mc_business_disc_rate,
//                    tranchrg_pass_thru            mc_business_per_item
//            from    tranchrg
//            where   app_seq_num       = :appSeqNum
//                    and cardtype_code = :mesConstants.APP_CT_MC_BUSINESS_CARD
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tranchrg_disc_rate            mc_business_disc_rate,\n                  tranchrg_pass_thru            mc_business_per_item\n          from    tranchrg\n          where   app_seq_num       =  :1 \n                  and cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_MC_BUSINESS_CARD);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1646^9*/
        setFields(it.getResultSet());
        
        it.close();
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1653^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tranchrg_discrate_type        pricing_plan,
//                    tranchrg_interchangefee_type  bet_type,
//                    tranchrg_disc_rate            disc_rate,
//                    tranchrg_pass_thru            per_item,
//                    tranchrg_interchangefee_fee   bet_set
//            from    tranchrg
//            where   app_seq_num       = :appSeqNum
//                    and cardtype_code = :mesConstants.APP_CT_VISA
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tranchrg_discrate_type        pricing_plan,\n                  tranchrg_interchangefee_type  bet_type,\n                  tranchrg_disc_rate            disc_rate,\n                  tranchrg_pass_thru            per_item,\n                  tranchrg_interchangefee_fee   bet_set\n          from    tranchrg\n          where   app_seq_num       =  :1 \n                  and cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1663^9*/
        setFields(it.getResultSet());
      
        it.close();
      
        // load check card pricing if present
        /*@lineinfo:generated-code*//*@lineinfo:1669^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tranchrg_disc_rate            disc_rate_check,
//                    tranchrg_pass_thru            per_item_check
//            from    tranchrg
//            where   app_seq_num       = :appSeqNum
//                    and cardtype_code = :mesConstants.APP_CT_VISA_CHECK_CARD
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tranchrg_disc_rate            disc_rate_check,\n                  tranchrg_pass_thru            per_item_check\n          from    tranchrg\n          where   app_seq_num       =  :1 \n                  and cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA_CHECK_CARD);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1676^9*/
        setFields(it.getResultSet());
        
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("loadVmcPricing()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** protected void loadSiteInspection(long appSeqNum)
  **
  ** Loads site inspection data.
  */
  protected void loadSiteInspection(long appSeqNum)
  {
    ResultSetIterator         it          = null;
    ResultSet                 rs          = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1704^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  siteinsp_comment              pricing_comments,
//                  siteinsp_name_flag            q_name_match,
//                  siteinsp_inv_sign_flag        q_signage,
//                  siteinsp_bus_hours_flag       q_hours_posted,
//                  siteinsp_inv_viewed_flag      q_inventory_review,
//                  siteinsp_inv_consistant_flag  q_inventory_consistent,
//                  siteinsp_vol_flag             q_inventory_adequate,
//                  siteinsp_no_of_emp            employee_count,
//                  siteinsp_inv_street           inventory_addr,
//                  siteinsp_inv_city             inventory_csz_city,
//                  siteinsp_inv_state            inventory_csz_state,
//                  siteinsp_inv_zip              inventory_csz_zip,
//                  siteinsp_bus_loc              location_type,
//                  siteinsp_bus_loc_comment      location_desc,
//                  siteinsp_bus_address          location_addr_type,
//                  siteinsp_bus_street           location_addr,
//                  siteinsp_bus_city             location_csz_city,
//                  siteinsp_bus_state            location_csz_state,
//                  siteinsp_bus_zip              location_csz_zip,
//                  siteinsp_inv_value            inventory_value,
//                  siteinsp_full_flag            fulfillment_house,
//                  siteinsp_full_name            fulfillment_name,
//                  siteinsp_full_street          fulfillment_addr,
//                  siteinsp_full_city            fulfillment_csz_city,
//                  siteinsp_full_state           fulfillment_csz_state,
//                  siteinsp_full_zip             fulfillment_csz_zip,
//                  siteinsp_soft_flag            security_software,
//                  siteinsp_soft_name            security_software_vendor
//          from    siteinspection
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  siteinsp_comment              pricing_comments,\n                siteinsp_name_flag            q_name_match,\n                siteinsp_inv_sign_flag        q_signage,\n                siteinsp_bus_hours_flag       q_hours_posted,\n                siteinsp_inv_viewed_flag      q_inventory_review,\n                siteinsp_inv_consistant_flag  q_inventory_consistent,\n                siteinsp_vol_flag             q_inventory_adequate,\n                siteinsp_no_of_emp            employee_count,\n                siteinsp_inv_street           inventory_addr,\n                siteinsp_inv_city             inventory_csz_city,\n                siteinsp_inv_state            inventory_csz_state,\n                siteinsp_inv_zip              inventory_csz_zip,\n                siteinsp_bus_loc              location_type,\n                siteinsp_bus_loc_comment      location_desc,\n                siteinsp_bus_address          location_addr_type,\n                siteinsp_bus_street           location_addr,\n                siteinsp_bus_city             location_csz_city,\n                siteinsp_bus_state            location_csz_state,\n                siteinsp_bus_zip              location_csz_zip,\n                siteinsp_inv_value            inventory_value,\n                siteinsp_full_flag            fulfillment_house,\n                siteinsp_full_name            fulfillment_name,\n                siteinsp_full_street          fulfillment_addr,\n                siteinsp_full_city            fulfillment_csz_city,\n                siteinsp_full_state           fulfillment_csz_state,\n                siteinsp_full_zip             fulfillment_csz_zip,\n                siteinsp_soft_flag            security_software,\n                siteinsp_soft_name            security_software_vendor\n        from    siteinspection\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1736^7*/
      
      rs = it.getResultSet();
      
      setFields(rs);
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadSiteInspection()",e.toString());
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  /*
  ** protected void loadTerminationFees(long appSeqNum)
  **
  ** Retrieves termination fee data from merchant table
  */
  protected void loadTerminationFees(long appSeqNum)
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1768^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(termination_fee, 0.0) termination_fee,
//                  nvl(termination_months,0) termination_months
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(termination_fee, 0.0) termination_fee,\n                nvl(termination_months,0) termination_months\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.app.PricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1774^7*/
      
      rs = it.getResultSet();
      
      setFields(rs);
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadTerminationFees(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  protected boolean loadAppData()
  {
    boolean loadOk = false;
    try
    {
      connect();
      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        loadVmcPricing(appSeqNum);
        loadSiteInspection(appSeqNum);
        loadTerminationFees(appSeqNum);
      }
      loadOk = true;
    }
    catch(Exception e)
    {
      logEntry("loadAppData()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return loadOk;
  }

  /*************************************************************************
  **
  **   Submit
  **
  **************************************************************************/
  private void submitCardPricing(long appSeqNum, int cardType, String discRate, String perItem)
  {
    try
    {
      if(discRate != null && !discRate.equals(""))
      {
        int recCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:1833^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//            
//            from    tranchrg
//            where   app_seq_num = :appSeqNum and
//                    cardtype_code = :cardType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n           \n          from    tranchrg\n          where   app_seq_num =  :1  and\n                  cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.app.PricingBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1840^9*/
      
        if(recCount == 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1844^11*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//              (
//                app_seq_num,
//                cardtype_code,
//                tranchrg_discrate_type,
//                tranchrg_disc_rate,
//                tranchrg_pass_thru,
//                tranchrg_per_tran,
//                tranchrg_interchangefee_type,
//                tranchrg_interchangefee_fee
//              )
//              values
//              (
//                :appSeqNum,
//                :cardType,
//                :getData("pricingPlan"),
//                :discRate,
//                :perItem,
//                0,
//                :getData("betType"),
//                :getData("betSet")
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2305 = getData("pricingPlan");
 String __sJT_2306 = getData("betType");
 String __sJT_2307 = getData("betSet");
   String theSqlTS = "insert into tranchrg\n            (\n              app_seq_num,\n              cardtype_code,\n              tranchrg_discrate_type,\n              tranchrg_disc_rate,\n              tranchrg_pass_thru,\n              tranchrg_per_tran,\n              tranchrg_interchangefee_type,\n              tranchrg_interchangefee_fee\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n              0,\n               :6 ,\n               :7 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
   __sJT_st.setString(3,__sJT_2305);
   __sJT_st.setString(4,discRate);
   __sJT_st.setString(5,perItem);
   __sJT_st.setString(6,__sJT_2306);
   __sJT_st.setString(7,__sJT_2307);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1868^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:1872^11*/

//  ************************************************************
//  #sql [Ctx] { update  tranchrg
//              set     tranchrg_discrate_type        = :getData("pricingPlan"),
//                      tranchrg_disc_rate            = :discRate,
//                      tranchrg_pass_thru            = :perItem,
//                      tranchrg_per_tran             = 0,
//                      tranchrg_interchangefee_type  = :getData("betType"),
//                      tranchrg_interchangefee_fee   = :getData("betSet")
//              where   app_seq_num                   = :appSeqNum
//                      and cardtype_code             = :cardType
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2308 = getData("pricingPlan");
 String __sJT_2309 = getData("betType");
 String __sJT_2310 = getData("betSet");
   String theSqlTS = "update  tranchrg\n            set     tranchrg_discrate_type        =  :1 ,\n                    tranchrg_disc_rate            =  :2 ,\n                    tranchrg_pass_thru            =  :3 ,\n                    tranchrg_per_tran             = 0,\n                    tranchrg_interchangefee_type  =  :4 ,\n                    tranchrg_interchangefee_fee   =  :5 \n            where   app_seq_num                   =  :6 \n                    and cardtype_code             =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2308);
   __sJT_st.setString(2,discRate);
   __sJT_st.setString(3,perItem);
   __sJT_st.setString(4,__sJT_2309);
   __sJT_st.setString(5,__sJT_2310);
   __sJT_st.setLong(6,appSeqNum);
   __sJT_st.setInt(7,cardType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1883^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("submitCardPricing(" + appSeqNum + ", " + cardType + ", " + discRate + ", " + perItem + ")", e.toString());
      addError("submitCardPricing: " + e.toString());
    }
  }
  
  private void submitDetailedVmcPricing(long appSeqNum)
  {
    try
    {
      // store the pricing type (bet type) in merchant
      // (betType is general category, betSet is actual bet type to store)
      String betType = fields.getData("betType");
      String betSet  = fields.getData("betSet");
      /*@lineinfo:generated-code*//*@lineinfo:1902^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     pricing_grid  = :betSet,
//                  bet_type_code = :betSet
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     pricing_grid  =  :1 ,\n                bet_type_code =  :2 \n        where   app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,betSet);
   __sJT_st.setString(2,betSet);
   __sJT_st.setLong(3,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1908^7*/
      
      // visa consumer card
      submitCardPricing(appSeqNum, mesConstants.APP_CT_VISA, 
                        getData("visaDiscRate"), getData("visaPerItem"));
                        
      // visa check card
      submitCardPricing(appSeqNum, mesConstants.APP_CT_VISA_CHECK_CARD,
                        getData("visaCheckDiscRate"), getData("visaCheckPerItem"));
      
      // visa business card
      submitCardPricing(appSeqNum, mesConstants.APP_CT_VISA_BUSINESS_CARD,
                        getData("visaBusinessDiscRate"), getData("visaBusinessPerItem"));
                        
      // mc consumer card
      submitCardPricing(appSeqNum, mesConstants.APP_CT_MC,
                        getData("mcDiscRate"), getData("mcPerItem"));
      
      // mc check chard
      submitCardPricing(appSeqNum, mesConstants.APP_CT_MC_CHECK_CARD,
                        getData("mcCheckDiscRate"), getData("mcCheckPerItem"));
                        
      // mc business card
      submitCardPricing(appSeqNum, mesConstants.APP_CT_MC_BUSINESS_CARD,
                        getData("mcBusinessDiscRate"), getData("mcBusinessPerItem"));
    }
    catch(Exception e)
    {
      logEntry("submitDetailedVmcPricing(" + appSeqNum + ")", e.toString());
      addError("submitDetailedVmcPricing: " + e.toString());
    }
  }
  
  private void submitRolledUpVmcPricing(long appSeqNum)
  {
    try
    {
      // store the pricing type (bet type) in merchant
      // (betType is general category, betSet is actual bet type to store)
      String betType = fields.getData("betType");
      String betSet  = fields.getData("betSet");
      /*@lineinfo:generated-code*//*@lineinfo:1949^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     pricing_grid  = :betSet,
//                  bet_type_code = :betSet
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     pricing_grid  =  :1 ,\n                bet_type_code =  :2 \n        where   app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,betSet);
   __sJT_st.setString(2,betSet);
   __sJT_st.setLong(3,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1955^7*/
      
      
      // visa
      submitCardPricing(appSeqNum, mesConstants.APP_CT_VISA,
                        getData("discRate"), getData("perItem"));
                        
      // mc
      submitCardPricing(appSeqNum, mesConstants.APP_CT_MC,
                        getData("discRate"), getData("perItem"));
         
      // add check and business card entries               
      if(! fields.getData("discRateCheck").equals(""))
      {
        submitCardPricing(appSeqNum, mesConstants.APP_CT_VISA_CHECK_CARD,
                          getData("discRateCheck"), getData("perItemCheck"));
                          
        submitCardPricing(appSeqNum, mesConstants.APP_CT_VISA_BUSINESS_CARD,
                          getData("discRate"), getData("perItem"));
                          
        submitCardPricing(appSeqNum, mesConstants.APP_CT_MC_CHECK_CARD,
                          getData("discRateCheck"), getData("perItemCheck"));
                          
        submitCardPricing(appSeqNum, mesConstants.APP_CT_MC_BUSINESS_CARD,
                          getData("discRate"), getData("perItem"));
      }
    }
    catch(Exception e)
    {
      logEntry("submitRolledUpVmcPricing()", e.toString());
      addError("submitRolledUpVmcPricing: " + e.toString());
    }
  }

  /*
  ** protected void submitVmcPricing(long appSeqNum)
  **
  ** Stores information relating to V/MC pricing, including discount rate,
  ** per item fees, minimum discount, etc.  Most of this information is
  ** stored in the tranchrg table for some reason.  The information is stored 
  ** in duplicate in records for both Visa and MasterCard.  Discount rate and 
  ** per item fees may come from multiple sources on the actual page depending 
  ** on the pricing plans that are supported by the application type.  The 
  ** pricing plan type is stored in the merchant table (in pricing_grid AND 
  ** bet_type_code).
  */
  protected void submitVmcPricing(long appSeqNum)
  {
    try
    {
      // remove existing pricing from tranchrg table
      /*@lineinfo:generated-code*//*@lineinfo:2006^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    tranchrg
//          where   app_seq_num = :appSeqNum and
//                  cardtype_code in
//                  (
//                    :mesConstants.APP_CT_VISA,
//                    :mesConstants.APP_CT_VISA_CHECK_CARD,
//                    :mesConstants.APP_CT_VISA_BUSINESS_CARD,
//                    :mesConstants.APP_CT_MC,
//                    :mesConstants.APP_CT_MC_CHECK_CARD,
//                    :mesConstants.APP_CT_MC_BUSINESS_CARD
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    tranchrg\n        where   app_seq_num =  :1  and\n                cardtype_code in\n                (\n                   :2 ,\n                   :3 ,\n                   :4 ,\n                   :5 ,\n                   :6 ,\n                   :7 \n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   __sJT_st.setInt(3,mesConstants.APP_CT_VISA_CHECK_CARD);
   __sJT_st.setInt(4,mesConstants.APP_CT_VISA_BUSINESS_CARD);
   __sJT_st.setInt(5,mesConstants.APP_CT_MC);
   __sJT_st.setInt(6,mesConstants.APP_CT_MC_CHECK_CARD);
   __sJT_st.setInt(7,mesConstants.APP_CT_MC_BUSINESS_CARD);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2020^7*/
      
      if(useDetailedVmcPricing)
      {
        submitDetailedVmcPricing(appSeqNum);
      }
      else
      {
        submitRolledUpVmcPricing(appSeqNum);
      }

    }
    catch(Exception e)
    {
      logEntry("submitVmcPricing()", e.toString());
      addError("submitVmcPricing: " + e.toString());
    }
  }
  
  protected final void setDetailedVmcPricing()
  {
    try
    {
      // first remove rolled-up pricing fields
      fields.deleteField("discRate");
      fields.deleteField("perItem");
      fields.deleteField("discRateCheck");
      fields.deleteField("perItemCheck");
      
      // set up new v/mc pricing fields
      FieldGroup gNewVmcPricing = new FieldGroup("gNewVmcPricing");
      
      gNewVmcPricing.add (new DiscountField       ("visaDiscRate",          false));
      gNewVmcPricing.add (new SmallCurrencyField  ("visaPerItem",           5,6,true));
      gNewVmcPricing.add (new DiscountField       ("visaCheckDiscRate",     true));
      gNewVmcPricing.add (new SmallCurrencyField  ("visaCheckPerItem",      5,6,true));
      gNewVmcPricing.add (new DiscountField       ("visaBusinessDiscRate",  true));
      gNewVmcPricing.add (new SmallCurrencyField  ("visaBusinessPerItem",   5,6,true));
      gNewVmcPricing.add (new DiscountField       ("mcDiscRate",            true));
      gNewVmcPricing.add (new SmallCurrencyField  ("mcPerItem",             5,6,true));
      gNewVmcPricing.add (new DiscountField       ("mcCheckDiscRate",       true));
      gNewVmcPricing.add (new SmallCurrencyField  ("mcCheckPerItem",        5,6,true));
      gNewVmcPricing.add (new DiscountField       ("mcBusinessDiscRate",    true));
      gNewVmcPricing.add (new SmallCurrencyField  ("mcBusinessPerItem",     5,6,true));
      
      fields.add(gNewVmcPricing);
      
      // set up validations for various scenarios
      getField("visaDiscRate").addValidation(new DetailedPricingValidation(gNewVmcPricing));
      
      useDetailedVmcPricing = true;
    }
    catch(Exception e)
    {
      logEntry("setDetailedVmcPricing()", e.toString());
      addError("setDetailedVmcPricing(): " + e.toString());
    }
  }

  /*
  ** protected void submitSiteInspection(long appSeqNum)
  **
  ** Stores site inspection info.
  */
  protected void submitSiteInspection(long appSeqNum)
  {
    try
    {
      // clear any old siteinspection record for this app
      /*@lineinfo:generated-code*//*@lineinfo:2089^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    siteinspection
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    siteinspection\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2094^7*/

      // submit site inspection data
      /*@lineinfo:generated-code*//*@lineinfo:2097^7*/

//  ************************************************************
//  #sql [Ctx] { insert into siteinspection
//          (
//            app_seq_num,
//            siteinsp_comment,
//            siteinsp_name_flag,
//            siteinsp_inv_sign_flag,
//            siteinsp_bus_hours_flag,
//            siteinsp_inv_viewed_flag,
//            siteinsp_inv_consistant_flag,
//            siteinsp_vol_flag,
//            siteinsp_full_flag,
//            siteinsp_soft_flag,
//            siteinsp_inv_street,
//            siteinsp_inv_city,
//            siteinsp_inv_state,
//            siteinsp_inv_zip,
//            siteinsp_full_street,
//            siteinsp_full_city,
//            siteinsp_full_state,
//            siteinsp_full_zip,
//            siteinsp_bus_street,
//            siteinsp_bus_city,
//            siteinsp_bus_state,
//            siteinsp_bus_zip,
//            siteinsp_inv_value,
//            siteinsp_full_name,
//            siteinsp_no_of_emp,
//            siteinsp_bus_loc,
//            siteinsp_bus_loc_comment,
//            siteinsp_bus_address,
//            siteinsp_soft_name
//          )
//          values
//          (
//            :appSeqNum,
//            :fields.getData("pricingComments"),
//            :fields.getData("qNameMatch").toUpperCase(),
//            :fields.getData("qSignage").toUpperCase(),
//            :fields.getData("qHoursPosted").toUpperCase(),
//            :fields.getData("qInventoryReview").toUpperCase(),
//            :fields.getData("qInventoryConsistent").toUpperCase(),
//            :fields.getData("qInventoryAdequate").toUpperCase(),
//            :fields.getData("fulfillmentHouse").toUpperCase(),
//            :fields.getData("securitySoftware").toUpperCase(),
//            :fields.getData("inventoryAddr"),
//            :fields.getData("inventoryCszCity"),
//            :fields.getData("inventoryCszState"),
//            :fields.getData("inventoryCszZip"),
//            :fields.getData("fulfillmentAddr"),
//            :fields.getData("fulfillmentCszCity"),
//            :fields.getData("fulfillmentCszState"),
//            :fields.getData("fulfillmentCszZip"),
//            :fields.getData("locationAddr"),
//            :fields.getData("locationCszCity"),
//            :fields.getData("locationCszState"),
//            :fields.getData("locationCszZip"),
//            :fields.getData("inventoryValue"),
//            :fields.getData("fulfillmentName"),
//            :fields.getData("employeeCount"),
//            :fields.getData("locationType"),
//            :fields.getData("locationDesc"),
//            :fields.getData("locationAddrType"),
//            :fields.getData("securitySoftwareVendor")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2311 = fields.getData("pricingComments");
 String __sJT_2312 = fields.getData("qNameMatch").toUpperCase();
 String __sJT_2313 = fields.getData("qSignage").toUpperCase();
 String __sJT_2314 = fields.getData("qHoursPosted").toUpperCase();
 String __sJT_2315 = fields.getData("qInventoryReview").toUpperCase();
 String __sJT_2316 = fields.getData("qInventoryConsistent").toUpperCase();
 String __sJT_2317 = fields.getData("qInventoryAdequate").toUpperCase();
 String __sJT_2318 = fields.getData("fulfillmentHouse").toUpperCase();
 String __sJT_2319 = fields.getData("securitySoftware").toUpperCase();
 String __sJT_2320 = fields.getData("inventoryAddr");
 String __sJT_2321 = fields.getData("inventoryCszCity");
 String __sJT_2322 = fields.getData("inventoryCszState");
 String __sJT_2323 = fields.getData("inventoryCszZip");
 String __sJT_2324 = fields.getData("fulfillmentAddr");
 String __sJT_2325 = fields.getData("fulfillmentCszCity");
 String __sJT_2326 = fields.getData("fulfillmentCszState");
 String __sJT_2327 = fields.getData("fulfillmentCszZip");
 String __sJT_2328 = fields.getData("locationAddr");
 String __sJT_2329 = fields.getData("locationCszCity");
 String __sJT_2330 = fields.getData("locationCszState");
 String __sJT_2331 = fields.getData("locationCszZip");
 String __sJT_2332 = fields.getData("inventoryValue");
 String __sJT_2333 = fields.getData("fulfillmentName");
 String __sJT_2334 = fields.getData("employeeCount");
 String __sJT_2335 = fields.getData("locationType");
 String __sJT_2336 = fields.getData("locationDesc");
 String __sJT_2337 = fields.getData("locationAddrType");
 String __sJT_2338 = fields.getData("securitySoftwareVendor");
   String theSqlTS = "insert into siteinspection\n        (\n          app_seq_num,\n          siteinsp_comment,\n          siteinsp_name_flag,\n          siteinsp_inv_sign_flag,\n          siteinsp_bus_hours_flag,\n          siteinsp_inv_viewed_flag,\n          siteinsp_inv_consistant_flag,\n          siteinsp_vol_flag,\n          siteinsp_full_flag,\n          siteinsp_soft_flag,\n          siteinsp_inv_street,\n          siteinsp_inv_city,\n          siteinsp_inv_state,\n          siteinsp_inv_zip,\n          siteinsp_full_street,\n          siteinsp_full_city,\n          siteinsp_full_state,\n          siteinsp_full_zip,\n          siteinsp_bus_street,\n          siteinsp_bus_city,\n          siteinsp_bus_state,\n          siteinsp_bus_zip,\n          siteinsp_inv_value,\n          siteinsp_full_name,\n          siteinsp_no_of_emp,\n          siteinsp_bus_loc,\n          siteinsp_bus_loc_comment,\n          siteinsp_bus_address,\n          siteinsp_soft_name\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n           :16 ,\n           :17 ,\n           :18 ,\n           :19 ,\n           :20 ,\n           :21 ,\n           :22 ,\n           :23 ,\n           :24 ,\n           :25 ,\n           :26 ,\n           :27 ,\n           :28 ,\n           :29 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"30com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_2311);
   __sJT_st.setString(3,__sJT_2312);
   __sJT_st.setString(4,__sJT_2313);
   __sJT_st.setString(5,__sJT_2314);
   __sJT_st.setString(6,__sJT_2315);
   __sJT_st.setString(7,__sJT_2316);
   __sJT_st.setString(8,__sJT_2317);
   __sJT_st.setString(9,__sJT_2318);
   __sJT_st.setString(10,__sJT_2319);
   __sJT_st.setString(11,__sJT_2320);
   __sJT_st.setString(12,__sJT_2321);
   __sJT_st.setString(13,__sJT_2322);
   __sJT_st.setString(14,__sJT_2323);
   __sJT_st.setString(15,__sJT_2324);
   __sJT_st.setString(16,__sJT_2325);
   __sJT_st.setString(17,__sJT_2326);
   __sJT_st.setString(18,__sJT_2327);
   __sJT_st.setString(19,__sJT_2328);
   __sJT_st.setString(20,__sJT_2329);
   __sJT_st.setString(21,__sJT_2330);
   __sJT_st.setString(22,__sJT_2331);
   __sJT_st.setString(23,__sJT_2332);
   __sJT_st.setString(24,__sJT_2333);
   __sJT_st.setString(25,__sJT_2334);
   __sJT_st.setString(26,__sJT_2335);
   __sJT_st.setString(27,__sJT_2336);
   __sJT_st.setString(28,__sJT_2337);
   __sJT_st.setString(29,__sJT_2338);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2163^7*/
    }
    catch(Exception e)
    {
      logEntry( "submitSiteInspection()", e.toString());
      addError( "submitSiteInspection: " + e.toString());
    }
  }
  
  protected void submitTerminationFees(long appSeqNum)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2176^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     termination_fee = :getData("terminationFee"),
//                  termination_months = :getData("terminationMonths")
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2339 = getData("terminationFee");
 String __sJT_2340 = getData("terminationMonths");
   String theSqlTS = "update  merchant\n        set     termination_fee =  :1 ,\n                termination_months =  :2 \n        where   app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"31com.mes.app.PricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2339);
   __sJT_st.setString(2,__sJT_2340);
   __sJT_st.setLong(3,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2182^7*/
    }
    catch(Exception e)
    {
      logEntry("submitTerminationFees(" + appSeqNum + ")", e.toString());
    }
  }

  protected boolean submitAppData()
  {
    boolean submitOk = false;
    try
    {
      connect();
      
      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        submitVmcPricing(appSeqNum);
        submitSiteInspection(appSeqNum);
        submitTerminationFees(appSeqNum);
      }
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitAppData()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return submitOk;
  }
}/*@lineinfo:generated-code*/