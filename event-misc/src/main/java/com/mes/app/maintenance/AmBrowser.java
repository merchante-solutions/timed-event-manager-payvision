/*@lineinfo:filename=AmBrowser*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/maintenance/AmBrowser.sqlj $

  AmBrowser
  
  Allows administrators to browse through a hierarchical view of the
  association user mapping hierarchy.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 8/12/04 9:54a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.maintenance;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesHierarchy;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HtmlBlock;
import com.mes.forms.HtmlLink;
import sqlj.runtime.ResultSetIterator;

public class AmBrowser extends FieldBean
{
  public class AssocNode extends HierarchyNode
  {
    private String itemCode;
    private int itemNum;

    public AssocNode(long nodeId, String nodeName, int hierarchyType,
      String itemCode, int itemNum)
    {
      super(nodeId,nodeName,-1L,0,hierarchyType,-1);
      
      this.itemCode = itemCode;
      this.itemNum = itemNum;
    }
    
    public AssocNode(ResultSet rs) throws Exception
    {
      super(rs);
      
      try
      {
        itemCode  = rs.getString("item_code");
        itemNum   = rs.getInt   ("item_num");
      }
      catch (Exception e)
      {
        String eMsg = this.getClass().getName() + "::HierarchyNode(): "
          + e.toString();
        System.out.println(eMsg);
        throw new Exception(eMsg);
      }
    }
    
    public String getItemCode() { return itemCode; }
    public int    getItemNum()  { return itemNum; }
    public int    getGroupNum() { return (itemCode.equals("G") ? itemNum : -1); }
    public int    getCatNum()   { return (itemCode.equals("C") ? itemNum : -1); }
    public int    getAssocNum() { return (itemCode.equals("A") ? itemNum : -1); }
    
    public HtmlBlock getHtmlBlock()
    {
      HtmlBlock block = new HtmlBlock();
      HtmlBlock label = new HtmlBlock();
      HtmlBlock links = new HtmlBlock();
      
      if (itemCode.equals("G"))
      {
        label.add("Group");
        links.add(new HtmlLink("/jsp/app/maintenance/am_group_action.jsp?mode=add&"
          + "parentId=" + getNodeId(),"add subgroup"));
        links.add(", ");
        links.add(new HtmlLink("/jsp/app/maintenance/am_category_action.jsp?mode=add&"
          + "parentId=" + getNodeId(),"add category"));
        links.add(", ");
        links.add(new HtmlLink("/jsp/app/maintenance/am_group_action.jsp?mode=edit&"
          + "groupId=" + getNodeId(),"edit"));
        links.add(", ");
        links.add(new HtmlLink("/jsp/app/maintenance/am_group_action.jsp?mode=delete&"
          + "groupId=" + getNodeId(),"delete"));
      }
      else if (itemCode.equals("C"))
      {
        label.add("Category");
        links.add(new HtmlLink("/jsp/app/maintenance/am_assoc_action.jsp?mode=add&"
          + "parentId=" + getNodeId(),"add assoc"));
        links.add(", ");
        links.add(new HtmlLink("/jsp/app/maintenance/am_category_action.jsp?mode=delete&"
          + "categoryId=" + getNodeId(),"delete"));
      }
      else
      {
        label.add("Association");
        links.add(new HtmlLink("/jsp/app/maintenance/am_assoc_action.jsp?mode=edit&"
          + "assocId=" + getNodeId(),"edit"));
        links.add(", ");
        links.add(new HtmlLink("/jsp/app/maintenance/am_assoc_action.jsp?mode=delete&"
          + "assocId=" + getNodeId(),"delete"));
      }
        
      label.add(" - " + getNodeName());
      
      if (itemCode.equals("A"))
      {
        label.add(" " + itemNum);
      }
      
      block.add(label);
      block.add(" [ ");
      block.add(links);
      block.add(" ]");
      
      return block;
    }
  }
  
  public class AssocLoader extends HierarchyLoader
  {
    protected ResultSetIterator getQueryData(int hierarchyType, long rootId)
    {
      ResultSetIterator it = null;
      try
      {
        // select all hierarchy nodes below the root id 
        /*@lineinfo:generated-code*//*@lineinfo:145^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    h2.ancestor                     parent_id,
//                      h2.descendent                   node_id,
//                      h1.relation                     depth,
//                      h2.hier_type                    hierarchy_type,
//                      h2.entity_type                  node_type,
//                      i.item_code                     item_code,
//                      i.item_num                      item_num,
//                      nvl(i.name,'no name')           name
//                    
//            from      t_hierarchy       h1,
//                      t_hierarchy       h2,
//                      am_item           i
//                    
//            where     h1.ancestor = :rootId
//                      and h1.entity_type <> :MesHierarchy.ET_MERCHANT
//                      and h1.relation > 0
//                      and h1.descendent = h2.descendent
//                      and h1.hier_type = h2.hier_type
//                      and h2.relation = 1
//                      and h2.entity_type <> :MesHierarchy.ET_MERCHANT
//                      and h2.descendent = i.id(+)
//                    
//            order by  h1.relation,
//                      i.name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    h2.ancestor                     parent_id,\n                    h2.descendent                   node_id,\n                    h1.relation                     depth,\n                    h2.hier_type                    hierarchy_type,\n                    h2.entity_type                  node_type,\n                    i.item_code                     item_code,\n                    i.item_num                      item_num,\n                    nvl(i.name,'no name')           name\n                  \n          from      t_hierarchy       h1,\n                    t_hierarchy       h2,\n                    am_item           i\n                  \n          where     h1.ancestor =  :1 \n                    and h1.entity_type <>  :2 \n                    and h1.relation > 0\n                    and h1.descendent = h2.descendent\n                    and h1.hier_type = h2.hier_type\n                    and h2.relation = 1\n                    and h2.entity_type <>  :3 \n                    and h2.descendent = i.id(+)\n                  \n          order by  h1.relation,\n                    i.name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.maintenance.AmBrowser",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,rootId);
   __sJT_st.setInt(2,MesHierarchy.ET_MERCHANT);
   __sJT_st.setInt(3,MesHierarchy.ET_MERCHANT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.maintenance.AmBrowser",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:171^9*/
      }
      catch (Exception e)
      {
        String sig = "getQueryData(hierarchyType=" + hierarchyType
          + ", rootId=" + rootId +")";
        System.out.println(this.getClass().getName()
          + "::" + sig + ": " + e.toString());
        logEntry(sig,e.toString());
      }
      return it;
    }

    protected HierarchyNode getRootNode(long rootId, String rootName,
      int hierarchyType)
    {
      return new AssocNode(rootId,rootName,hierarchyType,"G",rootType);
    }
  
    protected HierarchyNode getNewNode(ResultSet rs)
    {
      AssocNode node = null;
      try
      {
        node = new AssocNode(rs);
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName()
          + "::loadHierarchy(new node): " + e.toString());
        logEntry("loadHierarchy(new node)",e.toString());
      }
      return node;
    }
  }
  
  HierarchyLoader   loader    = new AssocLoader();
  HierarchyRenderer renderer  = new HierarchyRenderer();
  
  public AmBrowser()
  {
  }
  
  public class ToggleField extends Field
  {
    public ToggleField(String fname)
    {
      super(fname,0,0,true);
    }
    
    protected String processData(String rawData)
    {
      try
      {
        loader.toggleNode(Long.parseLong(rawData));
      }
      catch (Exception e)
      {
        System.out.println("Unable to toggle id: " + rawData);
      }
      return rawData;
    }
  }
  
  public class RefreshField extends Field
  {
    public RefreshField(String fname)
    {
      super(fname,0,0,true);
    }
    
    protected String processData(String rawData)
    {
      try
      {
        loadHierarchy();
      }
      catch (Exception e)
      {
        System.out.println("Unable to refresh hierarchy loader: " + rawData);
      }
      return rawData;
    }
  }
  
  private int rootType = -1;
  
  private void loadHierarchy()
  {
    if (user != null)
    {
      // get user's assoc group mapping
      ResultSetIterator it = null;
      try
      {
        connect();
        
        long rootId = -1L;
        int rootType = -1;
        String rootName = null;
        
        /*@lineinfo:generated-code*//*@lineinfo:272^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  i.id        root_id,
//                    i.item_num  root_type,
//                    i.name      root_name
//            from    am_item i,
//                    am_user u
//            where   u.login_name = :user.getLoginName()
//                    and u.id = i.id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2973 = user.getLoginName();
  try {
   String theSqlTS = "select  i.id        root_id,\n                  i.item_num  root_type,\n                  i.name      root_name\n          from    am_item i,\n                  am_user u\n          where   u.login_name =  :1 \n                  and u.id = i.id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.maintenance.AmBrowser",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2973);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.maintenance.AmBrowser",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:281^9*/
        ResultSet rs = it.getResultSet();
        
        if (rs.next())
        {
          rootId = rs.getLong("root_id");
          rootName = rs.getString("root_name");
          this.rootType = rs.getInt("root_type");
        }
        
        loader.loadHierarchy(9,rootId,rootName);
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + "::loadHierarchy(): "
          + e.toString());
        logEntry("loadHierarchy",e.toString());
      }
      finally
      {
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
        
  protected void preHandleRequest(HttpServletRequest request)
  {
    // load the hierarchy in this hook method after user
    // object has been retrieved from session
    loadHierarchy();
  }

  protected void createFields(HttpServletRequest request)
  {
    fields.add(new ToggleField("toggleId"));
    fields.add(new RefreshField("refresh"));
  }
  
  public String renderHtml(String pageLink)
  {
    try
    {
      return renderer.renderHtml(loader,pageLink);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::renderHtml(): "
        + e.toString());
      logEntry("renderHtml",e.toString());
    }
    return null;
  }
}/*@lineinfo:generated-code*/