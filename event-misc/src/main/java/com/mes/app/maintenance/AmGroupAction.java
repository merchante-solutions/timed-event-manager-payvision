/*@lineinfo:filename=AmGroupAction*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/maintenance/AmGroupAction.sqlj $

  AmGroupAction
  
  Adds, edits and deletes association groups.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 8/12/04 9:54a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.maintenance;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesHierarchy;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.tools.DropDownTable;
import com.mes.tools.HierarchyTools;
import sqlj.runtime.ResultSetIterator;


public class AmGroupAction extends FieldBean
{
  private int hierarchyType = MesHierarchy.HT_ASSOC_MAPPINGS;
  
  public AmGroupAction()
  {
  }
  
  public class GroupTypeTable extends DropDownTable
  {
    public GroupTypeTable() throws java.sql.SQLException
    {
      addElement("","select one");
      
      ResultSetIterator it = null;
      try
      {      
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:62^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  type_id,
//                    name
//            from    am_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  type_id,\n                  name\n          from    am_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.maintenance.AmGroupAction",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.maintenance.AmGroupAction",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:67^9*/
        ResultSet rs = it.getResultSet();

        while(rs.next())
        {
          addElement(rs);
        }
      }
      finally
      {
        try{ it.close(); } catch(Exception e) { }
        cleanUp();
      }
    }
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField("mode"));
      fields.add(new HiddenField("parentId"));
      fields.add(new HiddenField("groupId"));
      fields.add(new HiddenField("parentName"));
      fields.add(new HiddenField("fixedTypeFlag"));
      
      fields.add(new DropDownField("groupType",new GroupTypeTable(),false));
      fields.add(new Field("groupName",40,40,false));
      
      fields.add(new ModeButtonField("submitter",fields.getField("mode")));
      
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
  
  private boolean addLoad()
  {
    boolean loadOk = false;
    ResultSetIterator it = null;
    
    try
    {
      System.out.println("addLoad()...");
      // determine parent type (or lack thereof)
      // and load parent name
      long parentId = fields.getField("parentId").asLong();
      if (parentId != 0)
      {
        connect();
      
        // fix group type if parent has type
        /*@lineinfo:generated-code*//*@lineinfo:126^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  item_num  group_type,
//                    name      parent_name
//            from    am_item
//            where   id = :parentId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  item_num  group_type,\n                  name      parent_name\n          from    am_item\n          where   id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.maintenance.AmGroupAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,parentId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.maintenance.AmGroupAction",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:132^9*/
        ResultSet rs = it.getResultSet();
        if (rs.next())
        {
          int groupType = rs.getInt("group_type");
          if (groupType != 0)
          {
            setData("groupType",rs.getString("group_type"));
            setData("fixedTypeFlag","y");
          }
          setData("parentName",rs.getString("parent_name"));
        }
        loadOk = true;
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::addLoad(): "
        + e.toString());
      logEntry("addLoad",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }

    return loadOk;
  }
  
  private boolean editLoad()
  {
    boolean loadOk = false;
    ResultSetIterator it = null;
    
    try
    {
      // parent's group type determines if this group type is fixed
      // load both to figure it out
      long groupId = fields.getField("groupId").asLong();
      if (groupId != 0)
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:176^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  i1.item_num               my_type,
//                    i1.name                   group_name,
//                    nvl(i2.item_num,0)        parent_type,
//                    nvl(i2.name,'undefined')  parent_name
//            from    am_item i1,
//                    am_item i2,
//                    t_hierarchy h
//            where   i1.id = :groupId
//                    and i1.id = h.descendent(+)
//                    and (h.relation = 1 or h.relation is null)
//                    and h.ancestor = i2.id(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  i1.item_num               my_type,\n                  i1.name                   group_name,\n                  nvl(i2.item_num,0)        parent_type,\n                  nvl(i2.name,'undefined')  parent_name\n          from    am_item i1,\n                  am_item i2,\n                  t_hierarchy h\n          where   i1.id =  :1 \n                  and i1.id = h.descendent(+)\n                  and (h.relation = 1 or h.relation is null)\n                  and h.ancestor = i2.id(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.maintenance.AmGroupAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,groupId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.maintenance.AmGroupAction",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:189^9*/
        ResultSet rs = it.getResultSet();
        if (rs.next())
        {
          if (rs.getLong("parent_type") != 0)
          {
            // parent is not root, so use parent's type, set to fixed
            setData("groupType",rs.getString("parent_type"));
            setData("fixedTypeFlag","y");
          }
          else if (rs.getLong("my_type") != 0)
          {
            // parent is root, so use current type, leave as not fixed
            setData("groupType",rs.getString("my_type"));
          }
          else
          {
            // no parent, this is root, set to fixed, 
            setData("groupType","0");
            setData("fixedTypeFlag","y");
          }
          setData("parentName",rs.getString("parent_name"));
          setData("groupName",rs.getString("group_name"));
          loadOk = true;
        }
      }
      
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::editLoad(): "
        + e.toString());
      logEntry("editLoad",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }

    return loadOk;
  }
  
  private boolean deleteLoad()
  {
    boolean loadOk = false;
    ResultSetIterator it = null;
    
    try
    {
      // need to load group name
      long groupId = fields.getField("groupId").asLong();
      if (groupId != 0)
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:245^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(name,'group not found') group_name,
//                    nvl(item_num,0)             group_type
//            from    am_item
//            where   id = :groupId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(name,'group not found') group_name,\n                  nvl(item_num,0)             group_type\n          from    am_item\n          where   id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.maintenance.AmGroupAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,groupId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.maintenance.AmGroupAction",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:251^9*/
        ResultSet rs = it.getResultSet();
        if (rs.next())
        {
          setData("groupName",rs.getString("group_name"));
          setData("groupType",rs.getString("group_type"));
          loadOk = true;
        }
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::deleteLoad(): "
        + e.toString());
      logEntry("deleteLoad",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }

    return loadOk;
  }
  
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    String mode = getData("mode");
    System.out.println("mode=" + mode);
    
    if (mode.equals("add"))
    {
      loadOk = addLoad();
    }
    else if (mode.equals("edit"))
    {
      loadOk = editLoad();
    }
    else if (mode.equals("delete"))
    {
      loadOk = deleteLoad();
    }
    
    return loadOk;
  }
  
  private boolean addSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      connect();
      
      // get hierarchy id for new group
      long groupId = HierarchyTools.getNewNodeId(hierarchyType);
      
      // insert new group item
      String groupName = getData("groupName");
      int groupType = fields.getField("groupType").asInteger();
      /*@lineinfo:generated-code*//*@lineinfo:313^7*/

//  ************************************************************
//  #sql [Ctx] { insert into am_item
//          values
//          ( :groupId,
//            'G',
//            :groupType,
//            :groupName )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into am_item\n        values\n        (  :1 ,\n          'G',\n           :2 ,\n           :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.maintenance.AmGroupAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,groupId);
   __sJT_st.setInt(2,groupType);
   __sJT_st.setString(3,groupName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:321^7*/
      
      // insert into hierarchy
      long parentId = fields.getField("parentId").asLong();
      int entityType = MesHierarchy.ET_AM_GROUP;
      HierarchyTools.insertNode(groupId,entityType,groupName,parentId,hierarchyType);
      
      submitOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::addSubmit(): "
        + e.toString());
      logEntry("addSubmit",e.toString());
      e.printStackTrace();
    }
    finally
    {
      cleanUp();
    }
    
    return submitOk;
  }
  
  private boolean editSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      connect();
      
      // update the group item
      long groupId = fields.getField("groupId").asLong();
      int groupType = fields.getField("groupType").asInteger();
      String groupName = getData("groupName");
      
      /*@lineinfo:generated-code*//*@lineinfo:358^7*/

//  ************************************************************
//  #sql [Ctx] { update  am_item
//          set     item_num  = :groupType,
//                  name      = :groupName
//          where   id = :groupId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  am_item\n        set     item_num  =  :1 ,\n                name      =  :2 \n        where   id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.app.maintenance.AmGroupAction",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,groupType);
   __sJT_st.setString(2,groupName);
   __sJT_st.setLong(3,groupId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:364^7*/
      
      System.out.println("updated group item " + groupId + " with " + groupType + ", " + groupName);
      submitOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::editSubmit(): "
        + e.toString());
      logEntry("editSubmit",e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return submitOk;
  }
  
  private boolean deleteSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      connect();
      
      // delete all items under this node
      long groupId = fields.getField("groupId").asLong();
      /*@lineinfo:generated-code*//*@lineinfo:393^7*/

//  ************************************************************
//  #sql [Ctx] { delete from am_item
//          where id in ( select  descendent
//                        from    t_hierarchy
//                        where   ancestor = :groupId )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from am_item\n        where id in ( select  descendent\n                      from    t_hierarchy\n                      where   ancestor =  :1  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.app.maintenance.AmGroupAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,groupId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:399^7*/
      
      // delete from hierarchy
      HierarchyTools.deleteNode(groupId,hierarchyType);
      
      submitOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::deleteSubmit(): "
        + e.toString());
      logEntry("deleteSubmit",e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return submitOk;
  }

  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    String mode = getData("mode");
    if (mode.equals("add"))
    {
      submitOk = addSubmit();
    }
    else if (mode.equals("edit"))
    {
      submitOk = editSubmit();
    }
    else if (mode.equals("delete"))
    {
      submitOk = deleteSubmit();
    }
    
    return submitOk;
  }
  
  public String getTitle()
  {
    String mode = getData("mode");
    StringBuffer title = new StringBuffer("Association Group");
    if (mode.equals("add"))
    {
      title.insert(0,"Add ");
    }
    else if (mode.equals("edit"))
    {
      title.insert(0,"Edit ");
    }
    else if (mode.equals("delete"))
    {
      title.insert(0,"Delete ");
    }
    return title.toString();
  }
  
  public String getSubtitle()
  {
    String mode = getData("mode");
    String subtitle = "";
    if (mode.equals("add"))
    {
      subtitle = "Add a new subgroup to '" + getData("parentName") + "'";
    }
    else if (mode.equals("edit"))
    {
      subtitle = "Edit group '" + getData("groupName") + "'";
    }
    else if (mode.equals("delete"))
    {
      subtitle = "Delete group '" + getData("groupName") + "'";
    }
    return subtitle;
  }
  
  public boolean isFixedType()
  {
    return getData("fixedTypeFlag").equals("y");
  }
}/*@lineinfo:generated-code*/