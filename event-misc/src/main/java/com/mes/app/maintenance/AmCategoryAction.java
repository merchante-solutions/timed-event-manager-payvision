/*@lineinfo:filename=AmCategoryAction*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/maintenance/AmCategoryAction.sqlj $

  AmCategoryAction
  
  Adds, deletes association categories.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 8/12/04 9:54a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.maintenance;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesHierarchy;
import com.mes.forms.DropDownField;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.tools.DropDownTable;
import com.mes.tools.HierarchyTools;
import sqlj.runtime.ResultSetIterator;


public class AmCategoryAction extends FieldBean
{
  private int hierarchyType = MesHierarchy.HT_ASSOC_MAPPINGS;
  
  public class CategoryTypeTable extends DropDownTable
  {
    public CategoryTypeTable(HttpServletRequest request) throws Exception
    {
      addElement("","select one");
      
      long parentId = 0;
      try
      {
        parentId = Long.parseLong((String)request.getParameter("parentId"));
      }
      catch (Exception e) {}
      
      if (parentId != 0)
      {
        ResultSetIterator it = null;
        try
        {      
          connect();
          /*@lineinfo:generated-code*//*@lineinfo:67^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  c.cat_id,
//                      c.name
//              from    am_cat c,
//                      am_item i
//              where   i.id = :parentId
//                      and i.item_num = c.type_id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  c.cat_id,\n                    c.name\n            from    am_cat c,\n                    am_item i\n            where   i.id =  :1 \n                    and i.item_num = c.type_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.maintenance.AmCategoryAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,parentId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.maintenance.AmCategoryAction",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:75^11*/
          ResultSet rs = it.getResultSet();

          while(rs.next())
          {
            addElement(rs);
          }
        }
        finally
        {
          try{ it.close(); } catch(Exception e) { }
          cleanUp();
        }
      }
    }
  }
  
  public AmCategoryAction()
  {
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField("mode"));
      fields.add(new HiddenField("parentId"));
      fields.add(new HiddenField("parentName"));
      fields.add(new HiddenField("parentType"));
      fields.add(new HiddenField("categoryId"));
      fields.add(new HiddenField("categoryName"));
      fields.add(new DropDownField("categoryType",new CategoryTypeTable(request),false));
      
      fields.add(new ModeButtonField("submitter",fields.getField("mode")));
      
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
  
  /*
  ** Given a parent group id, loads parent name, parent type and creates
  ** a category picker for the parent type.
  */
  private boolean addLoad()
  {
    boolean loadOk = false;
    ResultSetIterator it = null;
    
    try
    {
      // determine parent type (or lack thereof)
      // and load parent name
      long parentId = fields.getField("parentId").asLong();
      if (parentId != 0)
      {
        connect();
      
        // determine parent group's type and name
        /*@lineinfo:generated-code*//*@lineinfo:141^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  item_num  parent_type,
//                    name      parent_name
//            from    am_item
//            where   id = :parentId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  item_num  parent_type,\n                  name      parent_name\n          from    am_item\n          where   id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.maintenance.AmCategoryAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,parentId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.maintenance.AmCategoryAction",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:147^9*/
        ResultSet rs = it.getResultSet();
        if (rs.next())
        {
          setData("parentType",rs.getString("parent_type"));
          setData("parentName",rs.getString("parent_name"));
        }

        loadOk = true;
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::addLoad(): "
        + e.toString());
      logEntry("addLoad",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }

    return loadOk;
  }
  
  /*
  ** Given a parent group id, loads parent name, parent type and creates
  */
  private boolean deleteLoad()
  {
    boolean loadOk = false;
    ResultSetIterator it = null;
    
    try
    {
      // need to load group name
      long categoryId = fields.getField("categoryId").asLong();
      if (categoryId != 0)
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:189^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(name,'category not found')  category_name,
//                    nvl(item_num,0)                 category_type
//            from    am_item
//            where   id = :categoryId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(name,'category not found')  category_name,\n                  nvl(item_num,0)                 category_type\n          from    am_item\n          where   id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.maintenance.AmCategoryAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,categoryId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.maintenance.AmCategoryAction",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:195^9*/
        ResultSet rs = it.getResultSet();
        if (rs.next())
        {
          setData("categoryName",rs.getString("category_name"));
          setData("categoryType",rs.getString("category_type"));
          loadOk = true;
        }
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::deleteLoad(): "
        + e.toString());
      logEntry("deleteLoad",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }

    return loadOk;
  }
  
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    String mode = getData("mode");
    if (mode.equals("add"))
    {
      loadOk = addLoad();
    }
    else if (mode.equals("delete"))
    {
      loadOk = deleteLoad();
    }
    
    return loadOk;
  }
  
  private boolean addSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      connect();
      
      // get hierarchy id for new category
      long categoryId = HierarchyTools.getNewNodeId(hierarchyType);
      
      // insert new group item
      DropDownField catField  = (DropDownField)fields.getField("categoryType");
      int categoryType        = catField.asInteger();
      String categoryName     = catField.getDescription();
      /*@lineinfo:generated-code*//*@lineinfo:252^7*/

//  ************************************************************
//  #sql [Ctx] { insert into am_item
//          values
//          ( :categoryId,
//            'C',
//            :categoryType,
//            :categoryName )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into am_item\n        values\n        (  :1 ,\n          'C',\n           :2 ,\n           :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.maintenance.AmCategoryAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,categoryId);
   __sJT_st.setInt(2,categoryType);
   __sJT_st.setString(3,categoryName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:260^7*/
      
      // insert into hierarchy
      long parentId = fields.getField("parentId").asLong();
      int entityType = MesHierarchy.ET_AM_CATEGORY;
      HierarchyTools.insertNode(categoryId,entityType,categoryName,parentId,hierarchyType);
      
      submitOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::addSubmit(): "
        + e.toString());
      logEntry("addSubmit",e.toString());
      e.printStackTrace();
    }
    finally
    {
      cleanUp();
    }
    
    return submitOk;
  }
  
  private boolean deleteSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      connect();
      
      // delete all items under this node
      long categoryId = fields.getField("categoryId").asLong();
      /*@lineinfo:generated-code*//*@lineinfo:294^7*/

//  ************************************************************
//  #sql [Ctx] { delete from am_item
//          where id in ( select  descendent
//                        from    t_hierarchy
//                        where   ancestor = :categoryId )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from am_item\n        where id in ( select  descendent\n                      from    t_hierarchy\n                      where   ancestor =  :1  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.maintenance.AmCategoryAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,categoryId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:300^7*/
      
      // delete from hierarchy
      HierarchyTools.deleteNode(categoryId,hierarchyType);
      
      submitOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::deleteSubmit(): "
        + e.toString());
      logEntry("deleteSubmit",e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return submitOk;
  }

  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    String mode = getData("mode");
    if (mode.equals("add"))
    {
      submitOk = addSubmit();
    }
    else if (mode.equals("delete"))
    {
      submitOk = deleteSubmit();
    }
    
    return submitOk;
  }
  
  public String getTitle()
  {
    String mode = getData("mode");
    StringBuffer title = new StringBuffer("Association Category");
    if (mode.equals("add"))
    {
      title.insert(0,"Add ");
    }
    else if (mode.equals("delete"))
    {
      title.insert(0,"Delete ");
    }
    return title.toString();
  }
  
  public String getSubtitle()
  {
    String mode = getData("mode");
    String subtitle = "";
    if (mode.equals("add"))
    {
      subtitle = "Add a new category to '" + getData("parentName") + "'";
    }
    else if (mode.equals("delete"))
    {
      subtitle = "Delete category '" + getData("categoryName") + "'";
    }
    return subtitle;
  }
  
  public boolean isFixedType()
  {
    return getData("fixedTypeFlag").equals("y");
  }
}/*@lineinfo:generated-code*/