/*@lineinfo:filename=HierarchyLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/maintenance/HierarchyLoader.sqlj $

  HierarchyLoader
  
  Given a root and a hierarchy type this will load the hierarchy below
  the root node.  The hierarchy is loaded into memory in the form of linked
  HierarchyNode objects.  A HierarchyRenderer can then use this loader object
  to display the hierarchy.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 10/20/03 5:15p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.maintenance;

import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Iterator;
import com.mes.constants.MesHierarchy;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class HierarchyLoader extends SQLJConnectionBase
{
  private boolean _isLoaded = false;
  
  public HierarchyLoader()
  {
  }
  
  private HierarchyNode rootNode  = null;
  private Hashtable     nodeHash  = new Hashtable();
  
  /*
  ** protected ResultSetIterator getQueryData(int hierarchyType, long rootId)
  **
  ** Runs query to get all hierarchy data below the given rootId with
  ** the hierarchyType specified.  This class can be overridden by
  ** custom loaders when a custom node type needs additional data.
  **
  ** RETURNS: ResultSetIterator containing the hierarchy node data.
  */
  protected ResultSetIterator getQueryData(int hierarchyType, long rootId)
  {
    ResultSetIterator it = null;
    try
    {
      // select all hierarchy nodes below the root id 
      /*@lineinfo:generated-code*//*@lineinfo:68^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    h2.ancestor                     parent_id,
//                    h2.descendent                   node_id,
//                    h1.relation                     depth,
//                    nvl(hn.name,'(no name given)')  name,
//                    h2.hier_type                    hierarchy_type,
//                    h2.entity_type                  node_type
//                    
//          from      t_hierarchy       h1,
//                    t_hierarchy       h2,
//                    t_hierarchy_names hn
//                    
//          where     h1.ancestor = :rootId
//                    and h1.entity_type <> :MesHierarchy.ET_MERCHANT
//                    and h1.relation > 0
//                    and h1.descendent = h2.descendent
//                    and h1.hier_type = h2.hier_type
//                    and h2.relation = 1
//                    and h2.entity_type <> :MesHierarchy.ET_MERCHANT
//                    and h2.descendent = hn.hier_id(+)
//                    
//          order by  h1.relation,
//                    hn.name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    h2.ancestor                     parent_id,\n                  h2.descendent                   node_id,\n                  h1.relation                     depth,\n                  nvl(hn.name,'(no name given)')  name,\n                  h2.hier_type                    hierarchy_type,\n                  h2.entity_type                  node_type\n                  \n        from      t_hierarchy       h1,\n                  t_hierarchy       h2,\n                  t_hierarchy_names hn\n                  \n        where     h1.ancestor =  :1 \n                  and h1.entity_type <>  :2 \n                  and h1.relation > 0\n                  and h1.descendent = h2.descendent\n                  and h1.hier_type = h2.hier_type\n                  and h2.relation = 1\n                  and h2.entity_type <>  :3 \n                  and h2.descendent = hn.hier_id(+)\n                  \n        order by  h1.relation,\n                  hn.name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.maintenance.HierarchyLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,rootId);
   __sJT_st.setInt(2,MesHierarchy.ET_MERCHANT);
   __sJT_st.setInt(3,MesHierarchy.ET_MERCHANT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.maintenance.HierarchyLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:92^7*/
    }
    catch (Exception e)
    {
      String sig = "getQueryData(hierarchyType=" + hierarchyType
        + ", rootId=" + rootId +")";
      System.out.println(this.getClass().getName()
        + "::" + sig + ": " + e.toString());
      logEntry(sig,e.toString());
    }
    return it;
  }
  
  /*
  ** protected HierarchyNode getRootNode(long rootId, String rootName,
  **   int hierarchyType)
  **
  ** Creates a node to represent the root node of the hierarchy section
  ** to be loaded.  Can be overloaded by custom loaders to return a
  ** custom node type.
  **
  ** RETURN: a HierarchyNode containing the root node information.
  */
  protected HierarchyNode getRootNode(long rootId, String rootName,
    int hierarchyType)
  {
    return new HierarchyNode(rootId,rootName,-1L,0,hierarchyType,-1);
  }
  
  /*
  ** protected HierarchyNode getNewNode(ResultSet rs)
  **
  ** Generates a new hierarchy node with the current row of the given
  ** result set.  This class can be overridden when a custom node type
  ** needs to be used in custom loaders.
  **
  ** RETURNS: generated HierarchyNode object.
  */
  protected HierarchyNode getNewNode(ResultSet rs)
  {
    HierarchyNode node = null;
    try
    {
      node = new HierarchyNode(rs);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::loadHierarchy(new node): " + e.toString());
      logEntry("loadHierarchy(new node)",e.toString());
    }
    return node;
  }

  public class NodeMarker
  {
    private long nodeId;
    
    public NodeMarker(long nodeId)
    {
      this.nodeId = nodeId;
    }
    
    public long getNodeId()
    {
      return nodeId;
    }
    
    public Long getKey()
    {
      return nodeId;
    }
  }
  
  private Hashtable openHash = null;
  
  private void syncViewState()
  {
    // make sure a view state hash table exists
    if (openHash == null)
    {
      openHash = new Hashtable();
    }
    
    // scan through open hash, remove any nodes that are no longer
    // in the hierarchy
    for (Iterator i = openHash.values().iterator(); i.hasNext();)
    {
      NodeMarker marker = (NodeMarker)i.next();
      if (nodeHash.get(marker.getKey()) == null)
      {
        i.remove();
      }
    }
  }
  
  /*
  ** protected HierarchyNode loadHierarchy(int hierarchyType, long rootId, 
  **   String rootName)
  **
  ** Requires a hierarchy type, root id and root name.
  **
  ** RETURNS: root node of loaded hierarchy.
  */
  protected HierarchyNode loadHierarchy(int hierarchyType, long rootId, 
    String rootName)
  {
    int prog = 0;
    ResultSetIterator it = null;
    _isLoaded = false;
    
    try
    {
      connect();
      
      // create a root node based on hierarchy type
      rootNode = getRootNode(rootId,rootName,hierarchyType);
      nodeHash.put(rootId, rootNode);
      
      prog = 1;
      
      // ordered by
      // distance from root to node
      it = getQueryData(hierarchyType,rootId);
      if (it != null)
      {
        ResultSet rs = it.getResultSet();
      
        prog = 2;
      
        // build/refresh hierarchy
        while (rs.next())
        {
          // create a node
          HierarchyNode node = getNewNode(rs);
        
          // add it into hierarchy storage
          if (node != null)
          {
            // put node in hash table
            nodeHash.put(node.getNodeId(), node);
    
            // locate parent
            HierarchyNode parent = (HierarchyNode) nodeHash.get(node.getParentId());
            if (parent == null)
            {
              System.out.println("could not find parent " + node.getParentId() 
                + " of child " + node.getNodeId());
            }
      
            // add node as child to parent
            parent.add(node);
          }
        }
        
        // synchronize view state
        syncViewState();
      
        _isLoaded = true;
      }
    }
    catch (Exception e)
    {
      String sig = "loadHierarchy(prog=" + prog +")";
      System.out.println(this.getClass().getName() + "::" + sig + ": "
        + e.toString());
      logEntry(sig,e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
    
    return rootNode;
  }
  
  /*
  ** public HierarchyNode loadHierarchyByType(int hierarchyType) throws Exception
  **
  ** Loads the entire hierarchy of a given hierarchy type.
  **
  ** RETURNS: root node of the loaded hierarchy.
  */
  public HierarchyNode loadHierarchyByType(int hierarchyType) throws Exception
  {
    long    rootId;
    String  rootName;
      
    // load the root id and name from the hierarchy type table
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:287^7*/

//  ************************************************************
//  #sql [Ctx] { select  root_id,
//                  name
//  
//          
//  
//          from    t_hierarchy_types
//  
//          where   hier_type = :hierarchyType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  root_id,\n                name\n\n         \n\n        from    t_hierarchy_types\n\n        where   hier_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.maintenance.HierarchyLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,hierarchyType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rootId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   rootName = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:298^7*/
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::loadHierarchyByType(): "
        + e.toString());
      logEntry("loadHierarchyByType",e.toString());
      throw new Exception("Unable to load hierarchy for hierarchy type " 
        + hierarchyType);
    }
    finally
    {
      cleanUp();
    }
    
    // load the hierarchy with the determined root info
    return loadHierarchy(hierarchyType,rootId,rootName);
  }
      
  /*
  ** public HierarchyNode loadHierarchyWithRoot(long rootId, int hierarchyType)
  **   throws Exception
  **
  ** Loads the hierarchy nodes beneath a given root id.
  **
  ** RETURNS: root node of the loaded hierarchy.
  */
  public HierarchyNode loadHierarchyWithRoot(long rootId, int hierarchyType)
    throws Exception
  {
    String  rootName;
      
    // load the root name from the hierarchy names table
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:335^7*/

//  ************************************************************
//  #sql [Ctx] { select  name
//          
//          from    t_hierarchy_names
//          where   hier_id = :rootId
//                  and hier_type = :hierarchyType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  name\n         \n        from    t_hierarchy_names\n        where   hier_id =  :1 \n                and hier_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.maintenance.HierarchyLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,rootId);
   __sJT_st.setInt(2,hierarchyType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rootName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:342^7*/
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::loadHierarchyWithRoot(): "
        + e.toString());
      logEntry("loadHierarchyWithRoot",e.toString());
      throw new Exception("Unable to load hierarchy with rootId " + rootId 
        +", hierarchyType " + hierarchyType);
    }
    finally
    {
      cleanUp();
    }
    
    // load the hierarchy with the determined root info
    return loadHierarchy(hierarchyType,rootId,rootName);
  }
  
  public boolean isLoaded() { return _isLoaded; }
  
  public HierarchyNode getRootNode() { return rootNode; }
  
  public boolean nodeIsOpen(HierarchyNode node)
  {
    Long key = node.getNodeId();
    return openHash.get(key) != null;
  }
  
  public boolean nodeIsOpen(long nodeId)
  {
    return openHash.get(nodeId) != null;
  }
  
  public void toggleNode(long nodeId)
  { 
    // look for the node state in the open hash table
    NodeMarker marker = (NodeMarker)openHash.get(nodeId);
    if (marker != null)
    {
      // node is marked as open so remove it from the hash
      openHash.remove(nodeId);
    }
    else
    {
      // node is not open, add a marker for it to the open hash
      marker = new NodeMarker(nodeId);
      openHash.put(nodeId, marker);
    }
  }
  
  public void toggleNode(HierarchyNode node)
  {
    toggleNode(node.getNodeId());
  }
}/*@lineinfo:generated-code*/