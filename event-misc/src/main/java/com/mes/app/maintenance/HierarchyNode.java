/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/maintenance/HierarchyNode.java $

  HierarchyNode
  
  Base class of a hierarchy node used by hierarchy loaders and renderers.
  Implements default storage and rendering that can by extended by custom
  node classes.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 10/20/03 5:15p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.maintenance;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.forms.HtmlBlock;

public class HierarchyNode extends Vector
{
  private String  nodeName;
  private long    nodeId;
  private long    parentId;
  private int     depth;
  private int     hierarchyType;
  private int     nodeType;
  
  private boolean isRefreshed = true;
  private boolean _isOpen = false;
  
  public HierarchyNode(long nodeId, String nodeName, long parentId,
    int depth, int hierarchyType, int nodeType)
  {
    this.nodeId         = nodeId;
    this.nodeName       = nodeName;
    this.parentId       = parentId;
    this.depth          = depth;
    this.hierarchyType  = hierarchyType;
    this.nodeType       = nodeType;
  }
  
  public HierarchyNode(ResultSet rs) throws Exception
  {
    try
    {
      nodeId        = rs.getLong  ("node_id");
      nodeName      = rs.getString("name");
      parentId      = rs.getLong  ("parent_id");
      depth         = rs.getInt   ("depth");
      hierarchyType = rs.getInt   ("hierarchy_type");
      nodeType      = rs.getInt   ("node_type");
    }
    catch (Exception e)
    {
      String eMsg = this.getClass().getName() + "::HierarchyNode(): "
        + e.toString();
      System.out.println(eMsg);
      throw new Exception(eMsg);
    }
  }
  
  public String toString()
  {
    return nodeName + " (" + nodeId + ") " + parentId + " " + depth + " " 
      + hierarchyType;
  }
  
  public long     getNodeId()         { return nodeId;        }
  public long     getParentId()       { return parentId;      }
  public int      getDepth()          { return depth;         }
  public String   getNodeName()       { return nodeName;      }
  public int      getHierarchyType()  { return hierarchyType; }
  public int      getNodeType()       { return nodeType;      }
  public boolean  isOpen()            { return _isOpen;       }
  
  public void open()
  {
    _isOpen = true;
  };
  public void close()
  {
    _isOpen = false;
  }
  public void toggle()
  {
    _isOpen = !_isOpen;
  }
  
  public HtmlBlock getHtmlBlock()
  {
    HtmlBlock block = new HtmlBlock();
    block.add(nodeName + "&nbsp;(" + nodeId + ", " + hierarchyType + ")");
    return block;
  }
}

