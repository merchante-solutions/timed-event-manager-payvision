/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/maintenance/ModeButtonField.java $

  ModeButtonField
  
  Defines a button with a dependency on a "mode" field.  The mode field is
  used at render time to display a button with text reflecting the following
  mode mappings: "add" -> "Add", "edit" -> "Save", "delete" -> "Delete".

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 8/12/04 12:00p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.maintenance;

import com.mes.forms.ButtonField;
import com.mes.forms.Field;

public class ModeButtonField extends ButtonField
{
  private Field modeField = null;
  
  public ModeButtonField(String fname, Field modeField)
  {
    super(fname);
    
    this.modeField = modeField;
  }
  
  public String renderHtmlField()
  {
    String mode = modeField.getData();
    
    if (mode.equals("add"))
    {
      buttonText = "Add";
    }
    else if (mode.equals("edit"))
    {
      buttonText = "Save";
    }
    else if (mode.equals("delete"))
    {
      buttonText = "Delete";
    }
    else
    {
      buttonText = "Submit";
    }
    
    return super.renderHtmlField();
  }
}
  
