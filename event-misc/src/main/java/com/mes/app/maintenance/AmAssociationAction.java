/*@lineinfo:filename=AmAssociationAction*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/maintenance/AmAssociationAction.sqlj $

  AmAssociationAction
  
  Adds, edits and deletes association groups.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 8/12/04 9:54a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.maintenance;

import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesHierarchy;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.tools.HierarchyTools;
import sqlj.runtime.ResultSetIterator;


public class AmAssociationAction extends FieldBean
{
  private int hierarchyType = MesHierarchy.HT_ASSOC_MAPPINGS;
  
  public AmAssociationAction()
  {
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField("mode"));
      fields.add(new HiddenField("parentId"));
      fields.add(new HiddenField("parentName"));
      fields.add(new HiddenField("assocId"));
      fields.add(new Field("assocName",40,40,false));
      fields.add(new Field("assocNum",6,6,false));
      
      fields.add(new ModeButtonField("submitter",fields.getField("mode")));
      
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
  
  /*
  ** needs to load parent name using the parent id
  */
  private boolean addLoad()
  {
    boolean loadOk = false;
    ResultSetIterator it = null;
    
    try
    {
      // determine parent type (or lack thereof)
      // and load parent name
      long parentId = fields.getField("parentId").asLong();
      if (parentId != 0)
      {
        connect();
      
        /*@lineinfo:generated-code*//*@lineinfo:94^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  name parent_name
//            from    am_item
//            where   id = :parentId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  name parent_name\n          from    am_item\n          where   id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.maintenance.AmAssociationAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,parentId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.maintenance.AmAssociationAction",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:99^9*/
        setFields(it.getResultSet());
        loadOk = true;
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::addLoad(): "
        + e.toString());
      logEntry("addLoad",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }

    return loadOk;
  }
  
  /*
  ** needs to load parent id, parent name, assoc num, assoc name using
  ** assoc id
  */
  private boolean editLoad()
  {
    boolean loadOk = false;
    ResultSetIterator it = null;
    
    try
    {
      long assocId = fields.getField("assocId").asLong();
      if (assocId != 0)
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:135^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  i1.item_num assoc_num,
//                    i1.name     assoc_name,
//                    i2.id       parent_id,
//                    i2.name     parent_name
//            from    am_item i1,
//                    am_item i2,
//                    t_hierarchy h
//            where   i1.id = :assocId
//                    and i1.id = h.descendent(+)
//                    and (h.relation = 1 or h.relation is null)
//                    and h.ancestor = i2.id(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  i1.item_num assoc_num,\n                  i1.name     assoc_name,\n                  i2.id       parent_id,\n                  i2.name     parent_name\n          from    am_item i1,\n                  am_item i2,\n                  t_hierarchy h\n          where   i1.id =  :1 \n                  and i1.id = h.descendent(+)\n                  and (h.relation = 1 or h.relation is null)\n                  and h.ancestor = i2.id(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.maintenance.AmAssociationAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,assocId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.maintenance.AmAssociationAction",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:148^9*/
        setFields(it.getResultSet());
        loadOk = true;
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::editLoad(): "
        + e.toString());
      logEntry("editLoad",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }

    return loadOk;
  }
  
  /*
  ** needs to load assoc num and assoc name using assoc id
  */
  private boolean deleteLoad()
  {
    boolean loadOk = false;
    ResultSetIterator it = null;
    
    try
    {
      // need to load group name
      long assocId = fields.getField("assocId").asLong();
      if (assocId != 0)
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:184^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(name,'assoc not found') assoc_name,
//                    nvl(item_num,0)             assoc_num
//            from    am_item
//            where   id = :assocId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(name,'assoc not found') assoc_name,\n                  nvl(item_num,0)             assoc_num\n          from    am_item\n          where   id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.maintenance.AmAssociationAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,assocId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.maintenance.AmAssociationAction",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:190^9*/
        setFields(it.getResultSet());
        loadOk = true;
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::deleteLoad(): "
        + e.toString());
      logEntry("deleteLoad",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }

    return loadOk;
  }
  
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    String mode = getData("mode");
    if (mode.equals("add"))
    {
      loadOk = addLoad();
    }
    else if (mode.equals("edit"))
    {
      loadOk = editLoad();
    }
    else if (mode.equals("delete"))
    {
      loadOk = deleteLoad();
    }
    
    return loadOk;
  }
  
  private boolean addSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      connect();
      
      // get hierarchy id for new group
      long assocId = HierarchyTools.getNewNodeId(hierarchyType);
      
      // insert new group item
      String assocName = getData("assocName");
      int assocNum = fields.getField("assocNum").asInteger();
      /*@lineinfo:generated-code*//*@lineinfo:245^7*/

//  ************************************************************
//  #sql [Ctx] { insert into am_item
//          values
//          ( :assocId,
//            'A',
//            :assocNum,
//            :assocName )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into am_item\n        values\n        (  :1 ,\n          'A',\n           :2 ,\n           :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.maintenance.AmAssociationAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,assocId);
   __sJT_st.setInt(2,assocNum);
   __sJT_st.setString(3,assocName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:253^7*/
      
      // insert into hierarchy
      long parentId = fields.getField("parentId").asLong();
      int entityType = MesHierarchy.ET_AM_ASSOC;
      HierarchyTools.insertNode(assocId,entityType,assocName,parentId,hierarchyType);
      
      submitOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::addSubmit(): "
        + e.toString());
      logEntry("addSubmit",e.toString());
      e.printStackTrace();
    }
    finally
    {
      cleanUp();
    }
    
    return submitOk;
  }
  
  private boolean editSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      connect();
      
      // update the group item
      long assocId = fields.getField("assocId").asLong();
      int assocNum = fields.getField("assocNum").asInteger();
      String assocName = getData("assocName");
      
      /*@lineinfo:generated-code*//*@lineinfo:290^7*/

//  ************************************************************
//  #sql [Ctx] { update  am_item
//          set     item_num  = :assocNum,
//                  name      = :assocName
//          where   id = :assocId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  am_item\n        set     item_num  =  :1 ,\n                name      =  :2 \n        where   id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.maintenance.AmAssociationAction",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,assocNum);
   __sJT_st.setString(2,assocName);
   __sJT_st.setLong(3,assocId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:296^7*/
      
      submitOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::editSubmit(): "
        + e.toString());
      logEntry("editSubmit",e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return submitOk;
  }
  
  private boolean deleteSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      connect();
      
      // delete all items under this node
      long assocId = fields.getField("assocId").asLong();
      /*@lineinfo:generated-code*//*@lineinfo:324^7*/

//  ************************************************************
//  #sql [Ctx] { delete from am_item
//          where id in ( select  descendent
//                        from    t_hierarchy
//                        where   ancestor = :assocId )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from am_item\n        where id in ( select  descendent\n                      from    t_hierarchy\n                      where   ancestor =  :1  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.app.maintenance.AmAssociationAction",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,assocId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:330^7*/
      
      // delete from hierarchy
      HierarchyTools.deleteNode(assocId,hierarchyType);
      
      submitOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::deleteSubmit(): "
        + e.toString());
      logEntry("deleteSubmit",e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return submitOk;
  }

  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    String mode = getData("mode");
    if (mode.equals("add"))
    {
      submitOk = addSubmit();
    }
    else if (mode.equals("edit"))
    {
      submitOk = editSubmit();
    }
    else if (mode.equals("delete"))
    {
      submitOk = deleteSubmit();
    }
    
    return submitOk;
  }
  
  public String getTitle()
  {
    String mode = getData("mode");
    StringBuffer title = new StringBuffer("Association");
    if (mode.equals("add"))
    {
      title.insert(0,"Add ");
    }
    else if (mode.equals("edit"))
    {
      title.insert(0,"Edit ");
    }
    else if (mode.equals("delete"))
    {
      title.insert(0,"Delete ");
    }
    return title.toString();
  }
  
  public String getSubtitle()
  {
    String mode = getData("mode");
    String subtitle = "";
    if (mode.equals("add"))
    {
      subtitle = "Add a new association to '" + getData("parentName") + "'";
    }
    else if (mode.equals("edit"))
    {
      subtitle = "Edit association '" + getData("assocName") + "'";
    }
    else if (mode.equals("delete"))
    {
      subtitle = "Delete association '" + getData("assocName") + "'";
    }
    return subtitle;
  }
}/*@lineinfo:generated-code*/