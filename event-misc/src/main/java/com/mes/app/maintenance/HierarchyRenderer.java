/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/maintenance/HierarchyRenderer.java $

  HierarchyRenderer
  
  Displays the hierarchy stored in a HierarchyLoader object.  Given a
  loaded object it will display the hierarchy contained in it in tree
  form with collapsable and expandable branches.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 8/12/04 9:54a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.maintenance;

import java.util.Iterator;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.HtmlBlock;
import com.mes.forms.HtmlImage;
import com.mes.forms.HtmlLink;
import com.mes.forms.HtmlNobr;
import com.mes.forms.HtmlRenderable;
import com.mes.forms.HtmlTable;
import com.mes.forms.HtmlTableCell;
import com.mes.forms.HtmlTableRow;

public class HierarchyRenderer extends SQLJConnectionBase
{
  private HierarchyLoader loader = null;
  private String pageLink = null;
  
  public static final int NO_CHILD_NO_SIB           = 0;
  public static final int OPEN_ROOT_NODE            = 1;
  public static final int CLOSED_ROOT_NODE          = 2;
  public static final int OPEN_CHILD_WITH_SIB       = 3;
  public static final int CLOSED_CHILD_WITH_SIB     = 4;
  public static final int NO_CHILD_WITH_SIB         = 5;
  public static final int OPEN_CHILD_WITH_NO_SIB    = 6;
  public static final int CLOSED_CHILD_WITH_NO_SIB  = 7;
  public static final int EMPTY_CHILD_WITH_NO_SIB   = 8;
  public static final int EMPTY_CHILD_WITH_SIB      = 9;
  
  public static final int IMAGE_COUNT               = 10;
  
  private HtmlImage[] imageTags = null;
  
  public class ImageConfig
  {
    public String path;
    public int width;
    public int height;
    
    public ImageConfig(String path, int width, int height)
    {
      this.path   = path;
      this.width  = width;
      this.height = height;
    }
  }
  
  private ImageConfig[] defaultImages =
  {
    new ImageConfig("/images/treeview/blank_space.gif",      20,20),
    new ImageConfig("/images/treeview/open_root.gif",        20,20),
    new ImageConfig("/images/treeview/closed_root.gif",      20,20),
    new ImageConfig("/images/treeview/open_child_sib.gif",   20,20),
    new ImageConfig("/images/treeview/closed_child_sib.gif", 20,20),
    new ImageConfig("/images/treeview/no_child_sib.gif",     20,20),
    new ImageConfig("/images/treeview/open_child.gif",       20,20),
    new ImageConfig("/images/treeview/closed_child.gif",     20,20),
    new ImageConfig("/images/treeview/empty_child.gif",      20,20),
    new ImageConfig("/images/treeview/empty_child_sib.gif",  20,20)
  };
  
  public HierarchyRenderer()
  {
    imageTags = new HtmlImage[IMAGE_COUNT];
    for (int i = 0; i < IMAGE_COUNT; ++i)
    {
      imageTags[i] = new HtmlImage();
      imageTags[i].setImage (defaultImages[i].path);
      imageTags[i].setWidth (defaultImages[i].width);
      imageTags[i].setHeight(defaultImages[i].height);
      imageTags[i].setProperty("hspace","0");
      imageTags[i].setProperty("vspace","0");
      imageTags[i].setProperty("align","absmiddle");
      imageTags[i].setProperty("border","0");
    }
  }
  
  // node types
  private int nodeCount = 0;
  
  public HtmlBlock getImageTags(long nodeId, int[] treeCols)
  {
    HtmlBlock imageBlob = new HtmlBlock();
    for (int i = 0; i < treeCols.length; ++i)
    {
      boolean isEmpty = (treeCols[i] == EMPTY_CHILD_WITH_NO_SIB
                        || treeCols[i] == EMPTY_CHILD_WITH_SIB);
      boolean isLink  = (!isEmpty && i == treeCols.length - 1);
      HtmlRenderable imageTag = null;
      if (isLink)
      {
        imageTag = new HtmlLink(pageLink + "?toggleId=" + nodeId);
        ((HtmlLink)imageTag).add(imageTags[treeCols[i]]);
      }
      else
      {
        imageTag = imageTags[treeCols[i]];
      }
      imageBlob.add(imageTag);
    }
    return imageBlob;
  }
  
  protected HtmlBlock getNodeData(HierarchyNode node)
  {
    HtmlBlock block = new HtmlBlock();
    block.add(node.getNodeName() + "&nbsp;(" + node.getNodeId() 
      + ", " + node.getHierarchyType() + ")");
    return block;
  }
      
  private HtmlTableRow getNodeRow(HierarchyNode node,int[] treeCols)
  {
    HtmlTableRow row = new HtmlTableRow();
    row.setProperty("valign","middle");
    
    HtmlTableCell cell = row.add(new HtmlTableCell());
    HtmlNobr      nobr = (HtmlNobr)cell.add(new HtmlNobr());
    
    nobr.add(getImageTags(node.getNodeId(),treeCols));
    nobr.add(node.getHtmlBlock());

    cell.setSuppressFormattingDeep(true);
    cell.setProperty("class","smallText");
    
    return row;
  }
      
  public HtmlBlock getChildren(HierarchyNode node, int[] parentTreeCols,
    boolean hasSib)
  {
    HtmlBlock children = new HtmlBlock();
    
    try
    {
      ++nodeCount;
      
      // initialize an array of tree column status ints
      int treeCols[] = new int[parentTreeCols.length + 1];
      for (int i = 0; i < parentTreeCols.length; ++i)
      {
        switch (parentTreeCols[i])
        {
          case NO_CHILD_NO_SIB:
          case OPEN_ROOT_NODE:
          case CLOSED_ROOT_NODE:
          case OPEN_CHILD_WITH_NO_SIB:
          case CLOSED_CHILD_WITH_NO_SIB:
          case EMPTY_CHILD_WITH_NO_SIB:
            treeCols[i] = NO_CHILD_NO_SIB;
            break;
          
          case OPEN_CHILD_WITH_SIB:
          case CLOSED_CHILD_WITH_SIB:
          case EMPTY_CHILD_WITH_SIB:
          case NO_CHILD_WITH_SIB:
            treeCols[i] = NO_CHILD_WITH_SIB;
            break;
        }
      }
      boolean isEmpty   = (node.size() <= 0);
      boolean isOpen    = loader.nodeIsOpen(node);
      treeCols[treeCols.length - 1] =
        ( isEmpty ?
          ( hasSib ? EMPTY_CHILD_WITH_SIB : EMPTY_CHILD_WITH_NO_SIB ) :
          ( isOpen ? 
            ( hasSib ? OPEN_CHILD_WITH_SIB : OPEN_CHILD_WITH_NO_SIB ) :
            ( hasSib ? CLOSED_CHILD_WITH_SIB : CLOSED_CHILD_WITH_NO_SIB )
          )
        );
      
      // add node row
      children.add(getNodeRow(node,treeCols));
      
      // add child rows
      if (isOpen)
      {
        for (Iterator i = node.iterator(); i.hasNext(); )
        {
          HierarchyNode childNode = (HierarchyNode)i.next();
          children.addContainerContents(getChildren(childNode,treeCols,i.hasNext()));
        }
      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::getChildren(): "
        + e.toString());
      logEntry("getChildren",e.toString());
    }

    return children;
  }
  
  private String doRender()
  {
    HtmlTable tbl = new HtmlTable();
    nodeCount = 1;
    
    try
    {
      // get the root node from the loader
      HierarchyNode node = loader.getRootNode();
      
      // initialize an array of tree column ints (just 1 for root node)
      int treeCols[] = new int[1];
      boolean isOpen = loader.nodeIsOpen(node);
      treeCols[0] = (isOpen ? OPEN_ROOT_NODE : CLOSED_ROOT_NODE);
      
      // set table properties
      tbl.setProperty("border","0");
      tbl.setProperty("width","100%");
      tbl.setProperty("height","100%");
      tbl.setProperty("cellspacing","0");
      tbl.setProperty("cellpadding","0");
      
      // add root node row
      tbl.add(getNodeRow(node,treeCols));
      
      // add child rows
      if (isOpen)
      {
        for (Iterator i = node.iterator(); i.hasNext(); )
        {
          HierarchyNode childNode = (HierarchyNode)i.next();
          tbl.addContainerContents(getChildren(childNode,treeCols,i.hasNext()));
        }
      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::doRender(): "
        + e.toString());
      logEntry("doRender",e.toString());
    }
    
    return tbl.renderHtml();
  }
  
  /*
  ** public String renderHtml(HierarchyLoader loader pageLink) throws Exception
  **
  ** Given an HierarchyLoader this will cause the hierarchy to be rendered
  ** in the form of an html table.  The page link provides an url that the
  ** rendered html will use when rendering collapse/expand links (i.e. the
  ** page that is calling this).
  **
  ** RETURNS: String containing html that displays the hierarchy.
  */
  public String renderHtml(HierarchyLoader loader, String pageLink) throws Exception
  {
    if (!loader.isLoaded())
    {
      throw new Exception("HierarchyLoader object must be loaded prior to "
        +"rendering");
    }
    
    this.loader = loader;
    this.pageLink = pageLink;
    
    return doRender();
  }
}
