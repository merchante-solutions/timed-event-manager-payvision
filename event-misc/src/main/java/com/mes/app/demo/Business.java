/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/ffb/Business.sqlj $

  Description:
  
  Business
  
  1st Financial Bank online application merchant information page bean.

  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/15/04 4:19p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.demo;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.SicField;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Business extends com.mes.app.BusinessBase
{
  {
    appType = 3;
    curScreenId = 1;
  }
  
  protected class SterlingIndustryTypeTable extends DropDownTable
  {
    public SterlingIndustryTypeTable()
    {
      addElement("","select one");
      addElement("1","Retail");
      addElement("2","Restaurant");
      addElement("10","Lodging");
      addElement("17","Supermarket");
      addElement("5","Internet");
      addElement("6","Services");
      addElement("7","Direct Marketing");
      addElement("8","Other");
      addElement("9","Cash Advance");
    }
  }
  
  protected class BranchTable extends DropDownTable
  {
    public BranchTable(long topHid)
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:76^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mod(th.descendent,1000000)  assoc_num,
//                    gn.group_name               branch_name
//            from    t_hierarchy th,
//                    group_names gn
//            where   th.ancestor = :topHid and
//                    th.relation = 2 and
//                    th.descendent = gn.group_number and
//                    lower(gn.group_name) not like ('%chain%')
//            order by gn.group_name        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mod(th.descendent,1000000)  assoc_num,\n                  gn.group_name               branch_name\n          from    t_hierarchy th,\n                  group_names gn\n          where   th.ancestor =  :1  and\n                  th.relation = 2 and\n                  th.descendent = gn.group_number and\n                  lower(gn.group_name) not like ('%chain%')\n          order by gn.group_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.demo.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,topHid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.demo.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:87^9*/
        
        rs = it.getResultSet();
        
        addElement("", "select one");
        while(rs.next())
        {
          addElement(rs.getString("assoc_num"), rs.getString("branch_name"));
        }
      }
      catch(Exception e)
      {
        logEntry("BranchTable()",e.toString());
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // make some fields required
      fields.getField("accountType").makeRequired();
      fields.getField("taxpayerId").makeRequired();
      fields.getField("yearsOpen").makeRequired();
      
      // make some fields not required
      fields.getField("sourceOfInfo").makeOptional();
      fields.getField("establishedDate").makeOptional();
      fields.getField("bankAddress").makeOptional();
      fields.getField("bankCsz").makeOptional();
      
      fields.add(new SicField("sicCode", "SIC Code", false));
      
      // set defaults
      
      // default merchant history fields
      fields.getField("haveProcessed").setData("N");
      fields.getField("statementsProvided").setData("N");
      fields.getField("haveCanceled").setData("N");
      
      // add validation of the confirmation fields
      fields.getField("confirmCheckingAccount")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("checkingAccount"),"Checking Account #"));
      fields.getField("confirmTransitRouting")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("transitRouting"),"Transit Routing #"));
      
      // add referring branch assoc num dropdown
      fields.add(new DropDownField("assocNum",new BranchTable(3870511001L),false));
      
      // add rep code (merchant.client_data_1)
      fields.add(new Field("clientData1", "Sales Representative", 40, 20, false));
      
      // remove existing industry type table and add sterling special
      fields.add(new DropDownField("industryType", "Industry", new SterlingIndustryTypeTable(), false));
      
      // reset all fields, including those just added to be of class form
      fields.setHtmlExtra("class=\"formText\"");
      
      // remove amex and discover validations
      fields.getField("amexAccepted").removeValidation("amexValidation");
      fields.getField("discoverAccepted").removeValidation("discoverValidation");
      
      createPosPartnerExtendedFields();
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
    
    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }
  }
  
  protected boolean loadAppData()
  {
    long                appSeqNum   = 0L;
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = super.loadAppData();
    
    try
    {
      appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if(result && appSeqNum != 0)
      {
        connect();
      
        // load client data
        /*@lineinfo:generated-code*//*@lineinfo:197^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  client_data_1
//            from    merchant
//            where   app_seq_num = :fields.getField("appSeqNum").asInteger()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2771 = fields.getField("appSeqNum").asInteger();
  try {
   String theSqlTS = "select  client_data_1\n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.demo.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_2771);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.demo.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:202^9*/
      
        rs = it.getResultSet();
      
        setFields(rs);
      
        result = true;
      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::loadAppData(): " + e.toString());
      logEntry("loadAppData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  protected boolean submitAppData()
  {
    boolean submitOk = false;
    try
    {
      connect();
      
      submitOk = super.submitAppData();
      
      if(submitOk)
      {
        long appSeqNum = fields.getField("appSeqNum").asInteger();
      
        if(appSeqNum != 0L)
        {
          // make sure sic code gets put in the proper place for auto-approving
          /*@lineinfo:generated-code*//*@lineinfo:242^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//              set     sic_code = :fields.getData("sicCode"),
//                      merch_mcc = :fields.getData("sicCode"),
//                      client_data_1 = :fields.getData("clientData1")
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2772 = fields.getData("sicCode");
 String __sJT_2773 = fields.getData("sicCode");
 String __sJT_2774 = fields.getData("clientData1");
   String theSqlTS = "update  merchant\n            set     sic_code =  :1 ,\n                    merch_mcc =  :2 ,\n                    client_data_1 =  :3 \n            where   app_seq_num =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.demo.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2772);
   __sJT_st.setString(2,__sJT_2773);
   __sJT_st.setString(3,__sJT_2774);
   __sJT_st.setLong(4,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:249^11*/
        }
      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::submitAppData(): " + e.toString());
      logEntry("submitAppDatq()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return submitOk;
  }
}/*@lineinfo:generated-code*/