/**************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/ffb/Equipment.java $

  Description:
  
  Equipment
  
  1st Financial Bank online application equipment page bean.


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/21/04 11:04a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.demo;

import javax.servlet.http.HttpServletRequest;
import com.mes.app.EquipmentBase;
import com.mes.constants.mesConstants;
import com.mes.forms.CheckboxField;

public class Equipment extends EquipmentBase
{
  {
    appType = 3;
    curScreenId = 2;
  }

  // overridden list of items to allow renaming  
  public static final String[][] SterlingEquipSelectDisplay =
  {
    { "termPrint",  "Integrated Terminal/Printer Sets"  },       // mesConstants.APP_EQUIP_TYPE_TERM_PRINTER     
    { "termPinPad", "Integrated Terminal/Printer/Pinpad Sets"  },// mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD
    { "terminal",   "Terminals"  },                   // mesConstants.APP_EQUIP_TYPE_TERMINAL         
    { "printer",    "Printers"  },                    // mesConstants.APP_EQUIP_TYPE_PRINTER          
    { "pinPad",     "Pinpads"  },                     // mesConstants.APP_EQUIP_TYPE_PINPAD           
    { "peripheral", "Peripherals"  },                 // mesConstants.APP_EQUIP_TYPE_PERIPHERAL       
    { "imprinter",  "Imprinters"  },                  // mesConstants.APP_EQUIP_TYPE_IMPRINTER        
  };

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // validations
      fields.getField("trainingType").makeRequired();
      
      // make comm type required
      fields.getField("commType").makeRequired();
      
      // defaults
      fields.add(new CheckboxField("autoCloseEnable", "AutoBatchClose", true));
//      fields.getField("autoCloseEnable").setData("y");
//      fields.getField("autoCloseHour").setData("08");
//      fields.getField("autoCloseMin").setData("00");
//      fields.getField("autoCloseAmPm").setData("PM");
      
      // reset all fields, including those just added to be of class formText
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
  
  protected boolean loadAppData()
  {
    boolean loadOk = false;
    try
    {
      // load business address by default into shipping info.  If data has
      // already been submitted it will be overwritten by the loadAppData call
      autoLoadAddress(mesConstants.ADDR_TYPE_BUSINESS);
      loadOk = super.loadAppData();
    }
    catch(Exception e)
    {
      logEntry("loadAppData()", e.toString());
      System.out.println(this.getClass().getName() + "::loadAppData(): " + e.toString());
    }
    
    return loadOk;
  }
}
