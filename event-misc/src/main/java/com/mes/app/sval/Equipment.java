/**************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/mes/Equipment.java $

  Description:

  Equipment

  Banner Bank online application equipment page bean.


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-09-19 11:13:32 -0700 (Wed, 19 Sep 2007) $
  Version            : $Revision: 14159 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.sval;

import javax.servlet.http.HttpServletRequest;
import com.mes.app.EquipmentBase;
import com.mes.forms.CheckboxField;

public class Equipment extends EquipmentBase
{
  {
    appType = 54;
    curScreenId = 2;
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      // reset terminal application field
      //fields.deleteField("terminalApp");
      //fields.add(new DropDownField("terminalApp",new TerminalApplicationTable(),true));

      // re-create some fields so that they are defaulted on
      fields.deleteField("avsEnable");
      fields.deleteField("fraudControlEnable");
      fields.deleteField("purchasingCardEnable");
      fields.deleteField("invoicePromptEnable");
      fields.add( new CheckboxField("avsEnable","AVS On",true) );
      fields.add( new CheckboxField("fraudControlEnable","Fraud Control On (last 4 digits of card)",true) );
      fields.add( new CheckboxField("purchasingCardEnable","Purchasing Card Flag On",true) );
      fields.add( new CheckboxField("invoicePromptEnable","Invoice # Prompt On",true) );

      // set trainingType field to be required
      fields.getField("trainingType").makeRequired();

      // default phone training to be MES
      fields.setData("trainingType", "M");

      // make comm type required
      fields.getField("commType").makeRequired();

      // reset all fields, including those just added to be of class formText
      fields.setHtmlExtra("class=\"formText\"");

      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
}
