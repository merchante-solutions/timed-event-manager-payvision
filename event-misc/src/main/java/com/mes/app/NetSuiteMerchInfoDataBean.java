/*@lineinfo:filename=NetSuiteMerchInfoDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/NetSuiteMerchInfoDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 9/15/04 9:52a $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.CheckboxField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DisabledCheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.PasswordField;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TaxIdField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.forms.ZipField;
import com.mes.support.MesMath;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class NetSuiteMerchInfoDataBean extends AppDataBeanBase
{
  //******************************not used anymore*****************************
  private static final int T7P_TERMINAL_PRICE                       = 399;
  private static final int IMPRINTER_PRICE                          = 35;
  private static final int IMPRINTER_PLATE_FEE                      = 3;

  private static final String T7P_TERMINAL_MODEL                    = "HPT7PRIP";
  private static final String T7P_TERMINAL_PRODUCT_OPTION           = "1";
  private static final String IMPRINTER_MODEL                       = "IP5";
  private static final String IMPRINTER_PLATE_MODEL                 = "IPPL";
  //***************************************************************************

  private static final int FEE_MONTHLY_MIN                          = 25;
  private static final int FEE_GATEWAY_SETUP                        = 150;
  private static final int FEE_GATEWAY_MONTHLY                      = 49;
  private static final int FEE_CHARGEBACK                           = 20;
  private static final int FEE_ACH_REJECT                           = 15;

  private static final String VISA_DISCOUNT_RATE_CARD_PRESENT       = "2.30";
  private static final String VISA_PER_ITEM_CARD_PRESENT            = "0.0";
  private static final String NON_BANKCARD_PER_ITEM_FEE             = "0.25";
  private static final String VISA_DISCOUNT_RATE_CARD_NOT_PRESENT   = "2.20";
  private static final String VISA_PER_ITEM_CARD_NOT_PRESENT        = "0.28";
  private static final String VISA_MID_QUAL_DOWNGRADE               = "0.45";
  private static final String VISA_NON_QUAL_DOWNGRADE               = "1.75";
  private static final String INTERNET_GATEWAY_TRANSACTION_FEE      = "0.10";

  private static final int VISA_CAT_BUSINESS_SERVICES               = 7;
  private static final int VISA_CAT_BUSINESS_GOODS                  = 8;
  private static final int VISA_CAT_ECOMMERCE_INTERNET              = 9;
  private static final int VISA_CAT_HEALTH_FOOD_SERVICES            = 10;
  private static final int VISA_CAT_OTHER                           = 99;

  private static final int BUS_TYPE_SOLE_PROPRIETOR                 = 1;

  public static final int     CT_VISA                     = 0;
  public static final int     CT_MC                       = 1;
  public static final int     CT_DINERS                   = 2;
  public static final int     CT_DISCOVER                 = 3;
  public static final int     CT_AMEX                     = 4;
  public static final int     CT_JCB                      = 5;
  public static final int     CT_DEBIT                    = 6;
  public static final int     CT_CHECK                    = 7;
  public static final int     CT_EBT                      = 8;
  public static final int     CT_TYME                     = 9;
  public static final int     CT_COUNT                    = 10;

  public static final int     CT_INTERNET                 = mesConstants.APP_CT_INTERNET;
    
  public static final int     FNAME_ACCEPTED              = 0;
  public static final int     FNAME_ACCOUNT_NUMBER        = 1;
  
  public static final int     FIDX_ADDR_LINE1             = 0;
  public static final int     FIDX_ADDR_LINE2             = 1;
  public static final int     FIDX_ADDR_CITY              = 2;
  public static final int     FIDX_ADDR_STATE             = 3;
  public static final int     FIDX_ADDR_ZIP               = 4;
  public static final int     FIDX_ADDR_COUNTRY           = 5;
  public static final int     FIDX_ADDR_PHONE             = 6;
  public static final int     FIDX_ADDR_FAX               = 7;
  
  public static final int     FIDX_OWNER_LAST_NAME        = 0;
  public static final int     FIDX_OWNER_FIRST_NAME       = 1;
  public static final int     FIDX_OWNER_SSN              = 2;
  public static final int     FIDX_OWNER_PERCENT          = 3;
  public static final int     FIDX_OWNER_MONTH            = 4;
  public static final int     FIDX_OWNER_YEAR             = 5;
  public static final int     FIDX_OWNER_TITLE            = 6;
  
  public static final int     FIDX_ACCOUNT_NUMBER         = 0;
  public static final int     FIDX_NEW_ACCT_RATE          = 1;
  public static final int     FIDX_SPLIT_DIAL             = 2;
  
  // converts teh CT_XXX index into the corresponding
  // set of form field names.  
  protected static String[][]   CardTypeToFieldNames =
  {
    { "vmcAccepted",      null                           }, // CT_VISA
    { "vmcAccepted",      null                           }, // CT_MC
    { "dinersAccepted",   "dinersAcctNum"                }, // CT_DINERS
    { "discoverAccepted", "discoverAcctNum"              }, // CT_DISCOVER
    { "amexAccepted",     "amexAcctNum"                  }, // CT_AMEX
    { "jcbAccepted",      "jcbAcctNum"                   }, // CT_JCB
    { null,               null                           }, // CT_DEBIT
    { null,               null                           }, // CT_CHECK
    { null,               null                           }, // CT_EBT
    { null,               null                           }, // CT_TYME
  };
  
  // converts the CT_XXX index into the corresponding 
  // application card type to be used in the merchpsyoption and
  // tranchrg application tables.
  protected static int[] CardTypeToAppCardType =
  {
    mesConstants.APP_CT_VISA,             // CT_VISA
    mesConstants.APP_CT_MC,               // CT_MC
    mesConstants.APP_CT_DINERS_CLUB,      // CT_DINERS
    mesConstants.APP_CT_DISCOVER,         // CT_DISCOVER
    mesConstants.APP_CT_AMEX,             // CT_AMEX
    mesConstants.APP_CT_JCB,              // CT_JCB
    mesConstants.APP_CT_DEBIT,            // CT_DEBIT
    mesConstants.APP_CT_CHECK_AUTH,       // CT_CHECK
    mesConstants.APP_CT_EBT,              // CT_EBT
    mesConstants.APP_CT_TYME_NETWORK,     // CT_TYME
  };

  private static final String[][]   ProductList = 
  {
    { "EDC (Electronic Draft Capture-Dial Terminal)", Integer.toString(mesConstants.POS_DIAL_TERMINAL) },
    { "Internet Solutions"                          , Integer.toString(mesConstants.POS_INTERNET) },
    { "POS Partner 2000"                            , Integer.toString(mesConstants.POS_PC) },
    { "DialPay (Telephone Authorization & Capture)" , Integer.toString(mesConstants.POS_DIAL_AUTH) },
    { "Other Vital Certified Product:"              , Integer.toString(mesConstants.POS_OTHER) },
  };

  protected static String[][] BusinessCatRadioButtons = 
  {
    { "Business Services"             , Integer.toString( VISA_CAT_BUSINESS_SERVICES     ) },
    { "Business Goods"                , Integer.toString( VISA_CAT_BUSINESS_GOODS        ) },
    { "eCommerce & Internet Services" , Integer.toString( VISA_CAT_ECOMMERCE_INTERNET    ) },
    { "Health & Food Services"        , Integer.toString( VISA_CAT_HEALTH_FOOD_SERVICES  ) },
    { "Other"                         , Integer.toString( VISA_CAT_OTHER                 ) }
  };
  
  public class TransitRoutingValidation 
    extends SQLJConnectionBase
    implements Validation
  {
    String          ErrorMessage      = null;
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }
    public boolean validate( String fieldData )
    {
      int         itemCount     = 0;
      boolean     retVal        = true;

      try
      {
        if(countDigits(fieldData) != 9)
        {
          ErrorMessage  = "Business checking account transit routing number must be nine (9) digits";
          retVal        = false;
        }
        else
        {
          connect();
          /*@lineinfo:generated-code*//*@lineinfo:189^11*/

//  ************************************************************
//  #sql [Ctx] { select count(transit_routing_num) 
//              from   rap_app_bank_aba
//              where  transit_routing_num = :fieldData
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(transit_routing_num)  \n            from   rap_app_bank_aba\n            where  transit_routing_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fieldData);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:194^11*/
          if ( itemCount == 0 )
          {
            if ( hasWarning( mesConstants.ERROR_ABA_NOT_FOUND ) == false )
            {
              // add a warning to the warning list.  warnings 
              // are only issued a single time.  submitting a
              // second time will cause the data to be considered
              // valid.
              ErrorMessage = "Transit Routing number was not found in our list of valid ABA Numbers.  Please confirm that it is correct and resubmit.";
              addWarning( mesConstants.ERROR_ABA_NOT_FOUND );
              retVal = false;
            }
            // else there was already a warning issues, let it go
          }
          else    // found, be sure to remove any existing warnings
          {
            removeWarning( mesConstants.ERROR_ABA_NOT_FOUND );
          }
        }
      }
      catch( java.sql.SQLException e )
      {
        logEntry( "validate()", e.toString() );
        ErrorMessage = "ABA validation failed: " + e.toString();
        retVal = false;
      }
      finally
      {
        cleanUp();
      }
      return( retVal );
    }
  }

  public class NoPOBoxValidation implements Validation
  {
    public boolean validate(String fieldData)
    {
      String[] illegals = 
        { "po box", "p.o. box", "pobox", "p.o.box", "p. o. box", "p o box",
          "post office box", "postoffice box", "pob" };
          
      String address = fieldData.toLowerCase();
      for (int i = 0; i < illegals.length; ++i)
      {
        if (address.indexOf(illegals[i]) == 0)
        {
          return false;
        }
      }
      
      return true;
    }
    
    public String getErrorText()
    {
      return( "Post office boxes not allowed" );
    }
  }
  
  public abstract class AccountValidation implements Validation
  {
    protected   String              ErrorMessage    = null;
    protected   Field[]             FieldList       = null;
    
    public AccountValidation( Field[] fields )
    {
      FieldList = fields;  
    }
    
    public String getErrorText()
    {
      String      retVal    = "";
      
      if ( ErrorMessage != null )
      {
        retVal = ErrorMessage;
      }
      return(retVal);
    }
    
    protected int getFieldsWithDataCount( )
    {
      int           retVal      = 0;
      
      for( int i = 0; i < FieldList.length; ++i )
      {
        if( FieldList[i].isBlank() == false )
        { 
          ++retVal;
        }
      }
      return( retVal );
    }      
    
    protected int getFirstFieldWithDataIndex( )
    {
      int           fieldIndex        = 0;
      
      for( fieldIndex = 0; fieldIndex < FieldList.length; ++fieldIndex )
      {
        // check for data 
        if ( FieldList[fieldIndex].isBlank() == false )
        {
          break;
        }
      }
      return( fieldIndex );
    }
    
    protected boolean hasAdditionalData( )
    {
      boolean       retVal      = false;
      
      for( int i = 0; i < FieldList.length; ++i )
      {
        if( ! FieldList[i].isBlank() )
        { 
          retVal = true;
          break;
        }
      }
      return( retVal );
    }      
    
    abstract public boolean validate( String fieldData );
  }
  
  public class AmexAccountValidation extends AccountValidation
  {
    public AmexAccountValidation( Field[] fields )
    { 
      super( fields );
    }
    
    public boolean validate(String fieldData)
    {
      Field       field         = null;
      int         fieldIndex    = 0;
      int         hasDataCount  = 0;
      long        temp          = 0L;
            
      // reset the error message
      ErrorMessage = null;
      
      // amex not checked or value is "N"
      if( isBlank(fieldData) == true || fieldData.toUpperCase().equals("N") )
      {
        if ( hasAdditionalData() )
        {
          ErrorMessage = "Please check the Amex payment option or remove the additional Amex data provided";
        }
      }
      else    // amex payment option is checked
      {
        hasDataCount = getFieldsWithDataCount();
        
        if ( hasDataCount == 1 )      // only one field has data
        {
          // get the index into the fields array of the
          // first field that is not blank.
          fieldIndex  = getFirstFieldWithDataIndex();
          field       = FieldList[fieldIndex];
          
          switch( fieldIndex )
          {
            case FIDX_ACCOUNT_NUMBER:
              try
              {
                // first start by validating that it is a number
                temp  = field.asLong();
        
                if ( field.getData().length() < 10 )
                {
                  ErrorMessage = "Amex Merchant Number must be at least 10 digits long";
                }
              }
              catch( NumberFormatException e )
              {
                ErrorMessage = "Amex Merchant Number must be a valid number";
              }
              break;
              
//              case FIDX_NEW_ACCT_RATE:
//              case FIDX_SPLIT_DIAL:
            default:
              break;
          }              
        }            
        else if ( hasDataCount == 0 )     // no additional data
        {
          ErrorMessage = "Please provide a valid Amex SE# or specify you would like a new account opened for you";  
        }
        else    // hasDataCount > 1
        {
          ErrorMessage = "Please provide a valid Amex SE# or specify you would like a new account opened for you, but not both";
        }
      }
      return( ErrorMessage == null );
    }
  }
  
  public class DinersAccountValidation extends AccountValidation
  {
    public DinersAccountValidation( Field[] fields )
    {
      super( fields );
    }
    
    public boolean validate(String fieldData)
    {
      // reset the error message
      ErrorMessage = null;
      
      if( isBlank(fieldData) == true || fieldData.toUpperCase().equals("N") )
      {
        if ( hasAdditionalData() )
        {
          ErrorMessage = "Please check the Diners Club payment option or remove the Diners merchant number";
        }
      }
      else
      {
        try
        {
          long temp = Long.parseLong(FieldList[FIDX_ACCOUNT_NUMBER].getData());
          if ( FieldList[FIDX_ACCOUNT_NUMBER].getData().length() < 10 )
          {
            ErrorMessage = "Diners Merchant Number must be 10 digits long";
          }
        }
        catch( NumberFormatException e )
        {
          ErrorMessage = "Diners Merchant Number must be a valid number";
        }
        catch( Exception e )
        {
          ErrorMessage = e.toString();
        }
      }
      return( ErrorMessage == null );
    }
  }
  
  public class DiscoverAccountValidation extends AccountValidation
  {
    public DiscoverAccountValidation( Field[] fields )
    {
      super( fields );
    }
    
    public boolean validate(String fieldData)
    {
      Field       field         = null;
      int         fieldIndex    = 0;
      int         hasDataCount  = 0;
      long        temp          = 0L;
    
      // reset the error message
      ErrorMessage = null;
      
      if( isBlank(fieldData) == true || fieldData.toUpperCase().equals("N") )
      {
        if ( hasAdditionalData() )
        {
          ErrorMessage = "Please check the Discover payment box or remove the additional Discover account information provided";
        }
      }
      else
      {
        hasDataCount = getFieldsWithDataCount();
        
        if ( hasDataCount == 1 )
        {
          fieldIndex    = getFirstFieldWithDataIndex();
          field         = FieldList[fieldIndex];
        
          switch( fieldIndex )
          {
            case FIDX_ACCOUNT_NUMBER:
              try
              {
                temp = Long.parseLong(field.getData());
                if ( field.getData().startsWith("60110") != true )
                {
                  ErrorMessage = "Discover Merchant Number must begin with 60110";
                }
                else if ( field.getData().length() < 15 )
                {
                  ErrorMessage = "Discover Merchant Number must be at least 15 digits long";
                }
              }
              catch( NumberFormatException e )
              {
                ErrorMessage = "Discover Merchant Number must be a valid number";
              }
              break;
              
//            case FIDX_NEW_ACCT_RATE:
            default:
              break;
          }
        }
        else if ( hasDataCount == 0 )
        {
          ErrorMessage = "Please provide either a valid Discover Merchant # OR specify you would like to open a new account.";
        }
        else // hasDataCount > 1
        {
          ErrorMessage = "Please provide either a valid Discover Merchant # OR specify you would like to open a new account, but not both";
        }
      }
      return( ErrorMessage == null );
    }
  }
  
  public class JCBAccountValidation extends AccountValidation
  {
    public JCBAccountValidation( Field[] fields )
    {
      super( fields );
    }
    
    public boolean validate(String fieldData)
    {
      // reset the error message and validate
      ErrorMessage = null;
      
      if( isBlank(fieldData) == true || fieldData.toUpperCase().equals("N") )
      {
        if ( hasAdditionalData() )
        {
          ErrorMessage = "Please check the JCB payment box or remove the JCB merchant number";
        }
      }
      else
      {
        try
        {
          String    tempData  = FieldList[FIDX_ACCOUNT_NUMBER].getData();
          long      temp      = Long.parseLong(tempData);
          
          if ( tempData.length() < 4 )
          {
            ErrorMessage = "Please provide a valid JCB Merchant #";
          }
        }
        catch( NumberFormatException e )
        {
          ErrorMessage = "JCB Merchant Number must be a valid number";
        }
      }
      return( ErrorMessage == null );
    }
  }
  
  public class InternetProductValidation implements Validation
  {
    String              ErrorMessage      = null;
    RadioButtonField    ProductField      = null;
    
    public InternetProductValidation( RadioButtonField product, String errorMsg )
    {
      ProductField  = product;
      ErrorMessage  = errorMsg;
    }
    
    public String getErrorText()
    {
      String      retVal    = "";
      
      if ( ErrorMessage != null )
      {
        retVal = ErrorMessage;
      }
      return(retVal);
    }
    
    public boolean validate(String fieldData)
    {
      boolean           retVal    = true;
      StringBuffer      temp      = null;
      
      try
      {
        if( Integer.parseInt(ProductField.getSelectedButtonValue()) == mesConstants.POS_INTERNET )
        {
          if ( fieldData.equals("") )
          {
            retVal        = false;
          }
        }
      }        
      catch( Exception e )
      {
        retVal = false;
      }
      return(retVal);
    }
  }

  protected class BusCatOtherValidation implements Validation
  {
    String                  ErrorMessage      = null;
    FieldGroup              Fields            = null;
    
    public BusCatOtherValidation( FieldGroup fields )
    {
      Fields = fields;
    }
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate( String fdata )
    {
      ErrorMessage = null;

      try
      {
        switch( Fields.getField("businessCat").asInteger() )
        {
          case VISA_CAT_OTHER:
            if ( Fields.getField("businessDesc").isBlank() )
            {
              ErrorMessage = "Please provide of Description of your business.  Include type of goods & services sold.";
            }
            break;

          default:
            if(Fields.getField("businessCat").asInteger() == 0)
              ErrorMessage = "Please select a valid business category and a corresponding business type";
            else if(!Fields.getField("businessDesc").isBlank())
              ErrorMessage = "Remove the description of your business or choose type of business Other";
            break;
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Please select a valid business category and a corresponding business type";
      }        
      return( ErrorMessage == null );
    }
  }

  protected class BusCatBusinessServicesValidation implements Validation
  {
    String                  ErrorMessage      = null;
    FieldGroup              Fields            = null;
    
    public BusCatBusinessServicesValidation( FieldGroup fields )
    {
      Fields = fields;
    }
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate( String fdata )
    {
      ErrorMessage = null;

      try
      {
        switch( Fields.getField("businessCat").asInteger() )
        {
          case VISA_CAT_BUSINESS_SERVICES:
            if ( Fields.getField("busCatBusinessServicesTypes").isBlank() )
            {
              ErrorMessage = "Please select a type from the Business Services Business Category";
            }
            break;

          default:
            if(Fields.getField("businessCat").asInteger() == 0)
              ErrorMessage = "Please select a valid business category and a corresponding business type";
            else if(!Fields.getField("busCatBusinessServicesTypes").isBlank())
              ErrorMessage = "Unselect Business Services Type or choose Business Services category";
            break;
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Please select a valid business category and a corresponding business type";
      }        
      return( ErrorMessage == null );
    }
  }

  protected class BusCatBusinessGoodsValidation implements Validation
  {
    String                  ErrorMessage      = null;
    FieldGroup              Fields            = null;
    
    public BusCatBusinessGoodsValidation( FieldGroup fields )
    {
      Fields = fields;
    }
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate( String fdata )
    {
      ErrorMessage = null;

      try
      {
        switch( Fields.getField("businessCat").asInteger() )
        {
          
          case VISA_CAT_BUSINESS_GOODS:
            if ( Fields.getField("busCatBusinessGoodsTypes").isBlank() )
            {
              ErrorMessage = "Please select a type from the Business Goods Business Category";
            }
            break;
          
          default:
            if(Fields.getField("businessCat").asInteger() == 0)
              ErrorMessage = "Please select a valid business category and a corresponding business type";
            else if(!Fields.getField("busCatBusinessGoodsTypes").isBlank())
              ErrorMessage = "Unselect Business Goods Type or choose Business Goods category";
            break;
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Please select a valid business category and a corresponding business type";
      }        
      return( ErrorMessage == null );
    }
  }

  protected class BusCatEcommerceValidation implements Validation
  {
    String                  ErrorMessage      = null;
    FieldGroup              Fields            = null;
    
    public BusCatEcommerceValidation( FieldGroup fields )
    {
      Fields = fields;
    }
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate( String fdata )
    {
      ErrorMessage = null;

      try
      {
        switch( Fields.getField("businessCat").asInteger() )
        {
         
          case VISA_CAT_ECOMMERCE_INTERNET:
            if ( Fields.getField("busCatEcommerceTypes").isBlank() )
            {
              ErrorMessage = "Please select a type from the eCommerce & Internet Services Business Category";
            }
            break;

          default:
            if(Fields.getField("businessCat").asInteger() == 0)
              ErrorMessage = "Please select a valid business category and a corresponding business type";
            else if(!Fields.getField("busCatEcommerceTypes").isBlank())
              ErrorMessage = "Unselect eCommerce & Internet Services Type or choose eCommerce & Internet Services category";
            break;
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Please select a valid business category and a corresponding business type";
      }        
      return( ErrorMessage == null );
    }
  }

  protected class BusCatHealthTypesValidation implements Validation
  {
    String                  ErrorMessage      = null;
    FieldGroup              Fields            = null;
    
    public BusCatHealthTypesValidation( FieldGroup fields )
    {
      Fields = fields;
    }
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate( String fdata )
    {
      ErrorMessage = null;

      try
      {
        switch( Fields.getField("businessCat").asInteger() )
        {
          case VISA_CAT_HEALTH_FOOD_SERVICES:
            if ( Fields.getField("busCatHealthTypes").isBlank() )
            {
              ErrorMessage = "Please select a type from the Health & Food Services Business Category";
            }
            break;
          
          default:
            if(Fields.getField("businessCat").asInteger() == 0)
              ErrorMessage = "Please select a valid business category and a corresponding business type";
            else if(!Fields.getField("busCatHealthTypes").isBlank())
              ErrorMessage = "Unselect Health & Food Services Type or choose Health & Food Services category";
            break;
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Please select a valid business category and a corresponding business type";
      }        
      return( ErrorMessage == null );
    }
  }

  public class OtherProductValidation implements Validation
  {
    String              ErrorMessage      = null;
    RadioButtonField    ProductField      = null;
    
    public OtherProductValidation( RadioButtonField product, String errorMsg )
    {
      ProductField  = product;
      ErrorMessage  = errorMsg;
    }
    
    public String getErrorText()
    {
      String      retVal    = "";
      
      if ( ErrorMessage != null )
      {
        retVal = ErrorMessage;
      }
      return(retVal);
    }
    
    public boolean validate(String fieldData)
    {
      boolean           retVal    = true;
      StringBuffer      temp      = null;
      
      try
      {
        if( Integer.parseInt(ProductField.getSelectedButtonValue()) == mesConstants.POS_OTHER )
        {
          if ( fieldData.equals("") )
          {
            retVal = false;
          }
        }
      }        
      catch( Exception e )
      {
        retVal = false;
      }
      return(retVal);
    }
  }

  protected class YearField  extends NumberField
  {
    public YearField( String fname, boolean nullAllowed )
    {
      super(fname,4,4,nullAllowed,0);
      addValidation(new YearValidation("Invalid year",nullAllowed));
    }

    protected String processData(String rawData)
    {
      if( rawData == null || rawData.equals("0") )
      {
        rawData = "";
      }
      return( rawData );
    }
  }

  protected class LocationYearsTable extends DropDownTable
  {
    public LocationYearsTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("0","0 - 1 Year");
      addElement("2","1 - 3 Years");
      addElement("4","3 - 5 Years");
      addElement("5","5+ Years");
    }
  }


  protected class MonthTable extends DropDownTable
  {
    public MonthTable()
    {
      // value/name pairs
      addElement("","select");
      addElement("1","Jan");
      addElement("2","Feb");
      addElement("3","Mar");
      addElement("4","Apr");
      addElement("5","May");
      addElement("6","Jun");
      addElement("7","Jul");
      addElement("8","Aug");
      addElement("9","Sep");
      addElement("10","Oct");
      addElement("11","Nov");
      addElement("12","Dec");
    }
  }

  private class HealthFoodBusTypeTable extends DropDownTable
  {
    public HealthFoodBusTypeTable()
      throws java.sql.SQLException 
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;
      
      try
      {
        connect();
        addElement("","Select Health & Food Services Type");
        /*@lineinfo:generated-code*//*@lineinfo:938^9*/

//  ************************************************************
//  #sql [Ctx] it = { select   COMPANY_TYPE_CODE,
//                     COMPANY_TYPE_DESC
//            from     VISA_COMPANY_TYPES
//            where    COMPANY_TYPE_CAT = :VISA_CAT_HEALTH_FOOD_SERVICES
//            order by COMPANY_TYPE_DESC
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   COMPANY_TYPE_CODE,\n                   COMPANY_TYPE_DESC\n          from     VISA_COMPANY_TYPES\n          where    COMPANY_TYPE_CAT =  :1 \n          order by COMPANY_TYPE_DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,VISA_CAT_HEALTH_FOOD_SERVICES);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:945^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }

  private class BusinessGoodsBusTypeTable extends DropDownTable
  {
    public BusinessGoodsBusTypeTable()
      throws java.sql.SQLException 
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;
      
      try
      {
        connect();
        addElement("","Select Business Goods Type");
        /*@lineinfo:generated-code*//*@lineinfo:975^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  COMPANY_TYPE_CODE,
//                    COMPANY_TYPE_DESC
//            from    VISA_COMPANY_TYPES
//            where   COMPANY_TYPE_CAT = :VISA_CAT_BUSINESS_GOODS 
//            order by COMPANY_TYPE_DESC
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  COMPANY_TYPE_CODE,\n                  COMPANY_TYPE_DESC\n          from    VISA_COMPANY_TYPES\n          where   COMPANY_TYPE_CAT =  :1  \n          order by COMPANY_TYPE_DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,VISA_CAT_BUSINESS_GOODS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:982^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }

  private class EcommerceInternetBusTypeTable extends DropDownTable
  {
    public EcommerceInternetBusTypeTable()
      throws java.sql.SQLException 
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;
      
      try
      {
        connect();
        addElement("","Select eCommerce & Internet Services Type");
        /*@lineinfo:generated-code*//*@lineinfo:1012^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  COMPANY_TYPE_CODE,
//                    COMPANY_TYPE_DESC
//            from    VISA_COMPANY_TYPES
//            where   COMPANY_TYPE_CAT = :VISA_CAT_ECOMMERCE_INTERNET 
//            order by COMPANY_TYPE_DESC
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  COMPANY_TYPE_CODE,\n                  COMPANY_TYPE_DESC\n          from    VISA_COMPANY_TYPES\n          where   COMPANY_TYPE_CAT =  :1  \n          order by COMPANY_TYPE_DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,VISA_CAT_ECOMMERCE_INTERNET);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1019^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }

  private class BusinessServicesBusTypeTable extends DropDownTable
  {
    public BusinessServicesBusTypeTable()
      throws java.sql.SQLException 
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;
      
      try
      {
        connect();
        addElement("","Select Business Services Type");
        /*@lineinfo:generated-code*//*@lineinfo:1049^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  COMPANY_TYPE_CODE,
//                    COMPANY_TYPE_DESC
//            from    VISA_COMPANY_TYPES
//            where   COMPANY_TYPE_CAT = :VISA_CAT_BUSINESS_SERVICES 
//            order by COMPANY_TYPE_DESC
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  COMPANY_TYPE_CODE,\n                  COMPANY_TYPE_DESC\n          from    VISA_COMPANY_TYPES\n          where   COMPANY_TYPE_CAT =  :1  \n          order by COMPANY_TYPE_DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,VISA_CAT_BUSINESS_SERVICES);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1056^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }


  protected class BusinessTypeTable extends DropDownTable
  {
    public BusinessTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Sole Proprietorship");
      addElement("2","Corporation");
      addElement("3","Partnership");
      addElement("4","Medical or Legal Corporation");
      addElement("5","Association/Estate/Trust");
      addElement("6","Tax Exempt");
      addElement("7","Government");
      addElement("8","Limited Liability Company");
    }
  }
  
  protected class IndustryTypeTable extends DropDownTable
  {
    public IndustryTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail");
      addElement("2","Restaurant");
      addElement("3","Hotel");
      addElement("4","Motel");
      addElement("5","Internet");
      addElement("6","Services");
      addElement("7","Direct Marketing");
      addElement("8","Other");
    }
  }
  
  protected class LocationTypeTable extends DropDownTable
  {
    public LocationTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail Storefront");
      addElement("2","Internet Storefront");
      addElement("3","Business Office");
      addElement("4","Private Residence");
      addElement("5","Other");
      addElement("6","Bank");
    }
  }
  
  protected class ApplicationTypeTable extends DropDownTable
  {
    public ApplicationTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Single Outlet");
      addElement("2","Chain");
      addElement("3","Addl. Chain Outlet");
    }
  }
  
  private class InternetProductTable extends DropDownTable
  {
    public InternetProductTable()
      throws java.sql.SQLException 
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;
      
      try
      {
        connect();
        addElement("","select one");
        /*@lineinfo:generated-code*//*@lineinfo:1148^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  pos_code,
//                    pos_desc
//            from    pos_category
//            where   pos_type = 3 or pos_type = 2
//            order by pos_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pos_code,\n                  pos_desc\n          from    pos_category\n          where   pos_type = 3 or pos_type = 2\n          order by pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1155^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }
  
  private class AccountInfoSourceTable extends DropDownTable
  {
    public AccountInfoSourceTable()
      throws java.sql.SQLException
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;

      try
      {      
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:1184^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  bais.source_id,
//                    bais.source_desc
//            from    bank_account_info_source bais
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bais.source_id,\n                  bais.source_desc\n          from    bank_account_info_source bais";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1189^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }
  
  protected class RefundPolicyTable extends DropDownTable
  {
    public RefundPolicyTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Refund in 30 Days or Less");
      addElement("2","No Refunds or Exchanges");
      addElement("3","Exchanges Only");
      addElement("4","Not Applicable");
    }
  }

  public NetSuiteMerchInfoDataBean()
  {
  }

  public String getAnnualSalesString( )
  {
    StringBuffer      retVal          = new StringBuffer();
    double            temp            = 0.0;

    try
    {
      temp = Double.parseDouble( fields.getField("monthlySales").getData() );
      if ( temp != 0.0 )
      {
        temp *= 12;
        retVal.append("Annual Sales = ");
        retVal.append(MesMath.toCurrency(temp));
      }
    }
    catch( NumberFormatException e )
    {
      // don't generate any log entries just because
      // the number is invalid
    }
    catch( Exception e )
    {
      logEntry("getAnnualSalesString()",e.toString());
    }
    
    return( retVal.toString() );
  }

  public String getAnnualVMCSalesString( )
  {
    StringBuffer      retVal          = new StringBuffer();
    double            temp            = 0.0;

    try
    {
      temp = Double.parseDouble( fields.getField("monthlyVMCSales").getData() );
      if ( temp != 0.0 )
      {
        temp *= 12;
        retVal.append("Annual Visa/MC Sales = ");
        retVal.append(MesMath.toCurrency(temp));
      }
    }
    catch( NumberFormatException e )
    {
      // don't generate any log entries just because
      // the number is invalid
    }
    catch( Exception e )
    {
      logEntry("getAnnualVMCSalesString()",e.toString());
    }
    
    return( retVal.toString() );
  }
  
  public void init()
  {
    super.init();
    
    OnlyOneValidation     onlyOneVal      = null;
    Field                 temp            = null;
    Validation            val             = null;

    try
    {    

      // business info
      //fields.add(new Field("promotionCode1",1,1,false));
      //fields.add(new Field("promotionCode2",1,1,false));
      //fields.add(new Field("promotionCode3",1,1,false));
      //fields.add(new Field("promotionCode4",1,1,false));
      //fields.add(new Field("netSuiteAcctNum",16,35,false));

      //we dont set data here.. cause it gets feed through incoming data in the request
      fields.add(new HiddenField("netSuiteAcctNum"));
      fields.add(new Field("promotionCode", "Promotion Code", 50, 15, true));

      fields.add(new Field("businessName",25,35,false));
      fields.add(new Field("businessLegalName",50,35,false));
      fields.add(new TaxIdField("taxpayerId",false));
      fields.add(new PhoneField("businessPhone",false));
      fields.add(new Field("businessAddress1",32,35,false));
      fields.getField("businessAddress1").addValidation(new NoPOBoxValidation());
      fields.add(new Field("businessAddress2",32,35,true));
      fields.add(new Field("businessCity",25,20,false));
      fields.add(new DropDownField("businessState",new StateDropDownTable(),false));
      fields.add(new ZipField("businessZip",false,fields.getField("businessState")));
      fields.add(new EmailField("businessEmail",45,20,false));
      fields.add(new DropDownField("establishedMonth",new MonthTable(), false) );
      fields.add(new YearField("establishedYear",false) );
      fields.add(new Field("contactNameFirst",20,35,false) );
      fields.add(new Field("contactNameLast",20,35,false) );
      fields.add(new PhoneField("contactPhone",false) );
      fields.add(new EmailField("contactEmail",75,35,true) );
      fields.add(new PhoneField("businessFax",false) );

      fields.add(new DropDownField("locationYears", new LocationYearsTable(), false) );

      //fields.add(new DropDownField("busCatHealthTypes", new HealthFoodBusTypeTable(), true));
      //fields.getField("busCatHealthTypes").addValidation( new BusCatHealthTypesValidation( fields ) );

      //fields.add(new DropDownField("busCatBusinessGoodsTypes", new BusinessGoodsBusTypeTable(), true));
      //fields.getField("busCatBusinessGoodsTypes").addValidation( new BusCatBusinessGoodsValidation( fields ) );

      //fields.add(new DropDownField("busCatEcommerceTypes", new EcommerceInternetBusTypeTable(), true));
      //fields.getField("busCatEcommerceTypes").addValidation( new BusCatEcommerceValidation( fields ) );

      //fields.add(new DropDownField("busCatBusinessServicesTypes", new BusinessServicesBusTypeTable(), true));
      //fields.getField("busCatBusinessServicesTypes").addValidation( new BusCatBusinessServicesValidation( fields ) );

      fields.add(new TextareaField("businessDesc",50,2,40,false));
      //fields.getField("businessDesc").addValidation( new BusCatOtherValidation( fields ) );

      //fields.add(new RadioButtonField( "businessCat", BusinessCatRadioButtons, -1, false, "Please select a Business Category" ));

      //fields.getField("businessCat").addValidation( new BusinessCatValidation( fields ) );

      fields.add(new DropDownField("businessType",new BusinessTypeTable(),false));
      
      //fields.add(new Field("businessType",25,35,true));
      
      fields.add(new Field("webUrl",75,35,false));
      fields.add(new DropDownField("haveProcessed",new YesNoTable(),false));
      fields.add(new Field("previousProcessor",40,35,true));
      fields.getField("previousProcessor").addValidation(new IfYesNotBlankValidation(fields.getField("haveProcessed"),"Please provide the name of the previous processor"));
      fields.add(new DropDownField("statementsProvided",new YesNoTable(),false));
      fields.add(new DropDownField("haveCanceled",new YesNoTable(),false));
      fields.add(new Field("canceledProcessor",40,35,true));
      fields.getField("canceledProcessor").addValidation(new IfYesNotBlankValidation(fields.getField("haveCanceled"),"Please provide the name of the cancelling processor"));
      fields.add(new Field("canceledReason",40,35,true));
      fields.getField("canceledReason").addValidation(new IfYesNotBlankValidation(fields.getField("haveCanceled"),"Please provide reason account was cancelled"));
      fields.add(new DropDownField("cancelMonth",new MonthTable(), true) );
      fields.getField("cancelMonth").addValidation(new IfYesNotBlankValidation(fields.getField("haveCanceled"),"Please select the month account was cancelled"));
      temp = new YearField("cancelYear",true);
      temp.addValidation(new IfYesNotBlankValidation(fields.getField("haveCanceled"),"Please provide a valid 4-digit year account was cancelled"));
      fields.add(temp);
    
      // This defaults to "US" which is currently the
      // only valid value.
      temp = new HiddenField("businessCountry");
      temp.setData("US");
      fields.add(temp);

      //This defaults the product type to internet
      temp = new HiddenField("productType");
      temp.setData("2"); //Internet
      fields.add(temp);


      //This defaults the product type to dial terminal (edc)
      temp = new HiddenField("internetType");
      temp.setData("202"); //payflow pro
      fields.add(temp);


      fields.add(new DisabledCheckboxField("vmcAccepted","Visa/MasterCard",true));

      //This visa mastercard always accepted.. the only one accepted
      //temp = new HiddenField("vmcAccepted");
      //temp.setData("Y");
      //fields.add(temp);

      temp = new HiddenField("motoPercentage");
      temp.setData("100"); //payflow pro
      fields.add(temp);
      
      // 7 - payment options
      fields.add(new DisabledCheckboxField ("vmcAccepted",     "Visa/MasterCard",true));
      
      // Amex
      fields.add(new CheckboxField    ("amexAccepted","American Express",false));
      fields.add(new CheckboxField    ("amexNewAccount","",false));
      fields.add(new NumberField      ("amexAcctNum",16,25,true,0) );
      
      //need to fix this validation
      val = new AmexAccountValidation( new Field[]
                                     {
                                        fields.getField("amexAcctNum"),
                                        fields.getField("amexNewAccount")
                                     } );
      
      fields.getField("amexAccepted").addValidation(val);

      // setup the only one validation for PIP and split dial
      //onlyOneVal = new OnlyOneValidation("Please select either Amex Split Dial or PIP, not both");
      //onlyOneVal.addField(fields.getField("amexSplitDial"));
      //onlyOneVal.addField(fields.getField("amexPIP"));
      //fields.getField("amexAccepted").addValidation(onlyOneVal);

      // Discover
      fields.add( new CheckboxField("discoverAccepted","Discover",false) );
      fields.add( new NumberField("discoverAcctNum",15,25,true,0) );
      fields.add( new CheckboxField("discNewAccount","",false));

      val = new DiscoverAccountValidation( new Field[]
                                           {
                                             fields.getField("discoverAcctNum"),
                                             fields.getField("discNewAccount")
                                           } );

      fields.getField("discoverAccepted").addValidation(val);

      // Diners
      fields.add(new CheckboxField("dinersAccepted","Diners Club",false));
      fields.add( new NumberField("dinersAcctNum",10,25,true,0) );
      val = new DinersAccountValidation( new Field[] { fields.getField("dinersAcctNum") } );
      fields.getField("dinersAccepted").addValidation(val);

      // JCB
      fields.add( new CheckboxField("jcbAccepted","JCB",false) );
      fields.add( new NumberField("jcbAcctNum",15,25,true,0) );
      val = new JCBAccountValidation( new Field[] { fields.getField("jcbAcctNum") } );
      fields.getField("jcbAccepted").addValidation(val);




      fields.add(new HiddenField("agreement"));    //@ necessary?
    
      // business checking account info
      fields.add(new Field("bankName",30,35,false));
    
      // setup the checking account and the confirmation validation
      fields.add(new PasswordField("checkingAccount",17,35,false));
      temp = new Field("confirmCheckingAccount",17,35,false);
      temp.addValidation(new FieldEqualsFieldValidation(fields.getField("checkingAccount"),"Checking Account #"));
      fields.add(temp);

      // setup the transit routing and confirmation validation
      fields.add(new PasswordField("transitRouting",9,35,false));
      temp = new Field("confirmTransitRouting",9,35,false);
      temp.addValidation(new FieldEqualsFieldValidation(fields.getField("transitRouting"),"Transit Routing #"));
      temp.addValidation(new TransitRoutingValidation());
      fields.add(temp);
    
      // add a hidden field (legacy) for the type of 
      // account.  This defaults to 2 which is checking
      // account.  See the table mes.bankacc_type
      temp = new HiddenField("typeOfAcct");
      temp.setData("2");
      fields.add(temp);
    
      //fields.add(new DropDownField("sourceOfInfo",new AccountInfoSourceTable(),false));
      
      fields.add(new NumberField("yearsOpen",3,35,false,0));
      fields.add(new Field("bankAddress",32,35,false));
      fields.add(new Field("bankCity",25,35,false));
      fields.add(new DropDownField("bankState",new StateDropDownTable(),false));
      fields.add(new ZipField("bankZip",false,fields.getField("bankState")));

      // primary owner information
      fields.add(new Field("owner1FirstName",20,20,false));
      fields.add(new Field("owner1LastName",20,20,false));
      fields.add(new TaxIdField("owner1SSN",false));
      temp = new NumberField("owner1Percent",3,3,false,0);
      temp.addValidation(new PercentValidation("Primary owner's percentage of ownership is invalid"));
      fields.add(temp);
      fields.add(new Field("owner1Address1",32,32,false));
      fields.add(new Field("owner1City",25,25,false));
      fields.add(new DropDownField("owner1State",new StateDropDownTable(),false));
      fields.add(new ZipField("owner1Zip",false,fields.getField("owner1State")));
      fields.add(new PhoneField("owner1Phone",true));
      fields.add(new Field("owner1Title",40,20,true));
      fields.add(new DropDownField("owner1SinceMonth",new MonthTable(), true) );
      fields.add(new YearField("owner1SinceYear",true) );
    
      // transaction information
      fields.add(new CurrencyField("monthlySales",11,9,false));
      fields.add(new CurrencyField("monthlyVMCSales",11,9,false));
      fields.add(new CurrencyField("averageTicket",11,9,false));
    
      fields.add(new DropDownField("refundPolicy",new RefundPolicyTable(),false));

    
      //fields.add(new NumberField("imprinterPlates",2,3,true,0));
      fields.add(new TextareaField("comments",4000,8,80,true));

      addHtmlExtra("class=\"formFields\"");
    }
    catch( Exception e )
    {
      logEntry("init()",e.toString());
    }
  }
  
  public void loadData()
  {
 
    //dont want to load data for this app to prevent people from seeing
    //other peoples apps.. so we just return 
    if(true)
    {
      return;
    }

    ResultSetIterator             it        = null;
    ResultSet                     resultSet = null;

    try
    {
      
      if ( AppSeqNum != APP_SEQ_NEW )
      {
        // extract the merchant number for this app
        /*@lineinfo:generated-code*//*@lineinfo:1529^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(mr.merch_number,0) 
//            from    merchant          mr
//            where   mr.app_seq_num = :AppSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(mr.merch_number,0)  \n          from    merchant          mr\n          where   mr.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   MerchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1534^9*/
      
        // query the merchant data to be placed into fields
        /*@lineinfo:generated-code*//*@lineinfo:1537^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mr.merch_business_name          as business_name,
//                    mr.merch_federal_tax_id         as taxpayer_id,
//                    mr.merch_number                 as merchant_number,
//                    mr.merch_business_establ_month  as established_month,
//                    mr.merch_business_establ_year   as established_year,
//                    mr.merch_legal_name             as business_legal_name,
//                    mr.merch_mailing_name           as mailing_name,
//                    mr.merch_email_address          as business_email,
//                    --mr.industype_code               as busCatBusinessServicesTypes,
//                    --mr.industype_code               as busCatBusinessGoodsTypes,
//                    --mr.industype_code               as busCatEcommerceTypes,
//                    --mr.industype_code               as busCatHealthTypes,
//                    mr.bustype_code                 as business_type,
//                    vct.company_type_cat            as business_cat,
//                    mr.merch_busgoodserv_descr      as business_desc,
//                    mr.merch_num_of_locations       as num_locations,
//                    mr.merch_years_at_loc           as location_years,
//                    mr.loctype_code                 as location_type,
//                    mr.merch_prior_cc_accp_flag     as have_processed,
//                    mr.merch_prior_processor        as previous_processor,
//                    mr.merch_cc_acct_term_flag      as have_canceled,
//                    mr.merch_cc_term_name           as canceled_processor,
//                    mr.merch_term_reason            as canceled_reason,
//                    mr.merch_term_year              as cancel_year,
//                    mr.merch_term_month             as cancel_month,
//                    mr.merch_month_tot_proj_sales   as monthly_sales,
//                    mr.merch_month_visa_mc_sales    as monthly_vmc_sales,
//                    mr.merch_average_cc_tran        as average_ticket,
//                    mr.merch_mail_phone_sales       as moto_percentage,
//                    mr.merch_notes                  as comments,
//                    mr.refundtype_code              as refund_policy,
//                    mr.merch_application_type       as application_type,
//                    mr.merch_referring_bank         as referring_bank,
//                    mr.merch_agreement              as agreement,
//                    mr.merch_prior_statements       as statements_provided,
//                    mr.merch_web_url                as web_url,
//                    mr.asso_number                  as not_used_1,
//                    mr.merch_bank_number            as not_used_2,
//                    mr.franchise_code               as not_used_3,
//                    mr.app_sic_code                 as not_used_4,
//                    mr.net_suite_acct_num           as net_suite_acct_num
//                    --mr.promotion_code               as promotion_code1,
//                    --mr.promotion_code               as promotion_code2,
//                    --mr.promotion_code               as promotion_code3,
//                    --mr.promotion_code               as promotion_code4
//  
//            from    merchant            mr,
//                    visa_company_types  vct
//  
//            where   mr.app_seq_num    = :AppSeqNum  and 
//                    mr.industype_code = vct.company_type_code(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mr.merch_business_name          as business_name,\n                  mr.merch_federal_tax_id         as taxpayer_id,\n                  mr.merch_number                 as merchant_number,\n                  mr.merch_business_establ_month  as established_month,\n                  mr.merch_business_establ_year   as established_year,\n                  mr.merch_legal_name             as business_legal_name,\n                  mr.merch_mailing_name           as mailing_name,\n                  mr.merch_email_address          as business_email,\n                  --mr.industype_code               as busCatBusinessServicesTypes,\n                  --mr.industype_code               as busCatBusinessGoodsTypes,\n                  --mr.industype_code               as busCatEcommerceTypes,\n                  --mr.industype_code               as busCatHealthTypes,\n                  mr.bustype_code                 as business_type,\n                  vct.company_type_cat            as business_cat,\n                  mr.merch_busgoodserv_descr      as business_desc,\n                  mr.merch_num_of_locations       as num_locations,\n                  mr.merch_years_at_loc           as location_years,\n                  mr.loctype_code                 as location_type,\n                  mr.merch_prior_cc_accp_flag     as have_processed,\n                  mr.merch_prior_processor        as previous_processor,\n                  mr.merch_cc_acct_term_flag      as have_canceled,\n                  mr.merch_cc_term_name           as canceled_processor,\n                  mr.merch_term_reason            as canceled_reason,\n                  mr.merch_term_year              as cancel_year,\n                  mr.merch_term_month             as cancel_month,\n                  mr.merch_month_tot_proj_sales   as monthly_sales,\n                  mr.merch_month_visa_mc_sales    as monthly_vmc_sales,\n                  mr.merch_average_cc_tran        as average_ticket,\n                  mr.merch_mail_phone_sales       as moto_percentage,\n                  mr.merch_notes                  as comments,\n                  mr.refundtype_code              as refund_policy,\n                  mr.merch_application_type       as application_type,\n                  mr.merch_referring_bank         as referring_bank,\n                  mr.merch_agreement              as agreement,\n                  mr.merch_prior_statements       as statements_provided,\n                  mr.merch_web_url                as web_url,\n                  mr.asso_number                  as not_used_1,\n                  mr.merch_bank_number            as not_used_2,\n                  mr.franchise_code               as not_used_3,\n                  mr.app_sic_code                 as not_used_4,\n                  mr.net_suite_acct_num           as net_suite_acct_num\n                  --mr.promotion_code               as promotion_code1,\n                  --mr.promotion_code               as promotion_code2,\n                  --mr.promotion_code               as promotion_code3,\n                  --mr.promotion_code               as promotion_code4\n\n          from    merchant            mr,\n                  visa_company_types  vct\n\n          where   mr.app_seq_num    =  :1   and \n                  mr.industype_code = vct.company_type_code(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1590^9*/
        resultSet = it.getResultSet();
        
        // set the rest of the form related fields
        setFields(resultSet);
        
        //setVisaBusTypeCode();
        //setPromotionCodes();

        resultSet.close();
        it.close();

        // business location address (no PO Boxes)
        /*@lineinfo:generated-code*//*@lineinfo:1603^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1           as business_address_1,
//                    address_line2           as business_address_2,
//                    address_city            as business_city,
//                    countrystate_code       as business_state,
//                    country_code            as business_country,
//                    address_zip             as business_zip,
//                    address_phone           as business_phone,
//                    address_fax             as business_fax
//            from    address
//            where   app_seq_num = :AppSeqNum and
//                    addresstype_code = :mesConstants.ADDR_TYPE_BUSINESS
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1           as business_address_1,\n                  address_line2           as business_address_2,\n                  address_city            as business_city,\n                  countrystate_code       as business_state,\n                  country_code            as business_country,\n                  address_zip             as business_zip,\n                  address_phone           as business_phone,\n                  address_fax             as business_fax\n          from    address\n          where   app_seq_num =  :1  and\n                  addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_BUSINESS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1616^9*/
        setFields(it.getResultSet());
        it.close();

        // business mailing address
        /*@lineinfo:generated-code*//*@lineinfo:1621^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1           as mailing_address_1,
//                    address_line2           as mailing_address_2,
//                    address_city            as mailing_city,
//                    countrystate_code       as mailing_state,
//                    address_zip             as mailing_zip
//            from    address
//            where   app_seq_num = :AppSeqNum and
//                    addresstype_code = :mesConstants.ADDR_TYPE_MAILING
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1           as mailing_address_1,\n                  address_line2           as mailing_address_2,\n                  address_city            as mailing_city,\n                  countrystate_code       as mailing_state,\n                  address_zip             as mailing_zip\n          from    address\n          where   app_seq_num =  :1  and\n                  addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_MAILING);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1631^9*/
        setFields(it.getResultSet());
        it.close();

        // primary owner address
        /*@lineinfo:generated-code*//*@lineinfo:1636^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1           as owner1_address_1,
//                    address_line2           as owner1_address_2,
//                    address_city            as owner1_city,
//                    countrystate_code       as owner1_state,
//                    country_code            as owner1_country,
//                    address_zip             as owner1_zip,
//                    address_phone           as owner1_phone
//            from    address
//            where   app_seq_num = :AppSeqNum and
//                    addresstype_code = :mesConstants.ADDR_TYPE_OWNER1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1           as owner1_address_1,\n                  address_line2           as owner1_address_2,\n                  address_city            as owner1_city,\n                  countrystate_code       as owner1_state,\n                  country_code            as owner1_country,\n                  address_zip             as owner1_zip,\n                  address_phone           as owner1_phone\n          from    address\n          where   app_seq_num =  :1  and\n                  addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_OWNER1);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1648^9*/
        setFields(it.getResultSet());
        it.close();

        // secondary owner address
        /*@lineinfo:generated-code*//*@lineinfo:1653^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1           as owner2_address_1,
//                    address_line2           as owner2_address_2,
//                    address_city            as owner2_city,
//                    countrystate_code       as owner2_state,
//                    country_code            as owner2_country,
//                    address_zip             as owner2_zip,
//                    address_phone           as owner2_phone
//            from    address
//            where   app_seq_num = :AppSeqNum and
//                    addresstype_code = :mesConstants.ADDR_TYPE_OWNER2
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1           as owner2_address_1,\n                  address_line2           as owner2_address_2,\n                  address_city            as owner2_city,\n                  countrystate_code       as owner2_state,\n                  country_code            as owner2_country,\n                  address_zip             as owner2_zip,\n                  address_phone           as owner2_phone\n          from    address\n          where   app_seq_num =  :1  and\n                  addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_OWNER2);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1665^9*/
        setFields(it.getResultSet());
        it.close();

        // bank address
        /*@lineinfo:generated-code*//*@lineinfo:1670^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1           as bank_address,
//                    address_city            as bank_city,
//                    countrystate_code       as bank_state,
//                    address_zip             as bank_zip,
//                    address_phone           as bank_phone
//            from    address
//            where   app_seq_num = :AppSeqNum and
//                    addresstype_code = :mesConstants.ADDR_TYPE_CHK_ACCT_BANK
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1           as bank_address,\n                  address_city            as bank_city,\n                  countrystate_code       as bank_state,\n                  address_zip             as bank_zip,\n                  address_phone           as bank_phone\n          from    address\n          where   app_seq_num =  :1  and\n                  addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_CHK_ACCT_BANK);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1680^9*/
        setFields(it.getResultSet());
        it.close();

        // primary business owner
        /*@lineinfo:generated-code*//*@lineinfo:1685^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_last_name      as owner1_last_name,
//                    busowner_first_name     as owner1_first_name,
//                    busowner_ssn            as owner1_ssn,
//                    busowner_owner_perc     as owner1_percent,
//                    busowner_period_month   as owner1_since_month,
//                    busowner_period_year    as owner1_since_year,
//                    busowner_title          as owner1_title
//            from    businessowner
//            where   app_seq_num  = :AppSeqNum and
//                    busowner_num = :mesConstants.BUS_OWNER_PRIMARY
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_last_name      as owner1_last_name,\n                  busowner_first_name     as owner1_first_name,\n                  busowner_ssn            as owner1_ssn,\n                  busowner_owner_perc     as owner1_percent,\n                  busowner_period_month   as owner1_since_month,\n                  busowner_period_year    as owner1_since_year,\n                  busowner_title          as owner1_title\n          from    businessowner\n          where   app_seq_num  =  :1  and\n                  busowner_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.BUS_OWNER_PRIMARY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1697^9*/
        setFields(it.getResultSet());
        it.close();

        // secondary business owner
        /*@lineinfo:generated-code*//*@lineinfo:1702^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_last_name      as owner2_last_name,
//                    busowner_first_name     as owner2_first_name,
//                    decode(busowner_ssn,
//                           null,' ',
//                           0, ' ',
//                           busowner_ssn)    as owner2_ssn,
//                    busowner_owner_perc     as owner2_percent,
//                    busowner_period_month   as owner2_since_month,
//                    busowner_period_year    as owner2_since_year,
//                    busowner_title          as owner2_title
//            from    businessowner
//            where   app_seq_num  = :AppSeqNum and
//                    busowner_num = :mesConstants.BUS_OWNER_SECONDARY
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_last_name      as owner2_last_name,\n                  busowner_first_name     as owner2_first_name,\n                  decode(busowner_ssn,\n                         null,' ',\n                         0, ' ',\n                         busowner_ssn)    as owner2_ssn,\n                  busowner_owner_perc     as owner2_percent,\n                  busowner_period_month   as owner2_since_month,\n                  busowner_period_year    as owner2_since_year,\n                  busowner_title          as owner2_title\n          from    businessowner\n          where   app_seq_num  =  :1  and\n                  busowner_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.BUS_OWNER_SECONDARY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1717^9*/
        setFields(it.getResultSet());
        it.close();

        // contact data
        /*@lineinfo:generated-code*//*@lineinfo:1722^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchcont_prim_first_name   as contact_name_first,
//                    merchcont_prim_last_name    as contact_name_last,
//                    merchcont_prim_phone        as contact_phone,
//                    merchcont_prim_email        as contact_email
//            from    merchcontact      
//            where   app_seq_num = :AppSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merchcont_prim_first_name   as contact_name_first,\n                  merchcont_prim_last_name    as contact_name_last,\n                  merchcont_prim_phone        as contact_phone,\n                  merchcont_prim_email        as contact_email\n          from    merchcontact      \n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1730^9*/
        setFields(it.getResultSet());
        it.close();

        // bank account information
        /*@lineinfo:generated-code*//*@lineinfo:1735^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  bankacc_type                as type_of_acct,
//                    merchbank_info_source       as source_of_info,
//                    merchbank_name              as bank_name,
//                    merchbank_acct_num          as checking_account,
//                    merchbank_acct_num          as confirm_checking_account,
//                    merchbank_transit_route_num as transit_routing,
//                    merchbank_transit_route_num as confirm_transit_routing,
//                    merchbank_num_years_open    as years_open
//            from    merchbank
//            where   app_seq_num = :AppSeqNum and 
//                    merchbank_acct_srnum = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bankacc_type                as type_of_acct,\n                  merchbank_info_source       as source_of_info,\n                  merchbank_name              as bank_name,\n                  merchbank_acct_num          as checking_account,\n                  merchbank_acct_num          as confirm_checking_account,\n                  merchbank_transit_route_num as transit_routing,\n                  merchbank_transit_route_num as confirm_transit_routing,\n                  merchbank_num_years_open    as years_open\n          from    merchbank\n          where   app_seq_num =  :1  and \n                  merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1748^9*/
        setFields(it.getResultSet());
        it.close();

        // payment options
        /*@lineinfo:generated-code*//*@lineinfo:1753^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code                   as card_type,
//                    merchpo_card_merch_number       as merchant_number,
//                    merchpo_provider_name           as provider_name,
//                    merchpo_rate                    as rate,
//                    merchpo_fee                     as per_item,
//                    merchpo_split_dial              as split_dial,
//                    merchpo_pip                     as amex_pip
//            from   merchpayoption
//            where  app_seq_num   = :AppSeqNum 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code                   as card_type,\n                  merchpo_card_merch_number       as merchant_number,\n                  merchpo_provider_name           as provider_name,\n                  merchpo_rate                    as rate,\n                  merchpo_fee                     as per_item,\n                  merchpo_split_dial              as split_dial,\n                  merchpo_pip                     as amex_pip\n          from   merchpayoption\n          where  app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1764^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          switch( resultSet.getInt("card_type") )
          {
            case mesConstants.APP_CT_VISA:      // ignore, always selected
            case mesConstants.APP_CT_MC:
              break;

            case mesConstants.APP_CT_DEBIT:
              fields.getField("debitAccepted").setData("y");
              fields.getField("fcsNumber").setData(resultSet.getString("merchant_number"));
              break;

            case mesConstants.APP_CT_EBT:
              fields.getField("ebtAccepted").setData("y");
              fields.getField("fcsNumber").setData(resultSet.getString("merchant_number"));
              break;

            case mesConstants.APP_CT_DINERS_CLUB:
              fields.getField("dinersAccepted").setData("y");
              fields.getField("dinersAcctNum").setData(resultSet.getString("merchant_number"));
              break;

            case mesConstants.APP_CT_JCB:
              fields.getField("jcbAccepted").setData("y");
              fields.getField("jcbAcctNum").setData(resultSet.getString("merchant_number"));
              break;

            case mesConstants.APP_CT_AMEX:
              fields.getField("amexAccepted").setData("y");
              fields.getField("amexAcctNum").setData(resultSet.getString("merchant_number"));
              //fields.getField("amexSplitDial").setData(resultSet.getString("split_dial"));
              //fields.getField("amexPIP").setData(resultSet.getString("amex_pip"));
              if (resultSet.getString("merchant_number") == null)
              {
                fields.setData("amexNewAccount","y");
              }
              break;

            case mesConstants.APP_CT_DISCOVER:
              fields.getField("discoverAccepted").setData("y");
              fields.getField("discoverAcctNum").setData(resultSet.getString("merchant_number"));
              if (resultSet.getString("merchant_number") == null)
              {
                fields.setData("discNewAccount","y");
              }
              break;

            case mesConstants.APP_CT_CHECK_AUTH:
              fields.getField("checkAccepted").setData(resultSet.getString("rate"));
              fields.getField("checkProvider").setData(resultSet.getString("provider_name"));
              fields.getField("checkAcctNum").setData(resultSet.getString("merchant_number"));
              break;

            default:    // ignore
              break;
          }
        }
        resultSet.close();
        it.close();

        // POS type 
        /*@lineinfo:generated-code*//*@lineinfo:1829^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mp.pos_code        as internet_type,
//                    mp.pos_param       as web_url,
//                    pc.pos_type        as product_type
//            from    merch_pos     mp,
//                    pos_category  pc
//            where   mp.app_seq_num = :AppSeqNum and
//                    pc.pos_code = mp.pos_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mp.pos_code        as internet_type,\n                  mp.pos_param       as web_url,\n                  pc.pos_type        as product_type\n          from    merch_pos     mp,\n                  pos_category  pc\n          where   mp.app_seq_num =  :1  and\n                  pc.pos_code = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.app.NetSuiteMerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1838^9*/
        setFields(it.getResultSet());
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadData()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  public void setFields(HttpServletRequest request)
  {
    super.setFields(request);
  }

  public void setCapturedField(String capturedData)
  {
    fields.getField("netSuiteAcctNum").setData(capturedData);
  }
  
  private void setVisaBusTypeCode()
  {
    switch(fields.getField("businessCat").asInteger())
    {
      case VISA_CAT_BUSINESS_SERVICES:
        fields.getField("busCatBusinessGoodsTypes").setData("");
        fields.getField("busCatEcommerceTypes").setData("");
        fields.getField("busCatHealthTypes").setData("");
      break;

      case VISA_CAT_BUSINESS_GOODS:
        fields.getField("busCatBusinessServicesTypes").setData("");
        fields.getField("busCatEcommerceTypes").setData("");
        fields.getField("busCatHealthTypes").setData("");
      break;

      case VISA_CAT_ECOMMERCE_INTERNET:
        fields.getField("busCatBusinessServicesTypes").setData("");
        fields.getField("busCatBusinessGoodsTypes").setData("");
        fields.getField("busCatHealthTypes").setData("");
      break;

      case VISA_CAT_HEALTH_FOOD_SERVICES:
        fields.getField("busCatBusinessServicesTypes").setData("");
        fields.getField("busCatBusinessGoodsTypes").setData("");
        fields.getField("busCatEcommerceTypes").setData("");
      break;

      default:
        fields.getField("busCatBusinessServicesTypes").setData("");
        fields.getField("busCatBusinessGoodsTypes").setData("");
        fields.getField("busCatEcommerceTypes").setData("");
        fields.getField("busCatHealthTypes").setData("");
      break;
    }
  }

  private void setPromotionCodes()
  {
    if( !isBlank(fields.getField("promotionCode1").getData()) && (fields.getField("promotionCode1").getData()).length() == 4 )
    {
      fields.getField("promotionCode1").setData((fields.getField("promotionCode1").getData()).substring(0,1));
      fields.getField("promotionCode2").setData((fields.getField("promotionCode2").getData()).substring(1,2));
      fields.getField("promotionCode3").setData((fields.getField("promotionCode3").getData()).substring(2,3));
      fields.getField("promotionCode4").setData((fields.getField("promotionCode4").getData()).substring(3));
    }
  }

  protected void storeAddressData( int addressType, Field[] fields )
  {
    boolean       skipInsert      = true;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1917^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    address
//          where   app_seq_num = :AppSeqNum and
//                  addresstype_code = :addressType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    address\n        where   app_seq_num =  :1  and\n                addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,addressType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1923^7*/
    
      try
      {
        // only insert the record if address line 1 and
        // the address city are present.
        if ( !fields[FIDX_ADDR_LINE1].isBlank() &&
             !fields[FIDX_ADDR_CITY].isBlank() )
        {
          skipInsert = false;
        }             
      }
      catch( NullPointerException e )
      {
      }
        
      // only insert address with valid data
      if ( skipInsert == false )
      {        
        /*@lineinfo:generated-code*//*@lineinfo:1942^9*/

//  ************************************************************
//  #sql [Ctx] { insert into address 
//            (
//              address_line1,
//              address_line2,
//              address_city,
//              countrystate_code,
//              address_zip,
//              country_code,
//              address_phone,
//              address_fax,
//              app_seq_num,
//              addresstype_code
//            )
//            values 
//            (
//              :(fields[FIDX_ADDR_LINE1]  == null) ? null : fields[FIDX_ADDR_LINE1].getData(),
//              :(fields[FIDX_ADDR_LINE2]  == null) ? null : fields[FIDX_ADDR_LINE2].getData(),
//              :(fields[FIDX_ADDR_CITY]   == null) ? null : fields[FIDX_ADDR_CITY].getData(),
//              :(fields[FIDX_ADDR_STATE]  == null) ? null : fields[FIDX_ADDR_STATE].getData(),
//              :(fields[FIDX_ADDR_ZIP]    == null) ? null : fields[FIDX_ADDR_ZIP].getData(),
//              :(fields[FIDX_ADDR_COUNTRY]== null) ? null : fields[FIDX_ADDR_COUNTRY].getData(),
//              :(fields[FIDX_ADDR_PHONE]  == null) ? null : fields[FIDX_ADDR_PHONE].getData(),
//              :(fields[FIDX_ADDR_FAX]    == null) ? null : fields[FIDX_ADDR_FAX].getData(),
//              :AppSeqNum,
//              :addressType
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2245 = (fields[FIDX_ADDR_LINE1]  == null) ? null : fields[FIDX_ADDR_LINE1].getData();
 String __sJT_2246 = (fields[FIDX_ADDR_LINE2]  == null) ? null : fields[FIDX_ADDR_LINE2].getData();
 String __sJT_2247 = (fields[FIDX_ADDR_CITY]   == null) ? null : fields[FIDX_ADDR_CITY].getData();
 String __sJT_2248 = (fields[FIDX_ADDR_STATE]  == null) ? null : fields[FIDX_ADDR_STATE].getData();
 String __sJT_2249 = (fields[FIDX_ADDR_ZIP]    == null) ? null : fields[FIDX_ADDR_ZIP].getData();
 String __sJT_2250 = (fields[FIDX_ADDR_COUNTRY]== null) ? null : fields[FIDX_ADDR_COUNTRY].getData();
 String __sJT_2251 = (fields[FIDX_ADDR_PHONE]  == null) ? null : fields[FIDX_ADDR_PHONE].getData();
 String __sJT_2252 = (fields[FIDX_ADDR_FAX]    == null) ? null : fields[FIDX_ADDR_FAX].getData();
   String theSqlTS = "insert into address \n          (\n            address_line1,\n            address_line2,\n            address_city,\n            countrystate_code,\n            address_zip,\n            country_code,\n            address_phone,\n            address_fax,\n            app_seq_num,\n            addresstype_code\n          )\n          values \n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2245);
   __sJT_st.setString(2,__sJT_2246);
   __sJT_st.setString(3,__sJT_2247);
   __sJT_st.setString(4,__sJT_2248);
   __sJT_st.setString(5,__sJT_2249);
   __sJT_st.setString(6,__sJT_2250);
   __sJT_st.setString(7,__sJT_2251);
   __sJT_st.setString(8,__sJT_2252);
   __sJT_st.setLong(9,AppSeqNum);
   __sJT_st.setInt(10,addressType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1970^9*/
      }        
    }
    catch(Exception e)
    {
      addError("storeAddressData: " + e.toString());
      logEntry( "storeAddressData (" + addressType + "): ", e.toString());
    }
  }
  
  protected void storeBankData( )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1984^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    merchbank
//          where   app_seq_num = :AppSeqNum and
//                  merchbank_acct_srnum = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    merchbank\n        where   app_seq_num =  :1  and\n                merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1990^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:1992^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchbank       
//          (
//            app_seq_num,
//            bankacc_type,
//            --merchbank_info_source,
//            merchbank_name,
//            merchbank_acct_num,
//            merchbank_transit_route_num,
//            merchbank_num_years_open,
//            merchbank_acct_srnum
//          )
//          values 
//          (
//            :AppSeqNum,
//            :fields.getField("typeOfAcct").getData(),
//            --:(fields.getField("sourceOfInfo").getData()),
//            :fields.getField("bankName").getData(),
//            :fields.getField("checkingAccount").getData(),
//            :fields.getField("transitRouting").getData(),
//            :fields.getField("yearsOpen").getData(),
//            1
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2253 = fields.getField("typeOfAcct").getData();
 String __sJT_2254 = fields.getField("bankName").getData();
 String __sJT_2255 = fields.getField("checkingAccount").getData();
 String __sJT_2256 = fields.getField("transitRouting").getData();
 String __sJT_2257 = fields.getField("yearsOpen").getData();
   String theSqlTS = "insert into merchbank       \n        (\n          app_seq_num,\n          bankacc_type,\n          --merchbank_info_source,\n          merchbank_name,\n          merchbank_acct_num,\n          merchbank_transit_route_num,\n          merchbank_num_years_open,\n          merchbank_acct_srnum\n        )\n        values \n        (\n           :1 ,\n           :2 ,\n          --:(fields.getField(\"sourceOfInfo\").getData()),\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          1\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,__sJT_2253);
   __sJT_st.setString(3,__sJT_2254);
   __sJT_st.setString(4,__sJT_2255);
   __sJT_st.setString(5,__sJT_2256);
   __sJT_st.setString(6,__sJT_2257);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2016^7*/
    }
    catch(Exception e)
    {
      addError("storeBankData: " + e.toString());
      logEntry("storeBankData()", e.toString());
    }
  }
  
  protected void storeBusinessOwnerData(int ownerId, Field[] fields)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2029^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    businessowner
//          where   app_seq_num = :AppSeqNum and
//                  busowner_num = :ownerId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    businessowner\n        where   app_seq_num =  :1  and\n                busowner_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,ownerId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2035^7*/
      
      if ( ( fields[FIDX_OWNER_SSN] != null ) &&
           ( !fields[FIDX_OWNER_SSN].isBlank() ) )
      {           
        /*@lineinfo:generated-code*//*@lineinfo:2040^9*/

//  ************************************************************
//  #sql [Ctx] { insert into businessowner
//            (
//              app_seq_num,
//              busowner_num,
//              busowner_last_name,
//              busowner_first_name,
//              busowner_ssn,
//              busowner_owner_perc,
//              busowner_period_month,
//              busowner_period_year,
//              busowner_title
//            )
//            values 
//            (
//              :AppSeqNum,
//              :ownerId,
//              :(fields[FIDX_OWNER_LAST_NAME]   == null) ? null : fields[FIDX_OWNER_LAST_NAME].getData(),
//              :(fields[FIDX_OWNER_FIRST_NAME]  == null) ? null : fields[FIDX_OWNER_FIRST_NAME].getData(),
//              :(fields[FIDX_OWNER_SSN]         == null) ? null : fields[FIDX_OWNER_SSN].getData(),
//              :(fields[FIDX_OWNER_PERCENT]     == null) ? null : fields[FIDX_OWNER_PERCENT].getData(),
//              :(fields[FIDX_OWNER_MONTH]       == null) ? null : fields[FIDX_OWNER_MONTH].getData(),
//              :(fields[FIDX_OWNER_YEAR]        == null) ? null : fields[FIDX_OWNER_YEAR].getData(),
//              :(fields[FIDX_OWNER_TITLE]       == null) ? null : fields[FIDX_OWNER_TITLE].getData()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2258 = (fields[FIDX_OWNER_LAST_NAME]   == null) ? null : fields[FIDX_OWNER_LAST_NAME].getData();
 String __sJT_2259 = (fields[FIDX_OWNER_FIRST_NAME]  == null) ? null : fields[FIDX_OWNER_FIRST_NAME].getData();
 String __sJT_2260 = (fields[FIDX_OWNER_SSN]         == null) ? null : fields[FIDX_OWNER_SSN].getData();
 String __sJT_2261 = (fields[FIDX_OWNER_PERCENT]     == null) ? null : fields[FIDX_OWNER_PERCENT].getData();
 String __sJT_2262 = (fields[FIDX_OWNER_MONTH]       == null) ? null : fields[FIDX_OWNER_MONTH].getData();
 String __sJT_2263 = (fields[FIDX_OWNER_YEAR]        == null) ? null : fields[FIDX_OWNER_YEAR].getData();
 String __sJT_2264 = (fields[FIDX_OWNER_TITLE]       == null) ? null : fields[FIDX_OWNER_TITLE].getData();
   String theSqlTS = "insert into businessowner\n          (\n            app_seq_num,\n            busowner_num,\n            busowner_last_name,\n            busowner_first_name,\n            busowner_ssn,\n            busowner_owner_perc,\n            busowner_period_month,\n            busowner_period_year,\n            busowner_title\n          )\n          values \n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,ownerId);
   __sJT_st.setString(3,__sJT_2258);
   __sJT_st.setString(4,__sJT_2259);
   __sJT_st.setString(5,__sJT_2260);
   __sJT_st.setString(6,__sJT_2261);
   __sJT_st.setString(7,__sJT_2262);
   __sJT_st.setString(8,__sJT_2263);
   __sJT_st.setString(9,__sJT_2264);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2066^9*/
      }        
    }
    catch(Exception e)
    {
      addError("storeBusinessOwnerData: " + e.toString());
      logEntry("storeBusinessOwnerData( " + ownerId + " )", e.toString());
    }
  }
  
  protected void storeContactData( )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2080^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchcontact
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merchcontact\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2085^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:2087^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchcontact   
//          (
//            app_seq_num,
//            merchcont_prim_first_name,
//            merchcont_prim_last_name,
//            merchcont_prim_phone,
//            merchcont_prim_email
//          )
//          values 
//          (
//            :AppSeqNum,
//            :fields.getField("contactNameFirst").getData(),
//            :fields.getField("contactNameLast").getData(),
//            :fields.getField("contactPhone").getData(),
//            :fields.getField("contactEmail").getData()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2265 = fields.getField("contactNameFirst").getData();
 String __sJT_2266 = fields.getField("contactNameLast").getData();
 String __sJT_2267 = fields.getField("contactPhone").getData();
 String __sJT_2268 = fields.getField("contactEmail").getData();
   String theSqlTS = "insert into merchcontact   \n        (\n          app_seq_num,\n          merchcont_prim_first_name,\n          merchcont_prim_last_name,\n          merchcont_prim_phone,\n          merchcont_prim_email\n        )\n        values \n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,__sJT_2265);
   __sJT_st.setString(3,__sJT_2266);
   __sJT_st.setString(4,__sJT_2267);
   __sJT_st.setString(5,__sJT_2268);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2105^7*/
    }
    catch(Exception e)
    {
      addError("storeContactData: " + e.toString());
      logEntry("storeContactData()", e.toString());
    }
  }
  
  public void storeData()
  {
    try
    {
      if ( AppSeqNum == APP_SEQ_NEW )
      {
        // this will generate a new app sequence nubmer and
        // app control number then create a new entry in 
        // the application and merchant tables 
        newAppInit();
      }
      
      // update the merchant table data
      storeMerchantData();

      // store the addresses
      storeAddressData(mesConstants.ADDR_TYPE_BUSINESS, 
                        new Field[]{  fields.getField("businessAddress1"), 
                                      fields.getField("businessAddress2"),
                                      fields.getField("businessCity"),
                                      fields.getField("businessState"),
                                      fields.getField("businessZip"),
                                      fields.getField("businessCountry"),
                                      fields.getField("businessPhone"),
                                      fields.getField("businessFax") } );
                                      
      storeAddressData(mesConstants.ADDR_TYPE_OWNER1, 
                        new Field[]{  fields.getField("owner1Address1"), 
                                      null,
                                      fields.getField("owner1City"),
                                      fields.getField("owner1State"),
                                      fields.getField("owner1Zip"),
                                      null,
                                      fields.getField("owner1Phone"),
                                      null } );
                                      
      storeAddressData(mesConstants.ADDR_TYPE_CHK_ACCT_BANK, 
                        new Field[]{  fields.getField("bankAddress"), 
                                      null,
                                      fields.getField("bankCity"),
                                      fields.getField("bankState"),
                                      fields.getField("bankZip"),
                                      null,
                                      fields.getField("bankPhone"),
                                      null } );
                
                
      storeBusinessOwnerData( 1, new Field[]{ fields.getField("owner1LastName"),
                                              fields.getField("owner1FirstName"),
                                              fields.getField("owner1SSN"),
                                              fields.getField("owner1Percent"),
                                              fields.getField("owner1SinceMonth"),
                                              fields.getField("owner1SinceYear"),
                                              fields.getField("owner1Title") } );
                            
      // store the remainder of 
      // the data for this page                            
      storeContactData();         // contact data for this application
      storeBankData();            // bank data
      storePayOptions();          // accepted cards
      storePosData();             // pos type
      storeEquipmentData();       // equipment related 
      storePricingData();        // pricing data for cardtypes, since its only one page
      storeMiscFees();            // misc fees for visa app, since its only one page
      storeWarnings();            // warnings the app submission generated
      
      markPageComplete();         // mark this page done in screen_progress
    }
    catch( Exception e )
    {
      addError("storeData: " + e.toString());
      logEntry("storeData()",e.toString());
    }
    finally
    {
    }
  }
  
  protected void storeMiscFees( )
  {
    Field             field       = null;
    
    try 
    {
      
      /*@lineinfo:generated-code*//*@lineinfo:2199^7*/

//  ************************************************************
//  #sql [Ctx] { delete  
//          from   miscchrg
//          where  app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  \n        from   miscchrg\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2204^7*/
        

      /*@lineinfo:generated-code*//*@lineinfo:2207^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          (
//            app_seq_num,
//            misc_code,
//            misc_chrg_amount
//          )
//          values
//          (
//            :AppSeqNum,
//            :mesConstants.APP_MISC_CHARGE_GATEWAY_SETUP,
//            :FEE_GATEWAY_SETUP  
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into miscchrg\n        (\n          app_seq_num,\n          misc_code,\n          misc_chrg_amount\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3   \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"29com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_GATEWAY_SETUP);
   __sJT_st.setInt(3,FEE_GATEWAY_SETUP);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2221^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2223^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          (
//            app_seq_num,
//            misc_code,
//            misc_chrg_amount
//          )
//          values
//          (
//            :AppSeqNum,
//            :mesConstants.APP_MISC_CHARGE_MONTHLY_GATEWAY,
//            :FEE_GATEWAY_MONTHLY
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into miscchrg\n        (\n          app_seq_num,\n          misc_code,\n          misc_chrg_amount\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"30com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_MONTHLY_GATEWAY);
   __sJT_st.setInt(3,FEE_GATEWAY_MONTHLY);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2237^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2239^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          (
//            app_seq_num,
//            misc_code,
//            misc_chrg_amount
//          )
//          values
//          (
//            :AppSeqNum,
//            :mesConstants.APP_MISC_CHARGE_CHARGEBACK,
//            :FEE_CHARGEBACK
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into miscchrg\n        (\n          app_seq_num,\n          misc_code,\n          misc_chrg_amount\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"31com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_CHARGEBACK);
   __sJT_st.setInt(3,FEE_CHARGEBACK);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2253^7*/

/*
      #sql [Ctx]
      {
        insert into miscchrg
        (
          app_seq_num,
          misc_code,
          misc_chrg_amount
        )
        values
        (
          :AppSeqNum,
          :(mesConstants.APP_MISC_CHARGE_ACH_REJECT_FEE),
          :FEE_ACH_REJECT
        )
      };
*/


    }
    catch(Exception e)
    {
      logEntry("storeMiscFees()",e.toString());
      addError("storeMiscFees: " + e.toString());
    }
    finally
    {
    }
  }


  protected void storePricingData()
  {
    try
    {

      /*@lineinfo:generated-code*//*@lineinfo:2291^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from  tranchrg
//          where app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from  tranchrg\n        where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"32com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2296^7*/

      //visa
      /*@lineinfo:generated-code*//*@lineinfo:2299^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran,
//            mid_qualification_downgrade,
//            non_qualification_downgrade
//          )
//          values
//          (
//            :AppSeqNum,
//            :mesConstants.APP_CT_VISA,
//            :mesConstants.APP_PS_FIXED_PLUS_PER_ITEM,
//            :VISA_DISCOUNT_RATE_CARD_PRESENT,
//            :VISA_PER_ITEM_CARD_PRESENT,
//            :FEE_MONTHLY_MIN,
//            0,
//            :VISA_MID_QUAL_DOWNGRADE,
//            :VISA_NON_QUAL_DOWNGRADE
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran,\n          mid_qualification_downgrade,\n          non_qualification_downgrade\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0,\n           :7 ,\n           :8 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"33com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   __sJT_st.setInt(3,mesConstants.APP_PS_FIXED_PLUS_PER_ITEM);
   __sJT_st.setString(4,VISA_DISCOUNT_RATE_CARD_PRESENT);
   __sJT_st.setString(5,VISA_PER_ITEM_CARD_PRESENT);
   __sJT_st.setInt(6,FEE_MONTHLY_MIN);
   __sJT_st.setString(7,VISA_MID_QUAL_DOWNGRADE);
   __sJT_st.setString(8,VISA_NON_QUAL_DOWNGRADE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2325^7*/

      //Mastercard
      /*@lineinfo:generated-code*//*@lineinfo:2328^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran,
//            mid_qualification_downgrade,
//            non_qualification_downgrade
//          )
//          values
//          (
//            :AppSeqNum,
//            :mesConstants.APP_CT_MC,
//            :mesConstants.APP_PS_FIXED_PLUS_PER_ITEM,
//            :VISA_DISCOUNT_RATE_CARD_PRESENT,
//            :VISA_PER_ITEM_CARD_PRESENT,
//            :FEE_MONTHLY_MIN,
//            0,
//            :VISA_MID_QUAL_DOWNGRADE,
//            :VISA_NON_QUAL_DOWNGRADE
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran,\n          mid_qualification_downgrade,\n          non_qualification_downgrade\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0,\n           :7 ,\n           :8 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"34com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_MC);
   __sJT_st.setInt(3,mesConstants.APP_PS_FIXED_PLUS_PER_ITEM);
   __sJT_st.setString(4,VISA_DISCOUNT_RATE_CARD_PRESENT);
   __sJT_st.setString(5,VISA_PER_ITEM_CARD_PRESENT);
   __sJT_st.setInt(6,FEE_MONTHLY_MIN);
   __sJT_st.setString(7,VISA_MID_QUAL_DOWNGRADE);
   __sJT_st.setString(8,VISA_NON_QUAL_DOWNGRADE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2354^7*/


      for (int ct = 2; ct < CT_COUNT; ++ct)
      {
        // extract the accepted flag
        String fname = CardTypeToFieldNames[ct][FNAME_ACCEPTED];
        
        if ( fname == null )
        {
          continue;     // skip this entry
        }
        
        boolean accepted = fields.getField(fname).getData().toUpperCase().equals("Y");
          
        // convert the card type index into the application
        // specific card type using the lookup array
        int appCardType = CardTypeToAppCardType[ct];

        if ( accepted == true )
        {
          /*@lineinfo:generated-code*//*@lineinfo:2375^11*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//              (
//                app_seq_num,
//                cardtype_code,
//                tranchrg_per_tran
//              )
//              values
//              (
//                :AppSeqNum,
//                :appCardType,
//                :NON_BANKCARD_PER_ITEM_FEE
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into tranchrg\n            (\n              app_seq_num,\n              cardtype_code,\n              tranchrg_per_tran\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"35com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,appCardType);
   __sJT_st.setString(3,NON_BANKCARD_PER_ITEM_FEE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2389^11*/
        }
        else
        {
          //System.out.println("this card no accepto: " + appCardType);
        }
      }

      //put in internet transaction fee
      /*@lineinfo:generated-code*//*@lineinfo:2398^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_per_tran
//          )
//          values
//          (
//            :AppSeqNum,
//            :CT_INTERNET,
//            :INTERNET_GATEWAY_TRANSACTION_FEE
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_per_tran\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"36com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,CT_INTERNET);
   __sJT_st.setString(3,INTERNET_GATEWAY_TRANSACTION_FEE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2412^7*/


    }
    catch (Exception e)
    {
      addError("storePricingData: " + e.toString());
      logEntry("storePricingData()", e.toString());
    }

  }


  protected void storeEquipmentData()
  {
    int     imprinterPlateCount   = 0;//fields.getField("imprinterPlates").asInteger();
    
    int     productType           = fields.getField("productType").asInteger();
    
    try
    {
      switch( productType )
      {
        case mesConstants.POS_DIAL_TERMINAL:
        case mesConstants.POS_WIRELESS_TERMINAL:
          // delete any entries for a PC product but leave 
          // the other equipment rows in alone.  these
          // rows are manipulated through the equipment 
          // selection page of the application.
          /*@lineinfo:generated-code*//*@lineinfo:2441^11*/

//  ************************************************************
//  #sql [Ctx] { delete 
//              from    merchequipment
//              where   app_seq_num = :AppSeqNum and
//                      equip_model = 'PCPS'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n            from    merchequipment\n            where   app_seq_num =  :1  and\n                    equip_model = 'PCPS'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"37com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2447^11*/
          
          //now add the terminal, the imprinter and the imprinter plates (hardcoded for all visa apps)
          /*@lineinfo:generated-code*//*@lineinfo:2450^11*/

//  ************************************************************
//  #sql [Ctx] { insert into merchequipment
//              (
//                app_seq_num,
//                equiptype_code,
//                equiplendtype_code,
//                merchequip_amount,
//                equip_model,
//                merchequip_equip_quantity,
//                quantity_deployed,
//                prod_option_id
//              )
//              values 
//              (
//                :AppSeqNum,
//                :mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES,
//                :mesConstants.APP_EQUIP_PURCHASE,
//                :IMPRINTER_PLATE_FEE,
//                :IMPRINTER_PLATE_MODEL,
//                1,
//                0,
//                null
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchequipment\n            (\n              app_seq_num,\n              equiptype_code,\n              equiplendtype_code,\n              merchequip_amount,\n              equip_model,\n              merchequip_equip_quantity,\n              quantity_deployed,\n              prod_option_id\n            )\n            values \n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n              1,\n              0,\n              null\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"38com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(4,IMPRINTER_PLATE_FEE);
   __sJT_st.setString(5,IMPRINTER_PLATE_MODEL);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2474^11*/
          
          /*@lineinfo:generated-code*//*@lineinfo:2476^11*/

//  ************************************************************
//  #sql [Ctx] { insert into merchequipment
//              (
//                app_seq_num,
//                equiptype_code,
//                equiplendtype_code,
//                merchequip_amount,
//                equip_model,
//                merchequip_equip_quantity,
//                quantity_deployed,
//                prod_option_id
//              )
//              values 
//              (
//                :AppSeqNum,
//                :mesConstants.APP_EQUIP_TYPE_IMPRINTER,
//                :mesConstants.APP_EQUIP_PURCHASE,
//                :IMPRINTER_PRICE,
//                :IMPRINTER_MODEL,
//                1,
//                0,
//                null
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchequipment\n            (\n              app_seq_num,\n              equiptype_code,\n              equiplendtype_code,\n              merchequip_amount,\n              equip_model,\n              merchequip_equip_quantity,\n              quantity_deployed,\n              prod_option_id\n            )\n            values \n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n              1,\n              0,\n              null\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"39com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_IMPRINTER);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(4,IMPRINTER_PRICE);
   __sJT_st.setString(5,IMPRINTER_MODEL);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2500^11*/

          /*@lineinfo:generated-code*//*@lineinfo:2502^11*/

//  ************************************************************
//  #sql [Ctx] { insert into merchequipment
//              (
//                app_seq_num,
//                equiptype_code,
//                equiplendtype_code,
//                merchequip_amount,
//                equip_model,
//                merchequip_equip_quantity,
//                quantity_deployed,
//                prod_option_id
//              )
//              values 
//              (
//                :AppSeqNum,
//                :mesConstants.APP_EQUIP_TYPE_TERM_PRINTER,
//                :mesConstants.APP_EQUIP_PURCHASE,
//                :T7P_TERMINAL_PRICE,
//                :T7P_TERMINAL_MODEL,
//                1,
//                0,
//                :T7P_TERMINAL_PRODUCT_OPTION
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchequipment\n            (\n              app_seq_num,\n              equiptype_code,\n              equiplendtype_code,\n              merchequip_amount,\n              equip_model,\n              merchequip_equip_quantity,\n              quantity_deployed,\n              prod_option_id\n            )\n            values \n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n              1,\n              0,\n               :6 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"40com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_TERM_PRINTER);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(4,T7P_TERMINAL_PRICE);
   __sJT_st.setString(5,T7P_TERMINAL_MODEL);
   __sJT_st.setString(6,T7P_TERMINAL_PRODUCT_OPTION);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2526^11*/

          
          break;
          
        case mesConstants.POS_DIAL_AUTH:
          // delete everything except imprinters.
          // imprinters are setup through the equipment
          // selection page when the POS type is dial pay
          /*@lineinfo:generated-code*//*@lineinfo:2535^11*/

//  ************************************************************
//  #sql [Ctx] { delete 
//              from    merchequipment
//              where   app_seq_num = :AppSeqNum and
//                      equiptype_code <> :mesConstants.APP_EQUIP_TYPE_IMPRINTER
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n            from    merchequipment\n            where   app_seq_num =  :1  and\n                    equiptype_code <>  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"41com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_IMPRINTER);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2541^11*/
          break;
          
        case mesConstants.POS_STAGE_ONLY:
        case mesConstants.POS_GLOBAL_PC:
          // leave merchant equipment alone for these types
          break;
          
//        case mesConstants.POS_PC:
//        case mesConstants.POS_INTERNET:
//        case mesConstants.POS_OTHER:
//        case mesConstants.POS_GPS:
        default:
          // delete all the equipment for this application
          /*@lineinfo:generated-code*//*@lineinfo:2555^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    merchequipment
//              where   app_seq_num = :AppSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    merchequipment\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"42com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2560^11*/
          break;
      }
      
      // if this is a PC product, create a row for the PC software
      if (productType == mesConstants.POS_PC)
      {
        /*@lineinfo:generated-code*//*@lineinfo:2567^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchequipment 
//            (
//              app_seq_num,
//              merchequip_equip_quantity,
//              equiplendtype_code,
//              equiptype_code,
//              equip_model
//            )
//            values 
//            ( 
//              :AppSeqNum,
//              1,        -- one item
//              :mesConstants.APP_EQUIP_PURCHASE,
//              :mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE,
//              'PCPC'
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchequipment \n          (\n            app_seq_num,\n            merchequip_equip_quantity,\n            equiplendtype_code,\n            equiptype_code,\n            equip_model\n          )\n          values \n          ( \n             :1 ,\n            1,        -- one item\n             :2 ,\n             :3 ,\n            'PCPC'\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"43com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2585^9*/
      }

      if( imprinterPlateCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:2590^9*/

//  ************************************************************
//  #sql [Ctx] { delete 
//            from    merchequipment
//            where   app_seq_num = :AppSeqNum and
//                    equiptype_code = :mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n          from    merchequipment\n          where   app_seq_num =  :1  and\n                  equiptype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"44com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2596^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:2598^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchequipment
//            (
//              app_seq_num,
//              equiptype_code,
//              equiplendtype_code,
//              merchequip_amount,
//              equip_model,
//              merchequip_equip_quantity,
//              prod_option_id
//            )
//            values 
//            (
//              :AppSeqNum,
//              :mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES,
//              :mesConstants.APP_EQUIP_PURCHASE,
//              null,
//              'IPPL',
//              :imprinterPlateCount,
//              null
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchequipment\n          (\n            app_seq_num,\n            equiptype_code,\n            equiplendtype_code,\n            merchequip_amount,\n            equip_model,\n            merchequip_equip_quantity,\n            prod_option_id\n          )\n          values \n          (\n             :1 ,\n             :2 ,\n             :3 ,\n            null,\n            'IPPL',\n             :4 ,\n            null\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"45com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(4,imprinterPlateCount);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2620^9*/
      }
    }
    catch (Exception e)
    {
      addError("storeEquipmentData: " + e.toString());
      logEntry("storeEquipmentData()", e.toString());
    }
  }

  private int getVisaBusTypeCode()
  {
    int retVal = 0;

    switch(fields.getField("businessCat").asInteger())
    {
      case VISA_CAT_BUSINESS_SERVICES:
        retVal = fields.getField("busCatBusinessServicesTypes").asInteger();
      break;

      case VISA_CAT_BUSINESS_GOODS:
        retVal = fields.getField("busCatBusinessGoodsTypes").asInteger();
      break;

      case VISA_CAT_ECOMMERCE_INTERNET:
        retVal = fields.getField("busCatEcommerceTypes").asInteger();
      break;

      case VISA_CAT_HEALTH_FOOD_SERVICES:
        retVal = fields.getField("busCatHealthTypes").asInteger();
      break;

      case VISA_CAT_OTHER:
        retVal = fields.getField("businessCat").asInteger();
      break;
    }

    return retVal;
  }

  private String getPromotionCode()
  {
    return fields.getField("promotionCode1").getData() + fields.getField("promotionCode2").getData() + fields.getField("promotionCode3").getData() + fields.getField("promotionCode4").getData();
  }


  protected void storeMerchantData()
  {
    try
    {
      
      /*@lineinfo:generated-code*//*@lineinfo:2671^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_business_name         = :fields.getField("businessName").getData(),
//                  merch_federal_tax_id        = :fields.getField("taxpayerId").getData(),
//                  merch_email_address         = :fields.getField("businessEmail").getData(),
//                  merch_legal_name            = :fields.getField("businessLegalName").getData(),
//                  merch_business_establ_month = :fields.getField("establishedMonth").getData(),
//                  merch_business_establ_year  = :fields.getField("establishedYear").getData(),
//                  bustype_code                = :fields.getField("businessType").getData(),
//                  net_suite_acct_num          = :fields.getField("netSuiteAcctNum").getData(),
//   
//                  --promotion_code              = :(getPromotionCode()),
//                  industype_code              = '8',
//                  merch_num_of_locations      = '1',
//                  loctype_code                = '2',
//                  merch_application_type      = '1',
//                  merch_mail_phone_sales        = :fields.getField("motoPercentage").getData(),
//                  --merch_referring_bank        = :(fields.getField("referringBank").getData()),
//                  merch_web_url               = :fields.getField("webUrl").getData(),
//                  pricing_grid                = '2',
//                  bet_type_code               = '2',
//                  merch_busgoodserv_descr     = :fields.getField("businessDesc").getData(),
//                  merch_years_at_loc          = :fields.getField("locationYears").getData(),
//                  merch_prior_cc_accp_flag    = :fields.getField("haveProcessed").getData(),
//                  merch_prior_processor       = :fields.getField("previousProcessor").getData(),
//                  merch_cc_acct_term_flag     = :fields.getField("haveCanceled").getData(),
//                  merch_cc_term_name          = :fields.getField("canceledProcessor").getData(),
//                  merch_term_reason           = :fields.getField("canceledReason").getData(),
//                  merch_term_month            = :fields.getField("cancelMonth").getData(),
//                  merch_term_year             = :fields.getField("cancelYear").getData(),
//                  merch_month_tot_proj_sales  = :fields.getField("monthlySales").getData(),
//                  merch_month_visa_mc_sales   = :fields.getField("monthlyVMCSales").getData(),
//                  merch_average_cc_tran       = :fields.getField("averageTicket").getData(),
//                  merch_notes                 = :fields.getField("comments").getData(),
//                  refundtype_code             = :fields.getField("refundPolicy").getData(),
//                  merch_prior_statements      = :fields.getField("statementsProvided").getData(),
//                  promotion_code              = :fields.getField("promotionCode").getData()
//          where   app_seq_num                 = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2269 = fields.getField("businessName").getData();
 String __sJT_2270 = fields.getField("taxpayerId").getData();
 String __sJT_2271 = fields.getField("businessEmail").getData();
 String __sJT_2272 = fields.getField("businessLegalName").getData();
 String __sJT_2273 = fields.getField("establishedMonth").getData();
 String __sJT_2274 = fields.getField("establishedYear").getData();
 String __sJT_2275 = fields.getField("businessType").getData();
 String __sJT_2276 = fields.getField("netSuiteAcctNum").getData();
 String __sJT_2277 = fields.getField("motoPercentage").getData();
 String __sJT_2278 = fields.getField("webUrl").getData();
 String __sJT_2279 = fields.getField("businessDesc").getData();
 String __sJT_2280 = fields.getField("locationYears").getData();
 String __sJT_2281 = fields.getField("haveProcessed").getData();
 String __sJT_2282 = fields.getField("previousProcessor").getData();
 String __sJT_2283 = fields.getField("haveCanceled").getData();
 String __sJT_2284 = fields.getField("canceledProcessor").getData();
 String __sJT_2285 = fields.getField("canceledReason").getData();
 String __sJT_2286 = fields.getField("cancelMonth").getData();
 String __sJT_2287 = fields.getField("cancelYear").getData();
 String __sJT_2288 = fields.getField("monthlySales").getData();
 String __sJT_2289 = fields.getField("monthlyVMCSales").getData();
 String __sJT_2290 = fields.getField("averageTicket").getData();
 String __sJT_2291 = fields.getField("comments").getData();
 String __sJT_2292 = fields.getField("refundPolicy").getData();
 String __sJT_2293 = fields.getField("statementsProvided").getData();
 String __sJT_2294 = fields.getField("promotionCode").getData();
   String theSqlTS = "update  merchant\n        set     merch_business_name         =  :1 ,\n                merch_federal_tax_id        =  :2 ,\n                merch_email_address         =  :3 ,\n                merch_legal_name            =  :4 ,\n                merch_business_establ_month =  :5 ,\n                merch_business_establ_year  =  :6 ,\n                bustype_code                =  :7 ,\n                net_suite_acct_num          =  :8 ,\n \n                --promotion_code              = :(getPromotionCode()),\n                industype_code              = '8',\n                merch_num_of_locations      = '1',\n                loctype_code                = '2',\n                merch_application_type      = '1',\n                merch_mail_phone_sales        =  :9 ,\n                --merch_referring_bank        = :(fields.getField(\"referringBank\").getData()),\n                merch_web_url               =  :10 ,\n                pricing_grid                = '2',\n                bet_type_code               = '2',\n                merch_busgoodserv_descr     =  :11 ,\n                merch_years_at_loc          =  :12 ,\n                merch_prior_cc_accp_flag    =  :13 ,\n                merch_prior_processor       =  :14 ,\n                merch_cc_acct_term_flag     =  :15 ,\n                merch_cc_term_name          =  :16 ,\n                merch_term_reason           =  :17 ,\n                merch_term_month            =  :18 ,\n                merch_term_year             =  :19 ,\n                merch_month_tot_proj_sales  =  :20 ,\n                merch_month_visa_mc_sales   =  :21 ,\n                merch_average_cc_tran       =  :22 ,\n                merch_notes                 =  :23 ,\n                refundtype_code             =  :24 ,\n                merch_prior_statements      =  :25 ,\n                promotion_code              =  :26 \n        where   app_seq_num                 =  :27";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"46com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2269);
   __sJT_st.setString(2,__sJT_2270);
   __sJT_st.setString(3,__sJT_2271);
   __sJT_st.setString(4,__sJT_2272);
   __sJT_st.setString(5,__sJT_2273);
   __sJT_st.setString(6,__sJT_2274);
   __sJT_st.setString(7,__sJT_2275);
   __sJT_st.setString(8,__sJT_2276);
   __sJT_st.setString(9,__sJT_2277);
   __sJT_st.setString(10,__sJT_2278);
   __sJT_st.setString(11,__sJT_2279);
   __sJT_st.setString(12,__sJT_2280);
   __sJT_st.setString(13,__sJT_2281);
   __sJT_st.setString(14,__sJT_2282);
   __sJT_st.setString(15,__sJT_2283);
   __sJT_st.setString(16,__sJT_2284);
   __sJT_st.setString(17,__sJT_2285);
   __sJT_st.setString(18,__sJT_2286);
   __sJT_st.setString(19,__sJT_2287);
   __sJT_st.setString(20,__sJT_2288);
   __sJT_st.setString(21,__sJT_2289);
   __sJT_st.setString(22,__sJT_2290);
   __sJT_st.setString(23,__sJT_2291);
   __sJT_st.setString(24,__sJT_2292);
   __sJT_st.setString(25,__sJT_2293);
   __sJT_st.setString(26,__sJT_2294);
   __sJT_st.setLong(27,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2710^7*/
    }
    catch (Exception e)
    {
      addError( "storeMerchantData: " + e.toString() );
      logEntry( "storeMerchantData()", e.toString() );
    }
    finally
    {
    }
  }
  
  protected void storePayOptions()
  {
    String        acctId            = null;
    String        amexSplitDial     = null;
    String        amexPIP           = null;
    int           appCardType       = 0;
    boolean       accepted          = false;
    String        fee               = "";
    String        fname             = null;  
    String        providerName      = "";
    String        rate              = "";
    int           recId             = 0;
    
    
    try
    {
      for (int ct = 0; ct < CT_COUNT; ++ct)
      {
        // reset the per card type elements
        rate          = null;
        fee           = null;
        providerName  = null;
        amexSplitDial = "N";
        amexPIP       = "N";
        
        // extract the accepted flag
        fname = CardTypeToFieldNames[ct][FNAME_ACCEPTED];
        if ( fname == null )
        {
          continue;     // skip this entry
        }
        
        accepted = fields.getField(fname).getData().toUpperCase().equals("Y");
          

        // extract the account number
        try
        {
          fname   = CardTypeToFieldNames[ct][FNAME_ACCOUNT_NUMBER];
          acctId  = fields.getField(fname).getData();
          if(acctId == null || acctId.equals(""))
          {
            acctId = "0";
          }
        }
        catch( NullPointerException e )
        {
          // no account number for this card type.. set account number to zero
          acctId  = "0";
        }          
        
        // convert the card type index into the application
        // specific card type using the lookup array
        appCardType = CardTypeToAppCardType[ct];

        // extract data from card type specific fields
        switch (ct)
        {
          case CT_DISCOVER:
            if (fields.getData("discNewAccount").toUpperCase().equals("Y"))
            {
              rate = "2.44";
              fee  = "0.08";
            }
            break;

          case CT_AMEX:
            if (fields.getData("amexNewAccount").toUpperCase().equals("Y"))
            {
              rate = "3.2";
            }
            //amexSplitDial = fields.getField("amexSplitDial").getData().toUpperCase();
            //amexPIP       = fields.getField("amexPIP").getData().toUpperCase();
            break;

          case CT_CHECK:
            providerName  = fields.getField("checkProvider").getData();
            break;
        }
        

        // remove any entries for this card type 
        // from the table before inserting
        /*@lineinfo:generated-code*//*@lineinfo:2805^9*/

//  ************************************************************
//  #sql [Ctx] { delete 
//            from    merchpayoption
//            where   app_seq_num = :AppSeqNum and
//                    cardtype_code = :appCardType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n          from    merchpayoption\n          where   app_seq_num =  :1  and\n                  cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"47com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,appCardType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2811^9*/
        
        if ( accepted == true )
        {
          recId++;        // increment the row id
          
          /*@lineinfo:generated-code*//*@lineinfo:2817^11*/

//  ************************************************************
//  #sql [Ctx] { insert into  merchpayoption            
//              (
//                app_seq_num,
//                cardtype_code,
//                merchpo_card_merch_number,
//                merchpo_provider_name,
//                card_sr_number,
//                merchpo_rate,
//                merchpo_split_dial,
//                merchpo_pip,
//                merchpo_fee
//              )
//              values
//              (
//                :AppSeqNum,
//                :appCardType,
//                :acctId,
//                :providerName,
//                :recId,
//                :rate,
//                :amexSplitDial,   -- n/a unless amex
//                :amexPIP,         -- n/a unless amex
//                :fee
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into  merchpayoption            \n            (\n              app_seq_num,\n              cardtype_code,\n              merchpo_card_merch_number,\n              merchpo_provider_name,\n              card_sr_number,\n              merchpo_rate,\n              merchpo_split_dial,\n              merchpo_pip,\n              merchpo_fee\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n               :7 ,   -- n/a unless amex\n               :8 ,         -- n/a unless amex\n               :9 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"48com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,appCardType);
   __sJT_st.setString(3,acctId);
   __sJT_st.setString(4,providerName);
   __sJT_st.setInt(5,recId);
   __sJT_st.setString(6,rate);
   __sJT_st.setString(7,amexSplitDial);
   __sJT_st.setString(8,amexPIP);
   __sJT_st.setString(9,fee);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2843^11*/
        }
        else      // card type is not accepted
        {
          // remove any tranchrg entries that have already
          // bees setup for this card type
          /*@lineinfo:generated-code*//*@lineinfo:2849^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    tranchrg
//              where   app_seq_num = :AppSeqNum and
//                      cardtype_code = :appCardType
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    tranchrg\n            where   app_seq_num =  :1  and\n                    cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"49com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,appCardType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2855^11*/
        }
      }
    }
    catch(Exception e)
    {
      addError("storePayOptions: " + e.toString());
      logEntry("storePayOptions()", e.toString());
    }
  }
  
  protected void storePosData( )
  {
    String                  edcFlag       = null;
    String                  param         = null;  
    int                     posCode       = 0;
    int                     productType   = 0;
    
    try
    {
      // extract an integer version of the product type from the form field
      productType = fields.getField("productType").asInteger();

      // delete the rows for this sequence number      
      /*@lineinfo:generated-code*//*@lineinfo:2879^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    merch_pos
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    merch_pos\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"50com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2884^7*/
      
      // set the product specific values
      switch ( productType )
      {
        case mesConstants.POS_WIRELESS_TERMINAL:
          posCode = mesConstants.APP_MPOS_WIRELESS_TERMINAL;
          edcFlag = "Y";
          break;

        case mesConstants.POS_PC:
          posCode = mesConstants.APP_MPOS_POS_PARTNER_2000;
          edcFlag = "N";          
          break;

        case mesConstants.POS_INTERNET:
          posCode = Integer.parseInt( fields.getField("internetType").getData() );
          param   = fields.getField("webUrl").getData();
          edcFlag = "N";          
          break;

        case mesConstants.POS_DIAL_AUTH:
          posCode = mesConstants.APP_MPOS_DIAL_PAY;
          edcFlag = "N";          
          break;

        case mesConstants.POS_OTHER:
          posCode = mesConstants.APP_MPOS_OTHER_VITAL_PRODUCT;
          param   = fields.getField("vitalProduct").getData();
          edcFlag = "N";          
          break;

        case mesConstants.POS_STAGE_ONLY:
          posCode = mesConstants.APP_MPOS_STAGE_ONLY;
          edcFlag = "N";          
          break;
          
        case mesConstants.POS_GLOBAL_PC:
          posCode = mesConstants.APP_MPOS_GLOBAL_PC_WINDOWS;
          edcFlag = "N";
          break;
          
        case mesConstants.POS_GPS:
          posCode = mesConstants.APP_MPOS_GPS;
           edcFlag = "N";
          break;
          
//        case mesConstants.POS_DIAL_TERMINAL:
        default:
          posCode = mesConstants.APP_MPOS_DIAL_TERMINAL;     
          edcFlag = "Y";          
          break;
      }
      
      // delete tran charge records for internet and dialpay
      // except when the current product is one of those products
      // this insures that the tranchrg table will get cleaned up
      // in the event that the product is changed from internet or 
      // dialpay to something different.
      /*@lineinfo:generated-code*//*@lineinfo:2943^7*/

//  ************************************************************
//  #sql [Ctx] { delete  
//          from    tranchrg
//          where   app_seq_num = :AppSeqNum and
//                  cardtype_code in 
//                    ( 
//                      :mesConstants.APP_CT_INTERNET, -- 20,
//                      :mesConstants.APP_CT_DIAL_PAY  -- 21
//                    ) and
//                  cardtype_code <> :posCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  \n        from    tranchrg\n        where   app_seq_num =  :1  and\n                cardtype_code in \n                  ( \n                     :2 , -- 20,\n                     :3   -- 21\n                  ) and\n                cardtype_code <>  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"51com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_INTERNET);
   __sJT_st.setInt(3,mesConstants.APP_CT_DIAL_PAY);
   __sJT_st.setInt(4,posCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2954^7*/
      
      // update the EDC flag in the merchant table based on the
      // new product selection
      /*@lineinfo:generated-code*//*@lineinfo:2958^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_edc_flag = :edcFlag
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     merch_edc_flag =  :1 \n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"52com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,edcFlag);
   __sJT_st.setLong(2,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2963^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:2965^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_pos 
//          (
//            app_seq_num,
//            pos_code,
//            pos_param
//          )
//          values
//          (
//            :AppSeqNum,
//            :posCode,
//            :param
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_pos \n        (\n          app_seq_num,\n          pos_code,\n          pos_param\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"53com.mes.app.NetSuiteMerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,posCode);
   __sJT_st.setString(3,param);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2979^7*/
    }
    catch (Exception e)
    {
      addError("storePosData: " + e.toString());
      logEntry("storePosData()", e.toString());
    }
  }
}/*@lineinfo:generated-code*/