package com.mes.app;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import com.mes.support.MesMath;
import com.mes.tools.DropDownTable;

/**
 * A drop down table for DropDownField's that want to give a list of (usually)
 * fractional dollar amounts.  Handles the formatting of the amounts, etc.
 */

public class DollarAmountTable extends DropDownTable
{

  /**
   * Constructor.
   *
   * Given a string containing comma-seperated values (parsable as doubles),
   * drop down elements are created.  A flag indicates whether or not a 
   * "non-selected" option starts the list.  Another flag allows a variation 
   * where the descriptor says "No Fee".  Whenever a zero value is encountered 
   * the descriptor is padded with html non-breaking space character codes.  
   * This will generally cause drop down fields to be displayed with a 
   * consistent width.
   */
  public DollarAmountTable(boolean hasNonselected, boolean hasNoFee, 
    String amountsStr)
  {
    // process amounts string into individual amount items
    List amounts = new ArrayList();
    StringTokenizer tok = new StringTokenizer(amountsStr,",");
    while (tok.hasMoreTokens())
    {
      // convert amt to double and back to make sure it gets rendered
      // as a string in the proper format (i.e. "0.1" and "3.0" as opposed
      // to ".1" or "3")
      amounts.add(Double.toString(Double.parseDouble(tok.nextToken())));
    }
      
    // add optional selections
    if (hasNonselected)
    {
      addElement("","select one");
    }
    if (hasNoFee)
    {
      addElement("","No Fee");
    }
    
    // add each amount as a selection
    for (Iterator i = amounts.iterator(); i.hasNext();)
    {
      String value = (String)i.next();
      
      //replaced this with toFractionalCurrency method
      //String descriptor = MesMath.toCurrency(value);
      
      //BBT had a couple values that were precise to 3 decimal places 
      //and the toCurrency method was rounding them up, 
      //so I used this method instead and specified 3 places of precision
      String descriptor 
        = MesMath.toFractionalCurrency(Double.parseDouble(value.trim()),3);

      if (Double.parseDouble(value) == 0)
      {
        descriptor += "&nbsp;&nbsp;&nbsp;";
      }
      addElement(value,descriptor);
    }
  }
  public DollarAmountTable(String amountsStr)
  {
    this(false,false,amountsStr);
  }
}

