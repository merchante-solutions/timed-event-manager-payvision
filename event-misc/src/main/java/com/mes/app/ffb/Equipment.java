/**************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/ffb/Equipment.java $

  Description:
  
  Equipment
  
  1st Financial Bank online application equipment page bean.


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/21/04 11:04a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.ffb;

import javax.servlet.http.HttpServletRequest;
import com.mes.app.EquipmentBase;

public class Equipment extends EquipmentBase
{
  {
    appType = 41;
    curScreenId = 2;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.getField("trainingType").makeRequired();
      
      // reset all fields, including those just added to be of class formText
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
}
