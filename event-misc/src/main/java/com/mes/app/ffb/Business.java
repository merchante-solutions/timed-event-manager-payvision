/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/ffb/Business.sqlj $

  Description:
  
  Business
  
  1st Financial Bank online application merchant information page bean.

  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/15/04 4:19p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.ffb;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.DropDownField;
import sqlj.runtime.ResultSetIterator;

public class Business extends com.mes.app.BusinessBase
{
  {
    appType = 41;
    curScreenId = 1;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // make some fields required
      fields.getField("mailingName").makeRequired();
      fields.getField("mailingAddress1").makeRequired();
      fields.getField("mailingCsz").makeRequired();
      fields.getField("accountType").makeRequired();
      fields.getField("taxpayerId").makeRequired();
      fields.getField("yearsOpen").makeRequired();
      
      // make some fields not required
      fields.getField("sourceOfInfo").makeOptional();
      
      // add validation of the confirmation fields
      fields.getField("confirmCheckingAccount")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("checkingAccount"),"Checking Account #"));
      fields.getField("confirmTransitRouting")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("transitRouting"),"Transit Routing #"));
      
      // add referring branch assoc num dropdown
      fields.add(new DropDownField("assocNum",new BranchTable(3941400073L),false));

      // alternate error indicator
//@      fields.setFixImage("/images/arrow1_left.gif",10,10);
      
      // remove amex and discover validations
      fields.getField("amexAccepted").removeValidation("amexValidation");
      fields.getField("discoverAccepted").removeValidation("discoverValidation");
      
      createPosPartnerExtendedFields();
      
      // reset all fields, including those just added to be of class form
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
    
    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }
  }
  
  protected boolean loadAppData()
  {
    long                appSeqNum   = 0L;
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = super.loadAppData();
    
    try
    {
      appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if(result && appSeqNum != 0)
      {
        connect();
      
        // load client data
        /*@lineinfo:generated-code*//*@lineinfo:123^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  client_data_1
//            from    merchant
//            where   app_seq_num = :fields.getField("appSeqNum").asInteger()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2872 = fields.getField("appSeqNum").asInteger();
  try {
   String theSqlTS = "select  client_data_1\n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.ffb.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_2872);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.ffb.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:128^9*/
      
        rs = it.getResultSet();
      
        setFields(rs);
      
        result = true;
      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::loadAppData(): " + e.toString());
      logEntry("loadAppData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  protected boolean submitAppData()
  {
    long                  appSeqNum   = 0L;
    boolean               result      = super.submitAppData();
    
    try
    {
      appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if(result && appSeqNum != 0L)
      {
        connect();
      
        // update merchant table with cross ref field
        /*@lineinfo:generated-code*//*@lineinfo:166^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     client_data_1 = :fields.getField("clientData1").getData()
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2873 = fields.getField("clientData1").getData();
   String theSqlTS = "update  merchant\n          set     client_data_1 =  :1 \n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.app.ffb.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2873);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:171^9*/
      
        result = true;
      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::submitAppData(): " + e.toString());
      logEntry("submitAppData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
}/*@lineinfo:generated-code*/