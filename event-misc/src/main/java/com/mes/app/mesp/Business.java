/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: $

  Description:

  Business

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.mesp;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.forms.SicField;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Business extends com.mes.app.BusinessBase
{
  {
    appType = 73;
    curScreenId = 1;
  }

  protected class BusinessTypeTable extends DropDownTable
  {
    public BusinessTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Sole Proprietorship");
      addElement("2","Corporation");
      addElement("3","Partnership");
      addElement("4","Medical or Legal Corporation");
      addElement("6","Tax Exempt");
      addElement("7","Government");
      addElement("8","Limited Liability Company");
    }
  }

  protected class IndustryTypeTable extends DropDownTable
  {
    public IndustryTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail - 010450");
      addElement("2","Restaurant - 011100");
      addElement("10","Lodging - 011101");
      addElement("17","Supermarket - 010450");
      addElement("5","Internet - 010450");
      addElement("7","Direct Marketing - 010450");
      addElement("8","Other - 010450");
      addElement("6","Services - 010450");
      addElement("9","Cash Advance - 010450");
    }
  }

  protected class LocationTypeTable extends DropDownTable
  {
    public LocationTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail Storefront");
      addElement("2","Internet Storefront");
      addElement("3","Business Office");
      addElement("4","Private Residence");
      addElement("7","Separate Building");
      addElement("6","Bank");
      addElement("5","Other");
    }
  }

  protected class BranchTable extends DropDownTable
  {
    public BranchTable(long topHid)
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:107^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mod(th1.descendent,1000000) assoc_num,
//                    substr(gn.group_name,1,40)  branch_name
//            from    app_branch_dropdown abd,
//                    t_hierarchy th1,
//                    t_hierarchy th2,
//                    group_names gn
//            where   abd.app_type = :appType and
//                    th2.ancestor = :topHid and
//                    th2.descendent = abd.hierarchy_node and
//                    abd.hierarchy_node = th1.descendent and
//                    th1.relation = 0 and
//                    th1.ancestor = gn.group_number
//            order by gn.group_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mod(th1.descendent,1000000) assoc_num,\n                  substr(gn.group_name,1,40)  branch_name\n          from    app_branch_dropdown abd,\n                  t_hierarchy th1,\n                  t_hierarchy th2,\n                  group_names gn\n          where   abd.app_type =  :1  and\n                  th2.ancestor =  :2  and\n                  th2.descendent = abd.hierarchy_node and\n                  abd.hierarchy_node = th1.descendent and\n                  th1.relation = 0 and\n                  th1.ancestor = gn.group_number\n          order by gn.group_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.mesp.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   __sJT_st.setLong(2,topHid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.mesp.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:122^9*/

        rs = it.getResultSet();

        addElement("","select one");
        while (rs.next())
        {
          addElement(rs.getString("assoc_num"),rs.getString("branch_name"));
        }
      }
      catch (Exception e)
      {
        logEntry("BranchTable()",e.toString());
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      // make some fields required
      fields.getField("accountType").makeRequired();
      fields.getField("taxpayerId").makeRequired();
      fields.getField("yearsOpen").makeRequired();

      // make some fields not required
      fields.getField("sourceOfInfo").makeOptional();

      // add sic code as visible field
      fields.deleteField("sicCode");
      fields.add(new SicField("sicCode", "SIC Code", false));
      fields.getField("sicCode").makeOptional();

      fields.deleteField("assocNum");
      fields.add(new NumberField("otherAssocNum", "Other", 6, 6, true, 0));

      // add validation of the confirmation fields
      fields.getField("confirmCheckingAccount")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("checkingAccount"),"Checking Account #"));
      fields.getField("confirmTransitRouting")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("transitRouting"),"Transit Routing #"));

      // add referring branch assoc num dropdown
      fields.add(new DropDownField("assocNum",new BranchTable(user.getHierarchyNode()), true));

      // override location and industry type tables for CCB
      fields.add(new DropDownField    ("industryType",      "Industry",new IndustryTypeTable(),false));
      fields.add(new DropDownField    ("locationType",      "Type of Business Location",new LocationTypeTable(),false));

      fields.add(new Field("clientData1", 40, 20, true));

      fields.getField("businessEmail").addValidation(new InternetEmailRequiredValidation(fields.getField("productType")));
      
      fields.getField("otherAssocNum").addValidation(new DropDownAssocValidation(fields.getField("assocNum")));

      createPosPartnerExtendedFields();

      // reset all fields, including those just added to be of class form
      fields.setHtmlExtra("class=\"formText\"");

      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      logEntry("createFields()",e.toString());
    }
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);

    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }
    
    // set some defaults
    fields.setData("statementsProvided", "N");
    fields.setData("haveCanceled", "N");
    fields.setData("owner1Percent", "100");
  }

  protected void preHandleRequest(HttpServletRequest request)
  {
    super.preHandleRequest(request);
    
    // set association number field if 
  }
  
  protected boolean isValidAssociation(String assocNum)
  {
    boolean result = false;
    
    try
    {
      connect();
      
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:234^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(th.descendent)
//          
//          from    app_branch_dropdown abd,
//                  t_hierarchy th
//          where   abd.app_type = :appType and
//                  abd.hierarchy_node = 3941000000 + to_number(:assocNum) and
//                  abd.hierarchy_node = th.descendent and
//                  th.ancestor = :user.getHierarchyNode()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_2977 = user.getHierarchyNode();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(th.descendent)\n         \n        from    app_branch_dropdown abd,\n                t_hierarchy th\n        where   abd.app_type =  :1  and\n                abd.hierarchy_node = 3941000000 + to_number( :2 ) and\n                abd.hierarchy_node = th.descendent and\n                th.ancestor =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.mesp.Business",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appType);
   __sJT_st.setString(2,assocNum);
   __sJT_st.setLong(3,__sJT_2977);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:244^7*/
      
      result = ( recCount > 0 );
    }
    catch(Exception e)
    {
      logEntry("isValidAssociation("+assocNum+")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( result );
  }

  protected boolean loadAppData()
  {
    long                appSeqNum   = 0L;
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = super.loadAppData();

    try
    {
      appSeqNum = fields.getField("appSeqNum").asInteger();

      if(result && appSeqNum != 0)
      {
        connect();

        // load client data
        /*@lineinfo:generated-code*//*@lineinfo:276^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  client_data_1
//            from    merchant
//            where   app_seq_num = :fields.getField("appSeqNum").asInteger()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2978 = fields.getField("appSeqNum").asInteger();
  try {
   String theSqlTS = "select  client_data_1\n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.mesp.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_2978);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.mesp.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:281^9*/

        rs = it.getResultSet();

        setFields(rs);
        
        rs.close();
        it.close();
        
        // check to see if loaded assoc number fits with available list
        String assocNum = getData("assocNum");
        if( assocNum != null && ! assocNum.equals("") )
        {
          if( isValidAssociation(assocNum) )
          {
            setData("otherAssocNum", "");
          }
          else
          {
            setData("assocNum", "");
            setData("otherAssocNum", assocNum);
          }
        }
        
        rs.close();
        it.close();

        result = true;
      }
    }
    catch(Exception e)
    {
      logEntry("loadAppData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return( result );
  }

  protected boolean submitAppData()
  {
    long    appSeqNum = 0L;
    boolean result    = super.submitAppData();

    try
    {
      connect();

      appSeqNum = fields.getField("appSeqNum").asInteger();

      if(result && appSeqNum != 0L)
      {
        // update merchant table with rep code data
        /*@lineinfo:generated-code*//*@lineinfo:339^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     client_data_1 = :getData("clientData1")
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2979 = getData("clientData1");
   String theSqlTS = "update  merchant\n          set     client_data_1 =  :1 \n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.mesp.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2979);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:344^9*/
        
        // if other association data is filled then put that data in the
        // assoc_num column in the merchant table
        if( ! getData("otherAssocNum").equals("") )
        {
          /*@lineinfo:generated-code*//*@lineinfo:350^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//              set     asso_number = :getData("otherAssocNum")
//              where   app_seq_num = :getData("appSeqNum")
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2980 = getData("otherAssocNum");
 String __sJT_2981 = getData("appSeqNum");
   String theSqlTS = "update  merchant\n            set     asso_number =  :1 \n            where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.mesp.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2980);
   __sJT_st.setString(2,__sJT_2981);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:355^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("submitAppData()", e.toString());
      result = false;
    }
    finally
    {
      cleanUp();
    }

    return( result );
  }
  
  public class DropDownAssocValidation
    implements Validation
  {
    private String  errorText = null;
    private Field   assoc     = null;
    
    public DropDownAssocValidation(Field dropDownAssoc)
    {
      assoc = dropDownAssoc;
    }
    
    public String getErrorText()
    {
      return( errorText );
    }
    
    public boolean validate(String fdata)
    {
      boolean result = true;
      
      if(fdata != null && ! fdata.equals(""))
      {
        // dropdown must have empty value
        if(assoc.getData() != null && !assoc.getData().equals(""))
        {
          result = false;
          errorText =  "Only one of Referring Branch and Alt. Association is allowed";
        }
      }
      else
      {
        if(assoc.getData() == null || assoc.getData().equals(""))
        {
          result = false;
          errorText = "Required";
        } 
      }
      
      return( result );
    }
  }
}/*@lineinfo:generated-code*/