/*@lineinfo:filename=Pricing*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/merchcom/Pricing.sqlj $

  Description:

  Pricing

  Banner Bank online app pricing page bean.

  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 12/01/03 4:06p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.merchcom;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class Pricing extends SQLJConnectionBase
{
  
  private static final int    MINIMUM_DISCOUNT      = 20;

  private static final int    MONTHLY_SERVICE_FEE   = 15;
  private static final int    CHARGEBACK_FEE        = 20;
  private static final int    MERCH_ACCT_SETUP_FEE  = 99;

  private static final int    PRICING_TYPE          = 2;
  private static final int    PRICING_PLAN          = mesConstants.APP_PS_FIXED_PLUS_PER_ITEM;
  private static final String DISCOUNT_RATE         = "2.3";
  private static final int    PER_ITEM              = 0;
  private static final String NON_BANK_AUTH_FEE     = ".30";

  private long appSeqNum = 0L;
  
  public Pricing(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
  }

  private void storeTransactionFees()
  {

    try
    {
     
      /*@lineinfo:generated-code*//*@lineinfo:69^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     pricing_grid  = :PRICING_TYPE,
//                  bet_type_code = :PRICING_TYPE
//          where   app_seq_num   = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     pricing_grid  =  :1 ,\n                bet_type_code =  :2 \n        where   app_seq_num   =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.app.merchcom.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,PRICING_TYPE);
   __sJT_st.setInt(2,PRICING_TYPE);
   __sJT_st.setLong(3,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:75^7*/

      // clear tranchrg records
      /*@lineinfo:generated-code*//*@lineinfo:78^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from  tranchrg
//          where app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from  tranchrg\n        where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.merchcom.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:83^7*/

      // store v/mc pricing
      /*@lineinfo:generated-code*//*@lineinfo:86^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.APP_CT_VISA,
//            :PRICING_PLAN,
//            :DISCOUNT_RATE,
//            :PER_ITEM,
//            :MINIMUM_DISCOUNT,
//            0
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.merchcom.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   __sJT_st.setInt(3,PRICING_PLAN);
   __sJT_st.setString(4,DISCOUNT_RATE);
   __sJT_st.setInt(5,PER_ITEM);
   __sJT_st.setInt(6,MINIMUM_DISCOUNT);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:108^7*/

      /*@lineinfo:generated-code*//*@lineinfo:110^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.APP_CT_MC,
//            :PRICING_PLAN,
//            :DISCOUNT_RATE,
//            :PER_ITEM,
//            :MINIMUM_DISCOUNT,
//            0
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.merchcom.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_MC);
   __sJT_st.setInt(3,PRICING_PLAN);
   __sJT_st.setString(4,DISCOUNT_RATE);
   __sJT_st.setInt(5,PER_ITEM);
   __sJT_st.setInt(6,MINIMUM_DISCOUNT);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:132^7*/
      
      ResultSetIterator it = null;
      ResultSet         rs = null;
       
      /*@lineinfo:generated-code*//*@lineinfo:137^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    *
//          from      merchpayoption
//          where     app_seq_num = :appSeqNum and
//                    cardtype_code not in (1,4)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    *\n        from      merchpayoption\n        where     app_seq_num =  :1  and\n                  cardtype_code not in (1,4)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.merchcom.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.app.merchcom.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:143^7*/
      
      rs = it.getResultSet();

      while(rs.next())
      {
        /*@lineinfo:generated-code*//*@lineinfo:149^9*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//            ( app_seq_num,
//              cardtype_code,
//              tranchrg_per_tran 
//            )
//            values
//            ( :appSeqNum,
//              :rs.getInt("cardtype_code"),
//              :NON_BANK_AUTH_FEE
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2974 = rs.getInt("cardtype_code");
   String theSqlTS = "insert into tranchrg\n          ( app_seq_num,\n            cardtype_code,\n            tranchrg_per_tran \n          )\n          values\n          (  :1 ,\n             :2 ,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.app.merchcom.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,__sJT_2974);
   __sJT_st.setString(3,NON_BANK_AUTH_FEE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:161^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("storeTransactionFees()", e.toString());
    }
  }

  /*
  ** private void storeMiscFees()
  **
  ** Stores all miscellaneous fees.
  */
  private void storeMiscFees()
  {
    try
    {

      /*@lineinfo:generated-code*//*@lineinfo:180^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from   miscchrg
//          where  app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from   miscchrg\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.merchcom.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:185^7*/


      //99 dollar merchant account setup fee
      /*@lineinfo:generated-code*//*@lineinfo:189^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          ( app_seq_num,
//            misc_code,
//            misc_chrg_amount 
//          )
//          values
//          ( :appSeqNum,
//            :mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_SETUP,
//            :MERCH_ACCT_SETUP_FEE
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into miscchrg\n        ( app_seq_num,\n          misc_code,\n          misc_chrg_amount \n        )\n        values\n        (  :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.app.merchcom.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_SETUP);
   __sJT_st.setInt(3,MERCH_ACCT_SETUP_FEE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:201^7*/

      // 15 dollar monthly service fee
      /*@lineinfo:generated-code*//*@lineinfo:204^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          ( app_seq_num,
//            misc_code,
//            misc_chrg_amount 
//          )
//          values
//          ( :appSeqNum,
//            :mesConstants.APP_MISC_CHARGE_MONTHLY_SERVICE_FEE,
//            :MONTHLY_SERVICE_FEE 
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into miscchrg\n        ( app_seq_num,\n          misc_code,\n          misc_chrg_amount \n        )\n        values\n        (  :1 ,\n           :2 ,\n           :3  \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.app.merchcom.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_MONTHLY_SERVICE_FEE);
   __sJT_st.setInt(3,MONTHLY_SERVICE_FEE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:216^7*/
   

      //20 dollar chargeback fee
      /*@lineinfo:generated-code*//*@lineinfo:220^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          ( app_seq_num,
//            misc_code,
//            misc_chrg_amount 
//          )
//          values
//          ( :appSeqNum,
//            :mesConstants.APP_MISC_CHARGE_CHARGEBACK,
//            :CHARGEBACK_FEE
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into miscchrg\n        ( app_seq_num,\n          misc_code,\n          misc_chrg_amount \n        )\n        values\n        (  :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.app.merchcom.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_CHARGEBACK);
   __sJT_st.setInt(3,CHARGEBACK_FEE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:232^7*/


    }
    catch(Exception e)
    {
      logEntry("storeMiscFees()",e.toString());
    }
  }



  public boolean submitAppData()
  {
    boolean submitOk = false;
    try
    {
      // don't try to load data if no appSeqNum is set
      if (appSeqNum != 0L)
      {
        connect();
        storeTransactionFees();
        storeMiscFees();
      }
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitAppData()",e.toString());
      System.out.println(this.getClass().getName() + "::submitAppData(): " 
        + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return submitOk;
  }

}/*@lineinfo:generated-code*/