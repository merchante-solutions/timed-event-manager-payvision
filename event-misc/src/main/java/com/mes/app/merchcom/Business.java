/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/merchcom/Business.java $

  Description:
  
  Business
  
  Banner Bank online application merchant information page bean.

  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 12/18/03 11:53a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.merchcom;

import javax.servlet.http.HttpServletRequest;
import com.mes.app.BusinessBase;
import com.mes.constants.mesConstants;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;

public class Business extends BusinessBase
{
  {
    appType     = 35;
    curScreenId = 1;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    Field   tempField            = null;
    
    try
    {
      
      fields.getField(  "mailingName"      ).removeValidation("required");
      fields.getField(  "mailingAddress1"  ).removeValidation("required");
      fields.getField(  "mailingCsz"       ).removeValidation("required");
      //fields.getField(  "mailingCszCity"      ).removeValidation("required");
      //fields.getField(  "mailingCszState"     ).removeValidation("required");
      //fields.getField(  "mailingCszZip"       ).removeValidation("required");
      
      // fix bank fields
      //fields.add(new Field("confirmCheckingAccount",  "Confirm Checking Acct. #",17,15,false));
      fields.getField("confirmCheckingAccount")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("checkingAccount"),"Checking Account #"));
      
      //fields.add(new Field("confirmTransitRouting",  "Confirm Transit Routing. #",9,15,false));
      fields.getField("confirmTransitRouting")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("transitRouting"),"Transit Routing #"));

      tempField = new HiddenField("accountType");
      tempField.setData("N"); //set to new account for every account.. no conversions
      fields.add(tempField);

      tempField = new HiddenField("applicationType");
      tempField.setData("1"); //set to single outlet
      fields.add(tempField);

      tempField = new HiddenField("numLocations");
      tempField.setData("1"); //set to 1 location
      fields.add(tempField);

      tempField = new HiddenField("locationType");
      tempField.setData("2"); //set to internet store front
      fields.add(tempField);

      tempField = new HiddenField("motoPercentage");
      tempField.setData("100"); //set to internet store front
      fields.add(tempField);

      tempField = new HiddenField("internetPercent");
      tempField.setData("100"); //set to internet store front
      fields.add(tempField);

      tempField = new HiddenField("cardPercent");
      tempField.setData("0"); 
      fields.add(tempField);

      tempField = new HiddenField("noCardPercent");
      tempField.setData("100"); 
      fields.add(tempField);

      tempField = new HiddenField("productType");
      tempField.setData(Integer.toString(mesConstants.POS_INTERNET)); //set to product type to internet
      fields.add(tempField);

      tempField = new HiddenField("internetType");
      tempField.setData(Integer.toString(mesConstants.APP_MPOS_AUTHORIZE_NET)); //set to internet type to authorize.net
      fields.add(tempField);
     
      fields.add(new Field("webUrl",25,30,false));

      fields.getField("businessEmail").makeRequired();

      // reset all fields, including those just added to be of class form
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }

  protected boolean submitAppData()
  {
    boolean submitOk = false;

    submitOk = super.submitAppData();

    long appSeqNum = fields.getField("appSeqNum").asInteger();
    Pricing merchPricing = new Pricing(appSeqNum);

    submitOk = merchPricing.submitAppData();
  
    return submitOk;
  }
  


  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
    
    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }
  }
}










                              