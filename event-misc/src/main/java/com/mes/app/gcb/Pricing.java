/*@lineinfo:filename=Pricing*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: $

  Description:

  Pricing

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.gcb;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.FeeSet.EquipFee;
import com.mes.app.PricingBase;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import com.mes.ops.AutoUploadChargeRecords;
import sqlj.runtime.ResultSetIterator;

public class Pricing extends PricingBase
{
  {
    appType = 72;
    curScreenId = 3;
  }
  
  public static final int  MAX_NUM_UNIQUE_FEES       = 2;
  /*************************************************************************
  **
  **   Drop Down Tables
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Radio Button Lists
  **
  **************************************************************************/
  protected static final String[][] uniqueFeeTypeList =
  {
    { "One-Time",   Integer.toString(AutoUploadChargeRecords.CHARGE_TYPE_SINGLE)  },
    { "Recurring",  Integer.toString(AutoUploadChargeRecords.CHARGE_TYPE_MONTHLY) }
  };
  
  /*************************************************************************
  **
  **   Validations
  **
  **************************************************************************/
  protected class UniqueFeeValidation implements Validation
  {
    private Field descField;
    private String errorText;
    
    public UniqueFeeValidation(Field descField)
    {
      this.descField = descField;
    }
    
    public boolean validate(String fdata)
    {
      double feeAmount = 0;
      boolean isValid = true;
      
      try
      {
        feeAmount = Double.parseDouble(fdata);
      }
      catch (Exception e) {}
      
      if (feeAmount != 0)
      {
        if (descField.isBlank())
        {
          isValid = false;
          errorText = "Description of unique fee required";
        }
      }
      else if (!descField.isBlank())
      {
        isValid = false;
        errorText = "Provide an amount or clear the unique fee description";
      }
      
      return isValid;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }
  
  /*************************************************************************
  **
  **   Custom Fields
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Field Bean
  **
  **************************************************************************/
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      connect();
      
      // initialize the wireless equipment object
      wirelessSet = new WirelessEquipmentSet(appSeqNum);
      add(wirelessSet);
      
      // v/mc pricing options
      fields.setData("pricingPlan","1");
      
      // setup bet options
      fields.setData("betType","72");
      
      Field betSet72 = new DropDownField("betSet_72",new BetSet(72),false);
      fields.add(betSet72);
      fields.add(new BetSetMapperField());
      betSet72.setOptionalCondition(
        new FieldValueCondition(fields.getField("betType"),"72"));
        
      // set site inspection fields to be required
      fields.getField("locationType").makeRequired();
      fields.getField("locationAddrType").makeRequired();
      fields.getField("locationCsz").makeRequired();
      fields.getField("employeeCount").makeRequired();

      // make site inspection questions required      
      for(int i=0; i < questionList.length; ++i)
      {
        fields.getField(questionList[i][Q_FIELD_NAME]).makeRequired();
      }
      
      // make sure any equipment purchase or rental fees are required
      EquipFee[] equipFees = feeSet.getEquipFees();
      
      for(int i=0; i < equipFees.length; ++i)
      {
        EquipFee fee = (EquipFee)(equipFees[i]);
        
        // generate field name from equipFee data
        String equipFieldName = "equipFee_" + fee.getModel() + "_" + fee.getLendDesc();
        
        // make any owned equipment support fee optional
        if(!fee.getModel().equals("OWNEDEQUIP"))
        {
          fields.getField(equipFieldName).makeRequired();
        }
      }
      
      // add unique fees
      FieldGroup gOddFees = new FieldGroup  ("gOddFees");
      for(int i=0; i<MAX_NUM_UNIQUE_FEES; ++i)
      {
        gOddFees.add(new RadioButtonField ("uniqueFeeType_"+Integer.toString(i), uniqueFeeTypeList,-1,true,""));
        gOddFees.add(new CurrencyField    ("uniqueFee_"+Integer.toString(i),     8,6,true));
        gOddFees.add(new Field            ("uniqueFeeDesc_"+Integer.toString(i), 30,20,true));
        
        gOddFees.getField("uniqueFee_"+Integer.toString(i)).addValidation(
          new UniqueFeeValidation(gOddFees.getField("uniqueFeeDesc_"+Integer.toString(i))));
      }
      
      fields.add(gOddFees);
        
      // set the field style class
      fields.setHtmlExtra("class=\"formText\"");

      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      e.printStackTrace();
      logEntry("createFields()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*************************************************************************
  **
  **   Load
  **
  **************************************************************************/
  protected void loadOddFees(long appSeqNum)
  {
    ResultSetIterator it    = null;
    ResultSet         rs    = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:236^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  charge_rec_type unique_fee_type,
//                  amount          unique_fee,
//                  description     unique_fee_desc
//          from    appo_charge_recs
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  charge_rec_type unique_fee_type,\n                amount          unique_fee,\n                description     unique_fee_desc\n        from    appo_charge_recs\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.gcb.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.gcb.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:243^7*/
      
      rs = it.getResultSet();
      
      int idx = 0;
      while(rs.next())
      {
        fields.setData("uniqueFeeType_"+Integer.toString(idx),  rs.getString("unique_fee_type"));
        fields.setData("uniqueFee_"+Integer.toString(idx),      rs.getString("unique_fee"));
        fields.setData("uniqueFeeDesc_"+Integer.toString(idx),  rs.getString("unique_fee_desc"));
        
        ++idx;
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadOddFees(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  protected boolean loadAppData()
  {
    long                appSeqNum = 0L;
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    boolean             result    = super.loadAppData();
    
    try
    {
      connect();
      
      appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if(result && appSeqNum != 0)
      {
        loadOddFees(appSeqNum);
      }
    }
    catch(Exception e)
    {
      System.out.println("com.mes.app.mes.Pricing.loadAppData(): " + e.toString());
      logEntry("loadAppData(" + appSeqNum + ")", e.toString());
      result = false;
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }

  /*************************************************************************
  **
  **   Submit
  **
  **************************************************************************/
  protected void submitOddFees(long appSeqNum)
  {
    try
    {
      connect();
      
      // remove existing odd fees
      /*@lineinfo:generated-code*//*@lineinfo:318^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    appo_charge_recs
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    appo_charge_recs\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.gcb.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:323^7*/
      
      // submit new fees if they exist
      for(int i=0; i<MAX_NUM_UNIQUE_FEES; ++i)
      {
        Field uniqueFee = fields.getField("uniqueFee_"+Integer.toString(i));
        
        if(uniqueFee != null && !uniqueFee.getData().equals(""))
        {
          /*@lineinfo:generated-code*//*@lineinfo:332^11*/

//  ************************************************************
//  #sql [Ctx] { insert into appo_charge_recs
//              (
//                app_seq_num,
//                charge_rec_num,
//                charge_rec_type,
//                amount,
//                description
//              )
//              values
//              (
//                :appSeqNum,
//                :i+1,
//                :fields.getData("uniqueFeeType_"+Integer.toString(i)),
//                :fields.getData("uniqueFee_"+Integer.toString(i)),
//                :fields.getData("uniqueFeeDesc_"+Integer.toString(i))
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2945 = i+1;
 String __sJT_2946 = fields.getData("uniqueFeeType_"+Integer.toString(i));
 String __sJT_2947 = fields.getData("uniqueFee_"+Integer.toString(i));
 String __sJT_2948 = fields.getData("uniqueFeeDesc_"+Integer.toString(i));
   String theSqlTS = "insert into appo_charge_recs\n            (\n              app_seq_num,\n              charge_rec_num,\n              charge_rec_type,\n              amount,\n              description\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.gcb.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,__sJT_2945);
   __sJT_st.setString(3,__sJT_2946);
   __sJT_st.setString(4,__sJT_2947);
   __sJT_st.setString(5,__sJT_2948);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:350^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("submitOddFees(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  protected boolean submitAppData()
  {
    long    appSeqNum = 0L;
    boolean result    = super.submitAppData();
    
    try
    {
      connect();
      
      appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if(result && appSeqNum != 0L)
      {
        // store daily discount data (make sure it is 'n')
        /*@lineinfo:generated-code*//*@lineinfo:378^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_dly_discount_flag = 'N'
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n          set     merch_dly_discount_flag = 'N'\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.gcb.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:383^9*/
        
        // store odd fees
        submitOddFees(appSeqNum);
      }
    }
    catch(Exception e)
    {
      System.out.println("com.mes.app.mes.submitAppData(): " + e.toString());
      logEntry("submitAppData(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return result;
  }
  
  public boolean allowedToSubmit()
  {
    // for Exchange Bank allow to submit the very first time
    return true;
  }
  
}/*@lineinfo:generated-code*/