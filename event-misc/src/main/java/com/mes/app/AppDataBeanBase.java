/*@lineinfo:filename=AppDataBeanBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/AppDataBeanBase.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 4/08/04 2:49p $
  Version            : $Revision: 11 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app;

import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.constants.MesUsers;
import com.mes.constants.mesConstants;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.support.HttpHelper;
import com.mes.tools.AppNotifyBean;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public abstract class AppDataBeanBase extends FieldBean
{
  // constants
  public static final int       APP_SEQ_INVALID   = -1;
  public static final int       APP_SEQ_NEW       = 0;
  
  public static final int       APP_NOT_FINISHED  = 0;
  public static final int       APP_USER          = 1;    // must be last
  
  protected class YesNoTable extends DropDownTable
  {
    public YesNoTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("Y","Yes");
      addElement("N","No");
    }
  }
  
  private class PageData
  {
    public String       BeanName          = null;
    public String       Description       = null;
    public int          PageNumber        = 0;
    public String       JspName           = null;
    
    public PageData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      BeanName    = resultSet.getString("bean_name");
      Description = resultSet.getString("description");
      PageNumber  = resultSet.getInt("page_number");
      JspName     = resultSet.getString("jsp");
    }
    
    // screen id is a bit field based on the
    // order of the page in the screen_sequence
    // table.
    public long getScreenIdBit( )
    {
      long      retVal      = 0L;
      
      // if the page number is 0 (or negative?)
      // then just return a bit weight of 0
      if ( PageNumber > 0 )
      {
        retVal = (1 << (PageNumber-1));
      }
      return( retVal );
    }
  }

  protected int                   AppBankNumber     = 0;
  protected String                AppSourceType     = null;
  protected long                  AppSeqNum         = APP_SEQ_INVALID;
  protected long                  AppMerchNum       = -1L;
  protected int                   AppSeqId          = -1;
  protected int                   AppType           = -1;
  protected UserBean              AppUserBean       = null;
  protected String                CalcTargetName    = null;
  protected String                ErrorHeader       = "Please correct the following errors:";
//@  protected long                  ControlNumber     = 0L; 
  protected long                  MerchantId        = 0L;
  protected int                   PageIndex         = 0;
  protected Vector                Pages             = new Vector();
  protected Vector                Warnings          = new Vector();
  
  /*
  ** CONSTRUCTOR
  */  
  public AppDataBeanBase()
  {
  }
  
  public void addWarning( String warningId )
  {
    Warnings.addElement( warningId );
  }
  
  /*
  ** FUNCTION canShowSubmit
  **
  ** returns true if the user should see a "submit" button on the bottom of
  ** the application form.  This function should return true if at least one
  ** of the following are true:
  **
  ** 1. the application is not complete (merchant.merch_credit_status)
  ** 2. the user has "EDIT_APPLICATION" rights and the account has not activated
  */
  public boolean canShowSubmit( )
  {
    int               creditStatus  = 0;
    boolean           result        = false;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:151^7*/

//  ************************************************************
//  #sql [Ctx] { select  merch_credit_status 
//          from    merchant
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_credit_status  \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.AppDataBeanBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   creditStatus = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:156^7*/
      
      if( (creditStatus == APP_NOT_FINISHED) || 
          AppUserBean.hasRight(MesUsers.RIGHT_APPLICATION_EDIT) )
      {
        result = true;
      }
    }
    catch(Exception e)
    {
      result = true;
    }
    finally
    {
    }
    return(result);
  }
  
  public int countDigits(String raw)
  {
    int result = 0;
    
    for(int i=0; i<raw.length(); ++i)
    {
      if(Character.isDigit(raw.charAt(i)))
      {
        ++result;
      }
    }
    return(result);
  }
  
  public void encodeAppUrl( StringBuffer buffer )
  {
    encodeAppUrl( buffer, AppSeqNum );
  }
  
  public void encodeAppUrl( StringBuffer buffer, long appSeqNum )
  {
    buffer.append( ( ( buffer.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
    buffer.append("primaryKey=");
    buffer.append(appSeqNum);
    encodeWarningsUrl(buffer);
  }
  
  public void encodeWarningsUrl( StringBuffer buffer )
  {
    if ( !Warnings.isEmpty() )
    {
      buffer.append( ( ( buffer.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
      buffer.append("appWarnings=");
      buffer.append( getWarningsString() );
    }
  }    
  
  public String getAppUrl( String pageName )
  {
    return( getAppUrl( pageName, AppSeqNum ) );
  }
  
  public String getAppUrl( String pageName, long appSeqNum )
  {
    StringBuffer        buffer = new StringBuffer( pageName );

    // encode the buffer with all the params for
    // the current org id.
    encodeAppUrl( buffer, appSeqNum );
    
    return( buffer.toString() );
  }
  
  public final String getCalcTargetName( )
  {
    return( CalcTargetName );
  }
  
  public long getAppSeqNum( )
  {
    return( AppSeqNum );
  }
  
  public long getMerchNum()
  {
    return getMerchNum(AppSeqNum);
  }
  
  public long getMerchNum(long appSeqNum)
  {
    if (AppMerchNum == -1L && appSeqNum != APP_SEQ_INVALID)
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:248^9*/

//  ************************************************************
//  #sql [Ctx] { select  merch_number
//            
//            from    merchant
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_number\n           \n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.AppDataBeanBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   AppMerchNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:254^9*/
      }
      catch (Exception e)
      {
      }
    }
    return AppMerchNum;
  }
  
  public long getControlNumber( )
  {
    return( getControlNumber(AppSeqNum) );
  }
  
  public long getControlNumber( long appSeqNum )
  {
    long              result  = 0L;
    
    try
    {
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:276^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merc_cntrl_number)
//          
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merc_cntrl_number)\n         \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.AppDataBeanBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:282^7*/
      
      if(recCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:286^9*/

//  ************************************************************
//  #sql [Ctx] { select  merc_cntrl_number 
//            from    merchant
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merc_cntrl_number  \n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.AppDataBeanBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:291^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("getControlNumber(" + appSeqNum + ")", e.toString());
    }
    finally
    {
    }
    
    return( result );
  }
  
  public long getDigits(String raw)
  {
    long          result      = 0L;
    StringBuffer  digits      = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }
      
      result = Long.parseLong(digits.toString());
    }
    catch(Exception e)
    {
      if(raw != null && !raw.equals(""))
      {
        logEntry( "getDigits('" + raw + "'): ", e.toString());
      }
    }
    return(result);
  }
  
  public String getNextPage()
  {
    return( ((PageData)Pages.elementAt( PageIndex+1 )).JspName );
  }
  
  public String getPageDescription()
  {
    return( ((PageData)Pages.elementAt( PageIndex )).Description );
  }
  
  public String getPrevPage()
  {
    return( ((PageData)Pages.elementAt( PageIndex-1 )).JspName );
  }
  
  public String getWarningsString( )
  {
    StringBuffer        retVal      = new StringBuffer();
    
    for( int i = 0; i < Warnings.size(); ++i )
    {
      retVal.append( (String)Warnings.elementAt(i) );
      retVal.append( "#" );
    }
    return( retVal.toString() );
  }

  public boolean hasNextPage( )
  {
    return( PageIndex < Pages.size() );
  }
  
  public boolean hasPrevPage( )
  {
    return( PageIndex > 0 );
  }
  
  public boolean hasWarning( String warningId )
  {
    boolean     retVal      = false;
    
    // look for this warning in the current warnings list
    for( int i = 0; i < Warnings.size(); ++i )
    {
      if ( ((String)Warnings.elementAt(i)).equals(warningId) )
      {
        retVal = true;
        break;
      }
    }
    return( retVal );
  }
  
  public void init()
  {
    fields.removeAllFields();
  }
  
  public static boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return( pass );
  }
  
  /*
  ** public boolean isNewApp()
  **
  ** Uses xml_applications to determine if this is a new app.  A new app is
  ** actually an app that has not been completed.  If the app has not been
  ** added to xml_applications it is considered incomplete.
  **
  ** TRUE: if app is new, else false.
  */
  private boolean newAppChecked = false;
  public boolean isNewApp()
  {
    boolean isIncomplete = true;
    try
    {    
      if (!newAppChecked && AppSeqNum != APP_SEQ_NEW)
      {
        int appCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:420^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//            
//            from    xml_applications
//            where   app_seq_num = :AppSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n           \n          from    xml_applications\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.AppDataBeanBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:426^9*/
        isIncomplete = (appCount == 0);
        newAppChecked = true;
      }
    }
    catch (Exception e)
    {
      logEntry("isNewApp()",e.toString());
    }
    return isIncomplete;
  }
  
  public boolean isPageComplete( )
  {
    PageData      page        = (PageData)Pages.elementAt(PageIndex);
    boolean       retVal      = false;
    long          val         = 0L;
    
    try
    {
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:448^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(sp.screen_complete)
//          
//          from    screen_progress sp
//          where   sp.screen_sequence_id = :AppSeqId and
//                  sp.screen_pk = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(sp.screen_complete)\n         \n        from    screen_progress sp\n        where   sp.screen_sequence_id =  :1  and\n                sp.screen_pk =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.AppDataBeanBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,AppSeqId);
   __sJT_st.setLong(2,AppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:455^7*/
      
      if(recCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:459^9*/

//  ************************************************************
//  #sql [Ctx] { select  sp.screen_complete 
//            from    screen_progress sp
//            where   sp.screen_sequence_id = :AppSeqId and
//                    sp.screen_pk          = :AppSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sp.screen_complete  \n          from    screen_progress sp\n          where   sp.screen_sequence_id =  :1  and\n                  sp.screen_pk          =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.AppDataBeanBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,AppSeqId);
   __sJT_st.setLong(2,AppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   val = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:465^9*/
      
        retVal = ( ( val & page.getScreenIdBit() ) != 0 );
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("isPageComplete()",e.toString());
    }
    return( retVal );
  }
  
  public void markPageComplete()
  {
    PageData      page  = (PageData)Pages.elementAt(PageIndex);
    long          val   = 0L;
    
    try
    {
      // this assumes that there is always an entry in screen
      // progress for a valid AppSeqNum/AppSeqId combination.
      // a row is automatically inserted into the screen_progress
      // table during the new app initialization 
      // [see AppDataBeanBase.newAppInit()]
      /*@lineinfo:generated-code*//*@lineinfo:489^7*/

//  ************************************************************
//  #sql [Ctx] { select  sp.screen_complete 
//          from    screen_progress     sp
//          where   sp.screen_sequence_id = :AppSeqId and
//                  sp.screen_pk = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sp.screen_complete  \n        from    screen_progress     sp\n        where   sp.screen_sequence_id =  :1  and\n                sp.screen_pk =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.AppDataBeanBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,AppSeqId);
   __sJT_st.setLong(2,AppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   val = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:495^7*/
      
      // Bit OR in the current screens value
      val |= page.getScreenIdBit();
      
      /*@lineinfo:generated-code*//*@lineinfo:500^7*/

//  ************************************************************
//  #sql [Ctx] { update  screen_progress
//          set     screen_complete = :val
//          where   screen_sequence_id = :AppSeqId and
//                  screen_pk = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  screen_progress\n        set     screen_complete =  :1 \n        where   screen_sequence_id =  :2  and\n                screen_pk =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.app.AppDataBeanBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,val);
   __sJT_st.setInt(2,AppSeqId);
   __sJT_st.setLong(3,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:506^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:508^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:511^7*/
    }
    catch( Exception e )
    {
      logEntry("markPageComplete()",e.toString());
    }
  }
  
  protected boolean newAppInit( )
  {
    long                controlNum      =     0L;
    long                controlSeq      =     10000000000L;
    AppNotifyBean       notifyBean      =     null;
    boolean             retVal          =     false;
    
    try
    {
      // use AppSeqNumGenerator to create app seq num and control number
      AppSeqNumGenerator  numGen = new AppSeqNumGenerator(Ctx);
      AppSeqNum   = numGen.getAppSeqNum();
      controlNum  = numGen.getControlNumber();
      
      // insert an entry in the application table and
      // notify the necessary users
      /*@lineinfo:generated-code*//*@lineinfo:535^7*/

//  ************************************************************
//  #sql [Ctx] { insert into application 
//          (      
//            app_seq_num,                   
//            appsrctype_code,               
//            app_created_date,              
//            app_scrn_code,                 
//            app_user_id,                   
//            app_user_login,                
//            app_type,                      
//            screen_sequence_id
//          )            
//          values 
//          (
//            :AppSeqNum,
//            :AppSourceType,
//            sysdate,
//            null,
//            :AppUserBean.getUserId(),
//            :AppUserBean.getLoginName(),
//            :AppType,
//            :AppSeqId
//          ) 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1938 = AppUserBean.getUserId();
 String __sJT_1939 = AppUserBean.getLoginName();
   String theSqlTS = "insert into application \n        (      \n          app_seq_num,                   \n          appsrctype_code,               \n          app_created_date,              \n          app_scrn_code,                 \n          app_user_id,                   \n          app_user_login,                \n          app_type,                      \n          screen_sequence_id\n        )            \n        values \n        (\n           :1 ,\n           :2 ,\n          sysdate,\n          null,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.app.AppDataBeanBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,AppSourceType);
   __sJT_st.setLong(3,__sJT_1938);
   __sJT_st.setString(4,__sJT_1939);
   __sJT_st.setInt(5,AppType);
   __sJT_st.setInt(6,AppSeqId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:559^7*/
      
      // create the base entry in the merchant table
      /*@lineinfo:generated-code*//*@lineinfo:562^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant
//          ( 
//            app_seq_num, 
//            merc_cntrl_number, 
//            merch_bank_number
//          )
//          values
//          (
//            :AppSeqNum,
//            :controlNum,
//            :AppBankNumber
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchant\n        ( \n          app_seq_num, \n          merc_cntrl_number, \n          merch_bank_number\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.app.AppDataBeanBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setLong(2,controlNum);
   __sJT_st.setInt(3,AppBankNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:576^7*/
      
      // make an entry in screen progress with no bits set.
      /*@lineinfo:generated-code*//*@lineinfo:579^7*/

//  ************************************************************
//  #sql [Ctx] { insert into screen_progress
//          (
//            screen_sequence_id,
//            screen_pk,
//            screen_complete
//          )
//          values
//          (
//            :AppSeqId,
//            :AppSeqNum,
//            0
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into screen_progress\n        (\n          screen_sequence_id,\n          screen_pk,\n          screen_complete\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n          0\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.app.AppDataBeanBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,AppSeqId);
   __sJT_st.setLong(2,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:593^7*/
      
      // post notification to required users
      notifyBean = new AppNotifyBean();
      notifyBean.notifyStatus(AppSeqNum, mesConstants.APP_STATUS_INCOMPLETE, AppType);
      
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("newAppInit()", e.toString());
    }
    finally
    {
    }
    return( retVal );
  }
  
  public void recalculate( )
  {
  }
  
  public void removeWarning( String warningId )
  {
    
    for( int i = 0; i < Warnings.size(); ++i )
    {
      if ( ((String)Warnings.elementAt(i)).equals(warningId) )
      {
        Warnings.removeElementAt(i);
      }
    }
  }
  
  public String renderErrors( )
  {
    Vector              allFields   = fields.getFieldsVector();
    int                 errorCount  = 0;
    StringBuffer        errorOutput = new StringBuffer();
    Field               f           = null;
    
    for( int i = 0; i < allFields.size(); ++i )
    {
      f = (Field)allFields.elementAt(i);
      
      if ( f.getHasError() == true )
      {
        if ( errorCount == 0 )
        {
          // add the html header
          errorOutput.append("<div align=\"center\">\n");
          errorOutput.append("<table width=\"98%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" );
          errorOutput.append("<tr valign=\"top\">\n");
          errorOutput.append("  <td valign=\"top\">\n");
    
          // allow specific apps to turn off the error header
          if ( ErrorHeader != null )
          {
            errorOutput.append("    <u><b><font color=\"red\">");
            errorOutput.append( ErrorHeader );
            errorOutput.append("</font></b></u>\n");
            errorOutput.append("    <br>\n");
          }
        }
        
        errorOutput.append("<font color=\"red\">");
        errorOutput.append("  <i>");
        errorOutput.append(f.getErrorText());
        errorOutput.append("  </i>");
        errorOutput.append("  <br>");
        errorOutput.append("</font>");
        ++errorCount;
      }        
    }
    
    if ( errorCount > 0 )
    {
      // add the html footer tags
      errorOutput.append("  </td>\n");
      errorOutput.append("</tr>\n");
      errorOutput.append("</table>\n");
      errorOutput.append("</div>\n");
    }      
    return( errorOutput.toString() );
  }
  
  /***************************************************************************
  * Overridable methods
  ***************************************************************************/
  /*
  ** FUNCTION loadData
  **
  **  Loads the data for this application from the database.
  **  Must be overloaded by derived classes.
  */
  abstract public void loadData();
  
  /*
  ** protected void screenSequenceInit()
  **
  ** Loads the screen sequence page data based on the current AppType.
  **
  ** This may be called once the AppType has been set.  (Does not derive
  ** AppType from AppSeqNum).
  */
  protected void screenSequenceInit()
  {
    ResultSetIterator     it            = null;
    ResultSet             resultSet     = null;
    
    try
    {
      // load the bank number, sequence id etc for this app type
      /*@lineinfo:generated-code*//*@lineinfo:706^7*/

//  ************************************************************
//  #sql [Ctx] { select  appsrctype_code,
//                  screen_sequence_id,
//                  app_bank_number 
//                  
//          
//                  
//          from    org_app
//          
//          where   app_type = :AppType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  appsrctype_code,\n                screen_sequence_id,\n                app_bank_number \n                \n         \n                \n        from    org_app\n        \n        where   app_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.app.AppDataBeanBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,AppType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   AppSourceType = (String)__sJT_rs.getString(1);
   AppSeqId = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   AppBankNumber = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:719^7*/
      
      // load the screen sequence data for this app sequence
      /*@lineinfo:generated-code*//*@lineinfo:722^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    screen_classname   as bean_name,
//                    screen_description as description,
//                    screen_order       as page_number,
//                    screen_jsp         as jsp
//                    
//          from      screen_sequence
//          
//          where     screen_sequence_id = :AppSeqId
//          
//          order by  screen_order
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    screen_classname   as bean_name,\n                  screen_description as description,\n                  screen_order       as page_number,\n                  screen_jsp         as jsp\n                  \n        from      screen_sequence\n        \n        where     screen_sequence_id =  :1 \n        \n        order by  screen_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.app.AppDataBeanBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,AppSeqId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.app.AppDataBeanBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:734^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        Pages.add( new PageData(resultSet) );
        
        // if the class name in the current row is the same as the 
        // class name of this object, then set the page number
        if ( this.getClass().getName().equals(resultSet.getString("bean_name")) )
        {
          PageIndex = (Pages.size() - 1);
        }
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      // should never happen
      logEntry("screenSequenceInit()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
    
  /*
  ** protected void screenSequenceInit(long appSeqNum)
  **
  ** Loads AppType (and AppSeqNum) from the appSeqNum given and then calls 
  ** the main screen sequence init method.
  */
  protected void screenSequenceInit(long appSeqNum)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:772^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_type, app_seq_num
//          
//          from    application
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type, app_seq_num\n         \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.app.AppDataBeanBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   AppType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   AppSeqNum = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:778^7*/
      
      screenSequenceInit();
    }
    catch (Exception e)
    {
      logEntry("screenSequenceInit(" + appSeqNum + ")",e.toString());
    }
  }

  public void setAppUserBean( UserBean aub )
  {
    AppUserBean = aub;
  }
  
  /*
  ** FUNCTION setDefaults
  **
  **  This should be overloaded by derived classes if necessary
  */
  public void setDefaults()
  {
  }
  
  public void setErrorHeader( String newHeader )
  {
    ErrorHeader = newHeader;
  }
  
  // 
  //  setProperties
  //
  //  This method is used to load class members only.  Form
  //  field data is loaded through the setFields(request) 
  //  method.  Derived classes that overload this method
  //  MUST call this instance through the super reference.
  //  
  public void setProperties( HttpServletRequest request )
  { 
    long        controlNum    = 0L;
    Field       field         = null;
    String      temp          = null;
    int         userAppType   = AppUserBean.getAppType();
    
    if ( (AppSeqNum = HttpHelper.getLong(request,"primaryKey",APP_SEQ_NEW)) == APP_SEQ_NEW )
    {
      if ( (controlNum = HttpHelper.getLong(request,"controlNum",0L)) != 0L )
      {
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:828^11*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(app_seq_num,:APP_SEQ_NEW) 
//              from    merchant
//              where   merc_cntrl_number = :controlNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(app_seq_num, :1 )  \n            from    merchant\n            where   merc_cntrl_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.app.AppDataBeanBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,APP_SEQ_NEW);
   __sJT_st.setLong(2,controlNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   AppSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:833^11*/
        }
        catch( java.sql.SQLException e )
        {
          AppSeqNum = APP_SEQ_NEW;
        }
      }
    }
    
    // check to see if a recalculate button has been pressed
    if ( HttpHelper.getString(request,"recalculate",null) != null )
    {
      CalcTargetName = "calc";
    }
    else  // button not pressed, no target name
    {
      CalcTargetName = null;
    }
    
    // if an app seq num is specified, try to load sequence 
    // info using the app seq num
    if( AppSeqNum != APP_SEQ_NEW )
    {
      int       itemCount  = 0;
      
      try
      {
        // if there is a request to load a particular app,
        // make sure that the user has rights to view the
        // app in question.  
        /*@lineinfo:generated-code*//*@lineinfo:863^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//            from    application
//            where   app_seq_num = :AppSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)  \n          from    application\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.app.AppDataBeanBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:868^9*/
      }
      catch( java.sql.SQLException e )    // should never happen
      {
        itemCount = 0;
      }
      
      // did not find any for the specified 
      // app sequence number, start a new app
      if ( itemCount == 0 )
      {
        AppSeqNum = APP_SEQ_NEW;
      }
      else
      {
        // load the screen sequence info using the app seq num
        screenSequenceInit(AppSeqNum);
      }
    }      
    
    // new app, default the app type, load the sequence info
    // from the app type
    if (AppSeqNum == APP_SEQ_NEW)
    {
      AppType   = HttpHelper.getInt(request,"appType",userAppType);
      if ( ( AppType != userAppType ) &&
           ( !AppUserBean.hasRight( MesUsers.RIGHT_ACCOUNT_SERVICING ) ) )
      {   
        // if the user does not have account servicing rights and
        // they are trying to view an app of a type other than their
        // own type, then deny them access to the application by
        // forcing the app sequence number to NEW.   
        AppType = AppUserBean.getAppType();
      }

      // call screenSequenceInit to load the screen sequence along with
      // appsrctype_code and sequence_type from org_app
      screenSequenceInit();
    }
    
    // setup the warnings passed from one instance of
    // the page to the next.
    temp = HttpHelper.getString(request,"appWarnings",null);
    if( temp != null )
    {
      setWarnings(temp);
    }
  } 
  
  public void setWarnings( String warningString )
  {
    int             idx           = 0;
    int             nextIdx       = warningString.indexOf("#");
    String          temp          = null;

    // clear the existing warnings
    Warnings.removeAllElements();
    
    // scan through the html param 
    // and extract the questionable data 
    // elements
    while( nextIdx != -1 )
    {
      Warnings.addElement( warningString.substring(idx, nextIdx) );
      idx     = (nextIdx + 1);
      nextIdx = warningString.indexOf("#",idx);
    }
  }
  
  /*
  ** FUNCTION showData
  **
  **  Objects can overload this method to dump their
  **  contents to the passed in PrintStream
  */
  public void showData( java.io.PrintStream out )
  {
  }
  
  /*
  ** FUNCTION showData
  **
  **  Objects can overload this method to dump their
  **  contents to the response HTTP stream
  */
  public void showData( HttpServletResponse response )
  {
  }
  
  public boolean showNextIcon( boolean ignoreComplete )
  {
    boolean     retVal  = false;
    
    if ( hasNextPage() && ( ignoreComplete || isPageComplete() ) )
    {
      retVal = true;
    }
    return( retVal );
  }    
  
  public boolean showPrevIcon( )
  { 
    // always let them go back if there is
    // somewhere to go.  if necessary overload
    // to add additional criteria
    return( hasPrevPage() );
  }    
  
  /*
  ** FUNCTION storeData
  **
  **  Stores the data from the current bean into the database.
  **  Must be overloaded by derived classes.
  */
  abstract public void storeData();
  
  /*
  ** FUNCTION storeDataNoComplete
  **
  **  Stores data in the database but does not mark the page as complete
  */
  public void storeDataNoComplete()
  {
  }
  
  protected void storeWarnings( )
  {
    int             warningCount    = 0;
    String          warningId       = null;
    
    try
    {
      for( int i = 0; i < Warnings.size(); ++i )
      {
        warningId = (String)Warnings.elementAt(i);
        /*@lineinfo:generated-code*//*@lineinfo:1003^9*/

//  ************************************************************
//  #sql [Ctx] { select  count( app_seq_num ) 
//            from    merchant_app_errors
//            where   app_seq_num = :AppSeqNum and
//                    error_code = :warningId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( app_seq_num )  \n          from    merchant_app_errors\n          where   app_seq_num =  :1  and\n                  error_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.app.AppDataBeanBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,warningId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   warningCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1009^9*/
        
        if ( warningCount == 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:1013^11*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant_app_errors 
//                ( app_seq_num, error_date, error_code )
//              values 
//                ( :AppSeqNum, sysdate, :warningId )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchant_app_errors \n              ( app_seq_num, error_date, error_code )\n            values \n              (  :1 , sysdate,  :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.app.AppDataBeanBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,warningId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1019^11*/
          /*@lineinfo:generated-code*//*@lineinfo:1020^11*/

//  ************************************************************
//  #sql [Ctx] { commit
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1023^11*/
        }
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "storeWarnings()",e.toString() );
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/