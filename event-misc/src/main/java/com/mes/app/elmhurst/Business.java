/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/elmhurst/Business.sqlj $

  Description:
  
  Business
  
  Elmhurst online application merchant information page bean.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 10/23/03 3:30p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.elmhurst;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.MerchInfoDataBean;
import com.mes.forms.CheckboxField;
import com.mes.forms.Condition;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Business extends MerchInfoDataBean
{
  public static class ElmRefundPolicyTable extends DropDownTable
  {
    public ElmRefundPolicyTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Refund in 30 Days or Less");
      addElement("2","No Refunds or Exchanges");
      addElement("3","Exchanges Only");
      addElement("6","Cash Back");
      addElement("7","Full Refund");
      addElement("4","Not Applicable");
    }
  }
  
  public static class BankAccountTable extends DropDownTable
  {
    public BankAccountTable()
    {
      addElement("","select one");
      addElement("2","Checking");
      addElement("1","Savings");
    }
  }

  /*
  ** public static class BranchTable extends DropDownTable
  **
  ** List of River City referring branches with association derived from each
  ** branch's hierarchy node id.
  */
  public static class BranchTable extends DropDownTable
  {
    public BranchTable()
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:87^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    t1.descendent assoc_hid,
//                      o2.org_name branch_name
//                 
//            from      t_hierarchy t1,
//                      t_hierarchy t2,
//                      organization o1,
//                      organization o2
//                 
//            where     t1.ancestor = 3941400041
//                      and t1.relation = 4
//                      and t1.descendent = o1.org_group
//                      and t2.descendent = t1.descendent
//                      and t2.relation = 1
//                      and t2.ancestor = o2.org_group
//                      and lower(o1.org_name) like 'non chain%'  
//                 
//            order by  t2.ancestor,o1.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    t1.descendent assoc_hid,\n                    o2.org_name branch_name\n               \n          from      t_hierarchy t1,\n                    t_hierarchy t2,\n                    organization o1,\n                    organization o2\n               \n          where     t1.ancestor = 3941400041\n                    and t1.relation = 4\n                    and t1.descendent = o1.org_group\n                    and t2.descendent = t1.descendent\n                    and t2.relation = 1\n                    and t2.ancestor = o2.org_group\n                    and lower(o1.org_name) like 'non chain%'  \n               \n          order by  t2.ancestor,o1.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.elmhurst.Business",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.elmhurst.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:106^9*/
        rs = it.getResultSet();
        
        addElement("","select one");
        while (rs.next())
        {
          addElement(rs.getString("assoc_hid"),rs.getString("branch_name"));
        }
      }
      catch (Exception e)
      {
        logEntry("BranchTable()",e.toString());
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  
  public class FieldNotBlankCondition implements Condition
  {
    private Field field;
    
    public FieldNotBlankCondition(Field field)
    {
      this.field = field;
    }
    
    public boolean isMet()
    {
      return !field.isBlank();
    }
  }
  
  public class SeasonalValidation implements Validation
  {
    private FieldGroup monthGroup;
    private String errorText = "";
    
    public SeasonalValidation(FieldGroup monthGroup)
    {
      this.monthGroup = monthGroup;
    }
    
    public boolean validate(String fdata)
    {
      if (fdata == null || fdata.equals(""))
      {
        return true;
      }
      
      // count up checked months
      boolean monthPicked = false;
      for (Iterator i = monthGroup.iterator(); i.hasNext() && !monthPicked;)
      {
        Field month = (Field)i.next();
        if (month.getData().toLowerCase().equals("y"))
        {
          monthPicked = true;
        }
      }
      
      if (fdata.toLowerCase().equals("y") && monthPicked)
      {
        return true;
      }
      
      if (fdata.toLowerCase().equals("n") && !monthPicked)
      {
        return true;
      }
      
      if (!monthPicked)
      {
        errorText = "Please select high season months for seasonal merchant";
      }
      else
      {
        errorText = "Clear selected high season months for non-seasonal merchant";
      }
      
      return false;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }
  
  public class AmexAccountValidation extends AccountValidation
  {
    public AmexAccountValidation( Field[] fields )
    { 
      super( fields );
    }
    
    public boolean validate(String fieldData)
    {
      Field       field         = null;
      int         fieldIndex    = 0;
      int         hasDataCount  = 0;
      long        temp          = 0L;
            
      // reset the error message
      ErrorMessage = null;
      
      // amex not checked or value is "N"
      if( isBlank(fieldData) == true || fieldData.toUpperCase().equals("N") )
      {
        if ( hasAdditionalData() )
        {
          ErrorMessage = "Either mark American Express as accepted or clear related fields";
        }
      }
      else    // amex payment option is checked
      {
        hasDataCount = getFieldsWithDataCount();
        
        if ( hasDataCount == 1 )      // only one field has data
        {
          // get the index into the fields array of the
          // first field that is not blank.
          fieldIndex  = getFirstFieldWithDataIndex();
          field       = FieldList[fieldIndex];
          
          switch( fieldIndex )
          {
            case FIDX_ACCOUNT_NUMBER:
              try
              {
                // first start by validating that it is a number
                temp  = field.asLong();
        
                if ( field.getData().length() < 10 )
                {
                  ErrorMessage = "Amex Merchant Number must be at least 10 digits long";
                }
              }
              catch( NumberFormatException e )
              {
                ErrorMessage = "Amex Merchant Number must be a valid number";
              }
              break;
              
//              case FIDX_NEW_ACCT_RATE:
//              case FIDX_SPLIT_DIAL:
            default:
              break;
          }              
        }            
        else
        {
          ErrorMessage = "Please do only one of the following: provide a valid Amex SE# or check Apply for new account";
        }
      }
      return( ErrorMessage == null );
    }
  }
  
  public class DiscoverAccountValidation extends AccountValidation
  {
    public DiscoverAccountValidation( Field[] fields )
    {
      super( fields );
    }
    
    public boolean validate(String fieldData)
    {
      Field       field         = null;
      int         fieldIndex    = 0;
      int         hasDataCount  = 0;
      long        temp          = 0L;
    
      // reset the error message
      ErrorMessage = null;
      
      if( isBlank(fieldData) == true || fieldData.toUpperCase().equals("N") )
      {
        if ( hasAdditionalData() )
        {
          ErrorMessage = "Either mark Discover as accepted or clear related fields";
        }
      }
      else
      {
        hasDataCount = getFieldsWithDataCount();
        
        if ( hasDataCount == 1 )
        {
          fieldIndex    = getFirstFieldWithDataIndex();
          field         = FieldList[fieldIndex];
        
          switch( fieldIndex )
          {
            case FIDX_ACCOUNT_NUMBER:
              try
              {
                temp = Long.parseLong(field.getData());
                if ( field.getData().startsWith("60110") != true )
                {
                  ErrorMessage = "Discover Merchant Number must begin with 60110";
                }
                else if ( field.getData().length() < 15 )
                {
                  ErrorMessage = "Discover Merchant Number must be at least 15 digits long";
                }
              }
              catch( NumberFormatException e )
              {
                ErrorMessage = "Discover Merchant Number must be a valid number";
              }
              break;
              
//            case FIDX_NEW_ACCT_RATE:
            default:
              break;
          }
        }
        else
        {
          ErrorMessage = "Please do only one of the following: provide a valid Discover Merchant # or check Apply for new account";
        }
      }
      return( ErrorMessage == null );
    }
  }
  
  public void init()
  {
    super.init();
    
    try
    {
      // elmhurst deltas (uses rivercity as baseline)

      // add additional 
      FieldGroup fgElm = new FieldGroup("fgElmhurst");
      
      fgElm.add(new DropDownField       ("elmSeasonal",             "Seasonal Merchant",new YesNoTable(),false));
      
      // make sic code required
      //fields.getField("sicCode").makeRequired();
      
      // setup seasonal merchant info
      FieldGroup fgElmMonths = new FieldGroup("fgElmMonths");

      fgElmMonths.add(new CheckboxField ("elmJan",                  "Jan",false));
      fgElmMonths.add(new CheckboxField ("elmFeb",                  "Feb",false));
      fgElmMonths.add(new CheckboxField ("elmMar",                  "Mar",false));
      fgElmMonths.add(new CheckboxField ("elmApr",                  "Apr",false));
      fgElmMonths.add(new CheckboxField ("elmMay",                  "May",false));
      fgElmMonths.add(new CheckboxField ("elmJun",                  "Jun",false));
      fgElmMonths.add(new CheckboxField ("elmJul",                  "Jul",false));
      fgElmMonths.add(new CheckboxField ("elmAug",                  "Aug",false));
      fgElmMonths.add(new CheckboxField ("elmSep",                  "Sep",false));
      fgElmMonths.add(new CheckboxField ("elmOct",                  "Oct",false));
      fgElmMonths.add(new CheckboxField ("elmNov",                  "Nov",false));
      fgElmMonths.add(new CheckboxField ("elmDec",                  "Dec",false));

      fgElm.add(fgElmMonths);
      
      // add business account types, second business account
      fgElm.add(new DropDownField       ("typeOfAcct",              "Bank Account Type",new BankAccountTable(),false));
      fgElm.add(new DropDownField       ("typeOfAcct2",             "Bank Account Type",new BankAccountTable(),false));
      
      fgElm.add(new Field               ("checkingAccount2",        "Checking Acct. #",17,35,true));
      fgElm.add(new Field               ("confirmCheckingAccount2", "Confirm Checking Acct. #",17,35,false));
      fgElm.add(new Field               ("transitRouting2",         "Transit Routing #",9,35,false));
      fgElm.add(new Field               ("confirmTransitRouting2",  "Confirm Transit Routing #",9,35,false));
      
      fgElm.getField("confirmCheckingAccount2")
        .addValidation(new FieldEqualsFieldValidation(
          fgElm.getField("checkingAccount2"),"Checking Account #"));
          
      fgElm.getField("confirmTransitRouting2")
        .addValidation(new FieldEqualsFieldValidation(
          fgElm.getField("transitRouting2"),"Transit Routing #"));
          
      fgElm.getField("transitRouting2")
        .addValidation(new TransitRoutingValidation());
        
      fgElm.getField("confirmCheckingAccount2")
        .setOptionalCondition(new FieldNotBlankCondition(
          fgElm.getField("checkingAccount2")));
          
      fgElm.getField("transitRouting2")
        .setOptionalCondition(new FieldNotBlankCondition(
          fgElm.getField("checkingAccount2")));
          
      fgElm.getField("confirmTransitRouting2")
        .setOptionalCondition(new FieldNotBlankCondition(
          fgElm.getField("checkingAccount2")));
          
      fgElm.getField("typeOfAcct2")
        .setOptionalCondition(new FieldNotBlankCondition(
          fgElm.getField("checkingAccount2")));
          
      fgElm.getField("elmSeasonal")
        .addValidation(new SeasonalValidation(fgElmMonths));
      
      fgElm.setGroupHtmlExtra("class=\"formFields\"");
      fields.add(fgElm);
      
      // remove required from legal name
      fields.getField("businessLegalName").removeValidation("required");
      
      // turn off required validation on acct info source 
      // and referring bank (won't be shown)
      fields.getField("sourceOfInfo") .removeValidation("required");
      fields.getField("referringBank").removeValidation("required");
      
      // replace base refund policy with rivercity list
      fields.add(new DropDownField("refundPolicy",new ElmRefundPolicyTable(),false));
      
      // add amex and discover apply on behalf of bank flags
      fields.add(new CheckboxField("elmApplyForAmex","Apply for new account",false));
      fields.add(new CheckboxField("elmApplyForDisc","Apply for new account",false));
      
      // replace base form's amex accepted check box and add the amex validation
      fields.add(new CheckboxField("amexAccepted","American Express",false));
      Validation val = new AmexAccountValidation( new Field[]
                                        {
                                          fields.getField("amexAcctNum"),
                                          fields.getField("elmApplyForAmex"),
                                          fields.getField("amexEsaRate")
                                        } );
      fields.getField("amexAccepted").addValidation(val);
      
      // add the split dial/pip validation
      OnlyOneValidation amexOnlyOneVal = new OnlyOneValidation(
        "Please select either Amex Split Dial or PIP, not both");
      amexOnlyOneVal.addField(fields.getField("amexSplitDial"));
      amexOnlyOneVal.addField(fields.getField("amexPIP"));
      fields.getField("amexAccepted").addValidation(amexOnlyOneVal);

      
      // replace base form's disc accepted check box and add the amex validation
      fields.add(new CheckboxField("discoverAccepted","Discover",false));
      val = new DiscoverAccountValidation( new Field[]
                                        {
                                          fields.getField("discoverAcctNum"),
                                          fields.getField("elmApplyForDisc"),
                                          fields.getField("discoverRapRate")
                                        } );
      fields.getField("discoverAccepted").addValidation(val);
      
      
      
      // set location number to 1 by default
      fields.setData("numLocations","1");

      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch( Exception e )
    {
      logEntry("init()",e.toString());
    }
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
    
    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }
  }
  
  protected void storeBankData()
  {
    super.storeBankData();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:486^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchbank
//          where   app_seq_num = :AppSeqNum and
//                  merchbank_acct_srnum = 2
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merchbank\n        where   app_seq_num =  :1  and\n                merchbank_acct_srnum = 2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.elmhurst.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:492^7*/

      if (!fields.getField("checkingAccount2").isBlank())
      {
        /*@lineinfo:generated-code*//*@lineinfo:496^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchbank
//            (
//              app_seq_num,
//              bankacc_type,
//              merchbank_info_source,
//              merchbank_name,
//              merchbank_acct_num,
//              merchbank_transit_route_num,
//              merchbank_num_years_open,
//              merchbank_acct_srnum
//            )
//            values
//            (
//              :AppSeqNum,
//              :fields.getField("typeOfAcct2").getData(),
//              :fields.getField("sourceOfInfo").getData(),
//              :fields.getField("bankName").getData(),
//              :fields.getField("checkingAccount2").getData(),
//              :fields.getField("transitRouting2").getData(),
//              :fields.getField("yearsOpen").getData(),
//              2
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2775 = fields.getField("typeOfAcct2").getData();
 String __sJT_2776 = fields.getField("sourceOfInfo").getData();
 String __sJT_2777 = fields.getField("bankName").getData();
 String __sJT_2778 = fields.getField("checkingAccount2").getData();
 String __sJT_2779 = fields.getField("transitRouting2").getData();
 String __sJT_2780 = fields.getField("yearsOpen").getData();
   String theSqlTS = "insert into merchbank\n          (\n            app_seq_num,\n            bankacc_type,\n            merchbank_info_source,\n            merchbank_name,\n            merchbank_acct_num,\n            merchbank_transit_route_num,\n            merchbank_num_years_open,\n            merchbank_acct_srnum\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n            2\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.elmhurst.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,__sJT_2775);
   __sJT_st.setString(3,__sJT_2776);
   __sJT_st.setString(4,__sJT_2777);
   __sJT_st.setString(5,__sJT_2778);
   __sJT_st.setString(6,__sJT_2779);
   __sJT_st.setString(7,__sJT_2780);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:520^9*/
      }
    }
    catch(Exception e)
    {
      addError("storeBankData: " + e.toString());
      logEntry("storeBankData()", e.toString());
    }
  }
    

  public void storeData( )
  {
    super.storeData();

    try
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:539^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_merch_elm
//            ( app_seq_num,
//              seasonal,
//              seas_jan,
//              seas_feb,
//              seas_mar,
//              seas_apr,
//              seas_may,
//              seas_jun,
//              seas_jul,
//              seas_aug,
//              seas_sep,
//              seas_oct,
//              seas_nov,
//              seas_dec,
//              apply_for_amex,
//              apply_for_disc  )
//              
//            values
//            ( :AppSeqNum,
//              : getData("elmSeasonal"),
//              : getData("elmJan"),
//              : getData("elmFeb"),
//              : getData("elmMar"),
//              : getData("elmApr"),
//              : getData("elmMay"),
//              : getData("elmJun"),
//              : getData("elmJul"),
//              : getData("elmAug"),
//              : getData("elmSep"),
//              : getData("elmOct"),
//              : getData("elmNov"),
//              : getData("elmDec"),
//              : getData("elmApplyForAmex"),
//              : getData("elmApplyForDisc") )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2781 =  getData("elmSeasonal");
 String __sJT_2782 =  getData("elmJan");
 String __sJT_2783 =  getData("elmFeb");
 String __sJT_2784 =  getData("elmMar");
 String __sJT_2785 =  getData("elmApr");
 String __sJT_2786 =  getData("elmMay");
 String __sJT_2787 =  getData("elmJun");
 String __sJT_2788 =  getData("elmJul");
 String __sJT_2789 =  getData("elmAug");
 String __sJT_2790 =  getData("elmSep");
 String __sJT_2791 =  getData("elmOct");
 String __sJT_2792 =  getData("elmNov");
 String __sJT_2793 =  getData("elmDec");
 String __sJT_2794 =  getData("elmApplyForAmex");
 String __sJT_2795 =  getData("elmApplyForDisc");
   String theSqlTS = "insert into app_merch_elm\n          ( app_seq_num,\n            seasonal,\n            seas_jan,\n            seas_feb,\n            seas_mar,\n            seas_apr,\n            seas_may,\n            seas_jun,\n            seas_jul,\n            seas_aug,\n            seas_sep,\n            seas_oct,\n            seas_nov,\n            seas_dec,\n            apply_for_amex,\n            apply_for_disc  )\n            \n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.elmhurst.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,__sJT_2781);
   __sJT_st.setString(3,__sJT_2782);
   __sJT_st.setString(4,__sJT_2783);
   __sJT_st.setString(5,__sJT_2784);
   __sJT_st.setString(6,__sJT_2785);
   __sJT_st.setString(7,__sJT_2786);
   __sJT_st.setString(8,__sJT_2787);
   __sJT_st.setString(9,__sJT_2788);
   __sJT_st.setString(10,__sJT_2789);
   __sJT_st.setString(11,__sJT_2790);
   __sJT_st.setString(12,__sJT_2791);
   __sJT_st.setString(13,__sJT_2792);
   __sJT_st.setString(14,__sJT_2793);
   __sJT_st.setString(15,__sJT_2794);
   __sJT_st.setString(16,__sJT_2795);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:576^9*/
      }
      catch (SQLException e)
      {
        /*@lineinfo:generated-code*//*@lineinfo:580^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_merch_elm
//            
//            set     seasonal        = : getData("elmSeasonal"),
//                    seas_jan        = : getData("elmJan"),
//                    seas_feb        = : getData("elmFeb"),
//                    seas_mar        = : getData("elmMar"),
//                    seas_apr        = : getData("elmApr"),
//                    seas_may        = : getData("elmMay"),
//                    seas_jun        = : getData("elmJun"),
//                    seas_jul        = : getData("elmJul"),
//                    seas_aug        = : getData("elmAug"),
//                    seas_sep        = : getData("elmSep"),
//                    seas_oct        = : getData("elmOct"),
//                    seas_nov        = : getData("elmNov"),
//                    seas_dec        = : getData("elmDec"),
//                    apply_for_amex  = : getData("elmApplyForAmex"),
//                    apply_for_disc  = : getData("elmApplyForDisc")
//                    
//            where   app_seq_num   = :AppSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2796 =  getData("elmSeasonal");
 String __sJT_2797 =  getData("elmJan");
 String __sJT_2798 =  getData("elmFeb");
 String __sJT_2799 =  getData("elmMar");
 String __sJT_2800 =  getData("elmApr");
 String __sJT_2801 =  getData("elmMay");
 String __sJT_2802 =  getData("elmJun");
 String __sJT_2803 =  getData("elmJul");
 String __sJT_2804 =  getData("elmAug");
 String __sJT_2805 =  getData("elmSep");
 String __sJT_2806 =  getData("elmOct");
 String __sJT_2807 =  getData("elmNov");
 String __sJT_2808 =  getData("elmDec");
 String __sJT_2809 =  getData("elmApplyForAmex");
 String __sJT_2810 =  getData("elmApplyForDisc");
   String theSqlTS = "update  app_merch_elm\n          \n          set     seasonal        =  :1 ,\n                  seas_jan        =  :2 ,\n                  seas_feb        =  :3 ,\n                  seas_mar        =  :4 ,\n                  seas_apr        =  :5 ,\n                  seas_may        =  :6 ,\n                  seas_jun        =  :7 ,\n                  seas_jul        =  :8 ,\n                  seas_aug        =  :9 ,\n                  seas_sep        =  :10 ,\n                  seas_oct        =  :11 ,\n                  seas_nov        =  :12 ,\n                  seas_dec        =  :13 ,\n                  apply_for_amex  =  :14 ,\n                  apply_for_disc  =  :15 \n                  \n          where   app_seq_num   =  :16";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.elmhurst.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2796);
   __sJT_st.setString(2,__sJT_2797);
   __sJT_st.setString(3,__sJT_2798);
   __sJT_st.setString(4,__sJT_2799);
   __sJT_st.setString(5,__sJT_2800);
   __sJT_st.setString(6,__sJT_2801);
   __sJT_st.setString(7,__sJT_2802);
   __sJT_st.setString(8,__sJT_2803);
   __sJT_st.setString(9,__sJT_2804);
   __sJT_st.setString(10,__sJT_2805);
   __sJT_st.setString(11,__sJT_2806);
   __sJT_st.setString(12,__sJT_2807);
   __sJT_st.setString(13,__sJT_2808);
   __sJT_st.setString(14,__sJT_2809);
   __sJT_st.setString(15,__sJT_2810);
   __sJT_st.setLong(16,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:601^9*/
      }
    }
    catch( Exception e )
    {
      logEntry("storeData()",e.toString());
    }
    finally
    {
    }
  }

  public void loadData( )
  {
    super.loadData();
    
    ResultSetIterator it = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:621^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  seasonal        as elm_seasonal,
//                  seas_jan        as elm_jan,
//                  seas_feb        as elm_feb,
//                  seas_mar        as elm_mar,
//                  seas_apr        as elm_apr,
//                  seas_may        as elm_may,
//                  seas_jun        as elm_jun,
//                  seas_jul        as elm_jul,
//                  seas_aug        as elm_aug,
//                  seas_sep        as elm_sep,
//                  seas_oct        as elm_oct,
//                  seas_nov        as elm_nov,
//                  seas_dec        as elm_dec,
//                  apply_for_amex  as elm_apply_for_amex,
//                  apply_for_disc  as elm_apply_for_disc
//          from    app_merch_elm
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  seasonal        as elm_seasonal,\n                seas_jan        as elm_jan,\n                seas_feb        as elm_feb,\n                seas_mar        as elm_mar,\n                seas_apr        as elm_apr,\n                seas_may        as elm_may,\n                seas_jun        as elm_jun,\n                seas_jul        as elm_jul,\n                seas_aug        as elm_aug,\n                seas_sep        as elm_sep,\n                seas_oct        as elm_oct,\n                seas_nov        as elm_nov,\n                seas_dec        as elm_dec,\n                apply_for_amex  as elm_apply_for_amex,\n                apply_for_disc  as elm_apply_for_disc\n        from    app_merch_elm\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.elmhurst.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.app.elmhurst.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:640^7*/
      setFields(it.getResultSet());
      it.close();

      // bank account information
      /*@lineinfo:generated-code*//*@lineinfo:645^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bankacc_type                as type_of_acct2,
//                  merchbank_acct_num          as checking_account2,
//                  merchbank_acct_num          as confirm_checking_account2,
//                  merchbank_transit_route_num as transit_routing2,
//                  merchbank_transit_route_num as confirm_transit_routing2
//          from    merchbank
//          where   app_seq_num = :AppSeqNum and
//                  merchbank_acct_srnum = 2
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bankacc_type                as type_of_acct2,\n                merchbank_acct_num          as checking_account2,\n                merchbank_acct_num          as confirm_checking_account2,\n                merchbank_transit_route_num as transit_routing2,\n                merchbank_transit_route_num as confirm_transit_routing2\n        from    merchbank\n        where   app_seq_num =  :1  and\n                merchbank_acct_srnum = 2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.elmhurst.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.app.elmhurst.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:655^7*/
      setFields(it.getResultSet());
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadData()",e.toString());
    }                          
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }
}/*@lineinfo:generated-code*/