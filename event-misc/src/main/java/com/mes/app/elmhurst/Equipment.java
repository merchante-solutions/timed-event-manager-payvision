/**************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/elmhurst/Equipment.java $

  Description:
  
  Equipment
  
  Elmhurst online application equipment page bean.


  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 2/11/04 3:56p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.elmhurst;

//import sqlj.runtime.ResultSetIterator;
import com.mes.app.EquipmentDataBean;
import com.mes.forms.Validation;

public class Equipment extends EquipmentDataBean
{
  public class RequiredIfNotDialpayValidation implements Validation
  {
    public boolean validate(String fdata)
    {
      return (fdata != null && !fdata.equals("")) || isDialPayApp();
    }
    
    public String getErrorText()
    {
      return "Field required";
    }
  }
  
  public void init( )
  {
    super.init();
    
    try
    {
      // set some elmhurst defaults
      fields.setData("resetTranNumEnable","y");
      fields.setData("fraudControlEnable","y");
      fields.setData("avsEnable","y");
      
      // make phone training option required
      fields.getField("trainingType")
        .addValidation(new RequiredIfNotDialpayValidation());
    }
    catch( Exception e )
    {
      System.out.println("init(): " + e.toString());
      logEntry("init()",e.toString());
    }
  }
}