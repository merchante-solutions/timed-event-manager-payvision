/*@lineinfo:filename=EquipmentBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/EquipmentBase.sqlj $

  Description:


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-09-06 11:36:48 -0700 (Thu, 06 Sep 2007) $
  Version            : $Revision: 14106 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.CityStateZipField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.support.HttpHelper;
import com.mes.support.NumberFormatter;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class EquipmentBase extends AppBase
{
  static Logger log = Logger.getLogger(EquipmentBase.class);

  private static final int          ER_NONE         = -1;
  private static final int          ER_USER         = 0;

  protected static final String[][] AmPmRadioButtons =
  {
    { "AM","AM" },
    { "PM","PM" }
  };

  protected static final String[][] TrainingTypeRadioButtons =
  {
    { "MES Help Desk",  "M"  },
    { "Sales/Bank Rep", "S"  },
    { "No Training",    "N"  }
  };

  protected static final String[][] CommTypeRadioButtons =
  {
    { "Dial",       Integer.toString(mesConstants.TERM_COMM_TYPE_DIAL)  },
    { "IP",         Integer.toString(mesConstants.TERM_COMM_TYPE_IP)    },
    { "Wireless",   Integer.toString(mesConstants.TERM_COMM_TYPE_WIRELESS)}
  };

  protected static final String[] AutoCloseErrorMessages =
  {
    "Please provide the hour(HH), minute(MM), and AM/PM of when the auto batch close will occur",
    "Please check the Auto Batch Close check box or delete the entered hour(HH) and minute(MM)"
  };

  protected static final String[] TerminalReminderErrorMessages =
  {
    "Please provide the hour(HH), minute(MM), and AM/PM of Terminal Reminder to Check Totals",
    "Please check the Terminal Reminder to Check Totals check box or delete the entered hour(HH) and minute(MM)"
  };

  protected static final String[] EquipmentFieldNames =
  {
    "peripheral",     // mesConstants.APP_EQUIP_TYPE_PERIPHERAL
    "terminal",       // mesConstants.APP_EQUIP_TYPE_TERMINAL
    "printer",        // mesConstants.APP_EQUIP_TYPE_PRINTER
    "pinPad",         // mesConstants.APP_EQUIP_TYPE_PINPAD
    "imprinter",      // mesConstants.APP_EQUIP_TYPE_IMPRINTER
    "termPrint",      // mesConstants.APP_EQUIP_TYPE_TERM_PRINTER
    "termPinPad",     // mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD
    null,             // mesConstants.APP_EQUIP_TYPE_OTHERS
    null,             // mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE
    null              // mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES
  };

  // sorted version of the above with the label visible
  // to the HTML browser
  public static final String[][] EquipSelectDisplay =
  {
    { "termPrint",  "Terminal/Printer Sets"  },       // mesConstants.APP_EQUIP_TYPE_TERM_PRINTER
    { "termPinPad", "Terminal/Printer/Pinpad Sets"  },// mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD
    { "terminal",   "Terminals"  },                   // mesConstants.APP_EQUIP_TYPE_TERMINAL
    { "printer",    "Printers"  },                    // mesConstants.APP_EQUIP_TYPE_PRINTER
    { "pinPad",     "Pinpads"  },                     // mesConstants.APP_EQUIP_TYPE_PINPAD
    { "peripheral", "Peripherals"  },                 // mesConstants.APP_EQUIP_TYPE_PERIPHERAL
    { "imprinter",  "Imprinters"  },                  // mesConstants.APP_EQUIP_TYPE_IMPRINTER
  };

  //**************************************************************************
  // field storage classes
  //**************************************************************************
  protected class EquipmentSelectionTable extends DropDownTable
  {
    public EquipmentSelectionTable( int appType, int equipType, int equipSection )
      throws java.sql.SQLException
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;

      try
      {
        connect();
        addElement("","select one");
        /*@lineinfo:generated-code*//*@lineinfo:123^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode( prod_option_id,
//                            null, equip_model,
//                            ( equip_model || '*' || prod_option_id )
//                          )                               as equip_model,
//                    equip_description                     as equip_desc
//            from    equipment_application
//            where   app_type      = :appType and
//                    equip_type    = :equipType and
//                    equip_section = :equipSection
//            order by equip_type,
//                     equip_sort_order asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode( prod_option_id,\n                          null, equip_model,\n                          ( equip_model || '*' || prod_option_id )\n                        )                               as equip_model,\n                  equip_description                     as equip_desc\n          from    equipment_application\n          where   app_type      =  :1  and\n                  equip_type    =  :2  and\n                  equip_section =  :3 \n          order by equip_type,\n                   equip_sort_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   __sJT_st.setInt(2,equipType);
   __sJT_st.setInt(3,equipSection);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.EquipmentBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:136^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }

  protected class LeaseCompanyTable extends DropDownTable
  {
    public LeaseCompanyTable()
      throws java.sql.SQLException
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;

      try
      {
        connect();
        addElement("","select one");
        /*@lineinfo:generated-code*//*@lineinfo:166^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  lease_company_code      as lease_code,
//                    lease_company_desc      as lease_desc
//            from    equip_lease_company
//            order by  lease_company_desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  lease_company_code      as lease_code,\n                  lease_company_desc      as lease_desc\n          from    equip_lease_company\n          order by  lease_company_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.EquipmentBase",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.EquipmentBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:172^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ resultSet.close(); } catch(Exception e) {}
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }

  protected class LeaseMonthsTable extends DropDownTable
  {
    public LeaseMonthsTable( )
    {
      addElement("","select one");
      addElement("12","12 months");
      addElement("24","24 months");
      addElement("36","36 months");
      addElement("48","48 months");
    }
  }

  protected class ShippingAddressTable extends DropDownTable
  {
    public ShippingAddressTable( )
    {
      addElement( Integer.toString(mesConstants.ADDR_TYPE_NONE),     "None" );
      addElement( Integer.toString(mesConstants.ADDR_TYPE_BUSINESS), "Use Business Address" );
      addElement( Integer.toString(mesConstants.ADDR_TYPE_MAILING),  "Use Mailing Address" );
      addElement( Integer.toString(mesConstants.ADDR_TYPE_SHIPPING), "Use Other Address" );
    }
  }

  protected class ShippingMethodTable extends DropDownTable
  {
    public ShippingMethodTable( long appSeqNum )
    {
      ResultSetIterator   it                = null;
      ResultSet           rs                = null;
      String              shippingMethod    = null;

      try
      {
        connect();

        try
        {
          // get the shipping method from the application
          /*@lineinfo:generated-code*//*@lineinfo:229^11*/

//  ************************************************************
//  #sql [Ctx] { select  pf.shipping_method 
//              from    pos_features  pf
//              where   pf.app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pf.shipping_method  \n            from    pos_features  pf\n            where   pf.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.EquipmentBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   shippingMethod = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:234^11*/
        }
        catch( Exception ee )
        {
          // ignore, shipping method not set yet
        }

        /*@lineinfo:generated-code*//*@lineinfo:241^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  code,
//                    description
//            from    shipping_methods
//            where   trunc(nvl( ( select  app.app_created_date
//                                 from    application app
//                                 where   app.app_seq_num(+) = :appSeqNum ),
//                               sysdate
//                             )
//                         ) between from_date and to_date or
//                    code = nvl(:shippingMethod,'-1')
//            order by display_order, from_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  code,\n                  description\n          from    shipping_methods\n          where   trunc(nvl( ( select  app.app_created_date\n                               from    application app\n                               where   app.app_seq_num(+) =  :1  ),\n                             sysdate\n                           )\n                       ) between from_date and to_date or\n                  code = nvl( :2 ,'-1')\n          order by display_order, from_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,shippingMethod);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.EquipmentBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:254^9*/
        rs = it.getResultSet();

        while( rs.next() )
        {
          addElement(rs);
        }
        rs.close();
      }
      catch( Exception e )
      {
        logEntry("ShippingMethodTable()",e.toString());
      }
      finally
      {
        try{ it.close(); } catch( Exception e ){}
        cleanUp();
      }
    }
  }

  protected class TerminalApplicationTable extends DropDownTable
  {
    public TerminalApplicationTable()
    {
      addElement("",            "select one"  );
      addElement("Retail",      "Retail"      );
      addElement("Restaurant",  "Restaurant"  );
      addElement("Lodging",     "Lodging"     );
      addElement("Cash Advance","Cash Advance");
    }
  }

  //**************************************************************************
  // Validations
  //**************************************************************************
  protected class ActionTimeValidation implements Validation
  {
    // error indexes
    private static final int          ER_NEED_DATA    = (ER_USER + 0);
    private static final int          ER_REMOVE_DATA  = (ER_USER + 1);

    private int                       ErrorIndex      = ER_NONE;
    private String[]                  ErrorMessages   = null;
    private Field[]                   TimeFields      = null;

    public ActionTimeValidation( Field[] fields, String[] errorMsgs )
    {
      ErrorMessages   = errorMsgs;
      TimeFields      = fields;
    }

    public String getErrorText()
    {
      return( (ErrorIndex == ER_NONE) ? "" : ErrorMessages[ErrorIndex] );
    }

    public boolean validate(String fdata)
    {
      boolean         shouldBeBlank   = true;

      // set the flag based on the checkbox value
      if ( fdata != null && fdata.toUpperCase().equals("Y") )
      {
        shouldBeBlank = false;
      }

      // reset the error message content
      ErrorIndex = ER_NONE;

      // scan through the fields and make sure that they
      // are full/empty based on the checkbox state.
      for( int i = 0; i < TimeFields.length; ++i )
      {
        if ( TimeFields[i].isBlank() != shouldBeBlank )
        {
          if( shouldBeBlank == true )
          {
            if(TimeFields[i] instanceof RadioButtonField)
            {
              //if its a radio button and it should be blank..we reset the field..
              //there is no way for a user to unselect a radio button
              ((RadioButtonField)TimeFields[i]).resetRadioButtons();
            }
            else
            {
              ErrorIndex = ER_REMOVE_DATA;
            }
          }
          else    // needs data
          {
            ErrorIndex = ER_NEED_DATA;
          }
          break;
        }
      }
      return( ErrorIndex == ER_NONE );
    }
  }

  protected class EquipCountValidation implements Validation
  {
    // error indexes
    private static final int          ER_NEED_COUNTS      = (ER_USER + 0);
    private static final int          ER_REMOVE_COUNTS    = (ER_USER + 1);

    private Field[]                   CountFields         = null;
    int                               ErrorIndex          = ER_NONE;
    private String[]                  ErrorMessages       = null;

    public EquipCountValidation( Field[] fields, String[] errorMsgs )
    {
      CountFields   = fields;
      ErrorMessages = errorMsgs;
    }

    public String getErrorText()
    {
      return( (ErrorIndex == ER_NONE) ? "" : ErrorMessages[ErrorIndex] );
    }

    private boolean hasCountFieldsWithData( )
    {
      boolean   retVal = false;

      for( int i = 0; i < CountFields.length; ++i )
      {
        if ( !CountFields[i].isBlank() && CountFields[i].asInteger() > 0 && CountFields[i].asInteger() < 100)
        {
          retVal = true;
          break;
        }
      }

      return( retVal );
    }

    public boolean validate(String fdata)
    {
      boolean           hasCounts     = hasCountFieldsWithData();

      ErrorIndex = ER_NONE;

      if ( fdata == null || fdata.equals("") )
      {
        if ( hasCounts == true )
        {
          ErrorIndex = ER_REMOVE_COUNTS;
        }
      }
      else    // has data, needs counts
      {
        if ( hasCounts == false )
        {
          ErrorIndex = ER_NEED_COUNTS;
        }
      }
      return( ErrorIndex == ER_NONE );
    }
  }

  protected class EquipCompatibilityValidation
    extends SQLJConnectionBase implements Validation
  {
    private String                    ErrorMessage        = null;

    public EquipCompatibilityValidation( )
    {
    }

    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      long        appSeqNum         = 0L;
      String      equipModel        = null;
      int         index             = 0;
      int         recCount          = 0;
      int         supported         = 0;

      // reset the error message content
      ErrorMessage = null;

      try
      {
        connect();

        if ( !isBlank(fdata) )
        {
          // get the appSeqNum for the current app
          appSeqNum = fields.getField("appSeqNum").asLong();

          // extract the model name from the field.  if necessary
          // separate the model from the product id
          if ( (index = fdata.indexOf("*")) != -1 )
          {
            // separate the model from the product code
            equipModel  = fdata.substring(0,index);
          }
          else
          {
            equipModel  = fdata;
          }

          /*@lineinfo:generated-code*//*@lineinfo:461^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(mpo.app_seq_num) 
//              from    merchpayoption    mpo
//              where   mpo.app_seq_num = :appSeqNum and
//                      mpo.cardtype_code = :mesConstants.APP_CT_CHECK_AUTH and
//                      mpo.merchpo_provider_name in
//                      (
//                        'CertegyWelcomeCheck',
//                        'CertegyECheck'
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mpo.app_seq_num)  \n            from    merchpayoption    mpo\n            where   mpo.app_seq_num =  :1  and\n                    mpo.cardtype_code =  :2  and\n                    mpo.merchpo_provider_name in\n                    (\n                      'CertegyWelcomeCheck',\n                      'CertegyECheck'\n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.EquipmentBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_CHECK_AUTH);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:472^11*/

          // Certegy programs only support certain equipment
          if ( recCount > 0 )
          {
            supported = 0;

            /*@lineinfo:generated-code*//*@lineinfo:479^13*/

//  ************************************************************
//  #sql [Ctx] { select  decode( nvl(eq.certegy_check_supported,'N'),
//                                'Y', 1, 0 )
//                
//                from    equipment   eq
//                where   eq.equip_model = :equipModel
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode( nvl(eq.certegy_check_supported,'N'),\n                              'Y', 1, 0 )\n               \n              from    equipment   eq\n              where   eq.equip_model =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.EquipmentBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,equipModel);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   supported = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:486^13*/

            if ( supported == 0 )
            {
              ErrorMessage = "Check provider does not support this type of equipment";
            }
          }

          if ( ErrorMessage == null )
          {
            /*@lineinfo:generated-code*//*@lineinfo:496^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(mpo.app_seq_num) 
//                from    merchpayoption    mpo
//                where   mpo.app_seq_num = :appSeqNum and
//                        mpo.cardtype_code = :mesConstants.APP_CT_VALUTEC_GIFT_CARD
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mpo.app_seq_num)  \n              from    merchpayoption    mpo\n              where   mpo.app_seq_num =  :1  and\n                      mpo.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.EquipmentBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VALUTEC_GIFT_CARD);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:502^13*/

            // valutec only supports certain equipment
            if ( recCount > 0 )
            {
              supported = 0;

              /*@lineinfo:generated-code*//*@lineinfo:509^15*/

//  ************************************************************
//  #sql [Ctx] { select  decode( nvl(eq.valutec_gift_card_supported,'N'),
//                                  'Y', 1, 0 )
//                  
//                  from    equipment   eq
//                  where   eq.equip_model = :equipModel
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode( nvl(eq.valutec_gift_card_supported,'N'),\n                                'Y', 1, 0 )\n                 \n                from    equipment   eq\n                where   eq.equip_model =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.EquipmentBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,equipModel);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   supported = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:516^15*/

              if ( supported == 0 )
              {
                ErrorMessage = "Valutec Gift Card program does not support this type of equipment";
              }
            }
          }
        }
      }
      catch( Exception e )
      {
        ErrorMessage = e.toString();
      }
      finally
      {
        cleanUp();
      }

      return( (ErrorMessage == null) );
    }
  }

  protected class ShippingAddressValidation implements Validation
  {
    private Field[]                   ShippingAddrFields  = null;
    private String                    ErrorMessage        = null;

    public ShippingAddressValidation( Field[] fields )
    {
      ShippingAddrFields = fields;
    }

    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      int             iData     = -1;

      // reset the error message content
      ErrorMessage = null;

      try
      {
        // value should be an integer, so extract it
        iData = Integer.parseInt(fdata);
      }
      catch( Exception e )
      {
        ErrorMessage = "Please select a valid shipping address type";
      }

      if ( ErrorMessage == null )
      {
        if ( iData == mesConstants.ADDR_TYPE_SHIPPING )
        {
          // scan through the fields and make sure that they have data
          for( int i = 0; i < ShippingAddrFields.length; ++i )
          {
            if ( ShippingAddrFields[i].isBlank() )
            {
              ErrorMessage = "Please provide the shipping address details or select a pre-defined shipping address";
              break;
            }
          }
        }
      }
      return( ErrorMessage == null );
    }
  }

  protected int leaseCount = 0;
  protected class LeaseInfoValidation implements Validation
  {
    public boolean validate(String fdata)
    {
      for(int i = 0; leaseCount == 0 && i < EquipmentFieldNames.length; ++i)
      {
        try
        {
          leaseCount += Integer.parseInt(
            fields.getData(EquipmentFieldNames[i] + "LeaseCount"));
        }
        catch (Exception e) {}
      }

      if (fdata.equals("") && leaseCount > 0)
      {
        return false;
      }

      return true;
    }

    public String getErrorText()
    {
      return "Field required when lease equipment specified";
    }
  }

  protected class TerminalAppValidation implements Validation
  {
    public boolean validate(String fdata)
    {
      boolean isDialTerminal = isPosType(mesConstants.POS_DIAL_TERMINAL);

      // if dial terminal then field is required
      if (isDialTerminal && (fdata == null || fdata.equals("")))
      {
        errorText = "Required";
        return false;
      }

      return true;
    }

    String errorText = "";

    public String getErrorText()
    {
      return errorText;
    }
  }

  //**************************************************************************
  // EquipmentDataBean
  //**************************************************************************

  // pos type (set on page 1) determines if certain fee options are present
  protected int posType = -1;

  /*
  ** protected boolean loadPosType()
  **
  ** Loads the pos type from the pos_category/merch_pos.  This is an option set
  ** on page 1 of the app.
  **
  ** RETURNS: true if load successful, else false.
  */
  protected boolean loadPosType()
  {
    boolean loadOk = false;
    try
    {
      connect();

      long appSeqNum = fields.getField("appSeqNum").asLong();
      /*@lineinfo:generated-code*//*@lineinfo:666^7*/

//  ************************************************************
//  #sql [Ctx] { select  pc.pos_type
//          
//          from    pos_category  pc,
//                  merch_pos     mp
//          where   mp.app_seq_num = :appSeqNum
//                  and pc.pos_code = mp.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pc.pos_type\n         \n        from    pos_category  pc,\n                merch_pos     mp\n        where   mp.app_seq_num =  :1 \n                and pc.pos_code = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.EquipmentBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   posType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:674^7*/
      loadOk = true;
    }
    catch (Exception e)
    {
      logEntry("loadPosType()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return loadOk;
  }

  /*
  ** protected int getPosType()
  **
  ** Loads pos type if not loaded already.
  **
  ** RETURN: loaded pos type.
  */
  protected int getPosType()
  {
    if (posType == -1)
    {
      loadPosType();
    }
    return posType;
  }

  /*
  ** protected boolean isPosType(int checkPosType)
  **
  ** Checks the pos type given against the app's pos type.
  **
  ** RETURNS: true if app's pos type matches the given pos type.
  */
  protected boolean isPosType(int checkPosType)
  {
    return checkPosType == getPosType();
  }

  protected String decodeActionTime(String baseName)
  {
    StringBuffer retVal = new StringBuffer();

    log.debug("entering decodeActionTime(" + baseName + ")");
    try
    {
      boolean isPm  = fields.getData(baseName + "AmPm").equals("PM");

      long hour = 0L;
      long min  = 0L;

      if( ! fields.getData(baseName + "Hour").equals("") &&
          ! fields.getData(baseName + "Min").equals(""))
      {
        hour  = Long.parseLong(fields.getData(baseName + "Hour"));
        min   = Long.parseLong(fields.getData(baseName + "Min"));

        // sanity checks
        if (hour < 0)
        {
          hour = 0;
        }
        if (hour > 23)
        {
          hour = 23;
        }
        if (min > 59)
        {
          min = 59;
        }
        if (min < 0)
        {
          min = 0;
        }

        // convert military times
        if (hour == 0)
        {
          hour = 12;
          isPm = false;
        }
        else if (hour > 12)
        {
          isPm = true;
          hour -= 12;
        }

        retVal.append(Long.toString(hour));
        retVal.append(":");
        retVal.append(NumberFormatter.getLongString(min,"00"));
        retVal.append(isPm ? "PM" : "AM");
      }
    }
    catch(Exception e)
    {
      logEntry("decodeActionTime()",e.toString());
    }
    return( retVal.toString() );
  }

  private StringBuffer equipmentPricingDetails = new StringBuffer("");
  public boolean equipmentPricingAssigned()
  {
    boolean           result  = false;
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();

      int pricingCount = 0;
      long appSeqNum = fields.getField("appSeqNum").asLong();

      /*@lineinfo:generated-code*//*@lineinfo:791^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(me.app_seq_num)
//          
//          from    merchequipment me
//          where   me.app_seq_num = :appSeqNum and
//                  me.merchequip_amount is not null and
//                  me.merchequip_amount > 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(me.app_seq_num)\n         \n        from    merchequipment me\n        where   me.app_seq_num =  :1  and\n                me.merchequip_amount is not null and\n                me.merchequip_amount > 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.EquipmentBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   pricingCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:799^7*/

      result = (pricingCount > 0);

      // while we're here, establish the details of the pricing so it can be
      // displayed
      if(result)
      {
        /*@lineinfo:generated-code*//*@lineinfo:807^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  me.equip_model||' ('||(nvl(eq.equip_descriptor, 'Unknown'))||'): '||me.merchequip_equip_quantity||' @'||to_char(me.merchequip_amount,'$9999.00')||' each' as pricing_detail
//            from    merchequipment me,
//                    equipment eq
//            where   me.app_seq_num = :appSeqNum and
//                    me.merchequip_amount is not null and
//                    me.merchequip_amount > 0 and
//                    me.equip_model = eq.equip_model(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  me.equip_model||' ('||(nvl(eq.equip_descriptor, 'Unknown'))||'): '||me.merchequip_equip_quantity||' @'||to_char(me.merchequip_amount,'$9999.00')||' each' as pricing_detail\n          from    merchequipment me,\n                  equipment eq\n          where   me.app_seq_num =  :1  and\n                  me.merchequip_amount is not null and\n                  me.merchequip_amount > 0 and\n                  me.equip_model = eq.equip_model(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.app.EquipmentBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:816^9*/

        rs = it.getResultSet();

        equipmentPricingDetails.setLength(0);

        while(rs.next())
        {
          equipmentPricingDetails.append(rs.getString("pricing_detail"));
          equipmentPricingDetails.append("<br>");
        }

        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("equipmentPricingAssigned()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return result;
  }

  public String getEquipmentPricingDetails()
  {
    return equipmentPricingDetails.toString();
  }

  public void getIndustryType( )
  {
    int           industryType        = 0;

    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();

      /*@lineinfo:generated-code*//*@lineinfo:859^7*/

//  ************************************************************
//  #sql [Ctx] { select  industype_code 
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  industype_code  \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.app.EquipmentBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   industryType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:864^7*/
    }
    catch(Exception e)
    {
      logEntry("getIndustryType()", e.toString());
    }
  }

  protected int getLeaseCountTotal()
  {
    Field   field         = null;
    int     retVal        =  0;

    for( int i = 0; i < EquipmentFieldNames.length; ++i )
    {
      if ( EquipmentFieldNames[i] != null )
      {
        field = fields.getField( EquipmentFieldNames[i] + "LeaseCount" );
        if( field != null && !field.isBlank() )
        {
          retVal += field.asInteger();
        }
      }
    }

    return( retVal );
  }

  public String loadBusinessPhoneString()
  {
    String              retVal      = "";

    try
    {
      connect();

      long appSeqNum = fields.getField("appSeqNum").asLong();
      if ( appSeqNum != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:903^9*/

//  ************************************************************
//  #sql [Ctx] { select  '(' ||
//                    substr(address_phone,1,3) ||
//                    ')' || ' ' ||
//                    substr(address_phone,4,3) ||
//                    '-' ||
//                    substr(address_phone,7)     
//            from    address
//            where   app_seq_num = :appSeqNum and
//                    addresstype_code = :mesConstants.ADDR_TYPE_BUSINESS
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  '(' ||\n                  substr(address_phone,1,3) ||\n                  ')' || ' ' ||\n                  substr(address_phone,4,3) ||\n                  '-' ||\n                  substr(address_phone,7)      \n          from    address\n          where   app_seq_num =  :1  and\n                  addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.app.EquipmentBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_BUSINESS);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:914^9*/
      }
    }
    catch( java.sql.SQLException e )
    {
      // no data found
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }

  protected void createFields(HttpServletRequest request)
  {
    long                  appSeqNum       = 0L;
    Field                 field           = null;
    String                fieldName       = null;

    super.createFields(request);

    try
    {
      appSeqNum = HttpHelper.getLong(request,"appSeqNum",0L);

      for( int etype = 0; etype < EquipmentFieldNames.length; ++etype )
      {
        fieldName = EquipmentFieldNames[etype];

        // some fields to do not support drop down in the needed
        // section of the equipment page because they do not support
        // selection.  for example, if the user selected POS Partner 2000
        // on the first page, there is no need to show the equipment page.
        if ( fieldName != null )
        {
          // make a drop down field with the equipment models available for
          // this type of equipment for this application type.
          fields.add( new NumberField( (fieldName + "RentalCount"),3,2,true,0 ) );
          fields.add( new NumberField( (fieldName + "PurchaseCount"),3,2,true,0 ) );
          fields.add( new NumberField( (fieldName + "LeaseCount"),3,2,true,0 ) );
          field = new DropDownField( (fieldName + "NeededModels"), new EquipmentSelectionTable( appType, etype, mesConstants.APP_ES_NEEDED ), true );
          field.addValidation( new EquipCountValidation( new Field[]
                                                        {
                                                          fields.getField( (fieldName + "RentalCount") ),
                                                          fields.getField( (fieldName + "PurchaseCount") ),
                                                          fields.getField( (fieldName + "LeaseCount") )
                                                        },
                                                        new String[]
                                                        {
                                                          "Please provide a number of items to buy, rent or lease OR change equipment selection (1-99)",
                                                          "Please select a model of equipment to buy, rent or lease OR remove the quantity"
                                                        }  ) );
          if ( fieldName.equals("termPrint") ||
               fieldName.equals("termPinPad") ||
               fieldName.equals("terminal") )
          {
            field.addValidation( new EquipCompatibilityValidation() );
          }
          fields.add(field);


          // setup the owned equipment selections.  note that passing -1
          // as the application type will return the "all equipment type"
          fields.add( new NumberField( (fieldName + "OwnedCount"),3,2,true,0 ) );
          field = new DropDownField( (fieldName + "OwnedModels"), new EquipmentSelectionTable( mesConstants.APP_TYPE_ALL_EQUIP, etype, mesConstants.APP_ES_OWNED ), true );
          field.addValidation( new EquipCountValidation( new Field[]
                                                        {
                                                          fields.getField( (fieldName + "OwnedCount") )
                                                        },
                                                        new String[]
                                                        {
                                                          "Please provide a number of items owned OR change the owned equipment selection",
                                                          "Please select the model of owned equipment OR remove the owned quantity"
                                                        }  ) );
          if ( fieldName.equals("termPrint") ||
               fieldName.equals("termPinPad") ||
               fieldName.equals("terminal") )
          {
            field.addValidation( new EquipCompatibilityValidation() );
          }
          fields.add(field);
        }
      }

      // Lease Company Details
      fields.add( new DropDownField( "leaseCompany", new LeaseCompanyTable(), true ) );
      fields.add( new Field("leaseAcctNum", 8, 9, true) );
      fields.add( new Field("leaseAuthNum", 8, 9, true) );
      fields.add( new DropDownField( "leaseMonths", new LeaseMonthsTable(), true ) );
      fields.add( new NumberField( "leaseMonthlyAmount", 10, 13, true, 2 ) );
      fields.add( new NumberField( "leaseFundingAmount", 10, 13, true, 2 ) );

      Validation leaseVal = new LeaseInfoValidation();
      fields.getField("leaseCompany").addValidation(leaseVal);
      fields.getField("leaseAcctNum").addValidation(leaseVal);
      fields.getField("leaseAuthNum").addValidation(leaseVal);
      fields.getField("leaseMonths").addValidation(leaseVal);
      fields.getField("leaseMonthlyAmount").addValidation(leaseVal);
      fields.getField("leaseFundingAmount").addValidation(leaseVal);

      //*********************
      // Terminal Application
      //*********************

      fields.add(new DropDownField("terminalApp",new TerminalApplicationTable(),true));

      fields.getField("terminalApp").addValidation(new TerminalAppValidation());

      //*********************
      // Terminal Features
      //*********************

      // Access Code
      fields.add( new CheckboxField( "accessCodeEnable", "Access Code (# to get outside line)", false ) );
      field = new Field( "accessCode", 10, 5, true );
      field.addValidation( new IfYesNotBlankValidation( fields.getField("accessCodeEnable"), "Please provide a valid Access Code" ) );
      field.addValidation( new IfNoBlankValidation( fields.getField("accessCodeEnable"), "Please check the Access Code Check Box or delete the entered Access Code" ) );
      fields.add( field );

      // Auto Close feature
      fields.add( new NumberField("autoCloseHour",2,2,true,0) );
      fields.add( new NumberField("autoCloseMin",2,2,true,0) );
      fields.add( new RadioButtonField( "autoCloseAmPm", AmPmRadioButtons, -1, true, "Please select either AM or PM for the auto batch close" ) );
      field = new CheckboxField( "autoCloseEnable", "Auto Batch Close", false );
      field.addValidation( new ActionTimeValidation( new Field[]
                                                    { fields.getField("autoCloseHour"),
                                                      fields.getField("autoCloseMin"),
                                                      fields.getField("autoCloseAmPm") },
                                                     AutoCloseErrorMessages ) );
      fields.add(field);

      // Receipt Header Line 4
      field = new Field("receiptLine4",25,30,true);
      if ( isTerminalApp() )
      {
        fields.add( new CheckboxField("receiptLine4Enable","Receipt Header Line 4",true) );
        field.setData( loadBusinessPhoneString() );     // set the default
        field.addValidation( new IfYesNotBlankValidation( fields.getField("receiptLine4Enable"),"Please provide Line 4 for the Receipt Header" ));
        field.addValidation( new IfNoBlankValidation( fields.getField("receiptLine4Enable"),"Please check the Receipt Header Line 4 check box or clear the entered Line 4" ));
      }
      else
      {
        fields.add( new CheckboxField("receiptLine4Enable","Receipt Header Line 4",false) );
      }
      fields.add(field);

      // Receipt Header Line 5
      fields.add( new CheckboxField("receiptLine5Enable","Receipt Header Line 5",false) );
      field = new Field("receiptLine5",25,30,true);
      field.addValidation( new IfYesNotBlankValidation( fields.getField("receiptLine5Enable"),"Please provide Line 5 for the Receipt Header" ) );
      field.addValidation( new IfNoBlankValidation( fields.getField("receiptLine5Enable"),"Please check the Receipt Header Line 5 check box or delete the entered Line 5" ) );
      fields.add(field);

      // Receipt Footer
      fields.add( new CheckboxField("receiptFooterEnable","Receipt Footer",false) );
      field = new Field("receiptFooter",25,30,true);
      field.addValidation( new IfYesNotBlankValidation( fields.getField("receiptFooterEnable"),"Please provide a Receipt Footer" ) );
      field.addValidation( new IfNoBlankValidation( fields.getField("receiptFooterEnable"),"Please check the Receipt Footer check box or delete the entered Footer" ) );
      fields.add(field);

      // misc checkbox fields
      fields.add( new CheckboxField("resetTranNumEnable","Reset Transaction # Daily",false) );
      fields.add( new CheckboxField("fraudControlEnable","Fraud Control On (last 4 digits of card)",false) );
      fields.add( new HiddenField("fraudControlType"));
      fields.add( new CheckboxField("invoicePromptEnable","Invoice # Prompt On",false) );
      fields.add( new CheckboxField("passwordProtectEnable","Password Protect On",false) );
      fields.add( new CheckboxField("purchasingCardEnable","Purchasing Card Flag On",true) );
      fields.add( new CheckboxField("avsEnable","AVS On",true) );
      fields.add( new CheckboxField("tipTimeSale","Tip Time Sale",false) );
      fields.add( new CheckboxField("cardTruncDisable","Card Truncation Off on Receipts",false) );
      fields.add( new CheckboxField("callWaitingFlag","Call Waiting",false) );
      fields.add( new CheckboxField("cvv2Flag","V Code (CVV2)",false) );

      // customer service phone
      fields.add( new PhoneField( "customerServicePhone", true ) );

      // debit flags
      fields.add( new CheckboxField("debitCashBackEnable","Cash Back with Debit",false) );
      field = new CurrencyField("debitCashBackLimit",10,5,true);
      field.addValidation( new IfYesNotBlankValidation( fields.getField("debitCashBackEnable"), "Please enter a valid cash back limit" ) );
      field.addValidation( new IfNoBlankValidation( fields.getField("debitCashBackEnable"), "Please select cash back with debit checkbox or delete cash back limit" ) );
      fields.add(field);
      fields.add( new CheckboxField("debitSurchargeEnable","Debit Surcharge", false) );
      field = new CurrencyField("debitSurchargeAmount",10,5,true);
      field.addValidation( new IfYesNotBlankValidation( fields.getField("debitSurchargeEnable"), "Please enter a valid debit surcharge amount" ) );
      field.addValidation( new IfNoBlankValidation( fields.getField("debitSurchargeEnable"), "Please select debit surcharge checkbox or delete debit surcharge amount" ) );
      fields.add(field);

      // Verifone terminal features
      fields.add( new NumberField("reminderHour",2,2,true,0) );
      fields.add( new NumberField("reminderMin",2,2,true,0) );
      fields.add( new RadioButtonField( "reminderAmPm", AmPmRadioButtons, -1, true, "Please select either AM or PM for the totals check reminder close" ) );
      field = new CheckboxField( "reminderEnable", "Terminal Reminder to Check Totals", false );
      field.addValidation( new ActionTimeValidation( new Field[]
                                                    { fields.getField("reminderHour"),
                                                      fields.getField("reminderMin"),
                                                      fields.getField("reminderAmPm") },
                                                     TerminalReminderErrorMessages ) );
      fields.add(field);

      fields.add( new CheckboxField("tipOptionFlag", "Tip Option On", false) );
      fields.add( new CheckboxField("serverPromptEnable", "Clerk/Server Prompt On", false) );
      fields.add( new CheckboxField("fineDiningFlag","Fine Dining",false) );

      // phone training
      fields.add( new RadioButtonField( "trainingType", TrainingTypeRadioButtons, -1, true, "Please select a valid training type" ) );

      // terminal comm type (dial or ip)
      fields.add( new RadioButtonField( "commType", CommTypeRadioButtons, -1, true, "Please select a valid comm type" ) );

      // shipping information
      fields.add(new HiddenField      ("shippingAddressType"));

      fields.add(new Field            ("shippingBusinessName",  50,30,true));
      fields.add(new Field            ("shippingAddr1",         32,30,true));
      fields.add(new Field            ("shippingAddr2",         32,30,true));
      fields.add(new CityStateZipField("shippingCsz",           30,true));

      fields.add(new Field            ("shippingContactName",   32,30,true));
      fields.add(new PhoneField       ("shippingPhone",         true));
      fields.add(new DropDownField    ("shippingMethod",        new ShippingMethodTable(appSeqNum),false));

      fields.add(new TextareaField    ("shippingComments",      70,2,90,true ));

      fields.add(new ButtonField      ("actionGetBusinessAddr", "Business Address..."));
      fields.add(new ButtonField      ("actionGetMailingAddr",  "Mailing  Address..."));

      fields.getField("shippingAddr1").addValidation(
        new NotAllowedValidation(new String[]
          { "po box", "p.o. box", "pobox", "p.o.box", "p. o. box",
            "p o box","post office box", "postoffice box", "pob" },
            "Post office boxes not allowed"));

      // general equipment comments
      fields.add(new TextareaField    ("equipmentComments",     250,7,50,true));

      // payment plan
      fields.add(new NumberField      ("paymentPlanMonths", "Payment Plan Months", 2, 2, true, 0, 0, 12));

      // refund type (display only)
      fields.add(new Field("refundType", 30, 30, true));

      // set all fields to have class of formText
      addHtmlExtra("class=\"formText\"");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }

  public boolean isDialPayApp( )
  {
    boolean   dialPay       = false;
    int       posType       = -1;

    try
    {
      connect();

      long appSeqNum = fields.getField("appSeqNum").asLong();
      /*@lineinfo:generated-code*//*@lineinfo:1178^7*/

//  ************************************************************
//  #sql [Ctx] { select  pos_code
//          
//          from    merch_pos
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pos_code\n         \n        from    merch_pos\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.app.EquipmentBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   posType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1184^7*/

      // DialPay or GPS payment solution
      if( posType == mesConstants.APP_MPOS_DIAL_PAY ||
          posType == mesConstants.APP_MPOS_GPS )
      {
        dialPay = true;
      }
    }
    catch( java.sql.SQLException e )
    {
      // app does not exist for this app sequence number
      System.out.println("isDialPayApp(): " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( dialPay );
  }

  public boolean isTerminalApp( )
  {
    int       posType       = -1;
    boolean   retVal        = false;

    try
    {
      connect();

      long appSeqNum = fields.getField("appSeqNum").asLong();
      /*@lineinfo:generated-code*//*@lineinfo:1215^7*/

//  ************************************************************
//  #sql [Ctx] { select  pos_code
//          
//          from    merch_pos
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pos_code\n         \n        from    merch_pos\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.app.EquipmentBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   posType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1221^7*/

      // DialPay or GPS payment solution
      if( posType == mesConstants.APP_MPOS_DIAL_TERMINAL ||
          posType == mesConstants.APP_MPOS_WIRELESS_TERMINAL )
      {
        retVal = true;
      }
    }
    catch( java.sql.SQLException e )
    {
      // app does not exist for this app sequence number
      System.out.println("isTerminalApp(): " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }

  public boolean isPosPartnerApp( )
  {
    int       posType       = -1;
    boolean   retVal        = false;

    try
    {
      connect();

      long appSeqNum = fields.getField("appSeqNum").asLong();
      /*@lineinfo:generated-code*//*@lineinfo:1252^7*/

//  ************************************************************
//  #sql [Ctx] { select  pos_code
//          
//          from    merch_pos
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pos_code\n         \n        from    merch_pos\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.app.EquipmentBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   posType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1258^7*/

      // DialPay or GPS payment solution
      if( posType == mesConstants.APP_MPOS_POS_PARTNER ||
          posType == mesConstants.APP_MPOS_POS_PARTNER_2000 )
      {
        retVal = true;
      }
    }
    catch( java.sql.SQLException e )
    {
      // app does not exist for this app sequence number
      System.out.println("isPOSPartnerApp(): " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }

  /*
  ** protected void autoLoadAddress(int addressType)
  **
  ** Loads an address from the address table using a given address type.
  ** The address data is loaded into the shipping address fields.  Used by
  ** main page auto act method.
  */
  protected void autoLoadAddress(int addressType)
  {
    ResultSetIterator it = null;

    try
    {
      connect();

      long appSeqNum = fields.getField("appSeqNum").asLong();

      /*@lineinfo:generated-code*//*@lineinfo:1296^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(a.address_name,m.merch_business_name) address_name,
//                  a.address_line1,
//                  a.address_line2,
//                  a.address_city,
//                  a.countrystate_code,
//                  a.address_zip,
//                  a.address_phone
//          from    address a,
//                  merchant m
//          where   a.app_seq_num = :appSeqNum
//                  and a.addresstype_code = :addressType
//                  and a.app_seq_num = m.app_seq_num (+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(a.address_name,m.merch_business_name) address_name,\n                a.address_line1,\n                a.address_line2,\n                a.address_city,\n                a.countrystate_code,\n                a.address_zip,\n                a.address_phone\n        from    address a,\n                merchant m\n        where   a.app_seq_num =  :1 \n                and a.addresstype_code =  :2 \n                and a.app_seq_num = m.app_seq_num (+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,addressType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.app.EquipmentBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1310^7*/
      ResultSet rs = it.getResultSet();
      if (rs.next())
      {
        fields.setData("shippingBusinessName",rs.getString("address_name"));
        fields.setData("shippingAddr1",       rs.getString("address_line1"));
        fields.setData("shippingAddr2",       rs.getString("address_line2"));
        fields.setData("shippingCszCity",     rs.getString("address_city"));
        fields.setData("shippingCszState",    rs.getString("countrystate_code"));
        fields.setData("shippingCszZip",      rs.getString("address_zip"));
        fields.setData("shippingPhone",       rs.getString("address_phone"));
      }
    }
    catch(Exception e)
    {
      logEntry("loadShippingAddress()", e.toString());
      System.out.println("loadShippingAddress(): " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      cleanUp();
    }
  }

  /*
  ** protected boolean autoAct()
  **
  ** Handles auto action.  Currently the only auto action supported is invoked
  ** with "actionLoadAddress", which causes an address of the specified type
  ** to be loaded into the shipping address fields.
  **
  ** RETURNS: true.
  */
  protected boolean autoAct()
  {
    {
      try
      {
        connect();

        if (autoActionName.equals("actionGetBusinessAddr"))
        {
          autoLoadAddress(mesConstants.ADDR_TYPE_BUSINESS);
        }
        else if (autoActionName.equals("actionGetMailingAddr"))
        {
          autoLoadAddress(mesConstants.ADDR_TYPE_MAILING);
        }
      }
      catch (Exception e)
      {
        logEntry("autoAct(load shipping addr)",e.toString());
        System.out.println("autoAct(load shipping addr): " + e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    return true;
  }


  /*
  ** private int getEquipCountForCat(String prefix)
  **
  ** Sums up number of equipment items requested for a particular equipment
  ** category (rental + purchase + lease + owned).  The prefix is the prefix
  ** of the field names to check.
  **
  ** RETURNS: the total number of equipment items in a particular category.
  */
  private int getEquipCountForCat(String prefix)
  {
    int equipCount = 0;
    try
    {
      if (prefix != null)
      {
        equipCount += fields.getField(prefix + "RentalCount").asInteger();
        equipCount += fields.getField(prefix + "PurchaseCount").asInteger();
        equipCount += fields.getField(prefix + "LeaseCount").asInteger();
        equipCount += fields.getField(prefix + "OwnedCount").asInteger();
      }
    }
    catch (Exception e)
    {
      logEntry("getEquipCountForCat(" + prefix + ")",e.toString());
      System.out.println("getEquipCountForCat(" + prefix + "): "
        + e.toString());
    }
    return equipCount;
  }

  /*
  ** private boolean isEquipSelectedForCat(String prefix)
  **
  ** Determines if a model has been selected in a particular category of
  ** equipment (specified by prefix).
  **
  ** RETURNS: true if equipment category selection is present.
  */
  private boolean isEquipSelectedForCat(String prefix)
  {
    boolean catSelected = false;
    try
    {
      if (prefix != null)
      {
        catSelected = !fields.getData(prefix + "NeededModels").equals("");
      }
    }
    catch (Exception e)
    {
      logEntry("getEquipCountForCat(" + prefix + ")",e.toString());
      System.out.println("getEquipCountForCat(" + prefix + "): "
        + e.toString());
    }
    return catSelected;
  }

  /*
  ** private int getEquipCount()
  **
  ** Sums up number of equipment items requested for all categories (rental +
  ** purchase + lease + owned).
  **
  ** RETURNS: the total number of equipment items.
  */
  private int getEquipCount()
  {
    int equipCount = 0;
    for(int i = 0; i < EquipmentFieldNames.length; ++i)
    {
      String prefix = EquipmentFieldNames[i];
      if (prefix != null)
      {
        equipCount += getEquipCountForCat(prefix);
      }
    }
    return equipCount;
  }

  /*
  ** private boolean isEquipSelected()
  **
  ** Sees if any equipment category has been selected.
  **
  ** RETURNS: true if any equipment categories have been selected.
  */
  private boolean isEquipSelected()
  {
    for(int i = 0; i < EquipmentFieldNames.length; ++i)
    {
      String prefix = EquipmentFieldNames[i];
      if (prefix != null && isEquipSelectedForCat(prefix))
      {
        return true;
      }
    }
    return false;
  }

  /*
  ** private boolean isEquipIndicatedForCat(String prefix)
  **
  ** Determines if equipment category has been selected or some amount of
  ** equipment has been requested for that category.  The category is given
  ** with prefix.
  **
  ** RETURNS: true if any equipment in the category have been selected or
  **          that category count is greater than zero.
  */
  private boolean isEquipIndicatedForCat(String prefix)
  {
    return  isEquipSelectedForCat(prefix) ||
            getEquipCountForCat(prefix) > 0;
  }

  /*
  ** private boolean isEquipIndicated()
  **
  ** RETURNS: true if any equipment category has been selected or if any
  **          equipment quantity has been specified for a category.
  */
  private boolean isEquipIndicated()
  {
    for(int i = 0; i < EquipmentFieldNames.length; ++i)
    {
      String prefix = EquipmentFieldNames[i];
      if (prefix != null && isEquipIndicatedForCat(prefix))
      {
        return true;
      }
    }
    return false;
  }

  // array of flags indicating what card types are accepted
  private boolean[] acceptedCardTypes = null;

  /*
  ** private void loadAcceptedCardTypes()
  **
  ** Determines which card types the merchant accepts.  Acceptance flags
  ** are stored in the acceptedCardTypes array.
  */
  private void loadAcceptedCardTypes()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;

    try
    {
      long appSeqNum = fields.getField("appSeqNum").asInteger();

      connect();
      /*@lineinfo:generated-code*//*@lineinfo:1528^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code
//          from    merchpayoption
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code\n        from    merchpayoption\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.app.EquipmentBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1533^7*/
      rs = it.getResultSet();
      acceptedCardTypes = new boolean[mesConstants.APP_CT_COUNT];
      while (rs.next())
      {
        acceptedCardTypes[rs.getInt("cardtype_code")] = true;
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadAcceptedCardTypes()",e.toString());
      System.out.println(this.getClass().getName() + "::loadAcceptedCardTypes(): "
        + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  /*
  ** private boolean[] getAcceptedCardTypes()
  **
  ** RETURNS: the accepted card types array (loading it if it has not been
  ** already).
  */
  private boolean[] getAcceptedCardTypes()
  {
    if (acceptedCardTypes == null)
    {
      loadAcceptedCardTypes();
    }
    return acceptedCardTypes;
  }

  /*
  ** private boolean isAccepting(int cardType)
  **
  ** RETURNS: true if the indicated card type is accepted by the merchant.
  */
  private boolean isAccepting(int cardType)
  {
    try
    {
      return getAcceptedCardTypes()[cardType];
    }
    catch (Exception e) {}

    return false;
  }

  /*
  ** protected void validateFields()
  **
  ** Does any general validation that is not specific to any particular field.
  */
  protected void validateFields()
  {
    // at least one equipment item is required if payment solution is edc
    if (isPosType(mesConstants.POS_DIAL_TERMINAL) && !isEquipIndicated())
    {
      addError("Select at least one piece of equipment "
                + "(needed for EDC merchants)");
    }

    // pin pad equipment is required if debit with bin is accepted
    if (isAccepting(mesConstants.APP_CT_DEBIT) &&
        !isEquipIndicatedForCat("pinPad") &&
        !isEquipIndicatedForCat("termPinPad"))
    {
      addError("Pin pad equipment is required (needed for "
                + "pin debit acceptance)");
    }
  }

  /*************************************************************************
  **
  **   Load
  **
  **************************************************************************/

  /*
  ** protected void loadShippingAddress(long appSeqNum)
  **
  ** Loads the shipping address from the address table.
  */
  protected void loadShippingAddress(long appSeqNum)
  {
    ResultSetIterator it = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1629^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(address_name,'')      shipping_business_name,
//                  nvl(address_line1,'')     shipping_addr1,
//                  nvl(address_line2,'')     shipping_addr2,
//                  nvl(address_city,'')      shipping_csz_city,
//                  nvl(countrystate_code,'') shipping_csz_state,
//                  nvl(address_zip,'')       shipping_csz_zip,
//                  nvl(address_phone,'')     shipping_phone
//  
//          from    address
//  
//          where   app_seq_num = :appSeqNum
//                  and addresstype_code = :mesConstants.ADDR_TYPE_SHIPPING
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(address_name,'')      shipping_business_name,\n                nvl(address_line1,'')     shipping_addr1,\n                nvl(address_line2,'')     shipping_addr2,\n                nvl(address_city,'')      shipping_csz_city,\n                nvl(countrystate_code,'') shipping_csz_state,\n                nvl(address_zip,'')       shipping_csz_zip,\n                nvl(address_phone,'')     shipping_phone\n\n        from    address\n\n        where   app_seq_num =  :1 \n                and addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_SHIPPING);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.app.EquipmentBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1643^7*/
      setFields(it.getResultSet());
    }
    catch(Exception e)
    {
      logEntry("loadShippingAddress()", e.toString());
      System.out.println("loadShippingAddress(): " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  /*
  ** protected void loadEquipmentData(long appSeqNum)
  **
  ** Loads equipment data from the database.
  */
  protected void loadEquipmentData(long appSeqNum)
  {
    int               equipType             = -1;
    Field             field                 = null;
    int               fieldNameBaseLength   = 0;
    StringBuffer      fieldNameBuffer       = new StringBuffer();
    String            fieldPostfix          = null;
    ResultSetIterator it                    = null;
    ResultSet         resultSet             = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1674^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equiptype_code                as equip_type,
//                  equiplendtype_code            as lend_type,
//                  merchequip_equip_quantity     as equip_count,
//                  decode(prod_option_id,
//                         null,equip_model,
//                         equip_model || '*' || prod_option_id)  as product_id,
//                  payment_plan_months           as payment_plan_months
//          from    merchequipment      me
//          where   me.app_seq_num = :appSeqNum
//          order by me.equiptype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equiptype_code                as equip_type,\n                equiplendtype_code            as lend_type,\n                merchequip_equip_quantity     as equip_count,\n                decode(prod_option_id,\n                       null,equip_model,\n                       equip_model || '*' || prod_option_id)  as product_id,\n                payment_plan_months           as payment_plan_months\n        from    merchequipment      me\n        where   me.app_seq_num =  :1 \n        order by me.equiptype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.app.EquipmentBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1686^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        equipType = resultSet.getInt("equip_type");

        // reset the field name data
        fieldNameBuffer.setLength(0);
        fieldPostfix = null;

        switch( equipType )
        {
          case mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES:
            fieldNameBuffer.append("imprinterPlates");
            break;

//          case mesConstants.APP_EQUIP_TYPE_PERIPHERAL:
//          case mesConstants.APP_EQUIP_TYPE_TERMINAL:
//          case mesConstants.APP_EQUIP_TYPE_PRINTER:
//          case mesConstants.APP_EQUIP_TYPE_PINPAD:
//          case mesConstants.APP_EQUIP_TYPE_IMPRINTER:
//          case mesConstants.APP_EQUIP_TYPE_TERM_PRINTER:
//          case mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD:
//          case mesConstants.APP_EQUIP_TYPE_OTHERS:
//          case mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE:
          default:
            // add the base name and store the base length of the field
            fieldNameBuffer.append( EquipmentFieldNames[ equipType ] );
            fieldNameBaseLength = fieldNameBuffer.length();
            fieldPostfix = "NeededModels";    // assume needed

            switch( resultSet.getInt("lend_type" ) )
            {
              case mesConstants.APP_EQUIP_PURCHASE:
                fieldNameBuffer.append( "PurchaseCount" );

                // set payment plan field
                fields.getField("paymentPlanMonths").setData(resultSet.getString("payment_plan_months"));
                break;

              case mesConstants.APP_EQUIP_BUY_REFURBISHED:
                fieldNameBuffer.append( "RefurbCount" );

                // set payment plan field
                fields.getField("paymentPlanMonths").setData(resultSet.getString("payment_plan_months"));
                break;

              case mesConstants.APP_EQUIP_RENT:
                fieldNameBuffer.append( "RentalCount" );
                break;

              case mesConstants.APP_EQUIP_LEASE:
                fieldNameBuffer.append( "LeaseCount" );
                break;

              case mesConstants.APP_EQUIP_SWAP:
                fieldNameBuffer.append( "SwapCount" );
                break;

              case mesConstants.APP_EQUIP_OWNED:
                fieldNameBuffer.append( "OwnedCount" );
                fieldPostfix = "OwnedModels";     // switch to owned
                break;
            }
            break;
        }
        // extract the field data from the result set
        field = fields.getField( fieldNameBuffer.toString() );
        if ( field != null )
        {
          field.setData( resultSet.getString("equip_count") );
        }

        // setup the drop down box to point to the correct entry.
        if ( fieldPostfix != null )
        {
          // reset to just the field base name
          fieldNameBuffer.setLength( fieldNameBaseLength );

          // add the post fix to the base name
          fieldNameBuffer.append( fieldPostfix );

          // extract the field reference and set the contents if non null
          field = fields.getField( fieldNameBuffer.toString() );
          if ( field != null )
          {
            field.setData( resultSet.getString("product_id") );
          }
        }
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadEquipmentData()",e.toString());
      addError("loadEquipmentData: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }

  /*
  ** protected void loadFeatureData(long appSeqNum)
  **
  ** Loads terminal application feature data.
  */
  protected void loadFeatureData(long appSeqNum)
  {
    int                         index         = 0;
    ResultSetIterator           it            = null;
    ResultSet                   resultSet     = null;
    String                      temp          = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1805^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pf.access_code_flag                 as access_code_enable,
//                  pf.access_code                      as access_code,
//                  pf.auto_batch_close_flag            as auto_close_enable,
//                  pf.auto_batch_close_time            as auto_close_time,
//                  pf.header_line4_flag                as receipt_line4_enable,
//                  nvl(pf.header_line4, ' ')           as receipt_line4,
//                  pf.header_line5_flag                as receipt_line5_enable,
//                  pf.header_line5                     as receipt_line5,
//                  pf.footer_flag                      as receipt_footer_enable,
//                  footer                              as receipt_footer,
//                  pf.reset_ref_flag                   as reset_tran_num_enable,
//                  pf.invoice_prompt_flag              as invoice_prompt_enable,
//                  pf.fraud_control_flag               as fraud_control_enable,
//                  pf.fraud_control_option             as fraud_control_type,
//                  pf.password_protect_flag            as password_protect_enable,
//                  -- only used by CB&T derived app
//                  -- pf.phone_training_flag              as training_enable,
//                  pf.terminal_reminder_flag           as reminder_enable,
//                  pf.terminal_reminder_time           as reminder_time,
//                  pf.avs_flag                         as avs_enable,
//                  pf.phone_training                   as training_type,
//                  pf.cash_back_flag                   as debit_cash_back_enable,
//                  pf.cash_back_limit                  as debit_cash_back_limit,
//                  pf.purchasing_card_flag             as purchasing_card_enable,
//                  pf.card_truncation_flag             as card_trunc_disable,
//                  pf.debit_surcharge_flag             as debit_surcharge_enable,
//                  pf.debit_surcharge_amount           as debit_surcharge_amount,
//                  pf.customer_service_phone           as customer_service_phone,
//                  pf.tip_option_flag                  as tip_option_flag,
//                  pf.clerk_enabled_flag               as server_prompt_enable,
//                  pf.addresstype_code                 as shipping_address_type,
//                  pf.equipment_comment                as equipment_comments,
//                  pf.call_waiting_flag                as call_waiting_flag,
//                  pf.cvv2_flag                        as cvv2_flag,
//                  pf.shipping_comment                 as shipping_comments,
//                  pf.shipping_method                  as shipping_method,
//                  pf.fine_dining_flag                 as fine_dining_flag,
//                  pf.manualclose_flag                 as manualclose_flag,
//                  pf.shipping_contact_name            as shipping_contact_name,
//                  nvl(pf.term_comm_type,:mesConstants.TERM_COMM_TYPE_DIAL) as comm_type,
//                  pf.tip_time_sale                    as tip_time_sale
//                  -- where are these used?
//                  --pf.no_printer_needed,
//                  --pf.no_printer_owned
//          from    pos_features      pf
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pf.access_code_flag                 as access_code_enable,\n                pf.access_code                      as access_code,\n                pf.auto_batch_close_flag            as auto_close_enable,\n                pf.auto_batch_close_time            as auto_close_time,\n                pf.header_line4_flag                as receipt_line4_enable,\n                nvl(pf.header_line4, ' ')           as receipt_line4,\n                pf.header_line5_flag                as receipt_line5_enable,\n                pf.header_line5                     as receipt_line5,\n                pf.footer_flag                      as receipt_footer_enable,\n                footer                              as receipt_footer,\n                pf.reset_ref_flag                   as reset_tran_num_enable,\n                pf.invoice_prompt_flag              as invoice_prompt_enable,\n                pf.fraud_control_flag               as fraud_control_enable,\n                pf.fraud_control_option             as fraud_control_type,\n                pf.password_protect_flag            as password_protect_enable,\n                -- only used by CB&T derived app\n                -- pf.phone_training_flag              as training_enable,\n                pf.terminal_reminder_flag           as reminder_enable,\n                pf.terminal_reminder_time           as reminder_time,\n                pf.avs_flag                         as avs_enable,\n                pf.phone_training                   as training_type,\n                pf.cash_back_flag                   as debit_cash_back_enable,\n                pf.cash_back_limit                  as debit_cash_back_limit,\n                pf.purchasing_card_flag             as purchasing_card_enable,\n                pf.card_truncation_flag             as card_trunc_disable,\n                pf.debit_surcharge_flag             as debit_surcharge_enable,\n                pf.debit_surcharge_amount           as debit_surcharge_amount,\n                pf.customer_service_phone           as customer_service_phone,\n                pf.tip_option_flag                  as tip_option_flag,\n                pf.clerk_enabled_flag               as server_prompt_enable,\n                pf.addresstype_code                 as shipping_address_type,\n                pf.equipment_comment                as equipment_comments,\n                pf.call_waiting_flag                as call_waiting_flag,\n                pf.cvv2_flag                        as cvv2_flag,\n                pf.shipping_comment                 as shipping_comments,\n                pf.shipping_method                  as shipping_method,\n                pf.fine_dining_flag                 as fine_dining_flag,\n                pf.manualclose_flag                 as manualclose_flag,\n                pf.shipping_contact_name            as shipping_contact_name,\n                nvl(pf.term_comm_type, :1 ) as comm_type,\n                pf.tip_time_sale                    as tip_time_sale\n                -- where are these used:2\n                --pf.no_printer_needed,\n                --pf.no_printer_owned\n        from    pos_features      pf\n        where   app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.TERM_COMM_TYPE_DIAL);
   __sJT_st.setLong(2,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.app.EquipmentBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1853^7*/
      resultSet = it.getResultSet();
      if ( resultSet.next() )
      {
        setFields(resultSet,false);

        // extract the time fields
        temp = resultSet.getString("auto_close_time");
        if (temp != null)
        {
          if( temp.length() >= 6 )
          {
            index = temp.indexOf(':');
            fields.getField("autoCloseHour").setData( temp.substring(0,index) );
            fields.getField("autoCloseMin").setData( temp.substring((index+1),(index+3)) );
            fields.getField("autoCloseAmPm").setData( temp.substring((index+3)) );
          }
        }

        temp = resultSet.getString("reminder_time");
        if (temp != null)
        {
          if( temp.length() >= 6 )
          {
            index = temp.indexOf(':');
            fields.getField("reminderHour").setData( temp.substring(0,index) );
            fields.getField("reminderMin").setData( temp.substring((index+1),(index+3)) );
            fields.getField("reminderAmPm").setData( temp.substring((index+3)) );
          }
        }
      }
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:1886^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  terminal_application                as terminal_app
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  terminal_application                as terminal_app\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.app.EquipmentBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1891^7*/

      resultSet = it.getResultSet();
      setFields(resultSet);

      resultSet.close();
      it.close();

      // get refund policy so it can be shown on the equipment page if desired
      /*@lineinfo:generated-code*//*@lineinfo:1900^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(rt.refundtype_desc,'None Selected')  refund_type
//          from    merchant mr,
//                  refundtype rt
//          where   mr.app_seq_num = :appSeqNum and
//                  mr.refundtype_code = rt.refundtype_code(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(rt.refundtype_desc,'None Selected')  refund_type\n        from    merchant mr,\n                refundtype rt\n        where   mr.app_seq_num =  :1  and\n                mr.refundtype_code = rt.refundtype_code(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.app.EquipmentBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1907^7*/

      resultSet = it.getResultSet();
      setFields(resultSet);

      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadFeatureData()",e.toString());
      System.out.println("loadFeatureData(): " + e.toString());
    }
    finally
    {
      try{ resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  /*
  ** protected void loadLeaseData(long appSeqNum)
  **
  ** Loads lease data.
  */
  protected void loadLeaseData(long appSeqNum)
  {
    ResultSetIterator it              = null;
    ResultSet         resultSet       = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1939^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ml.lease_company_code           as lease_company,
//                  ml.lease_account_num            as lease_acct_num,
//                  ml.lease_auth_num               as lease_auth_num,
//                  ml.lease_months_num             as lease_months,
//                  ml.lease_monthly_amount         as lease_monthly_amount,
//                  ml.lease_funding_amount         as lease_funding_amount
//          from    merchequiplease      ml
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ml.lease_company_code           as lease_company,\n                ml.lease_account_num            as lease_acct_num,\n                ml.lease_auth_num               as lease_auth_num,\n                ml.lease_months_num             as lease_months,\n                ml.lease_monthly_amount         as lease_monthly_amount,\n                ml.lease_funding_amount         as lease_funding_amount\n        from    merchequiplease      ml\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"23com.mes.app.EquipmentBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1949^7*/
      resultSet = it.getResultSet();
      if( resultSet.next() )
      {
        setFields(resultSet,false);
      }
      resultSet.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("loadLeaseData()",e.toString());
      addError("loadLeaseData: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  /*
  ** protected void loadAppSpecData()
  **
  ** Super-classes should override this method to load application
  ** specific data.
  */
  protected void loadAppSpecData()
  {
  }

  /*
  ** protected boolean loadAppData()
  **
  ** Loads page data from database.
  */
  protected boolean loadAppData()
  {
    boolean loadOk = false;
    try
    {
      connect();

      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        loadShippingAddress(appSeqNum);
        loadEquipmentData(appSeqNum);
        loadFeatureData(appSeqNum);
        loadLeaseData(appSeqNum);

        loadAppSpecData();
      }
      loadOk = true;
    }
    catch(Exception e)
    {
      logEntry("loadAppData()",e.toString());
      System.out.println(this.getClass().getName() + "::loadAppData(): "
        + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return loadOk;
  }

  /*************************************************************************
  **
  **   Submit
  **
  **************************************************************************/

  /*
  ** protected void submitEquipmentData(long appSeqNum)
  **
  ** Stores equipment selection data.
  */
  protected void submitEquipmentData(long appSeqNum)
  {
    int                   equipCount      = 0;
    String                equipModel      = null;
    Field                 field           = null;
    String                fieldName       = null;
    int                   index           = 0;
    String                productId       = null;
    String                temp            = null;

    try
    {
      // delete all the existing equipment except PC products
      // and imprinter plates, these are handled on first page
      /*@lineinfo:generated-code*//*@lineinfo:2042^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchequipment
//          where   app_seq_num = :appSeqNum
//                  and equip_model != 'PCPS'
//                  and equip_model != 'IPPL'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merchequipment\n        where   app_seq_num =  :1 \n                and equip_model != 'PCPS'\n                and equip_model != 'IPPL'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2049^7*/

      // add new equipment
      for ( int eType = 0; eType < EquipmentFieldNames.length; ++eType )
      {
        // if there is a valid field for this type of equipment,
        // then insert the data if the user selected something
        // from the drop down list of models.
        if ( EquipmentFieldNames[eType] != null )
        {
          for( int appSection = 0; appSection < 2; ++appSection )
          {
            // extract the either the needed or the owned field based
            // on which section of the application data we are currently
            // processing.
            field = fields.getField( EquipmentFieldNames[eType] + (( appSection == 0 ) ? "NeededModels" : "OwnedModels") );

            // if the field is non-null and it is not blank
            // then they have selected a piece of equipment,
            // validation will insure that at lease one of
            // the count fields has a valid value > 0.
            if ( field != null && !field.isBlank() )
            {
              // extract the model name from the field.  if necessary
              // separate the model from the product id
              temp = field.getData();
              if ( (index = temp.indexOf("*")) != -1 )
              {
                // separate the model from the product code
                equipModel  = temp.substring(0,index);
                productId   = temp.substring(index+1);
              }
              else
              {
                equipModel  = temp;
                productId   = null;
              }
              for( int lType = 0; lType < mesConstants.APP_EQUIP_COUNT; ++lType )
              {
                // if we are processing the needed section then skip the
                // owned.  if we are processing the owned section skip
                // everything but the owned.
                if( (appSection == 0 && lType == mesConstants.APP_EQUIP_OWNED) ||
                    (appSection == 1 && lType != mesConstants.APP_EQUIP_OWNED) )
                {
                  continue;
                }
                switch( lType )
                {
                  case mesConstants.APP_EQUIP_PURCHASE:
                    fieldName = EquipmentFieldNames[eType] + "PurchaseCount";
                    break;
                  case mesConstants.APP_EQUIP_RENT:
                    fieldName = EquipmentFieldNames[eType] + "RentalCount";
                    break;
                  case mesConstants.APP_EQUIP_LEASE:
                    fieldName = EquipmentFieldNames[eType] + "LeaseCount";
                    break;
                  case mesConstants.APP_EQUIP_BUY_REFURBISHED:
                    fieldName = EquipmentFieldNames[eType] + "RefurbCount";
                    break;
                  case mesConstants.APP_EQUIP_OWNED:
                    fieldName = EquipmentFieldNames[eType] + "OwnedCount";
                    break;
                  case mesConstants.APP_EQUIP_SWAP:
                    fieldName = EquipmentFieldNames[eType] + "SwapCount";
                    break;
                  default:
                    fieldName = "";   // reset the field name
                    break;
                }
                field = fields.getField(fieldName);

                // found the field
                if ( field != null && !field.isBlank() )
                {
                  equipCount = field.asInteger();
                  if ( equipCount > 0 )
                  {
                    /*@lineinfo:generated-code*//*@lineinfo:2128^21*/

//  ************************************************************
//  #sql [Ctx] { insert into merchequipment
//                        (
//                          app_seq_num,
//                          equiptype_code,
//                          equiplendtype_code,
//                          merchequip_amount,
//                          equip_model,
//                          merchequip_equip_quantity,
//                          prod_option_id,
//                          quantity_deployed,
//                          payment_plan_months
//                        )
//                        values
//                        (
//                          :appSeqNum,
//                          :eType,
//                          :lType,
//                          0,
//                          :equipModel,
//                          :equipCount,
//                          :productId,
//                          0,
//                          :fields.getField("paymentPlanMonths").getData()
//                        )
//                       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2026 = fields.getField("paymentPlanMonths").getData();
   String theSqlTS = "insert into merchequipment\n                      (\n                        app_seq_num,\n                        equiptype_code,\n                        equiplendtype_code,\n                        merchequip_amount,\n                        equip_model,\n                        merchequip_equip_quantity,\n                        prod_option_id,\n                        quantity_deployed,\n                        payment_plan_months\n                      )\n                      values\n                      (\n                         :1 ,\n                         :2 ,\n                         :3 ,\n                        0,\n                         :4 ,\n                         :5 ,\n                         :6 ,\n                        0,\n                         :7 \n                      )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,eType);
   __sJT_st.setInt(3,lType);
   __sJT_st.setString(4,equipModel);
   __sJT_st.setInt(5,equipCount);
   __sJT_st.setString(6,productId);
   __sJT_st.setString(7,__sJT_2026);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2154^21*/
                  }
                }
              }
            }     // end if equipment selection field is null or blank
          }     // end for loop through app sections (0 = needed, 1 = owned)
        }     // end if EquipmentFieldName[etype] != null
      }     // end for loop through equipment types
    }
    catch( Exception e )
    {
      logEntry("submitEquipmentData()",e.toString());
      System.out.println("submitEquipmentData(): " + e.toString());
    }
  }

  /*
  ** protected void submitFeatureData(long appSeqNum)
  **
  ** Stores terminal feature options in the database.
  */
  protected void submitFeatureData(long appSeqNum)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2179^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    pos_features
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    pos_features\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2184^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2186^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     terminal_application = :fields.getData("terminalApp")
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2027 = fields.getData("terminalApp");
   String theSqlTS = "update  merchant\n        set     terminal_application =  :1 \n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2027);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2191^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2193^7*/

//  ************************************************************
//  #sql [Ctx] { insert into pos_features
//          (
//            app_seq_num,
//            access_code_flag,
//            access_code,
//            auto_batch_close_flag,
//            auto_batch_close_time,
//            header_line4_flag,
//            header_line4,
//            header_line5_flag,
//            header_line5,
//            footer_flag,
//            footer,
//            reset_ref_flag,
//            invoice_prompt_flag,
//            fraud_control_flag,
//            fraud_control_option,
//            password_protect_flag,
//            phone_training_flag,
//            terminal_reminder_flag,
//            terminal_reminder_time,
//            tip_option_flag,
//            clerk_enabled_flag,
//            equipment_comment,
//            addresstype_code,
//            no_printer_needed,
//            no_printer_owned,
//            avs_flag,
//            phone_training,
//            cash_back_flag,
//            cash_back_limit,
//            purchasing_card_flag,
//            card_truncation_flag,
//            debit_surcharge_flag,
//            debit_surcharge_amount,
//            customer_service_phone,
//            call_waiting_flag,
//            cvv2_flag,
//            shipping_contact_name,
//            shipping_method,
//            shipping_comment,
//            fine_dining_flag,
//            tip_time_sale,
//            term_comm_type
//          )
//          values
//          (
//            :appSeqNum,
//            :fields.getField("accessCodeEnable").getData().toUpperCase(),
//            :fields.getField("accessCode").getData(),
//            :fields.getField("autoCloseEnable").getData().toUpperCase(),
//            :decodeActionTime("autoClose"),
//            :fields.getField("receiptLine4Enable").getData().toUpperCase(),
//            :fields.getField("receiptLine4").getData(),
//            :fields.getField("receiptLine5Enable").getData().toUpperCase(),
//            :fields.getField("receiptLine5").getData(),
//            :fields.getField("receiptFooterEnable").getData().toUpperCase(),
//            :fields.getField("receiptFooter").getData(),
//            'Y',      -- resetTranNumEnable always on
//            :fields.getField("invoicePromptEnable").getData().toUpperCase(),
//            'Y',      -- fraudControlEnable always on
//            :fields.getField("fraudControlType").getData(),
//            null,     -- passwordProtectEnable?
//            null,     -- phone training?
//            null,     -- reminderEnable never used
//            null,     -- reminder never used
//            :fields.getField("tipOptionFlag").getData().toUpperCase(),
//            :fields.getField("serverPromptEnable").getData().toUpperCase(),
//            :fields.getField("equipmentComments").getData(),
//            :fields.getField("shippingAddressType").getData(),
//            'N',      -- no printer needed?
//            'N',      -- no printer owned?
//            :fields.getField("avsEnable").getData().toUpperCase(),
//            :fields.getField("trainingType").getData(),
//            :fields.getField("debitCashBackEnable").getData().toUpperCase(),
//            :fields.getField("debitCashBackLimit").getData(),
//            :fields.getField("purchasingCardEnable").getData().toUpperCase(),
//            :fields.getField("cardTruncDisable").getData(),
//            :fields.getField("debitSurchargeEnable").getData().toUpperCase(),
//            :fields.getField("debitSurchargeAmount").getData(),
//            :fields.getField("customerServicePhone").getData(),
//            :fields.getField("callWaitingFlag").getData(),
//            :fields.getField("cvv2Flag").getData(),
//            :fields.getField("shippingContactName").getData(),
//            :fields.getField("shippingMethod").getData(),
//            :fields.getField("shippingComments").getData(),
//            null,     -- fineDiningFlag
//            :fields.getData("tipTimeSale"),
//            :fields.getData("commType")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2028 = fields.getField("accessCodeEnable").getData().toUpperCase();
 String __sJT_2029 = fields.getField("accessCode").getData();
 String __sJT_2030 = fields.getField("autoCloseEnable").getData().toUpperCase();
 String __sJT_2031 = decodeActionTime("autoClose");
 String __sJT_2032 = fields.getField("receiptLine4Enable").getData().toUpperCase();
 String __sJT_2033 = fields.getField("receiptLine4").getData();
 String __sJT_2034 = fields.getField("receiptLine5Enable").getData().toUpperCase();
 String __sJT_2035 = fields.getField("receiptLine5").getData();
 String __sJT_2036 = fields.getField("receiptFooterEnable").getData().toUpperCase();
 String __sJT_2037 = fields.getField("receiptFooter").getData();
 String __sJT_2038 = fields.getField("invoicePromptEnable").getData().toUpperCase();
 String __sJT_2039 = fields.getField("fraudControlType").getData();
 String __sJT_2040 = fields.getField("tipOptionFlag").getData().toUpperCase();
 String __sJT_2041 = fields.getField("serverPromptEnable").getData().toUpperCase();
 String __sJT_2042 = fields.getField("equipmentComments").getData();
 String __sJT_2043 = fields.getField("shippingAddressType").getData();
 String __sJT_2044 = fields.getField("avsEnable").getData().toUpperCase();
 String __sJT_2045 = fields.getField("trainingType").getData();
 String __sJT_2046 = fields.getField("debitCashBackEnable").getData().toUpperCase();
 String __sJT_2047 = fields.getField("debitCashBackLimit").getData();
 String __sJT_2048 = fields.getField("purchasingCardEnable").getData().toUpperCase();
 String __sJT_2049 = fields.getField("cardTruncDisable").getData();
 String __sJT_2050 = fields.getField("debitSurchargeEnable").getData().toUpperCase();
 String __sJT_2051 = fields.getField("debitSurchargeAmount").getData();
 String __sJT_2052 = fields.getField("customerServicePhone").getData();
 String __sJT_2053 = fields.getField("callWaitingFlag").getData();
 String __sJT_2054 = fields.getField("cvv2Flag").getData();
 String __sJT_2055 = fields.getField("shippingContactName").getData();
 String __sJT_2056 = fields.getField("shippingMethod").getData();
 String __sJT_2057 = fields.getField("shippingComments").getData();
 String __sJT_2058 = fields.getData("tipTimeSale");
 String __sJT_2059 = fields.getData("commType");
   String theSqlTS = "insert into pos_features\n        (\n          app_seq_num,\n          access_code_flag,\n          access_code,\n          auto_batch_close_flag,\n          auto_batch_close_time,\n          header_line4_flag,\n          header_line4,\n          header_line5_flag,\n          header_line5,\n          footer_flag,\n          footer,\n          reset_ref_flag,\n          invoice_prompt_flag,\n          fraud_control_flag,\n          fraud_control_option,\n          password_protect_flag,\n          phone_training_flag,\n          terminal_reminder_flag,\n          terminal_reminder_time,\n          tip_option_flag,\n          clerk_enabled_flag,\n          equipment_comment,\n          addresstype_code,\n          no_printer_needed,\n          no_printer_owned,\n          avs_flag,\n          phone_training,\n          cash_back_flag,\n          cash_back_limit,\n          purchasing_card_flag,\n          card_truncation_flag,\n          debit_surcharge_flag,\n          debit_surcharge_amount,\n          customer_service_phone,\n          call_waiting_flag,\n          cvv2_flag,\n          shipping_contact_name,\n          shipping_method,\n          shipping_comment,\n          fine_dining_flag,\n          tip_time_sale,\n          term_comm_type\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n          'Y',      -- resetTranNumEnable always on\n           :12 ,\n          'Y',      -- fraudControlEnable always on\n           :13 ,\n          null,     -- passwordProtectEnable:14\n          null,     -- phone training:15\n          null,     -- reminderEnable never used\n          null,     -- reminder never used\n           :16 ,\n           :17 ,\n           :18 ,\n           :19 ,\n          'N',      -- no printer needed:20\n          'N',      -- no printer owned:21\n           :22 ,\n           :23 ,\n           :24 ,\n           :25 ,\n           :26 ,\n           :27 ,\n           :28 ,\n           :29 ,\n           :30 ,\n           :31 ,\n           :32 ,\n           :33 ,\n           :34 ,\n           :35 ,\n          null,     -- fineDiningFlag\n           :36 ,\n           :37 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_2028);
   __sJT_st.setString(3,__sJT_2029);
   __sJT_st.setString(4,__sJT_2030);
   __sJT_st.setString(5,__sJT_2031);
   __sJT_st.setString(6,__sJT_2032);
   __sJT_st.setString(7,__sJT_2033);
   __sJT_st.setString(8,__sJT_2034);
   __sJT_st.setString(9,__sJT_2035);
   __sJT_st.setString(10,__sJT_2036);
   __sJT_st.setString(11,__sJT_2037);
   __sJT_st.setString(12,__sJT_2038);
   __sJT_st.setString(13,__sJT_2039);
   __sJT_st.setString(14,__sJT_2040);
   __sJT_st.setString(15,__sJT_2041);
   __sJT_st.setString(16,__sJT_2042);
   __sJT_st.setString(17,__sJT_2043);
   __sJT_st.setString(18,__sJT_2044);
   __sJT_st.setString(19,__sJT_2045);
   __sJT_st.setString(20,__sJT_2046);
   __sJT_st.setString(21,__sJT_2047);
   __sJT_st.setString(22,__sJT_2048);
   __sJT_st.setString(23,__sJT_2049);
   __sJT_st.setString(24,__sJT_2050);
   __sJT_st.setString(25,__sJT_2051);
   __sJT_st.setString(26,__sJT_2052);
   __sJT_st.setString(27,__sJT_2053);
   __sJT_st.setString(28,__sJT_2054);
   __sJT_st.setString(29,__sJT_2055);
   __sJT_st.setString(30,__sJT_2056);
   __sJT_st.setString(31,__sJT_2057);
   __sJT_st.setString(32,__sJT_2058);
   __sJT_st.setString(33,__sJT_2059);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2285^7*/
    }
    catch(Exception e)
    {
      logEntry("submitFeatureData()", e.toString());
      System.out.println("submitFeatureData(): " + e.toString());
    }
  }

  /*
  ** protected void submitLeaseData(long appSeqNum)
  **
  ** Stores lease information in the database.
  */
  protected void submitLeaseData(long appSeqNum)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2303^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchequiplease
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merchequiplease\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2308^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2310^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchequiplease
//          (
//            app_seq_num,
//            lease_company_code,
//            lease_account_num,
//            lease_auth_num,
//            lease_months_num,
//            lease_monthly_amount,
//            lease_funding_amount
//          )
//          values
//          (
//            :appSeqNum,
//            :fields.getField("leaseCompany").getData(),
//            :fields.getField("leaseAcctNum").getData(),
//            :fields.getField("leaseAuthNum").getData(),
//            :fields.getField("leaseMonths").getData(),
//            :fields.getField("leaseMonthlyAmount").getData(),
//            :fields.getField("leaseFundingAmount").getData()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2060 = fields.getField("leaseCompany").getData();
 String __sJT_2061 = fields.getField("leaseAcctNum").getData();
 String __sJT_2062 = fields.getField("leaseAuthNum").getData();
 String __sJT_2063 = fields.getField("leaseMonths").getData();
 String __sJT_2064 = fields.getField("leaseMonthlyAmount").getData();
 String __sJT_2065 = fields.getField("leaseFundingAmount").getData();
   String theSqlTS = "insert into merchequiplease\n        (\n          app_seq_num,\n          lease_company_code,\n          lease_account_num,\n          lease_auth_num,\n          lease_months_num,\n          lease_monthly_amount,\n          lease_funding_amount\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"30com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_2060);
   __sJT_st.setString(3,__sJT_2061);
   __sJT_st.setString(4,__sJT_2062);
   __sJT_st.setString(5,__sJT_2063);
   __sJT_st.setString(6,__sJT_2064);
   __sJT_st.setString(7,__sJT_2065);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2332^7*/
    }
    catch (Exception e)
    {
      logEntry("submitLeaseData()", e.toString());
      System.out.println("submitLeaseData: " + e.toString());
    }
  }

  /*
  ** protected void submitShippingAddress(long appSeqNum)
  **
  ** Loads whatever address is stored in the address table of the type ship-
  ** ping address (unlike the load address method which can be used to load
  ** any type of address).
  */
  protected void submitShippingAddress(long appSeqNum)
  {
    try
    {
      // clear prior address record if it exists
      /*@lineinfo:generated-code*//*@lineinfo:2353^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    address
//          where   app_seq_num = :appSeqNum and
//                  addresstype_code = :mesConstants.ADDR_TYPE_SHIPPING
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    address\n        where   app_seq_num =  :1  and\n                addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_SHIPPING);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2359^7*/

      // insert a new shipping address
      /*@lineinfo:generated-code*//*@lineinfo:2362^7*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//          (
//            app_seq_num,
//            addresstype_code,
//            address_name,
//            address_line1,
//            address_line2,
//            address_city,
//            countrystate_code,
//            address_zip,
//            address_phone
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.ADDR_TYPE_SHIPPING,
//            :fields.getField("shippingBusinessName").getData(),
//            :fields.getField("shippingAddr1").getData(),
//            :fields.getField("shippingAddr2").getData(),
//            :fields.getField("shippingCszCity").getData(),
//            :fields.getField("shippingCszState").getData(),
//            :fields.getField("shippingCszZip").getData(),
//            :fields.getField("shippingPhone").getData()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2066 = fields.getField("shippingBusinessName").getData();
 String __sJT_2067 = fields.getField("shippingAddr1").getData();
 String __sJT_2068 = fields.getField("shippingAddr2").getData();
 String __sJT_2069 = fields.getField("shippingCszCity").getData();
 String __sJT_2070 = fields.getField("shippingCszState").getData();
 String __sJT_2071 = fields.getField("shippingCszZip").getData();
 String __sJT_2072 = fields.getField("shippingPhone").getData();
   String theSqlTS = "insert into address\n        (\n          app_seq_num,\n          addresstype_code,\n          address_name,\n          address_line1,\n          address_line2,\n          address_city,\n          countrystate_code,\n          address_zip,\n          address_phone\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"32com.mes.app.EquipmentBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_SHIPPING);
   __sJT_st.setString(3,__sJT_2066);
   __sJT_st.setString(4,__sJT_2067);
   __sJT_st.setString(5,__sJT_2068);
   __sJT_st.setString(6,__sJT_2069);
   __sJT_st.setString(7,__sJT_2070);
   __sJT_st.setString(8,__sJT_2071);
   __sJT_st.setString(9,__sJT_2072);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2388^7*/
    }
    catch(Exception e)
    {
      logEntry("submitShippingAddress()", e.toString());
      System.out.println("submitShippingAddress(): " + e.toString());
    }
  }

  /*
  ** protected void storeAppSpecData()
  **
  ** Superclasses should override this method to store application specific
  ** data.
  */
  protected void storeAppSpecData()
  {
  }

  /*
  ** protected boolean submitAppData()
  **
  ** Submits page data to the database.
  **
  ** RETURNS: true if all data is submitted successfully, else false.
  */
  protected boolean submitAppData()
  {
    boolean submitOk = false;
    try
    {
      connect();
      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      log.debug("Submitting App Data: " + appSeqNum);
      if (appSeqNum != 0L)
      {
        // only allow equipment data changes if the app has not yet been completed
        submitEquipmentData(appSeqNum);
        submitFeatureData(appSeqNum);
        submitLeaseData(appSeqNum);
        submitShippingAddress(appSeqNum);

        storeAppSpecData();
      }
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitAppData()",e.toString());
      System.out.println(this.getClass().getName() + "::submitAppData(): "
        + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return submitOk;
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);

    try
    {
      // default comm type to IP if virtual terminal
      if(isPosType(mesConstants.POS_VIRTUAL_TERMINAL))
      {
        fields.setData("commType", Integer.toString(mesConstants.TERM_COMM_TYPE_IP));
      }
    }
    catch(Exception e)
    {
      logEntry("postHandleRequest()", e.toString());
    }
  }
}/*@lineinfo:generated-code*/