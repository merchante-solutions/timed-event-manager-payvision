/*@lineinfo:filename=CharlestonApp*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.app;

import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.ops.InsertCreditQueue;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;

public class CharlestonApp extends AppBase
{
  static Logger log = Logger.getLogger(CharlestonApp.class);
  
  public long appSeqNum       = 0L;
  public long appControlNum   = 0L;
  public long templateSeqNum  = 246939L;
  
  private String    dbaName = "";
  private String    ddaNum = "";
  private String    taxId = "";
  private String    transitRouting = "";
  
  {
    appType = mesConstants.APP_TYPE_MESC;
    curScreenId = 1;
  }
  
  public CharlestonApp()
  {
    super();
  }
  
  public CharlestonApp(String dbaName, String ddaNum, String taxId, String transitRouting)
  {
    super();
    
    if(dbaName.length() > 25)
    {
      this.dbaName      = dbaName.substring(0, 25);
    }
    else
    {
      this.dbaName      = dbaName;
    }
    this.ddaNum         = ddaNum;
    this.taxId          = taxId;
    this.transitRouting = transitRouting;
  }
  
  protected void createNewApp()
  {
    super.createNewApp(null);
    
    try
    {
      appSeq.setAppSeqNum(fields.getField("appSeqNum").asLong());
      appSeqNum = fields.getField("appSeqNum").asLong();
      appControlNum = appSeq.getControlNum();
      log.debug("appSeqNum = " + appSeqNum + ", controlNum = " + appControlNum);
      appSeq.getFirstScreen().markAsComplete();
      appSeq.getLastScreen().markAsComplete();
    }
    catch(Exception e)
    {
      logEntry("createNewApp()", e.toString());
    }
  }
  
  private void initializeApp()
  {
    try
    {
      // initialize user object
      log.debug("creating new user bean");
      user = new UserBean();
      log.debug("force validating user");
      user.forceValidate("pkite");
      
      // create fields
      log.debug("creating fields");
      createFields(null);
      
      log.debug("post handle request");
      super.postHandleRequest(null);
      
      log.debug("creating new app");
      createNewApp();
    }
    catch(Exception e)
    {
      logEntry("initializeApp()", e.toString());
    }
  }
  
  private void createMerchant()
  {
    try
    {
      // delete merchant record
      /*@lineinfo:generated-code*//*@lineinfo:106^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.CharlestonApp",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:111^7*/
      
      // insert new record with values from existing record
      /*@lineinfo:generated-code*//*@lineinfo:114^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant
//          (
//            app_seq_num,
//            merc_cntrl_number,
//            asso_number,
//            sic_code,
//            loctype_code,
//            refundtype_code,
//            bustype_code,
//            industype_code,
//            merch_years_at_loc,
//            merch_num_of_locations,
//            merch_business_name,
//            merch_business_establ_month,
//            merch_business_establ_year,
//            merch_legal_name,
//            merch_federal_tax_id,
//            merch_month_tot_proj_sales,
//            merch_month_visa_mc_sales,
//            merch_average_cc_tran,
//            merch_prior_cc_accp_flag,
//            merch_cc_acct_term_flag,
//            merch_mail_phone_sales,
//            merch_mcc,
//            merch_pos_add_ind,
//            merch_met_table_number,
//            merch_invg_code,
//            merch_deposit_flag,
//            merch_dly_excpt_flag,
//            merch_fraud_flag,
//            merch_visa_cps_ind,
//            merch_visa_psirf_flag,
//            merch_visa_eps_flag,
//            merch_mc_merit_ind,
//            merch_tax_disc_ind,
//            merch_mc_smkt_flag,
//            merch_visa_eirf_flag,
//            merch_mc_premier_flag,
//            merch_mc_petrocat_flag,
//            merch_mc_warehouse_flag,
//            merch_visainc_status_ind,
//            merch_dly_ach_flag,
//            merch_vrs_flag,
//            merch_stat_hold_flag,
//            merch_bank_number,
//            merch_stat_print_flag,
//            merch_edc_flag,
//            merch_paper_draft_flag,
//            merch_interchg_dollar_flag,
//            merch_interchg_number_flag,
//            merch_amex_pcid_num,
//            merch_application_type,
//            merch_prior_statements,
//            merch_busgoodserv_descr,
//            merch_gender,
//            app_sic_code,
//            merch_dly_discount_flag,
//            merch_basis_pt_increase,
//            seasonal_flag,
//            vmc_accept_types
//          )
//          select  :appSeqNum,
//                  :appControlNum,
//                  asso_number,
//                  sic_code,
//                  loctype_code,
//                  refundtype_code,
//                  bustype_code,
//                  industype_code,
//                  merch_years_at_loc,
//                  merch_num_of_locations,
//                  :dbaName,
//                  merch_business_establ_month,
//                  merch_business_establ_year,
//                  merch_legal_name,
//                  :taxId,
//                  merch_month_tot_proj_sales,
//                  merch_month_visa_mc_sales,
//                  merch_average_cc_tran,
//                  merch_prior_cc_accp_flag,
//                  merch_cc_acct_term_flag,
//                  merch_mail_phone_sales,
//                  merch_mcc,
//                  merch_pos_add_ind,
//                  merch_met_table_number,
//                  merch_invg_code,
//                  merch_deposit_flag,
//                  merch_dly_excpt_flag,
//                  merch_fraud_flag,
//                  merch_visa_cps_ind,
//                  merch_visa_psirf_flag,
//                  merch_visa_eps_flag,
//                  merch_mc_merit_ind,
//                  merch_tax_disc_ind,
//                  merch_mc_smkt_flag,
//                  merch_visa_eirf_flag,
//                  merch_mc_premier_flag,
//                  merch_mc_petrocat_flag,
//                  merch_mc_warehouse_flag,
//                  merch_visainc_status_ind,
//                  merch_dly_ach_flag,
//                  merch_vrs_flag,
//                  merch_stat_hold_flag,
//                  merch_bank_number,
//                  merch_stat_print_flag,
//                  merch_edc_flag,
//                  merch_paper_draft_flag,
//                  merch_interchg_dollar_flag,
//                  merch_interchg_number_flag,
//                  merch_amex_pcid_num,
//                  merch_application_type,
//                  merch_prior_statements,
//                  merch_busgoodserv_descr,
//                  merch_gender,
//                  app_sic_code,
//                  merch_dly_discount_flag,
//                  merch_basis_pt_increase,
//                  seasonal_flag,
//                  vmc_accept_types
//          from    merchant
//          where   app_seq_num = :templateSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchant\n        (\n          app_seq_num,\n          merc_cntrl_number,\n          asso_number,\n          sic_code,\n          loctype_code,\n          refundtype_code,\n          bustype_code,\n          industype_code,\n          merch_years_at_loc,\n          merch_num_of_locations,\n          merch_business_name,\n          merch_business_establ_month,\n          merch_business_establ_year,\n          merch_legal_name,\n          merch_federal_tax_id,\n          merch_month_tot_proj_sales,\n          merch_month_visa_mc_sales,\n          merch_average_cc_tran,\n          merch_prior_cc_accp_flag,\n          merch_cc_acct_term_flag,\n          merch_mail_phone_sales,\n          merch_mcc,\n          merch_pos_add_ind,\n          merch_met_table_number,\n          merch_invg_code,\n          merch_deposit_flag,\n          merch_dly_excpt_flag,\n          merch_fraud_flag,\n          merch_visa_cps_ind,\n          merch_visa_psirf_flag,\n          merch_visa_eps_flag,\n          merch_mc_merit_ind,\n          merch_tax_disc_ind,\n          merch_mc_smkt_flag,\n          merch_visa_eirf_flag,\n          merch_mc_premier_flag,\n          merch_mc_petrocat_flag,\n          merch_mc_warehouse_flag,\n          merch_visainc_status_ind,\n          merch_dly_ach_flag,\n          merch_vrs_flag,\n          merch_stat_hold_flag,\n          merch_bank_number,\n          merch_stat_print_flag,\n          merch_edc_flag,\n          merch_paper_draft_flag,\n          merch_interchg_dollar_flag,\n          merch_interchg_number_flag,\n          merch_amex_pcid_num,\n          merch_application_type,\n          merch_prior_statements,\n          merch_busgoodserv_descr,\n          merch_gender,\n          app_sic_code,\n          merch_dly_discount_flag,\n          merch_basis_pt_increase,\n          seasonal_flag,\n          vmc_accept_types\n        )\n        select   :1 ,\n                 :2 ,\n                asso_number,\n                sic_code,\n                loctype_code,\n                refundtype_code,\n                bustype_code,\n                industype_code,\n                merch_years_at_loc,\n                merch_num_of_locations,\n                 :3 ,\n                merch_business_establ_month,\n                merch_business_establ_year,\n                merch_legal_name,\n                 :4 ,\n                merch_month_tot_proj_sales,\n                merch_month_visa_mc_sales,\n                merch_average_cc_tran,\n                merch_prior_cc_accp_flag,\n                merch_cc_acct_term_flag,\n                merch_mail_phone_sales,\n                merch_mcc,\n                merch_pos_add_ind,\n                merch_met_table_number,\n                merch_invg_code,\n                merch_deposit_flag,\n                merch_dly_excpt_flag,\n                merch_fraud_flag,\n                merch_visa_cps_ind,\n                merch_visa_psirf_flag,\n                merch_visa_eps_flag,\n                merch_mc_merit_ind,\n                merch_tax_disc_ind,\n                merch_mc_smkt_flag,\n                merch_visa_eirf_flag,\n                merch_mc_premier_flag,\n                merch_mc_petrocat_flag,\n                merch_mc_warehouse_flag,\n                merch_visainc_status_ind,\n                merch_dly_ach_flag,\n                merch_vrs_flag,\n                merch_stat_hold_flag,\n                merch_bank_number,\n                merch_stat_print_flag,\n                merch_edc_flag,\n                merch_paper_draft_flag,\n                merch_interchg_dollar_flag,\n                merch_interchg_number_flag,\n                merch_amex_pcid_num,\n                merch_application_type,\n                merch_prior_statements,\n                merch_busgoodserv_descr,\n                merch_gender,\n                app_sic_code,\n                merch_dly_discount_flag,\n                merch_basis_pt_increase,\n                seasonal_flag,\n                vmc_accept_types\n        from    merchant\n        where   app_seq_num =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.app.CharlestonApp",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,appControlNum);
   __sJT_st.setString(3,dbaName);
   __sJT_st.setString(4,taxId);
   __sJT_st.setLong(5,templateSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:237^7*/
    }
    catch(Exception e)
    {
      logEntry("createMerchant()", e.toString());
    }
  }
  
  private void createTranchrg()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:249^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchpayoption
//          (
//            app_seq_num,
//            merchpo_card_merch_number,
//            cardtype_code,
//            card_sr_number,
//            merchpo_split_dial,
//            merchpo_pip
//          )
//          select  :appSeqNum,
//                  merchpo_card_merch_number,
//                  cardtype_code,
//                  card_sr_number,
//                  merchpo_split_dial,
//                  merchpo_pip
//          from    merchpayoption
//          where   app_seq_num = :templateSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchpayoption\n        (\n          app_seq_num,\n          merchpo_card_merch_number,\n          cardtype_code,\n          card_sr_number,\n          merchpo_split_dial,\n          merchpo_pip\n        )\n        select   :1 ,\n                merchpo_card_merch_number,\n                cardtype_code,\n                card_sr_number,\n                merchpo_split_dial,\n                merchpo_pip\n        from    merchpayoption\n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.CharlestonApp",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,templateSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:268^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:270^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_mmin_chrg,
//            tranchrg_discrate_type,
//            tranchrg_float_disc_flag,
//            tranchrg_disc_rate,
//            tranchrg_per_tran
//          )
//          select  :appSeqNum,
//                  cardtype_code,
//                  tranchrg_mmin_chrg,
//                  tranchrg_discrate_type,
//                  tranchrg_float_disc_flag,
//                  tranchrg_disc_rate,
//                  tranchrg_per_tran
//          from    tranchrg
//          where   app_seq_num = :templateSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_mmin_chrg,\n          tranchrg_discrate_type,\n          tranchrg_float_disc_flag,\n          tranchrg_disc_rate,\n          tranchrg_per_tran\n        )\n        select   :1 ,\n                cardtype_code,\n                tranchrg_mmin_chrg,\n                tranchrg_discrate_type,\n                tranchrg_float_disc_flag,\n                tranchrg_disc_rate,\n                tranchrg_per_tran\n        from    tranchrg\n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.CharlestonApp",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,templateSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:291^7*/
    }
    catch(Exception e)
    {
      logEntry("createTranchrg()", e.toString());
    }
  }
  
  private void createMerchPos()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:303^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_pos
//          (
//            app_seq_num,
//            pos_code,
//            pos_param,
//            pg_email
//          )
//          select  :appSeqNum,
//                  pos_code,
//                  pos_param,
//                  pg_email
//          from    merch_pos
//          where   app_seq_num = :templateSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_pos\n        (\n          app_seq_num,\n          pos_code,\n          pos_param,\n          pg_email\n        )\n        select   :1 ,\n                pos_code,\n                pos_param,\n                pg_email\n        from    merch_pos\n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.CharlestonApp",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,templateSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:318^7*/
    }
    catch(Exception e) 
    {
      logEntry("createMerchPos()", e.toString());
    }
  }
  
  private void createSiteInspection()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:330^7*/

//  ************************************************************
//  #sql [Ctx] { insert into siteinspection
//          (
//            app_seq_num,
//            siteinsp_no_of_emp,
//            siteinsp_inv_street,
//            siteinsp_inv_zip,
//            siteinsp_bus_loc,
//            siteinsp_bus_loc_comment,
//            siteinsp_bus_address,
//            siteinsp_bus_city,
//            siteinsp_bus_state,
//            siteinsp_bus_street,
//            siteinsp_bus_zip,
//            siteinsp_full_city,
//            siteinsp_full_flag,
//            siteinsp_full_name,
//            siteinsp_full_state,
//            siteinsp_full_street,
//            siteinsp_full_zip,
//            siteinsp_inv_state,
//            siteinsp_inv_value,
//            siteinsp_soft_flag,
//            siteinsp_soft_name,
//            siteinsp_setupkit_type,
//            siteinsp_setupkit_size,
//            siteinsp_status,
//            siteinsp_comment
//          )
//          select  :appSeqNum,
//                  siteinsp_no_of_emp,
//                  siteinsp_inv_street,
//                  siteinsp_inv_zip,
//                  siteinsp_bus_loc,
//                  siteinsp_bus_loc_comment,
//                  siteinsp_bus_address,
//                  siteinsp_bus_city,
//                  siteinsp_bus_state,
//                  siteinsp_bus_street,
//                  siteinsp_bus_zip,
//                  siteinsp_full_city,
//                  siteinsp_full_flag,
//                  siteinsp_full_name,
//                  siteinsp_full_state,
//                  siteinsp_full_street,
//                  siteinsp_full_zip,
//                  siteinsp_inv_state,
//                  siteinsp_inv_value,
//                  siteinsp_soft_flag,
//                  siteinsp_soft_name,
//                  siteinsp_setupkit_type,
//                  siteinsp_setupkit_size,
//                  siteinsp_status,
//                  ''
//          from    siteinspection
//          where   app_seq_num = :templateSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into siteinspection\n        (\n          app_seq_num,\n          siteinsp_no_of_emp,\n          siteinsp_inv_street,\n          siteinsp_inv_zip,\n          siteinsp_bus_loc,\n          siteinsp_bus_loc_comment,\n          siteinsp_bus_address,\n          siteinsp_bus_city,\n          siteinsp_bus_state,\n          siteinsp_bus_street,\n          siteinsp_bus_zip,\n          siteinsp_full_city,\n          siteinsp_full_flag,\n          siteinsp_full_name,\n          siteinsp_full_state,\n          siteinsp_full_street,\n          siteinsp_full_zip,\n          siteinsp_inv_state,\n          siteinsp_inv_value,\n          siteinsp_soft_flag,\n          siteinsp_soft_name,\n          siteinsp_setupkit_type,\n          siteinsp_setupkit_size,\n          siteinsp_status,\n          siteinsp_comment\n        )\n        select   :1 ,\n                siteinsp_no_of_emp,\n                siteinsp_inv_street,\n                siteinsp_inv_zip,\n                siteinsp_bus_loc,\n                siteinsp_bus_loc_comment,\n                siteinsp_bus_address,\n                siteinsp_bus_city,\n                siteinsp_bus_state,\n                siteinsp_bus_street,\n                siteinsp_bus_zip,\n                siteinsp_full_city,\n                siteinsp_full_flag,\n                siteinsp_full_name,\n                siteinsp_full_state,\n                siteinsp_full_street,\n                siteinsp_full_zip,\n                siteinsp_inv_state,\n                siteinsp_inv_value,\n                siteinsp_soft_flag,\n                siteinsp_soft_name,\n                siteinsp_setupkit_type,\n                siteinsp_setupkit_size,\n                siteinsp_status,\n                ''\n        from    siteinspection\n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.app.CharlestonApp",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,templateSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:387^7*/
    }
    catch(Exception e)
    {
      logEntry("createSiteInspection()", e.toString());
    }
  }
  
  private void createAddress()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:399^7*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//          (
//            app_seq_num,
//            address_line1,
//            addresstype_code,
//            address_line2,
//            address_city,
//            country_code,
//            address_zip,
//            countrystate_code,
//            address_phone,
//            address_fax
//          )
//          select  :appSeqNum,
//                  address_line1,
//                  addresstype_code,
//                  address_line2,
//                  address_city,
//                  country_code,
//                  address_zip,
//                  countrystate_code,
//                  address_phone,
//                  address_fax
//          from    address
//          where   app_seq_num = :templateSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into address\n        (\n          app_seq_num,\n          address_line1,\n          addresstype_code,\n          address_line2,\n          address_city,\n          country_code,\n          address_zip,\n          countrystate_code,\n          address_phone,\n          address_fax\n        )\n        select   :1 ,\n                address_line1,\n                addresstype_code,\n                address_line2,\n                address_city,\n                country_code,\n                address_zip,\n                countrystate_code,\n                address_phone,\n                address_fax\n        from    address\n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.app.CharlestonApp",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,templateSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:426^7*/
    }
    catch(Exception e)
    {
      logEntry("createAddress()", e.toString());
    }
  }
  
  private void createMerchBank()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:438^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchbank
//          (
//            app_seq_num,
//            merchbank_name,
//            bankacc_type,
//            merchbank_acct_srnum,
//            merchbank_acct_num,
//            merchbank_transit_route_num,
//            merchbank_num_years_open,
//            merchbank_info_source
//          )
//          select  :appSeqNum,
//                  merchbank_name,
//                  bankacc_type,
//                  merchbank_acct_srnum,
//                  :ddaNum,
//                  :transitRouting,
//                  merchbank_num_years_open,
//                  merchbank_info_source
//          from    merchbank
//          where   app_seq_num = :templateSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchbank\n        (\n          app_seq_num,\n          merchbank_name,\n          bankacc_type,\n          merchbank_acct_srnum,\n          merchbank_acct_num,\n          merchbank_transit_route_num,\n          merchbank_num_years_open,\n          merchbank_info_source\n        )\n        select   :1 ,\n                merchbank_name,\n                bankacc_type,\n                merchbank_acct_srnum,\n                 :2 ,\n                 :3 ,\n                merchbank_num_years_open,\n                merchbank_info_source\n        from    merchbank\n        where   app_seq_num =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.app.CharlestonApp",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,ddaNum);
   __sJT_st.setString(3,transitRouting);
   __sJT_st.setLong(4,templateSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:461^7*/
    }
    catch(Exception e)
    {
      logEntry("createMerchBank()", e.toString());
    }
  }
  
  private void createMerchContact()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:473^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchcontact
//          (
//            app_seq_num,
//            merchcont_prim_first_name,
//            merchcont_prim_last_name,
//            merchcont_prim_phone
//          )
//          select  :appSeqNum,
//                  merchcont_prim_first_name,
//                  merchcont_prim_last_name,
//                  merchcont_prim_phone
//          from    merchcontact
//          where   app_seq_num = :templateSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchcontact\n        (\n          app_seq_num,\n          merchcont_prim_first_name,\n          merchcont_prim_last_name,\n          merchcont_prim_phone\n        )\n        select   :1 ,\n                merchcont_prim_first_name,\n                merchcont_prim_last_name,\n                merchcont_prim_phone\n        from    merchcontact\n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.app.CharlestonApp",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,templateSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:488^7*/
    }
    catch(Exception e)
    {
      logEntry("createMerchContact()", e.toString());
    }
  }
  
  private void createBusinessOwner()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:500^7*/

//  ************************************************************
//  #sql [Ctx] { insert into businessowner
//          (
//            app_seq_num,
//            busowner_first_name,
//            busowner_owner_perc,
//            busowner_num,
//            busowner_period_month,
//            busowner_ssn,
//            busowner_period_year,
//            busowner_last_name
//          )
//          select  :appSeqNum,
//                  busowner_first_name,
//                  busowner_owner_perc,
//                  busowner_num,
//                  busowner_period_month,
//                  busowner_ssn,
//                  busowner_period_year,
//                  busowner_last_name
//          from    businessowner
//          where   app_seq_num = :templateSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into businessowner\n        (\n          app_seq_num,\n          busowner_first_name,\n          busowner_owner_perc,\n          busowner_num,\n          busowner_period_month,\n          busowner_ssn,\n          busowner_period_year,\n          busowner_last_name\n        )\n        select   :1 ,\n                busowner_first_name,\n                busowner_owner_perc,\n                busowner_num,\n                busowner_period_month,\n                busowner_ssn,\n                busowner_period_year,\n                busowner_last_name\n        from    businessowner\n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.app.CharlestonApp",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,templateSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:523^7*/
    }
    catch(Exception e)
    {
      logEntry("createBusinessOwner()", e.toString());
    }
  }
  
  private void markAppComplete()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:535^7*/

//  ************************************************************
//  #sql [Ctx] { update  screen_progress
//          set     screen_complete = 255
//          where   screen_sequence_id = 2
//                  and screen_pk = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  screen_progress\n        set     screen_complete = 255\n        where   screen_sequence_id = 2\n                and screen_pk =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.app.CharlestonApp",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:541^7*/
      
      (new InsertCreditQueue()).addApp(appSeqNum, 357538);
    }
    catch(Exception e)
    {
      logEntry("markAppComplete()", e.toString());
    }
  }
  
  private void create()
  {
    try
    {
      connect();
      
      initializeApp();
      
      log.debug("Creating merchant record");
      createMerchant();
      
      log.debug("Creating tranchrg record");
      createTranchrg();
      
      log.debug("Creating merch_pos record");
      createMerchPos();
      
      log.debug("Creating siteinspection record");
      createSiteInspection();
      
      log.debug("Creating address record");
      createAddress();
      
      log.debug("creating merchbank record");
      createMerchBank();
      
      log.debug("creating merchcontact record");
      createMerchContact();
      
      log.debug("creating businessowner record");
      createBusinessOwner();
      
      log.debug("marking app as complete and inserting into credit queue");
      markAppComplete();
    }
    catch(Exception e)
    {
      logEntry("create()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public static void main(String[] args)
  {
    try
    {
      SQLJConnectionBase.initStandalonePool();
      //SQLJConnectionBase.initStandalone("DEBUG");
      
      // 0 = DBA Name
      // 1 = DDA Number
      // 2 = tax ID
      // 3 = transit routing
      CharlestonApp app = new CharlestonApp(args[0], args[1], args[2], args[3]);
      
      app.create();
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
    }
    finally
    {
      OracleConnectionPool.destroy();
      MesDefaults.destroy();
      HttpHelper.destroy();
    }

    System.exit(0);
  }
}/*@lineinfo:generated-code*/