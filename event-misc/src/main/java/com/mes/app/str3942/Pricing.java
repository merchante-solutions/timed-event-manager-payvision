/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/ffb/Pricing.java $

  Description:

  Pricing

  1st Financial Bank online app pricing page bean.  

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.str3942;

import javax.servlet.http.HttpServletRequest;
import com.mes.app.PricingBase;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.SmallCurrencyField;

public class Pricing extends PricingBase
{
  {
    appType = 66;
    curScreenId = 3;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      connect();
      
      // initialize the wireless equipment object
      wirelessSet = new WirelessEquipmentSet(appSeqNum);
      add(wirelessSet);
      
      // v/mc pricing options
      fields.setData("pricingPlan","3");

      // setup bet options
      fields.setData("betType","66");
      
      Field betSet66 = new DropDownField("betSet_66",new BetSet(66),false);
      fields.add(betSet66);
      fields.add(new BetSetMapperField());
      betSet66.setOptionalCondition(
        new FieldValueCondition(fields.getField("betType"),"66"));
        
      fields.add(new SmallCurrencyField("perItem", 5, 6, true, 0.00f, 0.99f));
        
      // set the field style class
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      e.printStackTrace();
      logEntry("createFields()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
}
