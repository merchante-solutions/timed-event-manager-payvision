/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/mes/Business.sqlj $

  Description:
  
  Business
  
  CB&T online application merchant information page bean.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/20/04 5:06p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.fnms;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.SicField;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Business extends com.mes.app.BusinessBase
{
  {
    appType = 48;
    curScreenId = 1;
  }
  
  protected class IndustryTypeTable extends DropDownTable
  {
    public IndustryTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail - 010450");
      addElement("2","Restaurant - 011100");
      addElement("10","Lodging - 011101");
      addElement("17","Supermarket - 010450");
      addElement("5","Internet - 010450");
      addElement("7","Direct Marketing - 010450");
      addElement("8","Other - 010450");
      addElement("3","Hotel - 011101");
      addElement("4","Motel - 011101");
      addElement("6","Services - 010450");
    }
  }
  
  protected class LocationTypeTable extends DropDownTable
  {
    public LocationTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail Storefront");
      addElement("2","Internet Storefront");
      addElement("3","Business Office");
      addElement("4","Private Residence");
      addElement("7","Separate Building");
      addElement("5","Other");
    }
  }
  
  protected class BranchTable extends DropDownTable
  {
    public BranchTable(long topHid)
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:93^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    mod(t1.descendent,1000000)  assoc_num,
//                      o2.org_name                 branch_name
//            from      t_hierarchy t1,
//                      t_hierarchy t2,
//                      organization o1,
//                      organization o2
//            where     t1.ancestor = :topHid
//                      and t1.relation = 5
//                      and t1.descendent = o1.org_group
//                      and t2.descendent = t1.descendent
//                      and t2.relation = 1
//                      and t2.ancestor = o2.org_group
//                      and lower(o1.org_name) like '%non%chain%'  
//            order by  o2.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mod(t1.descendent,1000000)  assoc_num,\n                    o2.org_name                 branch_name\n          from      t_hierarchy t1,\n                    t_hierarchy t2,\n                    organization o1,\n                    organization o2\n          where     t1.ancestor =  :1 \n                    and t1.relation = 5\n                    and t1.descendent = o1.org_group\n                    and t2.descendent = t1.descendent\n                    and t2.relation = 1\n                    and t2.ancestor = o2.org_group\n                    and lower(o1.org_name) like '%non%chain%'  \n          order by  o2.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.fnms.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,topHid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.fnms.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:109^9*/
        rs = it.getResultSet();
        
        addElement("","select one");
        while (rs.next())
        {
          addElement(rs.getString("assoc_num"),rs.getString("branch_name"));
        }
      }
      catch (Exception e)
      {
        logEntry("BranchTable()",e.toString());
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // make some fields required
      fields.getField("accountType").makeRequired();
      fields.getField("taxpayerId").makeRequired();
      fields.getField("yearsOpen").makeRequired();
      
      // make some fields not required
      fields.getField("sourceOfInfo").makeOptional();
      
      // add sic code as visible field
      fields.deleteField("sicCode");
      fields.add(new SicField("sicCode", "SIC Code", false));
      fields.getField("sicCode").makeOptional();
      
      fields.deleteField("assocNum");
      fields.add(new NumberField("assocNum", "Association #", 6, 6, true, 0));

      // make refund policy default to "30 days or less"      
      fields.deleteField("refundPolicy");
      fields.add(new HiddenField("refundPolicy"));
      setData("refundPolicy", "1");

      // add validation of the confirmation fields
      fields.getField("confirmCheckingAccount")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("checkingAccount"),"Checking Account #"));
      fields.getField("confirmTransitRouting")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("transitRouting"),"Transit Routing #"));

      // add referring branch assoc num dropdown
      fields.add(new DropDownField("assocNum",new BranchTable(3941500006L), false));
      
      // override description of existingVerisign so that it is generic
      fields.add(new CheckboxField("existingVerisign",  "Merchant has an existing Internet account",false));
      
      // override location and industry type tables for CCB
      fields.add(new DropDownField    ("industryType",      "Industry",new IndustryTypeTable(),false));
      fields.add(new DropDownField    ("locationType",      "Type of Business Location",new LocationTypeTable(),false));
      
      fields.add(new Field("clientData1", 40, 20, true));
      
      fields.getField("businessEmail").addValidation(new InternetEmailRequiredValidation(fields.getField("productType")));
      
      createPosPartnerExtendedFields();
      
      // reset all fields, including those just added to be of class form
      fields.setHtmlExtra("class=\"formText\"");
      
      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      logEntry("createFields()",e.toString());
    }
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
    
    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }
  }
  
  protected boolean loadAppData()
  {
    long                appSeqNum   = 0L;
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = super.loadAppData();
    
    try
    {
      appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if(result && appSeqNum != 0)
      {
        connect();
      
        // load client data
        /*@lineinfo:generated-code*//*@lineinfo:221^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  client_data_1
//            from    merchant
//            where   app_seq_num = :fields.getField("appSeqNum").asInteger()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2888 = fields.getField("appSeqNum").asInteger();
  try {
   String theSqlTS = "select  client_data_1\n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.fnms.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_2888);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.fnms.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:226^9*/
      
        rs = it.getResultSet();
      
        setFields(rs);
      
        result = true;
      }
    }
    catch(Exception e)
    {
      logEntry("loadAppData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( result );
  }
  
  protected boolean submitAppData()
  {
    long    appSeqNum = 0L;
    boolean result    = super.submitAppData();
    
    try
    {
      connect();
      
      appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if(result && appSeqNum != 0L)
      {
        // update merchant table with rep code data
        /*@lineinfo:generated-code*//*@lineinfo:263^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     client_data_1 = :getData("clientData1")
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2889 = getData("clientData1");
   String theSqlTS = "update  merchant\n          set     client_data_1 =  :1 \n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.fnms.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2889);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:268^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("submitAppData()", e.toString());
      result = false;
    }
    finally
    {
      cleanUp();
    }
    
    return( result );
  }
}/*@lineinfo:generated-code*/