/*@lineinfo:filename=Info*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/storefront/Business.sqlj $

  Description:
  
  Business
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.card;

import javax.servlet.http.HttpServletRequest;
import com.mes.app.VirtualAppBusinessBase;
import com.mes.constants.mesConstants;

public class Info extends VirtualAppBusinessBase
{
  {
    appType       = mesConstants.APP_TYPE_CARDINAL;
    curScreenId   = 1;
    submitPartial = true;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.getField("promotionCode").makeRequired();
      
      fields.getField("locationYears").makeOptional();
      fields.getField("statementsProvided").makeOptional();
      fields.getField("haveCanceled").makeOptional();
      fields.getField("canceledProcessor").makeOptional();
      fields.getField("canceledReason").makeOptional();
      fields.getField("cancelDate").makeOptional();
      fields.getField("bankName").makeOptional();
      fields.getField("checkingAccount").makeOptional();
      fields.getField("confirmCheckingAccount").makeOptional();
      fields.getField("transitRouting").makeOptional();
      fields.getField("confirmTransitRouting").makeOptional();
      fields.getField("sourceOfInfo").makeOptional();
      fields.getField("bankAddress").makeOptional();
      fields.getField("bankCsz").makeOptional();
      fields.getField("bankPhone").makeOptional();
      fields.getField("owner1Name").makeOptional();
      fields.getField("owner1Title").makeOptional();
      fields.getField("owner1Address1").makeOptional();
      fields.getField("owner1SSN").makeOptional();
      fields.getField("owner1Csz").makeOptional();
      fields.getField("owner1Since").makeOptional();
      fields.getField("owner1Phone").makeOptional();
      fields.getField("owner1Percent").makeOptional();
      fields.getField("owner1Gender").makeOptional();
      fields.getField("refundPolicy").makeOptional();
      
      fields.getField("taxpayerId").makeOptional();
      fields.getField("establishedDate").makeOptional();
      fields.getField("businessType").makeOptional();
    }
    catch(Exception e)
    {
      logEntry("createFields()-card\\Business", e.toString());
    }
  }
  
  protected boolean submitAppData()
  {
    boolean submitOk = false;

    try
    {
      connect();
      
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if(appSeqNum != 0L)
      {
        submitMerchant(appSeqNum);
        //submitBank(appSeqNum);
        submitAddresses(appSeqNum);
        //submitOwners(appSeqNum);
        submitContact(appSeqNum);
        submitCards(appSeqNum);
        //submitProduct(appSeqNum);
        //submitEquipment(appSeqNum);
      }
      
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitAppData(" + fields.getData("appSeqNum") + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  
    return submitOk;
  }
}/*@lineinfo:generated-code*/