/*@lineinfo:filename=RcMerchInfo*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/rivercity/RcMerchInfo.sqlj $

  Description:  


  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 7/18/03 2:55p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.rivercity;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.MerchInfoDataBean;
import com.mes.forms.DropDownField;
import com.mes.forms.FieldGroup;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class RcMerchInfo extends MerchInfoDataBean
{
  public static class RcRefundPolicyTable extends DropDownTable
  {
    public RcRefundPolicyTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Refund in 30 Days or Less");
      addElement("2","No Refunds or Exchanges");
      addElement("3","Exchanges Only");
      addElement("6","Cash Back");
      addElement("7","Full Refund");
      addElement("4","Not Applicable");
    }
  }

  /*
  ** public static class BranchTable extends DropDownTable
  **
  ** List of River City referring branches with association derived from each
  ** branch's hierarchy node id.
  */
  public static class BranchTable extends DropDownTable
  {
    public BranchTable()
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:73^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    mod(t1.descendent,1000000)  assoc_num,
//                      o2.org_name                 branch_name
//                 
//            from      t_hierarchy t1,
//                      t_hierarchy t2,
//                      organization o1,
//                      organization o2
//                 
//            where     t1.ancestor = 3941400041
//                      and t1.relation = 4
//                      and t1.descendent = o1.org_group
//                      and t2.descendent = t1.descendent
//                      and t2.relation = 1
//                      and t2.ancestor = o2.org_group
//                      and lower(o1.org_name) like 'non chain%'  
//                 
//            order by  t2.ancestor,o1.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mod(t1.descendent,1000000)  assoc_num,\n                    o2.org_name                 branch_name\n               \n          from      t_hierarchy t1,\n                    t_hierarchy t2,\n                    organization o1,\n                    organization o2\n               \n          where     t1.ancestor = 3941400041\n                    and t1.relation = 4\n                    and t1.descendent = o1.org_group\n                    and t2.descendent = t1.descendent\n                    and t2.relation = 1\n                    and t2.ancestor = o2.org_group\n                    and lower(o1.org_name) like 'non chain%'  \n               \n          order by  t2.ancestor,o1.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.rivercity.RcMerchInfo",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.rivercity.RcMerchInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:92^9*/
        rs = it.getResultSet();
        
        addElement("","select one");
        while (rs.next())
        {
          addElement(rs.getString("assoc_num"),rs.getString("branch_name"));
        }
      }
      catch (Exception e)
      {
        logEntry("BranchTable()",e.toString());
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  
  public void init( )
  {
    super.init();
    
    try
    {
      // add additional 
      FieldGroup fgRc = new FieldGroup("fgRiverCity");

      // replaces base class field with drop down of rivercity branch assocs
      fgRc.add(new DropDownField("assocNum",      new BranchTable(),false));
      
      fgRc.setGroupHtmlExtra("class=\"formFields\"");
      fields.add(fgRc);
      
      // remove required from legal name
      fields.getField("businessLegalName").removeValidation("required");
      
      // make sic code required
      fields.getField("sicCode").makeRequired();
      
      // turn off required validation on acct info source 
      // and referring bank (won't be shown)
      fields.getField("sourceOfInfo") .removeValidation("required");
      fields.getField("referringBank").removeValidation("required");
      
      // make required validation on primary owner 
      // conditional based on business type
      //Condition cIsNotTaxExempt = new NotCondition(
      //  new FieldValueCondition(fields.getField("businessType"),"6"));
      fields.getField("owner1FirstName" ).removeValidation("required");   //.setOptionalCondition(cIsNotTaxExempt);
      fields.getField("owner1LastName"  ).removeValidation("required");   //.setOptionalCondition(cIsNotTaxExempt);
      fields.getField("owner1SSN"       ).removeValidation("required");   //.setOptionalCondition(cIsNotTaxExempt);
      fields.getField("owner1Percent"   ).removeValidation("required");   //.setOptionalCondition(cIsNotTaxExempt);
      fields.getField("owner1Address1"  ).removeValidation("required");   //.setOptionalCondition(cIsNotTaxExempt);
      fields.getField("owner1City"      ).removeValidation("required");   //.setOptionalCondition(cIsNotTaxExempt);
      fields.getField("owner1State"     ).removeValidation("required");   //.setOptionalCondition(cIsNotTaxExempt);
      fields.getField("owner1Zip"       ).removeValidation("required");   //.setOptionalCondition(cIsNotTaxExempt);
      fields.getField("owner1Phone"     ).removeValidation("required");   //.setOptionalCondition(cIsNotTaxExempt);
      fields.getField("owner1Title"     ).removeValidation("required");   //.setOptionalCondition(cIsNotTaxExempt);
      fields.getField("owner1SinceMonth").removeValidation("required");   //.setOptionalCondition(cIsNotTaxExempt);
      fields.getField("owner1SinceYear" ).removeValidation("required");   //.setOptionalCondition(cIsNotTaxExempt);
      fields.getField("owner1Gender"    ).removeValidation("required");   //.setOptionalCondition(cIsNotTaxExempt);
      
      // default fill bank address to rivercity bank hq
      fields.setData("bankName"               ,"River City Bank");
      fields.setData("bankAddress"            ,"2485 Natomas Park Dr.");
      fields.setData("bankCity"               ,"Sacramento");
      fields.setData("bankState"              ,"CA");
      fields.setData("bankZip"                ,"95833");
      fields.setData("transitRouting"         ,"121133416");
      fields.setData("confirmTransitRouting"  ,"121133416");
      
      // replace base refund policy with rivercity list
      fields.add(new DropDownField("refundPolicy",new RcRefundPolicyTable(),false));
      fields.getField("refundPolicy").setHtmlExtra("class=\"formFields\"");
      
      // set location number to 1 by default
      fields.setData("numLocations","1");
      
      // create the extended POS Partner 2000 fields
      createPosPartnerExtendedFields();
      
      // must be last call in order to set all fields
      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch( Exception e )
    {
      logEntry("init()",e.toString());
    }
  }
  
  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
    
    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }
  }
  
  public void loadData( )
  {
    super.loadData();
    
    ResultSetIterator it = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:205^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  account_type  as rc_account_type,
//                  assoc_hid     as rc_branch_hid
//          from    app_rc_merchant
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  account_type  as rc_account_type,\n                assoc_hid     as rc_branch_hid\n        from    app_rc_merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.rivercity.RcMerchInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.rivercity.RcMerchInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:211^7*/
      setFields(it.getResultSet());
    }
    catch( Exception e )
    {
      logEntry("loadData()",e.toString());
    }                          
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }

  public void storeData( )
  {
    super.storeData();

    try
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:232^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_rc_merchant
//            ( app_seq_num,
//              account_type,
//              assoc_hid )
//              
//            values
//            ( :AppSeqNum,
//              : getData("rcAccountType"),
//              : getData("rcBranchHid") )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3059 =  getData("rcAccountType");
 String __sJT_3060 =  getData("rcBranchHid");
   String theSqlTS = "insert into app_rc_merchant\n          ( app_seq_num,\n            account_type,\n            assoc_hid )\n            \n          values\n          (  :1 ,\n             :2 ,\n             :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.rivercity.RcMerchInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,__sJT_3059);
   __sJT_st.setString(3,__sJT_3060);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:243^9*/
      }
      catch (SQLException e)
      {
        /*@lineinfo:generated-code*//*@lineinfo:247^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_rc_merchant
//            
//            set     account_type  = : getData("rcAccountType"),
//                    assoc_hid     = : getData("rcBranchHid")
//                    
//            where   app_seq_num   = :AppSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3061 =  getData("rcAccountType");
 String __sJT_3062 =  getData("rcBranchHid");
   String theSqlTS = "update  app_rc_merchant\n          \n          set     account_type  =  :1 ,\n                  assoc_hid     =  :2 \n                  \n          where   app_seq_num   =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.rivercity.RcMerchInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3061);
   __sJT_st.setString(2,__sJT_3062);
   __sJT_st.setLong(3,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:255^9*/
      }
    }
    catch( Exception e )
    {
      logEntry("storeData()",e.toString());
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/