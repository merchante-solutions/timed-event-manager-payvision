/*@lineinfo:filename=Pricing*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/mes/Pricing.sqlj $

  Description:

  Pricing

  CB&T online app pricing page bean.  Extends PricingBase with CB&T custom
  pricing options.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/29/04 9:46a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.mes;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.FeeSet.EquipFee;
import com.mes.app.PricingBase;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import sqlj.runtime.ResultSetIterator;

public class Pricing extends PricingBase
{
  {
    appType = 28;
    curScreenId = 3;
  }
  
  /*************************************************************************
  **
  **   Drop Down Tables
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Radio Button Lists
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Validations
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Custom Fields
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Field Bean
  **
  **************************************************************************/
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      connect();
      
      // initialize the wireless equipment object
      wirelessSet = new WirelessEquipmentSet(appSeqNum);
      add(wirelessSet);
      
      // v/mc pricing options
      
      fields.setData("pricingPlan","1");
      
      // setup bet options
      fields.setData("betType","40");
      
      Field betSet40 = new DropDownField("betSet_40",new BetSet(40),false);
      fields.add(betSet40);
      fields.add(new BetSetMapperField());
      betSet40.setOptionalCondition(
        new FieldValueCondition(fields.getField("betType"),"40"));
        
      // daily discount selector
      fields.add(new CheckboxField("dailyDiscount", "Daily Discount", true));
      
      // set site inspection fields to be required
      fields.getField("locationType").makeRequired();
      fields.getField("locationAddrType").makeRequired();
      fields.getField("locationCsz").makeRequired();
      fields.getField("employeeCount").makeRequired();
      
      for(int i=0; i < questionList.length; ++i)
      {
        fields.getField(questionList[i][Q_FIELD_NAME]).makeRequired();
      }
      
      // make sure any equipment purchase or rental fees are required
      EquipFee[] equipFees = feeSet.getEquipFees();
      
      for(int i=0; i < equipFees.length; ++i)
      {
        EquipFee fee = (EquipFee)(equipFees[i]);
        
        // generate field name from equipFee data
        String equipFieldName = "equipFee_" + fee.getModel() + "_" + fee.getLendDesc();
        
        fields.getField(equipFieldName).makeRequired();
      }
        
      // set the field style class
      fields.setHtmlExtra("class=\"formText\"");

      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      e.printStackTrace();
      logEntry("createFields()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*************************************************************************
  **
  **   Load
  **
  **************************************************************************/
  protected boolean loadAppData()
  {
    long                appSeqNum = 0L;
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    boolean             result    = super.loadAppData();
    
    try
    {
      connect();
      
      appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if(result && appSeqNum != 0)
      {
        // load flag from database
        /*@lineinfo:generated-code*//*@lineinfo:182^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(merch_dly_discount_flag,
//                      null, 'y',
//                      'D',  'y',
//                      'n')  daily_discount
//            from    merchant
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(merch_dly_discount_flag,\n                    null, 'y',\n                    'D',  'y',\n                    'n')  daily_discount\n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.mes.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.mes.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:190^9*/
        
        rs = it.getResultSet();
        
        setFields(rs);
      }
    }
    catch(Exception e)
    {
      System.out.println("com.mes.app.mes.Pricing.loadAppData(): " + e.toString());
      logEntry("loadAppData(" + appSeqNum + ")", e.toString());
      result = false;
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }

  /*************************************************************************
  **
  **   Submit
  **
  **************************************************************************/
  protected boolean submitAppData()
  {
    long    appSeqNum = 0L;
    boolean result    = super.submitAppData();
    
    try
    {
      connect();
      
      appSeqNum = fields.getField("appSeqNum").asInteger();
      
      // check to see if daily discount was un-checked
      if(autoSubmitName != null && autoSubmitName.equals("actionAllowSubmit"))
      {
        // submitting.  check to see if daily discount exists in request.
        // if not, un-set the daily discount field
        if(request.getParameter("dailyDiscount") == null)
        {
          fields.getField("dailyDiscount").setData("n");
        }
      }
      
      if(result && appSeqNum != 0L)
      {
        // store daily discount data
        /*@lineinfo:generated-code*//*@lineinfo:243^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_dly_discount_flag = decode(:fields.getField("dailyDiscount").getData(),
//                                                'n', 'N',
//                                                'y', 'D',
//                                                'D')
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2976 = fields.getField("dailyDiscount").getData();
   String theSqlTS = "update  merchant\n          set     merch_dly_discount_flag = decode( :1 ,\n                                              'n', 'N',\n                                              'y', 'D',\n                                              'D')\n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.app.mes.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2976);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:251^9*/
      }
    }
    catch(Exception e)
    {
      System.out.println("com.mes.app.mes.submitAppData(): " + e.toString());
      logEntry("submitAppData(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return result;
  }
  
}/*@lineinfo:generated-code*/