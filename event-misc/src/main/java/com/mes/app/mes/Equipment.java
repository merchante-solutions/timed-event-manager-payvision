/**************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/mes/Equipment.java $

  Description:
  
  Equipment
  
  Banner Bank online application equipment page bean.


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/13/04 11:05a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.mes;

import javax.servlet.http.HttpServletRequest;
//import sqlj.runtime.ResultSetIterator;
import com.mes.app.EquipmentBase;

public class Equipment extends EquipmentBase
{
  {
    appType = 28;
    curScreenId = 2;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // set trainingType field to be required
      fields.getField("trainingType").makeRequired();
      
      // terminal comm type required
      fields.getField("commType").makeRequired();
      
      // reset all fields, including those just added to be of class formText
      fields.setHtmlExtra("class=\"formText\"");
      
      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
}
