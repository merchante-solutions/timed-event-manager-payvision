/*@lineinfo:filename=SicBrowser*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/SicBrowser.sqlj $

  Description:  
  
  SicBrowser
  
  Loads a list of SIC codes with their descriptions (alphabetized
  by description or SIC).

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 10/23/03 2:58p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.TreeSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.SortableReport;
import sqlj.runtime.ResultSetIterator;

public class SicBrowser extends SortableReport
{
  public static final int SB_SIC          = 1;
  public static final int SB_DESCRIPTION  = 2;
  
  private SortByType      sbt             = new SortByType(SB_DESCRIPTION);
  
  private TreeSet reportData;
  
  public class ReportRecord implements Comparable
  {
    private SortByType  sbt;
    private int         sic;
    private String      description;
    
    private boolean     isGood            = false;
    private String      error             = null;
    
    public String toString()
    {
      return "sic: " + sic + " " + description;
    }
    
    public ReportRecord(ResultSet rs, SortByType sbt)
    {
      this.sbt = sbt;
      try
      {
        sic         = rs.getInt   ("sic");
        description = rs.getString("description");
        
        isGood = true;
      }
      catch (Exception e)
      {
        System.out.println("ReportRecord: " + e);
        error = e.toString();
      }
    }
    
    public int      getSic()          { return sic; }
    public String   getDescription()  { return description; }

    public boolean  getIsGood()       { return isGood; }
    public String   getError()        { return error; }

    public int compareTo(Object o)
    {
      ReportRecord that = (ReportRecord)o;
      int result = 0;
      
      try
      {
        switch (sbt.getSortBy())
        {
          case SB_SIC:
            result = compare(sic,that.sic);
            break;
            
          case SB_DESCRIPTION:
          default:
            result = compare(description.toUpperCase(),
              that.description.toUpperCase());
            break;
        }
      }
      catch (Exception e) {}

      if (result == 0)
      {
        try
        {
          result = compare(sic,that.sic);
        }
        catch (Exception e) {}
      }

      // if reverse sort specified, flip the result around
      if (sbt.getReverseSort())
      {
        result *= (-1);
      }

      return result;
    }
  }
  
  private boolean dataLoaded        = false;
  private boolean isFirstTime       = true;
  private boolean refreshRequested  = false;

  public SicBrowser()
  {
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    fields.add(new Field("lookupValue",25,0,true));
    fields.add(new ButtonField("search"));
    
    // reset all fields, including those just added to be of class form
    fields.setHtmlExtra("class=\"formText\"");
  }
  
  public void requestRefresh()
  {
    refreshRequested = true;
  }

  public void getData() throws Exception
  {
    ResultSetIterator it = null;
    
    try
    {
      connect();

      // string version (with wildcards) of lookup value
      String lookupValue = getData("lookupValue");
      String stringLookup = "";
      if (lookupValue.trim().equals(""))
      {
        stringLookup = "passall";
      }
      else
      {
        stringLookup = "%" + lookupValue.toUpperCase() + "%";
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:172^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sic,
//                  description
//          from    rap_sic_codes
//          where   'passall'          = :stringLookup
//                  or sic          like :stringLookup
//                  or description  like :stringLookup
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sic,\n                description\n        from    rap_sic_codes\n        where   'passall'          =  :1 \n                or sic          like  :2 \n                or description  like  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.SicBrowser",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,stringLookup);
   __sJT_st.setString(2,stringLookup);
   __sJT_st.setString(3,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.SicBrowser",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:180^7*/

      ResultSet rs = it.getResultSet();
      reportData = new TreeSet();
      while (rs.next())
      {
        reportData.add(new ReportRecord(rs,sbt));
      }

      dataLoaded = true;
      isFirstTime = false;
    }
    catch (Exception e)
    {
      // log the error...
      String func = this.getClass().getName() + "::getData()";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);

      // ...but still throw the exception
      throw e;
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  public Iterator iterator() throws Exception
  {
    // always get new data
    getData();

    // return an iterator into the report data
    return reportData.iterator();
  }
}/*@lineinfo:generated-code*/