/*@lineinfo:filename=PricingDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/PricingDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 7/18/03 8:47a $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.forms.CheckboxField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DisabledCheckboxField;
import com.mes.forms.DiscountField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.forms.ZipField;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class PricingDataBean extends AppDataBeanBase
{
  private static final double[]       ExcludeZero     = { 0.0 };

  // these are all the misc charges supported by the default app
  protected static int[] miscCharges =
  {
    mesConstants.APP_MISC_CHARGE_CHARGEBACK,
    mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE,
    mesConstants.APP_MISC_CHARGE_HELP_DESK,
    mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT,
    mesConstants.APP_MISC_CHARGE_WEB_REPORTING,
    mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_APP,
    mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_SETUP,
    mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP
  };
  
  protected int[] getMiscCharges()
  {
    return miscCharges;
  }
  
  protected static String[][] PricePlanRadioButtons = 
  {
    { "Fixed Rate Plan"             , Integer.toString( mesConstants.APP_PS_FIXED_RATE )          },
    { "Interchange Plan"            , Integer.toString( mesConstants.APP_PS_INTERCHANGE )         },
    { "Fixed Rate + Per Item Plan"  , Integer.toString( mesConstants.APP_PS_FIXED_PLUS_PER_ITEM ) }
  };
  
  // indexes into the QuestionFields string array
  public static final int           Q_FIELD_NAME        = 0;
  public static final int           Q_TEXT              = 1;
  protected static String[][] QuestionFields = 
  {
      // field name           // question
    { "qNameMatch",           "Did name posted at business match business name on application?" },
    { "qSignage",             "Did location appear to have appropriate signage?" },
    { "qHoursPosted",         "Were business hours posted?" },
    { "qInventoryReview",     "Was merchant's inventory viewed?" },
    { "qInventoryConsistent", "Was inventory consistent with merchant's type of business?" },
    { "qInventoryAdequate",   "Did inventory appear to be adequate to support the sales volume indicated on the application?" }
  };
  
  //**************************************************************************
  // Drop Down Tables
  //**************************************************************************
  protected class BasisPointIncTable extends DropDownTable
  {
    public BasisPointIncTable( )
    {
      addElement("","select one");
      addElement("00","00bp");
      addElement("15","15bp");
    }
  }
  
  protected class LocationAddressTable extends DropDownTable
  {
    public LocationAddressTable( )
    {
      //@ should be fixed to use codes instead of text 
      addElement("","select one");
      addElement("Business Address on Application", "Business Address on Application" );
      addElement("Mailing Address on Application", "Mailing Address on Application" );
      addElement("Other", "Other" );
    }
  }
  
  protected class LocationTypeTable extends DropDownTable
  {
    public LocationTypeTable( )
    {
      //@ should be fixed to use codes instead of text 
      addElement("", "select one");
      addElement("Office Building", "Office Building" );
      addElement("Industrial Park", "Industrial Park" );
      addElement("Strip Mall", "Strip Mall" );
      addElement("Residence", "Residence" );
      addElement("Other", "Other" );
    }
  }
  
  protected class PricingTableDefault extends DropDownTable
  {
    private double[]                    ExcludeList     = null;

    public PricingTableDefault( )
    {
      // exclude $0.00 by default
      ExcludeList = ExcludeZero;
      init();
    }
    
    public PricingTableDefault( double[] excluded )
    {
      ExcludeList = excluded;
      init();
    }
    
    private boolean exclude( double perItemAmount )
    {
      boolean       retVal        = false;
      
      if ( ExcludeList != null )
      {
        for( int i = 0; i < ExcludeList.length; ++i )
        {
          if ( perItemAmount == ExcludeList[i] )
          {
            retVal = true;
            break;
          }
        }
      }
      return( retVal );
    }
    
    private void init( )
    {
      addElement("", "select one");
      
      // setup the default pricing between $0.00 and $0.30
      for( double perItem = 0.0; perItem <= 0.30; perItem += 0.05 )
      {
        if ( ! exclude( perItem ) )
        {
          addElement( NumberFormatter.getDoubleString(perItem,"###0.##"),
                      MesMath.toFractionalCurrency(perItem,3) );
        }
      }
    }
  }
  
  protected class PricingTableDebit extends DropDownTable
  {
    public PricingTableDebit( )
    {
      addElement("", "select one");
      addElement("0.4" , "$.40/$.63");
      addElement("0.6" , "$.60/$.63");
    }
  }
  
  protected class PricingGridTable extends DropDownTable
  {
    public PricingGridTable( )
    {
      ResultSetIterator               it        = null;
      ResultSet                       resultSet = null;
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:191^9*/

//  ************************************************************
//  #sql [Ctx] it = { select ic.grid_type,
//                   ic.grid_desc
//            from   interchange_cost ic,
//                   application      app
//            where  app.app_seq_num = :AppSeqNum and
//                   (
//                     nvl(ic.app_type,-1) = -1 or
//                     ic.app_type = app.app_type
//                   )
//            order by grid_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select ic.grid_type,\n                 ic.grid_desc\n          from   interchange_cost ic,\n                 application      app\n          where  app.app_seq_num =  :1  and\n                 (\n                   nvl(ic.app_type,-1) = -1 or\n                   ic.app_type = app.app_type\n                 )\n          order by grid_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.PricingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:203^9*/          
        resultSet = it.getResultSet();
      
        addElement("","select one");
        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      catch( java.sql.SQLException e )
      {
        logEntry( "PricingGridTable()", e.toString() );
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) {}
        cleanUp();
      }
    }
  }
  
  //**************************************************************************
  // Validations
  //**************************************************************************
  protected class FulfillmentHouseValidation implements Validation
  {
    String                  ErrorMessage      = null;
    FieldGroup              Fields            = null;
    
    public FulfillmentHouseValidation( FieldGroup fields )
    {
      Fields = fields;
    }
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }
    
    public boolean validate( String fdata )
    {
      ErrorMessage = null;
      
      // isBlank prevents NullPointerException
      if ( !isBlank(fdata) && fdata.toUpperCase().equals("Y") )
      {
        if (  Fields.getField("fulfillmentAddr").isBlank() ||
              Fields.getField("fulfillmentCity").isBlank() ||
              Fields.getField("fulfillmentState").isBlank() ||
              Fields.getField("fulfillmentZip").isBlank() )
        {
          ErrorMessage = "Please provide the full address of the Fulfillment House";
        }                
      }
      return( ErrorMessage == null );
    }
  }
  
  protected class LocationAddressValidation implements Validation
  {
    String                  ErrorMessage      = null;
    FieldGroup              Fields            = null;
    
    public LocationAddressValidation( FieldGroup fields )
    {
      Fields = fields;
    }
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }
    
    public boolean validate( String fdata )
    {
      ErrorMessage = null;
      
      try
      {
        if ( fdata.equals("Other") )
        {
          if (  Fields.getField("locationAddr").isBlank() ||
                Fields.getField("locationCity").isBlank() ||
                Fields.getField("locationState").isBlank() ||
                Fields.getField("locationZip").isBlank() )
          {
            ErrorMessage = "Please provide the full address of the inspected location";
          }                
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Please select the address of the inspected location";
      }        
      return( ErrorMessage == null );
    }
  }
  
  protected class LocationTypeValidation implements Validation
  {
    Field       OtherField          = null;
    
    public LocationTypeValidation( Field otherField )
    {
      OtherField      = otherField;
    }
    
    public String getErrorText() 
    {
      return("Please provide an explanation when selecting Other for the business location");
    }
    
    public boolean validate( String fdata )
    {
      return ( !fdata.equals("Other") || !OtherField.isBlank() );
    }
  }
  
  protected class PricingPlanValidation implements Validation
  {
    String                  ErrorMessage      = null;
    FieldGroup              Fields            = null;
    
    public PricingPlanValidation( FieldGroup fields )
    {
      Fields = fields;
    }
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }
    
    public boolean validate( String fdata )
    {
      ErrorMessage = null;
      
      try
      {
        switch( Integer.parseInt(fdata) )
        {
          case mesConstants.APP_PS_FIXED_RATE:
            if ( Fields.getField("fixedPlanDiscRate").isBlank() )
            {
              ErrorMessage = "Please provide a valid discount rate for the Fixed Rate Plan";
            }
            break;
          
          case mesConstants.APP_PS_INTERCHANGE:
            if ( Fields.getField("icPlanPerItem").isBlank() )
            {
              ErrorMessage = "Please provide a valid interchange per item for the Interchange Plan";
            }
            break;
          
          case mesConstants.APP_PS_FIXED_PLUS_PER_ITEM:
            if ( Fields.getField("fixedPlusDiscRate").isBlank() ||
                 Fields.getField("fixedPlusPerItem").isBlank() )
            {
              ErrorMessage = "Please provide valid discount and per item rates for the Fixed Rate + Per Item Plan";
            }
            break;

          default:
            ErrorMessage = "Please select a valid pricing plan";
            break;
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Please select a valid pricing plan";
      }        
      return( ErrorMessage == null );
    }
  }
  
  //**************************************************************************
  // Data Structures
  //**************************************************************************
  public class EquipmentFee
  {
    public    int                 EquipCount        = 0;
    public    String              EquipDesc         = null;
    protected String              EquipModel        = null;
    protected int                 LendType          = 0;
    public    String              LendTypeDesc      = null;
    
    public EquipmentFee( ResultSet resultSet )
      throws java.sql.SQLException
    {
      Field               field   =   null;
      
      // store the HTML descriptoins
      LendType      = resultSet.getInt("lend_type");
      LendTypeDesc  = resultSet.getString("lend_type_desc");
      EquipDesc     = resultSet.getString("equip_desc");
      EquipModel    = resultSet.getString("equip_model");
      EquipCount    = resultSet.getInt("equip_count");
      
      // add the currency input field
      field = new CurrencyField( getFieldName(),8,10,false);
      field.setData( resultSet.getString("per_item") );
      //fields.add( field );
    }
    
    public String getFieldName( )
    {
      StringBuffer      fieldName     = new StringBuffer();
      
      fieldName.append( EquipModel );
      fieldName.append( "_" );
      fieldName.append( LendType );
      fieldName.append( "_PerItem" );
      
      return( fieldName.toString() );
    }
    
    public double getTotalAmount( )
    {
      double      perItem       = 0.0;
      try
      {
        //perItem = fields.getField(getFieldName()).asDouble();
      }
      catch( Exception e )
      {
      }
      return( EquipCount * perItem );
    }
  }
  
  //**************************************************************************
  // Data Bean
  //**************************************************************************
  protected Vector            EquipmentFees         = new Vector();
  
  public PricingDataBean( )
  {
  }
  
  protected boolean allowAmexZero( )
  {
    String              allowZero     = null;
    boolean             retVal        = false;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:452^7*/

//  ************************************************************
//  #sql [Ctx] { -- true if either split dial OR PIP was selected
//          select  decode( nvl(po.merchpo_split_dial,'N'),
//                          'Y','Y',
//                          nvl(po.merchpo_pip,'N') )        
//          from    merchpayoption          po
//          where   po.app_seq_num = :AppSeqNum and   
//                  cardtype_code = :mesConstants.APP_CT_AMEX  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "-- true if either split dial OR PIP was selected\n        select  decode( nvl(po.merchpo_split_dial,'N'),\n                        'Y','Y',\n                        nvl(po.merchpo_pip,'N') )         \n        from    merchpayoption          po\n        where   po.app_seq_num =  :1  and   \n                cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.PricingDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_AMEX);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   allowZero = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:461^7*/
      retVal = allowZero.equals("Y");
    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public String getCardTypeDesc( int ct )
  {
    String          retVal      = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:475^7*/

//  ************************************************************
//  #sql [Ctx] { select  ct.cardtype_desc 
//          from    cardtype    ct
//          where   ct.cardtype_code = :ct
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ct.cardtype_desc  \n        from    cardtype    ct\n        where   ct.cardtype_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.PricingDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,ct);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:480^7*/
    }
    catch( Exception e )
    {
      retVal = "Card Type " + ct;
    }
    return( retVal );
  }
  
  public Vector getEquipmentFees()
  {
    return( EquipmentFees );
  }
  
  public String getMiscChargeDesc( int mc )
  {
    String          retVal      = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:499^7*/

//  ************************************************************
//  #sql [Ctx] { select  md.misc_description 
//          from    miscdescrs        md
//          where   md.misc_code = :mc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  md.misc_description  \n        from    miscdescrs        md\n        where   md.misc_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.PricingDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,mc);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:504^7*/
    }
    catch( Exception e )
    {
      retVal = "Misc Charge " + mc;
    }
    return( retVal );
  }
  
  public int[] getMiscChargeList( )
  {
    return getMiscCharges();
  }
  
  public int getPaymentSolution( )
  {
    int         retVal          = -1;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:523^7*/

//  ************************************************************
//  #sql [Ctx] { select  pc.pos_type   
//          from    merch_pos     mp,
//                  pos_category  pc
//          where   mp.app_seq_num = :AppSeqNum and
//                  pc.pos_code = mp.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pc.pos_type    \n        from    merch_pos     mp,\n                pos_category  pc\n        where   mp.app_seq_num =  :1  and\n                pc.pos_code = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.PricingDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:530^7*/
    }
    catch(Exception e)
    {
      logEntry("getPaymentSolution()",e.toString());
    }
    return( retVal );
  }
  
  protected DropDownTable getPricingTable( int ct )
  {
    DropDownTable       retVal      = null;
    
    switch( ct )
    {
      case mesConstants.APP_CT_DEBIT:
        // debit has custom pricing table
        retVal = new PricingTableDebit();
        break;
        
      case mesConstants.APP_CT_INTERNET:
        // exclude $0.05 and $0.25 per item
        retVal = new PricingTableDefault( new double[] { 0.05, 0.25 } );
        break;
        
      case mesConstants.APP_CT_AMEX:
        if ( allowAmexZero() == true )
        {
          retVal = new PricingTableDefault(null);
          break;
        }
        // else fall through and give the default
        
      default:
        // default list excludes $0.00 
        // it is in 5 cent increments
        // from $0.05 and $0.30 per item.
        retVal = new PricingTableDefault();
        break;
    }
    
    return( retVal );
  }
  
  public String[][] getQuestionList( )
  {
    return( QuestionFields );
  }
  
  protected boolean hasPerItemFee( int cardType )
  {
    int         paySolCardType  = mesConstants.APP_CT_COUNT;
    boolean     retVal          = false;
    int         rowCount        = 0;
    
    try
    {
      switch( getPaymentSolution() )
      {
        case mesConstants.APP_PAYSOL_INTERNET:
          paySolCardType = mesConstants.APP_CT_INTERNET;
          break;
          
        case mesConstants.APP_PAYSOL_DIAL_PAY:
          paySolCardType = mesConstants.APP_CT_INTERNET;
          break;
      }
      
      // some payment solution (i.e. internet) have implied
      // card types that are always enabled.  If this is 
      // one of those, then make sure the per item field 
      // gets created.
      if( cardType == paySolCardType )
      {
        retVal = true;
      }
      else      // do the generic lookup
      {
        // true if the payment type was selected with the merchant information
        // AND the payment type supports an additional per item fee outside
        // of discount and interchange related fees.
        /*@lineinfo:generated-code*//*@lineinfo:611^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(po.cardtype_code) 
//            from    merchpayoption        po
//            where   po.app_seq_num = :AppSeqNum and
//                    po.cardtype_code = :cardType and
//                    po.cardtype_code in 
//                      ( 
//                        :mesConstants.APP_CT_DINERS_CLUB,
//                        :mesConstants.APP_CT_DISCOVER,
//                        :mesConstants.APP_CT_JCB,
//                        :mesConstants.APP_CT_AMEX,
//                        :mesConstants.APP_CT_DEBIT,
//                        :mesConstants.APP_CT_CHECK_AUTH,
//                        :mesConstants.APP_CT_INTERNET,
//                        :mesConstants.APP_CT_DIAL_PAY,
//                        :mesConstants.APP_CT_EBT
//                      )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(po.cardtype_code)  \n          from    merchpayoption        po\n          where   po.app_seq_num =  :1  and\n                  po.cardtype_code =  :2  and\n                  po.cardtype_code in \n                    ( \n                       :3 ,\n                       :4 ,\n                       :5 ,\n                       :6 ,\n                       :7 ,\n                       :8 ,\n                       :9 ,\n                       :10 ,\n                       :11 \n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.PricingDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,cardType);
   __sJT_st.setInt(3,mesConstants.APP_CT_DINERS_CLUB);
   __sJT_st.setInt(4,mesConstants.APP_CT_DISCOVER);
   __sJT_st.setInt(5,mesConstants.APP_CT_JCB);
   __sJT_st.setInt(6,mesConstants.APP_CT_AMEX);
   __sJT_st.setInt(7,mesConstants.APP_CT_DEBIT);
   __sJT_st.setInt(8,mesConstants.APP_CT_CHECK_AUTH);
   __sJT_st.setInt(9,mesConstants.APP_CT_INTERNET);
   __sJT_st.setInt(10,mesConstants.APP_CT_DIAL_PAY);
   __sJT_st.setInt(11,mesConstants.APP_CT_EBT);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:629^9*/
        retVal = ( rowCount > 0 );
      }        
    }
    catch( Exception e )
    {
    }
    return( retVal );
  }
  
  public void init( )
  {
    String                  desc        = null;
    Field                   field       = null;
    
    super.init();
    
    try
    {    
      // pricing plan
      fields.add( new DiscountField("fixedPlanDiscRate", true) );
      fields.add( new NumberField("icPlanPerItem", 5, 3, true, 2) );
      fields.add( new DiscountField("fixedPlusDiscRate", true) );
      fields.add( new NumberField("fixedPlusPerItem", 5, 3, true, 2) );
      field = new RadioButtonField( "pricingPlan", PricePlanRadioButtons, -1, false, "Please select a Pricing Scenario" );
      field.addValidation( new PricingPlanValidation( fields ) );
      fields.add(field);
      
      // basis point increase
      fields.add( new DisabledCheckboxField("basisPointIncEnable","Basis Point Increase", true) );
      field = new DropDownField("basisPointInc",new BasisPointIncTable(), true);
      field.addValidation( new IfYesNotBlankValidation( fields.getField("basisPointIncEnable"), "Please select a basis point increase" ) );
      fields.add(field);
      
      fields.add( new DisabledCheckboxField("dailyDiscountEnable","Daily Discount", true) );
      fields.add( new DisabledCheckboxField("minDiscountEnable","Minimum Monthly Discount", true) );
      
      // setup the default minimum discount
      field = new HiddenField("minDiscountAmount");
      field.setData("25");  
      fields.add(field);
      
      fields.add( new DropDownField( "pricingGrid", new PricingGridTable(), false ) );

      // scan through all the card types and create a field for each one that:
      // 1) supports a per item fee (outside of discount and interchange) 
      // 2) the user selected under payment options
      // 
      for( int ct = 0; ct < mesConstants.APP_CT_COUNT; ++ct )
      {
        if ( hasPerItemFee(ct) )
        {
          fields.add( new DropDownField( ("perItemFee" + ct), getPricingTable(ct), false ) );
        }
      }
      
      // setup the miscellaneous fees
      int[] miscCharges = getMiscCharges();
      for( int mc = 0; mc < miscCharges.length; ++mc )
      {
        desc = getMiscChargeDesc(miscCharges[mc]);
        fields.add( new CheckboxField( ("miscChargeEnable" + miscCharges[mc]), desc, false ) );
        field = new CurrencyField( ("miscChargeAmount" + miscCharges[mc]), 8, 6, true );
        field.addValidation( new IfYesNotBlankValidation( fields.getField("miscChargeEnable" + miscCharges[mc]), "Please provide a valid fee amount for the " + desc ) );
        field.addValidation( new IfNoBlankValidation( fields.getField("miscChargeEnable" + miscCharges[mc]), "Please remove the " + desc + " fee amount or check the checkbox" ) );
        fields.add(field);
      }
      
      // equipment fees require access to the
      // database to retrieve data from previous
      // application pages.  
      initEquipmentFees();
      fields.add( new NumberField( "taxRate", 4, 8, true, 2 ) );
      
      // add the comments field
      fields.add( new TextareaField( "pricingComments", 400, 7, 50, true ) );
      
      // add the site inspection section
      fields.add( new Field( "locationDesc", 100, 37, true ) );
      field = new DropDownField("locationType",new LocationTypeTable(),false);
      field.addValidation( new LocationTypeValidation( fields.getField("locationDesc") ) );
      fields.add(field);
      fields.add( new Field( "locationAddr", 50, 35, true ) );
      fields.add( new Field( "locationCity", 50, 35, true ) );
      fields.add( new DropDownField("locationState", new StateDropDownTable(), true ) );
      fields.add( new ZipField( "locationZip", true, fields.getField("locationState") ) );
      field = new DropDownField("locationAddrType",new LocationAddressTable(), true);
      field.addValidation( new LocationAddressValidation( fields ) );
      fields.add(field);
      
      fields.add( new NumberField( "employeeCount", 6, 3, true, 0 ) );
      
      for( int i = 0; i < QuestionFields.length; ++i )
      {
        fields.add( new DropDownField( QuestionFields[i][Q_FIELD_NAME], new YesNoTable(), true ) );
      }        
      
      fields.add( new Field( "inventoryAddr", 50, 35, true ) );
      fields.add( new Field( "inventoryCity", 50, 35, true ) );
      fields.add( new DropDownField("inventoryState", new StateDropDownTable(), true ) );
      fields.add( new ZipField( "inventoryZip", true, fields.getField("inventoryState") ) );
      
      fields.add( new CurrencyField( "inventoryValue", 12, 12, true ) );
      
      fields.add( new Field( "fulfillmentName", 100, 35, true ) );
      fields.add( new Field( "fulfillmentAddr", 50, 35, true ) );
      fields.add( new Field( "fulfillmentCity", 50, 35, true ) );
      fields.add( new DropDownField("fulfillmentState", new StateDropDownTable(), true ) );
      fields.add( new ZipField( "fulfillmentZip", true, fields.getField("fulfillmentState") ) );
      field = new DropDownField( "fulfillmentHouse", new YesNoTable(), true );
      field.addValidation( new FulfillmentHouseValidation( fields ) );
      fields.add(field);
      
      fields.add( new DropDownField("securitySoftware", new YesNoTable(), true ) );
      field = new Field( "securitySoftwareVendor", 100, 35, true );
      field.addValidation( new IfYesNotBlankValidation( fields.getField("securitySoftware"), "Please specify the name of the security software used at this location" ) );
      fields.add(field);
      
      addHtmlExtra("class=\"formFields\"");
    }
    catch( Exception e )
    {
      logEntry("init()",e.toString());
    }
    finally
    {
    }
  }
  
  protected void initEquipmentFees( )
  {
    ResultSetIterator         it            = null;
    ResultSet                 resultSet     = null;  
    
    try
    {
      EquipmentFees.removeAllElements();
      
      /*@lineinfo:generated-code*//*@lineinfo:767^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  eq.equip_model                          as equip_model,
//                  decode( po.prod_option_des,
//                          null, eq.equip_descriptor,
//                          ( eq.equip_descriptor || '-' ||
//                            po.prod_option_des ) )        as equip_desc,
//                  lt.equiplendtype_code                   as lend_type,
//                  lt.equiplendtype_description            as lend_type_desc,
//                  me.merchequip_equip_quantity            as equip_count,
//                  me.merchequip_amount                    as per_item
//          from    merchequipment    me,
//                  equipment         eq,
//                  equiplendtype     lt,
//                  prodoption        po
//          where   me.app_seq_num = :AppSeqNum and
//                  -- all of the buy, rent or lease items
//                  me.equiplendtype_code in 
//                  (
//                    :mesConstants.APP_EQUIP_PURCHASE,         -- 1,
//                    :mesConstants.APP_EQUIP_RENT,             -- 2,
//                    :mesConstants.APP_EQUIP_BUY_REFURBISHED,  -- 4,
//                    :mesConstants.APP_EQUIP_LEASE             -- 5              
//                  ) and
//                  eq.equip_model = me.equip_model and
//                  po.prod_option_id(+) = me.prod_option_id and
//                  lt.equiplendtype_code = me.equiplendtype_code
//          order by me.equiplendtype_code, eq.equiptype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  eq.equip_model                          as equip_model,\n                decode( po.prod_option_des,\n                        null, eq.equip_descriptor,\n                        ( eq.equip_descriptor || '-' ||\n                          po.prod_option_des ) )        as equip_desc,\n                lt.equiplendtype_code                   as lend_type,\n                lt.equiplendtype_description            as lend_type_desc,\n                me.merchequip_equip_quantity            as equip_count,\n                me.merchequip_amount                    as per_item\n        from    merchequipment    me,\n                equipment         eq,\n                equiplendtype     lt,\n                prodoption        po\n        where   me.app_seq_num =  :1  and\n                -- all of the buy, rent or lease items\n                me.equiplendtype_code in \n                (\n                   :2 ,         -- 1,\n                   :3 ,             -- 2,\n                   :4 ,  -- 4,\n                   :5              -- 5              \n                ) and\n                eq.equip_model = me.equip_model and\n                po.prod_option_id(+) = me.prod_option_id and\n                lt.equiplendtype_code = me.equiplendtype_code\n        order by me.equiplendtype_code, eq.equiptype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_RENT);
   __sJT_st.setInt(4,mesConstants.APP_EQUIP_BUY_REFURBISHED);
   __sJT_st.setInt(5,mesConstants.APP_EQUIP_LEASE);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.app.PricingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:795^7*/
      resultSet = it.getResultSet();
    
      while(resultSet.next())
      {
        EquipmentFees.addElement( new EquipmentFee( resultSet ) );
      }
      resultSet.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:805^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  null                                    as equip_model,
//                  'Equipment Support Fee'                 as equip_desc,
//                  me.equiplendtype_code                   as lend_type,
//                  lt.equiplendtype_description            as lend_type_desc,
//                  sum(me.merchequip_equip_quantity)       as equip_count,
//                  avg(me.merchequip_amount)               as per_item
//          from    merchequipment    me,
//                  equipment         eq,
//                  equiplendtype     lt,
//                  prodoption        po
//          where   me.app_seq_num = :AppSeqNum and
//                  -- all of the owned equipment except imprinters
//                  me.equiplendtype_code  = :mesConstants.APP_EQUIP_OWNED and
//                  me.equiptype_code <> :mesConstants.APP_EQUIP_TYPE_IMPRINTER and
//                  eq.equip_model = me.equip_model and
//                  po.prod_option_id(+) = me.prod_option_id and
//                  lt.equiplendtype_code = me.equiplendtype_code
//          group by me.equiplendtype_code, lt.equiplendtype_description                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  null                                    as equip_model,\n                'Equipment Support Fee'                 as equip_desc,\n                me.equiplendtype_code                   as lend_type,\n                lt.equiplendtype_description            as lend_type_desc,\n                sum(me.merchequip_equip_quantity)       as equip_count,\n                avg(me.merchequip_amount)               as per_item\n        from    merchequipment    me,\n                equipment         eq,\n                equiplendtype     lt,\n                prodoption        po\n        where   me.app_seq_num =  :1  and\n                -- all of the owned equipment except imprinters\n                me.equiplendtype_code  =  :2  and\n                me.equiptype_code <>  :3  and\n                eq.equip_model = me.equip_model and\n                po.prod_option_id(+) = me.prod_option_id and\n                lt.equiplendtype_code = me.equiplendtype_code\n        group by me.equiplendtype_code, lt.equiplendtype_description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_OWNED);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_TYPE_IMPRINTER);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.app.PricingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:825^7*/
      resultSet = it.getResultSet();
    
      if(resultSet.next())
      {
        EquipmentFees.addElement( new EquipmentFee( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry("initEquipmentFees()",e.toString());
    }      
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  public void loadData( )
  {
    try
    { 
      loadPricingPlan();
      loadNonBankFees();
      loadMiscFees();
      // equipment is loaded during the field initialization
      loadSiteInspection();
    }
    catch( Exception e )
    {
      logEntry("loadData()",e.toString());
    }
    finally
    {
    }
  }
  
  protected void loadMiscFees( )
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      // load charges from database
      /*@lineinfo:generated-code*//*@lineinfo:872^7*/

//  ************************************************************
//  #sql [Ctx] it = { select misc_code                  misc_code,
//                 misc_chrg_amount           charge_amount,
//                 decode(misc_chrg_amount,
//                        null,'n',0,'n','y') charge_enable
//                        
//          from   miscchrg
//          
//          where  app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select misc_code                  misc_code,\n               misc_chrg_amount           charge_amount,\n               decode(misc_chrg_amount,\n                      null,'n',0,'n','y') charge_enable\n                      \n        from   miscchrg\n        \n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.app.PricingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:882^7*/
      rs = it.getResultSet();

      // set the charge info in the fields
      boolean defaultsCleared = false;
      int[] miscCharges = getMiscCharges();
      while(rs.next())
      {
        // clear out any default field data
        if (!defaultsCleared)
        {
          for (int i = 0; i < miscCharges.length; ++i)
          {
            fields.setData("miscChargeEnable" + miscCharges[i],"n");
            fields.setData("miscChargeAmount" + miscCharges[i],"");
          }
          defaultsCleared = true;
        }
      
        int miscCode = rs.getInt("misc_code");
        for( int i = 0; i < miscCharges.length; ++i )
        {
          if ( miscCode == miscCharges[i] )
          {
            String chargeAmount = Float.toString(rs.getFloat("charge_amount"));
            fields.setData("miscChargeEnable" + miscCode,
                            rs.getString("charge_enable"));
            fields.setData("miscChargeAmount" + miscCode,chargeAmount);
            break;
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadMiscFees()",e.toString());
      addError("loadMiscFees: " + e.toString());
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  protected void loadNonBankFees( )
  {
    Field                     field       = null;
    ResultSetIterator         it          = null;
    ResultSet                 resultSet   = null;
    
    try 
    {
      /*@lineinfo:generated-code*//*@lineinfo:935^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  po.cardtype_code                    as card_type,
//                  nvl(ct.cardtype_desc,
//                      to_char(po.cardtype_code))      as card_desc,
//                  po.merchpo_card_merch_number        as account_number,
//                  tc.tranchrg_per_tran                as per_item
//          from    merchpayoption        po,
//                  tranchrg              tc,
//                  cardtype              ct
//          where   po.app_seq_num = :AppSeqNum and
//                  po.cardtype_code in 
//                    ( 
//                      :mesConstants.APP_CT_DINERS_CLUB,
//                      :mesConstants.APP_CT_DISCOVER,
//                      :mesConstants.APP_CT_JCB,
//                      :mesConstants.APP_CT_AMEX,
//                      :mesConstants.APP_CT_DEBIT,
//                      :mesConstants.APP_CT_CHECK_AUTH,
//                      :mesConstants.APP_CT_INTERNET,
//                      :mesConstants.APP_CT_DIAL_PAY,
//                      :mesConstants.APP_CT_EBT
//                    ) and
//                  tc.app_seq_num(+)   = po.app_seq_num and
//                  tc.cardtype_code(+) = po.cardtype_code and
//                  ct.cardtype_code(+) = po.cardtype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  po.cardtype_code                    as card_type,\n                nvl(ct.cardtype_desc,\n                    to_char(po.cardtype_code))      as card_desc,\n                po.merchpo_card_merch_number        as account_number,\n                tc.tranchrg_per_tran                as per_item\n        from    merchpayoption        po,\n                tranchrg              tc,\n                cardtype              ct\n        where   po.app_seq_num =  :1  and\n                po.cardtype_code in \n                  ( \n                     :2 ,\n                     :3 ,\n                     :4 ,\n                     :5 ,\n                     :6 ,\n                     :7 ,\n                     :8 ,\n                     :9 ,\n                     :10 \n                  ) and\n                tc.app_seq_num(+)   = po.app_seq_num and\n                tc.cardtype_code(+) = po.cardtype_code and\n                ct.cardtype_code(+) = po.cardtype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_DINERS_CLUB);
   __sJT_st.setInt(3,mesConstants.APP_CT_DISCOVER);
   __sJT_st.setInt(4,mesConstants.APP_CT_JCB);
   __sJT_st.setInt(5,mesConstants.APP_CT_AMEX);
   __sJT_st.setInt(6,mesConstants.APP_CT_DEBIT);
   __sJT_st.setInt(7,mesConstants.APP_CT_CHECK_AUTH);
   __sJT_st.setInt(8,mesConstants.APP_CT_INTERNET);
   __sJT_st.setInt(9,mesConstants.APP_CT_DIAL_PAY);
   __sJT_st.setInt(10,mesConstants.APP_CT_EBT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.app.PricingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:961^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        field = fields.getField("perItemFee" + resultSet.getString("card_type"));
        if (field != null)
        {
          field.setData( resultSet.getString("per_item") );
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadNonBankFees()",e.toString());
      addError("loadNonBankFees: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) { }
    }
  }
  
  protected void loadPricingPlan( )
  {
    ResultSetIterator         it          = null;
    ResultSet                 resultSet   = null;
    
    try 
    {
      /*@lineinfo:generated-code*//*@lineinfo:991^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mr.pricing_grid                   as pricing_grid,
//                  mr.merch_basis_pt_increase        as basis_point_inc,
//                  decode(mr.merch_basis_pt_increase,
//                        null, 'n', 'y' )            as basis_point_inc_enable
//          from    merchant      mr
//          where   mr.app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mr.pricing_grid                   as pricing_grid,\n                mr.merch_basis_pt_increase        as basis_point_inc,\n                decode(mr.merch_basis_pt_increase,\n                      null, 'n', 'y' )            as basis_point_inc_enable\n        from    merchant      mr\n        where   mr.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.app.PricingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:999^7*/
      setFields(it.getResultSet());
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:1003^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tranchrg_disc_rate                as disc_rate,
//                  tranchrg_pass_thru                as per_tran,
//                  tranchrg_discrate_type            as pricing_plan,
//                  tranchrg_mmin_chrg                as min_disc_amount,
//                  decode( tranchrg_mmin_chrg,
//                          null, 'n',
//                          0, 'n',
//                          'y' )                     as min_disc_enable
//          from    tranchrg                  
//          where   app_seq_num = :AppSeqNum and
//                  cardtype_code = :mesConstants.APP_CT_VISA
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tranchrg_disc_rate                as disc_rate,\n                tranchrg_pass_thru                as per_tran,\n                tranchrg_discrate_type            as pricing_plan,\n                tranchrg_mmin_chrg                as min_disc_amount,\n                decode( tranchrg_mmin_chrg,\n                        null, 'n',\n                        0, 'n',\n                        'y' )                     as min_disc_enable\n        from    tranchrg                  \n        where   app_seq_num =  :1  and\n                cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.app.PricingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1016^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        fields.getField("pricingPlan").setData(resultSet.getString("pricing_plan"));
        
        switch( fields.getField("pricingPlan").asInteger() )
        {
          case mesConstants.APP_PS_FIXED_RATE:
            fields.getField("fixedPlanDiscRate").setData( resultSet.getString("disc_rate") );
            break;
            
          case mesConstants.APP_PS_INTERCHANGE:
            fields.getField("icPlanPerItem").setData( resultSet.getString("per_tran") );
            break;

          case mesConstants.APP_PS_FIXED_PLUS_PER_ITEM:
            fields.getField("fixedPlusDiscRate").setData( resultSet.getString("disc_rate") );
            fields.getField("fixedPlusPerItem").setData( resultSet.getString("per_tran") );
            break;
        }
        
        // set the min discount amount and enable
        setFields(resultSet,false);
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadPricingPlan()",e.toString());
      addError("loadPricingPlan: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) { }
    }
  }
  
  protected void loadSiteInspection( )
  {
    ResultSetIterator         it          = null;
    
    try 
    {
      /*@lineinfo:generated-code*//*@lineinfo:1062^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  siteinsp_comment                  as pricing_comments,
//                  siteinsp_name_flag                as q_name_match,
//                  siteinsp_inv_sign_flag            as q_signage,
//                  siteinsp_bus_hours_flag           as q_hours_posted,
//                  siteinsp_inv_viewed_flag          as q_inventory_review,
//                  siteinsp_inv_consistant_flag      as q_inventory_consistent,
//                  siteinsp_vol_flag                 as q_inventory_adequate,
//                  siteinsp_no_of_emp                as employee_count,
//                  siteinsp_inv_street               as inventory_addr,
//                  siteinsp_inv_city                 as inventory_city,
//                  siteinsp_inv_state                as inventory_state,
//                  siteinsp_inv_zip                  as inventory_zip,
//                  siteinsp_bus_loc                  as location_type,
//                  siteinsp_bus_loc_comment          as location_desc,
//                  siteinsp_bus_address              as location_addr_type,
//                  siteinsp_bus_street               as location_addr,
//                  siteinsp_bus_city                 as location_city,
//                  siteinsp_bus_state                as location_state,
//                  siteinsp_bus_zip                  as location_zip,
//                  siteinsp_inv_value                as inventory_value,
//                  siteinsp_full_flag                as fulfillment_house,
//                  siteinsp_full_name                as fulfillment_name,
//                  siteinsp_full_street              as fulfillment_addr,
//                  siteinsp_full_city                as fulfillment_city,
//                  siteinsp_full_state               as fulfillment_state,
//                  siteinsp_full_zip                 as fulfillment_zip,
//                  siteinsp_soft_flag                as security_software,
//                  siteinsp_soft_name                as security_software_vendor
//          from    siteinspection            si
//          where   si.app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  siteinsp_comment                  as pricing_comments,\n                siteinsp_name_flag                as q_name_match,\n                siteinsp_inv_sign_flag            as q_signage,\n                siteinsp_bus_hours_flag           as q_hours_posted,\n                siteinsp_inv_viewed_flag          as q_inventory_review,\n                siteinsp_inv_consistant_flag      as q_inventory_consistent,\n                siteinsp_vol_flag                 as q_inventory_adequate,\n                siteinsp_no_of_emp                as employee_count,\n                siteinsp_inv_street               as inventory_addr,\n                siteinsp_inv_city                 as inventory_city,\n                siteinsp_inv_state                as inventory_state,\n                siteinsp_inv_zip                  as inventory_zip,\n                siteinsp_bus_loc                  as location_type,\n                siteinsp_bus_loc_comment          as location_desc,\n                siteinsp_bus_address              as location_addr_type,\n                siteinsp_bus_street               as location_addr,\n                siteinsp_bus_city                 as location_city,\n                siteinsp_bus_state                as location_state,\n                siteinsp_bus_zip                  as location_zip,\n                siteinsp_inv_value                as inventory_value,\n                siteinsp_full_flag                as fulfillment_house,\n                siteinsp_full_name                as fulfillment_name,\n                siteinsp_full_street              as fulfillment_addr,\n                siteinsp_full_city                as fulfillment_city,\n                siteinsp_full_state               as fulfillment_state,\n                siteinsp_full_zip                 as fulfillment_zip,\n                siteinsp_soft_flag                as security_software,\n                siteinsp_soft_name                as security_software_vendor\n        from    siteinspection            si\n        where   si.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.app.PricingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1094^7*/        
      setFields(it.getResultSet());
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadSiteInspection()",e.toString());
      addError("loadSiteInspection: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) { }
    }
  }

  public void storeData( )
  {
    try
    {
      storeTransactionFees();     // all transaction related charges
      storeMiscFees();
      storeEquipmentFees();
      storeSiteInspection();
      markPageComplete();         // mark this page done in screen_progress
    }
    catch( Exception e )
    {
      logEntry("storeData()",e.toString());
    }
    finally
    {
    }
  }
  
  protected void storeEquipmentFees( )
  {
    EquipmentFee            equipFee        = null;
    Field                   field           = null;
    
    try
    {
      for( int i = 0; i < EquipmentFees.size(); ++i )
      {
        // extract the current equipment fee and corresponding field
        equipFee      = (EquipmentFee) EquipmentFees.elementAt(i);
        field         = fields.getField( equipFee.getFieldName() );
        
        /*@lineinfo:generated-code*//*@lineinfo:1141^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchequipment me
//            set     me.merchequip_amount = :field.getData()
//            where   me.app_seq_num = :AppSeqNum and
//                    me.equip_model = 
//                      -- if the equipment model is null 
//                      -- and the lend type is owned then
//                      -- force a match so that all owned
//                      -- equipment gets assigned the same amount
//                      decode( :equipFee.EquipModel,
//                              null, decode( :equipFee.LendType,
//                                            :mesConstants.APP_EQUIP_OWNED, me.equip_model,
//                                            'NONE' ),
//                              :equipFee.EquipModel ) and
//                    me.equiplendtype_code = :equipFee.LendType 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2341 = field.getData();
   String theSqlTS = "update  merchequipment me\n          set     me.merchequip_amount =  :1 \n          where   me.app_seq_num =  :2  and\n                  me.equip_model = \n                    -- if the equipment model is null \n                    -- and the lend type is owned then\n                    -- force a match so that all owned\n                    -- equipment gets assigned the same amount\n                    decode(  :3 ,\n                            null, decode(  :4 ,\n                                           :5 , me.equip_model,\n                                          'NONE' ),\n                             :6  ) and\n                  me.equiplendtype_code =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2341);
   __sJT_st.setLong(2,AppSeqNum);
   __sJT_st.setString(3,equipFee.EquipModel);
   __sJT_st.setInt(4,equipFee.LendType);
   __sJT_st.setInt(5,mesConstants.APP_EQUIP_OWNED);
   __sJT_st.setString(6,equipFee.EquipModel);
   __sJT_st.setInt(7,equipFee.LendType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1157^9*/
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("storeEquipmentFees()",e.toString());
    }      
    finally
    {
    }
  }
  
  protected void storeMiscFees( )
  {
    Field             field       = null;
    
    try 
    {
      for ( int mc = 0; mc < mesConstants.APP_MISC_CHARGE_COUNT; ++mc )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1177^9*/

//  ************************************************************
//  #sql [Ctx] { delete  
//            from   miscchrg
//            where  app_seq_num = :AppSeqNum and
//                   misc_code = :mc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  \n          from   miscchrg\n          where  app_seq_num =  :1  and\n                 misc_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mc);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1183^9*/
        
        field = fields.getField("miscChargeEnable"+mc);
        if ( field != null && field.getData().toUpperCase().equals("Y") )
        {
          /*@lineinfo:generated-code*//*@lineinfo:1188^11*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//              (
//                app_seq_num,
//                misc_code,
//                misc_chrg_amount
//              )
//              values
//              (
//                :AppSeqNum,
//                :mc,
//                :fields.getField("miscChargeAmount"+mc).getData()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2342 = fields.getField("miscChargeAmount"+mc).getData();
   String theSqlTS = "insert into miscchrg\n            (\n              app_seq_num,\n              misc_code,\n              misc_chrg_amount\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mc);
   __sJT_st.setString(3,__sJT_2342);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1202^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("storeMiscFees()",e.toString());
      addError("storeMiscFees: " + e.toString());
    }
    finally
    {
    }
  }
  
  protected void storeSiteInspection( )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1220^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    siteinspection
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    siteinspection\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1225^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:1227^7*/

//  ************************************************************
//  #sql [Ctx] { insert into siteinspection
//          (
//            app_seq_num,
//            siteinsp_comment,
//            siteinsp_name_flag,
//            siteinsp_inv_sign_flag,
//            siteinsp_bus_hours_flag,
//            siteinsp_inv_viewed_flag,
//            siteinsp_inv_consistant_flag,
//            siteinsp_vol_flag,
//            siteinsp_full_flag,
//            siteinsp_soft_flag,
//            siteinsp_inv_street,
//            siteinsp_inv_city,
//            siteinsp_inv_state,
//            siteinsp_inv_zip,
//            siteinsp_full_street,
//            siteinsp_full_city,
//            siteinsp_full_state,
//            siteinsp_full_zip,
//            siteinsp_bus_street,
//            siteinsp_bus_city,
//            siteinsp_bus_state,
//            siteinsp_bus_zip,
//            siteinsp_inv_value,
//            siteinsp_full_name,
//            siteinsp_no_of_emp,
//            siteinsp_bus_loc,
//            siteinsp_bus_loc_comment,
//            siteinsp_bus_address,
//            siteinsp_soft_name
//          )
//          values
//          ( 
//            :AppSeqNum,
//            :fields.getField("pricingComments").getData(),
//            :fields.getField("qNameMatch").getData().toUpperCase(),
//            :fields.getField("qSignage").getData().toUpperCase(),
//            :fields.getField("qHoursPosted").getData().toUpperCase(),
//            :fields.getField("qInventoryReview").getData().toUpperCase(),
//            :fields.getField("qInventoryConsistent").getData().toUpperCase(),
//            :fields.getField("qInventoryAdequate").getData().toUpperCase(),
//            :fields.getField("fulfillmentHouse").getData().toUpperCase(),
//            :fields.getField("securitySoftware").getData().toUpperCase(),
//            :fields.getField("inventoryAddr").getData(),
//            :fields.getField("inventoryCity").getData(),
//            :fields.getField("inventoryState").getData(),
//            :fields.getField("inventoryZip").getData(),
//            :fields.getField("fulfillmentAddr").getData(),
//            :fields.getField("fulfillmentCity").getData(),
//            :fields.getField("fulfillmentState").getData(),
//            :fields.getField("fulfillmentZip").getData(),
//            :fields.getField("locationAddr").getData(),
//            :fields.getField("locationCity").getData(),
//            :fields.getField("locationState").getData(),
//            :fields.getField("locationZip").getData(),
//            :fields.getField("inventoryValue").getData(),
//            :fields.getField("fulfillmentName").getData(),
//            :fields.getField("employeeCount").getData(),
//            :fields.getField("locationType").getData(),
//            :fields.getField("locationDesc").getData(),
//            :fields.getField("locationAddrType").getData(),
//            :fields.getField("securitySoftwareVendor").getData()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2343 = fields.getField("pricingComments").getData();
 String __sJT_2344 = fields.getField("qNameMatch").getData().toUpperCase();
 String __sJT_2345 = fields.getField("qSignage").getData().toUpperCase();
 String __sJT_2346 = fields.getField("qHoursPosted").getData().toUpperCase();
 String __sJT_2347 = fields.getField("qInventoryReview").getData().toUpperCase();
 String __sJT_2348 = fields.getField("qInventoryConsistent").getData().toUpperCase();
 String __sJT_2349 = fields.getField("qInventoryAdequate").getData().toUpperCase();
 String __sJT_2350 = fields.getField("fulfillmentHouse").getData().toUpperCase();
 String __sJT_2351 = fields.getField("securitySoftware").getData().toUpperCase();
 String __sJT_2352 = fields.getField("inventoryAddr").getData();
 String __sJT_2353 = fields.getField("inventoryCity").getData();
 String __sJT_2354 = fields.getField("inventoryState").getData();
 String __sJT_2355 = fields.getField("inventoryZip").getData();
 String __sJT_2356 = fields.getField("fulfillmentAddr").getData();
 String __sJT_2357 = fields.getField("fulfillmentCity").getData();
 String __sJT_2358 = fields.getField("fulfillmentState").getData();
 String __sJT_2359 = fields.getField("fulfillmentZip").getData();
 String __sJT_2360 = fields.getField("locationAddr").getData();
 String __sJT_2361 = fields.getField("locationCity").getData();
 String __sJT_2362 = fields.getField("locationState").getData();
 String __sJT_2363 = fields.getField("locationZip").getData();
 String __sJT_2364 = fields.getField("inventoryValue").getData();
 String __sJT_2365 = fields.getField("fulfillmentName").getData();
 String __sJT_2366 = fields.getField("employeeCount").getData();
 String __sJT_2367 = fields.getField("locationType").getData();
 String __sJT_2368 = fields.getField("locationDesc").getData();
 String __sJT_2369 = fields.getField("locationAddrType").getData();
 String __sJT_2370 = fields.getField("securitySoftwareVendor").getData();
   String theSqlTS = "insert into siteinspection\n        (\n          app_seq_num,\n          siteinsp_comment,\n          siteinsp_name_flag,\n          siteinsp_inv_sign_flag,\n          siteinsp_bus_hours_flag,\n          siteinsp_inv_viewed_flag,\n          siteinsp_inv_consistant_flag,\n          siteinsp_vol_flag,\n          siteinsp_full_flag,\n          siteinsp_soft_flag,\n          siteinsp_inv_street,\n          siteinsp_inv_city,\n          siteinsp_inv_state,\n          siteinsp_inv_zip,\n          siteinsp_full_street,\n          siteinsp_full_city,\n          siteinsp_full_state,\n          siteinsp_full_zip,\n          siteinsp_bus_street,\n          siteinsp_bus_city,\n          siteinsp_bus_state,\n          siteinsp_bus_zip,\n          siteinsp_inv_value,\n          siteinsp_full_name,\n          siteinsp_no_of_emp,\n          siteinsp_bus_loc,\n          siteinsp_bus_loc_comment,\n          siteinsp_bus_address,\n          siteinsp_soft_name\n        )\n        values\n        ( \n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n           :16 ,\n           :17 ,\n           :18 ,\n           :19 ,\n           :20 ,\n           :21 ,\n           :22 ,\n           :23 ,\n           :24 ,\n           :25 ,\n           :26 ,\n           :27 ,\n           :28 ,\n           :29 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,__sJT_2343);
   __sJT_st.setString(3,__sJT_2344);
   __sJT_st.setString(4,__sJT_2345);
   __sJT_st.setString(5,__sJT_2346);
   __sJT_st.setString(6,__sJT_2347);
   __sJT_st.setString(7,__sJT_2348);
   __sJT_st.setString(8,__sJT_2349);
   __sJT_st.setString(9,__sJT_2350);
   __sJT_st.setString(10,__sJT_2351);
   __sJT_st.setString(11,__sJT_2352);
   __sJT_st.setString(12,__sJT_2353);
   __sJT_st.setString(13,__sJT_2354);
   __sJT_st.setString(14,__sJT_2355);
   __sJT_st.setString(15,__sJT_2356);
   __sJT_st.setString(16,__sJT_2357);
   __sJT_st.setString(17,__sJT_2358);
   __sJT_st.setString(18,__sJT_2359);
   __sJT_st.setString(19,__sJT_2360);
   __sJT_st.setString(20,__sJT_2361);
   __sJT_st.setString(21,__sJT_2362);
   __sJT_st.setString(22,__sJT_2363);
   __sJT_st.setString(23,__sJT_2364);
   __sJT_st.setString(24,__sJT_2365);
   __sJT_st.setString(25,__sJT_2366);
   __sJT_st.setString(26,__sJT_2367);
   __sJT_st.setString(27,__sJT_2368);
   __sJT_st.setString(28,__sJT_2369);
   __sJT_st.setString(29,__sJT_2370);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1293^7*/
    }
    catch(Exception e)
    {
      logEntry( "storeSiteInspection()", e.toString());
      addError( "storeSiteInspection: " + e.toString());
    }
  }
  
  protected void storeTransactionFees( )
  {
    String                  discRate        = null;
    Field                   field           = null;
    String                  perItem         = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1310^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     pricing_grid = :fields.getField("pricingGrid").getData(),
//                  bet_type_code = :fields.getField("pricingGrid").getData(),
//                  merch_basis_pt_increase = :fields.getField("basisPointInc").getData()
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2371 = fields.getField("pricingGrid").getData();
 String __sJT_2372 = fields.getField("pricingGrid").getData();
 String __sJT_2373 = fields.getField("basisPointInc").getData();
   String theSqlTS = "update  merchant\n        set     pricing_grid =  :1 ,\n                bet_type_code =  :2 ,\n                merch_basis_pt_increase =  :3 \n        where   app_seq_num =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2371);
   __sJT_st.setString(2,__sJT_2372);
   __sJT_st.setString(3,__sJT_2373);
   __sJT_st.setLong(4,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1317^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1319^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from  tranchrg
//          where app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from  tranchrg\n        where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1324^7*/

      switch( fields.getField("pricingPlan").asInteger() )
      {
        case mesConstants.APP_PS_FIXED_RATE:
          discRate = fields.getField("fixedPlanDiscRate").getData();
          break;
          
        case mesConstants.APP_PS_INTERCHANGE:
          perItem = fields.getField("icPlanPerItem").getData();
          break;

        case mesConstants.APP_PS_FIXED_PLUS_PER_ITEM:
          discRate = fields.getField("fixedPlusDiscRate").getData();
          perItem = fields.getField("fixedPlusPerItem").getData();
          break;
      }

      /*@lineinfo:generated-code*//*@lineinfo:1342^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran
//          )
//          values
//          (
//            :AppSeqNum,
//            :mesConstants.APP_CT_VISA,
//            :fields.getField("pricingPlan").getData(),
//            :discRate,
//            :perItem,
//            :fields.getField("minDiscountAmount").getData(),
//            0
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2374 = fields.getField("pricingPlan").getData();
 String __sJT_2375 = fields.getField("minDiscountAmount").getData();
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   __sJT_st.setString(3,__sJT_2374);
   __sJT_st.setString(4,discRate);
   __sJT_st.setString(5,perItem);
   __sJT_st.setString(6,__sJT_2375);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1364^7*/
      
      for ( int ct = 0; ct < mesConstants.APP_CT_COUNT; ++ct )
      {
        field = fields.getField( ("perItemFee" + ct) );
        if ( field != null )
        {
          /*@lineinfo:generated-code*//*@lineinfo:1371^11*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//              (
//                app_seq_num,
//                cardtype_code,
//                tranchrg_per_tran
//              )
//              values
//              (
//                :AppSeqNum,
//                :ct,
//                :field.getData()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2376 = field.getData();
   String theSqlTS = "insert into tranchrg\n            (\n              app_seq_num,\n              cardtype_code,\n              tranchrg_per_tran\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.app.PricingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,ct);
   __sJT_st.setString(3,__sJT_2376);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1385^11*/
        }
      }        
    }
    catch(Exception e)
    {
      logEntry("storeTransactionFees()", e.toString());
      addError("storeTransactionFees: " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/