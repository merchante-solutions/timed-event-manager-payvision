/*@lineinfo:filename=Page3Base*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/Page1Base.sqlj $

  Description:

  Page1Base

  Base bean for Page 1 of virtual app (merchant-submission version)

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-04-18 16:44:44 -0700 (Fri, 18 Apr 2008) $
  Version            : $Revision: 14760 $

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.virtual;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TaxIdField;
import com.mes.forms.TextareaField;
import com.mes.forms.ZipField;
import com.mes.support.XSSFilter;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Page3Base extends MerchantFacingAppBase
{
  static Logger log = Logger.getLogger(Page3Base.class);
  
  protected class BusinessTypeTable extends DropDownTable
  {
    public BusinessTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Sole Proprietorship");
      addElement("2","Corporation");
      addElement("3","Partnership");
      addElement("4","Medical or Legal Corporation");
      addElement("5","Association/Estate/Trust");
      addElement("6","Tax Exempt");
      addElement("7","Government");
      addElement("8","Limited Liability Company");
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields( request );
    
    try
    {
      submitPartial = true;
      
      fields.add  (new DropDownField    ("businessType",      "Type of Business",new BusinessTypeTable(),false));
      fields.add  (new Field            ("businessName",      "Business Name (DBA)",25,25,false));
      fields.add  (new Field            ("businessLegalName", "Business Legal Name",50,25,false));
      fields.add  (new TaxIdField       ("taxpayerId",        "Federal Taxpayer ID",true));
      fields.add  (new DateStringField  ("establishedDate",   "Business Established Date",false,true));
      fields.add  (new NumberField      ("locationYears",     "# of Years at Main Location",3,3,false,0));
      fields.add  (new TextareaField    ("businessDesc",      "Business Description",200,5,20,false));
      fields.add  (new Field            ("businessAddress1",  "Business Address Line 1",32,30,false));
      fields.add  (new Field            ("businessAddress2",  "Business Address Line 2",32,30,true));
      fields.add  (new EmailField       ("businessEmail",     "Business e-mail Address",45,30,false));
      fields.add  (new PhoneField       ("businessPhone",     "Business Phone Number",false));
      fields.add  (new PhoneField       ("businessFax",       "Business Fax Number",false));
      fields.add  (new Field            ("businessCity",      "Business City", 25, 25, false));
      fields.add  (new DropDownField    ("businessState",     "Business State", new StateDropDownTable(), false));
      fields.add  (new ZipField         ("businessZip",       "Business Zip", false, fields.getField("businessState")));
      fields.add  (new Field            ("webUrl",            100,45,true));

      // taxpayerid is optional if business type is Sole Proprietorship      
      fields.getField("taxpayerId").addValidation(new OptionalIfValueValidation(
        fields.getField("businessType"), "1", "Required for this business type"));
      
      fields.setHtmlExtra("class=\"formFields\"");
      
      fields.getField("businessType").setHtmlExtra("class=\"formFields\" onChange=\"SetTaxIDView()\"");
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
    }
  }
  
  protected void submitMerchant(long appSeqNum)
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:102^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_business_name         = :fields.getData("businessName"),
//                  merch_federal_tax_id        = :fields.getData("taxpayerId"),
//                  merch_email_address         = :fields.getData("businessEmail"),
//                  merch_legal_name            = :fields.getData("businessLegalName"),
//                  merch_business_establ_month = :fields.getData("establishedDateMonth"),
//                  merch_business_establ_year  = :fields.getData("establishedDateYear"),
//                  bustype_code                = :fields.getData("businessType"),
//                  merch_busgoodserv_descr     = :fields.getData("businessDesc"),
//                  merch_num_of_locations      = 1, --:(fields.getData("numLocations")),
//                  merch_years_at_loc          = :fields.getData("locationYears"),
//                  loctype_code                = 2, --:(fields.getData("locationType")),
//                  merch_application_type      = 1, --:(fields.getData("applicationType")),
//                  merch_prior_cc_accp_flag    = merch_prior_cc_accp_flag, --:(fields.getData("haveProcessed")),
//                  merch_prior_processor       = merch_prior_processor, --:(fields.getData("previousProcessor")),
//                  merch_cc_acct_term_flag     = merch_cc_acct_term_flag, --:(fields.getData("haveCanceled")),
//                  merch_cc_term_name          = merch_cc_term_name, --:(fields.getData("canceledProcessor")),
//                  merch_term_reason           = merch_term_reason, --:(fields.getData("canceledReason")),
//                  merch_term_month            = merch_term_month, --:(fields.getData("cancelDateMonth")),
//                  merch_term_year             = merch_term_year, --:(fields.getData("cancelDateYear")),
//                  merch_month_tot_proj_sales  = merch_month_tot_proj_sales, --:(fields.getData("monthlySales")),
//                  merch_month_visa_mc_sales   = merch_month_visa_mc_sales, --:(fields.getData("monthlyVMCSales")),
//                  annual_sales                = annual_sales, --:(fields.getData("annualSales")),
//                  annual_vmc_sales            = annual_vmc_sales, --:(fields.getData("annualVMCSales")),
//                  merch_average_cc_tran       = merch_average_cc_tran, --:(fields.getData("averageTicket")),
//                  highest_ticket              = highest_ticket, --:(fields.getData("highestTicket")),
//                  merch_mail_phone_sales      = 100, --:(fields.getData("motoPercentage")),
//                  refundtype_code             = refundtype_code, --:(fields.getData("refundPolicy")),
//                  merch_referring_bank        = merch_referring_bank, --:(fields.getData("referringBank")),
//                  merch_prior_statements      = merch_prior_statements, --:(fields.getData("statementsProvided")),
//                  merch_gender                = merch_gender, --:(fields.getData("owner1Gender")),
//                  asso_number                 = asso_number, --:(fields.getData("assocNum")),
//                  account_type                = 'N', --:(fields.getData("accountType")),
//                  svb_cif_num                 = svb_cif_num, --:(fields.getData("cif")),
//                  app_sic_code                = app_sic_code, --:(fields.getData("sicCode")),
//                  seasonal_flag               = seasonal_flag, --:seasonalFlag,
//                  seasonal_months             = seasonal_months, --:seasonalMonths,
//                  ebt_options                 = ebt_options, --:(fields.getData("ebtOptions")),
//                  percent_internet            = 100, --:(fields.getData("internetPercent")),
//                  percent_card_present        = 0, --:(fields.getData("cardPercent")),
//                  percent_card_not_present    = 100, --:(fields.getData("noCardPercent")),
//                  merch_web_url               = :fields.getData("webUrl"),
//                  merch_notes                 = merch_notes, --:(fields.getData("comments")),
//                  existing_verisign           = existing_verisign, --:(fields.getData("existingVerisign")),
//                  total_num_multi_merch       = total_num_multi_merch, --:(fields.getData("multiMerchTotal")),
//                  num_multi_merch             = num_multi_merch, --:(fields.getData("multiMerchNum")),
//                  multi_merch_master_dba      = multi_merch_master_dba, --:(fields.getData("multiMerchMasterDba")),
//                  multi_merch_master_cntrl    = multi_merch_master_cntrl, --:(fields.getData("multiMerchMasterControl")),
//                  vmc_accept_types            = vmc_accept_types, --:(fields.getData("vmcAcceptTypes")),
//                  customer_service_phone      = :fields.getData("businessPhone")
//          where   app_seq_num                 = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3312 = fields.getData("businessName");
 String __sJT_3313 = fields.getData("taxpayerId");
 String __sJT_3314 = fields.getData("businessEmail");
 String __sJT_3315 = fields.getData("businessLegalName");
 String __sJT_3316 = fields.getData("establishedDateMonth");
 String __sJT_3317 = fields.getData("establishedDateYear");
 String __sJT_3318 = fields.getData("businessType");
 String __sJT_3319 = fields.getData("businessDesc");
 String __sJT_3320 = fields.getData("locationYears");
 String __sJT_3321 = fields.getData("webUrl");
 String __sJT_3322 = fields.getData("businessPhone");
   String theSqlTS = "update  merchant\n        set     merch_business_name         =  :1 ,\n                merch_federal_tax_id        =  :2 ,\n                merch_email_address         =  :3 ,\n                merch_legal_name            =  :4 ,\n                merch_business_establ_month =  :5 ,\n                merch_business_establ_year  =  :6 ,\n                bustype_code                =  :7 ,\n                merch_busgoodserv_descr     =  :8 ,\n                merch_num_of_locations      = 1, --:(fields.getData(\"numLocations\")),\n                merch_years_at_loc          =  :9 ,\n                loctype_code                = 2, --:(fields.getData(\"locationType\")),\n                merch_application_type      = 1, --:(fields.getData(\"applicationType\")),\n                merch_prior_cc_accp_flag    = merch_prior_cc_accp_flag, --:(fields.getData(\"haveProcessed\")),\n                merch_prior_processor       = merch_prior_processor, --:(fields.getData(\"previousProcessor\")),\n                merch_cc_acct_term_flag     = merch_cc_acct_term_flag, --:(fields.getData(\"haveCanceled\")),\n                merch_cc_term_name          = merch_cc_term_name, --:(fields.getData(\"canceledProcessor\")),\n                merch_term_reason           = merch_term_reason, --:(fields.getData(\"canceledReason\")),\n                merch_term_month            = merch_term_month, --:(fields.getData(\"cancelDateMonth\")),\n                merch_term_year             = merch_term_year, --:(fields.getData(\"cancelDateYear\")),\n                merch_month_tot_proj_sales  = merch_month_tot_proj_sales, --:(fields.getData(\"monthlySales\")),\n                merch_month_visa_mc_sales   = merch_month_visa_mc_sales, --:(fields.getData(\"monthlyVMCSales\")),\n                annual_sales                = annual_sales, --:(fields.getData(\"annualSales\")),\n                annual_vmc_sales            = annual_vmc_sales, --:(fields.getData(\"annualVMCSales\")),\n                merch_average_cc_tran       = merch_average_cc_tran, --:(fields.getData(\"averageTicket\")),\n                highest_ticket              = highest_ticket, --:(fields.getData(\"highestTicket\")),\n                merch_mail_phone_sales      = 100, --:(fields.getData(\"motoPercentage\")),\n                refundtype_code             = refundtype_code, --:(fields.getData(\"refundPolicy\")),\n                merch_referring_bank        = merch_referring_bank, --:(fields.getData(\"referringBank\")),\n                merch_prior_statements      = merch_prior_statements, --:(fields.getData(\"statementsProvided\")),\n                merch_gender                = merch_gender, --:(fields.getData(\"owner1Gender\")),\n                asso_number                 = asso_number, --:(fields.getData(\"assocNum\")),\n                account_type                = 'N', --:(fields.getData(\"accountType\")),\n                svb_cif_num                 = svb_cif_num, --:(fields.getData(\"cif\")),\n                app_sic_code                = app_sic_code, --:(fields.getData(\"sicCode\")),\n                seasonal_flag               = seasonal_flag, --:seasonalFlag,\n                seasonal_months             = seasonal_months, --:seasonalMonths,\n                ebt_options                 = ebt_options, --:(fields.getData(\"ebtOptions\")),\n                percent_internet            = 100, --:(fields.getData(\"internetPercent\")),\n                percent_card_present        = 0, --:(fields.getData(\"cardPercent\")),\n                percent_card_not_present    = 100, --:(fields.getData(\"noCardPercent\")),\n                merch_web_url               =  :10 ,\n                merch_notes                 = merch_notes, --:(fields.getData(\"comments\")),\n                existing_verisign           = existing_verisign, --:(fields.getData(\"existingVerisign\")),\n                total_num_multi_merch       = total_num_multi_merch, --:(fields.getData(\"multiMerchTotal\")),\n                num_multi_merch             = num_multi_merch, --:(fields.getData(\"multiMerchNum\")),\n                multi_merch_master_dba      = multi_merch_master_dba, --:(fields.getData(\"multiMerchMasterDba\")),\n                multi_merch_master_cntrl    = multi_merch_master_cntrl, --:(fields.getData(\"multiMerchMasterControl\")),\n                vmc_accept_types            = vmc_accept_types, --:(fields.getData(\"vmcAcceptTypes\")),\n                customer_service_phone      =  :11 \n        where   app_seq_num                 =  :12";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.app.virtual.Page3Base",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3312);
   __sJT_st.setString(2,__sJT_3313);
   __sJT_st.setString(3,__sJT_3314);
   __sJT_st.setString(4,__sJT_3315);
   __sJT_st.setString(5,__sJT_3316);
   __sJT_st.setString(6,__sJT_3317);
   __sJT_st.setString(7,__sJT_3318);
   __sJT_st.setString(8,__sJT_3319);
   __sJT_st.setString(9,__sJT_3320);
   __sJT_st.setString(10,__sJT_3321);
   __sJT_st.setString(11,__sJT_3322);
   __sJT_st.setLong(12,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:155^7*/
    }
    catch(Exception e)
    {
      logEntry("submitMerchant(" + appSeqNum + ")", e.toString());
      throw( e );
    }
  }
  
  protected void submitAddresses(long appSeqNum)
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:169^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    address
//          where   app_seq_num = :appSeqNum
//                  and addresstype_code = :mesConstants.ADDR_TYPE_BUSINESS
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    address\n        where   app_seq_num =  :1 \n                and addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.virtual.Page3Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_BUSINESS);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:175^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:177^7*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//          (
//            address_name,
//            address_line1,
//            address_line2,
//            address_city,
//            countrystate_code,
//            address_zip,
//            country_code,
//            address_phone,
//            address_fax,
//            app_seq_num,
//            addresstype_code
//          )
//          values
//          (                                    
//            :XSSFilter.encodeXSS(getData("businessName")),
//            :XSSFilter.encodeXSS(getData("businessAddress1")),
//            :XSSFilter.encodeXSS(getData("businessAddress2")),
//            :XSSFilter.encodeXSS(getData("businessCity")),
//            :XSSFilter.encodeXSS(getData("businessState")),
//            :XSSFilter.encodeXSS(getData("businessZip")),
//            :XSSFilter.encodeXSS(getData("businessCountry")),
//            :XSSFilter.encodeXSS(getData("businessPhone")),
//            :XSSFilter.encodeXSS(getData("businessFax")),
//            :appSeqNum,
//            :mesConstants.ADDR_TYPE_BUSINESS
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3323 = XSSFilter.encodeXSS(getData("businessName"));
 String __sJT_3324 = XSSFilter.encodeXSS(getData("businessAddress1"));
 String __sJT_3325 = XSSFilter.encodeXSS(getData("businessAddress2"));
 String __sJT_3326 = XSSFilter.encodeXSS(getData("businessCity"));
 String __sJT_3327 = XSSFilter.encodeXSS(getData("businessState"));
 String __sJT_3328 = XSSFilter.encodeXSS(getData("businessZip"));
 String __sJT_3329 = XSSFilter.encodeXSS(getData("businessCountry"));
 String __sJT_3330 = XSSFilter.encodeXSS(getData("businessPhone"));
 String __sJT_3331 = XSSFilter.encodeXSS(getData("businessFax"));
   String theSqlTS = "insert into address\n        (\n          address_name,\n          address_line1,\n          address_line2,\n          address_city,\n          countrystate_code,\n          address_zip,\n          country_code,\n          address_phone,\n          address_fax,\n          app_seq_num,\n          addresstype_code\n        )\n        values\n        (                                    \n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.virtual.Page3Base",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3323);
   __sJT_st.setString(2,__sJT_3324);
   __sJT_st.setString(3,__sJT_3325);
   __sJT_st.setString(4,__sJT_3326);
   __sJT_st.setString(5,__sJT_3327);
   __sJT_st.setString(6,__sJT_3328);
   __sJT_st.setString(7,__sJT_3329);
   __sJT_st.setString(8,__sJT_3330);
   __sJT_st.setString(9,__sJT_3331);
   __sJT_st.setLong(10,appSeqNum);
   __sJT_st.setInt(11,mesConstants.ADDR_TYPE_BUSINESS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:207^7*/
    }
    catch(Exception e)
    {
      logEntry("submitAddresses("+appSeqNum+")", e.toString());
      throw( e );
    }
  }
  
  protected boolean submitAppData()
  {
    boolean submitOk = false;
    
    try
    {
      connect();
      setAutoCommit(false);
      
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if( appSeqNum != 0 )
      {
        submitMerchant(appSeqNum);
        submitAddresses(appSeqNum);
      }
      
      commit();
      
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitData", e.toString());
      rollback();
    }
    finally
    {
      cleanUp();
    }
    
    return( submitOk );
  }
  
  protected void loadMerchant(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    try
    {
      // load data from merchant
      /*@lineinfo:generated-code*//*@lineinfo:256^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_business_name           business_name,
//                  lpad(to_char(merch_federal_tax_id),
//                    9,'0')                      taxpayer_id,
//                  merch_business_establ_month   established_date_month,
//                  merch_business_establ_year    established_date_year,
//                  merch_legal_name              business_legal_name,
//                  merch_email_address           business_email,
//                  bustype_code                  business_type,
//                  merch_busgoodserv_descr       business_desc,
//                  merch_web_url                 web_url,
//                  merch_years_at_loc            location_years
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_business_name           business_name,\n                lpad(to_char(merch_federal_tax_id),\n                  9,'0')                      taxpayer_id,\n                merch_business_establ_month   established_date_month,\n                merch_business_establ_year    established_date_year,\n                merch_legal_name              business_legal_name,\n                merch_email_address           business_email,\n                bustype_code                  business_type,\n                merch_busgoodserv_descr       business_desc,\n                merch_web_url                 web_url,\n                merch_years_at_loc            location_years\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.virtual.Page3Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.virtual.Page3Base",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:271^7*/
      setFields(it.getResultSet());
    }
    catch (Exception e)
    {
      logEntry("loadMerchant("+appSeqNum+")", e.toString());
      throw( e );
    }
    finally
    {
      it.close();
    }
  }
  
  protected void loadAddresses(long appSeqNum)
  {
    ResultSetIterator it = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:290^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1     business_address_1,
//                  address_line2     business_address_2,
//                  address_city      business_city,
//                  countrystate_code business_state,
//                  address_zip       business_zip,
//                  address_phone     business_phone,
//                  address_fax       business_fax
//          from    address
//          where   app_seq_num = :appSeqNum and
//                  addresstype_code = :mesConstants.ADDR_TYPE_BUSINESS
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1     business_address_1,\n                address_line2     business_address_2,\n                address_city      business_city,\n                countrystate_code business_state,\n                address_zip       business_zip,\n                address_phone     business_phone,\n                address_fax       business_fax\n        from    address\n        where   app_seq_num =  :1  and\n                addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.virtual.Page3Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_BUSINESS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.app.virtual.Page3Base",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:302^7*/
      setFields(it.getResultSet());
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadAddresses("+appSeqNum+")", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  protected boolean loadPageData(long appSeqNum)
  {
    boolean loadOk = false;
    
    try
    {
      if( appSeqNum != 0L )
      {
        loadMerchant(appSeqNum);
        loadAddresses(appSeqNum);
      }
      
      loadOk = true;
    }
    catch(Exception e)
    {
      logEntry("loadAppData()", e.toString());
    }
    
    return( loadOk );
  }
}/*@lineinfo:generated-code*/