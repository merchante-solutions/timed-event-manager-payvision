/*@lineinfo:filename=AppComplete*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/AppDone.sqlj $

  Description:
  
  AppDone
  
  Bean used by app completion page.  To allow it to be used by multiple
  screen sequences this bean will use the appSeqNum to determine what its
  app type and screen sequence id are.  It also causes the app to be
  inserted into the credit queue and automatically marks itself as complete.


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.virtual.wpp;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.virtual.MerchantFacingAppBase;
import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.forms.ImageButtonField;
import com.mes.forms.NumberField;
import com.mes.ops.InsertCreditQueue;
import sqlj.runtime.ResultSetIterator;

public class AppComplete extends MerchantFacingAppBase
{
  {
    appType = 76;
    curScreenId = 7;
  }
  
  /*
  ** protected void createFields(HttpServletRequest request)
  **
  ** Determines app type and screen id based on the app seq num.  Unlike
  ** most page beans, app done is generic and may be used with multiple
  ** screen sequences.  The bean asssumes that it 
  */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    fields.add(new NumberField("minimumDiscount", 10, 10, true, 2));
    fields.add(new NumberField("monthlyServiceFee", 10, 10, true, 2));
    fields.add(new NumberField("chargebackFee", 10, 10, true, 2));
    fields.add(new NumberField("pricingType", 10, 10, true, 0));
    fields.add(new NumberField("pricingPlan", 10, 10, true, 0));
    fields.add(new NumberField("discountRate", 10, 10, true, 2));
    fields.add(new NumberField("perItem", 10, 10, true, 2));
    fields.add(new NumberField("nonBankAuthFee", 10, 10, true, 2));
    fields.add(new NumberField("betType", 10, 10, true, 0));
    fields.add(new NumberField("comboId", 10, 10, true, 0));
    fields.add(new NumberField("setupFee", 10, 10, true, 0));
    fields.add(new NumberField("pciCompliance", 10, 10, true, 0));
    
    fields.add(new ImageButtonField("submit", "Continue", "/images/partner_referral/finishbutton.gif",
                                      24, 80, "Continue"));
                                      
    fields.setHtmlExtra("class=\"formFields\"");
  }
  
  private boolean updateAppStatus(long appSeqNum)
  {
    boolean loadOk = false;
    try
    {
      String appAlreadyCompleted  = "N";
      String toAddr               = "";
      
      /*@lineinfo:generated-code*//*@lineinfo:93^7*/

//  ************************************************************
//  #sql [Ctx] { select  decode(app_completed,
//                    null, 'N',
//                    'Y'),
//                  contact_email
//          
//          from    virtual_app_contact
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode(app_completed,\n                  null, 'N',\n                  'Y'),\n                contact_email\n         \n        from    virtual_app_contact\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appAlreadyCompleted = (String)__sJT_rs.getString(1);
   toAddr = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:103^7*/
      
      if( appAlreadyCompleted.equals("N") )
      {
        /*@lineinfo:generated-code*//*@lineinfo:107^9*/

//  ************************************************************
//  #sql [Ctx] { update  virtual_app_contact
//            set     app_completed = sysdate
//            where   app_seq_num = :appSeqNum
//                    and app_completed is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  virtual_app_contact\n          set     app_completed = sysdate\n          where   app_seq_num =  :1 \n                  and app_completed is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:113^9*/
      
        WPPEmail.sendWPPEmail(appSeq.getControlNum(), MesEmails.MSG_ADDRS_WPP_COMPLETE, toAddr);
        (new InsertCreditQueue()).addApp(appSeqNum,user.getUserId());
        loadOk = true;
      }
    }
    catch(Exception e)
    {
      logEntry("updateAppStatus(" + appSeqNum + ")", e.toString());
    }
    
    return( loadOk );
  }
  
  private void storeTransactionFees(long appSeqNum)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:132^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     pricing_grid  = :fields.getData("pricingType"),
//                  bet_type_code = :fields.getData("pricingType")
//          where   app_seq_num   = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3379 = fields.getData("pricingType");
 String __sJT_3380 = fields.getData("pricingType");
   String theSqlTS = "update  merchant\n        set     pricing_grid  =  :1 ,\n                bet_type_code =  :2 \n        where   app_seq_num   =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3379);
   __sJT_st.setString(2,__sJT_3380);
   __sJT_st.setLong(3,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:138^7*/

      // clear tranchrg records
      /*@lineinfo:generated-code*//*@lineinfo:141^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from  tranchrg
//          where app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from  tranchrg\n        where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:146^7*/

      // store v/mc pricing
      /*@lineinfo:generated-code*//*@lineinfo:149^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_per_auth,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran,
//            tranchrg_interchangefee_type,
//            tranchrg_interchangefee_fee
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.APP_CT_VISA,
//            :fields.getData("pricingPlan"),
//            :fields.getData("discountRate"),
//            :fields.getData("perItem"),
//            :fields.getData("minimumDiscount"),
//            0,
//            :fields.getData("betType"),
//            :fields.getData("comboId")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3381 = fields.getData("pricingPlan");
 String __sJT_3382 = fields.getData("discountRate");
 String __sJT_3383 = fields.getData("perItem");
 String __sJT_3384 = fields.getData("minimumDiscount");
 String __sJT_3385 = fields.getData("betType");
 String __sJT_3386 = fields.getData("comboId");
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_per_auth,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran,\n          tranchrg_interchangefee_type,\n          tranchrg_interchangefee_fee\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0,\n           :7 ,\n           :8 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   __sJT_st.setString(3,__sJT_3381);
   __sJT_st.setString(4,__sJT_3382);
   __sJT_st.setString(5,__sJT_3383);
   __sJT_st.setString(6,__sJT_3384);
   __sJT_st.setString(7,__sJT_3385);
   __sJT_st.setString(8,__sJT_3386);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:175^7*/

      /*@lineinfo:generated-code*//*@lineinfo:177^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_per_auth,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran,
//            tranchrg_interchangefee_type,
//            tranchrg_interchangefee_fee
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.APP_CT_MC,
//            :fields.getData("pricingPlan"),
//            :fields.getData("discountRate"),
//            :fields.getData("perItem"),
//            :fields.getData("minimumDiscount"),
//            0,
//            :fields.getData("betType"),
//            :fields.getData("comboId")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3387 = fields.getData("pricingPlan");
 String __sJT_3388 = fields.getData("discountRate");
 String __sJT_3389 = fields.getData("perItem");
 String __sJT_3390 = fields.getData("minimumDiscount");
 String __sJT_3391 = fields.getData("betType");
 String __sJT_3392 = fields.getData("comboId");
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_per_auth,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran,\n          tranchrg_interchangefee_type,\n          tranchrg_interchangefee_fee\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0,\n           :7 ,\n           :8 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_MC);
   __sJT_st.setString(3,__sJT_3387);
   __sJT_st.setString(4,__sJT_3388);
   __sJT_st.setString(5,__sJT_3389);
   __sJT_st.setString(6,__sJT_3390);
   __sJT_st.setString(7,__sJT_3391);
   __sJT_st.setString(8,__sJT_3392);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:203^7*/
      
      ResultSetIterator it = null;
      ResultSet         rs = null;
       
      /*@lineinfo:generated-code*//*@lineinfo:208^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    *
//          from      merchpayoption
//          where     app_seq_num = :appSeqNum and
//                    cardtype_code not in (1,4)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    *\n        from      merchpayoption\n        where     app_seq_num =  :1  and\n                  cardtype_code not in (1,4)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.app.virtual.wpp.AppComplete",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:214^7*/
      
      rs = it.getResultSet();

      while(rs.next())
      {
        /*@lineinfo:generated-code*//*@lineinfo:220^9*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//            ( app_seq_num,
//              cardtype_code,
//              tranchrg_per_tran 
//            )
//            values
//            ( :appSeqNum,
//              :rs.getInt("cardtype_code"),
//              :fields.getData("nonBankAuthFee")
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_3393 = rs.getInt("cardtype_code");
 String __sJT_3394 = fields.getData("nonBankAuthFee");
   String theSqlTS = "insert into tranchrg\n          ( app_seq_num,\n            cardtype_code,\n            tranchrg_per_tran \n          )\n          values\n          (  :1 ,\n             :2 ,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,__sJT_3393);
   __sJT_st.setString(3,__sJT_3394);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:232^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("storeTransactionFees()", e.toString());
    }
  }
  
  private void storeMiscFees(long appSeqNum)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:245^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from   miscchrg
//          where  app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from   miscchrg\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:250^7*/

      // monthly service fee
      /*@lineinfo:generated-code*//*@lineinfo:253^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          ( 
//            app_seq_num,
//            misc_code,
//            misc_chrg_amount 
//          )
//          values
//          ( 
//            :appSeqNum,
//            :mesConstants.APP_MISC_CHARGE_MONTHLY_SERVICE_FEE,
//            :fields.getData("monthlyServiceFee")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3395 = fields.getData("monthlyServiceFee");
   String theSqlTS = "insert into miscchrg\n        ( \n          app_seq_num,\n          misc_code,\n          misc_chrg_amount \n        )\n        values\n        ( \n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_MONTHLY_SERVICE_FEE);
   __sJT_st.setString(3,__sJT_3395);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:267^7*/
   

      // chargeback fee
      /*@lineinfo:generated-code*//*@lineinfo:271^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          ( 
//            app_seq_num,
//            misc_code,
//            misc_chrg_amount 
//          )
//          values
//          ( 
//            :appSeqNum,
//            :mesConstants.APP_MISC_CHARGE_CHARGEBACK,
//            :fields.getData("chargebackFee")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3396 = fields.getData("chargebackFee");
   String theSqlTS = "insert into miscchrg\n        ( \n          app_seq_num,\n          misc_code,\n          misc_chrg_amount \n        )\n        values\n        ( \n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_CHARGEBACK);
   __sJT_st.setString(3,__sJT_3396);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:285^7*/
      
      // setup fee
      /*@lineinfo:generated-code*//*@lineinfo:288^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          (
//            app_seq_num,
//            misc_code,
//            misc_chrg_amount
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.APP_MISC_CHARGE_GATEWAY_SETUP,
//            :fields.getData("setupFee")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3397 = fields.getData("setupFee");
   String theSqlTS = "insert into miscchrg\n        (\n          app_seq_num,\n          misc_code,\n          misc_chrg_amount\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_GATEWAY_SETUP);
   __sJT_st.setString(3,__sJT_3397);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:302^7*/
      
      // pci compliance fee
      /*@lineinfo:generated-code*//*@lineinfo:305^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          (
//            app_seq_num,
//            misc_code,
//            misc_chrg_amount
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.APP_MISC_CHARGE_PCI_COMPLIANCE,
//            :fields.getData("pciCompliance")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3398 = fields.getData("pciCompliance");
   String theSqlTS = "insert into miscchrg\n        (\n          app_seq_num,\n          misc_code,\n          misc_chrg_amount\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_PCI_COMPLIANCE);
   __sJT_st.setString(3,__sJT_3398);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:319^7*/
    }
    catch(Exception e)
    {
      logEntry("storeMiscFees()",e.toString());
    }
  }
  
  private void submitPricingData(long appSeqNum)
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:334^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  vcp.minimum_discount,
//                  vcp.monthly_service_fee,
//                  vcp.chargeback_fee,
//                  vcp.pricing_type,
//                  vcp.pricing_plan,
//                  vcp.discount_rate,
//                  vcp.per_item,
//                  vcp.non_bank_auth_fee,
//                  vcp.bet_type,
//                  vcp.combo_id,
//                  vcp.setup_fee
//          from    virtual_app_contact vac,
//                  virtual_app_types vat,
//                  vapp_channel_pricing vcp
//          where   vac.app_seq_num = :appSeqNum
//                  and vac.app_descriptor = vat.app_descriptor
//                  and vat.app_type = vcp.app_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vcp.minimum_discount,\n                vcp.monthly_service_fee,\n                vcp.chargeback_fee,\n                vcp.pricing_type,\n                vcp.pricing_plan,\n                vcp.discount_rate,\n                vcp.per_item,\n                vcp.non_bank_auth_fee,\n                vcp.bet_type,\n                vcp.combo_id,\n                vcp.setup_fee\n        from    virtual_app_contact vac,\n                virtual_app_types vat,\n                vapp_channel_pricing vcp\n        where   vac.app_seq_num =  :1 \n                and vac.app_descriptor = vat.app_descriptor\n                and vat.app_type = vcp.app_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.app.virtual.wpp.AppComplete",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:353^7*/
      
      rs = it.getResultSet();
      
      setFields(rs);
      rs.close();
      it.close();
      
      // pci compliance fee is 30 bucks per year
      setData("pciCompliance", "30");
      
      storeTransactionFees(appSeqNum);
      storeMiscFees(appSeqNum);
    }
    catch(Exception e)
    {
      logEntry("submitPricingData(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void updateAppComplete(long appSeqNum)
  {
    try
    {
      int screenSequence    = 0;
      int completeProgress  = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:385^7*/

//  ************************************************************
//  #sql [Ctx] { select  app.screen_sequence_id-8000 screen_seqeunce,
//                  power(2,max(ss.screen_id))-1
//          
//          from    application app,
//                  screen_sequence ss
//          where   app.app_seq_num = :appSeqNum
//                  and app.screen_sequence_id-8000 = ss.screen_sequence_id
//          group by app.screen_sequence_id-8000
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app.screen_sequence_id-8000 screen_seqeunce,\n                power(2,max(ss.screen_id))-1\n         \n        from    application app,\n                screen_sequence ss\n        where   app.app_seq_num =  :1 \n                and app.screen_sequence_id-8000 = ss.screen_sequence_id\n        group by app.screen_sequence_id-8000";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   screenSequence = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   completeProgress = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:396^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:398^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    screen_progress
//          where   screen_pk = :appSeqNum
//                  and screen_sequence_id = :screenSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    screen_progress\n        where   screen_pk =  :1 \n                and screen_sequence_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,screenSequence);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:404^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:406^7*/

//  ************************************************************
//  #sql [Ctx] { insert into screen_progress
//          (
//            screen_sequence_id,
//            screen_pk,
//            screen_complete
//          )
//          values
//          (
//            :screenSequence,
//            :appSeqNum,
//            :completeProgress
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into screen_progress\n        (\n          screen_sequence_id,\n          screen_pk,\n          screen_complete\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.app.virtual.wpp.AppComplete",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,screenSequence);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,completeProgress);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:420^7*/
    }
    catch(Exception e)
    {
      logEntry("updateAppComplete(" + appSeqNum + ")", e.toString());
    }
  }

  /*
  ** protected boolean loadAppData()
  **
  ** There is no way to submit from the app completed page, so app_init.jsp
  ** will always be calling loadAppData when a user accesses this page.  Rather
  ** that load anything, this method will cause the app to be inserted into
  ** the credit queue.  It also marks this screen in the sequence as being
  ** complete.
  **
  ** RETURNS: true if no errors, else false.
  */
  protected boolean loadAppData()
  {
    long  appSeqNum = 0L;
    
    boolean loadOk = false;
    try
    {
      connect();
      
      // mark the sequence screen as complete
      appSeq.getCurrentScreen().markAsComplete();
      
      appSeqNum = fields.getField("appSeqNum").asInteger();
      
      // insert the app into the credit queue
      if( appSeqNum != 0L )
      {
        // clean up virtual_app_contact, send complete email if necessary
        if( updateAppStatus(appSeqNum) )
        {
          // flesh out app data to include pricing details
          submitPricingData(appSeqNum);
          
          // mark app as complete (even for old-school verision)
          updateAppComplete(appSeqNum);
        }
      }
      
      loadOk = true;
    }
    catch( Exception e )
    {
      logEntry("loadAppData(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( loadOk );
  }
}/*@lineinfo:generated-code*/