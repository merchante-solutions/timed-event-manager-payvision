/*@lineinfo:filename=Page1Base*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/Page1Base.sqlj $

  Description:

  Page1Base

  Base bean for Page 1 of virtual app (merchant-submission version)

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-04-18 16:44:44 -0700 (Fri, 18 Apr 2008) $
  Version            : $Revision: 14760 $

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.virtual;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.PhoneField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.ZipField;
import com.mes.support.XSSFilter;
import sqlj.runtime.ResultSetIterator;

public class Page1Base extends MerchantFacingAppBase
{
  static Logger log = Logger.getLogger(Page1Base.class);
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields( request );
    
    try
    {
      submitPartial = true;
      
      fields.add(new HiddenField("ad"));
      fields.add(new Field("companyName", "Company", 25, 20, false));
      fields.add(new Field("companyAddress", "Address", 32, 20, false));
      fields.add(new Field("companyCity", "City", 25, 20, false));
      fields.add(new DropDownField("companyState", "State", new StateDropDownTable(), false));
      fields.add(new ZipField("companyZip", "Zip Code", false, fields.getField("companyState")));
      
      fields.add(new Field("contactFirstName", "First Name", 40, 20, false));
      fields.add(new Field("contactLastName",  "Last Name", 40, 20, false));
      fields.add(new EmailField("contactEmail", "Email Address", 45, 20, false));
      fields.add(new PhoneField("contactPhone", "Phone", false));
      
      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
    }
  }
  
  private void submitAppSpecificData(long appSeqNum)
    throws Exception
  {
    try
    {
      // merchant
      /*@lineinfo:generated-code*//*@lineinfo:74^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_business_name = :XSSFilter.encodeXSS(getData("companyName")),
//                  merch_email_address = :XSSFilter.encodeXSS(getData("contactEmail"))
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3281 = XSSFilter.encodeXSS(getData("companyName"));
 String __sJT_3282 = XSSFilter.encodeXSS(getData("contactEmail"));
   String theSqlTS = "update  merchant\n        set     merch_business_name =  :1 ,\n                merch_email_address =  :2 \n        where   app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.app.virtual.Page1Base",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3281);
   __sJT_st.setString(2,__sJT_3282);
   __sJT_st.setLong(3,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:80^7*/
      
      // merchcontact
      /*@lineinfo:generated-code*//*@lineinfo:83^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchcontact
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merchcontact\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.virtual.Page1Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:88^7*/
      /*@lineinfo:generated-code*//*@lineinfo:89^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchcontact
//          (
//            app_seq_num,
//            merchcont_prim_first_name,
//            merchcont_prim_last_name,
//            merchcont_prim_phone,
//            merchcont_prim_email
//          )
//          values
//          (
//            :appSeqNum,
//            :XSSFilter.encodeXSS(getData("contactFirstName")),
//            :XSSFilter.encodeXSS(getData("contactLastName")),
//            :XSSFilter.encodeXSS(getData("contactPhone")),
//            :XSSFilter.encodeXSS(getData("contactEmail"))
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3283 = XSSFilter.encodeXSS(getData("contactFirstName"));
 String __sJT_3284 = XSSFilter.encodeXSS(getData("contactLastName"));
 String __sJT_3285 = XSSFilter.encodeXSS(getData("contactPhone"));
 String __sJT_3286 = XSSFilter.encodeXSS(getData("contactEmail"));
   String theSqlTS = "insert into merchcontact\n        (\n          app_seq_num,\n          merchcont_prim_first_name,\n          merchcont_prim_last_name,\n          merchcont_prim_phone,\n          merchcont_prim_email\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.virtual.Page1Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_3283);
   __sJT_st.setString(3,__sJT_3284);
   __sJT_st.setString(4,__sJT_3285);
   __sJT_st.setString(5,__sJT_3286);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:107^7*/
      
      // address
      /*@lineinfo:generated-code*//*@lineinfo:110^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    address
//          where   app_seq_num = :appSeqNum
//                  and addresstype_code = :mesConstants.ADDR_TYPE_BUSINESS
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    address\n        where   app_seq_num =  :1 \n                and addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.virtual.Page1Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_BUSINESS);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:116^7*/
      /*@lineinfo:generated-code*//*@lineinfo:117^7*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//          (
//            address_name,
//            address_line1,
//            address_city,
//            countrystate_code,
//            address_zip,
//            address_phone,
//            app_seq_num,
//            addresstype_code
//          )
//          values
//          (
//            :XSSFilter.encodeXSS(getData("companYName")),
//            :XSSFilter.encodeXSS(getData("companyAddress")),
//            :XSSFilter.encodeXSS(getData("companyCity")),
//            :XSSFilter.encodeXSS(getData("companyState")),
//            :XSSFilter.encodeXSS(getData("companyZip")),
//            :XSSFilter.encodeXSS(getData("contactPhone")),
//            :appSeqNum,
//            :mesConstants.ADDR_TYPE_BUSINESS
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3287 = XSSFilter.encodeXSS(getData("companYName"));
 String __sJT_3288 = XSSFilter.encodeXSS(getData("companyAddress"));
 String __sJT_3289 = XSSFilter.encodeXSS(getData("companyCity"));
 String __sJT_3290 = XSSFilter.encodeXSS(getData("companyState"));
 String __sJT_3291 = XSSFilter.encodeXSS(getData("companyZip"));
 String __sJT_3292 = XSSFilter.encodeXSS(getData("contactPhone"));
   String theSqlTS = "insert into address\n        (\n          address_name,\n          address_line1,\n          address_city,\n          countrystate_code,\n          address_zip,\n          address_phone,\n          app_seq_num,\n          addresstype_code\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.virtual.Page1Base",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3287);
   __sJT_st.setString(2,__sJT_3288);
   __sJT_st.setString(3,__sJT_3289);
   __sJT_st.setString(4,__sJT_3290);
   __sJT_st.setString(5,__sJT_3291);
   __sJT_st.setString(6,__sJT_3292);
   __sJT_st.setLong(7,appSeqNum);
   __sJT_st.setInt(8,mesConstants.ADDR_TYPE_BUSINESS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:141^7*/
    }
    catch(Exception e)
    {
      logEntry("submitAppSpecificData("+appSeqNum+")", e.toString());
      throw( e );
    }
  }
  
  private void submitContactData(long appSeqNum)
    throws Exception
  {
    try
    {
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:157^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    virtual_app_contact
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    virtual_app_contact\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.virtual.Page1Base",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:163^7*/
      
      if( recCount > 0 )
      {
        // update existing record in contact information
        /*@lineinfo:generated-code*//*@lineinfo:168^9*/

//  ************************************************************
//  #sql [Ctx] { update  virtual_app_contact
//            set     company_name        = :XSSFilter.encodeXSS(getData("companyName")), 
//                    company_address     = :XSSFilter.encodeXSS(getData("companyAddress")),
//                    company_city        = :XSSFilter.encodeXSS(getData("companyCity")),  
//                    company_state       = :XSSFilter.encodeXSS(getData("companyState")),
//                    company_zip         = :XSSFilter.encodeXSS(getData("companyZip")),   
//                    contact_first_name  = :XSSFilter.encodeXSS(getData("contactFirstName")), 
//                    contact_last_name   = :XSSFilter.encodeXSS(getData("contactLastName")), 
//                    contact_email       = :XSSFilter.encodeXSS(getData("contactEmail")), 
//                    contact_phone       = :XSSFilter.encodeXSS(getData("contactPhone")),
//                    app_started         = decode(app_started, null, sysdate, app_started)
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3293 = XSSFilter.encodeXSS(getData("companyName"));
 String __sJT_3294 = XSSFilter.encodeXSS(getData("companyAddress"));
 String __sJT_3295 = XSSFilter.encodeXSS(getData("companyCity"));
 String __sJT_3296 = XSSFilter.encodeXSS(getData("companyState"));
 String __sJT_3297 = XSSFilter.encodeXSS(getData("companyZip"));
 String __sJT_3298 = XSSFilter.encodeXSS(getData("contactFirstName"));
 String __sJT_3299 = XSSFilter.encodeXSS(getData("contactLastName"));
 String __sJT_3300 = XSSFilter.encodeXSS(getData("contactEmail"));
 String __sJT_3301 = XSSFilter.encodeXSS(getData("contactPhone"));
   String theSqlTS = "update  virtual_app_contact\n          set     company_name        =  :1 , \n                  company_address     =  :2 ,\n                  company_city        =  :3 ,  \n                  company_state       =  :4 ,\n                  company_zip         =  :5 ,   \n                  contact_first_name  =  :6 , \n                  contact_last_name   =  :7 , \n                  contact_email       =  :8 , \n                  contact_phone       =  :9 ,\n                  app_started         = decode(app_started, null, sysdate, app_started)\n          where   app_seq_num =  :10";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.app.virtual.Page1Base",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3293);
   __sJT_st.setString(2,__sJT_3294);
   __sJT_st.setString(3,__sJT_3295);
   __sJT_st.setString(4,__sJT_3296);
   __sJT_st.setString(5,__sJT_3297);
   __sJT_st.setString(6,__sJT_3298);
   __sJT_st.setString(7,__sJT_3299);
   __sJT_st.setString(8,__sJT_3300);
   __sJT_st.setString(9,__sJT_3301);
   __sJT_st.setLong(10,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:182^9*/
      }
      else
      {
        // insert new record along with start date
        /*@lineinfo:generated-code*//*@lineinfo:187^9*/

//  ************************************************************
//  #sql [Ctx] { insert into virtual_app_contact
//            (
//              app_seq_num,
//              company_name,
//              company_address,
//              company_city,
//              company_state, 
//              company_zip, 
//              contact_first_name, 
//              contact_last_name, 
//              contact_email, 
//              contact_phone,
//              app_started,
//              app_descriptor
//            )
//            values
//            (
//              :appSeqNum,
//              :XSSFilter.encodeXSS(getData("companyName")),
//              :XSSFilter.encodeXSS(getData("companyAddress")),
//              :XSSFilter.encodeXSS(getData("companyCity")),
//              :XSSFilter.encodeXSS(getData("companyState")),
//              :XSSFilter.encodeXSS(getData("companyZip")),
//              :XSSFilter.encodeXSS(getData("contactFirstName")),
//              :XSSFilter.encodeXSS(getData("contactLastName")),
//              :XSSFilter.encodeXSS(getData("contactEmail")),
//              :XSSFilter.encodeXSS(getData("contactPhone")),
//              sysdate,
//              :XSSFilter.encodeXSS(getData("ad"))
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3302 = XSSFilter.encodeXSS(getData("companyName"));
 String __sJT_3303 = XSSFilter.encodeXSS(getData("companyAddress"));
 String __sJT_3304 = XSSFilter.encodeXSS(getData("companyCity"));
 String __sJT_3305 = XSSFilter.encodeXSS(getData("companyState"));
 String __sJT_3306 = XSSFilter.encodeXSS(getData("companyZip"));
 String __sJT_3307 = XSSFilter.encodeXSS(getData("contactFirstName"));
 String __sJT_3308 = XSSFilter.encodeXSS(getData("contactLastName"));
 String __sJT_3309 = XSSFilter.encodeXSS(getData("contactEmail"));
 String __sJT_3310 = XSSFilter.encodeXSS(getData("contactPhone"));
 String __sJT_3311 = XSSFilter.encodeXSS(getData("ad"));
   String theSqlTS = "insert into virtual_app_contact\n          (\n            app_seq_num,\n            company_name,\n            company_address,\n            company_city,\n            company_state, \n            company_zip, \n            contact_first_name, \n            contact_last_name, \n            contact_email, \n            contact_phone,\n            app_started,\n            app_descriptor\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n            sysdate,\n             :11 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.app.virtual.Page1Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_3302);
   __sJT_st.setString(3,__sJT_3303);
   __sJT_st.setString(4,__sJT_3304);
   __sJT_st.setString(5,__sJT_3305);
   __sJT_st.setString(6,__sJT_3306);
   __sJT_st.setString(7,__sJT_3307);
   __sJT_st.setString(8,__sJT_3308);
   __sJT_st.setString(9,__sJT_3309);
   __sJT_st.setString(10,__sJT_3310);
   __sJT_st.setString(11,__sJT_3311);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:219^9*/
      }
      
      // add data to app-related database tables
      submitAppSpecificData(appSeqNum);
    }
    catch(Exception e)
    {
      logEntry("submitContactData(" + appSeqNum + ")", e.toString());
      throw( e );
    }
  }
  
  protected boolean submitAppData()
  {
    boolean submitOk = false;
    
    try
    {
      connect();
      setAutoCommit(false);
      
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if( appSeqNum != 0 )
      {
        submitContactData(appSeqNum);
      }
      
      commit();
      
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitAppData", e.toString());
      rollback();
    }
    finally
    {
      cleanUp();
    }
    
    return( submitOk );
  }
  
  protected void loadContactData(long appSeqNum)
    throws Exception
  {
    ResultSetIterator it = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:272^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  company_name        company_name,
//                  company_address     company_address,
//                  company_city        company_city,
//                  company_state       companY_state,
//                  companY_zip         company_zip,
//                  contact_first_name  contact_first_name,
//                  contact_last_name   contact_last_name,
//                  contact_email       contact_email,
//                  contact_phone       contact_phone
//          from    virtual_app_contact
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  company_name        company_name,\n                company_address     company_address,\n                company_city        company_city,\n                company_state       companY_state,\n                companY_zip         company_zip,\n                contact_first_name  contact_first_name,\n                contact_last_name   contact_last_name,\n                contact_email       contact_email,\n                contact_phone       contact_phone\n        from    virtual_app_contact\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.virtual.Page1Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.app.virtual.Page1Base",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:285^7*/
      
      setFields(it.getResultSet());
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadContactData("+ appSeqNum +")", e.toString());
      throw( e );
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  
  protected boolean loadPageData(long appSeqNum)
  {
    boolean loadOk = false;
    
    try
    {
      if( appSeqNum != 0L )
      {
        loadContactData(appSeqNum);
      }
      
      loadOk = true;
    }
    catch(Exception e)
    {
      logEntry("loadAppData()", e.toString());
    }
    
    return( loadOk );
  }
  
  public String getSplashScreen()
  {
    // default to all-in-one
    String splashScreen = "include/all_in_one_splash.inc";
    
    try
    {
      if( getData("ad").equals("vt") )
      {
        splashScreen = "include/vir_term_splash.inc";
      }
      else
      {
        splashScreen = "include/all_in_one_splash.inc";
      }
    }
    catch(Exception e)
    {
      logEntry("getSplashScreen()", e.toString());
    }
    
    return( splashScreen );
  }
}/*@lineinfo:generated-code*/