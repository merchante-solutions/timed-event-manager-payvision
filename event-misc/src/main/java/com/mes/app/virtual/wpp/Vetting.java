/*@lineinfo:filename=Vetting*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/Page1Base.sqlj $

  Description:

  Page1Base

  Base bean for Page 1 of virtual app (merchant-submission version)

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-04-18 16:44:44 -0700 (Fri, 18 Apr 2008) $
  Version            : $Revision: 14760 $

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.virtual.wpp;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.app.AppSeqNumGenerator;
import com.mes.app.AppSequence;
import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioButtonField;
import com.mes.support.HttpHelper;

public class Vetting extends FieldBean
{
  public boolean  moveOn      = false;
  public long     controlNum  = 0;
  public long     appSeqNum   = 0;
  static Logger   log         = Logger.getLogger(Vetting.class);
  
  private long    vettingSequence = 0L;
  
  protected static final String[][] productTypeList =
  {
    { "Virtual Terminal", "vt"  },
    { "All in One",       "wpp" }
  };
  
  protected static final String[][] yesNoRadioList =
  {
    { "Yes", "Y" },
    { "No",  "N" }
  };

  public Vetting(HttpServletRequest request)
  {
    createFields();
    
    if( WPPEmail.ServerUrl == null || WPPEmail.ServerUrl.equals("") )
    {
      WPPEmail.ServerUrl = HttpHelper.getServerURL(request);
    }
    
    setProperties(request);
  }
  
  public String getStatusString()
  {
    StringBuffer result = new StringBuffer("");
    
    if(getData("pass").length() > 0 && ! isValid())
    {
      result.append("<span class=\"formTextNoticeRed\">");
      result.append("Validation Failed.  See errors below.");
      result.append("</span>");
    }
    else
    {
      if(getData("fail").length() > 0)
      {
        result.append("Merchant failed vetting.");
      }
      else if(getData("pass").length() > 0)
      {
        result.append("Merchant passed vetting and has been sent an email to continue submitting their application.");
      }
    }
    
    return( result.toString() );
  }
  
  private void setProperties(HttpServletRequest request)
  {
    
    try
    {
      connect();
      
      setFields(request);
      
      register(user.getLoginName(), request);
      
      // check for submission
      if( getData("fail").length() > 0 )
      {
        // failed vetting, but go ahead and store data anyway
        storeVettingData();
        moveOn = true;
      }
      else if( getData("pass").length() > 0 )
      {
        // validate form data
        if( isValid() )
        {
          storeVettingData();
          storeAppData();
          moveOn = true;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void createFields()
  {
    try
    {
      fields.add(new Field            ("contactFirstName", "First Name", 40, 20, false));
      fields.add(new Field            ("contactLastName",  "Last Name", 40, 20, false));
      fields.add(new Field            ("companyName", "Company", 25, 20, false));
      fields.add(new EmailField       ("contactEmail", "Email Address", 45, 20, false));
      fields.add(new PhoneField       ("contactPhone", "Phone", false));
      fields.add(new RadioButtonField ("appDescriptor", productTypeList, -1, false, "Required"));
      fields.add(new Field            ("industryType", "Industry Type", 40, 20, false));
      fields.add(new RadioButtonField ("industryRestricted", yesNoRadioList, -1, false, "Required"));
      fields.add(new Field            ("productSold", "Product or Service", 40, 20, false));
      fields.add(new RadioButtonField ("productRestricted", yesNoRadioList, -1, false, "Required"));
      fields.add(new RadioButtonField ("haveProcessed", yesNoRadioList, -1, false, "Required"));
      fields.add(new Field            ("previousProcessor", "Previous Processor", 40, 25, true));
      fields.add(new RadioButtonField ("statementsProvided", yesNoRadioList, -1, true, "Required"));
      fields.add(new RadioButtonField ("haveCanceled", yesNoRadioList, -1, true, "Required"));
      fields.add(new Field            ("canceledProcessor", "Name of Processor", 40, 20, true));
      fields.add(new Field            ("canceledReason", "Reason for Cancellation",40,30,true));
      fields.add(new DateStringField  ("cancelDate", "Date of Cancellation", true, false));
      fields.add(new RadioButtonField ("declaredBankruptcy", yesNoRadioList, -1, false, "Required"));
      fields.add(new DateStringField  ("bankruptcyDate", "Date declared Bankruptcy", true, false));
      fields.add(new RadioButtonField ("failRequirements", yesNoRadioList, -1, false, "Required"));
      
      fields.add(new ButtonField      ("fail", "Failed Vetting"));
      fields.add(new ButtonField      ("pass", "Continue"));
      
      fields.getField("previousProcessor")
        .addValidation(new IfYesNotBlankValidation(fields.getField("haveProcessed"),
          "Please provide the name of the previous processor"));
      fields.getField("statementsProvided")
        .addValidation(new IfYesNotBlankValidation(fields.getField("haveProcessed"),
          "Required"));
      fields.getField("haveCanceled")
        .addValidation(new IfYesNotBlankValidation(fields.getField("haveProcessed"),
          "Required"));
          
      fields.getField("canceledProcessor")
        .addValidation(new IfYesNotBlankValidation(fields.getField("haveCanceled"),
          "Please provide the name of the cancelling processor"));
      fields.getField("canceledReason")
        .addValidation(new IfYesNotBlankValidation(fields.getField("haveCanceled"),
          "Please provide reason account was cancelled"));
      fields.getField("cancelDate")
        .addValidation(new IfYesNotBlankValidation(fields.getField("haveCanceled"),
          "Please provide a date of cancellation"));
          
      fields.getField("bankruptcyDate")
        .addValidation(new IfYesNotBlankValidation(fields.getField("declaredBankruptcy"),
          "Please provide date bankruptcy was declared"));
          
      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
    }
  }
  
  private void storeVettingData()
  {
    try
    {
      int vettingStatus = 0;
      
      if(getData("pass").length() > 0)
      {
        vettingStatus = 1;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:205^7*/

//  ************************************************************
//  #sql [Ctx] { select  VA_VETTNG_SEQUENCE.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  VA_VETTNG_SEQUENCE.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.virtual.wpp.Vetting",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vettingSequence = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:210^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:212^7*/

//  ************************************************************
//  #sql [Ctx] { insert into virtual_app_vetting
//          (
//            id,
//            contact_first_name,
//            contact_last_name,
//            company_name,
//            contact_email,
//            contact_phone,
//            app_descriptor,
//            industry_type,
//            industry_restricted,
//            product_sold,
//            product_restricted,
//            have_processed,
//            previous_processor,
//            statements_provided,
//            have_canceled,
//            canceled_processor,
//            canceled_reason,
//            cancel_date,
//            fail_requirements,
//            declared_bankruptcy,
//            date_submitted,
//            vetting_status,
//            vetting_user
//          )
//          values
//          (
//            :vettingSequence,
//            :getData("contactFirstName"),
//            :getData("contactLastName"),
//            :getData("companyName"),
//            :getData("contactEmail"),
//            :getData("contactPhone"),
//            :getData("appDescriptor"),
//            :getData("industryType"),
//            :getData("industryRestricted"),
//            :getData("productSold"),
//            :getData("productRestricted"),
//            :getData("haveProcessed"),
//            :getData("previousProcessor"),
//            :getData("statementsProvided"),
//            :getData("haveCanceled"),
//            :getData("canceledProcessor"),
//            :getData("canceledReason"),
//            :getData("cancelDate"),
//            :getData("failRequirements"),
//            :getData("declaredBankruptcy"),
//            sysdate,
//            :vettingStatus,
//            :user.getLoginName()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3399 = getData("contactFirstName");
 String __sJT_3400 = getData("contactLastName");
 String __sJT_3401 = getData("companyName");
 String __sJT_3402 = getData("contactEmail");
 String __sJT_3403 = getData("contactPhone");
 String __sJT_3404 = getData("appDescriptor");
 String __sJT_3405 = getData("industryType");
 String __sJT_3406 = getData("industryRestricted");
 String __sJT_3407 = getData("productSold");
 String __sJT_3408 = getData("productRestricted");
 String __sJT_3409 = getData("haveProcessed");
 String __sJT_3410 = getData("previousProcessor");
 String __sJT_3411 = getData("statementsProvided");
 String __sJT_3412 = getData("haveCanceled");
 String __sJT_3413 = getData("canceledProcessor");
 String __sJT_3414 = getData("canceledReason");
 String __sJT_3415 = getData("cancelDate");
 String __sJT_3416 = getData("failRequirements");
 String __sJT_3417 = getData("declaredBankruptcy");
 String __sJT_3418 = user.getLoginName();
   String theSqlTS = "insert into virtual_app_vetting\n        (\n          id,\n          contact_first_name,\n          contact_last_name,\n          company_name,\n          contact_email,\n          contact_phone,\n          app_descriptor,\n          industry_type,\n          industry_restricted,\n          product_sold,\n          product_restricted,\n          have_processed,\n          previous_processor,\n          statements_provided,\n          have_canceled,\n          canceled_processor,\n          canceled_reason,\n          cancel_date,\n          fail_requirements,\n          declared_bankruptcy,\n          date_submitted,\n          vetting_status,\n          vetting_user\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n           :16 ,\n           :17 ,\n           :18 ,\n           :19 ,\n           :20 ,\n          sysdate,\n           :21 ,\n           :22 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.app.virtual.wpp.Vetting",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,vettingSequence);
   __sJT_st.setString(2,__sJT_3399);
   __sJT_st.setString(3,__sJT_3400);
   __sJT_st.setString(4,__sJT_3401);
   __sJT_st.setString(5,__sJT_3402);
   __sJT_st.setString(6,__sJT_3403);
   __sJT_st.setString(7,__sJT_3404);
   __sJT_st.setString(8,__sJT_3405);
   __sJT_st.setString(9,__sJT_3406);
   __sJT_st.setString(10,__sJT_3407);
   __sJT_st.setString(11,__sJT_3408);
   __sJT_st.setString(12,__sJT_3409);
   __sJT_st.setString(13,__sJT_3410);
   __sJT_st.setString(14,__sJT_3411);
   __sJT_st.setString(15,__sJT_3412);
   __sJT_st.setString(16,__sJT_3413);
   __sJT_st.setString(17,__sJT_3414);
   __sJT_st.setString(18,__sJT_3415);
   __sJT_st.setString(19,__sJT_3416);
   __sJT_st.setString(20,__sJT_3417);
   __sJT_st.setInt(21,vettingStatus);
   __sJT_st.setString(22,__sJT_3418);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:266^7*/
    }
    catch(Exception e)
    {
      logEntry("storeVettingData()", e.toString());
    }
  }
  
  private void storeAppData()
  {
    try
    {
      // create new app
      AppSeqNumGenerator numGen = new AppSeqNumGenerator(Ctx);
      
      appSeqNum   = numGen.getAppSeqNum();
      controlNum  = numGen.getControlNumber();
      
      int userId = 0;
      String loginName = "";
      
      if(getData("appDescriptor").equals("vt"))
      {
        loginName = "miniapp_vt";
      }
      else
      {
        loginName = "miniapp_wpp";
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:296^7*/

//  ************************************************************
//  #sql [Ctx] { select  user_id
//          
//          from    users
//          where   login_name = :loginName
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  user_id\n         \n        from    users\n        where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.virtual.wpp.Vetting",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   userId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:302^7*/
      
      // create application record
      AppSequence appSeq = new AppSequence();
      appSeq.setAppType(mesConstants.APP_TYPE_PAYPAL_REFERRAL, true);
      appSeq.setCurScreenId(1);
      
      /*@lineinfo:generated-code*//*@lineinfo:309^7*/

//  ************************************************************
//  #sql [Ctx] { insert into application
//          (
//            app_seq_num,
//            appsrctype_code,
//            app_created_date,
//            app_scrn_code,
//            app_user_id,
//            app_user_login,
//            app_type,
//            screen_sequence_id
//          )
//          values
//          (
//            :appSeqNum,
//            :appSeq.getAppSourceType(),
//            sysdate,
//            null,
//            :userId,
//            :loginName,
//            :mesConstants.APP_TYPE_PAYPAL_REFERRAL,
//            :appSeq.getSeqId()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3419 = appSeq.getAppSourceType();
 int __sJT_3420 = appSeq.getSeqId();
   String theSqlTS = "insert into application\n        (\n          app_seq_num,\n          appsrctype_code,\n          app_created_date,\n          app_scrn_code,\n          app_user_id,\n          app_user_login,\n          app_type,\n          screen_sequence_id\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n          sysdate,\n          null,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.virtual.wpp.Vetting",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_3419);
   __sJT_st.setInt(3,userId);
   __sJT_st.setString(4,loginName);
   __sJT_st.setInt(5,mesConstants.APP_TYPE_PAYPAL_REFERRAL);
   __sJT_st.setInt(6,__sJT_3420);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:333^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:335^7*/

//  ************************************************************
//  #sql [Ctx] { update  virtual_app_vetting
//          set     app_seq_num = :appSeqNum
//          where   id = :vettingSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  virtual_app_vetting\n        set     app_seq_num =  :1 \n        where   id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.virtual.wpp.Vetting",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,vettingSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:340^7*/
      
      // create merchant record for new app
      /*@lineinfo:generated-code*//*@lineinfo:343^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant
//          (
//            app_seq_num,
//            merc_cntrl_number,
//            merch_bank_number,
//            merch_prior_cc_accp_flag,
//            merch_prior_processor,
//            merch_cc_acct_term_flag,
//            merch_cc_term_name,
//            merch_term_reason,
//            merch_term_month,
//            merch_term_year,
//            merch_prior_statements,
//            merch_business_name,
//            merch_email_address
//          )
//          values
//          (
//            :appSeqNum,
//            :controlNum,
//            :appSeq.getBankNum(),
//            :getData("haveProcessed"),
//            :getData("previousProcessor"),
//            :getData("haveCanceled"),
//            :getData("canceledProcessor"),
//            :getData("canceledReason"),
//            :getData("cancelDateMonth"),
//            :getData("cancelDateYear"),
//            :getData("statementsProvided"),
//            :getData("companyName"),
//            :getData("contactEmail")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_3421 = appSeq.getBankNum();
 String __sJT_3422 = getData("haveProcessed");
 String __sJT_3423 = getData("previousProcessor");
 String __sJT_3424 = getData("haveCanceled");
 String __sJT_3425 = getData("canceledProcessor");
 String __sJT_3426 = getData("canceledReason");
 String __sJT_3427 = getData("cancelDateMonth");
 String __sJT_3428 = getData("cancelDateYear");
 String __sJT_3429 = getData("statementsProvided");
 String __sJT_3430 = getData("companyName");
 String __sJT_3431 = getData("contactEmail");
   String theSqlTS = "insert into merchant\n        (\n          app_seq_num,\n          merc_cntrl_number,\n          merch_bank_number,\n          merch_prior_cc_accp_flag,\n          merch_prior_processor,\n          merch_cc_acct_term_flag,\n          merch_cc_term_name,\n          merch_term_reason,\n          merch_term_month,\n          merch_term_year,\n          merch_prior_statements,\n          merch_business_name,\n          merch_email_address\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n           :12 ,\n           :13 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.app.virtual.wpp.Vetting",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,controlNum);
   __sJT_st.setInt(3,__sJT_3421);
   __sJT_st.setString(4,__sJT_3422);
   __sJT_st.setString(5,__sJT_3423);
   __sJT_st.setString(6,__sJT_3424);
   __sJT_st.setString(7,__sJT_3425);
   __sJT_st.setString(8,__sJT_3426);
   __sJT_st.setString(9,__sJT_3427);
   __sJT_st.setString(10,__sJT_3428);
   __sJT_st.setString(11,__sJT_3429);
   __sJT_st.setString(12,__sJT_3430);
   __sJT_st.setString(13,__sJT_3431);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:377^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:379^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchcontact
//          (
//            app_seq_num,
//            merchcont_prim_first_name,
//            merchcont_prim_last_name,
//            merchcont_prim_phone,
//            merchcont_prim_email
//          )
//          values
//          (
//            :appSeqNum,
//            :getData("contactFirstName"),
//            :getData("contactLastName"),
//            :getData("contactPhone"),
//            :getData("contactEmail")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3432 = getData("contactFirstName");
 String __sJT_3433 = getData("contactLastName");
 String __sJT_3434 = getData("contactPhone");
 String __sJT_3435 = getData("contactEmail");
   String theSqlTS = "insert into merchcontact\n        (\n          app_seq_num,\n          merchcont_prim_first_name,\n          merchcont_prim_last_name,\n          merchcont_prim_phone,\n          merchcont_prim_email\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.app.virtual.wpp.Vetting",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_3432);
   __sJT_st.setString(3,__sJT_3433);
   __sJT_st.setString(4,__sJT_3434);
   __sJT_st.setString(5,__sJT_3435);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:397^7*/
      
      // send email to merchant to allow continuation of app
      WPPEmail.sendWPPEmail(controlNum, MesEmails.MSG_ADDRS_WPP_CONTACT, getData("contactEmail"));
      
      // create record in virtual_app_contact
      /*@lineinfo:generated-code*//*@lineinfo:403^7*/

//  ************************************************************
//  #sql [Ctx] { insert into virtual_app_contact
//          (
//            app_seq_num,
//            app_descriptor,
//            company_name,
//            contact_first_name,
//            contact_last_name,
//            contact_email,
//            contact_phone,
//            email_sent
//          )
//          values
//          (
//            :appSeqNum,
//            :getData("appDescriptor"),
//            :getData("companyName"),
//            :getData("contactFirstName"),
//            :getData("contactLastName"),
//            :getData("contactEmail"),
//            :getData("contactPhone"),
//            'Y'
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3436 = getData("appDescriptor");
 String __sJT_3437 = getData("companyName");
 String __sJT_3438 = getData("contactFirstName");
 String __sJT_3439 = getData("contactLastName");
 String __sJT_3440 = getData("contactEmail");
 String __sJT_3441 = getData("contactPhone");
   String theSqlTS = "insert into virtual_app_contact\n        (\n          app_seq_num,\n          app_descriptor,\n          company_name,\n          contact_first_name,\n          contact_last_name,\n          contact_email,\n          contact_phone,\n          email_sent\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n          'Y'\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.app.virtual.wpp.Vetting",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_3436);
   __sJT_st.setString(3,__sJT_3437);
   __sJT_st.setString(4,__sJT_3438);
   __sJT_st.setString(5,__sJT_3439);
   __sJT_st.setString(6,__sJT_3440);
   __sJT_st.setString(7,__sJT_3441);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:427^7*/
    }
    catch(Exception e)
    {
      logEntry("storeAppData()", e.toString());
    }
  }
}/*@lineinfo:generated-code*/