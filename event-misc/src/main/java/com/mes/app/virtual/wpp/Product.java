/*@lineinfo:filename=Product*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/Page1Base.sqlj $

  Description:

  Page1Base

  Base bean for Page 1 of virtual app (merchant-submission version)

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-04-18 16:44:44 -0700 (Fri, 18 Apr 2008) $
  Version            : $Revision: 14760 $

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.virtual.wpp;

import javax.servlet.http.HttpServletRequest;
import com.mes.app.virtual.Page6Base;
import com.mes.forms.ImageButtonField;

public class Product extends Page6Base
{
  {
    appType = 76;
    curScreenId = 6;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields( request );
    
    fields.add(new ImageButtonField("submit", "Continue", "/images/partner_referral/submitbutton.gif",
                                      24, 80, "Continue"));
    
  }
}/*@lineinfo:generated-code*/