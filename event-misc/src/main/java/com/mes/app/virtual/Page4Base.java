/*@lineinfo:filename=Page4Base*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/Page1Base.sqlj $

  Description:

  Page1Base

  Base bean for Page 1 of virtual app (merchant-submission version)

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-04-18 16:44:44 -0700 (Fri, 18 Apr 2008) $
  Version            : $Revision: 14760 $

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.virtual;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.support.XSSFilter;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Page4Base extends MerchantFacingAppBase
{
  static Logger log = Logger.getLogger(Page3Base.class);
  
  protected static final String[][] yesNoRadioList =
  {
    { "No",  "N" },
    { "Yes", "Y" }
  };

  protected class RefundPolicyTable extends DropDownTable
  {
    public RefundPolicyTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Refund in 30 Days or Less");
      addElement("2","No Refunds or Exchanges");
      addElement("3","Exchanges Only");
      addElement("4","Not Applicable");
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields( request );
    
    try
    {
      // merchant history
      FieldGroup gMerchHist = new FieldGroup("gMerchHist");

      gMerchHist.add(new RadioButtonField ("haveProcessed",     yesNoRadioList,-1,false,"Required"));
      gMerchHist.add(new Field            ("previousProcessor", "If Yes, Name of Previous Processor",40,25,true));
      gMerchHist.add(new RadioButtonField ("statementsProvided",yesNoRadioList,-1,true,"Required"));
      gMerchHist.add(new RadioButtonField ("haveCanceled",      yesNoRadioList,-1,true,"Required"));
      gMerchHist.add(new Field            ("canceledProcessor", "If Yes, Name of Processor",40,20,true));
      gMerchHist.add(new Field            ("canceledReason",    "Reason for Cancellation",40,30,true));
      gMerchHist.add(new DateStringField  ("cancelDate",        "Date of Cancellation",true,true));
      
      // add javascript calls
      RadioButtonField tmp = (RadioButtonField)(gMerchHist.getField("haveProcessed"));
      tmp.setOnClick(0, "javascript:SetMerchantView()");
      tmp.setOnClick(1, "javascript:SetMerchantView()");
      
      tmp = (RadioButtonField)(gMerchHist.getField("haveCanceled"));
      tmp.setOnClick(0, "javascript:SetCancelledView()");
      tmp.setOnClick(1, "javascript:SetCancelledView()");

      gMerchHist.getField("previousProcessor")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveProcessed"),
          "Please provide the name of the previous processor"));
      gMerchHist.getField("statementsProvided")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveProcessed"),
          "Required"));
      gMerchHist.getField("haveCanceled")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveProcessed"),
          "Required"));
          
      //gMerchHist.getField("canceledProcessor")
      //  .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
      //    "Please provide the name of the cancelling processor"));
      gMerchHist.getField("canceledReason")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
          "Please provide reason account was cancelled"));
      gMerchHist.getField("cancelDate")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
          "Please provide a date of cancellation"));

      fields.add(gMerchHist);
      
      // transaction info
      FieldGroup gTranInfo = new FieldGroup("gTranInfo");

      gTranInfo.add (new CurrencyField    ("monthlySales",          "Total Estimated Monthly Sales",11,9,false,0,99999999.99F));
      gTranInfo.add (new CurrencyField    ("monthlyVMCSales",       "Total Estimated Monthly Visa/MC Sales",11,9,false,0,99999999.99F));
      gTranInfo.add (new CurrencyField    ("averageTicket",         "Estimated Average Credit Card Ticket",11,9,false,0,99999999.99F));
      gTranInfo.add (new DropDownField    ("refundPolicy",          "Refund Policy",new RefundPolicyTable(),false));
      gTranInfo.add (new NumberField      ("percentInternational",  "% International Sales",3,3,false,0));

      gTranInfo.getField("percentInternational")
        .addValidation(new PercentValidation(
          "Invalid Percentage",0,100));

      fields.add(gTranInfo);
      
      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
    }
  }
  
  protected void submitMerchant(long appSeqNum)
    throws Exception
  {
    try
    {
      if(getData("statementsProvided").length() == 0)
      {
        setData("statementsProvided", "N");
      }
      
      if(getData("haveCanceled").length() == 0)
      {
        setData("haveCanceled", "N");
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:146^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_prior_cc_accp_flag    = :XSSFilter.encodeXSS(getData("haveProcessed")),
//                  merch_prior_processor       = :XSSFilter.encodeXSS(getData("previousProcessor")),
//                  merch_cc_acct_term_flag     = :XSSFilter.encodeXSS(getData("haveCanceled")),
//                  merch_cc_term_name          = :XSSFilter.encodeXSS(getData("canceledProcessor")),
//                  merch_term_reason           = :XSSFilter.encodeXSS(getData("canceledReason")),
//                  merch_term_month            = :XSSFilter.encodeXSS(getData("cancelDateMonth")),
//                  merch_term_year             = :XSSFilter.encodeXSS(getData("cancelDateYear")),
//                  merch_month_tot_proj_sales  = :XSSFilter.encodeXSS(getData("monthlySales")),
//                  merch_month_visa_mc_sales   = :XSSFilter.encodeXSS(getData("monthlyVMCSales")),
//                  merch_average_cc_tran       = :XSSFilter.encodeXSS(getData("averageTicket")),
//                  refundtype_code             = :XSSFilter.encodeXSS(getData("refundPolicy")),
//                  percent_international       = :XSSFilter.encodeXSS(getData("percentInternational")),
//                  merch_prior_statements      = :XSSFilter.encodeXSS(getData("statementsProvided"))
//          where   app_seq_num                 = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3332 = XSSFilter.encodeXSS(getData("haveProcessed"));
 String __sJT_3333 = XSSFilter.encodeXSS(getData("previousProcessor"));
 String __sJT_3334 = XSSFilter.encodeXSS(getData("haveCanceled"));
 String __sJT_3335 = XSSFilter.encodeXSS(getData("canceledProcessor"));
 String __sJT_3336 = XSSFilter.encodeXSS(getData("canceledReason"));
 String __sJT_3337 = XSSFilter.encodeXSS(getData("cancelDateMonth"));
 String __sJT_3338 = XSSFilter.encodeXSS(getData("cancelDateYear"));
 String __sJT_3339 = XSSFilter.encodeXSS(getData("monthlySales"));
 String __sJT_3340 = XSSFilter.encodeXSS(getData("monthlyVMCSales"));
 String __sJT_3341 = XSSFilter.encodeXSS(getData("averageTicket"));
 String __sJT_3342 = XSSFilter.encodeXSS(getData("refundPolicy"));
 String __sJT_3343 = XSSFilter.encodeXSS(getData("percentInternational"));
 String __sJT_3344 = XSSFilter.encodeXSS(getData("statementsProvided"));
   String theSqlTS = "update  merchant\n        set     merch_prior_cc_accp_flag    =  :1 ,\n                merch_prior_processor       =  :2 ,\n                merch_cc_acct_term_flag     =  :3 ,\n                merch_cc_term_name          =  :4 ,\n                merch_term_reason           =  :5 ,\n                merch_term_month            =  :6 ,\n                merch_term_year             =  :7 ,\n                merch_month_tot_proj_sales  =  :8 ,\n                merch_month_visa_mc_sales   =  :9 ,\n                merch_average_cc_tran       =  :10 ,\n                refundtype_code             =  :11 ,\n                percent_international       =  :12 ,\n                merch_prior_statements      =  :13 \n        where   app_seq_num                 =  :14";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.app.virtual.Page4Base",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3332);
   __sJT_st.setString(2,__sJT_3333);
   __sJT_st.setString(3,__sJT_3334);
   __sJT_st.setString(4,__sJT_3335);
   __sJT_st.setString(5,__sJT_3336);
   __sJT_st.setString(6,__sJT_3337);
   __sJT_st.setString(7,__sJT_3338);
   __sJT_st.setString(8,__sJT_3339);
   __sJT_st.setString(9,__sJT_3340);
   __sJT_st.setString(10,__sJT_3341);
   __sJT_st.setString(11,__sJT_3342);
   __sJT_st.setString(12,__sJT_3343);
   __sJT_st.setString(13,__sJT_3344);
   __sJT_st.setLong(14,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:163^7*/
    }
    catch(Exception e)
    {
      logEntry("submitMerchant(" + appSeqNum + ")", e.toString());
      throw( e );
    }
  }
  
  protected boolean submitAppData()
  {
    boolean submitOk = false;
    
    try
    {
      connect();
      setAutoCommit(false);
      
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if( appSeqNum != 0 )
      {
        submitMerchant(appSeqNum);
      }
      
      commit();
      
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitData", e.toString());
      rollback();
    }
    finally
    {
      cleanUp();
    }
    
    return( submitOk );
  }
  
  protected void loadMerchant(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    try
    {
      // load data from merchant
      /*@lineinfo:generated-code*//*@lineinfo:211^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_prior_cc_accp_flag      have_processed,
//                  merch_prior_processor         previous_processor,
//                  merch_cc_acct_term_flag       have_canceled,
//                  merch_cc_term_name            canceled_processor,
//                  merch_term_reason             canceled_reason,
//                  merch_term_year               cancel_date_year,
//                  merch_term_month              cancel_date_month,
//                  merch_month_tot_proj_sales    monthly_sales,
//                  merch_month_visa_mc_sales     monthly_vmc_sales,
//                  merch_average_cc_tran         average_ticket,
//                  refundtype_code               refund_policy,
//                  merch_prior_statements        statements_provided,
//                  percent_international         percent_international
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_prior_cc_accp_flag      have_processed,\n                merch_prior_processor         previous_processor,\n                merch_cc_acct_term_flag       have_canceled,\n                merch_cc_term_name            canceled_processor,\n                merch_term_reason             canceled_reason,\n                merch_term_year               cancel_date_year,\n                merch_term_month              cancel_date_month,\n                merch_month_tot_proj_sales    monthly_sales,\n                merch_month_visa_mc_sales     monthly_vmc_sales,\n                merch_average_cc_tran         average_ticket,\n                refundtype_code               refund_policy,\n                merch_prior_statements        statements_provided,\n                percent_international         percent_international\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.virtual.Page4Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.virtual.Page4Base",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:228^7*/
      setFields(it.getResultSet());
    }
    catch (Exception e)
    {
      logEntry("loadMerchant("+appSeqNum+")", e.toString());
      throw( e );
    }
    finally
    {
      it.close();
    }
  }
  
  protected boolean loadPageData(long appSeqNum)
  {
    boolean loadOk = false;
    
    try
    {
      if( appSeqNum != 0L )
      {
        loadMerchant(appSeqNum);
      }
      
      loadOk = true;
    }
    catch(Exception e)
    {
      logEntry("loadAppData()", e.toString());
    }
    
    return( loadOk );
  }
}/*@lineinfo:generated-code*/