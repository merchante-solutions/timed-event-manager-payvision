/*@lineinfo:filename=WPPEmail*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/Page1Base.sqlj $

  Description:

  Page1Base

  Base bean for Page 1 of virtual app (merchant-submission version)

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-04-18 16:44:44 -0700 (Fri, 18 Apr 2008) $
  Version            : $Revision: 14760 $

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.virtual.wpp;

import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;


public class WPPEmail extends SQLJConnectionBase
{
  public static String ServerUrl = "";
  
  public WPPEmail()
  {
  }
  
  public void send(long controlNum, int emailType, String toAddr)
  {
    try
    {
      boolean validType = false;
      
      MailMessage msg = new MailMessage();
      
      msg.setAddresses(emailType);
      msg.addTo(toAddr);
      
      StringBuffer  body    = new StringBuffer("");
      String        subject = "";
      
      switch(emailType)
      {
        case MesEmails.MSG_ADDRS_WPP_CONTACT:
          subject = "Merchant e-Solutions - Access your On-Line Merchant Application";
          body.append("Thank you for your interest in Merchant e-Solutions.  ");
          body.append("You have just taken your first step in completing our secure on line merchant application form.  ");
          body.append("Our easy to use, step-by-step process will lead you through the merchant application process.  ");
          body.append("At any time during this process, you may save your application data and return later.  ");
          body.append("To access your merchant application now and during the process, please use the following link: ");
          body.append(ServerUrl);
          body.append("/srv/apply?controlNum=");
          body.append(controlNum);
          body.append("\n\n\n");
          body.append("Merchant e-Solutions is looking forward to serving your payment needs.  ");
          body.append("Please call or email 1 888-898-7693 or sales@merchante-solutions.com with any questions.  ");
          body.append("\n\n");
          body.append("Best Regards");
          body.append("\n\n\n");
          body.append("Sales\n");
          body.append("Merchant e-Solutions\n");
          body.append("www.merchante-solutions.com\n");
          validType = true;
          break;
        
        case MesEmails.MSG_ADDRS_WPP_COMPLETE:
          subject = "Merchant e-Solutions - On-Line Merchant Application is Complete";
          body.append("Thank you for completing Merchant e-Solutions on line merchant application form.  ");
          body.append("Your application will be reviewed within the next 24 hours.  ");
          body.append("In the meantime, if you have any questions, please call or email 1 888-898-7693 or sales@merchante-solutions.com with any questions.  ");
          body.append("\n\n");
          body.append("We are looking forward to serving your payment processing needs both now and in the years to come.  ");
          body.append("\n\n");
          body.append("Best Regards");
          body.append("\n\n\n");
          body.append("Sales\n");
          body.append("Merchant e-Solutions\n");
          body.append("www.merchante-solutions.com\n");
          validType = true;
          break;
      }
      
      if( validType )
      {
        msg.setSubject(subject);
        msg.setText(body.toString());
        
        msg.send();
      }
    }
    catch(Exception e)
    {
      logEntry("send(" + controlNum + ", " + emailType + ", " + toAddr + ")", e.toString());
    }
  }
  
  public static void sendWPPEmail(long controlNum, int emailType, String toAddr)
  {
    WPPEmail worker = new WPPEmail();
    
    worker.send(controlNum, emailType, toAddr);
  }
}/*@lineinfo:generated-code*/