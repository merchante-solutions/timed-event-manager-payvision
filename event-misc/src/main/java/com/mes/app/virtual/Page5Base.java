/*@lineinfo:filename=Page5Base*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/Page1Base.sqlj $

  Description:

  Page1Base

  Base bean for Page 1 of virtual app (merchant-submission version)

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-04-18 16:44:44 -0700 (Fri, 18 Apr 2008) $
  Version            : $Revision: 14760 $

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.virtual;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.forms.CheckboxField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TaxIdField;
import com.mes.forms.ZipField;
import com.mes.support.XSSFilter;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Page5Base extends MerchantFacingAppBase
{
  static Logger log = Logger.getLogger(Page3Base.class);
  
  private class AccountInfoSourceTable extends DropDownTable
  {
    public AccountInfoSourceTable() throws java.sql.SQLException
    {
      ResultSetIterator it  = null;
      ResultSet         rs  = null;

      try
      {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:52^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  source_id,
//                    source_desc
//            from    bank_account_info_source
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  source_id,\n                  source_desc\n          from    bank_account_info_source";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.virtual.Page5Base",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.virtual.Page5Base",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:57^9*/
        rs = it.getResultSet();

        while(rs.next())
        {
          addElement(rs);
        }
      }
      finally
      {
        try{ it.close(); } catch(Exception e) { }
        cleanUp();
      }
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields( request );
    
    try
    {
      // primary owner
      FieldGroup gOwner1 = new FieldGroup("gOwner1");

      gOwner1.add   (new Field            ("owner1FirstName",   "First Name",25, 15,false));
      gOwner1.add   (new Field            ("owner1LastName",    "Last Name",25, 15,false));
      gOwner1.add   (new Field            ("owner1Title",       "Title",40,15,true));
      gOwner1.add   (new TaxIdField       ("owner1Ssn",         "Soc. Security #",false));
      gOwner1.add   (new DateStringField  ("owner1Since",       "Owner/Officer Since",true,true));
      gOwner1.add   (new NumberField      ("owner1Percent",     "Percent Owned",3,3,false,0));
      gOwner1.add   (new CheckboxField    ("addrSameAsBusiness","Address Same as Business",false,false));
      gOwner1.add   (new Field            ("owner1Address1",    "Address line 1",32,15,false));
      gOwner1.add   (new Field            ("owner1Address2",    "Address line 2",32,15,true));
      gOwner1.add   (new Field            ("owner1City",        "City",25,15,false));
      gOwner1.add   (new DropDownField    ("owner1State",       "State", new StateDropDownTable(), false));
      gOwner1.add   (new ZipField         ("owner1Zip",         "Zip Code", false, fields.getField("Owner1state")));
      gOwner1.add   (new EmailField       ("owner1Email",       "E-mail Address",75,30,false));
      gOwner1.add   (new PhoneField       ("owner1Phone",       "Phone Number",false));
      
      CheckboxField addrSameAsBusiness = (CheckboxField)gOwner1.getField("addrSameAsBusiness");
      
      // add javascript
      addrSameAsBusiness.setOnClick("SetAddressView()");
      
      gOwner1.getField("owner1Address1").setOptionalCondition(
        new FieldValueCondition(addrSameAsBusiness,"n"));
      gOwner1.getField("owner1City").setOptionalCondition(
        new FieldValueCondition(addrSameAsBusiness,"n"));
      gOwner1.getField("owner1State").setOptionalCondition(
        new FieldValueCondition(addrSameAsBusiness,"n"));
      gOwner1.getField("owner1Zip").setOptionalCondition(
        new FieldValueCondition(addrSameAsBusiness,"n"));
      gOwner1.getField("owner1Email").setOptionalCondition(
        new FieldValueCondition(addrSameAsBusiness,"n"));
      gOwner1.getField("owner1Phone").setOptionalCondition(
        new FieldValueCondition(addrSameAsBusiness,"n"));
      
      gOwner1.getField("owner1Percent")
        .addValidation(new PercentValidation(
          "Invalid Percentage",1,100));

      fields.add(gOwner1);
      
      // bank account info
      FieldGroup gBankAcct = new FieldGroup("gBankAcct");

      gBankAcct.add (new Field            ("bankName",          "Name of Bank",30,30,false));
      gBankAcct.add (new Field            ("checkingAccount",   "Checking Acct. #",17,30,false));
      gBankAcct.add (new Field            ("confirmCheckingAccount","Confirm Account #",17,30,false));
      gBankAcct.add (new Field            ("transitRouting",    "Transit Routing #",9,30,false));
      gBankAcct.add (new Field            ("confirmTransitRouting","Confirm Transit Routing #",9,30,false));
      gBankAcct.add (new DropDownField    ("sourceOfInfo",      "Source of Acct. Info",new AccountInfoSourceTable(),false));
      gBankAcct.add (new Field            ("bankAddress",       "Bank Address",32,25,false));
      gBankAcct.add (new Field            ("bankCity",          "City",25,25,false));
      gBankAcct.add (new DropDownField    ("bankState",         "State", new StateDropDownTable(), false));
      gBankAcct.add (new ZipField         ("bankZip",           "Zip Code", false, fields.getField("bankState")));
      gBankAcct.add (new PhoneField       ("bankPhone",         "Bank Phone",true));
      
      gBankAcct.add (new HiddenField      ("typeOfAcct"));
      gBankAcct.add (new HiddenField      ("billingMethod"));
      
      gBankAcct.getField("typeOfAcct").setData("2");
      gBankAcct.getField("billingMethod").setData("Debit"); // default

      gBankAcct.getField("transitRouting")
        .addValidation(new TransitRoutingValidation());
        
      // add validation of the confirmation fields
      gBankAcct.getField("confirmCheckingAccount")
        .addValidation(new FieldEqualsFieldValidation(
          gBankAcct.getField("checkingAccount"),"Checking Account #"));
      gBankAcct.getField("confirmTransitRouting")
        .addValidation(new FieldEqualsFieldValidation(
          gBankAcct.getField("transitRouting"),"Transit Routing #"));

      fields.add(gBankAcct);
      
      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
    }
  }
  
  protected void submitBank(long appSeqNum)
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:168^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchbank
//          where   app_seq_num = :appSeqNum
//                  and merchbank_acct_srnum = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merchbank\n        where   app_seq_num =  :1 \n                and merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.virtual.Page5Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:174^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:176^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchbank
//          (
//            app_seq_num,
//            bankacc_type,
//            merchbank_info_source,
//            merchbank_name,
//            merchbank_acct_num,
//            merchbank_transit_route_num,
//            merchbank_num_years_open,
//            billing_method,
//            merchbank_acct_srnum
//          )
//          values
//          (
//            :appSeqNum,
//            :XSSFilter.encodeXSS(getData("typeOfAcct")),
//            :XSSFilter.encodeXSS(getData("sourceOfInfo")),
//            :XSSFilter.encodeXSS(getData("bankName")),
//            :XSSFilter.encodeXSS(getData("checkingAccount")),
//            :XSSFilter.encodeXSS(getData("transitRouting")),
//            :XSSFilter.encodeXSS(getData("yearsOpen")),
//            :XSSFilter.encodeXSS(getData("billingMethod")),
//            1
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3345 = XSSFilter.encodeXSS(getData("typeOfAcct"));
 String __sJT_3346 = XSSFilter.encodeXSS(getData("sourceOfInfo"));
 String __sJT_3347 = XSSFilter.encodeXSS(getData("bankName"));
 String __sJT_3348 = XSSFilter.encodeXSS(getData("checkingAccount"));
 String __sJT_3349 = XSSFilter.encodeXSS(getData("transitRouting"));
 String __sJT_3350 = XSSFilter.encodeXSS(getData("yearsOpen"));
 String __sJT_3351 = XSSFilter.encodeXSS(getData("billingMethod"));
   String theSqlTS = "insert into merchbank\n        (\n          app_seq_num,\n          bankacc_type,\n          merchbank_info_source,\n          merchbank_name,\n          merchbank_acct_num,\n          merchbank_transit_route_num,\n          merchbank_num_years_open,\n          billing_method,\n          merchbank_acct_srnum\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n          1\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.virtual.Page5Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_3345);
   __sJT_st.setString(3,__sJT_3346);
   __sJT_st.setString(4,__sJT_3347);
   __sJT_st.setString(5,__sJT_3348);
   __sJT_st.setString(6,__sJT_3349);
   __sJT_st.setString(7,__sJT_3350);
   __sJT_st.setString(8,__sJT_3351);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:202^7*/
    }
    catch(Exception e)
    {
      logEntry("submitBank(" + appSeqNum + ")", e.toString());
      throw( e );
    }
  }
  
  protected void submitOwner(long appSeqNum)
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:216^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    businessowner
//          where   app_seq_num = :appSeqNum
//                  and busowner_num = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    businessowner\n        where   app_seq_num =  :1 \n                and busowner_num = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.virtual.Page5Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:222^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:224^7*/

//  ************************************************************
//  #sql [Ctx] { insert into businessowner
//          (
//            app_seq_num,
//            busowner_num,
//            busowner_first_name,
//            busowner_last_name,
//            busowner_ssn,
//            busowner_owner_perc,
//            busowner_period_month,
//            busowner_period_year,
//            busowner_title
//          )
//          values
//          (
//            :appSeqNum,
//            1,
//            :XSSFilter.encodeXSS(getData("owner1FirstName")),
//            :XSSFilter.encodeXSS(getData("owner1LastName")),
//            :XSSFilter.encodeXSS(getData("owner1Ssn")),
//            :XSSFilter.encodeXSS(getData("owner1Percent")),
//            :XSSFilter.encodeXSS(getData("owner1SinceMonth")),
//            :XSSFilter.encodeXSS(getData("owner1SinceYear")),
//            :XSSFilter.encodeXSS(getData("owner1Title"))
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3352 = XSSFilter.encodeXSS(getData("owner1FirstName"));
 String __sJT_3353 = XSSFilter.encodeXSS(getData("owner1LastName"));
 String __sJT_3354 = XSSFilter.encodeXSS(getData("owner1Ssn"));
 String __sJT_3355 = XSSFilter.encodeXSS(getData("owner1Percent"));
 String __sJT_3356 = XSSFilter.encodeXSS(getData("owner1SinceMonth"));
 String __sJT_3357 = XSSFilter.encodeXSS(getData("owner1SinceYear"));
 String __sJT_3358 = XSSFilter.encodeXSS(getData("owner1Title"));
   String theSqlTS = "insert into businessowner\n        (\n          app_seq_num,\n          busowner_num,\n          busowner_first_name,\n          busowner_last_name,\n          busowner_ssn,\n          busowner_owner_perc,\n          busowner_period_month,\n          busowner_period_year,\n          busowner_title\n        )\n        values\n        (\n           :1 ,\n          1,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.virtual.Page5Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_3352);
   __sJT_st.setString(3,__sJT_3353);
   __sJT_st.setString(4,__sJT_3354);
   __sJT_st.setString(5,__sJT_3355);
   __sJT_st.setString(6,__sJT_3356);
   __sJT_st.setString(7,__sJT_3357);
   __sJT_st.setString(8,__sJT_3358);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:250^7*/
    }
    catch(Exception e)
    {
      logEntry("submitOwner("+appSeqNum+")", e.toString());
      throw( e );
    }
  }
  
  protected void submitAddresses(long appSeqNum)
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:264^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    address
//          where   app_seq_num = :appSeqNum
//                  and addresstype_code = :mesConstants.ADDR_TYPE_OWNER1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    address\n        where   app_seq_num =  :1 \n                and addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.virtual.Page5Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_OWNER1);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:270^7*/
      
      if( getData("addrSameAsBusiness").equals("y") )
      {
        /*@lineinfo:generated-code*//*@lineinfo:274^9*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//            (
//              address_name,
//              address_line1,
//              address_line2,
//              address_city,
//              countrystate_code,
//              address_zip,
//              address_phone,
//              addresstype_code,
//              app_seq_num
//            )
//            select  address_name,
//                    address_line1,
//                    address_line2,
//                    address_city,
//                    countrystate_code,
//                    address_zip,
//                    address_phone,
//                    :mesConstants.ADDR_TYPE_OWNER1,
//                    app_seq_num
//            from    address
//            where   app_seq_num = :appSeqNum
//                    and addresstype_code = :mesConstants.ADDR_TYPE_BUSINESS
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into address\n          (\n            address_name,\n            address_line1,\n            address_line2,\n            address_city,\n            countrystate_code,\n            address_zip,\n            address_phone,\n            addresstype_code,\n            app_seq_num\n          )\n          select  address_name,\n                  address_line1,\n                  address_line2,\n                  address_city,\n                  countrystate_code,\n                  address_zip,\n                  address_phone,\n                   :1 ,\n                  app_seq_num\n          from    address\n          where   app_seq_num =  :2 \n                  and addresstype_code =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.app.virtual.Page5Base",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.ADDR_TYPE_OWNER1);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,mesConstants.ADDR_TYPE_BUSINESS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:300^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:302^9*/

//  ************************************************************
//  #sql [Ctx] { update  address
//            set     email_address = (select mr.merch_email_address from merchant mr where mr.app_seq_num = :appSeqNum)
//            where   app_seq_num = :appSeqNum
//                    and addresstype_code = :mesConstants.ADDR_TYPE_OWNER1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  address\n          set     email_address = (select mr.merch_email_address from merchant mr where mr.app_seq_num =  :1 )\n          where   app_seq_num =  :2 \n                  and addresstype_code =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.app.virtual.Page5Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,mesConstants.ADDR_TYPE_OWNER1);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:308^9*/
      }
      else
      {
        StringBuffer ownerName = new StringBuffer("");
      
        ownerName.append(getData("owner1FirstName"));
        ownerName.append(" ");
        ownerName.append(getData("owner1LastName"));
      
        String ownerNameVal = "";
        if( ownerName.length() > 40 )
        {
          ownerNameVal = ownerName.substring(0, 40);
        }
        else
        {
          ownerNameVal = ownerName.toString();
        }
      
      
        /*@lineinfo:generated-code*//*@lineinfo:329^9*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//            (
//              address_name,
//              address_line1,
//              address_line2,
//              address_city,
//              countrystate_code,
//              address_zip,
//              address_phone,
//              email_address,
//              app_seq_num,
//              addresstype_code
//            )
//            values
//            (                                    
//              :ownerNameVal,
//              :XSSFilter.encodeXSS(getData("owner1Address1")),
//              :XSSFilter.encodeXSS(getData("owner1Address2")),
//              :XSSFilter.encodeXSS(getData("owner1City")),
//              :XSSFilter.encodeXSS(getData("owner1State")),
//              :XSSFilter.encodeXSS(getData("owner1Zip")),
//              :XSSFilter.encodeXSS(getData("owner1Phone")),
//              :XSSFilter.encodeXSS(getData("owner1Email")),
//              :appSeqNum,
//              :mesConstants.ADDR_TYPE_OWNER1
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3359 = XSSFilter.encodeXSS(getData("owner1Address1"));
 String __sJT_3360 = XSSFilter.encodeXSS(getData("owner1Address2"));
 String __sJT_3361 = XSSFilter.encodeXSS(getData("owner1City"));
 String __sJT_3362 = XSSFilter.encodeXSS(getData("owner1State"));
 String __sJT_3363 = XSSFilter.encodeXSS(getData("owner1Zip"));
 String __sJT_3364 = XSSFilter.encodeXSS(getData("owner1Phone"));
 String __sJT_3365 = XSSFilter.encodeXSS(getData("owner1Email"));
   String theSqlTS = "insert into address\n          (\n            address_name,\n            address_line1,\n            address_line2,\n            address_city,\n            countrystate_code,\n            address_zip,\n            address_phone,\n            email_address,\n            app_seq_num,\n            addresstype_code\n          )\n          values\n          (                                    \n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.app.virtual.Page5Base",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,ownerNameVal);
   __sJT_st.setString(2,__sJT_3359);
   __sJT_st.setString(3,__sJT_3360);
   __sJT_st.setString(4,__sJT_3361);
   __sJT_st.setString(5,__sJT_3362);
   __sJT_st.setString(6,__sJT_3363);
   __sJT_st.setString(7,__sJT_3364);
   __sJT_st.setString(8,__sJT_3365);
   __sJT_st.setLong(9,appSeqNum);
   __sJT_st.setInt(10,mesConstants.ADDR_TYPE_OWNER1);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:357^9*/
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:360^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    address
//          where   app_seq_num = :appSeqNum
//                  and addresstype_code = :mesConstants.ADDR_TYPE_CHK_ACCT_BANK
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    address\n        where   app_seq_num =  :1 \n                and addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.virtual.Page5Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_CHK_ACCT_BANK);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:366^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:368^7*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//          (
//            address_name,
//            address_line1,
//            address_city,
//            countrystate_code,
//            address_zip,
//            address_phone,
//            app_seq_num,
//            addresstype_code
//          )
//          values
//          (
//            :XSSFilter.encodeXSS(getData("bankName")),
//            :XSSFilter.encodeXSS(getData("bankAddress")),
//            :XSSFilter.encodeXSS(getData("bankCity")),
//            :XSSFilter.encodeXSS(getData("bankState")),
//            :XSSFilter.encodeXSS(getData("bankZip")),
//            :XSSFilter.encodeXSS(getData("bankPhone")),
//            :appSeqNum,
//            :mesConstants.ADDR_TYPE_CHK_ACCT_BANK
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3366 = XSSFilter.encodeXSS(getData("bankName"));
 String __sJT_3367 = XSSFilter.encodeXSS(getData("bankAddress"));
 String __sJT_3368 = XSSFilter.encodeXSS(getData("bankCity"));
 String __sJT_3369 = XSSFilter.encodeXSS(getData("bankState"));
 String __sJT_3370 = XSSFilter.encodeXSS(getData("bankZip"));
 String __sJT_3371 = XSSFilter.encodeXSS(getData("bankPhone"));
   String theSqlTS = "insert into address\n        (\n          address_name,\n          address_line1,\n          address_city,\n          countrystate_code,\n          address_zip,\n          address_phone,\n          app_seq_num,\n          addresstype_code\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.app.virtual.Page5Base",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3366);
   __sJT_st.setString(2,__sJT_3367);
   __sJT_st.setString(3,__sJT_3368);
   __sJT_st.setString(4,__sJT_3369);
   __sJT_st.setString(5,__sJT_3370);
   __sJT_st.setString(6,__sJT_3371);
   __sJT_st.setLong(7,appSeqNum);
   __sJT_st.setInt(8,mesConstants.ADDR_TYPE_CHK_ACCT_BANK);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:392^7*/
    }
    catch(Exception e)
    {
      logEntry("submitAddresses("+appSeqNum+")", e.toString());
      throw( e );
    }
  }
  
  protected boolean submitAppData()
  {
    boolean submitOk = false;
    
    try
    {
      connect();
      setAutoCommit(false);
      
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if( appSeqNum != 0 )
      {
        submitOwner(appSeqNum);
        submitAddresses(appSeqNum);
        submitBank(appSeqNum);
      }
      
      commit();
      
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitData", e.toString());
      rollback();
    }
    finally
    {
      cleanUp();
    }
    
    return( submitOk );
  }
  
  protected void loadOwner(long appSeqNum)
  {
    ResultSetIterator it = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:442^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_first_name   owner1_first_name,
//                  busowner_last_name    owner1_last_name,
//                  busowner_ssn          owner1_ssn,
//                  busowner_owner_perc   owner1_percent,
//                  busowner_period_month owner1_since_month,
//                  busowner_period_year  owner1_since_year,
//                  busowner_title        owner1_title
//          from    businessowner
//          where   app_seq_num = :appSeqNum
//                  and busowner_num = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_first_name   owner1_first_name,\n                busowner_last_name    owner1_last_name,\n                busowner_ssn          owner1_ssn,\n                busowner_owner_perc   owner1_percent,\n                busowner_period_month owner1_since_month,\n                busowner_period_year  owner1_since_year,\n                busowner_title        owner1_title\n        from    businessowner\n        where   app_seq_num =  :1 \n                and busowner_num = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.app.virtual.Page5Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.app.virtual.Page5Base",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:454^7*/
      setFields(it.getResultSet());
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadOwner(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  protected void loadAddresses(long appSeqNum)
  {
    ResultSetIterator it = null;
    try
    {
      // owner 1 address
      /*@lineinfo:generated-code*//*@lineinfo:474^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1     owner1_address1,
//                  address_line2     owner1_address2,
//                  address_city      owner1_city,
//                  countrystate_code owner1_state,
//                  address_zip       owner1_zip,
//                  address_phone     owner1_phone,
//                  email_address     owner1_email
//          from    address
//          where   app_seq_num = :appSeqNum and
//                  addresstype_code = :mesConstants.ADDR_TYPE_OWNER1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1     owner1_address1,\n                address_line2     owner1_address2,\n                address_city      owner1_city,\n                countrystate_code owner1_state,\n                address_zip       owner1_zip,\n                address_phone     owner1_phone,\n                email_address     owner1_email\n        from    address\n        where   app_seq_num =  :1  and\n                addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.app.virtual.Page5Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_OWNER1);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.app.virtual.Page5Base",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:486^7*/
      setFields(it.getResultSet());
      it.close();
      
      // bank address
      /*@lineinfo:generated-code*//*@lineinfo:491^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1     bank_address,
//                  address_city      bank_city,
//                  countrystate_code bank_state,
//                  address_zip       bank_zip,
//                  address_phone     bank_phone
//          from    address
//          where   app_seq_num = :appSeqNum
//                  and addresstype_code = :mesConstants.ADDR_TYPE_CHK_ACCT_BANK
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1     bank_address,\n                address_city      bank_city,\n                countrystate_code bank_state,\n                address_zip       bank_zip,\n                address_phone     bank_phone\n        from    address\n        where   app_seq_num =  :1 \n                and addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.app.virtual.Page5Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_CHK_ACCT_BANK);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.app.virtual.Page5Base",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:501^7*/
      setFields(it.getResultSet());
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadAddresses("+appSeqNum+")", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  protected void loadBank(long appSeqNum)
  {
    ResultSetIterator it = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:521^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bankacc_type                type_of_acct,
//                  merchbank_info_source       source_of_info,
//                  merchbank_name              bank_name,
//                  merchbank_acct_num          checking_account,
//                  merchbank_acct_num          confirm_checking_account,
//                  merchbank_transit_route_num transit_routing,
//                  merchbank_transit_route_num confirm_transit_routing,
//                  billing_method              billing_method
//          from    merchbank
//          where   app_seq_num = :appSeqNum
//                  and merchbank_acct_srnum = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bankacc_type                type_of_acct,\n                merchbank_info_source       source_of_info,\n                merchbank_name              bank_name,\n                merchbank_acct_num          checking_account,\n                merchbank_acct_num          confirm_checking_account,\n                merchbank_transit_route_num transit_routing,\n                merchbank_transit_route_num confirm_transit_routing,\n                billing_method              billing_method\n        from    merchbank\n        where   app_seq_num =  :1 \n                and merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.app.virtual.Page5Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.app.virtual.Page5Base",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:534^7*/
      setFields(it.getResultSet());
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadBank("+appSeqNum+")", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  protected boolean loadPageData(long appSeqNum)
  {
    boolean loadOk = false;
    
    try
    {
      if( appSeqNum != 0L )
      {
        loadOwner(appSeqNum);
        loadAddresses(appSeqNum);
        loadBank(appSeqNum);
      }
      
      loadOk = true;
    }
    catch(Exception e)
    {
      logEntry("loadAppData()", e.toString());
    }
    
    return( loadOk );
  }
}/*@lineinfo:generated-code*/