/*@lineinfo:filename=Page6Base*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/Page1Base.sqlj $

  Description:

  Page1Base

  Base bean for Page 1 of virtual app (merchant-submission version)

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-04-18 16:44:44 -0700 (Fri, 18 Apr 2008) $
  Version            : $Revision: 14760 $

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.virtual;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.forms.CheckboxField;
import com.mes.forms.DisabledCheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.TextareaField;
import com.mes.forms.WebAddressField;
import com.mes.support.XSSFilter;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Page6Base extends MerchantFacingAppBase
{
  static Logger log = Logger.getLogger(Page3Base.class);
  
  protected static final String[][] yesNoRadioList =
  {
    { "No",  "N" },
    { "Yes", "Y" }
  };

  protected static final String[][] yesNoNARadioList =
  {
    { "No",  "N" },
    { "Yes", "Y" },
    { "N/A", "*" }
  };
  
  protected class ShopCartDropDownTable extends DropDownTable
  {
    public ShopCartDropDownTable( )
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      
      try
      {
        connect();
        
        addElement("", "select one");
        /*@lineinfo:generated-code*//*@lineinfo:67^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cart_id,
//                    cart_desc
//            from    shopping_carts
//            order by cart_desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cart_id,\n                  cart_desc\n          from    shopping_carts\n          order by cart_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.virtual.Page6Base",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.virtual.Page6Base",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:73^9*/
        rs = it.getResultSet();
        
        while(rs.next())
        {
          addElement(rs.getString("cart_id"), rs.getString("cart_desc"));
        }
        addElement("-1", "Other");
        addElement("-1", "Not made a decision yet");
      }
      catch(Exception e)
      {
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields( request );
    
    try
    {
      // payment options
      FieldGroup gPayOpts = new FieldGroup("gPayOpts");

      gPayOpts.add  (new DisabledCheckboxField("vmcAccepted",     "Visa/MasterCard",true,false));
      gPayOpts.add  (new CheckboxField        ("discoverAccepted","Discover",false,false));
      gPayOpts.add  (new CheckboxField        ("amexAccepted",    "American Express",false,false));
      gPayOpts.add  (new NumberField          ("amexAcctNum",     "Amex Acct #",16,25,true,0));
      gPayOpts.add  (new CheckboxField        ("amexNeeded",      "Need New Amex Acct#",false,false));
      gPayOpts.add  (new CheckboxField        ("payPalAccepted",  "PayPal Payments",false,false));
      gPayOpts.add  (new CheckboxField        ("bmlAccepted",     "BillMeLater",false,false));
      gPayOpts.add  (new WebAddressField      ("webUrl",          100,45,true));
      
      CheckboxField amexAccepted = (CheckboxField)(gPayOpts.getField("amexAccepted"));
      
      // add javascript
      amexAccepted.setOnClick("SetAmexView()");
      
      CardAcceptedValidation amexVal = new CardAcceptedValidation();
      amexVal.addField(gPayOpts.getField("amexAcctNum"));
      amexVal.addField(gPayOpts.getField("amexNeeded"));
      gPayOpts.getField("amexAccepted").addValidation(amexVal, "amexValidation");
      
      fields.add(gPayOpts);
      
      FieldGroup gInt = new FieldGroup("gInt");
      
      gInt.add( new RadioButtonField("useShoppingCart", yesNoNARadioList,-1,false,"Required"));
      gInt.add( new DropDownField   ("shoppingCart", new ShopCartDropDownTable(), true));
      gInt.add( new RadioButtonField("useApi", yesNoRadioList,-1,true,"Required"));
      gInt.add( new TextareaField   ("comments", 200,5,20,true));
      gInt.add( new CheckboxField   ("agreeMerchant", "Merchant Agreement", false, false));
      gInt.add( new CheckboxField   ("agreePricing", "Pricing Schedule", false, false));
      gInt.add( new HiddenField     ("adminEmail"));
      
      RadioButtonField tmp = (RadioButtonField)(gInt.getField("useShoppingCart"));
      tmp.setOnClick(0, "javascript:SetCartView(0)");
      tmp.setOnClick(1, "javascript:SetCartView(1)");
      tmp.setOnClick(2, "javascript:SetCartView(2)");
      
      gInt.getField("shoppingCart").addValidation(new RequiredIfValueValidation(gInt.getField("useShoppingCart"), "Y", "Required"));
      gInt.getField("agreeMerchant").addValidation(new CheckboxRequiredValidation());
      gInt.getField("agreePricing").addValidation(new CheckboxRequiredValidation());
      
      fields.add(gInt);
      
      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
    }
  }
  
  private void submitCard(long appSeqNum, int cardType, String acctNum, String rate, int cardSeq)
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:159^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchpayoption
//          where   app_seq_num = :appSeqNum
//                  and cardtype_code = :cardType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merchpayoption\n        where   app_seq_num =  :1 \n                and cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.virtual.Page6Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:165^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:167^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchpayoption
//          (
//            app_seq_num,
//            cardtype_code,
//            merchpo_card_merch_number,
//            card_sr_number,
//            merchpo_rate
//          )
//          values
//          (
//            :appSeqNum,
//            :cardType,
//            :acctNum,
//            :cardSeq,
//            :rate
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchpayoption\n        (\n          app_seq_num,\n          cardtype_code,\n          merchpo_card_merch_number,\n          card_sr_number,\n          merchpo_rate\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.virtual.Page6Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
   __sJT_st.setString(3,acctNum);
   __sJT_st.setInt(4,cardSeq);
   __sJT_st.setString(5,rate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:185^7*/
    }
    catch(Exception e)
    {
      logEntry("submitCard(" + appSeqNum + ")", e.toString());
      throw( e );
    }
  }
  
  private void submitCards(long appSeqNum)
    throws Exception
  {
    int cardSeq = 0;
    try
    {
      // visa/mastercard
      submitCard(appSeqNum, mesConstants.APP_CT_VISA, "", "",++cardSeq);
      submitCard(appSeqNum, mesConstants.APP_CT_MC, "", "",++cardSeq);
      
      // discover
      if( getData("discoverAccepted").equals("y") )
      {
        submitCard(appSeqNum, mesConstants.APP_CT_DISCOVER, "", "2.44", ++cardSeq);
      }
      
      if( getData("amexAccepted").equals("y") )
      {
        String amexRate = "";
        if( getData("amexNeeded").equals("y") )
        {
          amexRate = "3.50";
        }
        submitCard(appSeqNum, mesConstants.APP_CT_AMEX, getData("amexAcctNum"), amexRate, ++cardSeq);
      }
      
      if( getData("payPalAccepted").equals("y") )
      {
        submitCard(appSeqNum, mesConstants.APP_CT_PAYPAL, "", "", ++cardSeq);
      }
      
    }
    catch(Exception e)
    {
      logEntry("submitCardAcceptance(" + appSeqNum + ")", e.toString());
      throw( e );
    }
  }
  
  private void submitProduct(long appSeqNum)
    throws Exception
  {
    int     posType   = mesConstants.POS_UNKNOWN;
    int     posCode   = mesConstants.APP_MPOS_UNKNOWN;
    String  posParam  = "";
    String  pgEmail   = "";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:243^7*/

//  ************************************************************
//  #sql [Ctx] { update  virtual_app_contact
//          set     use_shopping_cart   = :getData("useShoppingCart"),
//                  shopping_cart_desc  = :getData("shoppingCart"),
//                  use_api             = :getData("useApi"),
//                  agree_merchant      = :getData("agreeMerchant"),
//                  agree_pricing       = :getData("agreePricing"),
//                  solution_comments   = :XSSFilter.encodeXSS(getData("comments"))
//          where   app_seq_num         = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3372 = getData("useShoppingCart");
 String __sJT_3373 = getData("shoppingCart");
 String __sJT_3374 = getData("useApi");
 String __sJT_3375 = getData("agreeMerchant");
 String __sJT_3376 = getData("agreePricing");
 String __sJT_3377 = XSSFilter.encodeXSS(getData("comments"));
   String theSqlTS = "update  virtual_app_contact\n        set     use_shopping_cart   =  :1 ,\n                shopping_cart_desc  =  :2 ,\n                use_api             =  :3 ,\n                agree_merchant      =  :4 ,\n                agree_pricing       =  :5 ,\n                solution_comments   =  :6 \n        where   app_seq_num         =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.virtual.Page6Base",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3372);
   __sJT_st.setString(2,__sJT_3373);
   __sJT_st.setString(3,__sJT_3374);
   __sJT_st.setString(4,__sJT_3375);
   __sJT_st.setString(5,__sJT_3376);
   __sJT_st.setString(6,__sJT_3377);
   __sJT_st.setLong(7,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:253^7*/
      
      // determine product selection from answers to questions
      if( getData("ad").equals("vt") )
      {
        // merchant wants virtual terminal, that was easy.
        posType = mesConstants.POS_VIRTUAL_TERMINAL;
        posCode = mesConstants.APP_MPOS_VIRTUAL_TERMINAL;
        posParam = getData("adminEmail");
        pgEmail = getData("adminEmail");
      }
      else
      {
        if( getData("useShoppingCart").equals("Y") )
        {
          if(getData("shoppingCart").equals("-1"))
          {
            posType = mesConstants.POS_UNKNOWN;
            posCode = mesConstants.APP_MPOS_UNKNOWN;
            posParam = getData("adminEmail");
            pgEmail = getData("adminEmail");
          }
          else
          {
            // if shopping cart is supported by MES then they will be using our payment gateway
            // otherwise they will be using PayPal
            int recCount = 0;
            /*@lineinfo:generated-code*//*@lineinfo:280^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//                
//                from    shopping_carts
//                where   cart_id = :getData("shoppingCart")
//                        and supports_mes = 'Y'
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3378 = getData("shoppingCart");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n               \n              from    shopping_carts\n              where   cart_id =  :1 \n                      and supports_mes = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.virtual.Page6Base",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3378);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:287^13*/
            
            if(recCount > 0 && getData("payPalAccepted").equals("n"))
            {
              // MES Payment Gateway
              posType = mesConstants.POS_PAYMENT_GATEWAY;
              posCode = mesConstants.APP_MPOS_PAYMENT_GATEWAY_CNP;
              posParam = getData("webUrl");
              pgEmail = getData("adminEmail");
            }
            else
            {
              posType = mesConstants.POS_INTERNET;
              posCode = mesConstants.APP_MPOS_VERISIGN_PAYFLOW_PRO;
              posParam = getData("webUrl");
              pgEmail = getData("adminEmail");
            }
          }
        }
        else
        {
          // no shopping cart, may be intending to code to API
          if( getData("useApi").equals("Y") )
          {
            posType = mesConstants.POS_PAYMENT_GATEWAY;
            posCode = mesConstants.APP_MPOS_PAYMENT_GATEWAY_CNP;
            posParam = getData("webUrl");
            pgEmail = getData("adminEmail");
          }
          else
          {
            posType = mesConstants.POS_UNKNOWN;
            posCode = mesConstants.APP_MPOS_UNKNOWN;
            posParam = getData("webUrl");
            pgEmail = getData("adminEmail");
          }
        }
      }
      
      // now we have pos type.  add to proper database table to flesh out app
      /*@lineinfo:generated-code*//*@lineinfo:327^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merch_pos
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merch_pos\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.virtual.Page6Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:332^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:334^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_edc_flag = 'N',
//                  industype_code = 5
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     merch_edc_flag = 'N',\n                industype_code = 5\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.app.virtual.Page6Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:340^7*/

      /*@lineinfo:generated-code*//*@lineinfo:342^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_pos
//          (
//            app_seq_num,
//            pos_code,
//            pos_param,
//            pg_email,
//            existing
//          )
//          values
//          (
//            :appSeqNum,
//            :posCode,
//            :posParam,
//            :pgEmail,
//            'N'
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_pos\n        (\n          app_seq_num,\n          pos_code,\n          pos_param,\n          pg_email,\n          existing\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n          'N'\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.app.virtual.Page6Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,posCode);
   __sJT_st.setString(3,posParam);
   __sJT_st.setString(4,pgEmail);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:360^7*/
      
    }
    catch(Exception e)
    {
      logEntry("submitProduct(" + appSeqNum + ")", e.toString());
      throw( e );
    }
  }
  
  protected boolean submitAppData()
  {
    boolean submitOk = false;
    
    try
    {
      connect();
      setAutoCommit(false);
      
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if( appSeqNum != 0 )
      {
        submitCards(appSeqNum);
        submitProduct(appSeqNum);
      }
      
      commit();
      
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitData", e.toString());
      rollback();
    }
    finally
    {
      cleanUp();
    }
    
    return( submitOk );
  }
  
  private void loadCards(long appSeqNum)
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:411^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code             card_type,
//                  merchpo_card_merch_number merchant_number,
//                  decode(merchpo_rate,
//                    null, 'n',
//                    'y')                    rate
//          from    merchpayoption
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code             card_type,\n                merchpo_card_merch_number merchant_number,\n                decode(merchpo_rate,\n                  null, 'n',\n                  'y')                    rate\n        from    merchpayoption\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.virtual.Page6Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.app.virtual.Page6Base",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:420^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        switch(rs.getInt("card_type"))
        {
          case mesConstants.APP_CT_VISA:
          case mesConstants.APP_CT_MC:
            break;
            
          case mesConstants.APP_CT_DISCOVER:
            setData("discoverAccepted", "y");
            break;
            
          case mesConstants.APP_CT_AMEX:
            setData("amexAccepted", "y");
            setData("amexAcctNum", rs.getString("merchant_number"));
            setData("amexNeeded", rs.getString("rate"));
            break;
            
          case mesConstants.APP_CT_PAYPAL:
            setData("payPalAccepted", "y");
            break;
          
          default:
            break;
        }
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadCards(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void loadProduct(long appSeqNum)
  {
    ResultSetIterator it = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:471^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  vac.use_shopping_cart   use_shopping_cart,
//                  vac.shopping_cart_desc  shopping_cart,
//                  vac.use_api             use_api,
//                  vac.solution_comments   comments,
//                  vac.agree_merchant      agree_merchant,
//                  vac.agree_pricing       agree_pricing,
//                  mr.merch_email_address  admin_email,
//                  mr.merch_web_url        web_url
//          from    virtual_app_contact vac,
//                  merchant mr
//          where   vac.app_seq_num = :appSeqNum
//                  and vac.app_seq_num = mr.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vac.use_shopping_cart   use_shopping_cart,\n                vac.shopping_cart_desc  shopping_cart,\n                vac.use_api             use_api,\n                vac.solution_comments   comments,\n                vac.agree_merchant      agree_merchant,\n                vac.agree_pricing       agree_pricing,\n                mr.merch_email_address  admin_email,\n                mr.merch_web_url        web_url\n        from    virtual_app_contact vac,\n                merchant mr\n        where   vac.app_seq_num =  :1 \n                and vac.app_seq_num = mr.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.virtual.Page6Base",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.app.virtual.Page6Base",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:485^7*/
      
      setFields(it.getResultSet());
      it.close();
      
      if(getData("ad").equals("vt"))
      {
        // default some fields
        setData("useShoppingCart", "A");
        setData("shoppingCart", "");
        setData("useApi", "N");
      }
    }
    catch(Exception e)
    {
      logEntry("loadProduct(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  protected boolean loadPageData(long appSeqNum)
  {
    boolean loadOk = false;
    
    try
    {
      if( appSeqNum != 0L )
      {
        loadCards(appSeqNum);
        loadProduct(appSeqNum);
      }
      
      loadOk = true;
    }
    catch(Exception e)
    {
      logEntry("loadPageData()", e.toString());
    }
    
    return( loadOk );
  }
  
  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
    
    try
    {
      if(getData("ad").equals("vt"))
      {
        // default some fields
        setData("useShoppingCart", "*");
        setData("shoppingCart", "");
        setData("useApi", "N");
      }
    }
    catch(Exception e)
    {
      logEntry("postHandleRequest()", e.toString());
    }
  }
}/*@lineinfo:generated-code*/