/*@lineinfo:filename=ApplyNow*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/Page1Base.sqlj $

  Description:

  Page1Base

  Base bean for Page 1 of virtual app (merchant-submission version)

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-04-18 16:44:44 -0700 (Fri, 18 Apr 2008) $
  Version            : $Revision: 14760 $

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.virtual.wpp;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.virtual.Page1Base;
import com.mes.constants.MesEmails;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class ApplyNow extends Page1Base
{
  {
    appType = 76;
    curScreenId = 1;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields( request );
                                        
    if( WPPEmail.ServerUrl == null || WPPEmail.ServerUrl.equals("") )
    {
      WPPEmail.ServerUrl = HttpHelper.getServerURL(request);
    }
  }
  
  protected boolean submitAppData()
  {
    long              appSeqNum = 0L;
    boolean           result    = super.submitAppData();
    ResultSetIterator it        = null;
    ResultSet         rs        = null;
    
    try
    {
      connect();
      
      appSeqNum = fields.getField("appSeqNum").asInteger();
      boolean sendEmail = false;
      
      String  toAddr  = "";
      String  ad      = "";
      
      /*@lineinfo:generated-code*//*@lineinfo:73^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  vac.contact_email       to_address,
//                  nvl(vac.email_sent,'N') email_sent,
//                  vac.app_descriptor
//          from    virtual_app_contact vac
//          where   vac.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vac.contact_email       to_address,\n                nvl(vac.email_sent,'N') email_sent,\n                vac.app_descriptor\n        from    virtual_app_contact vac\n        where   vac.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.virtual.wpp.ApplyNow",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.virtual.wpp.ApplyNow",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:80^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        sendEmail = (rs.getString("email_sent").equals("N"));
        toAddr    = rs.getString("to_address");
        ad        = rs.getString("app_descriptor");
      }
      
      rs.close();
      it.close();
      
      if( sendEmail )
      {
        WPPEmail.sendWPPEmail(appSeq.getControlNum(), MesEmails.MSG_ADDRS_WPP_CONTACT, toAddr);
        
        /*@lineinfo:generated-code*//*@lineinfo:98^9*/

//  ************************************************************
//  #sql [Ctx] { update  virtual_app_contact
//            set     email_sent = 'Y'
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  virtual_app_contact\n          set     email_sent = 'Y'\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.app.virtual.wpp.ApplyNow",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:103^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("submitAppData()", e.toString());
      result = false;
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( result );
  }
}/*@lineinfo:generated-code*/