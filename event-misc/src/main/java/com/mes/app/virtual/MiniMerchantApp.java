/*@lineinfo:filename=MiniMerchantApp*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/utils/AuthNetRouter.java $

  Description:
  
  PdfStatementServlet
  
  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-01-11 14:23:48 -0800 (Fri, 11 Jan 2008) $
  Version            : $Revision: 14474 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.virtual;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.AppSequence;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.SequenceScreen;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class MiniMerchantApp extends SQLJConnectionBase
{
  private String  LoginName = "";
  private int     AppType   = -1;
  private String  StartPage = "";
  
  public boolean initialized = false;
  
  public MiniMerchantApp(HttpServletRequest request)
  {
    ResultSetIterator   it = null;
    ResultSet           rs = null;
    try
    {
      connect();
      
      // new app submission (ad = app descriptor)
      String appDescriptor = HttpHelper.getString(request, "ad", "");
      
      if(appDescriptor.length() > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:61^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  vat.login_name  login_name,
//                    vat.app_type    app_type,
//                    vat.start_page  start_page
//            from    virtual_app_types vat
//            where   vat.app_descriptor = :appDescriptor
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vat.login_name  login_name,\n                  vat.app_type    app_type,\n                  vat.start_page  start_page\n          from    virtual_app_types vat\n          where   vat.app_descriptor =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.virtual.MiniMerchantApp",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,appDescriptor);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.virtual.MiniMerchantApp",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:68^9*/
      
        rs = it.getResultSet();
      
        if(rs.next())
        {
          LoginName   = rs.getString("login_name");
          AppType     = rs.getInt("app_type");
          StartPage   = rs.getString("start_page");
          initialized = true;
        }
        rs.close();
        it.close();
      }
      else
      {
        // revisit existing app (either from pre-vetting or just returning to complete)
        String  controlNumber = HttpHelper.getString(request, "controlNum", "");
        long    appSeqNum     = 0L;
        
        if( controlNumber.length() > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:90^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  vat.login_name      login_name,
//                      vat.app_type        app_type,
//                      vac.app_seq_num     app_seq_num,
//                      vat.start_page      start_page
//              from    merchant mr,
//                      virtual_app_contact vac,
//                      virtual_app_types vat
//              where   mr.merc_cntrl_number = :controlNumber
//                      and mr.app_seq_num = vac.app_seq_num 
//                      and vac.app_descriptor = vat.app_descriptor
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vat.login_name      login_name,\n                    vat.app_type        app_type,\n                    vac.app_seq_num     app_seq_num,\n                    vat.start_page      start_page\n            from    merchant mr,\n                    virtual_app_contact vac,\n                    virtual_app_types vat\n            where   mr.merc_cntrl_number =  :1 \n                    and mr.app_seq_num = vac.app_seq_num \n                    and vac.app_descriptor = vat.app_descriptor";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.virtual.MiniMerchantApp",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,controlNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.virtual.MiniMerchantApp",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:102^11*/
          
          rs = it.getResultSet();
          
          if(rs.next())
          {
            LoginName   = rs.getString("login_name");
            AppType     = rs.getInt("app_type");
            appSeqNum   = rs.getLong("app_seq_num");
            StartPage   = rs.getString("start_page");
            initialized = true;
          }
          rs.close();
          it.close();
          
          if( initialized )
          {
            // establish app sequence
            AppSequence appSeq = new AppSequence();
          
            appSeq.setAppType(AppType, true);
            appSeq.setCurScreenId(1);
            appSeq.setAppSeqNum(appSeqNum);
            
            SequenceScreen curScreen = appSeq.getFirstIncompleteScreen();
            
            if( curScreen == null)
            {
              // user must have completed app and so show last page
              curScreen = appSeq.getLastScreen();
            }
            
            if( curScreen == null)
            {
              // default to first page
              StartPage = StartPage = "?appSeqNum=" + appSeqNum;
            }
            else
            {
              StartPage = curScreen.getJsp() + "?appSeqNum=" + appSeqNum;
            }
          
            initialized = true;
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("MiniMerchantApp()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public String getLoginName()
  {
    return( LoginName );
  }
  public void setLoginName(String loginName)
  {
    LoginName = loginName;
  }
  
  public int getAppType()
  {
    return( AppType );
  }
  public void setAppType(int appType)
  {
    AppType = appType;
  }
  
  public String getStartPage()
  {
    return( StartPage );
  }
  public void setStartPage(String startPage)
  {
    StartPage = startPage;
  }
}/*@lineinfo:generated-code*/