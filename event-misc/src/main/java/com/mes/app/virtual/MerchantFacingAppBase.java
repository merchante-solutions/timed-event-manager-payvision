/*@lineinfo:filename=MerchantFacingAppBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/Page1Base.sqlj $

  Description:

  Page1Base

  Base bean for Page 1 of virtual app (merchant-submission version)

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-04-18 16:44:44 -0700 (Fri, 18 Apr 2008) $
  Version            : $Revision: 14760 $

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.virtual;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.app.AppBase;
import com.mes.forms.HiddenField;
import com.mes.forms.ImageButtonField;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class MerchantFacingAppBase extends AppBase
{
  static Logger log = Logger.getLogger(MerchantFacingAppBase.class);
  
  {
    useXSSFilter = true;
  }
  
  public boolean isCompleted()
  {
    boolean isComplete = false;
    
    try
    {
      isComplete = (HttpHelper.isProdServer(null) && 
                    appSeq.isAppComplete(fields.getField("appSeqNum").asLong()));
    }
    catch(Exception e)
    {
    }
    
    return( isComplete );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields( request );
    
    try
    {
      submitPartial = true;
      
      fields.add(new HiddenField("ad"));
      
      fields.add(new ImageButtonField("submit", "Continue", "/images/partner_referral/continuebutton.gif",
                                        24, 80, "Continue"));
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
    }
  }
  
  protected boolean submitAppData()
  {
    boolean submitOk = false;
    
    try
    {
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitAppData", e.toString());
    }
    finally
    {
    }
    
    return( submitOk );
  }
  
  protected boolean loadPageData(long appSeqNum)
  {
    return ( true );
  }
  
  protected boolean loadAppData()
  {
    boolean loadOk        = false;
    ResultSetIterator it  = null;
    
    try
    {
      connect();
      
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if( appSeqNum != 0 )
      {
        // load ad (app descriptor) to help determine app functionality
        /*@lineinfo:generated-code*//*@lineinfo:118^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  vac.app_descriptor  ad
//            from    virtual_app_contact vac
//            where   vac.app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vac.app_descriptor  ad\n          from    virtual_app_contact vac\n          where   vac.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.virtual.MerchantFacingAppBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.virtual.MerchantFacingAppBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:123^9*/
        
        setFields(it.getResultSet());
        it.close();
      
        loadOk = loadPageData(appSeqNum);
      }
    }
    catch(Exception e)
    {
      logEntry("loadAppData()", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( loadOk );
  }
}/*@lineinfo:generated-code*/