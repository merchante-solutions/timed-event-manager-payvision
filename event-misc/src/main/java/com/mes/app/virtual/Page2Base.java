/*@lineinfo:filename=Page2Base*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/Page1Base.sqlj $

  Description:

  Page1Base

  Base bean for Page 1 of virtual app (merchant-submission version)

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-04-18 16:44:44 -0700 (Fri, 18 Apr 2008) $
  Version            : $Revision: 14760 $

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.virtual;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

public class Page2Base extends MerchantFacingAppBase
{
  static Logger log = Logger.getLogger(Page2Base.class);
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields( request );
    
    try
    {
      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
    }
  }
  
  public String getStepContent()
  {
    // default to all-in-one app
    String stepContent = "include/all_in_one_next_steps.inc";
    
    try
    {
      if( getData("ad").equals("vt") )
      {
        stepContent = "include/vir_term_next_steps.inc";
      }
      else
      {
        stepContent = "include/all_in_one_next_steps.inc";
      }
    }
    catch(Exception e)
    {
      logEntry("getStepContent()", e.toString());
    }
    
    return( stepContent );
  }
}/*@lineinfo:generated-code*/