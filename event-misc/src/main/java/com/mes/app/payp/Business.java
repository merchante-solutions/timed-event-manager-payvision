/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: $

  Description:

  Business

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.payp;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.SicField;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Business extends com.mes.app.BusinessBase
{
  {
    appType = 76;
    curScreenId = 1;
  }

  protected class BusinessTypeTable extends DropDownTable
  {
    public BusinessTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Sole Proprietorship");
      addElement("2","Corporation");
      addElement("3","Partnership");
      addElement("4","Medical or Legal Corporation");
      addElement("6","Tax Exempt");
      addElement("7","Government");
      addElement("8","Limited Liability Company");
    }
  }

  protected class IndustryTypeTable extends DropDownTable
  {
    public IndustryTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail - 010450");
      addElement("2","Restaurant - 011100");
      addElement("10","Lodging - 011101");
      addElement("17","Supermarket - 010450");
      addElement("5","Internet - 010450");
      addElement("7","Direct Marketing - 010450");
      addElement("8","Other - 010450");
      addElement("6","Services - 010450");
      addElement("9","Cash Advance - 010450");
    }
  }

  protected class LocationTypeTable extends DropDownTable
  {
    public LocationTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail Storefront");
      addElement("2","Internet Storefront");
      addElement("3","Business Office");
      addElement("4","Private Residence");
      addElement("7","Separate Building");
      addElement("6","Bank");
      addElement("5","Other");
    }
  }

  protected class BranchTable extends DropDownTable
  {
    public BranchTable(long topHid)
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:107^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mod(th.descendent,1000000)  assoc_num,
//                    gn.group_name               branch_name
//            from    app_branch_dropdown abd,
//                    t_hierarchy th,
//                    group_names gn
//            where   abd.app_type = :appType and
//                    th.descendent = abd.hierarchy_node and
//                    th.relation = 1 and
//                    th.ancestor = gn.group_number
//            order by gn.group_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mod(th.descendent,1000000)  assoc_num,\n                  gn.group_name               branch_name\n          from    app_branch_dropdown abd,\n                  t_hierarchy th,\n                  group_names gn\n          where   abd.app_type =  :1  and\n                  th.descendent = abd.hierarchy_node and\n                  th.relation = 1 and\n                  th.ancestor = gn.group_number\n          order by gn.group_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.payp.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.payp.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:119^9*/

        rs = it.getResultSet();

        addElement("","select one");
        while (rs.next())
        {
          addElement(rs.getString("assoc_num"),rs.getString("branch_name"));
        }
      }
      catch (Exception e)
      {
        logEntry("BranchTable()",e.toString());
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      // make some fields required
      fields.getField("accountType").makeRequired();

      // add sic code as visible field
      fields.deleteField("sicCode");
      fields.add(new SicField("sicCode", "SIC Code", false));
      fields.getField("sicCode").makeOptional();

      fields.deleteField("assocNum");
      fields.add(new NumberField("assocNum", "Association #", 6, 6, true, 0));

      // add validation of the confirmation fields
      fields.getField("confirmCheckingAccount")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("checkingAccount"),"Checking Account #"));
      fields.getField("confirmTransitRouting")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("transitRouting"),"Transit Routing #"));

      // add referring branch assoc num dropdown
      //@ACT - top hierarchy node
      fields.add(new DropDownField("assocNum",new BranchTable(-1L), true));

      // override description of existingVerisign so that it is generic
      fields.add(new CheckboxField("existingVerisign",  "Merchant has an existing Internet account",false));

      // override location and industry type tables for CCB
      fields.add(new DropDownField    ("industryType",      "Industry",new IndustryTypeTable(),false));
      fields.add(new DropDownField    ("locationType",      "Type of Business Location",new LocationTypeTable(),false));

      fields.add(new Field("clientData1", 40, 20, true));
      fields.add(new HiddenField("merchantComments"));

      fields.getField("businessEmail").addValidation(new InternetEmailRequiredValidation(fields.getField("productType")));

      createPosPartnerExtendedFields();

      // make some fields not required
      fields.getField("sourceOfInfo").makeOptional();
      fields.getField("taxpayerId").makeOptional();
      fields.getField("yearsOpen").makeOptional();
      fields.getField("webUrl").makeOptional();
      
      fields.getField("accountType").makeOptional();
      fields.getField("businessType").makeOptional();
      fields.getField("industryType").makeOptional();
      fields.getField("locationType").makeOptional();
      fields.getField("locationYears").makeOptional();
      fields.getField("businessDesc").makeOptional();
      fields.getField("establishedDate").makeOptional();
      fields.getField("applicationType").makeOptional();
      fields.getField("numLocations").makeOptional();
      fields.getField("monthlySales").makeOptional();
      fields.getField("monthlyVMCSales").makeOptional();
      fields.getField("averageTicket").makeOptional();
      fields.getField("refundPolicy").makeOptional();
      fields.getField("motoPercentage").makeOptional();
      fields.getField("haveProcessed").makeOptional();
      fields.getField("statementsProvided").makeOptional();
      fields.getField("haveCanceled").makeOptional();
      fields.getField("owner1Name").makeOptional();
      fields.getField("owner1SSN").makeOptional();
      fields.getField("owner1Percent").makeOptional();
      fields.getField("owner1Address1").makeOptional();
      fields.getField("owner1Csz").makeOptional();
      fields.getField("bankName").makeOptional();
      fields.getField("yearsOpen").makeOptional();
      fields.getField("checkingAccount").makeOptional();
      fields.getField("confirmCheckingAccount").makeOptional();
      fields.getField("transitRouting").makeOptional();
      fields.getField("confirmTransitRouting").makeOptional();
      fields.getField("bankAddress").makeOptional();
      fields.getField("bankCsz").makeOptional();
      fields.getField("motoPercentage").removeAllValidation();
      fields.getField("productType").makeOptional();
      
      // reset all fields, including those just added to be of class form
      fields.setHtmlExtra("class=\"formText\"");

      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      logEntry("createFields()",e.toString());
    }
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);

    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }
  }

  protected void preHandleRequest(HttpServletRequest request)
  {
    super.preHandleRequest(request);
  }

  protected boolean loadAppData()
  {
    long                appSeqNum   = 0L;
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = super.loadAppData();

    try
    {
      appSeqNum = fields.getField("appSeqNum").asInteger();

      if(result && appSeqNum != 0)
      {
        connect();

        // load client data
        /*@lineinfo:generated-code*//*@lineinfo:268^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  client_data_1
//            from    merchant
//            where   app_seq_num = :fields.getField("appSeqNum").asInteger()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_3034 = fields.getField("appSeqNum").asInteger();
  try {
   String theSqlTS = "select  client_data_1\n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.payp.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_3034);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.payp.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:273^9*/

        rs = it.getResultSet();

        setFields(rs);
        
        rs.close();
        it.close();
        
        /*@lineinfo:generated-code*//*@lineinfo:282^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  solution_comments merchant_comments
//            from    virtual_app_contact
//            where   app_seq_num = :getData("appSeqNum")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3035 = getData("appSeqNum");
  try {
   String theSqlTS = "select  solution_comments merchant_comments\n          from    virtual_app_contact\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.payp.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3035);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.payp.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:287^9*/
        
        rs = it.getResultSet();
        setFields(rs);
        
        rs.close();
        it.close();

        result = true;
      }
    }
    catch(Exception e)
    {
      logEntry("loadAppData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return( result );
  }

  protected boolean submitAppData()
  {
    long    appSeqNum = 0L;
    boolean result    = super.submitAppData();

    try
    {
      connect();

      appSeqNum = fields.getField("appSeqNum").asInteger();

      if(result && appSeqNum != 0L)
      {
        // update merchant table with rep code data
        /*@lineinfo:generated-code*//*@lineinfo:326^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     client_data_1 = :getData("clientData1")
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3036 = getData("clientData1");
   String theSqlTS = "update  merchant\n          set     client_data_1 =  :1 \n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.payp.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3036);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:331^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("submitAppData()", e.toString());
      result = false;
    }
    finally
    {
      cleanUp();
    }

    return( result );
  }
}/*@lineinfo:generated-code*/