/*@lineinfo:filename=VirtualAppBusinessBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/VirtualAppBusinessBase.sqlj $

  Description:
  

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-01-09 15:58:49 -0800 (Fri, 09 Jan 2009) $
  Version            : $Revision: 15700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app;

import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.CheckboxField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import sqlj.runtime.ResultSetIterator;

public class VirtualAppBusinessBase extends BusinessBase
{
  // defaults for amex and discover rates -- must be changed in child class if necessary
  protected double AmexRate     = 3.2;
  protected double DiscoverRate = 2.44;
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    Field   tempField            = null;
    
    try
    {
      fields.getField(  "mailingName"      ).removeValidation("required");
      fields.getField(  "mailingAddress1"  ).removeValidation("required");
      fields.getField(  "mailingCsz"       ).removeValidation("required");
      
      // fix bank fields
      fields.getField("confirmCheckingAccount")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("checkingAccount"),"Checking Account #"));
      
      //fields.add(new Field("confirmTransitRouting",  "Confirm Transit Routing. #",9,15,false));
      fields.getField("confirmTransitRouting")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("transitRouting"),"Transit Routing #"));

      fields.add(new HiddenField("accountType"));
      fields.add(new HiddenField("applicationType"));
      fields.add(new HiddenField("numLocations"));
      fields.add(new HiddenField("locationType"));
      fields.add(new HiddenField("motoPercentage"));
      fields.add(new HiddenField("internetPercent"));
      fields.add(new HiddenField("cardPercent"));
      fields.add(new HiddenField("noCardPercent"));
      fields.add(new HiddenField("productType"));
      fields.add(new HiddenField("internetType"));
      fields.add(new HiddenField("orderId"));
      fields.add(new HiddenField("assoc"));
      
      setData("accountType", "N");        //set to new account for every account.. no conversions
      setData("applicationType", "1");    //set to single outlet
      setData("numLocations", "1");
      setData("locationType", "2");       //set to internet store front
      setData("motoPercentage", "100");   //set to internet store front
      setData("internetPercent", "100");  //set to internet store front
      setData("industryType", "5");       // set to internet industry type
      setData("cardPercent", "0");
      setData("noCardPercent", "100");
      
      // these should be overridden by the extending class
      setData("productType", Integer.toString(mesConstants.POS_INTERNET));
      setData("internetType", Integer.toString(mesConstants.APP_MPOS_AUTHORIZE_NET));
     
      fields.add(new Field("webUrl",75,30,false));
      
      // modify Amex and Discover fields so that a rate isn't required
      fields.add(new CheckboxField("amexNeeded", "I currently do not have an Amex SE merchant number and would like Merchant e-Solutions to apply for one on my behalf.  I understand that American Express will charge me a separate rate for American Express Card transactions.", false));
      fields.add(new CheckboxField("discoverNeeded", "I currently do not have a Discover merchant number and would like Merchant e-Solutions to apply for one on my behalf.  I understand that Discover will charge me a seprarate rate for Discover Card transactions.", false));
      
      CardAcceptedValidation amexVal = new CardAcceptedValidation();
      amexVal.addField(getField("amexAcctNum"));
      amexVal.addField(getField("amexNeeded"));
      getField("amexAccepted").addValidation(amexVal, "amexValidation");
      
      CardAcceptedValidation discVal = new CardAcceptedValidation();
      discVal.addField(getField("discoverAcctNum"));
      discVal.addField(getField("discoverNeeded"));
      getField("discoverAccepted").addValidation(discVal, "discoverValidation");
      
      fields.getField("businessEmail").makeRequired();
      fields.getField("internetEmail").makeOptional();
      
      fields.add(new Field("promotionCode", "Promotion Code", 50, 15, true));

      // reset all fields, including those just added to be of class form
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
  
  protected boolean loadAppData()
  {
    ResultSetIterator   it          = null;
    boolean             loadOk      = false;
    
    try
    {
      connect();
      
      loadOk = super.loadAppData();
      
      if ( loadOk )
      {
        loadOk = false;
        
        long appSeqNum = getLong("appSeqNum");
        /*@lineinfo:generated-code*//*@lineinfo:142^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mr.promotion_code     as promotion_code
//            from    merchant    mr
//            where   mr.app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mr.promotion_code     as promotion_code\n          from    merchant    mr\n          where   mr.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.VirtualAppBusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.VirtualAppBusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:147^9*/
        setFields(it.getResultSet());
        
        loadOk = true;
      }        
    }
    catch(Exception e)
    {
      logEntry("loadData()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      cleanUp();
    }
    return( loadOk );
  }

  protected boolean submitAppData()
  {
    boolean submitOk = false;

    try
    {
      connect();
      
      // make sure association and orderId (promotion code) are reflected
      if ( hasData("orderId") )
      {
        setData("promotionCode",getData("orderId"));
      }
      if ( hasData("assoc") )
      {
        setData("assocNum",getData("assoc"));
      }
      
      submitOk = super.submitAppData();
      
      // add promo code
      /*@lineinfo:generated-code*//*@lineinfo:186^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     promotion_code = :fields.getData("promotionCode"),
//                  client_data_1 = :fields.getData("promotionCode")
//          where   app_seq_num = :fields.getData("appSeqNum")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2377 = fields.getData("promotionCode");
 String __sJT_2378 = fields.getData("promotionCode");
 String __sJT_2379 = fields.getData("appSeqNum");
   String theSqlTS = "update  merchant\n        set     promotion_code =  :1 ,\n                client_data_1 =  :2 \n        where   app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.app.VirtualAppBusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2377);
   __sJT_st.setString(2,__sJT_2378);
   __sJT_st.setString(3,__sJT_2379);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:192^7*/
      
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitAppData(" + fields.getData("appSeqNum") + ")", e.toString());
      submitOk = false;
    }
    finally
    {
      cleanUp();
    }
  
    return submitOk;
  }
  


  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
    
    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }
    
    // set amex and discover rates
    setData("amexEsaRate", Double.toString(AmexRate));
    setData("discoverRapRate", Double.toString(DiscoverRate));
    
    // set promo code
    setData("promotionCode",  hasData("orderId") ? getData("orderId") : getData("promotionCode"));
  }
}/*@lineinfo:generated-code*/