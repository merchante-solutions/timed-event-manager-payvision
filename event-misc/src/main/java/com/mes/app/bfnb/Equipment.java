/**************************************************************************

  FILE: $URL: $

  Description:

  Equipment

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.bfnb;

import javax.servlet.http.HttpServletRequest;
import com.mes.app.EquipmentBase;

public class Equipment extends EquipmentBase
{
  {
    appType = 79;
    curScreenId = 2;
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      // reset terminal application field
      //fields.deleteField("terminalApp");
      //fields.add(new DropDownField("terminalApp",new TerminalApplicationTable(),true));

      // set trainingType field to be required
      fields.getField("trainingType").makeRequired();

      // default phone training to be MES
      fields.setData("trainingType", "M");

      // make comm type required
      fields.getField("commType").makeRequired();

      // reset all fields, including those just added to be of class formText
      fields.setHtmlExtra("class=\"formText\"");

      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
}
