/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/fmbank/Pricing.java $

  Description:

  Pricing

  Farmers & Merchants Bank online app pricing page bean.  

  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 8/26/04 3:14p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.fmbank;

import javax.servlet.http.HttpServletRequest;
import com.mes.app.PricingBase;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;

public class Pricing extends PricingBase
{
  {
    appType = 40;
    curScreenId = 3;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      connect();
      
      // initialize the wireless equipment object
      wirelessSet = new WirelessEquipmentSet(appSeqNum);
      add(wirelessSet);
      
      // v/mc pricing options
      fields.setData("pricingPlan","3");

      // setup bet options
      fields.setData("betType","38");
      
      Field betSet38 = new DropDownField("betSet_38",new BetSet(38),false);
      fields.add(betSet38);
      fields.add(new BetSetMapperField());
      betSet38.setOptionalCondition(
        new FieldValueCondition(fields.getField("betType"),"38"));
        
      // set the field style class
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      e.printStackTrace();
      logEntry("createFields()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
}
