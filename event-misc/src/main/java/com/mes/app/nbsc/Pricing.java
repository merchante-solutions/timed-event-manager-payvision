/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/fmbank/Pricing.java $

  Description:

  Pricing

  NBSC online app pricing page bean.

  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 8/26/04 3:14p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.nbsc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.DollarAmountTable;
import com.mes.app.PricingBase;
import com.mes.constants.mesConstants;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.Validation;

public class Pricing extends PricingBase
{
  {
    appType = 43;
    curScreenId = 3;
  }
  
  protected class CountFieldValidation implements Validation
  {
    private Field feeField;
    private String errorText;
    
    public CountFieldValidation(Field feeField)
    {
      this.feeField = feeField;
    }
    
    public boolean validate(String fdata)
    {
      int count = 0;
      double feeAmount = feeField.asDouble();
      boolean isValid = true;
      
      try
      {
        count = Integer.parseInt(fdata);
      }
      catch (Exception e) {}
      
      if (count == 0 && feeAmount != 0)
      {
        isValid = false;
        errorText = "Specify quantity for " + feeField.getLabel();
      }
      else if (count != 0 && feeAmount == 0)
      {
        isValid = false;
        errorText = "Clear quantity for " + feeField.getLabel();
      }
      
      return isValid;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      connect();
      
      // initialize the wireless equipment object
      wirelessSet = new WirelessEquipmentSet(appSeqNum);
      add(wirelessSet);
      
      // v/mc pricing options
      fields.setData("pricingPlan","3");

      // setup bet options
      fields.setData("betType","43");
      
      Field betSet43 = new DropDownField("betSet_43",new BetSet(43),false);
      fields.add(betSet43);
      fields.add(new BetSetMapperField());
      betSet43.setOptionalCondition(
        new FieldValueCondition(fields.getField("betType"),"43"));
        
      // "reprogramming" fees and quantities
      FieldGroup gOddFees = new FieldGroup  ("gOddFees");
      
      gOddFees.add(new DropDownField("reprogTerminalFee","Terminal Reprogramming Fee",new DollarAmountTable(false,false,"0,75"),true));
      gOddFees.add(new DropDownField("reprogPrinterFee","Printer Reprogramming Fee",new DollarAmountTable(false,false,"0,75"),true));
      gOddFees.add(new DropDownField("imprinterFee","Imprinter Fee",new DollarAmountTable(false,false,"0,40,50"),true));
      gOddFees.add(new Field("reprogTerminalCount",4,3,true));
      gOddFees.add(new Field("reprogPrinterCount",4,3,true));
      gOddFees.add(new Field("imprinterCount",4,3,true));
      
      gOddFees.getField("reprogTerminalCount").addValidation(
        new CountFieldValidation(gOddFees.getField("reprogTerminalFee")));
      gOddFees.getField("reprogPrinterCount").addValidation(
        new CountFieldValidation(gOddFees.getField("reprogPrinterFee")));
      gOddFees.getField("imprinterCount").addValidation(
        new CountFieldValidation(gOddFees.getField("imprinterFee")));
      
      fields.add(gOddFees);

      // set the field style class
      fields.setHtmlExtra("class=\"formText\"");

      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      e.printStackTrace();
      logEntry("createFields()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  protected void loadOddFees(long appSeqNum)
  {
    PreparedStatement ps  = null;
    ResultSet         rs  = null;

    try
    {
      String qs
        = " select  mc1.misc_chrg_amount          reprog_terminal_fee,    "
        + "         mc2.misc_chrg_amount          reprog_printer_fee,     "
        + "         mc3.misc_chrg_amount          imprinter_fee,          "
        + "         si.siteinsp_num_reprog_terms  reprog_terminal_count,  "
        + "         si.siteinsp_num_reprog_prints reprog_printer_count,   "
        + "         si.siteinsp_num_imprinters    imprinter_count         "
        + " from    siteinspection                si,                     "
        + "         miscchrg                      mc1,                    "
        + "         miscchrg                      mc2,                    "
        + "         miscchrg                      mc3                     "
        + " where   si.app_seq_num = ?                                    "
        + "         and si.app_seq_num = mc1.app_seq_num (+)              "
        + "         and si.app_seq_num = mc2.app_seq_num (+)              "
        + "         and si.app_seq_num = mc3.app_seq_num (+)              "
        + "         and mc1.misc_code = ?                                 "
        + "         and mc2.misc_code = ?                                 "
        + "         and mc3.misc_code = ?                                 ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_MISC_CHARGE_TERM_REPROG_FEE);
      ps.setInt(3,mesConstants.APP_MISC_CHARGE_PRINTER_REPROG_FEE);
      ps.setInt(4,mesConstants.APP_MISC_CHARGE_IMPRINTER);

      rs = ps.executeQuery();
      if (rs.next())
      {
        setData("reprogTerminalFee",Double.toString(rs.getDouble("reprog_terminal_fee")));
        setData("reprogPrinterFee",Double.toString(rs.getDouble("reprog_printer_fee")));
        setData("imprinterFee",Double.toString(rs.getDouble("imprinter_fee")));
        setData("reprogTerminalCount",rs.getString("reprog_terminal_count"));
        setData("reprogPrinterCount",rs.getString("reprog_printer_count"));
        setData("imprinterCount",rs.getString("imprinter_count"));
      }
    }
    catch(Exception e)
    {
      String sig = this.getClass().getName() + "::loadOddFees()";
      e.printStackTrace();
      logEntry(sig,e.toString());
      addError(sig + ": " + e);
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ ps.close(); } catch(Exception e) {}
    }
  }

  protected boolean loadAppData()
  {
    boolean loadOk = false;

    try
    {
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        connect();

        loadOddFees(appSeqNum);
      }
      loadOk = super.loadAppData();
    }
    catch(Exception e)
    {
      logEntry("loadAppData()",e.toString());
      System.out.println(this.getClass().getName() + "::loadAppData(): "
        + e.toString());
    }
    finally
    {
      cleanUp();
    }

    return loadOk;
  }

  protected void submitOddFees(long appSeqNum)
  {
    PreparedStatement ps = null;

    try
    {
      // clear old odd fees
      String qs
        = " delete                                "
        + " from    miscchrg                      "
        + " where   app_seq_num = ?               "
        + "         and misc_code in ( ?, ?, ? )  ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_MISC_CHARGE_TERM_REPROG_FEE);
      ps.setInt(3,mesConstants.APP_MISC_CHARGE_PRINTER_REPROG_FEE);
      ps.setInt(4,mesConstants.APP_MISC_CHARGE_IMPRINTER);

      ps.execute();
      ps.close();

      // insert query
      qs
        = " insert into miscchrg  "
        + " ( app_seq_num,        "
        + "   misc_code,          "
        + "   misc_chrg_amount )  "
        + " values                "
        + " ( ?, ?, ? )           ";

      // terminal reprogramming
      ps = con.prepareStatement(qs);
      ps.setLong  (1,appSeqNum);
      ps.setInt   (2,mesConstants.APP_MISC_CHARGE_TERM_REPROG_FEE);
      ps.setDouble(3,getField("reprogTerminalFee").asDouble());

      ps.execute();
      ps.close();

      // printer reprogramming
      ps = con.prepareStatement(qs);
      ps.setLong  (1,appSeqNum);
      ps.setInt   (2,mesConstants.APP_MISC_CHARGE_PRINTER_REPROG_FEE);
      ps.setDouble(3,getField("reprogPrinterFee").asDouble());

      ps.execute();
      ps.close();

      // imprinter
      ps = con.prepareStatement(qs);
      ps.setLong  (1,appSeqNum);
      ps.setInt   (2,mesConstants.APP_MISC_CHARGE_IMPRINTER);
      ps.setDouble(3,getField("imprinterFee").asDouble());

      ps.execute();
    }
    catch (Exception e)
    {
      String sig = this.getClass().getName() + "::submitOddFees()";
      e.printStackTrace();
      logEntry(sig,e.toString());
      addError(sig + ": " + e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) {}
    }
  }

  protected void submitSiteInspection(long appSeqNum)
  {
    PreparedStatement ps = null;

    try
    {
      // clear old site inspection
      String qs
        = " delete                  "
        + " from    siteinspection  "
        + " where   app_seq_num = ? ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      ps.execute();
      ps.close();

      // insert query
      qs
        = " insert into siteinspection                          "
        + " ( app_seq_num,                                      "
        + "   siteinsp_comment,                                 "
        + "   siteinsp_name_flag,                               "
        + "   siteinsp_inv_sign_flag,                           "
        + "   siteinsp_bus_hours_flag,                          "
        + "   siteinsp_inv_viewed_flag,                         "
        + "   siteinsp_inv_consistant_flag,                     "
        + "   siteinsp_vol_flag,                                "
        + "   siteinsp_full_flag,                               "
        + "   siteinsp_soft_flag,                               "
        + "   siteinsp_inv_street,                              "
        + "   siteinsp_inv_city,                                "
        + "   siteinsp_inv_state,                               "
        + "   siteinsp_inv_zip,                                 "
        + "   siteinsp_full_street,                             "
        + "   siteinsp_full_city,                               "
        + "   siteinsp_full_state,                              "
        + "   siteinsp_full_zip,                                "
        + "   siteinsp_bus_street,                              "
        + "   siteinsp_bus_city,                                "
        + "   siteinsp_bus_state,                               "
        + "   siteinsp_bus_zip,                                 "
        + "   siteinsp_inv_value,                               "
        + "   siteinsp_full_name,                               "
        + "   siteinsp_no_of_emp,                               "
        + "   siteinsp_bus_loc,                                 "
        + "   siteinsp_bus_loc_comment,                         "
        + "   siteinsp_bus_address,                             "
        + "   siteinsp_soft_name,                               "
        + "   siteinsp_num_reprog_terms,                        "
        + "   siteinsp_num_reprog_prints,                       "
        + "   siteinsp_num_imprinters )                         "
        + " values                                              "
        + " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,   "
        + "   ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )  ";

      ps = con.prepareStatement(qs);
      ps.setLong  (1, appSeqNum);
      ps.setString(2, fields.getData("pricingComments"));
      ps.setString(3, fields.getData("qNameMatch").toUpperCase());
      ps.setString(4, fields.getData("qSignage").toUpperCase());
      ps.setString(5, fields.getData("qHoursPosted").toUpperCase());
      ps.setString(6, fields.getData("qInventoryReview").toUpperCase());
      ps.setString(7, fields.getData("qInventoryConsistent").toUpperCase());
      ps.setString(8, fields.getData("qInventoryAdequate").toUpperCase());
      ps.setString(9, fields.getData("fulfillmentHouse").toUpperCase());
      ps.setString(10,fields.getData("securitySoftware").toUpperCase());
      ps.setString(11,fields.getData("inventoryAddr"));
      ps.setString(12,fields.getData("inventoryCszCity"));
      ps.setString(13,fields.getData("inventoryCszState"));
      ps.setString(14,fields.getData("inventoryCszZip"));
      ps.setString(15,fields.getData("fulfillmentAddr"));
      ps.setString(16,fields.getData("fulfillmentCszCity"));
      ps.setString(17,fields.getData("fulfillmentCszState"));
      ps.setString(18,fields.getData("fulfillmentCszZip"));
      ps.setString(19,fields.getData("locationAddr"));
      ps.setString(20,fields.getData("locationCszCity"));
      ps.setString(21,fields.getData("locationCszState"));
      ps.setString(22,fields.getData("locationCszZip"));
      ps.setString(23,fields.getData("inventoryValue"));
      ps.setString(24,fields.getData("fulfillmentName"));
      ps.setString(25,fields.getData("employeeCount"));
      ps.setString(26,fields.getData("locationType"));
      ps.setString(27,fields.getData("locationDesc"));
      ps.setString(28,fields.getData("locationAddrType"));
      ps.setString(29,fields.getData("securitySoftwareVendor"));
      ps.setString(30,fields.getData("reprogTerminalCount"));
      ps.setString(31,fields.getData("reprogPrinterCount"));
      ps.setString(32,fields.getData("imprinterCount"));
        
      ps.execute();
    }
    catch (Exception e)
    {
      String sig = this.getClass().getName() + "::submitSiteInspection()";
      e.printStackTrace();
      logEntry(sig,e.toString());
      addError(sig + ": " + e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) {}
    }
  }

  protected boolean submitAppData()
  {
    boolean submitOk = false;

    try
    {
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        connect();
      
        submitOddFees(appSeqNum);
      }
      submitOk = super.submitAppData();
    }
    catch(Exception e)
    {
      logEntry("submitAppData()",e.toString());
      System.out.println(this.getClass().getName() + "::submitAppData(): "
        + e.toString());
    }
    finally
    {
      cleanUp();
    }

    return submitOk;
  }
}
