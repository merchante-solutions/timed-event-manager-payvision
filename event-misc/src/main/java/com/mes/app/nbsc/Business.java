/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/ffb/Business.sqlj $

  Description:
  
  Business
  
  1st Financial Bank online application merchant information page bean.

  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/15/04 4:19p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.nbsc;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import sqlj.runtime.ResultSetIterator;

public class Business extends com.mes.app.BusinessBase
{
  {
    appType = 43;
    curScreenId = 1;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // make some fields required
      fields.getField("businessLegalName").makeRequired();
      fields.getField("taxpayerId").makeRequired();
      fields.getField("yearsOpen").makeRequired();
      
      // make some fields not required
      fields.getField("sourceOfInfo").makeOptional();
      
      // add/replace some nbsc special fields
      fields.add(new Field      ("assocNum",    "Association No.",6,6,false));
      fields.add(new Field      ("repCode",     "Rep Code",4,6,false));

      // these no longer appear on the app due to new cb&t review queues
      //fields.add(new NumberField("sicCode",     "SIC",4,6,true,0));
      //fields.add(new NumberField("creditScore", "Credit Score",3,6,false,0));
      fields.add(new HiddenField("creditScore"));
      
      // default transit routing number to NBSC's transit routing ???
      //fields.getField("transitRouting").setData("325171740");
      //fields.getField("confirmTransitRouting").setData("325171740");
      
      // make bank account and transit confirmation fields optional (not used)
      fields.getField("confirmCheckingAccount").makeOptional();
      fields.getField("confirmTransitRouting").makeOptional();

      // reset all fields, including those just added to be of class form
      fields.setHtmlExtra("class=\"formText\"");
      
      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);

      createPosPartnerExtendedFields();
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
    
    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }
  }
  
  protected boolean loadAppData()
  {
    long                appSeqNum   = 0L;
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = super.loadAppData();
    
    try
    {
      appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if(result && appSeqNum != 0)
      {
        connect();
      
        // load client data
        /*@lineinfo:generated-code*//*@lineinfo:122^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sic_code          sic_code,
//                    asso_number       assoc_num,
//                    merch_rep_code    rep_code,
//                    cbt_credit_score  credit_score
//            from    merchant
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sic_code          sic_code,\n                  asso_number       assoc_num,\n                  merch_rep_code    rep_code,\n                  cbt_credit_score  credit_score\n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.nbsc.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.nbsc.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:130^9*/
        rs = it.getResultSet();
        setFields(rs);

        result = true;
      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::loadAppData(): " + e.toString());
      logEntry("loadAppData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  protected boolean submitAppData()
  {
    boolean submitOk = false;
    try
    {
      connect();
      
      submitOk = super.submitAppData();
      
      if(submitOk)
      {
        long appSeqNum = fields.getField("appSeqNum").asInteger();
      
        if(appSeqNum != 0L)
        {
          // store the nbsc special fields, sic code looks to 
          // be stored in multiple places
          /*@lineinfo:generated-code*//*@lineinfo:169^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//              set     sic_code          = :fields.getData("sicCode"),
//                      app_sic_code      = :fields.getData("sicCode"),
//                      merch_mcc         = :fields.getData("sicCode"),
//                      merch_rep_code    = :fields.getData("repCode"),
//                      asso_number       = :fields.getData("assocNum"),
//                      cbt_credit_score  = :fields.getData("creditScore")
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3026 = fields.getData("sicCode");
 String __sJT_3027 = fields.getData("sicCode");
 String __sJT_3028 = fields.getData("sicCode");
 String __sJT_3029 = fields.getData("repCode");
 String __sJT_3030 = fields.getData("assocNum");
 String __sJT_3031 = fields.getData("creditScore");
   String theSqlTS = "update  merchant\n            set     sic_code          =  :1 ,\n                    app_sic_code      =  :2 ,\n                    merch_mcc         =  :3 ,\n                    merch_rep_code    =  :4 ,\n                    asso_number       =  :5 ,\n                    cbt_credit_score  =  :6 \n            where   app_seq_num =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.app.nbsc.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3026);
   __sJT_st.setString(2,__sJT_3027);
   __sJT_st.setString(3,__sJT_3028);
   __sJT_st.setString(4,__sJT_3029);
   __sJT_st.setString(5,__sJT_3030);
   __sJT_st.setString(6,__sJT_3031);
   __sJT_st.setLong(7,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:179^11*/
        }
      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::submitAppData(): " + e.toString());
      logEntry("submitAppDatq()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return submitOk;
  }
}/*@lineinfo:generated-code*/