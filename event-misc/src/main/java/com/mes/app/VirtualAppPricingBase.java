/*@lineinfo:filename=VirtualAppPricingBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/VirtualAppPricingBase.sqlj $

  Description:

  Pricing

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/07/04 3:36p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.forms.FieldBean;
import com.mes.forms.NumberField;
import sqlj.runtime.ResultSetIterator;

public class VirtualAppPricingBase extends FieldBean
{
  protected long  appSeqNum = 0L;
  protected int   appType   = 0;
  
  public VirtualAppPricingBase(long appSeqNum, int appType)
  {
    this.appSeqNum = appSeqNum;
    this.appType = appType;
  }
  
  public boolean submitAppData()
  {
    boolean           submitOk  = false;
    ResultSetIterator it        = null;
    ResultSet         rs        = null;
    
    try
    {
      connect();
      
      // create fields
      createPricingFields();
      
      // retrieve channel pricing to set fields
      /*@lineinfo:generated-code*//*@lineinfo:59^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    vapp_channel_pricing
//          where   app_type = :appType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    vapp_channel_pricing\n        where   app_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.VirtualAppPricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.VirtualAppPricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:64^7*/
      
      rs = it.getResultSet();
      setFields(rs);
      
      // don't try to load data if no appSeqNum is set
      if (appSeqNum != 0L)
      {
        // retrieve data from channel pricing
        storeTransactionFees();
        storeMiscFees();
      }
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitAppData()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return submitOk;
  }
  
  protected void createPricingFields()
  {
    try
    {
      fields.add(new NumberField("minimumDiscount", 10, 10, true, 2));
      fields.add(new NumberField("monthlyServiceFee", 10, 10, true, 2));
      fields.add(new NumberField("chargebackFee", 10, 10, true, 2));
      fields.add(new NumberField("pricingType", 10, 10, true, 0));
      fields.add(new NumberField("pricingPlan", 10, 10, true, 0));
      fields.add(new NumberField("discountRate", 10, 10, true, 2));
      fields.add(new NumberField("perItem", 10, 10, true, 2));
      fields.add(new NumberField("nonBankAuthFee", 10, 10, true, 2));
      fields.add(new NumberField("betType", 10, 10, true, 0));
      fields.add(new NumberField("comboId", 10, 10, true, 0));
      fields.add(new NumberField("setupFee", 10, 10, true, 0));
    }
    catch(Exception e)
    {
      logEntry("createPricingFields()", e.toString());
    }
  }
  
  protected void storeTransactionFees()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:115^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     pricing_grid  = :fields.getData("pricingType"),
//                  bet_type_code = :fields.getData("pricingType")
//          where   app_seq_num   = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2380 = fields.getData("pricingType");
 String __sJT_2381 = fields.getData("pricingType");
   String theSqlTS = "update  merchant\n        set     pricing_grid  =  :1 ,\n                bet_type_code =  :2 \n        where   app_seq_num   =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.app.VirtualAppPricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2380);
   __sJT_st.setString(2,__sJT_2381);
   __sJT_st.setLong(3,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:121^7*/

      // clear tranchrg records
      /*@lineinfo:generated-code*//*@lineinfo:124^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from  tranchrg
//          where app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from  tranchrg\n        where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.VirtualAppPricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:129^7*/

      // store v/mc pricing
      /*@lineinfo:generated-code*//*@lineinfo:132^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_per_auth,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran,
//            tranchrg_interchangefee_type,
//            tranchrg_interchangefee_fee
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.APP_CT_VISA,
//            :fields.getData("pricingPlan"),
//            :fields.getData("discountRate"),
//            :fields.getData("perItem"),
//            :fields.getData("minimumDiscount"),
//            0,
//            :fields.getData("betType"),
//            :fields.getData("comboId")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2382 = fields.getData("pricingPlan");
 String __sJT_2383 = fields.getData("discountRate");
 String __sJT_2384 = fields.getData("perItem");
 String __sJT_2385 = fields.getData("minimumDiscount");
 String __sJT_2386 = fields.getData("betType");
 String __sJT_2387 = fields.getData("comboId");
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_per_auth,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran,\n          tranchrg_interchangefee_type,\n          tranchrg_interchangefee_fee\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0,\n           :7 ,\n           :8 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.VirtualAppPricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   __sJT_st.setString(3,__sJT_2382);
   __sJT_st.setString(4,__sJT_2383);
   __sJT_st.setString(5,__sJT_2384);
   __sJT_st.setString(6,__sJT_2385);
   __sJT_st.setString(7,__sJT_2386);
   __sJT_st.setString(8,__sJT_2387);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:158^7*/

      /*@lineinfo:generated-code*//*@lineinfo:160^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_per_auth,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran,
//            tranchrg_interchangefee_type,
//            tranchrg_interchangefee_fee
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.APP_CT_MC,
//            :fields.getData("pricingPlan"),
//            :fields.getData("discountRate"),
//            :fields.getData("perItem"),
//            :fields.getData("minimumDiscount"),
//            0,
//            :fields.getData("betType"),
//            :fields.getData("comboId")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2388 = fields.getData("pricingPlan");
 String __sJT_2389 = fields.getData("discountRate");
 String __sJT_2390 = fields.getData("perItem");
 String __sJT_2391 = fields.getData("minimumDiscount");
 String __sJT_2392 = fields.getData("betType");
 String __sJT_2393 = fields.getData("comboId");
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_per_auth,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran,\n          tranchrg_interchangefee_type,\n          tranchrg_interchangefee_fee\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0,\n           :7 ,\n           :8 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.VirtualAppPricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_MC);
   __sJT_st.setString(3,__sJT_2388);
   __sJT_st.setString(4,__sJT_2389);
   __sJT_st.setString(5,__sJT_2390);
   __sJT_st.setString(6,__sJT_2391);
   __sJT_st.setString(7,__sJT_2392);
   __sJT_st.setString(8,__sJT_2393);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:186^7*/
      
      ResultSetIterator it = null;
      ResultSet         rs = null;
       
      /*@lineinfo:generated-code*//*@lineinfo:191^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    *
//          from      merchpayoption
//          where     app_seq_num = :appSeqNum and
//                    cardtype_code not in (1,4)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    *\n        from      merchpayoption\n        where     app_seq_num =  :1  and\n                  cardtype_code not in (1,4)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.VirtualAppPricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.app.VirtualAppPricingBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^7*/
      
      rs = it.getResultSet();

      while(rs.next())
      {
        /*@lineinfo:generated-code*//*@lineinfo:203^9*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//            ( app_seq_num,
//              cardtype_code,
//              tranchrg_per_tran 
//            )
//            values
//            ( :appSeqNum,
//              :rs.getInt("cardtype_code"),
//              :fields.getData("nonBankAuthFee")
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2394 = rs.getInt("cardtype_code");
 String __sJT_2395 = fields.getData("nonBankAuthFee");
   String theSqlTS = "insert into tranchrg\n          ( app_seq_num,\n            cardtype_code,\n            tranchrg_per_tran \n          )\n          values\n          (  :1 ,\n             :2 ,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.app.VirtualAppPricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,__sJT_2394);
   __sJT_st.setString(3,__sJT_2395);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:215^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("storeTransactionFees()", e.toString());
    }
  }
  
  /*
  ** private void storeMiscFees()
  **
  ** Stores all miscellaneous fees.
  */
  protected void storeMiscFees()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:233^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from   miscchrg
//          where  app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from   miscchrg\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.VirtualAppPricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:238^7*/

      // monthly service fee
      /*@lineinfo:generated-code*//*@lineinfo:241^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          ( 
//            app_seq_num,
//            misc_code,
//            misc_chrg_amount 
//          )
//          values
//          ( 
//            :appSeqNum,
//            :mesConstants.APP_MISC_CHARGE_MONTHLY_SERVICE_FEE,
//            :fields.getData("monthlyServiceFee")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2396 = fields.getData("monthlyServiceFee");
   String theSqlTS = "insert into miscchrg\n        ( \n          app_seq_num,\n          misc_code,\n          misc_chrg_amount \n        )\n        values\n        ( \n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.app.VirtualAppPricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_MONTHLY_SERVICE_FEE);
   __sJT_st.setString(3,__sJT_2396);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:255^7*/
   

      // chargeback fee
      /*@lineinfo:generated-code*//*@lineinfo:259^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          ( 
//            app_seq_num,
//            misc_code,
//            misc_chrg_amount 
//          )
//          values
//          ( 
//            :appSeqNum,
//            :mesConstants.APP_MISC_CHARGE_CHARGEBACK,
//            :fields.getData("chargebackFee")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2397 = fields.getData("chargebackFee");
   String theSqlTS = "insert into miscchrg\n        ( \n          app_seq_num,\n          misc_code,\n          misc_chrg_amount \n        )\n        values\n        ( \n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.app.VirtualAppPricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_CHARGEBACK);
   __sJT_st.setString(3,__sJT_2397);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:273^7*/
      
      // setup fee
      /*@lineinfo:generated-code*//*@lineinfo:276^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          (
//            app_seq_num,
//            misc_code,
//            misc_chrg_amount
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.APP_MISC_CHARGE_GATEWAY_SETUP,
//            :fields.getData("setupFee")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2398 = fields.getData("setupFee");
   String theSqlTS = "insert into miscchrg\n        (\n          app_seq_num,\n          misc_code,\n          misc_chrg_amount\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.app.VirtualAppPricingBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_GATEWAY_SETUP);
   __sJT_st.setString(3,__sJT_2398);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:290^7*/
    }
    catch(Exception e)
    {
      logEntry("storeMiscFees()",e.toString());
    }
  }
}/*@lineinfo:generated-code*/