/**************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/mtwest/Equipment.java $

  Description:
  
  Equipment
  
  Mountain West Bank online application equipment page bean.


  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 1/13/04 4:04p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.mtwest;

import com.mes.app.EquipmentDataBean;
import com.mes.forms.Field;

public class Equipment extends EquipmentDataBean
{
  public Equipment()
  {
  }
  
  public void init()
  {
    Field field = null;
    
    super.init();
    
    try
    {
      // default phone training to mes help desk
      fields.setData("trainingType","M");
      
      // make comm type required
      fields.getField("commType").makeRequired();
    }
    catch( Exception e )
    {
      System.out.println("init(): " + e.toString());
      logEntry("init()",e.toString());
    }
  }
}