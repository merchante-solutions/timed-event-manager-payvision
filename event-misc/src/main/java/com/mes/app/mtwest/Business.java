/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/mtwest/Business.sqlj $

  Description:
  
  Business
  
  Mountain West online application merchant information page bean.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 3/23/04 8:19a $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.mtwest;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.MerchInfoDataBean;
import com.mes.forms.Condition;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Business extends MerchInfoDataBean
{
  /*
  ** public static class BranchTable extends DropDownTable
  **
  ** List of River City referring branches with association derived from each
  ** branch's hierarchy node id.
  */
  public static class BranchTable extends DropDownTable
  {
    /*
    ** public BranchTable(long topHid)
    **
    ** Creates a drop down table populated by a bank's branch's non-chain
    ** associations.  The name used is from the level above the asscociation
    ** level, which should be the general branch name.  This assumes that
    ** the topHid given is four levels above the association level.
    */
    public BranchTable(long topHid)
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:70^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    mod(t1.descendent,1000000)  assoc_num,
//                      o2.org_name                 branch_name
//            from      t_hierarchy t1,
//                      t_hierarchy t2,
//                      organization o1,
//                      organization o2
//            where     t1.ancestor = :topHid
//                      and t1.relation = 4
//                      and t1.descendent = o1.org_group
//                      and t2.descendent = t1.descendent
//                      and t2.relation = 1
//                      and t2.ancestor = o2.org_group
//                      and lower(o1.org_name) like '%non chain%'  
//            order by  o2.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mod(t1.descendent,1000000)  assoc_num,\n                    o2.org_name                 branch_name\n          from      t_hierarchy t1,\n                    t_hierarchy t2,\n                    organization o1,\n                    organization o2\n          where     t1.ancestor =  :1 \n                    and t1.relation = 4\n                    and t1.descendent = o1.org_group\n                    and t2.descendent = t1.descendent\n                    and t2.relation = 1\n                    and t2.ancestor = o2.org_group\n                    and lower(o1.org_name) like '%non chain%'  \n          order by  o2.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.mtwest.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,topHid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.mtwest.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:86^9*/
        rs = it.getResultSet();
        
        addElement("","select one");
        while (rs.next())
        {
          addElement(rs.getString("assoc_num"),rs.getString("branch_name"));
        }
      }
      catch (Exception e)
      {
        logEntry("BranchTable()",e.toString());
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  
  public static class RefundPolicyTable extends DropDownTable
  {
    public RefundPolicyTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Refund in 30 Days or Less");
      addElement("2","No Refunds or Exchanges");
      addElement("3","Exchanges Only");
      addElement("6","Cash Back");
      addElement("7","Full Refund");
      addElement("4","Not Applicable");
    }
  }

  public class FieldNotBlankCondition implements Condition
  {
    private Field field;
    
    public FieldNotBlankCondition(Field field)
    {
      this.field = field;
    }
    
    public boolean isMet()
    {
      return !field.isBlank();
    }
  }
  
  public void init()
  {
    super.init();
    
    try
    {
      // Mountain West deltas (uses rivercity as baseline)

      // replaces base class field with drop down of mountain west branch assocs
      fields.add(new DropDownField("assocNum",new BranchTable(3941400055L),false));
      
      // remove required from legal name and contact last name
      fields.getField("businessLegalName").removeValidation("required");
      fields.getField("contactNameLast").removeValidation("required");
      
      // turn of required validation on merch history fields
      fields.getField("haveProcessed"     ).removeValidation("required");
      fields.getField("statementsProvided").removeValidation("required");
      fields.getField("haveCanceled"      ).removeValidation("required");

      // turn off required validation on acct info source, years open
      // and referring bank (won't be shown)
      fields.getField("sourceOfInfo"      ).removeValidation("required");
      fields.getField("referringBank"     ).removeValidation("required");
      fields.getField("yearsOpen"         ).removeValidation("required");

      // default fill bank address to mountain west bank
      fields.setData("bankName",    "Mountain West Bank");
      fields.setData("bankAddress", "125 Ironwood Dr.");
      fields.setData("bankCity",    "Coeur d'Alene");
      fields.setData("bankState",   "ID");
      fields.setData("bankZip",     "83814");

      // replace base refund policy with mountain west list
      fields.add(new DropDownField("refundPolicy",new RefundPolicyTable(),false));

      // set location number to 1 by default
      fields.setData("numLocations","1");

      // set default transit routing num
      fields.setData("transitRouting",        "123171955");
      fields.setData("confirmTransitRouting", "123171955");
      
      // create the extended POS Partner 2000 fields
      createPosPartnerExtendedFields();
      
      // reset all fields, including those just added to be of class formFields
      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch( Exception e )
    {
      logEntry("init()",e.toString());
    }
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);

    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }
  }
}/*@lineinfo:generated-code*/