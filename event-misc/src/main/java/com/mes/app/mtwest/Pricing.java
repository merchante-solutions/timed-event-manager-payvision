/*@lineinfo:filename=Pricing*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/mtwest/Pricing.sqlj $

  Description:

  Pricing

  Mountain West Bank online app pricing page bean.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-02-03 10:39:36 -0800 (Tue, 03 Feb 2009) $
  Version            : $Revision: 15771 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.mtwest;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Vector;
import com.mes.app.AppDataBeanBase;
import com.mes.constants.mesConstants;
import com.mes.forms.CurrencyField;
import com.mes.forms.DiscountField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.SmallCurrencyField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.forms.ZipField;
import com.mes.support.MesMath;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Pricing extends AppDataBeanBase
{
  //**************************************************************************
  // Drop Down Tables
  //**************************************************************************

  protected class DollarAmountTable extends DropDownTable
  {
    public DollarAmountTable(boolean hasNonselected, boolean hasNoFee, double[] amounts)
    {
      if (hasNonselected)
      {
        addElement("","select one");
      }
      if (hasNoFee)
      {
        addElement("","No Fee");
      }
      for (int i = 0; i < amounts.length; ++i)
      {
        String value = Double.toString(amounts[i]);
        String descriptor = MesMath.toCurrency(amounts[i]);
        if (amounts[i] == 0)
        {
          descriptor += "&nbsp;&nbsp;&nbsp;";
        }
        addElement(value,descriptor);
      }
    }
    
    public DollarAmountTable(double[] amounts)
    {
      this(false,false,amounts);
    }
  }

  protected class PricingTypeTable extends DropDownTable
  {
    public PricingTypeTable()
    {
      addElement("","select one");
      addElement("1","Retail - pass thru interchange");
      addElement("2","MOTO - pass thru interchange");
    }
  }
  
  protected class LocationAddressTable extends DropDownTable
  {
    public LocationAddressTable( )
    {
      //@ should be fixed to use codes instead of text
      addElement("","select one");
      addElement("Business Address on Application", "Business Address on Application" );
      addElement("Mailing Address on Application", "Mailing Address on Application" );
      addElement("Other", "Other" );
    }
  }

  protected class LocationTypeTable extends DropDownTable
  {
    public LocationTypeTable( )
    {
      //@ should be fixed to use codes instead of text
      addElement("",                  "select one");
      addElement("Retail Storefront", "Retail Storefront" );
      addElement("Office Building",   "Office Building" );
      addElement("Industrial Park",   "Industrial Park" );
      addElement("Strip Mall",        "Strip Mall" );
      addElement("Residence",         "Residence" );
      addElement("Other",             "Other" );
    }
  }
  
  //**************************************************************************
  // Validations
  //**************************************************************************
  
  protected class PricingPlanValidation implements Validation
  {
    private String errorText;

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String fdata)
    {
      String rateItemErrorText 
        = "Discount rate and per item amount required when selected";

      try
      {
        switch(Integer.parseInt(fdata))
        {
          case mesConstants.APP_PS_FIXED_PLUS_PER_ITEM:
            if (fields.getField("discRate1").isBlank() ||
                fields.getField("perItem1").isBlank())
            {
              errorText = rateItemErrorText;
            }
            break;

          case mesConstants.APP_PS_INTERCHANGE:
            if (fields.getField("discRate2").isBlank() ||
                fields.getField("perItem2").isBlank())
            {
              errorText = rateItemErrorText;
            }
            break;
            
          case mesConstants.APP_PS_MTWEST_4090_5090:
            if (fields.getField("discRate3").isBlank() ||
                fields.getField("perItem3").isBlank())
            {
              errorText = rateItemErrorText;
            }
            break;
            
          case mesConstants.APP_PS_MTWEST_4178_5178:
            if(fields.getField("discRate4").isBlank() ||
                fields.getField("perItem4").isBlank())
            {
              errorText = rateItemErrorText;
            }
            break;
            
          case mesConstants.APP_PS_MTWEST_4179_5179:
            if(fields.getField("discRate5").isBlank() ||
                fields.getField("perItem5").isBlank())
            {
              errorText = rateItemErrorText;
            }
            break;
            
          case mesConstants.APP_PS_MTWEST_4180_5180:
            if(fields.getField("discRate6").isBlank() ||
                fields.getField("perItem6").isBlank())
            {
              errorText = rateItemErrorText;
            }
            break;
        }
      }
      catch( Exception e )
      {
        errorText = "Please select a valid pricing plan";
      }
      return (errorText == null);
    }
  }

  protected class FulfillmentHouseValidation implements Validation
  {
    String                  ErrorMessage      = null;
    FieldGroup              Fields            = null;

    public FulfillmentHouseValidation( FieldGroup fields )
    {
      Fields = fields;
    }

    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate( String fdata )
    {
      ErrorMessage = null;

      // isBlank prevents NullPointerException
      if ( !isBlank(fdata) && fdata.toUpperCase().equals("Y") )
      {
        if (Fields.getField("fulfillmentAddr").isBlank() ||
            Fields.getField("fulfillmentCity").isBlank() ||
            Fields.getField("fulfillmentState").isBlank() ||
            Fields.getField("fulfillmentZip").isBlank())
        {
          ErrorMessage = "Please provide the full address of the Fulfillment House";
        }
      }
      return( ErrorMessage == null );
    }
  }

  protected class LocationAddressValidation implements Validation
  {
    private String errorText;

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String fdata)
    {
      boolean isValid = true;
      try
      {
        if (fdata.equals("Other"))
        {
          if (fields.getField("locationAddr").isBlank() ||
              fields.getField("locationCity").isBlank() ||
              fields.getField("locationState").isBlank() ||
              fields.getField("locationZip").isBlank())
          {
            errorText = "Please provide the full address of the inspected location";
            isValid = false;
          }
        }
      }
      catch(Exception e)
      {
        errorText = "Please select the address of the inspected location";
        isValid = false;
      }
      
      return isValid;
    }
  }

  protected class LocationTypeValidation implements Validation
  {
    Field       OtherField          = null;

    public LocationTypeValidation( Field otherField )
    {
      OtherField      = otherField;
    }

    public String getErrorText()
    {
      return("Please provide an explanation when selecting Other for the business location");
    }

    public boolean validate( String fdata )
    {
      return ( !OtherField.getData().equals("Other")
               || (fdata != null && fdata.length() > 0) );
    }
  }

  //**************************************************************************
  // Data Bean
  //**************************************************************************

  public Pricing()
  {
  }

  // pricing types
  protected static String[][] PricePlanRadioButtons =
  {
    { "Fixed Rate Plan",    Integer.toString( mesConstants.APP_PS_FIXED_PLUS_PER_ITEM ) },
    { "BET 4001/5001 Interchange Plan",   Integer.toString( mesConstants.APP_PS_INTERCHANGE )         },
    { "BET 4090/5090 Interchange & Assessment pass through + .10% added on Assessment calculation", Integer.toString( mesConstants.APP_PS_MTWEST_4090_5090 )    },
    { "BET 4178/5178 Retail = Rewards/World .20% + $0.10 - MID .80% + $0.10 - NON 1.50% + $0.10", Integer.toString(mesConstants.APP_PS_MTWEST_4178_5178) },
    { "BET 4179/5179 MOTO = MID .60% + $0.10 - NON 1.15% + $0.10", Integer.toString(mesConstants.APP_PS_MTWEST_4179_5179) },
    { "BET 4180/5180 (Retail Costco Style)", Integer.toString(mesConstants.APP_PS_MTWEST_4180_5180) },
  };

  // pos type (set on page 1) determines if certain fee options are present
  protected int posType = -1;

  /*
  ** protected boolean loadPosType()
  **
  ** Loads the pos type from the pos_category/merch_pos.  This is an option set
  ** on page 1 of the app.
  **
  ** RETURNS: true if load successful, else false.
  */
  protected boolean loadPosType()
  {
    boolean loadOk = false;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:324^7*/

//  ************************************************************
//  #sql [Ctx] { select  pc.pos_type
//          
//          from    pos_category  pc,
//                  merch_pos     mp
//          where   mp.app_seq_num = :AppSeqNum
//                  and pc.pos_code = mp.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pc.pos_type\n         \n        from    pos_category  pc,\n                merch_pos     mp\n        where   mp.app_seq_num =  :1 \n                and pc.pos_code = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.mtwest.Pricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   posType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:332^7*/

      loadOk = true;
    }
    catch (Exception e)
    {
      logEntry("loadPosType()",e.toString());
    }
    finally
    {
    }
    return loadOk;
  }

  /*
  ** protected int getPosType()
  **
  ** Loads pos type if not loaded already.
  **
  ** RETURN: loaded pos type.
  */
  protected int getPosType()
  {
    if (posType == -1)
    {
      loadPosType();
    }
    return posType;
  }

  /*
  ** protected boolean isPosType(int checkPosType)
  **
  ** Checks the pos type given against the app's pos type.
  **
  ** RETURNS: true if app's pos type matches the given pos type.
  */
  protected boolean isPosType(int checkPosType)
  {
    return checkPosType == getPosType();
  }

  public class FeeItem
  {
    private String  label;
    private Field   feeField;
    private int     secType;
    private int     dbType;
    private int     dbCode;
    
    public FeeItem(Field feeField, String label, int secType, int dbType, int dbCode)
    {
      this.feeField = feeField;
      this.label    = label;
      this.secType  = secType;
      this.dbType   = dbType;
      this.dbCode   = dbCode;
    }
    
    public Field getField()
    {
      return feeField;
    }
    public String getLabel()
    {
      return label;
    }
    public int getSecType()
    {
      return secType;
    }
    public int getDbType()
    {
      return dbType;
    }
    public int getDbCode()
    {
      return dbCode;
    }
  }
    
  // section types
  private static final int  SEC_CARD_FEES     = 1;
  private static final int  SEC_MISC_CHARGES  = 2;
  
  // database fee types
  private static final int  DB_CARD_FEE       = 1;
  private static final int  DB_MISC_CHARGE    = 2;
  
  private Vector feeItems = new Vector();
  
  private String[] miscChargeDescs = null;
  
  /*
  ** protected boolean loadMiscChargeDescs()
  **
  ** Attempts to load charge item descriptions from the miscdescrs table into
  ** the miscChargeDescs array.
  **
  ** RETURNS: true if load successful, else false.
  */
  protected boolean loadMiscChargeDescs()
  {
    boolean           loadOk  = false;
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:441^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  misc_description,
//                  misc_code
//          from    miscdescrs
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  misc_description,\n                misc_code\n        from    miscdescrs";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.mtwest.Pricing",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.mtwest.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:446^7*/
      rs = it.getResultSet();

      miscChargeDescs = new String[mesConstants.APP_MISC_CHARGE_COUNT];
      while (rs.next())
      {
        int chargeIdx = rs.getInt("misc_code");
        if (chargeIdx < miscChargeDescs.length)
        {
          miscChargeDescs[chargeIdx] = rs.getString("misc_description");
        }
      }

      loadOk = true;
    }
    catch (Exception e)
    {
      logEntry("loadMiscChargeDescs()",e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { it.close(); } catch (Exception e) {}
    }

    return loadOk;
  }

  /*
  ** public String getMiscChargeDesc(int chargeIdx)
  **
  ** Retrieves the description corresponding with the charge index.
  **
  ** RETURNS: the charge description string corresponding with the index given,
  **          or null if index is out of range or the charge description array
  **          cannot be loaded.
  */
  public String getMiscChargeDesc(int chargeId)
  {
    String chargeDesc = null;
    if ((miscChargeDescs != null || loadMiscChargeDescs())
        && chargeId < miscChargeDescs.length)
    {
      chargeDesc = miscChargeDescs[chargeId];
    }
    
    // may want to use different descriptors on the app page 
    switch (chargeId)
    {
      case mesConstants.APP_MISC_CHARGE_RUSH_FEE:
        chargeDesc = "Rush Fee";
        break;
        
      default:
        break;
    }
    
    return chargeDesc;
  }

  /*
  ** private void addMiscCharge(int secType, int dbCode)
  **
  ** Creates a misc charge fee item with the section type and the misc charge
  ** code given and adds it to the fees vector.  Fee tables are created for
  ** each charge type if needed, currency fields are created for the rest.
  */
  private void addMiscCharge(int secType, int dbCode)
  {
    // skip certain charge types if certain conditions aren't met
    switch (dbCode)
    {
      // only do referral auth fees if dialpay merchant
      case mesConstants.APP_MISC_CHARGE_REFERRAL_AUTH_FEE:
        if (!isPosType(mesConstants.POS_DIAL_AUTH))
        {
          return;
        }
        break;
        
      // suppress voice auth option if merchant uses dial pay
      // (dialpay fee will replace the voice auth fee)
      case mesConstants.APP_MISC_CHARGE_VOICE_AUTH_FEE:
        if (isPosType(mesConstants.POS_DIAL_AUTH))
        {
          return;
        }
        break;
        
      // suppress virtual terminal fees if not vt
      case mesConstants.APP_MISC_CHARGE_VT_SETUP_FEE:
      case mesConstants.APP_MISC_CHARGE_VT_MONTHLY_FEE:
        if( ! isPosType(mesConstants.POS_VIRTUAL_TERMINAL) )
        {
          return;
        }
        break;
    }
    
    // get a description of the charge
    String feeLabel = getMiscChargeDesc(dbCode);

    // determine fee table and default fee amount
    // no fee table means use currency field
    DropDownTable feeTable = null;
    String defaultAmount = null;
    switch (dbCode)
    {
      case mesConstants.APP_MISC_CHARGE_VOICE_AUTH_FEE:
        feeTable = new DollarAmountTable(new double[] { 0, 0.55 });
        break;
      
      case mesConstants.APP_MISC_CHARGE_CHARGEBACK:
        feeTable = new DollarAmountTable(new double[] { 0, 12, 15, 20 });
        break;
                
      case mesConstants.APP_MISC_CHARGE_HELP_DESK:
        feeTable = new DollarAmountTable(new double[] { 0, 3, 6, 9 });
        break;
        
      case mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT:
        feeTable = new DollarAmountTable(new double[] { 0, 5, 6 });
        break;
      
      case mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_SETUP:
        feeTable = new DollarAmountTable(new double[] { 0, 20, 30, 40, 50, 60, 75 });
        break;
      
      case mesConstants.APP_MISC_CHARGE_RUSH_FEE:
        feeTable = new DollarAmountTable(new double[] { 0, 70 });
        break;
      
      case mesConstants.APP_MISC_CHARGE_EMERGENCY_REPLACEMENT:
        feeTable = new DollarAmountTable(new double[] { 0, 3 });
        break;
      
      case mesConstants.APP_MISC_CHARGE_SUPPLIES:
        feeTable = new DollarAmountTable(new double[] { 0, 3 });
        break;
      
      case mesConstants.APP_MISC_CHARGE_DEBIT_SETUP_FEE:
        feeTable = new DollarAmountTable(new double[] { 0, 25, 50 });
        break;
        
      case mesConstants.APP_MISC_CHARGE_PCI_MONTHLY:
        defaultAmount = "3.00";
        break;
    }

    // create the fee field
    Field feeField;
    String fieldName = "miscChargeFee" + dbCode;
    if (feeTable != null)
    {
      feeField = new DropDownField(fieldName,feeTable,true);
    }
    else
    {
      feeField = new CurrencyField(fieldName,8,6,true);
    }

    // set default amount
    if (defaultAmount != null)
    {
      feeField.setData(defaultAmount);
    }

    // add the fee field to the field beans field group
    fields.add(feeField);

    // add the fee item to the fee items vector
    feeItems.add(new FeeItem(feeField,feeLabel,secType,DB_MISC_CHARGE,dbCode));
  }

  // misc charges supported by app, section code followed by charge code
  private static int[][] miscChargeCodes =
  {
    { SEC_CARD_FEES,    mesConstants.APP_MISC_CHARGE_VOICE_AUTH_FEE           },
    { SEC_MISC_CHARGES, mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_SETUP        },
    { SEC_MISC_CHARGES, mesConstants.APP_MISC_CHARGE_HELP_DESK                },
    { SEC_MISC_CHARGES, mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT        },
    { SEC_MISC_CHARGES, mesConstants.APP_MISC_CHARGE_CHARGEBACK               },
    { SEC_MISC_CHARGES, mesConstants.APP_MISC_CHARGE_RUSH_FEE                 },
    { SEC_MISC_CHARGES, mesConstants.APP_MISC_CHARGE_EMERGENCY_REPLACEMENT    },
    { SEC_MISC_CHARGES, mesConstants.APP_MISC_CHARGE_SUPPLIES                 },
    { SEC_MISC_CHARGES, mesConstants.APP_MISC_CHARGE_DEBIT_SETUP_FEE          },
    { SEC_MISC_CHARGES, mesConstants.APP_MISC_CHARGE_VT_SETUP_FEE             },
    { SEC_MISC_CHARGES, mesConstants.APP_MISC_CHARGE_VT_MONTHLY_FEE           },
    { SEC_MISC_CHARGES, mesConstants.APP_MISC_CHARGE_PCI_MONTHLY              }
  };

  /*
  ** private void initMiscCharges()
  **
  ** Iterates through the misc charge codes list, adds each specified misc charge.
  */
  private void initMiscCharges()
  {
    for (int i = 0; i < miscChargeCodes.length; ++i)
    {
      addMiscCharge(miscChargeCodes[i][0],miscChargeCodes[i][1]);
    }
  }

  private int[]     acceptedCardTypes;
  private String[]  acceptedCardDescs;

  /*
  ** private void loadAcceptedCardTypes()
  **
  ** Loads all accepted card types that can have card fees associated with them
  ** into the accepted card types array.  Also loads card descriptions into the
  ** card descriptors array.
  */
  private void loadAcceptedCardTypes()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;

    try
    {
      Vector types = new Vector();
      Vector descs = new Vector();
      
      // query database for accepted card types
      /*@lineinfo:generated-code*//*@lineinfo:671^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ct.cardtype_desc,
//                  ct.cardtype_code
//          from    ( select  distinct(cardtype_code) as cardtype_code
//                    from    merchpayoption
//                    where   app_seq_num = :AppSeqNum ) at,
//                  cardtype ct
//          where   ct.cardtype_code = at.cardtype_code
//                  and ct.cardtype_code in
//                               -- card types allowing additional per item fee
//                               ( :mesConstants.APP_CT_DINERS_CLUB,
//                                 :mesConstants.APP_CT_DISCOVER,
//                                 :mesConstants.APP_CT_JCB,
//                                 :mesConstants.APP_CT_AMEX,
//                                 :mesConstants.APP_CT_DEBIT,
//                                 :mesConstants.APP_CT_CHECK_AUTH,
//                                 :mesConstants.APP_CT_EBT )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ct.cardtype_desc,\n                ct.cardtype_code\n        from    ( select  distinct(cardtype_code) as cardtype_code\n                  from    merchpayoption\n                  where   app_seq_num =  :1  ) at,\n                cardtype ct\n        where   ct.cardtype_code = at.cardtype_code\n                and ct.cardtype_code in\n                             -- card types allowing additional per item fee\n                             (  :2 ,\n                                :3 ,\n                                :4 ,\n                                :5 ,\n                                :6 ,\n                                :7 ,\n                                :8  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_DINERS_CLUB);
   __sJT_st.setInt(3,mesConstants.APP_CT_DISCOVER);
   __sJT_st.setInt(4,mesConstants.APP_CT_JCB);
   __sJT_st.setInt(5,mesConstants.APP_CT_AMEX);
   __sJT_st.setInt(6,mesConstants.APP_CT_DEBIT);
   __sJT_st.setInt(7,mesConstants.APP_CT_CHECK_AUTH);
   __sJT_st.setInt(8,mesConstants.APP_CT_EBT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.mtwest.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:689^7*/
      rs = it.getResultSet();
      while (rs.next())
      {
        types.add(rs.getString("cardtype_code"));
        descs.add(rs.getString("cardtype_desc"));
      }

      // internet is always a fee type
      types.add(Integer.toString(mesConstants.APP_CT_INTERNET));
      descs.add("Internet Transaction");

      // if pos type is dial pay, add dial pay transactions as a type
      if (isPosType(mesConstants.POS_DIAL_AUTH))
      {
        types.add(Integer.toString(mesConstants.APP_CT_DIAL_PAY));
        descs.add("DialPay Transaction");
      }

      // convert type strings to ints
      String[] typesStrs = (String[])(types.toArray(new String[0]));
      acceptedCardTypes = new int[typesStrs.length];
      for (int i = 0; i < typesStrs.length; ++i)
      {
        acceptedCardTypes[i] = Integer.parseInt(typesStrs[i]);
      }

      // get array of strings for the card type descriptors
      acceptedCardDescs = (String[])(descs.toArray(new String[0]));
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::loadAcceptedCardTypes(): "
        + e.toString());
      logEntry("loadAcceptedCardTypes()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
    }
  }

  /*
  ** private void addCardFee(int cardType, String descriptor)
  **
  ** Creates a field for the card type given, either a drop down with specific
  ** fee amounts or a free text currency field.  The fees default value is also
  ** determined as well as the descriptor suffix ("per auth" vs. "per item").
  */
  private void addCardFee(int cardType, String descriptor)
  {
    // skip certain card types unless conditions are met
    switch(cardType)
    {
      // only do internet tran auth fees if this is internet merchant
      case mesConstants.APP_CT_INTERNET:
        if (!isPosType(mesConstants.POS_INTERNET))
        {
          return;
        }
        break;
    }

    // create a fee table and set the default
    DropDownTable feeTable      = null;
    String        feeLabel      = descriptor;
    double        defaultFee    = 0;
    switch(cardType)
    {
      case mesConstants.APP_CT_DEBIT:
        feeTable = new DollarAmountTable(new double[] { 0, 0.4, 0.45, 0.6, 0.65 });
        break;

      case mesConstants.APP_CT_DIAL_PAY:
        feeTable = new DollarAmountTable(new double[] { 0, 0.55 });
        break;

      case mesConstants.APP_CT_EBT:
        feeTable = new DollarAmountTable(new double[] { 0, 0.3, 0.35, 0.4, 0.5 });
        break;

      // amex, discover, internet, diners, jcb, etc.
      default:
        feeTable = new DollarAmountTable(new double[] { 0, 0.15 });
        defaultFee = 0.15;
        break;
    }

    Field feeField = null;
    String fieldName = "cardFee" + cardType;
    if (feeTable != null)
    {
      // generate drop down field with fee table
      feeField = new DropDownField(fieldName,feeTable,true);
    }
    else
    {
      feeField = new CurrencyField(fieldName,8,6,true);
    }

    // set the default fee
    if (defaultFee != 0)
    {
      feeField.setData(Double.toString(defaultFee));
    }

    // add to field bean
    fields.add(feeField);

    // add the fee item to the fee items vector
    feeItems.add(new FeeItem(feeField,feeLabel,SEC_CARD_FEES,DB_CARD_FEE,cardType));
  }

  /*
  ** private void initCardFees()
  **
  ** Generate all applicable card per item fee fields.
  */
  private void initCardFees()
  {
    // load cards accepted array
    loadAcceptedCardTypes();

    // generate fee fields for each accepted card type
    for (int i = 0; i < acceptedCardTypes.length; ++i)
    {
      addCardFee(acceptedCardTypes[i],acceptedCardDescs[i]);
    }
    
    // generate v/mc per auth fees
    DropDownTable authTable = new DollarAmountTable(new double[]
    { 0.0,
      0.1,
      0.11,
      0.12,
      0.13,
      0.14,
      0.15,
      0.16,
      0.17,
      0.18,
      0.19,
      0.2,
      0.21,
      0.22,
      0.23,
      0.24,
      0.25,
      0.26,
      0.27,
      0.28,
      0.29,
      0.3 });
      
    fields.add(new DropDownField("perAuth", authTable, true));
  }

  /*
  ** private void initFeesAndCharges()
  **
  ** Initializes misc charge fees and card type fees.
  */
  private void initFeesAndCharges()
  {
    initCardFees();
    initMiscCharges();
  }

  /*
  ** private FeeItem[] getSectionFees(int secType)
  **
  ** Scans the fees vector and generates an array of FeeItems that match the given
  ** secType.
  **
  ** RETURNS: array of FeeItems of the given secType.
  */
  private FeeItem[] getSectionFees(int secType)
  {
    Vector sectionFees = new Vector();
    for (Iterator i = feeItems.iterator(); i.hasNext();)
    {
      FeeItem fi = (FeeItem)i.next();
      if (fi.getSecType() == secType)
      {
        sectionFees.add(fi);
      }
    }
    return (FeeItem[])sectionFees.toArray(new FeeItem[0]);
  }
  public FeeItem[] getCardSectionFees()
  {
    return getSectionFees(SEC_CARD_FEES);
  }
  public FeeItem[] getMiscSectionFees()
  {
    return getSectionFees(SEC_MISC_CHARGES);
  }

  private Field getFeeField(int dbType, int dbCode)
  {
    for (Iterator i = feeItems.iterator(); i.hasNext();)
    {
      FeeItem fi = (FeeItem)i.next();
      if (fi.getDbType() == dbType && fi.getDbCode() == dbCode)
      {
        return fi.getField();
      }
    }
    return null;
  }
  public Field getMiscChargeField(int feeType)
  {
    return getFeeField(DB_MISC_CHARGE,feeType);
  }
  public Field getCardTypeField(int cardType)
  {
    return getFeeField(DB_CARD_FEE,cardType);
  }

  /*
  ** private FeeItem[] getDbFees(int dbType)
  **
  ** Scans the fees vector and generates an array of FeeItems that match the given
  ** dbType.
  **
  ** RETURNS: array of FeeItems of the given dbType.
  */
  private FeeItem[] getDbFees(int dbType)
  {
    Vector dbFees = new Vector();
    for (Iterator i = feeItems.iterator(); i.hasNext();)
    {
      FeeItem fi = (FeeItem)i.next();
      if (fi.getDbType() == dbType)
      {
        dbFees.add(fi);
      }
    }
    return (FeeItem[])dbFees.toArray(new FeeItem[0]);
  }
  public FeeItem[] getDbCardFees()
  {
    return getDbFees(DB_CARD_FEE);
  }
  public FeeItem[] getDbMiscFees()
  {
    return getDbFees(DB_MISC_CHARGE);
  }

  protected boolean allowAmexZero( )
  {
    String              allowZero     = null;
    boolean             retVal        = false;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:945^7*/

//  ************************************************************
//  #sql [Ctx] { -- true if either split dial OR PIP was selected
//          select  decode( nvl(po.merchpo_split_dial,'N'),
//                          'Y','Y',
//                          nvl(po.merchpo_pip,'N') )        
//          from    merchpayoption          po
//          where   po.app_seq_num = :AppSeqNum and
//                  cardtype_code = :mesConstants.APP_CT_AMEX
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "-- true if either split dial OR PIP was selected\n        select  decode( nvl(po.merchpo_split_dial,'N'),\n                        'Y','Y',\n                        nvl(po.merchpo_pip,'N') )         \n        from    merchpayoption          po\n        where   po.app_seq_num =  :1  and\n                cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.mtwest.Pricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_AMEX);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   allowZero = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:954^7*/
      retVal = allowZero.equals("Y");
    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }

  public class EquipmentFee
  {
    public    int                 EquipCount        = 0;
    public    String              EquipDesc         = null;
    protected String              EquipModel        = null;
    protected int                 LendType          = 0;
    public    String              LendTypeDesc      = null;
    public    Field               perItemField      = null;

    public EquipmentFee(ResultSet rs)
      throws java.sql.SQLException
    {
      Field               field   =   null;

      // store the HTML descriptoins
      LendType      = rs.getInt("lend_type");
      LendTypeDesc  = rs.getString("lend_type_desc");
      EquipDesc     = rs.getString("equip_desc");
      EquipModel    = rs.getString("equip_model");
      EquipCount    = rs.getInt("equip_count");

      // add the currency input field
      String nameBase = getFieldNameBase();
      perItemField = new CurrencyField(nameBase + "_PerItem",8,10,false);
      perItemField.setData(rs.getString("per_item"));
      fields.add(perItemField);
    }

    public String getFieldNameBase()
    {
      StringBuffer fieldName = new StringBuffer();

      fieldName.append(EquipModel);
      fieldName.append("_");
      fieldName.append(LendType);

      return(fieldName.toString());
    }

    public double getTotalAmount()
    {
      double      perItem       = 0.0;
      try
      {
        perItem = perItemField.asDouble();
      }
      catch( Exception e )
      {
      }
      return( EquipCount * perItem );
    }
  }

  protected Vector EquipmentFees = new Vector();

  protected void initEquipmentFees( )
  {
    ResultSetIterator         it            = null;
    ResultSet                 resultSet     = null;

    try
    {
      EquipmentFees.removeAllElements();

      /*@lineinfo:generated-code*//*@lineinfo:1027^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  eq.equip_model                          as equip_model,
//                  decode( po.prod_option_des,
//                          null, eq.equip_descriptor,
//                          ( eq.equip_descriptor || '-' ||
//                            po.prod_option_des ) )        as equip_desc,
//                  lt.equiplendtype_code                   as lend_type,
//                  lt.equiplendtype_description            as lend_type_desc,
//                  me.merchequip_equip_quantity            as equip_count,
//                  me.merchequip_amount                    as per_item
//          from    merchequipment    me,
//                  equipment         eq,
//                  equiplendtype     lt,
//                  prodoption        po
//          where   me.app_seq_num = :AppSeqNum and
//                  -- all of the buy items
//                  me.equiplendtype_code in
//                  (
//                    :mesConstants.APP_EQUIP_PURCHASE,         -- 1
//                    :mesConstants.APP_EQUIP_RENT,             -- 2
//                    :mesConstants.APP_EQUIP_BUY_REFURBISHED,  -- 4
//                    :mesConstants.APP_EQUIP_LEASE             -- 5
//                  ) and
//                  eq.equip_model = me.equip_model and
//                  po.prod_option_id(+) = me.prod_option_id and
//                  lt.equiplendtype_code = me.equiplendtype_code
//          order by me.equiplendtype_code, eq.equiptype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  eq.equip_model                          as equip_model,\n                decode( po.prod_option_des,\n                        null, eq.equip_descriptor,\n                        ( eq.equip_descriptor || '-' ||\n                          po.prod_option_des ) )        as equip_desc,\n                lt.equiplendtype_code                   as lend_type,\n                lt.equiplendtype_description            as lend_type_desc,\n                me.merchequip_equip_quantity            as equip_count,\n                me.merchequip_amount                    as per_item\n        from    merchequipment    me,\n                equipment         eq,\n                equiplendtype     lt,\n                prodoption        po\n        where   me.app_seq_num =  :1  and\n                -- all of the buy items\n                me.equiplendtype_code in\n                (\n                   :2 ,         -- 1\n                   :3 ,             -- 2\n                   :4 ,  -- 4\n                   :5              -- 5\n                ) and\n                eq.equip_model = me.equip_model and\n                po.prod_option_id(+) = me.prod_option_id and\n                lt.equiplendtype_code = me.equiplendtype_code\n        order by me.equiplendtype_code, eq.equiptype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_RENT);
   __sJT_st.setInt(4,mesConstants.APP_EQUIP_BUY_REFURBISHED);
   __sJT_st.setInt(5,mesConstants.APP_EQUIP_LEASE);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.app.mtwest.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1055^7*/
      resultSet = it.getResultSet();

      while(resultSet.next())
      {
        EquipmentFees.addElement(new EquipmentFee(resultSet));
      }
      resultSet.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:1065^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  null                                    as equip_model,
//                  'Equipment Support Fee'                 as equip_desc,
//                  me.equiplendtype_code                   as lend_type,
//                  lt.equiplendtype_description            as lend_type_desc,
//                  sum(me.merchequip_equip_quantity)       as equip_count,
//                  avg(me.merchequip_amount)               as per_item
//          from    merchequipment    me,
//                  equipment         eq,
//                  equiplendtype     lt,
//                  prodoption        po
//          where   me.app_seq_num = :AppSeqNum and
//                  -- all of the owned equipment except imprinters
//                  me.equiplendtype_code  = :mesConstants.APP_EQUIP_OWNED and
//                  me.equiptype_code <> :mesConstants.APP_EQUIP_TYPE_IMPRINTER and
//                  eq.equip_model = me.equip_model and
//                  po.prod_option_id(+) = me.prod_option_id and
//                  lt.equiplendtype_code = me.equiplendtype_code
//          group by me.equiplendtype_code, lt.equiplendtype_description
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  null                                    as equip_model,\n                'Equipment Support Fee'                 as equip_desc,\n                me.equiplendtype_code                   as lend_type,\n                lt.equiplendtype_description            as lend_type_desc,\n                sum(me.merchequip_equip_quantity)       as equip_count,\n                avg(me.merchequip_amount)               as per_item\n        from    merchequipment    me,\n                equipment         eq,\n                equiplendtype     lt,\n                prodoption        po\n        where   me.app_seq_num =  :1  and\n                -- all of the owned equipment except imprinters\n                me.equiplendtype_code  =  :2  and\n                me.equiptype_code <>  :3  and\n                eq.equip_model = me.equip_model and\n                po.prod_option_id(+) = me.prod_option_id and\n                lt.equiplendtype_code = me.equiplendtype_code\n        group by me.equiplendtype_code, lt.equiplendtype_description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_OWNED);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_TYPE_IMPRINTER);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.app.mtwest.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1085^7*/
      resultSet = it.getResultSet();

      if(resultSet.next())
      {
        EquipmentFees.addElement(new EquipmentFee(resultSet));
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry("initEquipmentFees()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }

  // indexes into the QuestionFields string array
  public static final int Q_FIELD_NAME  = 0;
  public static final int Q_TEXT        = 1;

  protected static String[][] QuestionFields =
  {
      // field name           // question
    { "qNameMatch",           "Did name posted at business match business name on application?" },
    { "qSignage",             "Did location appear to have appropriate signage?" },
    { "qHoursPosted",         "Were business hours posted?" },
    { "qInventoryReview",     "Was merchant's inventory viewed?" },
    { "qInventoryConsistent", "Was inventory consistent with merchant's type of business?" },
    { "qInventoryAdequate",   "Did inventory appear to be adequate to support the sales volume indicated on the application?" }
  };

  public class NoneOrAllValidation implements Validation
  {
    private Vector fields = new Vector();
    private String errorText;

    public NoneOrAllValidation(Vector fields,String errorText)
    {
      this.fields = fields;
      this.errorText = errorText;
    }

    public NoneOrAllValidation(String errorText)
    {
      this.fields = new Vector();
      this.errorText = errorText;
    }

    public void add(Field addField)
    {
      fields.add(addField);
    }

    public boolean validate(String fieldData)
    {
      int blankFields = 0;
      for (Iterator i = fields.iterator(); i.hasNext();)
      {
        Field field = (Field)i.next();
        if (field.isBlank())
        {
          ++blankFields;
        }
      }

      return (blankFields == 0 || blankFields == fields.size());
    }

    public String getErrorText()
    {
      return errorText;
    }
  }
  
  public String getAccountType()
  {
    String accountType = "";
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1168^7*/

//  ************************************************************
//  #sql [Ctx] { select  account_type
//          
//          from    merchant
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  account_type\n         \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.mtwest.Pricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   accountType = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1174^7*/
    }
    catch (Exception e)
    {
      logEntry("getAccountType()",e.toString());
    }
    finally
    {
    }
    return accountType;
  }

  public void init( )
  {
    super.init();

    try
    {
      // 1. v/mc rates and fees

      // main v/mc pricing plan options
      fields.add(new RadioButtonField   ("pricingPlan",         PricePlanRadioButtons,-1,false,"Please select a Pricing Scenario"));
      fields.add(new DiscountField      ("discRate1",           true));
      fields.add(new SmallCurrencyField ("perItem1",            5,6,true));
      fields.add(new DiscountField      ("discRate2",           true));
      fields.add(new SmallCurrencyField ("perItem2",            5,6,true));
      fields.add(new DiscountField      ("discRate3",           true));
      fields.add(new SmallCurrencyField ("perItem3",            5,6,true));
      fields.add(new DiscountField      ("discRate4",           true));
      fields.add(new SmallCurrencyField ("perItem4",            5,6,true));
      fields.add(new DiscountField      ("discRate5",           true));
      fields.add(new SmallCurrencyField ("perItem5",            5,6,true));
      fields.add(new DiscountField      ("discRate6",           true));
      fields.add(new SmallCurrencyField ("perItem6",            5,6,true));
      fields.add(new DropDownField      ("pricingType",         new PricingTypeTable(),false));
      fields.add(new CurrencyField      ("minDiscountAmount",   6,8,true));

      fields.getField("pricingPlan").addValidation(new PricingPlanValidation());

      // 2. other payment type fees (card type fees)
      // 3. miscellaneous fees
      // these are now merged because some misc charges have
      // to be displayed under section 2
      initFeesAndCharges();

      // 4. equipment/software
      initEquipmentFees();
      fields.add(new NumberField        ("taxRate",             4,8,true,2));

      // 5. pricing comments
      fields.add(new TextareaField      ("pricingComments",     400,7,50,true));

      // 6. site information
      fields.add(new DropDownField      ("locationType",        new LocationTypeTable(),true));
      fields.add(new Field              ("locationDesc",        100,37,true ));
      fields.add(new Field              ("locationAddr",        50,35,true ));
      fields.add(new Field              ("locationCity",        50,35,true ));
      fields.add(new DropDownField      ("locationState",       new StateDropDownTable(),true));
      fields.add(new ZipField           ("locationZip",         true,fields.getField("locationState")));
      fields.add(new DropDownField      ("locationAddrType",    new LocationAddressTable(), true));
      fields.add(new NumberField        ("employeeCount",       6,3,true,0));

      for( int i = 0; i < QuestionFields.length; ++i )
      {
        fields.add(new DropDownField(QuestionFields[i][Q_FIELD_NAME],new YesNoTable(),true));
      }

      fields.getField("locationDesc").addValidation(
        new LocationTypeValidation(fields.getField("locationType")));

      fields.getField("locationAddrType").addValidation(
        new LocationAddressValidation());

      // 7. e-commerce/moto info
      fields.add(new Field              ("inventoryAddr",           50,35,true));
      fields.add(new Field              ("inventoryCity",           50,35,true));
      fields.add(new DropDownField      ("inventoryState",          new StateDropDownTable(), true ) );
      fields.add(new ZipField           ("inventoryZip",            true,fields.getField("inventoryState")));
      fields.add(new CurrencyField      ("inventoryValue",          12,12,true));
      fields.add(new Field              ("fulfillmentName",         100,35,true));
      fields.add(new Field              ("fulfillmentAddr",         50,35,true));
      fields.add(new Field              ("fulfillmentCity",         50,35,true));
      fields.add(new DropDownField      ("fulfillmentState",        new StateDropDownTable(),true));
      fields.add(new ZipField           ("fulfillmentZip",          true,fields.getField("fulfillmentState")));
      fields.add(new DropDownField      ("fulfillmentHouse",        new YesNoTable(),true));
      fields.add(new DropDownField      ("securitySoftware",        new YesNoTable(),true));
      fields.add(new Field              ("securitySoftwareVendor",  100,35,true));

      fields.getField("fulfillmentHouse").addValidation(
        new FulfillmentHouseValidation(fields));

      fields.getField("securitySoftwareVendor").addValidation(
        new IfYesNotBlankValidation(fields.getField("securitySoftware"),
          "Please specify the name of the security software used at this location"));

      // set the field style class
      fields.addHtmlExtra("class=\"formFields\"");
      
      // default Visa/MC auth fee to .12
      fields.setData("perAuth", "0.12");
    }
    catch( Exception e )
    {
      logEntry("init()",e.toString());
    }
    finally
    {
    }
  }

  /*
  ** protected void loadPricingPlan()
  **
  ** Load v/mc specific pricing info.
  */
  protected void loadPricingPlan()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      // load the pricing type
      /*@lineinfo:generated-code*//*@lineinfo:1297^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bet_type_code as pricing_type
//          from    merchant
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bet_type_code as pricing_type\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.app.mtwest.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1302^7*/
      setFields(it.getResultSet());
      it.close();

      // load v/mc pricing
      /*@lineinfo:generated-code*//*@lineinfo:1307^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tranchrg_disc_rate                as disc_rate,
//                  tranchrg_pass_thru                as per_tran,
//                  tranchrg_discrate_type            as pricing_plan,
//                  tranchrg_mmin_chrg                as min_discount_amount,
//                  tranchrg_per_auth                 as per_auth
//          from    tranchrg
//          where   app_seq_num   = :AppSeqNum and
//                  cardtype_code = :mesConstants.APP_CT_VISA
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tranchrg_disc_rate                as disc_rate,\n                tranchrg_pass_thru                as per_tran,\n                tranchrg_discrate_type            as pricing_plan,\n                tranchrg_mmin_chrg                as min_discount_amount,\n                tranchrg_per_auth                 as per_auth\n        from    tranchrg\n        where   app_seq_num   =  :1  and\n                cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.app.mtwest.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1317^7*/
      rs = it.getResultSet();

      // set the disc rate and per item fields corresponding with pricing plan
      if (rs.next())
      {
        fields.getField("pricingPlan").setData(rs.getString("pricing_plan"));
        switch( fields.getField("pricingPlan").asInteger() )
        {
          case mesConstants.APP_PS_FIXED_PLUS_PER_ITEM:
            fields.getField("discRate1").setData(rs.getString("disc_rate"));
            fields.getField("perItem1").setData(rs.getString("per_tran"));
            break;

          case mesConstants.APP_PS_INTERCHANGE:
            fields.getField("discRate2").setData(rs.getString("disc_rate"));
            fields.getField("perItem2").setData(rs.getString("per_tran"));
            break;

          case mesConstants.APP_PS_MTWEST_4090_5090:
            fields.getField("discRate3").setData(rs.getString("disc_rate"));
            fields.getField("perItem3").setData(rs.getString("per_tran"));
            break;
            
          case mesConstants.APP_PS_MTWEST_4178_5178:
            fields.getField("discRate4").setData(rs.getString("disc_rate"));
            fields.getField("perItem4").setData(rs.getString("per_tran"));
            break;
            
          case mesConstants.APP_PS_MTWEST_4179_5179:
            fields.getField("discRate5").setData(rs.getString("disc_rate"));
            fields.getField("perItem5").setData(rs.getString("per_tran"));
            break;
            
          case mesConstants.APP_PS_MTWEST_4180_5180:
            fields.getField("discRate6").setData(rs.getString("disc_rate"));
            fields.getField("perItem6").setData(rs.getString("per_tran"));
            break;
        }

        // set the min discount amount, per auth fee and enable
        setFields(rs,false);
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadPricingPlan()",e.toString());
      addError("loadPricingPlan: " + e.toString());
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** protected void loadNonBankFees()
  **
  ** Loads per item fees associated with non-v/mc card types.
  */
  protected void loadNonBankFees()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      // select all per item fees
      /*@lineinfo:generated-code*//*@lineinfo:1388^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tc.cardtype_code      as card_type,
//                  tc.tranchrg_per_tran  as per_item
//          from    merchpayoption  po,
//                  tranchrg        tc,
//                  cardtype        ct
//          where   tc.app_seq_num = :AppSeqNum
//                  and tc.cardtype_code in
//                    ( :mesConstants.APP_CT_DINERS_CLUB, -- 10
//                      :mesConstants.APP_CT_DISCOVER,    -- 14
//                      :mesConstants.APP_CT_JCB,         -- 15
//                      :mesConstants.APP_CT_AMEX,        -- 16
//                      :mesConstants.APP_CT_DEBIT,       -- 17
//                      :mesConstants.APP_CT_CHECK_AUTH,  -- 18
//                      :mesConstants.APP_CT_INTERNET,    -- 20
//                      :mesConstants.APP_CT_DIAL_PAY,    -- 21
//                      :mesConstants.APP_CT_EBT )        -- 22
//                  and tc.app_seq_num   = po.app_seq_num(+)
//                  and tc.cardtype_code = po.cardtype_code(+)
//                  and ct.cardtype_code(+) = tc.cardtype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tc.cardtype_code      as card_type,\n                tc.tranchrg_per_tran  as per_item\n        from    merchpayoption  po,\n                tranchrg        tc,\n                cardtype        ct\n        where   tc.app_seq_num =  :1 \n                and tc.cardtype_code in\n                  (  :2 , -- 10\n                     :3 ,    -- 14\n                     :4 ,         -- 15\n                     :5 ,        -- 16\n                     :6 ,       -- 17\n                     :7 ,  -- 18\n                     :8 ,    -- 20\n                     :9 ,    -- 21\n                     :10  )        -- 22\n                and tc.app_seq_num   = po.app_seq_num(+)\n                and tc.cardtype_code = po.cardtype_code(+)\n                and ct.cardtype_code(+) = tc.cardtype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_DINERS_CLUB);
   __sJT_st.setInt(3,mesConstants.APP_CT_DISCOVER);
   __sJT_st.setInt(4,mesConstants.APP_CT_JCB);
   __sJT_st.setInt(5,mesConstants.APP_CT_AMEX);
   __sJT_st.setInt(6,mesConstants.APP_CT_DEBIT);
   __sJT_st.setInt(7,mesConstants.APP_CT_CHECK_AUTH);
   __sJT_st.setInt(8,mesConstants.APP_CT_INTERNET);
   __sJT_st.setInt(9,mesConstants.APP_CT_DIAL_PAY);
   __sJT_st.setInt(10,mesConstants.APP_CT_EBT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.app.mtwest.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1409^7*/
      rs = it.getResultSet();

      // load fields
      while(rs.next())
      {
        Field field = getCardTypeField(rs.getInt("card_type"));
        if (field != null)
        {
          // only override default per item setting if a per item
          // value actually exists
          String perItem = Double.toString(rs.getDouble("per_item"));
          if (perItem != null)
          {
            field.setData(perItem);
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadNonBankFees()",e.toString());
      addError("loadNonBankFees: " + e.toString());
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** protected void loadMiscFees()
  **
  ** Load all miscellaneous fees.
  */
  protected void loadMiscFees()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      // load charges from database
      /*@lineinfo:generated-code*//*@lineinfo:1453^7*/

//  ************************************************************
//  #sql [Ctx] it = { select misc_code                  misc_code,
//                 misc_chrg_amount           charge_amount
//          from   miscchrg
//          where  app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select misc_code                  misc_code,\n               misc_chrg_amount           charge_amount\n        from   miscchrg\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.app.mtwest.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1459^7*/
      rs = it.getResultSet();

      // set the charge info in the fields
      boolean defaultsCleared = false;
      while(rs.next())
      {
        Field feeField = getMiscChargeField(rs.getInt("misc_code"));
        if (feeField != null)
        {
          feeField.setData(Double.toString(rs.getDouble("charge_amount")));
        }
      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::loadMiscFees(): "
        + e.toString());
      logEntry("loadMiscFees()",e.toString());
      addError("loadMiscFees: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** protected void loadSiteInspection()
  **
  ** Loads site inspection data.
  */
  protected void loadSiteInspection()
  {
    ResultSetIterator         it          = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1497^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  siteinsp_comment                  as pricing_comments,
//                  siteinsp_name_flag                as q_name_match,
//                  siteinsp_inv_sign_flag            as q_signage,
//                  siteinsp_bus_hours_flag           as q_hours_posted,
//                  siteinsp_inv_viewed_flag          as q_inventory_review,
//                  siteinsp_inv_consistant_flag      as q_inventory_consistent,
//                  siteinsp_vol_flag                 as q_inventory_adequate,
//                  siteinsp_no_of_emp                as employee_count,
//                  siteinsp_inv_street               as inventory_addr,
//                  siteinsp_inv_city                 as inventory_city,
//                  siteinsp_inv_state                as inventory_state,
//                  siteinsp_inv_zip                  as inventory_zip,
//                  siteinsp_bus_loc                  as location_type,
//                  siteinsp_bus_loc_comment          as location_desc,
//                  siteinsp_bus_address              as location_addr_type,
//                  siteinsp_bus_street               as location_addr,
//                  siteinsp_bus_city                 as location_city,
//                  siteinsp_bus_state                as location_state,
//                  siteinsp_bus_zip                  as location_zip,
//                  siteinsp_inv_value                as inventory_value,
//                  siteinsp_full_flag                as fulfillment_house,
//                  siteinsp_full_name                as fulfillment_name,
//                  siteinsp_full_street              as fulfillment_addr,
//                  siteinsp_full_city                as fulfillment_city,
//                  siteinsp_full_state               as fulfillment_state,
//                  siteinsp_full_zip                 as fulfillment_zip,
//                  siteinsp_soft_flag                as security_software,
//                  siteinsp_soft_name                as security_software_vendor
//          from    siteinspection            si
//          where   si.app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  siteinsp_comment                  as pricing_comments,\n                siteinsp_name_flag                as q_name_match,\n                siteinsp_inv_sign_flag            as q_signage,\n                siteinsp_bus_hours_flag           as q_hours_posted,\n                siteinsp_inv_viewed_flag          as q_inventory_review,\n                siteinsp_inv_consistant_flag      as q_inventory_consistent,\n                siteinsp_vol_flag                 as q_inventory_adequate,\n                siteinsp_no_of_emp                as employee_count,\n                siteinsp_inv_street               as inventory_addr,\n                siteinsp_inv_city                 as inventory_city,\n                siteinsp_inv_state                as inventory_state,\n                siteinsp_inv_zip                  as inventory_zip,\n                siteinsp_bus_loc                  as location_type,\n                siteinsp_bus_loc_comment          as location_desc,\n                siteinsp_bus_address              as location_addr_type,\n                siteinsp_bus_street               as location_addr,\n                siteinsp_bus_city                 as location_city,\n                siteinsp_bus_state                as location_state,\n                siteinsp_bus_zip                  as location_zip,\n                siteinsp_inv_value                as inventory_value,\n                siteinsp_full_flag                as fulfillment_house,\n                siteinsp_full_name                as fulfillment_name,\n                siteinsp_full_street              as fulfillment_addr,\n                siteinsp_full_city                as fulfillment_city,\n                siteinsp_full_state               as fulfillment_state,\n                siteinsp_full_zip                 as fulfillment_zip,\n                siteinsp_soft_flag                as security_software,\n                siteinsp_soft_name                as security_software_vendor\n        from    siteinspection            si\n        where   si.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.app.mtwest.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1529^7*/
      setFields(it.getResultSet());
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadSiteInspection()",e.toString());
      addError("loadSiteInspection: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) { }
    }
  }

  /*
  ** public void loadData()
  **
  ** Loads all field data.
  */
  public void loadData()
  {
    try
    {
      loadPricingPlan();
      loadNonBankFees();
      loadMiscFees();
      loadSiteInspection();

      // equipment data is loaded during field initialization
    }
    catch( Exception e )
    {
      logEntry("loadData()",e.toString());
    }
    finally
    {
    }
  }

  /*
  ** protected void storeTransactionFees()
  **
  ** Stores all transaction fees, including v/mc pricing info and all non-v/mc
  ** card per item fees.
  */
  protected void storeTransactionFees()
  {
    try
    {
      // store the pricing type
      int pricingType = fields.getField("pricingType").asInteger();
      /*@lineinfo:generated-code*//*@lineinfo:1581^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     pricing_grid  = :pricingType,
//                  bet_type_code = :pricingType
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     pricing_grid  =  :1 ,\n                bet_type_code =  :2 \n        where   app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,pricingType);
   __sJT_st.setInt(2,pricingType);
   __sJT_st.setLong(3,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1587^7*/

      // clear tranchrg records
      /*@lineinfo:generated-code*//*@lineinfo:1590^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from  tranchrg
//          where app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from  tranchrg\n        where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1595^7*/

      // determine disc rate and per item based on pricing plan
      String  discRate  = null;
      String  perItem   = null;
      switch(fields.getField("pricingPlan").asInteger())
      {
        case mesConstants.APP_PS_FIXED_PLUS_PER_ITEM:
          discRate = fields.getData("discRate1");
          perItem  = fields.getData("perItem1");
          break;

        case mesConstants.APP_PS_INTERCHANGE:
          discRate = fields.getData("discRate2");
          perItem  = fields.getData("perItem2");
          break;

        case mesConstants.APP_PS_MTWEST_4090_5090:
          discRate = fields.getData("discRate3");
          perItem  = fields.getData("perItem3");
          break;
          
        case mesConstants.APP_PS_MTWEST_4178_5178:
          discRate = fields.getData("discRate4");
          perItem  = fields.getData("perItem4");
          break;
          
        case mesConstants.APP_PS_MTWEST_4179_5179:
          discRate = fields.getData("discRate5");
          perItem  = fields.getData("perItem5");
          break;
        
        case mesConstants.APP_PS_MTWEST_4180_5180:
          discRate = fields.getData("discRate6");
          perItem  = fields.getData("perItem6");
          break;
      }

      // store v/mc pricing
      /*@lineinfo:generated-code*//*@lineinfo:1634^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran,
//            tranchrg_per_auth
//          )
//          values
//          (
//            :AppSeqNum,
//            :mesConstants.APP_CT_VISA,
//            :fields.getData("pricingPlan"),
//            :discRate,
//            :perItem,
//            :fields.getData("minDiscountAmount"),
//            0,
//            :fields.getData("perAuth")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2987 = fields.getData("pricingPlan");
 String __sJT_2988 = fields.getData("minDiscountAmount");
 String __sJT_2989 = fields.getData("perAuth");
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran,\n          tranchrg_per_auth\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0,\n           :7 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   __sJT_st.setString(3,__sJT_2987);
   __sJT_st.setString(4,discRate);
   __sJT_st.setString(5,perItem);
   __sJT_st.setString(6,__sJT_2988);
   __sJT_st.setString(7,__sJT_2989);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1658^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1660^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran,
//            tranchrg_per_auth
//          )
//          values
//          (
//            :AppSeqNum,
//            :mesConstants.APP_CT_MC,
//            :fields.getData("pricingPlan"),
//            :discRate,
//            :perItem,
//            :fields.getData("minDiscountAmount"),
//            0,
//            :fields.getData("perAuth")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2990 = fields.getData("pricingPlan");
 String __sJT_2991 = fields.getData("minDiscountAmount");
 String __sJT_2992 = fields.getData("perAuth");
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran,\n          tranchrg_per_auth\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0,\n           :7 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_MC);
   __sJT_st.setString(3,__sJT_2990);
   __sJT_st.setString(4,discRate);
   __sJT_st.setString(5,perItem);
   __sJT_st.setString(6,__sJT_2991);
   __sJT_st.setString(7,__sJT_2992);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1684^7*/


      // store additional card type pricing
      FeeItem[] cardFields = getDbCardFees();
      for (int i = 0; i < cardFields.length; ++i)
      {
        /*@lineinfo:generated-code*//*@lineinfo:1691^9*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//            ( app_seq_num,
//              cardtype_code,
//              tranchrg_per_tran )
//            values
//            ( :AppSeqNum,
//              :cardFields[i].getDbCode(),
//              :cardFields[i].getField().getData() )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2993 = cardFields[i].getDbCode();
 String __sJT_2994 = cardFields[i].getField().getData();
   String theSqlTS = "insert into tranchrg\n          ( app_seq_num,\n            cardtype_code,\n            tranchrg_per_tran )\n          values\n          (  :1 ,\n             :2 ,\n             :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,__sJT_2993);
   __sJT_st.setString(3,__sJT_2994);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1701^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("storeTransactionFees()", e.toString());
      addError("storeTransactionFees: " + e.toString());
    }
  }

  /*
  ** protected void storeMiscFees()
  **
  ** Stores all miscellaneous fees.
  */
  protected void storeMiscFees()
  {
    try
    {
      // clear all misc fees from the database prior to inserting
      /*@lineinfo:generated-code*//*@lineinfo:1721^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from   miscchrg
//          where  app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from   miscchrg\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1726^7*/

      // place all non-zero fees in database
      FeeItem[] miscFees = getDbMiscFees();
      for (int i = 0; i < miscFees.length; ++i)
      {
        // if the fee is not zero, insert into db
        Field feeField = miscFees[i].getField();
        if (feeField.asDouble() != 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1736^11*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//              ( app_seq_num,
//                misc_code,
//                misc_chrg_amount )
//              values
//              ( :AppSeqNum,
//                :miscFees[i].getDbCode(),
//                :feeField.getData() )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2995 = miscFees[i].getDbCode();
 String __sJT_2996 = feeField.getData();
   String theSqlTS = "insert into miscchrg\n            ( app_seq_num,\n              misc_code,\n              misc_chrg_amount )\n            values\n            (  :1 ,\n               :2 ,\n               :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,__sJT_2995);
   __sJT_st.setString(3,__sJT_2996);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1746^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("storeMiscFees()",e.toString());
      addError("storeMiscFees: " + e.toString());
    }
  }

  /*
  ** protected void storeEquipmentFees()
  **
  ** Stores all equipment fee info.
  */
  protected void storeEquipmentFees()
  {
    EquipmentFee            equipFee        = null;
    Field                   field           = null;

    try
    {
      for( int i = 0; i < EquipmentFees.size(); ++i )
      {
        // extract the current equipment fee and corresponding field
        equipFee = (EquipmentFee) EquipmentFees.elementAt(i);

        /*@lineinfo:generated-code*//*@lineinfo:1774^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchequipment me
//            set     me.merchequip_amount = :equipFee.perItemField.getData()
//            where   me.app_seq_num = :AppSeqNum and
//                    me.equip_model =
//                      -- if the equipment model is null
//                      -- and the lend type is owned then
//                      -- force a match so that all owned
//                      -- equipment gets assigned the same amount
//                      decode( :equipFee.EquipModel,
//                              null, decode( :equipFee.LendType,
//                                            :mesConstants.APP_EQUIP_OWNED, me.equip_model,
//                                            'NONE' ),
//                              :equipFee.EquipModel ) and
//                    me.equiplendtype_code = :equipFee.LendType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2997 = equipFee.perItemField.getData();
   String theSqlTS = "update  merchequipment me\n          set     me.merchequip_amount =  :1 \n          where   me.app_seq_num =  :2  and\n                  me.equip_model =\n                    -- if the equipment model is null\n                    -- and the lend type is owned then\n                    -- force a match so that all owned\n                    -- equipment gets assigned the same amount\n                    decode(  :3 ,\n                            null, decode(  :4 ,\n                                           :5 , me.equip_model,\n                                          'NONE' ),\n                             :6  ) and\n                  me.equiplendtype_code =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2997);
   __sJT_st.setLong(2,AppSeqNum);
   __sJT_st.setString(3,equipFee.EquipModel);
   __sJT_st.setInt(4,equipFee.LendType);
   __sJT_st.setInt(5,mesConstants.APP_EQUIP_OWNED);
   __sJT_st.setString(6,equipFee.EquipModel);
   __sJT_st.setInt(7,equipFee.LendType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1790^9*/
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("storeEquipmentFees()",e.toString());
    }
    finally
    {
    }
  }

  /*
  ** protected void storeSiteInspection()
  **
  ** Stores site inspection info.
  */
  protected void storeSiteInspection( )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1811^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    siteinspection
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    siteinspection\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1816^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1818^7*/

//  ************************************************************
//  #sql [Ctx] { insert into siteinspection
//          (
//            app_seq_num,
//            siteinsp_comment,
//            siteinsp_name_flag,
//            siteinsp_inv_sign_flag,
//            siteinsp_bus_hours_flag,
//            siteinsp_inv_viewed_flag,
//            siteinsp_inv_consistant_flag,
//            siteinsp_vol_flag,
//            siteinsp_full_flag,
//            siteinsp_soft_flag,
//            siteinsp_inv_street,
//            siteinsp_inv_city,
//            siteinsp_inv_state,
//            siteinsp_inv_zip,
//            siteinsp_full_street,
//            siteinsp_full_city,
//            siteinsp_full_state,
//            siteinsp_full_zip,
//            siteinsp_bus_street,
//            siteinsp_bus_city,
//            siteinsp_bus_state,
//            siteinsp_bus_zip,
//            siteinsp_inv_value,
//            siteinsp_full_name,
//            siteinsp_no_of_emp,
//            siteinsp_bus_loc,
//            siteinsp_bus_loc_comment,
//            siteinsp_bus_address,
//            siteinsp_soft_name
//          )
//          values
//          (
//            :AppSeqNum,
//            :fields.getField("pricingComments").getData(),
//            :fields.getField("qNameMatch").getData().toUpperCase(),
//            :fields.getField("qSignage").getData().toUpperCase(),
//            :fields.getField("qHoursPosted").getData().toUpperCase(),
//            :fields.getField("qInventoryReview").getData().toUpperCase(),
//            :fields.getField("qInventoryConsistent").getData().toUpperCase(),
//            :fields.getField("qInventoryAdequate").getData().toUpperCase(),
//            :fields.getField("fulfillmentHouse").getData().toUpperCase(),
//            :fields.getField("securitySoftware").getData().toUpperCase(),
//            :fields.getField("inventoryAddr").getData(),
//            :fields.getField("inventoryCity").getData(),
//            :fields.getField("inventoryState").getData(),
//            :fields.getField("inventoryZip").getData(),
//            :fields.getField("fulfillmentAddr").getData(),
//            :fields.getField("fulfillmentCity").getData(),
//            :fields.getField("fulfillmentState").getData(),
//            :fields.getField("fulfillmentZip").getData(),
//            :fields.getField("locationAddr").getData(),
//            :fields.getField("locationCity").getData(),
//            :fields.getField("locationState").getData(),
//            :fields.getField("locationZip").getData(),
//            :fields.getField("inventoryValue").getData(),
//            :fields.getField("fulfillmentName").getData(),
//            :fields.getField("employeeCount").getData(),
//            :fields.getField("locationType").getData(),
//            :fields.getField("locationDesc").getData(),
//            :fields.getField("locationAddrType").getData(),
//            :fields.getField("securitySoftwareVendor").getData()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2998 = fields.getField("pricingComments").getData();
 String __sJT_2999 = fields.getField("qNameMatch").getData().toUpperCase();
 String __sJT_3000 = fields.getField("qSignage").getData().toUpperCase();
 String __sJT_3001 = fields.getField("qHoursPosted").getData().toUpperCase();
 String __sJT_3002 = fields.getField("qInventoryReview").getData().toUpperCase();
 String __sJT_3003 = fields.getField("qInventoryConsistent").getData().toUpperCase();
 String __sJT_3004 = fields.getField("qInventoryAdequate").getData().toUpperCase();
 String __sJT_3005 = fields.getField("fulfillmentHouse").getData().toUpperCase();
 String __sJT_3006 = fields.getField("securitySoftware").getData().toUpperCase();
 String __sJT_3007 = fields.getField("inventoryAddr").getData();
 String __sJT_3008 = fields.getField("inventoryCity").getData();
 String __sJT_3009 = fields.getField("inventoryState").getData();
 String __sJT_3010 = fields.getField("inventoryZip").getData();
 String __sJT_3011 = fields.getField("fulfillmentAddr").getData();
 String __sJT_3012 = fields.getField("fulfillmentCity").getData();
 String __sJT_3013 = fields.getField("fulfillmentState").getData();
 String __sJT_3014 = fields.getField("fulfillmentZip").getData();
 String __sJT_3015 = fields.getField("locationAddr").getData();
 String __sJT_3016 = fields.getField("locationCity").getData();
 String __sJT_3017 = fields.getField("locationState").getData();
 String __sJT_3018 = fields.getField("locationZip").getData();
 String __sJT_3019 = fields.getField("inventoryValue").getData();
 String __sJT_3020 = fields.getField("fulfillmentName").getData();
 String __sJT_3021 = fields.getField("employeeCount").getData();
 String __sJT_3022 = fields.getField("locationType").getData();
 String __sJT_3023 = fields.getField("locationDesc").getData();
 String __sJT_3024 = fields.getField("locationAddrType").getData();
 String __sJT_3025 = fields.getField("securitySoftwareVendor").getData();
   String theSqlTS = "insert into siteinspection\n        (\n          app_seq_num,\n          siteinsp_comment,\n          siteinsp_name_flag,\n          siteinsp_inv_sign_flag,\n          siteinsp_bus_hours_flag,\n          siteinsp_inv_viewed_flag,\n          siteinsp_inv_consistant_flag,\n          siteinsp_vol_flag,\n          siteinsp_full_flag,\n          siteinsp_soft_flag,\n          siteinsp_inv_street,\n          siteinsp_inv_city,\n          siteinsp_inv_state,\n          siteinsp_inv_zip,\n          siteinsp_full_street,\n          siteinsp_full_city,\n          siteinsp_full_state,\n          siteinsp_full_zip,\n          siteinsp_bus_street,\n          siteinsp_bus_city,\n          siteinsp_bus_state,\n          siteinsp_bus_zip,\n          siteinsp_inv_value,\n          siteinsp_full_name,\n          siteinsp_no_of_emp,\n          siteinsp_bus_loc,\n          siteinsp_bus_loc_comment,\n          siteinsp_bus_address,\n          siteinsp_soft_name\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n           :16 ,\n           :17 ,\n           :18 ,\n           :19 ,\n           :20 ,\n           :21 ,\n           :22 ,\n           :23 ,\n           :24 ,\n           :25 ,\n           :26 ,\n           :27 ,\n           :28 ,\n           :29 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.app.mtwest.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,__sJT_2998);
   __sJT_st.setString(3,__sJT_2999);
   __sJT_st.setString(4,__sJT_3000);
   __sJT_st.setString(5,__sJT_3001);
   __sJT_st.setString(6,__sJT_3002);
   __sJT_st.setString(7,__sJT_3003);
   __sJT_st.setString(8,__sJT_3004);
   __sJT_st.setString(9,__sJT_3005);
   __sJT_st.setString(10,__sJT_3006);
   __sJT_st.setString(11,__sJT_3007);
   __sJT_st.setString(12,__sJT_3008);
   __sJT_st.setString(13,__sJT_3009);
   __sJT_st.setString(14,__sJT_3010);
   __sJT_st.setString(15,__sJT_3011);
   __sJT_st.setString(16,__sJT_3012);
   __sJT_st.setString(17,__sJT_3013);
   __sJT_st.setString(18,__sJT_3014);
   __sJT_st.setString(19,__sJT_3015);
   __sJT_st.setString(20,__sJT_3016);
   __sJT_st.setString(21,__sJT_3017);
   __sJT_st.setString(22,__sJT_3018);
   __sJT_st.setString(23,__sJT_3019);
   __sJT_st.setString(24,__sJT_3020);
   __sJT_st.setString(25,__sJT_3021);
   __sJT_st.setString(26,__sJT_3022);
   __sJT_st.setString(27,__sJT_3023);
   __sJT_st.setString(28,__sJT_3024);
   __sJT_st.setString(29,__sJT_3025);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1884^7*/
    }
    catch(Exception e)
    {
      logEntry( "storeSiteInspection()", e.toString());
      addError( "storeSiteInspection: " + e.toString());
    }
  }

  /*
  ** public void storeData()
  **
  ** Stores all field data.
  */
  public void storeData()
  {
    try
    {
      storeTransactionFees();
      storeMiscFees();
      storeEquipmentFees();
      storeSiteInspection();
      markPageComplete();
    }
    catch( Exception e )
    {
      logEntry("storeData()",e.toString());
    }
  }

  public String[][] getQuestionList( )
  {
    return( QuestionFields );
  }

  public Vector getEquipmentFees()
  {
    return( EquipmentFees );
  }
}/*@lineinfo:generated-code*/