/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/storefront/Business.sqlj $

  Description:
  
  Business
  
  Banner Bank online application merchant information page bean.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/25/04 1:53p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.netsuite;

import javax.servlet.http.HttpServletRequest;
import com.mes.app.VirtualAppBusinessBase;
import com.mes.constants.mesConstants;
import com.mes.forms.HiddenField;

public class Business extends VirtualAppBusinessBase
{
  {
    appType     = mesConstants.APP_TYPE_NETSUITE_2;
    curScreenId = 1;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // create new field for netSuiteAcctNum
      fields.add(new HiddenField("netSuiteAcctNum"));
      
      setData("productType", Integer.toString(mesConstants.POS_INTERNET));
      setData("internetType", Integer.toString(mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK));
      
      // set to default netsuite association
      setData("assocNum", "600140");
      
    }
    catch(Exception e)
    {
      logEntry("createFields()-netsuite\\Business", e.toString());
    }
  }
  
  protected boolean submitAppData()
  {
    boolean submitOk = false;

    try
    {
      connect();
      submitOk = super.submitAppData();
    
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      Pricing merchPricing = new Pricing(appSeqNum);

      submitOk = merchPricing.submitAppData();
      
      // add netSuiteAcctNum to database
      /*@lineinfo:generated-code*//*@lineinfo:85^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     net_suite_acct_num = :getData("netSuiteAcctNum")
//          where   app_seq_num = :getData("appSeqNum")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3032 = getData("netSuiteAcctNum");
 String __sJT_3033 = getData("appSeqNum");
   String theSqlTS = "update  merchant\n        set     net_suite_acct_num =  :1 \n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.app.netsuite.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3032);
   __sJT_st.setString(2,__sJT_3033);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:90^7*/
    }
    catch(Exception e)
    {
      logEntry("submitAppData(" + fields.getData("appSeqNum") + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  
    return submitOk;
  }
}/*@lineinfo:generated-code*/