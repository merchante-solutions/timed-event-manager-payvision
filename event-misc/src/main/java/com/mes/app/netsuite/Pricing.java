/*@lineinfo:filename=Pricing*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/storefront/Pricing.sqlj $

  Description:

  Pricing

  Banner Bank online app pricing page bean.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/07/04 3:36p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.netsuite;

import com.mes.app.VirtualAppPricingBase;
import com.mes.constants.mesConstants;

public class Pricing extends VirtualAppPricingBase
{
  public Pricing(long appSeqNum)
  {
    super(appSeqNum, mesConstants.APP_TYPE_NETSUITE_2);
  }
}/*@lineinfo:generated-code*/