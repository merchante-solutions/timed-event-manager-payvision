/*@lineinfo:filename=EquipmentDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/EquipmentDataBean.sqlj $

  Description:


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-09-19 11:13:32 -0700 (Wed, 19 Sep 2007) $
  Version            : $Revision: 14159 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.CheckboxField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.forms.ZipField;
import com.mes.support.NumberFormatter;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class EquipmentDataBean extends AppDataBeanBase
{
  private static final int          ER_NONE         = -1;
  private static final int          ER_USER         = 0;

  protected static final String[][] AmPmRadioButtons =
  {
    { "AM","AM" },
    { "PM","PM" }
  };

  protected static final String[][] TrainingTypeRadioButtons =
  {
    { "MES Help Desk",  "M"  },
    { "Sales/Bank Rep", "S"  },
    { "No Training",    "N"  }
  };

  protected static final String[][] CommTypeRadioButtons =
  {
    { "Dial", Integer.toString(mesConstants.TERM_COMM_TYPE_DIAL)  },
    { "IP",   Integer.toString(mesConstants.TERM_COMM_TYPE_IP)    },
    { "Wireless",   Integer.toString(mesConstants.TERM_COMM_TYPE_WIRELESS)}
  };

  protected static final String[] AutoCloseErrorMessages =
  {
    "Please provide the hour(HH), minute(MM), and AM/PM of when the auto batch close will occur",
    "Please check the Auto Batch Close check box or delete the entered hour(HH) and minute(MM)"
  };

  protected static final String[] TerminalReminderErrorMessages =
  {
    "Please provide the hour(HH), minute(MM), and AM/PM of Terminal Reminder to Check Totals",
    "Please check the Terminal Reminder to Check Totals check box or delete the entered hour(HH) and minute(MM)"
  };

  private static final String[] EquipmentFieldNames =
  {
    "peripheral",     // mesConstants.APP_EQUIP_TYPE_PERIPHERAL
    "terminal",       // mesConstants.APP_EQUIP_TYPE_TERMINAL
    "printer",        // mesConstants.APP_EQUIP_TYPE_PRINTER
    "pinPad",         // mesConstants.APP_EQUIP_TYPE_PINPAD
    "imprinter",      // mesConstants.APP_EQUIP_TYPE_IMPRINTER
    "termPrint",      // mesConstants.APP_EQUIP_TYPE_TERM_PRINTER
    "termPinPad",     // mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD
    null,             // mesConstants.APP_EQUIP_TYPE_OTHERS
    null,             // mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE
    null,             // mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES
    null              // mesConstants.APP_EQUIP_TYPE_ACCESSORIES
  };

  // sorted version of the above with the label visible
  // to the HTML browser
  public static final String[][] EquipSelectDisplay =
  {
    { "termPrint",  "Terminal/Printer Sets"  },       // mesConstants.APP_EQUIP_TYPE_TERM_PRINTER
    { "termPinPad", "Terminal/Printer/Pinpad Sets"  },// mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD
    { "terminal",   "Terminals"  },                   // mesConstants.APP_EQUIP_TYPE_TERMINAL
    { "printer",    "Printers"  },                    // mesConstants.APP_EQUIP_TYPE_PRINTER
    { "pinPad",     "Pinpads"  },                     // mesConstants.APP_EQUIP_TYPE_PINPAD
    { "peripheral", "Peripherals"  },                 // mesConstants.APP_EQUIP_TYPE_PERIPHERAL
    { "imprinter",  "Imprinters"  },                  // mesConstants.APP_EQUIP_TYPE_IMPRINTER
  };

  //**************************************************************************
  // field storage classes
  //**************************************************************************
  protected class EquipmentSelectionTable extends DropDownTable
  {
    public EquipmentSelectionTable( int appType, int equipType, int equipSection )
      throws java.sql.SQLException
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;

      try
      {
        connect();
        addElement("","select one");
        /*@lineinfo:generated-code*//*@lineinfo:118^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode( prod_option_id,
//                            null, equip_model,
//                            ( equip_model || '*' || prod_option_id )
//                          )                               as equip_model,
//                    equip_description                     as equip_desc
//            from    equipment_application
//            where   app_type      = :appType and
//                    equip_type    = :equipType and
//                    equip_section = :equipSection
//            order by equip_type,
//                     equip_sort_order asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode( prod_option_id,\n                          null, equip_model,\n                          ( equip_model || '*' || prod_option_id )\n                        )                               as equip_model,\n                  equip_description                     as equip_desc\n          from    equipment_application\n          where   app_type      =  :1  and\n                  equip_type    =  :2  and\n                  equip_section =  :3 \n          order by equip_type,\n                   equip_sort_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   __sJT_st.setInt(2,equipType);
   __sJT_st.setInt(3,equipSection);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.EquipmentDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:131^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }

  protected class LeaseCompanyTable extends DropDownTable
  {
    public LeaseCompanyTable()
      throws java.sql.SQLException
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;

      try
      {
        connect();
        addElement("","select one");
        /*@lineinfo:generated-code*//*@lineinfo:161^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  lease_company_code      as lease_code,
//                    lease_company_desc      as lease_desc
//            from    equip_lease_company
//            order by  lease_company_desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  lease_company_code      as lease_code,\n                  lease_company_desc      as lease_desc\n          from    equip_lease_company\n          order by  lease_company_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.EquipmentDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.EquipmentDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:167^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }

  protected class LeaseMonthsTable extends DropDownTable
  {
    public LeaseMonthsTable( )
    {
      addElement("","select one");
      addElement("12","12 months");
      addElement("24","24 months");
      addElement("36","36 months");
      addElement("48","48 months");
    }
  }

  protected class ShippingAddressTable extends DropDownTable
  {
    public ShippingAddressTable( )
    {
      addElement( Integer.toString(mesConstants.ADDR_TYPE_NONE),     "None" );
      addElement( Integer.toString(mesConstants.ADDR_TYPE_BUSINESS), "Use Business Address" );
      addElement( Integer.toString(mesConstants.ADDR_TYPE_MAILING),  "Use Mailing Address" );
      addElement( Integer.toString(mesConstants.ADDR_TYPE_SHIPPING), "Use Other Address" );
    }
  }

  protected class ShippingMethodTable extends DropDownTable
  {
    public ShippingMethodTable( long appSeqNum )
    {
      ResultSetIterator   it                = null;
      ResultSet           rs                = null;
      String              shippingMethod    = null;

      try
      {
        connect();

        try
        {
          // get the shipping method from the application
          /*@lineinfo:generated-code*//*@lineinfo:223^11*/

//  ************************************************************
//  #sql [Ctx] { select  pf.shipping_method 
//              from    pos_features  pf
//              where   pf.app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pf.shipping_method  \n            from    pos_features  pf\n            where   pf.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.EquipmentDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   shippingMethod = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:228^11*/
        }
        catch( Exception ee )
        {
          // ignore, shipping method not set yet
        }

        /*@lineinfo:generated-code*//*@lineinfo:235^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  code,
//                    description
//            from    shipping_methods
//            where   trunc(nvl( ( select  app.app_created_date
//                                 from    application app
//                                 where   app.app_seq_num(+) = :appSeqNum ),
//                               sysdate
//                             )
//                         ) between from_date and to_date or
//                    code = nvl(:shippingMethod,'-1')
//            order by display_order, from_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  code,\n                  description\n          from    shipping_methods\n          where   trunc(nvl( ( select  app.app_created_date\n                               from    application app\n                               where   app.app_seq_num(+) =  :1  ),\n                             sysdate\n                           )\n                       ) between from_date and to_date or\n                  code = nvl( :2 ,'-1')\n          order by display_order, from_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,shippingMethod);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.EquipmentDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:248^9*/
        rs = it.getResultSet();

        while( rs.next() )
        {
          addElement(rs);
        }
        rs.close();
      }
      catch( Exception e )
      {
        logEntry("ShippingMethodTable()",e.toString());
      }
      finally
      {
        try{ it.close(); } catch( Exception e ){}
        cleanUp();
      }
    }
  }

  //**************************************************************************
  // Validations
  //**************************************************************************
  protected class EquipCompatibilityValidation
    extends SQLJConnectionBase implements Validation
  {
    private String                    ErrorMessage        = null;

    public EquipCompatibilityValidation( )
    {
    }

    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      long        appSeqNum         = 0L;
      String      equipModel        = null;
      int         index             = 0;
      int         recCount          = 0;
      int         supported         = 0;

      // reset the error message content
      ErrorMessage = null;

      try
      {
        connect();

        if ( !isBlank(fdata) )
        {
          // get the appSeqNum for the current app
          appSeqNum = getAppSeqNum();

          // extract the model name from the field.  if necessary
          // separate the model from the product id
          if ( (index = fdata.indexOf("*")) != -1 )
          {
            // separate the model from the product code
            equipModel  = fdata.substring(0,index);
          }
          else
          {
            equipModel  = fdata;
          }

          /*@lineinfo:generated-code*//*@lineinfo:318^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(mpo.app_seq_num) 
//              from    merchpayoption    mpo
//              where   mpo.app_seq_num = :appSeqNum and
//                      mpo.cardtype_code = :mesConstants.APP_CT_CHECK_AUTH and
//                      mpo.merchpo_provider_name in
//                      (
//                        'CertegyWelcomeCheck',
//                        'CertegyECheck'
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mpo.app_seq_num)  \n            from    merchpayoption    mpo\n            where   mpo.app_seq_num =  :1  and\n                    mpo.cardtype_code =  :2  and\n                    mpo.merchpo_provider_name in\n                    (\n                      'CertegyWelcomeCheck',\n                      'CertegyECheck'\n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.EquipmentDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_CHECK_AUTH);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:329^11*/

          // Certegy programs only support certain equipment
          if ( recCount > 0 )
          {
            supported = 0;

            /*@lineinfo:generated-code*//*@lineinfo:336^13*/

//  ************************************************************
//  #sql [Ctx] { select  decode( nvl(eq.certegy_check_supported,'N'),
//                                'Y', 1, 0 )
//                
//                from    equipment   eq
//                where   eq.equip_model = :equipModel
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode( nvl(eq.certegy_check_supported,'N'),\n                              'Y', 1, 0 )\n               \n              from    equipment   eq\n              where   eq.equip_model =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.EquipmentDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,equipModel);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   supported = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:343^13*/

            if ( supported == 0 )
            {
              ErrorMessage = "Check provider does not support this type of equipment";
            }
          }

          if ( ErrorMessage == null )
          {
            /*@lineinfo:generated-code*//*@lineinfo:353^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(mpo.app_seq_num) 
//                from    merchpayoption    mpo
//                where   mpo.app_seq_num = :appSeqNum and
//                        mpo.cardtype_code = :mesConstants.APP_CT_VALUTEC_GIFT_CARD
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mpo.app_seq_num)  \n              from    merchpayoption    mpo\n              where   mpo.app_seq_num =  :1  and\n                      mpo.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.EquipmentDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VALUTEC_GIFT_CARD);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:359^13*/

            // valutec only supports certain equipment
            if ( recCount > 0 )
            {
              supported = 0;

              /*@lineinfo:generated-code*//*@lineinfo:366^15*/

//  ************************************************************
//  #sql [Ctx] { select  decode( nvl(eq.valutec_gift_card_supported,'N'),
//                                  'Y', 1, 0 )
//                  
//                  from    equipment   eq
//                  where   eq.equip_model = :equipModel
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode( nvl(eq.valutec_gift_card_supported,'N'),\n                                'Y', 1, 0 )\n                 \n                from    equipment   eq\n                where   eq.equip_model =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.EquipmentDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,equipModel);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   supported = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:373^15*/

              if ( supported == 0 )
              {
                ErrorMessage = "Valutec Gift Card program does not support this type of equipment";
              }
            }
          }
        }
      }
      catch( Exception e )
      {
        ErrorMessage = e.toString();
      }
      finally
      {
        cleanUp();
      }

      return( (ErrorMessage == null) );
    }
  }

  protected class ActionTimeValidation implements Validation
  {
    // error indexes
    private static final int          ER_NEED_DATA    = (ER_USER + 0);
    private static final int          ER_REMOVE_DATA  = (ER_USER + 1);

    private int                       ErrorIndex      = ER_NONE;
    private String[]                  ErrorMessages   = null;
    private Field[]                   TimeFields      = null;

    public ActionTimeValidation( Field[] fields, String[] errorMsgs )
    {
      ErrorMessages   = errorMsgs;
      TimeFields      = fields;
    }

    public String getErrorText()
    {
      return( (ErrorIndex == ER_NONE) ? "" : ErrorMessages[ErrorIndex] );
    }

    public boolean validate(String fdata)
    {
      boolean         shouldBeBlank   = true;

      // set the flag based on the checkbox value
      if ( fdata != null && fdata.toUpperCase().equals("Y") )
      {
        shouldBeBlank = false;
      }

      // reset the error message content
      ErrorIndex = ER_NONE;

      // scan through the fields and make sure that they
      // are full/empty based on the checkbox state.
      for( int i = 0; i < TimeFields.length; ++i )
      {
        if ( TimeFields[i].isBlank() != shouldBeBlank )
        {
          if( shouldBeBlank == true )
          {
            ErrorIndex = ER_REMOVE_DATA;
          }
          else    // needs data
          {
            ErrorIndex = ER_NEED_DATA;
          }
          break;
        }
      }
      return( ErrorIndex == ER_NONE );
    }
  }

  protected class EquipCountValidation implements Validation
  {
    // error indexes
    private static final int          ER_NEED_COUNTS      = (ER_USER + 0);
    private static final int          ER_REMOVE_COUNTS    = (ER_USER + 1);

    private Field[]                   CountFields         = null;
    int                               ErrorIndex          = ER_NONE;
    private String[]                  ErrorMessages       = null;

    public EquipCountValidation( Field[] fields, String[] errorMsgs )
    {
      CountFields   = fields;
      ErrorMessages = errorMsgs;
    }

    public String getErrorText()
    {
      return( (ErrorIndex == ER_NONE) ? "" : ErrorMessages[ErrorIndex] );
    }

    private boolean hasCountFieldsWithData( )
    {
      boolean   retVal = false;

      for( int i = 0; i < CountFields.length; ++i )
      {
        if ( !CountFields[i].isBlank() && CountFields[i].asInteger() > 0 )
        {
          retVal = true;
          break;
        }
      }

      return( retVal );
    }

    public boolean validate(String fdata)
    {
      boolean           hasCounts     = hasCountFieldsWithData();

      ErrorIndex = ER_NONE;

      if ( isBlank(fdata) )
      {
        if ( hasCounts == true )
        {
          ErrorIndex = ER_REMOVE_COUNTS;
        }
      }
      else    // has data, needs counts
      {
        if ( hasCounts == false )
        {
          ErrorIndex = ER_NEED_COUNTS;
        }
      }
      return( ErrorIndex == ER_NONE );
    }
  }

  protected class ShippingAddressValidation implements Validation
  {
    private Field[]                   ShippingAddrFields  = null;
    private String                    ErrorMessage        = null;

    public ShippingAddressValidation( Field[] fields )
    {
      ShippingAddrFields = fields;
    }

    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      int             iData     = -1;

      // reset the error message content
      ErrorMessage = null;

      try
      {
        // value should be an integer, so extract it
        iData = Integer.parseInt(fdata);
      }
      catch( Exception e )
      {
        ErrorMessage = "Please select a valid shipping address type";
      }

      if ( ErrorMessage == null )
      {
        if ( iData == mesConstants.ADDR_TYPE_SHIPPING )
        {
          // scan through the fields and make sure that they have data
          for( int i = 0; i < ShippingAddrFields.length; ++i )
          {
            if ( ShippingAddrFields[i].isBlank() )
            {
              ErrorMessage = "Please provide the shipping address details or select a pre-defined shipping address";
              break;
            }
          }
        }
      }
      return( ErrorMessage == null );
    }
  }

  //**************************************************************************
  // EquipmentDataBean
  //**************************************************************************
  public EquipmentDataBean()
  {
  }

  protected String decodeActionTime( String fieldBaseName )
  {
    StringBuffer  retVal      = new StringBuffer();

    try
    {
      retVal.append( fields.getField( fieldBaseName + "Hour" ).getData() );
      retVal.append( ":" );
      retVal.append( NumberFormatter.getLongString( fields.getField( fieldBaseName + "Min" ).asLong(), "00" ) );
      retVal.append(" ");
      retVal.append( fields.getField( fieldBaseName + "AmPm" ).getData() );
    }
    catch(Exception e)
    {
      logEntry("decodeActionTime()",e.toString());
    }
    return( retVal.toString() );
  }

  public void getIndustryType( )
  {
    int           industryType        = 0;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:595^7*/

//  ************************************************************
//  #sql [Ctx] { select  industype_code 
//          from    merchant
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  industype_code  \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.EquipmentDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   industryType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:600^7*/
    }
    catch(Exception e)
    {
      logEntry("getIndustryType()", e.toString());
    }
  }

  protected int getLeaseCountTotal()
  {
    Field   field         = null;
    int     retVal        =  0;

    for( int i = 0; i < EquipmentFieldNames.length; ++i )
    {
      if ( EquipmentFieldNames[i] != null )
      {
        field = fields.getField( EquipmentFieldNames[i] + "LeaseCount" );
        if( field != null && !field.isBlank() )
        {
          retVal += field.asInteger();
        }
      }
    }

    return( retVal );
  }

  public void init()
  {
    Field                 field           = null;
    String                fieldName       = null;

    super.init();

    try
    {
      for( int etype = 0; etype < mesConstants.APP_EQUIP_TYPE_COUNT; ++etype )
      {
        fieldName = EquipmentFieldNames[etype];

        // some fields to do not support drop down in the needed
        // section of the equipment page because they do not support
        // selection.  for example, if the user selected POS Partner 2000
        // on the first page, there is no need to show the equipment page.
        if ( fieldName != null )
        {
          // make a drop down field with the equipment models available for
          // this type of equipment for this application type.
          fields.add( new NumberField( (fieldName + "RentalCount"),3,2,true,0 ) );
          fields.add( new NumberField( (fieldName + "PurchaseCount"),3,2,true,0 ) );
          fields.add( new NumberField( (fieldName + "LeaseCount"),3,2,true,0 ) );
          field = new DropDownField( (fieldName + "NeededModels"), new EquipmentSelectionTable( AppType, etype, mesConstants.APP_ES_NEEDED ), true );
          field.addValidation( new EquipCountValidation( new Field[]
                                                        {
                                                          fields.getField( (fieldName + "RentalCount") ),
                                                          fields.getField( (fieldName + "PurchaseCount") ),
                                                          fields.getField( (fieldName + "LeaseCount") )
                                                        },
                                                        new String[]
                                                        {
                                                          "Please provide a number of items to buy, rent or lease OR change equipment selection",
                                                          "Please select a model of equipment to buy, rent or lease OR remove the quantity"
                                                        }  ) );
          if ( fieldName.equals("termPrint") ||
               fieldName.equals("termPinPad") ||
               fieldName.equals("terminal") )
          {
            field.addValidation( new EquipCompatibilityValidation() );
          }
          fields.add(field);


          // setup the owned equipment selections.  note that passing -1
          // as the application type will return the "all equipment type"
          fields.add( new NumberField( (fieldName + "OwnedCount"),3,2,true,0 ) );
          field = new DropDownField( (fieldName + "OwnedModels"), new EquipmentSelectionTable( mesConstants.APP_TYPE_ALL_EQUIP, etype, mesConstants.APP_ES_OWNED ), true );
          field.addValidation( new EquipCountValidation( new Field[]
                                                        {
                                                          fields.getField( (fieldName + "OwnedCount") )
                                                        },
                                                        new String[]
                                                        {
                                                          "Please provide a number of items owned OR change the owned equipment selection",
                                                          "Please select the model of owned equipment OR remove the owned quantity"
                                                        }  ) );

          if ( fieldName.equals("termPrint") ||
               fieldName.equals("termPinPad") ||
               fieldName.equals("terminal") )
          {
            field.addValidation( new EquipCompatibilityValidation() );
          }
          fields.add(field);
        }
      }

      // Lease Company Details
      fields.add( new DropDownField( "leaseCompany", new LeaseCompanyTable(), true ) );
      fields.add( new Field("leaseAcctNum", 8, 9, true) );
      fields.add( new Field("leaseAuthNum", 8, 9, true) );
      fields.add( new DropDownField( "leaseMonths", new LeaseMonthsTable(), true ) );
      fields.add( new NumberField( "leaseMonthlyAmount", 10, 13, true, 2 ) );
      fields.add( new NumberField( "leaseFundingAmount", 10, 13, true, 2 ) );

      //*********************
      // Terminal Features
      //*********************

      // Access Code
      fields.add( new CheckboxField( "accessCodeEnable", "Access Code (# to get outside line)", false ) );
      field = new Field( "accessCode", 10, 5, true );
      field.addValidation( new IfYesNotBlankValidation( fields.getField("accessCodeEnable"), "Please provide a valid Access Code" ) );
      field.addValidation( new IfNoBlankValidation( fields.getField("accessCodeEnable"), "Please check the Access Code Check Box or delete the entered Access Code" ) );
      fields.add( field );

      // Auto Close feature
      fields.add( new NumberField("autoCloseHour",2,2,true,0) );
      fields.add( new NumberField("autoCloseMin",2,2,true,0) );
      fields.add( new RadioButtonField( "autoCloseAmPm", AmPmRadioButtons, -1, true, "Please select either AM or PM for the auto batch close" ) );
      field = new CheckboxField( "autoCloseEnable", "Auto Batch Close", false );
      field.addValidation( new ActionTimeValidation( new Field[]
                                                    { fields.getField("autoCloseHour"),
                                                      fields.getField("autoCloseMin"),
                                                      fields.getField("autoCloseAmPm") },
                                                     AutoCloseErrorMessages ) );
      fields.add(field);

      // Receipt Header Line 4
      field = new Field("receiptLine4",25,30,true);
      if(isDialPayApp())
      {
        // create receipt line 4 check box as Unchecked
        fields.add( new CheckboxField("receiptLine4Enable","Receipt Header Line 4",false) );
      }
      else
      {
        fields.add( new CheckboxField("receiptLine4Enable","Receipt Header Line 4",true) );
        field.setData( loadBusinessPhoneString() );     // set the default
      }
      field.addValidation( new IfYesNotBlankValidation( fields.getField("receiptLine4Enable"),"Please provide Line 4 for the Receipt Header" ) );
      field.addValidation( new IfNoBlankValidation( fields.getField("receiptLine4Enable"),"Please check the Receipt Header Line 4 check box or delete the entered Line 4" ) );
      fields.add(field);

      // Receipt Header Line 5
      fields.add( new CheckboxField("receiptLine5Enable","Receipt Header Line 5",false) );
      field = new Field("receiptLine5",25,30,true);
      field.addValidation( new IfYesNotBlankValidation( fields.getField("receiptLine5Enable"),"Please provide Line 5 for the Receipt Header" ) );
      field.addValidation( new IfNoBlankValidation( fields.getField("receiptLine5Enable"),"Please check the Receipt Header Line 5 check box or delete the entered Line 5" ) );
      fields.add(field);

      // Receipt Footer
      fields.add( new CheckboxField("receiptFooterEnable","Receipt Footer",false) );
      field = new Field("receiptFooter",25,30,true);
      field.addValidation( new IfYesNotBlankValidation( fields.getField("receiptFooterEnable"),"Please provide a Receipt Footer" ) );
      field.addValidation( new IfNoBlankValidation( fields.getField("receiptFooterEnable"),"Please check the Receipt Footer check box or delete the entered Footer" ) );
      fields.add(field);

      // misc checkbox fields
      fields.add( new CheckboxField("resetTranNumEnable","Reset Transaction # Daily",false) );
      fields.add( new CheckboxField("invoicePromptEnable","Invoice # Prompt On",false) );
      fields.add( new CheckboxField("fraudControlEnable","Fraud Control On (last 4 digits of card)",false) );
      fields.add( new CheckboxField("passwordProtectEnable","Password Protect On",false) );
      fields.add( new CheckboxField("purchasingCardEnable","Purchasing Card Flag On",false) );
      fields.add( new CheckboxField("avsEnable","AVS On",false) );
      fields.add( new CheckboxField("cardTruncDisable","Card Truncation Off on Receipts",false) );
      fields.add( new CheckboxField("callWaitingFlag","Call Waiting",false) );
      fields.add( new CheckboxField("cvv2Flag","V Code (CVV2)",false) );

      // customer service phone
      fields.add( new PhoneField( "customerServicePhone", true ) );

      // debit flags
      fields.add( new CheckboxField("debitCashBackEnable","Cash Back with Debit",false) );
      field = new CurrencyField("debitCashBackLimit",10,5,true);
      field.addValidation( new IfYesNotBlankValidation( fields.getField("debitCashBackEnable"), "Please enter a valid cash back limit" ) );
      field.addValidation( new IfNoBlankValidation( fields.getField("debitCashBackEnable"), "Please select cash back with debit checkbox or delete cash back limit" ) );
      fields.add(field);
      fields.add( new CheckboxField("debitSurchargeEnable","Debit Surcharge", false) );
      field = new CurrencyField("debitSurchargeAmount",10,5,true);
      field.addValidation( new IfYesNotBlankValidation( fields.getField("debitSurchargeEnable"), "Please enter a valid debit surcharge amount" ) );
      field.addValidation( new IfNoBlankValidation( fields.getField("debitSurchargeEnable"), "Please select debit surcharge checkbox or delete debit surcharge amount" ) );
      fields.add(field);

      // Verifone terminal features
      fields.add( new NumberField("reminderHour",2,2,true,0) );
      fields.add( new NumberField("reminderMin",2,2,true,0) );
      fields.add( new RadioButtonField( "reminderAmPm", AmPmRadioButtons, -1, true, "Please select either AM or PM for the totals check reminder close" ) );
      field = new CheckboxField( "reminderEnable", "Terminal Reminder to Check Totals", false );
      field.addValidation( new ActionTimeValidation( new Field[]
                                                    { fields.getField("reminderHour"),
                                                      fields.getField("reminderMin"),
                                                      fields.getField("reminderAmPm") },
                                                     TerminalReminderErrorMessages ) );
      fields.add(field);
      fields.add( new CheckboxField("tipOptionFlag", "Tip Option On", false) );
      fields.add( new CheckboxField("serverPromptEnable", "Clerk/Server Prompt On", false) );

      // phone training
      fields.add( new RadioButtonField( "trainingType", TrainingTypeRadioButtons, -1, true, "Please select a valid training type" ) );

      fields.add( new Field("shippingBusinessName", 32, 27, true) );
      fields.add( new Field("shippingContactName", 32, 27, true) );
      fields.add( new Field("shippingAddr1", 32, 27, true) );
      fields.add( new Field("shippingAddr2", 32, 27, true) );
      fields.add( new Field("shippingCity", 25, 27, true) );
      fields.add(new DropDownField("shippingState",new StateDropDownTable(),true));
      fields.add( new ZipField("shippingZip", true, fields.getField("shippingState")) );
      fields.add( new PhoneField("shippingPhone", true) );
      field = new DropDownField("shippingAddressType", new ShippingAddressTable(),false );
      field.addValidation( new ShippingAddressValidation( new Field[]
                                                          {
                                                            fields.getField("shippingBusinessName"),
                                                            fields.getField("shippingAddr1"),
                                                            fields.getField("shippingCity"),
                                                            fields.getField("shippingState"),
                                                            fields.getField("shippingZip"),
                                                          } ) );
      fields.add( field );

      fields.add( new DropDownField("shippingMethod", new ShippingMethodTable(getAppSeqNum()), false ) );
      fields.add( new TextareaField("shippingComments", 70, 7, 50, true ) );

      fields.add( new NumberField("imprinterPlates", 3, 2, true, 0) );
      fields.add( new TextareaField("equipmentComments", 250, 7, 50, true ) );

      // terminal comm type (dial or ip)
      fields.add( new RadioButtonField( "commType", CommTypeRadioButtons, -1, true, "Please select a valid comm type" ) );

      // the html extra tags for all the fields
      addHtmlExtra("class=\"formFields\"");

      // set special defaults
      setSpecialDefaults();
    }
    catch( Exception e )
    {
      logEntry("init()",e.toString());
    }
  }

  // pos type (set on page 1) determines if certain fee options are present
  protected int posType = -1;

  public boolean isDialPayApp( )
  {
    boolean   dialPay       = false;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:850^7*/

//  ************************************************************
//  #sql [Ctx] { select  pos_code
//          
//          from    merch_pos
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pos_code\n         \n        from    merch_pos\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.EquipmentDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   posType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:856^7*/

      // DialPay or GPS payment solution
      if( posType == mesConstants.APP_MPOS_DIAL_PAY ||
          posType == mesConstants.APP_MPOS_GPS )
      {
        dialPay = true;
      }
    }
    catch( java.sql.SQLException e )
    {
      // app does not exist for this app sequence number
      System.out.println("isDialPayApp(): " + e.toString());
    }
    return( dialPay );
  }

  public String loadBusinessPhoneString( )
  {
    String              retVal      = "";

    try
    {
      if ( AppSeqNum != APP_SEQ_NEW )
      {
        /*@lineinfo:generated-code*//*@lineinfo:881^9*/

//  ************************************************************
//  #sql [Ctx] { select  '(' ||
//                    substr(address_phone,1,3) ||
//                    ')' || ' ' ||
//                    substr(address_phone,4,3) ||
//                    '-' ||
//                    substr(address_phone,7)     
//            from    address
//            where   app_seq_num = :AppSeqNum and
//                    addresstype_code = :mesConstants.ADDR_TYPE_BUSINESS
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  '(' ||\n                  substr(address_phone,1,3) ||\n                  ')' || ' ' ||\n                  substr(address_phone,4,3) ||\n                  '-' ||\n                  substr(address_phone,7)      \n          from    address\n          where   app_seq_num =  :1  and\n                  addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.EquipmentDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_BUSINESS);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:892^9*/
      }
    }
    catch( java.sql.SQLException e )
    {
      // no data found
    }
    return( retVal );
  }

  public void loadData()
  {
    try
    {
      if ( AppSeqNum != APP_SEQ_NEW )
      {
        loadEquipmentData();
        loadFeatureData();
        loadLeaseData();          // load any lease data
        loadShippingAddress();    // must be done after shipping type loaded in loadFeatureData
      }
    }
    catch( Exception e )
    {
      logEntry("loadData()",e.toString());
    }
    finally
    {
    }
  }

  protected void loadEquipmentData( )
  {
    int               equipType             = -1;
    Field             field                 = null;
    int               fieldNameBaseLength   = 0;
    StringBuffer      fieldNameBuffer       = new StringBuffer();
    String            fieldPostfix          = null;
    ResultSetIterator it                    = null;
    ResultSet         resultSet             = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:935^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equiptype_code                as equip_type,
//                  equiplendtype_code            as lend_type,
//                  merchequip_equip_quantity     as equip_count,
//                  decode(prod_option_id,
//                         null,equip_model,
//                         equip_model || '*' || prod_option_id)  as product_id
//          from    merchequipment      me
//          where   me.app_seq_num = :AppSeqNum
//          order by me.equiptype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equiptype_code                as equip_type,\n                equiplendtype_code            as lend_type,\n                merchequip_equip_quantity     as equip_count,\n                decode(prod_option_id,\n                       null,equip_model,\n                       equip_model || '*' || prod_option_id)  as product_id\n        from    merchequipment      me\n        where   me.app_seq_num =  :1 \n        order by me.equiptype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.app.EquipmentDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:946^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        equipType = resultSet.getInt("equip_type");

        // reset the field name data
        fieldNameBuffer.setLength(0);
        fieldPostfix = null;

        switch( equipType )
        {
          case mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES:
            fieldNameBuffer.append("imprinterPlates");
            break;

//          case mesConstants.APP_EQUIP_TYPE_PERIPHERAL:
//          case mesConstants.APP_EQUIP_TYPE_TERMINAL:
//          case mesConstants.APP_EQUIP_TYPE_PRINTER:
//          case mesConstants.APP_EQUIP_TYPE_PINPAD:
//          case mesConstants.APP_EQUIP_TYPE_IMPRINTER:
//          case mesConstants.APP_EQUIP_TYPE_TERM_PRINTER:
//          case mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD:
//          case mesConstants.APP_EQUIP_TYPE_OTHERS:
//          case mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE:
          default:
            // add the base name and store the base length of the field
            fieldNameBuffer.append( EquipmentFieldNames[ equipType ] );
            fieldNameBaseLength = fieldNameBuffer.length();
            fieldPostfix = "NeededModels";    // assume needed

            switch( resultSet.getInt("lend_type" ) )
            {
              case mesConstants.APP_EQUIP_PURCHASE:
                fieldNameBuffer.append( "PurchaseCount" );
                break;

              case mesConstants.APP_EQUIP_BUY_REFURBISHED:
                fieldNameBuffer.append( "RefurbCount" );
                break;

              case mesConstants.APP_EQUIP_RENT:
                fieldNameBuffer.append( "RentalCount" );
                break;

              case mesConstants.APP_EQUIP_LEASE:
                fieldNameBuffer.append( "LeaseCount" );
                break;

              case mesConstants.APP_EQUIP_OWNED:
                fieldNameBuffer.append( "OwnedCount" );
                fieldPostfix = "OwnedModels";     // switch to owned
                break;
            }
            break;
        }
        // extract the field data from the result set
        field = fields.getField( fieldNameBuffer.toString() );
        if ( field != null )
        {
          field.setData( resultSet.getString("equip_count") );
        }

        // setup the drop down box to point to the correct entry.
        if ( fieldPostfix != null )
        {
          // reset to just the field base name
          fieldNameBuffer.setLength( fieldNameBaseLength );

          // add the post fix to the base name
          fieldNameBuffer.append( fieldPostfix );

          // extract the field reference and set the contents if non null
          field = fields.getField( fieldNameBuffer.toString() );
          if ( field != null )
          {
            field.setData( resultSet.getString("product_id") );
          }
        }
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadEquipmentData()",e.toString());
      addError("loadEquipmentData: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }

  protected void loadFeatureData( )
  {
    int                         index         = 0;
    ResultSetIterator           it            = null;
    ResultSet                   resultSet     = null;
    String                      temp          = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1050^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pf.access_code_flag                 as access_code_enable,
//                  pf.access_code                      as access_code,
//                  pf.auto_batch_close_flag            as auto_close_enable,
//                  pf.auto_batch_close_time            as auto_close_time,
//                  pf.header_line4_flag                as receipt_line4_enable,
//                  pf.header_line4                     as receipt_line4,
//                  pf.header_line5_flag                as receipt_line5_enable,
//                  pf.header_line5                     as receipt_line5,
//                  pf.footer_flag                      as receipt_footer_enable,
//                  footer                              as receipt_footer,
//                  pf.reset_ref_flag                   as reset_tran_num_enable,
//                  pf.invoice_prompt_flag              as invoice_prompt_enable,
//                  pf.fraud_control_flag               as fraud_control_enable,
//                  pf.password_protect_flag            as password_protect_enable,
//                  -- only used by CB&T derived app
//                  -- pf.phone_training_flag              as training_enable,
//                  pf.terminal_reminder_flag           as reminder_enable,
//                  pf.terminal_reminder_time           as reminder_time,
//                  pf.avs_flag                         as avs_enable,
//                  pf.phone_training                   as training_type,
//                  pf.cash_back_flag                   as debit_cash_back_enable,
//                  pf.cash_back_limit                  as debit_cash_back_limit,
//                  pf.purchasing_card_flag             as purchasing_card_enable,
//                  pf.card_truncation_flag             as card_trunc_disable,
//                  pf.debit_surcharge_flag             as debit_surcharge_enable,
//                  pf.debit_surcharge_amount           as debit_surcharge_amount,
//                  pf.customer_service_phone           as customer_service_phone,
//                  pf.tip_option_flag                  as tip_option_flag,
//                  pf.clerk_enabled_flag               as server_prompt_enable,
//                  pf.addresstype_code                 as shipping_address_type,
//                  pf.equipment_comment                as equipment_comments,
//                  pf.call_waiting_flag                as call_waiting_flag,
//                  pf.cvv2_flag                        as cvv2_flag,
//                  pf.shipping_comment                 as shipping_comments,
//                  nvl(pf.term_comm_type,:mesConstants.TERM_COMM_TYPE_DIAL) as comm_type,
//                  pf.shipping_method                  as shipping_method
//                  -- where are these used?
//                  --pf.fraud_control_option,
//                  --pf.no_printer_needed,
//                  --pf.no_printer_owned
//          from    pos_features      pf
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pf.access_code_flag                 as access_code_enable,\n                pf.access_code                      as access_code,\n                pf.auto_batch_close_flag            as auto_close_enable,\n                pf.auto_batch_close_time            as auto_close_time,\n                pf.header_line4_flag                as receipt_line4_enable,\n                pf.header_line4                     as receipt_line4,\n                pf.header_line5_flag                as receipt_line5_enable,\n                pf.header_line5                     as receipt_line5,\n                pf.footer_flag                      as receipt_footer_enable,\n                footer                              as receipt_footer,\n                pf.reset_ref_flag                   as reset_tran_num_enable,\n                pf.invoice_prompt_flag              as invoice_prompt_enable,\n                pf.fraud_control_flag               as fraud_control_enable,\n                pf.password_protect_flag            as password_protect_enable,\n                -- only used by CB&T derived app\n                -- pf.phone_training_flag              as training_enable,\n                pf.terminal_reminder_flag           as reminder_enable,\n                pf.terminal_reminder_time           as reminder_time,\n                pf.avs_flag                         as avs_enable,\n                pf.phone_training                   as training_type,\n                pf.cash_back_flag                   as debit_cash_back_enable,\n                pf.cash_back_limit                  as debit_cash_back_limit,\n                pf.purchasing_card_flag             as purchasing_card_enable,\n                pf.card_truncation_flag             as card_trunc_disable,\n                pf.debit_surcharge_flag             as debit_surcharge_enable,\n                pf.debit_surcharge_amount           as debit_surcharge_amount,\n                pf.customer_service_phone           as customer_service_phone,\n                pf.tip_option_flag                  as tip_option_flag,\n                pf.clerk_enabled_flag               as server_prompt_enable,\n                pf.addresstype_code                 as shipping_address_type,\n                pf.equipment_comment                as equipment_comments,\n                pf.call_waiting_flag                as call_waiting_flag,\n                pf.cvv2_flag                        as cvv2_flag,\n                pf.shipping_comment                 as shipping_comments,\n                nvl(pf.term_comm_type, :1 ) as comm_type,\n                pf.shipping_method                  as shipping_method\n                -- where are these used:2\n                --pf.fraud_control_option,\n                --pf.no_printer_needed,\n                --pf.no_printer_owned\n        from    pos_features      pf\n        where   app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.TERM_COMM_TYPE_DIAL);
   __sJT_st.setLong(2,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.app.EquipmentDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1094^7*/
      resultSet = it.getResultSet();
      if ( resultSet.next() )
      {
        setFields(resultSet,false);

        // extract the time fields
        temp = resultSet.getString("auto_close_time");
        if ( isBlank(temp) == false )
        {
          if( temp.length() >= 7 )
          {
            index = temp.indexOf(':');
            fields.getField("autoCloseHour").setData( temp.substring(0,index) );
            fields.getField("autoCloseMin").setData( temp.substring((index+1),(index+3)) );
            fields.getField("autoCloseAmPm").setData( temp.substring((index+4)) );
          }
        }

        temp = resultSet.getString("reminder_time");
        if ( isBlank(temp) == false )
        {
          if( temp.length() >= 7 )
          {
            index = temp.indexOf(':');
            fields.getField("reminderHour").setData( temp.substring(0,index) );
            fields.getField("reminderMin").setData( temp.substring((index+1),(index+3)) );
            fields.getField("reminderAmPm").setData( temp.substring((index+4)) );
          }
        }
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry( "loadFeatureData()", e.toString());
      addError( "loadFeatureData(): " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  protected void loadLeaseData( )
  {
    ResultSetIterator it              = null;
    ResultSet         resultSet       = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1146^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ml.lease_company_code           as lease_company,
//                  ml.lease_account_num            as lease_acct_num,
//                  ml.lease_auth_num               as lease_auth_num,
//                  ml.lease_months_num             as lease_months,
//                  ml.lease_monthly_amount         as lease_monthly_amount,
//                  ml.lease_funding_amount         as lease_funding_amount
//          from    merchequiplease      ml
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ml.lease_company_code           as lease_company,\n                ml.lease_account_num            as lease_acct_num,\n                ml.lease_auth_num               as lease_auth_num,\n                ml.lease_months_num             as lease_months,\n                ml.lease_monthly_amount         as lease_monthly_amount,\n                ml.lease_funding_amount         as lease_funding_amount\n        from    merchequiplease      ml\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.app.EquipmentDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1156^7*/
      resultSet = it.getResultSet();
      if( resultSet.next() )
      {
        setFields(resultSet,false);
      }
      resultSet.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("loadLeaseData()",e.toString());
      addError("loadLeaseData: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  protected void loadShippingAddress( )
  {
    ResultSetIterator           it        = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1182^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(ad.address_name,
//                      mr.merch_business_name) as shipping_business_name,
//                  address_line1               as shipping_addr1,
//                  address_line2               as shipping_addr2,
//                  address_city                as shipping_city,
//                  countrystate_code           as shipping_state,
//                  address_zip                 as shipping_zip,
//                  address_phone               as shipping_phone
//          from    address           ad,
//                  merchant          mr
//          where   ad.app_seq_num = :AppSeqNum and
//                  ad.addresstype_code = :fields.getField("shippingAddressType").asInteger() and
//                  mr.app_seq_num(+) = ad.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2073 = fields.getField("shippingAddressType").asInteger();
  try {
   String theSqlTS = "select  nvl(ad.address_name,\n                    mr.merch_business_name) as shipping_business_name,\n                address_line1               as shipping_addr1,\n                address_line2               as shipping_addr2,\n                address_city                as shipping_city,\n                countrystate_code           as shipping_state,\n                address_zip                 as shipping_zip,\n                address_phone               as shipping_phone\n        from    address           ad,\n                merchant          mr\n        where   ad.app_seq_num =  :1  and\n                ad.addresstype_code =  :2  and\n                mr.app_seq_num(+) = ad.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,__sJT_2073);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.app.EquipmentDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1197^7*/
      setFields(it.getResultSet());

      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadShippingAddress()", e.toString());
      addError("loadShippingAddress: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public void recalculate( )
  {
    // load the appropriate shipping
    // address from the database.
    loadShippingAddress();
  }

  public void storeData( )
  {
    storeEquipmentData();
    storeFeatureData();
    storeLeaseData();
    storeShippingAddress();
    markPageComplete();         // mark this page done in screen_progress
  }

  protected void storeEquipmentData()
  {
    int                   equipCount      = 0;
    String                equipModel      = null;
    Field                 field           = null;
    String                fieldName       = null;
    int                   index           = 0;
    String                productId       = null;
    String                temp            = null;

    try
    {
      // delete all the existing equipment except PC products?
      /*@lineinfo:generated-code*//*@lineinfo:1242^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchequipment
//          where   app_seq_num = :AppSeqNum and
//                  equip_model != 'PCPS'
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merchequipment\n        where   app_seq_num =  :1  and\n                equip_model != 'PCPS'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1249^7*/

      // add new equipment
      for ( int eType = 0; eType < mesConstants.APP_EQUIP_TYPE_COUNT; ++eType )
      {
        // if there is a valid field for this type of equipment,
        // then insert the data if the user selected something
        // from the drop down list of models.
        if ( EquipmentFieldNames[eType] != null )
        {
          for( int appSection = 0; appSection < 2; ++appSection )
          {
            // extract the either the needed or the owned field based
            // on which section of the application data we are currently
            // processing.
            field = fields.getField( EquipmentFieldNames[eType] + (( appSection == 0 ) ? "NeededModels" : "OwnedModels") );

            // if the field is non-null and it is not blank
            // then they have selected a piece of equipment,
            // validation will insure that at lease one of
            // the count fields has a valid value > 0.
            if ( field != null && !field.isBlank() )
            {
              // extract the model name from the field.  if necessary
              // separate the model from the product id
              temp = field.getData();
              if ( (index = temp.indexOf("*")) != -1 )
              {
                // separate the model from the product code
                equipModel  = temp.substring(0,index);
                productId   = temp.substring(index+1);
              }
              else
              {
                equipModel  = temp;
                productId   = null;
              }
              for( int lType = 0; lType < mesConstants.APP_EQUIP_COUNT; ++lType )
              {
                // if we are processing the needed section then skip the
                // owned.  if we are processing the owned section skip
                // everything but the owned.
                if( (appSection == 0 && lType == mesConstants.APP_EQUIP_OWNED) ||
                    (appSection == 1 && lType != mesConstants.APP_EQUIP_OWNED) )
                {
                  continue;
                }

                switch( lType )
                {
                  case mesConstants.APP_EQUIP_PURCHASE:
                    fieldName = EquipmentFieldNames[eType] + "PurchaseCount";
                    break;
                  case mesConstants.APP_EQUIP_RENT:
                    fieldName = EquipmentFieldNames[eType] + "RentalCount";
                    break;
                  case mesConstants.APP_EQUIP_LEASE:
                    fieldName = EquipmentFieldNames[eType] + "LeaseCount";
                    break;
                  case mesConstants.APP_EQUIP_BUY_REFURBISHED:
                    fieldName = EquipmentFieldNames[eType] + "RefurbCount";
                    break;
                  case mesConstants.APP_EQUIP_OWNED:
                    fieldName = EquipmentFieldNames[eType] + "OwnedCount";
                    break;
                  default:
                    fieldName = "";   // reset the field name
                    break;
                }
                field = fields.getField(fieldName);

                // found the field
                if ( field != null && !field.isBlank() )
                {
                  equipCount = field.asInteger();
                  if ( equipCount > 0 )
                  {
                    /*@lineinfo:generated-code*//*@lineinfo:1326^21*/

//  ************************************************************
//  #sql [Ctx] { insert into merchequipment
//                        (
//                          app_seq_num,
//                          equiptype_code,
//                          equiplendtype_code,
//                          merchequip_amount,
//                          equip_model,
//                          merchequip_equip_quantity,
//                          prod_option_id,
//                          quantity_deployed
//                        )
//                        values
//                        (
//                          :AppSeqNum,
//                          :eType,
//                          :lType,
//                          0,
//                          :equipModel,
//                          :equipCount,
//                          :productId,
//                          0
//                        )
//                       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchequipment\n                      (\n                        app_seq_num,\n                        equiptype_code,\n                        equiplendtype_code,\n                        merchequip_amount,\n                        equip_model,\n                        merchequip_equip_quantity,\n                        prod_option_id,\n                        quantity_deployed\n                      )\n                      values\n                      (\n                         :1 ,\n                         :2 ,\n                         :3 ,\n                        0,\n                         :4 ,\n                         :5 ,\n                         :6 ,\n                        0\n                      )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,eType);
   __sJT_st.setInt(3,lType);
   __sJT_st.setString(4,equipModel);
   __sJT_st.setInt(5,equipCount);
   __sJT_st.setString(6,productId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1350^21*/
                  }
                }
              }
            }     // end if equipment selection field is null or blank
          }     // end for loop through app sections (0 = needed, 1 = owned)
        }     // end if EquipmentFieldName[etype] != null
      }     // end for loop through equipment types

      // add imprinter plates
      if(!getData("imprinterPlates").equals("") && Integer.parseInt(getData("imprinterPlates")) > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:1362^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchequipment
//            (
//              app_seq_num,
//              equiptype_code,
//              equiplendtype_code,
//              merchequip_amount,
//              equip_model,
//              merchequip_equip_quantity,
//              quantity_deployed
//            )
//            values
//            (
//              :AppSeqNum,
//              :mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES,
//              :mesConstants.APP_EQUIP_PURCHASE,
//              0,
//              'IPPL',
//              :getData("imprinterPlates"),
//              0
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2074 = getData("imprinterPlates");
   String theSqlTS = "insert into merchequipment\n          (\n            app_seq_num,\n            equiptype_code,\n            equiplendtype_code,\n            merchequip_amount,\n            equip_model,\n            merchequip_equip_quantity,\n            quantity_deployed\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n            0,\n            'IPPL',\n             :4 ,\n            0\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setString(4,__sJT_2074);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1384^9*/
      }
    }
    catch( Exception e )
    {
      logEntry("storeEquipmentData()",e.toString());
    }
  }

  protected void storeFeatureData( )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1397^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    pos_features
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    pos_features\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1402^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1404^7*/

//  ************************************************************
//  #sql [Ctx] { insert into pos_features
//          (
//            app_seq_num,
//            access_code_flag,
//            access_code,
//            auto_batch_close_flag,
//            auto_batch_close_time,
//            header_line4_flag,
//            header_line4,
//            header_line5_flag,
//            header_line5,
//            footer_flag,
//            footer,
//            reset_ref_flag,
//            invoice_prompt_flag,
//            fraud_control_flag,
//            password_protect_flag,
//            phone_training_flag,
//            terminal_reminder_flag,
//            terminal_reminder_time,
//            tip_option_flag,
//            clerk_enabled_flag,
//            equipment_comment,
//            addresstype_code,
//            fraud_control_option,
//            no_printer_needed,
//            no_printer_owned,
//            avs_flag,
//            phone_training,
//            cash_back_flag,
//            cash_back_limit,
//            purchasing_card_flag,
//            card_truncation_flag,
//            debit_surcharge_flag,
//            debit_surcharge_amount,
//            customer_service_phone,
//            call_waiting_flag,
//            cvv2_flag,
//            shipping_contact_name,
//            shipping_method,
//            shipping_comment,
//            term_comm_type
//          )
//          values
//          (
//            :AppSeqNum,
//            :fields.getField("accessCodeEnable").getData().toUpperCase(),
//            :fields.getField("accessCode").getData(),
//            :fields.getField("autoCloseEnable").getData().toUpperCase(),
//            :decodeActionTime("autoClose"),
//            :fields.getField("receiptLine4Enable").getData().toUpperCase(),
//            :fields.getField("receiptLine4").getData(),
//            :fields.getField("receiptLine5Enable").getData().toUpperCase(),
//            :fields.getField("receiptLine5").getData(),
//            :fields.getField("receiptFooterEnable").getData().toUpperCase(),
//            :fields.getField("receiptFooter").getData(),
//            'Y',--resetTranNumEnable always ON
//            :fields.getField("invoicePromptEnable").getData().toUpperCase(),
//            'Y',--fraudControlEnable always ON
//            null,--:(fields.getField("passwordProtectEnable").getData().toUpperCase()),
//            null,
//            :fields.getField("reminderEnable").getData().toUpperCase(),
//            :decodeActionTime("reminder"),
//            :fields.getField("tipOptionFlag").getData().toUpperCase(),
//            :fields.getField("serverPromptEnable").getData().toUpperCase(),
//            :fields.getField("equipmentComments").getData(),
//            :fields.getField("shippingAddressType").getData(),
//            null,     -- fraud control option?
//            'N',      -- no printer needed?
//            'N',      -- no printer owned?
//            :fields.getField("avsEnable").getData().toUpperCase(),
//            :fields.getField("trainingType").getData(),
//            :fields.getField("debitCashBackEnable").getData().toUpperCase(),
//            :fields.getField("debitCashBackLimit").getData(),
//            :fields.getField("purchasingCardEnable").getData().toUpperCase(),
//            :fields.getField("cardTruncDisable").getData(),
//            :fields.getField("debitSurchargeEnable").getData().toUpperCase(),
//            :fields.getField("debitSurchargeAmount").getData(),
//            :fields.getField("customerServicePhone").getData(),
//            :fields.getField("callWaitingFlag").getData(),
//            :fields.getField("cvv2Flag").getData(),
//            :fields.getField("shippingContactName").getData(),
//            :fields.getField("shippingMethod").getData(),
//            :fields.getField("shippingComments").getData(),
//            :fields.getData("commType")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2075 = fields.getField("accessCodeEnable").getData().toUpperCase();
 String __sJT_2076 = fields.getField("accessCode").getData();
 String __sJT_2077 = fields.getField("autoCloseEnable").getData().toUpperCase();
 String __sJT_2078 = decodeActionTime("autoClose");
 String __sJT_2079 = fields.getField("receiptLine4Enable").getData().toUpperCase();
 String __sJT_2080 = fields.getField("receiptLine4").getData();
 String __sJT_2081 = fields.getField("receiptLine5Enable").getData().toUpperCase();
 String __sJT_2082 = fields.getField("receiptLine5").getData();
 String __sJT_2083 = fields.getField("receiptFooterEnable").getData().toUpperCase();
 String __sJT_2084 = fields.getField("receiptFooter").getData();
 String __sJT_2085 = fields.getField("invoicePromptEnable").getData().toUpperCase();
 String __sJT_2086 = fields.getField("reminderEnable").getData().toUpperCase();
 String __sJT_2087 = decodeActionTime("reminder");
 String __sJT_2088 = fields.getField("tipOptionFlag").getData().toUpperCase();
 String __sJT_2089 = fields.getField("serverPromptEnable").getData().toUpperCase();
 String __sJT_2090 = fields.getField("equipmentComments").getData();
 String __sJT_2091 = fields.getField("shippingAddressType").getData();
 String __sJT_2092 = fields.getField("avsEnable").getData().toUpperCase();
 String __sJT_2093 = fields.getField("trainingType").getData();
 String __sJT_2094 = fields.getField("debitCashBackEnable").getData().toUpperCase();
 String __sJT_2095 = fields.getField("debitCashBackLimit").getData();
 String __sJT_2096 = fields.getField("purchasingCardEnable").getData().toUpperCase();
 String __sJT_2097 = fields.getField("cardTruncDisable").getData();
 String __sJT_2098 = fields.getField("debitSurchargeEnable").getData().toUpperCase();
 String __sJT_2099 = fields.getField("debitSurchargeAmount").getData();
 String __sJT_2100 = fields.getField("customerServicePhone").getData();
 String __sJT_2101 = fields.getField("callWaitingFlag").getData();
 String __sJT_2102 = fields.getField("cvv2Flag").getData();
 String __sJT_2103 = fields.getField("shippingContactName").getData();
 String __sJT_2104 = fields.getField("shippingMethod").getData();
 String __sJT_2105 = fields.getField("shippingComments").getData();
 String __sJT_2106 = fields.getData("commType");
   String theSqlTS = "insert into pos_features\n        (\n          app_seq_num,\n          access_code_flag,\n          access_code,\n          auto_batch_close_flag,\n          auto_batch_close_time,\n          header_line4_flag,\n          header_line4,\n          header_line5_flag,\n          header_line5,\n          footer_flag,\n          footer,\n          reset_ref_flag,\n          invoice_prompt_flag,\n          fraud_control_flag,\n          password_protect_flag,\n          phone_training_flag,\n          terminal_reminder_flag,\n          terminal_reminder_time,\n          tip_option_flag,\n          clerk_enabled_flag,\n          equipment_comment,\n          addresstype_code,\n          fraud_control_option,\n          no_printer_needed,\n          no_printer_owned,\n          avs_flag,\n          phone_training,\n          cash_back_flag,\n          cash_back_limit,\n          purchasing_card_flag,\n          card_truncation_flag,\n          debit_surcharge_flag,\n          debit_surcharge_amount,\n          customer_service_phone,\n          call_waiting_flag,\n          cvv2_flag,\n          shipping_contact_name,\n          shipping_method,\n          shipping_comment,\n          term_comm_type\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n          'Y',--resetTranNumEnable always ON\n           :12 ,\n          'Y',--fraudControlEnable always ON\n          null,--:(fields.getField(\"passwordProtectEnable\").getData().toUpperCase()),\n          null,\n           :13 ,\n           :14 ,\n           :15 ,\n           :16 ,\n           :17 ,\n           :18 ,\n          null,     -- fraud control option:19\n          'N',      -- no printer needed:20\n          'N',      -- no printer owned:21\n           :22 ,\n           :23 ,\n           :24 ,\n           :25 ,\n           :26 ,\n           :27 ,\n           :28 ,\n           :29 ,\n           :30 ,\n           :31 ,\n           :32 ,\n           :33 ,\n           :34 ,\n           :35 ,\n           :36 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,__sJT_2075);
   __sJT_st.setString(3,__sJT_2076);
   __sJT_st.setString(4,__sJT_2077);
   __sJT_st.setString(5,__sJT_2078);
   __sJT_st.setString(6,__sJT_2079);
   __sJT_st.setString(7,__sJT_2080);
   __sJT_st.setString(8,__sJT_2081);
   __sJT_st.setString(9,__sJT_2082);
   __sJT_st.setString(10,__sJT_2083);
   __sJT_st.setString(11,__sJT_2084);
   __sJT_st.setString(12,__sJT_2085);
   __sJT_st.setString(13,__sJT_2086);
   __sJT_st.setString(14,__sJT_2087);
   __sJT_st.setString(15,__sJT_2088);
   __sJT_st.setString(16,__sJT_2089);
   __sJT_st.setString(17,__sJT_2090);
   __sJT_st.setString(18,__sJT_2091);
   __sJT_st.setString(19,__sJT_2092);
   __sJT_st.setString(20,__sJT_2093);
   __sJT_st.setString(21,__sJT_2094);
   __sJT_st.setString(22,__sJT_2095);
   __sJT_st.setString(23,__sJT_2096);
   __sJT_st.setString(24,__sJT_2097);
   __sJT_st.setString(25,__sJT_2098);
   __sJT_st.setString(26,__sJT_2099);
   __sJT_st.setString(27,__sJT_2100);
   __sJT_st.setString(28,__sJT_2101);
   __sJT_st.setString(29,__sJT_2102);
   __sJT_st.setString(30,__sJT_2103);
   __sJT_st.setString(31,__sJT_2104);
   __sJT_st.setString(32,__sJT_2105);
   __sJT_st.setString(33,__sJT_2106);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1492^7*/
    }
    catch(Exception e)
    {
      logEntry("storeFeatureData()", e.toString());
      addError("storeFeatureData: " + e.toString());
    }
  }

  protected void storeLeaseData( )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1505^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchequiplease
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merchequiplease\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1510^7*/

      if ( getLeaseCountTotal() > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1514^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchequiplease
//            (
//              app_seq_num,
//              lease_company_code,
//              lease_account_num,
//              lease_auth_num,
//              lease_months_num,
//              lease_monthly_amount,
//              lease_funding_amount
//            )
//            values
//            (
//              :AppSeqNum,
//              :fields.getField("leaseCompany").getData(),
//              :fields.getField("leaseAcctNum").getData(),
//              :fields.getField("leaseAuthNum").getData(),
//              :fields.getField("leaseMonths").getData(),
//              :fields.getField("leaseMonthlyAmount").getData(),
//              :fields.getField("leaseFundingAmount").getData()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2107 = fields.getField("leaseCompany").getData();
 String __sJT_2108 = fields.getField("leaseAcctNum").getData();
 String __sJT_2109 = fields.getField("leaseAuthNum").getData();
 String __sJT_2110 = fields.getField("leaseMonths").getData();
 String __sJT_2111 = fields.getField("leaseMonthlyAmount").getData();
 String __sJT_2112 = fields.getField("leaseFundingAmount").getData();
   String theSqlTS = "insert into merchequiplease\n          (\n            app_seq_num,\n            lease_company_code,\n            lease_account_num,\n            lease_auth_num,\n            lease_months_num,\n            lease_monthly_amount,\n            lease_funding_amount\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,__sJT_2107);
   __sJT_st.setString(3,__sJT_2108);
   __sJT_st.setString(4,__sJT_2109);
   __sJT_st.setString(5,__sJT_2110);
   __sJT_st.setString(6,__sJT_2111);
   __sJT_st.setString(7,__sJT_2112);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1536^9*/
      }
    }
    catch (Exception e)
    {
      logEntry("storeLeaseData()", e.toString());
      addError("storeLeaseData: " + e.toString());
    }
  }

  protected void storeShippingAddress( )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1550^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    address
//          where   app_seq_num = :AppSeqNum and
//                  addresstype_code = :mesConstants.ADDR_TYPE_SHIPPING
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    address\n        where   app_seq_num =  :1  and\n                addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_SHIPPING);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1556^7*/

      if ( fields.getField("shippingAddressType").asInteger() == mesConstants.ADDR_TYPE_SHIPPING )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1560^9*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//            (
//              app_seq_num,
//              addresstype_code,
//              address_name,
//              address_line1,
//              address_line2,
//              address_city,
//              countrystate_code,
//              address_zip,
//              address_phone
//            )
//            values
//            (
//              :AppSeqNum,
//              :mesConstants.ADDR_TYPE_SHIPPING,
//              :fields.getField("shippingBusinessName").getData(),
//              :fields.getField("shippingAddr1").getData(),
//              :fields.getField("shippingAddr2").getData(),
//              :fields.getField("shippingCity").getData(),
//              :fields.getField("shippingState").getData(),
//              :fields.getField("shippingZip").getData(),
//              :fields.getField("shippingPhone").getData()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2113 = fields.getField("shippingBusinessName").getData();
 String __sJT_2114 = fields.getField("shippingAddr1").getData();
 String __sJT_2115 = fields.getField("shippingAddr2").getData();
 String __sJT_2116 = fields.getField("shippingCity").getData();
 String __sJT_2117 = fields.getField("shippingState").getData();
 String __sJT_2118 = fields.getField("shippingZip").getData();
 String __sJT_2119 = fields.getField("shippingPhone").getData();
   String theSqlTS = "insert into address\n          (\n            app_seq_num,\n            addresstype_code,\n            address_name,\n            address_line1,\n            address_line2,\n            address_city,\n            countrystate_code,\n            address_zip,\n            address_phone\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.app.EquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_SHIPPING);
   __sJT_st.setString(3,__sJT_2113);
   __sJT_st.setString(4,__sJT_2114);
   __sJT_st.setString(5,__sJT_2115);
   __sJT_st.setString(6,__sJT_2116);
   __sJT_st.setString(7,__sJT_2117);
   __sJT_st.setString(8,__sJT_2118);
   __sJT_st.setString(9,__sJT_2119);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1586^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("storeShippingAddress()", e.toString());
      addError("storeShippingAddress: " + e.toString());
    }
  }

  /*
  ** protected boolean loadPosType()
  **
  ** Loads the pos type from the pos_category/merch_pos.  This is an option set
  ** on page 1 of the app.
  **
  ** RETURNS: true if load successful, else false.
  */
  protected boolean loadPosType()
  {
    boolean loadOk = false;
    try
    {
      connect();

      long appSeqNum = fields.getField("appSeqNum").asLong();
      /*@lineinfo:generated-code*//*@lineinfo:1612^7*/

//  ************************************************************
//  #sql [Ctx] { select  pc.pos_type
//          
//          from    pos_category  pc,
//                  merch_pos     mp
//          where   mp.app_seq_num = :appSeqNum
//                  and pc.pos_code = mp.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pc.pos_type\n         \n        from    pos_category  pc,\n                merch_pos     mp\n        where   mp.app_seq_num =  :1 \n                and pc.pos_code = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.app.EquipmentDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   posType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1620^7*/
      loadOk = true;
    }
    catch (Exception e)
    {
      logEntry("loadPosType()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return loadOk;
  }

  /*
  ** protected boolean isPosType(int checkPosType)
  **
  ** Checks the pos type given against the app's pos type.
  **
  ** RETURNS: true if app's pos type matches the given pos type.
  */
  protected boolean isPosType(int checkPosType)
  {
    return checkPosType == getPosType();
  }

  /*
  ** protected int getPosType()
  **
  ** Loads pos type if not loaded already.
  **
  ** RETURN: loaded pos type.
  */
  protected int getPosType()
  {
    if (posType == -1)
    {
      loadPosType();
    }
    return posType;
  }


  protected void setSpecialDefaults()
  {
    try
    {
      // default comm type to IP if virtual terminal
      if(isPosType(mesConstants.APP_MPOS_VIRTUAL_TERMINAL))
      {
        fields.setData("commType", Integer.toString(mesConstants.TERM_COMM_TYPE_IP));
      }
    }
    catch(Exception e)
    {
      logEntry("postHandleRequest()", e.toString());
    }
  }
}/*@lineinfo:generated-code*/