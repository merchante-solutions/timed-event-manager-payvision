/**************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/banner/Equipment.java $

  Description:
  
  Equipment
  
  Banner Bank online application equipment page bean.


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/29/04 4:21p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.banner;

import javax.servlet.http.HttpServletRequest;
//import sqlj.runtime.ResultSetIterator;
import com.mes.app.EquipmentBase;

public class Equipment extends EquipmentBase
{
  {
    appType = 30;
    curScreenId = 2;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // make comm type required
      fields.getField("commType").makeRequired();
      
      // reset all fields, including those just added to be of class formText
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
  
  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
  
    if ( isDialPayApp() )
    {
      // force the training button to MES
      fields.setData("trainingType","M");
    }
  }
}
