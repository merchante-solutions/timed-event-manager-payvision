/*@lineinfo:filename=VirtualAppLandingPage*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/storefront/LandingPage.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-09-23 15:42:51 -0700 (Tue, 23 Sep 2008) $
  Version            : $Revision: 15383 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonImageField;
import com.mes.forms.CheckboxField;
import com.mes.forms.CurrencyPrettyField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;
import sqlj.runtime.ResultSetIterator;

public class VirtualAppLandingPage extends FieldBean
{
  protected int appType = -1;
  
  public VirtualAppLandingPage()
  {
  }
  
  public void setProperties(HttpServletRequest request)
  {
    try
    {
      setFields(request);
      
      // get pricing and logo details
      getPricingDetails(request);
      
      register(user.getLoginName(), request);
      
      // check for page submission
      if(hasData("submitButton"))
      {
        if(isValid())
        {
          // add parameters to url
          StringBuffer extraParams = new StringBuffer(getData("sendToUrl"));
          
          if(hasData("assoc") || hasData("orderId") || hasData("CustomerNum"))
          {
            boolean paramAdded = false;
            extraParams.append("?");
            
            if(hasData("assoc"))
            {
              extraParams.append("assoc=");
              extraParams.append(getData("assoc"));
              paramAdded = true;
            }
            
            if(hasData("orderId"))
            {
              if(paramAdded)
              {
                extraParams.append("&");
              }
              
              extraParams.append("orderId=");
              extraParams.append(getData("orderId"));
              paramAdded = true;
            }
            
            if(hasData("CustomerNum"))
            {
              if(paramAdded)
              {
                extraParams.append("&");
              }
              extraParams.append("netSuiteAcctNum=");
              extraParams.append(getData("CustomerNum"));
              
              paramAdded = true;
            }
          }
          
          setData("sendToUrl", extraParams.toString());
          setData("mustRedirect", "Y");
        }
        else
        {
          setData("statusString", "<font color=\"red\">You must accept the terms by checking the appropriate box in order to proceed.</font>");
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
  }
  
  private void getPricingDetails(HttpServletRequest request)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    try
    {
      connect();
      
      appType = ((Integer)(request.getAttribute("appType"))).intValue();
      
      /*@lineinfo:generated-code*//*@lineinfo:121^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  vcp.minimum_discount,
//                  vcp.setup_fee,
//                  vcp.setup_fee_desc,
//                  vcp.monthly_service_fee,
//                  vcp.monthly_service_fee_desc,
//                  vcp.chargeback_fee,
//                  vcp.chargeback_fee_desc,
//                  vcp.discount_rate,
//                  vcp.discount_rate_desc,
//                  vcp.per_item,
//                  vcp.per_item_desc,
//                  vcp.non_bank_auth_fee,
//                  vcp.logo,
//                  vcp.logo_width,
//                  vcp.logo_height,
//                  vcp.landing_details,
//                  vcp.send_to_url,
//                  nvl(aib.label, '1.75% + $0.10') non_qual_surcharge,
//                  to_char(aib.vs_bet)||'/'||to_char(aib.mc_bet) bet_combo
//          from    vapp_channel_pricing vcp,
//                  appo_ic_bets aib
//          where   vcp.app_type = :appType and
//                  vcp.app_type = aib.app_type(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vcp.minimum_discount,\n                vcp.setup_fee,\n                vcp.setup_fee_desc,\n                vcp.monthly_service_fee,\n                vcp.monthly_service_fee_desc,\n                vcp.chargeback_fee,\n                vcp.chargeback_fee_desc,\n                vcp.discount_rate,\n                vcp.discount_rate_desc,\n                vcp.per_item,\n                vcp.per_item_desc,\n                vcp.non_bank_auth_fee,\n                vcp.logo,\n                vcp.logo_width,\n                vcp.logo_height,\n                vcp.landing_details,\n                vcp.send_to_url,\n                nvl(aib.label, '1.75% + $0.10') non_qual_surcharge,\n                to_char(aib.vs_bet)||'/'||to_char(aib.mc_bet) bet_combo\n        from    vapp_channel_pricing vcp,\n                appo_ic_bets aib\n        where   vcp.app_type =  :1  and\n                vcp.app_type = aib.app_type(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.VirtualAppLandingPage",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.VirtualAppLandingPage",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:146^7*/
      
      rs = it.getResultSet();
      
      setFields(rs);
    }
    catch(Exception e)
    {
      logEntry("getPricingDetails()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField      ("orderId"));
      fields.add(new HiddenField      ("assoc"));
      fields.add(new HiddenField      ("CustomerNum"));
      fields.add(new CheckboxField    ("accept", "I Accept", false));
      fields.add(new ButtonImageField ("submitButton", "Submit", "/images/submit.jpg", fields));
      fields.add(new Field            ("mustRedirect", 1, 1, true));
      fields.add(new Field            ("statusString", 80, 80, true));

      fields.add(new NumberField      ("setupFee", 10, 10, true, 2));
      fields.add(new Field            ("setupFeeDesc", 490, 490, true));
      fields.add(new NumberField      ("minimumDiscount", 10, 10, true, 2));
      fields.add(new Field            ("minimumDiscountDesc", 490, 490, true));
      fields.add(new NumberField      ("monthlyServiceFee", 10, 10, true, 2));
      fields.add(new Field            ("monthlyServiceFeeDesc", 490, 490, true));
      fields.add(new NumberField      ("chargebackFee", 10, 10, true, 2));
      fields.add(new Field            ("chargebackFeeDesc", 490, 490, true));
      fields.add(new NumberField      ("discountRate", 10, 10, true, 2));
      fields.add(new Field            ("discountRateDesc", 490, 490, true));
      fields.add(new CurrencyPrettyField  ("perItem", 10, 10, true));
      fields.add(new Field            ("perItemDesc", 490, 490, true));
      fields.add(new NumberField      ("nonBankAuthFee", 10, 10, true, 2));
      fields.add(new Field            ("nonQualSurcharge", 50, 10, true));
      fields.add(new Field            ("betCombo",10,10,true));
      fields.add(new Field            ("logo", "Logo", 60, 60, true));
      fields.add(new NumberField      ("logoWidth", 4, 4, true, 0));
      fields.add(new NumberField      ("logoHeight", 4, 4, true, 0));
      fields.add(new Field            ("landingDetails", 80, 80, true));
      
      fields.add(new Field("sendToUrl", 100, 100, false));
      
      fields.setData("statusString", "&nbsp;");
      
      fields.getField("accept").addValidation(new CheckboxCheckedValidation());
      
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
    }
  }
  
  public boolean hasSetupFee()
  {
    boolean result = false;
    
    try
    {
      System.out.println("setup fee: " + getData("setupFee"));
      result = (Integer.parseInt(getData("setupFee")) > 0);
    }
    catch(Exception e)
    {
      result = false;
    }
   
    return result;
  }
  
  public boolean mustRedirect()
  {
    return (getData("mustRedirect").equals("Y"));
  }
  
  public String getStatusString()
  {
    return getData("statusString");
  }
  
  public class CheckboxCheckedValidation implements Validation
  {
    public CheckboxCheckedValidation()
    {
    }
    
    public String getErrorText()
    {
      return ("You must accept the terms by checking this box in order to proceed");
    }
    
    public boolean validate(String fdata)
    {
      boolean valid = false;
      
      if(fdata != null && fdata.toLowerCase().equals("y"))
      {
        valid = true;
      }
      
      return valid;
    }
  }
}/*@lineinfo:generated-code*/