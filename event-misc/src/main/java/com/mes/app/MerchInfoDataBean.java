/*@lineinfo:filename=MerchInfoDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/MerchInfoDataBean.sqlj $

  Description:


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-05-16 10:18:06 -0700 (Fri, 16 May 2008) $
  Version            : $Revision: 14864 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DisabledCheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TaxIdField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.forms.ZipField;
import com.mes.support.MesMath;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class MerchInfoDataBean extends AppDataBeanBase
{
  public static final int     CT_VISA                     = 0;
  public static final int     CT_MC                       = 1;
  public static final int     CT_DEBIT                    = 2;
  public static final int     CT_DINERS                   = 3;
  public static final int     CT_DISCOVER                 = 4;
  public static final int     CT_AMEX                     = 5;
  public static final int     CT_JCB                      = 6;
  public static final int     CT_CHECK                    = 7;
  public static final int     CT_EBT                      = 8;
  public static final int     CT_TYME                     = 9;
  public static final int     CT_VALUTEC                  = 10;
  public static final int     CT_COUNT                    = 11;

  public static final int     FNAME_ACCEPTED              = 0;
  public static final int     FNAME_ACCOUNT_NUMBER        = 1;

  public static final int     FIDX_ADDR_LINE1             = 0;
  public static final int     FIDX_ADDR_LINE2             = 1;
  public static final int     FIDX_ADDR_CITY              = 2;
  public static final int     FIDX_ADDR_STATE             = 3;
  public static final int     FIDX_ADDR_ZIP               = 4;
  public static final int     FIDX_ADDR_COUNTRY           = 5;
  public static final int     FIDX_ADDR_PHONE             = 6;
  public static final int     FIDX_ADDR_FAX               = 7;

  public static final int     FIDX_OWNER_LAST_NAME        = 0;
  public static final int     FIDX_OWNER_FIRST_NAME       = 1;
  public static final int     FIDX_OWNER_SSN              = 2;
  public static final int     FIDX_OWNER_PERCENT          = 3;
  public static final int     FIDX_OWNER_MONTH            = 4;
  public static final int     FIDX_OWNER_YEAR             = 5;
  public static final int     FIDX_OWNER_TITLE            = 6;

  public static final int     FIDX_ACCOUNT_NUMBER         = 0;
  public static final int     FIDX_NEW_ACCT_RATE          = 1;
  public static final int     FIDX_SPLIT_DIAL             = 2;
  
  public boolean              allowVTLimited              = false;

  // converts teh CT_XXX index into the corresponding
  // set of form field names.
  protected static String[][]   CardTypeToFieldNames =
  {
    { "vmcAccepted",      null                           }, // CT_VISA
    { "vmcAccepted",      null                           }, // CT_MC
    { "debitAccepted",    null                           }, // CT_DEBIT
    { "dinersAccepted",   "dinersAcctNum"                }, // CT_DINERS
    { "discoverAccepted", "discoverAcctNum"              }, // CT_DISCOVER
    { "amexAccepted",     "amexAcctNum"                  }, // CT_AMEX
    { "jcbAccepted",      "jcbAcctNum"                   }, // CT_JCB
    { "checkAccepted",    "checkAcctNum"                 }, // CT_CHECK
    { "ebtAccepted",      "fcsNumber"                    }, // CT_EBT
    { null,               null                           }, // CT_TYME
    { "valutecAccepted",  "valutecAcctNum"               }, // CT_VALUTEC
  };

  // converts the CT_XXX index into the corresponding
  // application card type to be used in the merchpsyoption and
  // tranchrg application tables.
  protected static int[] CardTypeToAppCardType =
  {
    mesConstants.APP_CT_VISA,             // CT_VISA
    mesConstants.APP_CT_MC,               // CT_MC
    mesConstants.APP_CT_DEBIT,            // CT_DEBIT
    mesConstants.APP_CT_DINERS_CLUB,      // CT_DINERS
    mesConstants.APP_CT_DISCOVER,         // CT_DISCOVER
    mesConstants.APP_CT_AMEX,             // CT_AMEX
    mesConstants.APP_CT_JCB,              // CT_JCB
    mesConstants.APP_CT_CHECK_AUTH,       // CT_CHECK
    mesConstants.APP_CT_EBT,              // CT_EBT
    mesConstants.APP_CT_TYME_NETWORK,     // CT_TYME
    mesConstants.APP_CT_VALUTEC_GIFT_CARD,// CT_VALUTEC
  };

  private static final String[][]   ProductList =
  {
    { "Point of Sale Terminal (Electronic Draft Capture)",
      Integer.toString(mesConstants.POS_DIAL_TERMINAL     ) },
    { "Internet Solution",
      Integer.toString(mesConstants.POS_INTERNET          ) },
    { "PC Charge Express for Windows",
      Integer.toString(mesConstants.POS_CHARGE_EXPRESS    ) },
    { "PC Charge Pro for Windows",
      Integer.toString(mesConstants.POS_CHARGE_PRO        ) },
    { "POS Partner 2000",
      Integer.toString(mesConstants.POS_PC                ) },
    { "DialPay (Telephone Authorization & Capture)",
      Integer.toString(mesConstants.POS_DIAL_AUTH         ) },
    { "Vital Third Party Software",
      Integer.toString(mesConstants.POS_OTHER             ) },
    { "Wireless Terminal",
      Integer.toString(mesConstants.POS_WIRELESS_TERMINAL ) },
    { "Global PC Windows",
      Integer.toString(mesConstants.POS_GLOBAL_PC         ) },
    { "Touch Tone Capture",
      Integer.toString(mesConstants.POS_TOUCHTONECAPTURE  ) },
    { "MES Virtual Terminal",
      Integer.toString(mesConstants.POS_VIRTUAL_TERMINAL  ) },
    { "MES Payment Gateway",
      Integer.toString(mesConstants.POS_PAYMENT_GATEWAY  ) }
 /*
    { "EDC (Electronic Draft Capture-Dial Terminal)", Integer.toString(mesConstants.POS_DIAL_TERMINAL) },
    { "Internet Solutions"                          , Integer.toString(mesConstants.POS_INTERNET) },
    { "POS Partner 2000"                            , Integer.toString(mesConstants.POS_PC) },
    { "DialPay (Telephone Authorization & Capture)" , Integer.toString(mesConstants.POS_DIAL_AUTH) },
    { "Other Vital Certified Product:"              , Integer.toString(mesConstants.POS_OTHER) },
    { "MES Virtual Terminal"                        , Integer.toString(mesConstants.POS_VIRTUAL_TERMINAL) },
    { "MES Payment Gateway"                         , Integer.toString(mesConstants.POS_PAYMENT_GATEWAY  ) }
  */
  };

  protected String[][] PosPartnerList =
  {
    { "Dialup", "0" },
    { "SSL",    "1" }
  };

  protected String[][] cardList =
  {
    { "Card Present and/or Mail Order", Integer.toString(mesConstants.APP_MPOS_PAYMENT_GATEWAY_CP  ) },
    { "e-Commerce, Card Not Present",   Integer.toString(mesConstants.APP_MPOS_PAYMENT_GATEWAY_CNP ) }
  };

  //***************************************************************************
  // VALIDATIONS
  //***************************************************************************
  public static class CheckProviderValidation implements Validation
  {
    protected Field   AcceptedField   = null;
    protected String  ErrorMessage    = null;
    protected Field   OtherProvider   = null;

    public CheckProviderValidation( Field accepted, Field op )
    {
      AcceptedField = accepted;
      OtherProvider = op;
    }

    public boolean validate(String fdata)
    {
      ErrorMessage = null;

      try
      {
        if ( AcceptedField.getData().equals("y") )
        {
          if ( isBlank(fdata) )
          {
            ErrorMessage = "Checks accepted, must specify a check provider";
          }
          else
          {
            if ( fdata.equals("CPOther") && OtherProvider.isBlank() )
            {
              ErrorMessage = "Must specify the name of the check provider";
            }
          }
        }
        else  // checks not accepted
        {
          // the ACR does not allow for the other provider so null
          // must be tested for to prevent exceptions
          if ( !isBlank(fdata) || ( OtherProvider != null && !OtherProvider.isBlank() ) )
          {
            ErrorMessage = "Checks not accepted, please clear check provider fields";
          }
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Must specify a valid check provider";
      }
      return( (ErrorMessage == null) );
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }
  }

  public class DialTerminalRequiredValidation implements Validation
  {
    protected String      ErrorMessage      = null;
    protected Field       ProductTypeField  = null;

    public DialTerminalRequiredValidation( Field productType )
    {
      ProductTypeField  = productType;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      int           ptype     = 0;

      ErrorMessage = null;

      try
      {
        if ( fdata != null && fdata.equals("y") )
        {
          if ( ProductTypeField instanceof RadioButtonField )
          {
            ptype = Integer.parseInt(((RadioButtonField)ProductTypeField).getSelectedButtonValue());
          }
          else
          {
            ptype = ProductTypeField.asInteger();
          }

          if ( ptype != mesConstants.POS_DIAL_TERMINAL )
          {
            String  ptypeStr  = Integer.toString(ptype);

            for ( int i = 0; i < ProductList.length; ++i )
            {
              if ( ProductList[i][1].equals( ptypeStr ) )
              {
                ErrorMessage = "Not accepted with '" +
                                 ProductList[i][0] + "'";
                break;
              }
            }
            if ( ErrorMessage == null )
            {
              ErrorMessage = "Not accepted with the current product selection (" + ptypeStr + ")";
            }
          }
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Not accepted with the current product selection";
      }
      return( (ErrorMessage == null) );
    }
  }

  public static class WelcomeCheckValidation implements Validation
  {
    protected Field   CheckProviderField  = null;
    protected String  ErrorMessage        = null;

    public WelcomeCheckValidation( Field cp )
    {
      CheckProviderField  = cp;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      ErrorMessage = null;

      try
      {
        if ( !CheckProviderField.isBlank() &&
             CheckProviderField.getData().equals("CertegyWelcomeCheck") )
        {
          if ( isBlank(fdata) || fdata.length() != 10 )
          {
            ErrorMessage = "Welcome Check requires a 10 digit Station Number";
          }
        }
      }
      catch( Exception e )
      {
        // ignore exceptions
      }
      return( (ErrorMessage == null) );
    }
  }

  public static class ElecCheckValidation implements Validation
  {
    protected Field   CheckProviderField  = null;
    protected String  ErrorMessage        = null;

    public ElecCheckValidation( Field cp )
    {
      CheckProviderField  = cp;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      ErrorMessage = null;

      try
      {
        if ( !CheckProviderField.isBlank() &&
             CheckProviderField.getData().equals("CertegyElecCheck") )
        {
          if ( isBlank(fdata) || (fdata.length() != 10) )
          {
            ErrorMessage = "Certegy Elec Check requires a 10 digit Station Number";
          }
        }
      }
      catch( Exception e )
      {
        // ignore exceptions
      }
      return( (ErrorMessage == null) );
    }
  }

  public static class ElecCheckTermIdValidation implements Validation
  {
    protected Field   CheckProviderField  = null;
    protected String  ErrorMessage        = null;

    public ElecCheckTermIdValidation( Field cp )
    {
      CheckProviderField  = cp;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      ErrorMessage = null;

      try
      {
        if ( !CheckProviderField.isBlank() &&
             CheckProviderField.getData().equals("CertegyElecCheck") )
        {
          if ( isBlank(fdata) || (fdata.length() != 10) )
          {
            ErrorMessage = "Certegy Elec Check requires a 10 character Terminal ID";
          }
        }
      }
      catch( Exception e )
      {
        // ignore exceptions
      }
      return( (ErrorMessage == null) );
    }
  }

  public static class ValutecTermIdValidation implements Validation
  {
    protected Field   AcceptedField   = null;
    protected String  ErrorMessage    = null;

    public ValutecTermIdValidation( Field accepted )
    {
      AcceptedField = accepted;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      ErrorMessage = null;

      if ( AcceptedField.getData().equals("y") )
      {
        if ( isBlank(fdata) )
        {
          ErrorMessage = "Valutec Terminal ID is required";
        }
      }
      else  // valutec gift card not accepted
      {
        if ( !isBlank(fdata) )
        {
          ErrorMessage = "Valutec Gift Card not accepted, please clear Terminal ID field";
        }
      }
      return( (ErrorMessage == null) );
    }
  }

  public class TransitRoutingValidation extends SQLJConnectionBase
    implements Validation
  {
    private String errorText = null;

    public String getErrorText()
    {
      return ((errorText == null) ? "" : errorText);
    }

    public boolean validate( String fieldData )
    {
      // don't try to validate blank fields, let required validation handle this
      if (fieldData == null || fieldData.equals(""))
      {
        return true;
      }

      boolean     isValid     = true;
      int         itemCount   = 0;
      int         fraudCount  = 0;

      try
      {
        // validate 9 digits
        if (countDigits(fieldData) != 9)
        {
          errorText = "Transit routing number must be nine (9) digits";
          isValid   = false;
        }
        else
        {
          // look up the num in our table of known nums, get fraud flags too
          connect();

          /*@lineinfo:generated-code*//*@lineinfo:471^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(transit_routing_num),
//                      nvl(sum(decode(fraud_flag,'Y',1,0)),0)
//              
//              from    rap_app_bank_aba
//              where   transit_routing_num = :fieldData
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(transit_routing_num),\n                    nvl(sum(decode(fraud_flag,'Y',1,0)),0)\n             \n            from    rap_app_bank_aba\n            where   transit_routing_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.MerchInfoDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fieldData);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   fraudCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:479^11*/

          // detect flagged nums
          if (fraudCount != 0)
          {
            errorText = "This transit routing number is not allowed";
            isValid   = false;
          }
          // detect unknown nums
          else if (itemCount == 0)
          {
            // need to warn user if not found the first time
            if (!hasWarning(mesConstants.ERROR_ABA_NOT_FOUND))
            {
              addWarning(mesConstants.ERROR_ABA_NOT_FOUND);
              errorText = "Transit routing number was not found in our list "
                          + "of valid ABA Numbers.  Please confirm that it "
                          + "is correct and resubmit.";
              isValid   = false;
            }
            // if already warned then let it go
          }
          // not flagged, not unknown
          else
          {
            // clear any prior warnings
            removeWarning(mesConstants.ERROR_ABA_NOT_FOUND);
          }
        }
      }
      catch (java.sql.SQLException e)
      {
        logEntry("validate()",e.toString());
        errorText = "Transit routing number validation failed: " + e.toString();
        isValid = false;
      }
      finally
      {
        cleanUp();
      }

      return isValid;
    }
  }

  public class NoPOBoxValidation implements Validation
  {
    public boolean validate(String fieldData)
    {
      String[] illegals =
        { "po box", "p.o. box", "pobox", "p.o.box", "p. o. box", "p o box",
          "post office box", "postoffice box", "pob" };

      String address = fieldData.toLowerCase();
      for (int i = 0; i < illegals.length; ++i)
      {
        if (address.indexOf(illegals[i]) == 0)
        {
          return false;
        }
      }

      return true;
    }

    public String getErrorText()
    {
      return( "Post office boxes not allowed" );
    }
  }

  public abstract class AccountValidation implements Validation
  {
    protected   String              ErrorMessage    = null;
    protected   Field[]             FieldList       = null;

    public AccountValidation( Field[] fields )
    {
      FieldList = fields;
    }

    public String getErrorText()
    {
      String      retVal    = "";

      if ( ErrorMessage != null )
      {
        retVal = ErrorMessage;
      }
      return(retVal);
    }

    protected int getFieldsWithDataCount( )
    {
      int           retVal      = 0;

      for( int i = 0; i < FieldList.length; ++i )
      {
        if( FieldList[i].isBlank() == false )
        {
          ++retVal;
        }
      }
      return( retVal );
    }

    protected int getFirstFieldWithDataIndex( )
    {
      int           fieldIndex        = 0;

      for( fieldIndex = 0; fieldIndex < FieldList.length; ++fieldIndex )
      {
        // check for data
        if ( FieldList[fieldIndex].isBlank() == false )
        {
          break;
        }
      }
      return( fieldIndex );
    }

    protected boolean hasAdditionalData( )
    {
      boolean       retVal      = false;

      for( int i = 0; i < FieldList.length; ++i )
      {
        if( ! FieldList[i].isBlank() )
        {
          retVal = true;
          break;
        }
      }
      return( retVal );
    }

    abstract public boolean validate( String fieldData );
  }

  public class SicValidation extends SQLJConnectionBase
    implements Validation
  {
    public boolean validate(String fieldData)
    {
      if (fieldData == null || fieldData.length() == 0)
      {
        return true;
      }

      boolean isValid = false;

      try
      {
        connect();

        int intSic = -1;

        try
        {
          intSic = Integer.parseInt(fieldData);
        }
        catch (Exception e) {}

        ResultSetIterator it = null;
        /*@lineinfo:generated-code*//*@lineinfo:643^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sic_code
//            from    sic_codes
//            where   sic_code = :intSic
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sic_code\n          from    sic_codes\n          where   sic_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,intSic);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:648^9*/

        isValid = it.getResultSet().next();
      }
      catch (Exception e)
      {
        String func = this.getClass().getName() + "::validate(fieldData = "
          + fieldData + ")";
        String desc = e.toString();
        System.out.println(func + ": " + desc);
        logEntry(func,desc);
      }
      finally
      {
        cleanUp();
      }

      return isValid;
    }

    public String getErrorText()
    {
      return "Invalid Sic Code";
    }
  }

  public class AmexAccountValidation extends AccountValidation
  {
    public AmexAccountValidation( Field[] fields )
    {
      super( fields );
    }

    public boolean validate(String fieldData)
    {
      Field       field         = null;
      int         fieldIndex    = 0;
      int         hasDataCount  = 0;
      long        temp          = 0L;

      // reset the error message
      ErrorMessage = null;

      // amex not checked or value is "N"
      if( isBlank(fieldData) == true || fieldData.toUpperCase().equals("N") )
      {
        if ( hasAdditionalData() )
        {
          ErrorMessage = "Please check the Amex payment option or remove the additional Amex data provided";
        }
      }
      else    // amex payment option is checked
      {
        hasDataCount = getFieldsWithDataCount();

        if ( hasDataCount == 1 )      // only one field has data
        {
          // get the index into the fields array of the
          // first field that is not blank.
          fieldIndex  = getFirstFieldWithDataIndex();
          field       = FieldList[fieldIndex];

          switch( fieldIndex )
          {
            case FIDX_ACCOUNT_NUMBER:
              try
              {
                // first start by validating that it is a number
                temp  = field.asLong();

                if ( field.getData().length() < 10 )
                {
                  ErrorMessage = "Amex Merchant Number must be at least 10 digits long";
                }
              }
              catch( NumberFormatException e )
              {
                ErrorMessage = "Amex Merchant Number must be a valid number";
              }
              break;

//              case FIDX_NEW_ACCT_RATE:
//              case FIDX_SPLIT_DIAL:
            default:
              break;
          }
        }
        else if ( hasDataCount == 0 )     // no additional data
        {
          ErrorMessage = "Please provide a valid Amex SE# or ESA rate";
        }
        else    // hasDataCount > 1
        {
          ErrorMessage = "Please provide a valid Amex SE# or ESA rate, not both";
        }
      }
      return( ErrorMessage == null );
    }
  }

  public class EBTAccountValidation extends AccountValidation
  {
    public EBTAccountValidation(Field[] fields)
    {
      super(fields);
    }

    public boolean validate(String fieldData)
    {
      // reset the error message
      ErrorMessage = null;

      if( isBlank(fieldData) == true || fieldData.toUpperCase().equals("N") )
      {
        if ( hasAdditionalData() )
        {
          ErrorMessage = "Please check the EBT payment option or remove the FCS Number";
        }
      }
      else
      {
        try
        {
          long temp = Long.parseLong(FieldList[FIDX_ACCOUNT_NUMBER].getData());
        }
        catch( NumberFormatException e )
        {
          ErrorMessage = "FCS Number must be a valid number";
        }
        catch( Exception e )
        {
          ErrorMessage = e.toString();
        }
      }
      return( ErrorMessage == null );
    }
  }

  public class DinersAccountValidation extends AccountValidation
  {
    public DinersAccountValidation( Field[] fields )
    {
      super( fields );
    }

    public boolean validate(String fieldData)
    {
      // reset the error message
      ErrorMessage = null;

      if( isBlank(fieldData) == true || fieldData.toUpperCase().equals("N") )
      {
        if ( hasAdditionalData() )
        {
          ErrorMessage = "Please check the Diners Club payment option or remove the Diners merchant number";
        }
      }
      else
      {
        try
        {
          long temp = Long.parseLong(FieldList[FIDX_ACCOUNT_NUMBER].getData());
          if ( FieldList[FIDX_ACCOUNT_NUMBER].getData().length() < 10 )
          {
            ErrorMessage = "Diners Merchant Number must be 10 digits long";
          }
        }
        catch( NumberFormatException e )
        {
          ErrorMessage = "Diners Merchant Number must be a valid number";
        }
        catch( Exception e )
        {
          ErrorMessage = e.toString();
        }
      }
      return( ErrorMessage == null );
    }
  }

  public class DiscoverAccountValidation extends AccountValidation
  {
    public DiscoverAccountValidation( Field[] fields )
    {
      super( fields );
    }

    public boolean validate(String fieldData)
    {
      Field       field         = null;
      int         fieldIndex    = 0;
      int         hasDataCount  = 0;
      long        temp          = 0L;

      // reset the error message
      ErrorMessage = null;

      if( isBlank(fieldData) == true || fieldData.toUpperCase().equals("N") )
      {
        if ( hasAdditionalData() )
        {
          ErrorMessage = "Please check the Discover payment box or remove the additional Discover account information provided";
        }
      }
      else
      {
        hasDataCount = getFieldsWithDataCount();

        if ( hasDataCount == 1 )
        {
          fieldIndex    = getFirstFieldWithDataIndex();
          field         = FieldList[fieldIndex];

          switch( fieldIndex )
          {
            case FIDX_ACCOUNT_NUMBER:
              try
              {
                temp = Long.parseLong(field.getData());
                if ( field.getData().startsWith("60110") != true )
                {
                  ErrorMessage = "Discover Merchant Number must begin with 60110";
                }
                else if ( field.getData().length() < 15 )
                {
                  ErrorMessage = "Discover Merchant Number must be at least 15 digits long";
                }
              }
              catch( NumberFormatException e )
              {
                ErrorMessage = "Discover Merchant Number must be a valid number";
              }
              break;

//            case FIDX_NEW_ACCT_RATE:
            default:
              break;
          }
        }
        else if ( hasDataCount == 0 )
        {
          ErrorMessage = "Please provide either a valid Discover Merchant # OR RAP rate/fee";
        }
        else // hasDataCount > 1
        {
          ErrorMessage = "Please provide either a valid Discover Merchant # OR RAP rate/fee not both";
        }
      }
      return( ErrorMessage == null );
    }
  }

  public class JCBAccountValidation extends AccountValidation
  {
    public JCBAccountValidation( Field[] fields )
    {
      super( fields );
    }

    public boolean validate(String fieldData)
    {
      // reset the error message and validate
      ErrorMessage = null;

      if( isBlank(fieldData) == true || fieldData.toUpperCase().equals("N") )
      {
        if ( hasAdditionalData() )
        {
          ErrorMessage = "Please check the JCB payment box or remove the JCB merchant number";
        }
      }
      else
      {
        try
        {
          String    tempData  = FieldList[FIDX_ACCOUNT_NUMBER].getData();
          long      temp      = Long.parseLong(tempData);

          if ( tempData.length() < 4 )
          {
            ErrorMessage = "Please provide a valid JCB Merchant #";
          }
        }
        catch( NumberFormatException e )
        {
          ErrorMessage = "JCB Merchant Number must be a valid number";
        }
      }
      return( ErrorMessage == null );
    }
  }

  public class InternetProductValidation implements Validation
  {
    String              ErrorMessage      = null;
    RadioButtonField    ProductField      = null;

    public InternetProductValidation( RadioButtonField product, String errorMsg )
    {
      ProductField  = product;
      ErrorMessage  = errorMsg;
    }

    public String getErrorText()
    {
      String      retVal    = "";

      if ( ErrorMessage != null )
      {
        retVal = ErrorMessage;
      }
      return(retVal);
    }

    public boolean validate(String fieldData)
    {
      boolean           retVal    = true;
      StringBuffer      temp      = null;

      try
      {
        if( Integer.parseInt(ProductField.getSelectedButtonValue()) == mesConstants.POS_INTERNET )
        {
          if ( fieldData.equals("") )
          {
            retVal        = true;
          }
        }
      }
      catch( Exception e )
      {
        retVal = false;
      }
      return(retVal);
    }
  }

  public class OtherProductValidation implements Validation
  {
    String              ErrorMessage      = null;
    RadioButtonField    ProductField      = null;

    public OtherProductValidation( RadioButtonField product, String errorMsg )
    {
      ProductField  = product;
      ErrorMessage  = errorMsg;
    }

    public String getErrorText()
    {
      String      retVal    = "";

      if ( ErrorMessage != null )
      {
        retVal = ErrorMessage;
      }
      return(retVal);
    }

    public boolean validate(String fieldData)
    {
      boolean           retVal    = true;
      StringBuffer      temp      = null;

      try
      {
        if( Integer.parseInt(ProductField.getSelectedButtonValue()) == mesConstants.POS_OTHER )
        {
          if ( fieldData.equals("") )
          {
            retVal = false;
          }
        }
      }
      catch( Exception e )
      {
        retVal = true;
      }
      return(retVal);
    }
  }

  protected class YearField  extends NumberField
  {
    public YearField( String fname, boolean nullAllowed )
    {
      super(fname,4,4,nullAllowed,0);
      addValidation(new YearValidation("Invalid year",nullAllowed));
    }

    protected String processData(String rawData)
    {
      if( rawData == null || rawData.equals("0") )
      {
        rawData = "";
      }
      return( rawData );
    }
  }

  protected class SeasonalFieldGroup extends FieldGroup
  {
    public SeasonalFieldGroup(String fname)
    {
      super(fname);

      String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

      for (int i = 0; i < 12; ++i)
      {
        add(new CheckboxField("seasonalMonth" + i,months[i],false));
      }
    }

    public String getData()
    {
      StringBuffer monthString = new StringBuffer();
      for (int i = 0; i < 12; ++i)
      {
        monthString.append(getData("seasonalMonth" + i).toUpperCase());
      }
      return monthString.toString();
    }

    protected String processData(String rawData)
    {
      String monthString = rawData.toLowerCase();
      for (int i = 0; i < 12; ++i)
      {
        if (rawData.length() > i)
        {
          setData("seasonalMonth" + i,
            (monthString.charAt(i) == 'y' ? "y" : "n"));
        }
      }
      return rawData;
    }
  }

  //***************************************************************************
  // Drop Down Tables
  //***************************************************************************
  public static class CheckProviderTable extends DropDownTable
  {
    public CheckProviderTable( )
    {
      init(true);
    }

    public CheckProviderTable( boolean allowOther )
    {
      init( allowOther);
    }

    protected void init( boolean allowOther )
    {
      addElement("","select one");
      addElement("CertegyWelcomeCheck","Certegy Welcome Check");
      addElement("CertegyElecCheck","Certegy Elec Check");

      if ( allowOther )
      {
        addElement("CPOther","Other");
      }
    }
  }

  protected class NewConversionTable extends DropDownTable
  {
    public NewConversionTable()
    {
      addElement("","select one");
      addElement("N","New Account");
      addElement("C","Conversion Account");
    }
  }

  protected class MonthTable extends DropDownTable
  {
    public MonthTable()
    {
      // value/name pairs
      addElement("","select");
      addElement("1","Jan");
      addElement("2","Feb");
      addElement("3","Mar");
      addElement("4","Apr");
      addElement("5","May");
      addElement("6","Jun");
      addElement("7","Jul");
      addElement("8","Aug");
      addElement("9","Sep");
      addElement("10","Oct");
      addElement("11","Nov");
      addElement("12","Dec");
    }
  }

  protected class GenderTable extends DropDownTable
  {
    public GenderTable()
    {
      // value/name pairs
      addElement("","select");
      addElement("M","Male");
      addElement("F","Female");
    }
  }

  protected class BusinessTypeTable extends DropDownTable
  {
    public BusinessTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Sole Proprietorship");
      addElement("2","Corporation");
      addElement("3","Partnership");
      addElement("4","Medical or Legal Corporation");
      addElement("5","Association/Estate/Trust");
      addElement("6","Tax Exempt");
      addElement("7","Government");
      addElement("8","Limited Liability Company");
    }
  }

  protected class IndustryTypeTable extends DropDownTable
  {
    public IndustryTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail");
      addElement("2","Restaurant");
      addElement("3","Hotel");
      addElement("4","Motel");
      addElement("5","Internet");
      addElement("6","Services");
      addElement("7","Direct Marketing");
      addElement("8","Other");
    }
  }

  protected class LocationTypeTable extends DropDownTable
  {
    public LocationTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail Storefront");
      addElement("2","Internet Storefront");
      addElement("3","Business Office");
      addElement("4","Private Residence");
      addElement("5","Other");
      addElement("6","Bank");
    }
  }

  protected class ApplicationTypeTable extends DropDownTable
  {
    public ApplicationTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Single Outlet");
      addElement("2","Chain");
      addElement("3","Addl. Chain Outlet");
    }
  }

  private class InternetProductTable extends DropDownTable
  {
    public InternetProductTable()
      throws java.sql.SQLException
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;

      try
      {
        connect();
        addElement("","select one");
        /*@lineinfo:generated-code*//*@lineinfo:1231^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  pos_code,
//                    pos_desc
//            from    pos_category
//            where   pos_type = 2 and
//                    nvl(allowed_app_types, 'ALL') = 'ALL'
//            order by pos_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pos_code,\n                  pos_desc\n          from    pos_category\n          where   pos_type = 2 and\n                  nvl(allowed_app_types, 'ALL') = 'ALL'\n          order by pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.MerchInfoDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1239^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }

  private class AccountInfoSourceTable extends DropDownTable
  {
    public AccountInfoSourceTable()
      throws java.sql.SQLException
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;

      try
      {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:1268^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  bais.source_id,
//                    bais.source_desc
//            from    bank_account_info_source bais
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bais.source_id,\n                  bais.source_desc\n          from    bank_account_info_source bais";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.MerchInfoDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1273^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }

  protected class RefundPolicyTable extends DropDownTable
  {
    public RefundPolicyTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Refund in 30 Days or Less");
      addElement("2","No Refunds or Exchanges");
      addElement("3","Exchanges Only");
      addElement("4","Not Applicable");
    }
  }

  protected class PosPartnerOSTable extends DropDownTable
  {
    public PosPartnerOSTable()
    {
      ResultSetIterator it    = null;
      ResultSet         rs    = null;

      try
      {
        addElement("", "select one");

        connect();
        /*@lineinfo:generated-code*//*@lineinfo:1316^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  os_code,
//                    description
//            from    pos_partner_os
//            order by display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  os_code,\n                  description\n          from    pos_partner_os\n          order by display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.MerchInfoDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1322^9*/

        rs = it.getResultSet();
        while(rs.next())
        {
          addElement(rs);
        }
      }
      catch(Exception e)
      {
        logEntry("PosPartnerOSTable()", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }

  public MerchInfoDataBean()
  {
  }

  protected void createPosPartnerExtendedFields()
  {
    // turn on validations for extended POS Partner fields
    try
    {
      // POS Partner-specific fields
      FieldGroup gPosPartner = new FieldGroup("gPosPartner");
      gPosPartner.add  (new RadioButtonField ("posPartnerConnectivity", PosPartnerList, -1, false, "Please select the POS Partner connectivity"));
      gPosPartner.add  (new CheckboxField    ("posPartnerLevelII", "Level II", false));
      gPosPartner.add  (new CheckboxField    ("posPartnerLevelIII", "Level III", false));
      gPosPartner.add  (new DropDownField    ("posPartnerOS", new PosPartnerOSTable(), false));

      fields.add(gPosPartner);

      RadioButtonField productType =
        (RadioButtonField)fields.getField("productType");


      fields.getField("posPartnerConnectivity").setOptionalCondition(
        new FieldValueCondition(productType, Integer.toString(mesConstants.POS_PC)));
      fields.getField("posPartnerOS").setOptionalCondition(
        new FieldValueCondition(productType, Integer.toString(mesConstants.POS_PC)));

      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("createPosPartnerExtendedFields()", e.toString());
    }
  }

  public String getAnnualSalesString( )
  {
    StringBuffer      retVal          = new StringBuffer();
    double            temp            = 0.0;

    try
    {
      temp = Double.parseDouble( fields.getField("monthlySales").getData() );
      if ( temp != 0.0 )
      {
        temp *= 12;
        retVal.append("Annual Sales = ");
        retVal.append(MesMath.toCurrency(temp));
      }
    }
    catch( NumberFormatException e )
    {
      // don't generate any log entries just because
      // the number is invalid
    }
    catch( Exception e )
    {
      logEntry("getAnnualSalesString()",e.toString());
    }

    return( retVal.toString() );
  }

  public String getAnnualVMCSalesString( )
  {
    StringBuffer      retVal          = new StringBuffer();
    double            temp            = 0.0;

    try
    {
      temp = Double.parseDouble( fields.getField("monthlyVMCSales").getData() );
      if ( temp != 0.0 )
      {
        temp *= 12;
        retVal.append("Annual Visa/MC Sales = ");
        retVal.append(MesMath.toCurrency(temp));
      }
    }
    catch( NumberFormatException e )
    {
      // don't generate any log entries just because
      // the number is invalid
    }
    catch( Exception e )
    {
      logEntry("getAnnualVMCSalesString()",e.toString());
    }

    return( retVal.toString() );
  }

  String pathRoot = "/bea/wlserver6.1/config/mydomain/applications/mes";
  boolean showBorders = false;

  public void init()
  {
    super.init();

    OnlyOneValidation     onlyOneVal      = null;
    Field                 temp            = null;
    Validation            val             = null;

    try
    {
      // 1 - business info
      FieldGroup gBusInfo = new FieldGroup("gBusInfo");

      gBusInfo.add(new DropDownField  ("accountType",       "Application Type",new NewConversionTable(),false));
      gBusInfo.add(new NumberField    ("assocNum",          "Association #",6,20,false,0));
      gBusInfo.add(new NumberField    ("repCode",           "Rep Code",4,4,true,0));
      gBusInfo.add(new Field          ("cif",               "CIF #",16,16,true));
      gBusInfo.add(new Field          ("businessName",      "Business Name (DBA)",25,35,false));
      gBusInfo.add(new Field          ("businessLegalName", "Business Legal Name",50,35,false));
      gBusInfo.add(new TaxIdField     ("taxpayerId",        "Federal Taxpayer ID",false));
      gBusInfo.add(new PhoneField     ("businessPhone",     "Business Phone Number",false));
      gBusInfo.add(new Field          ("businessAddress1",  "Business Address Line 1",32,35,false));
      gBusInfo.add(new Field          ("businessAddress2",  "Business Address Line 2",32,35,true));
      gBusInfo.add(new Field          ("businessCity",      "Business City",25,20,false));
      gBusInfo.add(new DropDownField  ("businessState",     "Business State",new StateDropDownTable(),false));
      gBusInfo.add(new ZipField       ("businessZip",       "Business Zip",false,fields.getField("businessState")));
      gBusInfo.add(new EmailField     ("businessEmail",     "Business e-mail Address",45,20,true));
      gBusInfo.add(new DropDownField  ("establishedMonth",  "Business Established Date",new MonthTable(),false));
      gBusInfo.add(new YearField      ("establishedYear",   false));
      gBusInfo.add(new Field          ("contactNameFirst",  "Contact First Name",20,35,false));
      gBusInfo.add(new Field          ("contactNameLast",   "Contact Last Name",20,35,false));
      gBusInfo.add(new PhoneField     ("contactPhone",      "Contact Phone Number",false));
      gBusInfo.add(new PhoneField     ("customerServicePhone", "Customer Service Phone Number", true));
      gBusInfo.add(new Field          ("contactPhoneExt",   7,5,true));
      gBusInfo.add(new EmailField     ("contactEmail",      75,35,true) );
      gBusInfo.add(new Field          ("mailingName",       "Mailing Address Name",50,35,true));
      gBusInfo.add(new Field          ("mailingAddress1",   "Mailing Address Line 1",32,35,true));
      gBusInfo.add(new Field          ("mailingAddress2",   "Mailing Address Line 2",32,35,true));
      gBusInfo.add(new Field          ("mailingCity",       "Mailing City",24,35,true) );
      gBusInfo.add(new DropDownField  ("mailingState",      "Mailing State",new StateDropDownTable(),true));
      gBusInfo.add(new ZipField       ("mailingZip",        "Mailing Zip",true,fields.getField("mailingState")));
      gBusInfo.add(new PhoneField     ("businessFax",       "Business Fax Number",true));
      gBusInfo.add(new NumberField    ("sicCode",           "Sic",4,6,true,0));
      gBusInfo.add(new DropDownField  ("businessType",      "Type of Business",new BusinessTypeTable(),false));
      gBusInfo.add(new DropDownField  ("industryType",      "Industry",new IndustryTypeTable(),false));
      gBusInfo.add(new TextareaField  ("businessDesc",      "Business Description",50,2,40,false));
      gBusInfo.add(new DropDownField  ("applicationType",   "Type of Account Setup",new ApplicationTypeTable(),false));
      gBusInfo.add(new NumberField    ("numLocations",      "# of Locations",3,35,false,0));
      gBusInfo.add(new DropDownField  ("locationType",      "Type of Business Location",new LocationTypeTable(),false));
      gBusInfo.add(new NumberField    ("locationYears",     "# of Years at Main Location",3,35,false,0));

      gBusInfo.getField("businessAddress1"  ).addValidation(new NoPOBoxValidation() );
      gBusInfo.getField("sicCode"           ).addValidation(new SicValidation()     );

      fields.add(gBusInfo);

      // 2 - merchant history
      FieldGroup gMerchHist = new FieldGroup("gMerchHist");

      gMerchHist.add(new DropDownField("haveProcessed",       "Ever Accepted Credit Cards Before?",new YesNoTable(),false));
      gMerchHist.add(new Field        ("previousProcessor",   "If Yes, Name of Previous Processor",40,35,true));
      gMerchHist.add(new DropDownField("statementsProvided",  "Previous Processor Statements Provided?",new YesNoTable(),false));
      gMerchHist.add(new DropDownField("haveCanceled",        "Ever Had a Merchant Account Canceled?",new YesNoTable(),false));
      gMerchHist.add(new Field        ("canceledProcessor",   "If Yes, Name of Processor",40,35,true));
      gMerchHist.add(new Field        ("canceledReason",      "Reason for Cancellation",40,35,true));
      gMerchHist.add(new DropDownField("cancelMonth",         "Date of Cancellation",new MonthTable(),true));
      gMerchHist.add(new YearField    ("cancelYear",          true));
      gMerchHist.add(new Field        ("referringBank",       40,30,true));

      gMerchHist.getField("previousProcessor")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveProcessed"),
          "Please provide the name of the previous processor"));
      gMerchHist.getField("canceledProcessor")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
          "Please provide the name of the cancelling processor"));
      gMerchHist.getField("canceledReason")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
          "Please provide reason account was cancelled"));
      gMerchHist.getField("cancelMonth")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
          "Please select the month account was cancelled"));
      gMerchHist.getField("cancelYear")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
          "Please provide a valid 4-digit year account was cancelled"));

      fields.add(gMerchHist);

      // 3 - bank account info
      FieldGroup gBankAcct = new FieldGroup("gBankAcct");

      gBankAcct.add(new Field         ("bankName",                "Name of Bank",30,35,false));
      gBankAcct.add(new NumberField   ("yearsOpen",               "Years Open",3,35,false,0));
      gBankAcct.add(new Field         ("checkingAccount",         "Checking Acct. #",17,35,false));
      gBankAcct.add(new Field         ("confirmCheckingAccount",  "Confirm Checking Acct. #",17,35,false));
      gBankAcct.add(new Field         ("transitRouting",          "Transit Routing #",9,35,false));
      gBankAcct.add(new Field         ("confirmTransitRouting",   "Confirm Transit Routing #",9,35,false));
      gBankAcct.add(new DropDownField ("sourceOfInfo",            "Source of Acct. Info",new AccountInfoSourceTable(),false));
      gBankAcct.add(new Field         ("bankAddress",             "Bank Address",32,35,false));
      gBankAcct.add(new Field         ("bankCity",                "Bank City",25,35,false));
      gBankAcct.add(new DropDownField ("bankState",               "Bank State",new StateDropDownTable(),false));
      gBankAcct.add(new ZipField      ("bankZip",                 "Bank Zip",false,fields.getField("bankState")));
      gBankAcct.add(new PhoneField    ("bankPhone",               "Bank Phone",true));
      gBankAcct.add(new Field         ("referringBank",           "Referring Bank",40,30,true));
      gBankAcct.add(new HiddenField   ("typeOfAcct"));

      gBankAcct.getField("confirmCheckingAccount")
        .addValidation(new FieldEqualsFieldValidation(
          gBankAcct.getField("checkingAccount"),"Checking Account #"));
      gBankAcct.getField("confirmTransitRouting")
        .addValidation(new FieldEqualsFieldValidation(
          gBankAcct.getField("transitRouting"),"Transit Routing #"));
      gBankAcct.getField("confirmTransitRouting")
        .addValidation(new TransitRoutingValidation());
      gBankAcct.getField("typeOfAcct").setData("2");

      fields.add(gBankAcct);

      // 4 - primary owner
      FieldGroup gOwner1 = new FieldGroup("gOwner1");

      gOwner1.add(new Field           ("owner1FirstName",   "First Name",20,20,false));
      gOwner1.add(new Field           ("owner1LastName",    "Last Name",20,20,false));
      gOwner1.add(new TaxIdField      ("owner1SSN",         "Soc. Security #",false));
      gOwner1.add(new NumberField     ("owner1Percent",     "Percent Owned",3,3,false,0));
      gOwner1.add(new DropDownField   ("owner1Gender",      "Gender",new GenderTable(),false));
      gOwner1.add(new Field           ("owner1Address1",    "Residence Address",32,32,false));
      gOwner1.add(new Field           ("owner1City",        "City",25,25,false));
      gOwner1.add(new DropDownField   ("owner1State",       "State",new StateDropDownTable(),false));
      gOwner1.add(new ZipField        ("owner1Zip",         "Zip",false,fields.getField("owner1State")));
      gOwner1.add(new PhoneField      ("owner1Phone",       "Phone Number",true));
      gOwner1.add(new DropDownField   ("owner1SinceMonth",  "Owner/Officer Since",new MonthTable(), true));
      gOwner1.add(new YearField       ("owner1SinceYear",   true));
      gOwner1.add(new Field           ("owner1Title",       "Title",40,20,true));

      gOwner1.getField("owner1Percent")
        .addValidation(new PercentValidation(
          "Primary owner's percentage of ownership is invalid",true));

      fields.add(gOwner1);

      // 5 - secondary owner
      FieldGroup gOwner2 = new FieldGroup("gOwner2");

      gOwner2.add(new Field         ("owner2FirstName",   "First Name",20,20,true));
      gOwner2.add(new Field         ("owner2LastName",    "Last Name",20,20,true));
      gOwner2.add(new TaxIdField    ("owner2SSN",         "Soc. Security #",true));
      gOwner2.add(new NumberField   ("owner2Percent",     "Percent Owned",3,3,true,0));
      gOwner2.add(new Field         ("owner2Address1",    "Residence Address",32,32,true));
      gOwner2.add(new Field         ("owner2City",        "City",25,25,true));
      gOwner2.add(new DropDownField ("owner2State",       "State",new StateDropDownTable(),true));
      gOwner2.add(new ZipField      ("owner2Zip",         "Zip",true,fields.getField("owner2State")));
      gOwner2.add(new PhoneField    ("owner2Phone",       "Phone Number",true));
      gOwner2.add(new DropDownField ("owner2SinceMonth",  "Owner/Officer Since",new MonthTable(), true));
      gOwner2.add(new YearField     ("owner2SinceYear",   true));
      gOwner2.add(new Field         ("owner2Title",       "Title",40,20,true));

      gOwner2.getField("owner2Percent")
        .addValidation(new PercentValidation(
          "Secondary owner's percentage of ownership is invalid",true));

      fields.add(gOwner2);

      // 6 - transaction info
      FieldGroup gTranInfo = new FieldGroup("gTranInfo");

      gTranInfo.add(new CurrencyField ("monthlySales",    "Total Estimated Monthly Sales",11,9,false));
      gTranInfo.add(new CurrencyField ("monthlyVMCSales", "Total Estimated Monthly Visa/MC Sales",11,9,false));
      gTranInfo.add(new ButtonField   ("recalculate",     "Calculate Total Estimated Annual Sales","Calculate"));
      gTranInfo.add(new CurrencyField ("averageTicket",   "Estimated Average Credit Card Ticket",11,9,false));
      gTranInfo.add(new NumberField   ("motoPercentage",  "% Mail/Telephone Sales",3,3,false,0));
      gTranInfo.add(new DropDownField ("refundPolicy",    "Refund Policy",new RefundPolicyTable(),false));
      gTranInfo.add(new SeasonalFieldGroup("seasonalMonths"));

      gTranInfo.add(new NumberField   ("internetPercent", "% Internet Sales",3,3,true,0));
      gTranInfo.add(new NumberField   ("cardPercent",     "% Sales Cardholder Present",3,3,true,0));
      gTranInfo.add(new NumberField   ("noCardPercent",   "% Sales Cardholder Not Present",3,3,true,0));

      gTranInfo.getField("motoPercentage")
        .addValidation(new PercentValidation(
          "Please provide a valid mail order/telephone order percentage"));

      fields.add(gTranInfo);

      // 7 - payment options

      fields.add(new DisabledCheckboxField ("vmcAccepted",     "Visa/MasterCard",true));

      // Debit
      fields.add(new CheckboxField("debitAccepted","Debit",false));

      // Amex
      fields.add(new CheckboxField    ("amexAccepted","American Express",false));
      fields.add(new CheckboxField    ("amexSplitDial","Split Dial",false));
      fields.add(new CheckboxField    ("amexPIP","PIP",false));
      fields.add(new NumberField      ("amexEsaRate",5,5,true,2) );
      fields.add(new NumberField      ("amexAcctNum",16,25,true,0) );
      val = new AmexAccountValidation( new Field[]
                                       {
                                         fields.getField("amexAcctNum"),
                                         fields.getField("amexEsaRate")
                                       } );
      fields.getField("amexAccepted").addValidation(val);

      // setup the only one validation for PIP and split dial
      onlyOneVal = new OnlyOneValidation("Please select either Amex Split Dial or PIP, not both");
      onlyOneVal.addField(fields.getField("amexSplitDial"));
      onlyOneVal.addField(fields.getField("amexPIP"));
      fields.getField("amexAccepted").addValidation(onlyOneVal);

      // Discover
      fields.add( new CheckboxField("discoverAccepted","Discover",false) );
      fields.add( new NumberField("discoverRapRate",5,5,true,2) );
      fields.add( new NumberField("discoverAcctNum",15,25,true,0) );
      val = new DiscoverAccountValidation( new Field[]
                                           {
                                             fields.getField("discoverAcctNum"),
                                             fields.getField("discoverRapRate")
                                           } );
      fields.getField("discoverAccepted").addValidation(val);

      // Diners
      fields.add(new CheckboxField("dinersAccepted","Diners Club",false));
      fields.add( new NumberField("dinersAcctNum",10,25,true,0) );
      val = new DinersAccountValidation( new Field[] { fields.getField("dinersAcctNum") } );
      fields.getField("dinersAccepted").addValidation(val);

      // JCB
      fields.add( new CheckboxField("jcbAccepted","JCB",false) );
      fields.add( new NumberField("jcbAcctNum",25,25,true,0) );
      val = new JCBAccountValidation( new Field[] { fields.getField("jcbAcctNum") } );
      fields.getField("jcbAccepted").addValidation(val);

      // EBT
      fields.add(new CheckboxField("ebtAccepted","EBT",false));
      fields.add(new Field("fcsNumber",25,25,true));
      fields.getField("ebtAccepted").addValidation(new EBTAccountValidation(new Field[] { fields.getField("fcsNumber") } ));

      // check verification
      fields.add  (new CheckboxField    ("checkAccepted",     "Checks",false));
      fields.add  (new DropDownField    ("checkProvider",     "Check Provider",new CheckProviderTable(),true));
      fields.add  (new Field            ("checkProviderOther","Provider Name",25,25,true));
      fields.add  (new Field            ("checkAcctNum",      "Acct # / Station #",25,25,true));
      fields.add  (new Field            ("checkTermId",       "Elec Check Terminal ID",25,25,true));
      fields.add  (new CheckboxField    ("valutecAccepted",   "Valutec Gift Card",false));
      fields.add  (new Field            ("valutecAcctNum",    "Valutec Merchant #",25,25,true));
      fields.add  (new NumberField      ("valutecTermId",     "Valutec Terminal ID",10,25,true,0));

      fields.getField("valutecTermId")
        .addValidation( new ValutecTermIdValidation( fields.getField("valutecAccepted") ) );
      fields.getField("valutecTermId").setMinLength(5);

      fields.getField("checkProvider")
        .addValidation( new CheckProviderValidation( fields.getField("checkAccepted"),
                                                     fields.getField("checkProviderOther") ) );

      // insure proper lengths for check tids
      fields.getField("checkAcctNum")
        .addValidation( new WelcomeCheckValidation( fields.getField("checkProvider") ) );
      fields.getField("checkAcctNum")
        .addValidation( new ElecCheckValidation( fields.getField("checkProvider") ) );
      fields.getField("checkTermId")
        .addValidation( new ElecCheckTermIdValidation( fields.getField("checkProvider") ) );


      // type of POS product
      temp = new RadioButtonField("productType", ProductList, -1, false,
                                  "Please specify a product type" );
      fields.add(temp);

      RadioButtonField productType =
        (RadioButtonField)fields.getField("productType");

      fields.add(new DropDownField("internetType",new InternetProductTable(),true));
      //fields.getField("internetType").addValidation(
      //    new InternetProductValidation(productType,"Please select an internet solution type" ) );
      fields.add(new Field("webUrl",25,30,true));
      //fields.getField("webUrl").addValidation(
      //  new InternetProductValidation( productType, "Please provide a web store URL" ) );
      fields.add(new EmailField ("internetEmail",     100,45,false));

      fields.getField("internetType").setOptionalCondition(
        new FieldValueCondition(productType,
          Integer.toString(mesConstants.POS_INTERNET)));
      fields.getField("webUrl").setOptionalCondition(
        new FieldValueCondition(productType,
          Integer.toString(mesConstants.POS_INTERNET)));
      fields.getField("internetEmail").setOptionalCondition(
        new FieldValueCondition(productType,
          Integer.toString(mesConstants.POS_INTERNET)));

      fields.add(new CheckboxField
        ("existingInternet", "Merchant has existing Internet Solution", false));

      fields.add(new EmailField("vtEmail", 100, 45, false));
      fields.getField("vtEmail").setOptionalCondition(
        new FieldValueCondition(productType,
          Integer.toString(mesConstants.POS_VIRTUAL_TERMINAL)));

      fields.add(new Field("vitalProduct",40,45,true));
      fields.getField("vitalProduct").addValidation(
        new OtherProductValidation( productType ,"Please provide the name of the Vital Certified Product") );

      fields.add  (new CheckboxField  ("existingPCChargeExpress", "Merchant has existing PC Charge Express for Windows", false));
      fields.add  (new CheckboxField  ("existingPCChargePro", "Merchant has existing PC Charge Pro for Windows", false));


      //new Trident product 6-07
      fields.add  (new EmailField     ("pgEmail",       100,45,false));
      fields.add  (new Field          ("pgCertName",    100,45,false));
      fields.add  (new RadioButtonField("pgCard", cardList, -1, false, "Processing Options"));

      fields.getField("pgCertName").setOptionalCondition(
        new FieldValueCondition(productType,
          Integer.toString(mesConstants.POS_PAYMENT_GATEWAY)));
      fields.getField("pgEmail").setOptionalCondition(
        new FieldValueCondition(productType,
          Integer.toString(mesConstants.POS_PAYMENT_GATEWAY)));
      fields.getField("pgCard").setOptionalCondition(
        new FieldValueCondition(productType,
          Integer.toString(mesConstants.POS_PAYMENT_GATEWAY)));



      fields.add(new NumberField("imprinterPlates",2,3,true,0));
      fields.add(new TextareaField("comments",4000,8,80,true));

      // add validations for check and valutec gift card
      // need to be done after productType has been instantiated
      fields.getField("valutecAccepted")
        .addValidation( new DialTerminalRequiredValidation( fields.getField("productType") ) );
      fields.getField("checkAccepted")
        .addValidation( new DialTerminalRequiredValidation( fields.getField("productType") ) );

      // This defaults to "US" which is currently the
      // only valid value.
      temp = new HiddenField("businessCountry");
      temp.setData("US");
      fields.add(temp);

      fields.add(new HiddenField("agreement"));    //@ necessary?

      // setup form data
      form.setWidth(600);

      addHtmlExtra("class=\"formFields\"");
    }
    catch( Exception e )
    {
      logEntry("init()",e.toString());
    }
  }

  public void loadData()
  {
    ResultSetIterator             it        = null;
    ResultSet                     resultSet = null;

    try
    {
      if ( AppSeqNum != APP_SEQ_NEW )
      {
        // extract the merchant number for this app
        /*@lineinfo:generated-code*//*@lineinfo:1799^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(mr.merch_number,0) 
//            from    merchant          mr
//            where   mr.app_seq_num = :AppSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(mr.merch_number,0)  \n          from    merchant          mr\n          where   mr.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.MerchInfoDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   MerchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1804^9*/

        // query the merchant data to be placed into fields
        /*@lineinfo:generated-code*//*@lineinfo:1807^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mr.merch_business_name          as business_name,
//                    mr.merch_federal_tax_id         as taxpayer_id,
//                    mr.merch_number                 as merchant_number,
//                    mr.merch_business_establ_month  as established_month,
//                    mr.merch_business_establ_year   as established_year,
//                    mr.merch_legal_name             as business_legal_name,
//                    mr.merch_mailing_name           as mailing_name,
//                    mr.merch_email_address          as business_email,
//                    mr.bustype_code                 as business_type,
//                    mr.industype_code               as industry_type,
//                    mr.merch_busgoodserv_descr      as business_desc,
//                    mr.merch_num_of_locations       as num_locations,
//                    mr.merch_years_at_loc           as location_years,
//                    mr.loctype_code                 as location_type,
//                    mr.merch_prior_cc_accp_flag     as have_processed,
//                    mr.merch_prior_processor        as previous_processor,
//                    mr.merch_cc_acct_term_flag      as have_canceled,
//                    mr.merch_cc_term_name           as canceled_processor,
//                    mr.merch_term_reason            as canceled_reason,
//                    mr.merch_term_year              as cancel_year,
//                    mr.merch_term_month             as cancel_month,
//                    mr.merch_month_tot_proj_sales   as monthly_sales,
//                    mr.merch_month_visa_mc_sales    as monthly_vmc_sales,
//                    mr.merch_average_cc_tran        as average_ticket,
//                    mr.merch_mail_phone_sales       as moto_percentage,
//                    mr.merch_notes                  as comments,
//                    mr.refundtype_code              as refund_policy,
//                    mr.merch_application_type       as application_type,
//                    mr.merch_referring_bank         as referring_bank,
//                    mr.merch_agreement              as agreement,
//                    mr.merch_prior_statements       as statements_provided,
//                    mr.merch_web_url                as web_url,
//                    mr.app_sic_code                 as sic_code,
//                    mr.merch_gender                 as owner_1_gender,
//                    mr.merch_bank_number            as not_used_2,
//                    mr.franchise_code               as not_used_3,
//                    mr.asso_number                  as assoc_num,
//                    mr.account_type                 as account_type,
//                    mr.merch_rep_code               as rep_code,
//                    mr.svb_cif_num                  as cif,
//                    mr.seasonal_months              as seasonal_months,
//                    mr.percent_internet             as internet_percent,
//                    mr.percent_card_present         as card_percent,
//                    mr.percent_card_not_present     as no_card_percent,
//                    mr.customer_service_phone       as customer_service_phone
//            from    merchant          mr
//            where   mr.app_seq_num = :AppSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mr.merch_business_name          as business_name,\n                  mr.merch_federal_tax_id         as taxpayer_id,\n                  mr.merch_number                 as merchant_number,\n                  mr.merch_business_establ_month  as established_month,\n                  mr.merch_business_establ_year   as established_year,\n                  mr.merch_legal_name             as business_legal_name,\n                  mr.merch_mailing_name           as mailing_name,\n                  mr.merch_email_address          as business_email,\n                  mr.bustype_code                 as business_type,\n                  mr.industype_code               as industry_type,\n                  mr.merch_busgoodserv_descr      as business_desc,\n                  mr.merch_num_of_locations       as num_locations,\n                  mr.merch_years_at_loc           as location_years,\n                  mr.loctype_code                 as location_type,\n                  mr.merch_prior_cc_accp_flag     as have_processed,\n                  mr.merch_prior_processor        as previous_processor,\n                  mr.merch_cc_acct_term_flag      as have_canceled,\n                  mr.merch_cc_term_name           as canceled_processor,\n                  mr.merch_term_reason            as canceled_reason,\n                  mr.merch_term_year              as cancel_year,\n                  mr.merch_term_month             as cancel_month,\n                  mr.merch_month_tot_proj_sales   as monthly_sales,\n                  mr.merch_month_visa_mc_sales    as monthly_vmc_sales,\n                  mr.merch_average_cc_tran        as average_ticket,\n                  mr.merch_mail_phone_sales       as moto_percentage,\n                  mr.merch_notes                  as comments,\n                  mr.refundtype_code              as refund_policy,\n                  mr.merch_application_type       as application_type,\n                  mr.merch_referring_bank         as referring_bank,\n                  mr.merch_agreement              as agreement,\n                  mr.merch_prior_statements       as statements_provided,\n                  mr.merch_web_url                as web_url,\n                  mr.app_sic_code                 as sic_code,\n                  mr.merch_gender                 as owner_1_gender,\n                  mr.merch_bank_number            as not_used_2,\n                  mr.franchise_code               as not_used_3,\n                  mr.asso_number                  as assoc_num,\n                  mr.account_type                 as account_type,\n                  mr.merch_rep_code               as rep_code,\n                  mr.svb_cif_num                  as cif,\n                  mr.seasonal_months              as seasonal_months,\n                  mr.percent_internet             as internet_percent,\n                  mr.percent_card_present         as card_percent,\n                  mr.percent_card_not_present     as no_card_percent,\n                  mr.customer_service_phone       as customer_service_phone\n          from    merchant          mr\n          where   mr.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1856^9*/
        resultSet = it.getResultSet();

        // set the rest of the form related fields
        setFields(resultSet);

        resultSet.close();
        it.close();

        // business location address (no PO Boxes)
        /*@lineinfo:generated-code*//*@lineinfo:1866^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1           as business_address_1,
//                    address_line2           as business_address_2,
//                    address_city            as business_city,
//                    countrystate_code       as business_state,
//                    country_code            as business_country,
//                    address_zip             as business_zip,
//                    address_phone           as business_phone,
//                    address_fax             as business_fax
//            from    address
//            where   app_seq_num = :AppSeqNum and
//                    addresstype_code = :mesConstants.ADDR_TYPE_BUSINESS
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1           as business_address_1,\n                  address_line2           as business_address_2,\n                  address_city            as business_city,\n                  countrystate_code       as business_state,\n                  country_code            as business_country,\n                  address_zip             as business_zip,\n                  address_phone           as business_phone,\n                  address_fax             as business_fax\n          from    address\n          where   app_seq_num =  :1  and\n                  addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_BUSINESS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1879^9*/
        setFields(it.getResultSet());
        it.close();

        // business mailing address
        /*@lineinfo:generated-code*//*@lineinfo:1884^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1           as mailing_address_1,
//                    address_line2           as mailing_address_2,
//                    address_city            as mailing_city,
//                    countrystate_code       as mailing_state,
//                    address_zip             as mailing_zip
//            from    address
//            where   app_seq_num = :AppSeqNum and
//                    addresstype_code = :mesConstants.ADDR_TYPE_MAILING
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1           as mailing_address_1,\n                  address_line2           as mailing_address_2,\n                  address_city            as mailing_city,\n                  countrystate_code       as mailing_state,\n                  address_zip             as mailing_zip\n          from    address\n          where   app_seq_num =  :1  and\n                  addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_MAILING);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1894^9*/
        setFields(it.getResultSet());
        it.close();

        // primary owner address
        /*@lineinfo:generated-code*//*@lineinfo:1899^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1           as owner1_address_1,
//                    address_line2           as owner1_address_2,
//                    address_city            as owner1_city,
//                    countrystate_code       as owner1_state,
//                    country_code            as owner1_country,
//                    address_zip             as owner1_zip,
//                    address_phone           as owner1_phone
//            from    address
//            where   app_seq_num = :AppSeqNum and
//                    addresstype_code = :mesConstants.ADDR_TYPE_OWNER1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1           as owner1_address_1,\n                  address_line2           as owner1_address_2,\n                  address_city            as owner1_city,\n                  countrystate_code       as owner1_state,\n                  country_code            as owner1_country,\n                  address_zip             as owner1_zip,\n                  address_phone           as owner1_phone\n          from    address\n          where   app_seq_num =  :1  and\n                  addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_OWNER1);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1911^9*/
        setFields(it.getResultSet());
        it.close();

        // secondary owner address
        /*@lineinfo:generated-code*//*@lineinfo:1916^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1           as owner2_address_1,
//                    address_line2           as owner2_address_2,
//                    address_city            as owner2_city,
//                    countrystate_code       as owner2_state,
//                    country_code            as owner2_country,
//                    address_zip             as owner2_zip,
//                    address_phone           as owner2_phone
//            from    address
//            where   app_seq_num = :AppSeqNum and
//                    addresstype_code = :mesConstants.ADDR_TYPE_OWNER2
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1           as owner2_address_1,\n                  address_line2           as owner2_address_2,\n                  address_city            as owner2_city,\n                  countrystate_code       as owner2_state,\n                  country_code            as owner2_country,\n                  address_zip             as owner2_zip,\n                  address_phone           as owner2_phone\n          from    address\n          where   app_seq_num =  :1  and\n                  addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_OWNER2);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1928^9*/
        setFields(it.getResultSet());
        it.close();

        // bank address
        /*@lineinfo:generated-code*//*@lineinfo:1933^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1           as bank_address,
//                    address_city            as bank_city,
//                    countrystate_code       as bank_state,
//                    address_zip             as bank_zip,
//                    address_phone           as bank_phone
//            from    address
//            where   app_seq_num = :AppSeqNum and
//                    addresstype_code = :mesConstants.ADDR_TYPE_CHK_ACCT_BANK
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1           as bank_address,\n                  address_city            as bank_city,\n                  countrystate_code       as bank_state,\n                  address_zip             as bank_zip,\n                  address_phone           as bank_phone\n          from    address\n          where   app_seq_num =  :1  and\n                  addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_CHK_ACCT_BANK);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1943^9*/
        setFields(it.getResultSet());
        it.close();

        // primary business owner
        /*@lineinfo:generated-code*//*@lineinfo:1948^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_last_name      as owner1_last_name,
//                    busowner_first_name     as owner1_first_name,
//                    busowner_ssn            as owner1_ssn,
//                    busowner_owner_perc     as owner1_percent,
//                    busowner_period_month   as owner1_since_month,
//                    busowner_period_year    as owner1_since_year,
//                    busowner_title          as owner1_title
//            from    businessowner
//            where   app_seq_num  = :AppSeqNum and
//                    busowner_num = :mesConstants.BUS_OWNER_PRIMARY
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_last_name      as owner1_last_name,\n                  busowner_first_name     as owner1_first_name,\n                  busowner_ssn            as owner1_ssn,\n                  busowner_owner_perc     as owner1_percent,\n                  busowner_period_month   as owner1_since_month,\n                  busowner_period_year    as owner1_since_year,\n                  busowner_title          as owner1_title\n          from    businessowner\n          where   app_seq_num  =  :1  and\n                  busowner_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.BUS_OWNER_PRIMARY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1960^9*/
        setFields(it.getResultSet());
        it.close();

        // secondary business owner
        /*@lineinfo:generated-code*//*@lineinfo:1965^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_last_name      as owner2_last_name,
//                    busowner_first_name     as owner2_first_name,
//                    decode(busowner_ssn,
//                           null,' ',
//                           0, ' ',
//                           busowner_ssn)    as owner2_ssn,
//                    busowner_owner_perc     as owner2_percent,
//                    busowner_period_month   as owner2_since_month,
//                    busowner_period_year    as owner2_since_year,
//                    busowner_title          as owner2_title
//            from    businessowner
//            where   app_seq_num  = :AppSeqNum and
//                    busowner_num = :mesConstants.BUS_OWNER_SECONDARY
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_last_name      as owner2_last_name,\n                  busowner_first_name     as owner2_first_name,\n                  decode(busowner_ssn,\n                         null,' ',\n                         0, ' ',\n                         busowner_ssn)    as owner2_ssn,\n                  busowner_owner_perc     as owner2_percent,\n                  busowner_period_month   as owner2_since_month,\n                  busowner_period_year    as owner2_since_year,\n                  busowner_title          as owner2_title\n          from    businessowner\n          where   app_seq_num  =  :1  and\n                  busowner_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.BUS_OWNER_SECONDARY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1980^9*/
        setFields(it.getResultSet());
        it.close();

        // contact data
        /*@lineinfo:generated-code*//*@lineinfo:1985^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchcont_prim_first_name   as contact_name_first,
//                    merchcont_prim_last_name    as contact_name_last,
//                    merchcont_prim_phone        as contact_phone,
//                    merchcont_prim_phone_ext    as contact_phone_ext,
//                    merchcont_prim_email        as contact_email
//            from    merchcontact
//            where   app_seq_num = :AppSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merchcont_prim_first_name   as contact_name_first,\n                  merchcont_prim_last_name    as contact_name_last,\n                  merchcont_prim_phone        as contact_phone,\n                  merchcont_prim_phone_ext    as contact_phone_ext,\n                  merchcont_prim_email        as contact_email\n          from    merchcontact\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1994^9*/
        setFields(it.getResultSet());
        it.close();

        // bank account information
        /*@lineinfo:generated-code*//*@lineinfo:1999^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  bankacc_type                as type_of_acct,
//                    merchbank_info_source       as source_of_info,
//                    merchbank_name              as bank_name,
//                    merchbank_acct_num          as checking_account,
//                    merchbank_acct_num          as confirm_checking_account,
//                    merchbank_transit_route_num as transit_routing,
//                    merchbank_transit_route_num as confirm_transit_routing,
//                    merchbank_num_years_open    as years_open
//            from    merchbank
//            where   app_seq_num = :AppSeqNum and
//                    merchbank_acct_srnum = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bankacc_type                as type_of_acct,\n                  merchbank_info_source       as source_of_info,\n                  merchbank_name              as bank_name,\n                  merchbank_acct_num          as checking_account,\n                  merchbank_acct_num          as confirm_checking_account,\n                  merchbank_transit_route_num as transit_routing,\n                  merchbank_transit_route_num as confirm_transit_routing,\n                  merchbank_num_years_open    as years_open\n          from    merchbank\n          where   app_seq_num =  :1  and\n                  merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2012^9*/
        setFields(it.getResultSet());
        it.close();

        // payment options
        /*@lineinfo:generated-code*//*@lineinfo:2017^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code                   as card_type,
//                    merchpo_card_merch_number       as merchant_number,
//                    merchpo_provider_name           as provider_name,
//                    merchpo_rate                    as rate,
//                    merchpo_fee                     as per_item,
//                    merchpo_split_dial              as split_dial,
//                    merchpo_pip                     as amex_pip,
//                    merchpo_tid                     as tid
//            from   merchpayoption
//            where  app_seq_num   = :AppSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code                   as card_type,\n                  merchpo_card_merch_number       as merchant_number,\n                  merchpo_provider_name           as provider_name,\n                  merchpo_rate                    as rate,\n                  merchpo_fee                     as per_item,\n                  merchpo_split_dial              as split_dial,\n                  merchpo_pip                     as amex_pip,\n                  merchpo_tid                     as tid\n          from   merchpayoption\n          where  app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2029^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          switch( resultSet.getInt("card_type") )
          {
            case mesConstants.APP_CT_VISA:      // ignore, always selected
            case mesConstants.APP_CT_MC:
              break;

            case mesConstants.APP_CT_DEBIT:
              fields.getField("debitAccepted").setData("y");
              fields.getField("fcsNumber").setData(resultSet.getString("merchant_number"));
              break;

            case mesConstants.APP_CT_EBT:
              fields.getField("ebtAccepted").setData("y");
              fields.getField("fcsNumber").setData(resultSet.getString("merchant_number"));
              break;

            case mesConstants.APP_CT_DINERS_CLUB:
              fields.getField("dinersAccepted").setData("y");
              fields.getField("dinersAcctNum").setData(resultSet.getString("merchant_number"));
              break;

            case mesConstants.APP_CT_JCB:
              fields.getField("jcbAccepted").setData("y");
              fields.getField("jcbAcctNum").setData(resultSet.getString("merchant_number"));
              break;

            case mesConstants.APP_CT_AMEX:
              fields.getField("amexAccepted").setData("y");
              fields.getField("amexAcctNum").setData(resultSet.getString("merchant_number"));
              fields.getField("amexSplitDial").setData(resultSet.getString("split_dial"));
              fields.getField("amexPIP").setData(resultSet.getString("amex_pip"));
              fields.getField("amexEsaRate").setData(resultSet.getString("rate"));
              break;

            case mesConstants.APP_CT_DISCOVER:
              fields.getField("discoverAccepted").setData("y");
              fields.getField("discoverAcctNum").setData(resultSet.getString("merchant_number"));
              fields.getField("discoverRapRate").setData(resultSet.getString("rate"));
              break;

            case mesConstants.APP_CT_CHECK_AUTH:
              fields.getField("checkAccepted").setData("y");

              String  checkProvider       = resultSet.getString("provider_name");
              String  checkProviderOther  = null;

              // load the provider data, separate other description if necessary
              if( checkProvider.substring(0,7).equals("CPOther") )
              {
                checkProviderOther  = checkProvider.substring(8);
                checkProvider       = checkProvider.substring(0,7);
              }
              fields.setData("checkProvider", checkProvider);
              fields.setData("checkProviderOther", checkProviderOther);
              fields.setData("checkAcctNum", resultSet.getString("merchant_number"));
              fields.setData("checkTermId", resultSet.getString("tid"));
              break;

            case mesConstants.APP_CT_VALUTEC_GIFT_CARD:
              fields.setData("valutecAccepted", "y");
              fields.setData("valutecAcctNum", resultSet.getString("merchant_number"));
              fields.setData("valutecTermId", resultSet.getString("tid"));
              break;

            default:    // ignore
              break;
          }
        }
        resultSet.close();
        it.close();

        // POS type
        /*@lineinfo:generated-code*//*@lineinfo:2106^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mp.pos_code           as internet_type,
//                    mp.pos_param          as pos_param,
//                    pc.pos_type           as product_type,
//                    mp.pp_connectivity    as pos_partner_connectivity,
//                    mp.pp_level_ii        as pos_partner_level_ii,
//                    mp.pp_level_iii       as pos_partner_level_iii,
//                    mp.pp_os              as pos_partner_os,
//                    mp.pg_email           as pg_email,
//                    nvl(mp.existing, 'N') as existing
//            from    merch_pos     mp,
//                    pos_category  pc
//            where   mp.app_seq_num = :AppSeqNum and
//                    pc.pos_code = mp.pos_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mp.pos_code           as internet_type,\n                  mp.pos_param          as pos_param,\n                  pc.pos_type           as product_type,\n                  mp.pp_connectivity    as pos_partner_connectivity,\n                  mp.pp_level_ii        as pos_partner_level_ii,\n                  mp.pp_level_iii       as pos_partner_level_iii,\n                  mp.pp_os              as pos_partner_os,\n                  mp.pg_email           as pg_email,\n                  nvl(mp.existing, 'N') as existing\n          from    merch_pos     mp,\n                  pos_category  pc\n          where   mp.app_seq_num =  :1  and\n                  pc.pos_code = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.app.MerchInfoDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2121^9*/

        resultSet = it.getResultSet();

        if(resultSet.next())
        {
          fields.getField("productType").setData(resultSet.getString("product_type"));
          fields.getField("internetType").setData(resultSet.getString("internet_type"));

          // need to set different fields depending on the value of product_type
          switch(resultSet.getInt("product_type"))
          {

            case mesConstants.POS_OTHER:
              fields.setData("vitalProduct", resultSet.getString("pos_param"));
              break;

            case mesConstants.POS_INTERNET:
              fields.setData("webUrl", resultSet.getString("pos_param"));
              fields.setData("existingInternet", resultSet.getString("existing"));
              fields.setData("internetEmail", resultSet.getString("pg_email"));
              break;

            case mesConstants.POS_PC:
              // set pos partner fields
              fields.setData("posPartnerConnectivity", resultSet.getString("pos_partner_connectivity"));
              fields.setData("posPartnerLevelII", resultSet.getString("pos_partner_level_ii"));
              fields.setData("posPartnerLevelIII", resultSet.getString("pos_partner_level_iii"));
              fields.setData("posPartnerOS", resultSet.getString("pos_partner_os"));
              break;

            case mesConstants.POS_CHARGE_EXPRESS:
              fields.setData("existingPCChargeExpress", resultSet.getString("existing"));
              break;

            case mesConstants.POS_CHARGE_PRO:
              fields.setData("existingPCChargePro", resultSet.getString("existing"));
              break;

            case mesConstants.POS_VIRTUAL_TERMINAL:
              fields.setData("vtEmail", resultSet.getString("pos_param"));
              break;

            case mesConstants.POS_PAYMENT_GATEWAY:
              fields.setData("pgCertName",    resultSet.getString("pos_param"));
              fields.setData("pgEmail",       resultSet.getString("pg_email"));
              fields.setData("pgCard",        resultSet.getString("internet_type"));
              break;

            default:
              break;

          }
        }

        resultSet.close();
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadData()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }

  public void setFields(HttpServletRequest request)
  {
    super.setFields(request);

    // if this is not an internet product, be sure to reset
    // the internet product drop down to none selected
    if ( fields.getField("productType").asInteger() != mesConstants.POS_INTERNET )
    {
      fields.getField("internetType").setData("");
    }
  }

  protected void storeAddressData( int addressType, Field[] fields )
  {
    boolean       skipInsert      = true;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2208^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    address
//          where   app_seq_num = :AppSeqNum and
//                  addresstype_code = :addressType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    address\n        where   app_seq_num =  :1  and\n                addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,addressType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2214^7*/

      try
      {
        // only insert the record if address line 1 and
        // the address city are present.
        if ( !fields[FIDX_ADDR_LINE1].isBlank() &&
             !fields[FIDX_ADDR_CITY].isBlank() )
        {
          skipInsert = false;
        }
      }
      catch( NullPointerException e )
      {
      }

      // only insert address with valid data
      if ( skipInsert == false )
      {
        /*@lineinfo:generated-code*//*@lineinfo:2233^9*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//            (
//              address_line1,
//              address_line2,
//              address_city,
//              countrystate_code,
//              address_zip,
//              country_code,
//              address_phone,
//              address_fax,
//              app_seq_num,
//              addresstype_code
//            )
//            values
//            (
//              :(fields[FIDX_ADDR_LINE1]  == null) ? null : fields[FIDX_ADDR_LINE1].getData(),
//              :(fields[FIDX_ADDR_LINE2]  == null) ? null : fields[FIDX_ADDR_LINE2].getData(),
//              :(fields[FIDX_ADDR_CITY]   == null) ? null : fields[FIDX_ADDR_CITY].getData(),
//              :(fields[FIDX_ADDR_STATE]  == null) ? null : fields[FIDX_ADDR_STATE].getData(),
//              :(fields[FIDX_ADDR_ZIP]    == null) ? null : fields[FIDX_ADDR_ZIP].getData(),
//              :(fields[FIDX_ADDR_COUNTRY]== null) ? null : fields[FIDX_ADDR_COUNTRY].getData(),
//              :(fields[FIDX_ADDR_PHONE]  == null) ? null : fields[FIDX_ADDR_PHONE].getData(),
//              :(fields[FIDX_ADDR_FAX]    == null) ? null : fields[FIDX_ADDR_FAX].getData(),
//              :AppSeqNum,
//              :addressType
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2175 = (fields[FIDX_ADDR_LINE1]  == null) ? null : fields[FIDX_ADDR_LINE1].getData();
 String __sJT_2176 = (fields[FIDX_ADDR_LINE2]  == null) ? null : fields[FIDX_ADDR_LINE2].getData();
 String __sJT_2177 = (fields[FIDX_ADDR_CITY]   == null) ? null : fields[FIDX_ADDR_CITY].getData();
 String __sJT_2178 = (fields[FIDX_ADDR_STATE]  == null) ? null : fields[FIDX_ADDR_STATE].getData();
 String __sJT_2179 = (fields[FIDX_ADDR_ZIP]    == null) ? null : fields[FIDX_ADDR_ZIP].getData();
 String __sJT_2180 = (fields[FIDX_ADDR_COUNTRY]== null) ? null : fields[FIDX_ADDR_COUNTRY].getData();
 String __sJT_2181 = (fields[FIDX_ADDR_PHONE]  == null) ? null : fields[FIDX_ADDR_PHONE].getData();
 String __sJT_2182 = (fields[FIDX_ADDR_FAX]    == null) ? null : fields[FIDX_ADDR_FAX].getData();
   String theSqlTS = "insert into address\n          (\n            address_line1,\n            address_line2,\n            address_city,\n            countrystate_code,\n            address_zip,\n            country_code,\n            address_phone,\n            address_fax,\n            app_seq_num,\n            addresstype_code\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2175);
   __sJT_st.setString(2,__sJT_2176);
   __sJT_st.setString(3,__sJT_2177);
   __sJT_st.setString(4,__sJT_2178);
   __sJT_st.setString(5,__sJT_2179);
   __sJT_st.setString(6,__sJT_2180);
   __sJT_st.setString(7,__sJT_2181);
   __sJT_st.setString(8,__sJT_2182);
   __sJT_st.setLong(9,AppSeqNum);
   __sJT_st.setInt(10,addressType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2261^9*/
      }
    }
    catch(Exception e)
    {
      logEntry( "storeAddressData (" + addressType + "): ", e.toString());
    }
  }

  protected void storeBankData( )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2274^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchbank
//          where   app_seq_num = :AppSeqNum and
//                  merchbank_acct_srnum = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merchbank\n        where   app_seq_num =  :1  and\n                merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2280^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2282^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchbank
//          (
//            app_seq_num,
//            bankacc_type,
//            merchbank_info_source,
//            merchbank_name,
//            merchbank_acct_num,
//            merchbank_transit_route_num,
//            merchbank_num_years_open,
//            merchbank_acct_srnum
//          )
//          values
//          (
//            :AppSeqNum,
//            :fields.getField("typeOfAcct").getData(),
//            :fields.getField("sourceOfInfo").getData(),
//            :fields.getField("bankName").getData(),
//            :fields.getField("checkingAccount").getData(),
//            :fields.getField("transitRouting").getData(),
//            :fields.getField("yearsOpen").getData(),
//            1
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2183 = fields.getField("typeOfAcct").getData();
 String __sJT_2184 = fields.getField("sourceOfInfo").getData();
 String __sJT_2185 = fields.getField("bankName").getData();
 String __sJT_2186 = fields.getField("checkingAccount").getData();
 String __sJT_2187 = fields.getField("transitRouting").getData();
 String __sJT_2188 = fields.getField("yearsOpen").getData();
   String theSqlTS = "insert into merchbank\n        (\n          app_seq_num,\n          bankacc_type,\n          merchbank_info_source,\n          merchbank_name,\n          merchbank_acct_num,\n          merchbank_transit_route_num,\n          merchbank_num_years_open,\n          merchbank_acct_srnum\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n          1\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,__sJT_2183);
   __sJT_st.setString(3,__sJT_2184);
   __sJT_st.setString(4,__sJT_2185);
   __sJT_st.setString(5,__sJT_2186);
   __sJT_st.setString(6,__sJT_2187);
   __sJT_st.setString(7,__sJT_2188);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2306^7*/
    }
    catch(Exception e)
    {
      addError("storeBankData: " + e.toString());
      logEntry("storeBankData()", e.toString());
    }
  }

  protected void storeBusinessOwnerData(int ownerId, Field[] fields)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2319^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    businessowner
//          where   app_seq_num = :AppSeqNum and
//                  busowner_num = :ownerId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    businessowner\n        where   app_seq_num =  :1  and\n                busowner_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,ownerId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2325^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2327^7*/

//  ************************************************************
//  #sql [Ctx] { insert into businessowner
//          (
//            app_seq_num,
//            busowner_num,
//            busowner_last_name,
//            busowner_first_name,
//            busowner_ssn,
//            busowner_owner_perc,
//            busowner_period_month,
//            busowner_period_year,
//            busowner_title
//          )
//          values
//          (
//            :AppSeqNum,
//            :ownerId,
//            :(fields[FIDX_OWNER_LAST_NAME]   == null) ? null : fields[FIDX_OWNER_LAST_NAME].getData(),
//            :(fields[FIDX_OWNER_FIRST_NAME]  == null) ? null : fields[FIDX_OWNER_FIRST_NAME].getData(),
//            :(fields[FIDX_OWNER_SSN]         == null) ? null : fields[FIDX_OWNER_SSN].getData(),
//            :(fields[FIDX_OWNER_PERCENT]     == null) ? null : fields[FIDX_OWNER_PERCENT].getData(),
//            :(fields[FIDX_OWNER_MONTH]       == null) ? null : fields[FIDX_OWNER_MONTH].getData(),
//            :(fields[FIDX_OWNER_YEAR]        == null) ? null : fields[FIDX_OWNER_YEAR].getData(),
//            :(fields[FIDX_OWNER_TITLE]       == null) ? null : fields[FIDX_OWNER_TITLE].getData()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2189 = (fields[FIDX_OWNER_LAST_NAME]   == null) ? null : fields[FIDX_OWNER_LAST_NAME].getData();
 String __sJT_2190 = (fields[FIDX_OWNER_FIRST_NAME]  == null) ? null : fields[FIDX_OWNER_FIRST_NAME].getData();
 String __sJT_2191 = (fields[FIDX_OWNER_SSN]         == null) ? null : fields[FIDX_OWNER_SSN].getData();
 String __sJT_2192 = (fields[FIDX_OWNER_PERCENT]     == null) ? null : fields[FIDX_OWNER_PERCENT].getData();
 String __sJT_2193 = (fields[FIDX_OWNER_MONTH]       == null) ? null : fields[FIDX_OWNER_MONTH].getData();
 String __sJT_2194 = (fields[FIDX_OWNER_YEAR]        == null) ? null : fields[FIDX_OWNER_YEAR].getData();
 String __sJT_2195 = (fields[FIDX_OWNER_TITLE]       == null) ? null : fields[FIDX_OWNER_TITLE].getData();
   String theSqlTS = "insert into businessowner\n        (\n          app_seq_num,\n          busowner_num,\n          busowner_last_name,\n          busowner_first_name,\n          busowner_ssn,\n          busowner_owner_perc,\n          busowner_period_month,\n          busowner_period_year,\n          busowner_title\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,ownerId);
   __sJT_st.setString(3,__sJT_2189);
   __sJT_st.setString(4,__sJT_2190);
   __sJT_st.setString(5,__sJT_2191);
   __sJT_st.setString(6,__sJT_2192);
   __sJT_st.setString(7,__sJT_2193);
   __sJT_st.setString(8,__sJT_2194);
   __sJT_st.setString(9,__sJT_2195);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2353^7*/
    }
    catch(Exception e)
    {
      addError("storeBusinessOwnerData: " + e.toString());
      logEntry("storeBusinessOwnerData( " + ownerId + " )", e.toString());
    }
  }

  protected void storeContactData( )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2366^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchcontact
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merchcontact\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2371^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2373^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchcontact
//          (
//            app_seq_num,
//            merchcont_prim_first_name,
//            merchcont_prim_last_name,
//            merchcont_prim_phone,
//            merchcont_prim_phone_ext,
//            merchcont_prim_email
//          )
//          values
//          (
//            :AppSeqNum,
//            :fields.getField("contactNameFirst").getData(),
//            :fields.getField("contactNameLast").getData(),
//            :fields.getField("contactPhone").getData(),
//            :fields.getField("contactPhoneExt").getData(),
//            :fields.getField("contactEmail").getData()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2196 = fields.getField("contactNameFirst").getData();
 String __sJT_2197 = fields.getField("contactNameLast").getData();
 String __sJT_2198 = fields.getField("contactPhone").getData();
 String __sJT_2199 = fields.getField("contactPhoneExt").getData();
 String __sJT_2200 = fields.getField("contactEmail").getData();
   String theSqlTS = "insert into merchcontact\n        (\n          app_seq_num,\n          merchcont_prim_first_name,\n          merchcont_prim_last_name,\n          merchcont_prim_phone,\n          merchcont_prim_phone_ext,\n          merchcont_prim_email\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,__sJT_2196);
   __sJT_st.setString(3,__sJT_2197);
   __sJT_st.setString(4,__sJT_2198);
   __sJT_st.setString(5,__sJT_2199);
   __sJT_st.setString(6,__sJT_2200);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2393^7*/
    }
    catch(Exception e)
    {
      addError("storeContactData: " + e.toString());
      logEntry("storeContactData()", e.toString());
    }
  }

  public void storeData()
  {
    try
    {
      if ( AppSeqNum == APP_SEQ_NEW )
      {
        // this will generate a new app sequence nubmer and
        // app control number then create a new entry in
        // the application and merchant tables
        newAppInit();
      }

      // update the merchant table data
      storeMerchantData();

      // store the addresses
      storeAddressData(mesConstants.ADDR_TYPE_BUSINESS,
                        new Field[]{  fields.getField("businessAddress1"),
                                      fields.getField("businessAddress2"),
                                      fields.getField("businessCity"),
                                      fields.getField("businessState"),
                                      fields.getField("businessZip"),
                                      fields.getField("businessCountry"),
                                      fields.getField("businessPhone"),
                                      fields.getField("businessFax") } );

      storeAddressData(mesConstants.ADDR_TYPE_MAILING,
                        new Field[]{  fields.getField("mailingAddress1"),
                                      fields.getField("mailingAddress2"),
                                      fields.getField("mailingCity"),
                                      fields.getField("mailingState"),
                                      fields.getField("mailingZip"),
                                      null,
                                      null,
                                      null } );

      storeAddressData(mesConstants.ADDR_TYPE_OWNER1,
                        new Field[]{  fields.getField("owner1Address1"),
                                      null,
                                      fields.getField("owner1City"),
                                      fields.getField("owner1State"),
                                      fields.getField("owner1Zip"),
                                      null,
                                      fields.getField("owner1Phone"),
                                      null } );

      storeAddressData(mesConstants.ADDR_TYPE_OWNER2,
                        new Field[]{  fields.getField("owner2Address1"),
                                      null,
                                      fields.getField("owner2City"),
                                      fields.getField("owner2State"),
                                      fields.getField("owner2Zip"),
                                      null,
                                      fields.getField("owner2Phone"),
                                      null } );

      storeAddressData(mesConstants.ADDR_TYPE_CHK_ACCT_BANK,
                        new Field[]{  fields.getField("bankAddress"),
                                      null,
                                      fields.getField("bankCity"),
                                      fields.getField("bankState"),
                                      fields.getField("bankZip"),
                                      null,
                                      fields.getField("bankPhone"),
                                      null } );


      storeBusinessOwnerData( 1, new Field[]{ fields.getField("owner1LastName"),
                                              fields.getField("owner1FirstName"),
                                              fields.getField("owner1SSN"),
                                              fields.getField("owner1Percent"),
                                              fields.getField("owner1SinceMonth"),
                                              fields.getField("owner1SinceYear"),
                                              fields.getField("owner1Title") } );

      storeBusinessOwnerData( 2, new Field[]{ fields.getField("owner2LastName"),
                                              fields.getField("owner2FirstName"),
                                              fields.getField("owner2SSN"),
                                              fields.getField("owner2Percent"),
                                              fields.getField("owner2SinceMonth"),
                                              fields.getField("owner2SinceYear"),
                                              fields.getField("owner2Title") } );

      // store the remainder of
      // the data for this page
      storeContactData();         // contact data for this application
      storeBankData();            // bank data
      storePayOptions();          // accepted cards
      storePosData();             // pos type
      storeEquipmentData();       // equipment related
      storeWarnings();            // warnings the app submission generated

      markPageComplete();         // mark this page done in screen_progress
    }
    catch( Exception e )
    {
      logEntry("storeData()",e.toString());
    }
    finally
    {
    }
  }

  protected void storeEquipmentData()
  {
    int     imprinterPlateCount   = fields.getField("imprinterPlates").asInteger();
    int     productType           = fields.getField("productType").asInteger();

    try
    {
      switch( productType )
      {
        case mesConstants.POS_DIAL_TERMINAL:
        case mesConstants.POS_WIRELESS_TERMINAL:
          // delete any entries for a PC product but leave
          // the other equipment rows in alone.  these
          // rows are manipulated through the equipment
          // selection page of the application.
          /*@lineinfo:generated-code*//*@lineinfo:2520^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    merchequipment
//              where   app_seq_num = :AppSeqNum and
//                      equip_model = 'PCPS'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    merchequipment\n            where   app_seq_num =  :1  and\n                    equip_model = 'PCPS'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2526^11*/
          break;

        case mesConstants.POS_DIAL_AUTH:
          // delete everything except imprinters.
          // imprinters are setup through the equipment
          // selection page when the POS type is dial pay
          /*@lineinfo:generated-code*//*@lineinfo:2533^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    merchequipment
//              where   app_seq_num = :AppSeqNum and
//                      equiptype_code <> :mesConstants.APP_EQUIP_TYPE_IMPRINTER
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    merchequipment\n            where   app_seq_num =  :1  and\n                    equiptype_code <>  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_IMPRINTER);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2539^11*/
          break;

        case mesConstants.POS_STAGE_ONLY:
        case mesConstants.POS_GLOBAL_PC:
          // leave merchant equipment alone for these types
          break;

//        case mesConstants.POS_PC:
//        case mesConstants.POS_INTERNET:
//        case mesConstants.POS_OTHER:
//        case mesConstants.POS_GPS:
        default:
          // delete all the equipment for this application
          /*@lineinfo:generated-code*//*@lineinfo:2553^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    merchequipment
//              where   app_seq_num = :AppSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    merchequipment\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2558^11*/
          break;
      }

      // if this is a PC product, create a row for the PC software
      if (productType == mesConstants.POS_PC)
      {
        /*@lineinfo:generated-code*//*@lineinfo:2565^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchequipment
//            (
//              app_seq_num,
//              merchequip_equip_quantity,
//              equiplendtype_code,
//              equiptype_code,
//              equip_model
//            )
//            values
//            (
//              :AppSeqNum,
//              1,        -- one item
//              :mesConstants.APP_EQUIP_PURCHASE,
//              :mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE,
//              'PCPS'
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchequipment\n          (\n            app_seq_num,\n            merchequip_equip_quantity,\n            equiplendtype_code,\n            equiptype_code,\n            equip_model\n          )\n          values\n          (\n             :1 ,\n            1,        -- one item\n             :2 ,\n             :3 ,\n            'PCPS'\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"29com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2583^9*/
      }

      if( imprinterPlateCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:2588^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    merchequipment
//            where   app_seq_num = :AppSeqNum and
//                    equiptype_code = :mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    merchequipment\n          where   app_seq_num =  :1  and\n                  equiptype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2594^9*/

        /*@lineinfo:generated-code*//*@lineinfo:2596^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchequipment
//            (
//              app_seq_num,
//              equiptype_code,
//              equiplendtype_code,
//              merchequip_amount,
//              equip_model,
//              merchequip_equip_quantity,
//              prod_option_id
//            )
//            values
//            (
//              :AppSeqNum,
//              :mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES,
//              :mesConstants.APP_EQUIP_PURCHASE,
//              null,
//              'IPPL',
//              :imprinterPlateCount,
//              null
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchequipment\n          (\n            app_seq_num,\n            equiptype_code,\n            equiplendtype_code,\n            merchequip_amount,\n            equip_model,\n            merchequip_equip_quantity,\n            prod_option_id\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n            null,\n            'IPPL',\n             :4 ,\n            null\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"31com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(4,imprinterPlateCount);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2618^9*/
      }
    }
    catch (Exception e)
    {
      addError("storeEquipmentData: " + e.toString());
      logEntry("storeEquipmentData()", e.toString());
    }
  }

  protected void storeMerchantData()
  {
    try
    {
      String seasonalMonths = fields.getData("seasonalMonths");
      String seasonalFlag =
        (seasonalMonths.equals("NNNNNNNNNNNN") ? "Y" : "N");

      /*@lineinfo:generated-code*//*@lineinfo:2636^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_business_name         = :fields.getField("businessName").getData(),
//                  merch_federal_tax_id        = :fields.getField("taxpayerId").getData(),
//                  merch_email_address         = :fields.getField("businessEmail").getData(),
//                  merch_legal_name            = :fields.getField("businessLegalName").getData(),
//                  merch_mailing_name          = :fields.getField("mailingName").getData(),
//                  merch_business_establ_month = :fields.getField("establishedMonth").getData(),
//                  merch_business_establ_year  = :fields.getField("establishedYear").getData(),
//                  bustype_code                = :fields.getField("businessType").getData(),
//                  industype_code              = :fields.getField("industryType").getData(),
//                  merch_busgoodserv_descr     = :fields.getField("businessDesc").getData(),
//                  merch_num_of_locations      = :fields.getField("numLocations").getData(),
//                  merch_years_at_loc          = :fields.getField("locationYears").getData(),
//                  loctype_code                = :fields.getField("locationType").getData(),
//                  merch_application_type      = :fields.getField("applicationType").getData(),
//                  merch_prior_cc_accp_flag    = :fields.getField("haveProcessed").getData(),
//                  merch_prior_processor       = :fields.getField("previousProcessor").getData(),
//                  merch_cc_acct_term_flag     = :fields.getField("haveCanceled").getData(),
//                  merch_cc_term_name          = :fields.getField("canceledProcessor").getData(),
//                  merch_term_reason           = :fields.getField("canceledReason").getData(),
//                  merch_term_month            = :fields.getField("cancelMonth").getData(),
//                  merch_term_year             = :fields.getField("cancelYear").getData(),
//                  merch_month_tot_proj_sales  = :fields.getField("monthlySales").getData(),
//                  merch_month_visa_mc_sales   = :fields.getField("monthlyVMCSales").getData(),
//                  merch_average_cc_tran       = :fields.getField("averageTicket").getData(),
//                  merch_mail_phone_sales      = :fields.getField("motoPercentage").getData(),
//                  merch_notes                 = :fields.getField("comments").getData(),
//                  refundtype_code             = :fields.getField("refundPolicy").getData(),
//                  merch_referring_bank        = :fields.getField("referringBank").getData(),
//                  merch_prior_statements      = :fields.getField("statementsProvided").getData(),
//                  merch_web_url               = :fields.getField("webUrl").getData(),
//                  app_sic_code                = :fields.getField("sicCode").getData(),
//                  merch_gender                = :fields.getField("owner1Gender").getData(),
//                  asso_number                 = :fields.getField("assocNum").getData(),
//                  account_type                = :fields.getField("accountType").getData(),
//                  merch_rep_code              = :fields.getField("repCode").getData(),
//                  svb_cif_num                 = :fields.getField("cif").getData(),
//                  seasonal_flag               = :seasonalFlag,
//                  seasonal_months             = :seasonalMonths,
//                  percent_internet            = :fields.getData("internetPercent"),
//                  percent_card_present        = :fields.getData("cardPercent"),
//                  percent_card_not_present    = :fields.getData("noCardPercent"),
//                  customer_service_phone      = :fields.getData("customerServicePhone")
//          where   app_seq_num                 = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2201 = fields.getField("businessName").getData();
 String __sJT_2202 = fields.getField("taxpayerId").getData();
 String __sJT_2203 = fields.getField("businessEmail").getData();
 String __sJT_2204 = fields.getField("businessLegalName").getData();
 String __sJT_2205 = fields.getField("mailingName").getData();
 String __sJT_2206 = fields.getField("establishedMonth").getData();
 String __sJT_2207 = fields.getField("establishedYear").getData();
 String __sJT_2208 = fields.getField("businessType").getData();
 String __sJT_2209 = fields.getField("industryType").getData();
 String __sJT_2210 = fields.getField("businessDesc").getData();
 String __sJT_2211 = fields.getField("numLocations").getData();
 String __sJT_2212 = fields.getField("locationYears").getData();
 String __sJT_2213 = fields.getField("locationType").getData();
 String __sJT_2214 = fields.getField("applicationType").getData();
 String __sJT_2215 = fields.getField("haveProcessed").getData();
 String __sJT_2216 = fields.getField("previousProcessor").getData();
 String __sJT_2217 = fields.getField("haveCanceled").getData();
 String __sJT_2218 = fields.getField("canceledProcessor").getData();
 String __sJT_2219 = fields.getField("canceledReason").getData();
 String __sJT_2220 = fields.getField("cancelMonth").getData();
 String __sJT_2221 = fields.getField("cancelYear").getData();
 String __sJT_2222 = fields.getField("monthlySales").getData();
 String __sJT_2223 = fields.getField("monthlyVMCSales").getData();
 String __sJT_2224 = fields.getField("averageTicket").getData();
 String __sJT_2225 = fields.getField("motoPercentage").getData();
 String __sJT_2226 = fields.getField("comments").getData();
 String __sJT_2227 = fields.getField("refundPolicy").getData();
 String __sJT_2228 = fields.getField("referringBank").getData();
 String __sJT_2229 = fields.getField("statementsProvided").getData();
 String __sJT_2230 = fields.getField("webUrl").getData();
 String __sJT_2231 = fields.getField("sicCode").getData();
 String __sJT_2232 = fields.getField("owner1Gender").getData();
 String __sJT_2233 = fields.getField("assocNum").getData();
 String __sJT_2234 = fields.getField("accountType").getData();
 String __sJT_2235 = fields.getField("repCode").getData();
 String __sJT_2236 = fields.getField("cif").getData();
 String __sJT_2237 = fields.getData("internetPercent");
 String __sJT_2238 = fields.getData("cardPercent");
 String __sJT_2239 = fields.getData("noCardPercent");
 String __sJT_2240 = fields.getData("customerServicePhone");
   String theSqlTS = "update  merchant\n        set     merch_business_name         =  :1 ,\n                merch_federal_tax_id        =  :2 ,\n                merch_email_address         =  :3 ,\n                merch_legal_name            =  :4 ,\n                merch_mailing_name          =  :5 ,\n                merch_business_establ_month =  :6 ,\n                merch_business_establ_year  =  :7 ,\n                bustype_code                =  :8 ,\n                industype_code              =  :9 ,\n                merch_busgoodserv_descr     =  :10 ,\n                merch_num_of_locations      =  :11 ,\n                merch_years_at_loc          =  :12 ,\n                loctype_code                =  :13 ,\n                merch_application_type      =  :14 ,\n                merch_prior_cc_accp_flag    =  :15 ,\n                merch_prior_processor       =  :16 ,\n                merch_cc_acct_term_flag     =  :17 ,\n                merch_cc_term_name          =  :18 ,\n                merch_term_reason           =  :19 ,\n                merch_term_month            =  :20 ,\n                merch_term_year             =  :21 ,\n                merch_month_tot_proj_sales  =  :22 ,\n                merch_month_visa_mc_sales   =  :23 ,\n                merch_average_cc_tran       =  :24 ,\n                merch_mail_phone_sales      =  :25 ,\n                merch_notes                 =  :26 ,\n                refundtype_code             =  :27 ,\n                merch_referring_bank        =  :28 ,\n                merch_prior_statements      =  :29 ,\n                merch_web_url               =  :30 ,\n                app_sic_code                =  :31 ,\n                merch_gender                =  :32 ,\n                asso_number                 =  :33 ,\n                account_type                =  :34 ,\n                merch_rep_code              =  :35 ,\n                svb_cif_num                 =  :36 ,\n                seasonal_flag               =  :37 ,\n                seasonal_months             =  :38 ,\n                percent_internet            =  :39 ,\n                percent_card_present        =  :40 ,\n                percent_card_not_present    =  :41 ,\n                customer_service_phone      =  :42 \n        where   app_seq_num                 =  :43";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"32com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2201);
   __sJT_st.setString(2,__sJT_2202);
   __sJT_st.setString(3,__sJT_2203);
   __sJT_st.setString(4,__sJT_2204);
   __sJT_st.setString(5,__sJT_2205);
   __sJT_st.setString(6,__sJT_2206);
   __sJT_st.setString(7,__sJT_2207);
   __sJT_st.setString(8,__sJT_2208);
   __sJT_st.setString(9,__sJT_2209);
   __sJT_st.setString(10,__sJT_2210);
   __sJT_st.setString(11,__sJT_2211);
   __sJT_st.setString(12,__sJT_2212);
   __sJT_st.setString(13,__sJT_2213);
   __sJT_st.setString(14,__sJT_2214);
   __sJT_st.setString(15,__sJT_2215);
   __sJT_st.setString(16,__sJT_2216);
   __sJT_st.setString(17,__sJT_2217);
   __sJT_st.setString(18,__sJT_2218);
   __sJT_st.setString(19,__sJT_2219);
   __sJT_st.setString(20,__sJT_2220);
   __sJT_st.setString(21,__sJT_2221);
   __sJT_st.setString(22,__sJT_2222);
   __sJT_st.setString(23,__sJT_2223);
   __sJT_st.setString(24,__sJT_2224);
   __sJT_st.setString(25,__sJT_2225);
   __sJT_st.setString(26,__sJT_2226);
   __sJT_st.setString(27,__sJT_2227);
   __sJT_st.setString(28,__sJT_2228);
   __sJT_st.setString(29,__sJT_2229);
   __sJT_st.setString(30,__sJT_2230);
   __sJT_st.setString(31,__sJT_2231);
   __sJT_st.setString(32,__sJT_2232);
   __sJT_st.setString(33,__sJT_2233);
   __sJT_st.setString(34,__sJT_2234);
   __sJT_st.setString(35,__sJT_2235);
   __sJT_st.setString(36,__sJT_2236);
   __sJT_st.setString(37,seasonalFlag);
   __sJT_st.setString(38,seasonalMonths);
   __sJT_st.setString(39,__sJT_2237);
   __sJT_st.setString(40,__sJT_2238);
   __sJT_st.setString(41,__sJT_2239);
   __sJT_st.setString(42,__sJT_2240);
   __sJT_st.setLong(43,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2682^7*/
    }
    catch (Exception e)
    {
      logEntry( "storeMerchantData()", e.toString() );
    }
    finally
    {
    }
  }

  protected void storePayOptions()
  {
    String        acctId            = null;
    String        amexSplitDial     = null;
    String        amexPIP           = null;
    int           appCardType       = 0;
    boolean       accepted          = false;
    String        cardTid           = null;
    String        fee               = "";
    String        fname             = null;
    String        providerName      = "";
    String        rate              = "";
    int           recId             = 0;


    try
    {
      for (int ct = 0; ct < CT_COUNT; ++ct)
      {
        // reset the per card type elements
        cardTid       = null;
        rate          = null;
        fee           = null;
        providerName  = null;
        amexSplitDial = "N";
        amexPIP       = "N";

        // extract the accepted flag
        fname = CardTypeToFieldNames[ct][FNAME_ACCEPTED];
        if ( fname == null )
        {
          continue;     // skip this entry
        }
        accepted = fields.getField(fname).getData().toUpperCase().equals("Y");


        // extract the account number
        try
        {
          fname   = CardTypeToFieldNames[ct][FNAME_ACCOUNT_NUMBER];
          acctId  = fields.getField(fname).getData();
        }
        catch( NullPointerException e )
        {
          // no account number for this card type
          acctId  = null;
        }
        cardTid = acctId;     // default tid to the same value as mid

        // convert the card type index into the application
        // specific card type using the lookup array
        appCardType = CardTypeToAppCardType[ct];

        // extract data from card type specific fields
        switch (ct)
        {
          case CT_DISCOVER:
            rate          = fields.getField("discoverRapRate").getData();
            break;

          case CT_AMEX:
            rate          = fields.getField("amexEsaRate").getData();
            amexSplitDial = fields.getField("amexSplitDial").getData().toUpperCase();
            amexPIP       = fields.getField("amexPIP").getData().toUpperCase();
            break;

          case CT_CHECK:
            providerName  = fields.getField("checkProvider").getData();
            if ( providerName.equals("CPOther") )
            {
              // change to "CPOther:Some Check Provider Name"
              // this will be decoded when reading the data back
              // from the database table.
              providerName += (":" + fields.getData("checkProviderOther"));
            }
            cardTid = fields.getData("checkTermId");
            break;

          case CT_VALUTEC:
            cardTid = fields.getData("valutecTermId");
            break;
        }

        // remove any entries for this card type
        // from the table before inserting
        /*@lineinfo:generated-code*//*@lineinfo:2778^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    merchpayoption
//            where   app_seq_num = :AppSeqNum and
//                    cardtype_code = :appCardType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    merchpayoption\n          where   app_seq_num =  :1  and\n                  cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,appCardType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2784^9*/

        if ( accepted == true )
        {
          recId++;        // increment the row id

          /*@lineinfo:generated-code*//*@lineinfo:2790^11*/

//  ************************************************************
//  #sql [Ctx] { insert into  merchpayoption
//              (
//                app_seq_num,
//                cardtype_code,
//                merchpo_card_merch_number,
//                merchpo_provider_name,
//                card_sr_number,
//                merchpo_rate,
//                merchpo_split_dial,
//                merchpo_pip,
//                merchpo_fee
//              )
//              values
//              (
//                :AppSeqNum,
//                :appCardType,
//                :acctId,
//                :providerName,
//                :recId,
//                :rate,
//                :amexSplitDial,   -- n/a unless amex
//                :amexPIP,         -- n/a unless amex
//                :fee
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into  merchpayoption\n            (\n              app_seq_num,\n              cardtype_code,\n              merchpo_card_merch_number,\n              merchpo_provider_name,\n              card_sr_number,\n              merchpo_rate,\n              merchpo_split_dial,\n              merchpo_pip,\n              merchpo_fee\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n               :7 ,   -- n/a unless amex\n               :8 ,         -- n/a unless amex\n               :9 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"34com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,appCardType);
   __sJT_st.setString(3,acctId);
   __sJT_st.setString(4,providerName);
   __sJT_st.setInt(5,recId);
   __sJT_st.setString(6,rate);
   __sJT_st.setString(7,amexSplitDial);
   __sJT_st.setString(8,amexPIP);
   __sJT_st.setString(9,fee);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2816^11*/
        }
        else      // card type is not accepted
        {
          // remove any tranchrg entries that have already
          // bees setup for this card type
          /*@lineinfo:generated-code*//*@lineinfo:2822^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    tranchrg
//              where   app_seq_num = :AppSeqNum and
//                      cardtype_code = :appCardType
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    tranchrg\n            where   app_seq_num =  :1  and\n                    cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"35com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,appCardType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2828^11*/
        }
      }
    }
    catch(Exception e)
    {
      addError("storePayOptions: " + e.toString());
      logEntry("storePayOptions()", e.toString());
    }
  }

  protected void storePosData( )
  {
    String                  edcFlag       = "N";
    String                  param         = null;
    int                     posCode       = 0;
    int                     productType   = 0;
    String                  posExisting   = "N";

    String  pgEmail                       = null;
    String  pgECard                       = null;

    try
    {
      // extract an integer version of the product type from the form field
      productType = fields.getField("productType").asInteger();

      // delete the rows for this sequence number
      /*@lineinfo:generated-code*//*@lineinfo:2856^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merch_pos
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merch_pos\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2861^7*/

      // set the product specific values
      switch ( productType )
      {
        case mesConstants.POS_WIRELESS_TERMINAL:
          posCode = mesConstants.APP_MPOS_WIRELESS_TERMINAL;
          edcFlag = "Y";
          break;

        case mesConstants.POS_PC:
          posCode = mesConstants.APP_MPOS_POS_PARTNER_2000;
          break;

        case mesConstants.POS_INTERNET:
          posCode = Integer.parseInt( fields.getField("internetType").getData() );
          param   = fields.getField("webUrl").getData();
          //reusing pgEmail field in DB for admin email here
          pgEmail = fields.getField("internetEmail").getData();
          posExisting = fields.getField("existingInternet").getData().toUpperCase();
          break;

        case mesConstants.POS_VIRTUAL_TERMINAL:
          posCode = mesConstants.APP_MPOS_VIRTUAL_TERMINAL;
          param = getData("vtEmail");
          break;

        case mesConstants.POS_DIAL_AUTH:
          posCode = mesConstants.APP_MPOS_DIAL_PAY;
          break;

        case mesConstants.POS_OTHER:
          posCode = mesConstants.APP_MPOS_OTHER_VITAL_PRODUCT;
          param   = fields.getField("vitalProduct").getData();
          break;

        case mesConstants.POS_STAGE_ONLY:
          posCode = mesConstants.APP_MPOS_STAGE_ONLY;
          break;

        case mesConstants.POS_GLOBAL_PC:
          posCode = mesConstants.APP_MPOS_GLOBAL_PC_WINDOWS;
          break;

        case mesConstants.POS_GPS:
          posCode = mesConstants.APP_MPOS_GPS;
          break;

        case mesConstants.POS_PAYMENT_GATEWAY:
          posCode       = fields.getField("pgCard").asInteger();
          param         = getData("pgCertName");
          pgEmail       = getData("pgEmail");
          break;

//        case mesConstants.POS_DIAL_TERMINAL:
        default:
          posCode = mesConstants.APP_MPOS_DIAL_TERMINAL;
          edcFlag = "Y";
          break;
      }

      // delete tran charge records for internet and dialpay
      // except when the current product is one of those products
      // this insures that the tranchrg table will get cleaned up
      // in the event that the product is changed from internet or
      // dialpay to something different.
      /*@lineinfo:generated-code*//*@lineinfo:2927^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    tranchrg
//          where   app_seq_num = :AppSeqNum and
//                  cardtype_code in
//                    (
//                      :mesConstants.APP_CT_INTERNET, -- 20,
//                      :mesConstants.APP_CT_DIAL_PAY  -- 21
//                    ) and
//                  cardtype_code <> :posCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    tranchrg\n        where   app_seq_num =  :1  and\n                cardtype_code in\n                  (\n                     :2 , -- 20,\n                     :3   -- 21\n                  ) and\n                cardtype_code <>  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_INTERNET);
   __sJT_st.setInt(3,mesConstants.APP_CT_DIAL_PAY);
   __sJT_st.setInt(4,posCode);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2938^7*/

      // update the EDC flag in the merchant table based on the
      // new product selection
      /*@lineinfo:generated-code*//*@lineinfo:2942^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_edc_flag = :edcFlag
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     merch_edc_flag =  :1 \n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"38com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,edcFlag);
   __sJT_st.setLong(2,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2947^7*/

      if( productType == mesConstants.POS_PC &&
          fields.getField("posPartnerConnectivity") != null)
      {
        /*@lineinfo:generated-code*//*@lineinfo:2952^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_pos
//            (
//              app_seq_num,
//              pos_code,
//              pos_param,
//              pp_connectivity,
//              pp_level_ii,
//              pp_level_iii,
//              pp_os
//            )
//            values
//            (
//              :AppSeqNum,
//              :posCode,
//              :param,
//              :fields.getField("posPartnerConnectivity").asInteger(),
//              upper(:fields.getData("posPartnerLevelII")),
//              upper(:fields.getData("posPartnerLevelIII")),
//              :fields.getField("posPartnerOS").asInteger()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2241 = fields.getField("posPartnerConnectivity").asInteger();
 String __sJT_2242 = fields.getData("posPartnerLevelII");
 String __sJT_2243 = fields.getData("posPartnerLevelIII");
 int __sJT_2244 = fields.getField("posPartnerOS").asInteger();
   String theSqlTS = "insert into merch_pos\n          (\n            app_seq_num,\n            pos_code,\n            pos_param,\n            pp_connectivity,\n            pp_level_ii,\n            pp_level_iii,\n            pp_os\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n            upper( :5 ),\n            upper( :6 ),\n             :7 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"39com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,posCode);
   __sJT_st.setString(3,param);
   __sJT_st.setInt(4,__sJT_2241);
   __sJT_st.setString(5,__sJT_2242);
   __sJT_st.setString(6,__sJT_2243);
   __sJT_st.setInt(7,__sJT_2244);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2974^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:2978^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_pos
//            (
//              app_seq_num,
//              pos_code,
//              pos_param,
//              pg_email,
//              existing
//            )
//            values
//            (
//              :AppSeqNum,
//              :posCode,
//              :param,
//              :pgEmail,
//              :posExisting
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_pos\n          (\n            app_seq_num,\n            pos_code,\n            pos_param,\n            pg_email,\n            existing\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"40com.mes.app.MerchInfoDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,posCode);
   __sJT_st.setString(3,param);
   __sJT_st.setString(4,pgEmail);
   __sJT_st.setString(5,posExisting);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2996^9*/
      }
    }
    catch (Exception e)
    {
      addError("storePosData: " + e.toString());
      logEntry("storePosData()", e.toString());
    }
  }
}/*@lineinfo:generated-code*/