/*@lineinfo:filename=Pricing*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/sabre/Pricing.sqlj $

  Description:

  Pricing

  Sabre online app pricing page bean.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-02-01 16:39:25 -0800 (Fri, 01 Feb 2008) $
  Version            : $Revision: 14544 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.sabre;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.AppBase;
import com.mes.constants.mesConstants;
import com.mes.forms.CheckboxField;
import com.mes.forms.ConstantField;
import com.mes.forms.HiddenField;
import com.mes.forms.Validation;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class Pricing extends AppBase
{
  {
    appType = mesConstants.APP_TYPE_SABRE;
    curScreenId = 2;
  }
  
  public class MustCheckValidation implements Validation
  {
    String          ErrorMessage      = "You must accept this agreement to continue";
    
    public MustCheckValidation( )
    {
    }
    
    public String getErrorText()
    {
      return( ErrorMessage );
    }
    
    public boolean validate(String fieldData)
    {
      boolean     retVal    = true;
      
      if ( fieldData == null || !fieldData.toUpperCase().equals("Y") )
      {
        retVal = false;
      }
      return( retVal );
    }
  }

  public boolean allowedToSubmit()
  {
    boolean allowed = false;
    try
    {
      allowed = appSeq.getCurrentScreen().isComplete()
            || !fields.getField("actionAllowSubmit").isBlank();
    }
    catch (Exception e) 
    {
    }
    return( allowed );
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      connect();
      
      // pricing settings 
      fields.add(new CheckboxField("agreeToPricing","I Accept",false));
      fields.getField("agreeToPricing").addValidation( new MustCheckValidation() );
      fields.add(new HiddenField("pricingPlan"));
      fields.add(new HiddenField("discRate"));
      fields.add(new HiddenField("perItem"));
      fields.add(new HiddenField("minDiscountAmount"));
      fields.add(new HiddenField("chargebackFee"));

      // view only fields for each possible card type 
      fields.add(new ConstantField( "cardType" + mesConstants.APP_CT_VISA,       
                                    "Visa",
                                    "n",
                                    "formFieldsDim"));
      fields.add(new ConstantField( "cardType" + mesConstants.APP_CT_MC,
                                    "MasterCard",
                                    "n",
                                    "formFieldsDim"));
      fields.add(new ConstantField( "cardType" + mesConstants.APP_CT_DEBIT,
                                    "Debit",
                                    "n",
                                    "formFieldsDim"));
      fields.add(new ConstantField( "cardType" + mesConstants.APP_CT_DINERS_CLUB,
                                    "Diners",  
                                    "n",
                                    "formFieldsDim"));
      fields.add(new ConstantField( "cardType" + mesConstants.APP_CT_JCB,        
                                    "JCB",              
                                    "n",
                                    "formFieldsDim"));
      fields.add(new ConstantField( "cardType" + mesConstants.APP_CT_AMEX,       
                                    "American Express", 
                                    "n",
                                    "formFieldsDim"));
      fields.add(new ConstantField( "cardType" + mesConstants.APP_CT_DISCOVER,   
                                    "Discover Card", 
                                    "n",
                                    "formFieldsDim"));
      fields.add(new ConstantField( "cardType" + mesConstants.APP_CT_PRIVATE_LABEL_1,
                                    "UATP Card",
                                    "n",
                                    "formFieldsDim"));
      
      // set the app type and screen id
      fields.setData("appType",Integer.toString(mesConstants.APP_TYPE_SABRE));
      fields.setData("curScreenId","2");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  protected boolean loadAppData()
  {
    boolean loadOk = false;
    ResultSetIterator it = null;
    try
    {
      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        connect();
        
        loadCards(appSeqNum);
        loadMiscFees(appSeqNum);
        loadTransactionFees(appSeqNum);
      }
      loadOk = true;
    }
    catch(Exception e)
    {
      logEntry("loadAppData()",e.toString());
      System.out.println(this.getClass().getName() + "::loadAppData(): " 
        + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( loadOk );
  }

  protected void loadCards( long appSeqNum ) 
  {
    ResultSetIterator   it        = null;
    ResultSet           resultSet = null;
    
    try
    {
      // payment options
      /*@lineinfo:generated-code*//*@lineinfo:199^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code               card_type,
//                  merchpo_card_merch_number   merchant_number,
//                  merchpo_provider_name       provider_name,
//                  merchpo_rate                rate,
//                  merchpo_fee                 per_item,
//                  merchpo_split_dial          split_dial,
//                  merchpo_pip                 amex_pip
//          from   merchpayoption
//          where  app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code               card_type,\n                merchpo_card_merch_number   merchant_number,\n                merchpo_provider_name       provider_name,\n                merchpo_rate                rate,\n                merchpo_fee                 per_item,\n                merchpo_split_dial          split_dial,\n                merchpo_pip                 amex_pip\n        from   merchpayoption\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.sabre.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.sabre.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:210^7*/
      resultSet = it.getResultSet();
      
      while (resultSet.next())
      {
        setData(("cardType" + resultSet.getInt("card_type")),"y");
      }
      resultSet.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry( "loadCards(" + appSeqNum + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }
  
  protected void loadMiscFees(long appSeqNum)
  {
    ResultSetIterator       it      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:236^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  misc_chrg_amount as monthly_access_fee
//          from    miscchrg mc
//          where   mc.app_seq_num = :appSeqNum and
//                  mc.misc_code = :mesConstants.APP_MISC_CHARGE_MONTHLY_SERVICE_FEE
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  misc_chrg_amount as monthly_access_fee\n        from    miscchrg mc\n        where   mc.app_seq_num =  :1  and\n                mc.misc_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.sabre.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_MONTHLY_SERVICE_FEE);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.sabre.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:242^7*/
      setFields(it.getResultSet());
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:246^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  misc_chrg_amount as chargeback_fee
//          from    miscchrg mc
//          where   mc.app_seq_num = :appSeqNum and
//                  mc.misc_code = :mesConstants.APP_MISC_CHARGE_CHARGEBACK
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  misc_chrg_amount as chargeback_fee\n        from    miscchrg mc\n        where   mc.app_seq_num =  :1  and\n                mc.misc_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.sabre.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_CHARGEBACK);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.sabre.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:252^7*/
      setFields(it.getResultSet());
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadMiscFees()",e.toString());
      addError("loadMiscFees: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }
  
  protected void loadTransactionFees(long appSeqNum)
  {
    ResultSetIterator       it      = null;
    
    try
    {
      // store v/mc pricing
      /*@lineinfo:generated-code*//*@lineinfo:274^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tc.tranchrg_discrate_type     as pricing_plan,
//                  tc.tranchrg_disc_rate         as disc_rate,
//                  tc.tranchrg_pass_thru         as per_item,
//                  tc.tranchrg_mmin_chrg         as min_discount_amount
//          from    tranchrg    tc
//          where   tc.app_seq_num = :appSeqNum and
//                  tc.cardtype_code = :mesConstants.APP_CT_VISA
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tc.tranchrg_discrate_type     as pricing_plan,\n                tc.tranchrg_disc_rate         as disc_rate,\n                tc.tranchrg_pass_thru         as per_item,\n                tc.tranchrg_mmin_chrg         as min_discount_amount\n        from    tranchrg    tc\n        where   tc.app_seq_num =  :1  and\n                tc.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.sabre.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.sabre.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:283^7*/
      setFields(it.getResultSet());
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadTransactionFees()", e.toString());
      addError("loadTransactionFees: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    int         recCount      = 0;
    
    try
    {
      connect();
    
      super.postHandleRequest(request);
  
      fields.setData("pricingPlan",Integer.toString(mesConstants.APP_PS_FIXED_PLUS_PER_ITEM));
      fields.setData("discRate","2.95");
      fields.setData("perItem","0.0");
      fields.setData("chargebackFee","25");
      
      /*@lineinfo:generated-code*//*@lineinfo:313^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(ms.pcc) 
//          from    merchant_sabre  ms
//          where   ms.app_seq_num = :fields.getField("appSeqNum").asLong() and
//                  ms.primary_pcc is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3143 = fields.getField("appSeqNum").asLong();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ms.pcc)  \n        from    merchant_sabre  ms\n        where   ms.app_seq_num =  :1  and\n                ms.primary_pcc is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.sabre.Pricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3143);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:319^7*/
      
      // per jnix 7/28/2004 no minimum discount
      fields.setData("minDiscountAmount","0");

//@      // if recCount == 0 then the account is part of a 
//@      // chain.  chains do not pay minimum discount
//@      if ( recCount == 0 )
//@      {
//@        fields.setData("minDiscountAmount","0");
//@      }
//@      else  // single location
//@      {
//@        fields.setData("minDiscountAmount","35");
//@      }
    }
    catch(Exception e)
    {
      logEntry("postHandleRequest()",e.toString());
    }      
    finally
    {
      cleanUp();
    }
  }
  
  protected void preHandleRequest(HttpServletRequest request)
  {
    // see if user exists in session already
    UserBean tmpUser = (UserBean)(request.getSession().getAttribute("UserLogin"));
    
    if(tmpUser != null)
    {
      user = tmpUser;
    }
    
    // this will need to have logic added to support long app users...
    if (user == null && request != null)
    {
      user = new UserBean();
      user.forceValidate("sabre-virtualapp");
      request.getSession().setAttribute("UserLogin",user);
    }
  }
  
  protected void storeMiscFees()
  {
    try
    {
      // clear all misc fees from the database prior to inserting
      long appSeqNum = fields.getField("appSeqNum").asLong();
      /*@lineinfo:generated-code*//*@lineinfo:370^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from   miscchrg
//          where  app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from   miscchrg\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.sabre.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:375^7*/

      /*@lineinfo:generated-code*//*@lineinfo:377^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          ( 
//            app_seq_num,
//            misc_code,
//            misc_chrg_amount 
//          )
//          values
//          ( 
//            :appSeqNum,
//            :mesConstants.APP_MISC_CHARGE_CHARGEBACK,
//            :fields.getData("chargebackFee") 
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3144 = fields.getData("chargebackFee");
   String theSqlTS = "insert into miscchrg\n        ( \n          app_seq_num,\n          misc_code,\n          misc_chrg_amount \n        )\n        values\n        ( \n           :1 ,\n           :2 ,\n           :3  \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.app.sabre.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_CHARGEBACK);
   __sJT_st.setString(3,__sJT_3144);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:391^7*/
    }
    catch(Exception e)
    {
      logEntry("storeMiscFees()",e.toString());
      addError("storeMiscFees: " + e.toString());
    }
  }
  
  protected void storeTransactionFees()
  {
    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      // clear tranchrg records
      /*@lineinfo:generated-code*//*@lineinfo:407^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from  tranchrg
//          where app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from  tranchrg\n        where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.sabre.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:412^7*/
      
      // store v/mc pricing
      /*@lineinfo:generated-code*//*@lineinfo:415^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran,
//            tranchrg_per_auth
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.APP_CT_VISA,
//            :mesConstants.APP_PS_FIXED_PLUS_PER_ITEM,
//            :fields.getData("discRate"),
//            :fields.getData("perItem"),
//            :fields.getData("minDiscountAmount"),
//            0,
//            0
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3145 = fields.getData("discRate");
 String __sJT_3146 = fields.getData("perItem");
 String __sJT_3147 = fields.getData("minDiscountAmount");
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran,\n          tranchrg_per_auth\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0,\n          0\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.app.sabre.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   __sJT_st.setInt(3,mesConstants.APP_PS_FIXED_PLUS_PER_ITEM);
   __sJT_st.setString(4,__sJT_3145);
   __sJT_st.setString(5,__sJT_3146);
   __sJT_st.setString(6,__sJT_3147);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:439^7*/

      /*@lineinfo:generated-code*//*@lineinfo:441^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran,
//            tranchrg_per_auth
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.APP_CT_MC,
//            :mesConstants.APP_PS_FIXED_PLUS_PER_ITEM,
//            :fields.getData("discRate"),
//            :fields.getData("perItem"),
//            :fields.getData("minDiscountAmount"),
//            0,
//            0
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3148 = fields.getData("discRate");
 String __sJT_3149 = fields.getData("perItem");
 String __sJT_3150 = fields.getData("minDiscountAmount");
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran,\n          tranchrg_per_auth\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0,\n          0\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.app.sabre.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_MC);
   __sJT_st.setInt(3,mesConstants.APP_PS_FIXED_PLUS_PER_ITEM);
   __sJT_st.setString(4,__sJT_3148);
   __sJT_st.setString(5,__sJT_3149);
   __sJT_st.setString(6,__sJT_3150);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:465^7*/
      
      // amex
      /*@lineinfo:generated-code*//*@lineinfo:468^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran,
//            tranchrg_per_auth
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.APP_CT_AMEX,
//            :mesConstants.APP_PS_FIXED_PLUS_PER_ITEM,
//            :fields.getData("discRate"),
//            :fields.getData("perItem"),
//            :fields.getData("minDiscountAmount"),
//            0,
//            0
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3151 = fields.getData("discRate");
 String __sJT_3152 = fields.getData("perItem");
 String __sJT_3153 = fields.getData("minDiscountAmount");
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran,\n          tranchrg_per_auth\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0,\n          0\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.app.sabre.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_AMEX);
   __sJT_st.setInt(3,mesConstants.APP_PS_FIXED_PLUS_PER_ITEM);
   __sJT_st.setString(4,__sJT_3151);
   __sJT_st.setString(5,__sJT_3152);
   __sJT_st.setString(6,__sJT_3153);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:492^7*/
      
      // discover
      /*@lineinfo:generated-code*//*@lineinfo:495^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran,
//            tranchrg_per_auth
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.APP_CT_DISCOVER,
//            :mesConstants.APP_PS_FIXED_PLUS_PER_ITEM,
//            :fields.getData("discRate"),
//            :fields.getData("perItem"),
//            :fields.getData("minDiscountAmount"),
//            0,
//            0
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3154 = fields.getData("discRate");
 String __sJT_3155 = fields.getData("perItem");
 String __sJT_3156 = fields.getData("minDiscountAmount");
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran,\n          tranchrg_per_auth\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0,\n          0\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.app.sabre.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_DISCOVER);
   __sJT_st.setInt(3,mesConstants.APP_PS_FIXED_PLUS_PER_ITEM);
   __sJT_st.setString(4,__sJT_3154);
   __sJT_st.setString(5,__sJT_3155);
   __sJT_st.setString(6,__sJT_3156);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:519^7*/
      
      // UATP
      /*@lineinfo:generated-code*//*@lineinfo:522^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran,
//            tranchrg_per_auth
//          )
//          values
//          (
//            :appSeqNum,
//            :mesConstants.APP_CT_PRIVATE_LABEL_1,
//            :mesConstants.APP_PS_FIXED_PLUS_PER_ITEM,
//            :fields.getData("discRate"),
//            :fields.getData("perItem"),
//            :fields.getData("minDiscountAmount"),
//            0,
//            0
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3157 = fields.getData("discRate");
 String __sJT_3158 = fields.getData("perItem");
 String __sJT_3159 = fields.getData("minDiscountAmount");
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran,\n          tranchrg_per_auth\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0,\n          0\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.app.sabre.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_PRIVATE_LABEL_1);
   __sJT_st.setInt(3,mesConstants.APP_PS_FIXED_PLUS_PER_ITEM);
   __sJT_st.setString(4,__sJT_3157);
   __sJT_st.setString(5,__sJT_3158);
   __sJT_st.setString(6,__sJT_3159);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:546^7*/
      
    }
    catch(Exception e)
    {
      logEntry("storeTransactionFees()", e.toString());
      addError("storeTransactionFees: " + e.toString());
    }
  }

  protected boolean submitAppData()
  {
    boolean submitOk = false;
    try
    {
      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if (appSeqNum != 0L)
      {
        connect();
        storeTransactionFees();
        storeMiscFees();
      }
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitAppData()",e.toString());
      System.out.println(this.getClass().getName() + "::submitAppData(): " 
        + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( submitOk );
  }
}/*@lineinfo:generated-code*/