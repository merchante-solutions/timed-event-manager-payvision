/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/sabre/Business.sqlj $

  Description:
  
  Business
  
  Banner Bank online application merchant information page bean.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-02-01 16:39:25 -0800 (Fri, 01 Feb 2008) $
  Version            : $Revision: 14544 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.sabre;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.BusinessBase;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.ConstantField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class Business extends BusinessBase
{
  private static final String SABRE_AMEX_SE           = "5049892912";
  private static final String SABRE_AMEX_PCID         = "010450";
  private static final String SABRE_DISCOVER_MERCHNUM = "601101436679714";
  
  private static final char[] InvalidCharsBackOffice = 
  {
    '/','-',':','.','*'
  };

  private static final char[] InvalidCharsNoNumbers = 
  {
    '!','@','#','$','%','^','&','(',')','_','+','=','\\','|','{',
    '}','[',']',';','"','\'','>','<',',','?','~','`','1','2','3',
    '4','5','6','7','8','9','0'
  };

  private static final char[] InvalidCharsDefault = 
  {
    '!','@','#','$','%','^','&','(',')','_','+','=','\\','|','{',
    '}','[',']',';','"','\'','>','<',',','?','~','`'
  };
  
  private static final char[] InvalidCharsEmail = 
  {
    '!','#','$','%','^','&','(',')','+','=','\\','|','{',
    '}','[',']',';','"','>','<',',','?','~','`'
  };
  
  private static final String[][] yesNoRadioList =
  {
    { "No",  "N" },
    { "Yes", "Y" }
  };
  
  protected class ApplicationTypeTable extends DropDownTable
  {
    public ApplicationTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Single PCC Agency");
      addElement("2","Multiple PCC Agency");
    }
  }

  protected class BusinessTypeTable extends DropDownTable
  {
    public BusinessTypeTable( )
    {
      addElement("","select one");
      addElement("1","Sole Proprietorship");
      addElement("2","Corporation");
    }
  }
  
  protected class DepositMethodTable extends DropDownTable
  {
    public DepositMethodTable()
    {
      addElement("","select one");
      addElement("1","Deposit to branch location checking account");
      addElement("2","Deposit to primary location checking account as separate deposit");
      addElement("3","Deposit to primary location checking account as combined deposit");
    }
  }
  
  protected class SabreProductTable extends DropDownTable
  {
    public SabreProductTable()
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      try
      {
        connect();
        
        addElement("","select one");
        
        /*@lineinfo:generated-code*//*@lineinfo:127^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  product_code,
//                    description
//            from    merchant_sabre_products
//            order by product_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  product_code,\n                  description\n          from    merchant_sabre_products\n          order by product_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.sabre.Business",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.sabre.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:133^9*/
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          addElement(rs.getString("product_code"), rs.getString("description"));
        }
        
        rs.close();
        it.close();
      }
      catch(Exception e)
      {
        logEntry("SabreProductTable()", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }
  
  public class AppTypeValidation implements Validation
  {
    String          ErrorMessage      = null;
    Vector          ValFields         = new Vector();
    
    public AppTypeValidation( )
    {
    }
    
    public void addField( Field field )
    {
      ValFields.add( field );
    }
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }
    
    public boolean validate(String fieldData)
    {
      Field               field         = null;
      
      try
      {
        ErrorMessage = null;
        
        if ( fieldData != null && fieldData.equals("2") )
        {
          for( int i = 0; i < ValFields.size(); ++i )
          {
            field = (Field) ValFields.elementAt(i);
            if ( field.isBlank() )
            {
              ErrorMessage = "Must supply multiple location additional information";
              break;
            }
          }
        }
        else if ( fieldData != null )
        {
          for( int i = 0; i < ValFields.size(); ++i )
          {
            field = (Field) ValFields.elementAt(i);
            if ( !field.isBlank() )
            {
              ErrorMessage = "Please clear the multiple location additional information";
              break;
            }
          }
        }
      }
      catch( Exception e )
      {
        // ingore and consider this to be failed validation
        ErrorMessage = "Error: " + e.toString();
      }
      return( ErrorMessage == null );
    }
  }
  
  public class BackOfficeLengthValidation implements Validation
  {
    String          ErrorMessage      = null;
    int             MinLength         = 0;
    
    public BackOfficeLengthValidation( int minLength )
    {
      MinLength = minLength;
    }
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }
    
    public boolean validate(String fieldData)
    {
      ErrorMessage = "Field too short";

      if (fieldData == null ||              // empty or
          fieldData.equals("") ||           // blank or
          fieldData.length() >= MinLength)  // greater than min length
      {
        ErrorMessage = null;                // accept
      }
      
      return( ErrorMessage == null );
    }
  }
  
  public class SpecialCharValidation implements Validation
  {
    String          ErrorMessage      = null;
    char[]          InvalidChars      = null;
    
    public SpecialCharValidation( char[] invalidChars )
    {
      InvalidChars = invalidChars;
    }
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }
    
    public boolean validate(String fieldData)
    {
      StringBuffer        buf     = new StringBuffer();
      char                ch;
      
      try
      {
        ErrorMessage = null;
        
        if ( fieldData != null  )
        {
          for( int i = 0; i < fieldData.length(); ++i )
          {
            ch = fieldData.charAt(i);
            for ( int j = 0; j < InvalidChars.length; ++j )
            {
              if ( ch == InvalidChars[j] )
              {
                if ( buf.length() > 0 ) 
                {
                  buf.append(" ");
                }
                buf.append(InvalidChars[j]);
              }
            }
          }
          if ( buf.length() > 0 )
          {
            buf.insert(0,"Invalid character(s): ");
            ErrorMessage = buf.toString();
          }
        }
      }
      catch( Exception e )
      {
        // ingore and consider this to be failed validation
        ErrorMessage = "Error: " + e.toString();
      }
      return( ErrorMessage == null );
    }
  }
  
  protected boolean autoAct()
  {
    boolean       retVal      = true;
    
    if ( autoActionName.equals("actionAutoFillApp") )
    {
      retVal = autoFillFields();
    }
    return( retVal );
  }
  
  
  protected boolean autoFillFields()
  {
    long          appSeqNum   = 0L;
    String        appType     = null;
    String        depMethod   = null;
    String        primaryPcc  = null;
    boolean       retVal      = false;
    
    
    try
    {
      connect();
        
      if ( getField("applicationType").isValid() && 
           getData("applicationType").equals("2") )
      {
        /*@lineinfo:generated-code*//*@lineinfo:334^9*/

//  ************************************************************
//  #sql [Ctx] { select  mrs.app_seq_num        
//            from    merchant_sabre    mrs
//            where   upper(mrs.pcc) = upper( :getData("primaryPCC") )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3104 = getData("primaryPCC");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mrs.app_seq_num         \n          from    merchant_sabre    mrs\n          where   upper(mrs.pcc) = upper(  :1  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.sabre.Business",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3104);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:339^9*/
      
        // store the current app type params to preserve them through the load
        appType     = getData("applicationType");
        depMethod   = getData("pccDepositMethod");
        primaryPcc  = getData("primaryPCC");
      
        // change the seq num to the primary location seq num
        // and force a load of the primary app data.
        setData("appSeqNum", Long.toString(appSeqNum));
        loadAppData();
      
        // clear the necessary fields
        setData("appSeqNum","");
        setData("PCC","");
      
        // restore the current app type params
        setData("applicationType",appType);
        setData("pccDepositMethod",depMethod);
        setData("primaryPCC",primaryPcc);
      
        // 1 = deposit to location checking account, clear fields
        if( getData("pccDepositMethod").equals("1") )
        {
          setData("bankName","");
          setData("checkingAccount","");
          setData("confirmCheckingAccount","");
          setData("transitRouting","");
          setData("confirmTransitRouting","");
          setData("bankAddress","");
          setData("bankCsz","");
          setData("bankPhone","");
        }
        
        // indicated that this app has been auto-filled
        setData("autoFill","true");
        
        retVal = true;
      }
    }
    catch(Exception e)
    {
      logEntry("autoFillFields()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return(retVal);
  }
  

  protected void createFields(HttpServletRequest request)
  {
    FieldGroup        fgroup    = null;
    Field             field     = null;
    
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField("autoFill"));
    
      // 1 - business info
      fgroup = (FieldGroup)fields.getField("gBusInfo");
      fgroup.add(new Field        ("PCC",              "Psuedo City Code (PCC)",4,4,false));
      fgroup.add(new Field        ("primaryPCC",       "Psuedo City Code (PCC) for primary location",4,4,true));
      fgroup.add(new DropDownField("pccDepositMethod", "Deposit Method",new DepositMethodTable(),true));
      fgroup.add(new Field        ("statementDBA",     "Statement Name (DBA)",12,12,false));
      fgroup.add(new ButtonField  ("actionAutoFillApp","Multi-Agency (PCC) Auto-Fill", "Multi-Agency (PCC) Auto-Fill"));
      fgroup.add(new DropDownField("processFeesOnly",  "Process Fees Only", new YesNoTable(), false));
      fgroup.add(new NumberField  ("iata",             "IATA Number", 8, 12, true, 0)); 
      //fgroup.add(new DropDownField("incRates",         "Include Rates", new YesNoTable(), false));
      fgroup.add(new NumberField  ("netPctg",          "Discount Percentage", 8, 12, false, 2));
      fgroup.add(new NumberField  ("yearsWithSabre",   "Years with Sabre", 3, 4, false, 0)); 
      fgroup.add(new NumberField  ("arcNumber",        "ARC Number", 8, 10, true, 0)); 
      fgroup.add(new EmailField   ("alternateEmail",   "Business e-mail Address (Alt)",45,30,true));
      fgroup.add(new RadioButtonField("premierCustomer",    yesNoRadioList, -1, false, "Required"));
      fgroup.add(new DropDownField("sabreProduct",     "Sabre Product Identification", new SabreProductTable(), false));
      
      // setup the validation for the application type.
      // if the user enters chain they must fill in these fields
      fields.deleteField( "applicationType" );
      field = new DropDownField  ("applicationType","Type of Account Setup",new ApplicationTypeTable(),false);
      AppTypeValidation vAppType = new AppTypeValidation();
      vAppType.addField( fgroup.getField("primaryPCC") );
      vAppType.addField( fgroup.getField("pccDepositMethod") );
      field.addValidation(vAppType);
      fgroup.add(field);
      
      fields.deleteField("businessType");
      fgroup.add  (new DropDownField  ("businessType","Type of Business",new BusinessTypeTable(),false));
      fields.setData("industryType","6");   // set to services
      fields.setData("locationType","3");   // set to business office
      fields.setData("locationYears","0");  // not used, default
      fields.deleteField("businessDesc");
      fields.add( new ConstantField("businessDesc","Travel Agency","formFieldsDim") );
      fields.deleteField("sicCode");
      fields.add( new ConstantField("sicCode","4722","formFieldsDim") );               // set to travel default
      fields.setData("accountType","N");              // New accounts
      fields.getField("numLocations").makeOptional(); // allow blank, use for # in chain
      
      // 2 - merchant history
      fields.getField("haveProcessed").makeOptional();
      fields.getField("statementsProvided").makeOptional();
      fields.getField("haveCanceled").makeOptional();
      
      // 3 - bank account info
      fgroup = (FieldGroup) fields.getField("gBankAcct");
      fields.setData("sourceOfInfo","6");                 // default to "other"
      fields.getField("bankPhone").makeOptional();        // not used
      
      // add the confirmation fields for check account and transit routing
      fgroup.add(new Field("confirmCheckingAccount","Confirm Checking Acct. #",17,35,false));
      fgroup.add(new Field("confirmTransitRouting","Confirm Transit Routing #",9,35,false));
      
      // add the field equals validation to the confirmation numbers
      fgroup.getField("confirmCheckingAccount")
        .addValidation(new FieldEqualsFieldValidation(
          fgroup.getField("checkingAccount"),"Checking Account #"));
      fgroup.getField("confirmTransitRouting")
        .addValidation(new FieldEqualsFieldValidation(
          fgroup.getField("transitRouting"),"Transit Routing #"));
      
      // 4 - primary owner
      fields.getField("owner1Gender").makeOptional();     // not used
      fields.getField("owner1Percent").makeOptional();
      fields.getField("owner1SinceMonth").makeOptional();
      fields.getField("owner1SinceYear").makeOptional();
      fields.setData ("owner1SinceYear",Integer.toString(Calendar.getInstance().get(Calendar.YEAR)));
      fields.getField("owner1Title").makeOptional();
      fields.getField("owner1SSN").makeOptional();
      
      // 5 - secondary owner
      
      // 6 - transaction info
      fields.getField("monthlySales").makeOptional();     // not used
      fields.getField("motoPercentage").makeOptional();
      fields.setData("motoPercentage","100");
      fields.getField("internetPercent").makeOptional();
      fields.getField("cardPercent").makeOptional();
      fields.getField("noCardPercent").makeOptional();
      fields.getField("refundPolicy").makeOptional();
      
      // 7 - payment options
      fields.deleteField("vmcAccepted");        // delete so any validations
      fields.deleteField("amexAccepted");       // are removed
      fields.deleteField("discoverAccepted");   //
      fields.deleteField("discoverRapRate");    //
      fields.deleteField("dinersAccepted");     //
      
      // create new versions of the field
      fgroup = (FieldGroup)fields.getField("gPayOpts");
      fgroup.add(new HiddenField("vmcAccepted","y"));
      fgroup.add(new HiddenField("amexAccepted","y"));
      fgroup.add(new HiddenField("discoverAccepted","y"));
      fgroup.add(new HiddenField("discoverRapRate","0"));
      fgroup.add(new HiddenField("dinersAccepted","y"));
      
      // create new fields to hold charge and vendor codes for both sales and credits
      fgroup.add(new Field("salesChargeCode","Sales Charge Descriptor Code",3,3,true));
      fgroup.add(new Field("salesVendorCode","Sales Vendor Code",6,6,true));
      fgroup.add(new Field("refundsChargeCode","Refund Charge Descriptor Code",3,3,true));
      fgroup.add(new Field("refundsVendorCode","Refund Vendor Code",6,6,true));
      
      // set the minimum field length
      fgroup.getField("salesChargeCode").addValidation(new BackOfficeLengthValidation(3));
      fgroup.getField("salesVendorCode").addValidation(new BackOfficeLengthValidation(6));
      fgroup.getField("refundsChargeCode").addValidation(new BackOfficeLengthValidation(3));
      fgroup.getField("refundsVendorCode").addValidation(new BackOfficeLengthValidation(6));
      
      // 8 - pos product
      fields.setData("productType",Integer.toString(mesConstants.POS_OTHER));
      fields.setData("vitalProduct","Sabre");
      
      try
      {
        fields.getField("internetEmail").makeOptional();
      }
      catch(Exception fe)
      {
      }
      
      // set html extra
      fields.setHtmlExtra("class=\"formText\"");

      // set location number to 1 by default
      fields.setData("numLocations","1");
    
      // set the app type and screen id
      fields.setData("appType",Integer.toString(mesConstants.APP_TYPE_SABRE));
      fields.setData("curScreenId","1");
      
      // set extra on all fields not set yet
      fields.setHtmlExtra("class=\"formText\"");
      
      Vector allFields = fields.getFieldsVector();
      
      for (int i = 0; i < allFields.size(); ++i )
      {
        field = (Field)allFields.elementAt(i);
        
        if ( field.getName().equals("businessEmail") )
        {
          field.addValidation( new SpecialCharValidation(InvalidCharsEmail) );
          field.makeRequired();
        }
        else if ( field.getName().equals("alternateEmail") )
        {
          field.addValidation( new SpecialCharValidation(InvalidCharsEmail) );
        }
        else if ( field.getName().equals("salesChargeCode") ||
                  field.getName().equals("refundsChargeCode") )
        {
          field.addValidation( new SpecialCharValidation(InvalidCharsNoNumbers) );
          field.addValidation( new SpecialCharValidation(InvalidCharsBackOffice) );
        }
        else if ( field.getName().equals("salesVendorCode") ||
                  field.getName().equals("refundsVendorCode") )
        {
          field.addValidation( new SpecialCharValidation(InvalidCharsDefault) );
          field.addValidation( new SpecialCharValidation(InvalidCharsBackOffice) );
        }
        else    // all others get the default
        {
          field.addValidation( new SpecialCharValidation(InvalidCharsDefault) );
        }
      }
    }
    catch( Exception e )
    {
      logEntry("createFields()",e.toString());
    }
  }
  
  protected boolean loadAppData()
  {
    long                  appSeqNum   = 0L;
    ResultSetIterator     it          = null;
    boolean               retVal      = super.loadAppData();
    String                tempData    = null;
  
    try
    {
      appSeqNum   = fields.getField("appSeqNum").asInteger();
      
      if( retVal == true && appSeqNum != 0L )
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:589^9*/

//  ************************************************************
//  #sql [Ctx] { select  decode(mr.cloned_app,'Y','true','false') 
//            from    merchant    mr
//            where   mr.app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode(mr.cloned_app,'Y','true','false')  \n          from    merchant    mr\n          where   mr.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.sabre.Business",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   tempData = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:594^9*/
        setData("autoFill",tempData);
        
        /*@lineinfo:generated-code*//*@lineinfo:597^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ms.pcc                    as pcc,
//                    ms.primary_pcc            as primary_pcc,
//                    ms.deposit_method         as pcc_deposit_method,
//                    ms.statement_dba_name     as statement_dba,
//                    ms.sales_charge_code      as sales_charge_code,
//                    ms.sales_vendor_code      as sales_vendor_code,
//                    ms.refunds_charge_code    as refunds_charge_code,
//                    ms.refunds_vendor_code    as refunds_vendor_code,
//                    decode( nvl(ms.process_fares,'N'),
//                            'Y','N','Y' )     as process_fees_only,
//                    ms.iata_number            as iata,
//                    ms.years_with_sabre       as years_with_sabre,
//                    ms.arc_number             as arc_number,
//                    ms.alternate_email_address as alternate_email,
//                    ms.net_percentage         as net_pctg,
//                    ms.premier_customer       as premier_customer,
//                    ms.sabre_product          as sabre_product
//            from    merchant_sabre    ms
//            where   ms.app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ms.pcc                    as pcc,\n                  ms.primary_pcc            as primary_pcc,\n                  ms.deposit_method         as pcc_deposit_method,\n                  ms.statement_dba_name     as statement_dba,\n                  ms.sales_charge_code      as sales_charge_code,\n                  ms.sales_vendor_code      as sales_vendor_code,\n                  ms.refunds_charge_code    as refunds_charge_code,\n                  ms.refunds_vendor_code    as refunds_vendor_code,\n                  decode( nvl(ms.process_fares,'N'),\n                          'Y','N','Y' )     as process_fees_only,\n                  ms.iata_number            as iata,\n                  ms.years_with_sabre       as years_with_sabre,\n                  ms.arc_number             as arc_number,\n                  ms.alternate_email_address as alternate_email,\n                  ms.net_percentage         as net_pctg,\n                  ms.premier_customer       as premier_customer,\n                  ms.sabre_product          as sabre_product\n          from    merchant_sabre    ms\n          where   ms.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.sabre.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.sabre.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:618^9*/
        setFields(it.getResultSet());
        
        it.close();
      }        
    }
    catch(Exception e )
    {
      logEntry("loadAppData()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
      cleanUp();  // not really needed, see above
    }
    return( retVal );
  }
  
  protected void postHandleRequest(HttpServletRequest request)
  {
    StringBuffer  dba       = null;
    int           len       = 0;
    String        pcc       = null;
    String        temp      = null;
    
    super.postHandleRequest(request);
    
    // add the PCC to the end of the DBA name
    // if it is not already included.
    temp  = fields.getData("businessName");
    pcc   = fields.getData("PCC");
    
    if ( temp.indexOf(pcc) < 0 )
    {
      dba = new StringBuffer(temp);
      len = dba.length();
      
      if ( len+7 > 25 )
      {
        if ( len+5 > 25 )
        {
          dba.setLength(20);
        }
        dba.append(" ");
        dba.append(pcc);
      }
      else  // append 7 char version
      {
        dba.append(" - ");
        dba.append(pcc);
      }
      fields.setData("businessName",dba.toString());
    }
    
    // copy the address information
    setData("businessLegalName",getData("businessName"));
    setData("mailingName",getData("businessName"));
    setData("mailingAddress1",getData("businessAddress1"));
    setData("mailingAddress2",getData("businessAddress2"));
    setData("mailingCsz",getData("businessCsz"));
    setData("amexAcctNum",SABRE_AMEX_SE); // all sabre accounts use same SE
    setData("discoverAcctNum",SABRE_DISCOVER_MERCHNUM);
    
    try
    {
      // insert private label card (UATP) into merchpayoption
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:686^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchpayoption
//          where   app_seq_num = :getData("appSeqNum") and
//                  cardtype_code = :mesConstants.APP_CT_PRIVATE_LABEL_1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3105 = getData("appSeqNum");
  try {
   String theSqlTS = "delete\n        from    merchpayoption\n        where   app_seq_num =  :1  and\n                cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.sabre.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3105);
   __sJT_st.setInt(2,mesConstants.APP_CT_PRIVATE_LABEL_1);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:692^7*/
      
      int nextcard = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:696^7*/

//  ************************************************************
//  #sql [Ctx] { select  max(card_sr_number)+1
//          
//          from    merchpayoption
//          where   app_seq_num = :getData("appSeqNum")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3106 = getData("appSeqNum");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max(card_sr_number)+1\n         \n        from    merchpayoption\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.sabre.Business",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3106);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   nextcard = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:702^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:704^7*/

//  ************************************************************
//  #sql [Ctx] { insert into  merchpayoption
//          ( app_seq_num,
//            cardtype_code,
//            card_sr_number,
//            merchpo_split_dial
//          )
//          values
//          ( :getData("appSeqNum"),
//            :mesConstants.APP_CT_PRIVATE_LABEL_1,
//            :nextcard,
//            'N'
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3107 = getData("appSeqNum");
   String theSqlTS = "insert into  merchpayoption\n        ( app_seq_num,\n          cardtype_code,\n          card_sr_number,\n          merchpo_split_dial\n        )\n        values\n        (  :1 ,\n           :2 ,\n           :3 ,\n          'N'\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.app.sabre.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3107);
   __sJT_st.setInt(2,mesConstants.APP_CT_PRIVATE_LABEL_1);
   __sJT_st.setInt(3,nextcard);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:718^7*/
    }
    catch(Exception e)
    {
      logEntry("postHandleRequest()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  protected void preHandleRequest(HttpServletRequest request)
  {
    // see if user exists in session already
    UserBean tmpUser = (UserBean)(request.getSession().getAttribute("UserLogin"));
    
    if(tmpUser != null)
    {
      user = tmpUser;
    }
    
    // this will need to have logic added to support long app users...
    if ( user == null )
    {
      user = new UserBean();
      request.getSession().setAttribute("UserLogin",user);
    }
    if ( user.getUserLoginId() == null ||
         user.getUserLoginId().equals("") )
    {
      user.forceValidate("sabre-virtualapp");
    }
  
    if ( HttpHelper.getBoolean(request,"autoFill",false) == true )
    {
      // must load the multiple location data fields manually because the 
      // field setting portion of the setFields algorithm has not run yet.
      setData("applicationType",HttpHelper.getString(request,"applicationType",""));
      setData("pccDepositMethod",HttpHelper.getString(request,"pccDepositMethod",""));
      setData("primaryPCC",HttpHelper.getString(request,"primaryPCC",""));
      
      // preload the fields with 
      // the data from the primary
      // location's application
      autoFillFields();
      
      //@ debug 
      System.out.println("bankCsz = " + getData("bankCsz"));//@
    }
  }
  
  protected boolean submitAppData()
  {
    long                  appSeqNum   = 0L;
    int                   recCount    = 0;
    boolean               retVal      = super.submitAppData();;
  
    try
    {
      appSeqNum = fields.getField("appSeqNum").asInteger();
    
      if( retVal == true && appSeqNum != 0L )
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:784^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant mr
//            set     mr.cloned_app = decode(:getData("autoFill"),
//                                        'true','Y','N'),
//                    mr.merch_amex_pcid_num = :SABRE_AMEX_PCID                                      
//            where   mr.app_seq_num = :appSeqNum                                      
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3108 = getData("autoFill");
   String theSqlTS = "update  merchant mr\n          set     mr.cloned_app = decode( :1 ,\n                                      'true','Y','N'),\n                  mr.merch_amex_pcid_num =  :2                                       \n          where   mr.app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.app.sabre.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3108);
   __sJT_st.setString(2,SABRE_AMEX_PCID);
   __sJT_st.setLong(3,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:791^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:793^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(ms.app_seq_num) 
//            from    merchant_sabre    ms
//            where   ms.app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ms.app_seq_num)  \n          from    merchant_sabre    ms\n          where   ms.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.sabre.Business",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:798^9*/
        
        //
        // must do a seperate insert and update so that
        // the on-update trigger will correctly update
        // the sabre profile when certain key fields change.
        //
        if ( recCount == 0 )    // insert
        {
          /*@lineinfo:generated-code*//*@lineinfo:807^11*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant_sabre
//              (
//                app_seq_num,
//                pcc,
//                primary_pcc,
//                deposit_method,
//                statement_dba_name,
//                sales_charge_code,
//                sales_vendor_code,
//                refunds_charge_code,
//                refunds_vendor_code,
//                process_fares,
//                include_rates,
//                iata_number,
//                alternate_email_address,
//                years_with_sabre,
//                arc_number,
//                premier_customer,
//                net_percentage,
//                sabre_product
//              )
//              values
//              (
//                :appSeqNum,
//                :getData("PCC"),
//                :getData("primaryPCC"),
//                :getData("pccDepositMethod"),
//                :getData("statementDBA"),
//                :getData("salesChargeCode"),
//                :getData("salesVendorCode"),
//                :getData("refundsChargeCode"),
//                :getData("refundsVendorCode"),
//                decode( nvl(:getData("processFeesOnly"),'Y'),'N','Y','N' ),
//                decode( to_number(nvl(:getData("netPctg"),0)), 0, 'N', 'Y'),
//                :getData("iata"),
//                :getData("alternateEmail"),
//                :getData("yearsWithSabre"),
//                :getData("arcNumber"),
//                :getData("premierCustomer"),
//                :getData("netPctg"),
//                :getData("sabreProduct")
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3109 = getData("PCC");
 String __sJT_3110 = getData("primaryPCC");
 String __sJT_3111 = getData("pccDepositMethod");
 String __sJT_3112 = getData("statementDBA");
 String __sJT_3113 = getData("salesChargeCode");
 String __sJT_3114 = getData("salesVendorCode");
 String __sJT_3115 = getData("refundsChargeCode");
 String __sJT_3116 = getData("refundsVendorCode");
 String __sJT_3117 = getData("processFeesOnly");
 String __sJT_3118 = getData("netPctg");
 String __sJT_3119 = getData("iata");
 String __sJT_3120 = getData("alternateEmail");
 String __sJT_3121 = getData("yearsWithSabre");
 String __sJT_3122 = getData("arcNumber");
 String __sJT_3123 = getData("premierCustomer");
 String __sJT_3124 = getData("netPctg");
 String __sJT_3125 = getData("sabreProduct");
   String theSqlTS = "insert into merchant_sabre\n            (\n              app_seq_num,\n              pcc,\n              primary_pcc,\n              deposit_method,\n              statement_dba_name,\n              sales_charge_code,\n              sales_vendor_code,\n              refunds_charge_code,\n              refunds_vendor_code,\n              process_fares,\n              include_rates,\n              iata_number,\n              alternate_email_address,\n              years_with_sabre,\n              arc_number,\n              premier_customer,\n              net_percentage,\n              sabre_product\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n               :7 ,\n               :8 ,\n               :9 ,\n              decode( nvl( :10 ,'Y'),'N','Y','N' ),\n              decode( to_number(nvl( :11 ,0)), 0, 'N', 'Y'),\n               :12 ,\n               :13 ,\n               :14 ,\n               :15 ,\n               :16 ,\n               :17 ,\n               :18 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.app.sabre.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_3109);
   __sJT_st.setString(3,__sJT_3110);
   __sJT_st.setString(4,__sJT_3111);
   __sJT_st.setString(5,__sJT_3112);
   __sJT_st.setString(6,__sJT_3113);
   __sJT_st.setString(7,__sJT_3114);
   __sJT_st.setString(8,__sJT_3115);
   __sJT_st.setString(9,__sJT_3116);
   __sJT_st.setString(10,__sJT_3117);
   __sJT_st.setString(11,__sJT_3118);
   __sJT_st.setString(12,__sJT_3119);
   __sJT_st.setString(13,__sJT_3120);
   __sJT_st.setString(14,__sJT_3121);
   __sJT_st.setString(15,__sJT_3122);
   __sJT_st.setString(16,__sJT_3123);
   __sJT_st.setString(17,__sJT_3124);
   __sJT_st.setString(18,__sJT_3125);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:851^11*/
        }
        else    // update existing so trigger will work
        {
          /*@lineinfo:generated-code*//*@lineinfo:855^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_sabre    ms
//              set     pcc                 = :getData("PCC"),
//                      primary_pcc         = :getData("primaryPCC"),
//                      deposit_method      = :getData("pccDepositMethod"),
//                      statement_dba_name  = :getData("statementDBA"),
//                      sales_charge_code   = :getData("salesChargeCode"),
//                      sales_vendor_code   = :getData("salesVendorCode"),
//                      refunds_charge_code = :getData("refundsChargeCode"),
//                      refunds_vendor_code = :getData("refundsVendorCode"),
//                      include_rates       = decode( to_number(nvl(:getData("netPctg"),0)), 0, 'N', 'Y'),
//                      process_fares       = decode( nvl(:getData("processFeesOnly"),'Y'),'N','Y','N' ),
//                      iata_number         = :getData("iata"),
//                      years_with_sabre    = :getData("yearsWithSabre"),
//                      arc_number          = :getData("arcNumber"),
//                      alternate_email_address = :getData("alternateEmail"),
//                      premier_customer    = :getData("premierCustomer"),
//                      net_percentage      = :getData("netPctg"),
//                      sabre_product       = :getData("sabreProduct")
//              where   ms.app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3126 = getData("PCC");
 String __sJT_3127 = getData("primaryPCC");
 String __sJT_3128 = getData("pccDepositMethod");
 String __sJT_3129 = getData("statementDBA");
 String __sJT_3130 = getData("salesChargeCode");
 String __sJT_3131 = getData("salesVendorCode");
 String __sJT_3132 = getData("refundsChargeCode");
 String __sJT_3133 = getData("refundsVendorCode");
 String __sJT_3134 = getData("netPctg");
 String __sJT_3135 = getData("processFeesOnly");
 String __sJT_3136 = getData("iata");
 String __sJT_3137 = getData("yearsWithSabre");
 String __sJT_3138 = getData("arcNumber");
 String __sJT_3139 = getData("alternateEmail");
 String __sJT_3140 = getData("premierCustomer");
 String __sJT_3141 = getData("netPctg");
 String __sJT_3142 = getData("sabreProduct");
   String theSqlTS = "update  merchant_sabre    ms\n            set     pcc                 =  :1 ,\n                    primary_pcc         =  :2 ,\n                    deposit_method      =  :3 ,\n                    statement_dba_name  =  :4 ,\n                    sales_charge_code   =  :5 ,\n                    sales_vendor_code   =  :6 ,\n                    refunds_charge_code =  :7 ,\n                    refunds_vendor_code =  :8 ,\n                    include_rates       = decode( to_number(nvl( :9 ,0)), 0, 'N', 'Y'),\n                    process_fares       = decode( nvl( :10 ,'Y'),'N','Y','N' ),\n                    iata_number         =  :11 ,\n                    years_with_sabre    =  :12 ,\n                    arc_number          =  :13 ,\n                    alternate_email_address =  :14 ,\n                    premier_customer    =  :15 ,\n                    net_percentage      =  :16 ,\n                    sabre_product       =  :17 \n            where   ms.app_seq_num =  :18";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.app.sabre.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3126);
   __sJT_st.setString(2,__sJT_3127);
   __sJT_st.setString(3,__sJT_3128);
   __sJT_st.setString(4,__sJT_3129);
   __sJT_st.setString(5,__sJT_3130);
   __sJT_st.setString(6,__sJT_3131);
   __sJT_st.setString(7,__sJT_3132);
   __sJT_st.setString(8,__sJT_3133);
   __sJT_st.setString(9,__sJT_3134);
   __sJT_st.setString(10,__sJT_3135);
   __sJT_st.setString(11,__sJT_3136);
   __sJT_st.setString(12,__sJT_3137);
   __sJT_st.setString(13,__sJT_3138);
   __sJT_st.setString(14,__sJT_3139);
   __sJT_st.setString(15,__sJT_3140);
   __sJT_st.setString(16,__sJT_3141);
   __sJT_st.setString(17,__sJT_3142);
   __sJT_st.setLong(18,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:876^11*/
        }
      }        
    }
    catch(Exception e )
    {
      logEntry("submitAppData()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/