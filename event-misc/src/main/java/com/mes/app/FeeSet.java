/*@lineinfo:filename=FeeSet*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/FeeSet.sqlj $

  FeeSet
  
  Manages a set of fees for an application pricing page.  

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-05-16 10:18:06 -0700 (Fri, 16 May 2008) $
  Version            : $Revision: 14864 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002,2003,2004 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app;

import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.support.NumberFormatter;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class FeeSet extends FieldBean
{
  static    Logger    log       = Logger.getLogger(FeeSet.class);
  
  private   UserBean  user      = null;
  private   boolean   isLoaded  = false;

  protected Vector    fees      = new Vector();
  protected int       setId     = -1;
  protected long      appSeqNum = -1L;
  
  public static final int IT_TRAN_FEES    = 1;
  public static final int IT_EQUIP_FEES   = 2;
  public static final int IT_MISC_FEES    = 3;

  private int posType = -1;
  
  /*
  ** private int getFeeSetId()
  **
  ** Determines a fee set to use with this application.  First, a set assign-
  ** ment is looked for in appo_fee_apps.  If none is found then a default
  ** set is looked for using the user's hierarchy node and appo_fee_dflts.
  ** At the time that a default set assignment is determined, an entry will
  ** be made in app_fee_apps for this app_seq_num so that this app will
  ** always show the same fee set no matter who looks at it.
  **
  ** RETURNS: the fee set id assigned to this app.
  */
  private int getFeeSetId()
  {
    ResultSetIterator it = null;
    int setId = -1;
    
    try
    {
      connect();
      
      // see if fee set assignment for this app
      /*@lineinfo:generated-code*//*@lineinfo:88^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  set_id
//          from    appo_fee_apps
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  set_id\n        from    appo_fee_apps\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.FeeSet",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:93^7*/
      
      // get the set id assigned to this app if it exists
      ResultSet rs = it.getResultSet();
      if (rs.next())
      {
        setId = rs.getInt("set_id");
      }
      // no assigned fee set, look up a default for this user
      else
      {
        it.close();

        // look for a default set id assigned to the app type's
        // hierarchy node or nearest ancestor
        /*@lineinfo:generated-code*//*@lineinfo:108^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  afd.set_id
//            from    application app,
//                    org_app oa,
//                    appo_fee_dflts afd,
//                    t_hierarchy th
//            where   app.app_seq_num = :appSeqNum and
//                    app.app_type = oa.app_type and
//                    oa.hierarchy_node = th.descendent and
//                    th.ancestor = afd.hier_id
//            order by th.relation
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  afd.set_id\n          from    application app,\n                  org_app oa,\n                  appo_fee_dflts afd,\n                  t_hierarchy th\n          where   app.app_seq_num =  :1  and\n                  app.app_type = oa.app_type and\n                  oa.hierarchy_node = th.descendent and\n                  th.ancestor = afd.hier_id\n          order by th.relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.FeeSet",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:120^9*/
        
        /*
        // NOTE - this has been changed to use the original creator's user
        #sql [Ctx] it =
        {
          select  afd.set_id
          from    appo_fee_dflts afd,
                  t_hierarchy th,
                  users u,
                  application app
          where   app.app_seq_num = :appSeqNum and
                  app.app_user_login = u.login_name and
                  u.hierarchy_node = th.descendent and
                  th.ancestor = afd.hier_id
          order by th.relation
        };
        */
        
        /* OLD QUERY THAT USES THE CURRENT USER's HIERARCHY NODE
        
        #sql [Ctx] it =
        {
          select  afd.set_id
          from    appo_fee_dflts  afd,
                  t_hierarchy     t
          where   t.descendent = :(user.getHierarchyNode())
                  and t.ancestor = afd.hier_id
          order by t.relation
        };
        */
        
        rs = it.getResultSet();
        if (rs.next())
        {
          // found a default assignment
          setId = rs.getInt("set_id");
          
          // assign the default to this app
          /*@lineinfo:generated-code*//*@lineinfo:159^11*/

//  ************************************************************
//  #sql [Ctx] { insert into appo_fee_apps
//              ( app_seq_num, set_id )
//              values
//              ( :appSeqNum, :setId )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into appo_fee_apps\n            ( app_seq_num, set_id )\n            values\n            (  :1 ,  :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,setId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:165^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("getFeeSetId()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
    
    return setId;
  }
  
  /*
  ** Parameter definitions
  */
  
  // standard parameters
  public static final int FPID_SET_ID         = 1;
  public static final int FPID_ITEM_ID        = 2;
  public static final int FPID_ITEM_TYPE      = 3;
  public static final int FPID_LABEL          = 4;
  public static final int FPID_DISP_CAT       = 5;

  public static final int FPID_CARD_TYPE      = 20;
  public static final int FPID_TRAN_FEE_TYPE  = 21;
  
  public static final int FPID_EQUIP_MODEL    = 30;
  public static final int FPID_LEND_TYPE      = 31;
  public static final int FPID_LEND_DESC      = 32;
  public static final int FPID_QUANTITY       = 33;
  
  public static final int FPID_MISC_CODE      = 50;
  public static final int FPID_UNIQUE_DESC    = 51;
  
  // optional parameters
  public static final int FPID_DEFAULT_VAL    = 101;
  public static final int FPID_DROP_VALS      = 102;
  public static final int FPID_UPCHRG_TYPE    = 103;
  public static final int FPID_UPCHRG_AMT     = 104;
  
  // parameter data types
  public static final int FPDT_INT            = 1;
  public static final int FPDT_STRING         = 2;
  public static final int FPDT_DOUBLE         = 3;

  /*
  ** public static class ParameterDef
  **
  ** Parameter definition object.  Stores a name, id, and data type.
  */
  public static class FeeParameterDef
  {
    public String   name;
    public int      type;
  
    public FeeParameterDef(String name, int type)
    {
      this.name     = name;
      this.type     = type;
    }
  }
  
  protected Vector parmDefs = new Vector();

  /*
  ** protected void initFeeParameterDefs()
  **
  ** Creates the base set of parameter definitions.  Each definition specifies
  ** a name and a data type.
  */
  protected void initFeeParameterDefs()
  {
    parmDefs.add(new FeeParameterDef("setid",        FPDT_INT     ));
    parmDefs.add(new FeeParameterDef("itemid",       FPDT_INT     ));
    parmDefs.add(new FeeParameterDef("itemtype",     FPDT_INT     ));
    parmDefs.add(new FeeParameterDef("label",        FPDT_STRING  ));
    parmDefs.add(new FeeParameterDef("dispcat",      FPDT_INT     ));

    parmDefs.add(new FeeParameterDef("cardtype",     FPDT_INT     ));
    parmDefs.add(new FeeParameterDef("tranfeetype",  FPDT_INT     ));

    parmDefs.add(new FeeParameterDef("model",        FPDT_STRING  ));
    parmDefs.add(new FeeParameterDef("lendtype",     FPDT_INT     ));
    parmDefs.add(new FeeParameterDef("lenddesc",     FPDT_STRING  ));
    parmDefs.add(new FeeParameterDef("quantity",     FPDT_INT     ));

    parmDefs.add(new FeeParameterDef("misccode",     FPDT_INT     ));
    parmDefs.add(new FeeParameterDef("uniquedesc",   FPDT_STRING  ));

    parmDefs.add(new FeeParameterDef("default",      FPDT_STRING  ));
    parmDefs.add(new FeeParameterDef("dropvals",     FPDT_STRING  ));
    parmDefs.add(new FeeParameterDef("droptable",    FPDT_STRING  ));
    parmDefs.add(new FeeParameterDef("upchrgtype",   FPDT_INT     ));
    parmDefs.add(new FeeParameterDef("upchrgamt",    FPDT_DOUBLE  ));
  }
  
  public static class TranFeeTable extends DropDownTable
  {
    public TranFeeTable( String parm )
    {
      char            ch;
      StringBuffer    buf         = new StringBuffer();
      StringBuffer    descBuffer  = new StringBuffer();
      boolean         firstValue  = true;
      StringTokenizer tokens      = new StringTokenizer(parm,",");
      StringBuffer    valueBuffer = new StringBuffer();
      
      while (tokens.hasMoreTokens())
      {
        // reset variables
        buf.setLength(0);
        descBuffer.setLength(0);
        valueBuffer.setLength(0);
        firstValue = true;
        
        buf.append(tokens.nextToken());
        
        for(int i = 0; i < buf.length(); ++i)
        {
          ch = buf.charAt(i);
          
          if ( ch == '|' )
          {
            firstValue = false;
            continue;  
          }
          
          if( firstValue )
          {
            valueBuffer.append(ch);
          }
          else
          {
            descBuffer.append(ch);
          }
        }
        
        addElement(valueBuffer.toString(),descBuffer.toString());
      }
    }
  }
    
  /*
  ** public class FeeParameter
  **
  ** Stores a parameter to be associated with a Fee item.
  */
  public class FeeParameter
  {
    private FeeParameterDef def;
    private String          data;

    /*
    ** public FeeParameter(String parmStr) throws Exception
    **
    ** Parses a parameter string, validates it, determines a parm def and
    ** stores the def along with the data.
    */
    public FeeParameter(String parmStr)
      throws Exception
    {
      // parse a name value pair out of the parm string ('=' is separator)
      String parmName = null;
      String parmData = null;
      try
      {
        StringTokenizer tok = new StringTokenizer(parmStr,"=");
        parmName  = tok.nextToken();
        parmData  = tok.nextToken();
      }
      catch (Exception e)
      {
        throw new Exception("Invalid parameter string ('" + parmStr + "'): "
          + e.toString());
      }
      
      // find the parm def based on the parm name given
      FeeParameterDef parmDef = null;
      for (Iterator i = parmDefs.iterator(); i.hasNext();)
      {
        FeeParameterDef pd = (FeeParameterDef)i.next();
        if (pd.name.toLowerCase().equals(parmName.toLowerCase()))
        {
          parmDef = pd;
          break;
        }
      }
      if (parmDef == null)
      {
        for (Iterator i = parmDefs.iterator(); i.hasNext();)
        {
          FeeParameterDef pd = (FeeParameterDef)i.next();
        }
        throw new Exception("Invalid parameter name ('" + parmName + "')");
      }
      
      // validate the parm data using the parm def...
      try
      {
        switch (parmDef.type)
        {
          case FPDT_INT:
            int testInt = Integer.parseInt(parmData);
            break;
            
          case FPDT_DOUBLE:
            double testDouble = Double.parseDouble(parmData);
            break;
          
          default:
            break;
        }
        
      }
      catch (Exception e)
      {
        throw new Exception("Invalid parameter data ('" + parmData + "')");
      }
      
      // store parm def and data
      this.def = parmDef;
      this.data = parmData;
    }
    
    /*
    ** General accessors
    */    
    public FeeParameterDef getDef()
    {
      return def;
    }
    
    public String getName()
    {
      return def.name;
    }
    
    public int getType()
    {
      return def.type;
    }
    
    public String getData()
    {
      return data;
    }
    
    /*
    ** Converter accessors
    */
    public String asString()
    {
      return data;
    }
    public int asInt()
    {
      return Integer.parseInt(data);
    }
    public long asLong()
    {
      return Long.parseLong(data);
    }
    public double asDouble()
    {
      return Double.parseDouble(data);
    }
    
    public String toString()
    {
      return def.name + "='" + asString() + "'";
    }
  }
  
  /*
  ** public abstract class Fee
  **
  ** Base field for storing all information concerning a fee item.  The
  ** fee item id, type, label and field are contained here.  A FeeParameter
  ** vector is used to store optional data concerning the fee such as drop 
  ** down table values.  A rules vector contains rules that define any
  ** conditions that must be met for the field to be allowed.  This is an 
  ** abstract class, the method getFieldName() must be provided by child 
  ** classes.
  */
  public abstract class Fee
  {
    protected Vector  parms = new Vector();
    protected Vector  rules = new Vector();
    protected Field   feeField;
    
    /*
    ** public class Rule
    **
    ** Defines a condition that must be met in order for a fee item to be
    ** allowed.
    */
    public class Rule
    {
      private String expression;
    
      public Rule(String expression)
      {
        this.expression = expression;
      }
    
      public boolean isSatisfied() throws Exception
      {
        // for now only allow a simple comparison of form 'x=y'
        // $var_name is resolved by looking up var_name in 
        // the rule vars vector
        
        // get left and right hand sides of expression
        StringTokenizer tok = new StringTokenizer(expression,"=");
        String lhside, rhside;
        if (tok.hasMoreTokens())
        {
          lhside = tok.nextToken();
        }
        else
        {
          throw new Exception("Invalid rule expression, no lhside found ('"
            + expression + "')");
        }
        
        if (tok.hasMoreTokens())
        {
          rhside = tok.nextToken();
        }
        else
        {
          throw new Exception("Invalid rule expression, no rhside found ('"
            + expression + "')");
        }
      
        if (tok.hasMoreTokens())
        {
          throw new Exception("Invalid rule expression, too many '=' ('"
            + expression + "')");
        }
      
        // resolve left hand variable name
        if (lhside.charAt(0) == '$')
        {
          // look for a global variable first
          String varVal = (String)ruleVars.get(lhside.substring(1));
          if (varVal == null)
          {
            // no global, try for a local parameter
            FeeParameter parm = getParm(lhside.substring(1));
            if (parm == null)
            {
              // variable name doesn't resolve to anything
              throw new Exception("Invalid lhside var name '" + lhside + "'"
                + " ('" + expression + "')");
            }
            varVal = parm.asString();
          }
          lhside = varVal;
        }
      
        // resolve right hand variable name
        if (rhside.charAt(0) == '$')
        {
          // look for a global variable first
          String varVal = (String)ruleVars.get(rhside.substring(1));
          if (varVal == null)
          {
            // no global, try for a local parameter
            FeeParameter parm = getParm(rhside.substring(1));
            if (parm == null)
            {
              // variable name doesn't resolve to anything
              throw new Exception("Invalid rhside var name '" + rhside + "'"
                + " ('" + expression + "')");
            }
            varVal = parm.asString();
          }
          rhside = varVal;
        }
      
        // compare left hand side to right hand side
        return lhside.equals(rhside);
      }
    
      public String getExpression()
      {
        return expression;
      }
    }
        
    /*
    ** public Fee(ResultSet rs)
    **
    ** Takes a result set, loads all standard fee item parameters, validates
    ** their presence.  Loads optional parameters from the params field.
    ** Creates a fee field based on the parameters that were loaded.
    */
    public Fee(ResultSet rs)
    {
      String setId      = null;
      String itemId     = null;
      String itemType   = null;
      String label      = null;
      String dispCat    = null;

      try
      {
        setId       = rs.getString("set_id");
        itemId      = rs.getString("item_id");
        itemType    = rs.getString("item_type");
        label       = rs.getString("label");
        dispCat     = rs.getString("disp_cat");
        
        // load standard fee parameters
        if (setId == null)
        {
          throw new Exception("Missing set_id in result set");
        }
        parms.add(new FeeParameter("setid="         + setId));
        if (itemId == null)
        {
          throw new Exception("Missing item_id in result set");
        }
        parms.add(new FeeParameter("itemid="        + itemId));
        if (itemType == null)
        {
          throw new Exception("Missing item_type in result set");
        }
        parms.add(new FeeParameter("itemtype="      + itemType));
        if (label == null)
        {
          throw new Exception("Missing label in result set");
        }
        parms.add(new FeeParameter("label="         + label));
        if (dispCat == null)
        {
          throw new Exception("Missing disp_cat in result set");
        }
        parms.add(new FeeParameter("dispcat="       + dispCat));
        
        // load optional fee parameters
        String parmStr = rs.getString("parms");
        if (parmStr != null)
        {
          StringTokenizer tok = new StringTokenizer(parmStr,";");
          while (tok.hasMoreTokens())
          {
            parms.add(new FeeParameter(tok.nextToken()));
          }
        }
        
        // load condition rules
        String ruleStr = rs.getString("allow_rules");
        if (ruleStr != null)
        {
          StringTokenizer tok = new StringTokenizer(ruleStr,";");
          while (tok.hasMoreTokens())
          {
            String expression = tok.nextToken();
            rules.add(new Rule(expression));
          }
        }
        
      }
      catch (Exception e)
      {
        
        String eMsg = "error creating Fee (" + toString() + ") - " 
          + e.toString();
        logEntry("Fee constructor",eMsg);
      }
    }
    
    /*
    ** protected Field createField()
    **
    ** Creates a field to hold the fee amount.  Depending on parameter
    ** settings the field created may or may not be a drop down.  The field
    ** is initialized with a default value if it is available.
    **
    ** RETURNS: the created field.
    */
    protected Field createField()
    {
      FeeParameter  dropTableParm   = getParm("droptable");
      FeeParameter  dropValsParm    = getParm("dropvals");
      Field         newField        = null;
      String        fieldName       = getFieldName();
      
      if ( dropTableParm != null )    // check for full drop table first
      {
        newField = new DropDownField(fieldName,new TranFeeTable(dropTableParm.asString()),true);
      }
      else if (dropValsParm != null)
      {
        newField = new DropDownField(fieldName,new DollarAmountTable(dropValsParm.asString()),true);
      }
      else
      {
        newField = new CurrencyField(fieldName,8,6,true);
      }
      FeeParameter defaultParm = getParm("default");
      if (defaultParm != null)
      {
        newField.setData(defaultParm.asString());
      }
      return newField;
    }
    
    /*
    ** protected String getFieldName()
    **
    ** Generates a field name that uniquely identifies the fee field.
    **
    ** RETURNS: the field name generated.
    */
    protected abstract String getFieldName();

    /*
    ** public FeeParameter getParm(String parmName)
    **
    ** Searches the parm vector for a parameter with name matching the given
    ** name.
    **
    ** RETURNS: the FeeParameter found, null if no match is made.
    */
    public FeeParameter getParm(String parmName)
    {
      for (Iterator i = parms.iterator(); i.hasNext();)
      {
        FeeParameter parm = (FeeParameter)i.next();
        if (parm.getName().equals(parmName))
        {
          return parm;
        }
      }
      return null;
    }

    public int getItemId()
    {
      return getParm("itemid").asInt();
    }
    public int getItemType()
    {
      return getParm("itemtype").asInt();
    }
    public String getLabel()
    {
      return getParm("label").asString();
    }
    public int getDispCat()
    {
      return getParm("dispcat").asInt();
    }
    public int getUpchrgType()
    {
      FeeParameter ucParm = getParm("upchrgtype");
      return (ucParm == null ? 0 : ucParm.asInt());
    }
    public double getUpchrgAmt()
    {
      FeeParameter ucParm = getParm("upchrgamt");
      return (ucParm == null ? 0 : ucParm.asDouble());
    }
    
    /*
    ** public boolean isAllowed()
    **
    ** Determines if the fee is allowed based on any conditional rules that
    ** are defined for this field.
    **
    ** RETURNS: true if all conditions pass, else false.
    */
    public boolean isAllowed()
    {
      for (Iterator i = rules.iterator(); i.hasNext();)
      {
        Rule rule = (Rule)i.next();
        try
        {
          if (!rule.isSatisfied())
          {
            return false;
          }
        }
        catch (Exception e)
        {
          System.out.println("Warning: invalid rule in fee item (" 
            + e.toString() + ")");
        }
      }
      return true;
    }

    public Field getField()
    {
      if (feeField == null)
      {
        feeField = createField();
      }
      return feeField;
    }

    public String toString()
    {
      StringBuffer strBuf = new StringBuffer();

      strBuf.append(this.getClass().getName() + " parms:");
      if (parms.size() > 0)
      {
        for (Iterator i = parms.iterator(); i.hasNext();)
        {
          strBuf.append(" " + i.next().toString());
        }
      }
      else
      {
        strBuf.append(" no defined parms");
      }
      
      if (feeField != null)
      {
        strBuf.append("; field ");
        strBuf.append(feeField.getName());
        strBuf.append(": '");
        strBuf.append(feeField.getData());
        strBuf.append("'");
      }
      else
      {
        strBuf.append("; no defined field");
      }

      return strBuf.toString();
    }
    
    public double getFeeAmount()
    {
      try
      {
        return feeField.asDouble();
      }
      catch (Exception e) {}
      
      return 0;
    }
  }
  
  /*
  ** Tran fee type definitions.  These correspond with columns in the
  ** tranchrg table.  Most transaction fees get stored in this table.
  **
  ** Mappings:
  **
  **   1  TFT_CARD_TYPE_CODE  -> card type
  **   2  TFT_DISC_RATE_TYPE  -> discount rate plan, v/mc only
  **   3  TFT_DISC_RATE       -> discount rate, v/mc only
  **   4  TFT_PASS_THRU       -> per item amt, v/mc only
  **   5  TFT_MMIN_CHRG       -> minimum monthly discount, v/mc only
  **   6  TFT_PER_TRAN        -> fee amount, non-v/mc only
  **   7  TFT_ARU_FEE         -> aru fee, v/mc only
  **   8  TFT_PER_AUTH        -> fee amount, v/mc only
  **   9  TFT_VOICE_FEE       -> voice auth fee, v/mc only
  **   10 TFT_IC_CAT          -> interchange category, v/mc only
  **   11 TFT_IC_TYPE         -> interchange bet type code, v/mc only
  **   12 TFT_DEBIT_BRG       -> debit billing rate groups, debit only
  **   13 TFT_AVS_FEE         -> avs (address verification) fee
  */
  public static final int TFT_CARD_TYPE_CODE      = 1;
  public static final int TFT_DISC_RATE_TYPE      = 2;
  public static final int TFT_DISC_RATE           = 3;
  public static final int TFT_PASS_THRU           = 4;
  public static final int TFT_MMIN_CHRG           = 5;
  public static final int TFT_PER_TRAN            = 6;
  public static final int TFT_ARU_FEE             = 7;
  public static final int TFT_PER_AUTH            = 8;
  public static final int TFT_VOICE_FEE           = 9;
  public static final int TFT_IC_CAT              = 10;
  public static final int TFT_IC_TYPE             = 11;
  public static final int TFT_DEBIT_BRG           = 12;
  public static final int TFT_AVS_FEE             = 13;
  
  public class TranFeeTypeDef
  {
    public String label;
    public int    tft;
    
    public TranFeeTypeDef(String label, int tft)
    {
      this.label  = label;
      this.tft    = tft;
    }
  }
  
  public TranFeeTypeDef[] tftDefs =
  {
    new TranFeeTypeDef("cardtype",  TFT_CARD_TYPE_CODE ),
    new TranFeeTypeDef("disctype",  TFT_DISC_RATE_TYPE ),
    new TranFeeTypeDef("discrate",  TFT_DISC_RATE      ),
    new TranFeeTypeDef("passthru",  TFT_PASS_THRU      ),
    new TranFeeTypeDef("mminchrg",  TFT_MMIN_CHRG      ),
    new TranFeeTypeDef("pertran",   TFT_PER_TRAN       ),
    new TranFeeTypeDef("arufee",    TFT_ARU_FEE        ),
    new TranFeeTypeDef("perauth",   TFT_PER_AUTH       ),
    new TranFeeTypeDef("voicefee",  TFT_VOICE_FEE      ),
    new TranFeeTypeDef("iccat",     TFT_IC_CAT         ),
    new TranFeeTypeDef("ictype",    TFT_IC_TYPE        ),
    new TranFeeTypeDef("debitbrg",  TFT_DEBIT_BRG      ),
    new TranFeeTypeDef("avsfee",    TFT_AVS_FEE        )
  };

  /*
  ** public class TranFee extends Fee
  **
  ** Extends Fee, adds handling of card type for transaction fees.  Tran fees
  ** are loaded using card type acceptance in conjunction with presence of
  ** corresponding fee items.  Acceptance is determined by card type records
  ** in merchpayoption.  Each card type may link to some number of fee items
  ** in appo_fee_items.  The fee items specifiy a tran fee type which maps
  ** to specific fields in tranchrg, which is where the actual tran fee amounts
  ** are stored.  Only some combinations of card type and tran fee type are
  ** allowed (generally: per tran for non-v/mc card types, per auth for v/mc,
  ** certain other types for v/mc, any other combo is not allowed).
  */
  public class TranFee extends Fee
  {
    private String tftLabel = null;
    
    public TranFee(ResultSet rs)
    {
      super(rs);

      String cardType     = null;
      String tranFeeType  = null;

      try
      {
        cardType    = rs.getString("card_type");
        tranFeeType = rs.getString("tran_fee_type");

        if (cardType == null)
        {
          throw new Exception("Missing card_type in result set");
        }
        parms.add(new FeeParameter("cardtype=" + cardType));
        if (tranFeeType == null)
        {
          throw new Exception("Missing tran_fee_type in result set");
        }

        // validate the card type/tran fee type combination before adding
        int ct = Integer.parseInt(cardType);
        int tft = Integer.parseInt(tranFeeType);
        switch (tft)
        {
          // these columns in the tranchrg record are v/mc pricing elements
          // that are not handled by FeeSet currently
          case TFT_CARD_TYPE_CODE:
          case TFT_DISC_RATE_TYPE:
          case TFT_DISC_RATE:
          case TFT_PASS_THRU:
          case TFT_IC_CAT:
          case TFT_IC_TYPE:
            throw new Exception("Unsupported tran fee type (" + tranFeeType
              + ")");

          // these columns in tranchrg are specific to v/mc
          // NOTE: unlike non-v/mc auth fees, the v/mc auth fee is stored
          // in the per_auth column
          case TFT_MMIN_CHRG:
          case TFT_ARU_FEE:
          case TFT_PER_AUTH:
          case TFT_VOICE_FEE:
          case TFT_AVS_FEE:
            if (ct != mesConstants.APP_CT_VISA)
            {
              throw new Exception("Tran fee type (" + tranFeeType + ") not "
                + "valid with card type (" + cardType + ")");
            }
            break;

          // all non-v/mc auth fees are stored in the per_tran column
          // NOTE: unlike v/mc auth fees, all non-v/mc auth fees are stored
          // in the per_tran column
          case TFT_PER_TRAN:
            if (ct == mesConstants.APP_CT_VISA ||
                ct == mesConstants.APP_CT_MC)
            {
              throw new Exception("Tran fee type (" + tranFeeType + ") not "
                + "valid with card type (" + cardType + ")");
            }
            break;
            
          case TFT_DEBIT_BRG:
            if (ct != mesConstants.APP_CT_DEBIT )
            {
              throw new Exception("Tran fee type (" + tranFeeType + ") not "
                + "valid with card type (" + cardType + ")");
            }
            break;

          default:
            throw new Exception("Invalid tran fee type (" + tranFeeType + ")");
        }
        
        parms.add(new FeeParameter("tranfeetype=" + tranFeeType));

        // map the tran fee type to a tran fee type label
        for (int i = 0; i < tftDefs.length; ++i)
        {
          if (tftDefs[i].tft == tft)
          {
            tftLabel = tftDefs[i].label;
            break;
          }
        }
      }
      catch (Exception e)
      {
        String eMsg = "error creating TranFee (" + toString() + ") - "
          + e.toString();
        logEntry("TranFee constructor",eMsg);
      }
    }
    
    protected String getFieldName()
    {
      return "tranFee_" + getParm("cardtype").asString() + "_" + tftLabel;
    }

    public int getCardType()
    {
      return getParm("cardtype").asInt();
    }
    
    public int getTranFeeType()
    {
      return getParm("tranfeetype").asInt();
    }

    public boolean isCardType( int ct )
    {
      return( getCardType() == ct );
    }
    
    public boolean isDebit()
    {
      return( isCardType(mesConstants.APP_CT_DEBIT) );
    }
    
    public boolean isVmc()
    {
      return( isCardType(mesConstants.APP_CT_VISA) ||
              isCardType(mesConstants.APP_CT_MC)   ||
              isCardType(mesConstants.APP_CT_VISA_CHECK_CARD) ||
              isCardType(mesConstants.APP_CT_MC_CHECK_CARD) ||
              isCardType(mesConstants.APP_CT_VISA_BUSINESS_CARD) ||
              isCardType(mesConstants.APP_CT_MC_BUSINESS_CARD) );
    }
  }

  /*
  ** public class EquipFee extends Fee
  **
  ** Extends fee, adds handling of equip model and lend type for equipment
  ** fees.
  */
  public class EquipFee extends Fee
  {
    public EquipFee(ResultSet rs)
    {
      super(rs);

      String model    = null;
      String lendType = null;
      String lendDesc = null;
      String quantity = null;

      try
      {
        model     = rs.getString("model");
        lendType  = rs.getString("lend_type");
        lendDesc  = rs.getString("lend_desc");
        quantity  = rs.getString("quantity");
        if (model == null)
        {
          throw new Exception("Missing model in result set");
        }
        parms.add(new FeeParameter("model="     + model));
        if (lendType == null)
        {
          throw new Exception("Missing lend_type in result set");
        }
        parms.add(new FeeParameter("lendtype="  + lendType));
        if (lendDesc == null)
        {
          throw new Exception("Missing lend_desc in result set");
        }
        parms.add(new FeeParameter("lenddesc="  + lendDesc));
        if (quantity == null)
        {
          throw new Exception("Missing quantity in result set");
        }
        parms.add(new FeeParameter("quantity="  + quantity));
      }
      catch (Exception e)
      {
        String eMsg = "error creating EquipFee (" + toString() + ") - "
          + e.toString();
        logEntry("EquipFee constructor",eMsg);
      }
    }

    protected String getFieldName()
    {
      return "equipFee_" + getParm("model").asString() + "_" 
        + getParm("lenddesc").asString();
    }

    public String getModel()
    {
      return getParm("model").asString();
    }

    public int getLendType()
    {
      return getParm("lendtype").asInt();
    }

    public String getLendDesc()
    {
      return getParm("lenddesc").asString();
    }

    public int getQuantity()
    {
      return getParm("quantity").asInt();
    }
    
    public boolean isMerchOwned()
    {
      return getParm("lendtype").asInt() == mesConstants.APP_EQUIP_OWNED;
    }
  }

  /*
  ** public class MiscFee extends Fee
  **
  ** Extends fee, adds handling of misc charge code for miscellaneous charge
  ** fees.
  */
  public class MiscFee extends Fee
  {
    public MiscFee(ResultSet rs)
    {
      super(rs);

      String miscCode = null;

      try
      {
        miscCode = rs.getString("misc_code");
        if (miscCode == null)
        {
          throw new Exception("Missing misc_code in result set");
        }
        parms.add(new FeeParameter("misccode=" + miscCode));
      }
      catch (Exception e)
      {
        String eMsg = "error creating MiscFee (" + toString() + ") - "
          + e.toString();
        logEntry("MiscFee constructor",eMsg);
      }
    }

    protected String getFieldName()
    {
      return "miscFee_" + getParm("misccode").asString();
    }

    public int getMiscCode()
    {
      return getParm("misccode").asInt();
    }
  }

  protected Hashtable ruleVars = new Hashtable();

  /*
  ** protected void initRuleVars()
  **
  ** Loads various values into a rule hashtable that may be referenced by
  ** rules governing fees being allowed, etc.
  **
  ** Rule vars loaded:
  **
  **    proctype    - processor type code (bb&t variable)
  **
  **    poscode     - type of payment solution (101 = dial terminal, 201 = 
  **                  Verisign payflow link, 302 = POS Partner 2000, etc.)
  **                  "MPOS" constants in mesConstants, found in merch_pos
  **
  **    postype     - more general categorization of payment solution
  **                  (1 = dial terminal, 2 = internet, etc.)
  **                  "PAYSOL" constants in mesConstants, found in
  **                  pos_category, linked to through merch_pos
  **
  **    hasdebit    - merchant accepts pin debit (cardtype 17 exists in
  **                  merchpayoptions table)
  **
  **    unrolleddebit - merchant accepts pin debit and selected unrolled pricing
  **
  **    haswireless - merchant has wireless equipment (has equipment with
  **                  prod_option_id 20, 21 or 22)
  **
  **    accountType - type of account (usually conversion vs new acct)
  */
  protected void initRuleVars()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      connect();

      log.debug("getting rule vars from database for appSeqNum: " + appSeqNum);
      /*@lineinfo:generated-code*//*@lineinfo:1199^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(m.processor,0)    as proc_type,
//                  nvl(mp.pos_code,0)    as pos_code,
//                  nvl(pc.pos_type,0)    as pos_type,
//                  nvl2(mpo_dbt.app_seq_num,'true','false')
//                                        as has_debit,
//                  nvl(m.account_type,'N') as account_type,
//                  nvl(m.existing_verisign,'N') as existing_verisign,
//                  nvl(mp.existing,'N')  as existing
//          from    merchant              m,
//                  merch_pos             mp,
//                  pos_category          pc,
//                  
//                  ( select  mpo.app_seq_num
//                    from    merchpayoption mpo
//                    where   mpo.app_seq_num = :appSeqNum
//                            and mpo.cardtype_code = 17 ) 
//  
//                                        mpo_dbt
//          where   m.app_seq_num = :appSeqNum
//                  and m.app_seq_num = mp.app_seq_num
//                  and mp.pos_code = pc.pos_code
//                  and m.app_seq_num = mpo_dbt.app_seq_num (+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(m.processor,0)    as proc_type,\n                nvl(mp.pos_code,0)    as pos_code,\n                nvl(pc.pos_type,0)    as pos_type,\n                nvl2(mpo_dbt.app_seq_num,'true','false')\n                                      as has_debit,\n                nvl(m.account_type,'N') as account_type,\n                nvl(m.existing_verisign,'N') as existing_verisign,\n                nvl(mp.existing,'N')  as existing\n        from    merchant              m,\n                merch_pos             mp,\n                pos_category          pc,\n                \n                ( select  mpo.app_seq_num\n                  from    merchpayoption mpo\n                  where   mpo.app_seq_num =  :1 \n                          and mpo.cardtype_code = 17 ) \n\n                                      mpo_dbt\n        where   m.app_seq_num =  :2 \n                and m.app_seq_num = mp.app_seq_num\n                and mp.pos_code = pc.pos_code\n                and m.app_seq_num = mpo_dbt.app_seq_num (+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.FeeSet",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1223^7*/
      rs = it.getResultSet();
      if (rs.next())
      {
        ruleVars.put("proctype",    rs.getString("proc_type"));
        ruleVars.put("postype",     rs.getString("pos_type" ));
        ruleVars.put("poscode",     rs.getString("pos_code" ));
        ruleVars.put("hasdebit",    rs.getString("has_debit" ));
        
        ruleVars.put("accountType", rs.getString("account_type"));
        ruleVars.put("existingPOS", rs.getString("existing"));
        ruleVars.put("existingVerisign", rs.getString("existing_verisign"));
      }
      
      rs.close();
      it.close();
      
      // determine if merchant has wireless equipment
      log.debug("getting wireless info");
      int wirelessCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:1243^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(*) 
//          
//          from    merchequipment
//          
//          where   app_seq_num = :appSeqNum
//                  and prod_option_id in ( 20, 21, 22 )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)  \n        \n        from    merchequipment\n        \n        where   app_seq_num =  :1 \n                and prod_option_id in ( 20, 21, 22 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.FeeSet",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   wirelessCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1251^7*/
      ruleVars.put("haswireless",(wirelessCount > 0 ? "true" : "false"));
      
      // determine if merchant owns their own pin pads (bb&t)
      log.debug("getting owns pinpad rule");
      int pinpadCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:1257^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(*) 
//          
//          from    merchequipment
//          
//          where   app_seq_num = :appSeqNum
//                  and equiptype_code = 3
//                  and equiplendtype_code = 3
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)  \n        \n        from    merchequipment\n        \n        where   app_seq_num =  :1 \n                and equiptype_code = 3\n                and equiplendtype_code = 3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.FeeSet",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   pinpadCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1266^7*/
      ruleVars.put("ownspinpad",(pinpadCount > 0 ? "true" : "false"));

      // determine a couple of postype specific conditions (used by bb&t)
      log.debug("postype specific rules");
      String posType = (String)ruleVars.get("postype");
      boolean isDialpay = posType
        .equals(Integer.toString(mesConstants.POS_DIAL_AUTH));
      boolean isVTLimited = posType
        .equals(Integer.toString(mesConstants.POS_VT_LIMITED));
      boolean isTtc     = posType
        .equals(Integer.toString(mesConstants.POS_TOUCHTONECAPTURE));
      ruleVars.put("isdialpay", (isDialpay  ? "true" : "false"));
      ruleVars.put("isvtlimited", (isVTLimited ? "true" : "false"));
      ruleVars.put("isttc",     (isTtc      ? "true" : "false"));
      
      // get pos feature rules
      log.debug("pos feature rules");
      /*@lineinfo:generated-code*//*@lineinfo:1284^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  avs_flag
//          from    pos_features
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  avs_flag\n        from    pos_features\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.app.FeeSet",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1289^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        // assign avs flag rule
        ruleVars.put("isavs", rs.getString("avs_flag"));
      }
      
      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("initRuleVars()",e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  /*
  ** private void initTranFees()
  **
  ** Loads transaction fees that are present in merchpayoption and have a
  ** corresponding fee item.  A fee item is generated for each found fee
  ** and placed in the feeItems vector.  These fees support pos_type,
  ** bbt_proc_type and pos_code filtering.
  */
  private void initTranFees()
  {
    ResultSetIterator it = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1330^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  afs.id                            set_id,
//                  afi.id                            item_id,
//                  afi.type_id                       item_type,
//                  nvl(afi.label,ct.cardtype_desc)   label,
//                  afi.disp_cat                      disp_cat,
//                  afi.parms                         parms,
//                  mpo.cardtype_code                 card_type,
//                  afi.tran_fee_type                 tran_fee_type,
//                  afi.allow_rules                   allow_rules
//          from    appo_fee_sets   afs,
//                  appo_fee_items  afi,
//                  merchpayoption  mpo,
//                  cardtype        ct,
//                  application     app
//          where   afs.id = :setId
//                  and afs.item_id = afi.id
//                  and afi.type_id = :IT_TRAN_FEES
//                  and mpo.app_seq_num = :appSeqNum
//                  and mpo.cardtype_code = afi.cardtype_code
//                  and mpo.cardtype_code = ct.cardtype_code
//                  and mpo.app_seq_num = app.app_seq_num
//                  and trunc(app.app_created_date) between 
//                      nvl(afi.date_available_from, trunc(app.app_created_date)) and 
//                      nvl(afi.date_available_to, trunc(app.app_created_date))
//          order by afi.id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  afs.id                            set_id,\n                afi.id                            item_id,\n                afi.type_id                       item_type,\n                nvl(afi.label,ct.cardtype_desc)   label,\n                afi.disp_cat                      disp_cat,\n                afi.parms                         parms,\n                mpo.cardtype_code                 card_type,\n                afi.tran_fee_type                 tran_fee_type,\n                afi.allow_rules                   allow_rules\n        from    appo_fee_sets   afs,\n                appo_fee_items  afi,\n                merchpayoption  mpo,\n                cardtype        ct,\n                application     app\n        where   afs.id =  :1 \n                and afs.item_id = afi.id\n                and afi.type_id =  :2 \n                and mpo.app_seq_num =  :3 \n                and mpo.cardtype_code = afi.cardtype_code\n                and mpo.cardtype_code = ct.cardtype_code\n                and mpo.app_seq_num = app.app_seq_num\n                and trunc(app.app_created_date) between \n                    nvl(afi.date_available_from, trunc(app.app_created_date)) and \n                    nvl(afi.date_available_to, trunc(app.app_created_date))\n        order by afi.id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,setId);
   __sJT_st.setInt(2,IT_TRAN_FEES);
   __sJT_st.setLong(3,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.app.FeeSet",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1357^7*/
      ResultSet rs = it.getResultSet();
      while (rs.next())
      {
        Fee fee = new TranFee(rs);
        if (fee.isAllowed())
        {
          fees.add(fee);
          fields.add(fee.getField());
        }
      }
    }
    catch (Exception e)
    {
      logEntry("initTranFees()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  /*
  ** private void initEquipFees()
  **
  ** Loads equipment fees.  Equipment fees are linked to fee items by model
  ** and lend type.  A fee item may or may not exist for a given equipment
  ** model/lend type combination.  Merchant owned equipment is consolidated
  ** into one fee item.
  */
  private void initEquipFees()
  {
    ResultSetIterator it = null;

    try
    {
      connect();

      // gets all purchased, rented, leased and purchased-refurbished
      // equipment items associated with this app as well as a single
      // record summing up any merchant owned equipment if any exists,
      // any fee items associated with equipment items are linked in
      // with corresponding equipment records
      /*@lineinfo:generated-code*//*@lineinfo:1401^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.model                        model,
//                  ei.lend_type                    lend_type,
//                  ei.lend_desc                    lend_desc,
//                  ei.quantity                     quantity,
//                  nvl( fi.label, ei.equip_desc )  label,
//                  nvl( fi.disp_cat, 2 )           disp_cat,
//                  :setId                          set_id,
//                  nvl( fi.item_id, -1 )           item_id,
//                  :IT_EQUIP_FEES                  item_type,
//                  fi.parms                        parms,
//                  fi.allow_rules                  allow_rules
//          from    ( select  me.equip_model                model,
//                            me.prod_option_id             prod_id,
//                            me.equiplendtype_code         lend_type,
//                            nvl( lt.equiplendtype_description, '--' )
//                                                          lend_desc,
//                            ea.equip_description          equip_desc,
//                            nvl( me.merchequip_equip_quantity, 0 )
//                                                          quantity
//                    from    merchequipment        me,
//                            equipment_application ea,
//                            equiplendtype         lt,
//                            prodoption            po,
//                            application           a
//                    where   me.app_seq_num = :appSeqNum
//                            and me.equip_model = ea.equip_model
//                            and nvl(me.prod_option_id, -1) = nvl(ea.prod_option_id, -1)
//                            and a.app_seq_num = me.app_seq_num
//                            and a.app_type = ea.app_type
//                            and me.prod_option_id = po.prod_option_id (+)
//                            and me.equiplendtype_code = lt.equiplendtype_code
//                            and me.equiplendtype_code in
//                            ( : mesConstants.APP_EQUIP_PURCHASE,        -- ( 1,
//                              : mesConstants.APP_EQUIP_RENT,            --   2,
//                              : mesConstants.APP_EQUIP_BUY_REFURBISHED, --   4,
//                              : mesConstants.APP_EQUIP_LEASE )          --   5 )
//                    union
//                    select  'OWNEDEQUIP'                  model,
//                            -1                            prod_id,
//                            : mesConstants.APP_EQUIP_OWNED -- 3
//                                                          lend_type,
//                            'Owned'                       lend_desc,
//                            'Equipment Support Fee'       equip_desc,
//                            nvl( sum( me.merchequip_equip_quantity ), 0 )
//                                                          quantity
//                    from    merchequipment  me
//                    where   me.app_seq_num = :appSeqNum
//                            and me.equiplendtype_code = : mesConstants.APP_EQUIP_OWNED -- 3
//                  ) ei,
//                  ( select  afs.id                  set_id,
//                            afi.id                  item_id,
//                            afi.type_id             item_type,
//                            afi.equip_model         model,
//                            afi.equiplendtype_code  lend_type,
//                            afi.prod_id             prod_id,
//                            afi.label               label,
//                            afi.disp_cat            disp_cat,
//                            afi.parms               parms,
//                            afi.allow_rules         allow_rules
//                    from    appo_fee_sets   afs,
//                            appo_fee_items  afi,
//                            application app
//                    where   afs.id = :setId
//                            and afs.item_id = afi.id
//                            and afi.type_id = :IT_EQUIP_FEES
//                            and app.app_seq_num = :appSeqNum
//                            and trunc(app.app_created_date) between 
//                              nvl(afi.date_available_from, trunc(app.app_created_date)) and
//                              nvl(afi.date_available_to, trunc(app.app_created_date))
//                  ) fi
//          where   ei.quantity > 0
//                  and ei.model = fi.model (+)
//                  --and ei.lend_type = fi.lend_type (+)
//                  --and ei.prod_id = fi.prod_id (+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.model                        model,\n                ei.lend_type                    lend_type,\n                ei.lend_desc                    lend_desc,\n                ei.quantity                     quantity,\n                nvl( fi.label, ei.equip_desc )  label,\n                nvl( fi.disp_cat, 2 )           disp_cat,\n                 :1                           set_id,\n                nvl( fi.item_id, -1 )           item_id,\n                 :2                   item_type,\n                fi.parms                        parms,\n                fi.allow_rules                  allow_rules\n        from    ( select  me.equip_model                model,\n                          me.prod_option_id             prod_id,\n                          me.equiplendtype_code         lend_type,\n                          nvl( lt.equiplendtype_description, '--' )\n                                                        lend_desc,\n                          ea.equip_description          equip_desc,\n                          nvl( me.merchequip_equip_quantity, 0 )\n                                                        quantity\n                  from    merchequipment        me,\n                          equipment_application ea,\n                          equiplendtype         lt,\n                          prodoption            po,\n                          application           a\n                  where   me.app_seq_num =  :3 \n                          and me.equip_model = ea.equip_model\n                          and nvl(me.prod_option_id, -1) = nvl(ea.prod_option_id, -1)\n                          and a.app_seq_num = me.app_seq_num\n                          and a.app_type = ea.app_type\n                          and me.prod_option_id = po.prod_option_id (+)\n                          and me.equiplendtype_code = lt.equiplendtype_code\n                          and me.equiplendtype_code in\n                          (  :4 ,        -- ( 1,\n                             :5 ,            --   2,\n                             :6 , --   4,\n                             :7  )          --   5 )\n                  union\n                  select  'OWNEDEQUIP'                  model,\n                          -1                            prod_id,\n                           :8  -- 3\n                                                        lend_type,\n                          'Owned'                       lend_desc,\n                          'Equipment Support Fee'       equip_desc,\n                          nvl( sum( me.merchequip_equip_quantity ), 0 )\n                                                        quantity\n                  from    merchequipment  me\n                  where   me.app_seq_num =  :9 \n                          and me.equiplendtype_code =  :10  -- 3\n                ) ei,\n                ( select  afs.id                  set_id,\n                          afi.id                  item_id,\n                          afi.type_id             item_type,\n                          afi.equip_model         model,\n                          afi.equiplendtype_code  lend_type,\n                          afi.prod_id             prod_id,\n                          afi.label               label,\n                          afi.disp_cat            disp_cat,\n                          afi.parms               parms,\n                          afi.allow_rules         allow_rules\n                  from    appo_fee_sets   afs,\n                          appo_fee_items  afi,\n                          application app\n                  where   afs.id =  :11 \n                          and afs.item_id = afi.id\n                          and afi.type_id =  :12 \n                          and app.app_seq_num =  :13 \n                          and trunc(app.app_created_date) between \n                            nvl(afi.date_available_from, trunc(app.app_created_date)) and\n                            nvl(afi.date_available_to, trunc(app.app_created_date))\n                ) fi\n        where   ei.quantity > 0\n                and ei.model = fi.model (+)\n                --and ei.lend_type = fi.lend_type (+)\n                --and ei.prod_id = fi.prod_id (+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,setId);
   __sJT_st.setInt(2,IT_EQUIP_FEES);
   __sJT_st.setLong(3,appSeqNum);
   __sJT_st.setInt(4, mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(5, mesConstants.APP_EQUIP_RENT);
   __sJT_st.setInt(6, mesConstants.APP_EQUIP_BUY_REFURBISHED);
   __sJT_st.setInt(7, mesConstants.APP_EQUIP_LEASE);
   __sJT_st.setInt(8, mesConstants.APP_EQUIP_OWNED);
   __sJT_st.setLong(9,appSeqNum);
   __sJT_st.setInt(10, mesConstants.APP_EQUIP_OWNED);
   __sJT_st.setInt(11,setId);
   __sJT_st.setInt(12,IT_EQUIP_FEES);
   __sJT_st.setLong(13,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.app.FeeSet",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1477^7*/
      ResultSet rs = it.getResultSet();
      while (rs.next())
      {
        Fee fee = new EquipFee(rs);
        if (fee.isAllowed())
        {
          fees.add(fee);
          fields.add(fee.getField());
        }
      }
    }
    catch (Exception e)
    {
      logEntry("initEquipFees()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  /*
  ** private void initMiscFees()
  **
  ** Loads miscellaneous charge fees that are present in the fee item table
  ** for this fee set.  These fees are driven by the fee item table, unlike
  ** the other fee types that have dependencies on other tables.
  */
  private void initMiscFees()
  {
    ResultSetIterator it = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1515^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  afs.id            set_id,
//                  afi.id            item_id,
//                  afi.type_id       item_type,
//                  nvl(afi.label,md.misc_description)
//                                    label,
//                  afi.disp_cat      disp_cat,
//                  afi.parms         parms,
//                  afi.misc_code     misc_code,
//                  afi.allow_rules   allow_rules
//          from    appo_fee_sets     afs,
//                  appo_fee_items    afi,
//                  miscdescrs        md,
//                  application       app
//          where   afs.id = :setId
//                  and afs.item_id = afi.id
//                  and afi.type_id = :IT_MISC_FEES
//                  and afi.misc_code = md.misc_code (+)
//                  and app.app_seq_num = :appSeqNum
//                  and trunc(app.app_created_date) between 
//                    nvl(afi.date_available_from, trunc(app.app_created_date)) and 
//                    nvl(afi.date_available_to, trunc(app.app_created_date))
//          order by afi.id      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  afs.id            set_id,\n                afi.id            item_id,\n                afi.type_id       item_type,\n                nvl(afi.label,md.misc_description)\n                                  label,\n                afi.disp_cat      disp_cat,\n                afi.parms         parms,\n                afi.misc_code     misc_code,\n                afi.allow_rules   allow_rules\n        from    appo_fee_sets     afs,\n                appo_fee_items    afi,\n                miscdescrs        md,\n                application       app\n        where   afs.id =  :1 \n                and afs.item_id = afi.id\n                and afi.type_id =  :2 \n                and afi.misc_code = md.misc_code (+)\n                and app.app_seq_num =  :3 \n                and trunc(app.app_created_date) between \n                  nvl(afi.date_available_from, trunc(app.app_created_date)) and \n                  nvl(afi.date_available_to, trunc(app.app_created_date))\n        order by afi.id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,setId);
   __sJT_st.setInt(2,IT_MISC_FEES);
   __sJT_st.setLong(3,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.app.FeeSet",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1539^7*/
      ResultSet rs = it.getResultSet();
      while (rs.next())
      {
        Fee fee = new MiscFee(rs);
        if (fee.isAllowed())
        {
          fees.add(fee);
          fields.add(fee.getField());
        }
      }
    }
    catch (Exception e)
    {
      logEntry("initMiscFees()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
  }
  
  protected void initCustomFees()
  {
  }
  
  /*
  ** private void initFees()
  **
  ** Determine the fee set to use then initialize each category of fees.
  ** Categories include card type/transaction based fees, equipment fees,
  ** miscellaneous charges and other fees.
  */
  private void initFees()
  {
    try
    {
      // get the fee set (load using app id or get user's default set)
      setId = getFeeSetId();
      
      // init fee parameters defs
      initFeeParameterDefs();

      // initialize rule variables
      initRuleVars();
      
      // initialize each fee category
      initTranFees();
      initMiscFees();
      initEquipFees();
      
      initCustomFees();
    }
    catch (Exception e)
    {
      logEntry("initFees()",e.toString());
    }
  }

  /*
  ** public FeeSet(UserBean user, long appSeqNum)
  **
  ** Initializes the fee set to contain fee items corresponding with the
  ** appSeqNum given, or if no previous fee set assignment exists for the 
  ** app an assignment is made using the user's default.
  */
  public FeeSet(UserBean user, long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
    this.user = user;

    // initializes fees
    initFees();
  }
  
  
  /*
  ** protected Vector getFeeSubset(Class getClass)
  **
  ** Creates a vector and loads it with all fees that have the given class.
  **
  ** RETURNS: Vector containing the found fees of the getClass.
  */
  protected Vector getFeeSubset(Class getClass)
  {
    Vector feeVec = new Vector();
    for (Iterator i = fees.iterator(); i.hasNext();)
    {
      Fee fee = (Fee)i.next();
      if (fee.getClass() == getClass)
      {
        feeVec.add(fee);
      }
    }
    return feeVec;
  }
  public TranFee[] getTranFees()
  {
    return (TranFee[])getFeeSubset(TranFee.class).toArray(new TranFee[0]);
  }
  public EquipFee[] getEquipFees()
  {
    return (EquipFee[])getFeeSubset(EquipFee.class).toArray(new EquipFee[0]);
  }
  public MiscFee[] getMiscFees()
  {
    return (MiscFee[])getFeeSubset(MiscFee.class).toArray(new MiscFee[0]);
  }
  
  public static final int DC_TRAN   = 1;
  public static final int DC_EQUIP  = 2;
  public static final int DC_MISC   = 3;
  
  /*
  ** public Fee[] getCatFees(int dispCat)
  **
  ** Creates an array loaded with fees of a given display category.
  **
  ** RETURNS: Fee array containing matching fees.
  */
  public Fee[] getCatFees(int dispCat)
  {
    Vector feeVec = new Vector();
    for (Iterator i = fees.iterator(); i.hasNext();)
    {
      Fee fee = (Fee)i.next();
      if (fee.getDispCat() == dispCat)
      {
        feeVec.add(fee);
      }
    }
    return (Fee[])feeVec.toArray(new Fee[0]);
  }
  public Fee[] getTranCatFees()
  {
    return getCatFees(DC_TRAN);
  }
  public Fee[] getEquipCatFees()
  {
    return getCatFees(DC_EQUIP);
  }
  public Fee[] getMiscCatFees()
  {
    return getCatFees(DC_MISC);
  }
  
  /*
  ** public Fee getFee(int itemId)
  **
  ** Searches for a fee of any type with the given fee item id.
  **
  ** RETURNS: matching fee if found, else null.
  */
  public Fee getFee(int itemId)
  {
    for (Iterator i = fees.iterator(); i.hasNext();)
    {
      Fee fee = (Fee)i.next();
      if (fee.getItemId() == itemId)
      {
        return fee;
      }
    }
    return null;
  }
  
  /*
  ** private TranFee getTranFee(int cardType, int feeType)
  **
  ** Searches for a tran fee that matches the given card type and tran fee
  ** type.
  **
  ** RETURNS: matching fee if found, else null.
  */
  private TranFee getTranFee(int cardType, int tranFeeType)
  {
    for (Iterator i = fees.iterator(); i.hasNext();)
    {
      Fee fee = (Fee)i.next();
      if (fee instanceof TranFee)
      {
        TranFee tranFee = (TranFee)fee;
        if (tranFee.getCardType()     == cardType && 
            tranFee.getTranFeeType()  == tranFeeType)
        {
          return tranFee;
        }
      }
    }
    return null;
  }

  /*
  ** private void loadTranFee(int cardType, int tranFeeType, double feeAmount)
  **
  ** Loads a given fee amount into the fee field that corresponds with the
  ** given card type and tran fee type.  Originally would not set a zero fee, 
  ** but this was causing trouble on the bbt application when a zero fee was 
  ** selected it would get overridden by the default value
  */
  private void loadTranFee(int cardType, int tranFeeType, double feeAmount, boolean allowZeroFee)
  {
    // make sure a non-zero fee amount is being set, unless a zero fee is allowed
    if (feeAmount > 0 || (allowZeroFee && feeAmount == 0))
    {
      // set the fee amount in the corresponding fee field
      Field feeField = null;

      //put this code in a try block so it doesnt crash in the case where a fee does not exist.
      //if a fee doesn't exist, it will just skip it and not try to set the fee amount
      try
      {
        feeField = getTranFee(cardType,tranFeeType).getField();
      }
      catch(Exception e)
      {
      }

      if (feeField != null)
      {
        if(tranFeeType == TFT_DEBIT_BRG)
        {
          // have to set brg field padded to match format
          feeField.setData(NumberFormatter.getDoubleString(feeAmount, "00000"));
        }
        else
        {
          feeField.setData(Double.toString(feeAmount));
        }
      }
    }
  }

  /*
  ** private void loadTranFee(int cardType, int tranFeeType, double feeAmount)
  **
  ** Loads a given fee amount into the fee field that corresponds with the
  ** given card type and tran fee type.
  */
  private void loadTranFee(int cardType, int tranFeeType, double feeAmount)
  {
    loadTranFee(cardType, tranFeeType, feeAmount, false);
  }


  /*
  ** private void loadTranFees()
  **
  ** Loads all tran fees with any data available in tranchrg for this app.
  */
  private void loadTranFees()
  {
    ResultSetIterator it = null;
    
    try
    {
      // get all tranchrg recs for this app
      /*@lineinfo:generated-code*//*@lineinfo:1797^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code         card_type,
//                  tranchrg_per_auth     vmc_per_tran,
//                  tranchrg_mmin_chrg    vmc_min_disc,
//                  tranchrg_aru_fee      vmc_aru_fee,
//                  tranchrg_voice_fee    vmc_voice_fee,
//                  tranchrg_per_tran     nonvmc_per_tran,
//                  tranchrg_brg          tranchrg_brg
//          from    tranchrg
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code         card_type,\n                tranchrg_per_auth     vmc_per_tran,\n                tranchrg_mmin_chrg    vmc_min_disc,\n                tranchrg_aru_fee      vmc_aru_fee,\n                tranchrg_voice_fee    vmc_voice_fee,\n                tranchrg_per_tran     nonvmc_per_tran,\n                tranchrg_brg          tranchrg_brg\n        from    tranchrg\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.app.FeeSet",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1808^7*/
      
      // process each record, handle according to card type
      ResultSet rs = it.getResultSet();
      while (rs.next())
      {
        int cardType = rs.getInt("card_type");
        switch (cardType)
        {
          // handle all the v/mc fees
          case mesConstants.APP_CT_VISA:
            loadTranFee(cardType,TFT_PER_AUTH,  rs.getDouble("vmc_per_tran"),true);
            loadTranFee(cardType,TFT_MMIN_CHRG, rs.getDouble("vmc_min_disc"),true);
            loadTranFee(cardType,TFT_ARU_FEE,   rs.getDouble("vmc_aru_fee"),true);
            loadTranFee(cardType,TFT_VOICE_FEE, rs.getDouble("vmc_voice_fee"),true);
            break;
          
          // these are mirrors of visa data, just ignore
          case mesConstants.APP_CT_VISA_CHECK_CARD:
          case mesConstants.APP_CT_VISA_BUSINESS_CARD:
          case mesConstants.APP_CT_MC:
          case mesConstants.APP_CT_MC_CHECK_CARD:
          case mesConstants.APP_CT_MC_BUSINESS_CARD:
            break;
            
          case mesConstants.APP_CT_DEBIT:
            loadTranFee(cardType,TFT_DEBIT_BRG, rs.getDouble("tranchrg_brg"),true);
            loadTranFee(cardType,TFT_PER_TRAN,  rs.getDouble("nonvmc_per_tran"),true);
            break;
            
          // any other card type just load the non-v/mc per tran field amount
          default:
            loadTranFee(cardType,TFT_PER_TRAN,  rs.getDouble("nonvmc_per_tran"),true);
            break;
        }
      }
    }
    catch (Exception e)
    {
      logEntry("loadTranFees()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception ce) {}
    }
  }
  
  /*
  ** private EquipFee getEquipFee(String model, int lendType)
  **
  ** Searches for an equip fee that matches the given model and lend type.
  **
  ** RETURNS: matching fee if found, else null.
  */
  private EquipFee getEquipFee(String model, int lendType)
  {
    for (Iterator i = fees.iterator(); i.hasNext();)
    {
      Fee fee = (Fee)i.next();
      if (fee instanceof EquipFee)
      {
        EquipFee equipFee = (EquipFee)fee;
        if (equipFee.getModel().equals(model) &&
            equipFee.getLendType() == lendType)
        {
          return equipFee;
        }
      }
    }
    return null;
  }
  
  /*
  ** private void loadEquipFee(String model, int lendType, double feeAmount)
  **
  ** Loads a given fee amount into the fee field that corresponds with the
  ** given model and lend type.
  */
  private void loadEquipFee(String model, int lendType, double feeAmount)
  {
    // make sure a non-zero fee amount is being set
    if (feeAmount > 0)
    {
      // set the fee amount in the corresponding fee field
      Field feeField = getEquipFee(model,lendType).getField();
      if (feeField != null)
      {
        feeField.setData(Double.toString(feeAmount));
      }
    }
  }
  
  /*
  ** private void loadEquipFees()
  **
  ** Loads all equip fees with any data available in merchequipment for this 
  ** app.
  */
  private void loadEquipFees()
  {
    ResultSetIterator it = null;
    
    try
    {
      // get all equipment records for this app (gets only 1 merchant
      // owned record since they should all have the same fee amount)
      /*@lineinfo:generated-code*//*@lineinfo:1914^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_model                       model,
//                  equiplendtype_code                lend_type,
//                  merchequip_amount                 amount
//          from    merchequipment
//          where   app_seq_num = :appSeqNum
//                  and equiplendtype_code in
//                  ( :mesConstants.APP_EQUIP_PURCHASE,         -- 1
//                    :mesConstants.APP_EQUIP_RENT,             -- 2
//                    :mesConstants.APP_EQUIP_BUY_REFURBISHED,  -- 4
//                    :mesConstants.APP_EQUIP_LEASE )           -- 5
//          union
//          select  'OWNEDEQUIP'                      model,
//                  : mesConstants.APP_EQUIP_OWNED lend_type,
//                  merchequip_amount                 amount
//          from    merchequipment
//          where   app_seq_num = :appSeqNum
//                  and equiplendtype_code = : mesConstants.APP_EQUIP_OWNED
//                  and rownum = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_model                       model,\n                equiplendtype_code                lend_type,\n                merchequip_amount                 amount\n        from    merchequipment\n        where   app_seq_num =  :1 \n                and equiplendtype_code in\n                (  :2 ,         -- 1\n                   :3 ,             -- 2\n                   :4 ,  -- 4\n                   :5  )           -- 5\n        union\n        select  'OWNEDEQUIP'                      model,\n                 :6  lend_type,\n                merchequip_amount                 amount\n        from    merchequipment\n        where   app_seq_num =  :7 \n                and equiplendtype_code =  :8 \n                and rownum = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_RENT);
   __sJT_st.setInt(4,mesConstants.APP_EQUIP_BUY_REFURBISHED);
   __sJT_st.setInt(5,mesConstants.APP_EQUIP_LEASE);
   __sJT_st.setInt(6, mesConstants.APP_EQUIP_OWNED);
   __sJT_st.setLong(7,appSeqNum);
   __sJT_st.setInt(8, mesConstants.APP_EQUIP_OWNED);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.app.FeeSet",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1934^7*/
      
      // process each record, handle according to model and lend_type
      ResultSet rs = it.getResultSet();
      while (rs.next())
      {
        loadEquipFee(rs.getString("model"),rs.getInt("lend_type"),
          rs.getDouble("amount"));
      }
    }
    catch (Exception e)
    {
      logEntry("loadEquipFees()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception ce) {}
    }
  }
  
  /*
  ** private MiscFee getMiscFee(int miscCode)
  **
  ** Searches for misc fee fee that matches the given misc code.
  **
  ** RETURNS: matching fee if found, else null.
  */
  private MiscFee getMiscFee(int miscCode)
  {
    for (Iterator i = fees.iterator(); i.hasNext();)
    {
      Fee fee = (Fee)i.next();
      if (fee instanceof MiscFee)
      {
        MiscFee miscFee = (MiscFee)fee;
        if (miscFee.getMiscCode() == miscCode)
        {
          return miscFee;
        }
      }
    }
    return null;
  }
  
  /*
  ** private void loadMiscFee(int miscCode, double feeAmount, String uniqueDesc)
  **
  ** Loads a given fee amount into the fee field that corresponds with the
  ** given misc code.  Unique fee description should also be handled here.
  */
  private void loadMiscFee(int miscCode, double feeAmount, String uniqueDesc, boolean allowZeroFee)
  {
    try
    {    
      // make sure a non-zero fee amount is being set, unless a zero fee is allowed
      if (feeAmount > 0 || (allowZeroFee && feeAmount == 0))
      {
        // set the fee amount in the corresponding fee field
        MiscFee miscFee = getMiscFee(miscCode);
        if (miscFee != null)
        {
          Field feeField = miscFee.getField();
          if (feeField != null)
          {
            feeField.setData(Double.toString(feeAmount));
          }
        }
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::loadMiscFee("
        + "miscCode: " + miscCode + ", amt: " + feeAmount + ", desc: "
        + uniqueDesc + "): " + e.toString());
      logEntry("loadMiscFee()",e.toString());
    }
  }
  
  private void loadMiscFee(int miscCode, double feeAmount, String uniqueDesc)
  {
    loadMiscFee(miscCode, feeAmount, uniqueDesc, false);
  }


  /*
  ** private void loadMiscFees()
  **
  ** Loads all misc charge fees with any data available in miscchrg for this 
  ** app.
  */
  private void loadMiscFees()
  {
    ResultSetIterator it = null;
    
    try
    {
      // get all misc charge records for this app
      /*@lineinfo:generated-code*//*@lineinfo:2031^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  misc_code             misc_code,
//                  misc_chrg_amount      fee_amount,
//                  misc_chrgbasis_descr  unique_desc
//          from    miscchrg
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  misc_code             misc_code,\n                misc_chrg_amount      fee_amount,\n                misc_chrgbasis_descr  unique_desc\n        from    miscchrg\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.app.FeeSet",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2038^7*/
      
      // process each record, handle according to misc code
      ResultSet rs = it.getResultSet();
      while (rs.next())
      {
        loadMiscFee(rs.getInt("misc_code"),rs.getDouble("fee_amount"),
          rs.getString("unique_desc"),true);
      }
    }
    catch (Exception e)
    {
      logEntry("loadMiscFees()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception ce) {}
    }
  }
  
  protected void loadCustomFees()
  {
  }

  /*
  ** public void load()
  **
  ** Loads any stored fee data into fee fields.
  */
  public void load()
  {
    try
    {
      connect();
      
      loadTranFees();
      loadEquipFees();
      loadMiscFees();
      
      loadCustomFees();
    }
    finally
    {
      cleanUp();
    }
  }
  
  public boolean autoLoad()
  {
    load();
    return true;
  }
  
  /*
  ** private void submitTranFees()
  **
  ** Stores tran fee amounts in the tranchrg table.
  */
  private void submitTranFees()
  {
    try
    {
      // first handle non-v/mc tranchrg records, start by deleting all
      // tranchrg records that are not v/mc
      /*@lineinfo:generated-code*//*@lineinfo:2102^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    tranchrg
//          where   app_seq_num = :appSeqNum
//                  and cardtype_code not in 
//                    ( :mesConstants.APP_CT_VISA, 
//                      :mesConstants.APP_CT_VISA_CHECK_CARD,
//                      :mesConstants.APP_CT_VISA_BUSINESS_CARD,
//                      :mesConstants.APP_CT_MC,
//                      :mesConstants.APP_CT_MC_CHECK_CARD,
//                      :mesConstants.APP_CT_MC_BUSINESS_CARD )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    tranchrg\n        where   app_seq_num =  :1 \n                and cardtype_code not in \n                  (  :2 , \n                     :3 ,\n                     :4 ,\n                     :5 ,\n                     :6 ,\n                     :7  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   __sJT_st.setInt(3,mesConstants.APP_CT_VISA_CHECK_CARD);
   __sJT_st.setInt(4,mesConstants.APP_CT_VISA_BUSINESS_CARD);
   __sJT_st.setInt(5,mesConstants.APP_CT_MC);
   __sJT_st.setInt(6,mesConstants.APP_CT_MC_CHECK_CARD);
   __sJT_st.setInt(7,mesConstants.APP_CT_MC_BUSINESS_CARD);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2114^7*/
      
      // insert a tranchrg record for each found non-vmc tran fee
      TranFee[] tranFees = getTranFees();
      for (int i = 0; i < tranFees.length; ++i)
      {
        if (tranFees[i].getCardType() != mesConstants.APP_CT_VISA &&
            tranFees[i].getCardType() != mesConstants.APP_CT_MC &&
            tranFees[i].getCardType() != mesConstants.APP_CT_VISA_CHECK_CARD &&
            tranFees[i].getCardType() != mesConstants.APP_CT_MC_CHECK_CARD &&
            tranFees[i].getCardType() != mesConstants.APP_CT_VISA_BUSINESS_CARD &&
            tranFees[i].getCardType() != mesConstants.APP_CT_MC_BUSINESS_CARD )
        {
          if ( tranFees[i].getTranFeeType() == TFT_DEBIT_BRG )
          { 
            String brgTable = tranFees[i].getField().getData();
            
            if ( brgTable != null && !brgTable.equals("") )
            {
              // assumes that there was a debit per item field before this 
              // field in the fee set.  the previous debit item will have
              // created the entry into tranchrg.
              /*@lineinfo:generated-code*//*@lineinfo:2136^15*/

//  ************************************************************
//  #sql [Ctx] { update  tranchrg
//                  set     tranchrg_brg = :brgTable
//                  where   app_seq_num = :appSeqNum and
//                          cardtype_code =  : tranFees[i].getCardType()
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2120 =  tranFees[i].getCardType();
   String theSqlTS = "update  tranchrg\n                set     tranchrg_brg =  :1 \n                where   app_seq_num =  :2  and\n                        cardtype_code =   :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,brgTable);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,__sJT_2120);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2142^15*/
            }              
          }
          else    // assume per item fee
          {
            /*@lineinfo:generated-code*//*@lineinfo:2147^13*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//                ( app_seq_num,
//                  cardtype_code,
//                  tranchrg_per_tran )
//                values
//                ( :appSeqNum,
//                  : tranFees[i].getCardType(),
//                  : tranFees[i].getField().asDouble() )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2121 =  tranFees[i].getCardType();
 double __sJT_2122 =  tranFees[i].getField().asDouble();
   String theSqlTS = "insert into tranchrg\n              ( app_seq_num,\n                cardtype_code,\n                tranchrg_per_tran )\n              values\n              (  :1 ,\n                 :2 ,\n                 :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,__sJT_2121);
   __sJT_st.setDouble(3,__sJT_2122);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2157^13*/
          }            
        }
      }
    
      // collect v/mc fees
      double authFee  = 0;
      double voiceFee = 0;
      double aruFee   = 0;
      double minDisc  = 0;
      double avsFee   = 0;
      for (int i = 0; i < tranFees.length; ++i)
      {
        if (tranFees[i].getCardType() == mesConstants.APP_CT_VISA)
        {
          switch (tranFees[i].getTranFeeType())
          {
            case TFT_PER_AUTH:
              authFee = tranFees[i].getField().asDouble();
              break;
              
            case TFT_VOICE_FEE:
              voiceFee = tranFees[i].getField().asDouble();
              break;
              
            case TFT_ARU_FEE:
              aruFee = tranFees[i].getField().asDouble();
              break;
              
            case TFT_MMIN_CHRG:
              minDisc = tranFees[i].getField().asDouble();
              break;
              
            case TFT_AVS_FEE:
              avsFee = tranFees[i].getField().asDouble();
              break;
          }
        }
      }
      
      // just update any visa/mastercard records with the auth fees.
      // the records must exist in tranchrg due to the PricingBase submitVmcPricing() method
      // which occurs before this code is executed.
      
      // update or insert visa record data
      /*@lineinfo:generated-code*//*@lineinfo:2202^7*/

//  ************************************************************
//  #sql [Ctx] { update  tranchrg
//          set     tranchrg_mmin_chrg  = :minDisc,
//                  tranchrg_per_auth   = :authFee,
//                  tranchrg_aru_fee    = :aruFee,
//                  tranchrg_voice_fee  = :voiceFee,
//                  tranchrg_avs_fee    = :avsFee
//          where   app_seq_num         = :appSeqNum
//                  and cardtype_code in
//                  ( 
//                    : mesConstants.APP_CT_VISA,
//                    : mesConstants.APP_CT_VISA_CHECK_CARD,
//                    : mesConstants.APP_CT_VISA_BUSINESS_CARD,
//                    : mesConstants.APP_CT_MC,
//                    : mesConstants.APP_CT_MC_CHECK_CARD,
//                    : mesConstants.APP_CT_MC_BUSINESS_CARD
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  tranchrg\n        set     tranchrg_mmin_chrg  =  :1 ,\n                tranchrg_per_auth   =  :2 ,\n                tranchrg_aru_fee    =  :3 ,\n                tranchrg_voice_fee  =  :4 ,\n                tranchrg_avs_fee    =  :5 \n        where   app_seq_num         =  :6 \n                and cardtype_code in\n                ( \n                   :7 ,\n                   :8 ,\n                   :9 ,\n                   :10 ,\n                   :11 ,\n                   :12 \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,minDisc);
   __sJT_st.setDouble(2,authFee);
   __sJT_st.setDouble(3,aruFee);
   __sJT_st.setDouble(4,voiceFee);
   __sJT_st.setDouble(5,avsFee);
   __sJT_st.setLong(6,appSeqNum);
   __sJT_st.setInt(7, mesConstants.APP_CT_VISA);
   __sJT_st.setInt(8, mesConstants.APP_CT_VISA_CHECK_CARD);
   __sJT_st.setInt(9, mesConstants.APP_CT_VISA_BUSINESS_CARD);
   __sJT_st.setInt(10, mesConstants.APP_CT_MC);
   __sJT_st.setInt(11, mesConstants.APP_CT_MC_CHECK_CARD);
   __sJT_st.setInt(12, mesConstants.APP_CT_MC_BUSINESS_CARD);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2220^7*/
    }
    catch (Exception e)
    {
      logEntry("submitTranFees()",e.toString());
    }
  }
  
  /*
  ** private void submitEquipFees()
  **
  ** Stores equip fee amounts in the merchequipment table.
  */
  private void submitEquipFees()
  {
    try
    {
      // iterate through equipment fee items, update corresponding records
      // in merchequipment with the fee amount
      EquipFee[] equipFees = getEquipFees();
      for (int i = 0; i < equipFees.length; ++i)
      {
        // merch owned fee item needs to be applied to all equipment records
        // that are marked as merchant owned
        if (equipFees[i].isMerchOwned())
        {
          /*@lineinfo:generated-code*//*@lineinfo:2246^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchequipment
//              set     merchequip_amount       = : equipFees[i].getField().asDouble()
//              where   app_seq_num             = :appSeqNum
//                      and equiplendtype_code  = : mesConstants.APP_EQUIP_OWNED
//                      
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_2123 =  equipFees[i].getField().asDouble();
   String theSqlTS = "update  merchequipment\n            set     merchequip_amount       =  :1 \n            where   app_seq_num             =  :2 \n                    and equiplendtype_code  =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_2123);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3, mesConstants.APP_EQUIP_OWNED);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2253^11*/
        }
        // each other fee item corresponds with a specific record with
        // matching equip lend type code and equip model code
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:2259^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchequipment
//              set     merchequip_amount       = : equipFees[i].getField().asDouble()
//              where   app_seq_num             = :appSeqNum
//                      and equiplendtype_code  = : equipFees[i].getLendType()
//                      and equip_model         = : equipFees[i].getModel()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_2124 =  equipFees[i].getField().asDouble();
 int __sJT_2125 =  equipFees[i].getLendType();
 String __sJT_2126 =  equipFees[i].getModel();
   String theSqlTS = "update  merchequipment\n            set     merchequip_amount       =  :1 \n            where   app_seq_num             =  :2 \n                    and equiplendtype_code  =  :3 \n                    and equip_model         =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_2124);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,__sJT_2125);
   __sJT_st.setString(4,__sJT_2126);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2266^11*/
        }
      }
    }
    catch (Exception e)
    {
      logEntry("submitEquipFees()",e.toString());
    }
  }
  
  /*
  ** private void submitMiscFees()
  **
  ** Stores misc fee amounts in the miscchrg table.
  */
  private void submitMiscFees()
  {
    try
    {
      // place all non-zero fees in database
      MiscFee[] miscFees = getMiscFees();
      for (int i = 0; i < miscFees.length; ++i)
      {
        // determine if fee record exists
        int feeCnt = 0;
        /*@lineinfo:generated-code*//*@lineinfo:2291^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//            
//            from    miscchrg
//            where   app_seq_num = :appSeqNum
//                    and misc_code = : miscFees[i].getMiscCode()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2127 =  miscFees[i].getMiscCode();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n           \n          from    miscchrg\n          where   app_seq_num =  :1 \n                  and misc_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.app.FeeSet",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,__sJT_2127);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   feeCnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2298^9*/
        
        // process the fee field
        Field feeField = miscFees[i].getField();
        if (feeField != null)
        {
          double feeAmount = feeField.asDouble();
          
          // insert new record
          if (feeCnt == 0)
          {
            /*@lineinfo:generated-code*//*@lineinfo:2309^13*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//                ( app_seq_num,
//                  misc_code,
//                  misc_chrg_amount )
//                values
//                ( :appSeqNum,
//                  : miscFees[i].getMiscCode(),
//                  :feeAmount )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2128 =  miscFees[i].getMiscCode();
   String theSqlTS = "insert into miscchrg\n              ( app_seq_num,\n                misc_code,\n                misc_chrg_amount )\n              values\n              (  :1 ,\n                 :2 ,\n                 :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,__sJT_2128);
   __sJT_st.setDouble(3,feeAmount);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2319^13*/
          }
          // record exists, update with new amount
          else
          {
            /*@lineinfo:generated-code*//*@lineinfo:2324^13*/

//  ************************************************************
//  #sql [Ctx] { update  miscchrg
//                set     misc_chrg_amount = :feeAmount
//                where   app_seq_num = :appSeqNum
//                        and misc_code = : miscFees[i].getMiscCode()
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2129 =  miscFees[i].getMiscCode();
   String theSqlTS = "update  miscchrg\n              set     misc_chrg_amount =  :1 \n              where   app_seq_num =  :2 \n                      and misc_code =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.app.FeeSet",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,feeAmount);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,__sJT_2129);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2330^13*/
          }
        }
      }
    }
    catch (Exception e)
    {
      logEntry("submitMiscFees()",e.toString());
    }
  }
  
  protected void submitCustomFees()
  {
  }
 
 
  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
  }
 
  
  /*
  ** public void submit()
  **
  ** Submits all fee amounts to database storage.
  */
  public void submit()
  {
    try
    {
      connect();
      
      submitTranFees();
      submitEquipFees();
      submitMiscFees();
      
      submitCustomFees();
    }
    finally
    {
      cleanUp();
    }
  }
  
  public boolean autoSubmit()
  {
    submit();
    return true;
  }
}/*@lineinfo:generated-code*/