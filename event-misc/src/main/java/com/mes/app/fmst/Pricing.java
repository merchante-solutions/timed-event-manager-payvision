/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/fmst/Pricing.java $

  Description:

  Pricing

  CB&T online app pricing page bean.  Extends PricingBase with CB&T custom
  pricing options.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 3/10/04 12:19p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.fmst;

import javax.servlet.http.HttpServletRequest;
//import sqlj.runtime.ResultSetIterator;
import com.mes.app.PricingBase;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;

public class Pricing extends PricingBase
{
  {
    appType = 37;
    curScreenId = 3;
  }
  
  /*************************************************************************
  **
  **   Drop Down Tables
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Radio Button Lists
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Validations
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Custom Fields
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Field Bean
  **
  **************************************************************************/
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      connect();
      
      // initialize the wireless equipment object
      wirelessSet = new WirelessEquipmentSet(appSeqNum);
      add(wirelessSet);
      
      // v/mc pricing options
      
      // first main street dummy value of 1
      fields.setData("pricingPlan","1");

      // setup bet options
      fields.setData("betType","30");
      Field betSet30 = new DropDownField("betSet_30",new BetSet(30),false);
      fields.add(betSet30);
      fields.add(new BetSetMapperField());
      betSet30.setOptionalCondition(
        new FieldValueCondition(fields.getField("betType"),"30"));
        
      // set the field style class
      fields.setHtmlExtra("class=\"formText\"");

      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      e.printStackTrace();
      logEntry("createFields()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*************************************************************************
  **
  **   Load
  **
  **************************************************************************/

  /*************************************************************************
  **
  **   Submit
  **
  **************************************************************************/
}
