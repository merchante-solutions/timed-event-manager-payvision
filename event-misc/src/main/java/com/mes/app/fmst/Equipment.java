/**************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/fmst/Equipment.java $

  Description:
  
  Equipment
  
  Banner Bank online application equipment page bean.


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/04/04 11:32a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.fmst;

import javax.servlet.http.HttpServletRequest;
//import sqlj.runtime.ResultSetIterator;
import com.mes.app.EquipmentBase;
import com.mes.constants.mesConstants;

public class Equipment extends EquipmentBase
{
  {
    appType = 37;
    curScreenId = 2;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // reset all fields, including those just added to be of class formText
      fields.setHtmlExtra("class=\"formText\"");
      
      // set default shipping method to be overnight
      fields.setData("shippingMethod",
        Integer.toString(mesConstants.SHIPPING_METHOD_FEDEX_2DAY));
      
      
      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
}
