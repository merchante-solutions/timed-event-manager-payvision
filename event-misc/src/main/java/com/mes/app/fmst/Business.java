/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/fmst/Business.java $

  Description:
  
  Business
  
  CB&T online application merchant information page bean.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 3/08/04 9:37a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.fmst;

import javax.servlet.http.HttpServletRequest;
import com.mes.forms.DropDownField;

public class Business extends com.mes.app.BusinessBase
{
  {
    appType = 37;
    curScreenId = 1;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // make some fields required
      fields.getField("accountType").makeRequired();
      fields.getField("taxpayerId").makeRequired();
      fields.getField("yearsOpen").makeRequired();
      
      // make some fields not required
      fields.getField("sourceOfInfo").makeOptional();
      
      // add referring branch assoc num dropdown
      fields.add(new DropDownField("assocNum",new BranchTable(3941400067L),false));
      
      // make bank account and transit confirmation fields optional (not used)
      fields.getField("confirmCheckingAccount").makeOptional();
      fields.getField("confirmTransitRouting").makeOptional();

      // create the extended fields for POS partner
      createPosPartnerExtendedFields();

      // reset all fields, including those just added to be of class form
      fields.setHtmlExtra("class=\"formText\"");
      
      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
    
    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }
  }
}










                               