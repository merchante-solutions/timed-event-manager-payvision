/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/bcb/Pricing.java $

  Description:

  Pricing

  CB&T online app pricing page bean.  Extends PricingBase with CB&T custom
  pricing options.

  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/29/04 4:24p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.bcb;

import javax.servlet.http.HttpServletRequest;
//import sqlj.runtime.ResultSetIterator;
import com.mes.app.PricingBase;
import com.mes.constants.mesConstants;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;

public class Pricing extends PricingBase
{
  {
    appType = 38;
    curScreenId = 3;
  }
  
  /*************************************************************************
  **
  **   Drop Down Tables
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Radio Button Lists
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Validations
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Custom Fields
  **
  **************************************************************************/
  
  /*************************************************************************
  **
  **   Field Bean
  **
  **************************************************************************/
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      connect();
      
      // initialize the wireless equipment object
      wirelessSet = new WirelessEquipmentSet(appSeqNum);
      add(wirelessSet);
      
      // v/mc pricing options
      
      fields.setData("pricingPlan","3");

      // setup bet options
      fields.setData("betType","31");
      
      Field betSet31 = new DropDownField("betSet_31",new BetSet(31),false);
      fields.add(betSet31);
      fields.add(new BetSetMapperField());
      betSet31.setOptionalCondition(
        new FieldValueCondition(fields.getField("betType"),"31"));
        
      // require the e-commerce fields when user selected internet
      if ( isUsingProductType( mesConstants.POS_INTERNET ) )
      {
        fields.getField("inventoryValue").makeRequired();
        fields.getField("inventoryAddr").makeRequired();
        fields.getField("inventoryCsz").makeRequired();
        fields.getField("fulfillmentHouse").makeRequired();
        fields.getField("securitySoftware").makeRequired();
      }
        
      // set the field style class
      fields.setHtmlExtra("class=\"formText\"");

      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      e.printStackTrace();
      logEntry("createFields()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*************************************************************************
  **
  **   Load
  **
  **************************************************************************/

  /*************************************************************************
  **
  **   Submit
  **
  **************************************************************************/
}
