/*@lineinfo:filename=Equipment*//*@lineinfo:user-code*//*@lineinfo:1^1*//**************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/bbt/Equipment.sqlj $

  Description:
  
  Equipment
  
  BBT Bank online application equipment page bean.


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/28/04 12:19p $
  Version            : $Revision: 29 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.bbt;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.EquipmentBase;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.Condition;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NameField;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public final class Equipment extends EquipmentBase
{
  {
    appType     = 11;
    curScreenId = 3;
  }

  protected static final String[][] BBT_TipOptionRadioButtons = 
  {
    { "Tip Option On",  "Y" },
    { "Tip Option Off", "N" }
  };

  protected static final String[][] BBT_TrainingTypeRadioButtons = 
  {
    { "Training Only",             "T"  }, 
    { "Reprogram Only",            "R"  },
    { "Training and Reprograming", "B"  }

  };
  
  protected static final String[][] BBT_EquipSplySrvcsRadioButtons = 
  {
     { "EMS Services",        "EMS Services"      }
    ,{ "Per Incident",        "Per Incident"      }
    ,{ "EMS Supplies Only",   "EMS Supplies Only" }
    ,{ "Lease Rental",        "Lease Rental"      }
  };
  
  public static final String[][] BBT_EquipSelectDisplay =
  {
    { "imprinter",  "Imprinters"            },  // mesConstants.APP_EQUIP_TYPE_IMPRINTER        
    { "termPinPad", "Integrated Devices"    },  // mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD
    { "termPrint",  "Terminal/Printer Sets" },  // mesConstants.APP_EQUIP_TYPE_TERM_PRINTER     
    { "terminal",   "Terminals"             },  // mesConstants.APP_EQUIP_TYPE_TERMINAL         
    { "printer",    "Printers"              },  // mesConstants.APP_EQUIP_TYPE_PRINTER          
    { "pinPad",     "Pinpads"               },  // mesConstants.APP_EQUIP_TYPE_PINPAD           
    { "peripheral", "Peripherals"           },  // mesConstants.APP_EQUIP_TYPE_PERIPHERAL       
  };
  
  protected static final String[][] BBT_FraudControlButtons =
  {
    { "Last 4 Digits Of Card",  "1" },
    { "Full Card Number",       "2" },
  };

  protected static final String[][] AutoManualCloseRadioButtons = 
  {
    { "Auto Batch Close","Y"  }, 
    { "Manual Close","N"      }
  };



  protected class BBTShippingMethodTable extends DropDownTable
  {
    public BBTShippingMethodTable( )
    {
      addElement(Integer.toString(mesConstants.SHIPPING_METHOD_2DAY),          "2 Day"         );
      //addElement(Integer.toString(mesConstants.SHIPPING_METHOD_OVERNIGHT_AM),  "Overnight AM"  );
      //addElement(Integer.toString(mesConstants.SHIPPING_METHOD_OVERNIGHT_SAT), "Overnight Sat.");
      //addElement(Integer.toString(mesConstants.SHIPPING_METHOD_STANDARD),      "Standard"      );
      addElement(Integer.toString(mesConstants.SHIPPING_METHOD_1TO3DAY),       "1-3 Day");
      addElement(Integer.toString(mesConstants.SHIPPING_METHOD_NEXTDAYAM),     "Next Day Morning");
      addElement(Integer.toString(mesConstants.SHIPPING_METHOD_NEXTDAYPM),     "Next Day Afternoon");
      addElement(Integer.toString(mesConstants.SHIPPING_METHOD_SATURDAY),      "Saturday Delivery");
      addElement(Integer.toString(mesConstants.SHIPPING_METHOD_GROUND),        "Ground");
      addElement(Integer.toString(mesConstants.SHIPPING_METHOD_COD),           "COD");
      addElement(Integer.toString(mesConstants.SHIPPING_METHOD_USPS),          "USPS");
    }
  }
  
  protected class BBTShippingAddressTable extends DropDownTable
  {
    public BBTShippingAddressTable( )
    {
      addElement( Integer.toString(mesConstants.ADDR_TYPE_MSR),       "MSR"               );
      addElement( Integer.toString(mesConstants.ADDR_TYPE_BUSINESS),  "Merchant Business" );
      addElement( Integer.toString(mesConstants.ADDR_TYPE_MAILING),   "Merchant Mailing"  );
      addElement( Integer.toString(mesConstants.ADDR_TYPE_NONE),      "Other"             );
    }
  }

  protected class BBTEquipPaymentMethodTable extends DropDownTable
  {
    public BBTEquipPaymentMethodTable( )
    {
      addElement( "",                                   "select one" );
      addElement( mesConstants.PAY_METH_PURCHASE,       "Merchant Check (0)" );
      addElement( mesConstants.PAY_METH_INVOICE,        "Our Invoice (1)"               );
      addElement( mesConstants.PAY_METH_ACH,            "ACH Merchant (2)"              );
      addElement( mesConstants.PAY_METH_STATEMENT,      "Merchant Statement Post (C)"   );
      addElement( mesConstants.PAY_METH_RENTAL,         "Merchant Monthly Rental (I)"   );
      addElement( mesConstants.PAY_METH_LEASE_24MONTHS, "Merchant 24 Month Lease (R)"   );
      addElement( mesConstants.PAY_METH_LEASE_36MONTHS, "Merchant 36 Month Lease (T)"   );
      addElement( mesConstants.PAY_METH_LEASE_48MONTHS, "Merchant 48 Month Lease (J)"   );
    }
  }
  
  protected class PasswordProtectOptions extends FieldGroup
  {
    String[][] options = 
    {
       { "Credit",       "Credit"     } 
      ,{ "Auth Only",    "Auth Only"  }
      ,{ "Offline",      "Offline"    }
    };

    public PasswordProtectOptions(String fname)
    {
      super(fname);
      
      for (int i = 0; i < options.length; ++i)
      {
        add(new CheckboxField(options[i][0],options[i][1],false));
      }

      addValidation(new OrphanedPswdPtctOptionsValidation());
    }

    public class OrphanedPswdPtctOptionsValidation implements Validation
    {
      public OrphanedPswdPtctOptionsValidation()
      {
      }
    
      public String getErrorText()
      {
        return "The Password Protect terminal feature must be selected if one or more associated options are selected.";
      }
    
      public boolean validate(String fdata)
      {
        // m.o.: ensure no orphaned pp options exist
        
        if(((CheckboxField)fields.getField("passwordProtectEnable")).isChecked())
          return true;
        
        for (int i = 0; i < options.length; ++i)
        {
          if(((CheckboxField)getField(options[i][0])).isChecked())
            return false;
        }

        return true;
      }

    }

    public void load()
    {
      try {
        long appSeqNum = Equipment.this.fields.getField("appSeqNum").asLong();
        String sppo = null;
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:200^9*/

//  ************************************************************
//  #sql [Ctx] { select     decode(password_protect_authonly,1,',Auth Only') || decode(password_protect_credit,1,',Credit') || decode(password_protect_offline,1,',Offline')
//  
//            
//  
//            from       pos_features
//          
//            where      app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select     decode(password_protect_authonly,1,',Auth Only') || decode(password_protect_credit,1,',Credit') || decode(password_protect_offline,1,',Offline')\n\n           \n\n          from       pos_features\n        \n          where      app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.bbt.Equipment",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sppo = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:209^9*/
        this.setData(sppo);
       }
       catch(Exception e) {
       }
       finally {
        cleanUp();
       }
    }

    public void store()
    {
      try {
        long    appSeqNum = Equipment.this.fields.getField("appSeqNum").asLong();
        String  ppoData   = this.getData();
        String  sAuthOnly = (ppoData.indexOf("Auth Only") >=0)? "1":"0";
        String  sCredit   = (ppoData.indexOf("Credit")    >=0)? "1":"0";
        String  sOffline  = (ppoData.indexOf("Offline")   >=0)? "1":"0";
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:228^9*/

//  ************************************************************
//  #sql [Ctx] { update     pos_features
//  
//            set        PASSWORD_PROTECT_AUTHONLY  = :sAuthOnly
//                      ,PASSWORD_PROTECT_CREDIT    = :sCredit
//                      ,PASSWORD_PROTECT_OFFLINE   = :sOffline
//  
//            where     app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update     pos_features\n\n          set        PASSWORD_PROTECT_AUTHONLY  =  :1 \n                    ,PASSWORD_PROTECT_CREDIT    =  :2 \n                    ,PASSWORD_PROTECT_OFFLINE   =  :3 \n\n          where     app_seq_num =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.app.bbt.Equipment",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,sAuthOnly);
   __sJT_st.setString(2,sCredit);
   __sJT_st.setString(3,sOffline);
   __sJT_st.setLong(4,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:237^9*/
      }
      catch(Exception e) {
        System.out.println("PasswordProtectOptions.store() EXCEPTION: "+e.toString());
        logEntry("PasswordProtectOptions.store()",e.toString());
      }
      finally {
        cleanUp();
      }
    }
    
    protected String renderHtmlField()
    {
      StringBuffer sb = new StringBuffer();

      sb.append("<table>");
        sb.append("<tr>");

          sb.append("<td class=\"formFields\"><i>Options: </i></td>");

          for (Iterator i = iterator(); i.hasNext(); ) {
            Field f = (Field)i.next();

            sb.append("<td class=\"formFields\">");
              sb.append(f.renderHtml());
            sb.append("</td>");
            sb.append("<td width=10>&nbsp;</td>");
          }

        sb.append("</tr>");
      sb.append("</table>");
      
      return sb.toString();
    }

    public String getData()
    {
      StringBuffer optionString = new StringBuffer();
      CheckboxField cbf = null;
      for (int i = 0; i < options.length; ++i)
      {
        cbf = (CheckboxField)getField(options[i][0]);
        optionString.append(',');
        if(cbf.isChecked()) {
          optionString.append(options[i][0]);
        }
      }
      return optionString.substring(1);
    }
    
    protected String processData(String rawData)
    {
      
      // reset
      for (int i = 0; i < options.length; ++i) {
        setData(options[i][0],"n");
      }
      
      StringTokenizer st = new StringTokenizer(rawData, ",");
      while (st.hasMoreTokens()) {
        String t = st.nextToken();
        if(t.length() > 0) {
          setData(t,"y");
        }
      }
      
      return rawData;
    }
  
  }


  protected class BbtEquipValidation implements Validation
  {
    private Field   rentalCount         = null;
    private Field   purchaseCount       = null;
    private Field   leaseCount          = null;
    private Field   ownedCount          = null;
    private Field   refurbCount         = null;
    private Field   swapCount           = null;
    private String  errorText           = null;

    public BbtEquipValidation( Field[] fields)
    {
      this.rentalCount   = fields[0];
      this.purchaseCount = fields[1];
      this.leaseCount    = fields[2];
      this.ownedCount    = fields[3];
      this.refurbCount   = fields[4];
      this.swapCount     = fields[5];

    }               
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      
      if (rentalCount.asInteger()   > 0 || 
          purchaseCount.asInteger() > 0 || 
          leaseCount.asInteger()    > 0 || 
          refurbCount.asInteger()   > 0 || 
          swapCount.asInteger()     > 0)
      {
        if(fdata.equals("HPT77R"))
        {
          errorText = "Hypercom T77 (Friction) can only be merchant owned";
        }
        else if(fdata.equals("NRT2085"))
        {
          errorText = "Lipman Nurit 2085 can only be merchant owned";
        }
        else if(fdata.equals("VFOMNI3210IPPP"))
        {
          errorText = "Verifone Omni 3210 can only be merchant owned";
        }
        else if(fdata.equals("VFOMNI3300IP"))
        {
          errorText = "Verifone Omni 3300 can only be merchant owned";
        }
        else if(fdata.equals("VFTRANZ420IP"))
        {
          errorText = "Verifone Tranz 420 can only be merchant owned";
        }
        else if(fdata.equals("VFTRANZ460IP"))
        {
          errorText = "Verifone Tranz 460 can only be merchant owned";
        }
        else if(fdata.equals("HPT7EIP"))
        {
          errorText = "Hypercom T7E can only be merchant owned";
        }
        else if(fdata.equals("VFOMNI395IP"))
        {
          errorText = "Verifone Omni 395 can only be merchant owned";
        }
        else if(fdata.equals("VFOMNI396IP"))
        {
          errorText = "Verifone Omni 396 can only be merchant owned";
        }
        else if(fdata.equals("VFTRANZ330"))
        {
          errorText = "Verifone Tranz 330 (256k) can only be merchant owned";
        }
        else if(fdata.equals("ZXJP"))
        {
          errorText = "Verifone Zon Jr Plus can only be merchant owned";
        }
        else if(fdata.equals("VFZONXL"))
        {
          errorText = "Verifone Zon Jr XL can only be merchant owned";
        }
        else if(fdata.equals("VFXL300"))
        {
          errorText = "Verifone Zon Jr XL 300 can only be merchant owned";
        }
        else if(fdata.equals("CIDP560"))
        {
          errorText = "Citizens Printer 560 can only be merchant owned";
        }
        else if(fdata.equals("CIDP562"))
        {
          errorText = "Citizens Printer 562 can only be merchant owned";
        }
        else if(fdata.equals("HPP7R"))
        {
          errorText = "Hypercom P7 can only be merchant owned";
        }
        else if(fdata.equals("HPP8R"))
        {
          errorText = "Hypercom P8 can only be merchant owned";
        }
        else if(fdata.equals("VFPP300"))
        {
          errorText = "PrintPak 300 can only be merchant owned";
        }
        else if(fdata.equals("VFPP350"))
        {
          errorText = "PrintPak 350 can only be merchant owned";
        }
        else if(fdata.equals("IVIS612"))
        {
          errorText = "Scribe 612 can only be merchant owned";
        }
      }

      if (rentalCount.asInteger()   > 0 || 
          purchaseCount.asInteger() > 0 || 
          leaseCount.asInteger()    > 0 || 
          refurbCount.asInteger()   > 0)
      {
        if(fdata.equals("HPICE5500P15"))
        {
          errorText = "Hypercom ICE 5500 can only be swapped";
        }
        else if(fdata.equals("NRT2085PLUS"))
        {
          errorText = "Lipman Nurit 2085+ can only be swapped";
        }
        else if(fdata.equals("NRT3020IPP"))
        {
          errorText = "Lipman Nurit 3020 can only be swapped";
        }
        else if(fdata.equals("DSTIPPS"))
        {
          errorText = "Thales Talento T-IPPS can only be swapped";
        }
        else if(fdata.equals("NRT202"))
        {
          errorText = "Lipman Nurit 202 can only be swapped";
        }
        else if(fdata.equals("NRT252"))
        {
          errorText = "Lipman Nurit 252 can only be swapped";
        }
        else if(fdata.equals("VF102"))
        {
          errorText = "Verifone PinPad 102 can only be swapped";
        }
      }

      if (rentalCount.asInteger()   > 0 || 
          purchaseCount.asInteger() > 0 || 
          leaseCount.asInteger()    > 0 || 
          swapCount.asInteger()     > 0)
      {

        if(fdata.equals("HPT7PRIP"))
        {
          errorText = "Hypercom T7P (Friction) only available refurbished";
        }
        else if(fdata.equals("HPT7PTIP"))
        {
          errorText = "Hypercom T7PT (Thermal) only available refurbished";
        }
        else if(fdata.equals("VFOMNI3200IP"))
        {
          errorText = "Verifone Omni 3200 only available refurbished";
        }
        else if(fdata.equals("VFP250"))
        {
          errorText = "Verifone P-250 only available refurbished";
        }
        else if(fdata.equals("VF1000"))
        {
          errorText = "Verifone PinPad 1000 only available refurbished";
        }
      }

      if (refurbCount.asInteger() > 0)
      {
        if(fdata.equals("HPT7PLUS512"))
        {
          errorText = "Hypercom T7+ not available in refurbished";
        }
        else if(fdata.equals("HPS915FT"))
        {
          errorText = "Hypercom S-9 PIN pad not available in refurbished";
        }
        else if(fdata.equals("VF1000SE"))
        {
          errorText = "Verifone PIN pad 1000se not available in refurbished";
        }
        else if(fdata.equals("CRRDM6004F"))
        {
          errorText = "Check Reader RDM - 6004f not available in refurbished";
        }
      }

      if (rentalCount.asInteger()   > 0 || 
          leaseCount.asInteger()    > 0 || 
          refurbCount.asInteger()   > 0 || 
          swapCount.asInteger()     > 0)
      {

        if(fdata.equals("MTMWEDGE"))
        {
          errorText = "MagTek Card Reader White Mini Wedge can only be purchased new";
        }
        else if(fdata.equals("OMNI3750SWVLMNT"))
        {
          errorText = "Omni 3750 Swivel Stand can only be purchased new";
        }

      }
      return (errorText == null);
    }
  }



  public class EDCCondition implements Condition
  {
    private boolean isEdc = false;
    
    public EDCCondition()
    {
      isEdc = (getPaymentMethod() == mesConstants.POS_DIAL_TERMINAL);
    }
    
    public boolean isMet()
    {
      return isEdc;
    }
  }

  public class GovPurchCardCondition implements Condition
  {
    private boolean isGpc = false;
    
    public GovPurchCardCondition()
    {
      isGpc = (getBusinessNature() == mesConstants.BUSINESS_NATURE_GOV_PURCHCARD);
    }
    
    public boolean isMet()
    {
      return isGpc;
    }
  }

  public class IfGovPurchCardThenReqValidation implements Validation
  {
    private String    errorText   = "Required if business nature is Gov't/Purchasing Card";
    private Condition con         = null;
    
    public IfGovPurchCardThenReqValidation(Condition con)
    {
      this.con = con;  
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean isValid = true;
      
      if (con.isMet() && (fdata == null || fdata.equals("n")) )
      {
        isValid = false;
      }
      
      return isValid;
    }
  }

  public class MotoKeytailCondition implements Condition
  {
    private boolean isMkt = false;
    
    public MotoKeytailCondition()
    {
      isMkt = (getBusinessNature() == mesConstants.BUSINESS_NATURE_KEYTAIL || getBusinessNature() == mesConstants.BUSINESS_NATURE_MOTO);
    }
    
    public boolean isMet()
    {
      return isMkt;
    }
  }

  public class IfMotoKeytailThenReqValidation implements Validation
  {
    private String    errorText   = "Required if business nature is MOTO or Key Tail";
    private Condition con         = null;
    
    public IfMotoKeytailThenReqValidation(Condition con)
    {
      this.con = con;  
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean isValid = true;
      
      if (con.isMet() && (fdata == null || fdata.equals("n")) )
      {
        isValid = false;
      }
      
      return isValid;
    }
  }




  public class EquipPayMethValidation implements Validation
  {
    private String    errorText   = "Required if equipment payment method is ";
    private Field     fld         = null;
    private String    payMeth     = null;
    private String    payMethDesc = null;

    public EquipPayMethValidation(Field fld, String payMeth, String payMethDesc)
    {
      this.fld          = fld;  
      this.payMeth      = payMeth;
      this.payMethDesc  = payMethDesc;
    }
    
    public String getErrorText()
    {
      return errorText + payMethDesc;
    }
    
    public boolean validate(String fdata)
    {
      boolean isValid = true;
      
      if (!fld.isBlank() && fld.getData().equals(payMeth) && fdata.equals(""))
      {
        isValid = false;
      }
      
      return isValid;
    }
  }


  public class IfNotNullRequiredValidation implements Validation
  {
    private String    errorText   = "Required if quantity (greater than zero) is specified for this plate size";
    private Field     fld         = null;

    public IfNotNullRequiredValidation(Field fld)
    {
      this.fld = fld;  
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean isValid = true;
      
      if (!fld.isBlank() && fld.asInteger() > 0 && fdata.equals(""))
      {
        isValid = false;
      }
      
      return isValid;
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    Field           field           = null;
    String          fieldName       = null;
    
    super.createFields(request);
    
    try
    {
      fields.setData("appType",Integer.toString(appType));
      
      boolean integratedDvcsFieldsMade = false;
      
      for( int etype = 0; etype < EquipmentFieldNames.length; ++etype )
      {
        fieldName = EquipmentFieldNames[etype];
        
        // remove all the existing owned fields
        // and replace with hidden owned fields
        if ( fieldName != null )
        {
          // add refurbished counts
          fields.add( new NumberField( (fieldName + "RefurbCount"),3,2,true,0 ) );
          fields.add( new NumberField( (fieldName + "SwapCount"),3,2,true,0 ) );
          
          // owned is just another column of needed models
          fields.deleteField(fieldName + "OwnedModels");
          fields.add( new HiddenField(fieldName + "OwnedModels") );
        
          // remove existing needed to reset the validation
          fields.deleteField( (fieldName + "NeededModels") );

          // add the new field
          field = new DropDownField( (fieldName + "NeededModels"), new EquipmentSelectionTable( appType, etype, mesConstants.APP_ES_NEEDED ), true );
          field.addValidation( new EquipCountValidation( new Field[] 
                                                        {
                                                          fields.getField( (fieldName + "RentalCount") ), 
                                                          fields.getField( (fieldName + "PurchaseCount") ), 
                                                          fields.getField( (fieldName + "LeaseCount") ),
                                                          fields.getField( (fieldName + "OwnedCount") ),
                                                          fields.getField( (fieldName + "RefurbCount") ),
                                                          fields.getField( (fieldName + "SwapCount") ), 
                                                        }, 
                                                        new String[]
                                                        { 
                                                          "Please provide a number of items to buy, rent or lease OR change equipment selection",
                                                          "Please select a model of equipment to buy, rent or lease OR remove the quantity" 
                                                        }  ) );
          field.addValidation( new BbtEquipValidation( new Field[] 
                                                        {
                                                          fields.getField( (fieldName + "RentalCount"  ) ), 
                                                          fields.getField( (fieldName + "PurchaseCount") ), 
                                                          fields.getField( (fieldName + "LeaseCount"   ) ),
                                                          fields.getField( (fieldName + "OwnedCount"   ) ),
                                                          fields.getField( (fieldName + "RefurbCount"  ) ), 
                                                          fields.getField( (fieldName + "SwapCount"    ) ), 
                                                        } ) );
          fields.add( field );
        }
      }

      // Imprinters: Purchase only count supported
      fields.getField("imprinterRentalCount").makeReadOnly();
      fields.getField("imprinterLeaseCount").makeReadOnly();
      fields.getField("imprinterOwnedCount").makeReadOnly();
      fields.getField("imprinterRefurbCount").makeReadOnly();
      fields.getField("imprinterSwapCount").makeReadOnly();
      fields.getField("terminalSwapCount").makeReadOnly();
      fields.getField("printerSwapCount").makeReadOnly();
      fields.getField("peripheralSwapCount").makeReadOnly();
      fields.getField("termPrintSwapCount").makeReadOnly();

      
      // *** Terminal Features ***
      GovPurchCardCondition GPCC = new GovPurchCardCondition();
      MotoKeytailCondition  MKTC = new MotoKeytailCondition();

      fields.add( new CheckboxField("avsEnable","AVS/Invoice",(GPCC.isMet() || MKTC.isMet())));
      fields.getField("avsEnable").addValidation(new IfGovPurchCardThenReqValidation(new GovPurchCardCondition()));
      fields.getField("avsEnable").addValidation(new IfMotoKeytailThenReqValidation(new MotoKeytailCondition()));


      fields.add( new CheckboxField("purchasingCardEnable","Gov't/Purchasing Card",GPCC.isMet()));
      fields.getField("purchasingCardEnable").addValidation(new IfGovPurchCardThenReqValidation(new GovPurchCardCondition()));

      fields.add(new RadioButtonField ("tipOptionFlag",BBT_TipOptionRadioButtons,-1,false,"Required"));

      
      fields.add( new RadioButtonField( "autoCloseEnable", AutoManualCloseRadioButtons, -1, false, "Please select either auto batch close or manual close" ) );
      // if EDC is selected on Business page, 
      //  either Manual or Auto Close is required

      OrCondition oc = new OrCondition();
      oc.add(new GovPurchCardCondition());
      oc.add(new EDCCondition());

      ((RadioButtonField)fields.getField("autoCloseEnable")).setOptionalCondition(oc);

      fields.getField("autoCloseEnable").addValidation( new ActionTimeValidation( new Field[] 
                                                    { fields.getField("autoCloseHour"),
                                                      fields.getField("autoCloseMin"),
                                                      fields.getField("autoCloseAmPm") },
                                                     AutoCloseErrorMessages ) );



      fields.getField("accessCodeEnable").setLabel("Prefix to Dial Out");

      fields.getField("resetTranNumEnable").setLabel("Reset Reference Number");

      // 
      fields.deleteField( "terminalApp" );
      fields.add( new HiddenField( "terminalApp" ) );
      
      // dump the lease fields to remove validations, then 
      // add them back so the base class functions correctly.
      fields.deleteField( "leaseCompany" );
      fields.deleteField( "leaseAcctNum" );
      fields.deleteField( "leaseAuthNum" );
      fields.deleteField( "leaseMonths" );
      fields.deleteField( "leaseMonthlyAmount" );
      fields.deleteField( "leaseFundingAmount" );
      fields.add( new HiddenField( "leaseCompany" ) );
      fields.add( new HiddenField( "leaseAcctNum" ) );
      fields.add( new HiddenField( "leaseAuthNum" ) );
      fields.add( new HiddenField( "leaseMonths" ) );
      fields.add( new HiddenField( "leaseMonthlyAmount" ) );
      fields.add( new HiddenField( "leaseFundingAmount" ) );
      
      fields.add(new NumberField("imprinterPlateQtyLong" ,  2,  3,  true, 0));
      fields.add(new Field      ("impPlateLongLine1",       23, 25, true   ));
      fields.getField("impPlateLongLine1").addValidation(new IfNotNullRequiredValidation(fields.getField("imprinterPlateQtyLong")));
      fields.add(new Field      ("impPlateLongLine2",       23, 25, true   ));
      fields.getField("impPlateLongLine2").addValidation(new IfNotNullRequiredValidation(fields.getField("imprinterPlateQtyLong")));
      fields.add(new Field      ("impPlateLongLine3",       23, 25, true   ));
      fields.getField("impPlateLongLine3").addValidation(new IfNotNullRequiredValidation(fields.getField("imprinterPlateQtyLong")));
      fields.add(new Field      ("imprinterPlateMsgLong",   23, 25, true   )); //LINE 4
      //fields.getField("imprinterPlateMsgLong").addValidation(new IfNotNullRequiredValidation(fields.getField("imprinterPlateQtyLong")));

      fields.add(new NumberField("imprinterPlateQtyShort",  2,  3,  true, 0));
      fields.add(new Field      ("impPlateShortLine1",      15, 25, true   ));
      fields.getField("impPlateShortLine1").addValidation(new IfNotNullRequiredValidation(fields.getField("imprinterPlateQtyShort")));
      fields.add(new Field      ("impPlateShortLine2",      15, 25, true   ));
      fields.getField("impPlateShortLine2").addValidation(new IfNotNullRequiredValidation(fields.getField("imprinterPlateQtyShort")));
      fields.add(new Field      ("impPlateShortLine3",      15, 25, true   ));
      fields.getField("impPlateShortLine3").addValidation(new IfNotNullRequiredValidation(fields.getField("imprinterPlateQtyShort")));
      fields.add(new Field      ("imprinterPlateMsgShort",  15, 25, true   )); //LINE 4
      //fields.getField("imprinterPlateMsgShort").addValidation(new IfNotNullRequiredValidation(fields.getField("imprinterPlateQtyShort")));
      
      // set to BB&T default
      fields.add( new Field("receiptLine4",25,30,true));
      fields.add( new CheckboxField("receiptLine4Enable","Receipt Header Line 4",false) );
      fields.getField("receiptLine4").addValidation( new IfYesNotBlankValidation( fields.getField("receiptLine4Enable"),"Please provide Line 4 for the Receipt Header" ));
      fields.getField("receiptLine4").addValidation( new IfNoBlankValidation( fields.getField("receiptLine4Enable"),"Please check the Receipt Header Line 4 check box or clear the entered Line 4" ));
      
      fields.setData("receiptLine4","");
      
      // password Protect options
      fields.add( new PasswordProtectOptions("passwordProtectOptions"));

      fields.add( new RadioButtonField( "fraudControlType", BBT_FraudControlButtons, -1, true, "Please select a valid fraud control type" ) );

      
      // Equipment & Supply services 
      fields.add( new RadioButtonField( "equipSplyServiceType", BBT_EquipSplySrvcsRadioButtons, -1, false, "Please select an Equipment & Supply Service option" ) );
      
      
      // Equipment Deployment services
      
      // replace the normal training types
      // with the BB&T specific training types
      fields.add( new RadioButtonField( "trainingType", BBT_TrainingTypeRadioButtons, -1, true, "Please select a valid training type" ) );

      // if 'Training' OR 'Reprogram Only OR Both' selected, both training name and phone number are required
      oc = new OrCondition();
      oc.add(new FieldValueCondition(fields.getField("trainingType"),"T"));
      oc.add(new FieldValueCondition(fields.getField("trainingType"),"R"));
      oc.add(new FieldValueCondition(fields.getField("trainingType"),"B"));

      fields.add( new PhoneField("trainingPhoneNumber", "Training Phone Number", false) );
      fields.getField("trainingPhoneNumber").setOptionalCondition(oc);

      FieldGroup test = new NameField("trainingContactName", "Training Contact Name", 20, false, true);
      //add conditions so they get sent to fields within field groups
      test.addCondition(oc);
      fields.add(test);

      // if Reprogram Only or Both is selected the # of pieces of equipment is required
      oc = new OrCondition();
      oc.add(new FieldValueCondition(fields.getField("trainingType"),"R"));
      oc.add(new FieldValueCondition(fields.getField("trainingType"),"B"));

      fields.add( new Field("edsNumPcsEquip", "# of pieces of equipment", 3, 3, false ));
      fields.getField("edsNumPcsEquip").setOptionalCondition(oc);
            
      fields.add(new TextareaField    ("equipmentComments",     250,3,100,true));
      
      // Equipment Shipping
      fields.add( new Field        ("shippingComments","Shipping Comments",60,60,true ) );
      fields.add( new DropDownField("shippingAddressType", "", new BBTShippingAddressTable(),false, Integer.toString(mesConstants.ADDR_TYPE_BUSINESS) ) );
      fields.add( new DropDownField("shippingMethod", new BBTShippingMethodTable(),false) );
      fields.add( new ButtonField("actionLoadAddress", "Get Address") );
      
      //Equipment Payment Method
      fields.add( new DropDownField("equipPaymentMethod", new BBTEquipPaymentMethodTable(),true ) );
      fields.add( new Field        ("equipPayMethCheckNum","Check Number",10,10,true ) );
      fields.getField("equipPayMethCheckNum").addValidation( new EquipPayMethValidation(fields.getField("equipPaymentMethod"),"0","'Merchant Check'"));

      fields.add( new Field ("equipPayMethBankName",    "Bank Name",     30,25,true ) );
      fields.add( new Field ("equipPayMethBankAcctNum", "Account Number",17,25,true ) );
      fields.add( new Field ("equipPayMethBankAbaNum",  "T/R Number",    9, 25,true ) );

      fields.getField("equipPayMethBankName").addValidation(    new EquipPayMethValidation(fields.getField("equipPaymentMethod"),"2","'ACH Merchant'"));
      fields.getField("equipPayMethBankAcctNum").addValidation( new EquipPayMethValidation(fields.getField("equipPaymentMethod"),"2","'ACH Merchant'"));
      fields.getField("equipPayMethBankAbaNum").addValidation(  new EquipPayMethValidation(fields.getField("equipPaymentMethod"),"2","'ACH Merchant'"));

      // clear out all field group labels
      fields.clearLabel(true);
      
      // reset all fields, including those just added to be of class formText
      fields.setHtmlExtra("class=\"formText\"");

    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }

//we have to overload loadEquipmentData so that owned equipment gets put into   
//fieldNameBufferNeededModels instead of fieldNameOwnedModels because bbt is screwed up

  /*
  ** protected void loadEquipmentData(long appSeqNum)
  **
  ** Loads equipment data from the database.
  */
  protected void loadEquipmentData(long appSeqNum)
  {
    int               equipType             = -1;
    Field             field                 = null;
    int               fieldNameBaseLength   = 0;
    StringBuffer      fieldNameBuffer       = new StringBuffer();
    String            fieldPostfix          = null;
    String            secondaryFieldPostfix = null;
    ResultSetIterator it                    = null;
    ResultSet         resultSet             = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:948^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equiptype_code                as equip_type,
//                  equiplendtype_code            as lend_type,
//                  merchequip_equip_quantity     as equip_count,
//                  decode(prod_option_id,
//                         null,equip_model,
//                         equip_model || '*' || prod_option_id)  as product_id
//          from    merchequipment      me
//          where   me.app_seq_num = :appSeqNum
//          order by me.equiptype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equiptype_code                as equip_type,\n                equiplendtype_code            as lend_type,\n                merchequip_equip_quantity     as equip_count,\n                decode(prod_option_id,\n                       null,equip_model,\n                       equip_model || '*' || prod_option_id)  as product_id\n        from    merchequipment      me\n        where   me.app_seq_num =  :1 \n        order by me.equiptype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.bbt.Equipment",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.bbt.Equipment",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:959^7*/      
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        equipType = resultSet.getInt("equip_type");
        
        // reset the field name data
        fieldNameBuffer.setLength(0);
        fieldPostfix          = null;
        secondaryFieldPostfix = null;

        switch( equipType )
        {
          case mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES:
            fieldNameBuffer.append("imprinterPlates");
            break;
            
//          case mesConstants.APP_EQUIP_TYPE_PERIPHERAL:
//          case mesConstants.APP_EQUIP_TYPE_TERMINAL:
//          case mesConstants.APP_EQUIP_TYPE_PRINTER:
//          case mesConstants.APP_EQUIP_TYPE_PINPAD:
//          case mesConstants.APP_EQUIP_TYPE_IMPRINTER:
//          case mesConstants.APP_EQUIP_TYPE_TERM_PRINTER:
//          case mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD:
//          case mesConstants.APP_EQUIP_TYPE_OTHERS:
//          case mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE:
          default:
            // add the base name and store the base length of the field
            fieldNameBuffer.append( EquipmentFieldNames[ equipType ] );
            fieldNameBaseLength = fieldNameBuffer.length(); 
            fieldPostfix = "NeededModels";    // assume needed

            switch( resultSet.getInt("lend_type" ) )
            {
              case mesConstants.APP_EQUIP_PURCHASE:
                fieldNameBuffer.append( "PurchaseCount" );
                break;

              case mesConstants.APP_EQUIP_BUY_REFURBISHED:
                fieldNameBuffer.append( "RefurbCount" );
                break;

              case mesConstants.APP_EQUIP_RENT:
                fieldNameBuffer.append( "RentalCount" );
                break;
            
              case mesConstants.APP_EQUIP_LEASE:
                fieldNameBuffer.append( "LeaseCount" );
                break;

              case mesConstants.APP_EQUIP_SWAP:
                fieldNameBuffer.append( "SwapCount" );
                break;

              case mesConstants.APP_EQUIP_OWNED:
                fieldNameBuffer.append( "OwnedCount" );
                fieldPostfix          = "OwnedModels";     // switch to owned
                //we need this secondary field post fix because we display the owned equipment in the NeedModels section, but we also store
                //it in the OwnedModels section but its a hidden field... when we submit the request we take whats in NeedModels and put it in
                //OwnedModels then when we load it, it works outs... 
                secondaryFieldPostfix = "NeededModels";
                break;
            }
            break;
        }
        // extract the field data from the result set
        field = fields.getField( fieldNameBuffer.toString() );
        if ( field != null )
        {
          field.setData( resultSet.getString("equip_count") );
        }          
        
        // setup the drop down box to point to the correct entry.
        if ( fieldPostfix != null )
        {
          // reset to just the field base name
          fieldNameBuffer.setLength( fieldNameBaseLength );
          
          // add the post fix to the base name
          fieldNameBuffer.append( fieldPostfix );
          
          // extract the field reference and set the contents if non null
          field = fields.getField( fieldNameBuffer.toString() );
          if ( field != null )
          {
            field.setData( resultSet.getString("product_id") );
          }
        }            

        // setup the drop down box to point to the correct entry.
        if ( secondaryFieldPostfix != null )
        {
          // reset to just the field base name
          fieldNameBuffer.setLength( fieldNameBaseLength );
          
          // add the post fix to the base name
          fieldNameBuffer.append( secondaryFieldPostfix );
          
          // extract the field reference and set the contents if non null
          field = fields.getField( fieldNameBuffer.toString() );
          if ( field != null )
          {
            field.setData( resultSet.getString("product_id") );
          }
        }            



      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadEquipmentData()",e.toString());
      addError("loadEquipmentData: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }

  
  protected void loadAppSpecData()
  {
    ResultSetIterator           it        = null;
    
    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      /*@lineinfo:generated-code*//*@lineinfo:1091^7*/

//  ************************************************************
//  #sql [Ctx] it = { select     amb.EQUIPSPLY_SERVICETYPE
//                    ,amb.EDS_NUMPCSEQUIP
//                    ,amb.IMPRINTERPLATE_MSGLONG
//                    ,amb.IMPRINTERPLATE_MSGSHORT
//                    ,amb.IMPRINTERPLATE_QTYLONG
//                    ,amb.IMPRINTERPLATE_QTYSHORT
//                    ,decode(amb.TRAINING_CONTACTNAME,null,decode(mc.MERCHCONT_PRIM_FIRST_NAME,null,decode(bo.busowner_first_name, null, '', bo.busowner_first_name || ' ' || bo.busowner_last_name),mc.MERCHCONT_PRIM_FIRST_NAME || ' ' || mc.MERCHCONT_PRIM_LAST_NAME),amb.TRAINING_CONTACTNAME) TRAINING_CONTACTNAME
//                    ,decode(amb.TRAINING_PHONENUMBER,null,addr.address_phone,amb.TRAINING_PHONENUMBER) TRAINING_PHONENUMBER
//                    ,amb.EQUIP_PAYMENT_METHOD
//                    ,amb.PAY_METH_CHECK_NUM                                                                                                             EQUIP_PAY_METH_CHECK_NUM
//                    ,decode(amb.IMP_PLATE_LONG_LINE1,null,merch.MERCH_NUMBER,amb.IMP_PLATE_LONG_LINE1)                                                  IMP_PLATE_LONG_LINE1
//                    ,decode(amb.IMP_PLATE_LONG_LINE2,null,substr(merch.merch_business_name,1,23),amb.IMP_PLATE_LONG_LINE2)                              IMP_PLATE_LONG_LINE2
//                    ,decode(amb.IMP_PLATE_LONG_LINE3,null,substr((addr.address_city || ' ' || addr.countrystate_code),1,23),amb.IMP_PLATE_LONG_LINE3)   IMP_PLATE_LONG_LINE3
//                    ,decode(amb.IMP_PLATE_SHORT_LINE1,null,merch.MERCH_NUMBER,amb.IMP_PLATE_SHORT_LINE1)                                                IMP_PLATE_SHORT_LINE1
//                    ,decode(amb.IMP_PLATE_SHORT_LINE2,null,substr(merch.merch_business_name,1,15),amb.IMP_PLATE_SHORT_LINE2)                            IMP_PLATE_SHORT_LINE2
//                    ,decode(amb.IMP_PLATE_SHORT_LINE3,null,substr((addr.address_city || ' ' || addr.countrystate_code),1,15),amb.IMP_PLATE_SHORT_LINE3) IMP_PLATE_SHORT_LINE3
//  
//          from      app_merch_bbt amb,
//                    merchant      merch,
//                    address       addr,
//                    merchcontact  mc,
//                    businessowner bo
//  
//          where     amb.app_seq_num = :appSeqNum        and
//                    amb.app_seq_num = merch.app_seq_num and
//                    amb.app_seq_num = mc.app_seq_num    and
//                    amb.app_seq_num = addr.app_seq_num  and
//                    addr.addresstype_code = 1           and
//                    amb.app_seq_num = bo.app_seq_num    and
//                    bo.busowner_num = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select     amb.EQUIPSPLY_SERVICETYPE\n                  ,amb.EDS_NUMPCSEQUIP\n                  ,amb.IMPRINTERPLATE_MSGLONG\n                  ,amb.IMPRINTERPLATE_MSGSHORT\n                  ,amb.IMPRINTERPLATE_QTYLONG\n                  ,amb.IMPRINTERPLATE_QTYSHORT\n                  ,decode(amb.TRAINING_CONTACTNAME,null,decode(mc.MERCHCONT_PRIM_FIRST_NAME,null,decode(bo.busowner_first_name, null, '', bo.busowner_first_name || ' ' || bo.busowner_last_name),mc.MERCHCONT_PRIM_FIRST_NAME || ' ' || mc.MERCHCONT_PRIM_LAST_NAME),amb.TRAINING_CONTACTNAME) TRAINING_CONTACTNAME\n                  ,decode(amb.TRAINING_PHONENUMBER,null,addr.address_phone,amb.TRAINING_PHONENUMBER) TRAINING_PHONENUMBER\n                  ,amb.EQUIP_PAYMENT_METHOD\n                  ,amb.PAY_METH_CHECK_NUM                                                                                                             EQUIP_PAY_METH_CHECK_NUM\n                  ,decode(amb.IMP_PLATE_LONG_LINE1,null,merch.MERCH_NUMBER,amb.IMP_PLATE_LONG_LINE1)                                                  IMP_PLATE_LONG_LINE1\n                  ,decode(amb.IMP_PLATE_LONG_LINE2,null,substr(merch.merch_business_name,1,23),amb.IMP_PLATE_LONG_LINE2)                              IMP_PLATE_LONG_LINE2\n                  ,decode(amb.IMP_PLATE_LONG_LINE3,null,substr((addr.address_city || ' ' || addr.countrystate_code),1,23),amb.IMP_PLATE_LONG_LINE3)   IMP_PLATE_LONG_LINE3\n                  ,decode(amb.IMP_PLATE_SHORT_LINE1,null,merch.MERCH_NUMBER,amb.IMP_PLATE_SHORT_LINE1)                                                IMP_PLATE_SHORT_LINE1\n                  ,decode(amb.IMP_PLATE_SHORT_LINE2,null,substr(merch.merch_business_name,1,15),amb.IMP_PLATE_SHORT_LINE2)                            IMP_PLATE_SHORT_LINE2\n                  ,decode(amb.IMP_PLATE_SHORT_LINE3,null,substr((addr.address_city || ' ' || addr.countrystate_code),1,15),amb.IMP_PLATE_SHORT_LINE3) IMP_PLATE_SHORT_LINE3\n\n        from      app_merch_bbt amb,\n                  merchant      merch,\n                  address       addr,\n                  merchcontact  mc,\n                  businessowner bo\n\n        where     amb.app_seq_num =  :1         and\n                  amb.app_seq_num = merch.app_seq_num and\n                  amb.app_seq_num = mc.app_seq_num    and\n                  amb.app_seq_num = addr.app_seq_num  and\n                  addr.addresstype_code = 1           and\n                  amb.app_seq_num = bo.app_seq_num    and\n                  bo.busowner_num = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.bbt.Equipment",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.bbt.Equipment",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1123^7*/
      setFields(it.getResultSet());

      /*@lineinfo:generated-code*//*@lineinfo:1126^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  amb.PAY_METH_BANK_NAME                                                                                                                                                          EQUIP_PAY_METH_BANK_NAME
//                  ,decode(amb.PAY_METH_BANK_ACCT_NUM,null,decode(mb2.MERCHBANK_ACCT_NUM,null,mb1.MERCHBANK_ACCT_NUM,mb2.MERCHBANK_ACCT_NUM),amb.PAY_METH_BANK_ACCT_NUM)                           EQUIP_PAY_METH_BANK_ACCT_NUM
//                  ,decode(amb.PAY_METH_BANK_ABA_NUM,null,decode(mb2.MERCHBANK_TRANSIT_ROUTE_NUM,null,mb1.MERCHBANK_TRANSIT_ROUTE_NUM,mb2.MERCHBANK_TRANSIT_ROUTE_NUM),amb.PAY_METH_BANK_ABA_NUM)  EQUIP_PAY_METH_BANK_ABA_NUM
//          from    app_merch_bbt amb,
//                  merchbank     mb1,
//                  merchbank     mb2
//          where   amb.APP_SEQ_NUM = :appSeqNum         and
//                  amb.APP_SEQ_NUM = mb1.APP_SEQ_NUM(+) and 
//                  1 = mb1.MERCHBANK_ACCT_SRNUM(+)      and 
//                  amb.APP_SEQ_NUM = mb2.APP_SEQ_NUM(+) and 
//                  2 = mb2.MERCHBANK_ACCT_SRNUM(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  amb.PAY_METH_BANK_NAME                                                                                                                                                          EQUIP_PAY_METH_BANK_NAME\n                ,decode(amb.PAY_METH_BANK_ACCT_NUM,null,decode(mb2.MERCHBANK_ACCT_NUM,null,mb1.MERCHBANK_ACCT_NUM,mb2.MERCHBANK_ACCT_NUM),amb.PAY_METH_BANK_ACCT_NUM)                           EQUIP_PAY_METH_BANK_ACCT_NUM\n                ,decode(amb.PAY_METH_BANK_ABA_NUM,null,decode(mb2.MERCHBANK_TRANSIT_ROUTE_NUM,null,mb1.MERCHBANK_TRANSIT_ROUTE_NUM,mb2.MERCHBANK_TRANSIT_ROUTE_NUM),amb.PAY_METH_BANK_ABA_NUM)  EQUIP_PAY_METH_BANK_ABA_NUM\n        from    app_merch_bbt amb,\n                merchbank     mb1,\n                merchbank     mb2\n        where   amb.APP_SEQ_NUM =  :1          and\n                amb.APP_SEQ_NUM = mb1.APP_SEQ_NUM(+) and \n                1 = mb1.MERCHBANK_ACCT_SRNUM(+)      and \n                amb.APP_SEQ_NUM = mb2.APP_SEQ_NUM(+) and \n                2 = mb2.MERCHBANK_ACCT_SRNUM(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.bbt.Equipment",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.app.bbt.Equipment",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1139^7*/
      setFields(it.getResultSet());

      // password protect options
      PasswordProtectOptions ppo = (PasswordProtectOptions)fields.getField("passwordProtectOptions");
      ppo.load();
    }
    catch(Exception e)
    {
      logEntry("loadAppSpecData()",e.toString());
      addError("loadAppSpecData: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }
  
  protected void storeAppSpecData()
  {
    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();

      int cnt = 0;

      /*@lineinfo:generated-code*//*@lineinfo:1165^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(*)
//          
//          from    app_merch_bbt
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)\n         \n        from    app_merch_bbt\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.bbt.Equipment",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1171^7*/

      // new
      if(cnt == 0) {
        /*@lineinfo:generated-code*//*@lineinfo:1175^9*/

//  ************************************************************
//  #sql [Ctx] { insert into   app_merch_bbt
//                          
//            (
//              app_seq_num
//              ,EQUIPSPLY_SERVICETYPE
//              ,EDS_NUMPCSEQUIP
//              ,IMPRINTERPLATE_MSGLONG
//              ,IMPRINTERPLATE_MSGSHORT
//              ,IMPRINTERPLATE_QTYLONG
//              ,IMPRINTERPLATE_QTYSHORT
//              ,TRAINING_CONTACTNAME
//              ,TRAINING_PHONENUMBER
//              ,IMP_PLATE_LONG_LINE1
//              ,IMP_PLATE_LONG_LINE2
//              ,IMP_PLATE_LONG_LINE3
//              ,IMP_PLATE_SHORT_LINE1
//              ,IMP_PLATE_SHORT_LINE2
//              ,IMP_PLATE_SHORT_LINE3
//              ,PAY_METH_CHECK_NUM
//              ,PAY_METH_BANK_NAME 
//              ,PAY_METH_BANK_ACCT_NUM
//              ,PAY_METH_BANK_ABA_NUM
//            )
//            values
//            (
//              :appSeqNum
//              ,:fields.getField("equipSplyServiceType").getData()
//              ,:fields.getField("edsNumPcsEquip").getData()
//              ,:fields.getField("imprinterPlateMsgLong").getData()
//              ,:fields.getField("imprinterPlateMsgShort").getData()
//              ,:fields.getField("imprinterPlateQtyLong").getData()
//              ,:fields.getField("imprinterPlateQtyShort").getData()
//              ,:fields.getField("trainingContactName").getData()
//              ,:fields.getField("trainingPhoneNumber").getData()
//              ,:fields.getField("equipPaymentMethod").getData()
//              ,:fields.getField("impPlateLongLine1").getData()
//              ,:fields.getField("impPlateLongLine2").getData()
//              ,:fields.getField("impPlateLongLine3").getData()
//              ,:fields.getField("impPlateShortLine1").getData()
//              ,:fields.getField("impPlateShortLine2").getData()
//              ,:fields.getField("impPlateShortLine3").getData()
//              ,:fields.getField("equipPayMethCheckNum").getData()
//              ,:fields.getField("equipPayMethBankName").getData()
//              ,:fields.getField("equipPayMethBankAcctNum").getData()
//              ,:fields.getField("equipPayMethBankAbaNum").getData()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2541 = fields.getField("equipSplyServiceType").getData();
 String __sJT_2542 = fields.getField("edsNumPcsEquip").getData();
 String __sJT_2543 = fields.getField("imprinterPlateMsgLong").getData();
 String __sJT_2544 = fields.getField("imprinterPlateMsgShort").getData();
 String __sJT_2545 = fields.getField("imprinterPlateQtyLong").getData();
 String __sJT_2546 = fields.getField("imprinterPlateQtyShort").getData();
 String __sJT_2547 = fields.getField("trainingContactName").getData();
 String __sJT_2548 = fields.getField("trainingPhoneNumber").getData();
 String __sJT_2549 = fields.getField("equipPaymentMethod").getData();
 String __sJT_2550 = fields.getField("impPlateLongLine1").getData();
 String __sJT_2551 = fields.getField("impPlateLongLine2").getData();
 String __sJT_2552 = fields.getField("impPlateLongLine3").getData();
 String __sJT_2553 = fields.getField("impPlateShortLine1").getData();
 String __sJT_2554 = fields.getField("impPlateShortLine2").getData();
 String __sJT_2555 = fields.getField("impPlateShortLine3").getData();
 String __sJT_2556 = fields.getField("equipPayMethCheckNum").getData();
 String __sJT_2557 = fields.getField("equipPayMethBankName").getData();
 String __sJT_2558 = fields.getField("equipPayMethBankAcctNum").getData();
 String __sJT_2559 = fields.getField("equipPayMethBankAbaNum").getData();
   String theSqlTS = "insert into   app_merch_bbt\n                        \n          (\n            app_seq_num\n            ,EQUIPSPLY_SERVICETYPE\n            ,EDS_NUMPCSEQUIP\n            ,IMPRINTERPLATE_MSGLONG\n            ,IMPRINTERPLATE_MSGSHORT\n            ,IMPRINTERPLATE_QTYLONG\n            ,IMPRINTERPLATE_QTYSHORT\n            ,TRAINING_CONTACTNAME\n            ,TRAINING_PHONENUMBER\n            ,IMP_PLATE_LONG_LINE1\n            ,IMP_PLATE_LONG_LINE2\n            ,IMP_PLATE_LONG_LINE3\n            ,IMP_PLATE_SHORT_LINE1\n            ,IMP_PLATE_SHORT_LINE2\n            ,IMP_PLATE_SHORT_LINE3\n            ,PAY_METH_CHECK_NUM\n            ,PAY_METH_BANK_NAME \n            ,PAY_METH_BANK_ACCT_NUM\n            ,PAY_METH_BANK_ABA_NUM\n          )\n          values\n          (\n             :1 \n            , :2 \n            , :3 \n            , :4 \n            , :5 \n            , :6 \n            , :7 \n            , :8 \n            , :9 \n            , :10 \n            , :11 \n            , :12 \n            , :13 \n            , :14 \n            , :15 \n            , :16 \n            , :17 \n            , :18 \n            , :19 \n            , :20 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.app.bbt.Equipment",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_2541);
   __sJT_st.setString(3,__sJT_2542);
   __sJT_st.setString(4,__sJT_2543);
   __sJT_st.setString(5,__sJT_2544);
   __sJT_st.setString(6,__sJT_2545);
   __sJT_st.setString(7,__sJT_2546);
   __sJT_st.setString(8,__sJT_2547);
   __sJT_st.setString(9,__sJT_2548);
   __sJT_st.setString(10,__sJT_2549);
   __sJT_st.setString(11,__sJT_2550);
   __sJT_st.setString(12,__sJT_2551);
   __sJT_st.setString(13,__sJT_2552);
   __sJT_st.setString(14,__sJT_2553);
   __sJT_st.setString(15,__sJT_2554);
   __sJT_st.setString(16,__sJT_2555);
   __sJT_st.setString(17,__sJT_2556);
   __sJT_st.setString(18,__sJT_2557);
   __sJT_st.setString(19,__sJT_2558);
   __sJT_st.setString(20,__sJT_2559);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1223^9*/



      // existing
      } else {
        /*@lineinfo:generated-code*//*@lineinfo:1229^9*/

//  ************************************************************
//  #sql [Ctx] { update    app_merch_bbt
//            set       EQUIPSPLY_SERVICETYPE     = :fields.getField("equipSplyServiceType").getData()
//                      ,EDS_NUMPCSEQUIP          = :fields.getField("edsNumPcsEquip").getData()
//                      ,IMPRINTERPLATE_MSGLONG   = :fields.getField("imprinterPlateMsgLong").getData()
//                      ,IMPRINTERPLATE_MSGSHORT  = :fields.getField("imprinterPlateMsgShort").getData()
//                      ,IMPRINTERPLATE_QTYLONG   = :fields.getField("imprinterPlateQtyLong").getData()
//                      ,IMPRINTERPLATE_QTYSHORT  = :fields.getField("imprinterPlateQtyShort").getData()
//                      ,TRAINING_CONTACTNAME     = :fields.getField("trainingContactName").getData()
//                      ,TRAINING_PHONENUMBER     = :fields.getField("trainingPhoneNumber").getData()
//                      ,EQUIP_PAYMENT_METHOD     = :fields.getField("equipPaymentMethod").getData()
//                      ,IMP_PLATE_LONG_LINE1     = :fields.getField("impPlateLongLine1").getData()
//                      ,IMP_PLATE_LONG_LINE2     = :fields.getField("impPlateLongLine2").getData()
//                      ,IMP_PLATE_LONG_LINE3     = :fields.getField("impPlateLongLine3").getData()
//                      ,IMP_PLATE_SHORT_LINE1    = :fields.getField("impPlateShortLine1").getData()
//                      ,IMP_PLATE_SHORT_LINE2    = :fields.getField("impPlateShortLine2").getData()
//                      ,IMP_PLATE_SHORT_LINE3    = :fields.getField("impPlateShortLine3").getData()
//                      ,PAY_METH_CHECK_NUM       = :fields.getField("equipPayMethCheckNum").getData()
//                      ,PAY_METH_BANK_NAME       = :fields.getField("equipPayMethBankName").getData()
//                      ,PAY_METH_BANK_ACCT_NUM   = :fields.getField("equipPayMethBankAcctNum").getData()
//                      ,PAY_METH_BANK_ABA_NUM    = :fields.getField("equipPayMethBankAbaNum").getData()
//            where     app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2560 = fields.getField("equipSplyServiceType").getData();
 String __sJT_2561 = fields.getField("edsNumPcsEquip").getData();
 String __sJT_2562 = fields.getField("imprinterPlateMsgLong").getData();
 String __sJT_2563 = fields.getField("imprinterPlateMsgShort").getData();
 String __sJT_2564 = fields.getField("imprinterPlateQtyLong").getData();
 String __sJT_2565 = fields.getField("imprinterPlateQtyShort").getData();
 String __sJT_2566 = fields.getField("trainingContactName").getData();
 String __sJT_2567 = fields.getField("trainingPhoneNumber").getData();
 String __sJT_2568 = fields.getField("equipPaymentMethod").getData();
 String __sJT_2569 = fields.getField("impPlateLongLine1").getData();
 String __sJT_2570 = fields.getField("impPlateLongLine2").getData();
 String __sJT_2571 = fields.getField("impPlateLongLine3").getData();
 String __sJT_2572 = fields.getField("impPlateShortLine1").getData();
 String __sJT_2573 = fields.getField("impPlateShortLine2").getData();
 String __sJT_2574 = fields.getField("impPlateShortLine3").getData();
 String __sJT_2575 = fields.getField("equipPayMethCheckNum").getData();
 String __sJT_2576 = fields.getField("equipPayMethBankName").getData();
 String __sJT_2577 = fields.getField("equipPayMethBankAcctNum").getData();
 String __sJT_2578 = fields.getField("equipPayMethBankAbaNum").getData();
   String theSqlTS = "update    app_merch_bbt\n          set       EQUIPSPLY_SERVICETYPE     =  :1 \n                    ,EDS_NUMPCSEQUIP          =  :2 \n                    ,IMPRINTERPLATE_MSGLONG   =  :3 \n                    ,IMPRINTERPLATE_MSGSHORT  =  :4 \n                    ,IMPRINTERPLATE_QTYLONG   =  :5 \n                    ,IMPRINTERPLATE_QTYSHORT  =  :6 \n                    ,TRAINING_CONTACTNAME     =  :7 \n                    ,TRAINING_PHONENUMBER     =  :8 \n                    ,EQUIP_PAYMENT_METHOD     =  :9 \n                    ,IMP_PLATE_LONG_LINE1     =  :10 \n                    ,IMP_PLATE_LONG_LINE2     =  :11 \n                    ,IMP_PLATE_LONG_LINE3     =  :12 \n                    ,IMP_PLATE_SHORT_LINE1    =  :13 \n                    ,IMP_PLATE_SHORT_LINE2    =  :14 \n                    ,IMP_PLATE_SHORT_LINE3    =  :15 \n                    ,PAY_METH_CHECK_NUM       =  :16 \n                    ,PAY_METH_BANK_NAME       =  :17 \n                    ,PAY_METH_BANK_ACCT_NUM   =  :18 \n                    ,PAY_METH_BANK_ABA_NUM    =  :19 \n          where     app_seq_num =  :20";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.app.bbt.Equipment",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2560);
   __sJT_st.setString(2,__sJT_2561);
   __sJT_st.setString(3,__sJT_2562);
   __sJT_st.setString(4,__sJT_2563);
   __sJT_st.setString(5,__sJT_2564);
   __sJT_st.setString(6,__sJT_2565);
   __sJT_st.setString(7,__sJT_2566);
   __sJT_st.setString(8,__sJT_2567);
   __sJT_st.setString(9,__sJT_2568);
   __sJT_st.setString(10,__sJT_2569);
   __sJT_st.setString(11,__sJT_2570);
   __sJT_st.setString(12,__sJT_2571);
   __sJT_st.setString(13,__sJT_2572);
   __sJT_st.setString(14,__sJT_2573);
   __sJT_st.setString(15,__sJT_2574);
   __sJT_st.setString(16,__sJT_2575);
   __sJT_st.setString(17,__sJT_2576);
   __sJT_st.setString(18,__sJT_2577);
   __sJT_st.setString(19,__sJT_2578);
   __sJT_st.setLong(20,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1252^9*/
      }

      // password protect sub-features
      PasswordProtectOptions ppo = (PasswordProtectOptions)fields.getField("passwordProtectOptions");
      ppo.store();
    }
    catch(Exception e)
    {
      logEntry("storeAppSpecData()",e.toString());
      addError("storeAppSpecData: " + e.toString());
    }
    finally
    {
    }
  }

  private int paymentMethod = -1;
  
  /*
  ** public int getPaymentMethod()
  **
  ** Loads the merchant's payment method.  This is the same as their product
  ** type (which is a pos_type code in the pos_category table, referred to
  ** indirectly by the pos_code in the merch_pos table).
  **
  ** RETURNS: payment method.
  */
  public int getPaymentMethod()
  {
    if (paymentMethod < 0)
    {
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:1288^9*/

//  ************************************************************
//  #sql [Ctx] { select  pc.pos_type
//            
//            from    merch_pos     mp,
//                    pos_category  pc
//            where   mp.app_seq_num = :fields.getField("appSeqNum").asLong() and
//                    pc.pos_code = mp.pos_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_2579 = fields.getField("appSeqNum").asLong();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pc.pos_type\n           \n          from    merch_pos     mp,\n                  pos_category  pc\n          where   mp.app_seq_num =  :1  and\n                  pc.pos_code = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.bbt.Equipment",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_2579);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   paymentMethod = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1296^9*/
      }
      catch( Exception e )
      {
        System.out.println(this.getClass().getName() + "::getPaymentMethod(): "
          + e.toString());
        e.printStackTrace();
        logEntry("getPaymentMethod()",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    return paymentMethod;
  }




  private int businessNature = -1;
  
  /*
  ** public int getBusinessNature()
  **
  ** Loads the merchant's business nature.  
  **
  ** RETURNS: business nature.
  */
  public int getBusinessNature()
  {
    if (businessNature < 0)
    {
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:1333^9*/

//  ************************************************************
//  #sql [Ctx] { select  business_nature
//            
//            from    merchant
//            where   app_seq_num = :fields.getField("appSeqNum").asLong()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_2580 = fields.getField("appSeqNum").asLong();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  business_nature\n           \n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.bbt.Equipment",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_2580);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   businessNature = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1339^9*/
      }
      catch( Exception e )
      {
        System.out.println(this.getClass().getName() + "::getBusinessNature(): "
          + e.toString());
        e.printStackTrace();
        logEntry("getBusinessNature()",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    return businessNature;
  }


  protected boolean autoAct()
  {
    switch(fields.getField("shippingAddressType").asInt()) {
      
      case mesConstants.ADDR_TYPE_MAILING:
        autoActionName = "actionGetMailingAddr";
        return super.autoAct();
      case mesConstants.ADDR_TYPE_BUSINESS:
        autoActionName = "actionGetBusinessAddr";
        return super.autoAct();
      
      case mesConstants.ADDR_TYPE_MSR:
      {
        try {
      
          long appSeqNum = Equipment.this.fields.getField("appSeqNum").asLong();
          String nm=null,a1=null,a2=null,ct=null,st=null,zp=null;
      
          connect();
      
          /*@lineinfo:generated-code*//*@lineinfo:1377^11*/

//  ************************************************************
//  #sql [Ctx] { select  r.REP_NAME
//                     ,r.ADDRESS_LINE1
//                     ,r.ADDRESS_LINE2
//                     ,r.ADDRESS_CITY
//                     ,r.COUNTRYSTATE_CODE
//                     ,r.ADDRESS_ZIP
//              
//              from   app_merch_bbt_reps r
//                     ,app_merch_bbt          b
//              where  r.rep_id = b.MERCHREP_OFFICERNUM
//                     and b.app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  r.REP_NAME\n                   ,r.ADDRESS_LINE1\n                   ,r.ADDRESS_LINE2\n                   ,r.ADDRESS_CITY\n                   ,r.COUNTRYSTATE_CODE\n                   ,r.ADDRESS_ZIP\n             \n            from   app_merch_bbt_reps r\n                   ,app_merch_bbt          b\n            where  r.rep_id = b.MERCHREP_OFFICERNUM\n                   and b.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.bbt.Equipment",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 6) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(6,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   nm = (String)__sJT_rs.getString(1);
   a1 = (String)__sJT_rs.getString(2);
   a2 = (String)__sJT_rs.getString(3);
   ct = (String)__sJT_rs.getString(4);
   st = (String)__sJT_rs.getString(5);
   zp = (String)__sJT_rs.getString(6);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1390^11*/
      
          fields.getField("shippingBusinessName").setData(nm);
          fields.getField("shippingAddr1").setData(a1);
          fields.getField("shippingAddr2").setData(a2);
          fields.getField("shippingCszCity").setData(ct);
          fields.getField("shippingCszState").setData(st);
          fields.getField("shippingCszZip").setData(zp);

        }
        catch (Exception e) {
          logEntry("bbt.autoAct(load shipping addr)",e.toString());
          System.out.println("bbt.autoAct(load shipping addr): " + e.toString());
        }
        finally {
          cleanUp();
        }
        break;
      }
    
      default:
        return false;
    }

    return true;
  }
  
  protected void postHandleRequest(HttpServletRequest request)
  {
    String      fieldName     = null;

    super.postHandleRequest(request);
    
    for( int etype = 0; etype < EquipmentFieldNames.length; ++etype )
    {
      fieldName = EquipmentFieldNames[etype];
      
      // remove all the existing owned fields
      // and replace with hidden owned fields
      if ( fieldName != null )
      {
        // owned is just another column of needed models, so
        // mirror the models so the base class insert works
        // without modification.
        fields.setData( fieldName + "OwnedModels", fields.getData(fieldName + "NeededModels") );
      }
    }
  }

}/*@lineinfo:generated-code*/