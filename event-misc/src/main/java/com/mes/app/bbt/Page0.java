/*@lineinfo:filename=Page0*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/bbt/Page0.sqlj $

  Description:
  
  Used to generate ola control num.  I.e. "stubs" an ola app in order for 
  an app to be tracked from the onset of data entry.
  
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 9/27/04 11:38a $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.bbt;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.AppBase;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;
import com.mes.queues.QueueTools;
import sqlj.runtime.ResultSetIterator;


public final class Page0 extends AppBase
{
  {
    appType     = 11;
    curScreenId = 1;
  }
  

  public class MerchantNum15DigitsValidation implements Validation
  {
    private String  errorText     = "Merchant number must be 15 digits.";
  
    public String getErrorText()
    {
      return errorText;
    }
  
    public boolean validate(String fieldData)
    {
      boolean result = true;

      if(fieldData != null && !fieldData.equals(""))
      {
        result = (fieldData.length() == 15);
      }

      return result;
    }

  }

  public class UniqueMerchantNumValidation implements Validation
  {
    private String  errorText     = "This merchant number is already assigned to another account.";
    private int     appSeqNum     = -1;
    
    public UniqueMerchantNumValidation(Field appSeqNumField)
    {
      try
      {
        if(!appSeqNumField.getData().equals(""))
        {
          appSeqNum = Integer.parseInt(appSeqNumField.getData());
        }
      }
      catch(Exception e)
      {
        appSeqNum = -1;
      }
    }
  
    public String getErrorText()
    {
      return errorText;
    }
  
    public boolean validate(String fieldData)
    {
      ResultSetIterator it    = null;
      ResultSet         rs    = null;
      int               value = -1;
  
      if(fieldData == null || fieldData.equals(""))
      {
        return true;
      }

      try
      {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:116^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(merch_number)
//            
//            from    merchant
//            where   merch_number = :fieldData and
//                    app_seq_num != :appSeqNum        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merch_number)\n           \n          from    merchant\n          where   merch_number =  :1  and\n                  app_seq_num !=  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.bbt.Page0",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fieldData);
   __sJT_st.setInt(2,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   value = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:123^9*/
        
        if(value < 1)
        {
          // check mif table
          /*@lineinfo:generated-code*//*@lineinfo:128^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//              
//              from    mif
//              where   merchant_number = :fieldData
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n             \n            from    mif\n            where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.bbt.Page0",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fieldData);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   value = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:134^11*/
        }
      }
      catch(Exception e) 
      {
      }
      finally
      {
        cleanUp();
      }
      
      return value < 1;
    }

  }



  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // ??? Field f = null;
      
      fields.add  (new Field            ("merchBusinessName", "Business Name (DBA)",25,30,false));
      fields.add  (new CheckboxField    ("isRush",            "Is Rush?",false));
      fields.add  (new NumberField      ("merchantNumber",    "Merchant Number", 15, 30, false, 0));
      fields.getField("merchantNumber").addValidation(new UniqueMerchantNumValidation(fields.getField("appSeqNum")));
      fields.getField("merchantNumber").addValidation(new MerchantNum15DigitsValidation());

      // set extra on all fields not set yet
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch(Exception e)
    {
      logEntry("createFields()",e.toString());
    }

  }
  
  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
  }

  protected boolean loadAppData()
  {
    long                  appSeqNum   = 0L;
    ResultSetIterator     it          = null;
    boolean               retVal      = super.loadAppData();
  
    try
    {
      appSeqNum = fields.getField("appSeqNum").asInteger();
    
      if( retVal == true && appSeqNum != 0L )
      {
        connect();
      
        /*@lineinfo:generated-code*//*@lineinfo:195^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_business_name   merch_business_name,
//                    merch_number          merchant_number
//            from    merchant
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_business_name   merch_business_name,\n                  merch_number          merchant_number\n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.bbt.Page0",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.bbt.Page0",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:201^9*/
        
        setFields(it.getResultSet());
      }
    }
    catch(Exception e )
    {
      logEntry("loadAppData()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
      cleanUp();
    }
    return( retVal );
  }
  
  protected boolean submitAppData()
  {
    long                  appSeqNum   = 0L;
    int                   recCount    = 0;
    boolean               retVal      = super.submitAppData();

    try
    {
      connect();
      
      appSeqNum = fields.getField("appSeqNum").asInteger();

      if( retVal == true && appSeqNum != 0L )
      {

        // update merchant table bbt-specific columns
        /*@lineinfo:generated-code*//*@lineinfo:234^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_business_name = :fields.getField("merchBusinessName").getData(),
//                    merch_number        = :fields.getField("merchantNumber").getData()
//            where   app_seq_num         = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2581 = fields.getField("merchBusinessName").getData();
 String __sJT_2582 = fields.getField("merchantNumber").getData();
   String theSqlTS = "update  merchant\n          set     merch_business_name =  :1 ,\n                  merch_number        =  :2 \n          where   app_seq_num         =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.bbt.Page0",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2581);
   __sJT_st.setString(2,__sJT_2582);
   __sJT_st.setLong(3,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:240^9*/
      }
    }
    catch(Exception e )
    {
      System.out.println("bbt.submitAppData() EXCEPTION: "+e.toString());
      logEntry("bbt.submitAppData()",e.toString());
    }
    finally
    {
      cleanUp();  // not really needed, see above
    }
    return( retVal );
  }

  protected boolean autoSubmit()
  {
    boolean retVal = super.autoSubmit();
    
    try {
      
      long    appSeqNum = fields.getField("appSeqNum").asInteger();
      boolean isRush    = ((CheckboxField)fields.getField("isRush")).isChecked();

      if( retVal == true && appSeqNum != 0L )
      {
        // insert into the preparation queue
        QueueTools.insertQueue(appSeqNum, MesQueues.Q_BBT_QA_PREP, this.user, isRush);

        retVal = true;
      }

    }
    catch(Exception e) {
      System.out.println("bbt.autoSubmit() EXCEPTION: "+e.toString());
      logEntry("bbt.autoSubmit()",e.toString());
    }

    return retVal;
  }

}/*@lineinfo:generated-code*/