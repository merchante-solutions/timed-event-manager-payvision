/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/bbt/Business.sqlj $

  Description:
  
  Business
  
  Banner Bank online application merchant information page bean.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-09-19 11:13:32 -0700 (Wed, 19 Sep 2007) $
  Version            : $Revision: 14159 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.bbt;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.BusinessBase;
import com.mes.constants.mesConstants;
import com.mes.forms.BBTBusinessTypeTable;
import com.mes.forms.CheckboxField;
import com.mes.forms.Condition;
import com.mes.forms.DropDownField;
import com.mes.forms.DynamicLabelField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.MerchantTypeTaxIdField;
import com.mes.forms.NameField;
import com.mes.forms.NumberField;
import com.mes.forms.PasswordField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.SicField;
import com.mes.forms.Validation;
import com.mes.forms.VitalField;
import com.mes.support.StringUtilities;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public final class Business extends BusinessBase
{
  {
    appType     = 11;
    curScreenId = 2;
  }
  
  private static final String[][] ProcessorList = 
  {
    { "Vital",
      Integer.toString(mesConstants.APP_PROCESSOR_TYPE_VITAL)         },
    { "Global Central",
      Integer.toString(mesConstants.APP_PROCESSOR_TYPE_GLOBAL_CENTRAL)},
    { "Global East",
      Integer.toString(mesConstants.APP_PROCESSOR_TYPE_GLOBAL_EAST)   },
    { "Paymentech",
      Integer.toString(mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH)    },
  };
  
  private static final String[][] CheckProvidersList =
  {
    {"Global",          mesConstants.APP_BBT_CHECK_PROVIDER_GLOBAL  },
    {"T-Tech",          mesConstants.APP_BBT_CHECK_PROVIDER_TTECH   },
    {"RCK CPS",         mesConstants.APP_BBT_CHECK_PROVIDER_CPS     },
    {"Check Not Used",  ""                                          }
  };
  
  private static final String[][] CheckOptionsList =
  {
    {"Verification",              "1" },
    {"Traditional Guarantee",     "2" },
    {"Conversion w/Verification", "3" },
    {"Conversion w/Guarantee",    "4" },
    {"ARC",                       "5" }
  };
  
  protected class NatureOfBusinessTable extends DropDownTable
  {
    // BBT Nature of Business constants (corres. to mesdb.BBT_APP_BUSINESS_NATURE)
    public static final int BUSINESS_NATURE_UNDEFINED       = mesConstants.BUSINESS_NATURE_UNDEFINED;
    public static final int BUSINESS_NATURE_RETAIL          = mesConstants.BUSINESS_NATURE_RETAIL;
    public static final int BUSINESS_NATURE_RESTAURANT      = mesConstants.BUSINESS_NATURE_RESTAURANT;
    public static final int BUSINESS_NATURE_LODGING         = mesConstants.BUSINESS_NATURE_LODGING;
    public static final int BUSINESS_NATURE_GOV_PURCHCARD   = mesConstants.BUSINESS_NATURE_GOV_PURCHCARD;
    public static final int BUSINESS_NATURE_KEYTAIL         = mesConstants.BUSINESS_NATURE_KEYTAIL;
    public static final int BUSINESS_NATURE_MOTO            = mesConstants.BUSINESS_NATURE_MOTO;
    public static final int BUSINESS_NATURE_INTERNET        = mesConstants.BUSINESS_NATURE_INTERNET;
    
    public NatureOfBusinessTable() throws java.sql.SQLException 
    {
      ResultSetIterator it  = null;
      ResultSet         rs  = null;
      
      try
      {
        addElement("","select one");
        
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:104^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    value
//                      ,description
//            from      bbt_app_business_nature
//            order by  display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    value\n                    ,description\n          from      bbt_app_business_nature\n          order by  display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.bbt.Business",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.bbt.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:110^9*/
        rs = it.getResultSet();
        while(rs.next() )
        {
          addElement(rs);
        }
      }
      finally
      {
        try{ it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  
  protected class BBTMerchAgreementTermsTable extends DropDownTable
  {
    public BBTMerchAgreementTermsTable()
    {
      // value/name pairs
      addElement("NONE","NONE");
      addElement("1 YEAR","1 YEAR");
      addElement("2 YEAR","2 YEAR");
      addElement("3 YEAR","3 YEAR");
    }
  }

  
  protected class RepsTable extends DropDownTable
  {
    public RepsTable() throws java.sql.SQLException 
    {
      ResultSetIterator it  = null;
      ResultSet         rs  = null;
      
      try
      {
        addElement("","select one");
        
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:150^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  rep_id
//                    ,lpad(rep_id,5,'0') || ' - ' || rep_name
//            from    app_merch_bbt_reps
//            order by rep_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rep_id\n                  ,lpad(rep_id,5,'0') || ' - ' || rep_name\n          from    app_merch_bbt_reps\n          order by rep_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.bbt.Business",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.bbt.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:156^9*/
        rs = it.getResultSet();
        while(rs.next() )
        {
          addElement(rs);
        }
      }
      finally
      {
        try{ it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  
  public class CentersField extends Field
  {
    public CentersField(boolean nullAllowed)
    { 
      super("centerNumber","Center #",7,7,nullAllowed);
      
      // don't do check anymore
      //addValidation(new CenterValidation());

      addValidation(new CenterValidation2());
    }

    public class CenterValidation2 implements Validation
    {
      private String  errorText     = "Must be 7 digits";
    
      public String getErrorText()
      {
        return errorText;
      }
    
      public boolean validate(String fieldData)
      {
        boolean rval = false;

        if(fieldData != null && fieldData.length() == 7)
        {
          rval = true;
        }

        return rval;
      }

    }


    public class CenterValidation implements Validation
    {
      private String  errorText     = "";
    
      public String getErrorText()
      {
        return errorText;
      }
    
      public boolean validate(String fieldData)
      {
        boolean           rval  = false;
        String            value = null;
        ResultSetIterator it    = null;
        ResultSet         rs    = null;
    
        try
        {
          connect();
          /*@lineinfo:generated-code*//*@lineinfo:226^11*/

//  ************************************************************
//  #sql [Ctx] { select  name
//              
//              from    app_merch_bbt_centers
//              where   value = :fieldData
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  name\n             \n            from    app_merch_bbt_centers\n            where   value =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.bbt.Business",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fieldData);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   value = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:232^11*/
          
        }
        catch(Exception e) {
        }
        finally
        {
          cleanUp();
        }
        
        if(value!=null && value.length()>0) {
          rval=true;
        }
        else {
          errorText = "Invalid BB&T Center Number.";
        }

        return rval;
      }

    }
  
  }
  
  protected class AccountTypeTable extends DropDownTable
  {
    public AccountTypeTable()
    {
      addElement("","select one");
      addElement("1","Savings");
      addElement("2","Checking");
    }
  }
  

  protected class BBTInternetProductTable extends DropDownTable
  {
    public BBTInternetProductTable() throws java.sql.SQLException 
    {

      addElement("",  "select one");
      addElement(Integer.toString(mesConstants.APP_MPOS_VERISIGN_PAYFLOW_PRO), "Verisign Payflow Pro");
      addElement(Integer.toString(mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK), "Verisign Payflow Link");

/*      
      ResultSetIterator it  = null;
      ResultSet         rs  = null;
      
      try
      {
        addElement("","select one");
        
        connect();
        #sql [Ctx] it = 
        {
          select  pos_code,
                  pos_desc
          from    pos_category
          where   pos_type = 2 and    -- internet
                  lower(pos_desc) like '%verisign%'
          order by pos_code
        };
        rs = it.getResultSet();
        while(rs.next() )
        {
          addElement(rs);
        }
      }
      finally
      {
        try{ it.close(); } catch (Exception e) {}
        cleanUp();
      }
*/

    }
  }
  
  protected class BBTAssocNumberField extends Field
  {
    public BBTAssocNumberField()
    {
      super("assocNum", "Association #", 6, 6, false);
      //addValidation(new BBTAssocNumberValidation());
        // don't do check anymore
    }

    public class BBTAssocNumberValidation implements Validation
    {
      private String  errorText     = "";
    
      public String getErrorText()
      {
        return errorText;
      }
    
      public boolean validate(String fieldData)
      {
        boolean           rval  = false;
        ResultSetIterator it    = null;
        ResultSet         rs    = null;
        String            value = null;
    
        try
        {
          connect();
          /*@lineinfo:generated-code*//*@lineinfo:338^11*/

//  ************************************************************
//  #sql [Ctx] { select  name
//              
//              from    app_merch_bbt_associations
//              where   value = :fieldData
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  name\n             \n            from    app_merch_bbt_associations\n            where   value =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.bbt.Business",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fieldData);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   value = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:344^11*/
        }
        catch(Exception e) {
        }
        finally
        {
          cleanUp();
        }
        
        if(value!=null && value.length()>0) {
          rval=true;
        }
        else {
          errorText = "Invalid BB&T Association Number.";
        }
        
        return rval;
      }

    }
    
    public String getRenderData()
    {
      // left pad w/ 0s to 6 chars if value present
      return (fdata==null || fdata.length()<1)? "" : StringUtilities.rightJustify(fdata, 6, '0');
    }
  }

  protected class BBTOfficerField extends Field
  { 
    protected DynamicLabelField   officerName       = null;
    
    public BBTOfficerField(String name, String label, boolean nullAllowed)
    {
      super(name, label, 5, 5, nullAllowed);
      addValidation(new BBTRepCodeValidation(false));

      officerName = new DynamicLabelField((name+"Name"),"class=\"smallGrey\"");
    }
    
    public BBTOfficerField(String name, String label, boolean nullAllowed, boolean existenceCheck)
    {
      super(name, label, 5, 5, nullAllowed);
      addValidation(new BBTRepCodeValidation(existenceCheck));

      officerName = new DynamicLabelField((name+"Name"),"class=\"smallGrey\"");
    }
    
    public BBTOfficerField(String name, String label, boolean nullAllowed, boolean existenceCheck, int minLengthAllowed)
    {
      super(name, label, 5, 5, nullAllowed);
      addValidation(new BBTRepCodeValidation(existenceCheck));
      addValidation(new BBTRepCodeMinLengthAllowedValidation(minLengthAllowed));

      officerName = new DynamicLabelField((name+"Name"),"class=\"smallGrey\"");
    }


    public class BBTRepCodeMinLengthAllowedValidation implements Validation
    {
      private int minLengthAllowed = -1;

      public BBTRepCodeMinLengthAllowedValidation(int minLengthAllowed)
      {
        this.minLengthAllowed = minLengthAllowed;
      }

      public String getErrorText()
      {
        return "The data in this field must be atleast " + minLengthAllowed + " characters long";
      }

      public boolean validate(String fdata)
      {
        boolean result = true;

        if(fdata != null && !fdata.equals("") && fdata.length() < minLengthAllowed)
        {
          result = false;
        }

        return result;
      }
    }

    public class BBTRepCodeValidation extends NumericDataOnlyValidation
    {
      private String    errorText       = null;
      private boolean   existenceCheck  = false;
    
      public BBTRepCodeValidation(boolean existenceCheck)
      {
        this.existenceCheck = existenceCheck;
      }

      public String getErrorText()
      {
        return errorText;
      }
    
      //recursive function to strip out preceeding zeros
      private String stripPreceedingZero(String fdata)
      {
        if(fdata.startsWith("0"))
        {
          fdata = fdata.substring(1);
          return stripPreceedingZero(fdata);          
        }
        
        return fdata;
      }

      public boolean validate(String fdata)
      {
        boolean result = super.validate(fdata);
      
        if(result && existenceCheck) 
        {
          //this way if they enter 0247 when we do the existence check.. it will find the record 247 cause its stored as a number in the db
          fdata = stripPreceedingZero(fdata);

          // reset fdata to be the rightmost 4 digits if longer than 4 digits   WHY??????????????, IM TAKING IT OUT
          //if(fdata.length() > 4)
          //{
            //fdata = fdata.substring(fdata.length()-4);
          //}

          int     repCodeCount    = 0;

          try
          {
          
            connect();
        
            /*@lineinfo:generated-code*//*@lineinfo:478^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(*)
//                
//                from    app_merch_bbt_reps
//                where   rep_id = :fdata
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)\n               \n              from    app_merch_bbt_reps\n              where   rep_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.bbt.Business",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fdata);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   repCodeCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:484^13*/
        
          }
          catch(Exception e)
          {
            logEntry("BBTRepCodeValidation.validate",e.toString());
          }
          finally
          {
            cleanUp();
          }
          
          if(repCodeCount == 0)
          {
            errorText = "Invalid Rep Officer #";
            result = false;
          }

        }
      
        return result;
      }
    }

    public String renderHtml()
    {
      fillName();
      
      StringBuffer sb = new StringBuffer();
      sb.append(renderHtmlField() + renderErrorIndicator());
      sb.append("&nbsp;&nbsp;");
      sb.append(officerName.renderHtml());
      return sb.toString();
    }

    //recursive function to strip out preceeding zeros
    private String stripPreceedingZero(String fdata)
    {
      if(fdata.startsWith("0"))
      {
        fdata = fdata.substring(1);
        return stripPreceedingZero(fdata);          
      }
      
      return fdata;
    }


    private void fillName()
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      
      if(getData() == null || getData().length() < 1)
      {
        return;
      }

      String repId = stripPreceedingZero(getData());

      try 
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:548^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    rep_name
//            from      app_merch_bbt_reps
//            where     rep_id = :repId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    rep_name\n          from      app_merch_bbt_reps\n          where     rep_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.bbt.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,repId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.app.bbt.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:553^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          officerName.setData(rs.getString("rep_name"));
        }
        
        rs.close();
        it.close();
      }
      catch(Exception e) {
        logEntry("fillName:" + getData(),e.toString());
      }
      finally {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }

  }

  public class IfPosPartnerMustBeVitalProc implements Validation
  {
    private String    errorText   = "Cannot select Vital Pos Partner if not using Vital Processor";
    private Field     processor   = null;
    
    public IfPosPartnerMustBeVitalProc(Field processor)
    {
      this.processor = processor;  
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean isValid = true;
      
      if (fdata != null && processor != null && fdata.equals(Integer.toString(mesConstants.POS_PC)) && !processor.getData().equals(Integer.toString(mesConstants.APP_PROCESSOR_TYPE_VITAL)) )
      {
        isValid = false;
      }
      
      return isValid;
    }

  }

  public class IfEbtThenReqValidation implements Validation
  {
    private String    errorText   = "If EBT is selected Debit must also be selected";
    private Condition con         = null;
    
    public IfEbtThenReqValidation(Condition con)
    {
      this.con = con;  
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean isValid = true;
      
      if (con.isMet() && (fdata == null || fdata.equals("n")) )
      {
        isValid = false;
      }
      
      return isValid;
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      Field f = null;
      
      // make some unused fields optional
      fields.getField("contactPhone"    ).makeOptional();
      ((NameField)fields.getField("contactName")).makeOptional(true);
      
      fields.getField("industryType"    ).makeOptional();
      fields.getField("locationType"    ).makeOptional();
      fields.getField("locationYears"   ).makeOptional();
      fields.getField("establishedDate" ).makeOptional();
      fields.getField("businessDesc"    ).makeOptional();
      
      fields.getField("haveProcessed"   ).makeOptional();
      fields.getField("statementsProvided").makeOptional();
      fields.getField("haveCanceled"    ).makeOptional();
      
      fields.getField("bankName"        ).makeOptional();
      fields.getField("bankAddress"     ).makeOptional();
      fields.getField("bankCsz"         ).makeOptional();
      fields.getField("sourceOfInfo"    ).makeOptional();
      
      fields.getField("contactNameFirst").makeOptional();
      fields.getField("contactNameLast" ).makeOptional();
      
      fields.getField("owner1Percent"   ).makeOptional();
      fields.getField("owner1Address1"  ).makeOptional();
      fields.getField("owner1Csz"       ).makeOptional();
      fields.getField("owner1Gender"    ).makeOptional();
      
      fields.getField("monthlySales"    ).makeOptional();
      fields.getField("monthlyVMCSales" ).makeOptional();
      fields.getField("averageTicket"   ).makeOptional();
      fields.getField("refundPolicy"    ).makeOptional();
      fields.getField("internetEmail"   ).makeOptional();

      // make some previously optional fields required
      fields.getField("businessEmail"   ).makeRequired();
      fields.getField("fcsNumber"       ).makeRequired();
      fields.getField("ebtOptions"      ).makeRequired();
      
      // setup the extended pay options fields
      createPayOptsExtendedFields();
      
      // alter the pos array to suit bbt both in verbage and order presentation
      posTypeList = new String[][]
      {
        { "Touch Tone Capture",                             
          Integer.toString(mesConstants.POS_TOUCHTONECAPTURE  ) },
        
        { "Electronic Draft Capture (EDC) Dial Terminals",
          Integer.toString(mesConstants.POS_DIAL_TERMINAL     ) },
        
        { "DialPay (Telephone Authorization & Capture)",
          Integer.toString(mesConstants.POS_DIAL_AUTH         ) },
        
        { "Vital POS Partner",
          Integer.toString(mesConstants.POS_PC                ) },
        
        { "Global PC Advantage",                             
          Integer.toString(mesConstants.POS_GLOBAL_PC         ) },

        { "Internet Solution",
          Integer.toString(mesConstants.POS_INTERNET          ) },
        
        { "Wireless Terminal", 
          Integer.toString(mesConstants.POS_WIRELESS_TERMINAL ) },
        
        { "Stage Only - 3rd Party Software", 
          Integer.toString(mesConstants.POS_OTHER ) }
      };
      ((RadioButtonField)((FieldGroup)fields.getField("gProduct")).getField("productType")).resetRadioButtons(posTypeList);
      
      
      // software product NOT required if 3rd party selected
      fields.getField("vitalProduct").makeOptional();
      
      // re-use std app fields for Discover RAP specific for BB&T
      f = new NumberField  ("motoPercentage",    "% CNP",3,3,true,0);
      f.addValidation( new EsaRapRequiredValidation( fields.getField("discoverAccepted"),
                                                     fields.getField("discoverRapRate") ) );
      fields.add(f);
      f = new DropDownField("locationYears","Years in Business",new YearsInBusinessTable(),true);
      f.addValidation( new EsaRapRequiredValidation( fields.getField("discoverAccepted"),
                                                     fields.getField("discoverRapRate") ) );
      fields.add(f);
      
      // replace hidden assoc num field with non-hidden version
      fields.add(new BBTAssocNumberField());

      // replace hidden sic code field with non-hidden version
      fields.add(new SicField            ("sicCode",             "SIC", false));
      
      // replace businessName field to impose Vital naming restrictions
      fields.add(VitalField.buildVitalDbaNameField("businessName","Business Name (DBA)",false));
      
      // employ BBT spec business type options
      ((FieldGroup)fields.getField("gBusInfo")).add  (new DropDownField    ("businessType",      "Type of Business",new BBTBusinessTypeTable(),false));
      
      // bb&t unique business info fields
      
      // center number
      fields.add(new CentersField(false));

      // nature of business
      fields.add(new DropDownField    ("businessNature",      "Nature of Business",new NatureOfBusinessTable (),false) );
      fields.add(new CheckboxField    ("internetInquiryOnly", "Internet Inquiry Only", false) );
      
      //MERCHANT AGREEMENT TERMS
      
      fields.add(new DropDownField    ("merchAgreementTerm", "Term",new BBTMerchAgreementTermsTable (),false) );


      // rep and referring officer
      fields.add(new BBTOfficerField  ("repOfficerNumber",        "Merchant Rep Officer #", false, true) );
      fields.add(new BBTOfficerField  ("referringOfficerNumber",  "Referring Officer #", true, false,5) );

      fields.getField("taxpayerId").makeRequired();
      fields.add( new MerchantTypeTaxIdField    ("taxTypeId",           "Merchant Type Tax ID",false) );

      f = new RadioButtonField ("seasonalMerchant","Seasonal Merchant",yesNoRadioList,0,false,"Required",RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
      f.addValidation(new SeasonalMerchantValidation());
      f.setShowErrorText(true);
      fields.add(f);

      // IF SINGLE LOCATION IS SELECTED # OF LOCATIONS SHOULD NOT BE REQUIRED
      fields.getField("numLocations").setOptionalCondition(
        new FieldValueCondition(fields.getField("applicationType"),"2") );

      // if nature of business is "Internet" OR if internet inquiry only checked,
      //  web url field should be required
      OrCondition oc = new OrCondition();
      oc.add( new FieldValueCondition(fields.getField("businessNature"),Integer.toString(NatureOfBusinessTable.BUSINESS_NATURE_INTERNET)) );
      oc.add( new FieldValueCondition(fields.getField("internetInquiryOnly"),"y") );
      fields.getField("webUrl").setOptionalCondition( oc );
      
      // bank account types
      fields.add(new DropDownField    ("typeOfAcct",          "Type of Account",new AccountTypeTable(),false,"2") ); // default to checking
      fields.add(new DropDownField    ("feesAccountType",     "Type of Account",new AccountTypeTable(),true,"2") ); // default to checking
      fields.add(new DropDownField    ("billingMethod",       "Billing Method",new BillingMethodTable(),false,"Debit") );  // default to Debit
          
      // add fee (secondary) bank account fields
      fields.add(new PasswordField    ("feesAccount",         "Checking Acct. #",17,15,true));
      fields.add(new Field            ("confirmFeesAccount",  "Confirm Checking Acct. #",17,15,true));
      fields.add(new PasswordField    ("feesTransitRouting",  "Transit Routing #",9,15,true));
      fields.add(new Field            ("confirmFeesTransitRouting","Confirm Transit Routing #",9,15,true));
      fields.add(new DropDownField    ("feesBillingMethod",   "Billing Method",new BillingMethodTable(),false,"Debit") );  // default to Debit
      
      // replace non-masked checking account/trn fields with masked versions
      fields.add (new PasswordField   ("checkingAccount",   "Checking Acct. #",17,15,false));
      fields.add (new Field           ("confirmCheckingAccount",   "Confirm Checking Acct. #",17,15,false));
      fields.add (new PasswordField   ("transitRouting",    "Transit Routing #",9,15,false));
      fields.add (new Field           ("confirmTransitRouting",    "Confirm Transit Routing #",9,15,false));
      
      // add deposit acct # confirmation
      fields.getField("confirmCheckingAccount")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("checkingAccount"),"Deposit Checking Account #"));
          
      Validation trv = new TransitRoutingValidation();
      fields.getField("transitRouting")
        .addValidation(trv);
      fields.getField("feesTransitRouting")
        .addValidation(trv);
      
      // add deposit transit routing # confirmation
      fields.getField("confirmTransitRouting")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("transitRouting"),"Deposit Transit Routing #"));
          
      // add field to allow auto-filling mailing name from dba name
      fields.add(new CheckboxField    ("mailAddrUseDBA",      false));
      fields.add(new Field            ("corporateName",       "Corporate Name",50,30,true));
      
      fields.getField("confirmFeesAccount")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("feesAccount"),"Fees Account #"));
      fields.getField("confirmFeesTransitRouting")
        .addValidation(new FieldEqualsFieldValidation(
          fields.getField("feesTransitRouting"),"Fees Transit Routing #"));
      fields.getField("confirmFeesTransitRouting")
        .addValidation(new TransitRoutingValidation());
        
      // set special validation on mailing address to check for corporate name
      fields.getField("mailingAddress1").addValidation(new BBTMailingAddrValidation());
      fields.getField("mailingCsz").addValidation(new BBTMailingCSZValidation());
      
      // replace productType and internetType fields with bb&t versions
      fields.add(new DropDownField   ("internetType",         "Options",new BBTInternetProductTable(),false));

      fields.getField("internetType").setOptionalCondition(
        new FieldValueCondition(fields.getField("productType"),
          Integer.toString(mesConstants.POS_INTERNET)));
      
      // add bb&t processor options
      fields.add(new RadioButtonField("processor",            ProcessorList,-1,false,"Please specify a processor"));

      fields.getField("productType").addValidation(new IfPosPartnerMustBeVitalProc(fields.getField("processor")));
      
      // add bb&t check payment option details
      fields.add(new RadioButtonField("checkProvider",        CheckProvidersList,3,true,""));
      fields.add(new RadioButtonField("checkOption",          CheckOptionsList,-1,true,""));
      fields.add(new Field           ("globalNumber",         "Global #",25,25,false));
      fields.add(new Field           ("ttechNumber",          "T-Tech #",25,25,false));

      // add unique bb&t misc field
      fields.add(new RadioButtonField("webReporting","Web Based Reporting",yesNoRadioList,-1,false,"Required",RadioButtonField.LAYOUT_SPEC_HORIZONTAL));
      
      // add optional conditions on check fields
      fields.getField("checkProvider").addValidation(new CheckProviderValidation());
        
      fields.getField("checkOption").addValidation(new CheckOptionValidation());
      
      AndCondition ac = new AndCondition();
      ac.add( new FieldValueCondition(fields.getField("checkAccepted"), "y") );
      ac.add( new FieldValueCondition(fields.getField("checkProvider"), mesConstants.APP_BBT_CHECK_PROVIDER_GLOBAL) );
      fields.getField("globalNumber").setOptionalCondition(ac);
        
      ac = new AndCondition();
      ac.add( new FieldValueCondition(fields.getField("checkAccepted"), "y") );
      ac.add( new FieldValueCondition(fields.getField("checkProvider"), mesConstants.APP_BBT_CHECK_PROVIDER_TTECH) );
      fields.getField("ttechNumber").setOptionalCondition(ac);
      
      // make businessEmail optional if web reporting is set to No
      fields.getField("businessEmail").setOptionalCondition( 
        new FieldValueCondition(fields.getField("webReporting"),"Y") );
      
      // set validation for EBT
      fields.getField("fcsNumber").setOptionalCondition(
        new FieldValueCondition(fields.getField("ebtAccepted"),"y") );
      fields.getField("ebtOptions").setOptionalCondition(
        new FieldValueCondition(fields.getField("ebtAccepted"),"y") );

      //make required... but under certain condition only.. only if ebt is selected
      fields.getField("debitAccepted").addValidation(new IfEbtThenReqValidation(new FieldValueCondition(fields.getField("ebtAccepted"),"y")));

      fields.add(new CheckboxField    ("giftCardAccepted", "Gift Card", false) );



      // clear out all field group labels
      fields.clearLabel(true);
      
      // set extra on all fields not set yet
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch( Exception e )
    {
      logEntry("createFields()",e.toString());
    }
  }
  
  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);

    // fill mailing address from DBA if checkbox is checked
    if(fields.getField("mailAddrUseDBA").getData().equals("y"))
    {
      fields.getField("mailingAddress1").setData(fields.getField("businessAddress1").getData());
      fields.getField("mailingAddress2").setData(fields.getField("businessAddress2").getData());
      fields.getField("mailingCsz").setData(fields.getField("businessCsz").getData());
    }
    
    // set the default bank info (used by other apps like Amex ESA)
    if ( getData("bankName").equals("") )
    {
      setData("bankName", "BB&T"); 
    }      
    if ( getData("bankAddress").equals("") )
    {
      setData("bankAddress", "1604 S Tarboro St.");
    }
    if ( getData("bankCsz").equals("") )
    {
      setData("bankCsz", "Wilson, NC  27893");
    }      

    /*
    // fill check merchant number field based on selected data
    if(fields.getData("checkAccepted").equals("y"))
    {
      if(fields.getData("checkProvider").equals(mesConstants.APP_BBT_CHECK_PROVIDER_GLOBAL))
      {
        fields.setData("checkAcctNum", fields.getData("globalNumber"));
      }
      else if(fields.getData("checkProvider").equals(mesConstants.APP_BBT_CHECK_PROVIDER_TTECH))
      {
        fields.setData("checkAcctNum", fields.getData("ttechNumber"));
      }
    }
    */

  }

  protected boolean loadAppData()
  {
    long                  appSeqNum   = 0L;
    ResultSetIterator     it          = null;
    boolean               retVal      = super.loadAppData();
  
    try
    {
      appSeqNum = fields.getField("appSeqNum").asInteger();
    
      if( retVal == true && appSeqNum != 0L )
      {
        connect();
      
        /*@lineinfo:generated-code*//*@lineinfo:948^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mr.web_reporting                  as web_reporting,
//                    mr.processor                      as processor,
//                    mr.merch_referring_bank           as center_number,
//                    mr.business_nature                as business_nature,
//                    mr.tax_type_id                    as tax_type_id,
//                    mr.merch_legal_name               as corporate_name,
//                    lower(mr.mail_addr_use_bus_addr)  as mail_addr_use_dba
//            from    merchant        mr                 
//            where   mr.app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mr.web_reporting                  as web_reporting,\n                  mr.processor                      as processor,\n                  mr.merch_referring_bank           as center_number,\n                  mr.business_nature                as business_nature,\n                  mr.tax_type_id                    as tax_type_id,\n                  mr.merch_legal_name               as corporate_name,\n                  lower(mr.mail_addr_use_bus_addr)  as mail_addr_use_dba\n          from    merchant        mr                 \n          where   mr.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.bbt.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.app.bbt.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:959^9*/
        setFields(it.getResultSet());
        it.close();
        
        /*@lineinfo:generated-code*//*@lineinfo:963^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mb.bankacc_type                   fees_account_type,
//                    mb.merchbank_acct_num             fees_account,
//                    mb.merchbank_acct_num             confirm_fees_account,
//                    mb.merchbank_transit_route_num    fees_transit_routing,
//                    mb.billing_method                 fees_billing_method,
//                    mb.merchbank_transit_route_num    confirm_fees_transit_routing
//            from    merchbank     mb
//            where   mb.app_seq_num = :appSeqNum and
//                    mb.merchbank_acct_srnum = 2
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mb.bankacc_type                   fees_account_type,\n                  mb.merchbank_acct_num             fees_account,\n                  mb.merchbank_acct_num             confirm_fees_account,\n                  mb.merchbank_transit_route_num    fees_transit_routing,\n                  mb.billing_method                 fees_billing_method,\n                  mb.merchbank_transit_route_num    confirm_fees_transit_routing\n          from    merchbank     mb\n          where   mb.app_seq_num =  :1  and\n                  mb.merchbank_acct_srnum = 2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.bbt.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.app.bbt.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:974^9*/
        setFields(it.getResultSet());
        it.close();        
        
        // get bbt-specific fields
        /*@lineinfo:generated-code*//*@lineinfo:979^9*/

//  ************************************************************
//  #sql [Ctx] it = { select   MERCHREP_OFFICERNUM              rep_officer_number
//                    ,referring_officer                referring_officer_number
//                    ,check_provider                   check_provider
//                    ,check_option                     check_option
//                    ,global_number                    global_number
//                    ,ttech_number                     ttech_number
//                    ,seasonal_merchant                seasonal_merchant
//                    ,internet_inquiry_only            internet_inquiry_only
//                    ,merch_agreement_term             merch_agreement_term
//                    ,gift_card_accepted               gift_card_accepted
//            from    app_merch_bbt bbt
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   MERCHREP_OFFICERNUM              rep_officer_number\n                  ,referring_officer                referring_officer_number\n                  ,check_provider                   check_provider\n                  ,check_option                     check_option\n                  ,global_number                    global_number\n                  ,ttech_number                     ttech_number\n                  ,seasonal_merchant                seasonal_merchant\n                  ,internet_inquiry_only            internet_inquiry_only\n                  ,merch_agreement_term             merch_agreement_term\n                  ,gift_card_accepted               gift_card_accepted\n          from    app_merch_bbt bbt\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.bbt.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.app.bbt.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:993^9*/
        setFields(it.getResultSet());

        it.close();
        
        
        /*
        // set the proper check merchant number based on which check option
        // is chosen
        if(fields.getData("checkAccepted").equals("y"))
        {
          if(fields.getData("checkProvider").equals(mesConstants.APP_BBT_CHECK_PROVIDER_GLOBAL))
          {
            fields.setData("globalNumber", fields.getData("checkAcctNum"));
          }
          else if(fields.getData("checkProvider").equals(mesConstants.APP_BBT_CHECK_PROVIDER_TTECH))
          {
            fields.setData("ttechNumber", fields.getData("checkAcctNum"));
          }
        }
        */

      }        
    }
    catch(Exception e )
    {
      logEntry("loadAppData()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
      cleanUp();  // not really needed, see above
    }
    return( retVal );
  }
  
  protected boolean submitAppData()
  {
    long                  appSeqNum   = 0L;
    int                   recCount    = 0;
    boolean               retVal      = super.submitAppData();

    try
    {
      appSeqNum = fields.getField("appSeqNum").asInteger();
    
      
      if( retVal == true && appSeqNum != 0L )
      {
        String merchRepCode = fields.getField("repOfficerNumber").getData();
        if(merchRepCode.length() > 4) // (take only the 4 rightmost chars)
          merchRepCode = merchRepCode.substring(merchRepCode.length()-4);

        connect();

        // update merchant table bbt-specific columns
        /*@lineinfo:generated-code*//*@lineinfo:1049^9*/

//  ************************************************************
//  #sql [Ctx] { update merchant
//            set web_reporting         = :fields.getField("webReporting").getData(),
//                processor             = :fields.getField("processor").getData(),
//                merch_referring_bank  = :fields.getField("centerNumber").getData(),
//                merch_rep_code        = :merchRepCode,
//                business_nature       = :fields.getField("businessNature").getData(),
//                tax_type_id           = :fields.getField("taxTypeId").getData(),
//                merch_legal_name      = :fields.getField("corporateName").getData(),
//                mail_addr_use_bus_addr= :fields.getField("mailAddrUseDBA").getData().toUpperCase()
//            where app_seq_num         = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2510 = fields.getField("webReporting").getData();
 String __sJT_2511 = fields.getField("processor").getData();
 String __sJT_2512 = fields.getField("centerNumber").getData();
 String __sJT_2513 = fields.getField("businessNature").getData();
 String __sJT_2514 = fields.getField("taxTypeId").getData();
 String __sJT_2515 = fields.getField("corporateName").getData();
 String __sJT_2516 = fields.getField("mailAddrUseDBA").getData().toUpperCase();
   String theSqlTS = "update merchant\n          set web_reporting         =  :1 ,\n              processor             =  :2 ,\n              merch_referring_bank  =  :3 ,\n              merch_rep_code        =  :4 ,\n              business_nature       =  :5 ,\n              tax_type_id           =  :6 ,\n              merch_legal_name      =  :7 ,\n              mail_addr_use_bus_addr=  :8 \n          where app_seq_num         =  :9";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.app.bbt.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2510);
   __sJT_st.setString(2,__sJT_2511);
   __sJT_st.setString(3,__sJT_2512);
   __sJT_st.setString(4,merchRepCode);
   __sJT_st.setString(5,__sJT_2513);
   __sJT_st.setString(6,__sJT_2514);
   __sJT_st.setString(7,__sJT_2515);
   __sJT_st.setString(8,__sJT_2516);
   __sJT_st.setLong(9,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1061^9*/
      
        // update bank account information
        /*@lineinfo:generated-code*//*@lineinfo:1064^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    merchbank
//            where   app_seq_num         = :appSeqNum and
//                    merchbank_acct_srnum = 2
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    merchbank\n          where   app_seq_num         =  :1  and\n                  merchbank_acct_srnum = 2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.bbt.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1070^9*/

        if (!fields.getField("feesAccount").isBlank())
        {
          /*@lineinfo:generated-code*//*@lineinfo:1074^11*/

//  ************************************************************
//  #sql [Ctx] { insert into merchbank
//              (
//                app_seq_num,
//                bankacc_type,
//                merchbank_acct_num,
//                merchbank_transit_route_num,
//                billing_method,
//                merchbank_acct_srnum
//              )
//              values
//              (
//                :appSeqNum,
//                :fields.getField("feesAccountType").getData(),
//                :fields.getField("feesAccount").getData(),
//                :fields.getField("feesTransitRouting").getData(),
//                :fields.getField("feesBillingMethod").getData(),
//                2
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2517 = fields.getField("feesAccountType").getData();
 String __sJT_2518 = fields.getField("feesAccount").getData();
 String __sJT_2519 = fields.getField("feesTransitRouting").getData();
 String __sJT_2520 = fields.getField("feesBillingMethod").getData();
   String theSqlTS = "insert into merchbank\n            (\n              app_seq_num,\n              bankacc_type,\n              merchbank_acct_num,\n              merchbank_transit_route_num,\n              billing_method,\n              merchbank_acct_srnum\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n              2\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.app.bbt.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_2517);
   __sJT_st.setString(3,__sJT_2518);
   __sJT_st.setString(4,__sJT_2519);
   __sJT_st.setString(5,__sJT_2520);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1094^11*/
        }
        
        // update bbt-specific table
        /*@lineinfo:generated-code*//*@lineinfo:1098^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//            
//            from    app_merch_bbt
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n           \n          from    app_merch_bbt\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.app.bbt.Business",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1104^9*/
        
        if(recCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1108^11*/

//  ************************************************************
//  #sql [Ctx] { update  app_merch_bbt
//              set     merchrep_officernum     = :fields.getField("repOfficerNumber").getData()
//                      ,referring_officer      = :fields.getField("referringOfficerNumber").getData()
//                      ,check_provider         = :fields.getField("checkProvider").getData()
//                      ,check_option           = :fields.getField("checkOption").getData()
//                      ,global_number          = :fields.getField("globalNumber").getData()
//                      ,ttech_number           = :fields.getField("ttechNumber").getData()
//                      ,seasonal_merchant      = :fields.getField("seasonalMerchant").getData()
//                      ,internet_inquiry_only  = :fields.getField("internetInquiryOnly").getData()
//                      ,merch_agreement_term   = :fields.getField("merchAgreementTerm").getData()
//                      ,gift_card_accepted     = :fields.getField("giftCardAccepted").getData()
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2521 = fields.getField("repOfficerNumber").getData();
 String __sJT_2522 = fields.getField("referringOfficerNumber").getData();
 String __sJT_2523 = fields.getField("checkProvider").getData();
 String __sJT_2524 = fields.getField("checkOption").getData();
 String __sJT_2525 = fields.getField("globalNumber").getData();
 String __sJT_2526 = fields.getField("ttechNumber").getData();
 String __sJT_2527 = fields.getField("seasonalMerchant").getData();
 String __sJT_2528 = fields.getField("internetInquiryOnly").getData();
 String __sJT_2529 = fields.getField("merchAgreementTerm").getData();
 String __sJT_2530 = fields.getField("giftCardAccepted").getData();
   String theSqlTS = "update  app_merch_bbt\n            set     merchrep_officernum     =  :1 \n                    ,referring_officer      =  :2 \n                    ,check_provider         =  :3 \n                    ,check_option           =  :4 \n                    ,global_number          =  :5 \n                    ,ttech_number           =  :6 \n                    ,seasonal_merchant      =  :7 \n                    ,internet_inquiry_only  =  :8 \n                    ,merch_agreement_term   =  :9 \n                    ,gift_card_accepted     =  :10 \n            where   app_seq_num =  :11";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.app.bbt.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2521);
   __sJT_st.setString(2,__sJT_2522);
   __sJT_st.setString(3,__sJT_2523);
   __sJT_st.setString(4,__sJT_2524);
   __sJT_st.setString(5,__sJT_2525);
   __sJT_st.setString(6,__sJT_2526);
   __sJT_st.setString(7,__sJT_2527);
   __sJT_st.setString(8,__sJT_2528);
   __sJT_st.setString(9,__sJT_2529);
   __sJT_st.setString(10,__sJT_2530);
   __sJT_st.setLong(11,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1122^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:1126^11*/

//  ************************************************************
//  #sql [Ctx] { insert into app_merch_bbt
//              (
//                 app_seq_num
//                ,merchrep_officernum
//                ,referring_officer
//                ,check_provider
//                ,check_option
//                ,global_number
//                ,ttech_number
//                ,seasonal_merchant
//                ,internet_inquiry_only
//                ,merch_agreement_term
//                ,gift_card_accepted
//              )
//              values
//              (
//                 :appSeqNum
//                ,:fields.getField("repOfficerNumber").getData()
//                ,:fields.getField("referringOfficerNumber").getData()
//                ,:fields.getField("checkProvider").getData()
//                ,:fields.getField("checkOption").getData()
//                ,:fields.getField("globalNumber").getData()
//                ,:fields.getField("ttechNumber").getData()
//                ,:fields.getField("seasonalMerchant").getData()
//                ,:fields.getField("internetInquiryOnly").getData()
//                ,:fields.getField("merchAgreementTerm").getData()
//                ,:fields.getField("giftCardAccepted").getData()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2531 = fields.getField("repOfficerNumber").getData();
 String __sJT_2532 = fields.getField("referringOfficerNumber").getData();
 String __sJT_2533 = fields.getField("checkProvider").getData();
 String __sJT_2534 = fields.getField("checkOption").getData();
 String __sJT_2535 = fields.getField("globalNumber").getData();
 String __sJT_2536 = fields.getField("ttechNumber").getData();
 String __sJT_2537 = fields.getField("seasonalMerchant").getData();
 String __sJT_2538 = fields.getField("internetInquiryOnly").getData();
 String __sJT_2539 = fields.getField("merchAgreementTerm").getData();
 String __sJT_2540 = fields.getField("giftCardAccepted").getData();
   String theSqlTS = "insert into app_merch_bbt\n            (\n               app_seq_num\n              ,merchrep_officernum\n              ,referring_officer\n              ,check_provider\n              ,check_option\n              ,global_number\n              ,ttech_number\n              ,seasonal_merchant\n              ,internet_inquiry_only\n              ,merch_agreement_term\n              ,gift_card_accepted\n            )\n            values\n            (\n                :1 \n              , :2 \n              , :3 \n              , :4 \n              , :5 \n              , :6 \n              , :7 \n              , :8 \n              , :9 \n              , :10 \n              , :11 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.app.bbt.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_2531);
   __sJT_st.setString(3,__sJT_2532);
   __sJT_st.setString(4,__sJT_2533);
   __sJT_st.setString(5,__sJT_2534);
   __sJT_st.setString(6,__sJT_2535);
   __sJT_st.setString(7,__sJT_2536);
   __sJT_st.setString(8,__sJT_2537);
   __sJT_st.setString(9,__sJT_2538);
   __sJT_st.setString(10,__sJT_2539);
   __sJT_st.setString(11,__sJT_2540);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1156^11*/
        }
      }        
    }
    catch(Exception e )
    {
      logEntry("submitAppData()",e.toString());
    }
    finally
    {
      cleanUp();  // not really needed, see above
    }
    return( retVal );
  }

  protected class SeasonalMerchantValidation extends AtLeastOneValidation
  {
    public SeasonalMerchantValidation()
    {
      super("At least one seasonal month must be checked for Seasonal Merchants",(FieldGroup)fields.getField("seasonalMonths"));
    }
    
    public boolean validate(String fieldData)
    {
      // require at least one seasonal month be selected if seasonal merchant
      boolean isSeasonalMerchant = fieldData.toUpperCase().equals("Y");
      if(isSeasonalMerchant)
        return super.validate(null);
      else
        return true;
    }
  }
  
  public class BBTMailingAddrValidation
    implements Validation
  {
    private String  errorText     = "";
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {               
      boolean result = true;
      
      if(fdata == null || fdata.equals(""))
      {
        // mailing address can only be blank if either corporate name is blank
        // or use dba address checkbox is checked
        if( !fields.getField("corporateName").getData().equals("") &&
            !fields.getField("mailAddrUseDBA").getData().equals("y"))
        {
          // this field is now required
          errorText = "Required if Corporate Name is not blank";
          result = false;
        }
      }
      
      return result;
    }
  }
  
  public class BBTMailingCSZValidation
    implements Validation
  {
    private String errorText    = "";
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean result = true;
      
      if(fdata == null || fdata.equals(""))
      {
        if( !fields.getField("mailingAddress1").getData().equals("") &&
            !fields.getField("mailAddrUseDBA").getData().equals("y"))
        {
          errorText = "Required if Mailing Address Line 1 is not blank";
          result = false;
        }
      }
      
      return result;
    }
  }
  
  public class FieldDependenceValidation implements Validation
  {
    private String  errorText = null;
    
    private Field   checkField  = null;
    private String  checkVal    = null;
    
    public FieldDependenceValidation(Field f, String val, String errorMsg)
    {
      errorText = errorMsg;
      checkField = f;
      checkVal = val;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean result = true;
      
      if(fdata == null || fdata.equals(""))
      {
        // check to see if check field equals check data
        if(checkField.getData().equals(checkVal))
        {
          result = false;
        }
      }
      
      return result;
    }
  } 
  
  public class CheckProviderValidation implements Validation
  {
    private String errorText = null;
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean result = true;
      
      if((fdata == null || fdata.equals("")) && fields.getData("checkAccepted").equals("y"))
      {
        result = false;
        errorText = "Check Provider required";
      }
      
      return result;
    }
  }
  
  public class CheckOptionValidation implements Validation
  {
    private String errorText = null;
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean result = true;
      
      // get reference to RadioButton field
      RadioButtonField  checkOption = (RadioButtonField)(fields.getField("checkOption"));
      
      if(fields.getData("checkAccepted").equals("y"))
      {
        // check which check product was selected
        if(fields.getData("checkProvider").equals(mesConstants.APP_BBT_CHECK_PROVIDER_GLOBAL))
        {
          if(fdata == null || fdata.equals("") || Integer.parseInt(fdata) < 1 || Integer.parseInt(fdata) > 2)
          {
            result = false;
            errorText = "Global product required";
            checkOption.setErrorLabel(0);
          }
        }
        else if(fields.getData("checkProvider").equals(mesConstants.APP_BBT_CHECK_PROVIDER_TTECH))
        {
          if(fdata == null || fdata.equals("") || Integer.parseInt(fdata) < 3 || Integer.parseInt(fdata) > 5)
          {
            result = false;
            errorText = "T-Tech product required";
            checkOption.setErrorLabel(2);
          }
        }
      }
      
      return result;
    }
  }
  
}/*@lineinfo:generated-code*/