/*@lineinfo:filename=Pricing*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/bbt/Pricing.sqlj $

  Description:

  Pricing

  CB&T online app pricing page bean.  Extends PricingBase with CB&T custom
  pricing options.

  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/26/04 4:01p $
  Version            : $Revision: 23 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.bbt;

import java.sql.ResultSet;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.FeeSet;
import com.mes.app.FeeSet.Fee;
import com.mes.app.FeeSet.TranFee;
import com.mes.app.PricingBase;
import com.mes.constants.mesConstants;
import com.mes.forms.CheckboxField;
import com.mes.forms.Condition;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateField;
import com.mes.forms.DateStringField;
import com.mes.forms.DiscountField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.SmallCurrencyField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.reports.RiskScoreDataBean.PCIOtherValidation;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;


public class Pricing extends PricingBase
{
  {
    appType     = 11;
    curScreenId = 4;
  }

  public static final int WARN_PRICING_NOT_VALID            = 2;
  public static final int WARN_UNROLLED_PRICING_NOT_VALID   = 3;
  
  public static final int ICBET_TYPE_NONE = 0;
  public static final int ICBET_TYPE_STD  = 21;
  public static final int ICBET_TYPE_IC   = 22;
  public static final int ICBET_TYPE_TIER = 23;

  public static final String CHRG_BASIS_ONE_TIME  = "OT";
  public static final String CHRG_BASIS_MONTHLY   = "MC";
  public static final String CHRG_BASIS_YEARLY    = "YR";
 
  /*************************************************************************
  **
  **   Drop Down Tables
  **
  **************************************************************************/
  protected class PCIVendorTable extends DropDownTable
  {
    public PCIVendorTable( )
    {
      ResultSetIterator     it          = null;
      ResultSet             resultSet   = null;
      
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:90^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  pv.vendor_code,
//                    pv.vendor_name
//            from    risk_pci_vendors      pv
//            where   pv.hierarchy_node = 386700000
//            order by pv.display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pv.vendor_code,\n                  pv.vendor_name\n          from    risk_pci_vendors      pv\n          where   pv.hierarchy_node = 386700000\n          order by pv.display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.bbt.Pricing",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.bbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:97^9*/
        resultSet = it.getResultSet();
        
        addElement("","select");
        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      catch( Exception e )
      {
        logEntry("PCIVendorTable()",e.toString());
      }
      finally
      {
        try{ it.close(); } catch(Exception e){}
        cleanUp();
      }
    }
  }
  
  protected class OwnLeaseTable extends DropDownTable
  {
    public OwnLeaseTable()
    {
      addElement("","select one");
      addElement("O","Owned");
      addElement("L","Leased");
    }
  }
  
  protected class LocationTypeTable extends DropDownTable
  {
    public LocationTypeTable()
    {
      addElement("","select one");
      addElement("1","Retail Storefront");
      addElement("2","Mall");
      addElement("3","Office Building");
      addElement("4","Separate Building");
      addElement("5","Commercial Office");
      addElement("6","Industrial");
      addElement("7","Residential");
      addElement("8","Warehouse");
      addElement("9","Kiosk");
      addElement("10","Flea Market");
      addElement("11","Trade Show");
      addElement("12","Other");
    }
  }
  
  protected class LocationFootageTable extends DropDownTable
  {
    public LocationFootageTable()
    {
      addElement("","select one");
      addElement("250","0-250");
      addElement("500","251-500");
      addElement("2000","501-2000");
      addElement("2001","2000+");
    }
  }
  
  protected class LocationFloorTable extends DropDownTable
  {
    public LocationFloorTable()
    {
      addElement("","select one");
      addElement("1","Ground");
      addElement("2","Other");
    }
  }
  
  protected class EmployeeCountTable extends DropDownTable
  {
    public EmployeeCountTable()
    {
      addElement("","select one");
      addElement("3","1-3");
      addElement("5","3-5");
      addElement("10","5-10");
      addElement("20","10-20");
      addElement("50","20-50");
      addElement("51","50+");
    }
  }


  protected class MiscChargeTypeTable extends DropDownTable
  {
    public MiscChargeTypeTable()
    {
      addElement("MIS", "Miscellaneous");
      addElement("IMP", "Imprinter Fee");
      addElement("POS", "POS Terminal");
      addElement("MEM", "Membership Fee");
      addElement("CBF", "Chargeback Fee");
      addElement("ACH", "ACH Charge Rec");
    }
  }

  
  private String stdPricingDefault = null;
  
  protected class StandardPricingTable extends DropDownTable
  {
    public StandardPricingTable()
    {
      addElement("",  "select one");
      
      ResultSetIterator it = null;
      
      try
      {
        connect();
        
        String retailFlag   = "N";
        String govpurchFlag = "N";
        String motoFlag     = "N";

        switch (getBusNature())
        {
          case 1:
          case 2:
          case 3:
          default:
            retailFlag = "Y";
            break;
            
          case 4:
            govpurchFlag = "Y";
            break;
          case 5:
          case 6:
          case 7:
            motoFlag    = "Y";
            break;
        }
        
        String dialpayFlag =
          ( getPaymentMethod() == mesConstants.POS_DIAL_AUTH ? "Y" : "N" );
      
        /*@lineinfo:generated-code*//*@lineinfo:241^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sb.value        value,
//                    sb.is_default   is_default,
//                    sb.label || ' (' || ib.vs_bet || '/' || ib.mc_bet || ')'
//                                    label
//                    
//            from    bbt_std_pricing_bets sb,
//                    bbt_interchange_bets ib
//                    
//            where   sb.is_retail    = :retailFlag     and
//                    sb.is_dialpay   = :dialpayFlag    and
//                    sb.is_govpurch  = :govpurchFlag   and
//                    sb.is_moto      = :motoFlag       and
//                    sb.value = ib.bet_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sb.value        value,\n                  sb.is_default   is_default,\n                  sb.label || ' (' || ib.vs_bet || '/' || ib.mc_bet || ')'\n                                  label\n                  \n          from    bbt_std_pricing_bets sb,\n                  bbt_interchange_bets ib\n                  \n          where   sb.is_retail    =  :1      and\n                  sb.is_dialpay   =  :2     and\n                  sb.is_govpurch  =  :3    and\n                  sb.is_moto      =  :4        and\n                  sb.value = ib.bet_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,retailFlag);
   __sJT_st.setString(2,dialpayFlag);
   __sJT_st.setString(3,govpurchFlag);
   __sJT_st.setString(4,motoFlag);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.bbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:256^9*/
        
        ResultSet rs = it.getResultSet();
        String defaultValue = null;
        while (rs.next())
        {
          addElement(rs.getString("value"),rs.getString("label"));
          if (rs.getString("is_default").equals("Y"))
          {
            stdPricingDefault = rs.getString("value");
          }
        }
      }
      catch( Exception e )
      {
        System.out.println(this.getClass().getName() + "::StandardPricingTable(): "
          + e.toString());
        e.printStackTrace();
        logEntry("StandardPricingTable()",e.toString());
      }
      finally
      {
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  

  private String interchangePlusDefault = null;
  
  protected class InterchangePlusTable extends DropDownTable
  {

    public InterchangePlusTable()
    {
      addElement("",  "select one");
      
      ResultSetIterator it = null;
      
      try
      {
        connect();
        
        String retailFlag = null;
        switch (getBusNature())
        {
          case 1:
          case 2:
          case 3:
          default:
            retailFlag = "Y";
            break;
            
          case 4:
          case 5:
          case 6:
          case 7:
            retailFlag = "N";
            break;
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:318^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  icpb.value                                    value,
//                    ' (' || ib.vs_bet || '/' || ib.mc_bet || ')'  label,
//                    icpb.is_default                               is_default
//                    
//            from    bbt_ic_plus_pricing_bets icpb,
//                    bbt_interchange_bets  ib
//                    
//            where   icpb.is_retail = :retailFlag and
//                    icpb.value = ib.bet_type
//            
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  icpb.value                                    value,\n                  ' (' || ib.vs_bet || '/' || ib.mc_bet || ')'  label,\n                  icpb.is_default                               is_default\n                  \n          from    bbt_ic_plus_pricing_bets icpb,\n                  bbt_interchange_bets  ib\n                  \n          where   icpb.is_retail =  :1  and\n                  icpb.value = ib.bet_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,retailFlag);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.bbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:330^9*/
        
        ResultSet rs = it.getResultSet();
        String defaultValue = null;
        while (rs.next())
        {
          addElement(rs.getString("value"),rs.getString("label"));
          if (rs.getString("is_default").equals("Y"))
          {
            interchangePlusDefault = rs.getString("value");
          }
        }
        
      }
      catch( Exception e )
      {
        System.out.println(this.getClass().getName() + "::InterchangePlusTable(): "
          + e.toString());
        e.printStackTrace();
        logEntry("InterchangePlusTable()",e.toString());
      }
      finally
      {
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }






  private String tierPricingDefault = null;
  
  protected class TieredPricingTable extends DropDownTable
  {
    public TieredPricingTable()
    {
      addElement("",  "select one");
      
      ResultSetIterator it = null;
      
      try
      {
        connect();
        
        String retailFlag = null;
        switch (getBusNature())
        {
          case 1:
          case 2:
          case 3:
          default:
            retailFlag = "Y";
            break;
            
          case 4:
          case 5:
          case 6:
          case 7:
            retailFlag = "N";
            break;
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:396^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tb.value        value,
//                    tb.label || ' (' || ib.vs_bet || '/' || ib.mc_bet || ')'
//                                    label
//                    
//            from    bbt_tier_pricing_bets tb,
//                    bbt_interchange_bets  ib
//                    
//            where   tb.is_retail = :retailFlag and
//                    tb.value = ib.bet_type
//            
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tb.value        value,\n                  tb.label || ' (' || ib.vs_bet || '/' || ib.mc_bet || ')'\n                                  label\n                  \n          from    bbt_tier_pricing_bets tb,\n                  bbt_interchange_bets  ib\n                  \n          where   tb.is_retail =  :1  and\n                  tb.value = ib.bet_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,retailFlag);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.bbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:408^9*/
        
        ResultSet rs = it.getResultSet();
        String defaultValue = null;
        while (rs.next())
        {
          addElement(rs);
        }
        
        addElement("999999","Other");
      }
      catch( Exception e )
      {
        System.out.println(this.getClass().getName() + "::TieredPricingTable(): "
          + e.toString());
        e.printStackTrace();
        logEntry("TieredPricingTable()",e.toString());
      }
      finally
      {
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  
  /*************************************************************************
  **
  **   Radio Button Lists
  **
  **************************************************************************/
  
  private static final String[][] pricingTypeList = 
  {
    { "Standard Pricing", 
      Integer.toString(mesConstants.BBT_STANDARD_PRICING_PLAN) },
    { "Interchange Plus",
      Integer.toString(mesConstants.BBT_INTERCHANGE_PLUS_PLAN) },
    { "Tiered Pricing",
      Integer.toString(mesConstants.BBT_TIERED_PRICING_PLAN) }
  };
  
  private static final String[][] chargeBasisTypeList = 
  {
    { "One Time", CHRG_BASIS_ONE_TIME },
    { "Monthly",  CHRG_BASIS_MONTHLY  },
    { "Yearly",   CHRG_BASIS_YEARLY   }
  };


  /*************************************************************************
  **
  **   Validations
  **
  **************************************************************************/
  
  protected class RateValidation implements Validation
  {
    private     int     otherBetType    = -1;
    protected   String  ErrorMessage    = null;
    protected   int     WarningType     = WARN_PRICING_NOT_VALID;

    public RateValidation(int otherBetType)
    {
      this.otherBetType = otherBetType;
    }
   
    public String getErrorText()
    {
      return( ErrorMessage );
    }
    
    public boolean isBetSelected( )
    {
      return( fields.getField("betType").asInteger() == this.otherBetType );
    }
    
    public boolean validate(String fdata)
    {
      int     betType     = fields.getField("betType").asInteger();
      
      ErrorMessage = null;    // reset error message
      
      // only do validation for discount rates that fall under selected bet type
      if( isBetSelected() )
      {
        // convert discount rate to double
        double discRate = -1;
        try
        {
          discRate = Double.parseDouble(fdata);
          
          // validate rate based on bet type
          switch (betType)
          {
            // standard and tier pricing require
            // a rate between 1.25 and 9.99
            case ICBET_TYPE_STD:
            case ICBET_TYPE_TIER:
              if ((discRate < 1.25 || discRate > 9.99) && !hasWarning(WarningType) )
              {
                ErrorMessage = "Warning! This rate is outside of the standard parameters. Please review before continuing.";
                addWarning(WarningType);
              }
              else
              {
                removeWarning(WarningType);
              }
              break;
        
            // interchange plus may be empty or between 0 and 1.25
            case ICBET_TYPE_IC: 
              if (discRate > 1.25 && !hasWarning(WarningType) )
              {
                ErrorMessage = "Warning! This rate is outside of the standard parameters. Please review before continuing.";
                addWarning(WarningType);
              }
              else
              {
                removeWarning(WarningType);
              }
              break;
          
            default:
              break;
          }
        }
        catch (Exception e)
        {
          ErrorMessage = "Invalid discount rate";
        }
      }
      return( ErrorMessage == null );        
    }
  }
  
  protected class UnrolledRateValidation extends RateValidation
  {
    Field       UnrolledPricing     = null;
    
    public UnrolledRateValidation( int otherBetType, Field unrolled )
    {
      super(otherBetType);
      UnrolledPricing = unrolled;
      WarningType = WARN_UNROLLED_PRICING_NOT_VALID;
    }
    
    public String getErrorText()
    {
      return( ErrorMessage );
    }
    
    public boolean validate(String fdata)
    {
      boolean     retVal    = true;
    
      ErrorMessage = null;
      
      if ( isBetSelected() )
      {
        if ( UnrolledPricing.getData().equals("y") )
        {
          if ( fdata == null || fdata.equals("") )
          {
            ErrorMessage = "Field required when using unrolled pricing";
            retVal = false;
          }
          else
          {
            // do the standard rate validation
            retVal = super.validate(fdata);
          }
        }
        else    // using rolled pricing
        {
          if ( fdata != null && !fdata.equals("") )
          {
            ErrorMessage = "Field must be blank when using rolled pricing";
            retVal = false;
          }          
        }
      }        
      
      return( retVal );
    }
    
  }
  
  protected class HundredPercentValidation implements Validation
  {
    private Field otherField;
    
    public HundredPercentValidation(Field otherField)
    {
      this.otherField = otherField;
    }
    
    public boolean validate(String fdata)
    {
      // determine the total percentage (this field data + other field)
      int totalPercent = 0;
      try
      {
        totalPercent = Integer.parseInt(fdata);
      }
      catch (Exception e) {}
      totalPercent += otherField.asInteger();
      
      // if total percentage is 100, return true
      if (totalPercent == 100)
      {
        return true;
      }
      
      // if both fields are blank, return true
      if ((fdata == null ||  fdata.equals("")) &&
            otherField.isBlank())
      {
        return true;
      }
      
      // fields don't add up to 100 and they are not blank
      return false;
    }
    
    public String getErrorText()
    {
      return "Must total to 100%";
    }
  }
  
  public class ValidDateValidation implements Validation
  {
    private String  errorText   = "Please provide a valid date in MM/DD/YY format";
    private boolean ok999999    = false;
    private Field   self        = null;

    public ValidDateValidation(boolean ok999999, Field self)
    {
      this.ok999999 = ok999999;  
      this.self     = self;
    }

    public String getErrorText()
    {
      return (errorText + (ok999999 ? " or 999999 if there is no end date." : "."));
    }

    public boolean validate(String fdata)
    {
      boolean isValid = true;
      
      if (fdata == null || fdata.equals(""))
      {
        return isValid;
      }
      else if (ok999999 && fdata.equals("999999"))
      {
        return isValid;
      }
      else
      {
        try
        {
          Date parsedDate = parseDateString(fdata);
          if(parsedDate == null)
          {
            isValid = false;
          }
          else
          {
            self.setData(formatDateString(parsedDate));
          }
        }
        catch(Exception e)
        {
          isValid = false;
        }  
      }
     
      return isValid;
    }

    private Date parseDateString(String dateString) throws Exception
    {
      ParsePosition pos = new ParsePosition(0);
      SimpleDateFormat sdf 
        = new SimpleDateFormat((false ? "MM/yy" : "MM/dd/yy"));
      return sdf.parse(dateString,pos);
    }
  
    private String formatDateString(Date date)
    {
      if (date != null)
      {
        SimpleDateFormat sdf
          = new SimpleDateFormat((false ? "MM/yyyy" : "MM/dd/yy"));
        return sdf.format(date);
      }
      return "";
    }


  }


  public class IfNoNotBlankValidation implements Validation
  {
    private String  errorText   = "Field missing value";
    private Field   yesNoField  = null;
    
    public IfNoNotBlankValidation(Field yesNoField, String errorText)
    {
      this.yesNoField = yesNoField;  
      this.errorText  = errorText;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean isValid = true;
      
      if (yesNoField.getData().toUpperCase().equals("N") )
      {
        if (fdata == null || fdata.equals(""))
        {
          isValid = false;
        }
      }
      
      return isValid;
    }
  }
  
  public class IfYesNotBlankValidation implements Validation
  {
    private String  errorText   = "Field missing value";
    private Field   yesNoField  = null;
    
    public IfYesNotBlankValidation(Field yesNoField, String errorText)
    {
      this.yesNoField = yesNoField;  
      this.errorText  = errorText;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean isValid = true;
      
      if (yesNoField.getData().toUpperCase().equals("Y") )
      {
        if (fdata == null || fdata.equals(""))
        {
          isValid = false;;
        }
      }
      
      return isValid;
    }
  }

  public class IfNotBlankNotBlankValidation implements Validation
  {
    private String  errorText       = "Field missing value";
    private Field   notBlankField   = null;
    private Field   orNotBlankField = null;

    public IfNotBlankNotBlankValidation(Field notBlankField, Field orNotBlankField, String errorText)
    {
      this.notBlankField    = notBlankField;  
      this.orNotBlankField  = orNotBlankField;
      this.errorText        = errorText;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean isValid = true;
      
      if (!notBlankField.isBlank() || !orNotBlankField.isBlank())
      {
        if (fdata == null || fdata.equals(""))
        {
          isValid = false;;
        }
      }
      
      return isValid;
    }
  }


  public class IfNotBlankNotAllNValidation implements Validation
  {
    private String  errorText       = "Field missing value";
    private Field   notBlankField   = null;
    private Field   orNotBlankField = null;

    public IfNotBlankNotAllNValidation(Field notBlankField, Field orNotBlankField, String errorText)
    {
      this.notBlankField    = notBlankField;  
      this.orNotBlankField  = orNotBlankField;
      this.errorText        = errorText;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean isValid = true;
      
      if (!notBlankField.isBlank() || !orNotBlankField.isBlank())
      {
        if (fdata != null && fdata.equals("NNNNNNNNNNNN"))
        {
          isValid = false;;
        }
      }
      
      return isValid;
    }
  }
  
  public class DebitBRGValidation implements Validation
  {
    private String  ErrorText       = "Field missing value";
    private int     FieldType       = -1;
    private Field   UnrolledField   = null;

    public DebitBRGValidation( Field unrolledField, int ftype )
    {
      FieldType     = ftype;
      UnrolledField = unrolledField;
    }
    
    public String getErrorText()
    {
      return( ( ErrorText == null ) ? "" : ErrorText );
    }
    
    public boolean validate(String fdata)
    {
      ErrorText = null;
      
      if ( UnrolledField.getData().equals("y") )
      {
        if ( FieldType == FeeSet.TFT_DEBIT_BRG )
        {
          if( fdata == null || fdata.equals("") )
          {
            ErrorText = "Billing Rate Group required for unrolled pricing";
          }
        }
        else    // per item field type
        {
          double  dval = -1;
          
          try{ dval = Double.parseDouble(fdata); } catch( Exception e ){dval = -1;}
          if ( fdata != null && !fdata.equals("") && dval != 0.0 )
          {
            ErrorText = "Per item price must be $0.00 with unrolled pricing";
          }
        }
      }
      else    // per item pricing
      {
        if ( FieldType == FeeSet.TFT_DEBIT_BRG )
        {
          if ( fdata != null && !fdata.equals("") )
          {
            ErrorText = "Billing Rate Group not supported with fixed pricing";
          }
        }
        else    // per item field type
        {
          if( fdata == null || fdata.equals("") )
          {
            ErrorText = "Per item price required for fixed pricing";
          }
        }
      }
      
      return( ErrorText == null );
    }
  }

  /*************************************************************************
  **
  **   Custom Fields
  **
  **************************************************************************/
  
  protected class SeasonalFieldGroup extends FieldGroup
  {
    public SeasonalFieldGroup(String fname)
    {
      super(fname);
      
      String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

      for (int i = 0; i < 12; ++i)
      {
        add(new CheckboxField(fname + i,months[i],false));
      }
    }
    
    public String getData()
    {
      StringBuffer monthString = new StringBuffer();
      for (int i = 0; i < 12; ++i)
      {
        monthString.append(getData(fname + i).toUpperCase());
      }
      return monthString.toString();
    }
    
    protected String processData(String rawData)
    {
      String monthString = rawData.toLowerCase();
      for (int i = 0; i < 12; ++i)
      {
        if (rawData.length() > i)
        {
          setData(fname + i,
            (monthString.charAt(i) == 'y' ? "y" : "n"));
        }
      }
      return rawData;
    }

  }

  /*************************************************************************
  **
  **   Custom FeeSet
  **
  **************************************************************************/
  
  public class MyFeeSet extends FeeSet
  {
    public static final int IT_EMS_SERVICES = 4;
    public static final int IT_EMS_SUPPLIES = 5;
    
    protected void initFeeParameterDefs()
    {
      super.initFeeParameterDefs();
      parmDefs.add(new FeeParameterDef("equiptype",FPDT_INT));
    }
    
    public MyFeeSet(UserBean user, long appSeqNum)
    {
      super(user,appSeqNum);
    }
    
    protected void initRuleVars()
    {
      super.initRuleVars();
      
      ResultSetIterator it = null;

      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:989^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  equipsply_servicetype
//            from    app_merch_bbt
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equipsply_servicetype\n          from    app_merch_bbt\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.app.bbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:994^9*/
        ResultSet rs = it.getResultSet();
        boolean usesEmsServices = false;
        boolean usesEmsSupplies = false;
        if (rs.next())
        {
          String supplyServices = rs.getString("equipsply_servicetype")
            .toLowerCase();
          
          usesEmsServices = supplyServices.equals("ems services");
          usesEmsSupplies = supplyServices.equals("ems supplies only");
        }
        ruleVars.put("usesemsservices",(usesEmsServices ? "true" : "false"));
        ruleVars.put("usesemssupplies",(usesEmsSupplies ? "true" : "false"));
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + "::initRuleVars(): "
          + e.toString());
        logEntry("initRuleVars()",e.toString());
      }
      finally
      {
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
    
    public class EmsFee extends FeeSet.Fee
    {
      public EmsFee(ResultSet rs)
      {
        super(rs);
        
        String equipType  = null;
        String quantity   = null;
        
        try
        {
          equipType = rs.getString("equip_type");
          if (equipType == null)
          {
            throw new Exception("Missing equipType in result set");
          }
          parms.add(new FeeParameter("equiptype=" + equipType));
          
          quantity = rs.getString("quantity");
          if (quantity == null)
          {
            throw new Exception("Missing quantity in result set");
          }
          parms.add(new FeeParameter("quantity=" + quantity));
        }
        catch (Exception e)
        {
          String eMsg = "error creating EmsFee (" + toString() + ") - "
            + e.toString();
          System.out.println(this.getClass().getName() + "::EmsFee constructor: "
            + eMsg);
          logEntry("EmsFee constructor",eMsg);
        }
      }
        
      public int getEquipType()
      {
        return getParm("equipType").asInt();
      }
      public int getQuantity()
      {
        return getParm("quantity").asInt();
      }
      
      protected String getFieldName()
      {
        return "emsFee_" + getParm("equiptype").asString();
      }
    }
    
    protected void initCustomFees()
    {
      ResultSetIterator it = null;

      try
      {
        connect();
        
        // load EMS fee items
        /*@lineinfo:generated-code*//*@lineinfo:1081^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.equip_type           equip_type,
//                    ei.quantity             quantity,
//                    fi.label                label,
//                    fi.set_id               set_id,
//                    fi.item_id              item_id,
//                    fi.item_type            item_type,
//                    fi.disp_cat             disp_cat,
//                    fi.parms                parms,
//                    fi.allow_rules          allow_rules
//  
//            from    ( select    equiptype_code                    equip_type,
//                                sum( merchequip_equip_quantity )  quantity
//                      from      merchequipment
//                      where     app_seq_num = :appSeqNum
//                      group by  equiptype_code
//                    ) ei,
//                    ( select  afs.id                  set_id,
//                              afi.id                  item_id,
//                              afi.type_id             item_type,
//                              afi.label               label,
//                              afi.disp_cat            disp_cat,
//                              afi.parms               parms,
//                              afi.allow_rules         allow_rules
//                            
//                      from    appo_fee_sets   afs,
//                              appo_fee_items  afi
//                            
//                      where   afs.id = :setId
//                              and afs.item_id = afi.id
//                              and afi.type_id = :IT_EMS_SERVICES
//                    ) fi,
//                    appo_bbt_ems_links lnk
//                    
//            where   ei.equip_type = lnk.equip_type
//                    and lnk.set_id = fi.set_id
//                    and lnk.item_id = fi.item_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.equip_type           equip_type,\n                  ei.quantity             quantity,\n                  fi.label                label,\n                  fi.set_id               set_id,\n                  fi.item_id              item_id,\n                  fi.item_type            item_type,\n                  fi.disp_cat             disp_cat,\n                  fi.parms                parms,\n                  fi.allow_rules          allow_rules\n\n          from    ( select    equiptype_code                    equip_type,\n                              sum( merchequip_equip_quantity )  quantity\n                    from      merchequipment\n                    where     app_seq_num =  :1 \n                    group by  equiptype_code\n                  ) ei,\n                  ( select  afs.id                  set_id,\n                            afi.id                  item_id,\n                            afi.type_id             item_type,\n                            afi.label               label,\n                            afi.disp_cat            disp_cat,\n                            afi.parms               parms,\n                            afi.allow_rules         allow_rules\n                          \n                    from    appo_fee_sets   afs,\n                            appo_fee_items  afi\n                          \n                    where   afs.id =  :2 \n                            and afs.item_id = afi.id\n                            and afi.type_id =  :3 \n                  ) fi,\n                  appo_bbt_ems_links lnk\n                  \n          where   ei.equip_type = lnk.equip_type\n                  and lnk.set_id = fi.set_id\n                  and lnk.item_id = fi.item_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,setId);
   __sJT_st.setInt(3,IT_EMS_SERVICES);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.app.bbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1119^9*/
        ResultSet rs = it.getResultSet();
        while (rs.next())
        {
          Fee fee = new EmsFee(rs);
          if (fee.isAllowed())
          {
            fees.add(fee);
            fields.add(fee.getField());
          }
        }
        it.close();

        //supplies always has a quantity of 1
        /*@lineinfo:generated-code*//*@lineinfo:1133^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  -1                      equip_type,
//                    1                       quantity,
//                    ei.quantity             real_quantity,
//                    fi.label                label,
//                    fi.set_id               set_id,
//                    fi.item_id              item_id,
//                    fi.item_type            item_type,
//                    fi.disp_cat             disp_cat,
//                    fi.parms                parms,
//                    fi.allow_rules          allow_rules
//  
//            from    ( select    sum( merchequip_equip_quantity )  quantity
//                      from      merchequipment
//                      where     app_seq_num = :appSeqNum
//                      group by  app_seq_num
//                    ) ei,
//                    ( select  afs.id                  set_id,
//                              afi.id                  item_id,
//                              afi.type_id             item_type,
//                              afi.label               label,
//                              afi.disp_cat            disp_cat,
//                              afi.parms               parms,
//                              afi.allow_rules         allow_rules
//                            
//                      from    appo_fee_sets   afs,
//                              appo_fee_items  afi
//                            
//                      where   afs.id = :setId
//                              and afs.item_id = afi.id
//                              and afi.type_id = :IT_EMS_SUPPLIES
//                    ) fi
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  -1                      equip_type,\n                  1                       quantity,\n                  ei.quantity             real_quantity,\n                  fi.label                label,\n                  fi.set_id               set_id,\n                  fi.item_id              item_id,\n                  fi.item_type            item_type,\n                  fi.disp_cat             disp_cat,\n                  fi.parms                parms,\n                  fi.allow_rules          allow_rules\n\n          from    ( select    sum( merchequip_equip_quantity )  quantity\n                    from      merchequipment\n                    where     app_seq_num =  :1 \n                    group by  app_seq_num\n                  ) ei,\n                  ( select  afs.id                  set_id,\n                            afi.id                  item_id,\n                            afi.type_id             item_type,\n                            afi.label               label,\n                            afi.disp_cat            disp_cat,\n                            afi.parms               parms,\n                            afi.allow_rules         allow_rules\n                          \n                    from    appo_fee_sets   afs,\n                            appo_fee_items  afi\n                          \n                    where   afs.id =  :2 \n                            and afs.item_id = afi.id\n                            and afi.type_id =  :3 \n                  ) fi";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,setId);
   __sJT_st.setInt(3,IT_EMS_SUPPLIES);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.app.bbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1166^9*/
        rs = it.getResultSet();
        while (rs.next())
        {
          Fee fee = new EmsFee(rs);
          if (fee.isAllowed())
          {
            fees.add(fee);
            fields.add(fee.getField());
          }
        }
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + "::initCustomFees(): "
          + e.toString());
        logEntry("initCustomFees()",e.toString());
        e.printStackTrace();
      }
      finally
      {
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
    
    public static final int DC_EMS = 4;
    
    public Fee[] getEmsFee(int equipType)
    {
      return getCatFees(DC_EMS);
    }
    
    public EmsFee[] getEmsFees()
    {
      return (EmsFee[])getFeeSubset(EmsFee.class).toArray(new EmsFee[0]);
    }
    
    /*
    ** private void loadEmsFee(int itemId, double feeAmount)
    **
    ** Finds the fee item with the given item id and sets it's field data
    ** to the given fee amount.
    */
    private void loadEmsFee(int itemId, double feeAmount)
    {
      if (feeAmount >= 0)
      {
        Fee fee = getFee(itemId);
        if (fee != null)
        {
          fee.getField().setData(Double.toString(feeAmount));
        }
      }
    }
  
    /*
    ** private void loadEmsFees()
    **
    ** Loads ems fees.
    */
    private void loadEmsFees()
    {
      ResultSetIterator it = null;
    
      try
      {
        // get all ems fee recs for this app
        /*@lineinfo:generated-code*//*@lineinfo:1234^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  item_id,
//                    amount
//            from    appo_fees
//            where   app_seq_num = :appSeqNum
//                    and set_id = :setId
//                    and item_id in  ( select  item_id
//                                      from    appo_fee_items
//                                      where   set_id = :setId
//                                              and ( type_id = :IT_EMS_SERVICES
//                                                    or type_id = :IT_EMS_SUPPLIES ) )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  item_id,\n                  amount\n          from    appo_fees\n          where   app_seq_num =  :1 \n                  and set_id =  :2 \n                  and item_id in  ( select  item_id\n                                    from    appo_fee_items\n                                    where   set_id =  :3 \n                                            and ( type_id =  :4 \n                                                  or type_id =  :5  ) )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,setId);
   __sJT_st.setInt(3,setId);
   __sJT_st.setInt(4,IT_EMS_SERVICES);
   __sJT_st.setInt(5,IT_EMS_SUPPLIES);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.app.bbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1246^9*/
      
        // process each record, handle according to card type
        ResultSet rs = it.getResultSet();
        while (rs.next())
        {
          loadEmsFee(rs.getInt("item_id"),rs.getDouble("amount"));
        }
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + "::loadEmsFees(): "
          + e.toString());
        logEntry("loadEmsFees()",e.toString());
      }
      finally
      {
        try { it.close(); } catch (Exception ce) {}
      }
    }
    protected void loadCustomFees()
    {
      loadEmsFees();
    }

    /*
    ** private void submitEmsFees()
    **
    ** Stores ems fee amounts in the appo_fees table.
    */
    private void submitEmsFees()
    {
      try
      {
        // clear all ems fees
        /*@lineinfo:generated-code*//*@lineinfo:1281^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    appo_fees
//            where   app_seq_num = :appSeqNum
//                    and set_id = :setId
//                    and item_id in  ( select  item_id
//                                      from    appo_fee_items
//                                      where   set_id = :setId
//                                              and ( type_id = :IT_EMS_SERVICES
//                                                    or type_id = :IT_EMS_SUPPLIES ) )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    appo_fees\n          where   app_seq_num =  :1 \n                  and set_id =  :2 \n                  and item_id in  ( select  item_id\n                                    from    appo_fee_items\n                                    where   set_id =  :3 \n                                            and ( type_id =  :4 \n                                                  or type_id =  :5  ) )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,setId);
   __sJT_st.setInt(3,setId);
   __sJT_st.setInt(4,IT_EMS_SERVICES);
   __sJT_st.setInt(5,IT_EMS_SUPPLIES);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1292^9*/
      
        // store all non-zero ems fees
        EmsFee[] emsFees = getEmsFees();
        for (int i = 0; i < emsFees.length; ++i)
        {
          Field feeField = emsFees[i].getField();
          double feeAmount = feeField.asDouble();
          if (feeAmount >= 0)
          {
            int itemId      = emsFees[i].getItemId();
            int feeMultiple = emsFees[i].getQuantity();
            /*@lineinfo:generated-code*//*@lineinfo:1304^13*/

//  ************************************************************
//  #sql [Ctx] { insert into appo_fees
//                ( app_seq_num,
//                  set_id,
//                  item_id,
//                  misc_code,
//                  amount,
//                  quantity )
//                values
//                ( :appSeqNum,
//                  :setId,
//                  :itemId,
//                  : mesConstants.APP_MISC_CHARGE_BBT_EMS_FEE,
//                  :feeAmount,
//                  :feeMultiple )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into appo_fees\n              ( app_seq_num,\n                set_id,\n                item_id,\n                misc_code,\n                amount,\n                quantity )\n              values\n              (  :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                 :5 ,\n                 :6  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,setId);
   __sJT_st.setInt(3,itemId);
   __sJT_st.setInt(4, mesConstants.APP_MISC_CHARGE_BBT_EMS_FEE);
   __sJT_st.setDouble(5,feeAmount);
   __sJT_st.setInt(6,feeMultiple);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1320^13*/
          }
        }
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + "::submitEmsFees(): "
          + e.toString());
        logEntry("submitEmsFees()",e.toString());
      }
    }
    protected void submitCustomFees()
    {
      submitEmsFees();
    }
  }
  
  /*************************************************************************
  **
  **   Field Bean
  **
  **************************************************************************/
  
  protected FeeSet createFeeSet(long appSeqNum)
  {
    return new MyFeeSet(user,appSeqNum);
  }
  
  private int busNature = -1;
  
  public int getBusNature()
  {
    if (busNature < 0)
    {
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:1358^9*/

//  ************************************************************
//  #sql [Ctx] { select  business_nature
//            
//            from    merchant
//            where   app_seq_num = :fields.getField("appSeqNum").asLong()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_2583 = fields.getField("appSeqNum").asLong();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  business_nature\n           \n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.bbt.Pricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_2583);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   busNature = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1364^9*/
      }
      catch( Exception e )
      {
        System.out.println(this.getClass().getName() + "::getBusNature(): "
          + e.toString());
        e.printStackTrace();
        logEntry("getBusNature()",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    return busNature;
  }
  
  private int paymentMethod = -1;
  
  /*
  ** public int getPaymentMethod()
  **
  ** Loads the merchant's payment method.  This is the same as their product
  ** type (which is a pos_type code in the pos_category table, referred to
  ** indirectly by the pos_code in the merch_pos table).
  **
  ** RETURNS: payment method.
  */
  public int getPaymentMethod()
  {
    if (paymentMethod < 0)
    {
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:1400^9*/

//  ************************************************************
//  #sql [Ctx] { select  pc.pos_type
//            
//            from    merch_pos     mp,
//                    pos_category  pc
//            where   mp.app_seq_num = :fields.getField("appSeqNum").asLong() and
//                    pc.pos_code = mp.pos_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_2584 = fields.getField("appSeqNum").asLong();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pc.pos_type\n           \n          from    merch_pos     mp,\n                  pos_category  pc\n          where   mp.app_seq_num =  :1  and\n                  pc.pos_code = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.app.bbt.Pricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_2584);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   paymentMethod = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1408^9*/
      }
      catch( Exception e )
      {
        System.out.println(this.getClass().getName() + "::getPaymentMethod(): "
          + e.toString());
        e.printStackTrace();
        logEntry("getPaymentMethod()",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    return paymentMethod;
  }

  /*
  ** protected void createPricingFields()
  **
  ** Creates various vmc pricing field sets according to certain options 
  ** selected on prior pages (like nature of business and payment method
  ** method type on the business page).
  */
  protected void createPricingFields()
  {
    try
    {
      FieldGroup gPricing = new FieldGroup("gPricing");
      
      // add a checkbox to indicate unrolled
      CheckboxField unrolledField = new CheckboxField("unrolledPricing","Unrolled Pricing",false);
      gPricing.add(unrolledField);

      // create the pricing type mapper fields
      gPricing.add(new BetSetMapperField());
      gPricing.add(new DiscRateMapperField());
      gPricing.add(new PerItemMapperField());
      gPricing.add(new DiscRateCheckMapperField());
      gPricing.add(new PerItemCheckMapperField());
      
      // if dial pay only standard pricing is allowed
      if (getPaymentMethod() == mesConstants.POS_DIAL_AUTH)
      {
        // make betType hidden and fixed on standard (so mappers will work)
        Field betType = new HiddenField("betType",
          Integer.toString(mesConstants.BBT_STANDARD_PRICING_PLAN));
        gPricing.add(betType);

        // standard pricing
        Field betSet21 = new DropDownField ("betSet_21",   new StandardPricingTable(),false);
        Field stdRate  = new DiscountField ("discRate_21", false);
        Field stdRateCheck = new DiscountField ("discRateCheck_21", true);
        stdRate.addValidation(new RateValidation(ICBET_TYPE_STD));
        stdRateCheck.addValidation(new UnrolledRateValidation(ICBET_TYPE_STD,unrolledField));

        // set default if found
        if (stdPricingDefault != null)
        {
          betSet21.setData(stdPricingDefault);
        }

        gPricing.add(betSet21);
        gPricing.add(stdRate);
        gPricing.add(stdRateCheck);
        gPricing.add(new SmallCurrencyField("perItem_21",5,6,true));
        gPricing.add(new SmallCurrencyField("perItemCheck_21",5,6,true));
        
        Condition is21 = new FieldValueCondition(betType,Integer.toString(ICBET_TYPE_STD));
        betSet21.setOptionalCondition(is21);
        stdRate.setOptionalCondition(is21);
      }
      // 4 - Gov't/Purchasing card (only allows Interchange Plus)
      else if (getBusNature() == 4) 
      {
        /*
        // make betType hidden and fixed on ic plus (so mappers will work)
        Field betType = new HiddenField("betType",
          Integer.toString(mesConstants.BBT_INTERCHANGE_PLUS_PLAN));
        gPricing.add(betType);

        // interchange plus
        Field betSet22  = new HiddenField   ("betSet_22",   "1000");
        Field intRate   = new DiscountField ("discRate_22", false);
        intRate.addValidation(new RateValidation(ICBET_TYPE_IC));

        gPricing.add(betSet22);
        gPricing.add(intRate);
        gPricing.add(new SmallCurrencyField ("perItem_22",5,6,true));
      
        Condition is22 = new FieldValueCondition(betType,Integer.toString(ICBET_TYPE_IC));
        betSet22.setOptionalCondition(is22);
        intRate.setOptionalCondition(is22);
        */


        // overall pricing type radio button field
        Field betType = new RadioButtonField("betType",pricingTypeList,-1,false,"A pricing type must be selected");
        gPricing.add(betType);

        // standard pricing
        Field betSet21      = new DropDownField ("betSet_21",   new StandardPricingTable(),false);
        Field stdRate       = new DiscountField ("discRate_21", false);
        Field stdRateCheck  = new DiscountField ("discRateCheck_21", false);
        stdRate.addValidation(new RateValidation(ICBET_TYPE_STD));
        stdRateCheck.addValidation(new UnrolledRateValidation(ICBET_TYPE_STD,unrolledField));
        
        // set default if found
        if (stdPricingDefault != null)
        {
          betSet21.setData(stdPricingDefault);
        }

        gPricing.add(betSet21);
        gPricing.add(stdRate);
        gPricing.add(stdRateCheck);
        gPricing.add(new SmallCurrencyField("perItem_21",5,6,true));
        gPricing.add(new SmallCurrencyField("perItemCheck_21",5,6,true));
        
        Condition is21 = new FieldValueCondition(betType,Integer.toString(ICBET_TYPE_STD));
        betSet21.setOptionalCondition(is21);
        stdRate.setOptionalCondition(is21);

        // interchange plus
        Field betSet22     = new DropDownField ("betSet_22",   new InterchangePlusTable(),false);
        Field intRate      = new DiscountField ("discRate_22", false);
        Field intRateCheck = new DiscountField ("discRateCheck_22", true);
        intRate.addValidation(new RateValidation(ICBET_TYPE_IC));
        intRateCheck.addValidation(new UnrolledRateValidation(ICBET_TYPE_IC,unrolledField));

        // set default if found
        if (interchangePlusDefault != null)
        {
          betSet22.setData(interchangePlusDefault);
        }

      
        gPricing.add(betSet22);
        gPricing.add(intRate);
        gPricing.add(intRateCheck);
        gPricing.add(new SmallCurrencyField ("perItem_22",5,6,true));
        gPricing.add(new SmallCurrencyField ("perItemCheck_22",5,6,true));
      
        Condition is22 = new FieldValueCondition(betType,Integer.toString(ICBET_TYPE_IC));
        betSet22.setOptionalCondition(is22);
        intRate.setOptionalCondition(is22);
      }
      // anything else allows all pricing options (standard, ic plus, tiered)
      else
      {
        // overall pricing type radio button field
        Field betType = new RadioButtonField("betType",pricingTypeList,-1,false,"A pricing type must be selected");
        gPricing.add(betType);

        // standard pricing
        Field betSet21     = new DropDownField ("betSet_21",   new StandardPricingTable(),false);
        Field stdRate      = new DiscountField ("discRate_21", false);
        Field stdRateCheck = new DiscountField ("discRateCheck_21", true);
        stdRate.addValidation(new RateValidation(ICBET_TYPE_STD));
        stdRateCheck.addValidation(new UnrolledRateValidation(ICBET_TYPE_STD,unrolledField));
        
        // set default if found
        if (stdPricingDefault != null)
        {
          betSet21.setData(stdPricingDefault);
        }

        gPricing.add(betSet21);
        gPricing.add(stdRate);
        gPricing.add(stdRateCheck);
        gPricing.add(new SmallCurrencyField("perItem_21",5,6,true));
        gPricing.add(new SmallCurrencyField("perItemCheck_21",5,6,true));
        
        Condition is21 = new FieldValueCondition(betType,Integer.toString(ICBET_TYPE_STD));
        betSet21.setOptionalCondition(is21);
        stdRate.setOptionalCondition(is21);

        // interchange plus
        Field betSet22     = new DropDownField ("betSet_22",   new InterchangePlusTable(),false);
        Field intRate      = new DiscountField ("discRate_22", false);
        Field intRateCheck = new DiscountField ("discRateCheck_22", true);
        intRate.addValidation(new RateValidation(ICBET_TYPE_IC));
        intRateCheck.addValidation(new UnrolledRateValidation(ICBET_TYPE_IC,unrolledField));

        // set default if found
        if (interchangePlusDefault != null)
        {
          betSet22.setData(interchangePlusDefault);
        }

        gPricing.add(betSet22);
        gPricing.add(intRate);
        gPricing.add(intRateCheck);
        gPricing.add(new SmallCurrencyField ("perItem_22",5,6,true));
        gPricing.add(new SmallCurrencyField ("perItemCheck_22",5,6,true));
      
        Condition is22 = new FieldValueCondition(betType,Integer.toString(ICBET_TYPE_IC));
        betSet22.setOptionalCondition(is22);
        intRate.setOptionalCondition(is22);

        // tiered pricing
        Field betSet23    = new DropDownField ("betSet_23",   new TieredPricingTable(),false);
        Field otherVsBet  = new Field         ("vsOtherBet",  4,6,false);
        Field otherMcBet  = new Field         ("mcOtherBet",  4,6,false);
        
        // set default if found
        if (tierPricingDefault != null)
        {
          betSet23.setData(tierPricingDefault);
        }

        Field tierRate      = new DiscountField ("discRate_23", false);
        Field tierRateCheck = new DiscountField ("discRateCheck_23", true);
        tierRate.addValidation(new RateValidation(ICBET_TYPE_TIER));
        tierRateCheck.addValidation(new UnrolledRateValidation(ICBET_TYPE_TIER,unrolledField));

        gPricing.add(tierRate);
        gPricing.add(tierRateCheck);
        gPricing.add(new SmallCurrencyField ("perItem_23",5,6,true));
        gPricing.add(new SmallCurrencyField ("perItemCheck_23",5,6,true));


        gPricing.add(betSet23);
        gPricing.add(otherVsBet);
        gPricing.add(otherMcBet);
        
        Condition is23 = new FieldValueCondition(betType,Integer.toString(ICBET_TYPE_TIER));
        betSet23.setOptionalCondition(is23);
        tierRate.setOptionalCondition(is23);
        
        AndCondition isOther = new AndCondition();
        isOther.add(new FieldValueCondition(betSet23,"999999"));
        isOther.add(is23);
        otherVsBet.setOptionalCondition(isOther);
        otherMcBet.setOptionalCondition(isOther);
      }
      
      fields.add(gPricing);
      
      // bb&t will treat pricing plan as being synonymous with bet type, so
      // make pricingPlan an alias of betType
      fields.addAlias("betType","pricingPlan");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createPricingFields(): "
        + e.toString());
      e.printStackTrace();
      logEntry("createPricingFields()",e.toString());
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      connect();
      
      // add pricing bb&t pricing fields
      createPricingFields();
      
      FieldGroup gDebitFees = new FieldGroup  ("gDebitFees");
      // add a checkbox for unrolled debit      
      CheckboxField unrolledField = new CheckboxField("useDebitBrg","Use Billing Rate Group (unrolled)",false);
      gDebitFees.add(unrolledField);
      fields.add(gDebitFees);
      
      Fee[] tranFees = getFeeSet().getTranCatFees();
      for (int i = 0; i < tranFees.length; ++i)
      {
        if ( !((TranFee)tranFees[i]).isDebit() )
        {
          continue;
        }
        
        Field field = tranFees[i].getField();
        field.addValidation( new DebitBRGValidation( unrolledField, ((TranFee)tranFees[i]).getTranFeeType() ) );
      }        
      
      // add any fees not supported by FeeSet
      FieldGroup gOddFees = new FieldGroup  ("gOddFees");

      Field miscFeeDesc1        = new Field             ("miscFeeDesc1",  40,40,true);
      Field miscFeeAmt1         = new CurrencyField     ("miscFeeAmt1",  8,8,true);

      miscFeeAmt1.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc1,miscFeeAmt1, "Please provide the amount of this misc charge"));
      miscFeeDesc1.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc1,miscFeeAmt1, "Please provide the description of this misc charge"));
      gOddFees.add(miscFeeDesc1);
      gOddFees.add(miscFeeAmt1);

      Field miscFeeChrgBasis1   = new RadioButtonField  ("miscFeeChrgBasis1", chargeBasisTypeList,-1,true,"Required");
      miscFeeChrgBasis1.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc1,miscFeeAmt1, "Please select the charge basis of this misc charge"));
      gOddFees.add(miscFeeChrgBasis1);

      Field miscFeeStartDate1   = new Field             ("miscFeeStartDate1",8,8,true);
      miscFeeStartDate1.addValidation(new ValidDateValidation(false, miscFeeStartDate1));
      miscFeeStartDate1.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc1,miscFeeAmt1, "Please provide the start date of this misc charge"));
      gOddFees.add(miscFeeStartDate1);

      Field miscFeeEndDate1     = new Field             ("miscFeeEndDate1",8,8,true);
      miscFeeEndDate1.addValidation(new ValidDateValidation(true, miscFeeEndDate1));
      miscFeeEndDate1.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc1,miscFeeAmt1, "Please provide the end date of this misc charge"));
      gOddFees.add(miscFeeEndDate1);

      Field miscFeeBillFreq1    = new SeasonalFieldGroup("miscFeeBillFreq1");
      miscFeeBillFreq1.addValidation(new IfNotBlankNotAllNValidation(miscFeeDesc1,miscFeeAmt1, "Please select the billing frequency for this misc charge"));
      gOddFees.add(miscFeeBillFreq1);

      Field miscFeeChrgType1    = new DropDownField("miscFeeChrgType1",  new MiscChargeTypeTable(),true);   
      gOddFees.add(miscFeeChrgType1);


      Field miscFeeDesc2        = new Field             ("miscFeeDesc2",  40,40,true);
      Field miscFeeAmt2         = new CurrencyField     ("miscFeeAmt2",  8,8,true);
      miscFeeAmt2.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc2,miscFeeAmt2, "Please provide the amount of this misc charge"));
      miscFeeDesc2.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc2,miscFeeAmt2, "Please provide the description of this misc charge"));

      gOddFees.add(miscFeeAmt2);
      gOddFees.add(miscFeeDesc2);

      Field miscFeeChrgBasis2   = new RadioButtonField  ("miscFeeChrgBasis2", chargeBasisTypeList,-1,true,"Required");
      miscFeeChrgBasis2.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc2,miscFeeAmt2, "Please select the charge basis of this misc charge"));
      gOddFees.add(miscFeeChrgBasis2);

      Field miscFeeStartDate2   = new Field             ("miscFeeStartDate2",8,8,true);
      miscFeeStartDate2.addValidation(new ValidDateValidation(false,miscFeeStartDate2));
      miscFeeStartDate2.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc2,miscFeeAmt2, "Please provide the start date of this misc charge"));
      gOddFees.add(miscFeeStartDate2);

      Field miscFeeEndDate2     = new Field             ("miscFeeEndDate2",8,8,true);
      miscFeeEndDate2.addValidation(new ValidDateValidation(true,miscFeeEndDate2));
      miscFeeEndDate2.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc2,miscFeeAmt2, "Please provide the end date of this misc charge"));
      gOddFees.add(miscFeeEndDate2);

      Field miscFeeBillFreq2    = new SeasonalFieldGroup("miscFeeBillFreq2");
      miscFeeBillFreq2.addValidation(new IfNotBlankNotAllNValidation(miscFeeDesc2,miscFeeAmt2, "Please select the billing frequency for this misc charge"));
      gOddFees.add(miscFeeBillFreq2);

      Field miscFeeChrgType2    = new DropDownField("miscFeeChrgType2",  new MiscChargeTypeTable(),true);   
      gOddFees.add(miscFeeChrgType2);
      

      Field miscFeeDesc3        = new Field             ("miscFeeDesc3",  40,40,true);
      Field miscFeeAmt3         = new CurrencyField     ("miscFeeAmt3",  8,8,true);

      miscFeeAmt3.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc3,miscFeeAmt3, "Please provide the amount of this misc charge"));
      miscFeeDesc3.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc3,miscFeeAmt3, "Please provide the description of this misc charge"));
      gOddFees.add(miscFeeDesc3);
      gOddFees.add(miscFeeAmt3);

      Field miscFeeChrgBasis3   = new RadioButtonField  ("miscFeeChrgBasis3", chargeBasisTypeList,-1,true,"Required");
      miscFeeChrgBasis3.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc3,miscFeeAmt3, "Please select the charge basis of this misc charge"));
      gOddFees.add(miscFeeChrgBasis3);

      Field miscFeeStartDate3   = new Field             ("miscFeeStartDate3",8,8,true);
      miscFeeStartDate3.addValidation(new ValidDateValidation(false, miscFeeStartDate3));
      miscFeeStartDate3.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc3,miscFeeAmt3, "Please provide the start date of this misc charge"));
      gOddFees.add(miscFeeStartDate3);

      Field miscFeeEndDate3     = new Field             ("miscFeeEndDate3",8,8,true);
      miscFeeEndDate3.addValidation(new ValidDateValidation(true, miscFeeEndDate3));
      miscFeeEndDate3.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc3,miscFeeAmt3, "Please provide the end date of this misc charge"));
      gOddFees.add(miscFeeEndDate3);

      Field miscFeeBillFreq3    = new SeasonalFieldGroup("miscFeeBillFreq3");
      miscFeeBillFreq3.addValidation(new IfNotBlankNotAllNValidation(miscFeeDesc3,miscFeeAmt3, "Please select the billing frequency for this misc charge"));
      gOddFees.add(miscFeeBillFreq3);

      Field miscFeeChrgType3    = new DropDownField("miscFeeChrgType3",  new MiscChargeTypeTable(),true);   
      gOddFees.add(miscFeeChrgType3);


      Field miscFeeDesc4        = new Field             ("miscFeeDesc4",  40,40,true);
      Field miscFeeAmt4         = new CurrencyField     ("miscFeeAmt4",  8,8,true);
      miscFeeAmt4.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc4,miscFeeAmt4, "Please provide the amount of this misc charge"));
      miscFeeDesc4.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc4,miscFeeAmt4, "Please provide the description of this misc charge"));

      gOddFees.add(miscFeeAmt4);
      gOddFees.add(miscFeeDesc4);

      Field miscFeeChrgBasis4   = new RadioButtonField  ("miscFeeChrgBasis4", chargeBasisTypeList,-1,true,"Required");
      miscFeeChrgBasis4.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc4,miscFeeAmt4, "Please select the charge basis of this misc charge"));
      gOddFees.add(miscFeeChrgBasis4);

      Field miscFeeStartDate4   = new Field             ("miscFeeStartDate4",8,8,true);
      miscFeeStartDate4.addValidation(new ValidDateValidation(false,miscFeeStartDate4));
      miscFeeStartDate4.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc4,miscFeeAmt4, "Please provide the start date of this misc charge"));
      gOddFees.add(miscFeeStartDate4);

      Field miscFeeEndDate4     = new Field             ("miscFeeEndDate4",8,8,true);
      miscFeeEndDate4.addValidation(new ValidDateValidation(true,miscFeeEndDate4));
      miscFeeEndDate4.addValidation(new IfNotBlankNotBlankValidation(miscFeeDesc4,miscFeeAmt4, "Please provide the end date of this misc charge"));
      gOddFees.add(miscFeeEndDate4);

      Field miscFeeBillFreq4    = new SeasonalFieldGroup("miscFeeBillFreq4");
      miscFeeBillFreq4.addValidation(new IfNotBlankNotAllNValidation(miscFeeDesc4,miscFeeAmt4, "Please select the billing frequency for this misc charge"));
      gOddFees.add(miscFeeBillFreq4);

      Field miscFeeChrgType4    = new DropDownField("miscFeeChrgType4",  new MiscChargeTypeTable(),true);   
      gOddFees.add(miscFeeChrgType4);

      fields.add(gOddFees);
      
      // bb&t site inspection and underwriting fields
      fields.add(new DropDownField    ("buildingOwnLease",  new OwnLeaseTable(),true));
      fields.add(new DropDownField    ("locationType",      new LocationTypeTable(), !applyingForAmex()));
      fields.add(new DropDownField    ("locationFootage",   new LocationFootageTable(),true));
      fields.add(new DropDownField    ("locationFloor",     new LocationFloorTable(),true));

      fields.add(new DropDownField    ("employeeCount",     new EmployeeCountTable(),true));


      fields.add(new TextareaField    ("productDesc",       80,2,40,true));
      
      fields.add(new RadioButtonField ("wonOver",           yesNoRadioList,-1,true,"Required"));
      fields.add(new RadioButtonField ("haveCanceled",      yesNoRadioList,-1,true,"Required"));

      fields.add(new Field            ("canceledProcessor", 40,30,true));
      fields.getField("canceledProcessor").addValidation(
        new IfYesNotBlankValidation(fields.getField("haveCanceled"),
          "Please provide the name of the cancelling processor"));

      fields.add(new Field            ("canceledReason",    40,30,true));
      fields.getField("canceledReason").addValidation(
        new IfYesNotBlankValidation(fields.getField("haveCanceled"),
          "Please provide reason account was cancelled"));

      //fields.add(new DateStringField  ("canceledDate",      true));
      fields.add(new DateStringField  ("canceledDate", "Date of Cancellation",true,true));
      fields.getField("canceledDate").addValidation(
        new IfYesNotBlankValidation(fields.getField("haveCanceled"),
          "Please provide a date of cancellation"));

      fields.add(new Field            ("bbtRelationship",   40,30,true));

      fields.add(new NumberField      ("sicCode",           4,6,true,0));
      fields.add(new TextareaField    ("longProductDescription",1000,5,80,true));

//*******************************************************************************
      fields.add(new RadioButtonField ("isOpen",            yesNoRadioList,-1,true,"Required"));

      fields.add(new DateStringField  ("openDate",          true));
      fields.getField("openDate").addValidation(
        new IfNoNotBlankValidation(fields.getField("isOpen"),
          "Provide a date business will open"));
      
      fields.add(new DateStringField  ("establishedDate",   true));
      fields.getField("establishedDate").addValidation(
        new IfYesNotBlankValidation(fields.getField("isOpen"),
          "Provide date business opened"));

      fields.add(new CurrencyField    ("annualVolume",      11,9,true));
      fields.add(new CurrencyField    ("averageTicket",     11,9,true));
      
      fields.add(new NumberField      ("cpPercent",         3,3,true,0));
      fields.add(new NumberField      ("cnpPercent",        3,3,true,0));

      
      fields.getField("cpPercent").addValidation(
        new HundredPercentValidation(fields.getField("cnpPercent")));


//********************************************************************************
        
      fields.add(new NumberField      ("cpSwipePercent",    3,3,true,0));
      fields.add(new NumberField      ("cpImprintPercent",  3,3,true,0));
      
      fields.getField("cpSwipePercent").addValidation(
        new HundredPercentValidation(fields.getField("cpImprintPercent")));
        
      fields.add(new NumberField      ("cnpSwipePercent",   3,3,true,0));
      fields.add(new NumberField      ("cnpImprintPercent", 3,3,true,0));
      
      fields.getField("cnpSwipePercent").addValidation(
        new HundredPercentValidation(fields.getField("cnpImprintPercent")));
        
      fields.add(new RadioButtonField ("cnpTelemarketer",   yesNoRadioList,-1,true,"Required"));
      fields.add(new NumberField      ("b2bPercent",        3,3,true,0));
      fields.add(new RadioButtonField ("requiresPrepayment",yesNoRadioList,-1,true,"Required"));
      fields.add(new CurrencyField    ("prepaymentAmount",  11,9,true));
      fields.add(new NumberField      ("deliveryDays",      4,4,true,0));
      fields.add(new RadioButtonField ("recurringBilling",  yesNoRadioList,-1,true,"Required"));
      fields.add(new NumberField      ("foreignCardPercent",3,3,true,0));
      fields.add(new RadioButtonField ("wirelessTerminal",  yesNoRadioList,-1,true,"Required"));
      fields.add(new RadioButtonField ("dnsListMerchant",   yesNoRadioList,-1,true,"Required"));
      fields.add(new SeasonalFieldGroup("seasonalMonths"));
      fields.add(new EmailField       ("businessEmail",     "Business e-mail Address",75,50,true));
      
      //********************************************************************************
      // Risk/PCI fields
      //********************************************************************************
      boolean stageOnly = (getProductType() == mesConstants.POS_OTHER);
      
      // Does merchant have a website?
      fields.add(new RadioButtonField ("websiteInd",  yesNoRadioList,-1,true,"Required"));
      fields.add(new Field            ("webUrl",      75,50,true));
      fields.add(new Field            ("webUrl2",     75,50,true));
      fields.getField("webUrl")
        .addValidation(new IfYesNotBlankValidation(fields.getField("websiteInd"),
          "Website address is required"));
      
      // Does merchant use third party software to store, transmit, or process cardholder data?
      fields.add(new RadioButtonField ("thirdPartySwInd",  yesNoRadioList,-1,true,"Required"));
      fields.add(new Field            ("thirdPartyName",      75,50,true));
      fields.add(new Field            ("thirdPartySwName",    75,50,true));
      fields.add(new Field            ("thirdPartySwVer",     75,50,true));
      fields.getField("thirdPartyName")
        .addValidation(new IfYesNotBlankValidation(fields.getField("thirdPartySwInd"),
          "Must provide third party software vendor name") );
      fields.getField("thirdPartySwName")
        .addValidation(new IfYesNotBlankValidation(fields.getField("thirdPartySwInd"),
          "Must provide third party software name") );
      fields.getField("thirdPartySwVer")
        .addValidation(new IfYesNotBlankValidation(fields.getField("thirdPartySwInd"),
          "Must provide third party software version") );
      if( stageOnly )
      {
        setData("thirdPartySwInd","Y");
      }
          
      
      // Does merchant use any other third party agents to store, transmit, or process cardholder data?
      fields.add(new RadioButtonField ("thirdPartyAgentInd",  yesNoRadioList,-1,true,"Required"));
      fields.add(new Field            ("thirdPartyAgentName1",75,50,true));
      fields.add(new Field            ("thirdPartyAgentDesc1",75,50,true));
      fields.add(new Field            ("thirdPartyAgentName2",75,50,true));
      fields.add(new Field            ("thirdPartyAgentDesc2",75,50,true));
      fields.getField("thirdPartyAgentName1")
        .addValidation(new IfYesNotBlankValidation(fields.getField("thirdPartyAgentInd"),
          "Must provide third party name") );
      fields.getField("thirdPartyAgentDesc1")
        .addValidation(new IfYesNotBlankValidation(fields.getField("thirdPartyAgentInd"),
          "Must provide description of services provided") );
      
      // Internet only questions
      boolean internetBusiness = 
        ( (getBusinessNature() == mesConstants.BUSINESS_NATURE_INTERNET) ||
          (getProductType() == mesConstants.POS_INTERNET) );
      
      fields.add(new RadioButtonField ("internetSslCertInd",  yesNoRadioList,-1,!internetBusiness,"Required"));
      fields.add(new RadioButtonField ("internetDeliveryPolicyInd",  yesNoRadioList,-1,!internetBusiness,"Required"));
      fields.add(new RadioButtonField ("internetRefundPolicyInd",  yesNoRadioList,-1,!internetBusiness,"Required"));
      fields.add(new RadioButtonField ("internetServiceContactInd",  yesNoRadioList,-1,!internetBusiness,"Required"));
      fields.add(new RadioButtonField ("internetAddrOnWebsiteInd",  yesNoRadioList,-1,!internetBusiness,"Required"));
      fields.add(new RadioButtonField ("internetShipOutOfUsInd",  yesNoRadioList,-1,!internetBusiness,"Required"));

      // Risk validation
      fields.add(new NumberField      ("annualSalesCount"    ,10,7,false,0));
      fields.add(new RadioButtonField ("pciValRequiredInd",  yesNoRadioList,-1,false,"Required"));
      fields.add(new RadioButtonField ("pciCompliant",  yesNoRadioList,-1,false,"Required"));
      fields.add(new RadioButtonField ("paymentAppValConfInd",  yesNoRadioList,-1,false,"Required"));
      fields.add(new DropDownField    ( "pciVendor", "PCI Vendor", new PCIVendorTable(), true ));
      fields.add(new Field            ( "pciVendorOther",  "Other Vendor Name", 40,40, true ));
      fields.add(new DateField        ( "pciLastScanDate", "Last Scan Date", true ));
      fields.add(new DateField        ( "pciNextScanDate", "Next Scan Date", true ));
      
      fields.getField("pciVendorOther").addValidation(new PCIOtherValidation(fields.getField("pciVendor")));
      ((DateField)fields.getField("pciLastScanDate")).setDateFormat("MM/dd/yyyy");
      ((DateField)fields.getField("pciNextScanDate")).setDateFormat("MM/dd/yyyy");
      
      // risk comments
      fields.add(new TextareaField    ("underwritingComments",   400,2,80,true));
      
      // set the field style class
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      e.printStackTrace();
      logEntry("createFields()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*************************************************************************
  **
  **   Load
  **
  **************************************************************************/

  /*
  ** protected void loadOddFees(long appSeqNum) throws Exception
  **
  ** Load any fees not handled by the fee set.
  */
  protected void loadOddFees(long appSeqNum) throws Exception
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      //load Miscellaneous Charges
      /*@lineinfo:generated-code*//*@lineinfo:2008^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  misc_fee_desc1,
//                  misc_fee_amt1,
//                  misc_fee_chrg_basis1,
//                  misc_fee_start_date1,
//                  misc_fee_end_date1,
//                  misc_fee_bill_freq1,
//                  misc_fee_chrg_type1,
//                  misc_fee_desc2,
//                  misc_fee_amt2,
//                  misc_fee_chrg_basis2,
//                  misc_fee_start_date2,
//                  misc_fee_end_date2,
//                  misc_fee_bill_freq2,
//                  misc_fee_chrg_type2,
//                  misc_fee_desc3,
//                  misc_fee_amt3,
//                  misc_fee_chrg_basis3,
//                  misc_fee_start_date3,
//                  misc_fee_end_date3,
//                  misc_fee_bill_freq3,
//                  misc_fee_chrg_type3,
//                  misc_fee_desc4,
//                  misc_fee_amt4,
//                  misc_fee_chrg_basis4,
//                  misc_fee_start_date4,
//                  misc_fee_end_date4,
//                  misc_fee_bill_freq4,
//                  misc_fee_chrg_type4,
//                  bbt.website_ind                       as website_ind,
//                  bbt.third_party_sw_ind                as third_party_sw_ind,
//                  bbt.third_party_agent_ind             as third_party_agent_ind,
//                  bbt.internet_ssl_cert_ind             as internet_ssl_cert_ind,
//                  bbt.internet_delivery_policy_ind      as internet_delivery_policy_ind,
//                  bbt.internet_refund_policy_ind        as internet_refund_policy_ind,
//                  bbt.internet_service_contact_ind      as internet_service_contact_ind,
//                  bbt.internet_addr_on_website_ind      as internet_addr_on_website_ind,
//                  bbt.internet_ship_out_of_us_ind       as internet_ship_out_of_us_ind,
//                  bbt.pci_val_required_ind              as pci_val_required_ind,
//                  bbt.payment_app_val_conf_ind          as payment_app_val_conf_ind,
//                  bbt.merch_web_url2                    as web_url2,
//                  bbt.third_party_name                  as third_party_name,
//                  bbt.third_party_sw_name               as third_party_sw_name,
//                  bbt.third_party_sw_ver                as third_party_sw_ver,
//                  bbt.third_party_agent_name_1          as third_party_agent_name_1,
//                  bbt.third_party_agent_desc_1          as third_party_agent_desc_1,
//                  bbt.third_party_agent_name_2          as third_party_agent_name_2,
//                  bbt.third_party_agent_desc_2          as third_party_agent_desc_2,
//                  bbt.annual_sales_count                as annual_sales_count,
//                  bbt.underwriting_comment              as underwriting_comments
//          from    app_merch_bbt   bbt
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  misc_fee_desc1,\n                misc_fee_amt1,\n                misc_fee_chrg_basis1,\n                misc_fee_start_date1,\n                misc_fee_end_date1,\n                misc_fee_bill_freq1,\n                misc_fee_chrg_type1,\n                misc_fee_desc2,\n                misc_fee_amt2,\n                misc_fee_chrg_basis2,\n                misc_fee_start_date2,\n                misc_fee_end_date2,\n                misc_fee_bill_freq2,\n                misc_fee_chrg_type2,\n                misc_fee_desc3,\n                misc_fee_amt3,\n                misc_fee_chrg_basis3,\n                misc_fee_start_date3,\n                misc_fee_end_date3,\n                misc_fee_bill_freq3,\n                misc_fee_chrg_type3,\n                misc_fee_desc4,\n                misc_fee_amt4,\n                misc_fee_chrg_basis4,\n                misc_fee_start_date4,\n                misc_fee_end_date4,\n                misc_fee_bill_freq4,\n                misc_fee_chrg_type4,\n                bbt.website_ind                       as website_ind,\n                bbt.third_party_sw_ind                as third_party_sw_ind,\n                bbt.third_party_agent_ind             as third_party_agent_ind,\n                bbt.internet_ssl_cert_ind             as internet_ssl_cert_ind,\n                bbt.internet_delivery_policy_ind      as internet_delivery_policy_ind,\n                bbt.internet_refund_policy_ind        as internet_refund_policy_ind,\n                bbt.internet_service_contact_ind      as internet_service_contact_ind,\n                bbt.internet_addr_on_website_ind      as internet_addr_on_website_ind,\n                bbt.internet_ship_out_of_us_ind       as internet_ship_out_of_us_ind,\n                bbt.pci_val_required_ind              as pci_val_required_ind,\n                bbt.payment_app_val_conf_ind          as payment_app_val_conf_ind,\n                bbt.merch_web_url2                    as web_url2,\n                bbt.third_party_name                  as third_party_name,\n                bbt.third_party_sw_name               as third_party_sw_name,\n                bbt.third_party_sw_ver                as third_party_sw_ver,\n                bbt.third_party_agent_name_1          as third_party_agent_name_1,\n                bbt.third_party_agent_desc_1          as third_party_agent_desc_1,\n                bbt.third_party_agent_name_2          as third_party_agent_name_2,\n                bbt.third_party_agent_desc_2          as third_party_agent_desc_2,\n                bbt.annual_sales_count                as annual_sales_count,\n                bbt.underwriting_comment              as underwriting_comments\n        from    app_merch_bbt   bbt\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.app.bbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2061^7*/

      rs = it.getResultSet();
      setFields(rs);
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadOddFees()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  /*
  ** protected void loadDebitBRG
  **
  ** Loads checkbox on debit BRG if BRG group had been selected previously
  */
  protected void loadDebitBRG(long appSeqNum) throws Exception
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
   
   try
   {
      /*@lineinfo:generated-code*//*@lineinfo:2092^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(tc.tranchrg_brg, '00000', 'n', 'y')  use_debit_brg
//          from    tranchrg tc
//          where   tc.app_seq_num = :appSeqNum and
//                  tc.cardtype_code = :mesConstants.APP_CT_DEBIT
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(tc.tranchrg_brg, '00000', 'n', 'y')  use_debit_brg\n        from    tranchrg tc\n        where   tc.app_seq_num =  :1  and\n                tc.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_DEBIT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.app.bbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2098^7*/
    
      rs = it.getResultSet();
      setFields(rs);
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadDebitBRG()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  /*
  ** protected void loadUnderwriting(long appSeqNum) throws Exception
  **
  ** Loads BB&T underwriting information from app_merch_bbt.
  */
  protected void loadUnderwriting(long appSeqNum) throws Exception
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2129^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  building_own_lease,
//                  location_type,
//                  location_footage,
//                  location_floor,
//                  employee_count,
//                  established_date,
//                  product_desc,
//                  won_over,
//                  have_canceled,
//                  canceled_processor,
//                  canceled_reason,
//                  canceled_date,
//                  bbt_relationship,
//                  annual_volume,
//                  average_ticket,
//                  long_product_description,
//                  cp_percent,
//                  cnp_percent,
//                  cp_swipe_percent,
//                  cp_imprint_percent,
//                  cnp_swipe_percent,
//                  cnp_imprint_percent,
//                  cnp_telemarketer,
//                  b2b_percent,
//                  requires_prepayment,
//                  prepayment_amount,
//                  delivery_days,
//                  foreign_card_percent,
//                  dns_list_merchant,
//                  is_open,
//                  open_date,
//                  wireless_terminal,
//                  vs_other_bet,
//                  mc_other_bet,
//                  unrolled_pricing
//          from    app_merch_bbt
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  building_own_lease,\n                location_type,\n                location_footage,\n                location_floor,\n                employee_count,\n                established_date,\n                product_desc,\n                won_over,\n                have_canceled,\n                canceled_processor,\n                canceled_reason,\n                canceled_date,\n                bbt_relationship,\n                annual_volume,\n                average_ticket,\n                long_product_description,\n                cp_percent,\n                cnp_percent,\n                cp_swipe_percent,\n                cp_imprint_percent,\n                cnp_swipe_percent,\n                cnp_imprint_percent,\n                cnp_telemarketer,\n                b2b_percent,\n                requires_prepayment,\n                prepayment_amount,\n                delivery_days,\n                foreign_card_percent,\n                dns_list_merchant,\n                is_open,\n                open_date,\n                wireless_terminal,\n                vs_other_bet,\n                mc_other_bet,\n                unrolled_pricing\n        from    app_merch_bbt\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.app.bbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2168^7*/
      rs = it.getResultSet();
      setFields(rs);
      rs.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:2174^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mr.app_sic_code                 as sic_code,
//                  mr.seasonal_months              as seasonal_months,
//                  mr.merch_web_url                as web_url,
//                  mr.merch_email_address          as business_email,
//                  mr.risk_pci_vendor_code         as pci_vendor,
//                  mr.risk_pci_vendor_other        as pci_vendor_other,
//                  mr.risk_pci_last_scan_date      as pci_last_scan_date,
//                  mr.risk_pci_next_scan_date      as pci_next_scan_date,
//                  mr.risk_pci_compliant           as pci_compliant
//          from    merchant    mr
//          where   mr.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mr.app_sic_code                 as sic_code,\n                mr.seasonal_months              as seasonal_months,\n                mr.merch_web_url                as web_url,\n                mr.merch_email_address          as business_email,\n                mr.risk_pci_vendor_code         as pci_vendor,\n                mr.risk_pci_vendor_other        as pci_vendor_other,\n                mr.risk_pci_last_scan_date      as pci_last_scan_date,\n                mr.risk_pci_next_scan_date      as pci_next_scan_date,\n                mr.risk_pci_compliant           as pci_compliant\n        from    merchant    mr\n        where   mr.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.app.bbt.Pricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2187^7*/
      rs = it.getResultSet();
      setFields(rs);
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadUnderwriting()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  protected boolean loadAppData()
  {
    boolean loadOk = false;
    ResultSetIterator it = null;
    try
    {
      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        connect();

        loadOddFees(appSeqNum);
        loadUnderwriting(appSeqNum);
        loadDebitBRG(appSeqNum);
      }
      loadOk = super.loadAppData();
    }
    catch(Exception e)
    {
      logEntry("loadAppData()",e.toString());
      System.out.println(this.getClass().getName() + "::loadAppData(): "
        + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return loadOk;
  }

  /*************************************************************************
  **
  **   Submit
  **
  **************************************************************************/

  /*
  ** protected void submitOddFees(long appSeqNum) throws Exception
  **
  ** Stores any fees not handled by the fee set object.
  */
  protected void submitOddFees(long appSeqNum) throws Exception
  {
    try
    {

      // insert a row for this app if it's not present
      int rowCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:2255^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(*) row_count 
//          from    app_merch_bbt
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*) row_count  \n        from    app_merch_bbt\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.app.bbt.Pricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2260^7*/
      
      if (rowCount <= 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:2264^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_merch_bbt
//            ( app_seq_num )
//            values
//            ( :appSeqNum )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_merch_bbt\n          ( app_seq_num )\n          values\n          (  :1  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2270^9*/
      }
      
      // update the row with misc charge record data
      /*@lineinfo:generated-code*//*@lineinfo:2274^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_merch_bbt
//          set     MISC_FEE_DESC1          = : fields.getData("miscFeeDesc1"),
//                  MISC_FEE_AMT1           = : fields.getData("miscFeeAmt1"),
//                  MISC_FEE_CHRG_BASIS1    = : fields.getData("miscFeeChrgBasis1"),
//                  MISC_FEE_START_DATE1    = : fields.getData("miscFeeStartDate1"),
//                  MISC_FEE_END_DATE1      = : fields.getData("miscFeeEndDate1"),
//                  MISC_FEE_BILL_FREQ1     = : fields.getData("miscFeeBillFreq1"),
//                  MISC_FEE_CHRG_TYPE1     = : fields.getData("miscFeeChrgType1"),
//                  MISC_FEE_DESC2          = : fields.getData("miscFeeDesc2"),
//                  MISC_FEE_AMT2           = : fields.getData("miscFeeAmt2"),
//                  MISC_FEE_CHRG_BASIS2    = : fields.getData("miscFeeChrgBasis2"),
//                  MISC_FEE_START_DATE2    = : fields.getData("miscFeeStartDate2"),
//                  MISC_FEE_END_DATE2      = : fields.getData("miscFeeEndDate2"),
//                  MISC_FEE_BILL_FREQ2     = : fields.getData("miscFeeBillFreq2"),
//                  MISC_FEE_CHRG_TYPE2     = : fields.getData("miscFeeChrgType2"),
//                  MISC_FEE_DESC3          = : fields.getData("miscFeeDesc3"),
//                  MISC_FEE_AMT3           = : fields.getData("miscFeeAmt3"),
//                  MISC_FEE_CHRG_BASIS3    = : fields.getData("miscFeeChrgBasis3"),
//                  MISC_FEE_START_DATE3    = : fields.getData("miscFeeStartDate3"),
//                  MISC_FEE_END_DATE3      = : fields.getData("miscFeeEndDate3"),
//                  MISC_FEE_BILL_FREQ3     = : fields.getData("miscFeeBillFreq3"),
//                  MISC_FEE_CHRG_TYPE3     = : fields.getData("miscFeeChrgType3"),
//                  MISC_FEE_DESC4          = : fields.getData("miscFeeDesc4"),
//                  MISC_FEE_AMT4           = : fields.getData("miscFeeAmt4"),
//                  MISC_FEE_CHRG_BASIS4    = : fields.getData("miscFeeChrgBasis4"),
//                  MISC_FEE_START_DATE4    = : fields.getData("miscFeeStartDate4"),
//                  MISC_FEE_END_DATE4      = : fields.getData("miscFeeEndDate4"),
//                  MISC_FEE_BILL_FREQ4     = : fields.getData("miscFeeBillFreq4"),
//                  MISC_FEE_CHRG_TYPE4     = : fields.getData("miscFeeChrgType4")
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2585 =  fields.getData("miscFeeDesc1");
 String __sJT_2586 =  fields.getData("miscFeeAmt1");
 String __sJT_2587 =  fields.getData("miscFeeChrgBasis1");
 String __sJT_2588 =  fields.getData("miscFeeStartDate1");
 String __sJT_2589 =  fields.getData("miscFeeEndDate1");
 String __sJT_2590 =  fields.getData("miscFeeBillFreq1");
 String __sJT_2591 =  fields.getData("miscFeeChrgType1");
 String __sJT_2592 =  fields.getData("miscFeeDesc2");
 String __sJT_2593 =  fields.getData("miscFeeAmt2");
 String __sJT_2594 =  fields.getData("miscFeeChrgBasis2");
 String __sJT_2595 =  fields.getData("miscFeeStartDate2");
 String __sJT_2596 =  fields.getData("miscFeeEndDate2");
 String __sJT_2597 =  fields.getData("miscFeeBillFreq2");
 String __sJT_2598 =  fields.getData("miscFeeChrgType2");
 String __sJT_2599 =  fields.getData("miscFeeDesc3");
 String __sJT_2600 =  fields.getData("miscFeeAmt3");
 String __sJT_2601 =  fields.getData("miscFeeChrgBasis3");
 String __sJT_2602 =  fields.getData("miscFeeStartDate3");
 String __sJT_2603 =  fields.getData("miscFeeEndDate3");
 String __sJT_2604 =  fields.getData("miscFeeBillFreq3");
 String __sJT_2605 =  fields.getData("miscFeeChrgType3");
 String __sJT_2606 =  fields.getData("miscFeeDesc4");
 String __sJT_2607 =  fields.getData("miscFeeAmt4");
 String __sJT_2608 =  fields.getData("miscFeeChrgBasis4");
 String __sJT_2609 =  fields.getData("miscFeeStartDate4");
 String __sJT_2610 =  fields.getData("miscFeeEndDate4");
 String __sJT_2611 =  fields.getData("miscFeeBillFreq4");
 String __sJT_2612 =  fields.getData("miscFeeChrgType4");
   String theSqlTS = "update  app_merch_bbt\n        set     MISC_FEE_DESC1          =  :1 ,\n                MISC_FEE_AMT1           =  :2 ,\n                MISC_FEE_CHRG_BASIS1    =  :3 ,\n                MISC_FEE_START_DATE1    =  :4 ,\n                MISC_FEE_END_DATE1      =  :5 ,\n                MISC_FEE_BILL_FREQ1     =  :6 ,\n                MISC_FEE_CHRG_TYPE1     =  :7 ,\n                MISC_FEE_DESC2          =  :8 ,\n                MISC_FEE_AMT2           =  :9 ,\n                MISC_FEE_CHRG_BASIS2    =  :10 ,\n                MISC_FEE_START_DATE2    =  :11 ,\n                MISC_FEE_END_DATE2      =  :12 ,\n                MISC_FEE_BILL_FREQ2     =  :13 ,\n                MISC_FEE_CHRG_TYPE2     =  :14 ,\n                MISC_FEE_DESC3          =  :15 ,\n                MISC_FEE_AMT3           =  :16 ,\n                MISC_FEE_CHRG_BASIS3    =  :17 ,\n                MISC_FEE_START_DATE3    =  :18 ,\n                MISC_FEE_END_DATE3      =  :19 ,\n                MISC_FEE_BILL_FREQ3     =  :20 ,\n                MISC_FEE_CHRG_TYPE3     =  :21 ,\n                MISC_FEE_DESC4          =  :22 ,\n                MISC_FEE_AMT4           =  :23 ,\n                MISC_FEE_CHRG_BASIS4    =  :24 ,\n                MISC_FEE_START_DATE4    =  :25 ,\n                MISC_FEE_END_DATE4      =  :26 ,\n                MISC_FEE_BILL_FREQ4     =  :27 ,\n                MISC_FEE_CHRG_TYPE4     =  :28 \n        where   app_seq_num =  :29";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2585);
   __sJT_st.setString(2,__sJT_2586);
   __sJT_st.setString(3,__sJT_2587);
   __sJT_st.setString(4,__sJT_2588);
   __sJT_st.setString(5,__sJT_2589);
   __sJT_st.setString(6,__sJT_2590);
   __sJT_st.setString(7,__sJT_2591);
   __sJT_st.setString(8,__sJT_2592);
   __sJT_st.setString(9,__sJT_2593);
   __sJT_st.setString(10,__sJT_2594);
   __sJT_st.setString(11,__sJT_2595);
   __sJT_st.setString(12,__sJT_2596);
   __sJT_st.setString(13,__sJT_2597);
   __sJT_st.setString(14,__sJT_2598);
   __sJT_st.setString(15,__sJT_2599);
   __sJT_st.setString(16,__sJT_2600);
   __sJT_st.setString(17,__sJT_2601);
   __sJT_st.setString(18,__sJT_2602);
   __sJT_st.setString(19,__sJT_2603);
   __sJT_st.setString(20,__sJT_2604);
   __sJT_st.setString(21,__sJT_2605);
   __sJT_st.setString(22,__sJT_2606);
   __sJT_st.setString(23,__sJT_2607);
   __sJT_st.setString(24,__sJT_2608);
   __sJT_st.setString(25,__sJT_2609);
   __sJT_st.setString(26,__sJT_2610);
   __sJT_st.setString(27,__sJT_2611);
   __sJT_st.setString(28,__sJT_2612);
   __sJT_st.setLong(29,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2306^7*/



    }
    catch(Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitOddFees()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  /*
  ** protected void submitUnderwriting(long appSeqNum) throws Exception
  **
  ** Stores BB&T underwriting data in app_merch_bbt.
  */
  protected void submitUnderwriting(long appSeqNum) throws Exception
  {
    try
    {
      // insert a row for this app if it's not present
      int rowCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:2330^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(*) row_count 
//          from    app_merch_bbt
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*) row_count  \n        from    app_merch_bbt\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.app.bbt.Pricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2335^7*/
      
      if (rowCount <= 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:2339^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_merch_bbt
//            ( app_seq_num )
//            values
//            ( :appSeqNum )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_merch_bbt\n          ( app_seq_num )\n          values\n          (  :1  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2345^9*/
      }
      
      // update the row with underwriting data
      /*@lineinfo:generated-code*//*@lineinfo:2349^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_merch_bbt
//          set     building_own_lease        = : fields.getData("buildingOwnLease"),
//                  location_type             = : fields.getData("locationType"),
//                  location_footage          = : fields.getData("locationFootage"),
//                  location_floor            = : fields.getData("locationFloor"),
//                  employee_count            = : fields.getData("employeeCount"),
//                  established_date          = : ((DateStringField)fields.getField("establishedDate")).getSqlDateString(),
//                  product_desc              = : fields.getData("productDesc"),
//                  won_over                  = : fields.getData("wonOver"),
//                  have_canceled             = : fields.getData("haveCanceled"),
//                  canceled_processor        = : fields.getData("canceledProcessor"),
//                  canceled_reason           = : fields.getData("canceledReason"),
//                  canceled_date             = : ((DateStringField)fields.getField("canceledDate")).getSqlDateString(),
//                  bbt_relationship          = : fields.getData("bbtRelationship"),
//                  annual_volume             = : fields.getData("annualVolume"),
//                  average_ticket            = : fields.getData("averageTicket"),
//                  long_product_description  = : fields.getData("longProductDescripiton"),
//                  cp_percent                = : fields.getData("cpPercent"),
//                  cnp_percent               = : fields.getData("cnpPercent"),
//                  cp_swipe_percent          = : fields.getData("cpSwipePercent"),
//                  cp_imprint_percent        = : fields.getData("cpImprintPercent"),
//                  cnp_swipe_percent         = : fields.getData("cnpSwipePercent"),
//                  cnp_imprint_percent       = : fields.getData("cnpImprintPercent"),
//                  cnp_telemarketer          = : fields.getData("cnpTelemarketer"),
//                  b2b_percent               = : fields.getData("b2pPercent"),
//                  requires_prepayment       = : fields.getData("requiresPrepayment"),
//                  prepayment_amount         = : fields.getData("prepaymentAmount"),
//                  delivery_days             = : fields.getData("deliveryDays"),
//                  foreign_card_percent      = : fields.getData("foreignCardPercent"),
//                  dns_list_merchant         = : fields.getData("dnsListMerchant"),
//                  is_open                   = : fields.getData("isOpen"),
//                  open_date                 = : ((DateStringField)fields.getField("openDate")).getSqlDateString(),
//                  wireless_terminal         = : fields.getData("wirelessTerminal"),
//                  vs_other_bet              = : fields.getData("vsOtherBet"),
//                  mc_other_bet              = : fields.getData("mcOtherBet"),
//                  unrolled_pricing          = : fields.getData("unrolledPricing"),
//                  website_ind                   = : fields.getData("websiteInd"),
//                  third_party_sw_ind            = : fields.getData("thirdPartySwInd"),
//                  third_party_agent_ind         = : fields.getData("thirdPartyAgentInd"),
//                  internet_ssl_cert_ind         = : fields.getData("internetSslCertInd"),
//                  internet_delivery_policy_ind  = : fields.getData("internetDeliveryPolicyInd"),
//                  internet_refund_policy_ind    = : fields.getData("internetRefundPolicyInd"),
//                  internet_service_contact_ind  = : fields.getData("internetServiceContactInd"),
//                  internet_addr_on_website_ind  = : fields.getData("internetAddrOnWebsiteInd"),
//                  internet_ship_out_of_us_ind   = : fields.getData("internetShipOutOfUsInd"),
//                  pci_val_required_ind          = : fields.getData("pciValRequiredInd"),
//                  payment_app_val_conf_ind      = : fields.getData("paymentAppValConfInd"),
//                  merch_web_url2                = : fields.getData("webUrl2"),
//                  third_party_name              = : fields.getData("thirdPartyName"),
//                  third_party_sw_name           = : fields.getData("thirdPartySwName"),
//                  third_party_sw_ver            = : fields.getData("thirdPartySwVer"),
//                  third_party_agent_name_1      = : fields.getData("thirdPartyAgentName1"),
//                  third_party_agent_desc_1      = : fields.getData("thirdPartyAgentDesc1"),
//                  third_party_agent_name_2      = : fields.getData("thirdPartyAgentName2"),
//                  third_party_agent_desc_2      = : fields.getData("thirdPartyAgentDesc2"),
//                  annual_sales_count            = : fields.getData("annualSalesCount"),
//                  underwriting_comment          = : fields.getData("underwritingComments")
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2613 =  fields.getData("buildingOwnLease");
 String __sJT_2614 =  fields.getData("locationType");
 String __sJT_2615 =  fields.getData("locationFootage");
 String __sJT_2616 =  fields.getData("locationFloor");
 String __sJT_2617 =  fields.getData("employeeCount");
 String __sJT_2618 =  ((DateStringField)fields.getField("establishedDate")).getSqlDateString();
 String __sJT_2619 =  fields.getData("productDesc");
 String __sJT_2620 =  fields.getData("wonOver");
 String __sJT_2621 =  fields.getData("haveCanceled");
 String __sJT_2622 =  fields.getData("canceledProcessor");
 String __sJT_2623 =  fields.getData("canceledReason");
 String __sJT_2624 =  ((DateStringField)fields.getField("canceledDate")).getSqlDateString();
 String __sJT_2625 =  fields.getData("bbtRelationship");
 String __sJT_2626 =  fields.getData("annualVolume");
 String __sJT_2627 =  fields.getData("averageTicket");
 String __sJT_2628 =  fields.getData("longProductDescripiton");
 String __sJT_2629 =  fields.getData("cpPercent");
 String __sJT_2630 =  fields.getData("cnpPercent");
 String __sJT_2631 =  fields.getData("cpSwipePercent");
 String __sJT_2632 =  fields.getData("cpImprintPercent");
 String __sJT_2633 =  fields.getData("cnpSwipePercent");
 String __sJT_2634 =  fields.getData("cnpImprintPercent");
 String __sJT_2635 =  fields.getData("cnpTelemarketer");
 String __sJT_2636 =  fields.getData("b2pPercent");
 String __sJT_2637 =  fields.getData("requiresPrepayment");
 String __sJT_2638 =  fields.getData("prepaymentAmount");
 String __sJT_2639 =  fields.getData("deliveryDays");
 String __sJT_2640 =  fields.getData("foreignCardPercent");
 String __sJT_2641 =  fields.getData("dnsListMerchant");
 String __sJT_2642 =  fields.getData("isOpen");
 String __sJT_2643 =  ((DateStringField)fields.getField("openDate")).getSqlDateString();
 String __sJT_2644 =  fields.getData("wirelessTerminal");
 String __sJT_2645 =  fields.getData("vsOtherBet");
 String __sJT_2646 =  fields.getData("mcOtherBet");
 String __sJT_2647 =  fields.getData("unrolledPricing");
 String __sJT_2648 =  fields.getData("websiteInd");
 String __sJT_2649 =  fields.getData("thirdPartySwInd");
 String __sJT_2650 =  fields.getData("thirdPartyAgentInd");
 String __sJT_2651 =  fields.getData("internetSslCertInd");
 String __sJT_2652 =  fields.getData("internetDeliveryPolicyInd");
 String __sJT_2653 =  fields.getData("internetRefundPolicyInd");
 String __sJT_2654 =  fields.getData("internetServiceContactInd");
 String __sJT_2655 =  fields.getData("internetAddrOnWebsiteInd");
 String __sJT_2656 =  fields.getData("internetShipOutOfUsInd");
 String __sJT_2657 =  fields.getData("pciValRequiredInd");
 String __sJT_2658 =  fields.getData("paymentAppValConfInd");
 String __sJT_2659 =  fields.getData("webUrl2");
 String __sJT_2660 =  fields.getData("thirdPartyName");
 String __sJT_2661 =  fields.getData("thirdPartySwName");
 String __sJT_2662 =  fields.getData("thirdPartySwVer");
 String __sJT_2663 =  fields.getData("thirdPartyAgentName1");
 String __sJT_2664 =  fields.getData("thirdPartyAgentDesc1");
 String __sJT_2665 =  fields.getData("thirdPartyAgentName2");
 String __sJT_2666 =  fields.getData("thirdPartyAgentDesc2");
 String __sJT_2667 =  fields.getData("annualSalesCount");
 String __sJT_2668 =  fields.getData("underwritingComments");
   String theSqlTS = "update  app_merch_bbt\n        set     building_own_lease        =  :1 ,\n                location_type             =  :2 ,\n                location_footage          =  :3 ,\n                location_floor            =  :4 ,\n                employee_count            =  :5 ,\n                established_date          =  :6 ,\n                product_desc              =  :7 ,\n                won_over                  =  :8 ,\n                have_canceled             =  :9 ,\n                canceled_processor        =  :10 ,\n                canceled_reason           =  :11 ,\n                canceled_date             =  :12 ,\n                bbt_relationship          =  :13 ,\n                annual_volume             =  :14 ,\n                average_ticket            =  :15 ,\n                long_product_description  =  :16 ,\n                cp_percent                =  :17 ,\n                cnp_percent               =  :18 ,\n                cp_swipe_percent          =  :19 ,\n                cp_imprint_percent        =  :20 ,\n                cnp_swipe_percent         =  :21 ,\n                cnp_imprint_percent       =  :22 ,\n                cnp_telemarketer          =  :23 ,\n                b2b_percent               =  :24 ,\n                requires_prepayment       =  :25 ,\n                prepayment_amount         =  :26 ,\n                delivery_days             =  :27 ,\n                foreign_card_percent      =  :28 ,\n                dns_list_merchant         =  :29 ,\n                is_open                   =  :30 ,\n                open_date                 =  :31 ,\n                wireless_terminal         =  :32 ,\n                vs_other_bet              =  :33 ,\n                mc_other_bet              =  :34 ,\n                unrolled_pricing          =  :35 ,\n                website_ind                   =  :36 ,\n                third_party_sw_ind            =  :37 ,\n                third_party_agent_ind         =  :38 ,\n                internet_ssl_cert_ind         =  :39 ,\n                internet_delivery_policy_ind  =  :40 ,\n                internet_refund_policy_ind    =  :41 ,\n                internet_service_contact_ind  =  :42 ,\n                internet_addr_on_website_ind  =  :43 ,\n                internet_ship_out_of_us_ind   =  :44 ,\n                pci_val_required_ind          =  :45 ,\n                payment_app_val_conf_ind      =  :46 ,\n                merch_web_url2                =  :47 ,\n                third_party_name              =  :48 ,\n                third_party_sw_name           =  :49 ,\n                third_party_sw_ver            =  :50 ,\n                third_party_agent_name_1      =  :51 ,\n                third_party_agent_desc_1      =  :52 ,\n                third_party_agent_name_2      =  :53 ,\n                third_party_agent_desc_2      =  :54 ,\n                annual_sales_count            =  :55 ,\n                underwriting_comment          =  :56 \n        where   app_seq_num =  :57";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2613);
   __sJT_st.setString(2,__sJT_2614);
   __sJT_st.setString(3,__sJT_2615);
   __sJT_st.setString(4,__sJT_2616);
   __sJT_st.setString(5,__sJT_2617);
   __sJT_st.setString(6,__sJT_2618);
   __sJT_st.setString(7,__sJT_2619);
   __sJT_st.setString(8,__sJT_2620);
   __sJT_st.setString(9,__sJT_2621);
   __sJT_st.setString(10,__sJT_2622);
   __sJT_st.setString(11,__sJT_2623);
   __sJT_st.setString(12,__sJT_2624);
   __sJT_st.setString(13,__sJT_2625);
   __sJT_st.setString(14,__sJT_2626);
   __sJT_st.setString(15,__sJT_2627);
   __sJT_st.setString(16,__sJT_2628);
   __sJT_st.setString(17,__sJT_2629);
   __sJT_st.setString(18,__sJT_2630);
   __sJT_st.setString(19,__sJT_2631);
   __sJT_st.setString(20,__sJT_2632);
   __sJT_st.setString(21,__sJT_2633);
   __sJT_st.setString(22,__sJT_2634);
   __sJT_st.setString(23,__sJT_2635);
   __sJT_st.setString(24,__sJT_2636);
   __sJT_st.setString(25,__sJT_2637);
   __sJT_st.setString(26,__sJT_2638);
   __sJT_st.setString(27,__sJT_2639);
   __sJT_st.setString(28,__sJT_2640);
   __sJT_st.setString(29,__sJT_2641);
   __sJT_st.setString(30,__sJT_2642);
   __sJT_st.setString(31,__sJT_2643);
   __sJT_st.setString(32,__sJT_2644);
   __sJT_st.setString(33,__sJT_2645);
   __sJT_st.setString(34,__sJT_2646);
   __sJT_st.setString(35,__sJT_2647);
   __sJT_st.setString(36,__sJT_2648);
   __sJT_st.setString(37,__sJT_2649);
   __sJT_st.setString(38,__sJT_2650);
   __sJT_st.setString(39,__sJT_2651);
   __sJT_st.setString(40,__sJT_2652);
   __sJT_st.setString(41,__sJT_2653);
   __sJT_st.setString(42,__sJT_2654);
   __sJT_st.setString(43,__sJT_2655);
   __sJT_st.setString(44,__sJT_2656);
   __sJT_st.setString(45,__sJT_2657);
   __sJT_st.setString(46,__sJT_2658);
   __sJT_st.setString(47,__sJT_2659);
   __sJT_st.setString(48,__sJT_2660);
   __sJT_st.setString(49,__sJT_2661);
   __sJT_st.setString(50,__sJT_2662);
   __sJT_st.setString(51,__sJT_2663);
   __sJT_st.setString(52,__sJT_2664);
   __sJT_st.setString(53,__sJT_2665);
   __sJT_st.setString(54,__sJT_2666);
   __sJT_st.setString(55,__sJT_2667);
   __sJT_st.setString(56,__sJT_2668);
   __sJT_st.setLong(57,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2409^7*/
      
      String seasonalMonths = fields.getData("seasonalMonths");
      String seasonalFlag
        = (seasonalMonths.equals("NNNNNNNNNNNN") ? "N" : "Y");
        
      /*@lineinfo:generated-code*//*@lineinfo:2415^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     app_sic_code              = : fields.getData("sicCode"),
//                  seasonal_flag             = :seasonalFlag,
//                  seasonal_months           = :seasonalMonths,
//                  merch_web_url             = : fields.getData("webUrl"),
//                  merch_email_address       = : fields.getData("businessEmail"),
//                  risk_pci_vendor_code      = : fields.getData("pciVendor"),
//                  risk_pci_vendor_other     = : fields.getData("pciVendorOther"),
//                  risk_pci_last_scan_date   = : ((DateField)fields.getField("pciLastScanDate")).getSqlDate(),
//                  risk_pci_next_scan_date   = : ((DateField)fields.getField("pciNextScanDate")).getSqlDate(),
//                  risk_pci_compliant        = : fields.getData("pciCompliant")
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2669 =  fields.getData("sicCode");
 String __sJT_2670 =  fields.getData("webUrl");
 String __sJT_2671 =  fields.getData("businessEmail");
 String __sJT_2672 =  fields.getData("pciVendor");
 String __sJT_2673 =  fields.getData("pciVendorOther");
 java.sql.Date __sJT_2674 =  ((DateField)fields.getField("pciLastScanDate")).getSqlDate();
 java.sql.Date __sJT_2675 =  ((DateField)fields.getField("pciNextScanDate")).getSqlDate();
 String __sJT_2676 =  fields.getData("pciCompliant");
   String theSqlTS = "update  merchant\n        set     app_sic_code              =  :1 ,\n                seasonal_flag             =  :2 ,\n                seasonal_months           =  :3 ,\n                merch_web_url             =  :4 ,\n                merch_email_address       =  :5 ,\n                risk_pci_vendor_code      =  :6 ,\n                risk_pci_vendor_other     =  :7 ,\n                risk_pci_last_scan_date   =  :8 ,\n                risk_pci_next_scan_date   =  :9 ,\n                risk_pci_compliant        =  :10 \n        where   app_seq_num =  :11";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2669);
   __sJT_st.setString(2,seasonalFlag);
   __sJT_st.setString(3,seasonalMonths);
   __sJT_st.setString(4,__sJT_2670);
   __sJT_st.setString(5,__sJT_2671);
   __sJT_st.setString(6,__sJT_2672);
   __sJT_st.setString(7,__sJT_2673);
   __sJT_st.setDate(8,__sJT_2674);
   __sJT_st.setDate(9,__sJT_2675);
   __sJT_st.setString(10,__sJT_2676);
   __sJT_st.setLong(11,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2429^7*/
      
      if( !fields.getData("underwritingComments").equals("") )
      {
        long merchantId = 0L;
        // get merchant number
        /*@lineinfo:generated-code*//*@lineinfo:2435^9*/

//  ************************************************************
//  #sql [Ctx] { select  merch_number 
//            from    merchant
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_number  \n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.app.bbt.Pricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2440^9*/
        
        // make sure there isn't already a service call for this in the system
        int recCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:2444^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(rn.merchant_number) 
//            from    risk_notes  rn
//            where   rn.merchant_number = :merchantId and
//                    rn.type = 37 and -- risk/pci call type
//                    rn.user_id is null and
//                    rn.notes like 'Underwriting Comment:%'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(rn.merchant_number)  \n          from    risk_notes  rn\n          where   rn.merchant_number =  :1  and\n                  rn.type = 37 and -- risk/pci call type\n                  rn.user_id is null and\n                  rn.notes like 'Underwriting Comment:%'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.app.bbt.Pricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2452^9*/
        
        if ( recCount == 0 && merchantId != 0L )
        {
          /*@lineinfo:generated-code*//*@lineinfo:2456^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_notes
//              (
//                merchant_number,
//                note_date,
//                type,
//                notes
//              )
//              values
//              (
//                :merchantId,
//                sysdate,
//                37, -- risk/pci
//                ( 'Underwriting Comment: ' ||
//                  : fields.getData("underwritingComments") )
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2677 =  fields.getData("underwritingComments");
   String theSqlTS = "insert into risk_notes\n            (\n              merchant_number,\n              note_date,\n              type,\n              notes\n            )\n            values\n            (\n               :1 ,\n              sysdate,\n              37, -- risk/pci\n              ( 'Underwriting Comment: ' ||\n                 :2  )\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setString(2,__sJT_2677);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2473^11*/
        }
      }
    }
    catch(Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitUnderwriting()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }
  
  private void submitPricingComments(long appSeqNum) throws Exception
  {
    try
    {
      // if pricing comments field is non blank then submit to service calls table
      if(!fields.getData("pricingComments").equals(""))
      {
        long merchantNumber = 0L;
        // get merchant number
        /*@lineinfo:generated-code*//*@lineinfo:2493^9*/

//  ************************************************************
//  #sql [Ctx] { select  merch_number
//            
//            from    merchant
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_number\n           \n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.app.bbt.Pricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantNumber = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2499^9*/
        
        // make sure there isn't already a service call for this in the system
        int recCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:2503^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(sequence)
//            
//            from    service_calls
//            where   merchant_number = :merchantNumber and
//                    type = :mesConstants.CALL_TYPE_OTHER and
//                    other_description = 'System' and
//                    login_name = 'SYSTEM' and
//                    notes like 'Pricing Comment:%'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(sequence)\n           \n          from    service_calls\n          where   merchant_number =  :1  and\n                  type =  :2  and\n                  other_description = 'System' and\n                  login_name = 'SYSTEM' and\n                  notes like 'Pricing Comment:%'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.app.bbt.Pricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setInt(2,mesConstants.CALL_TYPE_OTHER);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2513^9*/
        
        if(recCount == 0)
        {
          // add the service call
          StringBuffer newNote = new StringBuffer("");
          newNote.append(fields.getData("pricingComments"));
          
          /*@lineinfo:generated-code*//*@lineinfo:2521^11*/

//  ************************************************************
//  #sql [Ctx] { call add_system_service_call(:merchantNumber, :newNote.toString())
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2678 = newNote.toString();
  try {
   String theSqlTS = "BEGIN add_system_service_call( :1 ,  :2 )\n          \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.app.bbt.Pricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setString(2,__sJT_2678);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2524^11*/
        }
      }
    }
    catch(Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitPricingComments()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }


  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);

    //if no misc charge fee desc or amt is entered.. 
    //clear out other fields they should be blank.. assume data entry was a mistake
    if(fields.getField("miscFeeDesc1").isBlank() && fields.getField("miscFeeAmt1").isBlank())
    {
      fields.getField("miscFeeChrgType1").setData("");  
      fields.getField("miscFeeChrgBasis1").setData("");
      fields.getField("miscFeeStartDate1").setData("");
      fields.getField("miscFeeEndDate1").setData("");  
      fields.getField("miscFeeBillFreq1").setData("NNNNNNNNNNNN"); 
    }

    if(fields.getField("miscFeeDesc2").isBlank() && fields.getField("miscFeeAmt2").isBlank())
    {
      fields.getField("miscFeeChrgType2").setData("");  
      fields.getField("miscFeeChrgBasis2").setData("");
      fields.getField("miscFeeStartDate2").setData("");
      fields.getField("miscFeeEndDate2").setData("");  
      fields.getField("miscFeeBillFreq2").setData("NNNNNNNNNNNN"); 
    }
    
    if(fields.getField("miscFeeDesc3").isBlank() && fields.getField("miscFeeAmt3").isBlank())
    {
      fields.getField("miscFeeChrgType3").setData("");  
      fields.getField("miscFeeChrgBasis3").setData("");
      fields.getField("miscFeeStartDate3").setData("");
      fields.getField("miscFeeEndDate3").setData("");  
      fields.getField("miscFeeBillFreq3").setData("NNNNNNNNNNNN"); 
    }

    if(fields.getField("miscFeeDesc4").isBlank() && fields.getField("miscFeeAmt4").isBlank())
    {
      fields.getField("miscFeeChrgType4").setData("");  
      fields.getField("miscFeeChrgBasis4").setData("");
      fields.getField("miscFeeStartDate4").setData("");
      fields.getField("miscFeeEndDate4").setData("");  
      fields.getField("miscFeeBillFreq4").setData("NNNNNNNNNNNN"); 
    }


    //if interchange plus is selected and rate is provided, zero out minimum monthly discount
    if(request.getParameter("betType") != null && request.getParameter("betType").equals("22"))
    {
      if(request.getParameter("discRate_22") != null && !request.getParameter("discRate_22").equals(""))
      {
        fields.getField("tranFee_1_mminchrg").setData("0");
      }
    }
  }

  protected boolean submitAppData()
  {
    boolean submitOk = false;
    try
    {
      connect();
      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        submitOddFees(appSeqNum);
        submitUnderwriting(appSeqNum);
        submitPricingComments(appSeqNum);
      }
      submitOk = super.submitAppData();
    }
    catch(Exception e)
    {
      logEntry("submitAppData()",e.toString());
      System.out.println(this.getClass().getName() + "::submitAppData(): "
        + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return submitOk;
  }
}/*@lineinfo:generated-code*/