/*@lineinfo:filename=BusinessBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/BusinessBase.sqlj $

  Description:

  BusinessBase

  Base bean for erchant information page bean (page 1).

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-01-09 15:58:49 -0800 (Fri, 09 Jan 2009) $
  Version            : $Revision: 15700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app;

import java.sql.ResultSet;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.CityStateZipField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateStringField;
import com.mes.forms.DisabledCheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NameField;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.SmallCurrencyField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TaxIdField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.ops.AmexESADataBean.AmexFlatFeeValidation;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class BusinessBase extends AppBase
{
  static Logger log = Logger.getLogger(BusinessBase.class);

  // flag for apps that track estimated annual volume rather than monthly
  protected boolean   useAnnualVolume = false;
  
  // flag for apps that need to show Virtual Terminal Limited
  public boolean   allowVTLimited  = false;

  // default RAP rate
  public static final double DEFAULT_RAP_RATE = 2.97;
  
  // rap rates
  public double[][] RAPRates = 
  {
    { 2.52, 2.97, 2.18 },
    { 2.22, 2.67, 1.93 },
    { 1.99, 2.44, 1.73 },
    { 1.86, 2.31, 1.62 },
    { 1.80, 2.25, 1.57 },
    { 1.77, 2.22, 1.55 },
    { 1.73, 2.18, 1.51 },
    { 1.69, 2.14, 1.49 }
  };
  
  /*************************************************************************
  **
  **   Drop Down Tables
  **
  **************************************************************************/

  protected class BillingMethodTable extends DropDownTable
  {
    public BillingMethodTable()
    {
      addElement("","select one");
      addElement("Debit","Debit");
      addElement("Remit","Remit");
    }
  }

  public static class CheckProviderTable extends DropDownTable
  {
    public CheckProviderTable( )
    {
      init(true);
    }

    public CheckProviderTable( boolean allowOther )
    {
      init( allowOther);
    }

    protected void init( boolean allowOther )
    {
      addElement("","select one");
      addElement("CertegyWelcomeCheck","Certegy Welcome Check");
      addElement("CertegyElecCheck","Certegy Elec Check");

      if ( allowOther )
      {
        addElement("CPOther","Other");
      }
    }
  }

  protected class NewConversionTable extends DropDownTable
  {
    public NewConversionTable()
    {
      addElement("","select one");
      addElement("N","New Account");
      addElement("C","Conversion Account");
    }
  }

  protected class YearsInBusinessTable extends DropDownTable
  {
    public YearsInBusinessTable()
    {
      addElement("","select one");
      addElement("0","Less Than 1 Year");
      addElement("1","1 Year");

      for ( int i = 2; i < 99; ++i )
      {
        addElement( String.valueOf(i), (String.valueOf(i) + " Years") );
      }
    }
  }

  protected class BusinessTypeTable extends DropDownTable
  {
    public BusinessTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Sole Proprietorship");
      addElement("2","Corporation");
      addElement("3","Partnership");
      addElement("4","Medical or Legal Corporation");
      addElement("5","Association/Estate/Trust");
      addElement("6","Tax Exempt");
      addElement("7","Government");
      addElement("8","Limited Liability Company");
    }
  }

  protected class IndustryTypeTable extends DropDownTable
  {
    public IndustryTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail");
      addElement("2","Restaurant");
      addElement("3","Hotel");
      addElement("4","Motel");
      addElement("5","Internet");
      addElement("6","Services");
      addElement("7","Direct Marketing");
      addElement("8","Other");
    }
  }

  protected class LocationTypeTable extends DropDownTable
  {
    public LocationTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail Storefront");
      addElement("2","Internet Storefront");
      addElement("3","Business Office");
      addElement("4","Private Residence");
      addElement("6","Bank");
      addElement("5","Other");
    }
  }

  protected class ApplicationTypeTable extends DropDownTable
  {
    public ApplicationTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Single Outlet");
      addElement("2","Chain");
      addElement("3","Addl. Chain Outlet");
    }
  }

  protected class GenderTable extends DropDownTable
  {
    public GenderTable()
    {
      // value/name pairs
      addElement("U","Not Given");
      addElement("M","Male");
      addElement("F","Female");
    }
  }

  protected class RefundPolicyTable extends DropDownTable
  {
    public RefundPolicyTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Refund in 30 Days or Less");
      addElement("2","No Refunds or Exchanges");
      addElement("3","Exchanges Only");
      addElement("4","Not Applicable");
    }
  }

  protected class EbtOptionsTable extends DropDownTable
  {
    public EbtOptionsTable()
    {
      addElement("","select one");
      addElement("1","Food Stamps");
      addElement("2","Cash Benefits");
      addElement("3","Both");
    }
  }

  private class AccountInfoSourceTable extends DropDownTable
  {
    public AccountInfoSourceTable() throws java.sql.SQLException
    {
      ResultSetIterator it  = null;
      ResultSet         rs  = null;

      try
      {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:249^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  source_id,
//                    source_desc
//            from    bank_account_info_source
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  source_id,\n                  source_desc\n          from    bank_account_info_source";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.BusinessBase",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:254^9*/
        rs = it.getResultSet();

        while(rs.next())
        {
          addElement(rs);
        }
      }
      finally
      {
        try{ it.close(); } catch(Exception e) { }
        cleanUp();
      }
    }
  }

  protected class InternetProductTable extends DropDownTable
  {
    public InternetProductTable() throws java.sql.SQLException
    {
      ResultSetIterator it  = null;
      ResultSet         rs  = null;

      try
      {
        connect();

        addElement("","select one");

        /*@lineinfo:generated-code*//*@lineinfo:283^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  pos_code,
//                    pos_desc,
//                    nvl(allowed_app_types, 'ALL') allowed_app_types
//            from    pos_category
//            where   pos_type = 2
//            order by pos_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pos_code,\n                  pos_desc,\n                  nvl(allowed_app_types, 'ALL') allowed_app_types\n          from    pos_category\n          where   pos_type = 2\n          order by pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.BusinessBase",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:291^9*/

        rs = it.getResultSet();

        while(rs.next() )
        {
          if(rs.getString("allowed_app_types").equals("ALL"))
          {
            addElement(rs.getString("pos_code"), rs.getString("pos_desc"));
          }
          else
          {
            String  allowedAppTypes = rs.getString("allowed_app_types");
            StringTokenizer tokenizer = new StringTokenizer(allowedAppTypes, ",");

            boolean okToAdd = false;
            while(tokenizer.hasMoreTokens())
            {
              try
              {
                if(getData("appType").equals(tokenizer.nextToken()))
                {
                  okToAdd = true;
                }
              }
              catch(Exception e)
              {
              }
            }

            if(true == okToAdd)
            {
              addElement(rs.getString("pos_code"), rs.getString("pos_desc"));
            }
          }
        }

        rs.close();
        it.close();
      }
      finally
      {
        try{ rs.close(); } catch (Exception e) {}
        try{ it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }

  protected class PosPartnerOSTable extends DropDownTable
  {
    public PosPartnerOSTable()
    {
      ResultSetIterator it    = null;
      ResultSet         rs    = null;

      try
      {
        addElement("", "select one");

        connect();
        /*@lineinfo:generated-code*//*@lineinfo:352^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  os_code,
//                    description
//            from    pos_partner_os
//            order by display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  os_code,\n                  description\n          from    pos_partner_os\n          order by display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.BusinessBase",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:358^9*/

        rs = it.getResultSet();
        while(rs.next())
        {
          addElement(rs);
        }
      }
      catch(Exception e)
      {
        logEntry("PosPartnerOSTable()", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }

  /*
  ** protected class BranchTable extends DropDownTable
  **
  ** Creates a drop down table containing association numbers paired with
  ** a corresponding hierarchy name.  Intended for use with River City style
  ** MES bank.  Constructor takes a hid that should be four levels up from
  ** associations for that bank.  The query looks for 'non chain' in the
  ** org name associated with the assocs.
  */
  protected class BranchTable extends DropDownTable
  {
    public BranchTable(long topHid)
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:399^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    mod(t1.descendent,1000000)  assoc_num,
//                      o2.org_name                 branch_name
//            from      t_hierarchy t1,
//                      t_hierarchy t2,
//                      organization o1,
//                      organization o2
//            where     t1.ancestor = :topHid
//                      and t1.relation = 4
//                      and t1.descendent = o1.org_group
//                      and t2.descendent = t1.descendent
//                      and t2.relation = 1
//                      and t2.ancestor = o2.org_group
//                      and lower(o1.org_name) like '%non%chain%'
//            order by  o2.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mod(t1.descendent,1000000)  assoc_num,\n                    o2.org_name                 branch_name\n          from      t_hierarchy t1,\n                    t_hierarchy t2,\n                    organization o1,\n                    organization o2\n          where     t1.ancestor =  :1 \n                    and t1.relation = 4\n                    and t1.descendent = o1.org_group\n                    and t2.descendent = t1.descendent\n                    and t2.relation = 1\n                    and t2.ancestor = o2.org_group\n                    and lower(o1.org_name) like '%non%chain%'\n          order by  o2.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,topHid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:415^9*/
        rs = it.getResultSet();

        addElement("","select one");
        while (rs.next())
        {
          addElement(rs.getString("assoc_num"),rs.getString("branch_name"));
        }
      }
      catch (Exception e)
      {
        logEntry("BranchTable()",e.toString());
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }

  /*************************************************************************
  **
  **   Radio Button Lists
  **
  **************************************************************************/

  protected static final String[][] yesNoRadioList =
  {
    { "No",  "N" },
    { "Yes", "Y" }
  };

  protected String[][] posTypeList =
  {
    { "Point of Sale Terminal (Electronic Draft Capture)",
      Integer.toString(mesConstants.POS_DIAL_TERMINAL     ) },
    { "Internet Solution",
      Integer.toString(mesConstants.POS_INTERNET          ) },
    { "PC Charge Express for Windows",
      Integer.toString(mesConstants.POS_CHARGE_EXPRESS    ) },
    { "PC Charge Pro for Windows",
      Integer.toString(mesConstants.POS_CHARGE_PRO        ) },
    { "POS Partner 2000",
      Integer.toString(mesConstants.POS_PC                ) },
    { "DialPay (Telephone Authorization & Capture)",
      Integer.toString(mesConstants.POS_DIAL_AUTH         ) },
    { "Vital Third Party Software",
      Integer.toString(mesConstants.POS_OTHER             ) },
    { "Wireless Terminal",
      Integer.toString(mesConstants.POS_WIRELESS_TERMINAL ) },
    { "Global PC Windows",
      Integer.toString(mesConstants.POS_GLOBAL_PC         ) },
    { "Touch Tone Capture",
      Integer.toString(mesConstants.POS_TOUCHTONECAPTURE  ) },
    { "MES Virtual Terminal",
      Integer.toString(mesConstants.POS_VIRTUAL_TERMINAL  ) },
    { "MES Payment Gateway",
      Integer.toString(mesConstants.POS_PAYMENT_GATEWAY   ) },
    { "MES Virtual Terminal LIMITED",
      Integer.toString(mesConstants.POS_VT_LIMITED        ) },
    { "Unknown (TBD)",
      Integer.toString(mesConstants.POS_UNKNOWN           ) }
  };

  protected String[][] cardList =
  {
    { "Card Present and/or Mail Order", Integer.toString(mesConstants.APP_MPOS_PAYMENT_GATEWAY_CP  ) },
    { "e-Commerce, Card Not Present",   Integer.toString(mesConstants.APP_MPOS_PAYMENT_GATEWAY_CNP ) }
  };

  protected String[][] chainTypeList =
  {
    { "Non-Chain Merchant", "0" },
    { "Chain Merchant",     "1" }
  };

  protected String[][] posPartnerList =
  {
    { "Dialup", "0" },
    { "SSL",    "1" }
  };

  /*************************************************************************
  **
  **   Validations
  **
  **************************************************************************/

  /*
  ** public class AnnualAmountValidation
  **
  ** Requires that an annual amount be less than $100,000,000
  */
  public class AnnualAmountValidation
    implements Validation
  {
    public AnnualAmountValidation()
    {
    }

    public String getErrorText()
    {
      return( "Amount too large (must be less than $100,000,000)" );
    }

    public boolean validate(String fdata)
    {
      boolean valid = true;

      try
      {
        if(Double.parseDouble(fdata) > 99999999L)
        {
          valid = false;
        }
      }
      catch(Exception e)
      {
      }

      return( valid );
    }
  }

  public class InternetEmailRequiredValidation implements Validation
  {
    private Field   productField = null;
    private String  errorText = "";

    public InternetEmailRequiredValidation(Field productField)
    {
      this.productField = productField;
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String fdata)
    {
      boolean result = true;

      try
      {
        if(fdata.equals(""))
        {
          // email is required if user has selected POS Partner or Internet as a product
          int pType = Integer.parseInt(productField.getData());

          switch(pType)
          {
            case mesConstants.POS_INTERNET:
              result = false;
              errorText = "Email address required for Internet Solution";
              break;
            case mesConstants.POS_PC:
              result = false;
              errorText = "Email address required for POS Partner 2000";
              break;
            //Not sure if this is needed
            /*
            case mesConstants.POS_PAYMENT_GATEWAY:
              result = false;
              errorText = "Email address required for MES Payment Gateway";
              break;
            */
            default:
              break;
          }
        }
      }
      catch(Exception e)
      {
        result = false;
        errorText = e.toString();
      }

      return result;
    }


  }

  public static class EsaRapRequiredValidation implements Validation
  {
    protected Field   AcceptedField       = null;
    protected String  ErrorMessage        = null;
    protected boolean FieldClearRequired  = true;
    protected Field   RateField           = null;

    public EsaRapRequiredValidation( Field accepted, Field rate, boolean fieldClear )
    {
      init( accepted, rate, fieldClear );
    }

    public EsaRapRequiredValidation( Field accepted, Field rate )
    {
      init( accepted, rate, true );
    }

    private void init( Field accepted, Field rate, boolean fieldClear )
    {
      AcceptedField       = accepted;
      FieldClearRequired  = fieldClear;
      RateField           = rate;
    }

    public boolean validate(String fdata)
    {
      ErrorMessage = null;

      try
      {
        if ( AcceptedField.getData().equals("y") )
        {
          if ( RateField.getData().equals("") )
          {
            if ( !isBlank(fdata) && FieldClearRequired )
            {
              ErrorMessage = "Field must be blank when rate not specified";
            }
          }
          else // card rate specified
          {
            if ( isBlank(fdata) )
            {
              ErrorMessage = "Field required when rate specified";
            }
          }
        }
        else  // card not accepted
        {
          if ( !isBlank(fdata) && FieldClearRequired )
          {
            ErrorMessage = "Card not accepted, please clear related fields";
          }
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Invalid card data";
      }
      return( (ErrorMessage == null) );
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }
  }

  public static class AmexAppRequiredValidation implements Validation
  {
    protected Field   AcceptedField   = null;
    protected String  ErrorMessage    = null;
    protected Field   RateField       = null;

    public AmexAppRequiredValidation( Field accepted, Field rate )
    {
      AcceptedField = accepted;
      RateField     = rate;
    }

    public boolean validate(String fdata)
    {
      ErrorMessage = null;

      try
      {
        if ( AcceptedField.getData().equals("y") && !RateField.isBlank() )
        {
          if ( isBlank(fdata) )
          {
            ErrorMessage = "Field required when applying for Amex";
          }
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Validation Error";
      }
      return( (ErrorMessage == null) );
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }
  }

  public static class CheckProviderValidation implements Validation
  {
    protected Field   AcceptedField   = null;
    protected String  ErrorMessage    = null;
    protected Field   OtherProvider   = null;

    public CheckProviderValidation( Field accepted, Field op )
    {
      AcceptedField = accepted;
      OtherProvider = op;
    }

    public boolean validate(String fdata)
    {
      ErrorMessage = null;

      try
      {
        if ( AcceptedField.getData().equals("y") )
        {
          if ( isBlank(fdata) )
          {
            ErrorMessage = "Checks accepted, must specify a check provider";
          }
          else
          {
            if ( fdata.equals("CPOther") && OtherProvider.isBlank() )
            {
              ErrorMessage = "Must specify the name of the check provider";
            }
          }
        }
        else  // checks not accepted
        {
          // the ACR does not allow for the other provider so null
          // must be tested for to prevent exceptions
          if ( !isBlank(fdata) || ( OtherProvider != null && !OtherProvider.isBlank() ) )
          {
            ErrorMessage = "Checks not accepted, please clear check provider fields";
          }
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Must specify a valid check provider";
      }
      return( (ErrorMessage == null) );
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }
  }

  public class DialTerminalRequiredValidation implements Validation
  {
    protected String      ErrorMessage      = null;
    protected Field       ProductTypeField  = null;

    public DialTerminalRequiredValidation( Field productType )
    {
      ProductTypeField  = productType;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      int           ptype     = 0;

      ErrorMessage = null;

      try
      {
        if ( fdata != null && fdata.equals("y") )
        {
          if ( ProductTypeField instanceof RadioButtonField )
          {
            ptype = Integer.parseInt(((RadioButtonField)ProductTypeField).getSelectedButtonValue());
          }
          else
          {
            ptype = ProductTypeField.asInteger();
          }

          if ( ptype != mesConstants.POS_DIAL_TERMINAL )
          {
            String  ptypeStr  = Integer.toString(ptype);

            for ( int i = 0; i < posTypeList.length; ++i )
            {
              if ( posTypeList[i][1].equals( ptypeStr ) )
              {
                ErrorMessage = "Not accepted with '" +
                                 posTypeList[i][0] + "'";
                break;
              }
            }
            if ( ErrorMessage == null )
            {
              ErrorMessage = "Not accepted with the current product selection (" + ptypeStr + ")";
            }
          }
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Not accepted with the current product selection";
      }
      return( (ErrorMessage == null) );
    }
  }

  public static class WelcomeCheckValidation implements Validation
  {
    protected Field   CheckProviderField  = null;
    protected String  ErrorMessage        = null;

    public WelcomeCheckValidation( Field cp )
    {
      CheckProviderField  = cp;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      ErrorMessage = null;

      try
      {
        if ( !CheckProviderField.isBlank() &&
             CheckProviderField.getData().equals("CertegyWelcomeCheck") )
        {
          if ( isBlank(fdata) || fdata.length() != 10 )
          {
            ErrorMessage = "Welcome Check requires a 10 digit Station Number";
          }
        }
      }
      catch( Exception e )
      {
        // ignore exceptions
      }
      return( (ErrorMessage == null) );
    }
  }

  public static class ElecCheckValidation implements Validation
  {
    protected Field   CheckProviderField  = null;
    protected String  ErrorMessage        = null;

    public ElecCheckValidation( Field cp )
    {
      CheckProviderField  = cp;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      ErrorMessage = null;

      try
      {
        if ( !CheckProviderField.isBlank() &&
             CheckProviderField.getData().equals("CertegyElecCheck") )
        {
          if ( isBlank(fdata) || (fdata.length() != 10) )
          {
            ErrorMessage = "Certegy Elec Check requires a 10 digit Station Number";
          }
        }
      }
      catch( Exception e )
      {
        // ignore exceptions
      }
      return( (ErrorMessage == null) );
    }
  }

  public static class ElecCheckTermIdValidation implements Validation
  {
    protected Field   CheckProviderField  = null;
    protected String  ErrorMessage        = null;

    public ElecCheckTermIdValidation( Field cp )
    {
      CheckProviderField  = cp;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      ErrorMessage = null;

      try
      {
        if ( !CheckProviderField.isBlank() &&
             CheckProviderField.getData().equals("CertegyElecCheck") )
        {
          if ( isBlank(fdata) || (fdata.length() != 10) )
          {
            ErrorMessage = "Certegy Elec Check requires a 10 character Terminal ID";
          }
        }
      }
      catch( Exception e )
      {
        // ignore exceptions
      }
      return( (ErrorMessage == null) );
    }
  }

  public static class ValutecTermIdValidation implements Validation
  {
    protected Field   AcceptedField   = null;
    protected String  ErrorMessage    = null;

    public ValutecTermIdValidation( Field accepted )
    {
      AcceptedField = accepted;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      ErrorMessage = null;

      if ( AcceptedField.getData().equals("y") )
      {
        if ( isBlank(fdata) )
        {
          ErrorMessage = "Valutec Terminal ID is required";
        }
      }
      else  // valutec gift card not accepted
      {
        if ( !isBlank(fdata) )
        {
          ErrorMessage = "Valutec Gift Card not accepted, please clear Terminal ID field";
        }
      }
      return( (ErrorMessage == null) );
    }
  }

  public class SPSAcctValidation implements Validation
  {
    protected Field   AcceptedField   = null;
    protected String  ErrorMessage    = null;

    public SPSAcctValidation( Field accepted )
    {
      AcceptedField = accepted;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      ErrorMessage = null;

      if ( AcceptedField.getData().equals("y") )
      {
        if(fdata.length() < 7 || fdata.length() > 10)
        {
          ErrorMessage = "SPS Acct # must be 7 to 10 digits in length";
        }
      }

      return( (ErrorMessage == null) );
    }
  }

  public class ProductAllowsDebitValidation implements Validation
  {
    protected String      ErrorMessage      = null;
    protected Field       ProductTypeField  = null;

    public ProductAllowsDebitValidation( Field productType )
    {
      ProductTypeField  = productType;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      int           ptype     = 0;

      ErrorMessage = null;

      try
      {
        if ( fdata != null && fdata.equals("y") )
        {
          if ( ProductTypeField instanceof RadioButtonField )
          {
            ptype = Integer.parseInt(((RadioButtonField)ProductTypeField).getSelectedButtonValue());
          }
          else
          {
            ptype = ProductTypeField.asInteger();
          }

          if ( ptype == mesConstants.POS_INTERNET )
          {
            String  ptypeStr  = Integer.toString(ptype);

            for ( int i = 0; i < posTypeList.length; ++i )
            {
              if ( posTypeList[i][1].equals( ptypeStr ) )
              {
                ErrorMessage = "Debit not available with '" +
                                 posTypeList[i][0] + "'";
                break;
              }
            }
            if ( ErrorMessage == null )
            {
              ErrorMessage = "Debit not available with the current product selection (" + ptypeStr + ")";
            }
          }
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Debit not available with the current product selection";
      }
      return( (ErrorMessage == null) );
    }
  }

  public class CreditContactRepValidation
    implements Validation
  {
    private Field   cbox      = null;
    
    public CreditContactRepValidation(Field checkbox)
    {
      cbox = checkbox;
    }
    
    public String getErrorText()
    {
      return( "Rep Email Address is required" );
    }
    
    public boolean validate(String fdata)
    {
      boolean result = true;
      
      if(cbox.getData().equals("y"))
      {
        // data is required for this field
        if( fdata == null || fdata.equals("") )
        {
          result = false;
        }
      }
      
      return( result );
    }
  }
  
  public class ExistingMerchantNumberValidation
    implements Validation
  {
    public ExistingMerchantNumberValidation()
    {
    }
    
    public String getErrorText()
    {
      return( "Old Merchant Number does not exist" );
    }
    
    public boolean validate(String fdata)
    {
      boolean result = true;
      
      if( fdata != null && ! fdata.equals("") )
      {
        try
        {
          connect();
        
          int recCount = 0;
        
          /*@lineinfo:generated-code*//*@lineinfo:1125^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//              
//              from    mif
//              where   merchant_number = :fdata
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n             \n            from    mif\n            where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.BusinessBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fdata);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1131^11*/
        
          result = ( recCount > 0 );
        }
        catch(Exception e)
        {
          logEntry("ExistingMerchantNumberValidation()", e.toString());
          result = false;
        }
        finally
        {
          cleanUp();
        }
      }
      
      return( result );
    }
  }
  
  /*************************************************************************
  **
  **   Custom Fields
  **
  **************************************************************************/

  protected class SeasonalFieldGroup extends FieldGroup
  {
    public SeasonalFieldGroup(String fname)
    {
      super(fname);

      String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

      for (int i = 0; i < 12; ++i)
      {
        add(new CheckboxField("seasonalMonth" + i,months[i],false));
      }
    }

    public String getData()
    {
      StringBuffer monthString = new StringBuffer();
      for (int i = 0; i < 12; ++i)
      {
        monthString.append(getData("seasonalMonth" + i).toUpperCase());
      }
      return monthString.toString();
    }

    protected String processData(String rawData)
    {
      String monthString = rawData.toLowerCase();
      for (int i = 0; i < 12; ++i)
      {
        if (rawData.length() > i)
        {
          setData("seasonalMonth" + i,
            (monthString.charAt(i) == 'y' ? "y" : "n"));
        }
      }
      return rawData;
    }

  }

  /*************************************************************************
  **
  **   Field Bean
  **
  **************************************************************************/

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      createBusInfoFields();

      createMerchHistFields();

      createPrimaryOwnerFields();

      createSecondaryOwnerFields();

      createTransInfoFields();

      createAcctInfoFields();

      createPayOptFields();

      createPOSFields();
      
      // override description of existingVerisign so that it is generic
      fields.add(new CheckboxField("existingVerisign",  "Merchant has an existing Internet account",false));
      
      // add check boxes for credit approval contacts
      fields.add(new CheckboxField("creditContactMerchant", "Merchant", false));
      fields.add(new CheckboxField("creditContactRep",      "Bank Rep", false));
      fields.add(new EmailField   ("repEmail",              "Rep Email Address", 75, 30, true));
      
      fields.getField("repEmail").addValidation(new CreditContactRepValidation(fields.getField("creditContactRep")));
      
      fields.add(new NumberField("oldMerchantNumber", "Old Merchant Number", 16, 16, true, 0));
      fields.getField("oldMerchantNumber").addValidation(new ExistingMerchantNumberValidation());

      // add calculate action button
      fields.add(new ButtonField("actionCalculate","Calculate"));

      // set html extra
      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }

  protected void createBusInfoFields()
  throws Exception
  {
    // business info
    FieldGroup gBusInfo = new FieldGroup("gBusInfo");

    gBusInfo.add  (new Field            ("businessName",      "Business Name (DBA)",25,30,false));
    gBusInfo.add  (new Field            ("businessAddress1",  "Business Address Line 1",32,30,false));
    gBusInfo.add  (new Field            ("businessAddress2",  "Business Address Line 2",32,30,true));
    gBusInfo.add  (new CityStateZipField("businessCsz",       "Business City, State, Zip",30,false));
    gBusInfo.add  (new Field            ("businessLegalName", "Business Legal Name",50,30,false));
    gBusInfo.add  (new TaxIdField       ("taxpayerId",        "Federal Taxpayer ID",true));

    gBusInfo.add  (new Field            ("mailingName",       "Mailing Address Name",50,30,true));
    gBusInfo.add  (new Field            ("mailingAddress1",   "Mailing Address Line 1",32,30,true));
    gBusInfo.add  (new Field            ("mailingAddress2",   "Mailing Address Line 2",32,30,true));
    gBusInfo.add  (new CityStateZipField("mailingCsz",        "Mailing City, State, Zip",30,true));

    gBusInfo.add  (new PhoneField       ("businessPhone",     "Business Phone Number",false));
    gBusInfo.add  (new PhoneField       ("businessFax",       "Business Fax Number",true));
    gBusInfo.add  (new EmailField       ("businessEmail",     "Business e-mail Address",45,30,true));
    gBusInfo.add  (new PhoneField       ("contactPhone",      "Contact Phone Number",false));
    gBusInfo.add  (new Field            ("contactPhoneExt",   7,4,true));
    gBusInfo.add  (new NameField        ("contactName",       "Contact Name",30,false,false));
    gBusInfo.add  (new EmailField       ("contactEmail",      75,35,true) );
    gBusInfo.add  (new PhoneField       ("customerServicePhone", "Customer Service Phone Number", true));

    gBusInfo.add  (new DropDownField    ("businessType",      "Type of Business",new BusinessTypeTable(),false));
    gBusInfo.add  (new DropDownField    ("industryType",      "Industry",new IndustryTypeTable(),false));
    gBusInfo.add  (new DropDownField    ("locationType",      "Type of Business Location",new LocationTypeTable(),false));
    gBusInfo.add  (new NumberField      ("locationYears",     "# of Years at Main Location",3,3,false,0));
    gBusInfo.add  (new DateStringField  ("establishedDate",   "Business Established Date",false,true));
    gBusInfo.add  (new TextareaField    ("businessDesc",      "Business Description",200,2,35,false));

    gBusInfo.add  (new DropDownField    ("accountType",       "Application Type",new NewConversionTable(),true));
    gBusInfo.add  (new Field            ("cif",               "CIF #",16,16,true));
    gBusInfo.add  (new DropDownField    ("applicationType",   "Type of Account Setup",new ApplicationTypeTable(),false));
    gBusInfo.add  (new NumberField      ("numLocations",      "# of Locations",3,3,false,0));

    gBusInfo.add  (new HiddenField      ("sicCode"));
    gBusInfo.add  (new HiddenField      ("assocNum"));

    FieldGroup gMultiMerch = new FieldGroup("gMultiMerch");
    gMultiMerch.add(new NumberField     ("multiMerchTotal",         "Total Multi-Merchant Count",2,2,true,0));
    gMultiMerch.add(new NumberField     ("multiMerchNum",           "Multi-Merchant No.",2,2,true,0));
    gMultiMerch.add(new Field           ("multiMerchMasterDba",     "Multi-Merchant Master DBA",25,25,true));
    gMultiMerch.add(new Field           ("multiMerchMasterControl", "Multi-Merchant Master Control No.",16,16,true));

    gBusInfo.add(gMultiMerch);

    AllOrNoneValidation vMultiMerch = new AllOrNoneValidation("Provide all multi-merchant info or none");
    vMultiMerch.addField(gMultiMerch.getField("multiMerchTotal"        ));
    vMultiMerch.addField(gMultiMerch.getField("multiMerchNum"          ));
    vMultiMerch.addField(gMultiMerch.getField("multiMerchMasterDba"    ));
    vMultiMerch.addField(gMultiMerch.getField("multiMerchMasterControl"));
    gMultiMerch.addGroupValidation(vMultiMerch);

    gBusInfo.getField("businessAddress1").addValidation(
      new NotAllowedValidation(new String[]
        { "po box", "p.o. box", "pobox", "p.o.box", "p. o. box",
          "p o box","post office box", "postoffice box", "pob" },
          "Post office boxes not allowed"));

    fields.add(gBusInfo);
  }

  protected void createMerchHistFields()
  throws Exception
  {
    // merchant history
    FieldGroup gMerchHist = new FieldGroup("gMerchHist");

    gMerchHist.add(new RadioButtonField ("haveProcessed",     yesNoRadioList,-1,false,"Required"));
    gMerchHist.add(new Field            ("previousProcessor", "If Yes, Name of Previous Processor",40,30,true));
    gMerchHist.add(new RadioButtonField ("statementsProvided",yesNoRadioList,-1,false,"Required"));
    gMerchHist.add(new RadioButtonField ("haveCanceled",      yesNoRadioList,-1,false,"Required"));
    gMerchHist.add(new Field            ("canceledProcessor", "If Yes, Name of Processor",40,30,true));
    gMerchHist.add(new Field            ("canceledReason",    "Reason for Cancellation",40,30,true));
    gMerchHist.add(new DateStringField  ("cancelDate",        "Date of Cancellation",true,true));
    gMerchHist.add(new Field            ("referringBank",     40,30,true));

    gMerchHist.getField("previousProcessor")
      .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveProcessed"),
        "Please provide the name of the previous processor"));
    gMerchHist.getField("canceledProcessor")
      .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
        "Please provide the name of the cancelling processor"));
    gMerchHist.getField("canceledReason")
      .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
        "Please provide reason account was cancelled"));
    gMerchHist.getField("cancelDate")
      .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
        "Please provide a date of cancellation"));

    fields.add(gMerchHist);
  }

  protected void createPrimaryOwnerFields()
  throws Exception
  {
    // primary owner
    FieldGroup gOwner1 = new FieldGroup("gOwner1");

    gOwner1.add   (new NameField        ("owner1Name",        "Name",30,false,false));
    gOwner1.add   (new TaxIdField       ("owner1SSN",         "Soc. Security #",false));
    gOwner1.add   (new NumberField      ("owner1Percent",     "Percent Owned",3,3,false,0));
    gOwner1.add   (new Field            ("owner1Address1",    "Residence Address",32,30,false));
    gOwner1.add   (new CityStateZipField("owner1Csz",         "City, State, Zip",30,false));
    gOwner1.add   (new PhoneField       ("owner1Phone",       "Phone Number",true));
    gOwner1.add   (new DateStringField  ("owner1Since",       "Owner/Officer Since",true,true));
    gOwner1.add   (new Field            ("owner1Title",       "Title",40,15,true));
    gOwner1.add   (new DropDownField    ("owner1Gender",      "Gender",new GenderTable(),false));
    gOwner1.add   (new Field            ("owner1DlNumber",    "Drivers License Number", 40, 13, true));
    gOwner1.add   (new DateStringField  ("owner1DlExpDate",   "Exp Date", true, false));
    gOwner1.add   (new DateStringField  ("owner1DlIssueDate", "Issue Date", true, false));
    gOwner1.add   (new DropDownField    ("owner1DlState",     "Issue State", new StateDropDownTable(), true));
    gOwner1.add   (new DateStringField  ("owner1DlDob",       "Date of Birth", true, false));

    gOwner1.getField("owner1Percent")
      .addValidation(new PercentValidation(
        "Primary owner's percentage of ownership is invalid",true));

    fields.add(gOwner1);
  }

  protected void createSecondaryOwnerFields()
  throws Exception
  {
    // secondary owner
    FieldGroup gOwner2 = new FieldGroup("gOwner2");

    gOwner2.add   (new NameField        ("owner2Name",        "Name",30,true,false));
    gOwner2.add   (new TaxIdField       ("owner2SSN",         "Soc. Security #",true));
    gOwner2.add   (new NumberField      ("owner2Percent",     "Percent Owned",3,3,true,0));
    gOwner2.add   (new Field            ("owner2Address1",    "Residence Address",32,30,true));
    gOwner2.add   (new CityStateZipField("owner2Csz",         "City, State, Zip",30,true));
    gOwner2.add   (new PhoneField       ("owner2Phone",       "Phone Number",true));
    gOwner2.add   (new DateStringField  ("owner2Since",       "Owner/Officer Since",true,true));
    gOwner2.add   (new Field            ("owner2Title",       "Title",40,15,true));
    gOwner2.add   (new Field            ("owner2DlNumber",    "Drivers License Number", 40, 13, true));
    gOwner2.add   (new DateStringField  ("owner2DlExpDate",   "Exp Date", true, false));
    gOwner2.add   (new DateStringField  ("owner2DlIssueDate", "Issue Date", true, false));
    gOwner2.add   (new DropDownField    ("owner2DlState",     "Issue State", new StateDropDownTable(), true));
    gOwner2.add   (new DateStringField  ("owner2DlDob",       "Date of Birth", true, false));

    gOwner2.getField("owner2Percent")
      .addValidation(new PercentValidation(
        "Secondary owner's percentage of ownership is invalid",true));

    fields.add(gOwner2);
  }

  protected void createTransInfoFields()
  throws Exception
  {
    // transaction info
    FieldGroup gTranInfo = new FieldGroup("gTranInfo");

    gTranInfo.add (new CurrencyField    ("monthlySales",      "Total Estimated Monthly Sales",11,9,false));
    gTranInfo.add (new CurrencyField    ("monthlyVMCSales",   "Total Estimated Monthly Visa/MC Sales",11,9,false));
    gTranInfo.add (new ButtonField      ("recalculate",       "Calculate Total Estimated Annual Sales","Calculate"));
    gTranInfo.add (new CurrencyField    ("averageTicket",     "Estimated Average Credit Card Ticket",11,9,false));
    gTranInfo.add (new NumberField      ("motoPercentage",    "% Mail/Telephone/Internet Sales",3,3,false,0));
    gTranInfo.add (new DropDownField    ("refundPolicy",      "Refund Policy",new RefundPolicyTable(),false));
    gTranInfo.add (new SeasonalFieldGroup("seasonalMonths"));

    gTranInfo.add (new NumberField      ("internetPercent",   "% Internet Sales",3,3,true,0));
    gTranInfo.add (new NumberField      ("cardPercent",       "% Sales Cardholder Present",3,3,true,0));
    gTranInfo.add (new NumberField      ("noCardPercent",     "% Sales Cardholder Not Present",3,3,true,0));

    // some special annual fields for those who like them (CB&T)
    gTranInfo.add (new CurrencyField    ("annualSales",     "Total Annual Gross Sales",11,9,true));
    gTranInfo.add (new CurrencyField    ("annualVMCSales",  "Estimated Annual Gross V/MC Sales",11,9,true));
    gTranInfo.add (new CurrencyField    ("highestTicket",   "Estimated Highest Ticket",11,9,true));

    gTranInfo.getField("motoPercentage")
      .addValidation(new PercentValidation(
        "Please provide a valid mail order/telephone order percentage"));

    if(useAnnualVolume)
    {
      gTranInfo.getField("annualSales")
        .addValidation(new AnnualAmountValidation() );
      gTranInfo.getField("annualVMCSales")
        .addValidation(new AnnualAmountValidation() );
    }

    fields.add(gTranInfo);
  }

  protected void createAcctInfoFields()
  throws Exception
  {
    // bank account info
    FieldGroup gBankAcct = new FieldGroup("gBankAcct");

    gBankAcct.add (new Field            ("bankName",          "Name of Bank",30,30,false));
    gBankAcct.add (new NumberField      ("yearsOpen",         "Years Open",3,6,true,0));
    gBankAcct.add (new Field            ("checkingAccount",   "Checking Acct. #",17,15,false));
    gBankAcct.add (new Field            ("confirmCheckingAccount","Confirm Account #",17,15,false));
    gBankAcct.add (new Field            ("transitRouting",    "Transit Routing #",9,15,false));
    gBankAcct.add (new Field            ("confirmTransitRouting","Confirm Transit Routing #",9,15,false));
    gBankAcct.add (new DropDownField    ("sourceOfInfo",      "Source of Acct. Info",new AccountInfoSourceTable(),false));
    gBankAcct.add (new Field            ("bankAddress",       "Bank Address",32,30,false));
    gBankAcct.add (new CityStateZipField("bankCsz",           "City, State, Zip",30,false));

    gBankAcct.add (new PhoneField       ("bankPhone",         "Bank Phone",true));
    gBankAcct.add (new Field            ("referringBank",     "Referring Bank",40,30,true));
    gBankAcct.add (new HiddenField      ("typeOfAcct"));
    gBankAcct.add (new HiddenField      ("billingMethod"));

    gBankAcct.getField("typeOfAcct").setData("2");
    gBankAcct.getField("billingMethod").setData("Debit"); // default

    gBankAcct.getField("transitRouting")
      .addValidation(new TransitRoutingValidation());

    fields.add(gBankAcct);
  }

  protected void createPayOptFields()
  throws Exception
  {
    // payment options
    FieldGroup gPayOpts = new FieldGroup("gPayOpts");

    gPayOpts.add  (new DisabledCheckboxField("vmcAccepted",   "Visa/MasterCard",true));
    gPayOpts.add  (new HiddenField      ("vmcAcceptTypes"));

    gPayOpts.add  (new CheckboxField    ("debitAccepted",     "Debit with Pin",false));
    gPayOpts.add  (new CheckboxField    ("ebtAccepted",       "EBT",false));
    gPayOpts.add  (new Field            ("fcsNumber",         "FCS #",7,25,true));
    gPayOpts.add  (new DropDownField    ("ebtOptions",        "EBT Options",new EbtOptionsTable(),true));
    gPayOpts.add  (new CheckboxField    ("amexAccepted",      "American Express",false));
    gPayOpts.add  (new CheckboxField    ("amexSplitDial",     "Split Dial",false));
    gPayOpts.add  (new CheckboxField    ("amexPip",           "PIP",false));
    gPayOpts.add  (new NumberField      ("amexEsaRate",       "Amex ESA Rate",5,5,true,2));
    gPayOpts.add  (new NumberField      ("amexAcctNum",       "Amex Acct #",16,25,true,0));
    gPayOpts.add  (new CheckboxField    ("discoverAccepted",  "Discover",false));
    gPayOpts.add  (new NumberField      ("discoverRapRate",   "Disc RAP Rate",5,5,true,2));
    gPayOpts.add  (new NumberField      ("discoverAcctNum",   "Disc Acct #",15,25,true,0));
    gPayOpts.add  (new CheckboxField    ("dinersAccepted",    "Diners Club",false));
    gPayOpts.add  (new NumberField      ("dinersAcctNum",     "Diners Acct #",10,25,true,0) );
    gPayOpts.add  (new CheckboxField    ("jcbAccepted",       "JCB",false));
    gPayOpts.add  (new NumberField      ("jcbAcctNum",        "JCB Acct #",25,25,true,0));
    gPayOpts.add  (new CheckboxField    ("checkAccepted",     "Checks",false));
    gPayOpts.add  (new DropDownField    ("checkProvider",     "Check Provider",new CheckProviderTable(),true));
    gPayOpts.add  (new Field            ("checkProviderOther","Provider Name",25,25,true));
    gPayOpts.add  (new Field            ("checkAcctNum",      "Acct # / Station #",25,25,true));
    gPayOpts.add  (new Field            ("checkTermId",       "Elec Check Terminal ID",25,25,true));
    gPayOpts.add  (new CheckboxField    ("valutecAccepted",   "Valutec Gift Card",false));
    gPayOpts.add  (new Field            ("valutecAcctNum",    "Valutec Merchant #",25,25,true));
    gPayOpts.add  (new NumberField      ("valutecTermId",     "Valutec Terminal ID",10,25,true,0));
    gPayOpts.add  (new CheckboxField    ("spsAccepted",       "Secure Payment Systems",false));
    gPayOpts.add  (new NumberField      ("spsAcctNum",        "SPS Acct #", 10, 10, true,0));

    gPayOpts.getField("valutecTermId")
      .addValidation( new ValutecTermIdValidation( gPayOpts.getField("valutecAccepted") ) );
    gPayOpts.getField("valutecTermId").setMinLength(5);

    gPayOpts.getField("checkProvider")
      .addValidation( new CheckProviderValidation( gPayOpts.getField("checkAccepted"),
                                                   gPayOpts.getField("checkProviderOther") ) );

    gPayOpts.getField("spsAcctNum").
      addValidation(new SPSAcctValidation(gPayOpts.getField("spsAccepted")));

    // insure proper lengths for check tids
    gPayOpts.getField("checkAcctNum")
      .addValidation( new WelcomeCheckValidation( gPayOpts.getField("checkProvider") ) );
    gPayOpts.getField("checkAcctNum")
      .addValidation( new ElecCheckValidation( gPayOpts.getField("checkProvider") ) );
    gPayOpts.getField("checkTermId")
      .addValidation( new ElecCheckTermIdValidation( gPayOpts.getField("checkProvider") ) );

    CardAcceptedValidation amexVal = new CardAcceptedValidation();
    amexVal.addField(gPayOpts.getField("amexAcctNum"));
    amexVal.addField(gPayOpts.getField("amexEsaRate"));
    gPayOpts.getField("amexAccepted").addValidation(amexVal, "amexValidation");

    OnlyOneValidation amexOnlyOneVal = new OnlyOneValidation(
      "Please select either Amex Split Dial or PIP, not both");
    amexOnlyOneVal.addField(gPayOpts.getField("amexSplitDial"));
    amexOnlyOneVal.addField(gPayOpts.getField("amexPip"));
    gPayOpts.getField("amexAccepted").addValidation(amexOnlyOneVal);

    CardAcceptedValidation discVal = new CardAcceptedValidation();
    discVal.addField(gPayOpts.getField("discoverAcctNum"));
    discVal.addField(gPayOpts.getField("discoverRapRate"));
    gPayOpts.getField("discoverAccepted").addValidation(discVal, "discoverValidation");

    CardAcceptedValidation dinersVal = new CardAcceptedValidation();
    dinersVal.addField(gPayOpts.getField("dinersAcctNum"));
    gPayOpts.getField("dinersAccepted").addValidation(dinersVal);

    CardAcceptedValidation jcbVal = new CardAcceptedValidation();
    jcbVal.addField(gPayOpts.getField("jcbAcctNum"));
    gPayOpts.getField("jcbAccepted").addValidation(jcbVal);

    CardAcceptedValidation spsVal = new CardAcceptedValidation();
    spsVal.addField(gPayOpts.getField("spsAcctNum"));
    gPayOpts.getField("spsAccepted").addValidation(spsVal);

    gPayOpts.getField("vmcAcceptTypes").setData("2");

    fields.add(gPayOpts);
  }

  protected void createPOSFields()
  throws Exception
  {
    // pos product
    FieldGroup gProduct = new FieldGroup("gProduct");

    gProduct.add  (new RadioButtonField("productType",      posTypeList,-1,false,"Please specify a product type"));
    gProduct.add  (new CheckboxField  ("rentalEquipment",   "Rental Equipment",false));
    gProduct.add  (new CheckboxField  ("buyEquipment",      "Buy Equipment",false));
    gProduct.add  (new CheckboxField  ("leaseEquipment",    "Lease Equipment",false));
    gProduct.add  (new CheckboxField  ("ownEquipment",      "Owned Equipment",false));
    gProduct.add  (new EmailField     ("vtEmail",           100,45,false));
    gProduct.add  (new EmailField     ("vtLimitedEmail",    100,45,false));
    gProduct.add  (new EmailField     ("internetEmail",     100,45,false));
    gProduct.add  (new DropDownField  ("internetType",      new InternetProductTable(),false));
    gProduct.add  (new CheckboxField  ("existingVerisign",  "Merchant has an existing PayPal/Cybersource/Auth.net account",false));
    gProduct.add  (new Field          ("webUrl",            100,45,false));
    gProduct.add  (new Field          ("vitalProduct",      40,45,false));
    gProduct.add  (new NumberField    ("imprinterPlates",   2,3,true,0));
    gProduct.add  (new TextareaField  ("comments",          4000,2,80,true));
    gProduct.add  (new CheckboxField  ("existingPCChargeExpress", "Merchant has existing PC Charge Express for Windows", false));
    gProduct.add  (new CheckboxField  ("existingPCChargePro", "Merchant has existing PC Charge Pro for Windows", false));
    gProduct.add  (new CheckboxField  ("existingInternet", "Merchant has existing Internet Solution", false));
    //new Trident product 6-07
    gProduct.add  (new EmailField     ("pgEmail",       100,45,false));
    gProduct.add  (new Field          ("pgCertName",    100,45,false));
    gProduct.add  (new RadioButtonField("pgCard", cardList, -1, false, "Processing Options"));


    RadioButtonField productType =
      (RadioButtonField)gProduct.getField("productType");
      
    // add OnClick for Virtual Terminal Limited radio button
    productType.setOnClick(12, "vtLimitedAlert()");

    gProduct.getField("internetType").setOptionalCondition(
      new FieldValueCondition(productType,
        Integer.toString(mesConstants.POS_INTERNET)));
    gProduct.getField("internetEmail").setOptionalCondition(
      new FieldValueCondition(productType,
        Integer.toString(mesConstants.POS_INTERNET)));
    gProduct.getField("webUrl").setOptionalCondition(
      new FieldValueCondition(productType,
        Integer.toString(mesConstants.POS_INTERNET)));

    gProduct.getField("vtEmail").setOptionalCondition(
      new FieldValueCondition(productType,
        Integer.toString(mesConstants.POS_VIRTUAL_TERMINAL)));
        
    gProduct.getField("vtLimitedEmail").setOptionalCondition(
      new FieldValueCondition(productType,
        Integer.toString(mesConstants.POS_VT_LIMITED)));

    gProduct.getField("vitalProduct").setOptionalCondition(
      new FieldValueCondition(productType,
        Integer.toString(mesConstants.POS_OTHER)));

    //new Trident product 6-07
    gProduct.getField("pgCertName").setOptionalCondition(
      new FieldValueCondition(productType,
        Integer.toString(mesConstants.POS_PAYMENT_GATEWAY)));
    gProduct.getField("pgEmail").setOptionalCondition(
      new FieldValueCondition(productType,
        Integer.toString(mesConstants.POS_PAYMENT_GATEWAY)));
    gProduct.getField("pgCard").setOptionalCondition(
      new FieldValueCondition(productType,
        Integer.toString(mesConstants.POS_PAYMENT_GATEWAY)));
    // add validations for check and valutec gift card
    // need to be done after productType has been instantiated
    FieldGroup gPayOpts = (FieldGroup)getField("gPayOpts");

    gPayOpts.getField("valutecAccepted")
      .addValidation( new DialTerminalRequiredValidation( gProduct.getField("productType") ) );
    gPayOpts.getField("checkAccepted")
      .addValidation( new DialTerminalRequiredValidation( gProduct.getField("productType") ) );
    gPayOpts.getField("debitAccepted")
      .addValidation( new ProductAllowsDebitValidation( gProduct.getField("productType") ) );

    fields.add(gProduct);
  }




  protected void createPayOptsExtendedFields()
  {
    Validation val = null;

    try
    {
      FieldGroup gPayOpts = (FieldGroup)getField("gPayOpts");

      // setup the amex fields
      gPayOpts.add  (new CurrencyField  ("amexAnnualSales",   "Est Annual Amex Sales",11,9,true));
      gPayOpts.add  (new CurrencyField  ("amexAvgTicket",     "Est Average Amex Ticket",11,9,true));
      gPayOpts.add  (new CheckboxField  ("amexFlatFee",       "Monthly Flat Fee < $4900",false));
      gPayOpts.add  (new NumberField    ("amexFranchiseSE",   "Franchise #",10,25,true,0));


      // setup annual sales as required for ESA app
      val = new EsaRapRequiredValidation(gPayOpts.getField("amexAccepted"),
                                         gPayOpts.getField("amexEsaRate"));
      gPayOpts.getField("amexAnnualSales").addValidation(val);

      // setup average ticket as required for ESA app
      val = new EsaRapRequiredValidation(gPayOpts.getField("amexAccepted"),
                                         gPayOpts.getField("amexEsaRate"));
      gPayOpts.getField("amexAvgTicket").addValidation(val);

      gPayOpts.getField("amexFlatFee").addValidation( new AmexFlatFeeValidation( gPayOpts.getField("amexAnnualSales") ) );

      // owner title is required for Amex ESA application
      val = new AmexAppRequiredValidation(gPayOpts.getField("amexAccepted"),
                                          gPayOpts.getField("amexEsaRate"));
      fields.getField("owner1Title").addValidation(val);

      // setup the discover fields
      gPayOpts.add( new CurrencyField     ("discoverAnnualSales", "Est Annual Discover Sales",11,9,true));
      gPayOpts.add( new CurrencyField     ("discoverAvgTicket",   "Est Average Discover Ticket",11,9,true));
      gPayOpts.add( new NumberField       ("discoverCapNumber",   "CAP #",16,25,true,0));
      gPayOpts.add( new SmallCurrencyField("discoverRapPerItem",5,6,true) );

      // setup annual sales as required for RAP app
      val = new EsaRapRequiredValidation(gPayOpts.getField("discoverAccepted"),
                                         gPayOpts.getField("discoverRapRate"));
      gPayOpts.getField("discoverAnnualSales").addValidation(val);

      // setup average ticket as required for RAP app
      val = new EsaRapRequiredValidation(gPayOpts.getField("discoverAccepted"),
                                         gPayOpts.getField("discoverRapRate"));
      gPayOpts.getField("discoverAvgTicket").addValidation(val);


      // set the default and make RAP per item required
      gPayOpts.setData("discoverRapPerItem","0.10");
      val = new EsaRapRequiredValidation(gPayOpts.getField("discoverAccepted"),
                                         gPayOpts.getField("discoverRapRate"),
                                         false);
      gPayOpts.getField("discoverRapPerItem").addValidation(val);

    }
    catch(Exception e)
    {
      logEntry("createPayOptsExtendedFields()", e.toString());
    }
  }

  protected void createPosPartnerExtendedFields()
  {
    // turn on validations for extended POS Partner fields
    try
    {
      // POS Partner-specific fields
      FieldGroup gPosPartner = new FieldGroup("gPosPartner");
      gPosPartner.add  (new RadioButtonField ("posPartnerConnectivity", posPartnerList, -1, false, "Please select the POS Partner connectivity"));
      gPosPartner.add  (new CheckboxField    ("posPartnerLevelII", "Level II", false));
      gPosPartner.add  (new CheckboxField    ("posPartnerLevelIII", "Level III", false));
      gPosPartner.add  (new DropDownField    ("posPartnerOS", new PosPartnerOSTable(), false));

      fields.add(gPosPartner);

      RadioButtonField productType =
        (RadioButtonField)fields.getField("productType");


      fields.getField("posPartnerConnectivity").setOptionalCondition(
        new FieldValueCondition(productType, Integer.toString(mesConstants.POS_PC)));
      fields.getField("posPartnerOS").setOptionalCondition(
        new FieldValueCondition(productType, Integer.toString(mesConstants.POS_PC)));

      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("createPosPartnerExtendedFields()", e.toString());
    }
  }

  protected void setAnnualVolume( boolean flag )
  {
    useAnnualVolume = flag;
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);

    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }

    // set monthly volume data from annual volume data if necessary
    if(useAnnualVolume)
    {
      setData("monthlySales", Double.toString(MesMath.round(getField("annualSales").asDouble()/12.0, 2)));
      setData("monthlyVMCSales", Double.toString(MesMath.round(getField("annualVMCSales").asDouble()/12.0, 2)));
    }
    else
    {
      setData("annualSales", NumberFormatter.getDoubleString(MesMath.round(getField("monthlySales").asDouble()*12.0,2), "########0.00"));
      setData("annualVMCSales", NumberFormatter.getDoubleString(MesMath.round(getField("monthlyVMCSales").asDouble()*12.0,2), "########0.00"));
    }
  }

  /*************************************************************************
  **
  **   Helper Methods
  **
  **************************************************************************/

  /*
  ** public String getAnnualSalesString()
  **
  ** RETURNS: String containing the annual sales volume of the merchant.
  */
  public String getAnnualSalesString()
  {
    StringBuffer annualSalesBuf = new StringBuffer();
    try
    {
      double annualSales = fields.getField("annualSales").asDouble();
      if (annualSales != 0)
      {
        annualSalesBuf.append(MesMath.toCurrency(annualSales));
      }
    }
    catch(NumberFormatException e) {}
    catch(Exception e)
    {
      logEntry("getAnnualSalesString()",e.toString());
    }
    return annualSalesBuf.toString();
  }

  /*
  ** public String getAnnualVmcSalesString()
  **
  ** Calculates the annual V/MC sales from the estimated monthly sales if
  ** given.
  **
  ** RETURNS: String containing the annual V/MC sales volume of the merchant.
  */
  public String getAnnualVmcSalesString()
  {
    StringBuffer annualSalesBuf = new StringBuffer();
    try
    {
      double annualSales = fields.getField("annualVMCSales").asDouble();
      if (annualSales != 0)
      {
        annualSalesBuf.append(MesMath.toCurrency(annualSales));
      }
    }
    catch(NumberFormatException e) {}
    catch(Exception e)
    {
      logEntry("getAnnualVmcSalesString()",e.toString());
    }
    return annualSalesBuf.toString();
  }

  /*
  ** public String getMonthlySalesString()
  **
  ** Calculates the monthly sales from the estimated monthly sales if given.
  **
  ** RETURNS: String containing the annual sales volume of the merchant.
  */
  public String getMonthlySalesString()
  {
    StringBuffer monthlySalesBuf = new StringBuffer();
    try
    {
      double monthlySales = getField("monthlySales").asDouble();
      if (monthlySales != 0)
      {
        monthlySalesBuf.append(MesMath.toCurrency(monthlySales));
      }
    }
    catch(NumberFormatException e) {}
    catch(Exception e)
    {
      logEntry("getMonthlySalesString()",e.toString());
    }
    return monthlySalesBuf.toString();
  }

  /*
  ** public String getMonthlyVmcSalesString()
  **
  ** Calculates the monthly V/MC sales from the estimated monthly sales if
  ** given.
  **
  ** RETURNS: String containing the monthly V/MC sales volume of the merchant.
  */
  public String getMonthlyVmcSalesString()
  {
    StringBuffer monthlySalesBuf = new StringBuffer();
    try
    {
      double monthlySales = fields.getField("monthlyVMCSales").asDouble();
      if (monthlySales != 0)
      {
        monthlySalesBuf.append(MesMath.toCurrency(monthlySales));
      }
    }
    catch(NumberFormatException e) {}
    catch(Exception e)
    {
      logEntry("getMonthlyVmcSalesString()",e.toString());
    }
    return monthlySalesBuf.toString();
  }

  /*
  ** public String getAnnualVmcTranCountString()
  **
  ** Calculates the annual V/MC sales from the estimated monthly sales if
  ** given.
  **
  ** RETURNS: String containing the annual V/MC sales volume of the merchant.
  */
  public String getAnnualVmcTranCountString()
  {
    StringBuffer annualCountBuf = new StringBuffer();
    try
    {
      double monthlySales = fields.getField("monthlyVMCSales").asDouble();
      double averageTicket = fields.getField("averageTicket").asDouble();
      if (monthlySales != 0 && averageTicket != 0)
      {
        int tranCount = (int)((monthlySales * 12) / averageTicket);
        annualCountBuf.append(tranCount);
      }
    }
    catch(NumberFormatException e) {}
    catch(Exception e)
    {
      logEntry("getAnnualVmcTranCountString()",e.toString());
    }
    return annualCountBuf.toString();
  }

  /*************************************************************************
  **
  **   Submit
  **
  **************************************************************************/

  public static final int     FIDX_ADDR_NAME              = 0;
  public static final int     FIDX_ADDR_LINE1             = 1;
  public static final int     FIDX_ADDR_LINE2             = 2;
  public static final int     FIDX_ADDR_CITY              = 3;
  public static final int     FIDX_ADDR_STATE             = 4;
  public static final int     FIDX_ADDR_ZIP               = 5;
  public static final int     FIDX_ADDR_COUNTRY           = 6;
  public static final int     FIDX_ADDR_PHONE             = 7;
  public static final int     FIDX_ADDR_FAX               = 8;

  /*
  ** protected void submitAddressData(int addressType, Field[] addrFields)
  **   throws Exception
  **
  ** Given an address type and an array of fields containing an address
  ** this method will store an address in the address table.
  */
  protected void submitAddressData(int addressType, Field[] addrFields)
    throws Exception
  {
    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();

      // don't insert if city or address line 1 is missing
      Field addr1 = addrFields[FIDX_ADDR_LINE1];
      Field city  = addrFields[FIDX_ADDR_CITY];
      if (addr1 != null && !addr1.isBlank() && city != null && !city.isBlank())
      {
        /*@lineinfo:generated-code*//*@lineinfo:1940^9*/

//  ************************************************************
//  #sql [Ctx] { delete from address
//            where app_seq_num = :appSeqNum
//                  and addresstype_code = :addressType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from address\n          where app_seq_num =  :1 \n                and addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,addressType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1945^9*/

        /*@lineinfo:generated-code*//*@lineinfo:1947^9*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//            ( address_name,
//              address_line1,
//              address_line2,
//              address_city,
//              countrystate_code,
//              address_zip,
//              country_code,
//              address_phone,
//              address_fax,
//              app_seq_num,
//              addresstype_code )
//            values
//            ( substr(:(addrFields[FIDX_ADDR_NAME]   == null) ? null : addrFields[FIDX_ADDR_NAME].getData(), 1, 32),
//              substr(:(addrFields[FIDX_ADDR_LINE1]  == null) ? null : addrFields[FIDX_ADDR_LINE1].getData(), 1, 32),
//              substr(:(addrFields[FIDX_ADDR_LINE2]  == null) ? null : addrFields[FIDX_ADDR_LINE2].getData(), 1, 32),
//              substr(:(addrFields[FIDX_ADDR_CITY]   == null) ? null : addrFields[FIDX_ADDR_CITY].getData(), 1, 25),
//              substr(:(addrFields[FIDX_ADDR_STATE]  == null) ? null : addrFields[FIDX_ADDR_STATE].getData(), 1, 2),
//              substr(:(addrFields[FIDX_ADDR_ZIP]    == null) ? null : addrFields[FIDX_ADDR_ZIP].getData(), 1, 10),
//              substr(:(addrFields[FIDX_ADDR_COUNTRY]== null) ? null : addrFields[FIDX_ADDR_COUNTRY].getData(), 1, 2),
//              substr(:(addrFields[FIDX_ADDR_PHONE]  == null) ? null : addrFields[FIDX_ADDR_PHONE].getData(), 1, 10),
//              substr(:(addrFields[FIDX_ADDR_FAX]    == null) ? null : addrFields[FIDX_ADDR_FAX].getData(), 1, 10),
//              :appSeqNum,
//              :addressType )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1941 = (addrFields[FIDX_ADDR_NAME]   == null) ? null : addrFields[FIDX_ADDR_NAME].getData();
 String __sJT_1942 = (addrFields[FIDX_ADDR_LINE1]  == null) ? null : addrFields[FIDX_ADDR_LINE1].getData();
 String __sJT_1943 = (addrFields[FIDX_ADDR_LINE2]  == null) ? null : addrFields[FIDX_ADDR_LINE2].getData();
 String __sJT_1944 = (addrFields[FIDX_ADDR_CITY]   == null) ? null : addrFields[FIDX_ADDR_CITY].getData();
 String __sJT_1945 = (addrFields[FIDX_ADDR_STATE]  == null) ? null : addrFields[FIDX_ADDR_STATE].getData();
 String __sJT_1946 = (addrFields[FIDX_ADDR_ZIP]    == null) ? null : addrFields[FIDX_ADDR_ZIP].getData();
 String __sJT_1947 = (addrFields[FIDX_ADDR_COUNTRY]== null) ? null : addrFields[FIDX_ADDR_COUNTRY].getData();
 String __sJT_1948 = (addrFields[FIDX_ADDR_PHONE]  == null) ? null : addrFields[FIDX_ADDR_PHONE].getData();
 String __sJT_1949 = (addrFields[FIDX_ADDR_FAX]    == null) ? null : addrFields[FIDX_ADDR_FAX].getData();
   String theSqlTS = "insert into address\n          ( address_name,\n            address_line1,\n            address_line2,\n            address_city,\n            countrystate_code,\n            address_zip,\n            country_code,\n            address_phone,\n            address_fax,\n            app_seq_num,\n            addresstype_code )\n          values\n          ( substr( :1 , 1, 32),\n            substr( :2 , 1, 32),\n            substr( :3 , 1, 32),\n            substr( :4 , 1, 25),\n            substr( :5 , 1, 2),\n            substr( :6 , 1, 10),\n            substr( :7 , 1, 2),\n            substr( :8 , 1, 10),\n            substr( :9 , 1, 10),\n             :10 ,\n             :11  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1941);
   __sJT_st.setString(2,__sJT_1942);
   __sJT_st.setString(3,__sJT_1943);
   __sJT_st.setString(4,__sJT_1944);
   __sJT_st.setString(5,__sJT_1945);
   __sJT_st.setString(6,__sJT_1946);
   __sJT_st.setString(7,__sJT_1947);
   __sJT_st.setString(8,__sJT_1948);
   __sJT_st.setString(9,__sJT_1949);
   __sJT_st.setLong(10,appSeqNum);
   __sJT_st.setInt(11,addressType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1973^9*/
      }
    }
    catch(Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitAddressData()";
      System.out.println(methodSig + ": " + e.toString());
      logEntry("submitAddressData (" + addressType + "): ", e.toString());
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  /*
  ** protected void submitMerchant(long appSeqNum) throws Exception
  **
  ** Stores most of the data that needs to go in the merchant table.
  */
  protected void submitMerchant(long appSeqNum) throws Exception
  {
    try
    {
      String seasonalMonths = fields.getData("seasonalMonths");
      String seasonalFlag
        = (seasonalMonths.equals("NNNNNNNNNNNN") ? "N" : "Y");

      // set monthly volume fields from annual data if necessary
      if( useAnnualVolume )
      {
        setData("monthlySales", Double.toString(MesMath.round(getField("annualSales").asDouble() / 12.0, 2)));
        setData("monthlyVMCSales", Double.toString(MesMath.round(getField("annualVMCSales").asDouble() / 12.0, 2)));
      }
      else
      {
        setData("annualSales", Double.toString(MesMath.round(getField("monthlySales").asDouble() * 12.0,2)));
        setData("annualVMCSales", Double.toString(MesMath.round(getField("monthlyVMCSales").asDouble() * 12.0,2)));
      }

      /*@lineinfo:generated-code*//*@lineinfo:2010^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_business_name         = :fields.getData("businessName"),
//                  merch_federal_tax_id        = :fields.getData("taxpayerId"),
//                  merch_email_address         = :fields.getData("businessEmail"),
//                  merch_legal_name            = :fields.getData("businessLegalName"),
//                  merch_mailing_name          = :fields.getData("mailingName"),
//                  merch_business_establ_month = :fields.getData("establishedDateMonth"),
//                  merch_business_establ_year  = :fields.getData("establishedDateYear"),
//                  bustype_code                = :fields.getData("businessType"),
//                  industype_code              = :fields.getData("industryType"),
//                  merch_busgoodserv_descr     = :fields.getData("businessDesc"),
//                  merch_num_of_locations      = :fields.getData("numLocations"),
//                  merch_years_at_loc          = :fields.getData("locationYears"),
//                  loctype_code                = :fields.getData("locationType"),
//                  merch_application_type      = :fields.getData("applicationType"),
//                  merch_prior_cc_accp_flag    = :fields.getData("haveProcessed"),
//                  merch_prior_processor       = :fields.getData("previousProcessor"),
//                  merch_cc_acct_term_flag     = :fields.getData("haveCanceled"),
//                  merch_cc_term_name          = :fields.getData("canceledProcessor"),
//                  merch_term_reason           = :fields.getData("canceledReason"),
//                  merch_term_month            = :fields.getData("cancelDateMonth"),
//                  merch_term_year             = :fields.getData("cancelDateYear"),
//                  merch_month_tot_proj_sales  = :fields.getData("monthlySales"),
//                  merch_month_visa_mc_sales   = :fields.getData("monthlyVMCSales"),
//                  annual_sales                = :fields.getData("annualSales"),
//                  annual_vmc_sales            = :fields.getData("annualVMCSales"),
//                  merch_average_cc_tran       = :fields.getData("averageTicket"),
//                  highest_ticket              = :fields.getData("highestTicket"),
//                  merch_mail_phone_sales      = :fields.getData("motoPercentage"),
//                  refundtype_code             = :fields.getData("refundPolicy"),
//                  merch_referring_bank        = :fields.getData("referringBank"),
//                  merch_prior_statements      = :fields.getData("statementsProvided"),
//                  merch_gender                = :fields.getData("owner1Gender"),
//                  asso_number                 = :fields.getData("assocNum"),
//                  account_type                = :fields.getData("accountType"),
//                  svb_cif_num                 = :fields.getData("cif"),
//                  app_sic_code                = :fields.getData("sicCode"),
//                  seasonal_flag               = :seasonalFlag,
//                  seasonal_months             = :seasonalMonths,
//                  ebt_options                 = :fields.getData("ebtOptions"),
//                  percent_internet            = :fields.getData("internetPercent"),
//                  percent_card_present        = :fields.getData("cardPercent"),
//                  percent_card_not_present    = :fields.getData("noCardPercent"),
//                  merch_web_url               = :fields.getData("webUrl"),
//                  merch_notes                 = :fields.getData("comments"),
//                  existing_verisign           = :fields.getData("existingVerisign"),
//                  total_num_multi_merch       = :fields.getData("multiMerchTotal"),
//                  num_multi_merch             = :fields.getData("multiMerchNum"),
//                  multi_merch_master_dba      = :fields.getData("multiMerchMasterDba"),
//                  multi_merch_master_cntrl    = :fields.getData("multiMerchMasterControl"),
//                  vmc_accept_types            = :fields.getData("vmcAcceptTypes"),
//                  customer_service_phone      = :fields.getData("customerServicePhone"),
//                  old_merchant_number         = :fields.getData("oldMerchantNumber")
//          where   app_seq_num                 = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1950 = fields.getData("businessName");
 String __sJT_1951 = fields.getData("taxpayerId");
 String __sJT_1952 = fields.getData("businessEmail");
 String __sJT_1953 = fields.getData("businessLegalName");
 String __sJT_1954 = fields.getData("mailingName");
 String __sJT_1955 = fields.getData("establishedDateMonth");
 String __sJT_1956 = fields.getData("establishedDateYear");
 String __sJT_1957 = fields.getData("businessType");
 String __sJT_1958 = fields.getData("industryType");
 String __sJT_1959 = fields.getData("businessDesc");
 String __sJT_1960 = fields.getData("numLocations");
 String __sJT_1961 = fields.getData("locationYears");
 String __sJT_1962 = fields.getData("locationType");
 String __sJT_1963 = fields.getData("applicationType");
 String __sJT_1964 = fields.getData("haveProcessed");
 String __sJT_1965 = fields.getData("previousProcessor");
 String __sJT_1966 = fields.getData("haveCanceled");
 String __sJT_1967 = fields.getData("canceledProcessor");
 String __sJT_1968 = fields.getData("canceledReason");
 String __sJT_1969 = fields.getData("cancelDateMonth");
 String __sJT_1970 = fields.getData("cancelDateYear");
 String __sJT_1971 = fields.getData("monthlySales");
 String __sJT_1972 = fields.getData("monthlyVMCSales");
 String __sJT_1973 = fields.getData("annualSales");
 String __sJT_1974 = fields.getData("annualVMCSales");
 String __sJT_1975 = fields.getData("averageTicket");
 String __sJT_1976 = fields.getData("highestTicket");
 String __sJT_1977 = fields.getData("motoPercentage");
 String __sJT_1978 = fields.getData("refundPolicy");
 String __sJT_1979 = fields.getData("referringBank");
 String __sJT_1980 = fields.getData("statementsProvided");
 String __sJT_1981 = fields.getData("owner1Gender");
 String __sJT_1982 = fields.getData("assocNum");
 String __sJT_1983 = fields.getData("accountType");
 String __sJT_1984 = fields.getData("cif");
 String __sJT_1985 = fields.getData("sicCode");
 String __sJT_1986 = fields.getData("ebtOptions");
 String __sJT_1987 = fields.getData("internetPercent");
 String __sJT_1988 = fields.getData("cardPercent");
 String __sJT_1989 = fields.getData("noCardPercent");
 String __sJT_1990 = fields.getData("webUrl");
 String __sJT_1991 = fields.getData("comments");
 String __sJT_1992 = fields.getData("existingVerisign");
 String __sJT_1993 = fields.getData("multiMerchTotal");
 String __sJT_1994 = fields.getData("multiMerchNum");
 String __sJT_1995 = fields.getData("multiMerchMasterDba");
 String __sJT_1996 = fields.getData("multiMerchMasterControl");
 String __sJT_1997 = fields.getData("vmcAcceptTypes");
 String __sJT_1998 = fields.getData("customerServicePhone");
 String __sJT_1999 = fields.getData("oldMerchantNumber");
   String theSqlTS = "update  merchant\n        set     merch_business_name         =  :1 ,\n                merch_federal_tax_id        =  :2 ,\n                merch_email_address         =  :3 ,\n                merch_legal_name            =  :4 ,\n                merch_mailing_name          =  :5 ,\n                merch_business_establ_month =  :6 ,\n                merch_business_establ_year  =  :7 ,\n                bustype_code                =  :8 ,\n                industype_code              =  :9 ,\n                merch_busgoodserv_descr     =  :10 ,\n                merch_num_of_locations      =  :11 ,\n                merch_years_at_loc          =  :12 ,\n                loctype_code                =  :13 ,\n                merch_application_type      =  :14 ,\n                merch_prior_cc_accp_flag    =  :15 ,\n                merch_prior_processor       =  :16 ,\n                merch_cc_acct_term_flag     =  :17 ,\n                merch_cc_term_name          =  :18 ,\n                merch_term_reason           =  :19 ,\n                merch_term_month            =  :20 ,\n                merch_term_year             =  :21 ,\n                merch_month_tot_proj_sales  =  :22 ,\n                merch_month_visa_mc_sales   =  :23 ,\n                annual_sales                =  :24 ,\n                annual_vmc_sales            =  :25 ,\n                merch_average_cc_tran       =  :26 ,\n                highest_ticket              =  :27 ,\n                merch_mail_phone_sales      =  :28 ,\n                refundtype_code             =  :29 ,\n                merch_referring_bank        =  :30 ,\n                merch_prior_statements      =  :31 ,\n                merch_gender                =  :32 ,\n                asso_number                 =  :33 ,\n                account_type                =  :34 ,\n                svb_cif_num                 =  :35 ,\n                app_sic_code                =  :36 ,\n                seasonal_flag               =  :37 ,\n                seasonal_months             =  :38 ,\n                ebt_options                 =  :39 ,\n                percent_internet            =  :40 ,\n                percent_card_present        =  :41 ,\n                percent_card_not_present    =  :42 ,\n                merch_web_url               =  :43 ,\n                merch_notes                 =  :44 ,\n                existing_verisign           =  :45 ,\n                total_num_multi_merch       =  :46 ,\n                num_multi_merch             =  :47 ,\n                multi_merch_master_dba      =  :48 ,\n                multi_merch_master_cntrl    =  :49 ,\n                vmc_accept_types            =  :50 ,\n                customer_service_phone      =  :51 ,\n                old_merchant_number         =  :52 \n        where   app_seq_num                 =  :53";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1950);
   __sJT_st.setString(2,__sJT_1951);
   __sJT_st.setString(3,__sJT_1952);
   __sJT_st.setString(4,__sJT_1953);
   __sJT_st.setString(5,__sJT_1954);
   __sJT_st.setString(6,__sJT_1955);
   __sJT_st.setString(7,__sJT_1956);
   __sJT_st.setString(8,__sJT_1957);
   __sJT_st.setString(9,__sJT_1958);
   __sJT_st.setString(10,__sJT_1959);
   __sJT_st.setString(11,__sJT_1960);
   __sJT_st.setString(12,__sJT_1961);
   __sJT_st.setString(13,__sJT_1962);
   __sJT_st.setString(14,__sJT_1963);
   __sJT_st.setString(15,__sJT_1964);
   __sJT_st.setString(16,__sJT_1965);
   __sJT_st.setString(17,__sJT_1966);
   __sJT_st.setString(18,__sJT_1967);
   __sJT_st.setString(19,__sJT_1968);
   __sJT_st.setString(20,__sJT_1969);
   __sJT_st.setString(21,__sJT_1970);
   __sJT_st.setString(22,__sJT_1971);
   __sJT_st.setString(23,__sJT_1972);
   __sJT_st.setString(24,__sJT_1973);
   __sJT_st.setString(25,__sJT_1974);
   __sJT_st.setString(26,__sJT_1975);
   __sJT_st.setString(27,__sJT_1976);
   __sJT_st.setString(28,__sJT_1977);
   __sJT_st.setString(29,__sJT_1978);
   __sJT_st.setString(30,__sJT_1979);
   __sJT_st.setString(31,__sJT_1980);
   __sJT_st.setString(32,__sJT_1981);
   __sJT_st.setString(33,__sJT_1982);
   __sJT_st.setString(34,__sJT_1983);
   __sJT_st.setString(35,__sJT_1984);
   __sJT_st.setString(36,__sJT_1985);
   __sJT_st.setString(37,seasonalFlag);
   __sJT_st.setString(38,seasonalMonths);
   __sJT_st.setString(39,__sJT_1986);
   __sJT_st.setString(40,__sJT_1987);
   __sJT_st.setString(41,__sJT_1988);
   __sJT_st.setString(42,__sJT_1989);
   __sJT_st.setString(43,__sJT_1990);
   __sJT_st.setString(44,__sJT_1991);
   __sJT_st.setString(45,__sJT_1992);
   __sJT_st.setString(46,__sJT_1993);
   __sJT_st.setString(47,__sJT_1994);
   __sJT_st.setString(48,__sJT_1995);
   __sJT_st.setString(49,__sJT_1996);
   __sJT_st.setString(50,__sJT_1997);
   __sJT_st.setString(51,__sJT_1998);
   __sJT_st.setString(52,__sJT_1999);
   __sJT_st.setLong(53,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2066^7*/
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitMerchant()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  /*
  ** protected void submitBank(long appSeqNum) throws Exception
  **
  ** Stores the merchant's business checking account data in merchbank.
  */
  protected void submitBank(long appSeqNum) throws Exception
  {
    try
    {
      if(! getData("checkingAccount").equals("") )
      {
        // clear existing bank record
        /*@lineinfo:generated-code*//*@lineinfo:2087^9*/

//  ************************************************************
//  #sql [Ctx] { delete from merchbank
//            where app_seq_num = :appSeqNum and
//                  merchbank_acct_srnum = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from merchbank\n          where app_seq_num =  :1  and\n                merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2092^9*/

        // store bank data
        /*@lineinfo:generated-code*//*@lineinfo:2095^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchbank
//            ( app_seq_num,
//              bankacc_type,
//              merchbank_info_source,
//              merchbank_name,
//              merchbank_acct_num,
//              merchbank_transit_route_num,
//              merchbank_num_years_open,
//              billing_method,
//              merchbank_acct_srnum
//              )
//            values
//            ( :appSeqNum,
//              :fields.getData("typeOfAcct"),
//              :fields.getData("sourceOfInfo"),
//              :fields.getData("bankName"),
//              :fields.getData("checkingAccount"),
//              :fields.getData("transitRouting"),
//              :fields.getData("yearsOpen"),
//              :fields.getData("billingMethod"),
//              1 )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2000 = fields.getData("typeOfAcct");
 String __sJT_2001 = fields.getData("sourceOfInfo");
 String __sJT_2002 = fields.getData("bankName");
 String __sJT_2003 = fields.getData("checkingAccount");
 String __sJT_2004 = fields.getData("transitRouting");
 String __sJT_2005 = fields.getData("yearsOpen");
 String __sJT_2006 = fields.getData("billingMethod");
   String theSqlTS = "insert into merchbank\n          ( app_seq_num,\n            bankacc_type,\n            merchbank_info_source,\n            merchbank_name,\n            merchbank_acct_num,\n            merchbank_transit_route_num,\n            merchbank_num_years_open,\n            billing_method,\n            merchbank_acct_srnum\n            )\n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n            1 )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_2000);
   __sJT_st.setString(3,__sJT_2001);
   __sJT_st.setString(4,__sJT_2002);
   __sJT_st.setString(5,__sJT_2003);
   __sJT_st.setString(6,__sJT_2004);
   __sJT_st.setString(7,__sJT_2005);
   __sJT_st.setString(8,__sJT_2006);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2118^9*/
      }

      // merchant's bank address
      submitAddressData(mesConstants.ADDR_TYPE_CHK_ACCT_BANK,
        new Field[] { fields.getField("bankName"),
                      fields.getField("bankAddress"),
                      null,
                      fields.getField("bankCszCity"),
                      fields.getField("bankCszState"),
                      fields.getField("bankCszZip"),
                      null,
                      fields.getField("bankPhone"),
                      null } );
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitBank()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  /*
  ** protected void submitAddresses(long appSeqNum) throws Exception
  **
  ** Submits addresses associated with the merchant's business.
  */
  protected void submitAddresses(long appSeqNum) throws Exception
  {
    try
    {
      // business address
      submitAddressData(mesConstants.ADDR_TYPE_BUSINESS,
        new Field[] { fields.getField("businessName"),
                      fields.getField("businessAddress1"),
                      fields.getField("businessAddress2"),
                      fields.getField("businessCszCity"),
                      fields.getField("businessCszState"),
                      fields.getField("businessCszZip"),
                      fields.getField("businessCountry"),
                      fields.getField("businessPhone"),
                      fields.getField("businessFax") } );

      // mailing address
      submitAddressData(mesConstants.ADDR_TYPE_MAILING,
        new Field[] { fields.getField("mailingName"),
                      fields.getField("mailingAddress1"),
                      fields.getField("mailingAddress2"),
                      fields.getField("mailingCszCity"),
                      fields.getField("mailingCszState"),
                      fields.getField("mailingCszZip"),
                      null,
                      null,
                      null } );
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitAddresses()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  // business owner field array indexes
  public static final int FIDX_OWNER_LAST_NAME  = 0;
  public static final int FIDX_OWNER_FIRST_NAME = 1;
  public static final int FIDX_OWNER_SSN        = 2;
  public static final int FIDX_OWNER_PERCENT    = 3;
  public static final int FIDX_OWNER_MONTH      = 4;
  public static final int FIDX_OWNER_YEAR       = 5;
  public static final int FIDX_OWNER_TITLE      = 6;
  public static final int FIDX_OWNER_DL_NUM     = 7;
  public static final int FIDX_OWNER_DL_EXP     = 8;
  public static final int FIDX_OWNER_DL_ISSUE   = 9;
  public static final int FIDX_OWNER_DL_STATE   = 10;
  public static final int FIDX_OWNER_DL_DOB     = 11;

  /*
  ** protected void submitBusinessOwnerRecord(long appSeqNum, int ownerId,
  **   Field[] fields) throws Exception
  **
  ** Submits a record to the businessowner table.
  */
  protected void submitOwnerRecord(long appSeqNum, int ownerId,
    Field[] ownerFields) throws Exception
  {
    try
    {
      // clear existing business record
      /*@lineinfo:generated-code*//*@lineinfo:2206^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    businessowner
//          where   app_seq_num   = :appSeqNum and
//                  busowner_num  = :ownerId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    businessowner\n        where   app_seq_num   =  :1  and\n                busowner_num  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,ownerId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2212^7*/

      // only store new business record if certain fields are not blank
      if (ownerFields[FIDX_OWNER_SSN] != null
          && !ownerFields[FIDX_OWNER_SSN].isBlank())
      {
        // extract the owner data
        String lastName = ownerFields[FIDX_OWNER_LAST_NAME] == null
          ? null : ownerFields[FIDX_OWNER_LAST_NAME].getData();
        String firstName = ownerFields[FIDX_OWNER_FIRST_NAME] == null
          ? null : ownerFields[FIDX_OWNER_FIRST_NAME].getData();
        String ssn = ownerFields[FIDX_OWNER_SSN] == null
          ? null : ownerFields[FIDX_OWNER_SSN].getData();
        String percent = ownerFields[FIDX_OWNER_PERCENT] == null
          ? null : ownerFields[FIDX_OWNER_PERCENT].getData();
        String month = ownerFields[FIDX_OWNER_MONTH] == null
          ? null : ownerFields[FIDX_OWNER_MONTH].getData();
        String year = ownerFields[FIDX_OWNER_YEAR] == null
          ? null : ownerFields[FIDX_OWNER_YEAR].getData();
        String title = ownerFields[FIDX_OWNER_TITLE] == null
          ? null : ownerFields[FIDX_OWNER_TITLE].getData();

        String dlNumber = ownerFields[FIDX_OWNER_DL_NUM] == null
          ? null : ownerFields[FIDX_OWNER_DL_NUM].getData();
        String dlExp = ownerFields[FIDX_OWNER_DL_EXP] == null
          ? null : ownerFields[FIDX_OWNER_DL_EXP].getData();
        String dlIssue = ownerFields[FIDX_OWNER_DL_ISSUE] == null
          ? null : ownerFields[FIDX_OWNER_DL_ISSUE].getData();
        String dlState = ownerFields[FIDX_OWNER_DL_STATE] == null
          ? null : ownerFields[FIDX_OWNER_DL_STATE].getData();
        String dlDOB = ownerFields[FIDX_OWNER_DL_DOB] == null
          ? null : ownerFields[FIDX_OWNER_DL_DOB].getData();

        // insert the record
        /*@lineinfo:generated-code*//*@lineinfo:2246^9*/

//  ************************************************************
//  #sql [Ctx] { insert into businessowner
//            ( app_seq_num,
//              busowner_num,
//              busowner_last_name,
//              busowner_first_name,
//              busowner_ssn,
//              busowner_owner_perc,
//              busowner_period_month,
//              busowner_period_year,
//              busowner_title,
//              busowner_dl_number,
//              busowner_dl_expiration,
//              busowner_dl_issue,
//              busowner_dl_state,
//              busowner_dl_dob )
//            values
//            ( :appSeqNum,
//              :ownerId,
//              :lastName,
//              :firstName,
//              :ssn,
//              :percent,
//              :month,
//              :year,
//              :title,
//              :dlNumber,
//              to_date(:dlExp, 'MM/DD/YYYY'),
//              to_date(:dlIssue, 'MM/DD/YYYY'),
//              :dlState,
//              to_date(:dlDOB, 'MM/DD/YYYY') )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into businessowner\n          ( app_seq_num,\n            busowner_num,\n            busowner_last_name,\n            busowner_first_name,\n            busowner_ssn,\n            busowner_owner_perc,\n            busowner_period_month,\n            busowner_period_year,\n            busowner_title,\n            busowner_dl_number,\n            busowner_dl_expiration,\n            busowner_dl_issue,\n            busowner_dl_state,\n            busowner_dl_dob )\n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n            to_date( :11 , 'MM/DD/YYYY'),\n            to_date( :12 , 'MM/DD/YYYY'),\n             :13 ,\n            to_date( :14 , 'MM/DD/YYYY') )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,ownerId);
   __sJT_st.setString(3,lastName);
   __sJT_st.setString(4,firstName);
   __sJT_st.setString(5,ssn);
   __sJT_st.setString(6,percent);
   __sJT_st.setString(7,month);
   __sJT_st.setString(8,year);
   __sJT_st.setString(9,title);
   __sJT_st.setString(10,dlNumber);
   __sJT_st.setString(11,dlExp);
   __sJT_st.setString(12,dlIssue);
   __sJT_st.setString(13,dlState);
   __sJT_st.setString(14,dlDOB);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2278^9*/
      }
    }
    catch(Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitOwnerRecord()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  /*
  ** protected void submitOwners(long appSeqNum) throws Exception
  **
  ** Submits primary and secondary owner information, including addresses and
  ** general data about the owner individuals.
  */
  protected void submitOwners(long appSeqNum) throws Exception
  {
    try
    {
      // primary owner address
      submitAddressData(mesConstants.ADDR_TYPE_OWNER1,
        new Field[] { fields.getField("owner1Name"),
                      fields.getField("owner1Address1"),
                      null,
                      fields.getField("owner1CszCity"),
                      fields.getField("owner1CszState"),
                      fields.getField("owner1CszZip"),
                      null,
                      fields.getField("owner1Phone"),
                      null } );

      // secondary owner address
      submitAddressData(mesConstants.ADDR_TYPE_OWNER2,
        new Field[] { fields.getField("owner2Name"),
                      fields.getField("owner2Address1"),
                      null,
                      fields.getField("owner2CszCity"),
                      fields.getField("owner2CszState"),
                      fields.getField("owner2CszZip"),
                      null,
                      fields.getField("owner2Phone"),
                      null } );

      // primary owner data
      submitOwnerRecord(appSeqNum,1,
        new Field[] { fields.getField("owner1NameLast"),
                      fields.getField("owner1NameFirst"),
                      fields.getField("owner1SSN"),
                      fields.getField("owner1Percent"),
                      fields.getField("owner1SinceMonth"),
                      fields.getField("owner1SinceYear"),
                      fields.getField("owner1Title"),
                      fields.getField("owner1DlNumber"),
                      fields.getField("owner1DlExpDate"),
                      fields.getField("owner1DlIssueDate"),
                      fields.getField("owner1DlState"),
                      fields.getField("owner1DlDob") } );

      // secondary owner data
      submitOwnerRecord(appSeqNum,2,
        new Field[] { fields.getField("owner2NameLast"),
                      fields.getField("owner2NameFirst"),
                      fields.getField("owner2SSN"),
                      fields.getField("owner2Percent"),
                      fields.getField("owner2SinceMonth"),
                      fields.getField("owner2SinceYear"),
                      fields.getField("owner2Title"),
                      fields.getField("owner2DlNumber"),
                      fields.getField("owner2DlExpDate"),
                      fields.getField("owner2DlIssueDate"),
                      fields.getField("owner2DlState"),
                      fields.getField("owner2DlDob") } );
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitOwners()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  /*
  ** protected void submitContact(long appSeqNum) throws Exception
  **
  ** Stores merchant contact data in merchcontact.
  */
  protected void submitContact(long appSeqNum) throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2368^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchcontact
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merchcontact\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2373^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2375^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchcontact
//          ( app_seq_num,
//            merchcont_prim_first_name,
//            merchcont_prim_last_name,
//            merchcont_prim_phone,
//            merchcont_prim_phone_ext,
//            merchcont_prim_email )
//          values
//          ( :appSeqNum,
//            :fields.getData("contactNameFirst"),
//            :fields.getData("contactNameLast"),
//            :fields.getData("contactPhone"),
//            :fields.getData("contactPhoneExt"),
//            :fields.getData("contactEmail") )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2007 = fields.getData("contactNameFirst");
 String __sJT_2008 = fields.getData("contactNameLast");
 String __sJT_2009 = fields.getData("contactPhone");
 String __sJT_2010 = fields.getData("contactPhoneExt");
 String __sJT_2011 = fields.getData("contactEmail");
   String theSqlTS = "insert into merchcontact\n        ( app_seq_num,\n          merchcont_prim_first_name,\n          merchcont_prim_last_name,\n          merchcont_prim_phone,\n          merchcont_prim_phone_ext,\n          merchcont_prim_email )\n        values\n        (  :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_2007);
   __sJT_st.setString(3,__sJT_2008);
   __sJT_st.setString(4,__sJT_2009);
   __sJT_st.setString(5,__sJT_2010);
   __sJT_st.setString(6,__sJT_2011);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2391^7*/
    }
    catch(Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitContact()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  // card option types
  public static final int CT_VISA     = 0;
  public static final int CT_MC       = 1;
  public static final int CT_DEBIT    = 2;
  public static final int CT_DINERS   = 3;
  public static final int CT_DISCOVER = 4;
  public static final int CT_AMEX     = 5;
  public static final int CT_JCB      = 6;
  public static final int CT_CHECK    = 7;
  public static final int CT_EBT      = 8;
  public static final int CT_DIAL_PAY = 9;
  public static final int CT_INTERNET = 10;
  public static final int CT_VALUTEC  = 11;
  public static final int CT_SPS      = 12;

  public static final int CT_COUNT    = 13;

  /*
  ** protected void submitCards(long appSeqNum) throws Exception
  **
  ** Stores which cards a merchant wishes to accept, and various options
  ** and data associated with each card type into the merchpayoption table.
  */
  protected void submitCards(long appSeqNum) throws Exception
  {
    Field       field     = null;

    try
    {
      int cardCount = 0;
      for (int cardIdx = 0; cardIdx < CT_COUNT; ++cardIdx)
      {
        boolean cardAccepted  = false;
        int     cardType      = -1;
        String  cardAcctNum   = null;
        String  cardRate      = null;
        String  cardPerItem   = null;
        String  amexSplitDial = "N";
        String  amexPip       = "N";
        String  checkProvider = null;
        String  cardTid       = null;
        String  annualSales   = null;
        String  averageTicket = null;
        String  groupNumber   = null;


        switch (cardIdx)
        {
          case CT_VISA:
            cardType      = mesConstants.APP_CT_VISA;
            cardAccepted  = true;
            break;

          case CT_MC:
            cardType      = mesConstants.APP_CT_MC;
            cardAccepted  = true;
            break;

          case CT_DEBIT:
            cardType      = mesConstants.APP_CT_DEBIT;
            cardAccepted  = fields.getData("debitAccepted").equals("y");
            break;

          case CT_EBT:
            cardType      = mesConstants.APP_CT_EBT;
            cardAccepted  = fields.getData("ebtAccepted").equals("y");
            cardAcctNum   = fields.getData("fcsNumber");
            break;

          case CT_DINERS:
            cardType      = mesConstants.APP_CT_DINERS_CLUB;
            cardAccepted  = fields.getData("dinersAccepted").equals("y");
            cardAcctNum   = fields.getData("dinersAcctNum");
            break;

          case CT_DISCOVER:
            cardType      = mesConstants.APP_CT_DISCOVER;
            cardAccepted  = fields.getData("discoverAccepted").equals("y");
            cardAcctNum   = fields.getData("discoverAcctNum");
            cardRate      = fields.getData("discoverRapRate");
            cardPerItem   = fields.getData("discoverRapPerItem");

            // extended fields
            annualSales   = fields.getData("discoverAnnualSales");
            averageTicket = fields.getData("discoverAvgTicket");
            groupNumber   = fields.getData("discoverCapNumber");
            break;

          case CT_AMEX:
            cardType      = mesConstants.APP_CT_AMEX;
            cardAccepted  = fields.getData("amexAccepted").equals("y");
            cardAcctNum   = fields.getData("amexAcctNum");
            cardRate      = fields.getData("amexEsaRate");
            amexSplitDial = fields.getData("amexSplitDial").toUpperCase();
            amexPip       = fields.getData("amexPip").toUpperCase();

            // extended fields
            annualSales   = fields.getData("amexAnnualSales");
            averageTicket = fields.getData("amexAvgTicket");
            groupNumber   = fields.getData("amexFranchiseSE");
            break;

          case CT_JCB:
            cardType      = mesConstants.APP_CT_JCB;
            cardAccepted  = fields.getData("jcbAccepted").equals("y");
            cardAcctNum   = fields.getData("jcbAcctNum");
            break;

          case CT_CHECK:
            cardType      = mesConstants.APP_CT_CHECK_AUTH;
            cardAccepted  = fields.getData("checkAccepted").equals("y");
            cardAcctNum   = fields.getData("checkAcctNum");
            cardTid       = fields.getData("checkTermId");
            checkProvider = fields.getData("checkProvider");
            if ( checkProvider.equals("CPOther") )
            {
              // change to "CPOther:Some Check Provider Name"
              // this will be decoded when reading the data back
              // from the database table.
              checkProvider += (":" + fields.getData("checkProviderOther"));
            }
            break;

          case CT_DIAL_PAY:
            cardType      = mesConstants.APP_CT_DIAL_PAY;
            cardAccepted  = (fields.getField("productType").asInteger()
                              == mesConstants.POS_DIAL_AUTH);
            break;

          case CT_INTERNET:
            cardType      = mesConstants.APP_CT_INTERNET;
            cardAccepted  = true;
            break;

          case CT_VALUTEC:
            cardType      = mesConstants.APP_CT_VALUTEC_GIFT_CARD;
            cardAccepted  = fields.getData("valutecAccepted").equals("y");
            cardAcctNum   = fields.getData("valutecAcctNum");
            cardTid       = fields.getData("valutecTermId");
            break;

          case CT_SPS:
            cardType      = mesConstants.APP_CT_SPS;
            cardAccepted  = fields.getData("spsAccepted").equals("y");
            cardAcctNum   = fields.getData("spsAcctNum");
            break;

          default:
            continue;
        }

        // clear any old record for this card type from merchpayoption
        /*@lineinfo:generated-code*//*@lineinfo:2552^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    merchpayoption
//            where   app_seq_num   = :appSeqNum and
//                    cardtype_code = :cardType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    merchpayoption\n          where   app_seq_num   =  :1  and\n                  cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2558^9*/

        // insert record for card type if it is accepted
        if (cardAccepted)
        {
          ++cardCount;
          /*@lineinfo:generated-code*//*@lineinfo:2564^11*/

//  ************************************************************
//  #sql [Ctx] { insert into  merchpayoption
//              ( app_seq_num,
//                cardtype_code,
//                merchpo_card_merch_number,
//                merchpo_provider_name,
//                card_sr_number,
//                merchpo_rate,
//                merchpo_fee,
//                merchpo_split_dial,
//                merchpo_pip,
//                merchpo_tid,
//                annual_sales,
//                average_ticket,
//                group_merchant_number
//              )
//              values
//              ( :appSeqNum,
//                :cardType,
//                :cardAcctNum,
//                :checkProvider,
//                :cardCount,
//                :cardRate,
//                :cardPerItem,
//                :amexSplitDial,
//                :amexPip,
//                :cardTid,
//                :annualSales,
//                :averageTicket,
//                :groupNumber
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into  merchpayoption\n            ( app_seq_num,\n              cardtype_code,\n              merchpo_card_merch_number,\n              merchpo_provider_name,\n              card_sr_number,\n              merchpo_rate,\n              merchpo_fee,\n              merchpo_split_dial,\n              merchpo_pip,\n              merchpo_tid,\n              annual_sales,\n              average_ticket,\n              group_merchant_number\n            )\n            values\n            (  :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n               :7 ,\n               :8 ,\n               :9 ,\n               :10 ,\n               :11 ,\n               :12 ,\n               :13 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
   __sJT_st.setString(3,cardAcctNum);
   __sJT_st.setString(4,checkProvider);
   __sJT_st.setInt(5,cardCount);
   __sJT_st.setString(6,cardRate);
   __sJT_st.setString(7,cardPerItem);
   __sJT_st.setString(8,amexSplitDial);
   __sJT_st.setString(9,amexPip);
   __sJT_st.setString(10,cardTid);
   __sJT_st.setString(11,annualSales);
   __sJT_st.setString(12,averageTicket);
   __sJT_st.setString(13,groupNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2596^11*/
        }
        // not accepted, clear related card info
        // from tranchrg table for this card type
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:2602^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    tranchrg
//              where   app_seq_num   = :appSeqNum and
//                      cardtype_code = :cardType
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    tranchrg\n            where   app_seq_num   =  :1  and\n                    cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2608^11*/
        }
      }
    }
    catch (Exception e)
    {
      throw new Exception("submitCards(): " + e.toString());
    }
  }

  /*
  ** protected void submitProduct(long appSeqNum) throws Exception
  **
  ** Stores merchant related to merchant's choice of POS solution product.
  ** Data is stored in merch_pos table.  Also, the EDC flag in merchant is
  ** updated and tranchrg items related to internet and dial pay are cleared
  ** if the product type is not of those types.
  */
  protected void submitProduct(long appSeqNum) throws Exception
  {
    try
    {
      int     productType = fields.getField("productType").asInteger();
      
      if( productType == 0 )
      {
        productType = mesConstants.POS_UNKNOWN;
      }
      
      int     posCode     = -1;
      String  posParam    = null;
      String  posExisting = "N";
      String  edcFlag     = "N";

      String  pgEmail       = null;

      // clear any old records from merch_pos
      /*@lineinfo:generated-code*//*@lineinfo:2645^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merch_pos
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merch_pos\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2650^7*/

      // set the product specific values
      switch(productType)
      {
        case mesConstants.POS_DIAL_TERMINAL:
          posCode   = mesConstants.APP_MPOS_DIAL_TERMINAL;
          edcFlag   = "Y";
          break;

        case mesConstants.POS_WIRELESS_TERMINAL:
          posCode   = mesConstants.APP_MPOS_WIRELESS_TERMINAL;
          edcFlag   = "Y";
          break;

        case mesConstants.POS_DIAL_AUTH:
          posCode   = mesConstants.APP_MPOS_DIAL_PAY;
          break;

        case mesConstants.POS_INTERNET:
          posCode   = Integer.parseInt(getData("internetType")); //fields.getField("internetType").asInteger();
          posParam  = getData("webUrl"); //fields.getField("webUrl").getData();
          posExisting = getData("existingInternet").toUpperCase(); //fields.getField("existingInternet").getData().toUpperCase();
          //reusing pgEmail field in DB for admin email here
          pgEmail = getData("internetEmail"); //fields.getField("internetEmail").getData();
          break;

        case mesConstants.POS_CHARGE_EXPRESS:
          posCode   = mesConstants.APP_MPOS_PC_CHARGE_EXPRESS;
          posExisting = fields.getField("existingPCChargeExpress").getData().toUpperCase();
          break;

        case mesConstants.POS_CHARGE_PRO:
          posCode   = mesConstants.APP_MPOS_PC_CHARGE_PRO;
          posExisting = fields.getField("existingPCChargePro").getData().toUpperCase();
          break;

        case mesConstants.POS_PC:
          posCode   = mesConstants.APP_MPOS_POS_PARTNER_2000;
          break;

        case mesConstants.POS_GLOBAL_PC:
          posCode   = mesConstants.APP_MPOS_GLOBAL_PC_WINDOWS;
          break;

        case mesConstants.POS_TOUCHTONECAPTURE:
          posCode   = mesConstants.APP_MPOS_TOUCH_TONE_CAPTURE;
          break;

        case mesConstants.POS_OTHER:
          posCode   = mesConstants.APP_MPOS_OTHER_VITAL_PRODUCT;
          posParam  = fields.getField("vitalProduct").getData();
          break;

        case mesConstants.POS_VIRTUAL_TERMINAL:
          posCode   = mesConstants.APP_MPOS_VIRTUAL_TERMINAL;
          posParam  = getData("vtEmail");
          break;
          
        case mesConstants.POS_VT_LIMITED:
          posCode   = mesConstants.APP_MPOS_VT_LIMITED;
          posParam  = getData("vtLimitedEmail");
          break;

        case mesConstants.POS_PAYMENT_GATEWAY:
          posCode       = fields.getField("pgCard").asInteger();
          posParam      = getData("pgCertName");
          pgEmail       = getData("pgEmail");
          break;
          
        case mesConstants.POS_UNKNOWN:
          posCode       = mesConstants.APP_MPOS_UNKNOWN;
          break;

        default:
          throw new Exception("Unsupported product type: " + productType);
      }

      // clear tranchrg records having to do with dialpay and
      // internet types except when one of those types is
      // selected as a product
      /*@lineinfo:generated-code*//*@lineinfo:2731^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    tranchrg
//          where   app_seq_num = :appSeqNum and
//                  ( ( cardtype_code = :mesConstants.APP_CT_INTERNET and
//                      :posCode <> :mesConstants.POS_INTERNET ) or
//                    ( cardtype_code  = :mesConstants.APP_CT_DIAL_PAY and
//                      :posCode <> :mesConstants.POS_DIAL_AUTH ) )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    tranchrg\n        where   app_seq_num =  :1  and\n                ( ( cardtype_code =  :2  and\n                     :3  <>  :4  ) or\n                  ( cardtype_code  =  :5  and\n                     :6  <>  :7  ) )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_INTERNET);
   __sJT_st.setInt(3,posCode);
   __sJT_st.setInt(4,mesConstants.POS_INTERNET);
   __sJT_st.setInt(5,mesConstants.APP_CT_DIAL_PAY);
   __sJT_st.setInt(6,posCode);
   __sJT_st.setInt(7,mesConstants.POS_DIAL_AUTH);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2740^7*/

      // update the EDC flag in the merchant table
      // based on the new product selection
      /*@lineinfo:generated-code*//*@lineinfo:2744^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_edc_flag = :edcFlag
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     merch_edc_flag =  :1 \n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,edcFlag);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2749^7*/

      // store the merchant's product choice
      if(productType == mesConstants.POS_PC && fields.getField("posPartnerConnectivity") != null)
      {
        // additional fields for POS Partner
        /*@lineinfo:generated-code*//*@lineinfo:2755^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_pos
//            (
//              app_seq_num,
//              pos_code,
//              pos_param,
//              pp_connectivity,
//              pp_level_ii,
//              pp_level_iii,
//              pp_os,
//              existing
//            )
//            values
//            (
//              :appSeqNum,
//              :posCode,
//              :posParam,
//              :fields.getField("posPartnerConnectivity").asInteger(),
//              upper(:fields.getData("posPartnerLevelII")),
//              upper(:fields.getData("posPartnerLevelIII")),
//              :fields.getField("posPartnerOS").asInteger(),
//              :posExisting
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2012 = fields.getField("posPartnerConnectivity").asInteger();
 String __sJT_2013 = fields.getData("posPartnerLevelII");
 String __sJT_2014 = fields.getData("posPartnerLevelIII");
 int __sJT_2015 = fields.getField("posPartnerOS").asInteger();
   String theSqlTS = "insert into merch_pos\n          (\n            app_seq_num,\n            pos_code,\n            pos_param,\n            pp_connectivity,\n            pp_level_ii,\n            pp_level_iii,\n            pp_os,\n            existing\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n            upper( :5 ),\n            upper( :6 ),\n             :7 ,\n             :8 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,posCode);
   __sJT_st.setString(3,posParam);
   __sJT_st.setInt(4,__sJT_2012);
   __sJT_st.setString(5,__sJT_2013);
   __sJT_st.setString(6,__sJT_2014);
   __sJT_st.setInt(7,__sJT_2015);
   __sJT_st.setString(8,posExisting);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2779^9*/
      }
      else
      {
        // additional fields for
        /*@lineinfo:generated-code*//*@lineinfo:2784^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_pos
//            (
//              app_seq_num,
//              pos_code,
//              pos_param,
//              pg_email,
//              existing
//            )
//            values
//            (
//              :appSeqNum,
//              :posCode,
//              :posParam,
//              :pgEmail,
//              :posExisting
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_pos\n          (\n            app_seq_num,\n            pos_code,\n            pos_param,\n            pg_email,\n            existing\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,posCode);
   __sJT_st.setString(3,posParam);
   __sJT_st.setString(4,pgEmail);
   __sJT_st.setString(5,posExisting);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2802^9*/
      }
     }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitProduct()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  /*
  ** protected void submitEquipment(long appSeqNum) throws Exception
  **
  ** Updates the merchequipment table with imprinter plate information and
  ** information having to do with PC product solutions.  Depending on the
  ** product type chosen, equipment records need to be created or cleared.
  */
  protected void submitEquipment(long appSeqNum) throws Exception
  {
    try
    {
      int productType = fields.getField("productType").asInteger();
      int plateCount  = fields.getField("imprinterPlates").asInteger();

      // pc product handling
      boolean isPc = false;
      String equipModel = null;

      // handle equipment issues related to the product type
      switch( productType )
      {
        // dial terminals: make sure no PC solution entries are left
        // in merchequipment, clear any imprinter plates
        case mesConstants.POS_DIAL_TERMINAL:
        case mesConstants.POS_WIRELESS_TERMINAL:
          /*@lineinfo:generated-code*//*@lineinfo:2837^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    merchequipment
//              where   app_seq_num = :appSeqNum and
//                      ( equip_model = 'PCPS' or
//                        equiptype_code =
//                          :mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    merchequipment\n            where   app_seq_num =  :1  and\n                    ( equip_model = 'PCPS' or\n                      equiptype_code =\n                         :2  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2845^11*/
          break;

        // pc products: set pc product model, turn on pc flag
        case mesConstants.POS_CHARGE_PRO:
          isPc = true;
          equipModel = "PCCHRGPRO";
          break;

        case mesConstants.POS_CHARGE_EXPRESS:
          isPc = true;
          equipModel = "PCCHRGEXP";
          break;

        case mesConstants.POS_PC:
          isPc = true;
          equipModel = "PCPS";
          break;

        case mesConstants.POS_GLOBAL_PC:
          isPc = true;
          equipModel = "PCGLOBAL";
          break;

        case mesConstants.POS_TOUCHTONECAPTURE:
          isPc = true;
          equipModel = "TTC";
          // TODO: resolve the equip model
          break;

        // anything else: clear all equipment options
        case mesConstants.POS_DIAL_AUTH:
        case mesConstants.POS_INTERNET:
        case mesConstants.POS_OTHER:
        default:
          /*@lineinfo:generated-code*//*@lineinfo:2880^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    merchequipment
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    merchequipment\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2885^11*/
          break;
      }

      // handle pc product types
      if (isPc)
      {
        // clear all items not matching the pc type
        /*@lineinfo:generated-code*//*@lineinfo:2893^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    merchequipment
//            where   app_seq_num = :appSeqNum
//                    and equip_model <> :equipModel
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    merchequipment\n          where   app_seq_num =  :1 \n                  and equip_model <>  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,equipModel);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2899^9*/

        // determine if record for this pc type already exists
        int pcCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:2903^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//            
//            from    merchequipment
//            where   app_seq_num = :appSeqNum
//                    and equip_model = :equipModel
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n           \n          from    merchequipment\n          where   app_seq_num =  :1 \n                  and equip_model =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.app.BusinessBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,equipModel);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   pcCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2910^9*/

        // add a new pc equipment item if none already exist
        if (pcCount == 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:2915^11*/

//  ************************************************************
//  #sql [Ctx] { insert into merchequipment
//              ( app_seq_num,
//                merchequip_equip_quantity,
//                equiplendtype_code,
//                equiptype_code,
//                equip_model )
//              values
//              ( :appSeqNum,
//                1,
//                :mesConstants.APP_EQUIP_PURCHASE,
//                :mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE,
//                :equipModel )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchequipment\n            ( app_seq_num,\n              merchequip_equip_quantity,\n              equiplendtype_code,\n              equiptype_code,\n              equip_model )\n            values\n            (  :1 ,\n              1,\n               :2 ,\n               :3 ,\n               :4  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE);
   __sJT_st.setString(4,equipModel);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2929^11*/
        }
      }

      // insert an imprinter plate record if plates specified
      if (plateCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:2936^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchequipment
//            ( app_seq_num,
//              equiptype_code,
//              equiplendtype_code,
//              merchequip_amount,
//              equip_model,
//              merchequip_equip_quantity,
//              prod_option_id )
//            values
//            ( :appSeqNum,
//              :mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES,
//              :mesConstants.APP_EQUIP_PURCHASE,
//              null,
//              'IPPL',
//              :plateCount,
//              null )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchequipment\n          ( app_seq_num,\n            equiptype_code,\n            equiplendtype_code,\n            merchequip_amount,\n            equip_model,\n            merchequip_equip_quantity,\n            prod_option_id )\n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n            null,\n            'IPPL',\n             :4 ,\n            null )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(4,plateCount);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2954^9*/
      }
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitEquipment()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  /*
  ** protected void submitCreditContact(long appSeqNum) throws Exception
  **
  ** Updates the credit_contact_info table 
  */
  protected void submitCreditContact(long appSeqNum) throws Exception
  {
    try
    {
      // update credit_contact_info
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:2976^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    credit_contact_info
//          where   app_seq_num = :getData("appSeqNum")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2016 = getData("appSeqNum");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    credit_contact_info\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.app.BusinessBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_2016);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2982^7*/
      
      if(recCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:2986^9*/

//  ************************************************************
//  #sql [Ctx] { update  credit_contact_info
//            set     contact_merchant  = :getData("creditContactMerchant"),
//                    contact_rep       = :getData("creditContactRep"),
//                    rep_email         = :getData("repEmail")
//            where   app_seq_num       = :getData("appSeqNum")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2017 = getData("creditContactMerchant");
 String __sJT_2018 = getData("creditContactRep");
 String __sJT_2019 = getData("repEmail");
 String __sJT_2020 = getData("appSeqNum");
   String theSqlTS = "update  credit_contact_info\n          set     contact_merchant  =  :1 ,\n                  contact_rep       =  :2 ,\n                  rep_email         =  :3 \n          where   app_seq_num       =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"29com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2017);
   __sJT_st.setString(2,__sJT_2018);
   __sJT_st.setString(3,__sJT_2019);
   __sJT_st.setString(4,__sJT_2020);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2993^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:2997^9*/

//  ************************************************************
//  #sql [Ctx] { insert into credit_contact_info
//            (
//              app_seq_num,
//              contact_merchant,
//              contact_rep,
//              rep_email
//            )
//            values
//            (
//              :getData("appSeqNum"),
//              :getData("creditContactMerchant"),
//              :getData("creditContactRep"),
//              :getData("repEmail")
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2021 = getData("appSeqNum");
 String __sJT_2022 = getData("creditContactMerchant");
 String __sJT_2023 = getData("creditContactRep");
 String __sJT_2024 = getData("repEmail");
   String theSqlTS = "insert into credit_contact_info\n          (\n            app_seq_num,\n            contact_merchant,\n            contact_rep,\n            rep_email\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"30com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2021);
   __sJT_st.setString(2,__sJT_2022);
   __sJT_st.setString(3,__sJT_2023);
   __sJT_st.setString(4,__sJT_2024);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3013^9*/
      }
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitCreditContact()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }
  
  /*
  ** protected boolean submitAppData()
  **
  ** Causes all data from the page to be stored in the database.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean submitAppData()
  {
    boolean submitOk = false;
    try
    {
      connect();


      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        submitMerchant(appSeqNum);
        submitBank(appSeqNum);
        submitAddresses(appSeqNum);
        submitOwners(appSeqNum);
        submitContact(appSeqNum);
        submitCards(appSeqNum);
        submitProduct(appSeqNum);
        submitEquipment(appSeqNum);
        submitCreditContact(appSeqNum);
      }
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitData()",e.toString());
      System.out.println(this.getClass().getName() + "::submitAppData(): "
        + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return submitOk;
  }

  /*************************************************************************
  **
  **   Load
  **
  **************************************************************************/

  /*
  ** protected void loadMerchant(long appSeqNum) throws Exception
  **
  ** Loads merchant table data into fields.
  */
  protected void loadMerchant(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    try
    {
      // load data from merchant
      /*@lineinfo:generated-code*//*@lineinfo:3084^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_business_name           business_name,
//                  lpad(to_char(merch_federal_tax_id),
//                    9,'0')                      taxpayer_id,
//                  merch_number                  merchant_number,
//                  merch_business_establ_month   established_date_month,
//                  merch_business_establ_year    established_date_year,
//                  merch_legal_name              business_legal_name,
//                  merch_mailing_name            mailing_name,
//                  merch_email_address           business_email,
//                  bustype_code                  business_type,
//                  industype_code                industry_type,
//                  merch_busgoodserv_descr       business_desc,
//                  merch_num_of_locations        num_locations,
//                  merch_years_at_loc            location_years,
//                  loctype_code                  location_type,
//                  merch_prior_cc_accp_flag      have_processed,
//                  merch_prior_processor         previous_processor,
//                  merch_cc_acct_term_flag       have_canceled,
//                  merch_cc_term_name            canceled_processor,
//                  merch_term_reason             canceled_reason,
//                  merch_term_year               cancel_date_year,
//                  merch_term_month              cancel_date_month,
//                  merch_month_tot_proj_sales    monthly_sales,
//                  merch_month_visa_mc_sales     monthly_vmc_sales,
//                  annual_sales                  annual_sales,
//                  annual_vmc_sales              annual_vmc_sales,
//                  merch_average_cc_tran         average_ticket,
//                  highest_ticket                highest_ticket,
//                  merch_mail_phone_sales        moto_percentage,
//                  refundtype_code               refund_policy,
//                  merch_application_type        application_type,
//                  merch_prior_statements        statements_provided,
//                  merch_gender                  owner_1_gender,
//                  merch_bank_number             not_used_2,
//                  franchise_code                not_used_3,
//                  asso_number                   assoc_num,
//                  account_type                  account_type,
//                  svb_cif_num                   cif,
//                  app_sic_code                  sic_code,
//                  seasonal_months               seasonal_months,
//                  percent_internet              internet_percent,
//                  percent_card_present          card_percent,
//                  percent_card_not_present      no_card_percent,
//                  ebt_options                   ebt_options,
//                  merch_application_type        application_type,
//                  merch_referring_bank          referring_bank,
//                  merch_web_url                 web_url,
//                  merch_notes                   comments,
//                  existing_verisign             existing_verisign,
//                  total_num_multi_merch         multi_merch_total,
//                  num_multi_merch               multi_merch_num,
//                  multi_merch_master_dba        multi_merch_master_dba,
//                  multi_merch_master_cntrl      multi_merch_master_control,
//                  vmc_accept_types              vmc_accept_types,
//                  customer_service_phone        customer_service_phone,
//                  old_merchant_number           old_merchant_number
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_business_name           business_name,\n                lpad(to_char(merch_federal_tax_id),\n                  9,'0')                      taxpayer_id,\n                merch_number                  merchant_number,\n                merch_business_establ_month   established_date_month,\n                merch_business_establ_year    established_date_year,\n                merch_legal_name              business_legal_name,\n                merch_mailing_name            mailing_name,\n                merch_email_address           business_email,\n                bustype_code                  business_type,\n                industype_code                industry_type,\n                merch_busgoodserv_descr       business_desc,\n                merch_num_of_locations        num_locations,\n                merch_years_at_loc            location_years,\n                loctype_code                  location_type,\n                merch_prior_cc_accp_flag      have_processed,\n                merch_prior_processor         previous_processor,\n                merch_cc_acct_term_flag       have_canceled,\n                merch_cc_term_name            canceled_processor,\n                merch_term_reason             canceled_reason,\n                merch_term_year               cancel_date_year,\n                merch_term_month              cancel_date_month,\n                merch_month_tot_proj_sales    monthly_sales,\n                merch_month_visa_mc_sales     monthly_vmc_sales,\n                annual_sales                  annual_sales,\n                annual_vmc_sales              annual_vmc_sales,\n                merch_average_cc_tran         average_ticket,\n                highest_ticket                highest_ticket,\n                merch_mail_phone_sales        moto_percentage,\n                refundtype_code               refund_policy,\n                merch_application_type        application_type,\n                merch_prior_statements        statements_provided,\n                merch_gender                  owner_1_gender,\n                merch_bank_number             not_used_2,\n                franchise_code                not_used_3,\n                asso_number                   assoc_num,\n                account_type                  account_type,\n                svb_cif_num                   cif,\n                app_sic_code                  sic_code,\n                seasonal_months               seasonal_months,\n                percent_internet              internet_percent,\n                percent_card_present          card_percent,\n                percent_card_not_present      no_card_percent,\n                ebt_options                   ebt_options,\n                merch_application_type        application_type,\n                merch_referring_bank          referring_bank,\n                merch_web_url                 web_url,\n                merch_notes                   comments,\n                existing_verisign             existing_verisign,\n                total_num_multi_merch         multi_merch_total,\n                num_multi_merch               multi_merch_num,\n                multi_merch_master_dba        multi_merch_master_dba,\n                multi_merch_master_cntrl      multi_merch_master_control,\n                vmc_accept_types              vmc_accept_types,\n                customer_service_phone        customer_service_phone,\n                old_merchant_number           old_merchant_number\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"31com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3144^7*/
      setFields(it.getResultSet());
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadMerchant()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      it.close();
    }
  }

  /*
  ** protected void loadAddresses(long appSeqNum) throws Exception
  **
  ** Loads merchant addresses into fields (main, mailing).
  */
  protected void loadAddresses(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    try
    {
      // business location address (no PO Boxes)
      /*@lineinfo:generated-code*//*@lineinfo:3169^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1     business_address_1,
//                  address_line2     business_address_2,
//                  address_city      business_csz_city,
//                  countrystate_code business_csz_state,
//                  country_code      business_country,
//                  address_zip       business_csz_zip,
//                  address_phone     business_phone,
//                  address_fax       business_fax
//          from    address
//          where   app_seq_num = :appSeqNum and
//                  addresstype_code = :mesConstants.ADDR_TYPE_BUSINESS
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1     business_address_1,\n                address_line2     business_address_2,\n                address_city      business_csz_city,\n                countrystate_code business_csz_state,\n                country_code      business_country,\n                address_zip       business_csz_zip,\n                address_phone     business_phone,\n                address_fax       business_fax\n        from    address\n        where   app_seq_num =  :1  and\n                addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_BUSINESS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"32com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3182^7*/
      setFields(it.getResultSet());
      it.close();

      // business mailing address
      /*@lineinfo:generated-code*//*@lineinfo:3187^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1     mailing_address_1,
//                  address_line2     mailing_address_2,
//                  address_city      mailing_csz_city,
//                  countrystate_code mailing_csz_state,
//                  address_zip       mailing_csz_zip
//          from    address
//          where   app_seq_num       = :appSeqNum and
//                  addresstype_code  = :mesConstants.ADDR_TYPE_MAILING
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1     mailing_address_1,\n                address_line2     mailing_address_2,\n                address_city      mailing_csz_city,\n                countrystate_code mailing_csz_state,\n                address_zip       mailing_csz_zip\n        from    address\n        where   app_seq_num       =  :1  and\n                addresstype_code  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_MAILING);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"33com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3197^7*/
      setFields(it.getResultSet());
      it.close();
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadAddresses()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      it.close();
    }
  }

  /*
  ** protected void loadOwners(long appSeqNum) throws Exception
  **
  ** Loads business principals (primary, secondary) into fields.
  */
  protected void loadOwners(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    try
    {
      // primary owner address
      /*@lineinfo:generated-code*//*@lineinfo:3223^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1     owner1_address_1,
//                  address_line2     owner1_address_2,
//                  address_city      owner1_csz_city,
//                  countrystate_code owner1_csz_state,
//                  country_code      owner1_country,
//                  address_zip       owner1_csz_zip,
//                  address_phone     owner1_phone
//          from    address
//          where   app_seq_num       = :appSeqNum and
//                  addresstype_code  = :mesConstants.ADDR_TYPE_OWNER1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1     owner1_address_1,\n                address_line2     owner1_address_2,\n                address_city      owner1_csz_city,\n                countrystate_code owner1_csz_state,\n                country_code      owner1_country,\n                address_zip       owner1_csz_zip,\n                address_phone     owner1_phone\n        from    address\n        where   app_seq_num       =  :1  and\n                addresstype_code  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_OWNER1);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"34com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3235^7*/
      setFields(it.getResultSet());
      it.close();

      // secondary owner address
      /*@lineinfo:generated-code*//*@lineinfo:3240^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1     owner2_address_1,
//                  address_line2     owner2_address_2,
//                  address_city      owner2_csz_city,
//                  countrystate_code owner2_csz_state,
//                  country_code      owner2_country,
//                  address_zip       owner2_csz_zip,
//                  address_phone     owner2_phone
//          from    address
//          where   app_seq_num       = :appSeqNum and
//                  addresstype_code  = :mesConstants.ADDR_TYPE_OWNER2
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1     owner2_address_1,\n                address_line2     owner2_address_2,\n                address_city      owner2_csz_city,\n                countrystate_code owner2_csz_state,\n                country_code      owner2_country,\n                address_zip       owner2_csz_zip,\n                address_phone     owner2_phone\n        from    address\n        where   app_seq_num       =  :1  and\n                addresstype_code  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"35com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_OWNER2);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"35com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3252^7*/
      setFields(it.getResultSet());
      it.close();

      // primary business owner
      /*@lineinfo:generated-code*//*@lineinfo:3257^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_last_name      owner1_name_last,
//                  busowner_first_name     owner1_name_first,
//                  lpad(to_char(busowner_ssn),
//                    9, '0')               owner1_ssn,
//                  busowner_owner_perc     owner1_percent,
//                  busowner_period_month   owner1_since_month,
//                  busowner_period_year    owner1_since_year,
//                  busowner_title          owner1_title,
//                  busowner_dl_number      owner1_dl_number,
//                  busowner_dl_expiration  owner1_dl_exp_date,
//                  busowner_dl_issue       owner1_dl_issue_date,
//                  busowner_dl_state       owner1_dl_state,
//                  busowner_dl_dob         owner1_dl_dob
//          from    businessowner
//          where   app_seq_num  = :appSeqNum and
//                  busowner_num = :mesConstants.BUS_OWNER_PRIMARY
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_last_name      owner1_name_last,\n                busowner_first_name     owner1_name_first,\n                lpad(to_char(busowner_ssn),\n                  9, '0')               owner1_ssn,\n                busowner_owner_perc     owner1_percent,\n                busowner_period_month   owner1_since_month,\n                busowner_period_year    owner1_since_year,\n                busowner_title          owner1_title,\n                busowner_dl_number      owner1_dl_number,\n                busowner_dl_expiration  owner1_dl_exp_date,\n                busowner_dl_issue       owner1_dl_issue_date,\n                busowner_dl_state       owner1_dl_state,\n                busowner_dl_dob         owner1_dl_dob\n        from    businessowner\n        where   app_seq_num  =  :1  and\n                busowner_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.BUS_OWNER_PRIMARY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"36com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3275^7*/
      setFields(it.getResultSet());
      it.close();

      // secondary business owner
      /*@lineinfo:generated-code*//*@lineinfo:3280^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_last_name      owner2_name_last,
//                  busowner_first_name     owner2_name_first,
//                  decode(busowner_ssn,
//                         null,' ',
//                         0, ' ',
//                         lpad(to_char(busowner_ssn),
//                              9,'0'))     owner2_ssn,
//                  busowner_owner_perc     owner2_percent,
//                  busowner_period_month   owner2_since_month,
//                  busowner_period_year    owner2_since_year,
//                  busowner_title          owner2_title,
//                  busowner_dl_number      owner2_dl_number,
//                  busowner_dl_expiration  owner2_dl_exp_date,
//                  busowner_dl_issue       owner2_dl_issue_date,
//                  busowner_dl_state       owner2_dl_state,
//                  busowner_dl_dob         owner2_dl_dob
//          from    businessowner
//          where   app_seq_num  = :appSeqNum and
//                  busowner_num = :mesConstants.BUS_OWNER_SECONDARY
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_last_name      owner2_name_last,\n                busowner_first_name     owner2_name_first,\n                decode(busowner_ssn,\n                       null,' ',\n                       0, ' ',\n                       lpad(to_char(busowner_ssn),\n                            9,'0'))     owner2_ssn,\n                busowner_owner_perc     owner2_percent,\n                busowner_period_month   owner2_since_month,\n                busowner_period_year    owner2_since_year,\n                busowner_title          owner2_title,\n                busowner_dl_number      owner2_dl_number,\n                busowner_dl_expiration  owner2_dl_exp_date,\n                busowner_dl_issue       owner2_dl_issue_date,\n                busowner_dl_state       owner2_dl_state,\n                busowner_dl_dob         owner2_dl_dob\n        from    businessowner\n        where   app_seq_num  =  :1  and\n                busowner_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.BUS_OWNER_SECONDARY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"37com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3301^7*/
      setFields(it.getResultSet());
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadOwners()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      it.close();
    }
  }

  /*
  ** protected void loadContact(long appSeqNum) throws Exception
  **
  ** Loads merchant contact information into fields.
  */
  protected void loadContact(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    try
    {
      // contact data
      /*@lineinfo:generated-code*//*@lineinfo:3326^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchcont_prim_first_name   contact_name_first,
//                  merchcont_prim_last_name    contact_name_last,
//                  merchcont_prim_phone        contact_phone,
//                  merchcont_prim_phone_ext    contact_phone_ext,
//                  merchcont_prim_email        contact_email
//          from    merchcontact
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merchcont_prim_first_name   contact_name_first,\n                merchcont_prim_last_name    contact_name_last,\n                merchcont_prim_phone        contact_phone,\n                merchcont_prim_phone_ext    contact_phone_ext,\n                merchcont_prim_email        contact_email\n        from    merchcontact\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"38com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"38com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3335^7*/
      setFields(it.getResultSet());

    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadContact()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      it.close();
    }
  }

  /*
  ** protected void loadBank(long appSeqNum) throws Exception
  **
  ** Loads merchant banking account data into fields.
  */
  protected void loadBank(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    try
    {
      // bank account information
      /*@lineinfo:generated-code*//*@lineinfo:3361^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bankacc_type                  type_of_acct,
//                  merchbank_info_source         source_of_info,
//                  merchbank_name                bank_name,
//                  merchbank_acct_num            checking_account,
//                  merchbank_acct_num            confirm_checking_account,
//                  merchbank_transit_route_num   transit_routing,
//                  merchbank_transit_route_num   confirm_transit_routing,
//                  merchbank_num_years_open      years_open,
//                  billing_method                billing_method
//          from    merchbank
//          where   app_seq_num = :appSeqNum and
//                  merchbank_acct_srnum = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bankacc_type                  type_of_acct,\n                merchbank_info_source         source_of_info,\n                merchbank_name                bank_name,\n                merchbank_acct_num            checking_account,\n                merchbank_acct_num            confirm_checking_account,\n                merchbank_transit_route_num   transit_routing,\n                merchbank_transit_route_num   confirm_transit_routing,\n                merchbank_num_years_open      years_open,\n                billing_method                billing_method\n        from    merchbank\n        where   app_seq_num =  :1  and\n                merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"39com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3375^7*/
      setFields(it.getResultSet());
      it.close();

      // bank address
      /*@lineinfo:generated-code*//*@lineinfo:3380^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1       bank_address,
//                  address_city        bank_csz_city,
//                  countrystate_code   bank_csz_state,
//                  address_zip         bank_csz_zip,
//                  address_phone       bank_phone
//          from    address
//          where   app_seq_num       = :appSeqNum and
//                  addresstype_code  = :mesConstants.ADDR_TYPE_CHK_ACCT_BANK
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1       bank_address,\n                address_city        bank_csz_city,\n                countrystate_code   bank_csz_state,\n                address_zip         bank_csz_zip,\n                address_phone       bank_phone\n        from    address\n        where   app_seq_num       =  :1  and\n                addresstype_code  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"40com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_CHK_ACCT_BANK);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"40com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3390^7*/
      setFields(it.getResultSet());
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadAddresses()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      it.close();
    }
  }

  /*
  ** protected void loadCards(long appSeqNum) throws Exception
  **
  ** Loads merchant card options.
  */
  protected void loadCards(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    try
    {
      // payment options
      /*@lineinfo:generated-code*//*@lineinfo:3415^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code               card_type,
//                  merchpo_card_merch_number   merchant_number,
//                  merchpo_provider_name       provider_name,
//                  merchpo_rate                rate,
//                  merchpo_fee                 per_item,
//                  merchpo_split_dial          split_dial,
//                  merchpo_pip                 amex_pip,
//                  merchpo_tid                 tid,
//                  annual_sales                annual_sales,
//                  average_ticket              average_ticket,
//                  group_merchant_number       group_number
//          from   merchpayoption
//          where  app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code               card_type,\n                merchpo_card_merch_number   merchant_number,\n                merchpo_provider_name       provider_name,\n                merchpo_rate                rate,\n                merchpo_fee                 per_item,\n                merchpo_split_dial          split_dial,\n                merchpo_pip                 amex_pip,\n                merchpo_tid                 tid,\n                annual_sales                annual_sales,\n                average_ticket              average_ticket,\n                group_merchant_number       group_number\n        from   merchpayoption\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"41com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"41com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3430^7*/
      ResultSet rs = it.getResultSet();
      while (rs.next())
      {
        switch (rs.getInt("card_type"))
        {
          case mesConstants.APP_CT_VISA:      // ignore, always selected
          case mesConstants.APP_CT_MC:
            break;

          case mesConstants.APP_CT_DEBIT:
            fields.setData("debitAccepted",     "y");
            fields.setData("fcsNumber",         rs.getString("merchant_number"));
            break;

          case mesConstants.APP_CT_EBT:
            fields.setData("ebtAccepted",       "y");
            fields.setData("fcsNumber",         rs.getString("merchant_number"));
            break;

          case mesConstants.APP_CT_DINERS_CLUB:
            fields.setData("dinersAccepted",    "y");
            fields.setData("dinersAcctNum",     rs.getString("merchant_number"));
            break;

          case mesConstants.APP_CT_JCB:
            fields.setData("jcbAccepted",       "y");
            fields.setData("jcbAcctNum",        rs.getString("merchant_number"));
            break;

          case mesConstants.APP_CT_AMEX:
            fields.setData("amexAccepted",      "y");
            fields.setData("amexAcctNum",       rs.getString("merchant_number"));
            fields.setData("amexSplitDial",     rs.getString("split_dial"));
            fields.setData("amexPIP",           rs.getString("amex_pip"));
            fields.setData("amexEsaRate",       rs.getString("rate"));

            // extended fields
            fields.setData("amexAnnualSales", rs.getString("annual_sales"));
            fields.setData("amexAvgTicket",   rs.getString("average_ticket"));
            fields.setData("amexFranchiseSE", rs.getString("group_number"));
            break;

          case mesConstants.APP_CT_DISCOVER:
            fields.setData("discoverAccepted",  "y");
            fields.setData("discoverAcctNum",   rs.getString("merchant_number"));
            fields.setData("discoverRapRate",   rs.getString("rate"));
            fields.setData("discoverRapPerItem",rs.getString("per_item"));

            // extended fields
            fields.setData("discoverAnnualSales", rs.getString("annual_sales"));
            fields.setData("discoverAvgTicket", rs.getString("average_ticket"));
            fields.setData("discoverCapNumber", rs.getString("group_number"));
            fields.setData("discoverCapNumber", rs.getString("group_number"));
            break;

          case mesConstants.APP_CT_CHECK_AUTH:
            fields.setData("checkAccepted",     "y");

            // load the provider data, separate other description if necessary
            String  checkProvider       = rs.getString("provider_name");
            String  checkProviderOther  = null;

            if( checkProvider.substring(0,7).equals("CPOther") )
            {
              checkProviderOther  = checkProvider.substring(8);
              checkProvider       = checkProvider.substring(0,7);
            }
            fields.setData("checkProvider", checkProvider);
            fields.setData("checkProviderOther", checkProviderOther);
            fields.setData("checkAcctNum", rs.getString("merchant_number"));
            fields.setData("checkTermId", rs.getString("tid"));
            break;

          case mesConstants.APP_CT_VALUTEC_GIFT_CARD:
            fields.setData("valutecAccepted", "y");
            fields.setData("valutecAcctNum", rs.getString("merchant_number"));
            fields.setData("valutecTermId", rs.getString("tid"));
            break;

          case mesConstants.APP_CT_SPS:
            fields.setData("spsAccepted", "y");
            fields.setData("spsAcctNum", rs.getString("merchant_number"));
            break;

          default:
            break;
        }
      }
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadCards()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      it.close();
    }
  }
  
  /*
  ** protected double calculateDiscoverRate()
  **
  ** Attempts to calculate Discover RAP discount rate based on logic from Discover
  */
  protected double calculateDiscoverRate()
  {
    double result = DEFAULT_RAP_RATE;
    
    try
    {
      int industryIdx = 0;
      int ticketIdx   = 0;
      
      if(! getData("industryType").equals("") &&
         ! getData("averageTicket").equals("") )
      {
        switch(Integer.parseInt(getData("industryType")))
        {
          case 2:   // Restaurant
            industryIdx = 2;
            break;
          
          case 5:   // Internet
          case 7:   // Direct Marketing (DMI)
          case 14:  // MOTO
            industryIdx = 1;
        
          case 1:   // Retail
          default:
            industryIdx = 0;
            break;
        }
      
        double avgTicket = Double.parseDouble(getData("averageTicket"));
      
        if( 0 <= avgTicket && avgTicket < 21 )
        {
          ticketIdx = 0;
        }
        else if( 21 <= avgTicket && avgTicket <31)
        {
          ticketIdx = 1;
        }
        else if( 31 <= avgTicket && avgTicket <51)
        {
          ticketIdx = 2;
        }
        else if( 51 <= avgTicket && avgTicket <71)
        {
          ticketIdx = 3;
        }
        else if( 71 <= avgTicket && avgTicket <91)
        {
          ticketIdx = 4;
        }
        else if( 91 <= avgTicket && avgTicket <121)
        {
          ticketIdx = 5;
        }
        else if( 121 <= avgTicket && avgTicket <151 )
        {
          ticketIdx = 6; 
        }
        else
        {
          ticketIdx = 7;
        }
        
        result = RAPRates[ticketIdx][industryIdx];
      }
    }
    catch(Exception e)
    {
      logEntry("calculateDiscoverRate(" + getData("appSeqNum") + ")", e.toString());
    }
    
    return( result );
  }

  /*
  ** protected void loadProduct(long appSeqNum) throws Exception
  **
  ** Loads merchant POS type data into app fields
  */
  protected void loadProduct(long appSeqNum) throws Exception
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    try
    {
      // product type
      /*@lineinfo:generated-code*//*@lineinfo:3623^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mp.pos_code           internet_type,
//                  mp.pos_param          pos_param,
//                  pc.pos_type           product_type,
//                  mp.pp_connectivity    pos_partner_connectivity,
//                  mp.pp_level_ii        pos_partner_level_ii,
//                  mp.pp_level_iii       pos_partner_level_iii,
//                  mp.pp_os              pos_partner_os,
//                  nvl(mp.existing, 'N') existing,
//                  mp.pg_email           pg_email,
//                  mp.pg_card_present    pg_card_present,
//                  mp.pg_ecomm           pg_ecomm
//          from    merch_pos     mp,
//                  pos_category  pc
//          where   mp.app_seq_num = :appSeqNum and
//                  pc.pos_code = mp.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mp.pos_code           internet_type,\n                mp.pos_param          pos_param,\n                pc.pos_type           product_type,\n                mp.pp_connectivity    pos_partner_connectivity,\n                mp.pp_level_ii        pos_partner_level_ii,\n                mp.pp_level_iii       pos_partner_level_iii,\n                mp.pp_os              pos_partner_os,\n                nvl(mp.existing, 'N') existing,\n                mp.pg_email           pg_email,\n                mp.pg_card_present    pg_card_present,\n                mp.pg_ecomm           pg_ecomm\n        from    merch_pos     mp,\n                pos_category  pc\n        where   mp.app_seq_num =  :1  and\n                pc.pos_code = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"42com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"42com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3640^7*/

      // there is a special condition here.  if the product type is POS_INTERNET
      // then the pos_param column should fill the webUrl field on the app.
      // However if the product type is POS_OTHER then the pos_param column
      // should fill the vitalProduct field on the app... this also now includes filling
      // the pgCertName for POS_PAYMENT_GATEWAY and the vtEmail field for POS_VIRTUAL_TERMINAL
      rs = it.getResultSet();

      if(rs.next())
      {
        // set internet type and product type
        fields.setData("internetType", rs.getString("internet_type"));
        fields.setData("productType", rs.getString("product_type"));

        switch(fields.getField("productType").asInteger())
        {
          case mesConstants.POS_OTHER:
            fields.setData("vitalProduct", rs.getString("pos_param"));
            break;

          case mesConstants.POS_INTERNET:
            fields.setData("webUrl", rs.getString("pos_param"));
            fields.setData("existingInternet", rs.getString("existing"));
            fields.setData("internetEmail", rs.getString("pg_email"));
            break;

          case mesConstants.POS_PC:
            // set pos partner fields
            fields.setData("posPartnerConnectivity", rs.getString("pos_partner_connectivity"));
            fields.setData("posPartnerLevelII", rs.getString("pos_partner_level_ii"));
            fields.setData("posPartnerLevelIII", rs.getString("pos_partner_level_iii"));
            fields.setData("posPartnerOS", rs.getString("pos_partner_os"));
            break;

          case mesConstants.POS_CHARGE_EXPRESS:
            fields.setData("existingPCChargeExpress", rs.getString("existing"));
            break;

          case mesConstants.POS_CHARGE_PRO:
            fields.setData("existingPCChargePro", rs.getString("existing"));
            break;

          case mesConstants.POS_VIRTUAL_TERMINAL:
            fields.setData("vtEmail", rs.getString("pos_param"));
            break;
            
          case mesConstants.POS_VT_LIMITED:
            fields.setData("vtLimitedEmail", rs.getString("pos_param"));
            break;

          case mesConstants.POS_PAYMENT_GATEWAY:
            fields.setData("pgCertName",    rs.getString("pos_param"));
            fields.setData("pgEmail",       rs.getString("pg_email"));
            fields.setData("pgCard",        rs.getString("internet_type"));
            break;

          default:
            break;
        }
      }

      // retrieve imprinter plate info if present
      int plateCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:3704^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchequipment
//          where   app_seq_num = :appSeqNum and
//                  equiptype_code = :mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES and
//                  equiplendtype_code = :mesConstants.APP_EQUIP_PURCHASE and
//                  equip_model = 'IPPL'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchequipment\n        where   app_seq_num =  :1  and\n                equiptype_code =  :2  and\n                equiplendtype_code =  :3  and\n                equip_model = 'IPPL'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"43com.mes.app.BusinessBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_PURCHASE);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   plateCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3713^7*/

      if(plateCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:3717^9*/

//  ************************************************************
//  #sql [Ctx] { select  merchequip_equip_quantity
//            
//            from    merchequipment
//            where   app_seq_num = :appSeqNum and
//                    equiptype_code = :mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES and
//                    equiplendtype_code = :mesConstants.APP_EQUIP_PURCHASE and
//                    equip_model = 'IPPL'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merchequip_equip_quantity\n           \n          from    merchequipment\n          where   app_seq_num =  :1  and\n                  equiptype_code =  :2  and\n                  equiplendtype_code =  :3  and\n                  equip_model = 'IPPL'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"44com.mes.app.BusinessBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_IMPRINTER_PLATES);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_PURCHASE);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   plateCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3726^9*/

        setData("imprinterPlates", Integer.toString(plateCount));
      }
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadProduct()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** protected void loadCreditContact(long appSeqNum) throws Exception
  **
  ** Loads merchant POS type data into app fields
  */
  protected void loadCreditContact(long appSeqNum) throws Exception
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:3754^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  contact_merchant  credit_contact_merchant,
//                  contact_rep       credit_contact_rep,
//                  rep_email         rep_email
//          from    credit_contact_info
//          where   app_seq_num = :getData("appSeqNum")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2025 = getData("appSeqNum");
  try {
   String theSqlTS = "select  contact_merchant  credit_contact_merchant,\n                contact_rep       credit_contact_rep,\n                rep_email         rep_email\n        from    credit_contact_info\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"45com.mes.app.BusinessBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2025);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"45com.mes.app.BusinessBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3761^7*/
      
      rs = it.getResultSet();
      
      setFields(rs);
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadCreditContact()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  /*
  ** protected boolean loadAppData()
  **
  ** Loads app data.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean loadAppData()
  {
    boolean loadOk = false;
    try
    {
      connect();

      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        loadMerchant(appSeqNum);
        loadAddresses(appSeqNum);
        loadOwners(appSeqNum);
        loadContact(appSeqNum);
        loadBank(appSeqNum);
        loadCards(appSeqNum);
        loadProduct(appSeqNum);
        loadCreditContact(appSeqNum);
      }
      loadOk = true;
    }
    catch(Exception e)
    {
      logEntry("loadData()",e.toString());
      System.out.println(this.getClass().getName() + "::loadAppData(): "
        + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return loadOk;
  }
}/*@lineinfo:generated-code*/