/*@lineinfo:filename=VaStart*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/verisign/VaStart.sqlj $

  Description:

  VaStart

  Data bean used by "first time sign-in" page of the verisign virtual app.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-05-31 14:24:22 -0700 (Thu, 31 May 2012) $
  Version            : $Revision: 20222 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.verisign;

import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.AppSequence;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.PasswordField;
import com.mes.forms.Validation;
import com.mes.user.UserBean;

public class VaStart extends VaBase
{
  public class PasswordValidation implements Validation
  {
    String errorText;
    
    public boolean validate(String fdata)
    {
      boolean isValid = false;
      
      // don't try to validate if blank, let required validation get that
      if (fdata == null || fdata.equals(""))
      {
        return true;
      }
      
      if (fdata.length() < 6)
      {
        errorText = "Must be 6 characters or more";
      }
      else
      {
        isValid = true;
      }
      
      return isValid;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }
    
  /*
  ** public void createFields(HttpServletRequest request)
  **
  ** Sets up verisign virtual app post fields and aliases.  Also creates fields
  ** to get sign in data from the user.
  */
  public void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    // post data fields
    fields.add(new HiddenField("vid"));
    fields.add(new HiddenField("enterpriseId"));
    fields.add(new HiddenField("smbid"));
    fields.add(new HiddenField("partnerName"));
    fields.add(new HiddenField("partnerId"));
    fields.add(new HiddenField("product"));
    fields.add(new HiddenField("quickStoreLogin"));
    fields.add(new HiddenField("companyName"));
    fields.add(new HiddenField("address1"));
    fields.add(new HiddenField("address2"));
    fields.add(new HiddenField("city"));
    fields.add(new HiddenField("state"));
    fields.add(new HiddenField("zip"));
    fields.add(new HiddenField("contactPhone"));
    fields.add(new HiddenField("contactFax"));
    fields.add(new HiddenField("businessEmail"));
    fields.add(new HiddenField("contactName"));
    fields.add(new HiddenField("contactFirstName"));
    fields.add(new HiddenField("contactLastName"));
    fields.add(new HiddenField("returnUrl"));
    fields.add(new HiddenField("companyUrl"));
    fields.add(new HiddenField("userLoginName"));
    fields.add(new HiddenField("nsiapp"));
    
    // http://www.merchantesolutions.com/application/team1.asp
    // https://www.merchante-solutions.com/jsp/setup/merchinfo2.jsp?id=292622

    // verisign aliases
    fields.addAlias("vid",              "VID");
    fields.addAlias("partnerName",      "PARTNER");
    fields.addAlias("partnerId",        "PID");
    fields.addAlias("product",          "PRODUCT");
    fields.addAlias("companyName",      "Company");
    fields.addAlias("companyName",      "COMPANY");
    fields.addAlias("address1",         "ADDR1");
    fields.addAlias("address2",         "ADDR2");
    fields.addAlias("city",             "CITY");
    fields.addAlias("state",            "STATE");
    fields.addAlias("zip",              "ZIP");
    fields.addAlias("contactPhone",     "PHONE1");
    fields.addAlias("contactFax",       "FAX1");
    fields.addAlias("businessEmail",    "EMAIL1");
    fields.addAlias("contactName",      "contactname");
    fields.addAlias("contactFirstName", "FNAME1");
    fields.addAlias("contactLastName",  "LNAME1");
    fields.addAlias("returnUrl",        "returnURL");
    fields.addAlias("returnUrl",        "RETURNURL");
    fields.addAlias("companyUrl",       "CompanyURL");
    fields.addAlias("companyUrl",       "COMPANYRL");

    // sign in fields
    fields.add(new Field        ("email",     "Email Address",60,30,false));
    fields.add(new PasswordField("password",  "Password",16,30,false));
    fields.add(new PasswordField("confirm",   "Confirm Password",16,30,false));

    fields.getField("confirm")
      .addValidation(new FieldEqualsFieldValidation(
        fields.getField("password"),"Password"));
    fields.getField("password").addValidation(new PasswordValidation());

    fields.add(new ButtonField("signin","Sign In","Sign In"));

    // verisign style error indicator
    fields.setFixImage("/images/arrow1_left.gif",10,10);

    // set the current screen id
    fields.setData("curScreenId","1");
  }
  
  /*
  ** private void storePostedData(HttpServletRequest)
  **
  ** Stores posted data in vs_post_data.  Only stores the data if it hasn't been
  ** stored before for this vid.
  */
  private void storePostedData(HttpServletRequest request)
  {
    if (!fields.getField("vid").isBlank())
    {
      String vid = fields.getData("vid");
      try
      {
        connect();
        
        // see if entry already exists for this vid
        int vidCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:175^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(vid)
//            
//            from    vs_post_data
//            where   vid = :vid
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(vid)\n           \n          from    vs_post_data\n          where   vid =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.verisign.VaStart",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,vid);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vidCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:181^9*/

        // store the post data if not already stored
        if (vidCount == 0)
        {
          // place the posted data in a string buffer
          StringBuffer  pd    = new StringBuffer("");
          Enumeration   myEnum  = request.getParameterNames();
          while (myEnum.hasMoreElements())
          {
            String name = (String)myEnum.nextElement();
            String[] values = request.getParameterValues(name);
            pd.append("[");
            pd.append(name);
            pd.append(": ");
            for (int i=0; i < values.length; ++i)
            {
              if (i > 0)
              {
                pd.append(", ");
              }
              pd.append(values[i]);
            }
            pd.append("] ");
          }

          // store the posted data
          /*@lineinfo:generated-code*//*@lineinfo:208^11*/

//  ************************************************************
//  #sql [Ctx] { insert into vs_post_data
//              ( vid,
//                post_date,
//                post_data )
//              values
//              ( :vid,
//                sysdate,
//                :pd.toString() )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3242 = pd.toString();
   String theSqlTS = "insert into vs_post_data\n            ( vid,\n              post_date,\n              post_data )\n            values\n            (  :1 ,\n              sysdate,\n               :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.app.verisign.VaStart",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,vid);
   __sJT_st.setString(2,__sJT_3242);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:218^11*/
        }
      }
      catch(Exception e)
      {
        System.out.println(getClass().getName() + "::storePostedData(): " 
          + e.toString());
        logEntry("storePostedData()",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
  }
  
  /*
  ** protected void postHandleRequest(HttpServletRequest request)
  **
  ** This method overrides default behavior which would normally assume the
  ** existence of a user in the session.  Instead this particular page actually
  ** removes any user in the session if it is present.  Upon successfully
  ** signing in a user will be generated in the session that is used with
  ** the rest of the screens in the virtual app (this happens in autoSubmit()).
  ** Posted data is stored here.
  */
  protected void postHandleRequest(HttpServletRequest request)
  {
    // remove any user in the session that may be present in case
    // a user returned to this page after signing in...this prevents
    // the problem where a user is present in the session but 
    request.getSession().removeAttribute("UserLogin");
    user = null;
    
    // may want to do some validations here of post data?
    storePostedData(request);
  }
  
  /*
  ** protected boolean autoSubmit()
  **
  ** If login id and password fields are valid this method will be called.  At this
  ** point a new application needs to be created.  This is accomplished by creating
  ** the app sequence object, configuring it, creating a user object and then using 
  ** the base app class's createNewApp() method.  After the app is created some of
  ** the posted data needs to be stored in vs_linking_table, as well as sign in
  ** fields in merchant.
  */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      // if this is a new signin id create new app, new signin, etc.
      signinIdExists = checkIdExists();
      if (!signinIdExists)
      {
        // create the application screen sequence
        appSeq = new AppSequence();
      
        // set the app type in the sequence 
        appSeq.setAppType(fields.getField("appType").asInteger());
      
        // create a user based on flavor of app we're using (! fix !)
        // and place it in the session
        user = new UserBean();
        user.forceValidate("vs-virtualapp");
        request.getSession().setAttribute("UserLogin",user);
      
        // create a new application
        createNewApp(request);
      
        // finish configuring the sequence object
        appSeq.setAppSeqNum(fields.getField("appSeqNum").asLong());
        appSeq.setCurScreenId(fields.getField("curScreenId").asInteger());
      
        long appSeqNum = fields.getField("appSeqNum").asLong();
      
        connect();
      
        // store vs_linking_table data
        /*@lineinfo:generated-code*//*@lineinfo:300^9*/

//  ************************************************************
//  #sql [Ctx] { insert into vs_linking_table
//            ( app_seq_num,
//              verisign_id,
//              merc_cntrl_number,
//              nsi_enterprise_id,
//              nsi_smbid,
//              nsi_login_id,
//              return_url,
//              business_url,
//              partner,
//              pid,
//              product )
//            values
//            ( :appSeqNum,
//              :fields.getData("vid"),
//              :fields.getData("controlNum"),
//              :fields.getData("enterpriseId"),
//              :fields.getData("smbid"),
//              :fields.getData("quickStoreLogin"),
//              :fields.getData("returnUrl"),
//              :fields.getData("companyUrl"),
//              :fields.getData("partnerName"),
//              :fields.getData("partnerId"),
//              :fields.getData("product") )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3243 = fields.getData("vid");
 String __sJT_3244 = fields.getData("controlNum");
 String __sJT_3245 = fields.getData("enterpriseId");
 String __sJT_3246 = fields.getData("smbid");
 String __sJT_3247 = fields.getData("quickStoreLogin");
 String __sJT_3248 = fields.getData("returnUrl");
 String __sJT_3249 = fields.getData("companyUrl");
 String __sJT_3250 = fields.getData("partnerName");
 String __sJT_3251 = fields.getData("partnerId");
 String __sJT_3252 = fields.getData("product");
   String theSqlTS = "insert into vs_linking_table\n          ( app_seq_num,\n            verisign_id,\n            merc_cntrl_number,\n            nsi_enterprise_id,\n            nsi_smbid,\n            nsi_login_id,\n            return_url,\n            business_url,\n            partner,\n            pid,\n            product )\n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.verisign.VaStart",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_3243);
   __sJT_st.setString(3,__sJT_3244);
   __sJT_st.setString(4,__sJT_3245);
   __sJT_st.setString(5,__sJT_3246);
   __sJT_st.setString(6,__sJT_3247);
   __sJT_st.setString(7,__sJT_3248);
   __sJT_st.setString(8,__sJT_3249);
   __sJT_st.setString(9,__sJT_3250);
   __sJT_st.setString(10,__sJT_3251);
   __sJT_st.setString(11,__sJT_3252);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:326^9*/
      
        // store sign in id and password
        /*@lineinfo:generated-code*//*@lineinfo:329^9*/

//  ************************************************************
//  #sql [Ctx] { insert into vs_login_ids
//            ( app_seq_num,
//              login_id, 
//              password,
//              create_date,
//              last_login )
//            values
//            ( :appSeqNum,
//              :fields.getData("email"), 
//              :fields.getData("password"),
//              sysdate,
//              sysdate )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3253 = fields.getData("email");
 String __sJT_3254 = fields.getData("password");
   String theSqlTS = "insert into vs_login_ids\n          ( app_seq_num,\n            login_id, \n            password,\n            create_date,\n            last_login )\n          values\n          (  :1 ,\n             :2 , \n             :3 ,\n            sysdate,\n            sysdate )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.verisign.VaStart",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_3253);
   __sJT_st.setString(3,__sJT_3254);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:343^9*/

        // store merchant data
        /*@lineinfo:generated-code*//*@lineinfo:346^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_login         = :fields.getData("email"),
//                    merch_password      = :fields.getData("password"),
//                    merch_business_name = :fields.getData("companyName"),
//                    merch_email_address = :fields.getData("businessEmail")
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3255 = fields.getData("email");
 String __sJT_3256 = fields.getData("password");
 String __sJT_3257 = fields.getData("companyName");
 String __sJT_3258 = fields.getData("businessEmail");
   String theSqlTS = "update  merchant\n          set     merch_login         =  :1 ,\n                  merch_password      =  :2 ,\n                  merch_business_name =  :3 ,\n                  merch_email_address =  :4 \n          where   app_seq_num =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.verisign.VaStart",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3255);
   __sJT_st.setString(2,__sJT_3256);
   __sJT_st.setString(3,__sJT_3257);
   __sJT_st.setString(4,__sJT_3258);
   __sJT_st.setLong(5,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:354^9*/
      
        // store contact data
        /*@lineinfo:generated-code*//*@lineinfo:357^9*/

//  ************************************************************
//  #sql [Ctx] { delete from merchcontact
//            where app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from merchcontact\n          where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.app.verisign.VaStart",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:361^9*/
      
        /*@lineinfo:generated-code*//*@lineinfo:363^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchcontact
//            ( merchcont_prim_first_name,
//              merchcont_prim_last_name,
//              merchcont_prim_phone,
//              app_seq_num )
//            values
//            ( :fields.getData("contactFirstName"),
//              :fields.getData("contactLastName"),
//              :fields.getData("contactPhone"),
//              :appSeqNum )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3259 = fields.getData("contactFirstName");
 String __sJT_3260 = fields.getData("contactLastName");
 String __sJT_3261 = fields.getData("contactPhone");
   String theSqlTS = "insert into merchcontact\n          ( merchcont_prim_first_name,\n            merchcont_prim_last_name,\n            merchcont_prim_phone,\n            app_seq_num )\n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.app.verisign.VaStart",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3259);
   __sJT_st.setString(2,__sJT_3260);
   __sJT_st.setString(3,__sJT_3261);
   __sJT_st.setLong(4,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:375^9*/
      
        // store address data
        /*@lineinfo:generated-code*//*@lineinfo:378^9*/

//  ************************************************************
//  #sql [Ctx] { delete from address
//            where app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from address\n          where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.app.verisign.VaStart",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:382^9*/
      
        /*@lineinfo:generated-code*//*@lineinfo:384^9*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//            ( address_line1,
//              address_line2,
//              address_city,
//              countrystate_code,
//              address_zip,
//              address_fax,
//              app_seq_num,
//              addresstype_code )
//            values
//            ( :fields.getData("address1"),
//              :fields.getData("address2"),
//              :fields.getData("city"),
//              :fields.getData("state"),
//              :fields.getData("zip"),
//              :fields.getData("contactFax"),
//              :appSeqNum,
//              1 )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3262 = fields.getData("address1");
 String __sJT_3263 = fields.getData("address2");
 String __sJT_3264 = fields.getData("city");
 String __sJT_3265 = fields.getData("state");
 String __sJT_3266 = fields.getData("zip");
 String __sJT_3267 = fields.getData("contactFax");
   String theSqlTS = "insert into address\n          ( address_line1,\n            address_line2,\n            address_city,\n            countrystate_code,\n            address_zip,\n            address_fax,\n            app_seq_num,\n            addresstype_code )\n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n            1 )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.app.verisign.VaStart",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3262);
   __sJT_st.setString(2,__sJT_3263);
   __sJT_st.setString(3,__sJT_3264);
   __sJT_st.setString(4,__sJT_3265);
   __sJT_st.setString(5,__sJT_3266);
   __sJT_st.setString(6,__sJT_3267);
   __sJT_st.setLong(7,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:404^9*/
      
        submitOk = true;
      }
    }
    catch (Exception e)
    {
      System.out.println(getClass().getName() + "::autoSubmit(): " + e.toString());
      logEntry("autoSubmit()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return submitOk;
  }

  /*
  ** private boolean checkIdExists()
  **
  ** Determines if a signin id is already present in vs_login_ids.
  **
  ** RETURNS: true if id exists, else false.
  */
  private boolean checkIdExists()
  {
    boolean checkResult = false;
    
    try
    {
      connect();
      
      // look for already existing login id
      int idCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:439^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(login_id)
//          
//          from    vs_login_ids
//          where   lower(login_id) = :fields.getData("email").toLowerCase()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3268 = fields.getData("email").toLowerCase();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(login_id)\n         \n        from    vs_login_ids\n        where   lower(login_id) =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.verisign.VaStart",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3268);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   idCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:445^7*/
      
      checkResult = (idCount > 0);
    }
    catch (Exception e)
    {
      System.out.println(getClass().getName() + "::checkIdExists(): " + e.toString());
      logEntry("checkIdExists()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return checkResult;
  }
  
  private boolean signinIdExists = false;
  
  /*
  ** public boolean getSigninIdExists()
  **
  ** RETURNS: true if the signin id the user tried to use is already in the system.
  */
  public boolean getSigninIdExists()
  {
    return signinIdExists;
  }
}/*@lineinfo:generated-code*/