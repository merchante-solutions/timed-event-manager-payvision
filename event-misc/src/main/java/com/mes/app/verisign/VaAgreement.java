/*@lineinfo:filename=VaAgreement*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/verisign/VaAgreement.sqlj $

  Description:  
  
  VaAgreement
  
  Data bean used by page 4 of the verisign virtual app.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/28/03 4:24p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.verisign;

import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.RadioButtonField;
import sqlj.runtime.ResultSetIterator;

public class VaAgreement extends VaBase
{
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // agreement radio button field
      fields.add(new RadioButtonField("agreement",
        new String[][] { { "I Agree", "1" }, { "I Do Not Agree", "2" } },
        -1, false,"Please indicate your agreement choice"));
        
      // submit button
      fields.add(new ButtonField("submit","Submit Application Data","Submit"));
      
      // verisign style error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);

      // set the current screen id
      fields.setData("curScreenId","5");
    }
    catch (Exception e)
    {
      System.out.println(getClass().getName() + "::createFields(): " + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
  
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    try
    {
      connect();
      
      long appSeqNum = fields.getField("appSeqNum").asLong();

      // store the agreement decision
      /*@lineinfo:generated-code*//*@lineinfo:80^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_agreement = :fields.getData("agreement")
//          where   app_seq_num     = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3193 = fields.getData("agreement");
   String theSqlTS = "update  merchant\n        set     merch_agreement =  :1 \n        where   app_seq_num     =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.app.verisign.VaAgreement",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3193);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:85^7*/

      submitOk = true;
    }
    catch (Exception e)
    {
      System.out.println(getClass().getName() + "::autoSubmit(): " + e.toString());
      logEntry("autoSubmit()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return submitOk;
  }
  
  protected boolean autoLoad()
  {
    boolean           loadOk  = false;
    ResultSetIterator it      = null;
    try
    {
      connect();
      
      long appSeqNum = fields.getField("appSeqNum").asLong();

      // load agreement decision
      /*@lineinfo:generated-code*//*@lineinfo:112^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_agreement as agreement
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_agreement as agreement\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.verisign.VaAgreement",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.verisign.VaAgreement",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:117^7*/
      setFields(it.getResultSet());

      loadOk = true;
    }
    catch (Exception e)
    {
      System.out.println(getClass().getName() + "::autoLoad(): " + e.toString());
      logEntry("autoLoad()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) { }
      cleanUp();
    }
    return loadOk;
  }
}/*@lineinfo:generated-code*/