/*@lineinfo:filename=VaReturn*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/verisign/VaReturn.sqlj $

  Description:  
  
  VaReturn
  
  Data bean used by return-to-app login page of the verisign virtual app.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 7/16/03 10:14a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.verisign;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.app.AppSequence;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.ButtonField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.PasswordField;
import com.mes.forms.Validation;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class VaReturn extends VaBase
{
  public class LoginIdValidation extends SQLJConnectionBase 
    implements Validation
  {
    public LoginIdValidation()
    {
    }
    
    public boolean validate(String fdata)
    {
      // don't try to validate when field is blank
      if (fdata == null || fdata.equals(""))
      {
        return true;
      }
      
      boolean isValid = false;
      try
      {
        connect();
        
        int count = 0;
        /*@lineinfo:generated-code*//*@lineinfo:66^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(login_id)
//            
//            from    vs_login_ids
//            where   login_id = :fdata
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(login_id)\n           \n          from    vs_login_ids\n          where   login_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.verisign.VaReturn",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fdata);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:72^9*/
        
        isValid = (count > 0);
      }
      catch(Exception e)
      {
        System.out.println(getClass().getName() + "::validate(): " 
          + e.toString());
        logEntry("validate()",e.toString());
      }
      finally
      {
        cleanUp();
      }
      return isValid;
    }
    
    public String getErrorText()
    {
      return "Login ID not found";
    }
  }
  
  public class PasswordValidation extends SQLJConnectionBase 
    implements Validation
  {
    private Field loginField;
    
    public PasswordValidation(Field loginField)
    {
      this.loginField = loginField;
    }
    
    public boolean validate(String fdata)
    {
      // don't try to validate if login field is invalid
      if (loginField.getHasError())
      {
        return true;
      }
      
      boolean           isValid = false;
      ResultSetIterator it      = null;
      ResultSet         rs      = null;
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:120^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  password
//            from    vs_login_ids
//            where   login_id = :loginField.getData()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3239 = loginField.getData();
  try {
   String theSqlTS = "select  password\n          from    vs_login_ids\n          where   login_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.verisign.VaReturn",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3239);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.verisign.VaReturn",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^9*/
        rs = it.getResultSet();
        if (rs.next())
        {
          isValid = rs.getString("password").equals(fdata);
        }
      }
      catch(Exception e)
      {
        System.out.println(getClass().getName() + "::validate(): " 
          + e.toString());
        logEntry("validate()",e.toString());
      }
      finally
      {
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
      return isValid;
    }
    
    public String getErrorText()
    {
      return "Invalid password";
    }
  }
  
  public VaReturn()
  {
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    fields.add(new EmailField     ("email",     "Email Address",60,30,false));
    fields.add(new PasswordField  ("password",  "Password",16,30,true));
    fields.add(new HiddenField    ("appSeqNum"));
    
    fields.getField("email").addValidation(new LoginIdValidation());
    fields.getField("password")
      .addValidation(new PasswordValidation(fields.getField("email")));
      
    fields.add(new ButtonField("submit","Sign In","Sign In"));
      
    // verisign style error indicator
    fields.setFixImage("/images/arrow1_left.gif",10,10);
    fields.setShowErrorText(true);
    
    // set the current screen id (pretending we're screen 1 here)
    fields.setData("curScreenId","1");
  }
  
  /*
  ** protected void postHandleRequest(HttpServletRequest request)
  **
  ** This method overrides default behavior which would normally assume the
  ** existence of a user in the session.  Instead this particular page actually
  ** removes any user in the session if it is present.  Upon successfully
  ** signing in a user will be generated in the session that is used with
  ** the rest of the screens in the virtual app.
  */
  protected void postHandleRequest(HttpServletRequest request)
  {
    // remove any user in the session that may be present in case
    // a user returned to this page after signing in...this prevents
    // the problem where a user is present in the session but 
    request.getSession().removeAttribute("UserLogin");
    user = null;
  }
  
  /*
  ** protected boolean autoSubmit()
  **
  ** Validation will have determined if the password and login id are valid.
  ** At this point a user need to be placed in the session, the last login
  ** date needs to be update and the app seq num needs to be retrieved
  ** from vs_login_ids.
  **
  ** RETURNS: true if login successful, else false.
  */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    try
    {
      // create the application screen sequence
      appSeq = new AppSequence();
    
      // set the app type in the sequence 
      appSeq.setAppType(fields.getField("appType").asInteger());
      
      // log the user in as vs-virtualapp ( ! Fix - support long app users )
      user = new UserBean();
      user.forceValidate("vs-virtualapp");
      request.getSession().setAttribute("UserLogin",user);
      
      connect();
      
      // update the last login date in vs_login_ids
      /*@lineinfo:generated-code*//*@lineinfo:225^7*/

//  ************************************************************
//  #sql [Ctx] { update  vs_login_ids
//          set     last_login = sysdate
//          where   login_id = :fields.getData("email")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3240 = fields.getData("email");
   String theSqlTS = "update  vs_login_ids\n        set     last_login = sysdate\n        where   login_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.verisign.VaReturn",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3240);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:230^7*/
      
      // grab app seq num from the login table
      long appSeqNum;
      /*@lineinfo:generated-code*//*@lineinfo:234^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_seq_num
//          
//          from    vs_login_ids
//          where   login_id = :fields.getData("email")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3241 = fields.getData("email");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_seq_num\n         \n        from    vs_login_ids\n        where   login_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.verisign.VaReturn",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3241);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:240^7*/
      fields.setData("appSeqNum",Long.toString(appSeqNum));
      
      // finish configuring the sequence object
      appSeq.setAppSeqNum(fields.getField("appSeqNum").asLong());
      appSeq.setCurScreenId(fields.getField("curScreenId").asInteger());
      
      submitOk = true;
    }
    catch(Exception e)
    {
      System.out.println(getClass().getName() + "::autoSubmit(): " 
        + e.toString());
      logEntry("autoSubmit()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return submitOk;
  }
}/*@lineinfo:generated-code*/