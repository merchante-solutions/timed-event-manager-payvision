/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/storefront/Business.sqlj $

  Description:
  
  Business
  
  Banner Bank online application merchant information page bean.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/25/04 1:53p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.storefront;

import javax.servlet.http.HttpServletRequest;
import com.mes.app.VirtualAppBusinessBase;
import com.mes.constants.mesConstants;

public class Business extends VirtualAppBusinessBase
{
  {
    appType     = mesConstants.APP_TYPE_STOREFRONT;
    curScreenId = 1;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      setData("productType", Integer.toString(mesConstants.POS_INTERNET));
      setData("internetType", Integer.toString(mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK));
      
      // set to default storefront association
      setData("assocNum", "660102");
    }
    catch(Exception e)
    {
      logEntry("createFields()-Storefront\\Business", e.toString());
    }
  }
  
  protected boolean submitAppData()
  {
    boolean submitOk = false;

    try
    {
      submitOk = super.submitAppData();
    
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      Pricing merchPricing = new Pricing(appSeqNum);

      submitOk = merchPricing.submitAppData();
    }
    catch(Exception e)
    {
      logEntry("submitAppData(" + fields.getData("appSeqNum") + ")", e.toString());
    }
    finally
    {
    }
  
    return submitOk;
  }
}/*@lineinfo:generated-code*/