/*@lineinfo:filename=Application*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/BusinessBase.sqlj $

  Description:  
  
  Application
  
  Verisign merchant account application

  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/24/04 12:00p $
  Version            : $Revision: 32 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.vs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.jdom.Document;
import com.mes.app.AppBase;
import com.mes.app.DollarAmountTable;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.CityStateZipField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateStringField;
import com.mes.forms.DisabledCheckboxField;
import com.mes.forms.DiscountField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NameField;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.RadioField;
import com.mes.forms.SmallCurrencyField;
import com.mes.forms.TaxIdField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.ops.InsertCreditQueue;
import com.mes.ops.QueueConstants;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class Application extends AppBase
{
  {
    appType = 44;
    curScreenId = 1;
  }
  
  static Logger log = Logger.getLogger(Application.class);

  // warning ids
  public static final int WARN_TRAN_NUM_NOT_FOUND = 1;

  private int maxChargeRecs = 20;
  private List chargeRecs = new ArrayList();


  /*************************************************************************
  **
  **   Virtual App Support (support for standalone application use)
  **
  **************************************************************************/

  public void doExternalSubmit() throws Exception
  {
    // create new app in databsae
    createNewApp(null);
    
    // set association from promotion code
    setAssociation();

    // submit the app data to the db
    submitAppData();

    // mark app screens as completed in screen sequence
    long appSeqNum = fields.getField("appSeqNum").asLong();
    appSeq.setAppSeqNum(appSeqNum);
    appSeq.getFirstScreen().markAsComplete();
    appSeq.getLastScreen().markAsComplete();

    // only add to credit queue if this is a production server
    if(HttpHelper.isProdServer(null))
    {
      // add to credit queue
      (new InsertCreditQueue()).addApp(appSeqNum,user.getUserId());
    }
    else
    {
      // just mark it as New for Application Status purposes
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:109^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_credit_status = :QueueConstants.CREDIT_NEW
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n          set     merch_credit_status =  :1 \n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.CREDIT_NEW);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:114^9*/
      }
      catch (Exception e)
      {
        logEntry("doExternalSubmit()",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
  }
  
  protected void setAssociation()
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:136^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  vam.association     assoc_num
//          from    vps_association_mapping vam
//          where   (
//                    vam.promotion_code = :fields.getData("promotionCode") or
//                    vam.promotion_code = '!DEFAULT'
//                  )
//          order by vam.promotion_code desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3442 = fields.getData("promotionCode");
  try {
   String theSqlTS = "select  vam.association     assoc_num\n        from    vps_association_mapping vam\n        where   (\n                  vam.promotion_code =  :1  or\n                  vam.promotion_code = '!DEFAULT'\n                )\n        order by vam.promotion_code desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3442);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:145^7*/
      
      rs = it.getResultSet();
      
      if(it.next())
      {
        setData("assocNum", rs.getString("assoc_num"));
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getAssociation()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  /*
  ** public void processXml(Document xmlDoc, long vappSeqNum)
  **
  ** Processes app data contained in an xml document object into field data
  ** and submits it as a new online app.
  */
  public void processXml(Document xmlDoc, long vappSeqNum)
  {
    try
    {
      connect();

      // process xml data into field data
      setFields(xmlDoc,MAP_TYPE_VERISIGN_ISO,Ctx,vappSeqNum,null);
      
      // establish association number from promotion code
      setAssociation();

      // create new app in databsae
      createNewApp(null);

      // submit the app data to the db
      submitAppData();

      // mark app screens as completed in screen sequence
      try
      {
        appSeq.setAppSeqNum(fields.getField("appSeqNum").asLong());
        appSeq.getFirstScreen().markAsComplete();
        appSeq.getLastScreen().markAsComplete();
      }
      catch(Exception e)
      {
        System.out.println(this.getClass().getName() + "::createNewApp(): "
          + e.toString());
        logEntry( "createNewApp()", e.toString());
      }
    }
    finally
    {
      cleanUp();
    }
  }

  
  /*************************************************************************
  **
  **   Drop Down Tables
  **
  **************************************************************************/

  /*
  ** protected class PricingTypeTable extends DropDownTable
  **
  ** Loads available interchange pricing bet types from database.
  */
  protected class PricingTypeTable extends DropDownTable
  {
    private boolean userHasRights(String rightStr)
    {
      if (rightStr.equals("all"))
      {
        return true;
      }

      // not supported in jdk 1.3 or earlier...
      String[] rights = split(rightStr,",");
      for (int i = 0; i < rights.length; ++i)
      {
        if (user.hasRight(Integer.parseInt(rights[i])))
        {
          return true;
        }
      }

      return false;
    }

    public PricingTypeTable() throws java.sql.SQLException
    {
      ResultSetIterator it  = null;
      ResultSet         rs  = null;

      try
      {
        long appSeqNum = fields.getField("appSeqNum").asLong();

        connect();
        /*@lineinfo:generated-code*//*@lineinfo:257^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  bet.combo_id,
//                    bet.label,
//                    nvl(bet.required_rights, 'all') required_rights
//            from    appo_ic_bets bet,
//                    application app
//            where   bet.app_type = :appType
//                    and bet.app_type = app.app_type (+)
//                    and ( app.app_seq_num = :appSeqNum 
//                            or ( app.app_seq_num is null and :appSeqNum = 0 ) )
//                    and trunc( nvl( app.app_created_date , sysdate ) ) 
//                          between bet.valid_from and bet.valid_to
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bet.combo_id,\n                  bet.label,\n                  nvl(bet.required_rights, 'all') required_rights\n          from    appo_ic_bets bet,\n                  application app\n          where   bet.app_type =  :1 \n                  and bet.app_type = app.app_type (+)\n                  and ( app.app_seq_num =  :2  \n                          or ( app.app_seq_num is null and  :3  = 0 ) )\n                  and trunc( nvl( app.app_created_date , sysdate ) ) \n                        between bet.valid_from and bet.valid_to";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setLong(3,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:270^9*/
        rs = it.getResultSet();

        addElement("","select one");
        while(rs.next())
        {
          if (userHasRights(rs.getString("required_rights")))
          {
            addElement(rs);
          }
        }
      }
      catch (java.sql.SQLException se)
      {
        throw se;
      }
      catch (Exception e)
      {
        String loc = this.getClass().getName() + ".PricingTypeTable()";
        logEntry(loc,e.toString());
        log.debug(loc + ": " + e);
      } 
      finally
      {
        try{ rs.close(); } catch(Exception e) { }
        try{ it.close(); } catch(Exception e) { }
        cleanUp();
      }
    }
  }

  protected class BusinessTypeTable extends DropDownTable
  {
    public BusinessTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Sole Proprietorship");
      addElement("2","Corporation");
      addElement("3","Partnership");
      addElement("4","Medical or Legal Corporation");
      addElement("5","Association/Estate/Trust");
      addElement("6","Tax Exempt");
      addElement("7","Government");
      addElement("8","Limited Liability Company");
    }
  }
  
  protected class IndustryTypeTable extends DropDownTable
  {
    public IndustryTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail");
      addElement("2","Restaurant");
      addElement("3","Hotel");
      addElement("4","Motel");
      addElement("5","Internet");
      addElement("6","Services");
      addElement("7","Direct Marketing");
      addElement("8","Other");
    }
  }
  
  protected class GenderTable extends DropDownTable
  {
    public GenderTable()
    {
      // value/name pairs
      addElement("U","Not Given");
      addElement("M","Male");
      addElement("F","Female");
    }
  }
  
  protected class RefundPolicyTable extends DropDownTable
  {
    public RefundPolicyTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Refund in 30 Days or Less");
      addElement("2","No Refunds or Exchanges");
      addElement("3","Exchanges Only");
      addElement("4","Not Applicable");
    }
  }

  private class AccountInfoSourceTable extends DropDownTable
  {
    public AccountInfoSourceTable() throws java.sql.SQLException
    {
      ResultSetIterator it  = null;
      ResultSet         rs  = null;

      try
      {      
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:369^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  source_id,
//                    source_desc
//            from    bank_account_info_source
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  source_id,\n                  source_desc\n          from    bank_account_info_source";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.vs.Application",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:374^9*/
        rs = it.getResultSet();

        addElement("","select one");
        while(rs.next())
        {
          addElement(rs);
        }
      }
      catch (java.sql.SQLException se)
      {
        throw se;
      }
      catch (Exception e)
      {
        String loc = this.getClass().getName() + ".AccountInfoSourceTable()";
        logEntry(loc,e.toString());
        log.debug(loc + ": " + e);
      }
      finally
      {
        try{ rs.close(); } catch(Exception e) { }
        try{ it.close(); } catch(Exception e) { }
        cleanUp();
      }
    }
  }

  protected class BankAccountTypeTable extends DropDownTable
  {
    public BankAccountTypeTable() throws java.sql.SQLException
    {
      ResultSetIterator it  = null;
      ResultSet         rs  = null;

      try
      {      
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:413^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    bankacc_type,
//                      bankacc_description
//            from      bankacctype
//            order by  bankacc_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    bankacc_type,\n                    bankacc_description\n          from      bankacctype\n          order by  bankacc_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.vs.Application",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:419^9*/
        rs = it.getResultSet();
        
        addElement("","select one");
        while(rs.next() )
        {
          addElement(rs);
        }
      }
      catch (java.sql.SQLException se)
      {
        throw se;
      }
      catch (Exception e)
      {
        String loc = this.getClass().getName() + ".BankAccountTypeTable()";
        logEntry(loc,e.toString());
        log.debug(loc + ": " + e);
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  
  public class CountryTable extends DropDownTable
  {
    public CountryTable() throws java.sql.SQLException
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
    
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:458^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  country_code,
//                    country_desc
//            from    country
//            order   by country_desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  country_code,\n                  country_desc\n          from    country\n          order   by country_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.vs.Application",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:464^9*/
        rs = it.getResultSet();
        
        addElement("","select one");
        while (rs.next())
        {
          addElement(rs);
        }
      }
      catch (java.sql.SQLException se)
      {
        throw se;
      }
      catch (Exception e)
      {
        String loc = this.getClass().getName() + ".CountryTable()";
        logEntry(loc,e.toString());
        log.debug(loc + ": " + e);
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  
  
  /*************************************************************************
  **
  **   Radio Button Lists
  **
  **************************************************************************/
  
  protected static final String[][] yesNoRadioList = 
  {
    { "No",  "N" },
    { "Yes", "Y" }
  };
  
  protected static final String[][] crTypeRadioList = 
  {
    { "0",  ""                        },  // flags charge slot not used
    { "1",  "One Time"                },
    { "2",  "Recurring"               },
    { "3",  "Minimum Monthly Discount" }   // xml data load min monthly discount
  };
  
  /*************************************************************************
  **
  **   Validations
  **
  **************************************************************************/
  
  /*
  ** public class TransitRoutingValidation extends SQLJConnectionBase
  **
  ** Validates transit routing numbers.  Makes sure the number is valid by
  ** checking it's length and then trying to look it up in rap_app_bank_aba,
  ** a table containing known trn's.  If not found and a prior warning has
  ** not been set, a warning is set and the field is invalid.  If a prior
  ** warning has been given then the field is considered valid.
  */
  public class TransitRoutingValidation extends SQLJConnectionBase
    implements Validation
  {
    private String errorText = null;
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      // never try to validate blank fields, let required validation handle this
      if (fdata == null || fdata.equals(""))
      {
        return true;
      }
      
      // make sure transit num has 9 digits
      if (countDigits(fdata) != 9)
      {
        errorText = "Transit routing number must be nine digits";
        return false;
      }
      
      boolean isValid = false;

      try
      {
        connect();
        
        // lookup the transit routing number
        int itemCount = 0;
        int fraudCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:562^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(transit_routing_num),
//                    nvl(sum(decode(fraud_flag,'Y',1,0)),0)
//            
//            from    rap_app_bank_aba
//            where   transit_routing_num = :fdata
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(transit_routing_num),\n                  nvl(sum(decode(fraud_flag,'Y',1,0)),0)\n           \n          from    rap_app_bank_aba\n          where   transit_routing_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.vs.Application",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fdata);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   fraudCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:570^9*/
        
        // if the transit routing number has been flagged as fraudulent
        // don't allow it to be submitted
        if (fraudCount > 0)
        {
          errorText = "Transit routing number not allowed";
        }
        // if user hasn't already been warned and transit routing number
        // is not found in our table, then add warning about unknown
        // transit routing num and flag field as invalid
        else if (itemCount == 0 && !hasWarning(WARN_TRAN_NUM_NOT_FOUND))
        {
          errorText = "Transit routing number not found, please confirm that "
                      + "it is correct and resubmit";
          addWarning(WARN_TRAN_NUM_NOT_FOUND);
        }
        // else clear any warning
        else
        {
          removeWarning(WARN_TRAN_NUM_NOT_FOUND);
          isValid = true;
        }
      }
      catch(java.sql.SQLException e)
      {
        logEntry("validate()",e.toString());
        errorText = "Transit routing number validation failed: " + e.toString();
      }
      finally
      {
        cleanUp();
      }
      
      return isValid;
    }
  }

  /*************************************************************************
  **
  **   Custom Fields
  **
  **************************************************************************/
  

  /*************************************************************************
  **
  **   Field Bean
  **
  **************************************************************************/
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add    (new Field            ("promotionCode",     "Promotion Code",50,25,false));
      fields.add    (new Field            ("assocNum",          "Association No.",6,6,true));

      // business info
      FieldGroup gBusInfo = new FieldGroup("gBusInfo");
      
      gBusInfo.add  (new Field            ("businessName",      "Business Name (DBA)",25,30,false));
      gBusInfo.add  (new Field            ("businessAddress1",  "Business Address Line 1",32,30,false));
      gBusInfo.add  (new Field            ("businessAddress2",  "Business Address Line 2",32,30,true));
      gBusInfo.add  (new CityStateZipField("businessCsz",       "Business City, State, Zip",30,false));
      gBusInfo.add  (new DropDownField    ("businessCountry",   new CountryTable(),false));
      gBusInfo.add  (new Field            ("businessLegalName", "Business Legal Name",50,30,false));
      gBusInfo.add  (new TaxIdField       ("taxpayerId",        "Federal Taxpayer ID",true));

      gBusInfo.add  (new Field            ("mailingName",       "Mailing Address Name",50,30,true));
      gBusInfo.add  (new Field            ("mailingAddress1",   "Mailing Address Line 1",32,30,true));
      gBusInfo.add  (new Field            ("mailingAddress2",   "Mailing Address Line 2",32,30,true));
      gBusInfo.add  (new CityStateZipField("mailingCsz",        "Mailing City, State, Zip",30,true));
      
      gBusInfo.add  (new Field            ("webUrl",            "Business Website",75,30,false));
      gBusInfo.add  (new PhoneField       ("businessPhone",     "Business Phone Number",false));
      gBusInfo.add  (new PhoneField       ("businessFax",       "Business Fax Number",true));
      gBusInfo.add  (new PhoneField       ("contactPhone",      "Contact Phone Number",false));
      gBusInfo.add  (new NameField        ("contactName",       "Contact Name",30,false,false));
      gBusInfo.add  (new EmailField       ("contactEmail",      75,35,true) );

      gBusInfo.add  (new DropDownField    ("businessType",      "Type of Business",new BusinessTypeTable(),false));
      gBusInfo.add  (new DropDownField    ("industryType",      "Industry",new IndustryTypeTable(),false));
      gBusInfo.add  (new NumberField      ("locationYears",     "# of Years at Main Location",3,3,false,0));
      gBusInfo.add  (new DateStringField  ("establishedDate",   "Business Established Date",false,true));
      gBusInfo.add  (new TextareaField    ("businessDesc",      "Business Description",60,2,30,false));
      
      gBusInfo.add  (new HiddenField      ("sicCode"));

      gBusInfo.getField("businessAddress1").addValidation(
        new NotAllowedValidation(new String[] 
          { "po box", "p.o. box", "pobox", "p.o.box", "p. o. box", 
            "p o box","post office box", "postoffice box", "pob" }, 
            "Post office boxes not allowed"));
      
      fields.add(gBusInfo);
      
      // merchant history
      FieldGroup gMerchHist = new FieldGroup("gMerchHist");

      gMerchHist.add(new RadioButtonField ("haveProcessed",     yesNoRadioList,-1,false,"Required"));
      gMerchHist.add(new Field            ("previousProcessor", "If Yes, Name of Previous Processor",40,30,true));
      gMerchHist.add(new RadioButtonField ("haveCanceled",      yesNoRadioList,-1,false,"Required"));
      gMerchHist.add(new Field            ("canceledProcessor", "If Yes, Name of Processor",40,30,true));
      gMerchHist.add(new Field            ("canceledReason",    "Reason for Cancellation",40,30,true));
      gMerchHist.add(new DateStringField  ("cancelDate",        "Date of Cancellation",true,true));

      gMerchHist.getField("previousProcessor")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveProcessed"),
          "Please provide the name of the previous processor"));
      gMerchHist.getField("canceledProcessor")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
          "Please provide the name of the cancelling processor"));
      gMerchHist.getField("canceledReason")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
          "Please provide reason account was cancelled"));
      gMerchHist.getField("cancelDate")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
          "Please provide a date of cancellation"));
      
      fields.add(gMerchHist);
      
      // primary owner
      FieldGroup gOwner1 = new FieldGroup("gOwner1");
      
      gOwner1.add   (new NameField        ("owner1Name",        "Name",30,false,false));
      gOwner1.add   (new TaxIdField       ("owner1SSN",         "Soc. Security #",false));
      gOwner1.add   (new NumberField      ("owner1Percent",     "Percent Owned",3,3,false,0));
      gOwner1.add   (new Field            ("owner1Address1",    "Residence Address",32,30,false));
      gOwner1.add   (new CityStateZipField("owner1Csz",         "City, State, Zip",30,false));
      gOwner1.add   (new PhoneField       ("owner1Phone",       "Phone Number",true));
      gOwner1.add   (new DateStringField  ("owner1Since",       "Owner/Officer Since",true,true));
      gOwner1.add   (new Field            ("owner1Title",       "Title",40,15,true));
      gOwner1.add   (new DropDownField    ("owner1Gender",      "Gender",new GenderTable(),true));

      gOwner1.getField("owner1Percent")
        .addValidation(new PercentValidation(
          "Primary owner's percentage of ownership is invalid",true));
      
      fields.add(gOwner1);

      // secondary owner
      FieldGroup gOwner2 = new FieldGroup("gOwner2");
      
      gOwner2.add   (new NameField        ("owner2Name",        "Name",30,true,false));
      gOwner2.add   (new TaxIdField       ("owner2SSN",         "Soc. Security #",true));
      gOwner2.add   (new NumberField      ("owner2Percent",     "Percent Owned",3,3,true,0));
      gOwner2.add   (new Field            ("owner2Address1",    "Residence Address",32,30,true));
      gOwner2.add   (new CityStateZipField("owner2Csz",         "City, State, Zip",30,true));
      gOwner2.add   (new PhoneField       ("owner2Phone",       "Phone Number",true));
      gOwner2.add   (new DateStringField  ("owner2Since",       "Owner/Officer Since",true,true));
      gOwner2.add   (new Field            ("owner2Title",       "Title",40,15,true));

      gOwner2.getField("owner2Percent")
        .addValidation(new PercentValidation(
          "Secondary owner's percentage of ownership is invalid",true));
      
      fields.add(gOwner2);

      // transaction info
      FieldGroup gTranInfo = new FieldGroup("gTranInfo");
      
      gTranInfo.add (new CurrencyField    ("monthlySales",      "Total Estimated Monthly Sales",11,9,false));
      gTranInfo.add (new CurrencyField    ("monthlyVMCSales",   "Total Estimated Monthly Visa/MC Sales",11,9,false));
      gTranInfo.add (new CurrencyField    ("averageTicket",     "Estimated Average Credit Card Ticket",11,9,false));
      gTranInfo.add (new DropDownField    ("refundPolicy",      "Refund Policy",new RefundPolicyTable(),false));
      
      fields.add(gTranInfo);
      
      // bank account info
      FieldGroup gBankAcct = new FieldGroup("gBankAcct");

      gBankAcct.add (new Field            ("checkingAccount",   "Checking Acct. #",17,15,false));
      gBankAcct.add (new Field            ("transitRouting",    "Transit Routing #",9,15,false));
      gBankAcct.add (new DropDownField    ("typeOfAcct",        "Account Type",new BankAccountTypeTable(),false));
      gBankAcct.add (new NumberField      ("yearsOpen",         "Years Open",3,6,true,0));
      gBankAcct.add (new Field            ("bankName",          "Name of Bank",30,30,false));
      gBankAcct.add (new Field            ("bankAddress",       "Bank Address",32,30,false));
      gBankAcct.add (new CityStateZipField("bankCsz",           "City, State, Zip",30,false));
      gBankAcct.add (new DropDownField    ("sourceOfInfo",      "Source of Acct. Info",new AccountInfoSourceTable(),false));

      gBankAcct.getField("transitRouting")
        .addValidation(new TransitRoutingValidation());

      fields.add(gBankAcct);

      // pricing options
      fields.add    (new DropDownField    ("pricingType",       "Pricing Type",new PricingTypeTable(),false));
      fields.add    (new DiscountField    ("discRate",          false));
      fields.add    (new SmallCurrencyField("perItem",          5,6,true));

      // payment options
      FieldGroup gPayOpts = new FieldGroup("gPayOpts");

      gPayOpts.add  (new DisabledCheckboxField("vmcAccepted",   "Visa/MasterCard",true));
      gPayOpts.add  (new DropDownField    ("vmcAuthFee",        "Default Auth Fee",new DollarAmountTable("0.0,0.3,0.35,0.4,0.45,0.55"),true));

      gPayOpts.add  (new CheckboxField    ("amexAccepted",      "American Express",false));
      gPayOpts.add  (new NumberField      ("amexAcctNum",       "Amex Acct #",16,25,true,0));
      gPayOpts.add  (new DropDownField    ("amexAuthFee",       "Amex Auth Fee",new DollarAmountTable("0.0,0.3,0.35,0.4,0.45,0.55"),true));

      gPayOpts.add  (new CheckboxField    ("discAccepted",      "Discover",false));
      gPayOpts.add  (new NumberField      ("discAcctNum",       "Disc Acct #",15,25,true,0));
      gPayOpts.add  (new DropDownField    ("discAuthFee",       "Disc Auth Fee",new DollarAmountTable("0.0,0.3,0.35,0.4,0.45,0.55"),true));

      gPayOpts.add  (new CheckboxField    ("dinrAccepted",      "Diners Club",false));
      gPayOpts.add  (new NumberField      ("dinrAcctNum",       "Diners Acct #",10,25,true,0) );
      gPayOpts.add  (new DropDownField    ("dinrAuthFee",       "Diners Auth Fee",new DollarAmountTable("0.0,0.3,0.35,0.4,0.45,0.55"),true));

      gPayOpts.add  (new CheckboxField    ("jcbAccepted",       "JCB",false));
      gPayOpts.add  (new NumberField      ("jcbAcctNum",        "JCB Acct #",25,25,true,0));
      gPayOpts.add  (new DropDownField    ("jcbAuthFee",        "JCB Auth Fee",new DollarAmountTable("0.0,0.3,0.35,0.4,0.45,0.55"),true));

      gPayOpts.add  (new DropDownField    ("voiceAuthFee",      "Voice Auth Fee",new DollarAmountTable("0.0,0.3"),true));
      
      gPayOpts.getField("discAcctNum").addValidation(
        new IfYesNotBlankValidation(
          gPayOpts.getField("discAccepted"),"Field required"));

      gPayOpts.getField("dinrAcctNum").addValidation(
        new IfYesNotBlankValidation(
          gPayOpts.getField("dinrAccepted"),"Field required"));

      gPayOpts.getField("jcbAcctNum").addValidation(
        new IfYesNotBlankValidation(
          gPayOpts.getField("jcbAccepted"),"Field required"));

      fields.add(gPayOpts);

      // charge records
      for (int recIdx = 1; recIdx <= maxChargeRecs; ++recIdx)
      {
        Field[] recFields = new Field[5];
        recFields[0] = new Field          ("crLabel_" + recIdx, "Charge Text " + recIdx,40,35,true);
        recFields[1] = new RadioField     ("crType_" + recIdx,  "Charge Type " + recIdx,crTypeRadioList);
        recFields[2] = new CurrencyField  ("crAmount_" + recIdx,"Charge Amount " + recIdx,11,9,true);
        recFields[3] = new Field          ("crProductType_" + recIdx,"Product Type " + recIdx, 80,20,true);
        recFields[4] = new ButtonField    ("actionCrDel_" + recIdx,"remove");

        OrCondition orCond = new OrCondition();
        orCond.add(new FieldValueCondition(recFields[1],"1"));
        orCond.add(new FieldValueCondition(recFields[1],"2"));
        Validation val = new ConditionalRequiredValidation(orCond);
        recFields[0].addValidation(val);
        recFields[2].addValidation(val);

        chargeRecs.add(recFields);
        for (int i = 0; i < recFields.length; ++i)
        {
          fields.add(recFields[i]);
        }
      }

      fields.add    (new ButtonField      ("actionCrAdd",       "add"));
      fields.add    (new CurrencyField    ("minMonthlyDisc",    "Minimum Monthly Discount",11,9,true));
      fields.add    (new DropDownField    ("chargebackFee",     "Chargeback Fee",new DollarAmountTable("0,10,12,15,20,25,30"),true));

      // comments
      fields.add    (new TextareaField    ("comments",          4000,2,80,true));

      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      log.debug(this.getClass().getName() + "::createFields(): "
        + e.toString());
      e.printStackTrace();
      logEntry("createFields()",e.toString());
    }
  }

  
  /*
  ** protected void postHandleRequest(HttpServletRequest request)
  **
  ** If a user does not exists in the session one is created.  Unlike other apps,
  ** the verisign virtual app has non-users filling out apps.  This creates a
  ** situation that requires a special virtual app user to be generated for use
  ** in the session.  The user will be either the vs-virtualapp user for those
  ** users or the former long app user type.  T
  */
  protected void postHandleRequest(HttpServletRequest request)
  {
    // see if user exists in session already
    UserBean tmpUser = null;
    
    if(request != null)
    {
      tmpUser = (UserBean)(request.getSession().getAttribute("UserLogin"));
    }
    
    if(tmpUser != null)
    {
      user = tmpUser;
    }
    
    // this will need to have logic added to support long app users...
    if (user == null)
    {
      user = new UserBean();
      user.forceValidate("vps");
      
      if(request != null)
      {
        request.getSession().setAttribute("UserLogin",user);
      }
    }

    // call base beans handler
    super.postHandleRequest(request);

    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }

    // transfer monthly minimum from charge record fields to monthly min field
    for (Iterator i = chargeRecs.iterator(); i.hasNext();)
    {
      Field[] recFields = (Field[])i.next();
      if (recFields[1].getData().equals("3"))
      {
        fields.setData("minMonthlyDisc",recFields[2].getData());
        clearRecFields(recFields);
      }
    }

    // sort charge record data so unused slots are at end of list
    int idx = chargeRecs.size() - 1;
    while (idx > 0)
    {
      Field[] curIdx = (Field[])chargeRecs.get(idx);
      Field[] prevIdx = (Field[])chargeRecs.get(idx - 1);
      boolean curInUse = !curIdx[1].getData().equals("0");
      boolean prevInUse = !prevIdx[1].getData().equals("0");
      if (curInUse && !prevInUse)
      {
        chargeRecs.add(chargeRecs.remove(idx - 1));
      }
      --idx;
    }
  }

  private void clearRecFields(Field[] recFields)
  {
    recFields[0].setData("");
    recFields[1].setData("0");
    recFields[2].setData("");
    recFields[3].setData("");
  }

  protected boolean autoAct()
  {
    String action = autoActionName;
    try
    {
      // add new charge rec, activate first unused slot
      if (action.equals("actionCrAdd"))
      {
        for (Iterator i = chargeRecs.iterator(); i.hasNext();)
        {
          Field[] recFields = (Field[])i.next();
          if (recFields[1].getData().equals("0"))
          {
            recFields[1].setData("1");
            return true;
          }
        }

        log.debug("max charges exceeded, cannot add charge");
        return false;
      }

      // remove charge record from indicated slot
      for (Iterator i = chargeRecs.iterator(); i.hasNext();)
      {
        Field[] recFields = (Field[])i.next();
        if (recFields[4].getName().equals(action))
        {
          clearRecFields(recFields);
          return true;
        }
      }

      log.debug("unhandled action: " + action);
    }
    catch (Exception e)
    {
      log.debug("error handling action " + action + ": " + e);
    }
      
    return false;
  }
 
  /*************************************************************************
  **
  **   Helper Methods
  **
  **************************************************************************/
  
  /*
  ** public List getChargeFields()
  **
  ** Scans the charge record fields and loads each set of fields that has
  ** a selected field type.
  **
  ** RETURNS: list of field arrays.
  */
  public List getChargeFields()
  {
    ArrayList fieldList = new ArrayList();
    for (Iterator i = chargeRecs.iterator(); i.hasNext();)
    {
      Field[] recFields = (Field[])i.next();
      String fieldVal = recFields[1].getData();
      if (!fieldVal.equals("0") && !fieldVal.equals(""))
      {
        fieldList.add(recFields);
      }
    }
    return fieldList;
  }

  public int getMaxChargeRecs()
  {
    return maxChargeRecs;
  }

  /*************************************************************************
  **
  **   Submit
  **
  **************************************************************************/
  
  public static final int     FIDX_ADDR_NAME              = 0;
  public static final int     FIDX_ADDR_LINE1             = 1;
  public static final int     FIDX_ADDR_LINE2             = 2;
  public static final int     FIDX_ADDR_CITY              = 3;
  public static final int     FIDX_ADDR_STATE             = 4;
  public static final int     FIDX_ADDR_ZIP               = 5;
  public static final int     FIDX_ADDR_COUNTRY           = 6;
  public static final int     FIDX_ADDR_PHONE             = 7;
  public static final int     FIDX_ADDR_FAX               = 8;
  
  /*
  ** protected void submitAddressData(int addressType, Field[] addrFields)
  **   throws Exception
  **
  ** Given an address type and an array of fields containing an address
  ** this method will store an address in the address table.
  */
  protected void submitAddressData(int addressType, Field[] addrFields)
    throws Exception
  {
    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      // don't insert if city or address line 1 is missing
      Field addr1 = addrFields[FIDX_ADDR_LINE1];
      Field city  = addrFields[FIDX_ADDR_CITY];
      if (addr1 != null && !addr1.isBlank() && city != null && !city.isBlank())
      {
        /*@lineinfo:generated-code*//*@lineinfo:1037^9*/

//  ************************************************************
//  #sql [Ctx] { delete from address
//            where app_seq_num = :appSeqNum
//                  and addresstype_code = :addressType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from address\n          where app_seq_num =  :1 \n                and addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,addressType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1042^9*/
      
        /*@lineinfo:generated-code*//*@lineinfo:1044^9*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//            ( address_name,
//              address_line1,
//              address_line2,
//              address_city,
//              countrystate_code,
//              address_zip,
//              country_code,
//              address_phone,
//              address_fax,
//              app_seq_num,
//              addresstype_code )
//            values
//            ( substr(:(addrFields[FIDX_ADDR_NAME]   == null) ? null : addrFields[FIDX_ADDR_NAME].getData(), 1, 32),
//              substr(:(addrFields[FIDX_ADDR_LINE1]  == null) ? null : addrFields[FIDX_ADDR_LINE1].getData(), 1, 32),
//              substr(:(addrFields[FIDX_ADDR_LINE2]  == null) ? null : addrFields[FIDX_ADDR_LINE2].getData(), 1, 32),
//              substr(:(addrFields[FIDX_ADDR_CITY]   == null) ? null : addrFields[FIDX_ADDR_CITY].getData(), 1, 25),
//              substr(:(addrFields[FIDX_ADDR_STATE]  == null) ? null : addrFields[FIDX_ADDR_STATE].getData(), 1, 2),
//              substr(:(addrFields[FIDX_ADDR_ZIP]    == null) ? null : addrFields[FIDX_ADDR_ZIP].getData(), 1, 10),
//              substr(:(addrFields[FIDX_ADDR_COUNTRY]== null) ? null : addrFields[FIDX_ADDR_COUNTRY].getData(), 1, 2),
//              substr(:(addrFields[FIDX_ADDR_PHONE]  == null) ? null : addrFields[FIDX_ADDR_PHONE].getData(), 1, 10),
//              substr(:(addrFields[FIDX_ADDR_FAX]    == null) ? null : addrFields[FIDX_ADDR_FAX].getData(), 1, 10),
//              :appSeqNum,
//              :addressType )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3443 = (addrFields[FIDX_ADDR_NAME]   == null) ? null : addrFields[FIDX_ADDR_NAME].getData();
 String __sJT_3444 = (addrFields[FIDX_ADDR_LINE1]  == null) ? null : addrFields[FIDX_ADDR_LINE1].getData();
 String __sJT_3445 = (addrFields[FIDX_ADDR_LINE2]  == null) ? null : addrFields[FIDX_ADDR_LINE2].getData();
 String __sJT_3446 = (addrFields[FIDX_ADDR_CITY]   == null) ? null : addrFields[FIDX_ADDR_CITY].getData();
 String __sJT_3447 = (addrFields[FIDX_ADDR_STATE]  == null) ? null : addrFields[FIDX_ADDR_STATE].getData();
 String __sJT_3448 = (addrFields[FIDX_ADDR_ZIP]    == null) ? null : addrFields[FIDX_ADDR_ZIP].getData();
 String __sJT_3449 = (addrFields[FIDX_ADDR_COUNTRY]== null) ? null : addrFields[FIDX_ADDR_COUNTRY].getData();
 String __sJT_3450 = (addrFields[FIDX_ADDR_PHONE]  == null) ? null : addrFields[FIDX_ADDR_PHONE].getData();
 String __sJT_3451 = (addrFields[FIDX_ADDR_FAX]    == null) ? null : addrFields[FIDX_ADDR_FAX].getData();
   String theSqlTS = "insert into address\n          ( address_name,\n            address_line1,\n            address_line2,\n            address_city,\n            countrystate_code,\n            address_zip,\n            country_code,\n            address_phone,\n            address_fax,\n            app_seq_num,\n            addresstype_code )\n          values\n          ( substr( :1 , 1, 32),\n            substr( :2 , 1, 32),\n            substr( :3 , 1, 32),\n            substr( :4 , 1, 25),\n            substr( :5 , 1, 2),\n            substr( :6 , 1, 10),\n            substr( :7 , 1, 2),\n            substr( :8 , 1, 10),\n            substr( :9 , 1, 10),\n             :10 ,\n             :11  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3443);
   __sJT_st.setString(2,__sJT_3444);
   __sJT_st.setString(3,__sJT_3445);
   __sJT_st.setString(4,__sJT_3446);
   __sJT_st.setString(5,__sJT_3447);
   __sJT_st.setString(6,__sJT_3448);
   __sJT_st.setString(7,__sJT_3449);
   __sJT_st.setString(8,__sJT_3450);
   __sJT_st.setString(9,__sJT_3451);
   __sJT_st.setLong(10,appSeqNum);
   __sJT_st.setInt(11,addressType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1070^9*/
      }
    }
    catch(Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitAddressData()";
      log.debug(methodSig + ": " + e.toString());
      logEntry("submitAddressData (" + addressType + "): ", e.toString());
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  /*
  ** protected void submitMerchant(long appSeqNum) throws Exception
  **
  ** Stores most of the data that needs to go in the merchant table.
  */
  protected void submitMerchant(long appSeqNum) throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1091^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_business_name         = :fields.getData("businessName"),
//                  merch_federal_tax_id        = :fields.getData("taxpayerId"),
//                  merch_email_address         = :fields.getData("businessEmail"),
//                  merch_legal_name            = :fields.getData("businessLegalName"),
//                  merch_mailing_name          = :fields.getData("mailingName"),
//                  merch_business_establ_month = :fields.getData("establishedDateMonth"),
//                  merch_business_establ_year  = :fields.getData("establishedDateYear"),
//                  bustype_code                = :fields.getData("businessType"),
//                  industype_code              = :fields.getData("industryType"),
//                  merch_busgoodserv_descr     = :fields.getData("businessDesc"),
//                  merch_num_of_locations      = :fields.getData("numLocations"),
//                  merch_years_at_loc          = :fields.getData("locationYears"),
//                  merch_prior_cc_accp_flag    = :fields.getData("haveProcessed"),
//                  merch_prior_processor       = :fields.getData("previousProcessor"),
//                  merch_cc_acct_term_flag     = :fields.getData("haveCanceled"),
//                  merch_cc_term_name          = :fields.getData("canceledProcessor"),
//                  merch_term_reason           = :fields.getData("canceledReason"),
//                  merch_term_month            = :fields.getData("cancelDateMonth"),
//                  merch_term_year             = :fields.getData("cancelDateYear"),
//                  merch_month_tot_proj_sales  = :fields.getData("monthlySales"),
//                  merch_month_visa_mc_sales   = :fields.getData("monthlyVMCSales"),
//                  merch_average_cc_tran       = :fields.getData("averageTicket"),
//                  refundtype_code             = :fields.getData("refundPolicy"),
//                  merch_gender                = :fields.getData("owner1Gender"),
//                  asso_number                 = :fields.getData("assocNum"),
//                  app_sic_code                = :fields.getData("sicCode"),
//                  merch_web_url               = :fields.getData("webUrl"),
//                  merch_notes                 = :fields.getData("comments"),
//                  promotion_code              = :fields.getData("promotionCode")
//          where   app_seq_num                 = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3452 = fields.getData("businessName");
 String __sJT_3453 = fields.getData("taxpayerId");
 String __sJT_3454 = fields.getData("businessEmail");
 String __sJT_3455 = fields.getData("businessLegalName");
 String __sJT_3456 = fields.getData("mailingName");
 String __sJT_3457 = fields.getData("establishedDateMonth");
 String __sJT_3458 = fields.getData("establishedDateYear");
 String __sJT_3459 = fields.getData("businessType");
 String __sJT_3460 = fields.getData("industryType");
 String __sJT_3461 = fields.getData("businessDesc");
 String __sJT_3462 = fields.getData("numLocations");
 String __sJT_3463 = fields.getData("locationYears");
 String __sJT_3464 = fields.getData("haveProcessed");
 String __sJT_3465 = fields.getData("previousProcessor");
 String __sJT_3466 = fields.getData("haveCanceled");
 String __sJT_3467 = fields.getData("canceledProcessor");
 String __sJT_3468 = fields.getData("canceledReason");
 String __sJT_3469 = fields.getData("cancelDateMonth");
 String __sJT_3470 = fields.getData("cancelDateYear");
 String __sJT_3471 = fields.getData("monthlySales");
 String __sJT_3472 = fields.getData("monthlyVMCSales");
 String __sJT_3473 = fields.getData("averageTicket");
 String __sJT_3474 = fields.getData("refundPolicy");
 String __sJT_3475 = fields.getData("owner1Gender");
 String __sJT_3476 = fields.getData("assocNum");
 String __sJT_3477 = fields.getData("sicCode");
 String __sJT_3478 = fields.getData("webUrl");
 String __sJT_3479 = fields.getData("comments");
 String __sJT_3480 = fields.getData("promotionCode");
   String theSqlTS = "update  merchant\n        set     merch_business_name         =  :1 ,\n                merch_federal_tax_id        =  :2 ,\n                merch_email_address         =  :3 ,\n                merch_legal_name            =  :4 ,\n                merch_mailing_name          =  :5 ,\n                merch_business_establ_month =  :6 ,\n                merch_business_establ_year  =  :7 ,\n                bustype_code                =  :8 ,\n                industype_code              =  :9 ,\n                merch_busgoodserv_descr     =  :10 ,\n                merch_num_of_locations      =  :11 ,\n                merch_years_at_loc          =  :12 ,\n                merch_prior_cc_accp_flag    =  :13 ,\n                merch_prior_processor       =  :14 ,\n                merch_cc_acct_term_flag     =  :15 ,\n                merch_cc_term_name          =  :16 ,\n                merch_term_reason           =  :17 ,\n                merch_term_month            =  :18 ,\n                merch_term_year             =  :19 ,\n                merch_month_tot_proj_sales  =  :20 ,\n                merch_month_visa_mc_sales   =  :21 ,\n                merch_average_cc_tran       =  :22 ,\n                refundtype_code             =  :23 ,\n                merch_gender                =  :24 ,\n                asso_number                 =  :25 ,\n                app_sic_code                =  :26 ,\n                merch_web_url               =  :27 ,\n                merch_notes                 =  :28 ,\n                promotion_code              =  :29 \n        where   app_seq_num                 =  :30";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3452);
   __sJT_st.setString(2,__sJT_3453);
   __sJT_st.setString(3,__sJT_3454);
   __sJT_st.setString(4,__sJT_3455);
   __sJT_st.setString(5,__sJT_3456);
   __sJT_st.setString(6,__sJT_3457);
   __sJT_st.setString(7,__sJT_3458);
   __sJT_st.setString(8,__sJT_3459);
   __sJT_st.setString(9,__sJT_3460);
   __sJT_st.setString(10,__sJT_3461);
   __sJT_st.setString(11,__sJT_3462);
   __sJT_st.setString(12,__sJT_3463);
   __sJT_st.setString(13,__sJT_3464);
   __sJT_st.setString(14,__sJT_3465);
   __sJT_st.setString(15,__sJT_3466);
   __sJT_st.setString(16,__sJT_3467);
   __sJT_st.setString(17,__sJT_3468);
   __sJT_st.setString(18,__sJT_3469);
   __sJT_st.setString(19,__sJT_3470);
   __sJT_st.setString(20,__sJT_3471);
   __sJT_st.setString(21,__sJT_3472);
   __sJT_st.setString(22,__sJT_3473);
   __sJT_st.setString(23,__sJT_3474);
   __sJT_st.setString(24,__sJT_3475);
   __sJT_st.setString(25,__sJT_3476);
   __sJT_st.setString(26,__sJT_3477);
   __sJT_st.setString(27,__sJT_3478);
   __sJT_st.setString(28,__sJT_3479);
   __sJT_st.setString(29,__sJT_3480);
   __sJT_st.setLong(30,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1124^7*/
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitMerchant()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  /*
  ** protected void submitBank(long appSeqNum) throws Exception
  **
  ** Stores the merchant's business checking account data in merchbank.
  */
  protected void submitBank(long appSeqNum) throws Exception
  {
    try
    {
      // clear existing bank record
      /*@lineinfo:generated-code*//*@lineinfo:1143^7*/

//  ************************************************************
//  #sql [Ctx] { delete from merchbank
//          where app_seq_num = :appSeqNum and
//                merchbank_acct_srnum = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from merchbank\n        where app_seq_num =  :1  and\n              merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1148^7*/

      // store bank data
      /*@lineinfo:generated-code*//*@lineinfo:1151^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchbank
//          ( app_seq_num,
//            bankacc_type,
//            merchbank_info_source,
//            merchbank_name,
//            merchbank_acct_num,
//            merchbank_transit_route_num,
//            merchbank_num_years_open,
//            billing_method,
//            merchbank_acct_srnum
//            )
//          values
//          ( :appSeqNum,
//            :fields.getData("typeOfAcct"),
//            :fields.getData("sourceOfInfo"),
//            :fields.getData("bankName"),
//            :fields.getData("checkingAccount"),
//            :fields.getData("transitRouting"),
//            :fields.getData("yearsOpen"),
//            :fields.getData("billingMethod"),
//            1 )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3481 = fields.getData("typeOfAcct");
 String __sJT_3482 = fields.getData("sourceOfInfo");
 String __sJT_3483 = fields.getData("bankName");
 String __sJT_3484 = fields.getData("checkingAccount");
 String __sJT_3485 = fields.getData("transitRouting");
 String __sJT_3486 = fields.getData("yearsOpen");
 String __sJT_3487 = fields.getData("billingMethod");
   String theSqlTS = "insert into merchbank\n        ( app_seq_num,\n          bankacc_type,\n          merchbank_info_source,\n          merchbank_name,\n          merchbank_acct_num,\n          merchbank_transit_route_num,\n          merchbank_num_years_open,\n          billing_method,\n          merchbank_acct_srnum\n          )\n        values\n        (  :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n          1 )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_3481);
   __sJT_st.setString(3,__sJT_3482);
   __sJT_st.setString(4,__sJT_3483);
   __sJT_st.setString(5,__sJT_3484);
   __sJT_st.setString(6,__sJT_3485);
   __sJT_st.setString(7,__sJT_3486);
   __sJT_st.setString(8,__sJT_3487);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1174^7*/

      // merchant's bank address
      submitAddressData(mesConstants.ADDR_TYPE_CHK_ACCT_BANK,
        new Field[] { fields.getField("bankName"),
                      fields.getField("bankAddress"),
                      null,
                      fields.getField("bankCszCity"),
                      fields.getField("bankCszState"),
                      fields.getField("bankCszZip"),
                      null,
                      fields.getField("bankPhone"),
                      null } );
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitBank()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  /*
  ** protected void submitAddresses(long appSeqNum) throws Exception
  **
  ** Submits addresses associated with the merchant's business.
  */
  protected void submitAddresses(long appSeqNum) throws Exception
  {
    try
    {
      // business address
      submitAddressData(mesConstants.ADDR_TYPE_BUSINESS,
        new Field[] { fields.getField("businessName"),
                      fields.getField("businessAddress1"),
                      fields.getField("businessAddress2"),
                      fields.getField("businessCszCity"),
                      fields.getField("businessCszState"),
                      fields.getField("businessCszZip"),
                      fields.getField("businessCountry"),
                      fields.getField("businessPhone"),
                      fields.getField("businessFax") } );

      // mailing address
      submitAddressData(mesConstants.ADDR_TYPE_MAILING,
        new Field[] { fields.getField("mailingName"),
                      fields.getField("mailingAddress1"),
                      fields.getField("mailingAddress2"),
                      fields.getField("mailingCszCity"),
                      fields.getField("mailingCszState"),
                      fields.getField("mailingCszZip"),
                      null,
                      null,
                      null } );
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitAddresses()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }
  
  // business owner field array indexes
  public static final int FIDX_OWNER_LAST_NAME  = 0;
  public static final int FIDX_OWNER_FIRST_NAME = 1;
  public static final int FIDX_OWNER_SSN        = 2;
  public static final int FIDX_OWNER_PERCENT    = 3;
  public static final int FIDX_OWNER_MONTH      = 4;
  public static final int FIDX_OWNER_YEAR       = 5;
  public static final int FIDX_OWNER_TITLE      = 6;
  
  /*
  ** protected void submitBusinessOwnerRecord(long appSeqNum, int ownerId, 
  **   Field[] fields) throws Exception
  **
  ** Submits a record to the businessowner table.
  */
  protected void submitOwnerRecord(long appSeqNum, int ownerId, 
    Field[] ownerFields) throws Exception
  {
    try
    {
      // clear existing business record
      /*@lineinfo:generated-code*//*@lineinfo:1256^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    businessowner
//          where   app_seq_num   = :appSeqNum and
//                  busowner_num  = :ownerId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    businessowner\n        where   app_seq_num   =  :1  and\n                busowner_num  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,ownerId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1262^7*/
      
      // only store new business record if certain fields are not blank
      if (ownerFields[FIDX_OWNER_SSN] != null 
          && !ownerFields[FIDX_OWNER_SSN].isBlank()) 
      {
        // extract the owner data
        String lastName = ownerFields[FIDX_OWNER_LAST_NAME] == null 
          ? null : ownerFields[FIDX_OWNER_LAST_NAME].getData();
        String firstName = ownerFields[FIDX_OWNER_FIRST_NAME] == null 
          ? null : ownerFields[FIDX_OWNER_FIRST_NAME].getData();
        String ssn = ownerFields[FIDX_OWNER_SSN] == null 
          ? null : ownerFields[FIDX_OWNER_SSN].getData();
        String percent = ownerFields[FIDX_OWNER_PERCENT] == null 
          ? null : ownerFields[FIDX_OWNER_PERCENT].getData();
        String month = ownerFields[FIDX_OWNER_MONTH] == null 
          ? null : ownerFields[FIDX_OWNER_MONTH].getData();
        String year = ownerFields[FIDX_OWNER_YEAR] == null 
          ? null : ownerFields[FIDX_OWNER_YEAR].getData();
        String title = ownerFields[FIDX_OWNER_TITLE] == null 
          ? null : ownerFields[FIDX_OWNER_TITLE].getData();
          
        // insert the record
        /*@lineinfo:generated-code*//*@lineinfo:1285^9*/

//  ************************************************************
//  #sql [Ctx] { insert into businessowner
//            ( app_seq_num,
//              busowner_num,
//              busowner_last_name,
//              busowner_first_name,
//              busowner_ssn,
//              busowner_owner_perc,
//              busowner_period_month,
//              busowner_period_year,
//              busowner_title )
//            values
//            ( :appSeqNum,
//              :ownerId,
//              :lastName,
//              :firstName,
//              :ssn,
//              :percent,
//              :month,
//              :year,
//              :title )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into businessowner\n          ( app_seq_num,\n            busowner_num,\n            busowner_last_name,\n            busowner_first_name,\n            busowner_ssn,\n            busowner_owner_perc,\n            busowner_period_month,\n            busowner_period_year,\n            busowner_title )\n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,ownerId);
   __sJT_st.setString(3,lastName);
   __sJT_st.setString(4,firstName);
   __sJT_st.setString(5,ssn);
   __sJT_st.setString(6,percent);
   __sJT_st.setString(7,month);
   __sJT_st.setString(8,year);
   __sJT_st.setString(9,title);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1307^9*/
      }
    }
    catch(Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitOwnerRecord()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  /*
  ** protected void submitOwners(long appSeqNum) throws Exception
  **
  ** Submits primary and secondary owner information, including addresses and
  ** general data about the owner individuals.
  */
  protected void submitOwners(long appSeqNum) throws Exception
  {
    try
    {
      // primary owner address
      submitAddressData(mesConstants.ADDR_TYPE_OWNER1,
        new Field[] { fields.getField("owner1Name"),
                      fields.getField("owner1Address1"),
                      null,
                      fields.getField("owner1CszCity"),
                      fields.getField("owner1CszState"),
                      fields.getField("owner1CszZip"),
                      null,
                      fields.getField("owner1Phone"),
                      null } );

      // secondary owner address
      submitAddressData(mesConstants.ADDR_TYPE_OWNER2,
        new Field[] { fields.getField("owner2Name"),
                      fields.getField("owner2Address1"),
                      null,
                      fields.getField("owner2CszCity"),
                      fields.getField("owner2CszState"),
                      fields.getField("owner2CszZip"),
                      null,
                      fields.getField("owner2Phone"),
                      null } );

      // primary owner data
      submitOwnerRecord(appSeqNum,1,
        new Field[] { fields.getField("owner1NameLast"),
                      fields.getField("owner1NameFirst"),
                      fields.getField("owner1SSN"),
                      fields.getField("owner1Percent"),
                      fields.getField("owner1SinceMonth"),
                      fields.getField("owner1SinceYear"),
                      fields.getField("owner1Title") } );

      // secondary owner data
      submitOwnerRecord(appSeqNum,2,
        new Field[] { fields.getField("owner2NameLast"),
                      fields.getField("owner2NameFirst"),
                      fields.getField("owner2SSN"),
                      fields.getField("owner2Percent"),
                      fields.getField("owner2SinceMonth"),
                      fields.getField("owner2SinceYear"),
                      fields.getField("owner2Title") } );
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitOwners()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }
  
  /*
  ** protected void submitContact(long appSeqNum) throws Exception
  **
  ** Stores merchant contact data in merchcontact.
  */
  protected void submitContact(long appSeqNum) throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1387^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merchcontact
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merchcontact\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1392^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1394^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchcontact
//          ( app_seq_num,
//            merchcont_prim_first_name,
//            merchcont_prim_last_name,
//            merchcont_prim_phone,
//            merchcont_prim_phone_ext,
//            merchcont_prim_email )
//          values
//          ( :appSeqNum,
//            :fields.getData("contactNameFirst"),
//            :fields.getData("contactNameLast"),
//            :fields.getData("contactPhone"),
//            :fields.getData("contactPhoneExt"),
//            :fields.getData("contactEmail") )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3488 = fields.getData("contactNameFirst");
 String __sJT_3489 = fields.getData("contactNameLast");
 String __sJT_3490 = fields.getData("contactPhone");
 String __sJT_3491 = fields.getData("contactPhoneExt");
 String __sJT_3492 = fields.getData("contactEmail");
   String theSqlTS = "insert into merchcontact\n        ( app_seq_num,\n          merchcont_prim_first_name,\n          merchcont_prim_last_name,\n          merchcont_prim_phone,\n          merchcont_prim_phone_ext,\n          merchcont_prim_email )\n        values\n        (  :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_3488);
   __sJT_st.setString(3,__sJT_3489);
   __sJT_st.setString(4,__sJT_3490);
   __sJT_st.setString(5,__sJT_3491);
   __sJT_st.setString(6,__sJT_3492);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1410^7*/
    }
    catch(Exception e)
    {
      String methodSig = this.getClass().getName() + "::submitContact()";
      throw new Exception(methodSig + ": " + e.toString());
    }
  }

  // card option types
  public static final int CT_VISA     = 0;
  public static final int CT_MC       = 1;
  public static final int CT_DEBIT    = 2;
  public static final int CT_DINERS   = 3;
  public static final int CT_DISCOVER = 4;
  public static final int CT_AMEX     = 5;
  public static final int CT_JCB      = 6;
  public static final int CT_CHECK    = 7;
  public static final int CT_EBT      = 8;
  public static final int CT_DIAL_PAY = 9;
  public static final int CT_INTERNET = 10;
  public static final int CT_VALUTEC  = 11;
  
  public static final int CT_COUNT    = 12;
  
  /*
  ** protected void submitCards(long appSeqNum) throws Exception
  **
  ** Stores which cards a merchant wishes to accept, and various options
  ** and data associated with each card type into the merchpayoption table.
  */
  protected void submitCards(long appSeqNum) throws Exception
  {
    try          
    {
      int cardCount = 0;
      for (int cardIdx = 0; cardIdx < CT_COUNT; ++cardIdx)
      {
        boolean cardAccepted  = false;
        int     cardType      = -1;
        String  cardAcctNum   = null;

        switch (cardIdx)
        {
          case CT_VISA:
            cardType      = mesConstants.APP_CT_VISA;
            cardAccepted  = true;
            break;
            
          case CT_MC:
            cardType      = mesConstants.APP_CT_MC;
            cardAccepted  = true;
            break;
          
          case CT_DINERS:
            cardType      = mesConstants.APP_CT_DINERS_CLUB;
            cardAccepted  = fields.getData("dinrAccepted").equals("y");
            cardAcctNum   = fields.getData("dinrAcctNum");
            break;
          
          case CT_DISCOVER:
            cardType      = mesConstants.APP_CT_DISCOVER;
            cardAccepted  = fields.getData("discAccepted").equals("y");
            cardAcctNum   = fields.getData("discAcctNum");
            break;
          
          case CT_AMEX:
            cardType      = mesConstants.APP_CT_AMEX;
            cardAccepted  = fields.getData("amexAccepted").equals("y");
            cardAcctNum   = fields.getData("amexAcctNum");
            break;
          
          case CT_JCB:
            cardType      = mesConstants.APP_CT_JCB;
            cardAccepted  = fields.getData("jcbAccepted").equals("y");
            cardAcctNum   = fields.getData("jcbAcctNum");
            break;
          
          default:
            continue;
        }
        
        // clear any old record for this card type from merchpayoption
        /*@lineinfo:generated-code*//*@lineinfo:1493^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    merchpayoption
//            where   app_seq_num   = :appSeqNum and
//                    cardtype_code = :cardType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    merchpayoption\n          where   app_seq_num   =  :1  and\n                  cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1499^9*/

        // insert record for card type if it is accepted
        if (cardAccepted)
        {
          ++cardCount;
          /*@lineinfo:generated-code*//*@lineinfo:1505^11*/

//  ************************************************************
//  #sql [Ctx] { insert into  merchpayoption
//              ( app_seq_num,
//                cardtype_code,
//                card_sr_number,
//                merchpo_card_merch_number
//              )
//              values
//              ( :appSeqNum,
//                :cardType,
//                :cardCount,
//                :cardAcctNum
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into  merchpayoption\n            ( app_seq_num,\n              cardtype_code,\n              card_sr_number,\n              merchpo_card_merch_number\n            )\n            values\n            (  :1 ,\n               :2 ,\n               :3 ,\n               :4 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
   __sJT_st.setInt(3,cardCount);
   __sJT_st.setString(4,cardAcctNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1519^11*/
        }
        // not accepted, clear related card info 
        // from tranchrg table for this card type
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:1525^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    tranchrg
//              where   app_seq_num   = :appSeqNum and
//                      cardtype_code = :cardType
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    tranchrg\n            where   app_seq_num   =  :1  and\n                    cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1531^11*/
        }
      }
    }
    catch (Exception e)
    {
      throw new Exception("submitCards(): " + e.toString());
    }
  }

  /*
  ** protected void submitPricing(long appSeqNum)
  **
  ** Stores pricing info, including non-V/MC auth fees.
  */
  protected void submitPricing(long appSeqNum)
  {
    try
    {
      // clear any existing pricing from tranchrg
      /*@lineinfo:generated-code*//*@lineinfo:1551^7*/

//  ************************************************************
//  #sql [Ctx] { delete from tranchrg where app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from tranchrg where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1554^7*/

      // v/mc record data
      String vmcPricingType = fields.getData("pricingType");
      String vmcDiscRate    = fields.getData("discRate");
      String vmcPerItem     = fields.getData("perItem");
      String vmcAuthFee     = fields.getData("vmcAuthFee");
      String minMonthlyDisc = fields.getData("minMonthlyDisc");
      String voiceAuthFee   = fields.getData("voiceAuthFee");

      // non v/mc record data
      String amexAuthFee    = fields.getData("amexAuthFee");
      String discAuthFee    = fields.getData("discAuthFee");
      String dinrAuthFee    = fields.getData("dinrAuthFee");
      String jcbAuthFee     = fields.getData("jcbAuthFee");

      log.debug("amexAuthFee: " + amexAuthFee);
      log.debug("discAuthFee: " + discAuthFee);
      log.debug("dinrAuthFee: " + dinrAuthFee);
      log.debug("jcbAuthFee: "  + jcbAuthFee);

      // set visa
      /*@lineinfo:generated-code*//*@lineinfo:1576^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          ( app_seq_num,
//            cardtype_code,
//            tranchrg_interchangefee_type,
//            tranchrg_interchangefee_fee,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_per_auth,
//            tranchrg_mmin_chrg,
//            tranchrg_voice_fee )
//          values
//          ( :appSeqNum,
//            :mesConstants.APP_CT_VISA,
//            44,
//            :vmcPricingType,
//            :vmcDiscRate,
//            :vmcPerItem,
//            :vmcAuthFee,
//            :minMonthlyDisc,
//            :voiceAuthFee )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into tranchrg\n        ( app_seq_num,\n          cardtype_code,\n          tranchrg_interchangefee_type,\n          tranchrg_interchangefee_fee,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_per_auth,\n          tranchrg_mmin_chrg,\n          tranchrg_voice_fee )\n        values\n        (  :1 ,\n           :2 ,\n          44,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   __sJT_st.setString(3,vmcPricingType);
   __sJT_st.setString(4,vmcDiscRate);
   __sJT_st.setString(5,vmcPerItem);
   __sJT_st.setString(6,vmcAuthFee);
   __sJT_st.setString(7,minMonthlyDisc);
   __sJT_st.setString(8,voiceAuthFee);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1598^7*/

      // mirror visa to mc
      /*@lineinfo:generated-code*//*@lineinfo:1601^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          ( app_seq_num,
//            cardtype_code,
//            tranchrg_interchangefee_type,
//            tranchrg_interchangefee_fee,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_per_auth,
//            tranchrg_mmin_chrg,
//            tranchrg_voice_fee )
//          values
//          ( :appSeqNum,
//            :mesConstants.APP_CT_MC,
//            44,
//            :vmcPricingType,
//            :vmcDiscRate,
//            :vmcPerItem,
//            :vmcAuthFee,
//            :minMonthlyDisc,
//            :voiceAuthFee )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into tranchrg\n        ( app_seq_num,\n          cardtype_code,\n          tranchrg_interchangefee_type,\n          tranchrg_interchangefee_fee,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_per_auth,\n          tranchrg_mmin_chrg,\n          tranchrg_voice_fee )\n        values\n        (  :1 ,\n           :2 ,\n          44,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_MC);
   __sJT_st.setString(3,vmcPricingType);
   __sJT_st.setString(4,vmcDiscRate);
   __sJT_st.setString(5,vmcPerItem);
   __sJT_st.setString(6,vmcAuthFee);
   __sJT_st.setString(7,minMonthlyDisc);
   __sJT_st.setString(8,voiceAuthFee);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1623^7*/

      // amex
      if (amexAuthFee.length() > 0 && !amexAuthFee.equals("0.0"))
      {
        /*@lineinfo:generated-code*//*@lineinfo:1628^9*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//            ( app_seq_num,
//              cardtype_code,
//              tranchrg_per_tran )
//            values
//            ( :appSeqNum,
//              :mesConstants.APP_CT_AMEX,
//              :amexAuthFee )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into tranchrg\n          ( app_seq_num,\n            cardtype_code,\n            tranchrg_per_tran )\n          values\n          (  :1 ,\n             :2 ,\n             :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_AMEX);
   __sJT_st.setString(3,amexAuthFee);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1638^9*/
      }

      // discover
      if (discAuthFee.length() > 0 && !discAuthFee.equals("0.0"))
      {
        /*@lineinfo:generated-code*//*@lineinfo:1644^9*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//            ( app_seq_num,
//              cardtype_code,
//              tranchrg_per_tran )
//            values
//            ( :appSeqNum,
//              :mesConstants.APP_CT_DISCOVER,
//              :discAuthFee )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into tranchrg\n          ( app_seq_num,\n            cardtype_code,\n            tranchrg_per_tran )\n          values\n          (  :1 ,\n             :2 ,\n             :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_DISCOVER);
   __sJT_st.setString(3,discAuthFee);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1654^9*/
      }

      // diners club
      if (dinrAuthFee.length() > 0 && !dinrAuthFee.equals("0.0"))
      {
        /*@lineinfo:generated-code*//*@lineinfo:1660^9*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//            ( app_seq_num,
//              cardtype_code,
//              tranchrg_per_tran )
//            values
//            ( :appSeqNum,
//              :mesConstants.APP_CT_DINERS_CLUB,
//              :dinrAuthFee )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into tranchrg\n          ( app_seq_num,\n            cardtype_code,\n            tranchrg_per_tran )\n          values\n          (  :1 ,\n             :2 ,\n             :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_DINERS_CLUB);
   __sJT_st.setString(3,dinrAuthFee);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1670^9*/
      }

      // jcb
      if (jcbAuthFee.length() > 0 && !jcbAuthFee.equals("0.0"))
      {
        /*@lineinfo:generated-code*//*@lineinfo:1676^9*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//            ( app_seq_num,
//              cardtype_code,
//              tranchrg_per_tran )
//            values
//            ( :appSeqNum,
//              :mesConstants.APP_CT_JCB,
//              :jcbAuthFee )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into tranchrg\n          ( app_seq_num,\n            cardtype_code,\n            tranchrg_per_tran )\n          values\n          (  :1 ,\n             :2 ,\n             :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_JCB);
   __sJT_st.setString(3,jcbAuthFee);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1686^9*/
      }
    }
    catch (Exception e)
    {
      log.error("Error in submitPricing(): " + e);
      e.printStackTrace();
      throw new RuntimeException(e.toString());
    }
  }

  protected void submitChargeRecords(long appSeqNum)
  {
    try
    {
      // clear miscchrg
      /*@lineinfo:generated-code*//*@lineinfo:1702^7*/

//  ************************************************************
//  #sql [Ctx] { delete from miscchrg where app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from miscchrg where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1705^7*/

      // chargebacks
      String cbFee = fields.getData("chargebackFee");
      if (!cbFee.equals(""))
      {
        /*@lineinfo:generated-code*//*@lineinfo:1711^9*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//            ( app_seq_num,
//              misc_code,
//              misc_chrg_amount )
//            values
//            ( :appSeqNum,
//              :mesConstants.APP_MISC_CHARGE_CHARGEBACK,
//              :cbFee )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into miscchrg\n          ( app_seq_num,\n            misc_code,\n            misc_chrg_amount )\n          values\n          (  :1 ,\n             :2 ,\n             :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_CHARGEBACK);
   __sJT_st.setString(3,cbFee);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1721^9*/
      }

      // clear appo_charge_recs
      /*@lineinfo:generated-code*//*@lineinfo:1725^7*/

//  ************************************************************
//  #sql [Ctx] { delete from appo_charge_recs where app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from appo_charge_recs where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1728^7*/

      // new style charge records
      for (Iterator i = getChargeFields().iterator(); i.hasNext();)
      {
        Field[] recFields = (Field[])i.next();
        String[] fieldStrs = split(recFields[0].getName(),"_");
        int chargeNum = Integer.parseInt(fieldStrs[1]);
        /*@lineinfo:generated-code*//*@lineinfo:1736^9*/

//  ************************************************************
//  #sql [Ctx] { insert into appo_charge_recs
//            ( app_seq_num,
//              charge_rec_num,
//              charge_rec_type,
//              amount,
//              description,
//              product_type )
//            values
//            ( :appSeqNum,
//              :chargeNum,
//              :recFields[1].getData(),
//              :recFields[2].getData(),
//              :recFields[0].getData(),
//              :recFields[3].getData() )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3493 = recFields[1].getData();
 String __sJT_3494 = recFields[2].getData();
 String __sJT_3495 = recFields[0].getData();
 String __sJT_3496 = recFields[3].getData();
   String theSqlTS = "insert into appo_charge_recs\n          ( app_seq_num,\n            charge_rec_num,\n            charge_rec_type,\n            amount,\n            description,\n            product_type )\n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"29com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,chargeNum);
   __sJT_st.setString(3,__sJT_3493);
   __sJT_st.setString(4,__sJT_3494);
   __sJT_st.setString(5,__sJT_3495);
   __sJT_st.setString(6,__sJT_3496);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1752^9*/
      }
    }
    catch (Exception e)
    {
      log.error("Error in submitChargeRecords(): " + e);
      e.printStackTrace();
      throw new RuntimeException(e.toString());
    }
  }

  protected void submitProductType(long appSeqNum)
  {
    try
    {
      // clear merch_pos
      /*@lineinfo:generated-code*//*@lineinfo:1768^7*/

//  ************************************************************
//  #sql [Ctx] { delete from merch_pos where app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from merch_pos where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"30com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1771^7*/

      // hardcode product type of verisign payflow link for now...
      /*@lineinfo:generated-code*//*@lineinfo:1774^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_pos
//          ( app_seq_num,
//            pos_code,
//            pos_param )
//          values
//          ( :appSeqNum,
//            :mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK,
//            :fields.getData("webUrl") )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3497 = fields.getData("webUrl");
   String theSqlTS = "insert into merch_pos\n        ( app_seq_num,\n          pos_code,\n          pos_param )\n        values\n        (  :1 ,\n           :2 ,\n           :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"31com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK);
   __sJT_st.setString(3,__sJT_3497);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1784^7*/
    }
    catch (Exception e)
    {
      log.error("Error in submitProductType(): " + e);
      e.printStackTrace();
      throw new RuntimeException(e.toString());
    }
  }

  /*
  ** protected boolean submitAppData()
  **
  ** Causes all data from the page to be stored in the database.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean submitAppData()
  {
    boolean submitOk = false;
    try
    {
      connect();
      
      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        submitMerchant(appSeqNum);
        submitBank(appSeqNum);
        submitAddresses(appSeqNum);
        submitOwners(appSeqNum);
        submitContact(appSeqNum);
        submitCards(appSeqNum);
        submitPricing(appSeqNum);
        submitChargeRecords(appSeqNum);
        submitProductType(appSeqNum);
      }
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submitData()",e.toString());
      log.debug(this.getClass().getName() + "::submitAppData(): " 
        + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return submitOk;
  }

  /*************************************************************************
  **
  **   Load
  **
  **************************************************************************/

  /*
  ** protected void loadMerchant(long appSeqNum) throws Exception
  **
  ** Loads merchant table data into fields.
  */
  protected void loadMerchant(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    try
    {
      // load data from merchant
      /*@lineinfo:generated-code*//*@lineinfo:1854^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_business_name           business_name,
//                  merch_federal_tax_id          taxpayer_id,
//                  merch_business_establ_month   established_date_month,
//                  merch_business_establ_year    established_date_year,
//                  merch_legal_name              business_legal_name,
//                  merch_mailing_name            mailing_name,
//                  merch_email_address           business_email,
//                  bustype_code                  business_type,
//                  industype_code                industry_type,
//                  merch_busgoodserv_descr       business_desc,
//                  merch_years_at_loc            location_years,
//                  merch_prior_cc_accp_flag      have_processed,
//                  merch_prior_processor         previous_processor,
//                  merch_cc_acct_term_flag       have_canceled,
//                  merch_cc_term_name            canceled_processor,
//                  merch_term_reason             canceled_reason,
//                  merch_term_year               cancel_date_year,
//                  merch_term_month              cancel_date_month,
//                  merch_month_tot_proj_sales    monthly_sales,
//                  merch_month_visa_mc_sales     monthly_vmc_sales,
//                  merch_average_cc_tran         average_ticket,
//                  refundtype_code               refund_policy,
//                  merch_gender                  owner_1_gender,
//                  asso_number                   assoc_num,
//                  app_sic_code                  sic_code,
//                  merch_web_url                 web_url,
//                  merch_notes                   comments,
//                  promotion_code                promotion_code
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_business_name           business_name,\n                merch_federal_tax_id          taxpayer_id,\n                merch_business_establ_month   established_date_month,\n                merch_business_establ_year    established_date_year,\n                merch_legal_name              business_legal_name,\n                merch_mailing_name            mailing_name,\n                merch_email_address           business_email,\n                bustype_code                  business_type,\n                industype_code                industry_type,\n                merch_busgoodserv_descr       business_desc,\n                merch_years_at_loc            location_years,\n                merch_prior_cc_accp_flag      have_processed,\n                merch_prior_processor         previous_processor,\n                merch_cc_acct_term_flag       have_canceled,\n                merch_cc_term_name            canceled_processor,\n                merch_term_reason             canceled_reason,\n                merch_term_year               cancel_date_year,\n                merch_term_month              cancel_date_month,\n                merch_month_tot_proj_sales    monthly_sales,\n                merch_month_visa_mc_sales     monthly_vmc_sales,\n                merch_average_cc_tran         average_ticket,\n                refundtype_code               refund_policy,\n                merch_gender                  owner_1_gender,\n                asso_number                   assoc_num,\n                app_sic_code                  sic_code,\n                merch_web_url                 web_url,\n                merch_notes                   comments,\n                promotion_code                promotion_code\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"32com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1886^7*/
      setFields(it.getResultSet());
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadMerchant()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      it.close();
    }
  }

  /*
  ** protected void loadAddresses(long appSeqNum) throws Exception
  **
  ** Loads merchant addresses into fields (main, mailing).
  */
  protected void loadAddresses(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    try
    {
      // business location address (no PO Boxes)
      /*@lineinfo:generated-code*//*@lineinfo:1911^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1     business_address_1,
//                  address_line2     business_address_2,
//                  address_city      business_csz_city,
//                  countrystate_code business_csz_state,
//                  country_code      business_country,
//                  address_zip       business_csz_zip,
//                  address_phone     business_phone,
//                  address_fax       business_fax
//          from    address
//          where   app_seq_num = :appSeqNum and
//                  addresstype_code = :mesConstants.ADDR_TYPE_BUSINESS
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1     business_address_1,\n                address_line2     business_address_2,\n                address_city      business_csz_city,\n                countrystate_code business_csz_state,\n                country_code      business_country,\n                address_zip       business_csz_zip,\n                address_phone     business_phone,\n                address_fax       business_fax\n        from    address\n        where   app_seq_num =  :1  and\n                addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_BUSINESS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"33com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1924^7*/
      setFields(it.getResultSet());
      it.close();

      // business mailing address
      /*@lineinfo:generated-code*//*@lineinfo:1929^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1     mailing_address_1,
//                  address_line2     mailing_address_2,
//                  address_city      mailing_csz_city,
//                  countrystate_code mailing_csz_state,
//                  address_zip       mailing_csz_zip
//          from    address
//          where   app_seq_num       = :appSeqNum and
//                  addresstype_code  = :mesConstants.ADDR_TYPE_MAILING
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1     mailing_address_1,\n                address_line2     mailing_address_2,\n                address_city      mailing_csz_city,\n                countrystate_code mailing_csz_state,\n                address_zip       mailing_csz_zip\n        from    address\n        where   app_seq_num       =  :1  and\n                addresstype_code  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_MAILING);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"34com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1939^7*/
      setFields(it.getResultSet());
      it.close();
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadAddresses()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      it.close();
    }
  }

  /*
  ** protected void loadOwners(long appSeqNum) throws Exception
  **
  ** Loads business principals (primary, secondary) into fields.
  */
  protected void loadOwners(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    try
    {
      // primary owner address
      /*@lineinfo:generated-code*//*@lineinfo:1965^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1     owner1_address_1,
//                  address_line2     owner1_address_2,
//                  address_city      owner1_csz_city,
//                  countrystate_code owner1_csz_state,
//                  country_code      owner1_country,
//                  address_zip       owner1_csz_zip,
//                  address_phone     owner1_phone
//          from    address
//          where   app_seq_num       = :appSeqNum and
//                  addresstype_code  = :mesConstants.ADDR_TYPE_OWNER1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1     owner1_address_1,\n                address_line2     owner1_address_2,\n                address_city      owner1_csz_city,\n                countrystate_code owner1_csz_state,\n                country_code      owner1_country,\n                address_zip       owner1_csz_zip,\n                address_phone     owner1_phone\n        from    address\n        where   app_seq_num       =  :1  and\n                addresstype_code  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"35com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_OWNER1);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"35com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1977^7*/
      setFields(it.getResultSet());
      it.close();

      // secondary owner address
      /*@lineinfo:generated-code*//*@lineinfo:1982^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1     owner2_address_1,
//                  address_line2     owner2_address_2,
//                  address_city      owner2_csz_city,
//                  countrystate_code owner2_csz_state,
//                  country_code      owner2_country,
//                  address_zip       owner2_csz_zip,
//                  address_phone     owner2_phone
//          from    address
//          where   app_seq_num       = :appSeqNum and
//                  addresstype_code  = :mesConstants.ADDR_TYPE_OWNER2
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1     owner2_address_1,\n                address_line2     owner2_address_2,\n                address_city      owner2_csz_city,\n                countrystate_code owner2_csz_state,\n                country_code      owner2_country,\n                address_zip       owner2_csz_zip,\n                address_phone     owner2_phone\n        from    address\n        where   app_seq_num       =  :1  and\n                addresstype_code  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_OWNER2);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"36com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1994^7*/
      setFields(it.getResultSet());
      it.close();

      // primary business owner
      /*@lineinfo:generated-code*//*@lineinfo:1999^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_last_name      owner1_name_last,
//                  busowner_first_name     owner1_name_first,
//                  busowner_ssn            owner1_ssn,
//                  busowner_owner_perc     owner1_percent,
//                  busowner_period_month   owner1_since_month,
//                  busowner_period_year    owner1_since_year,
//                  busowner_title          owner1_title
//          from    businessowner
//          where   app_seq_num  = :appSeqNum and
//                  busowner_num = :mesConstants.BUS_OWNER_PRIMARY
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_last_name      owner1_name_last,\n                busowner_first_name     owner1_name_first,\n                busowner_ssn            owner1_ssn,\n                busowner_owner_perc     owner1_percent,\n                busowner_period_month   owner1_since_month,\n                busowner_period_year    owner1_since_year,\n                busowner_title          owner1_title\n        from    businessowner\n        where   app_seq_num  =  :1  and\n                busowner_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.BUS_OWNER_PRIMARY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"37com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2011^7*/
      setFields(it.getResultSet());
      it.close();

      // secondary business owner
      /*@lineinfo:generated-code*//*@lineinfo:2016^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_last_name      owner2_name_last,
//                  busowner_first_name     owner2_name_first,
//                  decode(busowner_ssn,
//                         null,' ',
//                         0, ' ',
//                         busowner_ssn)    owner2_ssn,
//                  busowner_owner_perc     owner2_percent,
//                  busowner_period_month   owner2_since_month,
//                  busowner_period_year    owner2_since_year,
//                  busowner_title          owner2_title
//          from    businessowner
//          where   app_seq_num  = :appSeqNum and
//                  busowner_num = :mesConstants.BUS_OWNER_SECONDARY
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_last_name      owner2_name_last,\n                busowner_first_name     owner2_name_first,\n                decode(busowner_ssn,\n                       null,' ',\n                       0, ' ',\n                       busowner_ssn)    owner2_ssn,\n                busowner_owner_perc     owner2_percent,\n                busowner_period_month   owner2_since_month,\n                busowner_period_year    owner2_since_year,\n                busowner_title          owner2_title\n        from    businessowner\n        where   app_seq_num  =  :1  and\n                busowner_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"38com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.BUS_OWNER_SECONDARY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"38com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2031^7*/
      setFields(it.getResultSet());
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadOwners()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      it.close();
    }
  }

  /*
  ** protected void loadContact(long appSeqNum) throws Exception
  **
  ** Loads merchant contact information into fields.
  */
  protected void loadContact(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    try
    {
      // contact data
      /*@lineinfo:generated-code*//*@lineinfo:2056^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchcont_prim_first_name   contact_name_first,
//                  merchcont_prim_last_name    contact_name_last,
//                  merchcont_prim_phone        contact_phone,
//                  merchcont_prim_phone_ext    contact_phone_ext,
//                  merchcont_prim_email        contact_email
//          from    merchcontact
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merchcont_prim_first_name   contact_name_first,\n                merchcont_prim_last_name    contact_name_last,\n                merchcont_prim_phone        contact_phone,\n                merchcont_prim_phone_ext    contact_phone_ext,\n                merchcont_prim_email        contact_email\n        from    merchcontact\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"39com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2065^7*/
      setFields(it.getResultSet());

    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadContact()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      it.close();
    }
  }

  /*
  ** protected void loadBank(long appSeqNum) throws Exception
  **
  ** Loads merchant banking account data into fields.
  */
  protected void loadBank(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    try
    {
      // bank account information
      /*@lineinfo:generated-code*//*@lineinfo:2091^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bankacc_type                  type_of_acct,
//                  merchbank_info_source         source_of_info,
//                  merchbank_name                bank_name,
//                  merchbank_acct_num            checking_account,
//                  merchbank_transit_route_num   transit_routing,
//                  merchbank_num_years_open      years_open
//          from    merchbank
//          where   app_seq_num = :appSeqNum and
//                  merchbank_acct_srnum = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bankacc_type                  type_of_acct,\n                merchbank_info_source         source_of_info,\n                merchbank_name                bank_name,\n                merchbank_acct_num            checking_account,\n                merchbank_transit_route_num   transit_routing,\n                merchbank_num_years_open      years_open\n        from    merchbank\n        where   app_seq_num =  :1  and\n                merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"40com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"40com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2102^7*/
      setFields(it.getResultSet());
      it.close();

      // bank address
      /*@lineinfo:generated-code*//*@lineinfo:2107^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1       bank_address,
//                  address_city        bank_csz_city,
//                  countrystate_code   bank_csz_state,
//                  address_zip         bank_csz_zip,
//                  address_phone       bank_phone
//          from    address
//          where   app_seq_num       = :appSeqNum and
//                  addresstype_code  = :mesConstants.ADDR_TYPE_CHK_ACCT_BANK
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1       bank_address,\n                address_city        bank_csz_city,\n                countrystate_code   bank_csz_state,\n                address_zip         bank_csz_zip,\n                address_phone       bank_phone\n        from    address\n        where   app_seq_num       =  :1  and\n                addresstype_code  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"41com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_CHK_ACCT_BANK);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"41com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2117^7*/
      setFields(it.getResultSet());
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadAddresses()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      it.close();
    }
  }

  /*
  ** protected void loadCards(long appSeqNum) throws Exception
  **
  ** Loads merchant card options.
  */
  protected void loadCards(long appSeqNum) throws Exception
  {
    ResultSetIterator it = null;
    try
    {
      // payment options
      /*@lineinfo:generated-code*//*@lineinfo:2142^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code               card_type,
//                  merchpo_card_merch_number   merchant_number,
//                  merchpo_fee                 per_item
//          from   merchpayoption
//          where  app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code               card_type,\n                merchpo_card_merch_number   merchant_number,\n                merchpo_fee                 per_item\n        from   merchpayoption\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"42com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"42com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2149^7*/
      ResultSet rs = it.getResultSet();
      while (rs.next())
      {
        switch (rs.getInt("card_type"))
        {
          case mesConstants.APP_CT_VISA:      // ignore, always selected
          case mesConstants.APP_CT_MC:
            // set v/mc (all) auth fee here
            break;

          case mesConstants.APP_CT_DINERS_CLUB:
            fields.setData("dinrAccepted",    "y");
            fields.setData("dinrAcctNum",     rs.getString("merchant_number"));
            break;

          case mesConstants.APP_CT_JCB:
            fields.setData("jcbAccepted",       "y");
            fields.setData("jcbAcctNum",        rs.getString("merchant_number"));
            break;

          case mesConstants.APP_CT_AMEX:
            fields.setData("amexAccepted",      "y");
            fields.setData("amexAcctNum",       rs.getString("merchant_number"));
            break;

          case mesConstants.APP_CT_DISCOVER:
            fields.setData("discAccepted",  "y");
            fields.setData("discAcctNum",   rs.getString("merchant_number"));
            break;

          default:
            break;
        }
      }
    }
    catch (Exception e)
    {
      String methodSig = this.getClass().getName() + "::loadCards()";
      throw new Exception(methodSig + ": " + e.toString());
    }
    finally
    {
      it.close();
    }
  }

  /*
  ** protected void loadPricing(long appSeqNum)
  **
  ** Loads v/mc pricing and non-v/mc auth fees from tranchrg table.
  */
  protected void loadPricing(long appSeqNum)
  {
    ResultSetIterator it = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2207^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code,
//                  tranchrg_interchangefee_fee,
//                  tranchrg_disc_rate,
//                  tranchrg_pass_thru,
//                  tranchrg_per_auth,
//                  tranchrg_mmin_chrg,
//                  tranchrg_voice_fee,
//                  tranchrg_per_tran
//          from    tranchrg
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code,\n                tranchrg_interchangefee_fee,\n                tranchrg_disc_rate,\n                tranchrg_pass_thru,\n                tranchrg_per_auth,\n                tranchrg_mmin_chrg,\n                tranchrg_voice_fee,\n                tranchrg_per_tran\n        from    tranchrg\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"43com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"43com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2219^7*/

      ResultSet rs = it.getResultSet();
      String amountStr;
      while (rs.next())
      {
        switch (rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_VISA:
            fields.setData("pricingType",rs.getString("tranchrg_interchangefee_fee"));
            fields.setData("discRate",rs.getString("tranchrg_disc_rate"));
            fields.setData("perItem",rs.getString("tranchrg_pass_thru"));
            fields.setData("minMonthlyDisc",rs.getString("tranchrg_mmin_chrg"));
            amountStr = Double.toString(rs.getDouble("tranchrg_per_auth"));
            fields.setData("vmcAuthFee",amountStr);
            amountStr = Double.toString(rs.getDouble("tranchrg_voice_fee"));
            fields.setData("voiceAuthFee",amountStr);
            break;

          case mesConstants.APP_CT_AMEX:
            amountStr = Double.toString(rs.getDouble("tranchrg_per_tran"));
            fields.setData("amexAuthFee",amountStr);
            break;

          case mesConstants.APP_CT_DISCOVER:
            amountStr = Double.toString(rs.getDouble("tranchrg_per_tran"));
            fields.setData("discAuthFee",amountStr);
            break;

          case mesConstants.APP_CT_DINERS_CLUB:
            amountStr = Double.toString(rs.getDouble("tranchrg_per_tran"));
            fields.setData("dinrAuthFee",amountStr);
            break;

          case mesConstants.APP_CT_JCB:
            amountStr = Double.toString(rs.getDouble("tranchrg_per_tran"));
            fields.setData("jcbAuthFee",amountStr);
            break;
        }
      }
    }
    catch (Exception e)
    {
      log.error("Error in loadPricing(): " + e);
      e.printStackTrace();
      throw new RuntimeException(e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) { }
    }
  }

  /*
  ** protected void loadChargeRecords(long appSeqNum)
  **
  ** Loads charge record data from miscchrg (chargebacks only) and appo_charge_recs.
  */
  protected void loadChargeRecords(long appSeqNum)
  {
    ResultSetIterator it = null;

    try
    {
      // chargebacks
      /*@lineinfo:generated-code*//*@lineinfo:2284^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  misc_chrg_amount
//          from    miscchrg
//          where   app_seq_num = :appSeqNum
//                  and misc_code = :mesConstants.APP_MISC_CHARGE_CHARGEBACK
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  misc_chrg_amount\n        from    miscchrg\n        where   app_seq_num =  :1 \n                and misc_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"44com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_CHARGEBACK);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"44com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2290^7*/

      ResultSet rs = it.getResultSet();
      if (rs.next())
      {
        fields.setData("chargebackFee",
          Double.toString(rs.getDouble("misc_chrg_amount")));
      }
      rs.close();


      // charge records
      /*@lineinfo:generated-code*//*@lineinfo:2302^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  charge_rec_num,
//                  charge_rec_type,
//                  amount,
//                  description,
//                  product_type
//          from    appo_charge_recs
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  charge_rec_num,\n                charge_rec_type,\n                amount,\n                description,\n                product_type\n        from    appo_charge_recs\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"45com.mes.app.vs.Application",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"45com.mes.app.vs.Application",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2311^7*/

      rs = it.getResultSet();
      while (rs.next())
      {
        String recNum = rs.getString("charge_rec_num");
        fields.setData("crLabel_" + recNum,rs.getString("description"));
        fields.setData("crType_" + recNum,rs.getString("charge_rec_type"));
        fields.setData("crAmount_" + recNum,rs.getString("amount"));
        fields.setData("crProductType_" + recNum,rs.getString("product_type"));
      }
    }
    catch (Exception e)
    {
      log.error("Error in loadChargeRecords(): " + e);
      e.printStackTrace();
      throw new RuntimeException(e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) { }
    }
  }

  /*
  ** protected boolean loadAppData()
  **
  ** Loads app data.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean loadAppData()
  {
    boolean loadOk = false;
    ResultSetIterator it = null;
    try
    {
      connect();
      
      // don't try to load data if no appSeqNum is set
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      if (appSeqNum != 0L)
      {
        loadMerchant(appSeqNum);
        loadAddresses(appSeqNum);
        loadOwners(appSeqNum);
        loadContact(appSeqNum);
        loadBank(appSeqNum);
        loadCards(appSeqNum);
        loadPricing(appSeqNum);
        loadChargeRecords(appSeqNum);
      }
      loadOk = true;
    }
    catch(Exception e)
    {
      logEntry("loadData()",e.toString());
      log.debug(this.getClass().getName() + "::loadAppData(): " 
        + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return loadOk;
  }
}/*@lineinfo:generated-code*/