/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/mes/Business.sqlj $

  Description:
  
  Business
  
  CB&T online application merchant information page bean.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/20/04 5:06p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.ccb;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.NumberField;
import com.mes.forms.SicField;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class Business extends com.mes.app.BusinessBase
{
  {
    appType = 47;
    curScreenId = 1;
  }
  
  protected class IndustryTypeTable extends DropDownTable
  {
    public IndustryTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail - 010450");
      addElement("2","Restaurant - 011100");
      addElement("10","Lodging - 011101");
      addElement("17","Supermarket - 010450");
      addElement("5","Internet - 010450");
      addElement("6","Services - 010450");
      addElement("7","Direct Marketing - 010450");
      addElement("9","Cash Advance - 010450");
      addElement("8","Other - 010450");
    }
  }
  
  protected class LocationTypeTable extends DropDownTable
  {
    public LocationTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Retail Storefront");
      addElement("2","Internet Storefront");
      addElement("3","Business Office");
      addElement("4","Private Residence");
      addElement("7","Separate Building");
      addElement("6","Bank");
      addElement("5","Other");
    }
  }
  
  protected class BranchTable extends DropDownTable
  {
    public BranchTable(long topHid)
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      
      long              trueTopHid = 0L;
      
      try
      {
        connect();
        
        // determine whether topHid is valid -- if not then set to default top-level for ccb
        /*@lineinfo:generated-code*//*@lineinfo:96^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(th.descendent)  valid_count
//            from    t_hierarchy th
//            where   th.ancestor = 3941400094 and
//                    th.descendent = :topHid
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(th.descendent)  valid_count\n          from    t_hierarchy th\n          where   th.ancestor = 3941400094 and\n                  th.descendent =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.ccb.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,topHid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.ccb.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:102^9*/
        
        rs = it.getResultSet();
        
        if(rs.next() && rs.getInt("valid_count") > 0)
        {
          trueTopHid = topHid;
        }
        else
        {
          trueTopHid = 3941400094L;
        }
        
        rs.close();
        it.close();
        
        /*@lineinfo:generated-code*//*@lineinfo:118^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    mod(t1.descendent,1000000)  assoc_num,
//                      gn2.group_name              branch_name
//            from      t_hierarchy t1,
//                      t_hierarchy t2,
//                      group_names gn1,
//                      group_names gn2
//            where     t1.ancestor = :trueTopHid
//                      and t1.relation = to_number(substr(to_char(t1.ancestor), 5, 1))
//                      and t1.descendent = gn1.group_number
//                      and t2.descendent = t1.descendent
//                      and t2.relation = 1
//                      and t2.ancestor = gn2.group_number
//                      and lower(gn1.group_name) like '%non%chain%'
//            order by  gn2.group_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mod(t1.descendent,1000000)  assoc_num,\n                    gn2.group_name              branch_name\n          from      t_hierarchy t1,\n                    t_hierarchy t2,\n                    group_names gn1,\n                    group_names gn2\n          where     t1.ancestor =  :1 \n                    and t1.relation = to_number(substr(to_char(t1.ancestor), 5, 1))\n                    and t1.descendent = gn1.group_number\n                    and t2.descendent = t1.descendent\n                    and t2.relation = 1\n                    and t2.ancestor = gn2.group_number\n                    and lower(gn1.group_name) like '%non%chain%'\n          order by  gn2.group_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.ccb.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,trueTopHid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.ccb.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:134^9*/
        
        rs = it.getResultSet();
        
        addElement("", "select one");
        while(rs.next())
        {
          addElement(rs.getString("assoc_num"), rs.getString("branch_name"));
        }
      }
      catch(Exception e)
      {
        logEntry("BranchTable()",e.toString());
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // make some fields required
      fields.getField("accountType").makeRequired();
      fields.getField("taxpayerId").makeRequired();
      fields.getField("yearsOpen").makeRequired();
      
      // make some fields not required
      fields.getField("sourceOfInfo").makeOptional();
      
      // add sic code as visible field
      fields.deleteField("sicCode");
      fields.add(new SicField("sicCode", "SIC Code", false));
      fields.getField("sicCode").makeOptional();
      
      fields.deleteField("assocNum");
      fields.add(new NumberField("assocNum", "Association #", 6, 6, true, 0));

      // make bank account and transit confirmation fields optional (not used)
      fields.getField("confirmCheckingAccount").makeOptional();
      fields.getField("confirmTransitRouting").makeOptional();

      // add referring branch assoc num dropdown
      fields.add(new DropDownField("assocNum",new BranchTable(user.getHierarchyNode()),false));
      
      // override description of existingVerisign so that it is generic
      fields.add(new CheckboxField("existingVerisign",  "Merchant has an existing Internet account",false));
      
      // override location and industry type tables for CCB
      fields.add(new DropDownField    ("industryType",      "Industry",new IndustryTypeTable(),false));
      fields.add(new DropDownField    ("locationType",      "Type of Business Location",new LocationTypeTable(),false));
      
      fields.getField("businessEmail").addValidation(new InternetEmailRequiredValidation(fields.getField("productType")));
      
      createPosPartnerExtendedFields();
      
      // reset all fields, including those just added to be of class form
      fields.setHtmlExtra("class=\"formText\"");
      
      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      logEntry("createFields()",e.toString());
    }
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);
    
    // copy dba to legal name if legal is blank
    if (fields.getField("businessLegalName").isBlank())
    {
      fields.setData("businessLegalName",fields.getData("businessName"));
    }
  }
  
  protected boolean loadAppData()
  {
    long                appSeqNum   = 0L;
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = super.loadAppData();
    
    try
    {
      appSeqNum = fields.getField("appSeqNum").asInteger();
      
      if(result && appSeqNum != 0)
      {
        connect();
      
        // load client data
        /*@lineinfo:generated-code*//*@lineinfo:236^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  client_data_1
//            from    merchant
//            where   app_seq_num = :fields.getField("appSeqNum").asInteger()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_2737 = fields.getField("appSeqNum").asInteger();
  try {
   String theSqlTS = "select  client_data_1\n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.ccb.Business",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_2737);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.ccb.Business",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:241^9*/
      
        rs = it.getResultSet();
      
        setFields(rs);
      
        result = true;
      }
    }
    catch(Exception e)
    {
      logEntry("loadAppData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
}/*@lineinfo:generated-code*/