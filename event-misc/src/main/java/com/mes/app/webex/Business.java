/*@lineinfo:filename=Business*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/storefront/Business.sqlj $

  Description:
  
  Business
  
  Banner Bank online application merchant information page bean.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.webex;

import javax.servlet.http.HttpServletRequest;
import com.mes.app.VirtualAppBusinessBase;
import com.mes.constants.mesConstants;

public class Business extends VirtualAppBusinessBase
{
  {
    appType     = mesConstants.APP_TYPE_WEBEX;
    curScreenId = 1;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // create new field for netSuiteAcctNum
      //fields.add(new HiddenField("netSuiteAcctNum"));
      
      setData("productType", Integer.toString(mesConstants.POS_INTERNET));
      setData("internetType", Integer.toString(mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK));
      
      // set to default netsuite association
      setData("assocNum", "600231");
      
    }
    catch(Exception e)
    {
      logEntry("createFields()-webex\\Business", e.toString());
    }
  }
  
  protected boolean submitAppData()
  {
    boolean submitOk = false;

    try
    {
      connect();
      submitOk = super.submitAppData();
    
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      Pricing merchPricing = new Pricing(appSeqNum);

      submitOk = merchPricing.submitAppData();

      /*      
      // add netSuiteAcctNum to database
      #sql [Ctx]
      {
        update  merchant
        set     net_suite_acct_num = :(getData("netSuiteAcctNum"))
        where   app_seq_num = :(getData("appSeqNum"))
      };
      */
    }
    catch(Exception e)
    {
      logEntry("submitAppData(" + fields.getData("appSeqNum") + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  
    return submitOk;
  }
}/*@lineinfo:generated-code*/