/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/foothill/FhEquipment.java $

  Description:  


  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 5/08/03 4:50p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.foothill;

import java.sql.ResultSet;
//import sqlj.runtime.ResultSetIterator;
import com.mes.app.EquipmentDataBean;
import com.mes.forms.Field;

public class FhEquipment extends EquipmentDataBean
{
  public FhEquipment( )
  {
  }
  
  public void init( )
  {
    Field                   field       = null;
    
    super.init();
    
    try
    {    
      //@ create any necessary form fields
    }
    catch( Exception e )
    {
      logEntry("init()",e.toString());
    }
  }
  
  public void loadData( )
  {
    //ResultSetIterator           it        = null;
    ResultSet                   resultSet = null;
    
    super.loadData();
    
    try
    {
      //@ add database select statements to populate form fields
    }
    catch( Exception e )
    {
      logEntry("loadData()",e.toString());
    }                          
    finally
    {
      //try{ it.close(); } catch(Exception e) {}
    }
  }

  public void storeData( )
  {
    super.storeData();
    
    try
    {
      //@ add store database insert/update calls using form field data
    }
    catch( Exception e )
    {
      logEntry("storeData()",e.toString());
    }
    finally
    {
    }
  }
}
