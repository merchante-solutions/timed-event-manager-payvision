/*@lineinfo:filename=FhMerchInfo*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/foothill/FhMerchInfo.sqlj $

  Description:  


  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 7/22/03 8:35a $
  Version            : $Revision: 9 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app.foothill;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.mes.app.MerchInfoDataBean;
import com.mes.forms.DropDownField;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class FhMerchInfo extends MerchInfoDataBean
{
  /*
  ** public static class BranchTable extends DropDownTable
  **
  ** List of River City referring branches with association derived from each
  ** branch's hierarchy node id.
  */
  protected class BranchTable extends DropDownTable
  {
    public BranchTable()
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:58^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    mod(t1.descendent,1000000)  assoc_num,
//                      o2.org_name                 branch_name
//                      
//            from      t_hierarchy t1,
//                      t_hierarchy t2,
//                      organization o1,
//                      organization o2
//                      
//            where     t1.ancestor = 3941400042
//                      and t1.relation = 4
//                      and t1.descendent = o1.org_group
//                      and t2.descendent = t1.descendent
//                      and t2.relation = 1
//                      and t2.ancestor = o2.org_group
//                      and lower(o1.org_name) like '%non%chain%'
//                      
//            order by  t2.ancestor,o1.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mod(t1.descendent,1000000)  assoc_num,\n                    o2.org_name                 branch_name\n                    \n          from      t_hierarchy t1,\n                    t_hierarchy t2,\n                    organization o1,\n                    organization o2\n                    \n          where     t1.ancestor = 3941400042\n                    and t1.relation = 4\n                    and t1.descendent = o1.org_group\n                    and t2.descendent = t1.descendent\n                    and t2.relation = 1\n                    and t2.ancestor = o2.org_group\n                    and lower(o1.org_name) like '%non%chain%'\n                    \n          order by  t2.ancestor,o1.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.foothill.FhMerchInfo",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.foothill.FhMerchInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:77^9*/
        rs = it.getResultSet();
        
        addElement("","select one");
        while (rs.next())
        {
          addElement(rs.getString("assoc_num"),rs.getString("branch_name"));
        }
      }
      catch (Exception e)
      {
        logEntry("BranchTable()",e.toString());
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  
  public void init( )
  {
    super.init();
    
    try
    {
      // add additional 
      FieldGroup fgFh = new FieldGroup("fgFoothill");
      
      // replaces base class field with drop down of foothill branch assocs
      fgFh.add(new DropDownField("assocNum",      new BranchTable(),false));
      
      fgFh.add(new NumberField  ("fhBankIdNum",   6,35,false,0));
      fgFh.setGroupHtmlExtra("class=\"formFields\"");
      fields.add(fgFh);
      
      // make sic code required
      fields.getField("sicCode").makeRequired();
      
      // make primary owner phone number required
      fields.getField("owner1Phone").makeRequired();
      
      // turn off required validation on bank address fields
      fields.getField("sourceOfInfo" ).removeValidation("required");
      fields.getField("bankAddress"  ).removeValidation("required");
      fields.getField("bankCity"     ).removeValidation("required");
      fields.getField("bankState"    ).removeValidation("required");
      fields.getField("bankZip"      ).removeValidation("required");
      fields.getField("referringBank").removeValidation("required");
      
      // set some default data
      fields.setData("transitRouting","122232439");
      fields.setData("confirmTransitRouting","122232439");
      fields.setData("bankName","Foothill Independent Bank");
      
      // create the extended POS Partner 2000 fields
      createPosPartnerExtendedFields();
      
      // must be last call in order to set all fields
      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch( Exception e )
    {
      logEntry("init()",e.toString());
    }
  }
  
  public void loadData( )
  {
    super.loadData();
    
    ResultSetIterator it = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:154^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bank_id_num as fh_bank_id_num
//          from    app_merch_foothill
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bank_id_num as fh_bank_id_num\n        from    app_merch_foothill\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.foothill.FhMerchInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.foothill.FhMerchInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:159^7*/
      setFields(it.getResultSet());
    }
    catch( Exception e )
    {
      logEntry("loadData()",e.toString());
    }                          
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }

  public void storeData( )
  {
    super.storeData();

    try
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:180^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_merch_foothill
//            ( app_seq_num,
//              bank_id_num )
//            values
//            ( :AppSeqNum,
//              : getData("fhBankIdNum") )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2894 =  getData("fhBankIdNum");
   String theSqlTS = "insert into app_merch_foothill\n          ( app_seq_num,\n            bank_id_num )\n          values\n          (  :1 ,\n             :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.foothill.FhMerchInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,__sJT_2894);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:188^9*/
      }
      catch (SQLException e)
      {
        /*@lineinfo:generated-code*//*@lineinfo:192^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_merch_foothill
//            set     bank_id_num   = :getData("fhBankIdNum")
//            where   app_seq_num   = :AppSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2895 = getData("fhBankIdNum");
   String theSqlTS = "update  app_merch_foothill\n          set     bank_id_num   =  :1 \n          where   app_seq_num   =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.foothill.FhMerchInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2895);
   __sJT_st.setLong(2,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^9*/
      }
    }
    catch( Exception e )
    {
      logEntry("storeData()",e.toString());
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/