/*@lineinfo:filename=FhPricing*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/foothill/FhPricing.sqlj $

  Description:

  FhPricing

  River City Bank's online app pricing page bean.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 9/16/03 11:16a $
  Version            : $Revision: 11 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.foothill;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.app.AppDataBeanBase;
import com.mes.constants.mesConstants;
import com.mes.forms.CheckboxField;
import com.mes.forms.Condition;
import com.mes.forms.CurrencyField;
import com.mes.forms.DiscountField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.SmallCurrencyField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.forms.ZipField;
import com.mes.support.MesMath;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class FhPricing extends AppDataBeanBase
{

  //**************************************************************************
  // Drop Down Tables
  //**************************************************************************

  protected class DollarAmountTable extends DropDownTable
  {
    public DollarAmountTable(boolean hasNonselected, double[] amounts)
    {
      if (hasNonselected)
      {
        addElement("","select one");
      }
      for (int i = 0; i < amounts.length; ++i)
      {
        addElement(Double.toString(amounts[i]),MesMath.toCurrency(amounts[i]));
      }
    }
  }

  protected class PricingTypeTable extends DropDownTable
  {
    public PricingTypeTable()
    {
      addElement("","select one");
      addElement("1","Retail - pass thru interchange");
      addElement("2","Retail - pass thru + 25 bp");
      addElement("3","MOTO - pass thru interchange");
      addElement("4","MOTO - pass thru + 25 bp");
    }
  }

  protected class LocationAddressTable extends DropDownTable
  {
    public LocationAddressTable( )
    {
      //@ should be fixed to use codes instead of text
      addElement("","select one");
      addElement("Business Address on Application", "Business Address on Application" );
      addElement("Mailing Address on Application", "Mailing Address on Application" );
      addElement("Other", "Other" );
    }
  }

  protected class LocationTypeTable extends DropDownTable
  {
    public LocationTypeTable( )
    {
      //@ should be fixed to use codes instead of text
      addElement("", "select one");
      addElement("Office Building", "Office Building" );
      addElement("Industrial Park", "Industrial Park" );
      addElement("Strip Mall", "Strip Mall" );
      addElement("Residence", "Residence" );
      addElement("Other", "Other" );
    }
  }

  //**************************************************************************
  // Validations
  //**************************************************************************
  
  protected class FulfillmentHouseValidation implements Validation
  {
    String                  ErrorMessage      = null;
    FieldGroup              Fields            = null;

    public FulfillmentHouseValidation( FieldGroup fields )
    {
      Fields = fields;
    }

    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate( String fdata )
    {
      ErrorMessage = null;

      // isBlank prevents NullPointerException
      if ( !isBlank(fdata) && fdata.toUpperCase().equals("Y") )
      {
        if (Fields.getField("fulfillmentAddr").isBlank() ||
            Fields.getField("fulfillmentCity").isBlank() ||
            Fields.getField("fulfillmentState").isBlank() ||
            Fields.getField("fulfillmentZip").isBlank())
        {
          ErrorMessage = "Please provide the full address of the Fulfillment House";
        }
      }
      return( ErrorMessage == null );
    }
  }

  protected class LocationAddressValidation implements Validation
  {
    String                  ErrorMessage      = null;
    FieldGroup              Fields            = null;

    public LocationAddressValidation( FieldGroup fields )
    {
      Fields = fields;
    }

    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate( String fdata )
    {
      ErrorMessage = null;

      try
      {
        if ( fdata.equals("Other") )
        {
          if (  Fields.getField("locationAddr").isBlank() ||
                Fields.getField("locationCity").isBlank() ||
                Fields.getField("locationState").isBlank() ||
                Fields.getField("locationZip").isBlank() )
          {
            ErrorMessage = "Please provide the full address of the inspected location";
          }
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Please select the address of the inspected location";
      }
      return( ErrorMessage == null );
    }
  }

  protected class LocationTypeValidation implements Validation
  {
    Field       OtherField          = null;

    public LocationTypeValidation( Field otherField )
    {
      OtherField      = otherField;
    }

    public String getErrorText()
    {
      return("Please provide an explanation when selecting Other for the business location");
    }

    public boolean validate( String fdata )
    {
      return ( !OtherField.getData().equals("Other")
               || (fdata != null && fdata.length() > 0) );
    }
  }

  //**************************************************************************
  // Data Bean
  //**************************************************************************

  public FhPricing()
  {
  }

  // pricing types
  protected static String[][] PricePlanRadioButtons =
  {
    { "Fixed Rate Plan",    Integer.toString( mesConstants.APP_PS_FIXED_PLUS_PER_ITEM ) },
    { "Interchange Plan",   Integer.toString( mesConstants.APP_PS_INTERCHANGE )         }
  };

  // pos type (set on page 1) determines if certain fee options are present
  protected int posType = -1;

  /*
  ** protected boolean loadPosType()
  **
  ** Loads the pos type from the pos_category/merch_pos.  This is an option set
  ** on page 1 of the app.
  **
  ** RETURNS: true if load successful, else false.
  */
  protected boolean loadPosType()
  {
    boolean loadOk = false;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:233^7*/

//  ************************************************************
//  #sql [Ctx] { select  pc.pos_type
//          
//          from    pos_category  pc,
//                  merch_pos     mp
//          where   mp.app_seq_num = :AppSeqNum
//                  and pc.pos_code = mp.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pc.pos_type\n         \n        from    pos_category  pc,\n                merch_pos     mp\n        where   mp.app_seq_num =  :1 \n                and pc.pos_code = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.foothill.FhPricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   posType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:241^7*/

      loadOk = true;
    }
    catch (Exception e)
    {
      logEntry("loadPosType()",e.toString());
    }
    finally
    {
    }
    return loadOk;
  }

  /*
  ** protected int getPosType()
  **
  ** Loads pos type if not loaded already.
  **
  ** RETURN: loaded pos type.
  */
  protected int getPosType()
  {
    if (posType == -1)
    {
      loadPosType();
    }
    return posType;
  }

  /*
  ** protected boolean isPosType(int checkPosType)
  **
  ** Checks the pos type given against the app's pos type.
  **
  ** RETURNS: true if app's pos type matches the given pos type.
  */
  protected boolean isPosType(int checkPosType)
  {
    return checkPosType == getPosType();
  }

  /*
  ** protected Field getCardFeeDropDownField(int cardType)
  **
  ** Generates a drop down field for the given card type populated with
  ** the fees specific to the given card type.
  **
  ** RETURNS: drop down field containing card type fees.
  */
  protected Field getCardFeeDropDownField(int cardType)
  {
    // create a fee table and set the default
    DropDownTable feeTable = null;
    String defaultFee = null;
    switch(cardType)
    {
      // don't have debit info, make free field for now
      case mesConstants.APP_CT_DEBIT:
        return new CurrencyField("perItemFee" + cardType,8,6,true);

      default:
        feeTable = new DollarAmountTable(false,new double[] { 0.1, 0.15, 0.2 });
        defaultFee = "0.15";
        break;
    }

    // generate drop down field with fee table and set the default option
    Field feeField = new DropDownField("perItemFee" + cardType,feeTable,false);
    feeField.setData(defaultFee);

    return feeField;
  }

  /*
  ** protected boolean hasPerItemFee(int cardType)
  **
  ** Determines if the given card type is accepted (page 1 option) and if so,
  ** whether it may have additional per item fee associated with it.
  **
  ** RETURNS: true if the card is accepted and may have per item fees associated.
  */
  protected boolean hasPerItemFee(int cardType)
  {
    int         paySolCardType  = mesConstants.APP_CT_COUNT;
    boolean     hasFee          = false;
    int         rowCount        = 0;

    try
    {
      // look for special card type cases
      switch (cardType)
      {
        // they don't have internet auth fees for now
        case mesConstants.APP_CT_INTERNET:
          return false;

        // they don't have dial pay auth fees for now
        case mesConstants.APP_CT_DIAL_PAY:
          return false;
      }

      // see if card type was selected on page 1 and that card type
      // supports additional per item fee (i.e. not v/mc, fees and
      // pricing of which are determined by the main pricing options)
      /*@lineinfo:generated-code*//*@lineinfo:346^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(po.cardtype_code)
//          
//  
//          from    merchpayoption po
//  
//          where   po.app_seq_num        = :AppSeqNum
//                  and po.cardtype_code  = :cardType
//                  and po.cardtype_code in
//                    -- card types allowing additional per item fee
//                    ( :mesConstants.APP_CT_DINERS_CLUB,
//                      :mesConstants.APP_CT_DISCOVER,
//                      :mesConstants.APP_CT_JCB,
//                      :mesConstants.APP_CT_AMEX,
//                      :mesConstants.APP_CT_DEBIT,
//                      :mesConstants.APP_CT_CHECK_AUTH,
//                      :mesConstants.APP_CT_INTERNET,
//                      :mesConstants.APP_CT_DIAL_PAY,
//                      :mesConstants.APP_CT_EBT )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(po.cardtype_code)\n         \n\n        from    merchpayoption po\n\n        where   po.app_seq_num        =  :1 \n                and po.cardtype_code  =  :2 \n                and po.cardtype_code in\n                  -- card types allowing additional per item fee\n                  (  :3 ,\n                     :4 ,\n                     :5 ,\n                     :6 ,\n                     :7 ,\n                     :8 ,\n                     :9 ,\n                     :10 ,\n                     :11  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.foothill.FhPricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,cardType);
   __sJT_st.setInt(3,mesConstants.APP_CT_DINERS_CLUB);
   __sJT_st.setInt(4,mesConstants.APP_CT_DISCOVER);
   __sJT_st.setInt(5,mesConstants.APP_CT_JCB);
   __sJT_st.setInt(6,mesConstants.APP_CT_AMEX);
   __sJT_st.setInt(7,mesConstants.APP_CT_DEBIT);
   __sJT_st.setInt(8,mesConstants.APP_CT_CHECK_AUTH);
   __sJT_st.setInt(9,mesConstants.APP_CT_INTERNET);
   __sJT_st.setInt(10,mesConstants.APP_CT_DIAL_PAY);
   __sJT_st.setInt(11,mesConstants.APP_CT_EBT);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:366^7*/
      hasFee = (rowCount > 0);
    }
    catch( Exception e )
    {
      logEntry("hasPerItemFee(cardType=" + cardType + ")",e.toString());
    }
    return hasFee;
  }

  /*
  ** protected void initCardFees()
  **
  ** Generate all applicable card per item fee fields.
  */
  protected void initCardFees()
  {
    for(int ct = 1; ct < mesConstants.APP_CT_COUNT; ++ct)
    {
      if (hasPerItemFee(ct))
      {
        fields.add(getCardFeeDropDownField(ct));
      }
    }
  }

  // these are all the misc charges supported by the river city app
  protected static int[] miscCharges =
  {
    mesConstants.APP_MISC_CHARGE_CHARGEBACK,
    mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE,
    mesConstants.APP_MISC_CHARGE_HELP_DESK,
    mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT_FEE,
    mesConstants.APP_MISC_CHARGE_WEB_REPORTING,
    mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_SETUP,
    mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP,
    mesConstants.APP_MISC_CHARGE_REPROGRAMMING_FEE
  };

  protected int[] getMiscCharges()
  {
    return miscCharges;
  }
  
  // contains descriptions of miscellaneous charges
  protected String miscChargeDescs[] = null;

  /*
  ** protected boolean loadMiscChargeDescs()
  **
  ** Attempts to load charge item descriptions from the miscdescrs table into
  ** the miscChargeDescs array.
  **
  ** RETURNS: true if load successful, else false.
  */
  protected boolean loadMiscChargeDescs()
  {
    boolean           loadOk  = false;
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:429^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  misc_description,
//                  misc_code
//          from    miscdescrs
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  misc_description,\n                misc_code\n        from    miscdescrs";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.foothill.FhPricing",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.foothill.FhPricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:434^7*/
      rs = it.getResultSet();

      miscChargeDescs = new String[mesConstants.APP_MISC_CHARGE_COUNT];
      while (rs.next())
      {
        int chargeIdx = rs.getInt("misc_code");
        if (chargeIdx < miscChargeDescs.length)
        {
          miscChargeDescs[chargeIdx] = rs.getString("misc_description");
        }
      }

      loadOk = true;
    }
    catch (Exception e)
    {
      logEntry("loadMiscChargeDescs()",e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { it.close(); } catch (Exception e) {}
    }

    return loadOk;
  }

  /*
  ** public String getMiscChargeDesc(int chargeIdx)
  **
  ** Retrieves the description corresponding with the charge index.
  **
  ** RETURNS: the charge description string corresponding with the index given,
  **          or null if index is out of range or the charge description array
  **          cannot be loaded.
  */
  public String getMiscChargeDesc(int chargeIdx)
  {
    if ((miscChargeDescs != null || loadMiscChargeDescs())
        && chargeIdx < miscChargeDescs.length)
    {
      return miscChargeDescs[chargeIdx];
    }
    return null;
  }

  /*
  ** protected void initMiscCharges()
  **
  ** Creates miscellaneous charge checkboxes and amount fields.
  */
  protected void initMiscCharges()
  {
    // create fields for each item in the misc charges list
    int[] miscCharges = getMiscCharges();
    for(int mc = 0; mc < miscCharges.length; ++mc)
    {
      // get a description of the charge
      String chargeDesc = getMiscChargeDesc(miscCharges[mc]);

      // set default status of checkbox, adjust description if needed
      boolean isChecked = false;
      switch (miscCharges[mc])
      {
        case mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT_FEE:
        case mesConstants.APP_MISC_CHARGE_CHARGEBACK:
          isChecked = true;
          break;
          
        case mesConstants.APP_MISC_CHARGE_REPROGRAMMING_FEE:
          isChecked = isPosType(mesConstants.POS_DIAL_TERMINAL);
          break;
      }

      // create the checkbox
      Field chargeCb = new CheckboxField(("miscChargeEnable" + miscCharges[mc]),
        chargeDesc,isChecked);
      fields.add(chargeCb);

      // determine fee table and default fee amount
      // no fee table means use currency field
      DropDownTable feeTable = null;
      String defaultAmount = null;
      switch (miscCharges[mc])
      {
        case mesConstants.APP_MISC_CHARGE_CHARGEBACK:
          feeTable = new DollarAmountTable(true,new double[] { 20, 25 });
          defaultAmount = "20.0";
          break;

        case mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT_FEE:
          defaultAmount = (isPosType(mesConstants.POS_DIAL_TERMINAL) ? "15" : "12");
          break;

        case mesConstants.APP_MISC_CHARGE_WEB_REPORTING:
          feeTable = new DollarAmountTable(true,new double[] { 5, 10 });
          break;
          
        case mesConstants.APP_MISC_CHARGE_REPROGRAMMING_FEE:
          defaultAmount = (isPosType(mesConstants.POS_DIAL_TERMINAL) ? "30" : null);
          break;

        case mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_SETUP:
        case mesConstants.APP_MISC_CHARGE_HELP_DESK:
        case mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP:
          break;
      }

      // create the amount field
      Field amountField;
      String fieldName = "miscChargeAmount" + miscCharges[mc];
      if (feeTable != null)
      {
        amountField = new DropDownField(fieldName,feeTable,true);
      }
      else
      {
        amountField = new CurrencyField(fieldName,8,6,true);
      }
      amountField.addValidation(new IfYesNotBlankValidation(chargeCb,
        "Please provide a valid fee amount for the " + chargeDesc));

      // set default amount
      if (defaultAmount != null)
      {
        amountField.setData(defaultAmount);
      }

      fields.add(amountField);
    }
  }

  public String getCardTypeDesc( int ct )
  {
    String          retVal      = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:572^7*/

//  ************************************************************
//  #sql [Ctx] { select  ct.cardtype_desc 
//          from    cardtype    ct
//          where   ct.cardtype_code = :ct
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ct.cardtype_desc  \n        from    cardtype    ct\n        where   ct.cardtype_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.foothill.FhPricing",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,ct);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:577^7*/
    }
    catch( Exception e )
    {
      retVal = "Card Type " + ct;
    }
    return( retVal );
  }

  public class EquipmentFee
  {
    public    int                 EquipCount        = 0;
    public    String              EquipDesc         = null;
    protected String              EquipModel        = null;
    protected int                 LendType          = 0;
    public    String              LendTypeDesc      = null;

    public EquipmentFee( ResultSet resultSet )
      throws java.sql.SQLException
    {
      Field               field   =   null;

      // store the HTML descriptoins
      LendType      = resultSet.getInt("lend_type");
      LendTypeDesc  = resultSet.getString("lend_type_desc");
      EquipDesc     = resultSet.getString("equip_desc");
      EquipModel    = resultSet.getString("equip_model");
      EquipCount    = resultSet.getInt("equip_count");

      // add the currency input field
      field = new CurrencyField( getFieldName(),8,10,false);
      field.setData( resultSet.getString("per_item") );
      fields.add(field);
    }

    public String getFieldName( )
    {
      StringBuffer      fieldName     = new StringBuffer();

      fieldName.append( EquipModel );
      fieldName.append( "_" );
      fieldName.append( LendType );
      fieldName.append( "_PerItem" );

      return( fieldName.toString() );
    }

    public double getTotalAmount( )
    {
      double      perItem       = 0.0;
      try
      {
        perItem = fields.getField(getFieldName()).asDouble();
      }
      catch( Exception e )
      {
      }
      return( EquipCount * perItem );
    }
  }

  protected Vector EquipmentFees = new Vector();

  protected void initEquipmentFees( )
  {
    ResultSetIterator         it            = null;
    ResultSet                 resultSet     = null;

    try
    {
      EquipmentFees.removeAllElements();

      /*@lineinfo:generated-code*//*@lineinfo:649^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  eq.equip_model                          as equip_model,
//                  decode( po.prod_option_des,
//                          null, eq.equip_descriptor,
//                          ( eq.equip_descriptor || '-' ||
//                            po.prod_option_des ) )        as equip_desc,
//                  lt.equiplendtype_code                   as lend_type,
//                  lt.equiplendtype_description            as lend_type_desc,
//                  me.merchequip_equip_quantity            as equip_count,
//                  me.merchequip_amount                    as per_item
//          from    merchequipment    me,
//                  equipment         eq,
//                  equiplendtype     lt,
//                  prodoption        po
//          where   me.app_seq_num = :AppSeqNum and
//                  -- all of the buy, rent or lease items
//                  me.equiplendtype_code in
//                  (
//                    :mesConstants.APP_EQUIP_PURCHASE,         -- 1,
//                    :mesConstants.APP_EQUIP_RENT,             -- 2,
//                    :mesConstants.APP_EQUIP_BUY_REFURBISHED,  -- 4,
//                    :mesConstants.APP_EQUIP_LEASE             -- 5
//                  ) and
//                  eq.equip_model = me.equip_model and
//                  po.prod_option_id(+) = me.prod_option_id and
//                  lt.equiplendtype_code = me.equiplendtype_code
//          order by me.equiplendtype_code, eq.equiptype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  eq.equip_model                          as equip_model,\n                decode( po.prod_option_des,\n                        null, eq.equip_descriptor,\n                        ( eq.equip_descriptor || '-' ||\n                          po.prod_option_des ) )        as equip_desc,\n                lt.equiplendtype_code                   as lend_type,\n                lt.equiplendtype_description            as lend_type_desc,\n                me.merchequip_equip_quantity            as equip_count,\n                me.merchequip_amount                    as per_item\n        from    merchequipment    me,\n                equipment         eq,\n                equiplendtype     lt,\n                prodoption        po\n        where   me.app_seq_num =  :1  and\n                -- all of the buy, rent or lease items\n                me.equiplendtype_code in\n                (\n                   :2 ,         -- 1,\n                   :3 ,             -- 2,\n                   :4 ,  -- 4,\n                   :5              -- 5\n                ) and\n                eq.equip_model = me.equip_model and\n                po.prod_option_id(+) = me.prod_option_id and\n                lt.equiplendtype_code = me.equiplendtype_code\n        order by me.equiplendtype_code, eq.equiptype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_RENT);
   __sJT_st.setInt(4,mesConstants.APP_EQUIP_BUY_REFURBISHED);
   __sJT_st.setInt(5,mesConstants.APP_EQUIP_LEASE);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.app.foothill.FhPricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:677^7*/
      resultSet = it.getResultSet();

      while(resultSet.next())
      {
        EquipmentFees.addElement( new EquipmentFee( resultSet ) );
      }
      resultSet.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:687^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  null                                    as equip_model,
//                  'Equipment Support Fee'                 as equip_desc,
//                  me.equiplendtype_code                   as lend_type,
//                  lt.equiplendtype_description            as lend_type_desc,
//                  sum(me.merchequip_equip_quantity)       as equip_count,
//                  avg(me.merchequip_amount)               as per_item
//          from    merchequipment    me,
//                  equipment         eq,
//                  equiplendtype     lt,
//                  prodoption        po
//          where   me.app_seq_num = :AppSeqNum and
//                  -- all of the owned equipment except imprinters
//                  me.equiplendtype_code  = :mesConstants.APP_EQUIP_OWNED and
//                  me.equiptype_code <> :mesConstants.APP_EQUIP_TYPE_IMPRINTER and
//                  eq.equip_model = me.equip_model and
//                  po.prod_option_id(+) = me.prod_option_id and
//                  lt.equiplendtype_code = me.equiplendtype_code
//          group by me.equiplendtype_code, lt.equiplendtype_description
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  null                                    as equip_model,\n                'Equipment Support Fee'                 as equip_desc,\n                me.equiplendtype_code                   as lend_type,\n                lt.equiplendtype_description            as lend_type_desc,\n                sum(me.merchequip_equip_quantity)       as equip_count,\n                avg(me.merchequip_amount)               as per_item\n        from    merchequipment    me,\n                equipment         eq,\n                equiplendtype     lt,\n                prodoption        po\n        where   me.app_seq_num =  :1  and\n                -- all of the owned equipment except imprinters\n                me.equiplendtype_code  =  :2  and\n                me.equiptype_code <>  :3  and\n                eq.equip_model = me.equip_model and\n                po.prod_option_id(+) = me.prod_option_id and\n                lt.equiplendtype_code = me.equiplendtype_code\n        group by me.equiplendtype_code, lt.equiplendtype_description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_OWNED);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_TYPE_IMPRINTER);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.app.foothill.FhPricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:707^7*/
      resultSet = it.getResultSet();

      if(resultSet.next())
      {
        EquipmentFees.addElement( new EquipmentFee( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry("initEquipmentFees()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }

  // indexes into the QuestionFields string array
  public static final int Q_FIELD_NAME  = 0;
  public static final int Q_TEXT        = 1;

  protected static String[][] QuestionFields =
  {
      // field name           // question
    { "qNameMatch",           "Did name posted at business match business name on application?" },
    { "qSignage",             "Did location appear to have appropriate signage?" },
    { "qHoursPosted",         "Were business hours posted?" },
    { "qInventoryReview",     "Was merchant's inventory viewed?" },
    { "qInventoryConsistent", "Was inventory consistent with merchant's type of business?" },
    { "qInventoryAdequate",   "Did inventory appear to be adequate to support the sales volume indicated on the application?" }
  };

  public void init( )
  {
    super.init();

    try
    {

      // 1. v/mc rates and fees

      // main v/mc pricing plan options
      fields.add(new RadioButtonField("pricingPlan",PricePlanRadioButtons,-1,
        false,"Please select a Pricing Scenario"));

      // pricing plan options
      Condition cIsFixedRate
        = new FieldValueCondition(fields.getField("pricingPlan"),
            Integer.toString(mesConstants.APP_PS_FIXED_PLUS_PER_ITEM));
      fields.add(new DiscountField("discRate",false));
      fields.getField("discRate").setOptionalCondition(cIsFixedRate);
      fields.add(new SmallCurrencyField("perItem",5,6,false));
      fields.getField("perItem").setOptionalCondition(cIsFixedRate);

      // min monthly discount
      Field field = new CheckboxField("minDiscountEnabled",
        "Minimum Monthly Discount (V/MC)",true);
      fields.add(field);
      Field minAmount = new CurrencyField("minDiscountAmount",6,8,true);
      minAmount.addValidation(new IfYesNotBlankValidation(field,
        "Please provide a minimum monthly discount amount"));
      minAmount.setData("25.00");
      fields.add(minAmount);

      // bet pricing options
      fields.add(new DropDownField("pricingType",new PricingTypeTable(),false));

      // 2. other payment type fees (card type fees)

      // add accepted card types per item fees
      initCardFees();
      
      // 3. miscellaneous fees

      // setup the miscellaneous fees fields
      initMiscCharges();

      // 4. equipment/software

      // setup equipment fee fields
      initEquipmentFees();
      fields.add( new NumberField( "taxRate", 4, 8, true, 2 ) );

      // 5. pricing comments

      fields.add( new TextareaField( "pricingComments", 400, 7, 50, true ) );

      // 6. site information

      // add the site inspection section
      fields.add( new DropDownField("locationType",new LocationTypeTable(),true) );
      fields.add( new Field( "locationDesc", 100, 37, true ) );
      fields.getField("locationDesc").addValidation( 
        new LocationTypeValidation( fields.getField("locationType") ) );
      fields.add( new Field( "locationAddr", 50, 35, true ) );
      fields.add( new Field( "locationCity", 50, 35, true ) );
      fields.add( new DropDownField("locationState", new StateDropDownTable(), true ) );
      fields.add( new ZipField( "locationZip", true, fields.getField("locationState") ) );
      field = new DropDownField("locationAddrType",new LocationAddressTable(), true);
      field.addValidation( new LocationAddressValidation( fields ) );
      fields.add(field);

      fields.add( new NumberField( "employeeCount", 6, 3, true, 0 ) );

      for( int i = 0; i < QuestionFields.length; ++i )
      {
        fields.add(new DropDownField(QuestionFields[i][Q_FIELD_NAME],new YesNoTable(),true));
      }

      // 7. e-commerce/moto info

      fields.add( new Field( "inventoryAddr", 50, 35, true ) );
      fields.add( new Field( "inventoryCity", 50, 35, true ) );
      fields.add( new DropDownField("inventoryState", new StateDropDownTable(), true ) );
      fields.add( new ZipField( "inventoryZip", true, fields.getField("inventoryState") ) );

      fields.add( new CurrencyField( "inventoryValue", 12, 12, true ) );

      fields.add( new Field( "fulfillmentName", 100, 35, true ) );
      fields.add( new Field( "fulfillmentAddr", 50, 35, true ) );
      fields.add( new Field( "fulfillmentCity", 50, 35, true ) );
      fields.add( new DropDownField("fulfillmentState", new StateDropDownTable(), true ) );
      fields.add( new ZipField( "fulfillmentZip", true, fields.getField("fulfillmentState") ) );
      field = new DropDownField( "fulfillmentHouse", new YesNoTable(), true );
      field.addValidation( new FulfillmentHouseValidation( fields ) );
      fields.add(field);

      fields.add( new DropDownField("securitySoftware", new YesNoTable(), true ) );
      field = new Field( "securitySoftwareVendor", 100, 35, true );
      field.addValidation( new IfYesNotBlankValidation( fields.getField("securitySoftware"), "Please specify the name of the security software used at this location" ) );
      fields.add(field);

      addHtmlExtra("class=\"formFields\"");
    }
    catch( Exception e )
    {
      logEntry("init()",e.toString());
    }
    finally
    {
    }
  }

  /*
  ** protected void loadPricingPlan()
  **
  ** Load v/mc specific pricing info.
  */
  protected void loadPricingPlan()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      // load the pricing type
      /*@lineinfo:generated-code*//*@lineinfo:866^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bet_type_code as pricing_type
//          from    merchant
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bet_type_code as pricing_type\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.app.foothill.FhPricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:871^7*/
      setFields(it.getResultSet());
      it.close();

      // load v/mc pricing
      /*@lineinfo:generated-code*//*@lineinfo:876^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tranchrg_disc_rate                as disc_rate,
//                  tranchrg_pass_thru                as per_item,
//                  tranchrg_discrate_type            as pricing_plan,
//                  tranchrg_mmin_chrg                as min_discount_amount,
//                  decode( tranchrg_mmin_chrg,null,'n',0,'n','y')
//                                                    as min_discount_enabled
//  
//          from    tranchrg
//  
//          where   app_seq_num   = :AppSeqNum and
//                  cardtype_code = :mesConstants.APP_CT_VISA
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tranchrg_disc_rate                as disc_rate,\n                tranchrg_pass_thru                as per_item,\n                tranchrg_discrate_type            as pricing_plan,\n                tranchrg_mmin_chrg                as min_discount_amount,\n                decode( tranchrg_mmin_chrg,null,'n',0,'n','y')\n                                                  as min_discount_enabled\n\n        from    tranchrg\n\n        where   app_seq_num   =  :1  and\n                cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.app.foothill.FhPricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:889^7*/
      rs = it.getResultSet();

      // set the disc rate and per item fields corresponding with pricing plan
      if (rs.next())
      {
       // set perItem and discRate
        setFields(rs,false);
      }
      
      // make sure 0 or null min disc amt overrides default value
      if (rs.getFloat("min_discount_amount") == 0)
      {
        fields.setData("minDiscountAmount","");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadPricingPlan()",e.toString());
      addError("loadPricingPlan: " + e.toString());
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** protected void loadNonBankFees()
  **
  ** Loads per item fees associated with non-v/mc card types.
  */
  protected void loadNonBankFees()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      // select all per item fees
      /*@lineinfo:generated-code*//*@lineinfo:933^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  po.cardtype_code      as card_type,
//                  tc.tranchrg_per_tran  as per_item
//                  
//          from    merchpayoption  po,
//                  tranchrg        tc,
//                  cardtype        ct
//                  
//          where   po.app_seq_num = :AppSeqNum
//                  and po.cardtype_code in
//                    ( :mesConstants.APP_CT_DINERS_CLUB,
//                      :mesConstants.APP_CT_DISCOVER,
//                      :mesConstants.APP_CT_JCB,
//                      :mesConstants.APP_CT_AMEX,
//                      :mesConstants.APP_CT_DEBIT,
//                      :mesConstants.APP_CT_CHECK_AUTH,
//                      :mesConstants.APP_CT_INTERNET,
//                      :mesConstants.APP_CT_DIAL_PAY,
//                      :mesConstants.APP_CT_EBT )
//                  and tc.app_seq_num(+)   = po.app_seq_num
//                  and tc.cardtype_code(+) = po.cardtype_code
//                  and ct.cardtype_code(+) = po.cardtype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  po.cardtype_code      as card_type,\n                tc.tranchrg_per_tran  as per_item\n                \n        from    merchpayoption  po,\n                tranchrg        tc,\n                cardtype        ct\n                \n        where   po.app_seq_num =  :1 \n                and po.cardtype_code in\n                  (  :2 ,\n                     :3 ,\n                     :4 ,\n                     :5 ,\n                     :6 ,\n                     :7 ,\n                     :8 ,\n                     :9 ,\n                     :10  )\n                and tc.app_seq_num(+)   = po.app_seq_num\n                and tc.cardtype_code(+) = po.cardtype_code\n                and ct.cardtype_code(+) = po.cardtype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_DINERS_CLUB);
   __sJT_st.setInt(3,mesConstants.APP_CT_DISCOVER);
   __sJT_st.setInt(4,mesConstants.APP_CT_JCB);
   __sJT_st.setInt(5,mesConstants.APP_CT_AMEX);
   __sJT_st.setInt(6,mesConstants.APP_CT_DEBIT);
   __sJT_st.setInt(7,mesConstants.APP_CT_CHECK_AUTH);
   __sJT_st.setInt(8,mesConstants.APP_CT_INTERNET);
   __sJT_st.setInt(9,mesConstants.APP_CT_DIAL_PAY);
   __sJT_st.setInt(10,mesConstants.APP_CT_EBT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.app.foothill.FhPricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:956^7*/
      rs = it.getResultSet();

      // load fields
      while( rs.next() )
      {
        Field field = fields.getField("perItemFee" + rs.getString("card_type"));
        if (field != null)
        {
          // only override default per item setting if a per item
          // value actually exists
          String perItem = Double.toString(rs.getDouble("per_item"));
          if (perItem != null)
          {
            field.setData(perItem);
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadNonBankFees()",e.toString());
      addError("loadNonBankFees: " + e.toString());
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** protected void loadMiscFees()
  **
  ** Load all miscellaneous fees.
  */
  protected void loadMiscFees()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      // load charges from database
      /*@lineinfo:generated-code*//*@lineinfo:1000^7*/

//  ************************************************************
//  #sql [Ctx] it = { select misc_code                  misc_code,
//                 misc_chrg_amount           charge_amount,
//                 decode(misc_chrg_amount,
//                        null,'n',0,'n','y') charge_enable
//                        
//          from   miscchrg
//          
//          where  app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select misc_code                  misc_code,\n               misc_chrg_amount           charge_amount,\n               decode(misc_chrg_amount,\n                      null,'n',0,'n','y') charge_enable\n                      \n        from   miscchrg\n        \n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.app.foothill.FhPricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1010^7*/
      rs = it.getResultSet();

      // set the charge info in the fields
      boolean defaultsCleared = false;
      int[] miscCharges = getMiscCharges();
      while(rs.next())
      {
        // clear out any default field data
        if (!defaultsCleared)
        {
          for (int i = 0; i < miscCharges.length; ++i)
          {
            fields.setData("miscChargeEnable" + miscCharges[i],"n");
            fields.setData("miscChargeAmount" + miscCharges[i],"");
          }
          defaultsCleared = true;
        }
      
        int miscCode = rs.getInt("misc_code");
        for( int i = 0; i < miscCharges.length; ++i )
        {
          if ( miscCode == miscCharges[i] )
          {
            String chargeAmount = Float.toString(rs.getFloat("charge_amount"));
            fields.setData("miscChargeEnable" + miscCode,
                            rs.getString("charge_enable"));
            fields.setData("miscChargeAmount" + miscCode,chargeAmount);
            break;
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadMiscFees()",e.toString());
      addError("loadMiscFees: " + e.toString());
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** protected void loadSiteInspection()
  **
  ** Loads site inspection data.
  */
  protected void loadSiteInspection()
  {
    ResultSetIterator         it          = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1066^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  siteinsp_comment                  as pricing_comments,
//                  siteinsp_name_flag                as q_name_match,
//                  siteinsp_inv_sign_flag            as q_signage,
//                  siteinsp_bus_hours_flag           as q_hours_posted,
//                  siteinsp_inv_viewed_flag          as q_inventory_review,
//                  siteinsp_inv_consistant_flag      as q_inventory_consistent,
//                  siteinsp_vol_flag                 as q_inventory_adequate,
//                  siteinsp_no_of_emp                as employee_count,
//                  siteinsp_inv_street               as inventory_addr,
//                  siteinsp_inv_city                 as inventory_city,
//                  siteinsp_inv_state                as inventory_state,
//                  siteinsp_inv_zip                  as inventory_zip,
//                  siteinsp_bus_loc                  as location_type,
//                  siteinsp_bus_loc_comment          as location_desc,
//                  siteinsp_bus_address              as location_addr_type,
//                  siteinsp_bus_street               as location_addr,
//                  siteinsp_bus_city                 as location_city,
//                  siteinsp_bus_state                as location_state,
//                  siteinsp_bus_zip                  as location_zip,
//                  siteinsp_inv_value                as inventory_value,
//                  siteinsp_full_flag                as fulfillment_house,
//                  siteinsp_full_name                as fulfillment_name,
//                  siteinsp_full_street              as fulfillment_addr,
//                  siteinsp_full_city                as fulfillment_city,
//                  siteinsp_full_state               as fulfillment_state,
//                  siteinsp_full_zip                 as fulfillment_zip,
//                  siteinsp_soft_flag                as security_software,
//                  siteinsp_soft_name                as security_software_vendor
//          from    siteinspection            si
//          where   si.app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  siteinsp_comment                  as pricing_comments,\n                siteinsp_name_flag                as q_name_match,\n                siteinsp_inv_sign_flag            as q_signage,\n                siteinsp_bus_hours_flag           as q_hours_posted,\n                siteinsp_inv_viewed_flag          as q_inventory_review,\n                siteinsp_inv_consistant_flag      as q_inventory_consistent,\n                siteinsp_vol_flag                 as q_inventory_adequate,\n                siteinsp_no_of_emp                as employee_count,\n                siteinsp_inv_street               as inventory_addr,\n                siteinsp_inv_city                 as inventory_city,\n                siteinsp_inv_state                as inventory_state,\n                siteinsp_inv_zip                  as inventory_zip,\n                siteinsp_bus_loc                  as location_type,\n                siteinsp_bus_loc_comment          as location_desc,\n                siteinsp_bus_address              as location_addr_type,\n                siteinsp_bus_street               as location_addr,\n                siteinsp_bus_city                 as location_city,\n                siteinsp_bus_state                as location_state,\n                siteinsp_bus_zip                  as location_zip,\n                siteinsp_inv_value                as inventory_value,\n                siteinsp_full_flag                as fulfillment_house,\n                siteinsp_full_name                as fulfillment_name,\n                siteinsp_full_street              as fulfillment_addr,\n                siteinsp_full_city                as fulfillment_city,\n                siteinsp_full_state               as fulfillment_state,\n                siteinsp_full_zip                 as fulfillment_zip,\n                siteinsp_soft_flag                as security_software,\n                siteinsp_soft_name                as security_software_vendor\n        from    siteinspection            si\n        where   si.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.app.foothill.FhPricing",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1098^7*/
      setFields(it.getResultSet());
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadSiteInspection()",e.toString());
      addError("loadSiteInspection: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) { }
    }
  }

  /*
  ** public void loadData()
  **
  ** Loads all field data.
  */
  public void loadData()
  {
    try
    {
      loadPricingPlan();
      loadNonBankFees();
      loadMiscFees();
      loadSiteInspection();
      
      // equipment data is loaded during field initialization
    }
    catch( Exception e )
    {
      logEntry("loadData()",e.toString());
    }
    finally
    {
    }
  }

  /*
  ** protected void storeTransactionFees()
  **
  ** Stores all transaction fees, including v/mc pricing info and all non-v/mc
  ** card per item fees.
  */
  protected void storeTransactionFees()
  {
    try
    {
      
      // set bet type code and pricing grid
      Field pt = fields.getField("pricingType");
      /*@lineinfo:generated-code*//*@lineinfo:1151^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     pricing_grid  = :pt.getData(),
//                  bet_type_code = :pt.getData()
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2896 = pt.getData();
 String __sJT_2897 = pt.getData();
   String theSqlTS = "update  merchant\n        set     pricing_grid  =  :1 ,\n                bet_type_code =  :2 \n        where   app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2896);
   __sJT_st.setString(2,__sJT_2897);
   __sJT_st.setLong(3,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1157^7*/
      
      // clear tranchrg records
      /*@lineinfo:generated-code*//*@lineinfo:1160^7*/

//  ************************************************************
//  #sql [Ctx] { delete from tranchrg
//          where app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from tranchrg\n        where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1164^7*/
      
      // store v/mc pricing
      /*@lineinfo:generated-code*//*@lineinfo:1167^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran
//          )
//          values
//          (
//            :AppSeqNum,
//            :mesConstants.APP_CT_VISA,
//            :fields.getData("pricingPlan"),
//            :fields.getData("discRate"),
//            :fields.getData("perItem"),
//            :fields.getData("minDiscountAmount"),
//            0
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2898 = fields.getData("pricingPlan");
 String __sJT_2899 = fields.getData("discRate");
 String __sJT_2900 = fields.getData("perItem");
 String __sJT_2901 = fields.getData("minDiscountAmount");
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   __sJT_st.setString(3,__sJT_2898);
   __sJT_st.setString(4,__sJT_2899);
   __sJT_st.setString(5,__sJT_2900);
   __sJT_st.setString(6,__sJT_2901);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1189^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1191^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          (
//            app_seq_num,
//            cardtype_code,
//            tranchrg_discrate_type,
//            tranchrg_disc_rate,
//            tranchrg_pass_thru,
//            tranchrg_mmin_chrg,
//            tranchrg_per_tran
//          )
//          values
//          (
//            :AppSeqNum,
//            :mesConstants.APP_CT_MC,
//            :fields.getData("pricingPlan"),
//            :fields.getData("discRate"),
//            :fields.getData("perItem"),
//            :fields.getData("minDiscountAmount"),
//            0
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2902 = fields.getData("pricingPlan");
 String __sJT_2903 = fields.getData("discRate");
 String __sJT_2904 = fields.getData("perItem");
 String __sJT_2905 = fields.getData("minDiscountAmount");
   String theSqlTS = "insert into tranchrg\n        (\n          app_seq_num,\n          cardtype_code,\n          tranchrg_discrate_type,\n          tranchrg_disc_rate,\n          tranchrg_pass_thru,\n          tranchrg_mmin_chrg,\n          tranchrg_per_tran\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          0\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_MC);
   __sJT_st.setString(3,__sJT_2902);
   __sJT_st.setString(4,__sJT_2903);
   __sJT_st.setString(5,__sJT_2904);
   __sJT_st.setString(6,__sJT_2905);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1213^7*/

      // store additional card type pricing
      for (int ct = 0; ct < mesConstants.APP_CT_COUNT; ++ct)
      {
        Field field = fields.getField(("perItemFee" + ct));
        if (field != null)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1221^11*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//              (
//                app_seq_num,
//                cardtype_code,
//                tranchrg_per_tran
//              )
//              values
//              (                                             
//                :AppSeqNum,
//                :ct,
//                :field.getData()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2906 = field.getData();
   String theSqlTS = "insert into tranchrg\n            (\n              app_seq_num,\n              cardtype_code,\n              tranchrg_per_tran\n            )\n            values\n            (                                             \n               :1 ,\n               :2 ,\n               :3 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,ct);
   __sJT_st.setString(3,__sJT_2906);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1235^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("storeTransactionFees()", e.toString());
      addError("storeTransactionFees: " + e.toString());
    }
  }

  /*
  ** protected void storeMiscFees()
  **
  ** Stores all miscellaneous fees.
  */
  protected void storeMiscFees()
  {
    try
    {
      for (int mc = 0; mc < mesConstants.APP_MISC_CHARGE_COUNT; ++mc)
      {
        // clear this fee type from db
        /*@lineinfo:generated-code*//*@lineinfo:1258^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from   miscchrg
//            where  app_seq_num = :AppSeqNum and
//                   misc_code = :mc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from   miscchrg\n          where  app_seq_num =  :1  and\n                 misc_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mc);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1264^9*/

        // look for this fee type's field
        Field field = fields.getField("miscChargeEnable" + mc);
        if (field != null && field.getData().toUpperCase().equals("Y"))
        {
          // found the fee, so store it
          /*@lineinfo:generated-code*//*@lineinfo:1271^11*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//              (
//                app_seq_num,
//                misc_code,
//                misc_chrg_amount
//              )
//              values
//              (
//                :AppSeqNum,
//                :mc,
//                :fields.getData("miscChargeAmount" + mc)
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2907 = fields.getData("miscChargeAmount" + mc);
   String theSqlTS = "insert into miscchrg\n            (\n              app_seq_num,\n              misc_code,\n              misc_chrg_amount\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setInt(2,mc);
   __sJT_st.setString(3,__sJT_2907);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1285^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("storeMiscFees()",e.toString());
      addError("storeMiscFees: " + e.toString());
    }
    finally
    {
    }
  }

  /*
  ** protected void storeEquipmentFees()
  **
  ** Stores all equipment fee info.
  */
  protected void storeEquipmentFees()
  {
    EquipmentFee            equipFee        = null;
    Field                   field           = null;

    try
    {
      for( int i = 0; i < EquipmentFees.size(); ++i )
      {
        // extract the current equipment fee and corresponding field
        equipFee      = (EquipmentFee) EquipmentFees.elementAt(i);
        field         = fields.getField( equipFee.getFieldName() );

        /*@lineinfo:generated-code*//*@lineinfo:1317^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchequipment me
//            set     me.merchequip_amount = :field.getData()
//            where   me.app_seq_num = :AppSeqNum and
//                    me.equip_model =
//                      -- if the equipment model is null
//                      -- and the lend type is owned then
//                      -- force a match so that all owned
//                      -- equipment gets assigned the same amount
//                      decode( :equipFee.EquipModel,
//                              null, decode( :equipFee.LendType,
//                                            :mesConstants.APP_EQUIP_OWNED, me.equip_model,
//                                            'NONE' ),
//                              :equipFee.EquipModel ) and
//                    me.equiplendtype_code = :equipFee.LendType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2908 = field.getData();
   String theSqlTS = "update  merchequipment me\n          set     me.merchequip_amount =  :1 \n          where   me.app_seq_num =  :2  and\n                  me.equip_model =\n                    -- if the equipment model is null\n                    -- and the lend type is owned then\n                    -- force a match so that all owned\n                    -- equipment gets assigned the same amount\n                    decode(  :3 ,\n                            null, decode(  :4 ,\n                                           :5 , me.equip_model,\n                                          'NONE' ),\n                             :6  ) and\n                  me.equiplendtype_code =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_2908);
   __sJT_st.setLong(2,AppSeqNum);
   __sJT_st.setString(3,equipFee.EquipModel);
   __sJT_st.setInt(4,equipFee.LendType);
   __sJT_st.setInt(5,mesConstants.APP_EQUIP_OWNED);
   __sJT_st.setString(6,equipFee.EquipModel);
   __sJT_st.setInt(7,equipFee.LendType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1333^9*/
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("storeEquipmentFees()",e.toString());
    }
    finally
    {
    }
  }

  /*
  ** protected void storeSiteInspection()
  **
  ** Stores site inspection info.
  */
  protected void storeSiteInspection( )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1354^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    siteinspection
//          where   app_seq_num = :AppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    siteinspection\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1359^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1361^7*/

//  ************************************************************
//  #sql [Ctx] { insert into siteinspection
//          (
//            app_seq_num,
//            siteinsp_comment,
//            siteinsp_name_flag,
//            siteinsp_inv_sign_flag,
//            siteinsp_bus_hours_flag,
//            siteinsp_inv_viewed_flag,
//            siteinsp_inv_consistant_flag,
//            siteinsp_vol_flag,
//            siteinsp_full_flag,
//            siteinsp_soft_flag,
//            siteinsp_inv_street,
//            siteinsp_inv_city,
//            siteinsp_inv_state,
//            siteinsp_inv_zip,
//            siteinsp_full_street,
//            siteinsp_full_city,
//            siteinsp_full_state,
//            siteinsp_full_zip,
//            siteinsp_bus_street,
//            siteinsp_bus_city,
//            siteinsp_bus_state,
//            siteinsp_bus_zip,
//            siteinsp_inv_value,
//            siteinsp_full_name,
//            siteinsp_no_of_emp,
//            siteinsp_bus_loc,
//            siteinsp_bus_loc_comment,
//            siteinsp_bus_address,
//            siteinsp_soft_name
//          )
//          values
//          (
//            :AppSeqNum,
//            :fields.getField("pricingComments").getData(),
//            :fields.getField("qNameMatch").getData().toUpperCase(),
//            :fields.getField("qSignage").getData().toUpperCase(),
//            :fields.getField("qHoursPosted").getData().toUpperCase(),
//            :fields.getField("qInventoryReview").getData().toUpperCase(),
//            :fields.getField("qInventoryConsistent").getData().toUpperCase(),
//            :fields.getField("qInventoryAdequate").getData().toUpperCase(),
//            :fields.getField("fulfillmentHouse").getData().toUpperCase(),
//            :fields.getField("securitySoftware").getData().toUpperCase(),
//            :fields.getField("inventoryAddr").getData(),
//            :fields.getField("inventoryCity").getData(),
//            :fields.getField("inventoryState").getData(),
//            :fields.getField("inventoryZip").getData(),
//            :fields.getField("fulfillmentAddr").getData(),
//            :fields.getField("fulfillmentCity").getData(),
//            :fields.getField("fulfillmentState").getData(),
//            :fields.getField("fulfillmentZip").getData(),
//            :fields.getField("locationAddr").getData(),
//            :fields.getField("locationCity").getData(),
//            :fields.getField("locationState").getData(),
//            :fields.getField("locationZip").getData(),
//            :fields.getField("inventoryValue").getData(),
//            :fields.getField("fulfillmentName").getData(),
//            :fields.getField("employeeCount").getData(),
//            :fields.getField("locationType").getData(),
//            :fields.getField("locationDesc").getData(),
//            :fields.getField("locationAddrType").getData(),
//            :fields.getField("securitySoftwareVendor").getData()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_2909 = fields.getField("pricingComments").getData();
 String __sJT_2910 = fields.getField("qNameMatch").getData().toUpperCase();
 String __sJT_2911 = fields.getField("qSignage").getData().toUpperCase();
 String __sJT_2912 = fields.getField("qHoursPosted").getData().toUpperCase();
 String __sJT_2913 = fields.getField("qInventoryReview").getData().toUpperCase();
 String __sJT_2914 = fields.getField("qInventoryConsistent").getData().toUpperCase();
 String __sJT_2915 = fields.getField("qInventoryAdequate").getData().toUpperCase();
 String __sJT_2916 = fields.getField("fulfillmentHouse").getData().toUpperCase();
 String __sJT_2917 = fields.getField("securitySoftware").getData().toUpperCase();
 String __sJT_2918 = fields.getField("inventoryAddr").getData();
 String __sJT_2919 = fields.getField("inventoryCity").getData();
 String __sJT_2920 = fields.getField("inventoryState").getData();
 String __sJT_2921 = fields.getField("inventoryZip").getData();
 String __sJT_2922 = fields.getField("fulfillmentAddr").getData();
 String __sJT_2923 = fields.getField("fulfillmentCity").getData();
 String __sJT_2924 = fields.getField("fulfillmentState").getData();
 String __sJT_2925 = fields.getField("fulfillmentZip").getData();
 String __sJT_2926 = fields.getField("locationAddr").getData();
 String __sJT_2927 = fields.getField("locationCity").getData();
 String __sJT_2928 = fields.getField("locationState").getData();
 String __sJT_2929 = fields.getField("locationZip").getData();
 String __sJT_2930 = fields.getField("inventoryValue").getData();
 String __sJT_2931 = fields.getField("fulfillmentName").getData();
 String __sJT_2932 = fields.getField("employeeCount").getData();
 String __sJT_2933 = fields.getField("locationType").getData();
 String __sJT_2934 = fields.getField("locationDesc").getData();
 String __sJT_2935 = fields.getField("locationAddrType").getData();
 String __sJT_2936 = fields.getField("securitySoftwareVendor").getData();
   String theSqlTS = "insert into siteinspection\n        (\n          app_seq_num,\n          siteinsp_comment,\n          siteinsp_name_flag,\n          siteinsp_inv_sign_flag,\n          siteinsp_bus_hours_flag,\n          siteinsp_inv_viewed_flag,\n          siteinsp_inv_consistant_flag,\n          siteinsp_vol_flag,\n          siteinsp_full_flag,\n          siteinsp_soft_flag,\n          siteinsp_inv_street,\n          siteinsp_inv_city,\n          siteinsp_inv_state,\n          siteinsp_inv_zip,\n          siteinsp_full_street,\n          siteinsp_full_city,\n          siteinsp_full_state,\n          siteinsp_full_zip,\n          siteinsp_bus_street,\n          siteinsp_bus_city,\n          siteinsp_bus_state,\n          siteinsp_bus_zip,\n          siteinsp_inv_value,\n          siteinsp_full_name,\n          siteinsp_no_of_emp,\n          siteinsp_bus_loc,\n          siteinsp_bus_loc_comment,\n          siteinsp_bus_address,\n          siteinsp_soft_name\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n           :16 ,\n           :17 ,\n           :18 ,\n           :19 ,\n           :20 ,\n           :21 ,\n           :22 ,\n           :23 ,\n           :24 ,\n           :25 ,\n           :26 ,\n           :27 ,\n           :28 ,\n           :29 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.app.foothill.FhPricing",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AppSeqNum);
   __sJT_st.setString(2,__sJT_2909);
   __sJT_st.setString(3,__sJT_2910);
   __sJT_st.setString(4,__sJT_2911);
   __sJT_st.setString(5,__sJT_2912);
   __sJT_st.setString(6,__sJT_2913);
   __sJT_st.setString(7,__sJT_2914);
   __sJT_st.setString(8,__sJT_2915);
   __sJT_st.setString(9,__sJT_2916);
   __sJT_st.setString(10,__sJT_2917);
   __sJT_st.setString(11,__sJT_2918);
   __sJT_st.setString(12,__sJT_2919);
   __sJT_st.setString(13,__sJT_2920);
   __sJT_st.setString(14,__sJT_2921);
   __sJT_st.setString(15,__sJT_2922);
   __sJT_st.setString(16,__sJT_2923);
   __sJT_st.setString(17,__sJT_2924);
   __sJT_st.setString(18,__sJT_2925);
   __sJT_st.setString(19,__sJT_2926);
   __sJT_st.setString(20,__sJT_2927);
   __sJT_st.setString(21,__sJT_2928);
   __sJT_st.setString(22,__sJT_2929);
   __sJT_st.setString(23,__sJT_2930);
   __sJT_st.setString(24,__sJT_2931);
   __sJT_st.setString(25,__sJT_2932);
   __sJT_st.setString(26,__sJT_2933);
   __sJT_st.setString(27,__sJT_2934);
   __sJT_st.setString(28,__sJT_2935);
   __sJT_st.setString(29,__sJT_2936);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1427^7*/
    }
    catch(Exception e)
    {
      logEntry( "storeSiteInspection()", e.toString());
      addError( "storeSiteInspection: " + e.toString());
    }
  }

  /*
  ** public void storeData()
  **
  ** Stores all field data.
  */
  public void storeData()
  {
    try
    {
      storeTransactionFees();
      storeMiscFees();
      storeEquipmentFees();
      storeSiteInspection();
      markPageComplete();
    }
    catch( Exception e )
    {
      logEntry("storeData()",e.toString());
    }
  }

  public String[][] getQuestionList( )
  {
    return( QuestionFields );
  }

  public Vector getEquipmentFees()
  {
    return( EquipmentFees );
  }

  public int[] getMiscChargeList( )
  {
    return getMiscCharges();
  }
}/*@lineinfo:generated-code*/