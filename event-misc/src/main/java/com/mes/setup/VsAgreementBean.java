/*@lineinfo:filename=VsAgreementBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/VsAgreementBean.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 5/02/01 9:40a $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.setup;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

public class VsAgreementBean extends com.mes.database.SQLJDataBean
{
  private String merchantNumber = "";
  private String controlNumber  = "";
  private String dbaName        = "";
  private String ownerName      = "";
  private String ownerPhone     = "";
  private String contactName    = "";
  private String contactPhone   = "";
  private String agreement      = "";

  public boolean getAgreementData(long appSeqNum)
  {
    ResultSetIterator it        = null;
    ResultSet         rs        = null;
    boolean           result    = false;
    
    try
    {
      if(appSeqNum > 0L)
      {
        /*@lineinfo:generated-code*//*@lineinfo:53^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  merc.merch_number,merc.merc_cntrl_number,merc.merch_business_name,merc.merch_agreement,
//                    bus.busowner_first_name,bus.busowner_last_name,ad.address_phone,
//                    con.merchcont_prim_first_name,con.merchcont_prim_last_name,con.merchcont_prim_phone
//            from    merchant merc, businessowner bus, address ad, merchcontact con
//            where   merc.app_seq_num = bus.app_seq_num and
//                    merc.app_seq_num = ad.app_seq_num and
//                    merc.app_seq_num = con.app_seq_num and
//                    merc.app_seq_num = :appSeqNum and ad.addresstype_code = 4 and bus.busowner_num = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merc.merch_number,merc.merc_cntrl_number,merc.merch_business_name,merc.merch_agreement,\n                  bus.busowner_first_name,bus.busowner_last_name,ad.address_phone,\n                  con.merchcont_prim_first_name,con.merchcont_prim_last_name,con.merchcont_prim_phone\n          from    merchant merc, businessowner bus, address ad, merchcontact con\n          where   merc.app_seq_num = bus.app_seq_num and\n                  merc.app_seq_num = ad.app_seq_num and\n                  merc.app_seq_num = con.app_seq_num and\n                  merc.app_seq_num =  :1  and ad.addresstype_code = 4 and bus.busowner_num = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.setup.VsAgreementBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.setup.VsAgreementBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:63^9*/
        
        rs = it.getResultSet();

        if(rs.next())
        {
          merchantNumber = rs.getString("merch_number");
          controlNumber  = rs.getString("merc_cntrl_number");
          dbaName        = rs.getString("merch_business_name");
          ownerName      = (rs.getString("busowner_first_name") == null ? "" : rs.getString("busowner_first_name")) + " " + (rs.getString("busowner_last_name") == null ? "" : rs.getString("busowner_last_name"));
          ownerName      = ownerName.trim();
          ownerPhone     = rs.getString("address_phone");
          contactName    = (rs.getString("merchcont_prim_first_name") == null ? "" : rs.getString("merchcont_prim_first_name")) + " " + (rs.getString("merchcont_prim_last_name") == null ? "" : rs.getString("merchcont_prim_last_name"));
          contactName    = contactName.trim();
          contactPhone   = rs.getString("merchcont_prim_phone");
          agreement      = rs.getString("merch_agreement");
          result = true;
        }
        else
        {
          result = false;
        }
        it.close();
      }
      else
      {
        result = false; // no appSeqNum
      }
    }
    catch (Exception e)
    {
      logEntry("getAgreementData(" + appSeqNum + ")", e.toString());
      result = false;
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }


  public String getMerchantNumber()
  {
    String result = merchantNumber;
    if(isBlank(result))
    {
      result = "<font color=\"red\">Merchant Number Not Yet Assigned</font>";
    }
    return result;
  }
  public String getControlNumber()
  {
    String result = controlNumber;
    if(isBlank(result))
    {
      result = "<font color=\"red\">Control Number Error</font>";
    }
    return result;
  }
  public String getDbaName()
  {
    String result = dbaName;
    if(isBlank(result))
    {
      result = "<font color=\"red\">DBA Name Error</font>";
    }
    return result;
  }
  public String getOwnerName()
  {
    String result = ownerName;
    if(isBlank(result))
    {
      result = "<font color=\"red\">Owner Name Error</font>";
    }
    return result;
  }
  public String getOwnerPhone()
  {
    String result = ownerPhone;
    if(isBlank(result) || result.equals("0"))
    {
      result = "";
    }
    return result;
  }
  public String getContactName()
  {
    String result = contactName;
    if(isBlank(result))
    {
      result = "<font color=\"red\">Contact Name Error</font>";
    }
    return result;
  }
  public String getContactPhone()
  {
    String result = contactPhone;
    if(isBlank(result) || result.equals("0"))
    {
      result = "";
    }
    return result;
  }
  public String getAgreement()
  {
    String result = agreement;
    if(isBlank(result))
    {
      result = "N";
    }
    return result;
  }


}/*@lineinfo:generated-code*/