/*@lineinfo:filename=Address*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/Address.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/03/01 9:36a $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.setup;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

public class Address extends com.mes.database.SQLJConnection
{
  private static final int BUSINESS_ADDRESS = 1;
  private static final int MAILING_ADDRESS  = 2;

  private   String    businessLine1      =   "";
  private   String    businessLine2      =   "";
  private   String    businessCity       =   "";
  private   String    businessState      =   "";
  private   String    businessZip        =   "";
  private   String    businessZip4       =   "";
  private   String    businessCountry    =   "";
  private   String    businessPhone      =   "";

  private   String    mailingName        =   "";
  private   String    mailingLine1       =   "";
  private   String    mailingLine2       =   "";
  private   String    mailingCity        =   "";
  private   String    mailingState       =   "";
  private   String    mailingZip         =   "";
  private   String    mailingZip4        =   "";
  
  private   String    discBillingFlag    =   "0";
  private   String    rclBulletinFlag    =   "0";
  private   String    crbBulletinFlag    =   "0";
  private   String    cardMailerFlag     =   "0";
  private   String    callingCardFlag    =   "0";
  private   String    impRentalFlag      =   "0";
  private   String    membershipFeeFlag  =   "0";
  private   String    posTerminalFlag    =   "0";
  private   String    miscNameAddFlag    =   "0";
  private   String    uniqueMessageFlag  =   "0";
  private   String    adjustmentFlag     =   "0";
  private   String    chargebackFlag     =   "0";
  private   String    bet1Flag           =   "0";
  private   String    bet2Flag           =   "0";
  private   String    bet3Flag           =   "0";


  private   boolean   errorOccurred      =   false;

  public Address(long appSeqNum)
  {
    getAddresses(appSeqNum);
  }
  
  public void getAddresses(long appSeqNum)
  {
    ResultSetIterator     it              = null;
    ResultSet             rs              = null;
      
    try
    {
      // get the parent org and continue with it
      /*@lineinfo:generated-code*//*@lineinfo:84^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    address
//          where   app_seq_num = :appSeqNum
//          and (addresstype_code = :BUSINESS_ADDRESS or addresstype_code = :MAILING_ADDRESS)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    address\n        where   app_seq_num =  :1 \n        and (addresstype_code =  :2  or addresstype_code =  :3 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.setup.Address",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,BUSINESS_ADDRESS);
   __sJT_st.setInt(3,MAILING_ADDRESS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.setup.Address",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:90^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        switch(rs.getInt("addresstype_code"))
        {
          case BUSINESS_ADDRESS:
            businessLine1      =   rs.getString("address_line1");
            businessLine1      =   isBlank(businessLine1) ? "" : businessLine1;
            businessLine2      =   rs.getString("address_line2");
            businessLine2      =   isBlank(businessLine2) ? "" : businessLine2;
            businessCity       =   rs.getString("address_city");
            businessCity       =   isBlank(businessCity) ? "" : businessCity;
            businessState      =   rs.getString("countrystate_code");
            businessState      =   isBlank(businessState) ? "" : businessState;
            setZip(rs.getString("address_zip"), BUSINESS_ADDRESS);
            businessCountry    =   "US";
            businessPhone      =   rs.getString("address_phone");
            businessPhone      =   isBlank(businessPhone) ? "" : businessPhone;
          break;
          case MAILING_ADDRESS:
            System.out.println("mailing address has been reached");
            mailingLine1       =   rs.getString("address_line1");
            mailingLine1       =   isBlank(mailingLine1) ? "" : mailingLine1;
            System.out.println("mailing address 1 = " + mailingLine1);
            
            mailingLine2       =   rs.getString("address_line2");
            mailingLine2       =   isBlank(mailingLine2) ? "" : mailingLine2;
            System.out.println("mailing address 2 = " + mailingLine2);
            
            mailingCity        =   rs.getString("address_city");
            mailingCity        =   isBlank(mailingCity) ? "" : mailingCity;
            System.out.println("mailing city = " + mailingCity);
            
            mailingState       =   rs.getString("countrystate_code");
            mailingState       =   isBlank(mailingState) ? "" : mailingState;
            System.out.println("mailing State = " + mailingState);
            System.out.println("mailing zip = " + rs.getString("address_zip"));
            
            setZip(rs.getString("address_zip"), MAILING_ADDRESS);
          
          break;
        }
      }
      
      try
      {
        it.close();
      }
      catch(Exception e)
      {}
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::findClosestParent", e.toString());
      errorOccurred = true;
    }
    finally
    {
      cleanUp();
    }
  }

  private void setZip(String tempZip, int addressType)
  {
    String zip  = "";
    String zip4 = "0000";

    try
    {
      if(!isBlank(tempZip))
      {  
        if(tempZip.length() == 10 && tempZip.charAt(5) == '-')
        {
          zip   = tempZip.substring(0,5);
          zip   = isNumber(zip) ? zip : "";
          System.out.println("The zip = " + zip);
          zip4  = tempZip.substring(6);
          zip4  = isNumber(zip4) ? zip4 : "0000";
          System.out.println("The zip4 = " + zip4);
        }
        else if(tempZip.length() == 9 && isNumber(tempZip))
        {
          zip   = tempZip.substring(0,5);
          zip4  = tempZip.substring(5);
          System.out.println("The zip = " + zip);
          System.out.println("The zip4 = " + zip4);
        }
        else if(tempZip.length() == 5 && isNumber(tempZip))
        {
          zip   = tempZip;        
          System.out.println("The zip = " + zip);
          System.out.println("The zip4 = " + zip4);
        }
      }
      if(isBlank(zip))
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::setZip", "zip was blank - cant have that");
        errorOccurred = true;        
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::setZip", e.toString());
      errorOccurred = true;
    }

    switch(addressType)
    {
      case BUSINESS_ADDRESS:
        businessZip   = zip;
        businessZip4  = zip4;
      break;
      case MAILING_ADDRESS:
        mailingZip    = zip;
        mailingZip4   = zip4;
      break;
    }

  }

  public boolean isNumber(String test)
  {
    boolean pass = true;
    
    try
    {
      long num = Long.parseLong(test);
    }
    catch(Exception e)
    {
      pass = false;
    }
    
    return pass;
  }
  
  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }

  public void setMailingInfo(String mailingName)
  {
    if(!isBlank(mailingName))
    {
      mailingName = mailingName.trim();
    }

    if(isBlank(mailingName))
    {
      //make sure everything is blanked out
      mailingLine1       =   "";
      mailingLine2       =   "";
      mailingCity        =   "";
      mailingState       =   "";
      mailingZip         =   "";
      mailingZip4        =   "";
    }
    else
    {
      this.mailingName   = mailingName;

      //set flags accordingly
      discBillingFlag    =   "1";
      rclBulletinFlag    =   "1";
      crbBulletinFlag    =   "0";
      cardMailerFlag     =   "1";
      callingCardFlag    =   "1";
      impRentalFlag      =   "1";
      membershipFeeFlag  =   "1";
      posTerminalFlag    =   "1";
      miscNameAddFlag    =   "1";
      uniqueMessageFlag  =   "1";
      adjustmentFlag     =   "1";
      chargebackFlag     =   "1";
      bet1Flag           =   "1";
      bet2Flag           =   "1";
      bet3Flag           =   "1";
    }    
  }

  public boolean didErrorOccur()
  {
    return errorOccurred;
  }

  public String getBusinessLine1()
  {
    return businessLine1;
  }
  public String getBusinessLine2()
  {
    return businessLine2;
  }
  public String getBusinessCity()
  {
    return businessCity;
  }
  public String getBusinessState()
  {
    return businessState;
  }
  public String getBusinessZip()
  {
    return businessZip;
  }
  public String getBusinessZip4()
  {
    return businessZip4;
  }
  public String getBusinessCountry()
  {
    return businessCountry;
  }
  public String getBusinessPhone()
  {
    String result = "0000000000";
    if(isNumber(businessPhone))
    {
      result = businessPhone;
    }
    return result;
  }

  public String getMailingName()
  {
    return mailingName;
  }
  public String getMailingLine1()
  {
    return mailingLine1;
  }
  public String getMailingLine2()
  {
    return mailingLine2;
  }
  public String getMailingCity()
  {
    return mailingCity;
  }
  public String getMailingState()
  {
    return mailingState;
  }
  public String getMailingZip()
  {
    return mailingZip;
  }
  public String getMailingZip4()
  {
    return mailingZip4;
  }

  public String getDiscBillingFlag()
  {
    return discBillingFlag;
  }
  public String getRclBulletinFlag()
  {
    return rclBulletinFlag;
  }
  public String getCrbBulletinFlag()
  {
    return crbBulletinFlag;
  }
  public String getCardMailerFlag()
  {
    return cardMailerFlag;
  }
  public String getCallingCardFlag()
  {
    return callingCardFlag;
  }
  public String getImpRentalFlag()
  {
    return impRentalFlag;
  }
  public String getMembershipFeeFlag()
  {
    return membershipFeeFlag;
  }
  public String getPosTerminalFlag()
  {
    return posTerminalFlag;
  }
  public String getMiscNameAddFlag()
  {
    return miscNameAddFlag;
  }
  public String getUniqueMessageFlag()
  {
    return uniqueMessageFlag;
  }
  public String getAdjustmentFlag()
  {
    return adjustmentFlag;
  }
  public String getChargebackFlag()
  {
    return chargebackFlag;
  }
  public String getBet1Flag()
  {
    return bet1Flag;
  }
  public String getBet2Flag()
  {
    return bet2Flag;
  }
  public String getBet3Flag()
  {
    return bet3Flag;
  }

}/*@lineinfo:generated-code*/