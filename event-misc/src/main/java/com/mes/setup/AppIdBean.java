/*@lineinfo:filename=AppIdBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/AppIdBean.sqlj $

  Description:
    Base class for application data beans

    This class should maintain any data that is common to all of the
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.setup;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import com.mes.ops.QueueConstants;
import com.mes.screens.ScreenInfo;
import sqlj.runtime.ResultSetIterator;

public class AppIdBean extends com.mes.screens.SequenceIdBean
  implements Serializable
{
  public static final int APP_TYPE          = 1;
  public static final int OG_APP_TYPE       = 2;

  private long          appControlNum       = 0L;
  private ValidateUser  user                = null;
  private long          merchantNumber      = 0L;
  private int           merchCreditStatus   = 0;
  private String        appTypeDesc         = "";
  private String        appGif              = "";
  private int           appWidth            = 0;
  private int           appHeight           = 0;
  private int           appType             = -1;
  private String        appGifOg            = "";
  private int           appWidthOg          = 0;
  private int           appHeightOg         = 0;
  private int           originalAppType     = -1;
  private long          userSequenceType    = -1L;
  
  private String        sicCode             = "";
  private String        investigator        = "";
  private String        metTable            = "";
  private String        creditScore         = "";
  private String        tier2               = "N";
  private String        matchData           = "";

  public synchronized String getSicCode()
  {
    return this.sicCode;
  }
  
  public synchronized String getInvestigator()
  {
    return this.investigator;
  }
  
  public synchronized String getMetTable()
  {
    return this.metTable;
  }
  
  public synchronized String getCreditScore()
  {
    return this.creditScore;
  }
  
  public synchronized String getTier2()
  {
    return this.tier2;
  }
  
  public synchronized String getMatchData()
  {
    return this.matchData;
  }
  
  public synchronized void getCreditInfo()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      sicCode       = "";
      investigator  = "";
      metTable      = "";
      tier2         = "";
      creditScore   = "";
      
      /*@lineinfo:generated-code*//*@lineinfo:116^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.sic_code                                          sic_code,
//                  m.merch_met_table_number                            met_table,
//                  m.merch_invg_code                                   inv_code,
//                  decode(cs.score, null, 'NONE', to_char(cs.score))   score, 
//                  nvl(acad.tier_2, 'N')                               tier2,
//                  nvl(mr.response_action, ' ')                        match_resp
//          from    merchant  m,
//                  credit_scores cs,
//                  app_credit_additional_data acad,
//                  match_requests mr
//          where   m.app_seq_num = :getPrimaryKey() and
//                  m.app_seq_num = cs.app_seq_num(+) and
//                  m.app_seq_num = acad.app_seq_num(+) and
//                  m.app_seq_num = mr.app_seq_num(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1539 = getPrimaryKey();
  try {
   String theSqlTS = "select  m.sic_code                                          sic_code,\n                m.merch_met_table_number                            met_table,\n                m.merch_invg_code                                   inv_code,\n                decode(cs.score, null, 'NONE', to_char(cs.score))   score, \n                nvl(acad.tier_2, 'N')                               tier2,\n                nvl(mr.response_action, ' ')                        match_resp\n        from    merchant  m,\n                credit_scores cs,\n                app_credit_additional_data acad,\n                match_requests mr\n        where   m.app_seq_num =  :1  and\n                m.app_seq_num = cs.app_seq_num(+) and\n                m.app_seq_num = acad.app_seq_num(+) and\n                m.app_seq_num = mr.app_seq_num(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.setup.AppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1539);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.setup.AppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:132^7*/
      
      rs = it.getResultSet();
      
      StringBuffer  scores = new StringBuffer("");
      
      while(rs.next())
      {
        this.sicCode      = rs.getString("sic_code");
        this.investigator = rs.getString("inv_code");
        this.metTable     = rs.getString("met_table");
        this.tier2        = rs.getString("tier2");
        this.matchData    = rs.getString("match_resp");
        if(scores.length() > 1)
        {
          scores.append(", ");
        }
        scores.append(rs.getString("score"));
      }
      
      rs.close();
      it.close();
      
      this.creditScore = scores.toString();
    }
    catch(Exception e)
    {
      logEntry("getCreditInfo(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public synchronized String getAppTypeDesc(int type)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    String              result      = "";
    int                 useAppType  = 0;
    
    try
    {
      // get connection
      connect();
      
      useAppType = (type == APP_TYPE) ? this.appType : this.originalAppType;
      
      /*@lineinfo:generated-code*//*@lineinfo:183^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_name
//          from    org_app
//          where   app_type = :useAppType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_name\n        from    org_app\n        where   app_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.setup.AppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,useAppType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.setup.AppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:188^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        result = rs.getString("app_name") + " " + "Application";
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAppTypeDesc: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return result;
  }
  
  public synchronized String getPartnerInfo()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    StringBuffer        result  = new StringBuffer("");
    
    try
    {
      connect();
      
      // try to find partner information
      /*@lineinfo:generated-code*//*@lineinfo:224^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  vs.partner,
//                  vs.pid
//          from    vs_linking_table vs
//          where   app_seq_num = :getPrimaryKey()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1540 = getPrimaryKey();
  try {
   String theSqlTS = "select  vs.partner,\n                vs.pid\n        from    vs_linking_table vs\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.setup.AppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1540);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.setup.AppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:230^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        result.append("Partner: ");
        result.append(rs.getString("partner"));
        result.append(" (");
        result.append(rs.getString("pid"));
        result.append(")");
      }
      else
      {
        result.append("&nbsp;");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getPartnerInfo()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result.toString();
  }
  
  
  /*
  ** METHOD getCurPage(called when page first loads and if order is enforced)
     this method determines if page person is trying to enter is ok to be view, i.e.,
     is it the next page in the sequence...
     is it a page lower in the sequence than the last page completed...
     if its ok to be viewed it will allow them to view it, but
     if it isn't they are redirected to the first page that is ok for them to view
  */
  
  public synchronized String getCurPage(long screenId)
  {
    long lastScreen     = 1L;
    long lastScreenTemp = 1L;
    String result = "";
    
    if(!user.isValidUser() || !user.isRecordExists())
    {
      user.resetFlag();
      return "/jsp/setup/invaliduser.jsp";  
    }
    
    if(screenId != 1L && getCompletedScreens() == 0L)
    {
      return getFirstPage();
    }
    else if(screenId == 1L && getCompletedScreens() == 0L)
    {
      return "";
    }

    while(true)
    {
      lastScreenTemp = lastScreenTemp << 1;
      
      if((lastScreenTemp & getCompletedScreens()) == 0)
      {
        break;
      }
      
      lastScreen = lastScreen << 1;
    }
    
    if(lastScreen < screenId)
    {
      result = getNextPage(lastScreen);
      if (result.equals(getPage(screenId)))
      {
        result = "";
      }
    }
    else if(lastScreen >= screenId)
    {
      Vector s = screens.getScreens();
      ScreenInfo si = (ScreenInfo)s.elementAt(screens.getIndex(screenId));
      if(si.isVisitable())
      {
        result = "";
      }
      else
      {
        result = getNextPage(screenId); 
      }
    }
    
    return result;
  }
  
  /*
  ** METHOD getData(called everytime page opens)
     if session primary key != querystring primary key start a new session or 
     if session sequence id != applications sequence id start a new session
  */
  public synchronized void getData(long sequenceId, long primaryKey)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      connect();
      
      this.originalAppType = getOriginalAppType(primaryKey);
      if(this.originalAppType != -1)
      {
        setAppHeaderInfoOg(); //sets app header according to orignal apptype.
      }
      
      if(getPrimaryKey() != primaryKey || getSequenceId() != sequenceId)
      {
        //commented out the need to view an app on their own sequence.. so verisign 4 page app can be viewed on 2 page app
        if(user.isValidUser(primaryKey, this.appType))// && user.isValidSequence(sequenceId, userSequenceType)) //validate user
        {
          primaryKey = issuePrimaryKey(primaryKey);
          super.getData(sequenceId, primaryKey);
          checkCompletedScreens();
          setSkippedScreens();
          
          if(appType == com.mes.constants.mesConstants.APP_TYPE_DEMO)
          {
            setSequenceDemoInfo();
          }
          else
          {
            setAppHeaderInfo();
          }
        }
      }
      
      // get merchant number if it exists
      /*@lineinfo:generated-code*//*@lineinfo:374^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch.merch_number,
//                  merch.merch_credit_status,
//                  app.appsrctype_code
//          from    merchant      merch,
//                  application   app
//          where   merch.app_seq_num = :primaryKey and
//                  merch.app_seq_num = app.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch.merch_number,\n                merch.merch_credit_status,\n                app.appsrctype_code\n        from    merchant      merch,\n                application   app\n        where   merch.app_seq_num =  :1  and\n                merch.app_seq_num = app.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.setup.AppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.setup.AppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:383^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        this.merchantNumber     = rs.getLong("merch_number");
        this.appTypeDesc        = rs.getString("appsrctype_code");
        this.merchCreditStatus  = rs.getInt("merch_credit_status");
      }
      else
      {
        this.merchantNumber = 0L;
        this.appTypeDesc = "";
        this.merchCreditStatus = 0;
      }
      
      rs.close();
      it.close();     
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  /*
    this method checks to see if an entry has been made into the screen_progress table.
    if it has and this current session belongs to a different sequence type, e.g., 
    a verisign app being viewed by the mes application, then we set the completed screens 
    to the highest number for that sequence, so that the entire application can be navigated 
    through
  */
  public synchronized void checkCompletedScreens()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:430^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sp.screen_sequence_id
//          from    screen_progress sp,
//                  application     app
//          where   sp.screen_pk    = :getPrimaryKey() and
//                  sp.screen_pk    = app.app_seq_num and
//                  sp.screen_sequence_id = app.screen_sequence_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1541 = getPrimaryKey();
  try {
   String theSqlTS = "select  sp.screen_sequence_id\n        from    screen_progress sp,\n                application     app\n        where   sp.screen_pk    =  :1  and\n                sp.screen_pk    = app.app_seq_num and\n                sp.screen_sequence_id = app.screen_sequence_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.setup.AppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1541);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.setup.AppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:438^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        if(rs.getLong("screen_sequence_id") != getSequenceId())
        {
          if(getSequenceId() == com.mes.constants.mesConstants.SEQUENCE_ID_APPLICATION)
          {
            setCompletedScreens(1);
          }
          else if(getSequenceId() == com.mes.constants.mesConstants.SEQUENCE_APPLICATION)
          {
            setCompletedScreens(7);
          }
        }
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("checkCompletedScreens()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
    this method checks to see if an entry has been made into the screen_progress table.
    if it has and this current session belongs to a different sequence type, e.g., 
    a verisign app being viewed by the mes application, then we bypass the submit data method in 
   the base class. This avoids two entries in the screen_progress table for one application.
  */
  public synchronized void submitData(long screenId)
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      // get connection
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:488^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sp.screen_pk,
//                  sp.screen_sequence_id
//          from    screen_progress sp
//          where   screen_pk = :getPrimaryKey()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1542 = getPrimaryKey();
  try {
   String theSqlTS = "select  sp.screen_pk,\n                sp.screen_sequence_id\n        from    screen_progress sp\n        where   screen_pk =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.setup.AppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1542);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.setup.AppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:494^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        if(rs.getLong("screen_sequence_id") == getSequenceId())
        {
          super.submitData(screenId);  
        }
      }
      else
      {
        super.submitData(screenId);
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("submitData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  //either gets new primary key and control num and starts new session or
  //validates userId and sets primary key and control num and stats new session
  public synchronized long issuePrimaryKey(long primaryKey)
  {
    if(primaryKey <= 0L) // if no seqnum generate new one
    {
      primaryKey = setSeqNum();
    }
    else if(primaryKey > 0L)//else get control number
    {
      this.appControlNum   = getControlNumber(primaryKey);
    }
    
    // if appControlNum is still zero then just assign new app seq num and control num
    // because this means there is no entry in the merchant table
    if(appControlNum == 0L)
    {
      primaryKey = setSeqNum();
    }
    return primaryKey;
  }
  
  public synchronized int getOriginalAppType(long primaryKey)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    int                 result  = -1;
    
    try
    {
      connect();
      
      if(primaryKey > 0L)
      {
        /*@lineinfo:generated-code*//*@lineinfo:559^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_type
//            from    application
//            where   app_seq_num = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_type\n          from    application\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.setup.AppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.setup.AppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:564^9*/
    
        rs = it.getResultSet();
    
        if(rs.next())
        {
          result = rs.getInt("app_type");
        }
        else
        {
          result = -1;
        }
      
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("getOriginalAppType(" + primaryKey + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  public synchronized boolean isNewApp(long primaryKey)
  {
    boolean               result  = true;
    try
    {
      // get connection
      connect();
      
      int appCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:605^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//          
//          from    application
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num) \n         \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.setup.AppIdBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:611^7*/

      if(appCount > 0)
      {
        result = false;
      }
    }
    catch(Exception e)
    {
      logEntry("isNewApp(" + primaryKey + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return result;
  }
  
  /*
  ** METHOD setSeqNum
  */
  private long setSeqNum()
  {
    long                controlSeq  = 10000000000L;
    long                primaryKey  = 0L;
    
    try
    {
      // get connection
      connect();
      
      // get a new sequence number
      /*@lineinfo:generated-code*//*@lineinfo:643^7*/

//  ************************************************************
//  #sql [Ctx] { select  application_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  application_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.setup.AppIdBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   primaryKey = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:648^7*/

      // get a new control number
      Calendar      cal         = new GregorianCalendar();
      String        YYYY        = Integer.toString(cal.get(Calendar.YEAR));
      String        YY          = YYYY.substring(2);
      int           day         = cal.get(Calendar.DAY_OF_YEAR);

      // add the year
      controlSeq += ((cal.get(Calendar.YEAR) - 2000) * 100000000L);

      // add the julian day
      controlSeq += (cal.get(Calendar.DAY_OF_YEAR) * 100000L);

      long sequencePart   = 0L;
      // add the sequence number
      /*@lineinfo:generated-code*//*@lineinfo:664^7*/

//  ************************************************************
//  #sql [Ctx] { select  control_num_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  control_num_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.setup.AppIdBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sequencePart = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:669^7*/
      
      this.appControlNum = controlSeq + sequencePart;
    }
    catch(Exception e)
    {
      logEntry("setSeqNum()", e.toString());
      if(primaryKey == 0L)
      {
        primaryKey = 111;
      }
      
      this.appControlNum = controlSeq + 11111;
    }
    finally
    {
      cleanUp();
    }
    return primaryKey;
  }

  private long getControlNumber(long appSeqNum)
  {
    long              result  = 0L;
    
    try
    {
      // get connection
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:699^7*/

//  ************************************************************
//  #sql [Ctx] { select  merc_cntrl_number
//          
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merc_cntrl_number\n         \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.setup.AppIdBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:705^7*/
    }
    catch(Exception e)
    {
      logEntry("getControlNumber(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public synchronized void setSkippedScreens()
  {
    Vector s = screens.getScreens();
    int tempPosType = getPosType(getPrimaryKey());

    if(getSequenceId() == com.mes.constants.mesConstants.SEQUENCE_APPLICATION || getSequenceId() == com.mes.constants.mesConstants.SEQUENCE_APPLICATION_TRANSCOM)
    {
      ScreenInfo si = (ScreenInfo)s.elementAt(1);

      if(tempPosType != com.mes.constants.mesConstants.APP_PAYSOL_DIAL_TERMINAL && 
         tempPosType != com.mes.constants.mesConstants.APP_PAYSOL_DIAL_PAY      && 
         tempPosType != com.mes.constants.mesConstants.POS_GPS                  && 
         tempPosType != com.mes.constants.mesConstants.POS_WIRELESS_TERMINAL    &&
         tempPosType != com.mes.constants.mesConstants.POS_VIRTUAL_TERMINAL)
      {
        si.setSkipOver(true);
        if(getCompletedScreens() == 0L)
          submitData(7);
        else
          submitData(2);
      }
      else
      {
        si.setSkipOver(false);  
      }
    }
  }
  
  public synchronized int getPosType(long appSeqNum)
  {
    int             posType   = 1;
    
    try
    {
      // get connection
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:756^7*/

//  ************************************************************
//  #sql [Ctx] { select  pc.pos_type
//          
//          from    merch_pos     mp,
//                  pos_category  pc
//          where   mp.app_seq_num = :appSeqNum and
//                  mp.pos_code = pc.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pc.pos_type\n         \n        from    merch_pos     mp,\n                pos_category  pc\n        where   mp.app_seq_num =  :1  and\n                mp.pos_code = pc.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.setup.AppIdBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   posType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:764^7*/
    }
    catch(Exception e)
    {
    }
    finally
    {
      cleanUp();
    }
    
    return posType;
  }

  //this method gets all the specific info for a particular demo.. 
  public synchronized void setSequenceDemoInfo()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:787^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sequence_header,
//                  sequence_navigate,
//                  sequence_after_load,
//                  sequence_after_submit,
//                  sequence_errors,
//                  sequence_footer,
//                  demo_gif,
//                  demo_width,
//                  demo_height
//          from    sequence_demo_info
//          where   user_id = :getUserId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1543 = getUserId();
  try {
   String theSqlTS = "select  sequence_header,\n                sequence_navigate,\n                sequence_after_load,\n                sequence_after_submit,\n                sequence_errors,\n                sequence_footer,\n                demo_gif,\n                demo_width,\n                demo_height\n        from    sequence_demo_info\n        where   user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.setup.AppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1543);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.setup.AppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:800^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        // check for invalid entries
        setSequenceHeader(isBlank(rs.getString("sequence_header")) ? getSequenceHeader() : rs.getString("sequence_header"));
        setSequenceNavigate(isBlank(rs.getString("sequence_navigate")) ? getSequenceNavigate() : rs.getString("sequence_navigate"));
        setSequenceAfterLoad(isBlank(rs.getString("sequence_after_load")) ? getSequenceAfterLoad() : rs.getString("sequence_after_load"));
        setSequenceAfterSubmit(isBlank(rs.getString("sequence_after_submit")) ? getSequenceAfterSubmit() : rs.getString("sequence_after_submit"));
        setSequenceErrors(isBlank(rs.getString("sequence_errors")) ? getSequenceErrors() : rs.getString("sequence_errors"));
        setSequenceFooter(isBlank(rs.getString("sequence_footer")) ? getSequenceFooter() : rs.getString("sequence_footer"));
        
        if(!isBlank(rs.getString("demo_gif")))
        {
          this.appGif    = rs.getString("demo_gif");
          this.appWidth  = rs.getInt("demo_width");
          this.appHeight = rs.getInt("demo_height");
        }
        else
        {
          this.appGif    = "/images/genericDemo.gif";
          this.appWidth  = 90;
          this.appHeight = 38;
        }
      }
      else
      {
        this.appGif    = "/images/genericDemo.gif";
        this.appWidth  = 90;
        this.appHeight = 38;
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("setSequenceDemoInfo()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public synchronized void setAppHeaderInfo()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:858^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_gif,
//                  app_width,
//                  app_height
//          from    org_app
//          where   app_type = :this.appType
//                  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_gif,\n                app_width,\n                app_height\n        from    org_app\n        where   app_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.setup.AppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.appType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.setup.AppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:866^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        if(!isBlank(rs.getString("app_gif")))
        {
          this.appGif    = rs.getString("app_gif");
          this.appWidth  = rs.getInt("app_width");
          this.appHeight = rs.getInt("app_height");
        }
        else
        {
          this.appGif    = "";
          this.appWidth  = 1;
          this.appHeight = 1;
        }
      }
      else
      {
        this.appGif    = "";
        this.appWidth  = 1;
        this.appHeight = 1;
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("setAppHeaderInfo()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public synchronized void setAppHeaderInfoOg()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      if(this.originalAppType != -1)
      {
        /*@lineinfo:generated-code*//*@lineinfo:918^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_gif,
//                    app_width,
//                    app_height
//            from    org_app
//            where   app_type = :originalAppType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_gif,\n                  app_width,\n                  app_height\n          from    org_app\n          where   app_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.setup.AppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,originalAppType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.setup.AppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:925^9*/  
      
        rs = it.getResultSet();
      
        if(rs.next())
        {
          if(!isBlank(rs.getString("app_gif")))
          {
            this.appGifOg    = rs.getString("app_gif");
            this.appWidthOg  = rs.getInt("app_width");
            this.appHeightOg = rs.getInt("app_height");
          }
          else
          {
            this.appGifOg    = "";
            this.appWidthOg  = 1;
            this.appHeightOg = 1;
          }
        }
        else
        {
          this.appGifOg    = "";
          this.appWidthOg  = 1;
          this.appHeightOg = 1;
        }
      
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("setAppHeaderInfoOg()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }


  public synchronized long getAppControlNum()
  {
    return this.appControlNum;
  }
  public synchronized void setUserId(long userId, long sequenceId)
  {
    user = new ValidateUser();
    user.setUserId(userId);
  }
  public synchronized long getUserId()
  {
    return user.getUserId();
  }
  public synchronized String getAppGif()
  {
    return this.appGif;
  }
  public synchronized int getAppWidth()
  {
    return this.appWidth;
  }
  public synchronized int getAppHeight()
  {
    return this.appHeight;
  }

  public synchronized String getAppGifOg()
  {
    return this.appGifOg;
  }
  public synchronized int getAppWidthOg()
  {
    return this.appWidthOg;
  }
  public synchronized int getAppHeightOg()
  {
    return this.appHeightOg;
  }

  public synchronized long getMerchantNumber()
  {
    return this.merchantNumber;
  }
  public synchronized int getAppType()
  {
    return appType;
  }
  public void setAppType(String appType)
  {
    try
    {
      setAppType(Integer.parseInt(appType));
    }
    catch(Exception e)
    {
      logEntry("setAppType(" + appType + ")", e.toString());
    }
  }
  public synchronized void setAppType(int appType)
  {
    this.appType = appType;
  }
  
  public synchronized int getOriginalAppType()
  {
    int retVal = originalAppType;
    
    if(originalAppType == -1)
    {
      // return users default app type
      retVal = appType;
    }
    
    return( retVal );
  }

  public synchronized long getUserSequenceType()
  {
    return this.userSequenceType;
  }
  public void setUserSequenceType(String userSequenceType)
  {
    try
    {
      setUserSequenceType(Long.parseLong(userSequenceType));
    }
    catch(Exception e)
    {
      logEntry("setUserSequenceType(" + userSequenceType + ")", e.toString());
    }
  }
  public synchronized void setUserSequenceType(long userSequenceType)
  {
    this.userSequenceType = userSequenceType;
  }
  public boolean isEnforceOrder()
  {
    boolean result = true;
    return result;
  }
  
  public synchronized boolean okToShow()
  {
    return (merchCreditStatus == QueueConstants.CREDIT_APPROVE || merchCreditStatus == QueueConstants.CREDIT_DECLINE || merchCreditStatus == QueueConstants.CREDIT_PEND);
  }

  public synchronized String getAppTypeDesc()
  {
    return this.appTypeDesc;
  }

  public synchronized boolean allowAppType(int atype)
  {
    boolean result = false;

    if(this.appType == atype || this.originalAppType == atype)
    {
      result = true;
    }
    
    return result;
  }
}/*@lineinfo:generated-code*/