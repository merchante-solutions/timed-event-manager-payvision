/*@lineinfo:filename=EmailBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/EmailBean.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 3/07/01 12:36p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.setup;

import java.sql.ResultSet;
import java.sql.Timestamp;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class EmailBean extends com.mes.database.SQLJConnection
{
  private StringBuffer  batch               = new StringBuffer("BATCH EMPTY");
  private final int     app_type            = mesConstants.APP_TYPE_NSI;
  private boolean       errorsOccurred      = false;
  private boolean       amexaccepted        = false;
  private boolean       checksaccepted      = false;
  private boolean       dinersaccepted      = false;
  private boolean       discoveraccepted    = false;
  private boolean       jcbaccepted         = false;
  private Timestamp     lastRecordCompleted = null;

  public  EmailBean()
  {
  }
  
  public EmailBean( String connectString )
  {
    super(connectString);
  }
  
  public String getBatch()
  {
    ResultSet         rs              = null;
    ResultSetIterator it              = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:60^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  a.app_seq_num,
//                  a.merch_business_name,
//                  a.merch_federal_tax_id,
//                  a.merch_number,
//                  a.merch_email_address,
//                  b.address_line1,
//                  b.address_line2,
//                  b.address_city,
//                  b.countrystate_code,
//                  b.country_code,
//                  b.address_zip,
//                  b.address_phone,
//                  c.merchcont_prim_first_name,
//                  c.merchcont_prim_last_name,
//                  c.merchcont_prim_phone,
//                  d.pos_param,
//                  e.date_completed
//          from    merchant a,
//                  address b,
//                  merchcontact c,
//                  merch_pos d,
//                  app_account_complete e,
//                  application f
//          where   a.app_seq_num = e.app_seq_num and
//                  e.date_processed is null and a.app_seq_num = b.app_seq_num and
//                  b.addresstype_code = 1 and a.app_seq_num = f.app_seq_num and 
//                  f.app_type = :app_type and a.app_seq_num = c.app_seq_num and
//                  a.app_seq_num = d.app_seq_num
//        order by  e.date_completed asc
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  a.app_seq_num,\n                a.merch_business_name,\n                a.merch_federal_tax_id,\n                a.merch_number,\n                a.merch_email_address,\n                b.address_line1,\n                b.address_line2,\n                b.address_city,\n                b.countrystate_code,\n                b.country_code,\n                b.address_zip,\n                b.address_phone,\n                c.merchcont_prim_first_name,\n                c.merchcont_prim_last_name,\n                c.merchcont_prim_phone,\n                d.pos_param,\n                e.date_completed\n        from    merchant a,\n                address b,\n                merchcontact c,\n                merch_pos d,\n                app_account_complete e,\n                application f\n        where   a.app_seq_num = e.app_seq_num and\n                e.date_processed is null and a.app_seq_num = b.app_seq_num and\n                b.addresstype_code = 1 and a.app_seq_num = f.app_seq_num and \n                f.app_type =  :1  and a.app_seq_num = c.app_seq_num and\n                a.app_seq_num = d.app_seq_num\n      order by  e.date_completed asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.setup.EmailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,app_type);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.setup.EmailBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:92^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        batch.setLength(0);
        makeFile(rs);
        while(rs.next())
        {
          batch.append("\n");
          makeFile(rs);
          if(errorsOccurred)
          {
            break;
          }
        }
      }
      
      if(errorsOccurred)
      {
        batch.setLength(0);
        batch.append("ERROR OCCURRED");
      }
      else
      {
        updateRecords();
      }
    
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::getBatch(): " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "::getBatch(): " + e.toString());
      batch.setLength(0);
      batch.append("ERROR OCCURRED");
    }
    finally
    {
      cleanUp();
    }
    return batch.toString();
  }

private void makeFile(ResultSet rs)
{
  long appSeqNum = 0L;
  try
  {
    appSeqNum           = rs.getLong("app_seq_num");
    lastRecordCompleted = rs.getTimestamp("date_completed");

    batch.append("LoginName=");
    batch.append("~\n");
    batch.append("Pwd=");
    batch.append("~\n");
    batch.append("Status=");
    batch.append("~\n");
    
    setCardsAccepted(appSeqNum);
    batch.append(amexaccepted     ? "Amex_Host_Auth=VITA~\n"      : "Amex_Host_Auth=VITA~\n");
    batch.append(checksaccepted   ? "Check_Host_Auth=VITA~\n"     : "Check_Host_Auth=~\n");
    batch.append(dinersaccepted   ? "Diners_Host_Auth=VITA~\n"    : "Diners_Host_Auth=~\n");
    batch.append(discoveraccepted ? "Discover_Host_Auth=VITA~\n"  : "Discover_Host_Auth=~\n");
    batch.append(jcbaccepted      ? "JCB_Host_Auth=VITA~\n"       : "JCB_Host_Auth=~\n");
    resetCardsAccepted();

    batch.append("MC_Host_Auth=");
    batch.append("VITA~\n");
    batch.append("Visa_Host_Auth=");
    batch.append("VITA~\n");

    batch.append("Ips_Allowed=");
    batch.append("~\n");

    batch.append("Business_Name=");
    batch.append(isBlank(rs.getString("merch_business_name")) ? "~\n" : (rs.getString("merch_business_name").trim() + "~\n"));

    batch.append("Product_ID=");
    batch.append("~\n");

    batch.append("Address1=");
    batch.append(isBlank(rs.getString("address_line1")) ? "~\n" : (rs.getString("address_line1").trim() + "~\n"));

    batch.append("Address2=");
    batch.append(isBlank(rs.getString("address_line2")) ? "~\n" : (rs.getString("address_line2").trim() + "~\n"));

    batch.append("City=");
    batch.append(isBlank(rs.getString("address_city")) ? "~\n" : (rs.getString("address_city").trim() + "~\n"));

    batch.append("State=");
    batch.append(isBlank(rs.getString("countrystate_code")) ? "~\n" : (rs.getString("countrystate_code").trim() + "~\n"));

    batch.append("Zip=");
    batch.append(isBlank(rs.getString("address_zip")) ? "~\n" : (rs.getString("address_zip").trim() + "~\n"));

    batch.append("Country=");
    batch.append(isBlank(rs.getString("country_code")) ? "~\n" : (rs.getString("country_code").trim() + "~\n"));

    batch.append("URL=");
    batch.append(isBlank(rs.getString("pos_param")) ? "~\n" : (rs.getString("pos_param").trim() + "~\n"));


    batch.append("Acquirer=");
    batch.append("Merchante-Solutions.com~\n");

    batch.append("Merchant_Acct=");
    batch.append(isBlank(rs.getString("merch_number")) ? "~\n" : (rs.getString("merch_number").trim() + "~\n"));

    batch.append("Memo1=");
    batch.append("~\n");
    batch.append("Memo2=");
    batch.append("~\n");
    batch.append("Memo3=");
    batch.append("~\n");
    batch.append("Memo4=");
    batch.append("~\n");
    batch.append("Memo5=");
    batch.append("~\n");
    batch.append("Memo6=");
    batch.append("~\n");
    batch.append("Memo7=");
    batch.append("~\n");
    batch.append("Memo8=");
    batch.append("~\n");

    batch.append("Primary_FName=");
    batch.append(isBlank(rs.getString("merchcont_prim_first_name")) ? "~\n" : (rs.getString("merchcont_prim_first_name").trim() + "~\n"));

    batch.append("Primary_LName=");
    batch.append(isBlank(rs.getString("merchcont_prim_last_name")) ? "~\n" : (rs.getString("merchcont_prim_last_name").trim() + "~\n"));

    batch.append("Primary_Address1=");
    batch.append("~\n");
    batch.append("Primary_Address2=");
    batch.append("~\n");
    batch.append("Primary_City=");
    batch.append("~\n");
    batch.append("Primary_State=");
    batch.append("~\n");
    batch.append("Primary_Zip=");
    batch.append("~\n");

    batch.append("Primary_Email=");
    batch.append(isBlank(rs.getString("merch_email_address")) ? "~\n" : (rs.getString("merch_email_address").trim() + "~\n"));
    batch.append("Primary_Phone=");
    batch.append(isBlank(rs.getString("merchcont_prim_phone")) ? "~\n" : (rs.getString("merchcont_prim_phone").trim() + "~\n"));

    batch.append("Primary_Fax=");
    batch.append("~\n");
    batch.append("Secondary_Name=");
    batch.append("~\n");
    batch.append("Secondary_Address1=");
    batch.append("~\n");
    batch.append("Secondary_Address2=");
    batch.append("~\n");
    batch.append("Secondary_City=");
    batch.append("~\n");
    batch.append("Secondary_State=");
    batch.append("~\n");
    batch.append("Secondary_Zip=");
    batch.append("~\n");
    batch.append("Secondary_Email=");
    batch.append("~\n");
    batch.append("Secondary_Phone=");
    batch.append("~\n");
    batch.append("Secondary_Fax=");
    batch.append("~\n");

    batch.append("Bank_Name=");
    batch.append("Ray Campo~\n");
    batch.append("Bank_Address1=");
    batch.append("~\n");
    batch.append("Bank_Address2=");
    batch.append("~\n");
    batch.append("Bank_City=");
    batch.append("~\n");
    batch.append("Bank_State=");
    batch.append("~\n");
    batch.append("Bank_Zip=");
    batch.append("~\n");
    batch.append("Bank_Email=");
    batch.append("~\n");
    batch.append("Bank_Phone=");
    batch.append("6506286816~\n");
    batch.append("Bank_Fax=");
    batch.append("~\n");


    batch.append("Nash_Merchant_Id=");
    batch.append("~\n");
    batch.append("Nash_Terminal_Id=");
    batch.append("~\n");
    batch.append("Nova_Bank_Id=");
    batch.append("~\n");
    batch.append("Nova_Terminal_Id=");
    batch.append("~\n");

    batch.append("Vital_Bank_Id=");
    batch.append("433239~\n");
    batch.append("Vital_Merchant_Id=");
    batch.append(isBlank(rs.getString("merch_number")) ? "~\n" : (rs.getString("merch_number").trim() + "~\n"));

    batch.append("Vital_Store_Num=");
    batch.append("0001~\n");
    batch.append("Vital_Terminal_Num=");
    batch.append("0001~\n");

    batch.append("Vital_Zip=");
    batch.append(isBlank(rs.getString("address_zip")) ? "~\n" : (rs.getString("address_zip").trim() + "~\n"));

    batch.append("Vital_Time_Zone=");
    batch.append("705~\n");

    batch.append("Vital_Category=");
    batch.append("5734~\n");

    batch.append("Vital_Merchant_Name=");
    batch.append(isBlank(rs.getString("merch_business_name")) ? "~\n" : (rs.getString("merch_business_name").trim() + "~\n"));

    batch.append("Vital_Merchant_Phone=");
    batch.append(isBlank(rs.getString("address_phone")) ? "~\n" : (rs.getString("address_phone").trim() + "~\n"));

    batch.append("Vital_Merchant_State=");
    batch.append(isBlank(rs.getString("countrystate_code")) ? "~\n" : (rs.getString("countrystate_code").trim() + "~\n"));

    batch.append("Vital_Agent_Bin=");
    batch.append("000000~\n");

    batch.append("Vital_Agent_Chain=");
    batch.append("600008~\n");

    batch.append("Vital_Merchant_Location_Num=");
    batch.append("00001~\n");

    batch.append("Vital_Curr_Code=");
    batch.append("840~\n");

    batch.append("Vital_Country_Code=");
    batch.append("840~\n");

    batch.append("v_Number=");
    batch.append(getVnumber(appSeqNum) + "~\n");

    batch.append("Payt_Division_Num=");
    batch.append("~\n");
    batch.append("Payt_Curr_Code=");
    batch.append("~\n");
    batch.append("Tele_Ip_Code=");
    batch.append("~\n");
    batch.append("Sout_MerchantID=");
    batch.append("~\n");
    batch.append("Sout_Amex_se=");
    batch.append("~\n");
    batch.append("Sout_Diners_se=");
    batch.append("~\n");
    batch.append("Sout_Novus_se=");
    batch.append("~\n");
    batch.append("Sout_JCB_se=");
    batch.append("~\n");
    batch.append("Sout_JAL_se=");
    batch.append("~\n");
    batch.append("Sout_Sic=");
    batch.append("~\n");
    batch.append("Sout_State=");
    batch.append("~\n");
    batch.append("Sout_Zip=");
    batch.append("~\n");
    batch.append("Sout_Acquirer=");
    batch.append("~\n");
    batch.append("Amex_MerchantID=");
    batch.append("~\n");
    batch.append("Amex_Curr_Code=");
    batch.append("~\n");
    batch.append("Amex_Sic=");
    batch.append("~\n");
    batch.append("Amex_Establishment_Name=");
    batch.append("~\n");
    batch.append("Amex_Establishment_City=");
    batch.append("~\n");
    batch.append("Amex_Establishment_State=");
    batch.append("~\n");
    batch.append("Edsa_MerchantID=");
    batch.append("~\n");
    batch.append("Edsa_GroupID=");
    batch.append("~\n");
    batch.append("***");
  }
  catch(Exception e)
  {
    System.out.println(e.toString());
    com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "::makeFile(appseqnum=" + appSeqNum + "): " + e.toString());
    errorsOccurred = true;
  }
}
  
  private String getVnumber(long appSeqNum)
  {
    ResultSet         rs              = null;
    ResultSetIterator it              = null;
    String            result          = "";
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:395^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  vnumber
//          from    merch_vnumber
//          where   app_seq_num = :appSeqNum
//          order by terminal_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vnumber\n        from    merch_vnumber\n        where   app_seq_num =  :1 \n        order by terminal_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.setup.EmailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.setup.EmailBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:401^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        result = rs.getString("vnumber");  
      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::getVnumber(appseqnum =" + appSeqNum +"): " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "::getVnumber(appseqnum =" + appSeqNum +"): " + e.toString());
      errorsOccurred = true;
    }
    finally
    {
    }
    return result;
  }
  
  private void setCardsAccepted(long appSeqNum)
  {
    ResultSet         rs              = null;
    ResultSetIterator it              = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:429^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code
//          from    merchpayoption
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code\n        from    merchpayoption\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.setup.EmailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.setup.EmailBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:434^7*/

      rs = it.getResultSet();

      resetCardsAccepted();

      while(rs.next())
      {
        switch(rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_DISCOVER:
            discoveraccepted  = true;
            break;
          case mesConstants.APP_CT_JCB:
            jcbaccepted       = true;
            break;
          case mesConstants.APP_CT_AMEX:
            amexaccepted      = true;
            break;
          case mesConstants.APP_CT_CHECK_AUTH:
            checksaccepted    = true;
            break;
          case mesConstants.APP_CT_DINERS_CLUB:
            dinersaccepted    = true;
            break;
        }

      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::setCardsAccepted(appseqnum =" + appSeqNum +"): " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "::setCardsAccepted(appseqnum =" + appSeqNum +"): " + e.toString());
      errorsOccurred = true;
    }
  }

  private void resetCardsAccepted()
  {
    amexaccepted      = false;
    checksaccepted    = false;
    dinersaccepted    = false;
    discoveraccepted  = false;
    jcbaccepted       = false;
  }
  
  private void updateRecords()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:484^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_account_complete
//          set     date_processed = sysdate
//          where   date_processed is null and date_completed <= :lastRecordCompleted
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_account_complete\n        set     date_processed = sysdate\n        where   date_processed is null and date_completed <=  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.setup.EmailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,lastRecordCompleted);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:489^7*/
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::updateRecords(update crashed): " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "::updateRecords(update crashed): " + e.toString());
    }
  }
  
  public boolean isBlank(String test)
  {
    boolean pass = false;

    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }

    return pass;
  }

}/*@lineinfo:generated-code*/