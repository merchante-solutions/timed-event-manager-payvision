/*@lineinfo:filename=DiscoverPricingBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/DiscoverPricingBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 4/04/03 4:45p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.setup;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class DiscoverPricingBean extends SQLJConnectionBase
{
  public  static final String  VALID                  = "VALID";

  private static final String  GRID_TYPE_BUNDLED      = "B";
  private static final String  GRID_TYPE_UNBUNDLED    = "U";

  private static final String  MERCH_TYPE_RETAIL      = "RETAIL";
  private static final String  MERCH_TYPE_RESTAURANT  = "RESTAURANT";
  private static final String  MERCH_TYPE_LODGING     = "LODGING";
  private static final String  MERCH_TYPE_MOTO        = "MOTO";

  private String merchantType                         = "";
  private String annualSalesGroup                     = "";
  private String gridType                             = "";

  private double retailMinPerItem                     = 0.0;
  private double restaurantMinPerItem                 = 0.0;
  private double lodgingMinPerItem                    = 0.0;
  private double motoMinPerItem                       = 0.0;

  private int    appType                              = -1;

  public DiscoverPricingBean(int type)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;

    //set the application type.. either discover or discover ims
    this.appType = type;

    try
    {
      connect();
  
      /*@lineinfo:generated-code*//*@lineinfo:79^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   *
//          from     DISCOVER_PERITEM_FEES
//          where    app_type = :appType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   *\n        from     DISCOVER_PERITEM_FEES\n        where    app_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.setup.DiscoverPricingBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.setup.DiscoverPricingBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {

        if(rs.getString("MERCHANT_TYPE").equals(MERCH_TYPE_RETAIL))
        {
          this.retailMinPerItem = rs.getDouble("PER_ITEM_FEE");
        }
        if(rs.getString("MERCHANT_TYPE").equals(MERCH_TYPE_RESTAURANT))
        {
          this.restaurantMinPerItem = rs.getDouble("PER_ITEM_FEE");
        }
        else if(rs.getString("MERCHANT_TYPE").equals(MERCH_TYPE_LODGING))
        {
          this.lodgingMinPerItem = rs.getDouble("PER_ITEM_FEE");
        }
        else if(rs.getString("MERCHANT_TYPE").equals(MERCH_TYPE_MOTO))
        {
          this.motoMinPerItem  = rs.getDouble("PER_ITEM_FEE");
        }

      }
 
      it.close();
      
    }
    catch(Exception e)
    {
      logEntry("DiscoverPricingBean", e.toString());
    }
    finally
    {
      cleanUp();
    }

  }

  public String validateDiscountRate(double discountRate, double perItem, int pricedAs, double visaVol, double aveTic)
  {
    String result = VALID;
    
    this.merchantType       = getMerchType(pricedAs);
    this.annualSalesGroup   = getAnnualSalesGroup(visaVol);
    this.gridType           = getGridType(perItem);

    if(gridType.equals(GRID_TYPE_UNBUNDLED) && !isValidPerItem(perItem))
    {
      if(merchantType.equals(MERCH_TYPE_RETAIL))
      {
        result = "Minimum per item fee for this merchant is " + this.retailMinPerItem;
      }
      else if(merchantType.equals(MERCH_TYPE_RESTAURANT))
      {
        result = "Minimum per item fee for this merchant is " + this.restaurantMinPerItem;
      }
      else if(merchantType.equals(MERCH_TYPE_LODGING))
      {
        result = "Minimum per item fee for this merchant is " + this.lodgingMinPerItem;
      }
      else if(merchantType.equals(MERCH_TYPE_MOTO))
      {
        result = "Minimum per item fee for this merchant is " + this.motoMinPerItem;
      }

      return result;
    }

    //anything greater than 999 is looked at as 999 anyway..
    if(aveTic > 999.0)
    {
      aveTic = 999.0;
    }

    ResultSetIterator   it          = null;
    ResultSet           rs          = null;

    try
    {
      connect();
  
      /*@lineinfo:generated-code*//*@lineinfo:167^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   discount_rate
//          from     discover_pricing_grid
//          where    app_type                   = :appType          and
//                   grid_type                  = :gridType         and 
//                   merch_type                 = :merchantType     and
//                   ANNUAL_SALES_RANGE_GROUP   = :annualSalesGroup and
//                   :aveTic between AVE_TICKET_MIN_RANGE and AVE_TICKET_MAX_RANGE
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   discount_rate\n        from     discover_pricing_grid\n        where    app_type                   =  :1           and\n                 grid_type                  =  :2          and \n                 merch_type                 =  :3      and\n                 ANNUAL_SALES_RANGE_GROUP   =  :4  and\n                  :5  between AVE_TICKET_MIN_RANGE and AVE_TICKET_MAX_RANGE";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.setup.DiscoverPricingBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   __sJT_st.setString(2,gridType);
   __sJT_st.setString(3,merchantType);
   __sJT_st.setString(4,annualSalesGroup);
   __sJT_st.setDouble(5,aveTic);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.setup.DiscoverPricingBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:176^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        if(discountRate < rs.getDouble("discount_rate"))
        {
          result = "Discount Rate must be greater than or equal to " + rs.getString("discount_rate");
        }
      }
 
      it.close();
      
    }
    catch(Exception e)
    {
      logEntry("validateDiscountRate", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  private String getMerchType(int grid)
  {
    String result = "";

    switch(grid)
    {
      case mesConstants.APP_PG_LODGING:
        result = MERCH_TYPE_LODGING;
      break;

      case mesConstants.APP_PG_RETAIL_RESTAURANT:
        result = MERCH_TYPE_RETAIL;
      break;

      case mesConstants.APP_PG_RESTAURANT:
        result = MERCH_TYPE_RESTAURANT;
      break;

      case mesConstants.APP_PG_MOTO_INTERNET:
        result = MERCH_TYPE_MOTO;
      break;
    }

    return result;

  }

  private boolean isValidPerItem(double perItem)
  {
    boolean result = false;

    if(merchantType.equals(MERCH_TYPE_RETAIL) && perItem >= this.retailMinPerItem)
    {
      result = true;
    }
    else if(merchantType.equals(MERCH_TYPE_RESTAURANT) && perItem >= this.restaurantMinPerItem)
    {
      result = true;
    }
    else if(merchantType.equals(MERCH_TYPE_LODGING) && perItem >= this.lodgingMinPerItem)
    {
      result = true;
    }
    else if(merchantType.equals(MERCH_TYPE_MOTO) && perItem >= this.motoMinPerItem)
    {
      result = true;
    }

    return result;
  }

  private String getAnnualSalesGroup(double monthlyVol)
  {
    double yearlyVol = monthlyVol * 12;

    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    String              result      = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:266^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   ANNUAL_SALES_RANGE_GROUP_CODE
//          from     DISCOVER_SALESRANGE_GROUP_CODE
//          where    :yearlyVol between min_range and max_range
//                   and app_type = :appType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   ANNUAL_SALES_RANGE_GROUP_CODE\n        from     DISCOVER_SALESRANGE_GROUP_CODE\n        where     :1  between min_range and max_range\n                 and app_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.setup.DiscoverPricingBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,yearlyVol);
   __sJT_st.setInt(2,appType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.setup.DiscoverPricingBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:272^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = rs.getString("ANNUAL_SALES_RANGE_GROUP_CODE");
      }
 
      it.close();
      
    }
    catch(Exception e)
    {
      logEntry("getAnnualSalesGroup", e.toString());
    }
    finally
    {
      cleanUp();
    }
  
    return result;
  }
  
  private String getGridType(double perItem)
  {
    String result = "";

    if(perItem <= 0.0)
    {
      result = GRID_TYPE_BUNDLED;
    }
    else
    {
      result = GRID_TYPE_UNBUNDLED;
    }

    return result;
  }

  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }
}/*@lineinfo:generated-code*/