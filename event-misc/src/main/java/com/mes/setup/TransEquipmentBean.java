/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/TransEquipmentBean.java $

  Description:


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-01-15 15:38:18 -0800 (Tue, 15 Jan 2008) $
  Version            : $Revision: 14485 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.setup;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;

public class TransEquipmentBean extends com.mes.screens.SequenceDataBean
{
  private HashMap desModMap                               = new HashMap();
  private HashMap desAppMap                               = new HashMap();
  private HashMap modDesMap                               = new HashMap();
  private HashMap appDesMap                               = new HashMap();
  private HashMap modelCheckNeed                          = new HashMap();
  private HashMap modelCheckHave                          = new HashMap();

  public final static String  YES                         = "Y";
  public final static String  NO                          = "N";
  public final static String  TRANS_PHONE_TRAINING        = "T";
  public final static String  MES_PHONE_TRAINING          = "M";
  public final static String  OTHER_PHONE_TRAINING        = "O";


  public final static int NUM_OF_EQUIP                    = 129;
  public final static int NUM_OF_MODS                     = 65;
  public final static int NUM_OF_TYPES                    = 7;
  public final static int NUM_OF_APPS                     = 17;

  public final static int EQUIP_TYPE_IMPRINTER_PLATE      = 9;

  public final static String BUSINESS_ADD_TYPE            = "1";
  public final static String MAILING_ADD_TYPE             = "2";
  public final static String EQUIP_ADD_TYPE               = "3";
  public final static String INVALID_ADD_TYPE             = "-1";

  private final static Integer RETAIL                     = 1;
  private final static Integer RESTAURANT                 = 2;
  private final static Integer FINE_DINING                = 3;
  private final static Integer HOTEL                      = 4;
  private final static Integer CASH_ADVANCE               = 10;
  private final static Integer MOTEL                      = 11;
  private final static Integer DEBIT_TRACK1               = 8;
  private final static Integer DEBIT_TRACK2               = 9;
  private final static Integer RETAIL_TRACK1              = 12;
  private final static Integer RETAIL_TRACK2              = 13;
  private final static Integer LAN                        = 14;
  private final static Integer RESTAURANT_TRACK1          = 15;
  private final static Integer RESTAURANT_TRACK2          = 16;
  private final static Integer MOTEL_TRACK1               = 17;
  private final static Integer MOTEL_TRACK2               = 18;
  private final static Integer CASH_ADVANCE_TRACK2_ONLY   = 19;
  private final static Integer RETAIL_NO_DEBIT_CDPD_ONLY  = 20;

  private final static int BUY                            = mesConstants.APP_EQUIP_PURCHASE;
  private final static int RENT                           = mesConstants.APP_EQUIP_RENT;
  private final static int OWN                            = mesConstants.APP_EQUIP_OWNED;
  private final static int LEASE                          = mesConstants.APP_EQUIP_LEASE;

  private String[] equipDesc                              = new String[NUM_OF_EQUIP];
  private String[] equipDesc2                             = new String[NUM_OF_MODS];
  private String[] equipMod                               = new String[NUM_OF_MODS];
  private String[] appDesc                                = new String[NUM_OF_APPS];

  public String[] terminalPrinterNeed                     = new String[27];
  public String[] terminalPinpadNeed                      = new String[2];
  public String[] terminalNeed                            = new String[11];
  public String[] printersNeed                            = new String[3];
  public String[] pinpadsNeed                             = new String[5];
  public String[] imprintersNeed                          = new String[1];
  public String[] peripheralsNeed                         = new String[3];

  public String[] terminalPrinterOwn                      = new String[45];
  public String[] terminalPinpadOwn                       = new String[4];
  public String[] terminalOwn                             = new String[33];
  public String[] printersOwn                             = new String[16];
  public String[] pinpadsOwn                              = new String[6];
  public String[] imprintersOwn                           = new String[1];
  public String[] peripheralsOwn                          = new String[3];

  private String[] equipType                              = new String[NUM_OF_TYPES];
  private String[] equipTypeCode                          = new String[NUM_OF_TYPES];
  //variables store form data
  private String[] equipNeeded                            = new String[NUM_OF_TYPES];
  private String[] equipOwned                             = new String[NUM_OF_TYPES];
  private String[] equipNumRent                           = new String[NUM_OF_TYPES];
  private String[] equipNumBuy                            = new String[NUM_OF_TYPES];
  private String[] equipNumLease                          = new String[NUM_OF_TYPES];
  private String[] equipNumOwn                            = new String[NUM_OF_TYPES];

  private String[] equipAmtRent                           = new String[NUM_OF_TYPES];
  private String[] equipAmtBuy                            = new String[NUM_OF_TYPES];
  private String[] equipAmtOwn                            = new String[NUM_OF_TYPES];
  private String[] equipAmtLease                          = new String[NUM_OF_TYPES];


  public Vector need                                      = new Vector();
  public Vector own                                       = new Vector();

  private String section1Rent                             = "";
  private String section2Rent                             = "";
  private String section3Rent                             = "";
  private String section4Rent                             = "";
  private String section5Rent                             = "";
  private String section1Buy                              = "";
  private String section2Buy                              = "";
  private String section3Buy                              = "";
  private String section4Buy                              = "";
  private String section5Buy                              = "";
  private String section1Lease                            = "";
  private String section2Lease                            = "";
  private String section3Lease                            = "";
  private String section4Lease                            = "";
  private String section5Lease                            = "";
  private String section1Own                              = "";
  private String section2Own                              = "";
  private String section3Own                              = "";
  private String section4Own                              = "";
  private String section5Own                              = "";

  private String section1Need                             = "";
  private String section2Need                             = "";
  private String section3Need                             = "";
  private String section4Need                             = "";
  private String section5Need                             = "";
  private String section1Have                             = "";
  private String section2Have                             = "";
  private String section3Have                             = "";
  private String section4Have                             = "";
  private String section5Have                             = "";

  private String accessCodeFlag                           = "N";
  private String accessCodeComment                        = "-1";
  private String autoBatchFlag                            = "N";
  private String autoBatchHhComment                       = "-1";
  private String autoBatchMmComment                       = "-1";
  private String autoBatchAmPmComment                     = "-1";
  private String headerLine4Flag                          = "N";
  private String headerLine4Comment                       = "-1";
  private String headerLine5Flag                          = "N";
  private String headerLine5Comment                       = "-1";
  private String footerFlag                               = "N";
  private String footerComment                            = "-1";
  private String resetReferenceFlag                       = "Y";
  private String invoicePromptFlag                        = "N";
  private String fraudControlFlag                         = "N";
  private String avsOnFlag                                = "N";
  private String cashBackFlag                             = "N";
  private String debitSurchargeFlag                       = "N";

  private String purchasingCardFlag                       = "N";
  private String truncationCardFlag                       = "N";

  private String passwordProtectFlag                      = "N";
  private String phoneTrainingFlag                        = "Y";
  private String terminalReminderFlag                     = "N";
  private String terminalReminderHhComment                = "-1";
  private String terminalReminderMmComment                = "-1";
  private String terminalReminderAmPmComment              = "-1";
  private String equipmentComment                         = "-1";
  private String tipOptionFlag                            = "N";
  private String clerkEnabledFlag                         = "N";
  private String fineDiningFlag                           = "N";

  private String phoneTraining                            = "";
  private String cashBackLimit                            = "";
  private String debitSurchargeAmount                     = "";
  private String terminalInsurance                        = "N";
  private String terminalTrack                            = "";
  private String terminalPrinter                          = "";
  private String customerServicePhone                     = "";

  private String useAddress                               = INVALID_ADD_TYPE;
  private String shippingName                             = "";
  private String shippingContactName                      = "";
  private String shippingAddress1                         = "";
  private String shippingAddress2                         = "";
  private String shippingCity                             = "";
  private String shippingState                            = "-1";
  private String shippingZip                              = "";
  private String shippingPhone                            = "";
  private String shippingMethod                           = "";
  private String shippingComment                          = "";
  private boolean dialPay                                 = false;
  private boolean retail                                  = false;
  private boolean moto                                    = false;
  private String  businessPhone                           = "";
  private String  businessPhoneRaw                        = "";

  private int     needSectionNum                          = 1;
  private int     haveSectionNum                          = 1;

  private int     imprinterPlates                         = 0;
  private int     appType                                 = 0;
  private String  leaseCompany                            = "";
  private String  leaseActNum                             = "";
  private String  leaseAmt                                = "";
  private String  leaseMonths                             = "";
  private String  fundingAmt                              = "";

  private String  terminalApplication                     = "";

  private boolean leasingEquip                            = false;
  private Vector  monthOptions                            = new Vector();
  private Vector  companyOptions                          = new Vector();
  public  Vector  method                                  = new Vector();
  public  Vector  methodDesc                              = new Vector();
  public  Vector  descriptions                            = new Vector();
  public  Vector  models                                  = new Vector();
  public  Vector  descriptionsDial                        = new Vector();
  public  Vector  modelsDial                              = new Vector();
  public  Vector  descriptionsOwned                       = new Vector();
  public  Vector  modelsOwned                             = new Vector();
  public  Vector  descriptionsOwnedDial                   = new Vector();
  public  Vector  modelsOwnedDial                         = new Vector();
  public  Vector  termApps                                = new Vector();

  private int     ipTerminal                              = mesConstants.TERM_COMM_TYPE_DIAL;

  public TransEquipmentBean()
  {
    try // this try/catch block is to make sure database connections are cleaned up
    {
      PreparedStatement ps      = null;
      ResultSet         rs      = null;
      try
      {
        StringBuffer      qs      = new StringBuffer("");

        qs.append("select equip_model,equip_description ");
        qs.append("from equipment_application where app_type = 8 ");
        qs.append("order by equip_description");

        ps = getPreparedStatement(qs.toString());

        rs = ps.executeQuery();

        while(rs.next())
        {
           descriptions.add(rs.getString("equip_description"));
           models.add(rs.getString("equip_model"));
        }
        rs.close();
        ps.close();
      }
      catch(Exception e)
      {
        addError("Couldn't get equipment list");
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { ps.close(); } catch(Exception e) {}
      }

      try
      {
        StringBuffer      qs      = new StringBuffer("");

        qs.append("select equip_model,equip_description ");
        qs.append("from equipment_application where app_type = 8 and equip_type = 4 ");
        qs.append("order by equip_description");

        ps = getPreparedStatement(qs.toString());

        rs = ps.executeQuery();

        while(rs.next())
        {
           descriptionsDial.add(rs.getString("equip_description"));
           modelsDial.add(rs.getString("equip_model"));
        }
        rs.close();
        ps.close();
      }
      catch(Exception e)
      {
        addError("Couldn't get equipment list b");
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { ps.close(); } catch(Exception e) {}
      }

      try
      {
        StringBuffer      qs      = new StringBuffer("");

        qs.append("select equip_model,equip_description ");
        qs.append("from equipment_application where app_type = -1 ");
        qs.append("order by equip_description");

        ps = getPreparedStatement(qs.toString());

        rs = ps.executeQuery();

        while(rs.next())
        {
           descriptionsOwned.add(rs.getString("equip_description"));
           modelsOwned.add(rs.getString("equip_model"));
        }
        rs.close();
        ps.close();
      }
      catch(Exception e)
      {
        addError("Couldn't get equipment list c");
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { ps.close(); } catch(Exception e) {}
      }

      try
      {
        StringBuffer      qs      = new StringBuffer("");

        qs.append("select equip_model,equip_description ");
        qs.append("from equipment_application where app_type = -1 and equip_type = 4 ");
        qs.append("order by equip_description");

        ps = getPreparedStatement(qs.toString());

        rs = ps.executeQuery();

        while(rs.next())
        {
           descriptionsOwnedDial.add(rs.getString("equip_description"));
           modelsOwnedDial.add(rs.getString("equip_model"));
        }
        rs.close();
        ps.close();
      }
      catch(Exception e)
      {
        addError("Couldn't get equipment list d");
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { ps.close(); } catch(Exception e) {}
      }
    }
    catch(Exception e)
    {
    }
    finally
    {
      if(!isConnectionStale())
      {
        cleanUp();
      }
    }

    termApps.add("Retail");
    termApps.add("Restaurant");
    //termApps.add("Fine Dining");
    termApps.add("Lodging");
    termApps.add("Cash Advance");


    equipDesc[0]   = "Hypercom T7P/Roll-Retail";
    equipDesc[1]   = "Hypercom T7P/Roll-Restaurant";
    equipDesc[2]   = "Hypercom T7P/Roll-Fine Dining";
    equipDesc[3]   = "Hypercom T7P/Roll-Hotel";
    equipDesc[4]   = "Hypercom T7P/Thermal-Retail";
    equipDesc[5]   = "Hypercom T7P/Thermal-Restaurant";
    equipDesc[6]   = "Hypercom T7P/Thermal-Fine Dining";
    equipDesc[7]   = "Hypercom T7P/Thermal-Hotel";
    equipDesc[8]   = "VeriFone Omni 3200-Retail";
    equipDesc[9]   = "VeriFone Omni 3200-Restaurant";
    equipDesc[10]  = "VeriFone Tranz 460-Retail";
    equipDesc[11]  = "VeriFone Tranz 460-Restaurant";
    equipDesc[12]  = "VeriFone Omni 470-Retail";
    equipDesc[13]  = "VeriFone Omni 470-Restaurant";
    equipDesc[14]  = "VeriFone T380-Retail";
    equipDesc[15]  = "VeriFone T380-Restaurant";
    equipDesc[16]  = "VeriFone T380-Hotel";
    equipDesc[17]  = "VeriFone Roll 250";
    equipDesc[18]  = "VeriFone Roll 900";
    equipDesc[19]  = "Hypercom S8";
    equipDesc[20]  = "VeriFone PinPad 1000";
    equipDesc[21]  = "VeriFone Omni 395-Retail";
    equipDesc[22]  = "VeriFone Omni 395-Restaurant";
    equipDesc[23]  = "VeriFone Omni 396-Retail";
    equipDesc[24]  = "VeriFone Omni 396-Restaurant";
    equipDesc[25]  = "Hypercom T77/Roll-Retail";
    equipDesc[26]  = "Hypercom T77/Roll-Restaurant";
    equipDesc[27]  = "Hypercom T77/Roll-Fine Dining";
    equipDesc[28]  = "Hypercom T77/Roll-Hotel";
    equipDesc[29]  = "VeriFone T330-Retail";
    equipDesc[30]  = "VeriFone T330-Restaurant";
    equipDesc[31]  = "VeriFone T330-Motel";
    equipDesc[32]  = "VeriFone T330-Cash Advance Track 2 Only";
    equipDesc[33]  = "VeriFone Zon XL-Retail";
    equipDesc[34]  = "VeriFone Zon XL-Cash Advance";
    equipDesc[35]  = "Hypercom P7 Roll";
    equipDesc[36]  = "Hypercom P8 Roll";
    equipDesc[37]  = "Hypercom P7 Sprocket";
    equipDesc[38]  = "Hypercom P8 Sprocket";
    equipDesc[39]  = "VeriFone Printer 220";
    equipDesc[40]  = "VeriFone Slip Printer 150";
    equipDesc[41]  = "DataCard Silent Partner";
    equipDesc[42]  = "Hypercom S7";
    equipDesc[43]  = "VeriFone PinPad 102";
    equipDesc[44]  = "VeriFone PinPad 301";
    equipDesc[45]  = "Standard Imprinter Model";
    equipDesc[46]  = "Hypercom T7E-Retail";
    equipDesc[47]  = "Hypercom T7E-Restaurant";
    equipDesc[48]  = "Hypercom T7E-Fine Dining";
    equipDesc[49]  = "Hypercom T7E-Hotel";
    equipDesc[50]  = "Dassault Talento T-One-Retail";
    equipDesc[51]  = "Dassault Talento T-One-Restaurant";
    equipDesc[52]  = "Hypercom T77/Sprocket-Retail";
    equipDesc[53]  = "Hypercom T77/Sprocket-Restaurant";
    equipDesc[54]  = "Hypercom T77/Sprocket-Fine Dining";
    equipDesc[55]  = "Hypercom T77/Sprocket-Hotel";
    equipDesc[56]  = "Hypercom T77/Thermal-Retail";
    equipDesc[57]  = "Hypercom T77/Thermal-Restaurant";
    equipDesc[58]  = "Hypercom T77/Thermal-Fine Dining";
    equipDesc[59]  = "Hypercom T77/Thermal-Hotel";
    equipDesc[60]  = "IVI eN-Counter 4000-Retail";
    equipDesc[61]  = "IVI eN-Counter 4000-Restaurant";
    equipDesc[62]  = "VeriFone Omni 3300-Retail";
    equipDesc[63]  = "VeriFone Omni 3300-Restaurant";
    equipDesc[64]  = "Hypercom T7P/Sprocket-Retail";
    equipDesc[65]  = "Hypercom T7P/Sprocket-Restaurant";
    equipDesc[66]  = "Hypercom T7P/Sprocket-Fine Dining";
    equipDesc[67]  = "Hypercom T7P/Sprocket-Hotel";
    equipDesc[68]  = "Dassault Talento T-IPP-Retail";
    equipDesc[69]  = "Dassault Talento T-IPP-Restaurant";
    equipDesc[70]  = "Dassault Talento T-IPPS-Retail";
    equipDesc[71]  = "Dassault Talento T-IPPS-Restaurant";
    equipDesc[72]  = "Hypercom T7E-Retail";
    equipDesc[73]  = "Hypercom T7E-Restaurant";
    equipDesc[74]  = "Hypercom T7E-Fine Dining";
    equipDesc[75]  = "Hypercom T7E-Hotel";
    equipDesc[76]  = "VeriFone T380 X2-Retail";
    equipDesc[77]  = "VeriFone T380 X2-Restaurant";
    equipDesc[78]  = "Hypercom T8-Retail";
    equipDesc[79]  = "Hypercom T8-Restaurant";
    equipDesc[80]  = "Hypercom T8-Fine Dining";
    equipDesc[81]  = "Hypercom T8-Hotel";
    equipDesc[82]  = "VeriFone T330-Debit Track 1";
    equipDesc[83]  = "VeriFone T330-Debit Track 2";
    equipDesc[84]  = "VeriFone T380-Cash Advance";
    equipDesc[85]  = "Omron CAT 250-Retail";
    equipDesc[86]  = "Dassault T-Pad";
    equipDesc[87]  = "Hypercom S8D - short cord";
    equipDesc[88]  = "Hypercom S8D - long cord";
    equipDesc[89]  = "VeriFone PinPad 2000";
    equipDesc[90]  = "Citizen IDP 560 Roll";
    equipDesc[91]  = "Citizen IDP 562 Roll";
    equipDesc[92]  = "Citizen IDP 3530 Roll";
    equipDesc[93]  = "IVI Scribe 612";
    equipDesc[94]  = "Omron 053 Slip";
    equipDesc[95]  = "Omron 055 Roll";
    equipDesc[96]  = "VeriFone Printer 250";
    equipDesc[97]  = "VeriFone Roll 300";
    equipDesc[98]  = "VeriFone PrintPak 350";
    equipDesc[99]  = "VeriFone PrintPak 355";
    equipDesc[100] = "VeriFone Printer 600";
    equipDesc[101] = "VeriFone Printer 900R";
    equipDesc[102] = "Nurit 2085-Retail";
    equipDesc[103] = "Nurit 2085-Restaurant";
    equipDesc[104] = "VeriFone T330-Retail Track 1";
    equipDesc[105] = "VeriFone T330-Retail Track 2";
    equipDesc[106] = "Hypercom T7P/Roll-Cash Advance";
    equipDesc[107] = "Hypercom T7P/Sprocket-Cash Advance";
    equipDesc[108] = "Hypercom T7P/Thermal-Cash Advance";
    equipDesc[109] = "Hypercom T77/Roll-Cash Advance";
    equipDesc[110] = "Hypercom T77/Sprocket-Cash Advance";
    equipDesc[111] = "Hypercom T77/Thermal-Cash Advance";
    equipDesc[112] = "IVI eN-Counter 4000-LAN";
    equipDesc[113] = "Hypercom T7E-Cash Advance";
    equipDesc[114] = "VeriFone T330-Restaurant Track 1";
    equipDesc[115] = "VeriFone T330-Restaurant Track 2";
    equipDesc[116] = "VeriFone T330-Motel Track 1";
    equipDesc[117] = "VeriFone T330-Motel Track 2";
    equipDesc[118] = "Hypercom T8-Cash Advance";
    equipDesc[119] = "VeriFone CR600 Checkreader";
    equipDesc[120] = "MagTek-Mini Wedge";
    equipDesc[121] = "MagTek-Wedge";
    equipDesc[122] = "VeriFone XL 300-Retail";
    equipDesc[123] = "VeriFone XL 300-Cash Advance";
    equipDesc[124] = "Dassault Artema-Retail (No Debit) CDPD Only";
    equipDesc[125] = "Nurit 3010-Retail (No Debit) CDPD Only";
    equipDesc[126] = "VeriFone PrintPak 350/355";
    equipDesc[127] = "VeriFone PinPad 102/1000";
    equipDesc[128] = "VeriFone PinPad 301/2000";


    equipDesc2[0]  = "Hypercom T7P/Roll";
    equipDesc2[1]  = "Hypercom T7P/Thermal";
    equipDesc2[2]  = "VeriFone Omni 3200";
    equipDesc2[3]  = "VeriFone Tranz 460";
    equipDesc2[4]  = "VeriFone Omni 470";
    equipDesc2[5]  = "VeriFone T380";
    equipDesc2[6]  = "VeriFone Roll 250";
    equipDesc2[7]  = "VeriFone Roll 900";
    equipDesc2[8]  = "Hypercom S8";
    equipDesc2[9]  = "VeriFone PinPad 1000";
    equipDesc2[10] = "VeriFone Omni 395";
    equipDesc2[11] = "VeriFone Omni 396";
    equipDesc2[12] = "Hypercom T77/Roll";
    equipDesc2[13] = "VeriFone T330";
    equipDesc2[14] = "VeriFone Zon XL";
    equipDesc2[15] = "Hypercom P7 Roll";
    equipDesc2[16] = "Hypercom P8 Roll";
    equipDesc2[17] = "Hypercom P7 Sprocket";
    equipDesc2[18] = "Hypercom P8 Sprocket";
    equipDesc2[19] = "VeriFone Printer 220";
    equipDesc2[20] = "VeriFone Slip Printer 150";
    equipDesc2[21] = "DataCard Silent Partner";
    equipDesc2[22] = "Hypercom S7";
    equipDesc2[23] = "VeriFone PinPad 102";
    equipDesc2[24] = "VeriFone PinPad 301";
    equipDesc2[25] = "Standard Imprinter Model";
    equipDesc2[26] = "Hypercom T7E";
    equipDesc2[27] = "Dassault Talento T-One";
    equipDesc2[28] = "Hypercom T77/Sprocket";
    equipDesc2[29] = "Hypercom T77/Thermal";
    equipDesc2[30] = "IVI eN-Counter 4000";
    equipDesc2[31] = "VeriFone Omni 3300";
    equipDesc2[32] = "Hypercom T7P/Sprocket";
    equipDesc2[33] = "Dassault Talento T-IPP";
    equipDesc2[34] = "Dassault Talento T-IPPS";
    equipDesc2[35] = "Hypercom T7E";
    equipDesc2[36] = "VeriFone T380 X2";
    equipDesc2[37] = "Hypercom T8";
    equipDesc2[38] = "Omron CAT 250";
    equipDesc2[39] = "Dassault T-Pad";
    equipDesc2[40] = "Hypercom S8D - short cord";
    equipDesc2[41] = "Hypercom S8D - long cord";
    equipDesc2[42] = "VeriFone PinPad 2000";
    equipDesc2[43] = "Citizen IDP 560 Roll";
    equipDesc2[44] = "Citizen IDP 562 Roll";
    equipDesc2[45] = "Citizen IDP 3530 Roll";
    equipDesc2[46] = "IVI Scribe 612";
    equipDesc2[47] = "Omron 053 Slip";
    equipDesc2[48] = "Omron 055 Roll";
    equipDesc2[49] = "VeriFone Printer 250";
    equipDesc2[50] = "VeriFone Roll 300";
    equipDesc2[51] = "VeriFone PrintPak 350";
    equipDesc2[52] = "VeriFone PrintPak 355";
    equipDesc2[53] = "VeriFone Printer 600";
    equipDesc2[54] = "VeriFone Printer 900R";
    equipDesc2[55] = "Nurit 2085";
    equipDesc2[56] = "VeriFone CR600 Checkreader";
    equipDesc2[57] = "MagTek-Mini Wedge";
    equipDesc2[58] = "MagTek-Wedge";
    equipDesc2[59] = "VeriFone XL 300";
    equipDesc2[60] = "Dassault Artema";
    equipDesc2[61] = "Nurit 3010";
    equipDesc2[62] = "VeriFone PrintPak 350/355";
    equipDesc2[63] = "VeriFone PinPad 102/1000";
    equipDesc2[64] = "VeriFone PinPad 301/2000";

    equipMod[0]  = "HPT7PRIP";
    equipMod[1]  = "HPT7PTIP";
    equipMod[2]  = "VFOMNI3200IP";
    equipMod[3]  = "VFTRANZ460IP";
    equipMod[4]  = "VFOMNI470IPPP";
    equipMod[5]  = "VFTRANZ380";
    equipMod[6]  = "VFR250";
    equipMod[7]  = "VFR900";
    equipMod[8]  = "HPS8";
    equipMod[9]  = "VF1000";
    equipMod[10] = "VFOMNI395IP";
    equipMod[11] = "VFOMNI396IP";
    equipMod[12] = "HPT77R";
    equipMod[13] = "VFTRANZ330";
    equipMod[14] = "VFZONXL";
    equipMod[15] = "HPP7R";
    equipMod[16] = "HPP8R";
    equipMod[17] = "HPP7SP";
    equipMod[18] = "HPP8SP";
    equipMod[19] = "VFP220";
    equipMod[20] = "VFS150";
    equipMod[21] = "DTCDP";
    equipMod[22] = "HPS7";
    equipMod[23] = "VF102";
    equipMod[24] = "VF301";
    equipMod[25] = "IP5";
    equipMod[26] = "HPT7EIP";
    equipMod[27] = "DSTT1";
    equipMod[28] = "HPT77S";
    equipMod[29] = "HPT77T";
    equipMod[30] = "IVIEN4000";
    equipMod[31] = "VFOMNI3300IP";
    equipMod[32] = "HPT7PSIP";
    equipMod[33] = "DSTIPP";
    equipMod[34] = "DSTIPPS";
    equipMod[35] = "HPT7EIP";
    equipMod[36] = "VFTRANZ380X2";
    equipMod[37] = "HPT8IP";
    equipMod[38] = "OCAT250";
    equipMod[39] = "DSTPAD";
    equipMod[40] = "HPS8DS";
    equipMod[41] = "HPS8DL";
    equipMod[42] = "VF2000";
    equipMod[43] = "CIDP560";
    equipMod[44] = "CIDP562";
    equipMod[45] = "CIDP3530";
    equipMod[46] = "IVIS612";
    equipMod[47] = "O053S";
    equipMod[48] = "O055R";
    equipMod[49] = "VFP250";
    equipMod[50] = "VFR300";
    equipMod[51] = "VFPP350";
    equipMod[52] = "VFPP355";
    equipMod[53] = "VFP600";
    equipMod[54] = "VFP900R";
    equipMod[55] = "NRT2085";
    equipMod[56] = "VFCR600";
    equipMod[57] = "MTMWEDGE";
    equipMod[58] = "MTWEDGE";
    equipMod[59] = "VFXL300";
    equipMod[60] = "DSART";
    equipMod[61] = "NRT3010";
    equipMod[62] = "VFPP350/355";
    equipMod[63] = "VF102/1000";
    equipMod[64] = "VF301/2000";

    appDesc[0]    = "-Retail";
    appDesc[1]    = "-Restaurant";
    appDesc[2]    = "-Fine Dining";
    appDesc[3]    = "-Hotel";
    appDesc[4]    = "-Cash Advance";
    appDesc[5]    = "-Motel";
    appDesc[6]    = "-Debit Track 1";
    appDesc[7]    = "-Debit Track 2";
    appDesc[8]    = "-Retail Track 1";
    appDesc[9]    = "-Retail Track 2";
    appDesc[10]   = "-LAN";
    appDesc[11]   = "-Restaurant Track 1";
    appDesc[12]   = "-Restaurant Track 2";
    appDesc[13]   = "-Motel Track 1";
    appDesc[14]   = "-Motel Track 2";
    appDesc[15]   = "-Cash Advance Track 2 Only";
    appDesc[16]   = "-Retail (No Debit) CDPD Only";

    modDesMap.put(equipMod[0], equipDesc2[0]);
    modDesMap.put(equipMod[1], equipDesc2[1]);
    modDesMap.put(equipMod[2], equipDesc2[2]);
    modDesMap.put(equipMod[3], equipDesc2[3]);
    modDesMap.put(equipMod[4], equipDesc2[4]);
    modDesMap.put(equipMod[5], equipDesc2[5]);
    modDesMap.put(equipMod[6], equipDesc2[6]);
    modDesMap.put(equipMod[7], equipDesc2[7]);
    modDesMap.put(equipMod[8], equipDesc2[8]);
    modDesMap.put(equipMod[9], equipDesc2[9]);
    modDesMap.put(equipMod[10], equipDesc2[10]);
    modDesMap.put(equipMod[11], equipDesc2[11]);
    modDesMap.put(equipMod[12], equipDesc2[12]);
    modDesMap.put(equipMod[13], equipDesc2[13]);
    modDesMap.put(equipMod[14], equipDesc2[14]);
    modDesMap.put(equipMod[15], equipDesc2[15]);
    modDesMap.put(equipMod[16], equipDesc2[16]);
    modDesMap.put(equipMod[17], equipDesc2[17]);
    modDesMap.put(equipMod[18], equipDesc2[18]);
    modDesMap.put(equipMod[19], equipDesc2[19]);
    modDesMap.put(equipMod[20], equipDesc2[20]);
    modDesMap.put(equipMod[21], equipDesc2[21]);
    modDesMap.put(equipMod[22], equipDesc2[22]);
    modDesMap.put(equipMod[23], equipDesc2[23]);
    modDesMap.put(equipMod[24], equipDesc2[24]);
    modDesMap.put(equipMod[25], equipDesc2[25]);
    modDesMap.put(equipMod[26], equipDesc2[26]);
    modDesMap.put(equipMod[27], equipDesc2[27]);
    modDesMap.put(equipMod[28], equipDesc2[28]);
    modDesMap.put(equipMod[29], equipDesc2[29]);
    modDesMap.put(equipMod[30], equipDesc2[30]);
    modDesMap.put(equipMod[31], equipDesc2[31]);
    modDesMap.put(equipMod[32], equipDesc2[32]);
    modDesMap.put(equipMod[33], equipDesc2[33]);
    modDesMap.put(equipMod[34], equipDesc2[34]);
    modDesMap.put(equipMod[35], equipDesc2[35]);
    modDesMap.put(equipMod[36], equipDesc2[36]);
    modDesMap.put(equipMod[37], equipDesc2[37]);
    modDesMap.put(equipMod[38], equipDesc2[38]);
    modDesMap.put(equipMod[39], equipDesc2[39]);
    modDesMap.put(equipMod[40], equipDesc2[40]);
    modDesMap.put(equipMod[41], equipDesc2[41]);
    modDesMap.put(equipMod[42], equipDesc2[42]);
    modDesMap.put(equipMod[43], equipDesc2[43]);
    modDesMap.put(equipMod[44], equipDesc2[44]);
    modDesMap.put(equipMod[45], equipDesc2[45]);
    modDesMap.put(equipMod[46], equipDesc2[46]);
    modDesMap.put(equipMod[47], equipDesc2[47]);
    modDesMap.put(equipMod[48], equipDesc2[48]);
    modDesMap.put(equipMod[49], equipDesc2[49]);
    modDesMap.put(equipMod[50], equipDesc2[50]);
    modDesMap.put(equipMod[51], equipDesc2[51]);
    modDesMap.put(equipMod[52], equipDesc2[52]);
    modDesMap.put(equipMod[53], equipDesc2[53]);
    modDesMap.put(equipMod[54], equipDesc2[54]);
    modDesMap.put(equipMod[55], equipDesc2[55]);
    modDesMap.put(equipMod[56], equipDesc2[56]);
    modDesMap.put(equipMod[57], equipDesc2[57]);
    modDesMap.put(equipMod[58], equipDesc2[58]);
    modDesMap.put(equipMod[59], equipDesc2[59]);
    modDesMap.put(equipMod[60], equipDesc2[60]);
    modDesMap.put(equipMod[61], equipDesc2[61]);
    modDesMap.put(equipMod[62], equipDesc2[62]);
    modDesMap.put(equipMod[63], equipDesc2[63]);
    modDesMap.put(equipMod[64], equipDesc2[64]);

    desModMap.put(equipDesc[0],   equipMod[0]);
    desModMap.put(equipDesc[1],   equipMod[0]);
    desModMap.put(equipDesc[2],   equipMod[0]);
    desModMap.put(equipDesc[3],   equipMod[0]);
    desModMap.put(equipDesc[4],   equipMod[1]);
    desModMap.put(equipDesc[5],   equipMod[1]);
    desModMap.put(equipDesc[6],   equipMod[1]);
    desModMap.put(equipDesc[7],   equipMod[1]);
    desModMap.put(equipDesc[8],   equipMod[2]);
    desModMap.put(equipDesc[9],   equipMod[2]);
    desModMap.put(equipDesc[10],  equipMod[3]);
    desModMap.put(equipDesc[11],  equipMod[3]);
    desModMap.put(equipDesc[12],  equipMod[4]);
    desModMap.put(equipDesc[13],  equipMod[4]);
    desModMap.put(equipDesc[14],  equipMod[5]);
    desModMap.put(equipDesc[15],  equipMod[5]);
    desModMap.put(equipDesc[16],  equipMod[5]);
    desModMap.put(equipDesc[17],  equipMod[6]);
    desModMap.put(equipDesc[18],  equipMod[7]);
    desModMap.put(equipDesc[19],  equipMod[8]);
    desModMap.put(equipDesc[20],  equipMod[9]);
    desModMap.put(equipDesc[21],  equipMod[10]);
    desModMap.put(equipDesc[22],  equipMod[10]);
    desModMap.put(equipDesc[23],  equipMod[11]);
    desModMap.put(equipDesc[24],  equipMod[11]);
    desModMap.put(equipDesc[25],  equipMod[12]);
    desModMap.put(equipDesc[26],  equipMod[12]);
    desModMap.put(equipDesc[27],  equipMod[12]);
    desModMap.put(equipDesc[28],  equipMod[12]);
    desModMap.put(equipDesc[29],  equipMod[13]);
    desModMap.put(equipDesc[30],  equipMod[13]);
    desModMap.put(equipDesc[31],  equipMod[13]);
    desModMap.put(equipDesc[32],  equipMod[13]);
    desModMap.put(equipDesc[33],  equipMod[14]);
    desModMap.put(equipDesc[34],  equipMod[14]);
    desModMap.put(equipDesc[35],  equipMod[15]);
    desModMap.put(equipDesc[36],  equipMod[16]);
    desModMap.put(equipDesc[37],  equipMod[17]);
    desModMap.put(equipDesc[38],  equipMod[18]);
    desModMap.put(equipDesc[39],  equipMod[19]);
    desModMap.put(equipDesc[40],  equipMod[20]);
    desModMap.put(equipDesc[41],  equipMod[21]);
    desModMap.put(equipDesc[42],  equipMod[22]);
    desModMap.put(equipDesc[43],  equipMod[23]);
    desModMap.put(equipDesc[44],  equipMod[24]);
    desModMap.put(equipDesc[45],  equipMod[25]);
    desModMap.put(equipDesc[46],  equipMod[26]);
    desModMap.put(equipDesc[47],  equipMod[26]);
    desModMap.put(equipDesc[48],  equipMod[26]);
    desModMap.put(equipDesc[49],  equipMod[26]);
    desModMap.put(equipDesc[50],  equipMod[27]);
    desModMap.put(equipDesc[51],  equipMod[27]);
    desModMap.put(equipDesc[52],  equipMod[28]);
    desModMap.put(equipDesc[53],  equipMod[28]);
    desModMap.put(equipDesc[54],  equipMod[28]);
    desModMap.put(equipDesc[55],  equipMod[28]);
    desModMap.put(equipDesc[56],  equipMod[29]);
    desModMap.put(equipDesc[57],  equipMod[29]);
    desModMap.put(equipDesc[58],  equipMod[29]);
    desModMap.put(equipDesc[59],  equipMod[29]);
    desModMap.put(equipDesc[60],  equipMod[30]);
    desModMap.put(equipDesc[61],  equipMod[30]);
    desModMap.put(equipDesc[62],  equipMod[31]);
    desModMap.put(equipDesc[63],  equipMod[31]);
    desModMap.put(equipDesc[64],  equipMod[32]);
    desModMap.put(equipDesc[65],  equipMod[32]);
    desModMap.put(equipDesc[66],  equipMod[32]);
    desModMap.put(equipDesc[67],  equipMod[32]);
    desModMap.put(equipDesc[68],  equipMod[33]);
    desModMap.put(equipDesc[69],  equipMod[33]);
    desModMap.put(equipDesc[70],  equipMod[34]);
    desModMap.put(equipDesc[71],  equipMod[34]);
    desModMap.put(equipDesc[72],  equipMod[35]);
    desModMap.put(equipDesc[73],  equipMod[35]);
    desModMap.put(equipDesc[74],  equipMod[35]);
    desModMap.put(equipDesc[75],  equipMod[35]);
    desModMap.put(equipDesc[76],  equipMod[36]);
    desModMap.put(equipDesc[77],  equipMod[36]);
    desModMap.put(equipDesc[78],  equipMod[37]);
    desModMap.put(equipDesc[79],  equipMod[37]);
    desModMap.put(equipDesc[80],  equipMod[37]);
    desModMap.put(equipDesc[81],  equipMod[37]);
    desModMap.put(equipDesc[82],  equipMod[13]);
    desModMap.put(equipDesc[83],  equipMod[13]);
    desModMap.put(equipDesc[84],  equipMod[5]);
    desModMap.put(equipDesc[85],  equipMod[38]);
    desModMap.put(equipDesc[86],  equipMod[39]);
    desModMap.put(equipDesc[87],  equipMod[40]);
    desModMap.put(equipDesc[88],  equipMod[41]);
    desModMap.put(equipDesc[89],  equipMod[42]);
    desModMap.put(equipDesc[90],  equipMod[43]);
    desModMap.put(equipDesc[91],  equipMod[44]);
    desModMap.put(equipDesc[92],  equipMod[45]);
    desModMap.put(equipDesc[93],  equipMod[46]);
    desModMap.put(equipDesc[94],  equipMod[47]);
    desModMap.put(equipDesc[95],  equipMod[48]);
    desModMap.put(equipDesc[96],  equipMod[49]);
    desModMap.put(equipDesc[97],  equipMod[50]);
    desModMap.put(equipDesc[98],  equipMod[51]);
    desModMap.put(equipDesc[99],  equipMod[52]);
    desModMap.put(equipDesc[100], equipMod[53]);
    desModMap.put(equipDesc[101], equipMod[54]);
    desModMap.put(equipDesc[102], equipMod[55]);
    desModMap.put(equipDesc[103], equipMod[55]);
    desModMap.put(equipDesc[104], equipMod[13]);
    desModMap.put(equipDesc[105], equipMod[13]);
    desModMap.put(equipDesc[106], equipMod[0]);
    desModMap.put(equipDesc[107], equipMod[32]);
    desModMap.put(equipDesc[108], equipMod[1]);
    desModMap.put(equipDesc[109], equipMod[12]);
    desModMap.put(equipDesc[110], equipMod[28]);
    desModMap.put(equipDesc[111], equipMod[29]);
    desModMap.put(equipDesc[112], equipMod[30]);
    desModMap.put(equipDesc[113], equipMod[26]);
    desModMap.put(equipDesc[114], equipMod[13]);
    desModMap.put(equipDesc[115], equipMod[13]);
    desModMap.put(equipDesc[116], equipMod[13]);
    desModMap.put(equipDesc[117], equipMod[13]);
    desModMap.put(equipDesc[118], equipMod[37]);
    desModMap.put(equipDesc[119], equipMod[56]);
    desModMap.put(equipDesc[120], equipMod[57]);
    desModMap.put(equipDesc[121], equipMod[58]);
    desModMap.put(equipDesc[122], equipMod[59]);
    desModMap.put(equipDesc[123], equipMod[59]);
    desModMap.put(equipDesc[124], equipMod[60]);
    desModMap.put(equipDesc[125], equipMod[61]);
    desModMap.put(equipDesc[126], equipMod[62]);
    desModMap.put(equipDesc[127], equipMod[63]);
    desModMap.put(equipDesc[128], equipMod[64]);


    desAppMap.put(equipDesc[0],   RETAIL);
    desAppMap.put(equipDesc[1],   RESTAURANT);
    desAppMap.put(equipDesc[2],   FINE_DINING);
    desAppMap.put(equipDesc[3],   HOTEL);
    desAppMap.put(equipDesc[4],   RETAIL);
    desAppMap.put(equipDesc[5],   RESTAURANT);
    desAppMap.put(equipDesc[6],   FINE_DINING);
    desAppMap.put(equipDesc[7],   HOTEL);
    desAppMap.put(equipDesc[8],   RETAIL);
    desAppMap.put(equipDesc[9],   RESTAURANT);
    desAppMap.put(equipDesc[10],  RETAIL);
    desAppMap.put(equipDesc[11],  RESTAURANT);
    desAppMap.put(equipDesc[12],  RETAIL);
    desAppMap.put(equipDesc[13],  RESTAURANT);
    desAppMap.put(equipDesc[14],  RETAIL);
    desAppMap.put(equipDesc[15],  RESTAURANT);
    desAppMap.put(equipDesc[16],  HOTEL);
    desAppMap.put(equipDesc[21],  RETAIL);
    desAppMap.put(equipDesc[22],  RESTAURANT);
    desAppMap.put(equipDesc[23],  RETAIL);
    desAppMap.put(equipDesc[24],  RESTAURANT);
    desAppMap.put(equipDesc[25],  RETAIL);
    desAppMap.put(equipDesc[26],  RESTAURANT);
    desAppMap.put(equipDesc[27],  FINE_DINING);
    desAppMap.put(equipDesc[28],  HOTEL);
    desAppMap.put(equipDesc[29],  RETAIL);
    desAppMap.put(equipDesc[30],  RESTAURANT);
    desAppMap.put(equipDesc[31],  MOTEL);
    desAppMap.put(equipDesc[32],  CASH_ADVANCE_TRACK2_ONLY);
    desAppMap.put(equipDesc[33],  RETAIL);
    desAppMap.put(equipDesc[34],  CASH_ADVANCE);
    desAppMap.put(equipDesc[46],  RETAIL);
    desAppMap.put(equipDesc[47],  RESTAURANT);
    desAppMap.put(equipDesc[48],  FINE_DINING);
    desAppMap.put(equipDesc[49],  HOTEL);
    desAppMap.put(equipDesc[50],  RETAIL);
    desAppMap.put(equipDesc[51],  RESTAURANT);
    desAppMap.put(equipDesc[52],  RETAIL);
    desAppMap.put(equipDesc[53],  RESTAURANT);
    desAppMap.put(equipDesc[54],  FINE_DINING);
    desAppMap.put(equipDesc[55],  HOTEL);
    desAppMap.put(equipDesc[56],  RETAIL);
    desAppMap.put(equipDesc[57],  RESTAURANT);
    desAppMap.put(equipDesc[58],  FINE_DINING);
    desAppMap.put(equipDesc[59],  HOTEL);
    desAppMap.put(equipDesc[60],  RETAIL);
    desAppMap.put(equipDesc[61],  RESTAURANT);
    desAppMap.put(equipDesc[62],  RETAIL);
    desAppMap.put(equipDesc[63],  RESTAURANT);
    desAppMap.put(equipDesc[64],  RETAIL);
    desAppMap.put(equipDesc[65],  RESTAURANT);
    desAppMap.put(equipDesc[66],  FINE_DINING);
    desAppMap.put(equipDesc[67],  HOTEL);
    desAppMap.put(equipDesc[68],  RETAIL);
    desAppMap.put(equipDesc[69],  RESTAURANT);
    desAppMap.put(equipDesc[70],  RETAIL);
    desAppMap.put(equipDesc[71],  RESTAURANT);
    desAppMap.put(equipDesc[72],  RETAIL);
    desAppMap.put(equipDesc[73],  RESTAURANT);
    desAppMap.put(equipDesc[74],  FINE_DINING);
    desAppMap.put(equipDesc[75],  HOTEL);
    desAppMap.put(equipDesc[76],  RETAIL);
    desAppMap.put(equipDesc[77],  RESTAURANT);
    desAppMap.put(equipDesc[78],  RETAIL);
    desAppMap.put(equipDesc[79],  RESTAURANT);
    desAppMap.put(equipDesc[80],  FINE_DINING);
    desAppMap.put(equipDesc[81],  HOTEL);
    desAppMap.put(equipDesc[82],  DEBIT_TRACK1);
    desAppMap.put(equipDesc[83],  DEBIT_TRACK2);
    desAppMap.put(equipDesc[84],  CASH_ADVANCE);
    desAppMap.put(equipDesc[85],  RETAIL);
    desAppMap.put(equipDesc[102], RETAIL);
    desAppMap.put(equipDesc[103], RESTAURANT);
    desAppMap.put(equipDesc[104], RETAIL_TRACK1);
    desAppMap.put(equipDesc[105], RETAIL_TRACK2);
    desAppMap.put(equipDesc[106], CASH_ADVANCE);
    desAppMap.put(equipDesc[107], CASH_ADVANCE);
    desAppMap.put(equipDesc[108], CASH_ADVANCE);
    desAppMap.put(equipDesc[109], CASH_ADVANCE);
    desAppMap.put(equipDesc[110], CASH_ADVANCE);
    desAppMap.put(equipDesc[111], CASH_ADVANCE);
    desAppMap.put(equipDesc[112], LAN);
    desAppMap.put(equipDesc[113], CASH_ADVANCE);
    desAppMap.put(equipDesc[114], RESTAURANT_TRACK1);
    desAppMap.put(equipDesc[115], RESTAURANT_TRACK2);
    desAppMap.put(equipDesc[116], MOTEL_TRACK1);
    desAppMap.put(equipDesc[117], MOTEL_TRACK2);
    desAppMap.put(equipDesc[118], CASH_ADVANCE);
    desAppMap.put(equipDesc[122], RETAIL);
    desAppMap.put(equipDesc[123], CASH_ADVANCE);
    desAppMap.put(equipDesc[124], RETAIL_NO_DEBIT_CDPD_ONLY);
    desAppMap.put(equipDesc[125], RETAIL_NO_DEBIT_CDPD_ONLY);


    appDesMap.put(RETAIL,                     appDesc[0]);
    appDesMap.put(RESTAURANT,                 appDesc[1]);
    appDesMap.put(FINE_DINING,                appDesc[2]);
    appDesMap.put(HOTEL,                      appDesc[3]);
    appDesMap.put(CASH_ADVANCE,               appDesc[4]);
    appDesMap.put(MOTEL,                      appDesc[5]);
    appDesMap.put(DEBIT_TRACK1,               appDesc[6]);
    appDesMap.put(DEBIT_TRACK2,               appDesc[7]);
    appDesMap.put(RETAIL_TRACK1,              appDesc[8]);
    appDesMap.put(RETAIL_TRACK2,              appDesc[9]);
    appDesMap.put(LAN,                        appDesc[10]);
    appDesMap.put(RESTAURANT_TRACK1,          appDesc[11]);
    appDesMap.put(RESTAURANT_TRACK2,          appDesc[12]);
    appDesMap.put(MOTEL_TRACK1,               appDesc[13]);
    appDesMap.put(MOTEL_TRACK2,               appDesc[14]);
    appDesMap.put(CASH_ADVANCE_TRACK2_ONLY,   appDesc[15]);
    appDesMap.put(RETAIL_NO_DEBIT_CDPD_ONLY,  appDesc[16]);

    terminalPrinterNeed[0]      = equipDesc[50];
    terminalPrinterNeed[1]      = equipDesc[51];
    terminalPrinterNeed[2]      = equipDesc[124];
    terminalPrinterNeed[3]      = equipDesc[25];
    terminalPrinterNeed[4]      = equipDesc[26];
    terminalPrinterNeed[5]      = equipDesc[27];
    terminalPrinterNeed[6]      = equipDesc[28];
    terminalPrinterNeed[7]      = equipDesc[109];
    terminalPrinterNeed[8]      = equipDesc[52];
    terminalPrinterNeed[9]      = equipDesc[53];
    terminalPrinterNeed[10]     = equipDesc[54];
    terminalPrinterNeed[11]     = equipDesc[55];
    terminalPrinterNeed[12]     = equipDesc[110];
    terminalPrinterNeed[13]     = equipDesc[0];
    terminalPrinterNeed[14]     = equipDesc[1];
    terminalPrinterNeed[15]     = equipDesc[2];
    terminalPrinterNeed[16]     = equipDesc[3];
    terminalPrinterNeed[17]     = equipDesc[106];
    terminalPrinterNeed[18]     = equipDesc[4];
    terminalPrinterNeed[19]     = equipDesc[5];
    terminalPrinterNeed[20]     = equipDesc[6];
    terminalPrinterNeed[21]     = equipDesc[7];
    terminalPrinterNeed[22]     = equipDesc[108];
    terminalPrinterNeed[23]     = equipDesc[8];
    terminalPrinterNeed[24]     = equipDesc[9];
    terminalPrinterNeed[25]     = equipDesc[10];
    terminalPrinterNeed[26]     = equipDesc[11];

    terminalPinpadNeed[0]       = equipDesc[12];
    terminalPinpadNeed[1]       = equipDesc[13];

    terminalNeed[0]             = equipDesc[14];
    terminalNeed[1]             = equipDesc[15];
    terminalNeed[2]             = equipDesc[16];
    terminalNeed[3]             = equipDesc[84];
    terminalNeed[4]             = equipDesc[72];
    terminalNeed[5]             = equipDesc[73];
    terminalNeed[6]             = equipDesc[74];
    terminalNeed[7]             = equipDesc[75];
    terminalNeed[8]             = equipDesc[113];

    terminalNeed[9]             = equipDesc[76];
    terminalNeed[10]            = equipDesc[77];


    printersNeed[0]             = equipDesc[17];
    printersNeed[1]             = equipDesc[18];
    printersNeed[2]             = equipDesc[38];

    pinpadsNeed[0]              = equipDesc[86];
    pinpadsNeed[1]              = equipDesc[87];
    pinpadsNeed[2]              = equipDesc[88];
    pinpadsNeed[3]              = equipDesc[20];
    pinpadsNeed[4]              = equipDesc[89];

    imprintersNeed[0]           = equipDesc[45];

    peripheralsNeed[0]          = equipDesc[119];
    peripheralsNeed[1]          = equipDesc[120];
    peripheralsNeed[2]          = equipDesc[121];


    terminalPrinterOwn[0]       = equipDesc[50];
    terminalPrinterOwn[1]       = equipDesc[51];
    terminalPrinterOwn[2]       = equipDesc[124];
    terminalPrinterOwn[3]       = equipDesc[25];
    terminalPrinterOwn[4]       = equipDesc[26];
    terminalPrinterOwn[5]       = equipDesc[27];
    terminalPrinterOwn[6]       = equipDesc[28];
    terminalPrinterOwn[7]       = equipDesc[109];
    terminalPrinterOwn[8]       = equipDesc[52];
    terminalPrinterOwn[9]       = equipDesc[53];
    terminalPrinterOwn[10]      = equipDesc[54];
    terminalPrinterOwn[11]      = equipDesc[55];
    terminalPrinterOwn[12]      = equipDesc[110];
    terminalPrinterOwn[13]      = equipDesc[56];
    terminalPrinterOwn[14]      = equipDesc[57];
    terminalPrinterOwn[15]      = equipDesc[58];
    terminalPrinterOwn[16]      = equipDesc[59];
    terminalPrinterOwn[17]      = equipDesc[111];
    terminalPrinterOwn[18]      = equipDesc[0];
    terminalPrinterOwn[19]      = equipDesc[1];
    terminalPrinterOwn[20]      = equipDesc[2];
    terminalPrinterOwn[21]      = equipDesc[3];
    terminalPrinterOwn[22]      = equipDesc[106];
    terminalPrinterOwn[23]      = equipDesc[64];
    terminalPrinterOwn[24]      = equipDesc[65];
    terminalPrinterOwn[25]      = equipDesc[66];
    terminalPrinterOwn[26]      = equipDesc[67];
    terminalPrinterOwn[27]      = equipDesc[107];
    terminalPrinterOwn[28]      = equipDesc[4];
    terminalPrinterOwn[29]      = equipDesc[5];
    terminalPrinterOwn[30]      = equipDesc[6];
    terminalPrinterOwn[31]      = equipDesc[7];
    terminalPrinterOwn[32]      = equipDesc[108];
    terminalPrinterOwn[33]      = equipDesc[60];
    terminalPrinterOwn[34]      = equipDesc[61];
    terminalPrinterOwn[35]      = equipDesc[112];
    terminalPrinterOwn[36]      = equipDesc[102];
    terminalPrinterOwn[37]      = equipDesc[103];
    terminalPrinterOwn[38]      = equipDesc[125];
    terminalPrinterOwn[39]      = equipDesc[8];
    terminalPrinterOwn[40]      = equipDesc[9];
    terminalPrinterOwn[41]      = equipDesc[62];
    terminalPrinterOwn[42]      = equipDesc[63];
    terminalPrinterOwn[43]      = equipDesc[10];
    terminalPrinterOwn[44]      = equipDesc[11];



    terminalPinpadOwn[0]        = equipDesc[68];
    terminalPinpadOwn[1]        = equipDesc[69];
    terminalPinpadOwn[2]        = equipDesc[12];
    terminalPinpadOwn[3]        = equipDesc[13];

    terminalOwn[0]              = equipDesc[72];
    terminalOwn[1]              = equipDesc[73];
    terminalOwn[2]              = equipDesc[74];
    terminalOwn[3]              = equipDesc[75];
    terminalOwn[4]              = equipDesc[113];
    terminalOwn[5]              = equipDesc[78];
    terminalOwn[6]              = equipDesc[79];

    terminalOwn[7]              = equipDesc[80];
    terminalOwn[8]              = equipDesc[81];
    terminalOwn[9]              = equipDesc[118];
    terminalOwn[10]             = equipDesc[21];
    terminalOwn[11]             = equipDesc[22];
    terminalOwn[12]             = equipDesc[23];
    terminalOwn[13]             = equipDesc[24];
    terminalOwn[14]             = equipDesc[104];
    terminalOwn[15]             = equipDesc[105];
    terminalOwn[16]             = equipDesc[82];
    terminalOwn[17]             = equipDesc[83];
    terminalOwn[18]             = equipDesc[114];
    terminalOwn[19]             = equipDesc[115];
    terminalOwn[20]             = equipDesc[116];
    terminalOwn[21]             = equipDesc[117];
    terminalOwn[22]             = equipDesc[32];
    terminalOwn[23]             = equipDesc[14];
    terminalOwn[24]             = equipDesc[15];
    terminalOwn[25]             = equipDesc[16];
    terminalOwn[26]             = equipDesc[84];
    terminalOwn[27]             = equipDesc[76];
    terminalOwn[28]             = equipDesc[77];
    terminalOwn[29]             = equipDesc[33];
    terminalOwn[30]             = equipDesc[34];
    terminalOwn[31]             = equipDesc[122];
    terminalOwn[32]             = equipDesc[123];

    printersOwn[0]              = equipDesc[90];
    printersOwn[1]              = equipDesc[91];
    printersOwn[2]              = equipDesc[92];
    printersOwn[3]              = equipDesc[41];
    printersOwn[4]              = equipDesc[93];
    printersOwn[5]              = equipDesc[35];
    printersOwn[6]              = equipDesc[36];
    printersOwn[7]              = equipDesc[37];
    printersOwn[8]              = equipDesc[38];
    printersOwn[9]              = equipDesc[40];
    printersOwn[10]             = equipDesc[39];
    printersOwn[11]             = equipDesc[96];
    printersOwn[12]             = equipDesc[97];
    printersOwn[13]             = equipDesc[126];
    printersOwn[14]             = equipDesc[100];
    printersOwn[15]             = equipDesc[101];

    pinpadsOwn[0]               = equipDesc[86];
    pinpadsOwn[1]               = equipDesc[42];
    pinpadsOwn[2]               = equipDesc[87];
    pinpadsOwn[3]               = equipDesc[88];
    pinpadsOwn[4]               = equipDesc[127];
    pinpadsOwn[5]               = equipDesc[128];

    imprintersOwn[0]            = equipDesc[45];

    peripheralsOwn[0]           = equipDesc[119];
    peripheralsOwn[1]           = equipDesc[120];
    peripheralsOwn[2]           = equipDesc[121];


    equipType[0]                = "Terminal/Printer Sets";
    equipType[1]                = "Terminal/Printer/Pinpad Sets";
    equipType[2]                = "Terminals";
    equipType[3]                = "Printers";
    equipType[4]                = "Pinpads";
    equipType[5]                = "Peripherals";
    equipType[6]                = "Imprinters";

    equipTypeCode[0]            = "5";
    equipTypeCode[1]            = "6";
    equipTypeCode[2]            = "1";
    equipTypeCode[3]            = "2";
    equipTypeCode[4]            = "3";
    equipTypeCode[5]            = "0";
    equipTypeCode[6]            = "4";

    for(int i=0; i<this.NUM_OF_TYPES; i++)
    {
      equipNeeded[i]    = "-1";
      equipOwned[i]     = "-1";
      equipNumRent[i]   = "-1";
      equipNumBuy[i]    = "-1";
      equipNumLease[i]  = "-1";
      equipNumOwn[i]    = "-1";
      equipAmtRent[i]   = "-1";
      equipAmtBuy[i]    = "-1";
      equipAmtLease[i]  = "-1";
      equipAmtOwn[i]    = "-1";
    }

    try
    {
      need.add(terminalPrinterNeed);
      need.add(terminalPinpadNeed);
      need.add(terminalNeed);
      need.add(printersNeed);
      need.add(pinpadsNeed);
      need.add(peripheralsNeed);
      need.add(imprintersNeed);

      own.add(terminalPrinterOwn);
      own.add(terminalPinpadOwn);
      own.add(terminalOwn);
      own.add(printersOwn);
      own.add(pinpadsOwn);
      own.add(peripheralsOwn);
      own.add(imprintersOwn);

      monthOptions.add("12");
      monthOptions.add("24");
      monthOptions.add("36");
      monthOptions.add("48");

      companyOptions.add("LeaseComm");

    }
    catch(Exception e)
    {
      addError("Problem adding string[] to vector");
    }

    method.add(Integer.toString(mesConstants.SHIPPING_METHOD_2DAY));
    methodDesc.add("2 Day");
    method.add(Integer.toString(mesConstants.SHIPPING_METHOD_OVERNIGHT_AM));
    methodDesc.add("Overnight A.M.");
    method.add(Integer.toString(mesConstants.SHIPPING_METHOD_OVERNIGHT_SAT));
    methodDesc.add("Overnight Sat.");
    method.add(Integer.toString(mesConstants.SHIPPING_METHOD_STANDARD));
    methodDesc.add("Standard");

  }

  public void updateData(HttpServletRequest aReq)
  {
    for(int i=0; i<this.NUM_OF_TYPES; i++)
    {
      if(!isBlank(aReq.getParameter("equipNeeded" + i)) && !aReq.getParameter("equipNeeded" + i).equals("-1"))
      {
        this.equipNeeded[i] = aReq.getParameter("equipNeeded" + i);
      }
      else
      {
        this.equipNeeded[i] = "-1";
      }

      if(!isBlank(aReq.getParameter("equipNumRent" + i)))
      {
        this.equipNumRent[i] = aReq.getParameter("equipNumRent" + i);
      }
      else
      {
        this.equipNumRent[i] = "-1";
      }

      if(!isBlank(aReq.getParameter("equipNumBuy" + i)))
      {
        this.equipNumBuy[i] = aReq.getParameter("equipNumBuy" + i);
      }
      else
      {
        this.equipNumBuy[i] = "-1";
      }

      if(!isBlank(aReq.getParameter("equipNumLease" + i)))
      {
        this.equipNumLease[i] = aReq.getParameter("equipNumLease" + i);
        this.leasingEquip = true;
      }
      else
      {
        this.equipNumLease[i] = "-1";
      }

      if(!isBlank(aReq.getParameter("equipOwned" + i)) && !aReq.getParameter("equipOwned" + i).equals("-1"))
      {
        this.equipOwned[i] = aReq.getParameter("equipOwned" + i);
      }
      else
      {
        this.equipOwned[i] = "-1";
      }

      if(!isBlank(aReq.getParameter("equipNumOwn" + i)))
      {
        this.equipNumOwn[i] = aReq.getParameter("equipNumOwn" + i);
      }
      else
      {
        this.equipNumOwn[i] = "-1";
      }
    }
  }

  public int getPosType(long appSeqNum)
  {
    int posType = -1;
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select pc.pos_type pos_type ");
      qs.append("from   merch_pos mp, ");
      qs.append("       pos_category pc ");
      qs.append("where  mp.app_seq_num = ? and ");
      qs.append("       mp.pos_code = pc.pos_code");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        posType = rs.getInt("pos_type");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPosType(): " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
    return posType;
  }

  public boolean validate(long primaryKey)
  {
    int temp = 0;

    String tempClass = "";
    if(!isBlank(section1Need))
    {
      tempClass = (String)modelCheckNeed.get(section1Need);
      if(isBlank(tempClass))
      {
        modelCheckNeed.put(section1Need,"ineedtogohome");
      }
      else
      {
        addError("Each selection under Equipment Needed must be a unique model to that section");
      }

      if(!isBlank(section1Rent))
      {
        if(!isInt(section1Rent))
        {
          addError("Please provide a valid number of items to rent in section 1");
        }
      }
      if(!isBlank(section1Buy))
      {
        if(!isInt(section1Buy))
        {
          addError("Please provide a valid number of items to buy in section 1");
        }
      }
      if(!isBlank(section1Lease))
      {
        this.leasingEquip = true;
        if(!isInt(section1Lease))
        {
          addError("Please provide a valid number of items to lease in section 1");
        }
      }
      if(isBlank(section1Rent) && isBlank(section1Buy) && isBlank(section1Lease))
      {
        addError("Please provide the number of items to rent, buy or lease or select none for selection 1");
      }
    }
    else if(!isBlank(section1Rent) || !isBlank(section1Buy) || !isBlank(section1Lease))
    {
      addError("Please select an item from selection 1 or delete the number of items to rent, buy or lease");
    }

    if(!isBlank(section2Need))
    {
      tempClass = (String)modelCheckNeed.get(section2Need);
      if(isBlank(tempClass))
      {
        modelCheckNeed.put(section2Need,"ineedtogohome");
      }
      else
      {
        addError("Each selection under Equipment Needed must be a unique model to that section");
      }

      if(!isBlank(section2Rent))
      {
        if(!isInt(section2Rent))
        {
          addError("Please provide a valid number of items to rent in section 2");
        }
      }
      if(!isBlank(section2Buy))
      {
        if(!isInt(section2Buy))
        {
          addError("Please provide a valid number of items to buy in section 2");
        }
      }
      if(!isBlank(section2Lease))
      {
        this.leasingEquip = true;
        if(!isInt(section2Lease))
        {
          addError("Please provide a valid number of items to lease in section 2");
        }
      }
      if(isBlank(section2Rent) && isBlank(section2Buy) && isBlank(section2Lease))
      {
        addError("Please provide the number of items to rent, buy or lease or select none for selection 2");
      }
    }
    else if(!isBlank(section2Rent) || !isBlank(section2Buy) || !isBlank(section2Lease))
    {
      addError("Please select an item from selection 2 or delete the number of items to rent, buy or lease");
    }

    if(!isBlank(section3Need))
    {
      tempClass = (String)modelCheckNeed.get(section3Need);
      if(isBlank(tempClass))
      {
        modelCheckNeed.put(section3Need,"ineedtogohome");
      }
      else
      {
        addError("Each selection under Equipment Needed must be a unique model to that section");
      }

      if(!isBlank(section3Rent))
      {
        if(!isInt(section3Rent))
        {
          addError("Please provide a valid number of items to rent in section 3");
        }
      }
      if(!isBlank(section3Buy))
      {
        if(!isInt(section3Buy))
        {
          addError("Please provide a valid number of items to buy in section 3");
        }
      }
      if(!isBlank(section3Lease))
      {
        this.leasingEquip = true;
        if(!isInt(section3Lease))
        {
          addError("Please provide a valid number of items to lease in section 3");
        }
      }
      if(isBlank(section3Rent) && isBlank(section3Buy) && isBlank(section3Lease))
      {
        addError("Please provide the number of items to rent, buy or lease or select none for selection 3");
      }
    }
    else if(!isBlank(section3Rent) || !isBlank(section3Buy) || !isBlank(section3Lease))
    {
      addError("Please select an item from selection 3 or delete the number of items to rent, buy or lease");
    }

    if(!isBlank(section4Need))
    {
      tempClass = (String)modelCheckNeed.get(section4Need);
      if(isBlank(tempClass))
      {
        modelCheckNeed.put(section4Need,"ineedtogohome");
      }
      else
      {
        addError("Each selection under Equipment Needed must be a unique model to that section");
      }

      if(!isBlank(section4Rent))
      {
        if(!isInt(section4Rent))
        {
          addError("Please provide a valid number of items to rent in section 4");
        }
      }
      if(!isBlank(section4Buy))
      {
        if(!isInt(section4Buy))
        {
          addError("Please provide a valid number of items to buy in section 4");
        }
      }
      if(!isBlank(section4Lease))
      {
        this.leasingEquip = true;
        if(!isInt(section4Lease))
        {
          addError("Please provide a valid number of items to lease in section 4");
        }
      }
      if(isBlank(section4Rent) && isBlank(section4Buy) && isBlank(section4Lease))
      {
        addError("Please provide the number of items to rent, buy or lease or select none for selection 4");
      }
    }
    else if(!isBlank(section4Rent) || !isBlank(section4Buy) || !isBlank(section4Lease))
    {
      addError("Please select an item from selection 4 or delete the number of items to rent, buy or lease");
    }

    if(!isBlank(section5Need))
    {
      tempClass = (String)modelCheckNeed.get(section5Need);
      if(isBlank(tempClass))
      {
        modelCheckNeed.put(section5Need,"ineedtogohome");
      }
      else
      {
        addError("Each selection under Equipment Needed must be a unique model to that section");
      }

      if(!isBlank(section5Rent))
      {
        if(!isInt(section5Rent))
        {
          addError("Please provide a valid number of items to rent in section 5");
        }
      }
      if(!isBlank(section5Buy))
      {
        if(!isInt(section5Buy))
        {
          addError("Please provide a valid number of items to buy in section 5");
        }
      }
      if(!isBlank(section5Lease))
      {
        this.leasingEquip = true;
        if(!isInt(section5Lease))
        {
          addError("Please provide a valid number of items to lease in section 5");
        }
      }
      if(isBlank(section5Rent) && isBlank(section5Buy) && isBlank(section5Lease))
      {
        addError("Please provide the number of items to rent, buy or lease or select none for selection 5");
      }
    }
    else if(!isBlank(section5Rent) || !isBlank(section5Buy) || !isBlank(section5Lease))
    {
      addError("Please select an item from selection 5 or delete the number of items to rent, buy or lease");
    }

    if(!isBlank(section1Have))
    {
      tempClass = (String)modelCheckHave.get(section1Have);
      if(isBlank(tempClass))
      {
        modelCheckHave.put(section1Have,"ineedtogohome");
      }
      else
      {
        addError("Each selection under Equipment Owned must be a unique model to that section");
      }

      if(!isBlank(section1Own))
      {
        if(!isInt(section1Own))
        {
          addError("Please provide a valid number of items you own in section 1");
        }
      }
      else
      {
        addError("Please provide the number of items you own or select none for selection 1");
      }
    }
    else if(!isBlank(section1Own))
    {
      addError("Please select an item from selection 1 or delete the number of items you own");
    }

    if(!isBlank(section2Have))
    {
      tempClass = (String)modelCheckHave.get(section2Have);
      if(isBlank(tempClass))
      {
        modelCheckHave.put(section2Have,"ineedtogohome");
      }
      else
      {
        addError("Each selection under Equipment Owned must be a unique model to that section");
      }

      if(!isBlank(section2Own))
      {
        if(!isInt(section2Own))
        {
          addError("Please provide a valid number of items you own in section 2");
        }
      }
      else
      {
        addError("Please provide the number of items you own or select none for selection 2");
      }
    }
    else if(!isBlank(section2Own))
    {
      addError("Please select an item from selection 2 or delete the number of items you own");
    }

    if(!isBlank(section3Have))
    {
      tempClass = (String)modelCheckHave.get(section3Have);
      if(isBlank(tempClass))
      {
        modelCheckHave.put(section3Have,"ineedtogohome");
      }
      else
      {
        addError("Each selection under Equipment Owned must be a unique model to that section");
      }

      if(!isBlank(section3Own))
      {
        if(!isInt(section3Own))
        {
          addError("Please provide a valid number of items you own in section 3");
        }
      }
      else
      {
        addError("Please provide the number of items you own or select none for selection 3");
      }
    }
    else if(!isBlank(section3Own))
    {
      addError("Please select an item from selection 3 or delete the number of items you own");
    }

    if(!isBlank(section4Have))
    {
      tempClass = (String)modelCheckHave.get(section4Have);
      if(isBlank(tempClass))
      {
        modelCheckHave.put(section4Have,"ineedtogohome");
      }
      else
      {
        addError("Each selection under Equipment Owned must be a unique model to that section");
      }

      if(!isBlank(section4Own))
      {
        if(!isInt(section4Own))
        {
          addError("Please provide a valid number of items you own in section 4");
        }
      }
      else
      {
        addError("Please provide the number of items you own or select none for selection 4");
      }
    }
    else if(!isBlank(section4Own))
    {
      addError("Please select an item from selection 4 or delete the number of items you own");
    }

    if(!isBlank(section5Have))
    {
      tempClass = (String)modelCheckHave.get(section5Have);
      if(isBlank(tempClass))
      {
        modelCheckHave.put(section5Have,"ineedtogohome");
      }
      else
      {
        addError("Each selection under Equipment Owned must be a unique model to that section");
      }

      if(!isBlank(section5Own))
      {
        if(!isInt(section5Own))
        {
          addError("Please provide a valid number of items you own in section 5");
        }
      }
      else
      {
        addError("Please provide the number of items you own or select none for selection 5");
      }
    }
    else if(!isBlank(section5Own))
    {
      addError("Please select an item from selection 5 or delete the number of items you own");
    }

    if(isLeasingEquip() && appType == mesConstants.APP_TYPE_TRANSCOM)
    {
      if(isBlank(this.leaseMonths))
      {
        addError("If leasing equipment, please provide the Number of Months of the lease");
      }
    }
    else if(isLeasingEquip())
    {
      if(isBlank(this.leaseCompany))
      {
        addError("If leasing equipment, please select a Lease Company");
      }
      if(isBlank(this.leaseActNum))
      {
        addError("If leasing equipment, please provide a valid Lease Account Number");
      }
      if(isBlank(this.leaseAmt))
      {
        addError("If leasing equipment, please provide a valid Monthly Lease Amount");
      }
      if(isBlank(this.fundingAmt))
      {
        addError("If leasing equipment, please provide a valid Lease Funding Amount");
      }
      if(isBlank(this.leaseMonths))
      {
        addError("If leasing equipment, please select the Number of Months of the lease");
      }
    }

    if(getPosType(primaryKey) != mesConstants.POS_VIRTUAL_TERMINAL && isBlank(terminalApplication))
    {
      addError("Please select a terminal application in section 3");
    }

    if(this.accessCodeFlag.equals("Y") && this.accessCodeComment.equals("-1"))
    {
      addError("Please provide a valid Access Code ");
    }
    else if(!this.accessCodeFlag.equals("Y") && !this.accessCodeComment.equals("-1"))
    {
      addError("Please check the Access Code Check Box or delete the entered Access Code");
    }
    if(this.autoBatchFlag.equals("Y") && (this.autoBatchHhComment.equals("-1") || this.autoBatchMmComment.equals("-1") || this.autoBatchAmPmComment.equals("-1")))
    {
      addError("Please provide the hour(HH), minute(MM), and AM/PM of when the auto batch close will occur");
    }
    else if(!this.autoBatchFlag.equals("Y") && (!this.autoBatchHhComment.equals("-1") || !this.autoBatchMmComment.equals("-1")))
    {
      addError("Please check the Auto Batch Close check box or delete the entered hour(HH) and minute(MM)");
    }
    else if(this.autoBatchFlag.equals("Y") && !this.autoBatchHhComment.equals("-1") && !this.autoBatchMmComment.equals("-1") && !this.autoBatchAmPmComment.equals("-1") && this.autoBatchMmComment.length() != 2)
    {
      addError("Auto Batch Close Minute(MM) must be two digits long");
    }
    if(this.headerLine4Flag.equals("Y")  && this.headerLine4Comment.equals("-1"))
    {
      addError("Please provide Line 4 for the Receipt Header");
    }
    else if(!this.headerLine4Flag.equals("Y") && !this.headerLine4Comment.equals("-1"))
    {
      addError("Please check the Receipt Header Line 4 check box or delete the entered Line 4");
    }
    if(this.headerLine5Flag.equals("Y")  && this.headerLine5Comment.equals("-1"))
    {
      addError("Please provide Line 5 for the Receipt Header");
    }
    else if(!this.headerLine5Flag.equals("Y") && !this.headerLine5Comment.equals("-1"))
    {
      addError("Please check the Receipt Header Line 5 check box or delete the entered Line 5");
    }
    if(this.footerFlag.equals("Y")  && this.footerComment.equals("-1"))
    {
      addError("Please provide a Receipt Footer");
    }
    else if(!this.footerFlag.equals("Y")  && !this.footerComment.equals("-1"))
    {
      addError("Please check the Receipt Footer check box or delete the entered Footer");
    }

    if(cashBackFlag.equals("Y"))
    {
      try
      {
        double tempdub = Double.parseDouble(cashBackLimit);
      }
      catch(Exception e)
      {
        addError("Please enter a valid cash back limit");
      }
    }
    else if(!isBlank(cashBackLimit))
    {
      addError("Please select cash back with debit checkbox or delete cash back limit");
    }

    if(debitSurchargeFlag.equals("Y"))
    {
      try
      {
        double tempdub = Double.parseDouble(debitSurchargeAmount);
      }
      catch(Exception e)
      {
        addError("Please enter a valid debit surcharge amount");
      }
    }
    else if(!isBlank(debitSurchargeAmount))
    {
      addError("Please select debit surcharge checkbox or delete debit surcharge amount");
    }

    if(!isBlank(customerServicePhone) && !isValidPhone(customerServicePhone))
    {
      addError("Please provide a valid customer service phone #");
    }

    if(isBlank(phoneTraining))
    {
      addError("Please specify the type of phone training for merchant");
    }

    if(this.terminalReminderFlag.equals("Y") && (this.terminalReminderHhComment.equals("-1") || this.terminalReminderMmComment.equals("-1") || this.terminalReminderAmPmComment.equals("-1")))
    {
      addError("Please provide the hour(HH), minute(MM), and AM/PM of Terminal Reminder to Check Totals");
    }
    else if(!this.terminalReminderFlag.equals("Y") && (!this.terminalReminderHhComment.equals("-1") || !this.terminalReminderMmComment.equals("-1")))
    {
      addError("Please check the Terminal Reminder to Check Totals check box or delete the entered hour(HH) and minute(MM)");
    }
    else if(this.terminalReminderFlag.equals("Y") && !this.terminalReminderHhComment.equals("-1") && !this.terminalReminderMmComment.equals("-1") && !this.terminalReminderAmPmComment.equals("-1") && this.terminalReminderMmComment.length() != 2)
    {
      addError("Terminal Reminder to Check Totals Minute(MM) must be two digits long");
    }

    if(this.useAddress.equals(EQUIP_ADD_TYPE))
    {
      if(isBlank(shippingName))
      {
        addError("Please provide a shipping name");
      }
      if(isBlank(shippingAddress1))
      {
        addError("Please provide shipping address line 1");
      }
      if(isBlank(shippingCity))
      {
        addError("Please provide shipping city");
      }
      if(shippingState.equals("-1"))
      {
        addError("Please provide shipping state");
      }

      int num = countDigits(shippingZip);
      if(isBlank(shippingZip) || (num != 5 && num != 9))
      {
        addError("Please provide a valid shipping zip");
      }

      if(appType == mesConstants.APP_TYPE_TRANSCOM)
      {
        if(!isBlank(shippingPhone) && !isValidPhone(shippingPhone))
        {
          addError("Please provide a valid shipping phone");
        }
      }
    }

    if(appType == mesConstants.APP_TYPE_TRANSCOM)
    {
      if(this.equipmentComment.length() > 70)
      {
        addError("Equipment Comments must be less than 70 characters long. It is currently " + this.equipmentComment.length() + " characters long");
      }
    }
    else
    {
      if(this.equipmentComment.length() > 250)
      {
        addError("Equipment Comments must be less than 250 characters long. It is currently " + this.equipmentComment.length() + " characters long");
      }
    }

    if(this.shippingComment.length() > 70)
    {
      addError("Shipping Comments must be less than 70 characters long. It is currently " + this.shippingComment.length() + " characters long");
    }

    return(!hasErrors());
  }

  private boolean isInt(String test)
  {
    boolean result = false;

    try
    {
      int tempint = Integer.parseInt(test);
      result = true;
    }
    catch(Exception e)
    {
      result = false;
    }

    return result;
  }

  public void getData(long appSeqNum)
  {
    getEquipmentInfo(appSeqNum);
    getFeatureInfo(appSeqNum);
    getPaySolOption(appSeqNum);
    getIndustryType(appSeqNum);
    getBusinessPhone(appSeqNum);
  }

  private void getEquipInfoHelper(long appSeqNum, String model)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select equiplendtype_code,merchequip_equip_quantity ");
      qs.append("from merchequipment where app_seq_num = ? and equip_model = ? and equiplendtype_code in (1,2,5) ");
      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);
      ps.setString(2,model);
      rs = ps.executeQuery();
      while(rs.next())
      {
        switch(needSectionNum)
        {
          case 1:
            section1Need = model;
            switch(rs.getInt("equiplendtype_code"))
            {
              case BUY:
                this.section1Buy    = rs.getString("merchequip_equip_quantity");
              break;
              case LEASE:
                this.section1Lease  = rs.getString("merchequip_equip_quantity");
              break;
              case RENT:
                this.section1Rent   = rs.getString("merchequip_equip_quantity");
              break;
            }
          break;
          case 2:
            section2Need = model;
            switch(rs.getInt("equiplendtype_code"))
            {
              case BUY:
                this.section2Buy    = rs.getString("merchequip_equip_quantity");
              break;
              case LEASE:
                this.section2Lease  = rs.getString("merchequip_equip_quantity");
              break;
              case RENT:
                this.section2Rent   = rs.getString("merchequip_equip_quantity");
              break;
            }
          break;
          case 3:
            section3Need = model;
            switch(rs.getInt("equiplendtype_code"))
            {
              case BUY:
                this.section3Buy    = rs.getString("merchequip_equip_quantity");
              break;
              case LEASE:
                this.section3Lease  = rs.getString("merchequip_equip_quantity");
              break;
              case RENT:
                this.section3Rent   = rs.getString("merchequip_equip_quantity");
              break;
            }
          break;
          case 4:
            section4Need = model;
            switch(rs.getInt("equiplendtype_code"))
            {
              case BUY:
                this.section4Buy    = rs.getString("merchequip_equip_quantity");
              break;
              case LEASE:
                this.section4Lease  = rs.getString("merchequip_equip_quantity");
              break;
              case RENT:
                this.section4Rent   = rs.getString("merchequip_equip_quantity");
              break;
            }
          break;
          case 5:
            section5Need = model;
            switch(rs.getInt("equiplendtype_code"))
            {
              case BUY:
                this.section5Buy    = rs.getString("merchequip_equip_quantity");
              break;
              case LEASE:
                this.section5Lease  = rs.getString("merchequip_equip_quantity");
              break;
              case RENT:
                this.section5Rent   = rs.getString("merchequip_equip_quantity");
              break;
            }
          break;
        }
      }
      rs.close();
      ps.close();

      needSectionNum++;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getEquipInfoHelper: " + e.toString());
      addError("getEquipInfoHelper: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }
  private void getEquipmentInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    String            desc    = "";
    String            prod    = "";
    try
    {
      qs.append("select DISTINCT equip_model ");
      qs.append("from merchequipment where app_seq_num = ? and equiptype_code = ? and equiplendtype_code in (1,2,5) ");
      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);
      rs = ps.executeQuery();

      while(rs.next())
      {
        getEquipInfoHelper(appSeqNum, rs.getString("equip_model"));
      }
      rs.close();
      ps.close();

      qs.setLength(0);
      qs.append("select equiplendtype_code,equip_model,merchequip_equip_quantity,prod_option_id ");
      qs.append("from merchequipment where app_seq_num = ? and equiptype_code = ? and equiplendtype_code = ? ");
      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);
      ps.setInt(3,this.OWN);

      rs = ps.executeQuery();

      while(rs.next())
      {
        switch(haveSectionNum)
        {
          case 1:
            this.section1Have  = rs.getString("equip_model");
            this.section1Own   = rs.getString("merchequip_equip_quantity");
          break;
          case 2:
            this.section2Have  = rs.getString("equip_model");
            this.section2Own   = rs.getString("merchequip_equip_quantity");
          break;
          case 3:
            this.section3Have  = rs.getString("equip_model");
            this.section3Own   = rs.getString("merchequip_equip_quantity");
          break;
          case 4:
            this.section4Have  = rs.getString("equip_model");
            this.section4Own   = rs.getString("merchequip_equip_quantity");
          break;
          case 5:
            this.section5Have  = rs.getString("equip_model");
            this.section5Own   = rs.getString("merchequip_equip_quantity");
          break;
        }
        haveSectionNum++;
      }
      rs.close();
      ps.close();

      qs.setLength(0);
      qs.append("select equiplendtype_code,equip_model,merchequip_equip_quantity,prod_option_id ");
      qs.append("from merchequipment where app_seq_num = ? and equiptype_code = ? ");
      ps = getPreparedStatement(qs.toString());

      ps.setLong(1, appSeqNum);
      ps.setInt(2, this.EQUIP_TYPE_IMPRINTER_PLATE);
      rs = ps.executeQuery();
      if(rs.next())
      {
        this.imprinterPlates = rs.getInt("merchequip_equip_quantity");
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getEquipmentInfo: " + e.toString());
      addError("getEquipmentInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }
  private void getFeatureInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select                   ");
      qs.append("access_code_flag,        ");
      qs.append("access_code,             ");
      qs.append("auto_batch_close_flag,   ");
      qs.append("auto_batch_close_time,   ");
      qs.append("header_line4_flag,       ");
      qs.append("header_line4,            ");
      qs.append("header_line5_flag,       ");
      qs.append("header_line5,            ");
      qs.append("footer_flag,             ");
      qs.append("footer,                  ");
      qs.append("reset_ref_flag,          ");
      qs.append("invoice_prompt_flag,     ");
      qs.append("fraud_control_flag,      ");
      qs.append("password_protect_flag,   ");
      qs.append("phone_training_flag,     ");
      qs.append("terminal_reminder_flag,  ");
      qs.append("terminal_reminder_time,  ");
      qs.append("tip_option_flag,         ");
      qs.append("clerk_enabled_flag,      ");
      qs.append("addresstype_code,        ");
      qs.append("term_comm_type,          ");
      qs.append("equipment_comment        ");
      qs.append("from pos_features        ");
      qs.append("where app_seq_num = ?    ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        if(!isBlank(rs.getString("access_code_flag")) && rs.getString("access_code_flag").equals("Y"))
        {
          this.accessCodeFlag         = rs.getString("access_code_flag");
        }
        if(!isBlank(rs.getString("access_code")))
        {
          this.accessCodeComment      = rs.getString("access_code");
        }
        if(!isBlank(rs.getString("auto_batch_close_flag")) && rs.getString("auto_batch_close_flag").equals("Y"))
        {
          this.autoBatchFlag          = rs.getString("auto_batch_close_flag");
        }
        if(!isBlank(rs.getString("auto_batch_close_time")))
        {
          setBatchTime(rs.getString("auto_batch_close_time"));
        }
        if(!isBlank(rs.getString("header_line4_flag")) && rs.getString("header_line4_flag").equals("Y"))
        {
          this.headerLine4Flag        = rs.getString("header_line4_flag");
        }
        if(!isBlank(rs.getString("header_line4")))
        {
          this.headerLine4Comment     = rs.getString("header_line4");
        }
        if(!isBlank(rs.getString("header_line5_flag")) && rs.getString("header_line5_flag").equals("Y"))
        {
          this.headerLine5Flag        = rs.getString("header_line5_flag");
        }
        if(!isBlank(rs.getString("header_line5")))
        {
          this.headerLine5Comment     = rs.getString("header_line5");
        }
        if(!isBlank(rs.getString("footer_flag")) && rs.getString("footer_flag").equals("Y"))
        {
          this.footerFlag             = rs.getString("footer_flag");
        }
        if(!isBlank(rs.getString("footer")))
        {
          this.footerComment          = rs.getString("footer");
        }
        if(!isBlank(rs.getString("reset_ref_flag")) && rs.getString("reset_ref_flag").equals("Y"))
        {
          this.resetReferenceFlag     = rs.getString("reset_ref_flag");
        }
        if(!isBlank(rs.getString("invoice_prompt_flag")) && rs.getString("invoice_prompt_flag").equals("Y"))
        {
          this.invoicePromptFlag      = rs.getString("invoice_prompt_flag");
        }
        if(!isBlank(rs.getString("fraud_control_flag")) && rs.getString("fraud_control_flag").equals("Y"))
        {
          this.fraudControlFlag       = rs.getString("fraud_control_flag");
        }
        if(!isBlank(rs.getString("password_protect_flag")) && rs.getString("password_protect_flag").equals("Y"))
        {
          this.passwordProtectFlag    = rs.getString("password_protect_flag");
        }
        if(!isBlank(rs.getString("phone_training_flag")) && rs.getString("phone_training_flag").equals("Y"))
        {
          this.phoneTrainingFlag      = rs.getString("phone_training_flag");
        }
        if(!isBlank(rs.getString("terminal_reminder_flag")) && rs.getString("terminal_reminder_flag").equals("Y"))
        {
          this.terminalReminderFlag   = rs.getString("terminal_reminder_flag");
        }
        if(!isBlank(rs.getString("terminal_reminder_time")))
        {
          setReminderTime(rs.getString("terminal_reminder_time"));
        }
        if(!isBlank(rs.getString("tip_option_flag")) && rs.getString("tip_option_flag").equals("Y"))
        {
          this.tipOptionFlag          = rs.getString("tip_option_flag");
        }
        if(!isBlank(rs.getString("clerk_enabled_flag")) && rs.getString("clerk_enabled_flag").equals("Y"))
        {
          this.clerkEnabledFlag       = rs.getString("clerk_enabled_flag");
        }
        if(!isBlank(rs.getString("equipment_comment")))
        {
          this.equipmentComment       = rs.getString("equipment_comment");
        }
        if(!isBlank(rs.getString("addresstype_code")))
        {
          this.useAddress             = rs.getString("addresstype_code");
        }
        if(!isBlank(rs.getString("term_comm_type")))
        {
          this.ipTerminal = rs.getInt("term_comm_type");
        }
        if(this.useAddress.equals(EQUIP_ADD_TYPE))
        {
          getShippingAddress(appSeqNum);
        }
      }
      rs.close();
      ps.close();

      qs.setLength(0);
      qs.append("select                   ");
      qs.append("lease_term,              ");
      qs.append("avs_flag,                ");
      qs.append("phone_training,          ");
      qs.append("cash_back_flag,          ");
      qs.append("cash_back_limit,         ");
      qs.append("shipping_comment,        ");
      qs.append("terminal_insurance,      ");
      qs.append("terminal_track,          ");
      qs.append("terminal_printer,        ");
      qs.append("purchasing_card_flag,    ");
      qs.append("card_truncation_flag,    ");
      qs.append("debit_surcharge_flag,    ");
      qs.append("debit_surcharge_amount,  ");
      qs.append("customer_service_phone,  ");
      qs.append("fine_dining_flag,        ");
      qs.append("terminal_application     ");
      qs.append("from transcom_merchant   ");
      qs.append("where app_seq_num = ?    ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        if(!isBlank(rs.getString("lease_term")))
        {
          this.leaseMonths = rs.getString("lease_term");
        }
        if(!isBlank(rs.getString("avs_flag")))
        {
          this.avsOnFlag = rs.getString("avs_flag");
        }
        if(!isBlank(rs.getString("phone_training")))
        {
          this.phoneTraining = rs.getString("phone_training");
        }
        if(!isBlank(rs.getString("cash_back_flag")))
        {
          this.cashBackFlag = rs.getString("cash_back_flag");
        }

        if(!isBlank(rs.getString("fine_dining_flag")))
        {
          this.fineDiningFlag = rs.getString("fine_dining_flag");
        }

        if(!isBlank(rs.getString("cash_back_limit")))
        {
          this.cashBackLimit = rs.getString("cash_back_limit");
        }
        if(!isBlank(rs.getString("terminal_insurance")))
        {
          this.terminalInsurance = rs.getString("terminal_insurance");
        }
        if(!isBlank(rs.getString("terminal_track")))
        {
          this.terminalTrack = rs.getString("terminal_track");
        }
        if(!isBlank(rs.getString("terminal_printer")))
        {
          this.terminalPrinter = rs.getString("terminal_printer");
        }
        if(!isBlank(rs.getString("purchasing_card_flag")))
        {
          this.purchasingCardFlag = rs.getString("purchasing_card_flag");
        }
        if(!isBlank(rs.getString("card_truncation_flag")))
        {
          this.truncationCardFlag = rs.getString("card_truncation_flag");
        }
        if(!isBlank(rs.getString("debit_surcharge_flag")))
        {
          this.debitSurchargeFlag = rs.getString("debit_surcharge_flag");
        }
        if(!isBlank(rs.getString("debit_surcharge_amount")))
        {
          this.debitSurchargeAmount = rs.getString("debit_surcharge_amount");
        }
        if(!isBlank(rs.getString("customer_service_phone")))
        {
          this.customerServicePhone = rs.getString("customer_service_phone");
        }
        if(!isBlank(rs.getString("shipping_comment")))
        {
          this.shippingComment = rs.getString("shipping_comment");
        }
        if(!isBlank(rs.getString("terminal_application")))
        {
          this.terminalApplication = rs.getString("terminal_application");
        }
      }

      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getFeatureInfo: " + e.toString());
      addError("getFeatureInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }
  private void getShippingAddress(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {

      qs.append("select address_name,address_phone,address_line1,address_line2,address_city,countrystate_code,address_zip ");
      qs.append("from address where app_seq_num = ? and addresstype_code = ?");
      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);
      ps.setString(2, EQUIP_ADD_TYPE);

      rs = ps.executeQuery();

      if(rs.next())
      {
        if(!isBlank(rs.getString("address_name")))
        {
          this.shippingName     = rs.getString("address_name");
        }
        if(!isBlank(rs.getString("address_line1")))
        {
          this.shippingAddress1 = rs.getString("address_line1");
        }
        if(!isBlank(rs.getString("address_line2")))
        {
          this.shippingAddress2 = rs.getString("address_line2");
        }
        if(!isBlank(rs.getString("address_city")))
        {
          this.shippingCity     = rs.getString("address_city");
        }
        if(!isBlank(rs.getString("countrystate_code")))
        {
          this.shippingState    = rs.getString("countrystate_code");
        }
        if(!isBlank(rs.getString("address_zip")))
        {
          this.shippingZip      = rs.getString("address_zip");
        }
        if(!isBlank(rs.getString("address_phone")))
        {
          this.shippingPhone    = rs.getString("address_phone");
        }
      }

      rs.close();
      ps.close();

      qs.setLength(0);
      qs.append("select shipping_contact_name,shipping_method ");
      qs.append("from transcom_merchant where app_seq_num = ?");
      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        if(!isBlank(rs.getString("shipping_contact_name")))
        {
          this.shippingContactName = rs.getString("shipping_contact_name");
        }
        if(!isBlank(rs.getString("shipping_method")))
        {
          this.shippingMethod = rs.getString("shipping_method");
        }
      }

      rs.close();
      ps.close();

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getShippingAddress: " + e.toString());
      addError("getShippingAddress: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void getPaySolOption(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select pos_type ");
      qs.append("from merch_pos a, pos_category b ");
      qs.append("where a.app_seq_num = ? and a.pos_code = b.pos_code");

      ps = getPreparedStatement(qs.toString());

      ps.clearParameters();
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      if(rs.next())
      {
        if(rs.getInt("pos_type") == com.mes.constants.mesConstants.APP_PAYSOL_DIAL_PAY)
        {
          dialPay = true;
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPaySolOption: " + e.toString());
      addError("getPaySolOption: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void getIndustryType(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select industype_code from merchant where app_seq_num = ? ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      if(rs.next())
      {
        if(rs.getInt("industype_code") != mesConstants.APP_INDUSTYPE_RESTAURANT)
        {
          retail = true;
        }

        if(rs.getInt("industype_code") == mesConstants.APP_INDUSTYPE_INTERNET || rs.getInt("industype_code") == mesConstants.APP_INDUSTYPE_DIRECTMARKET)
        {
          moto = true;
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getIndustryType: " + e.toString());
      addError("getIndustryType: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void getBusinessPhone(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      // build base statement
      qs.append("select address_phone from address where app_seq_num = ? and ");
      qs.append("addresstype_code = ?");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setInt (2, 1); // business address
      rs = ps.executeQuery();
      if(rs.next())
      {
        businessPhone = rs.getString("address_phone");
        businessPhoneRaw = businessPhone;
        businessPhone = formatPhone(businessPhone);
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getBusinessPhone: " + e.toString());
      addError("getBusinessPhone: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }
  private String formatPhone(String phone)
  {
    if(isBlank(phone))
    {
      return "";
    }
    if(phone.length() != 10)
    {
      return "";
    }
    String areaCode   = phone.substring(0,3);
    String firstThree = phone.substring(3,6);
    String lastFour   = phone.substring(6);
    return ("(" + areaCode + ")" + " " + firstThree + "-" + lastFour);
  }
  private void getCurrentAmounts(long appSeqNum)
  {
    StringBuffer      qs       = new StringBuffer("");
    PreparedStatement ps       = null;
    ResultSet         rs       = null;
    int               lendType = 0;
    try
    {
      qs.append("select merchequip_amount,equip_model,equiplendtype_code from merchequipment ");
      qs.append("where app_seq_num = ? ");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      while(rs.next())
      {
        lendType = rs.getInt("equiplendtype_code");
        switch(lendType)
        {
          case BUY:
            for(int i=0; i<this.NUM_OF_TYPES; i++)
            {
              if(!isBlank((String)desModMap.get(this.equipNeeded[i])) && ((String)desModMap.get(this.equipNeeded[i])).equals(rs.getString("equip_model")))
              {
                if(!isBlank(rs.getString("merchequip_amount")))
                {
                  this.equipAmtBuy[i] = rs.getString("merchequip_amount");
                }
              }
            }
            break;
          case RENT:
            for(int i=0; i<this.NUM_OF_TYPES; i++)
            {
              if(!isBlank((String)desModMap.get(this.equipNeeded[i])) && ((String)desModMap.get(this.equipNeeded[i])).equals(rs.getString("equip_model")))
              {
                if(!isBlank(rs.getString("merchequip_amount")))
                {
                  this.equipAmtRent[i] = rs.getString("merchequip_amount");
                }
              }
            }
            break;
          case OWN:
            for(int i=0; i<this.NUM_OF_TYPES; i++)
            {
              if(!isBlank((String)desModMap.get(this.equipOwned[i])) && ((String)desModMap.get(this.equipOwned[i])).equals(rs.getString("equip_model")))
              {
                if(!isBlank(rs.getString("merchequip_amount")))
                {
                  this.equipAmtOwn[i] = rs.getString("merchequip_amount");
                }
              }
            }
            break;
          case LEASE:
            for(int i=0; i<this.NUM_OF_TYPES; i++)
            {
              if(!isBlank((String)desModMap.get(this.equipNeeded[i])) && ((String)desModMap.get(this.equipNeeded[i])).equals(rs.getString("equip_model")))
              {
                if(!isBlank(rs.getString("merchequip_amount")))
                {
                  this.equipAmtLease[i] = rs.getString("merchequip_amount");
                }
              }
            }
            break;
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCurrentAmounts: " + e.toString());
      addError("getCurrentAmounts: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  public void submitData(HttpServletRequest request, long appSeqNum)
  {
    submitEquipmentInfo(appSeqNum);
    submitFeatureInfo(appSeqNum);
    if(appType == mesConstants.APP_TYPE_TRANSCOM)
    {
      submitTranscomInfo(appSeqNum);
    }
  }

  private void submitTranscomInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select app_seq_num from transcom_merchant where app_seq_num = ? ");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      qs.setLength(0);

      if(rs.next())
      {
        qs.append("update transcom_merchant set     ");
        qs.append("avs_flag                 = ?,    ");
        qs.append("phone_training           = ?,    ");
        qs.append("cash_back_flag           = ?,    ");
        qs.append("cash_back_limit          = ?,    ");
        qs.append("terminal_insurance       = ?,    ");
        qs.append("terminal_track           = ?,    ");
        qs.append("terminal_printer         = ?,    ");
        qs.append("shipping_contact_name    = ?,    ");
        qs.append("shipping_method          = ?,    ");
        qs.append("shipping_comment         = ?,    ");
        qs.append("lease_term               = ?,    ");
        qs.append("purchasing_card_flag     = ?,    ");
        qs.append("card_truncation_flag     = ?,    ");
        qs.append("debit_surcharge_flag     = ?,    ");
        qs.append("debit_surcharge_amount   = ?,    ");
        qs.append("customer_service_phone   = ?,    ");
        qs.append("fine_dining_flag         = ?,    ");
        qs.append("terminal_application     = ?     ");
        qs.append("where app_seq_num        = ?     ");
      }
      else
      {
        qs.append("insert into transcom_merchant (  ");
        qs.append("avs_flag,                        ");
        qs.append("phone_training,                  ");
        qs.append("cash_back_flag,                  ");
        qs.append("cash_back_limit,                 ");
        qs.append("terminal_insurance,              ");
        qs.append("terminal_track,                  ");
        qs.append("terminal_printer,                ");
        qs.append("shipping_contact_name,           ");
        qs.append("shipping_method,                 ");
        qs.append("shipping_comment,                ");
        qs.append("lease_term,                      ");
        qs.append("purchasing_card_flag,            ");
        qs.append("card_truncation_flag,            ");
        qs.append("debit_surcharge_flag,            ");
        qs.append("debit_surcharge_amount,          ");
        qs.append("customer_service_phone,          ");
        qs.append("fine_dining_flag,                ");
        qs.append("terminal_application,            ");
        qs.append("app_seq_num)                     ");
        qs.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      }
      rs.close();
      ps.close();

      ps = getPreparedStatement(qs.toString());

      ps.setString(1,  avsOnFlag);
      ps.setString(2,  phoneTraining);
      ps.setString(3,  cashBackFlag);
      ps.setString(4,  cashBackLimit);
      ps.setString(5,  terminalInsurance);
      ps.setString(6,  terminalTrack);
      ps.setString(7,  terminalPrinter);
      ps.setString(8,  shippingContactName);
      ps.setString(9,  shippingMethod);
      ps.setString(10, shippingComment);
      ps.setString(11, leaseMonths);
      ps.setString(12, purchasingCardFlag);
      ps.setString(13, truncationCardFlag);
      ps.setString(14, debitSurchargeFlag);
      ps.setString(15, debitSurchargeAmount);
      ps.setLong  (16, getDigits(customerServicePhone));
      ps.setString(17, isBlank(fineDiningFlag) ? "N" : fineDiningFlag);
      ps.setString(18, terminalApplication);
      ps.setLong  (19, appSeqNum);

      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitTranscomInfo: insert failed");
        addError("submitTranscomInfo: Unable to insert/update record");
      }
      ps.close();

      // update new customer service phone number if necessary
      qs.setLength(0);

      qs.append("update merchant set customer_service_phone = ? ");
      qs.append("where  app_seq_num = ?");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, getDigits(customerServicePhone));
      ps.setLong(2, appSeqNum);

      ps.executeUpdate();

      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitTranscomInfo: " + e.toString());
      addError("submitTranscomInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }


  private void submitEquipmentInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {

      getCurrentAmounts(appSeqNum);

      qs.append("delete from merchequipment where app_seq_num = ? and equip_model != 'PCPS' ");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      ps.executeUpdate();
      ps.close();

      qs.setLength(0);
      qs.append("insert into merchequipment (app_seq_num,equiptype_code,equiplendtype_code,merchequip_amount,equip_model,merchequip_equip_quantity,prod_option_id,quantity_deployed) ");
      qs.append("values(?,?,?,?,?,?,?,?)");
      ps = getPreparedStatement(qs.toString());

      if(!isBlank(this.section1Need))
      {
        if(!isBlank(this.section1Buy))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,BUY); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section1Need);    //model
          ps.setString(6,this.section1Buy); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
        if(!isBlank(this.section1Rent))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,RENT); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section1Need);    //model
          ps.setString(6,this.section1Rent); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
        if(!isBlank(this.section1Lease))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,LEASE); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section1Need);    //model
          ps.setString(6,this.section1Lease); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
      }

      if(!isBlank(this.section2Need))
      {
        if(!isBlank(this.section2Buy))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,BUY); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section2Need);    //model
          ps.setString(6,this.section2Buy); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
        if(!isBlank(this.section2Rent))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,RENT); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section2Need);    //model
          ps.setString(6,this.section2Rent); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
        if(!isBlank(this.section2Lease))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,LEASE); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section2Need);    //model
          ps.setString(6,this.section2Lease); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
      }

      if(!isBlank(this.section3Need))
      {
        if(!isBlank(this.section3Buy))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,BUY); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section3Need);    //model
          ps.setString(6,this.section3Buy); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
        if(!isBlank(this.section3Rent))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,RENT); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section3Need);    //model
          ps.setString(6,this.section3Rent); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
        if(!isBlank(this.section3Lease))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,LEASE); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section3Need);    //model
          ps.setString(6,this.section3Lease); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
      }

      if(!isBlank(this.section4Need))
      {
        if(!isBlank(this.section4Buy))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,BUY); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section4Need);    //model
          ps.setString(6,this.section4Buy); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
        if(!isBlank(this.section4Rent))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,RENT); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section4Need);    //model
          ps.setString(6,this.section4Rent); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
        if(!isBlank(this.section4Lease))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,LEASE); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section4Need);    //model
          ps.setString(6,this.section4Lease); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
      }

      if(!isBlank(this.section5Need))
      {
        if(!isBlank(this.section5Buy))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,BUY); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section5Need);    //model
          ps.setString(6,this.section5Buy); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
        if(!isBlank(this.section5Rent))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,RENT); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section5Need);    //model
          ps.setString(6,this.section5Rent); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
        if(!isBlank(this.section5Lease))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,LEASE); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section5Need);    //model
          ps.setString(6,this.section5Lease); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
      }

      if(!isBlank(this.section1Have))
      {
        if(!isBlank(this.section1Own))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,this.OWN); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section1Have);    //model
          ps.setString(6,this.section1Own); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
      }
      if(!isBlank(this.section2Have))
      {
        if(!isBlank(this.section2Own))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,this.OWN); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section2Have);    //model
          ps.setString(6,this.section2Own); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
      }
      if(!isBlank(this.section3Have))
      {
        if(!isBlank(this.section3Own))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,this.OWN); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section3Have);    //model
          ps.setString(6,this.section3Own); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
      }
      if(!isBlank(this.section4Have))
      {
        if(!isBlank(this.section4Own))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,this.OWN); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section4Have);    //model
          ps.setString(6,this.section4Own); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
      }
      if(!isBlank(this.section5Have))
      {
        if(!isBlank(this.section5Own))
        {
          ps.setLong(1,appSeqNum);
          ps.setInt(2,mesConstants.APP_EQUIP_TYPE_OTHERS);///default to 7
          ps.setInt(3,this.OWN); //lend type code
          ps.setNull(4,java.sql.Types.FLOAT);
          ps.setString(5,this.section5Have);    //model
          ps.setString(6,this.section5Own); //number of
          ps.setNull(7,java.sql.Types.INTEGER);  //prodoption
          ps.setInt(8,0); //set quantity deployed = zero
          ps.executeUpdate();
        }
      }
      if(this.imprinterPlates > 0)
      {
        ps.setLong(1, appSeqNum);
        ps.setInt(2, this.EQUIP_TYPE_IMPRINTER_PLATE);
        ps.setInt(3, BUY);
        ps.setNull(4, java.sql.Types.FLOAT);
        ps.setString(5, "IPPL");
        ps.setInt(6, this.imprinterPlates);
        ps.setNull(7,java.sql.Types.INTEGER);
        ps.setInt(8,0); //set quantity deployed = zero

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitEquipmentInfo - Imprinter Plates: insert failed");
          addError("unable to insert imprinter plates quantity");
        }
      }
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitEquipmentInfo: " + e.toString());
      addError("submitEquipmentInfo: " + e.toString());
    }
  }

  private void deleteShippingAddress(long appSeqNum)
  {
    PreparedStatement ps      = null;

    try
    {
      ps = getPreparedStatement("delete address where app_seq_num = ? and addresstype_code = ? ");
      ps.setLong(1,appSeqNum);
      ps.setString(2, EQUIP_ADD_TYPE);

      ps.executeUpdate();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "deleteShippingAddress: " + e.toString());
      addError("deleteShippingAddress: " + e.toString());
    }
  }

  private void submitShippingAddress(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      ps = getPreparedStatement("select app_seq_num from address where app_seq_num = ? and addresstype_code = ? ");
      ps.setLong(1,appSeqNum);
      ps.setString(2, EQUIP_ADD_TYPE);

      rs = ps.executeQuery();
      qs.setLength(0);

      if(rs.next())
      {
        qs.append("update address set ");
        qs.append("address_name = ?, ");
        qs.append("address_line1 = ?, ");
        qs.append("address_line2 = ?, ");
        qs.append("address_city = ?, ");
        qs.append("countrystate_code = ?, ");
        qs.append("address_zip = ?, ");
        qs.append("address_phone = ? ");
        qs.append("where app_seq_num = ? and addresstype_code = ?");
      }
      else
      {
        qs.append("insert into address (");
        qs.append("address_name,address_line1,address_line2,");
        qs.append("address_city,");
        qs.append("countrystate_code,");
        qs.append("address_zip,");
        qs.append("address_phone,");
        qs.append("app_seq_num,addresstype_code) ");
        qs.append("values(?,?,?,?,?,?,?,?,?)");
      }
      rs.close();
      ps.close();

      ps = getPreparedStatement(qs.toString());

      ps.setString(1,shippingName);
      ps.setString(2,shippingAddress1);
      ps.setString(3,shippingAddress2);
      ps.setString(4,shippingCity);
      ps.setString(5,shippingState);
      ps.setString(6,shippingZip);
      ps.setLong(7,getDigits(shippingPhone));
      ps.setLong(8,appSeqNum);
      ps.setString(9,EQUIP_ADD_TYPE);

      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitShippingAddress: insert failed");
        addError("submitShippingAddress: Unable to insert/update record");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitShippingAddress: " + e.toString());
      addError("submitShippingAddress: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void submitFeatureInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select app_seq_num from pos_features where app_seq_num = ? ");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      qs.setLength(0);

      if(rs.next())
      {
        qs.append("update pos_features set ");
        qs.append("access_code_flag = ? , ");
        qs.append("access_code = ? , ");
        qs.append("auto_batch_close_flag = ?, ");
        qs.append("auto_batch_close_time = ? , ");
        qs.append("header_line4_flag = ?, ");
        qs.append("header_line4 = ? , ");
        qs.append("header_line5_flag = ?, ");
        qs.append("header_line5 = ? , ");
        qs.append("footer_flag = ?, ");
        qs.append("footer = ? , ");
        qs.append("reset_ref_flag = ?, ");
        qs.append("invoice_prompt_flag = ? , ");
        qs.append("fraud_control_flag = ?, ");
        qs.append("password_protect_flag = ? , ");
        qs.append("phone_training_flag = ?, ");
        qs.append("terminal_reminder_flag = ? , ");
        qs.append("terminal_reminder_time = ?, ");
        qs.append("tip_option_flag = ? , ");
        qs.append("clerk_enabled_flag = ?, ");
        qs.append("equipment_comment = ?, ");
        qs.append("addresstype_code  = ?, ");
        qs.append("term_comm_type = ?, ");
        qs.append("phone_training = ? ");
        qs.append("where app_seq_num = ? ");
      }
      else
      {
        qs.append("insert into pos_features (");
        qs.append("access_code_flag,access_code,");
        qs.append("auto_batch_close_flag,");
        qs.append("auto_batch_close_time,");
        qs.append("header_line4_flag,");
        qs.append("header_line4,");
        qs.append("header_line5_flag,");
        qs.append("header_line5,");
        qs.append("footer_flag,");
        qs.append("footer,");
        qs.append("reset_ref_flag,");
        qs.append("invoice_prompt_flag,");
        qs.append("fraud_control_flag,");
        qs.append("password_protect_flag,");
        qs.append("phone_training_flag,");
        qs.append("terminal_reminder_flag,");
        qs.append("terminal_reminder_time,");
        qs.append("tip_option_flag,");
        qs.append("clerk_enabled_flag,");
        qs.append("equipment_comment,");
        qs.append("addresstype_code,");
        qs.append("term_comm_type,");
        qs.append("phone_training,");
        qs.append("app_seq_num) ");
        qs.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      }
      rs.close();
      ps.close();

      ps = getPreparedStatement(qs.toString());

      ps.setString(1,getAccessCodeFlag());
      ps.setString(2,getAccessCodeComment());
      ps.setString(3,getAutoBatchFlag());
      ps.setString(4,getBatchTime());
      ps.setString(5,getHeaderLine4Flag());
      ps.setString(6,getHeaderLine4Comment());
      ps.setString(7,getHeaderLine5Flag());
      ps.setString(8,getHeaderLine5Comment());
      ps.setString(9,getFooterFlag());
      ps.setString(10,getFooterComment());
      ps.setString(11,getResetReferenceFlag());
      ps.setString(12,getInvoicePromptFlag());
      ps.setString(13,getFraudControlFlag());
      ps.setString(14,getPasswordProtectFlag());
      ps.setString(15,getPhoneTrainingFlag());
      ps.setString(16,getTerminalReminderFlag());
      ps.setString(17,getReminderTime());
      ps.setString(18,getTipOptionFlag());
      ps.setString(19,getClerkEnabledFlag());
      ps.setString(20,getEquipmentComment());
      ps.setString(21,this.useAddress);
      ps.setInt(22,ipTerminal);
      ps.setString(23,getPhoneTraining());
      ps.setLong(24,appSeqNum);

      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitFeatureInfo: insert failed");
        addError("submitFeatureInfo: Unable to insert/update record");
      }

      if(this.useAddress.equals(EQUIP_ADD_TYPE))
      {
        submitShippingAddress(appSeqNum);
      }
      else
      {
        deleteShippingAddress(appSeqNum);
      }

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitFeatureInfo: " + e.toString());
      addError("submitFeatureInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  public String getEquipType(int i)
  {
    String result = "";
    if(!this.equipType[i].equals("-1"))
    {
      result = this.equipType[i];
    }
    return result;
  }

  public String getEquipNeeded(int i)
  {
    return this.equipNeeded[i];
  }
  public String getEquipOwned(int i)
  {
    return this.equipOwned[i];
  }

  public String getEquipNumRent(int i)
  {
    String result = "";
    if(!this.equipNumRent[i].equals("-1"))
    {
      result = this.equipNumRent[i];
    }
    return result;
  }
  public String getEquipNumBuy(int i)
  {
    String result = "";
    if(!this.equipNumBuy[i].equals("-1"))
    {
      result = this.equipNumBuy[i];
    }
    return result;
  }
  public String getEquipNumLease(int i)
  {
    String result = "";
    if(!this.equipNumLease[i].equals("-1"))
    {
      result = this.equipNumLease[i];
    }
    return result;
  }

  public String getEquipNumOwn(int i)
  {
    String result = "";
    if(!this.equipNumOwn[i].equals("-1"))
    {
      result = this.equipNumOwn[i];
    }
    return result;
  }
  public void setAccessCodeFlag(String accessCodeFlag)
  {
    this.accessCodeFlag = accessCodeFlag;
  }
  public String getAccessCodeFlag()
  {
    return this.accessCodeFlag;
  }
  public void setAutoBatchFlag(String autoBatchFlag)
  {
    this.autoBatchFlag = autoBatchFlag;
  }
  public String getAutoBatchFlag()
  {
    return this.autoBatchFlag;
  }
  public void setHeaderLine4Flag(String headerLine4Flag)
  {
    this.headerLine4Flag = headerLine4Flag;
  }
  public String getHeaderLine4Flag()
  {
    return this.headerLine4Flag;
  }
  public void setHeaderLine5Flag(String headerLine5Flag)
  {
    this.headerLine5Flag = headerLine5Flag;
  }
  public String getHeaderLine5Flag()
  {
    return this.headerLine5Flag;
  }
  public void setFooterFlag(String footerFlag)
  {
    this.footerFlag = footerFlag;
  }
  public String getFooterFlag()
  {
    return this.footerFlag;
  }
  public void setResetReferenceFlag(String resetReferenceFlag)
  {
    this.resetReferenceFlag = resetReferenceFlag;
  }
  public String getResetReferenceFlag()
  {
    return this.resetReferenceFlag;
  }
  public void setInvoicePromptFlag(String invoicePromptFlag)
  {
    this.invoicePromptFlag = invoicePromptFlag;
  }
  public String getInvoicePromptFlag()
  {
    return this.invoicePromptFlag;
  }
  public void setFraudControlFlag(String fraudControlFlag)
  {
    this.fraudControlFlag = fraudControlFlag;
  }
  public String getFraudControlFlag()
  {
    return this.fraudControlFlag;
  }
  public void setPasswordProtectFlag(String passwordProtectFlag)
  {
    this.passwordProtectFlag = passwordProtectFlag;
  }
  public String getPasswordProtectFlag()
  {
    return this.passwordProtectFlag;
  }
  public void setPhoneTrainingFlag(String phoneTrainingFlag)
  {
    this.phoneTrainingFlag = phoneTrainingFlag;
  }
  public String getPhoneTrainingFlag()
  {
    return this.phoneTrainingFlag;
  }
  public void setTerminalReminderFlag(String terminalReminderFlag)
  {
    this.terminalReminderFlag = terminalReminderFlag;
  }
  public String getTerminalReminderFlag()
  {
    return this.terminalReminderFlag;
  }
  public void setTipOptionFlag(String tipOptionFlag)
  {
    this.tipOptionFlag = tipOptionFlag;
  }
  public String getTipOptionFlag()
  {
    return this.tipOptionFlag;
  }
  public void setClerkEnabledFlag(String clerkEnabledFlag)
  {
    this.clerkEnabledFlag = clerkEnabledFlag;
  }
  public String getClerkEnabledFlag()
  {
    return this.clerkEnabledFlag;
  }

  public void setFineDiningFlag(String fineDiningFlag)
  {
    this.fineDiningFlag = fineDiningFlag;
  }
  public String getFineDiningFlag()
  {
    return this.fineDiningFlag;
  }

  public void setAvsOnFlag(String avsOnFlag)
  {
    this.avsOnFlag = avsOnFlag;
  }
  public String getAvsOnFlag()
  {
    return this.avsOnFlag;
  }
  public void setCashBackFlag(String cashBackFlag)
  {
    this.cashBackFlag = cashBackFlag;
  }
  public String getCashBackFlag()
  {
    return this.cashBackFlag;
  }

  public void setDebitSurchargeFlag(String debitSurchargeFlag)
  {
    this.debitSurchargeFlag = debitSurchargeFlag;
  }
  public String getDebitSurchargeFlag()
  {
    return this.debitSurchargeFlag;
  }

  public void setPurchasingCardFlag(String purchasingCardFlag)
  {
    this.purchasingCardFlag = purchasingCardFlag;
  }
  public String getPurchasingCardFlag()
  {
    return this.purchasingCardFlag;
  }

  public void setTruncationCardFlag(String truncationCardFlag)
  {
    this.truncationCardFlag = truncationCardFlag;
  }
  public String getTruncationCardFlag()
  {
    return this.truncationCardFlag;
  }

  public String getPhoneTraining()
  {
    return this.phoneTraining;
  }
  public void setPhoneTraining(String phoneTraining)
  {
    this.phoneTraining = phoneTraining;
  }

  public String getTerminalInsurance()
  {
    return this.terminalInsurance;
  }
  public void setTerminalInsurance(String terminalInsurance)
  {
    this.terminalInsurance = terminalInsurance;
  }

  public String getTerminalTrack()
  {
    return this.terminalTrack;
  }
  public void setTerminalTrack(String terminalTrack)
  {
    this.terminalTrack = terminalTrack;
  }

  public String getTerminalPrinter()
  {
    return this.terminalPrinter;
  }
  public void setTerminalPrinter(String terminalPrinter)
  {
    this.terminalPrinter = terminalPrinter;
  }

  public String getCashBackLimit()
  {
    return this.cashBackLimit;
  }
  public void setCashBackLimit(String cashBackLimit)
  {
    this.cashBackLimit = cashBackLimit;
  }

  public String getDebitSurchargeAmount()
  {
    return this.debitSurchargeAmount;
  }
  public void setDebitSurchargeAmount(String debitSurchargeAmount)
  {
    this.debitSurchargeAmount = debitSurchargeAmount;
  }

  public String getSection1Need()
  {
    return this.section1Need;
  }
  public String getSection2Need()
  {
    return this.section2Need;
  }
  public String getSection3Need()
  {
    return this.section3Need;
  }
  public String getSection4Need()
  {
    return this.section4Need;
  }
  public String getSection5Need()
  {
    return this.section5Need;
  }

  public String getSection1Have()
  {
    return this.section1Have;
  }
  public String getSection2Have()
  {
    return this.section2Have;
  }
  public String getSection3Have()
  {
    return this.section3Have;
  }
  public String getSection4Have()
  {
    return this.section4Have;
  }
  public String getSection5Have()
  {
    return this.section5Have;
  }


  public String getSection1Rent()
  {
    return this.section1Rent;
  }
  public String getSection2Rent()
  {
    return this.section2Rent;
  }
  public String getSection3Rent()
  {
    return this.section3Rent;
  }
  public String getSection4Rent()
  {
    return this.section4Rent;
  }
  public String getSection5Rent()
  {
    return this.section5Rent;
  }

  public String getSection1Buy()
  {
    return this.section1Buy;
  }
  public String getSection2Buy()
  {
    return this.section2Buy;
  }
  public String getSection3Buy()
  {
    return this.section3Buy;
  }
  public String getSection4Buy()
  {
    return this.section4Buy;
  }
  public String getSection5Buy()
  {
    return this.section5Buy;
  }

  public String getSection1Lease()
  {
    return this.section1Lease;
  }
  public String getSection2Lease()
  {
    return this.section2Lease;
  }
  public String getSection3Lease()
  {
    return this.section3Lease;
  }
  public String getSection4Lease()
  {
    return this.section4Lease;
  }
  public String getSection5Lease()
  {
    return this.section5Lease;
  }

  public String getSection1Own()
  {
    return this.section1Own;
  }
  public String getSection2Own()
  {
    return this.section2Own;
  }
  public String getSection3Own()
  {
    return this.section3Own;
  }
  public String getSection4Own()
  {
    return this.section4Own;
  }
  public String getSection5Own()
  {
    return this.section5Own;
  }


  public void setSection1Rent(String section1Rent)
  {
    this.section1Rent = section1Rent;
  }
  public void setSection2Rent(String section2Rent)
  {
    this.section2Rent = section2Rent;
  }
  public void setSection3Rent(String section3Rent)
  {
    this.section3Rent = section3Rent;
  }
  public void setSection4Rent(String section4Rent)
  {
    this.section4Rent = section4Rent;
  }
  public void setSection5Rent(String section5Rent)
  {
    this.section5Rent = section5Rent;
  }

  public void setSection1Buy(String section1Buy)
  {
    this.section1Buy = section1Buy;
  }
  public void setSection2Buy(String section2Buy)
  {
    this.section2Buy = section2Buy;
  }
  public void setSection3Buy(String section3Buy)
  {
    this.section3Buy = section3Buy;
  }
  public void setSection4Buy(String section4Buy)
  {
    this.section4Buy = section4Buy;
  }
  public void setSection5Buy(String section5Buy)
  {
    this.section5Buy = section5Buy;
  }

  public void setSection1Lease(String section1Lease)
  {
    this.section1Lease = section1Lease;
  }
  public void setSection2Lease(String section2Lease)
  {
    this.section2Lease = section2Lease;
  }
  public void setSection3Lease(String section3Lease)
  {
    this.section3Lease = section3Lease;
  }
  public void setSection4Lease(String section4Lease)
  {
    this.section4Lease = section4Lease;
  }
  public void setSection5Lease(String section5Lease)
  {
    this.section5Lease = section5Lease;
  }

  public void setSection1Own(String section1Own)
  {
    this.section1Own = section1Own;
  }
  public void setSection2Own(String section2Own)
  {
    this.section2Own = section2Own;
  }
  public void setSection3Own(String section3Own)
  {
    this.section3Own = section3Own;
  }
  public void setSection4Own(String section4Own)
  {
    this.section4Own = section4Own;
  }
  public void setSection5Own(String section5Own)
  {
    this.section5Own = section5Own;
  }

  public void setSection1Need(String section1Need)
  {
    this.section1Need = section1Need;
  }
  public void setSection2Need(String section2Need)
  {
    this.section2Need = section2Need;
  }
  public void setSection3Need(String section3Need)
  {
    this.section3Need = section3Need;
  }
  public void setSection4Need(String section4Need)
  {
    this.section4Need = section4Need;
  }
  public void setSection5Need(String section5Need)
  {
    this.section5Need = section5Need;
  }

  public void setSection1Have(String section1Have)
  {
    this.section1Have = section1Have;
  }
  public void setSection2Have(String section2Have)
  {
    this.section2Have = section2Have;
  }
  public void setSection3Have(String section3Have)
  {
    this.section3Have = section3Have;
  }
  public void setSection4Have(String section4Have)
  {
    this.section4Have = section4Have;
  }
  public void setSection5Have(String section5Have)
  {
    this.section5Have = section5Have;
  }

  public void setAccessCodeComment(String accessCodeComment)
  {
    try
    {
      long temp = Long.parseLong(accessCodeComment);
      this.accessCodeComment = accessCodeComment;
    }
    catch(Exception e)
    {
      this.accessCodeComment = "-1";
    }
  }
  public String getAccessCodeComment()
  {
    String result = "";
    if(!this.accessCodeComment.equals("-1"))
    {
      result = this.accessCodeComment;
    }
    return result;
  }
  public void setAutoBatchHhComment(String autoBatchHhComment)
  {
    try
    {
      int temp = Integer.parseInt(autoBatchHhComment);
      if(temp > 12 || temp <= 0)
      {
        this.autoBatchHhComment = "-1";
      }
      else
      {
        this.autoBatchHhComment = autoBatchHhComment;
      }
    }
    catch(Exception e)
    {
      this.autoBatchHhComment = "-1";
    }
  }
  public String getAutoBatchHhComment()
  {
    String result = "";
    if(!this.autoBatchHhComment.equals("-1"))
    {
      result = this.autoBatchHhComment;
    }
    return result;
  }
  public void setAutoBatchMmComment(String autoBatchMmComment)
  {
    try
    {
      int temp = Integer.parseInt(autoBatchMmComment);
      if(temp > 59 || temp < 0)
      {
        this.autoBatchMmComment = "-1";
      }
      else
      {
        this.autoBatchMmComment = autoBatchMmComment;
      }
    }
    catch(Exception e)
    {
      this.autoBatchMmComment = "-1";
    }
  }
  public String getAutoBatchMmComment()
  {
    String result = "";
    if(!this.autoBatchMmComment.equals("-1"))
    {
      result = this.autoBatchMmComment;
    }
    return result;
  }


  public void setTerminalReminderHhComment(String terminalReminderHhComment)
  {
    try
    {
      int temp = Integer.parseInt(terminalReminderHhComment);
      if(temp > 12 || temp <= 0)
      {
        this.terminalReminderHhComment = "-1";
      }
      else
      {
        this.terminalReminderHhComment = terminalReminderHhComment;
      }
    }
    catch(Exception e)
    {
      this.terminalReminderHhComment = "-1";
    }
  }
  public String getTerminalReminderHhComment()
  {
    String result = "";
    if(!this.terminalReminderHhComment.equals("-1"))
    {
      result = this.terminalReminderHhComment;
    }
    return result;
  }
  public void setTerminalReminderMmComment(String terminalReminderMmComment)
  {
    try
    {
      int temp = Integer.parseInt(terminalReminderMmComment);
      if(temp > 59 || temp < 0)
      {
        this.terminalReminderMmComment = "-1";
      }
      else
      {
        this.terminalReminderMmComment = terminalReminderMmComment;
      }
    }
    catch(Exception e)
    {
      this.terminalReminderMmComment = "-1";
    }
  }
  public String getTerminalReminderMmComment()
  {
    String result = "";
    if(!this.terminalReminderMmComment.equals("-1"))
    {
      result = this.terminalReminderMmComment;
    }
    return result;
  }

  public void setTerminalReminderAmPmComment(String terminalReminderAmPmComment)
  {
    if(!isBlank(terminalReminderAmPmComment))
    {
      this.terminalReminderAmPmComment = terminalReminderAmPmComment;
    }
    else
    {
      this.terminalReminderAmPmComment = "-1";
    }
  }
  public String getTerminalReminderAmPmComment()
  {
    return this.terminalReminderAmPmComment;
  }

  public void setAutoBatchAmPmComment(String autoBatchAmPmComment)
  {
    if(!isBlank(autoBatchAmPmComment))
    {
      this.autoBatchAmPmComment = autoBatchAmPmComment;
    }
    else
    {
      this.autoBatchAmPmComment = "-1";
    }
  }
  public String getAutoBatchAmPmComment()
  {
    return this.autoBatchAmPmComment;
  }

  public void setHeaderLine4Comment(String headerLine4Comment)
  {
    if(!isBlank(headerLine4Comment))
    {
      this.headerLine4Comment = headerLine4Comment;
    }
    else
    {
      this.headerLine4Comment = "-1";
    }
  }
  public String getHeaderLine4Comment()
  {
    String result = "";
    if(!this.headerLine4Comment.equals("-1"))
    {
      result = this.headerLine4Comment;
    }
    return result;
  }
  public void setHeaderLine5Comment(String headerLine5Comment)
  {
    if(!isBlank(headerLine5Comment))
    {
      this.headerLine5Comment = headerLine5Comment;
    }
    else
    {
      this.headerLine5Comment = "-1";
    }
  }
  public String getHeaderLine5Comment()
  {
    String result = "";
    if(!this.headerLine5Comment.equals("-1"))
    {
      result = this.headerLine5Comment;
    }
    return result;
  }
  public void setFooterComment(String footerComment)
  {
    if(!isBlank(footerComment))
    {
      this.footerComment = footerComment;
    }
    else
    {
      this.footerComment = "-1";
    }
  }
  public String getFooterComment()
  {
    String result = "";
    if(!this.footerComment.equals("-1"))
    {
      result = this.footerComment;
    }
    return result;
  }
  public void setEquipmentComment(String equipmentComment)
  {
    if(!isBlank(equipmentComment))
    {
      this.equipmentComment = equipmentComment;
    }
    else
    {
      this.equipmentComment = "-1";
    }
  }
  public String getEquipmentComment()
  {
    String result = "";
    if(!this.equipmentComment.equals("-1"))
    {
      result = this.equipmentComment;
    }
    return result;
  }

  private String getBatchTime()
  {
    String result = "";
    if(!this.autoBatchHhComment.equals("-1") && !this.autoBatchMmComment.equals("-1") && !this.autoBatchAmPmComment.equals("-1"))
    {
      result = this.autoBatchHhComment + ":" + this.autoBatchMmComment + " " + this.autoBatchAmPmComment;
    }
    return result;
  }
  private String getReminderTime()
  {
    String result = "";
    if(!this.terminalReminderHhComment.equals("-1") && !this.terminalReminderMmComment.equals("-1") && !this.terminalReminderAmPmComment.equals("-1"))
    {
      result = this.terminalReminderHhComment + ":" + this.terminalReminderMmComment + " " + this.terminalReminderAmPmComment;
    }
    return result;
  }

  private void setBatchTime(String time)
  {
    if(isBlank(time))
    {
      return;
    }
    if(time.length() >= 7)
    {
      int index   = 0;

      index = time.indexOf(':');
      this.autoBatchHhComment   = time.substring(0,index);
      this.autoBatchMmComment   = time.substring(index+1, index+3);
      this.autoBatchAmPmComment = time.substring(index+4);
    }
  }
  private void setReminderTime(String time)
  {
    if(isBlank(time))
    {
      return;
    }
    if(time.length() >= 7)
    {
      int index   = 0;

      index = time.indexOf(':');
      this.terminalReminderHhComment   = time.substring(0,index);
      this.terminalReminderMmComment   = time.substring(index+1, index+3);
      this.terminalReminderAmPmComment = time.substring(index+4);
    }
  }

  public String getLeaseMonths()
  {
    return this.leaseMonths;
  }
  public String getLeaseCompany()
  {
    return this.leaseCompany;
  }
  public String getLeaseActNum()
  {
    return this.leaseActNum;
  }
  public String getLeaseAmt()
  {
    return formatCurr(this.leaseAmt);
  }
  public String getFundingAmt()
  {
    return formatCurr(this.fundingAmt);
  }

  public String formatCurr(String curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }

  public String getCurr(String num)
  {
    String result = "";
    try
    {
      if(validCurr(num))
      {
        if(!num.startsWith("$"))
        {
          num = "$" + num;
        }
        NumberFormat  nf       = NumberFormat.getCurrencyInstance(Locale.US);
        double        temp     = (nf.parse(num)).doubleValue();
                      result   = Double.toString(temp);
      }
    }
    catch(Exception e)
    {}
    return result;
  }

  public boolean validCurr(String raw)
  {
    boolean result  = true;
    int     sign    = 0;
    int     decimal = 0;

    try
    {
      if(!raw.startsWith("$"))
      {
        raw = "$" + raw;
      }

      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == ',') //let digits and commas slide
        {
        }
        else if(raw.charAt(i) == '$')
        {
          sign++;
        }
        else if(raw.charAt(i) == '.')
        {
          decimal++;
        }
        else
        {
          result = false;
        }
      }

      if(sign > 1 || decimal > 1)
      {
        result = false;
      }
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }

  public void setLeaseAmt(String leaseAmt)
  {
    this.leaseAmt = getCurr(leaseAmt);
  }
  public void setFundingAmt(String fundingAmt)
  {
    this.fundingAmt = getCurr(fundingAmt);
  }

  public void setLeaseCompany(String leaseCompany)
  {
    this.leaseCompany = leaseCompany;
  }
  public void setLeaseActNum(String leaseActNum)
  {
    String result = "";
    if(isNumber(leaseActNum))
    {
      result = leaseActNum;
    }
    this.leaseActNum = result;
  }

  public void setLeaseMonths(String leaseMonths)
  {
    try
    {
      int temp = Integer.parseInt(leaseMonths);
      this.leaseMonths = leaseMonths;
    }
    catch(Exception e)
    {
      this.leaseMonths = "";
    }
  }

  public String getCustomerServicePhone()
  {
    return this.customerServicePhone;
  }
  public void setCustomerServicePhone(String customerServicePhone)
  {
    this.customerServicePhone = customerServicePhone;
  }

  public String getTerminalApplication()
  {
    return this.terminalApplication;
  }
  public void setTerminalApplication(String terminalApplication)
  {
    this.terminalApplication = terminalApplication;
  }


  public String getUseAddress()
  {
    return this.useAddress;
  }
  public String getShippingName()
  {
    return this.shippingName;
  }
  public String getShippingContactName()
  {
    return this.shippingContactName;
  }
  public String getShippingPhone()
  {
    return this.shippingPhone;
  }
  public String getShippingMethod()
  {
    return this.shippingMethod;
  }
  public String getShippingComment()
  {
    return this.shippingComment;
  }


  public String getShippingAddress1()
  {
    return this.shippingAddress1;
  }
  public String getShippingAddress2()
  {
    return this.shippingAddress2;
  }
  public String getShippingCity()
  {
    return this.shippingCity;
  }
  public String getShippingState()
  {
    return this.shippingState;
  }
  public String getShippingZip()
  {
    return this.shippingZip;
  }

  public void setUseAddress(String temp)
  {
    this.useAddress = temp;
  }
  public void setShippingName(String temp)
  {
    this.shippingName = temp;
  }

  public void setShippingContactName(String shippingContactName)
  {
    this.shippingContactName = shippingContactName;
  }
  public void setShippingPhone(String shippingPhone)
  {
    this.shippingPhone = shippingPhone;
  }
  public void setShippingMethod(String shippingMethod)
  {
    this.shippingMethod =  shippingMethod;
  }
  public void setShippingComment(String shippingComment)
  {
    this.shippingComment = shippingComment;
  }


  public void setShippingAddress1(String temp)
  {
    this.shippingAddress1 = temp;
  }
  public void setShippingAddress2(String temp)
  {
    this.shippingAddress2 = temp;
  }
  public void setShippingCity(String temp)
  {
    this.shippingCity = temp;
  }
  public void setShippingState(String temp)
  {
    this.shippingState = temp;
  }
  public void setShippingZip(String temp)
  {
    this.shippingZip = temp;
  }
  public boolean isDialPay()
  {
    return dialPay;
  }
  public void setDialPay(String temp)
  {
    dialPay = true;
  }
  public boolean isRetail()
  {
    return retail;
  }
  public boolean isMoto()
  {
    return moto;
  }
  public String getBusinessPhone()
  {
    return this.businessPhone;
  }
  public String getBusinessPhoneRaw()
  {
    return this.businessPhoneRaw;
  }
  public boolean isLeasingEquip()
  {
    return leasingEquip;
  }

  public Vector getLeaseComOptions()
  {
    return companyOptions;
  }
  public Vector getLeaseMonOptions()
  {
    return monthOptions;
  }


  public String getImprinterPlates()
  {
    String result = "";
    if(this.imprinterPlates > 0)
    {
      result = String.valueOf(this.imprinterPlates);
    }

    return result;
  }
  public void setImprinterPlates(String imprinterPlates)
  {
    try
    {
      this.imprinterPlates = Integer.parseInt(imprinterPlates);
    }
    catch(Exception e)
    {
      this.imprinterPlates = 0;
    }
  }
  public void setAppType(String appType)
  {
    try
    {
      this.appType = Integer.parseInt(appType);
    }
    catch(Exception e)
    {
      this.appType = 0;
    }
  }

  public void setIpTerminal(String ipTerminal)
  {
    if(ipTerminal != null && ipTerminal.equals("Y"))
    {
      this.ipTerminal = mesConstants.TERM_COMM_TYPE_IP;
    }
  }
  public String getIpTerminal()
  {
    return (this.ipTerminal == mesConstants.TERM_COMM_TYPE_IP ? "Y" : "N");
  }

}