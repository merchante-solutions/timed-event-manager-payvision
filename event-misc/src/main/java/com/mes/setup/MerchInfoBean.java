/*@lineinfo:filename=MerchInfoBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/MerchInfoBean.sqlj $

  Description:


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-08-01 10:08:20 -0700 (Fri, 01 Aug 2008) $
  Version            : $Revision: 15207 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.setup;

import java.lang.reflect.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.constants.mesConstants;
import com.mes.tools.AppNotifyBean;
import sqlj.runtime.ResultSetIterator;

public class MerchInfoBean extends com.mes.screens.SequenceDataBean
{
  /*
   ** CONSTANTS
  */
  public final static int EQUIP_TYPE_IMPRINTER_PLATE      = 9;

  // card types
  public static final int     CT_VISA                     = 1;
  public static final int     CT_MC                       = 2;
  public static final int     CT_DEBIT                    = 3;
  public static final int     CT_DINERS                   = 4;
  public static final int     CT_DISCOVER                 = 5;
  public static final int     CT_AMEX                     = 6;
  public static final int     CT_JCB                      = 7;
  public static final int     CT_CHECK                    = 8;
  public static final int     CT_EBT                      = 9;
  public static final int     CT_TYME                     = 10;
  public static final int     CT_VALUTEC                  = 11;

  public static final int     NUM_CARDS                   = 11;

  // tsys card type codes
  public static final int     CARD_VISA                   = mesConstants.APP_CT_VISA;
  public static final int     CARD_VISA_CASH_ADVANCE      = mesConstants.APP_CT_VISA_CASH_ADVANCE;
  public static final int     CARD_VISA_LARGE_TICKET      = mesConstants.APP_CT_VISA_LARGE_TICKET;
  public static final int     CARD_MC                     = mesConstants.APP_CT_MC;
  public static final int     CARD_MC_CASH_ADVANCE        = mesConstants.APP_CT_MC_CASH_ADVANCE;
  public static final int     CARD_MC_LARGE_TICKET        = mesConstants.APP_CT_MC_LARGE_TICKET;
  public static final int     CARD_PRIVATE_LABEL_1        = mesConstants.APP_CT_PRIVATE_LABEL_1;
  public static final int     CARD_PRIVATE_LABEL_2        = mesConstants.APP_CT_PRIVATE_LABEL_2;
  public static final int     CARD_PRIVATE_LABEL_3        = mesConstants.APP_CT_PRIVATE_LABEL_3;
  public static final int     CARD_DINERS                 = mesConstants.APP_CT_DINERS_CLUB;
  public static final int     CARD_VISA_BUSINESS          = mesConstants.APP_CT_VISA_BUSINESS_CARD;
  public static final int     CARD_VISA_CHECK             = mesConstants.APP_CT_VISA_CHECK_CARD;
  public static final int     CARD_MC_BUSINESS            = mesConstants.APP_CT_MC_BUSINESS_CARD;
  public static final int     CARD_DISCOVER               = mesConstants.APP_CT_DISCOVER;
  public static final int     CARD_JCB                    = mesConstants.APP_CT_JCB;
  public static final int     CARD_AMEX                   = mesConstants.APP_CT_AMEX;
  public static final int     CARD_DEBIT                  = mesConstants.APP_CT_DEBIT;
  public static final int     CARD_EBT                    = mesConstants.APP_CT_EBT;
  public static final int     CARD_CHECK                  = mesConstants.APP_CT_CHECK_AUTH;
  public static final int     CARD_OTHER                  = mesConstants.APP_CT_OTHERS;
  public static final int     CARD_INTERNET               = mesConstants.APP_CT_INTERNET;
  public static final int     CARD_DIAL_PAY               = mesConstants.APP_CT_DIAL_PAY;
  public static final int     CARD_TYME_NETWORK           = mesConstants.APP_CT_TYME_NETWORK;

  // field sizes
  public static final int     MAX_SSN                     = 9;
  public static final int     MIN_SSN                     = 9;
  public static final int     MAX_PHONE                   = 10;
  public static final int     MIN_PHONE                   = 10;

  public static final String  SOURCE_CANCELLED_CHECK      = "Cancelled Check";
  public static final String  SOURCE_ACCOUNT_STATEMENT    = "Account Statement";
  public static final String  SOURCE_BANK_LETTER          = "Bank Letter";
  public static final String  SOURCE_COMPUTER_RECORD      = "Computer Record";
  public static final String  SOURCE_BY_PHONE             = "By Phone";
  public static final String  SOURCE_OTHER                = "Other";


  // state options
  public static final String states[] =
  {
    "select", "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA",
    "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MI",
    "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY",
    "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VT",
    "WA", "WI", "WV", "WY"
  };

  /*
  ** FIELDS
  */
  // svb extra info
  private String              svbCifNum                   = "";
  private String              svbCdNum                    = "";
  private double              svbCdCollateral             = 0.0;
  private String              svbAssoc                    = "";

  // nbsc extra info
  private String              nbscBankNum                 = "";
  private String              nbscAssNum                  = "";

  // discover extra info
  private String              discoverRepName               = "";
  private String              discoverRepId                 = "";
  private String              franchiseCode                 = "";
  private String              bankReferralNum               = "";
  private boolean             bankReferralApp               = false;
  private String              totalNumMultiMerchant         = "";
  private String              numMultiMerchant              = "";
  private String              multiMerchantMasterDba        = "";
  private String              multiMerchantMasterControlNum = "";


  //transcom extra info
  private String              cardholderPresent           = "";
  private String              cardholderNotPresent        = "";

  //verisign
  private String              salesRepChange              = "";
  private String              existingVerisign            = "";
  private String              promotionCode               = "";
  private String              channel                     = "";
  private boolean             shoppingCart                = false;


  //cbt extra info
  private String              cbtCreditScore              = "";
  private String              cbtAssNum                   = "";
  private String              cbtRepCode                  = "";


  //gold extra info
  private String              goldAccountType             = "";
  private String              goldAssNum                  = "";

  // business info
  private String              businessName                = "";
  private String              merchantNumber              = "";
  private String              businessLegalName           = "";
  private String              taxpayerId                  = "";
  private String              businessPhone               = "";
  private String              customerServicePhone        = "";
  private String              businessAddress1            = "";
  private String              businessAddress2            = "";
  private String              businessCity                = "";
  private int                 businessState               = 0;
  private String              businessCountry             = "US";
  private String              businessZip                 = "";
  private String              businessEmail               = "";
  private int                 establishedMonth            = 0;
  private String              establishedYear             = "";
  private String              contactNameFirst            = "";
  private String              contactNameLast             = "";
  private String              contactPhone                = "";
  private String              contactEmail                = "";
  private String              mailingName                 = "";
  private String              mailingAddress1             = "";
  private String              mailingAddress2             = "";
  private String              mailingCity                 = "";
  private int                 mailingState                = 0;
  private String              mailingZip                  = "";
  private String              businessFax                 = "";
  private String              product                     = "";
  private String              numLocations                = "";
  private String              locationYears               = "";
  private int                 businessType                = 0;
  private int                 industryType                = 0;
  private String              mccSicCode                  = "";
  private String              sicCode                     = "";
  private int                 locationType                = 0;
  private String              referringBank               = "";
  private int                 applicationType             = 0;
  private int                 transcomEbtOption           = 0;
  // processing history
  private int                 haveProcessed               = 0;
  private String              previousProcessor           = "";
  private int                 beingWon                    = 0;
  private String              beingWonReason              = "";
  private int                 statementsProvided          = 0;
  private int                 haveCanceled                = 0;
  private String              canceledProcessor           = "";
  private String              cancelReason                = "";
  private int                 cancelMonth                 = 0;
  private String              cancelYear                  = "";
  private String              agreement                   = "";
  private boolean             yesAgreeReq                 = false;

  // bb&t misc info
  private String              bbtCenterNum                = "";
  private String              bbtReferringOfficer         = "";
  // business checking account info
  private String              bankName                    = "";

  //used to determine whether or not to do account confirmation
  private boolean             accountConfirmationReq      = false;
  private boolean             accountSourceTypeReq        = false;

  private String              checkingAccount             = "";
  private String              transitRouting              = "";
  private String              confirmCheckingAccount      = "";
  private String              confirmTransitRouting       = "";

  private String              typeOfAcct                  = "";
  private String              sourceOfInfo                = "";
  private boolean             skipTypeError               = false;


  private String              transitRoutingBbtOther      = "";
  private String              yearsOpen                   = "";
  private String              bankAddress                 = "";
  private String              bankCity                    = "";
  private int                 bankState                   = 0;
  private String              bankZip                     = "";
  private String              bankPhone                   = "";
  // owner information
  private String              owner1FirstName             = "";
  private String              owner1LastName              = "";
  private String              owner1SSN                   = "";
  private String              owner1Percent               = "";
  private String              owner1Address1              = "";
  private String              owner1Address2              = "";
  private String              owner1City                  = "";
  private String              owner1Zip                   = "";
  private int                 owner1State                 = 0;
  private String              owner1Phone                 = "";
  private String              owner1Title                 = "";
  private int                 owner1SinceMonth            = 0;
  private String              owner1SinceYear             = "";
  // secondary owner  information
  private String              owner2FirstName             = "";
  private String              owner2LastName              = "";
  private String              owner2SSN                   = "";
  private String              owner2Percent               = "";
  private String              owner2Address1              = "";
  private String              owner2Address2              = "";
  private String              owner2City                  = "";
  private int                 owner2State                 = 0;
  private String              owner2Zip                   = "";
  private String              owner2Phone                 = "";
  private String              owner2Title                 = "";
  private int                 owner2SinceMonth            = 0;
  private String              owner2SinceYear             = "";
  // transaction information
  private String              monthlySales                = "";
  private String              vmcSales                    = "";
  private String              averageTicket               = "";
  private String              annualCount                 = "";
  private String              highVolumeMonths            = "NNNNNNNNNNNN";
  private boolean             seasonalMerchant            = false;
  private boolean             monthlySalesCom             = false;
  private boolean             vmcSalesCom                 = false;
  private boolean             averageTicketCom            = false;

  private String              motoPercentage              = "";
  private String              motoPercentageInternet      = "";
  private String              motoPercentageMailTele      = "";
  private String              motoPercentageMail          = "";
  private String              motoPercentageTelephone     = "";
  private String              internetSalesOption         = "";

  private int                 refundPolicy                = 0;
  private boolean             visaAccepted                = false;
  private boolean             mcAccepted                  = false;
  private boolean             debitAccepted               = false;
  private boolean             ebtAccepted                 = false;
  private boolean             tymeAccepted                = false;
  private String              fcsNumber                   = "";
  private boolean             dinrAccepted                = false;
  private String              dinrMerchId                 = "";
  private boolean             jcbAccepted                 = false;
  private String              jcbMerchId                  = "";
  private boolean             amexAccepted                = false;
  private String              amexMerchId                 = "";
  private String              amexRate                    = "";
  private boolean             amexSplitDial               = false;
  private boolean             amexPip                     = false;
  private boolean             amexNewAccount              = false;
  private boolean             discAccepted                = false;
  private boolean             discMotoRate                = false;
  private String              discMerchId                 = "";
  private String              discRate                    = "";
  private String              discFee                     = "";
  private boolean             discNewAccount              = false;

  // check fields
  private boolean             checkAccepted               = false;
  private String              checkProvider               = "";
  private String              checkProviderOther          = "";
  private String              checkMerchId                = "";
  private String              checkTermId                 = "";

  // valutec gift card fields
  private boolean             valutecAccepted             = false;
  private String              valutecMerchId              = "";
  private String              valutecTermId               = "";

  // POS Partner extended fields
  private String        posPartnerConnectivity            = "";
  private boolean       posPartnerLevelII                 = false;
  private boolean       posPartnerLevelIII                = false;
  private String        posPartnerOS                      = "";
  private Vector        posPartnerOSOptions               = new Vector();
  private boolean       supportsPosPartnerExtendedFields  = false;

  //bbt processor
  private String              bbtProcessor                = "";

  // product selection
  private int                 productType                 = 0;
  private int                 pcType                      = 0;
  private String              pcLicenses                  = "";
  private int                 internetType                = 0;
  private String              webUrl                      = "";
  private String              vitalProduct                = "";
  private String              thirdPartySoftware          = "";
  private String              vtEmail                     = "";
  private String              internetEmail               = "";
  private String              pgCertName                  = "";
  private String              pgEmail                     = "";
  private String              pgCard                      = "";

  private boolean             validEmail                  = false;

  // comments
  private String              comments                    = "";

  private int                 appType                     = mesConstants.APP_TYPE_MES;
  private boolean             recalculate                 = false;

  private int                 imprinterPlates             = 0;

  // drop down list options
  private boolean             optionsDataLoaded           = false;
  private Vector              yesNoOptions                = new Vector();
  private Vector              stateOptions                = new Vector();
  private Vector              monthOptions                = new Vector();
  private Vector              businessTypeOptions         = new Vector();

  private Vector              industryTypeOptions         = new Vector();

  private Vector              transIndustryTypeDesc       = new Vector();
  private Vector              transIndustryTypes          = new Vector();

  private Vector              expandedIndustryTypeDesc    = new Vector();
  private Vector              expandedIndustryTypes       = new Vector();

  private Vector              locationTypeOptions         = new Vector();
  private Vector              transLocationTypeOptions    = new Vector();
  private Vector              applicationTypeOptions      = new Vector();
  private Vector              refundPolicyOptions         = new Vector();
  private Vector              transRefundPolicyOptions    = new Vector();
  private Vector              pcTypeOptions               = new Vector();
  private Vector              pcTypeCodes                 = new Vector();
  private Vector              internetTypeOptions         = new Vector();
  private Vector              internetTypeCodes           = new Vector();
  private Vector              svbInternetTypeOptions      = new Vector();
  private Vector              svbInternetTypeCodes        = new Vector();
  private Vector              countryDesc                 = new Vector();
  private Vector              countryCode                 = new Vector();
  private Vector              transcomEbtOptions          = new Vector();

  private Vector              bbtBusinessTypeDesc         = new Vector();
  private Vector              bbtBusinessTypes            = new Vector();
  private Vector              bbtIndustryTypeDesc         = new Vector();
  private Vector              bbtIndustryTypes            = new Vector();
  private Vector              bbtLocationTypeDesc         = new Vector();
  private Vector              bbtLocationTypes            = new Vector();
  private Vector              bbtTransitRoutingDesc       = new Vector();
  private Vector              bbtTransitRoutingNumbers    = new Vector();
  private Vector              bbtProcessors               = new Vector();
  private Vector              bbtProcessorCodes           = new Vector();

  private Vector              transInternetSalesOptions         = new Vector();
  private Vector              transInternetSalesOptionsCodes    = new Vector();

  private Vector              discoverBankReferralNames         = new Vector();
  private Vector              discoverBankReferralNumbers       = new Vector();

  private Vector              discoverFranchiseDesc             = new Vector();
  private Vector              discoverFranchiseCode             = new Vector();

  private Vector              accountTypeDesc                   = new Vector();
  private Vector              accountTypeCode                   = new Vector();
  private Vector              infoSource                        = new Vector();
  private HashMap             questionableData                  = new HashMap();

  private Vector              rcNums                            = new Vector();

  /*
  ** CONSTRUCTOR
  */
  public MerchInfoBean()
  {
    loadOptionData();
  }
  public MerchInfoBean(String connectString)
  {
    super(connectString);

    loadOptionData();
  }

  /*
  ** METHOD loadOptionData
  */
  private void loadOptionData()
  {
    String[]          item  = null;
    PreparedStatement ps    = null;
    StringBuffer      qs    = new StringBuffer("");
    ResultSetIterator it    = null;
    ResultSet         rs    = null;

    try   // this try block is to make sure that the connection is closed
    {
      // yes/no options
      yesNoOptions.add("---");
      yesNoOptions.add("yes");
      yesNoOptions.add("no");

      // state options
      int s = Array.getLength(states);
      for (int i = 0; i < s; ++i)
      {
        stateOptions.add(states[i]);
      }

      // month options
      String months[] =
      {
        "select", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
      };
      s = Array.getLength(months);
      for (int i = 0; i < s; ++i)
      {
        monthOptions.add(months[i]);
      }
      months = null;

      //transcom ebt option
      transcomEbtOptions.add("select one");
      transcomEbtOptions.add("Cash Benefits");
      transcomEbtOptions.add("Food Stamps");
      transcomEbtOptions.add("Both");

      transInternetSalesOptions.add("select one");
      transInternetSalesOptions.add("Cardholder Present");
      transInternetSalesOptions.add("Cardholder Not Present");

      transInternetSalesOptionsCodes.add("");
      transInternetSalesOptionsCodes.add("1");
      transInternetSalesOptionsCodes.add("2");

      // business type options
      businessTypeOptions.add("select one");
      businessTypeOptions.add("Sole Proprietorship");
      businessTypeOptions.add("Corporation");
      businessTypeOptions.add("Partnership");
      businessTypeOptions.add("Medical or Legal Corporation");
      businessTypeOptions.add("Association/Estate/Trust");
      businessTypeOptions.add("Tax Exempt");
      businessTypeOptions.add("Government");
      businessTypeOptions.add("Limited Liability Company");

      //BB&T business desc
      bbtBusinessTypeDesc.add("select one");
      bbtBusinessTypeDesc.add("Sole Proprietorship");
      bbtBusinessTypeDesc.add("Corporation");
      bbtBusinessTypeDesc.add("Partnership");
      bbtBusinessTypeDesc.add("Non-Profit/Tax Exempt");

      //BB&T business types
      bbtBusinessTypes.add("");
      bbtBusinessTypes.add("1");
      bbtBusinessTypes.add("2");
      bbtBusinessTypes.add("3");
      bbtBusinessTypes.add("6");

      // industry type options
      industryTypeOptions.add("select one");
      industryTypeOptions.add("Retail");
      industryTypeOptions.add("Restaurant");
      industryTypeOptions.add("Hotel");
      industryTypeOptions.add("Motel");
      industryTypeOptions.add("Internet");
      industryTypeOptions.add("Services");
      industryTypeOptions.add("Direct Marketing");
      industryTypeOptions.add("Other");

      //Transcom industry type desc
      transIndustryTypeDesc.add("select one");
      transIndustryTypeDesc.add("Retail");
      transIndustryTypeDesc.add("Restaurant");
      transIndustryTypeDesc.add("Lodging");
      transIndustryTypeDesc.add("Supermarket");
      transIndustryTypeDesc.add("Internet");
      transIndustryTypeDesc.add("Services");
      transIndustryTypeDesc.add("Direct Marketing");
      transIndustryTypeDesc.add("Other");
      transIndustryTypeDesc.add("Cash Advance");

      //Transcom industry type options
      transIndustryTypes.add("");
      transIndustryTypes.add("1");
      transIndustryTypes.add("2");
      transIndustryTypes.add("10");
      transIndustryTypes.add("17");
      transIndustryTypes.add("6");
      transIndustryTypes.add("5");
      transIndustryTypes.add("7");
      transIndustryTypes.add("8");
      transIndustryTypes.add("9");

      //BB&T industry type desc
      bbtIndustryTypeDesc.add("select one");
      bbtIndustryTypeDesc.add("Retail");
      bbtIndustryTypeDesc.add("Restaurant");
      bbtIndustryTypeDesc.add("Lodging");
      bbtIndustryTypeDesc.add("Internet");
      bbtIndustryTypeDesc.add("Government/Purchasing Card");
      bbtIndustryTypeDesc.add("Retail/Government Purchasing Card");
      bbtIndustryTypeDesc.add("MOTO/Government Purchasing Card");
      bbtIndustryTypeDesc.add("Mail Order/Telephone Order");
      bbtIndustryTypeDesc.add("Manufactoring");
      bbtIndustryTypeDesc.add("Wholesale");
      bbtIndustryTypeDesc.add("Other");

      //BB&T industry type options
      bbtIndustryTypes.add("");
      bbtIndustryTypes.add("1");
      bbtIndustryTypes.add("2");
      bbtIndustryTypes.add("10");
      bbtIndustryTypes.add("6");
      bbtIndustryTypes.add("11");
      bbtIndustryTypes.add("12");
      bbtIndustryTypes.add("13");
      bbtIndustryTypes.add("14");
      bbtIndustryTypes.add("15");
      bbtIndustryTypes.add("16");
      bbtIndustryTypes.add("8");


      // location type options
      locationTypeOptions.add("select one");
      locationTypeOptions.add("Retail Storefront");
      locationTypeOptions.add("Internet Storefront");
      locationTypeOptions.add("Business Office");
      locationTypeOptions.add("Private Residence");
      locationTypeOptions.add("Other");
      locationTypeOptions.add("Bank");

      // location type options
      transLocationTypeOptions.add("select one");
      transLocationTypeOptions.add("Retail Storefront");
      transLocationTypeOptions.add("Internet Storefront");
      transLocationTypeOptions.add("Business Office");
      transLocationTypeOptions.add("Private Residence");
      transLocationTypeOptions.add("Separate Building");

      //BB&T location type desc
      bbtLocationTypeDesc.add("select one");
      bbtLocationTypeDesc.add("Shopping Center");
      bbtLocationTypeDesc.add("Mall");
      bbtLocationTypeDesc.add("Office Building");
      bbtLocationTypeDesc.add("Separate Building");
      bbtLocationTypeDesc.add("Commercial");
      bbtLocationTypeDesc.add("Industrial");
      bbtLocationTypeDesc.add("Residential");
      bbtLocationTypeDesc.add("Internet Storefront");
      bbtLocationTypeDesc.add("Other");

      //BB&T location types
      bbtLocationTypes.add("");
      bbtLocationTypes.add("8");
      bbtLocationTypes.add("11");
      bbtLocationTypes.add("3");
      bbtLocationTypes.add("7");
      bbtLocationTypes.add("9");
      bbtLocationTypes.add("10");
      bbtLocationTypes.add("4");
      bbtLocationTypes.add("2");
      bbtLocationTypes.add("5");

      // application type options
      applicationTypeOptions.add("select one");
      applicationTypeOptions.add("Single Outlet");
      applicationTypeOptions.add("Chain");
      applicationTypeOptions.add("Addl. Chain Outlet");

      // refund policy options
      refundPolicyOptions.add("select one");
      refundPolicyOptions.add("Refund in 30 Days or Less");
      refundPolicyOptions.add("No Refunds or Exchanges");
      refundPolicyOptions.add("Exchanges Only");
      refundPolicyOptions.add("Not Applicable");

      // refund policy options
      transRefundPolicyOptions.add("select one");
      transRefundPolicyOptions.add("Refund in 30 Days or Less");
      transRefundPolicyOptions.add("No Refunds or Exchanges");
      transRefundPolicyOptions.add("Exchanges Only");
      transRefundPolicyOptions.add("Not Applicable");
      transRefundPolicyOptions.add("Credit V/MC Account");

      bbtTransitRoutingDesc.add("select one or fill in textbox");
      bbtTransitRoutingDesc.add("North Carolina");
      bbtTransitRoutingDesc.add("South Carolina");
      bbtTransitRoutingDesc.add("Virginia");
      bbtTransitRoutingDesc.add("Georgia");
      bbtTransitRoutingDesc.add("Kentucky");
      bbtTransitRoutingDesc.add("West Virginia");
      bbtTransitRoutingDesc.add("Maryland");
      bbtTransitRoutingDesc.add("Alabama");
      bbtTransitRoutingDesc.add("DC");
      bbtTransitRoutingDesc.add("Tennessee");

      bbtTransitRoutingNumbers.add("");
      bbtTransitRoutingNumbers.add("053101121");
      bbtTransitRoutingNumbers.add("053201607");
      bbtTransitRoutingNumbers.add("051404260");
      bbtTransitRoutingNumbers.add("061113415");
      bbtTransitRoutingNumbers.add("042174486");
      bbtTransitRoutingNumbers.add("051503394");
      bbtTransitRoutingNumbers.add("055003308");
      bbtTransitRoutingNumbers.add("053904483");
      bbtTransitRoutingNumbers.add("054001547");
      bbtTransitRoutingNumbers.add("064208165");

      bbtProcessors.add("Vital");
      bbtProcessors.add("Global");
      bbtProcessors.add("PaymenTech");

      bbtProcessorCodes.add(Integer.toString(mesConstants.APP_PROCESSOR_TYPE_VITAL));
      bbtProcessorCodes.add(Integer.toString(mesConstants.APP_PROCESSOR_TYPE_GLOBAL));
      bbtProcessorCodes.add(Integer.toString(mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH));

      infoSource.add(SOURCE_CANCELLED_CHECK);
      infoSource.add(SOURCE_ACCOUNT_STATEMENT);
      infoSource.add(SOURCE_BANK_LETTER);
      infoSource.add(SOURCE_COMPUTER_RECORD);
      infoSource.add(SOURCE_BY_PHONE);
      infoSource.add(SOURCE_OTHER);

      // load pc and internet payement solution options from pos_category
      try
      {
        qs.setLength(0);
        qs.append("select * from pos_category ");
        qs.append("where pos_type = 3 or pos_type = 2 ");
        qs.append("and nvl(allowed_app_types, 'ALL') = 'ALL' ");
        qs.append("order by pos_code");
        ps = getPreparedStatement(qs.toString());
        rs = ps.executeQuery();

        pcTypeOptions.add("select one");
        pcTypeCodes.add("0");
        internetTypeOptions.add("select one");
        internetTypeCodes.add("0");
        svbInternetTypeOptions.add("select one");
        svbInternetTypeCodes.add("0");
        while (rs.next())
        {
          if (rs.getInt("pos_type") == 3)
          {
            pcTypeOptions.add(rs.getString("pos_desc"));
            pcTypeCodes.add(rs.getString("pos_code"));
          }
          else
          {
            internetTypeOptions.add(rs.getString("pos_desc"));
            internetTypeCodes.add(rs.getString("pos_code"));

            if(rs.getInt("pos_code") == 201 || rs.getInt("pos_code") == 202)
            {
              svbInternetTypeOptions.add(rs.getString("pos_desc"));
              svbInternetTypeCodes.add(rs.getString("pos_code"));
            }
          }
        }

        rs.close();
        ps.close();
        qs.setLength(0);
      }
      catch (Exception e)
      {
        addError("loadOptionData, loading from pos_category: " + e.toString());
        logEntry("loadOptionData, loading from pos_category: ", e.toString());
      }



      // load expanded industry types
      try
      {
        qs.setLength(0);
        qs.append("select   COMPANY_TYPE_CODE,  ");
        qs.append("         COMPANY_TYPE_DESC   ");
        qs.append("from     VISA_COMPANY_TYPES  ");
        qs.append("order by COMPANY_TYPE_DESC   ");

        ps = getPreparedStatement(qs.toString());
        rs = ps.executeQuery();

        expandedIndustryTypeDesc.add("select one");
        expandedIndustryTypes.add("");

        while (rs.next())
        {
          expandedIndustryTypeDesc.add(rs.getString("COMPANY_TYPE_DESC"));
          expandedIndustryTypes.add(rs.getString("COMPANY_TYPE_CODE"));
        }

        rs.close();
        ps.close();

        qs.setLength(0);
      }
      catch (Exception e)
      {
        addError("loadOptionData, loading from VISA_COMPANY_TYPES: " + e.toString());
        logEntry("loadOptionData, loading from VISA_COMPANY_TYPES: ", e.toString());
      }



      // load discover franchise codes
      try
      {
        qs.setLength(0);
        qs.append("select   CODE,                     ");
        qs.append("         DESCRIPTION               ");
        qs.append("from     DISCOVER_FRANCHISE_CODES  ");
        qs.append("order by DESCRIPTION               ");

        ps = getPreparedStatement(qs.toString());
        rs = ps.executeQuery();

        while (rs.next())
        {
          discoverFranchiseDesc.add(rs.getString("DESCRIPTION"));
          discoverFranchiseCode.add(rs.getString("CODE"));
        }

        rs.close();
        ps.close();

        qs.setLength(0);
      }
      catch (Exception e)
      {
        addError("loadOptionData, loading from DISCOVER_FRANCHISE_CODES: " + e.toString());
        logEntry("loadOptionData, loading from DISCOVER_FRANCHISE_CODES: ", e.toString());
      }



      // load country options from country
      try
      {
        qs.setLength(0);
        qs.append("select * from country ");
        qs.append("order by country_desc");
        ps = getPreparedStatement(qs.toString());
        rs = ps.executeQuery();

        while (rs.next())
        {
          countryDesc.add(rs.getString("country_desc"));
          countryCode.add(rs.getString("country_code"));
        }

        rs.close();
        ps.close();
        qs.setLength(0);
      }
      catch (Exception e)
      {
        addError("loadOptionData, loading from country: " + e.toString());
        logEntry("loadOptionData, loading from country: ", e.toString());
      }


      //get discover bank referrals
      try
      {
        qs.setLength(0);
        qs.append("select * from DISCOVER_REFERRAL_BANKS ");
        qs.append("order by bank_type,bank_name");
        ps = getPreparedStatement(qs.toString());
        rs = ps.executeQuery();

        discoverBankReferralNames.add("");
        discoverBankReferralNumbers.add("***");
        discoverBankReferralNames.add("REFERRAL BANKS");
        discoverBankReferralNumbers.add("***");
        boolean startOfList = true;

        while (rs.next())
        {
          if(rs.getString("bank_type").equals("REFERRAL ONLY") && startOfList)
          {
            startOfList = false;
            discoverBankReferralNames.add("");
            discoverBankReferralNumbers.add("***");
            discoverBankReferralNames.add("REFERRAL ONLY BANKS");
            discoverBankReferralNumbers.add("***");
          }

          discoverBankReferralNames.add(rs.getString("bank_association") + " - " + rs.getString("bank_name"));
          discoverBankReferralNumbers.add(rs.getString("bank_association"));
        }

        rs.close();
        ps.close();
        qs.setLength(0);
      }
      catch (Exception e)
      {
        addError("loadOptionData, loading from discover_bank_referrals: " + e.toString());
        logEntry("loadOptionData, loading from discover_bank_referrals: ", e.toString());
      }

      // load country options from country
      try
      {
        qs.setLength(0);
        qs.append("select * from bankacctype ");
        qs.append("order by bankacc_order");
        ps = getPreparedStatement(qs.toString());
        rs = ps.executeQuery();

        while (rs.next())
        {
          accountTypeDesc.add(rs.getString("bankacc_description"));
          accountTypeCode.add(rs.getString("bankacc_type"));
        }

        rs.close();
        ps.close();

        qs.setLength(0);
      }
      catch (Exception e)
      {
        addError("loadOptionData, loading from bankacctype: " + e.toString());
        logEntry("loadOptionData, loading from bankacctype: ", e.toString());
      }

      // load the POS Partner options
      qs.setLength(0);
      qs.append("select  os_code,         ");
      qs.append("        description      ");
      qs.append("from    pos_partner_os   ");
      qs.append("order by display_order   ");
      ps = getPreparedStatement(qs.toString());
      rs = ps.executeQuery();

      // cleanr the existing vector
      posPartnerOSOptions.removeAllElements();

      item = new String[2];
      item[0] = "";
      item[1] = "select one";
      posPartnerOSOptions.add(item);

      while (rs.next())
      {
        item = new String[2];
        item[0] = rs.getString("os_code");
        item[1] = rs.getString("description");
        posPartnerOSOptions.add(item);
      }
      rs.close();
      ps.close();

      // get SVB rc nums
      /*@lineinfo:generated-code*//*@lineinfo:905^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  association,
//                  rc_description
//          from    app_svb_rc_nums
//          order by rc_description
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  association,\n                rc_description\n        from    app_svb_rc_nums\n        order by rc_description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.setup.MerchInfoBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.setup.MerchInfoBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:911^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        rcNums.add(new RCNum(rs));
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadOptionData()",e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}

      if(!isConnectionStale())
      {
        cleanUp();
      }
    }

  }

  /*
  ** METHOD getMerchantData
  */
  private void getMerchantData(long appSeqNum)
  {
    StringBuffer      qs  = new StringBuffer("");
    PreparedStatement ps  = null;
    ResultSet         rs  = null;

    try
    {
      qs.append("select app_seq_num from merchant where app_seq_num = ? ");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        rs.close();
        ps.close();
        qs.setLength(0);
        qs.append("select merch_business_name,          ");
        qs.append("merch_federal_tax_id,                ");
        qs.append("merch_number,                        ");
        qs.append("merch_business_establ_month,         ");
        qs.append("merch_business_establ_year,          ");
        qs.append("merch_legal_name,                    ");
        qs.append("merch_mailing_name,                  ");
        qs.append("merch_email_address,                 ");
        qs.append("bustype_code,                        ");
        qs.append("industype_code,                      ");
        qs.append("merch_busgoodserv_descr,             ");
        qs.append("merch_num_of_locations,              ");
        qs.append("merch_years_at_loc,                  ");
        qs.append("loctype_code,                        ");
        qs.append("merch_prior_cc_accp_flag,            ");
        qs.append("merch_prior_processor,               ");
        qs.append("merch_cc_acct_term_flag,             ");
        qs.append("merch_cc_term_name,                  ");
        qs.append("merch_term_reason,                   ");
        qs.append("merch_term_year,                     ");
        qs.append("merch_term_month,                    ");
        qs.append("merch_month_tot_proj_sales,          ");
        qs.append("merch_month_visa_mc_sales,           ");
        qs.append("merch_average_cc_tran,               ");
        qs.append("merch_mail_phone_sales,              ");
        qs.append("merch_notes,                         ");
        qs.append("refundtype_code,                     ");
        qs.append("merch_application_type,              ");
        qs.append("svb_cif_num,                         ");
        qs.append("svb_cd_num,                          ");
        qs.append("nvl(svb_cd_collateral, 0.0) svb_cd_collateral, ");
        qs.append("svb_team_num,                        ");
        qs.append("merch_referring_bank,                ");
        qs.append("merch_agreement,                     ");
        qs.append("merch_prior_statements,              ");
        qs.append("asso_number,                         ");
        qs.append("merch_bank_number,                   ");
        qs.append("merch_web_url,                       ");
        qs.append("discover_rep_name,                   ");
        qs.append("discover_rep_id,                     ");
        qs.append("franchise_code,                      ");
        qs.append("bank_referral_num,                   ");
        qs.append("app_sic_code,                        ");
        qs.append("cbt_credit_score,                    ");
        qs.append("merch_rep_code,                      ");
        qs.append("existing_verisign,                   ");
        qs.append("account_type,                        ");
        qs.append("promotion_code,                      ");
        qs.append("client_data_1,                       ");
        qs.append("total_num_multi_merch,               ");
        qs.append("num_multi_merch,                     ");
        qs.append("multi_merch_master_cntrl,            ");
        qs.append("multi_merch_master_dba,              ");
        qs.append("seasonal_flag,                       ");
        qs.append("customer_service_phone               ");
        qs.append("from merchant where app_seq_num = ?  ");
        ps = getPreparedStatement(qs.toString());
        ps.setLong(1, appSeqNum);

        rs = ps.executeQuery();
        if(rs.next())
        {
          businessName      = isBlank(rs.getString("merch_business_name"))          ? "" : rs.getString("merch_business_name");

          taxpayerId        = isBlank(rs.getString("merch_federal_tax_id"))         ? "" : rs.getString("merch_federal_tax_id");
          taxpayerId        = padTo9(taxpayerId);

          businessEmail     = isBlank(rs.getString("merch_email_address"))          ? "" : rs.getString("merch_email_address");

          businessLegalName = isBlank(rs.getString("merch_legal_name"))             ? "" : rs.getString("merch_legal_name");

          mailingName       = isBlank(rs.getString("merch_mailing_name"))           ? "" : rs.getString("merch_mailing_name");

          establishedMonth  = isBlank(rs.getString("merch_business_establ_month"))  ? 0 : rs.getInt("merch_business_establ_month");

          establishedYear   = isBlank(rs.getString("merch_business_establ_year"))   ? "" : rs.getString("merch_business_establ_year");

          businessType      = isBlank(rs.getString("bustype_code"))                 ? 0 : rs.getInt("bustype_code");
          industryType      = isBlank(rs.getString("industype_code"))               ? 0 : rs.getInt("industype_code");
          locationType      = isBlank(rs.getString("loctype_code"))                 ? 0 : rs.getInt("loctype_code");
          cancelMonth       = isBlank(rs.getString("merch_term_month"))             ? 0 : rs.getInt("merch_term_month");
          refundPolicy      = isBlank(rs.getString("refundtype_code"))              ? 0 : rs.getInt("refundtype_code");
          applicationType   = isBlank(rs.getString("merch_application_type"))       ? 0 : rs.getInt("merch_application_type");
          merchantNumber    = isBlank(rs.getString("merch_number"))                 ? "" : rs.getString("merch_number");

          product           = isBlank(rs.getString("merch_busgoodserv_descr"))      ? "" : rs.getString("merch_busgoodserv_descr");
          numLocations      = isBlank(rs.getString("merch_num_of_locations"))       ? "" : rs.getString("merch_num_of_locations");
          locationYears     = isBlank(rs.getString("merch_years_at_loc"))           ? "" : rs.getString("merch_years_at_loc");
          previousProcessor = isBlank(rs.getString("merch_prior_processor"))        ? "" : rs.getString("merch_prior_processor");
          canceledProcessor = isBlank(rs.getString("merch_cc_term_name"))           ? "" : rs.getString("merch_cc_term_name");
          cancelReason      = isBlank(rs.getString("merch_term_reason"))            ? "" : rs.getString("merch_term_reason");
          cancelYear        = isBlank(rs.getString("merch_term_year"))              ? "" : rs.getString("merch_term_year");
          monthlySales      = isBlank(rs.getString("merch_month_tot_proj_sales"))   ? "" : rs.getString("merch_month_tot_proj_sales");
          vmcSales          = isBlank(rs.getString("merch_month_visa_mc_sales"))    ? "" : rs.getString("merch_month_visa_mc_sales");
          averageTicket     = isBlank(rs.getString("merch_average_cc_tran"))        ? "" : rs.getString("merch_average_cc_tran");
          motoPercentage    = isBlank(rs.getString("merch_mail_phone_sales"))       ? "" : rs.getString("merch_mail_phone_sales");
          comments          = isBlank(rs.getString("merch_notes"))                  ? "" : rs.getString("merch_notes");
          referringBank     = isBlank(rs.getString("merch_referring_bank"))         ? "" : rs.getString("merch_referring_bank");
          agreement         = isBlank(rs.getString("merch_agreement"))              ? "" : rs.getString("merch_agreement");
          goldAssNum        = isBlank(rs.getString("asso_number"))                  ? "" : rs.getString("asso_number");
          nbscAssNum        = isBlank(rs.getString("asso_number"))                  ? "" : rs.getString("asso_number");
          cbtAssNum         = isBlank(rs.getString("asso_number"))                  ? "" : rs.getString("asso_number");
          svbAssoc          = isBlank(rs.getString("asso_number"))                  ? "" : rs.getString("asso_number");
          cbtRepCode        = isBlank(rs.getString("merch_rep_code"))               ? "" : rs.getString("merch_rep_code");
          existingVerisign  = isBlank(rs.getString("existing_verisign"))            ? "" : rs.getString("existing_verisign");
          promotionCode     = isBlank(rs.getString("promotion_code"))               ? "" : rs.getString("promotion_code");
          channel           = isBlank(rs.getString("client_data_1"))                ? "" : rs.getString("client_data_1");
          nbscBankNum       = isBlank(rs.getString("merch_bank_number"))            ? "" : rs.getString("merch_bank_number");
          webUrl            = isBlank(rs.getString("merch_web_url"))                ? "" : rs.getString("merch_web_url");
          discoverRepName   = isBlank(rs.getString("discover_rep_name"))            ? "" : rs.getString("discover_rep_name");
          discoverRepId     = isBlank(rs.getString("discover_rep_id"))              ? "" : rs.getString("discover_rep_id");
          franchiseCode     = isBlank(rs.getString("franchise_code"))               ? "" : rs.getString("franchise_code");
          bankReferralNum   = isBlank(rs.getString("bank_referral_num"))            ? "" : rs.getString("bank_referral_num");


          if(!isBlank(bankReferralNum))
          {
            this.bankReferralApp = true;
          }

          totalNumMultiMerchant         = isBlank(rs.getString("total_num_multi_merch"))    ? "" : rs.getString("total_num_multi_merch");
          numMultiMerchant              = isBlank(rs.getString("num_multi_merch"))          ? "" : rs.getString("num_multi_merch");
          multiMerchantMasterDba        = isBlank(rs.getString("multi_merch_master_dba"))   ? "" : rs.getString("multi_merch_master_dba");
          multiMerchantMasterControlNum = isBlank(rs.getString("multi_merch_master_cntrl")) ? "" : rs.getString("multi_merch_master_cntrl");
          seasonalMerchant = (!isBlank(rs.getString("seasonal_flag"))  && rs.getString("seasonal_flag").equals("Y")) ? true : false;


          sicCode           = isBlank(rs.getString("app_sic_code"))                 ? "" : rs.getString("app_sic_code");

          String flag       = "";

          flag              = rs.getString("merch_prior_cc_accp_flag");
          if(isBlank(flag))
          {
            haveProcessed = 0;
          }
          else if(flag.equals("Y"))
          {
            haveProcessed = 1;
          }
          else
          {
            haveProcessed = 2;
          }

          flag              = rs.getString("merch_cc_acct_term_flag");
          if(isBlank(flag))
          {
            haveCanceled = 0;
          }
          else if(flag.equals("Y"))
          {
            haveCanceled = 1;
          }
          else
          {
            haveCanceled = 2;
          }

          flag              = rs.getString("merch_prior_statements");
          if(isBlank(flag))
          {
            statementsProvided = 0;
          }
          else if(flag.equals("Y"))
          {
            statementsProvided = 1;
          }
          else
          {
            statementsProvided = 2;
          }

          svbCifNum         = isBlank(rs.getString("svb_cif_num"))        ? "" : rs.getString("svb_cif_num");
          svbCdNum          = isBlank(rs.getString("svb_cd_num"))         ? "" : rs.getString("svb_cd_num");
          svbCdCollateral   = rs.getDouble("svb_cd_collateral");
          cbtCreditScore    = isBlank(rs.getString("cbt_credit_score"))   ? "" : rs.getString("cbt_credit_score");
          goldAccountType   = isBlank(rs.getString("account_type"))       ? "" : rs.getString("account_type");

          customerServicePhone = blankIfNull(rs.getString("customer_service_phone"));
        }
        rs.close();
        ps.close();
      }
      rs.close();
      ps.close();
    }
    catch (Exception e)
    {
      addError("getMerchantData: " + e.toString());
      logEntry("getMerchantData: ", e.toString());
    }
  }

  /*
  ** METHOD padTo9
  */
  private String padTo9(String taxId)
  {
    String paddedId = taxId;
    try
    {
      if (taxId.length() < 9 && taxId.length() > 0)
      {
        String zeroes = new String("000000000");
        paddedId = zeroes.substring(taxId.length()) + taxId;
      }
    }
    catch(Exception e)
    {
      paddedId = "";
    }
    return paddedId;
  }


  /*
  ** METHOD getStateIndex
  */
  private int getStateIndex(String state)
  {
    int foundState = 0;
    for (int i = 0; i < Array.getLength(states); ++i)
    {
      if (states[i].equals(state))
      {
        foundState = i;
      }
    }

    return foundState;
  }

  /*
  ** METHOD getAddressData
  */
  private void getAddressData(long appSeqNum)
  {
    StringBuffer      qs  = new StringBuffer("");
    PreparedStatement ps  = null;
    ResultSet         rs  = null;

    String state          = null;

    try
    {
      // build base statement
      qs.append("select                     ");
      qs.append("address_line1,             ");
      qs.append("address_line2,             ");
      qs.append("address_city,              ");
      qs.append("countrystate_code,         ");
      qs.append("country_code,              ");
      qs.append("address_zip,               ");
      qs.append("address_phone,             ");
      qs.append("address_fax                ");
      qs.append("from address               ");
      qs.append("where app_seq_num      = ? ");
      qs.append("and addresstype_code   = ? ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      // business address
      ps.setInt (2, 1);
      rs = ps.executeQuery();
      if(rs.next())
      {
        businessAddress1  = rs.getString("address_line1");
        businessAddress2  = rs.getString("address_line2");
        businessCity      = rs.getString("address_city");
        businessState     = getStateIndex(rs.getString("countrystate_code"));
        businessZip       = rs.getString("address_zip");
        businessCountry   = isBlank(rs.getString("country_code")) ? businessCountry : rs.getString("country_code").trim();
        businessPhone     = rs.getString("address_phone");
        businessPhone     = clearZero(businessPhone);
        businessFax       = rs.getString("address_fax");
        businessFax       = clearZero(businessFax);
      }
      rs.close();

      // mailing address
      ps.setInt (2, 2);
      rs = ps.executeQuery();
      if(rs.next())
      {
        mailingAddress1   = rs.getString("address_line1");
        mailingAddress2   = rs.getString("address_line2");
        mailingCity       = rs.getString("address_city");
        mailingState      = getStateIndex(rs.getString("countrystate_code"));
        mailingZip        = rs.getString("address_zip");
      }
      rs.close();

      // business owner 1
      ps.setInt (2, 4);
      rs = ps.executeQuery();
      if(rs.next())
      {
        owner1Address1    = rs.getString("address_line1");
        owner1Address2    = rs.getString("address_line2");
        owner1City        = rs.getString("address_city");
        owner1State       = getStateIndex(rs.getString("countrystate_code"));
        owner1Zip         = rs.getString("address_zip");
        owner1Phone       = rs.getString("address_phone");
        owner1Phone       = clearZero(owner1Phone);
      }
      rs.close();

      // business owner 2
      ps.setInt (2, 5);
      rs = ps.executeQuery();
      if(rs.next())
      {
        owner2Address1    = rs.getString("address_line1");
        owner2Address2    = rs.getString("address_line2");
        owner2City        = rs.getString("address_city");
        owner2State       = getStateIndex(rs.getString("countrystate_code"));
        owner2Zip         = rs.getString("address_zip");
        owner2Phone       = rs.getString("address_phone");
        owner2Phone       = clearZero(owner2Phone);
      }
      rs.close();

      // business checking account bank
      ps.setInt (2, 9);
      rs = ps.executeQuery();
      if(rs.next())
      {
        bankAddress       = rs.getString("address_line1");
        bankCity          = rs.getString("address_city");
        bankState         = getStateIndex(rs.getString("countrystate_code"));
        bankZip           = rs.getString("address_zip");
        bankPhone         = rs.getString("address_phone");
      }
      rs.close();
      ps.close();
    }
    catch (Exception e)
    {
      addError("getAddressData: " + e.toString());
      logEntry("getAddressData: ", e.toString());
    }
  }

  /*
  ** METHOD getBusinessOwnerData
  */
  private void getBusinessOwnerData(long appSeqNum)
  {
    StringBuffer      qs  = new StringBuffer("");
    PreparedStatement ps  = null;
    ResultSet         rs  = null;

    try
    {
      qs.append("select busowner_last_name, ");
      qs.append("busowner_first_name,       ");
      qs.append("busowner_ssn,              ");
      qs.append("busowner_owner_perc,       ");
      qs.append("busowner_period_month,     ");
      qs.append("busowner_period_year,      ");
      qs.append("busowner_title             ");
      qs.append("from businessowner         ");
      qs.append("where app_seq_num  = ?     ");
      qs.append("and busowner_num   = ?     ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      // business owner 1
      ps.setInt (2, 1);
      rs = ps.executeQuery();
      if (rs.next())
      {
        owner1LastName    = rs.getString("busowner_last_name");
        owner1FirstName   = rs.getString("busowner_first_name");
        owner1SSN         = padTo9(clearZero(rs.getString("busowner_ssn")));
        owner1Percent     = clearZero(rs.getString("busowner_owner_perc"));
        owner1SinceMonth  = rs.getInt   ("busowner_period_month");
        owner1SinceYear   = clearZero(rs.getString("busowner_period_year"));
        owner1Title       = rs.getString("busowner_title");
      }
      rs.close();

      // business owner 2
      ps.setInt(2, 2);
      rs = ps.executeQuery();
      if (rs.next())
      {
        owner2LastName    = rs.getString("busowner_last_name");
        owner2FirstName   = rs.getString("busowner_first_name");
        owner2SSN         = padTo9(clearZero(rs.getString("busowner_ssn")));
        owner2Percent     = clearZero(rs.getString("busowner_owner_perc"));
        owner2SinceMonth  = rs.getInt   ("busowner_period_month");
        owner2SinceYear   = clearZero(rs.getString("busowner_period_year"));
        owner2Title       = rs.getString("busowner_title");
      }
      rs.close();
      ps.close();
    }
    catch (Exception e)
    {
      addError("getBusinessOwnerData: " + e.toString());
      logEntry("getBusinessOwnerData: ", e.toString());
    }
  }

  /*
  ** METHOD getMerchContactData
  */
  private void getMerchContactData(long appSeqNum)
  {
    StringBuffer      qs  = new StringBuffer("");
    PreparedStatement ps  = null;
    ResultSet         rs  = null;

    try
    {
      qs.append("select                       ");
      qs.append("merchcont_prim_first_name,   ");
      qs.append("merchcont_prim_last_name,    ");
      qs.append("merchcont_prim_phone,        ");
      qs.append("merchcont_prim_email         ");
      qs.append("from merchcontact            ");
      qs.append("where app_seq_num = ?        ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        contactNameFirst  = rs.getString("merchcont_prim_first_name");
        contactNameLast   = rs.getString("merchcont_prim_last_name");
        contactPhone      = rs.getString("merchcont_prim_phone");
        contactPhone      = clearZero(contactPhone);
        contactEmail      = isBlank(rs.getString("merchcont_prim_email")) ? "" : rs.getString("merchcont_prim_email");
      }

      rs.close();
      ps.close();
    }
    catch (Exception e)
    {
      addError("getMerchContactData: " + e.toString());
      logEntry("getMerchContactData: ", e.toString());
    }
  }

  /*
  ** METHOD getMerchBankData
  */
  private void getMerchBankData(long appSeqNum)
  {
    StringBuffer      qs  = new StringBuffer("");
    PreparedStatement ps  = null;
    ResultSet         rs  = null;

    try
    {
      qs.append("select                       ");
      qs.append("bankacc_type,                ");
      qs.append("merchbank_info_source,       ");
      qs.append("merchbank_name,              ");
      qs.append("merchbank_acct_num,          ");
      qs.append("merchbank_transit_route_num, ");
      qs.append("merchbank_num_years_open     ");
      qs.append("from merchbank               ");
      qs.append("where app_seq_num        = ? ");
      qs.append("and merchbank_acct_srnum = 1 ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();
      if (rs.next())
      {
        bankName                = rs.getString("merchbank_name");

        typeOfAcct              = rs.getString("bankacc_type");
        sourceOfInfo            = isBlank(rs.getString("merchbank_info_source")) ? "" : rs.getString("merchbank_info_source");

        checkingAccount         = rs.getString("merchbank_acct_num");
        confirmCheckingAccount  = checkingAccount;

        transitRouting          = rs.getString("merchbank_transit_route_num");
        confirmTransitRouting   = transitRouting;

        yearsOpen               = rs.getString("merchbank_num_years_open");
      }
      rs.close();
      ps.close();
    }
    catch (Exception e)
    {
      addError("getMerchBankData: " + e.toString());
      logEntry("getMerchBankData: ", e.toString());
    }
  }

  /*
  ** METHOD getMerchPayOptionData
  */
  private void getMerchPayOptionData(long appSeqNum)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1477^7*/

//  ************************************************************
//  #sql [Ctx] it = { select merchpo_card_merch_number,
//                 merchpo_provider_name,
//                 merchpo_rate,
//                 merchpo_fee,
//                 merchpo_split_dial,
//                 merchpo_pip,
//                 merchpo_tid,
//                 cardtype_code
//          from   merchpayoption
//          where  app_seq_num   = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select merchpo_card_merch_number,\n               merchpo_provider_name,\n               merchpo_rate,\n               merchpo_fee,\n               merchpo_split_dial,\n               merchpo_pip,\n               merchpo_tid,\n               cardtype_code\n        from   merchpayoption\n        where  app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.setup.MerchInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.setup.MerchInfoBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1489^7*/
      rs = it.getResultSet();

      while( rs.next() )
      {
        switch( rs.getInt("cardtype_code") )
        {
          case mesConstants.APP_CT_VISA:
            visaAccepted     = true;
            break;

          case mesConstants.APP_CT_MC:
            mcAccepted     = true;
            break;

          case mesConstants.APP_CT_DEBIT:
            debitAccepted     = true;
            fcsNumber         = rs.getString("merchpo_card_merch_number");
            if(fcsNumber == null || fcsNumber.equals("0"))
            {
              fcsNumber = "";
            }
            break;

          case mesConstants.APP_CT_EBT:
            ebtAccepted = true;
            fcsNumber   = rs.getString("merchpo_card_merch_number");

            if(fcsNumber == null || fcsNumber.equals("0"))
            {
              fcsNumber = "";
            }
            break;

          case mesConstants.APP_CT_TYME_NETWORK:
            tymeAccepted = true;
            break;

          case mesConstants.APP_CT_DINERS_CLUB:
            dinrAccepted      = true;
            dinrMerchId       = rs.getString("merchpo_card_merch_number");

            if(dinrMerchId == null || dinrMerchId.equals("0"))
            {
              dinrMerchId = "";
            }

            // pre-fill with zeroes because sometimes diners merchant numbers start
            // with a zero
            if(!(dinrMerchId.equals("") && dinrMerchId.length() < 10))
            {
              StringBuffer newDinersId = new StringBuffer("");
              for(int i=0; i < (10 - dinrMerchId.length()); ++i)
              {
                newDinersId.append("0");
              }
              newDinersId.append(dinrMerchId);

              dinrMerchId = newDinersId.toString();
            }
            break;

          case mesConstants.APP_CT_JCB:
            jcbAccepted       = true;
            jcbMerchId        = rs.getString("merchpo_card_merch_number");

            if(jcbMerchId == null || jcbMerchId.equals("0"))
            {
              jcbMerchId = "";
            }
            break;

          case mesConstants.APP_CT_AMEX:
            amexAccepted      = true;
            amexMerchId       = rs.getString("merchpo_card_merch_number");
            amexRate          = rs.getString("merchpo_rate");
            if(!isBlank(rs.getString("merchpo_split_dial")))
            {
              amexSplitDial     = rs.getString("merchpo_split_dial").equals("Y");
            }

            if(!isBlank(rs.getString("merchpo_pip")))
            {
              amexPip           = rs.getString("merchpo_pip").equals("Y");
            }

            if (isBlank(amexRate))
            {
              amexRate = "";  //replaces possible null.
            }
            else
            {
              amexNewAccount  = true;  //for verisign apps, checks the new account box
            }

            if(amexMerchId == null || amexMerchId.equals("0"))
            {
              amexMerchId = "";
            }
            break;

          case mesConstants.APP_CT_DISCOVER:
            discAccepted      = true;
            discMerchId       = rs.getString("merchpo_card_merch_number");
            discRate          = rs.getString("merchpo_rate");
            if (isBlank(discRate))
            {
              discRate = "";  //replaces possible null.
            }
            else
            {
              discNewAccount  = true;  //for verisign apps, checks the new account box
            }

            discFee          = rs.getString("merchpo_fee");
            if (isBlank(discFee))
            {
              discFee = "";  //replaces possible null.
            }
            else
            {
              discNewAccount  = true;  //for verisign apps, checks the new account box
            }

            if(discMerchId == null || discMerchId.equals("0"))
            {
              discMerchId = "";
            }
            break;

          case mesConstants.APP_CT_CHECK_AUTH:
            checkAccepted       = true;
            checkMerchId        = rs.getString("merchpo_card_merch_number");
            checkTermId         = rs.getString("merchpo_tid");
            checkProvider       = rs.getString("merchpo_provider_name");
            checkProviderOther  = "";

            if(checkMerchId == null)
            {
              checkMerchId = "0";
            }
            if( checkProvider.substring(0,7).equals("CPOther") )
            {
              checkProviderOther  = checkProvider.substring(8);
              checkProvider       = checkProvider.substring(0,7);
            }
            break;

          case mesConstants.APP_CT_VALUTEC_GIFT_CARD:
            valutecAccepted   = true;
            valutecMerchId    = rs.getString("merchpo_card_merch_number");
            valutecTermId     = rs.getString("merchpo_tid");

            if(valutecMerchId == null)
            {
              valutecMerchId = "";
            }
            break;
        }
      }
      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      addError("getMerchPayOptionData: " + e.toString());
      logEntry("getMerchPayOptionData: ", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      cleanUp();
    }
  }

  /*
  ** METHOD getMerchPosData
  */
  private void getMerchPosData(long appSeqNum)
  {
    ResultSetIterator it    = null;
    ResultSet         rs    = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1674^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mpos.pos_code                 as pos_code,
//                  mpos.pos_param                as pos_param,
//                  nvl(mpos.pp_connectivity,'0') as pp_connectivity,
//                  nvl(mpos.pp_level_ii,'N')     as pp_level_ii,
//                  nvl(mpos.pp_level_iii,'N')    as pp_level_iii,
//                  nvl(mpos.pp_os,'')            as pp_os,
//                  pc.pos_type                   as pos_type,
//                  mpos.pg_email                 as pg_email
//          from    merch_pos    mpos,
//                  pos_category pc
//          where   mpos.app_seq_num = :appSeqNum and
//                  pc.pos_code    = mpos.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mpos.pos_code                 as pos_code,\n                mpos.pos_param                as pos_param,\n                nvl(mpos.pp_connectivity,'0') as pp_connectivity,\n                nvl(mpos.pp_level_ii,'N')     as pp_level_ii,\n                nvl(mpos.pp_level_iii,'N')    as pp_level_iii,\n                nvl(mpos.pp_os,'')            as pp_os,\n                pc.pos_type                   as pos_type,\n                mpos.pg_email                 as pg_email\n        from    merch_pos    mpos,\n                pos_category pc\n        where   mpos.app_seq_num =  :1  and\n                pc.pos_code    = mpos.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.setup.MerchInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.setup.MerchInfoBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1688^7*/
      rs = it.getResultSet();

      if (rs.next())
      {
        productType = rs.getInt("pos_type");
        switch (productType)
        {
          case mesConstants.POS_DIAL_TERMINAL:
          case mesConstants.POS_WIRELESS_TERMINAL:
          case mesConstants.POS_DIAL_AUTH:
          case mesConstants.POS_STAGE_ONLY:
          case mesConstants.POS_GLOBAL_PC:
          case mesConstants.POS_GPS:
          case mesConstants.POS_CHARGE_EXPRESS:
          case mesConstants.POS_CHARGE_PRO:
          default:
            break;

          case mesConstants.POS_INTERNET:
            internetType  = rs.getInt   ("pos_code");

            if(internetType == mesConstants.APP_MPOS_ESTOREFRONT_SHOPPING_CART_LINK)
            {
              setShoppingCart();
            }

            webUrl        = rs.getString("pos_param");
            internetEmail = rs.getString("pg_email");
            break;

          case mesConstants.POS_PC:
            pcType                  = rs.getInt   ("pos_code");

            // extract the extended fields
            posPartnerConnectivity  = rs.getString("pp_connectivity");
            posPartnerLevelII       = rs.getString("pp_level_ii").equals("Y");
            posPartnerLevelIII      = rs.getString("pp_level_iii").equals("Y");
            posPartnerOS            = rs.getString("pp_os");
            break;

          case mesConstants.POS_OTHER:
            vitalProduct  = rs.getString("pos_param");
            break;

          case mesConstants.POS_THIRD_PARTY_SOFTWARE:
            thirdPartySoftware = rs.getString("pos_param");
            break;

          case mesConstants.POS_VIRTUAL_TERMINAL:
            vtEmail = rs.getString("pos_param");
            break;

          case mesConstants.POS_PAYMENT_GATEWAY:
            pgCertName =    rs.getString("pos_param");
            pgEmail    =    rs.getString("pg_email");
            pgCard     =    rs.getString("pos_code");
            break;
        }
      }
      rs.close();
    }
    catch(Exception e)
    {
      addError("getMerchPosData: " + e.toString());
      logEntry("getMerchPosData()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }

  /*
  ** METHOD getMerchEquipmentData
  */
  private void getMerchEquipmentData(long appSeqNum)
  {
    StringBuffer      qs    = new StringBuffer("");
    PreparedStatement ps    = null;
    ResultSet         rs    = null;

    try
    {
      // get pc licenses
      qs.append("select merchequip_equip_quantity ");
      qs.append("from merchequipment              ");
      qs.append("where app_seq_num = ? and        ");
      qs.append("equip_model = 'PCPS'             ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong  (1,appSeqNum);

      rs = ps.executeQuery();
      if(rs.next())
      {
        pcLicenses = rs.getString("merchequip_equip_quantity");
      }
      rs.close();
      ps.close();

      // get imprinter plate quantity
      qs.setLength(0);
      qs.append("select   merchequip_equip_quantity ");
      qs.append("from     merchequipment            ");
      qs.append("where    app_seq_num     = ?       ");
      qs.append("and      equiptype_code  = ?       ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setInt(2, this.EQUIP_TYPE_IMPRINTER_PLATE);

      rs = ps.executeQuery();
      if(rs.next())
      {
        this.imprinterPlates = rs.getInt("merchequip_equip_quantity");
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      addError("getMerchEquipmentData: " + e.toString());
      logEntry("getMerchEquipmentData: ", e.toString());
    }
  }

  private void getMerchTranscomData(long appSeqNum)
  {
    StringBuffer      qs    = new StringBuffer("");
    PreparedStatement ps    = null;
    ResultSet         rs    = null;

    try
    {

      qs.append("select                     ");
      qs.append("annual_transaction_count,  ");
      qs.append("seasonal_merchant_flag,    ");
      qs.append("high_volume_months,        ");
      qs.append("ebt_option,                ");
      qs.append("perc_mail,                 ");
      qs.append("perc_telephone,            ");
      qs.append("perc_internet,             ");
      qs.append("mcc_sic_code,              ");
      qs.append("internet_sales_option,     ");
      qs.append("cardholder_present,        ");
      qs.append("cardholder_not_present     ");
      qs.append("from transcom_merchant     ");
      qs.append("where app_seq_num = ?      ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      qs.setLength(0);

      if (rs.next())
      {
        annualCount       = isBlank(rs.getString("annual_transaction_count")) ? ""              : rs.getString("annual_transaction_count");
        mccSicCode        = isBlank(rs.getString("mcc_sic_code"))             ? ""              : rs.getString("mcc_sic_code");
        highVolumeMonths  = isBlank(rs.getString("high_volume_months"))       ? "NNNNNNNNNNNN"  : rs.getString("high_volume_months");

        seasonalMerchant  = (!isBlank(rs.getString("seasonal_merchant_flag")) && (rs.getString("seasonal_merchant_flag")).equals("Y")) ? true : false;

        if(!isBlank(rs.getString("ebt_option")))
        {
          transcomEbtOption = rs.getInt("ebt_option");
        }

        motoPercentageMail        = isBlank(rs.getString("perc_mail"))      ? "" : rs.getString("perc_mail");
        motoPercentageTelephone   = isBlank(rs.getString("perc_telephone")) ? "" : rs.getString("perc_telephone");

        //to go back to the way it was.. just get rid of this line..
        motoPercentageMailTele    = sumOfMailAndTele(motoPercentageMail,motoPercentageTelephone);

        motoPercentageInternet    = isBlank(rs.getString("perc_internet"))          ? "" : rs.getString("perc_internet");
        internetSalesOption       = isBlank(rs.getString("internet_sales_option"))  ? "" : rs.getString("internet_sales_option");

        cardholderPresent         = isBlank(rs.getString("cardholder_present"))     ? "" : rs.getString("cardholder_present");
        cardholderNotPresent      = isBlank(rs.getString("cardholder_not_present")) ? "" : rs.getString("cardholder_not_present");
      }

      rs.close();
      ps.close();

    }
    catch (Exception e)
    {
      addError("getMerchTranscomData: " + e.toString());
      logEntry("getMerchTranscomData: ", e.toString());
    }
  }

  private String sumOfMailAndTele(String mailPerc, String telePerc)
  {
    int mail = 0;
    int tele = 0;
    int sum  = 0;

    try
    {
      mail = Integer.parseInt(mailPerc);
    }
    catch(Exception e)
    {
      mail = 0;
    }

    try
    {
      tele = Integer.parseInt(telePerc);
    }
    catch(Exception e)
    {
      tele = 0;
    }

    sum = mail + tele;

    return Integer.toString(sum);
  }

  /*
  ** METHOD getData
  */
  public void getData(long appSeqNum)
  {
    try
    {
      connect();

      getMerchantData       (appSeqNum);
      getAddressData        (appSeqNum);
      getBusinessOwnerData  (appSeqNum);
      getMerchContactData   (appSeqNum);
      getMerchBankData      (appSeqNum);
      getMerchPayOptionData (appSeqNum);
      getMerchPosData       (appSeqNum);
      getMerchEquipmentData (appSeqNum);
      getMerchTranscomData  (appSeqNum);
    }
    catch(Exception e)
    {
      logEntry("getData(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** METHOD submitData
     puts a new record in application table, userId is stored here and app_created_date
     everything else is irrelevant.
  */
  protected void submitApplicationData(HttpServletRequest req, long appSeqNum)
  {
    PreparedStatement   ps                = null;
    ResultSet           rs                = null;
    StringBuffer        qs                = new StringBuffer("");
    String              appSrcTypeCode    = "USER";
    long                screenSequenceId  = 0;

    try
    {
      long userId = com.mes.support.HttpHelper.getLong(req, "userId");
      String userLogin = com.mes.support.HttpHelper.getString(req, "userLogin");

      ps = getPreparedStatement("select app_seq_num from application where app_seq_num = ?");
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {

        //if already exists and we had a sales Rep Change, then make the change
        if(!isBlank(salesRepChange))
        {
          int idx = salesRepChange.indexOf("*");
          if(idx > 0)
          {
            rs.close();
            ps.close();

            ps = getPreparedStatement("update application set app_user_id = ?, app_user_login = ? where app_seq_num = ?");
            ps.setString(1, salesRepChange.substring(0,idx));
            ps.setString(2, salesRepChange.substring(idx+1));
            ps.setLong(3, appSeqNum);

            ps.executeUpdate();
          }
        }

      }
      else
      {
        ps = getPreparedStatement("select appsrctype_code,screen_sequence_id from org_app where app_type = ?");
        ps.setInt(1,appType);
        rs = ps.executeQuery();

        if(rs.next())
        {
          appSrcTypeCode    = rs.getString("appsrctype_code");
          screenSequenceId  = rs.getLong("screen_sequence_id");
        }

        // this is a new (insert) scenario
        qs.append("insert into application (      ");
        qs.append("app_seq_num,                   ");
        qs.append("appsrctype_code,               ");
        qs.append("app_created_date,              ");
        qs.append("app_scrn_code,                 ");
        qs.append("app_user_id,                   ");
        qs.append("app_user_login,                ");
        qs.append("app_type,                      ");
        qs.append("screen_sequence_id)            ");
        qs.append("values (?,?,sysdate,?,?,?,?,?) ");

        ps = getPreparedStatement(qs.toString());

        ps.setLong  (1,appSeqNum);
        ps.setString(2,appSrcTypeCode);
        ps.setNull  (3,java.sql.Types.VARCHAR);
        ps.setLong  (4,userId);
        ps.setString(5,userLogin);
        ps.setInt   (6,appType);
        ps.setLong  (7,screenSequenceId);
        ps.executeUpdate();

        // notify interested parties of new application status
        AppNotifyBean anb = new AppNotifyBean();
        anb.notifyStatus(appSeqNum, mesConstants.APP_STATUS_INCOMPLETE, appType);
      }
    }
    catch(Exception e)
    {
      addError(e.toString());
      logEntry("submitApplicationData: ", e.toString());
    }
  }

  /*
  ** METHOD submitMerchantData
  */
  protected void submitMerchantData1(HttpServletRequest req, long appSeqNum)
  {
    boolean           isStale   = false;

    try
    {
      long appControlNum = com.mes.support.HttpHelper.getLong(req, "appControlNum");

      if(appControlNum == 0L)
      {
        addError("Your application has timed out within the allowed 30 minutes, to be considerate of your sensitive information. Please open a new application or call your sales representative for assistance at 1-888-898-7693.");
      }

      if(isConnectionStale())
      {
        isStale = true;
        connect();
      }

      int applicationCount;

      /*@lineinfo:generated-code*//*@lineinfo:2058^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)  \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.setup.MerchInfoBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   applicationCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2063^7*/

      long  tempTaxId       = (isBlank(taxpayerId) ? getDigits(owner1SSN) : getDigits(taxpayerId));
      int   tempEstYear     = parseInt(establishedYear);
      int   tempNumLoc      = parseInt(numLocations);
      int   tempYearsAtLoc  = parseInt(locationYears);

      String assoNumber     = "";
      switch(appType)
      {
        case mesConstants.APP_TYPE_CBT:
          assoNumber = cbtAssNum;
        break;
        case mesConstants.APP_TYPE_NBSC:
          assoNumber = nbscAssNum;
        break;
        case mesConstants.APP_TYPE_GOLD:
          assoNumber = goldAssNum;
        break;
        case mesConstants.APP_TYPE_SVB:
          assoNumber = svbAssoc;
        default:
        break;
      }

      // default daily discount
      String  dailyDiscount = "N";

      switch(appType)
      {
        case mesConstants.APP_TYPE_MES:
        case mesConstants.APP_TYPE_VERISIGN:
        case mesConstants.APP_TYPE_NSI:
          dailyDiscount = "Y";
          break;
      }

      String tempSeasonalMerch = isSeasonalMerchant() ? "Y" : "N";

      if(applicationCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:2104^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_business_name         = :this.businessName,
//                    merch_federal_tax_id        = :tempTaxId,
//                    merch_email_address         = :this.businessEmail,
//                    merch_legal_name            = :this.businessLegalName,
//                    merch_mailing_name          = :this.mailingName,
//                    merch_business_establ_month = :this.establishedMonth,
//                    merch_business_establ_year  = :tempEstYear,
//                    bustype_code                = :this.businessType,
//                    industype_code              = :this.industryType,
//                    merch_busgoodserv_descr     = :this.product,
//                    merch_num_of_locations      = :tempNumLoc,
//                    merch_years_at_loc          = :tempYearsAtLoc,
//                    loctype_code                = :this.locationType,
//                    merch_application_type      = :this.applicationType,
//                    svb_cif_num                 = :this.svbCifNum,
//                    svb_cd_num                  = :this.svbCdNum,
//                    svb_cd_collateral           = :this.svbCdCollateral,
//                    merch_bank_number           = :this.nbscBankNum,
//                    asso_number                 = :assoNumber,
//                    merch_rep_code              = :this.cbtRepCode,
//                    existing_verisign           = :this.existingVerisign,
//                    promotion_code              = :this.promotionCode,
//                    client_data_1               = :this.channel,
//                    discover_rep_name           = :this.discoverRepName,
//                    discover_rep_id             = :this.discoverRepId,
//                    franchise_code              = :this.franchiseCode,
//                    bank_referral_num           = :this.bankReferralNum,
//                    app_sic_code                = :this.sicCode,
//                    cbt_credit_score            = :this.cbtCreditScore,
//                    account_type                = :this.goldAccountType,
//                    total_num_multi_merch       = :this.totalNumMultiMerchant,
//                    num_multi_merch             = :this.numMultiMerchant,
//                    multi_merch_master_dba      = :this.multiMerchantMasterDba,
//                    multi_merch_master_cntrl    = :this.multiMerchantMasterControlNum,
//                    seasonal_flag               = :tempSeasonalMerch,
//                    merch_dly_discount_flag     = :dailyDiscount,
//                    customer_service_phone      = :getDigits(customerServicePhone)
//            where   app_seq_num                 = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1544 = getDigits(customerServicePhone);
   String theSqlTS = "update  merchant\n          set     merch_business_name         =  :1 ,\n                  merch_federal_tax_id        =  :2 ,\n                  merch_email_address         =  :3 ,\n                  merch_legal_name            =  :4 ,\n                  merch_mailing_name          =  :5 ,\n                  merch_business_establ_month =  :6 ,\n                  merch_business_establ_year  =  :7 ,\n                  bustype_code                =  :8 ,\n                  industype_code              =  :9 ,\n                  merch_busgoodserv_descr     =  :10 ,\n                  merch_num_of_locations      =  :11 ,\n                  merch_years_at_loc          =  :12 ,\n                  loctype_code                =  :13 ,\n                  merch_application_type      =  :14 ,\n                  svb_cif_num                 =  :15 ,\n                  svb_cd_num                  =  :16 ,\n                  svb_cd_collateral           =  :17 ,\n                  merch_bank_number           =  :18 ,\n                  asso_number                 =  :19 ,\n                  merch_rep_code              =  :20 ,\n                  existing_verisign           =  :21 ,\n                  promotion_code              =  :22 ,\n                  client_data_1               =  :23 ,\n                  discover_rep_name           =  :24 ,\n                  discover_rep_id             =  :25 ,\n                  franchise_code              =  :26 ,\n                  bank_referral_num           =  :27 ,\n                  app_sic_code                =  :28 ,\n                  cbt_credit_score            =  :29 ,\n                  account_type                =  :30 ,\n                  total_num_multi_merch       =  :31 ,\n                  num_multi_merch             =  :32 ,\n                  multi_merch_master_dba      =  :33 ,\n                  multi_merch_master_cntrl    =  :34 ,\n                  seasonal_flag               =  :35 ,\n                  merch_dly_discount_flag     =  :36 ,\n                  customer_service_phone      =  :37 \n          where   app_seq_num                 =  :38";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.setup.MerchInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.businessName);
   __sJT_st.setLong(2,tempTaxId);
   __sJT_st.setString(3,this.businessEmail);
   __sJT_st.setString(4,this.businessLegalName);
   __sJT_st.setString(5,this.mailingName);
   __sJT_st.setInt(6,this.establishedMonth);
   __sJT_st.setInt(7,tempEstYear);
   __sJT_st.setInt(8,this.businessType);
   __sJT_st.setInt(9,this.industryType);
   __sJT_st.setString(10,this.product);
   __sJT_st.setInt(11,tempNumLoc);
   __sJT_st.setInt(12,tempYearsAtLoc);
   __sJT_st.setInt(13,this.locationType);
   __sJT_st.setInt(14,this.applicationType);
   __sJT_st.setString(15,this.svbCifNum);
   __sJT_st.setString(16,this.svbCdNum);
   __sJT_st.setDouble(17,this.svbCdCollateral);
   __sJT_st.setString(18,this.nbscBankNum);
   __sJT_st.setString(19,assoNumber);
   __sJT_st.setString(20,this.cbtRepCode);
   __sJT_st.setString(21,this.existingVerisign);
   __sJT_st.setString(22,this.promotionCode);
   __sJT_st.setString(23,this.channel);
   __sJT_st.setString(24,this.discoverRepName);
   __sJT_st.setString(25,this.discoverRepId);
   __sJT_st.setString(26,this.franchiseCode);
   __sJT_st.setString(27,this.bankReferralNum);
   __sJT_st.setString(28,this.sicCode);
   __sJT_st.setString(29,this.cbtCreditScore);
   __sJT_st.setString(30,this.goldAccountType);
   __sJT_st.setString(31,this.totalNumMultiMerchant);
   __sJT_st.setString(32,this.numMultiMerchant);
   __sJT_st.setString(33,this.multiMerchantMasterDba);
   __sJT_st.setString(34,this.multiMerchantMasterControlNum);
   __sJT_st.setString(35,tempSeasonalMerch);
   __sJT_st.setString(36,dailyDiscount);
   __sJT_st.setLong(37,__sJT_1544);
   __sJT_st.setLong(38,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2145^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:2149^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant
//            (
//              merc_cntrl_number,
//              merch_business_name,
//              merch_federal_tax_id,
//              merch_email_address,
//              merch_legal_name,
//              merch_mailing_name,
//              merch_business_establ_month,
//              merch_business_establ_year,
//              bustype_code,
//              industype_code,
//              merch_busgoodserv_descr,
//              merch_num_of_locations,
//              merch_years_at_loc,
//              loctype_code,
//              merch_application_type,
//              svb_cif_num,
//              svb_cd_num,
//              svb_cd_collateral,
//              merch_bank_number,
//              asso_number,
//              discover_rep_name,
//              discover_rep_id,
//              franchise_code,
//              bank_referral_num,
//              app_sic_code,
//              cbt_credit_score,
//              merch_rep_code,
//              existing_verisign,
//              promotion_code,
//              client_data_1,
//              account_type,
//              total_num_multi_merch,
//              num_multi_merch,
//              multi_merch_master_dba,
//              multi_merch_master_cntrl,
//              merch_dly_discount_flag,
//              seasonal_flag,
//              customer_service_phone,
//              app_seq_num
//            )
//            values
//            (
//              :appControlNum,
//              :this.businessName,
//              :tempTaxId,
//              :this.businessEmail,
//              :this.businessLegalName,
//              :this.mailingName,
//              :this.establishedMonth,
//              :tempEstYear,
//              :this.businessType,
//              :this.industryType,
//              :this.product,
//              :tempNumLoc,
//              :tempYearsAtLoc,
//              :this.locationType,
//              :this.applicationType,
//              :this.svbCifNum,
//              :this.svbCdNum,
//              :this.svbCdCollateral,
//              :this.nbscBankNum,
//              :assoNumber,
//              :this.discoverRepName,
//              :this.discoverRepId,
//              :this.franchiseCode,
//              :this.bankReferralNum,
//              :this.sicCode,
//              :this.cbtCreditScore,
//              :this.cbtRepCode,
//              :this.existingVerisign,
//              :this.promotionCode,
//              :this.channel,
//              :this.goldAccountType,
//              :this.totalNumMultiMerchant,
//              :this.numMultiMerchant,
//              :this.multiMerchantMasterDba,
//              :this.multiMerchantMasterControlNum,
//              :dailyDiscount,
//              :tempSeasonalMerch,
//              :getDigits(customerServicePhone),
//              :appSeqNum
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1545 = getDigits(customerServicePhone);
   String theSqlTS = "insert into merchant\n          (\n            merc_cntrl_number,\n            merch_business_name,\n            merch_federal_tax_id,\n            merch_email_address,\n            merch_legal_name,\n            merch_mailing_name,\n            merch_business_establ_month,\n            merch_business_establ_year,\n            bustype_code,\n            industype_code,\n            merch_busgoodserv_descr,\n            merch_num_of_locations,\n            merch_years_at_loc,\n            loctype_code,\n            merch_application_type,\n            svb_cif_num,\n            svb_cd_num,\n            svb_cd_collateral,\n            merch_bank_number,\n            asso_number,\n            discover_rep_name,\n            discover_rep_id,\n            franchise_code,\n            bank_referral_num,\n            app_sic_code,\n            cbt_credit_score,\n            merch_rep_code,\n            existing_verisign,\n            promotion_code,\n            client_data_1,\n            account_type,\n            total_num_multi_merch,\n            num_multi_merch,\n            multi_merch_master_dba,\n            multi_merch_master_cntrl,\n            merch_dly_discount_flag,\n            seasonal_flag,\n            customer_service_phone,\n            app_seq_num\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n             :28 ,\n             :29 ,\n             :30 ,\n             :31 ,\n             :32 ,\n             :33 ,\n             :34 ,\n             :35 ,\n             :36 ,\n             :37 ,\n             :38 ,\n             :39 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.setup.MerchInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appControlNum);
   __sJT_st.setString(2,this.businessName);
   __sJT_st.setLong(3,tempTaxId);
   __sJT_st.setString(4,this.businessEmail);
   __sJT_st.setString(5,this.businessLegalName);
   __sJT_st.setString(6,this.mailingName);
   __sJT_st.setInt(7,this.establishedMonth);
   __sJT_st.setInt(8,tempEstYear);
   __sJT_st.setInt(9,this.businessType);
   __sJT_st.setInt(10,this.industryType);
   __sJT_st.setString(11,this.product);
   __sJT_st.setInt(12,tempNumLoc);
   __sJT_st.setInt(13,tempYearsAtLoc);
   __sJT_st.setInt(14,this.locationType);
   __sJT_st.setInt(15,this.applicationType);
   __sJT_st.setString(16,this.svbCifNum);
   __sJT_st.setString(17,this.svbCdNum);
   __sJT_st.setDouble(18,this.svbCdCollateral);
   __sJT_st.setString(19,this.nbscBankNum);
   __sJT_st.setString(20,assoNumber);
   __sJT_st.setString(21,this.discoverRepName);
   __sJT_st.setString(22,this.discoverRepId);
   __sJT_st.setString(23,this.franchiseCode);
   __sJT_st.setString(24,this.bankReferralNum);
   __sJT_st.setString(25,this.sicCode);
   __sJT_st.setString(26,this.cbtCreditScore);
   __sJT_st.setString(27,this.cbtRepCode);
   __sJT_st.setString(28,this.existingVerisign);
   __sJT_st.setString(29,this.promotionCode);
   __sJT_st.setString(30,this.channel);
   __sJT_st.setString(31,this.goldAccountType);
   __sJT_st.setString(32,this.totalNumMultiMerchant);
   __sJT_st.setString(33,this.numMultiMerchant);
   __sJT_st.setString(34,this.multiMerchantMasterDba);
   __sJT_st.setString(35,this.multiMerchantMasterControlNum);
   __sJT_st.setString(36,dailyDiscount);
   __sJT_st.setString(37,tempSeasonalMerch);
   __sJT_st.setLong(38,__sJT_1545);
   __sJT_st.setLong(39,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2235^9*/
      }
    }
    catch (Exception e)
    {
      addError("submitMerchantData1: " + e.toString());
      logEntry("submitMerchantData1: ", e.toString());
    }
    finally
    {
      if(isStale)
      {
        cleanUp();
      }
    }
  }


  protected void submitMerchantData2(HttpServletRequest req, long appSeqNum)
  {
    boolean isStale   = false;

    try
    {
      long appControlNum = com.mes.support.HttpHelper.getLong(req, "appControlNum");

      if(appControlNum == 0L)
      {
        addError("Your application has timed out within the allowed 30 minutes, to be considerate of your sensitive information. Please open a new application or call your sales representative for assistance at 1-888-898-7693.");
      }

      if(isConnectionStale())
      {
        isStale = true;
        connect();
      }

      int applicationCount;
      /*@lineinfo:generated-code*//*@lineinfo:2273^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)  \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.setup.MerchInfoBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   applicationCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2278^7*/

      if(appType == mesConstants.APP_TYPE_TRANSCOM && isBlank(motoPercentage))
      {
        //to go back to normal.. just delete this line..
        divideUpPercents(motoPercentageMailTele); //this will divide motoPercentageMailTele into individual mail and tele %'s

        motoPercentage = sumUpPercents();
      }
      int       tempMotoPercentage  = parseInt(motoPercentage);
      String    haveProcessedFlag   = (haveProcessed == 1)      ? "Y" : "N";
      String    haveCanceledFlag    = (haveCanceled == 1)       ? "Y" : "N";
      String    stmtProvidedFlag    = (statementsProvided == 1) ? "Y" : "N";
      int       tempCancelYear      = parseInt(cancelYear);
      double    tempMonthlySales    = parseDouble(monthlySales);
      double    tempVMCSales        = parseDouble(vmcSales);
      double    tempAvgTicket       = parseDouble(averageTicket);


      if(applicationCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:2299^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_prior_cc_accp_flag    = :haveProcessedFlag,
//                    merch_prior_processor       = :this.previousProcessor,
//                    merch_cc_acct_term_flag     = :haveCanceledFlag,
//                    merch_cc_term_name          = :this.canceledProcessor,
//                    merch_term_reason           = :this.cancelReason,
//                    merch_term_month            = :this.cancelMonth,
//                    merch_term_year             = :tempCancelYear,
//                    merch_month_tot_proj_sales  = :tempMonthlySales,
//                    merch_month_visa_mc_sales   = :tempVMCSales,
//                    merch_average_cc_tran       = :tempAvgTicket,
//                    merch_mail_phone_sales      = :tempMotoPercentage,
//                    merch_notes                 = :this.comments,
//                    refundtype_code             = :this.refundPolicy,
//                    merch_referring_bank        = :this.referringBank,
//                    merch_prior_statements      = :stmtProvidedFlag,
//                    merch_web_url               = :this.webUrl
//            where   app_seq_num                 = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n          set     merch_prior_cc_accp_flag    =  :1 ,\n                  merch_prior_processor       =  :2 ,\n                  merch_cc_acct_term_flag     =  :3 ,\n                  merch_cc_term_name          =  :4 ,\n                  merch_term_reason           =  :5 ,\n                  merch_term_month            =  :6 ,\n                  merch_term_year             =  :7 ,\n                  merch_month_tot_proj_sales  =  :8 ,\n                  merch_month_visa_mc_sales   =  :9 ,\n                  merch_average_cc_tran       =  :10 ,\n                  merch_mail_phone_sales      =  :11 ,\n                  merch_notes                 =  :12 ,\n                  refundtype_code             =  :13 ,\n                  merch_referring_bank        =  :14 ,\n                  merch_prior_statements      =  :15 ,\n                  merch_web_url               =  :16 \n          where   app_seq_num                 =  :17";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.setup.MerchInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,haveProcessedFlag);
   __sJT_st.setString(2,this.previousProcessor);
   __sJT_st.setString(3,haveCanceledFlag);
   __sJT_st.setString(4,this.canceledProcessor);
   __sJT_st.setString(5,this.cancelReason);
   __sJT_st.setInt(6,this.cancelMonth);
   __sJT_st.setInt(7,tempCancelYear);
   __sJT_st.setDouble(8,tempMonthlySales);
   __sJT_st.setDouble(9,tempVMCSales);
   __sJT_st.setDouble(10,tempAvgTicket);
   __sJT_st.setInt(11,tempMotoPercentage);
   __sJT_st.setString(12,this.comments);
   __sJT_st.setInt(13,this.refundPolicy);
   __sJT_st.setString(14,this.referringBank);
   __sJT_st.setString(15,stmtProvidedFlag);
   __sJT_st.setString(16,this.webUrl);
   __sJT_st.setLong(17,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2319^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:2323^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant
//            (
//              merc_cntrl_number,
//              merch_prior_cc_accp_flag,
//              merch_prior_processor,
//              merch_cc_acct_term_flag,
//              merch_cc_term_name,
//              merch_term_reason,
//              merch_term_month,
//              merch_term_year,
//              merch_month_tot_proj_sales,
//              merch_month_visa_mc_sales,
//              merch_average_cc_tran,
//              merch_mail_phone_sales,
//              merch_notes,
//              refundtype_code,
//              merch_referring_bank,
//              merch_prior_statements,
//              merch_web_url,
//              app_seq_num
//            )
//            values
//            (
//              :appControlNum,
//              :haveProcessedFlag,
//              :this.previousProcessor,
//              :haveCanceledFlag,
//              :this.canceledProcessor,
//              :this.cancelReason,
//              :this.cancelMonth,
//              :tempCancelYear,
//              :tempMonthlySales,
//              :tempVMCSales,
//              :tempAvgTicket,
//              :tempMotoPercentage,
//              :this.comments,
//              :this.refundPolicy,
//              :this.referringBank,
//              :stmtProvidedFlag,
//              :this.webUrl,
//              :appSeqNum
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchant\n          (\n            merc_cntrl_number,\n            merch_prior_cc_accp_flag,\n            merch_prior_processor,\n            merch_cc_acct_term_flag,\n            merch_cc_term_name,\n            merch_term_reason,\n            merch_term_month,\n            merch_term_year,\n            merch_month_tot_proj_sales,\n            merch_month_visa_mc_sales,\n            merch_average_cc_tran,\n            merch_mail_phone_sales,\n            merch_notes,\n            refundtype_code,\n            merch_referring_bank,\n            merch_prior_statements,\n            merch_web_url,\n            app_seq_num\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.setup.MerchInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appControlNum);
   __sJT_st.setString(2,haveProcessedFlag);
   __sJT_st.setString(3,this.previousProcessor);
   __sJT_st.setString(4,haveCanceledFlag);
   __sJT_st.setString(5,this.canceledProcessor);
   __sJT_st.setString(6,this.cancelReason);
   __sJT_st.setInt(7,this.cancelMonth);
   __sJT_st.setInt(8,tempCancelYear);
   __sJT_st.setDouble(9,tempMonthlySales);
   __sJT_st.setDouble(10,tempVMCSales);
   __sJT_st.setDouble(11,tempAvgTicket);
   __sJT_st.setInt(12,tempMotoPercentage);
   __sJT_st.setString(13,this.comments);
   __sJT_st.setInt(14,this.refundPolicy);
   __sJT_st.setString(15,this.referringBank);
   __sJT_st.setString(16,stmtProvidedFlag);
   __sJT_st.setString(17,this.webUrl);
   __sJT_st.setLong(18,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2367^9*/
      }
    }
    catch (Exception e)
    {
      addError("submitMerchantData2: " + e.toString());
      logEntry("submitMerchantData2: ", e.toString());
    }
    finally
    {
      if(isStale)
      {
        cleanUp();
      }
    }
  }

  private void divideUpPercents(String mailTele)
  {
    int halfMailTele = 0;

    try
    {
      halfMailTele = Integer.parseInt(mailTele);
      halfMailTele /= 2;
    }
    catch(Exception e)
    {
      halfMailTele = 0;
    }

    motoPercentageMail      = Integer.toString(halfMailTele);
    motoPercentageTelephone = Integer.toString(halfMailTele);
  }

  private String sumUpPercents()
  {
    int mail = 0;
    int tele = 0;
    int inte = 0;
    int sum  = 0;

    try
    {
      mail = Integer.parseInt(motoPercentageMail);
    }
    catch(Exception e)
    {
      mail = 0;
    }

    try
    {
      tele = Integer.parseInt(motoPercentageTelephone);
    }
    catch(Exception e)
    {
      tele = 0;
    }

    try
    {
      inte = Integer.parseInt(motoPercentageInternet);
    }
    catch(Exception e)
    {
      inte = 0;
    }

    sum = mail + tele + inte;
    return Integer.toString(sum);
  }

  /*
  ** METHOD getStateString
  */
  private String getStateString(int stateIdx)
  {
    if (stateIdx == 0)
    {
      return "";
    }

    return states[stateIdx];
  }

  /*
  ** METHOD submitAddressData
  */
  protected void submitAddressData(long appSeqNum, int addressType)
  {
    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    boolean           recExists   = false;
    boolean           okToUpdate  = true;

    try
    {
      // see if a record already exists for this address type and app seq num
      qs.append("select count(app_seq_num) as cnt ");
      qs.append("from address                     ");
      qs.append("where app_seq_num    = ?         ");
      qs.append("and addresstype_code = ?         ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setInt (2, addressType);

      rs = ps.executeQuery();

      if(rs.next())
      {
        recExists = (rs.getInt("cnt") > 0);
      }

      rs.close();
      ps.close();

      qs.setLength(0);

      if(recExists)
      {
        qs.append("update address                   ");
        qs.append("set    address_line1     = ?,    ");
        qs.append("       address_line2     = ?,    ");
        qs.append("       address_city      = ?,    ");
        qs.append("       countrystate_code = ?,    ");
        qs.append("       address_zip       = ?,    ");
        qs.append("       country_code      = ?,    ");
        qs.append("       address_phone     = ?,    ");
        qs.append("       address_fax       = ?     ");
        qs.append("where  app_seq_num       = ?     ");
        qs.append("and    addresstype_code  = ?     ");
      }
      else
      {
        qs.append("insert into address (            ");
        qs.append("address_line1,                   ");
        qs.append("address_line2,                   ");
        qs.append("address_city,                    ");
        qs.append("countrystate_code,               ");
        qs.append("address_zip,                     ");
        qs.append("country_code,                    ");
        qs.append("address_phone,                   ");
        qs.append("address_fax,                     ");
        qs.append("app_seq_num,                     ");
        qs.append("addresstype_code)                ");
        qs.append("values (?,?,?,?,?,?,?,?,?,?)     ");
      }

      ps = getPreparedStatement(qs.toString());

      switch (addressType)
      {
        case mesConstants.ADDR_TYPE_BUSINESS:
          ps.setString(1,businessAddress1);
          ps.setString(2,businessAddress2);
          ps.setString(3,businessCity);
          ps.setString(4,getStateString(businessState));
          ps.setString(5,businessZip);
          ps.setString(6,businessCountry);
          ps.setLong  (7,getDigits(businessPhone));
          ps.setLong  (8,getDigits(businessFax));
          break;

        case mesConstants.ADDR_TYPE_MAILING:

          if(isBlank(mailingAddress1) && isBlank(mailingCity))
          {
            okToUpdate = false;
            break;
          }

          ps.setString(1,mailingAddress1);
          ps.setString(2,mailingAddress2);
          ps.setString(3,mailingCity);
          ps.setString(4,getStateString(mailingState));
          ps.setString(5,mailingZip);
          ps.setNull  (6,java.sql.Types.VARCHAR);
          ps.setLong  (7,0L);
          ps.setLong  (8,0L);
          break;

        case mesConstants.ADDR_TYPE_OWNER1:
          ps.setString(1,owner1Address1);
          ps.setString(2,owner1Address2);
          ps.setString(3,owner1City);
          ps.setString(4,getStateString(owner1State));
          ps.setString(5,owner1Zip);
          ps.setNull  (6,java.sql.Types.VARCHAR);
          ps.setLong  (7,getDigits(owner1Phone));
          ps.setLong  (8,0L);
          break;

        case mesConstants.ADDR_TYPE_OWNER2:

          if(isBlank(owner2Address1) && isBlank(owner2City))
          {
            okToUpdate = false;
            break;
          }

          ps.setString(1,owner2Address1);
          ps.setString(2,owner2Address2);
          ps.setString(3,owner2City);
          ps.setString(4,getStateString(owner2State));
          ps.setString(5,owner2Zip);
          ps.setNull  (6,java.sql.Types.VARCHAR);
          ps.setLong  (7,getDigits(owner2Phone));
          ps.setLong  (8,0L);
          break;

        case mesConstants.ADDR_TYPE_CHK_ACCT_BANK:
          ps.setString(1,bankAddress);
          ps.setString(2,"");
          ps.setString(3,bankCity);
          ps.setString(4,getStateString(bankState));
          ps.setString(5,bankZip);
          ps.setNull  (6,java.sql.Types.VARCHAR);
          ps.setLong  (7,getDigits(bankPhone));
          ps.setLong  (8,0L);
      }

      ps.setLong  (9, appSeqNum);
      ps.setInt   (10, addressType);

      if(okToUpdate && ps.executeUpdate() != 1)
      {
        logEntry("submitAddressData()","insert failed");
        addError("submitAddressData: Unable to insert/update record");
      }
    }
    catch(Exception e)
    {
      logEntry("submitAddressData (" + addressType + "): ", e.toString());
      addError("submitAddressData (" + addressType + "): " + e.toString());
    }
  }

  /*
  ** METHOD submitBusinessOwnerData
  */
  protected void submitBusinessOwnerData(long appSeqNum, int owner)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select app_seq_num     ");
      qs.append("from businessowner     ");
      qs.append("where app_seq_num = ?  ");
      qs.append("and busowner_num  = ?  ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setInt (2, owner);

      rs = ps.executeQuery();

      qs.setLength(0);

      if(rs.next())
      {
        qs.append("update businessowner set     ");
        qs.append("busowner_last_name     = ?,  ");
        qs.append("busowner_first_name    = ?,  ");
        qs.append("busowner_ssn           = ?,  ");
        qs.append("busowner_owner_perc    = ?,  ");
        qs.append("busowner_period_month  = ?,  ");
        qs.append("busowner_period_year   = ?,  ");
        qs.append("busowner_title         = ?   ");
        qs.append("where busowner_num     = ?   ");
        qs.append("and app_seq_num        = ?   ");
      }
      else
      {
        qs.append("insert into businessowner (  ");
        qs.append("busowner_last_name,          ");
        qs.append("busowner_first_name,         ");
        qs.append("busowner_ssn,                ");
        qs.append("busowner_owner_perc,         ");
        qs.append("busowner_period_month,       ");
        qs.append("busowner_period_year,        ");
        qs.append("busowner_title,              ");
        qs.append("busowner_num,                ");
        qs.append("app_seq_num               )  ");
        qs.append("values (?,?,?,?,?,?,?,?,?)   ");
      }

      rs.close();
      ps.close();

      ps = getPreparedStatement(qs.toString());

      switch (owner)
      {
        case 1:
          ps.setString(1,owner1LastName);
          ps.setString(2,owner1FirstName);
          ps.setLong  (3,getDigits(owner1SSN));
          ps.setDouble(4,parseDouble(owner1Percent));
          ps.setInt   (5,owner1SinceMonth);
          ps.setInt   (6,parseInt(owner1SinceYear));
          ps.setString(7,owner1Title);
          break;

        case 2:
          ps.setString(1,owner2LastName);
          ps.setString(2,owner2FirstName);
          ps.setLong  (3,getDigits(owner2SSN));
          ps.setDouble(4,parseDouble(owner2Percent));
          ps.setInt   (5,owner2SinceMonth);
          ps.setInt   (6,parseInt(owner2SinceYear));
          ps.setString(7,owner2Title);
          break;
      }

      ps.setInt   (8, owner);
      ps.setLong  (9, appSeqNum);

      if(ps.executeUpdate() != 1)
      {
        addError("submitBusinessOwnerData: Unable to insert/update record");
        logEntry("submitBusinessOwnerData()", "update failed");
      }

      ps.close();
    }
    catch(Exception e)
    {
      addError("submitBusinessOwnerData: " + e.toString());
      logEntry("submitBusinessOwnerData: ", e.toString());
    }
  }

  /*
  ** METHOD submitMerchContactData
  */
  protected void submitMerchContactData(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select app_seq_num     ");
      qs.append("from merchcontact      ");
      qs.append("where app_seq_num = ?  ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong  (1,appSeqNum);

      rs = ps.executeQuery();

      qs.setLength(0);

      if(rs.next())
      {
        qs.append("update merchcontact set          ");
        qs.append("merchcont_prim_first_name  = ?,  ");
        qs.append("merchcont_prim_last_name   = ?,  ");
        qs.append("merchcont_prim_phone       = ?,  ");
        qs.append("merchcont_prim_email       = ?   ");
        qs.append("where app_seq_num          = ?   ");
      }
      else
      {
        qs.append("insert into merchcontact   (     ");
        qs.append("merchcont_prim_first_name,       ");
        qs.append("merchcont_prim_last_name,        ");
        qs.append("merchcont_prim_phone,            ");
        qs.append("merchcont_prim_email,            ");
        qs.append("app_seq_num                )     ");
        qs.append("values (?,?,?,?,?)               ");
      }

      rs.close();
      ps.close();

      ps = getPreparedStatement(qs.toString());

      ps.setString(1,contactNameFirst);
      ps.setString(2,contactNameLast);
      ps.setLong  (3,getDigits(contactPhone));
      ps.setString(4,contactEmail);
      ps.setLong  (5,appSeqNum);

      if(ps.executeUpdate() != 1)
      {
        addError("submitMerchContactData: Unable to insert/update record");
        logEntry("submitMerchContactData()", "insert failed");
      }
    }
    catch(Exception e)
    {
      addError("submitMerchContactData: " + e.toString());
      logEntry("submitMerchContactData: ", e.toString());
    }
  }

  /*
  ** METHOD submitMerchBankData
  */
  protected void submitMerchBankData(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select app_seq_num           ");
      qs.append("from merchbank               ");
      qs.append("where app_seq_num        = ? ");
      qs.append("and merchbank_acct_srnum = 1 ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong  (1,appSeqNum);

      qs.setLength(0);

      rs = ps.executeQuery();

      if(rs.next())
      {
        qs.append("update merchbank set               ");
        qs.append("bankacc_type                 = ?,  ");
        qs.append("merchbank_info_source        = ?,  ");
        qs.append("merchbank_name               = ?,  ");
        qs.append("merchbank_acct_num           = ?,  ");
        qs.append("merchbank_transit_route_num  = ?,  ");
        qs.append("merchbank_num_years_open     = ?   ");
        qs.append("where app_seq_num            = ?   ");
        qs.append("and merchbank_acct_srnum     = ?   ");
      }
      else
      {
        qs.append("insert into merchbank       (  ");
        qs.append("bankacc_type,                  ");
        qs.append("merchbank_info_source,         ");
        qs.append("merchbank_name,                ");
        qs.append("merchbank_acct_num,            ");
        qs.append("merchbank_transit_route_num,   ");
        qs.append("merchbank_num_years_open,      ");
        qs.append("app_seq_num,                   ");
        qs.append("merchbank_acct_srnum         ) ");
        qs.append("values (?,?,?,?,?,?,?,?)       ");
      }

      ps = getPreparedStatement(qs.toString());

      ps.setString(1, (isBlank(typeOfAcct) ? "2" : typeOfAcct) );
      ps.setString(2, sourceOfInfo);
      ps.setString(3, bankName);
      ps.setString(4, checkingAccount);
      ps.setString(5, transitRouting);
      ps.setInt   (6, parseInt(yearsOpen));
      ps.setLong  (7, appSeqNum);
      ps.setInt   (8, 1);

      if(ps.executeUpdate() != 1)
      {
        addError("submitMerchBankData: Unable to insert/update record");
        logEntry("submitMerchBankData()", "insert/update failed");
      }

      ps.close();
    }
    catch(Exception e)
    {
      logEntry("submitAccountData: ", e.toString());
      addError("submitAccountData: " + e.toString());
    }
  }

  /*
  ** METHOD submitMerchPayOptionData
  */
  protected void submitMerchPayOptionData(long appSeqNum)
  {

    try
    {
      connect();

      int cardCount = 1;
      for (int i = 1; i <= NUM_CARDS; ++i)
      {
        int     cardType      = 0;
        long    merchId       = 0L;
        String  tid           = "";
        String  providerName  = "";
        boolean accepted      = false;
        String  merchpoRate   = "";
        String  merchpoFee    = "";
        boolean splitDial     = false;
        boolean pip           = false;

        switch (i)
        {
          case CT_VISA:
            cardType = CARD_VISA;
            if(appType == mesConstants.APP_TYPE_BBT)
            {
              accepted = visaAccepted;
            }
            else
            {
              accepted = true;
            }
            break;

          case CT_MC:
            cardType = CARD_MC;
            if(appType == mesConstants.APP_TYPE_BBT)
            {
              accepted = mcAccepted;
            }
            else
            {
              accepted = true;
            }
            break;

          case CT_DEBIT:
            cardType  = CARD_DEBIT;
            merchId   = parseLong(fcsNumber);
            accepted  = debitAccepted;
            break;

          case CT_EBT:
            cardType  = CARD_EBT;
            merchId   = parseLong(fcsNumber);
            accepted  = ebtAccepted;
            break;

          case CT_TYME:
            cardType  = CARD_TYME_NETWORK;
            merchId   = 0;
            accepted  = tymeAccepted;
            break;

          case CT_DINERS:
            cardType  = CARD_DINERS;
            merchId   = parseLong(dinrMerchId);
            accepted  = dinrAccepted;
            break;

          case CT_DISCOVER:
            cardType    = CARD_DISCOVER;
            merchId     = parseLong(discMerchId);
            merchpoRate = discRate;
            merchpoFee  = discFee;
            accepted    = discAccepted;
            break;

          case CT_AMEX:
            cardType    = CARD_AMEX;
            merchId     = parseLong(amexMerchId);
            merchpoRate = amexRate;
            accepted    = amexAccepted;
            splitDial   = amexSplitDial;
            pip         = amexPip;
            break;

          case CT_JCB:
            cardType  = CARD_JCB;
            merchId   = parseLong(jcbMerchId);
            accepted  = jcbAccepted;
            break;

          case CT_CHECK:
            cardType      = CARD_CHECK;
            merchId       = parseLong(checkMerchId);
            tid           = checkTermId;
            providerName  = checkProvider;
            if ( checkProvider.equals("CPOther") )
            {
              providerName += (":" + checkProviderOther);
            }
            accepted      = checkAccepted;
            break;

          case CT_VALUTEC:
            cardType      = mesConstants.APP_CT_VALUTEC_GIFT_CARD;
            merchId       = parseLong(valutecMerchId);
            tid           = valutecTermId;
            accepted      = valutecAccepted;
            break;
        }

        // delete any existing record
        /*@lineinfo:generated-code*//*@lineinfo:2963^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    merchpayoption
//            where   app_seq_num = :appSeqNum
//                    and cardtype_code = :cardType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    merchpayoption\n          where   app_seq_num =  :1 \n                  and cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.setup.MerchInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2969^9*/

        // if accepted, add the new record
        if (accepted)
        {
          cardCount++;

          // insert the record
          /*@lineinfo:generated-code*//*@lineinfo:2977^11*/

//  ************************************************************
//  #sql [Ctx] { insert into  merchpayoption
//              (
//                cardtype_code,
//                merchpo_card_merch_number,
//                merchpo_provider_name,
//                card_sr_number,
//                merchpo_rate,
//                app_seq_num,
//                merchpo_split_dial,
//                merchpo_pip,
//                merchpo_fee,
//                merchpo_tid
//              )
//              values
//              (
//                :cardType,
//                :merchId,
//                :providerName,
//                :cardCount,
//                :merchpoRate,
//                :appSeqNum,
//                :splitDial ? "Y" : "N",
//                :pip       ? "Y" : "N",
//                :merchpoFee,
//                :tid
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1546 = splitDial ? "Y" : "N";
 String __sJT_1547 = pip       ? "Y" : "N";
   String theSqlTS = "insert into  merchpayoption\n            (\n              cardtype_code,\n              merchpo_card_merch_number,\n              merchpo_provider_name,\n              card_sr_number,\n              merchpo_rate,\n              app_seq_num,\n              merchpo_split_dial,\n              merchpo_pip,\n              merchpo_fee,\n              merchpo_tid\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n               :7 ,\n               :8 ,\n               :9 ,\n               :10 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.setup.MerchInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,cardType);
   __sJT_st.setLong(2,merchId);
   __sJT_st.setString(3,providerName);
   __sJT_st.setInt(4,cardCount);
   __sJT_st.setString(5,merchpoRate);
   __sJT_st.setLong(6,appSeqNum);
   __sJT_st.setString(7,__sJT_1546);
   __sJT_st.setString(8,__sJT_1547);
   __sJT_st.setString(9,merchpoFee);
   __sJT_st.setString(10,tid);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3005^11*/
        }
      }
    }
    catch(Exception e)
    {
      addError("submitMerchPayOptionData: " + e.toString());
      logEntry("submitMerchPayOptionData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** METHOD submitMerchPosData
  */
  protected void submitMerchPosData(long appSeqNum)
  {
    StringBuffer      qs    = new StringBuffer("");
    PreparedStatement ps    = null;
    PreparedStatement ps1   = null;
    ResultSet         rs    = null;

    boolean addEmail = (productType ==  mesConstants.POS_PAYMENT_GATEWAY ||
                        productType ==  mesConstants.POS_INTERNET);
    try
    {
      qs.append("select app_seq_num     ");
      qs.append("from merch_pos         ");
      qs.append("where app_seq_num = ?  ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);

      qs.setLength(0);
      rs = ps.executeQuery();

      if (rs.next())
      {
        qs.append("update merch_pos set     ");
        qs.append("pos_code           = ?,  ");
        qs.append("pos_param          = ?   ");
        if(addEmail)
        {
            qs.append(", pg_email       = ? ");
        }
        qs.append("where app_seq_num  = ?   ");
      }
      else
      {
        qs.append("insert into merch_pos (  ");
        qs.append("pos_code,                ");
        qs.append("pos_param,               ");
        if(addEmail)
        {
          qs.append("pg_email,                ");
          qs.append("app_seq_num)             ");
          qs.append("values (?,?,?,?)         ");
        }
        else
        {
          qs.append("app_seq_num)             ");
          qs.append("values (?,?,?)           ");
        }
      }
      rs.close();
      ps.close();

      ps = getPreparedStatement(qs.toString());

      String param = "";
      String email = "";

      switch (productType)
      {
        case mesConstants.POS_DIAL_TERMINAL:
        default:
          ps.setInt(1,101);
          ps1 = getPreparedStatement("delete from tranchrg where app_seq_num = ? and (cardtype_code = 20 or cardtype_code = 21) " );
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          ps1 = getPreparedStatement("update merchant set merch_edc_flag = 'Y' where app_seq_num = ? ");
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          break;

        case mesConstants.POS_WIRELESS_TERMINAL:
          ps.setInt(1,901);
          ps1 = getPreparedStatement("delete from tranchrg where app_seq_num = ? and (cardtype_code = 20 or cardtype_code = 21) " );
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          ps1 = getPreparedStatement("update merchant set merch_edc_flag = 'Y' where app_seq_num = ? ");
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          break;


        case mesConstants.POS_PC:
          ps.setInt(1,302);

          /*@lineinfo:generated-code*//*@lineinfo:3111^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    tranchrg
//              where   app_seq_num = :appSeqNum and
//                      cardtype_code in ( 20,21 )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    tranchrg\n            where   app_seq_num =  :1  and\n                    cardtype_code in ( 20,21 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.setup.MerchInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3117^11*/

          /*@lineinfo:generated-code*//*@lineinfo:3119^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//              set     merch_edc_flag = 'N'
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n            set     merch_edc_flag = 'N'\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.setup.MerchInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3124^11*/

          // update with the extended pos partner values
          String levelIIStr   = (posPartnerLevelII  ? "Y" : "N");
          String levelIIIStr  = (posPartnerLevelIII ? "Y" : "N");
          /*@lineinfo:generated-code*//*@lineinfo:3129^11*/

//  ************************************************************
//  #sql [Ctx] { update merch_pos
//              set pp_connectivity = :posPartnerConnectivity,
//                  pp_level_ii     = :levelIIStr,
//                  pp_level_iii    = :levelIIIStr,
//                  pp_os           = :posPartnerOS
//              where app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update merch_pos\n            set pp_connectivity =  :1 ,\n                pp_level_ii     =  :2 ,\n                pp_level_iii    =  :3 ,\n                pp_os           =  :4 \n            where app_seq_num =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.setup.MerchInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,posPartnerConnectivity);
   __sJT_st.setString(2,levelIIStr);
   __sJT_st.setString(3,levelIIIStr);
   __sJT_st.setString(4,posPartnerOS);
   __sJT_st.setLong(5,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3137^11*/
          break;

        case mesConstants.POS_INTERNET:
          //if shopping cart is selected change internet type to reflect that.. this will only affect verisign apps..
          if(isShoppingCart())
          {
            internetType = mesConstants.APP_MPOS_ESTOREFRONT_SHOPPING_CART_LINK;
          }
          ps.setInt(1,internetType);
          param = webUrl;
          email = internetEmail;
          ps1 = getPreparedStatement("delete from tranchrg where app_seq_num = ? and cardtype_code = 21 " );
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          ps1 = getPreparedStatement("update merchant set merch_edc_flag = 'N' where app_seq_num = ? ");
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          break;

        case mesConstants.POS_DIAL_AUTH:
          ps.setInt(1,401);
          ps1 = getPreparedStatement("delete from tranchrg where app_seq_num = ? and cardtype_code = 20 " );
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          ps1 = getPreparedStatement("update merchant set merch_edc_flag = 'N' where app_seq_num = ? ");
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          break;

        case mesConstants.POS_OTHER:
          ps.setInt(1, 501);
          param = vitalProduct;
          ps1 = getPreparedStatement("delete from tranchrg where app_seq_num = ? and (cardtype_code = 20 or cardtype_code = 21) " );
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          ps1 = getPreparedStatement("update merchant set merch_edc_flag = 'N' where app_seq_num = ? ");
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          break;

        case mesConstants.POS_THIRD_PARTY_SOFTWARE:
          ps.setInt(1, 1001);
          param = thirdPartySoftware;
          ps1 = getPreparedStatement("delete from tranchrg where app_seq_num = ? and (cardtype_code = 20 or cardtype_code = 21) " );
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          ps1 = getPreparedStatement("update merchant set merch_edc_flag = 'N' where app_seq_num = ? ");
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          break;

        case mesConstants.POS_STAGE_ONLY:
          ps.setInt(1,601);
          ps1 = getPreparedStatement("delete from tranchrg where app_seq_num = ? and (cardtype_code = 20 or cardtype_code = 21) " );
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          ps1 = getPreparedStatement("update merchant set merch_edc_flag = 'N' where app_seq_num = ? ");
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          break;

        case mesConstants.POS_GLOBAL_PC:
          ps.setInt(1,701);
          ps1 = getPreparedStatement("delete from tranchrg where app_seq_num = ? and (cardtype_code = 20 or cardtype_code = 21) " );
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          ps1 = getPreparedStatement("update merchant set merch_edc_flag = 'N' where app_seq_num = ? ");
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          break;

        case mesConstants.POS_CHARGE_EXPRESS:
          ps.setInt(1,1101);
          ps1 = getPreparedStatement("delete from tranchrg where app_seq_num = ? and (cardtype_code = 20 or cardtype_code = 21) " );
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          ps1 = getPreparedStatement("update merchant set merch_edc_flag = 'N' where app_seq_num = ? ");
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          break;

        case mesConstants.POS_CHARGE_PRO:
          ps.setInt(1,1201);
          ps1 = getPreparedStatement("delete from tranchrg where app_seq_num = ? and (cardtype_code = 20 or cardtype_code = 21) " );
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          ps1 = getPreparedStatement("update merchant set merch_edc_flag = 'N' where app_seq_num = ? ");
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          break;

        case mesConstants.POS_GPS:
          ps.setInt(1,801);
          ps1 = getPreparedStatement("delete from tranchrg where app_seq_num = ? and (cardtype_code = 20 or cardtype_code = 21) " );
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          ps1 = getPreparedStatement("update merchant set merch_edc_flag = 'N' where app_seq_num = ? ");
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          break;

        case mesConstants.POS_VIRTUAL_TERMINAL:
          ps.setInt(1, 1401);
          param = vtEmail;
          ps1 = getPreparedStatement("delete from tranchrg where app_seq_num = ? and (cardtype_code = 20 or cardtype_code = 21) " );
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          ps1 = getPreparedStatement("update merchant set merch_edc_flag = 'N' where app_seq_num = ? ");
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          break;

        case mesConstants.POS_PAYMENT_GATEWAY:
          ps.setInt(1, parseInt(pgCard));
          param = pgCertName;
          email = pgEmail;
          ps1 = getPreparedStatement("delete from tranchrg where app_seq_num = ? and (cardtype_code = 20 or cardtype_code = 21) " );
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          ps1 = getPreparedStatement("update merchant set merch_edc_flag = 'N' where app_seq_num = ? ");
          ps1.setLong  (1,appSeqNum);
          ps1.executeUpdate();
          ps1.close();
          break;

      }

      //generic param
      ps.setString(2,param);

      //email added for payment gateway and internet
      if(addEmail)
      {
        ps.setString (3,email);
        ps.setLong   (4,appSeqNum);
      }
      else
      {
        ps.setLong  (3,appSeqNum);
      }


      if (ps.executeUpdate() != 1)
      {
        addError("submitMerchPaySolData: Unable to insert/update record");
        logEntry("submitMerchPaySolData()", "insert/update failed");
      }
    }
    catch (Exception e)
    {
      addError("submitMerchPosData: " + e.toString());
      logEntry("submitMerchPosData()", e.toString());
    }
  }


  private void deleteAllAddress(long appSeqNum)
  {
    PreparedStatement ps    = null;

    try
    {
      ps = getPreparedStatement("delete address where app_seq_num = ? and addresstype_code != ?");
      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.ADDR_TYPE_SHIPPING);
      ps.executeUpdate();
    }
    catch(Exception e)
    {
      addError("deleteAllAddress: " + e.toString());
      logEntry("deleteAllAddress()", e.toString());
    }
  }


  /*
  ** METHOD submitMerchEquipmentData
  */
  protected void submitMerchEquipmentData(long appSeqNum)
  {
    StringBuffer      qs    = new StringBuffer("");
    PreparedStatement ps    = null;
    ResultSet         rs    = null;

    try
    {

      if (productType == mesConstants.POS_PC || productType == mesConstants.POS_CHARGE_EXPRESS || productType == mesConstants.POS_CHARGE_PRO)
      {
        ps = getPreparedStatement("delete merchequipment where app_seq_num = ? and equip_model != ?");
        ps.setLong(1,appSeqNum);
        ps.setString(2,"PCPS");
        ps.executeUpdate();

        qs.append("select app_seq_num     ");
        qs.append("from merchequipment    ");
        qs.append("where app_seq_num  = ? ");
        qs.append("and equip_model    = ? ");

        ps = getPreparedStatement(qs.toString());
        ps.setLong(1,appSeqNum);
        ps.setString(2,"PCPS");

        qs.setLength(0);
        rs = ps.executeQuery();
        if (rs.next())
        {
          qs.append("update merchequipment set        ");
          qs.append("merchequip_equip_quantity  = ?,  ");
          qs.append("equiplendtype_code         = ?,  ");
          qs.append("equiptype_code             = ?   ");
          qs.append("where equip_model          = ?   ");
          qs.append("and app_seq_num            = ?   ");
        }
        else
        {
          qs.append("insert into merchequipment (     ");
          qs.append("merchequip_equip_quantity,       ");
          qs.append("equiplendtype_code,              ");
          qs.append("equiptype_code,                  ");
          qs.append("equip_model,                     ");
          qs.append("app_seq_num)                     ");
          qs.append("values (?,?,?,?,?)               ");
        }
        rs.close();

        ps = getPreparedStatement(qs.toString());
        ps.setString(1,"1");
        ps.setInt   (2,1);
        ps.setInt   (3,mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE);
        ps.setString(4,"PCPS");
        ps.setLong  (5,appSeqNum);
        if (ps.executeUpdate() != 1)
        {
          addError("submitMerchPosData: Unable to insert/update pcLicenses record");
          logEntry("submitMerchPosData()", "insert/update failed");
        }
        ps.close();
      }
      else if(productType == mesConstants.POS_DIAL_TERMINAL)
      {
        ps = getPreparedStatement("delete merchequipment where app_seq_num = ? and equip_model = ?");
        ps.setLong(1,appSeqNum);
        ps.setString(2,"PCPS");
        ps.executeUpdate();
        ps.close();
      }
      else if(productType == mesConstants.POS_WIRELESS_TERMINAL)
      {
        ps = getPreparedStatement("delete merchequipment where app_seq_num = ? and equip_model = ?");
        ps.setLong(1,appSeqNum);
        ps.setString(2,"PCPS");
        ps.executeUpdate();
        ps.close();
      }
      else if(productType == mesConstants.POS_DIAL_AUTH)
      {
        ps = getPreparedStatement("delete merchequipment where app_seq_num = ? and equiptype_code != ?");
        ps.setLong(1,appSeqNum);
        ps.setInt(2,4);
        ps.executeUpdate();
        ps.close();
      }
      else if(productType != mesConstants.POS_DIAL_TERMINAL)
      {
        ps = getPreparedStatement("delete merchequipment where app_seq_num = ?");
        ps.setLong(1,appSeqNum);
        ps.executeUpdate();
        ps.close();
      }
      else if(productType != mesConstants.POS_STAGE_ONLY)
      {
        ps = getPreparedStatement("delete merchequipment where app_seq_num = ?");
        ps.setLong(1,appSeqNum);
        ps.executeUpdate();
        ps.close();
      }
      else if(productType != mesConstants.POS_GLOBAL_PC)
      {
        ps = getPreparedStatement("delete merchequipment where app_seq_num = ?");
        ps.setLong(1,appSeqNum);
        ps.executeUpdate();
        ps.close();
      }

      if(imprinterPlates > 0)
      {
        ps = getPreparedStatement("delete from merchequipment where app_seq_num = ? and equiptype_code = ?");
        ps.setLong(1, appSeqNum);
        ps.setInt(2, EQUIP_TYPE_IMPRINTER_PLATE);
        ps.executeUpdate();
        ps.close();

        qs.setLength(0);
        qs.append("insert into merchequipment     ");
        qs.append(" (                             ");
        qs.append("   app_seq_num,                ");
        qs.append("   equiptype_code,             ");
        qs.append("   equiplendtype_code,         ");
        qs.append("   merchequip_amount,          ");
        qs.append("   equip_model,                ");
        qs.append("   merchequip_equip_quantity,  ");
        qs.append("   prod_option_id              ");
        qs.append(" )                             ");
        qs.append("values (?,?,?,?,?,?,?)         ");

        ps = getPreparedStatement(qs.toString());

        ps.setLong(1,   appSeqNum);
        ps.setInt(2,    this.EQUIP_TYPE_IMPRINTER_PLATE);
        ps.setInt(3,    mesConstants.APP_EQUIP_PURCHASE);
        ps.setNull(4,   java.sql.Types.FLOAT);
        ps.setString(5, "IPPL");
        ps.setInt(6,    this.imprinterPlates);
        ps.setNull(7,   java.sql.Types.INTEGER);

        ps.executeUpdate();
        ps.close();
      }
    }
    catch (Exception e)
    {
      addError("submitMerchEquipmentData: " + e.toString());
      logEntry("submitMerchEquipmentData()", e.toString());
    }
  }



  private void submitMerchantTranscomData(long appSeqNum)
  {
    StringBuffer      qs    = new StringBuffer("");
    PreparedStatement ps    = null;
    ResultSet         rs    = null;

    try
    {
      qs.append("select app_seq_num     ");
      qs.append("from transcom_merchant ");
      qs.append("where app_seq_num = ?  ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      qs.setLength(0);
      if (rs.next())
      {
        qs.append("update transcom_merchant set                 ");
        qs.append("annual_transaction_count       = ?,          ");
        qs.append("seasonal_merchant_flag         = ?,          ");
        qs.append("high_volume_months             = ?,          ");
        qs.append("ebt_option                     = ?,          ");
        qs.append("perc_mail                      = ?,          ");
        qs.append("perc_telephone                 = ?,          ");
        qs.append("perc_internet                  = ?,          ");
        qs.append("mcc_sic_code                   = ?,          ");
        qs.append("internet_sales_option          = ?,          ");
        qs.append("cardholder_present             = ?,          ");
        qs.append("cardholder_not_present         = ?           ");
        qs.append("where app_seq_num              = ?           ");
      }
      else
      {
        qs.append("insert into transcom_merchant (              ");
        qs.append("annual_transaction_count,                    ");
        qs.append("seasonal_merchant_flag,                      ");
        qs.append("high_volume_months,                          ");
        qs.append("ebt_option,                                  ");
        qs.append("perc_mail,                                   ");
        qs.append("perc_telephone,                              ");
        qs.append("perc_internet,                               ");
        qs.append("mcc_sic_code,                                ");
        qs.append("internet_sales_option,                       ");
        qs.append("cardholder_present,                          ");
        qs.append("cardholder_not_present,                      ");
        qs.append("app_seq_num)                                 ");
        qs.append("values (?,?,?,?,?,?,?,?,?,?,?,?)             ");
      }

      rs.close();
      ps.close();

      ps = getPreparedStatement(qs.toString());

      ps.setString(1,  annualCount);
      ps.setString(2,  (seasonalMerchant ? "Y" : "N"));
      ps.setString(3,  highVolumeMonths);
      ps.setInt(4,     transcomEbtOption);
      ps.setString(5,  motoPercentageMail);
      ps.setString(6,  motoPercentageTelephone);
      ps.setString(7,  motoPercentageInternet);
      ps.setString(8,  mccSicCode);
      ps.setString(9,  internetSalesOption);
      ps.setString(10, cardholderPresent);
      ps.setString(11, cardholderNotPresent);
      ps.setLong  (12, appSeqNum);

      if (ps.executeUpdate() != 1)
      {
        logEntry("submitMerchantTranscomData()", "insert/update failed");
        addError("submitMerchantTranscomData: Unable to insert/update record");
      }

      ps.close();
    }
    catch (Exception e)
    {
      logEntry("submitMerchantTranscomData()", e.toString());
      addError("submitMerchantTranscomData: " + e.toString());
    }
  }

  /*
  ** METHOD submitData
  */
  public void submitData(HttpServletRequest req, long appSeqNum)
  {
    if(!hasErrors())
    {
      submitApplicationData(req, appSeqNum);
      submitMerchantData1(req, appSeqNum);
      submitMerchantData2(req, appSeqNum);
      if(appType == mesConstants.APP_TYPE_TRANSCOM)
      {
        submitMerchantTranscomData(appSeqNum);
      }
    }

    if(!hasErrors())
    {
      deleteAllAddress(appSeqNum);
      submitAddressData(appSeqNum,mesConstants.ADDR_TYPE_BUSINESS);
    }

    if(!hasErrors() && !isBlank(mailingName))
    {
      submitAddressData(appSeqNum,mesConstants.ADDR_TYPE_MAILING);
    }

    if(!hasErrors() && !isBlank(owner1Address1))
    {
      submitAddressData(appSeqNum,mesConstants.ADDR_TYPE_OWNER1);
    }

    if (!isBlank(owner2Address1))
    {
      submitAddressData(appSeqNum,mesConstants.ADDR_TYPE_OWNER2);
    }

    if(!hasErrors())
    {
      submitAddressData(appSeqNum,mesConstants.ADDR_TYPE_CHK_ACCT_BANK);
    }

    if(!hasErrors())
    {
      submitBusinessOwnerData(appSeqNum,1);
      submitBusinessOwnerData(appSeqNum,2);
    }

    if(!hasErrors())
    {
      submitMerchContactData(appSeqNum);
    }

    if(!hasErrors())
    {
      submitMerchBankData(appSeqNum);
    }

    if(!hasErrors())
    {
      submitMerchPayOptionData(appSeqNum);
    }

    if(!hasErrors())
    {
      submitMerchPosData(appSeqNum);
    }

    if(!hasErrors())
    {
      submitMerchEquipmentData(appSeqNum);
    }

    //this will put potential errors (if present) in error database..
    if(!hasErrors())
    {
      submitQuestionableDataList(appSeqNum);
    }

  }

  /*
  ** METHOD validate
  */
  public boolean validate()
  {
    int num;

    if(appType == mesConstants.APP_TYPE_SVB && isBlank(svbCifNum))
    {
      addError("Please provide a valid numeric CIF number in the CIF # field");
    }

    if (appType == mesConstants.APP_TYPE_SVB && isBlank(svbCdNum))
    {
      addError("Please provide a valid CD amount in the CD $ field");
    }

    if(appType == mesConstants.APP_TYPE_SVB && svbCdCollateral <= 0.0)
    {
      addError("Please provide a valid CD Collateral Percentage");
    }

    // svb association check JRF
    if(appType == mesConstants.APP_TYPE_SVB && isBlank(svbAssoc))
    {
      addError("Please select an RC Number/association from the drop down list");
    }

    if (appType == mesConstants.APP_TYPE_GOLD && isBlank(goldAccountType))
    {
      addError("Please select a Type of Application");
    }

    if (appType == mesConstants.APP_TYPE_CBT)
    {
      if(isBlank(cbtCreditScore)      ||
         cbtCreditScore.length() != 3 ||
         cbtCreditScore.equals("000") ||
         cbtCreditScore.equals("999") ||
         cbtCreditScore.equals("123") ||
         !isNumber(cbtCreditScore))
      {
        addError("Please provide a valid credit score");
      }
    }

    if(appType == mesConstants.APP_TYPE_NBSC && (isBlank(nbscBankNum) || !isNumber(nbscBankNum) || nbscBankNum.length() != 4))
    {
      addError("Please provide a valid numeric Bank Number in the Bank # field");
    }

    if (appType == mesConstants.APP_TYPE_NBSC && (isBlank(nbscAssNum) || !isNumber(nbscAssNum) || nbscAssNum.length() != 6))
    {
      addError("Please provide a valid numeric Association Number in the Association # field");
    }

    if (appType == mesConstants.APP_TYPE_GOLD && !isBlank(goldAssNum) && (!isNumber(goldAssNum) || goldAssNum.length() != 6))
    {
      addError("Please provide a valid numeric Association Number in the Association # field. It must contain 6 digits.");
    }

    if((appType == mesConstants.APP_TYPE_DISCOVER || appType == mesConstants.APP_TYPE_DISCOVER_IMS || appType == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL)
         && isBlank(discoverRepName))
    {
      addError("Please provide a Discover Rep Name");
    }

    if((appType == mesConstants.APP_TYPE_DISCOVER || appType == mesConstants.APP_TYPE_DISCOVER_IMS || appType == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL)
        && isBlank(discoverRepId))
    {
      addError("Please provide a Discover Rep Id");
    }
    else if((appType == mesConstants.APP_TYPE_DISCOVER || appType == mesConstants.APP_TYPE_DISCOVER_IMS || appType == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL)
        && discoverRepId.length() != 5)
    {
      addError("Discover Rep Id must be 5 characters in length");
    }
    else if((appType == mesConstants.APP_TYPE_DISCOVER || appType == mesConstants.APP_TYPE_DISCOVER_IMS || appType == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL) && !isValidDiscoverRepId(discoverRepId))
    {
      addError(discoverRepId + " is not a valid Discover Rep Id");
    }

    if((appType == mesConstants.APP_TYPE_DISCOVER || appType == mesConstants.APP_TYPE_DISCOVER_IMS)
        && !isBlank(franchiseCode) && franchiseCode.length() != 4)
    {
      addError("Franchise Code must be 4 characters in length");
    }

    if(!isBlank(this.totalNumMultiMerchant) && !isNumber(this.totalNumMultiMerchant))
    {
      addError("Total # of apps for this multi-merchant setup must be numeric");
    }

    if(!isBlank(this.numMultiMerchant) && !isNumber(this.numMultiMerchant))
    {
      addError("Of Total # of apps for this setup, this app # must be numeric");
    }

    if((appType == mesConstants.APP_TYPE_DISCOVER || appType == mesConstants.APP_TYPE_DISCOVER_IMS || appType == mesConstants.APP_TYPE_MES) && !isBlank(this.totalNumMultiMerchant))
    {
      if(isBlank(this.multiMerchantMasterDba) || isBlank(this.multiMerchantMasterControlNum) || isBlank(this.numMultiMerchant))
      {
        addError("If this is part of a multi-merchant setup, you must provide the Master DBA, Master Control #, and the app number for this application");
      }
      else if(!isNumber(this.multiMerchantMasterControlNum))
      {
        addError("Master Merchant Control Number must be a valid number");
      }
    }

    if(appType == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL && (isBlank(bankReferralNum) || bankReferralNum.length() != 6))
    {
      addError("Please provide a valid 6 digit Bank Referral Number");
    }

    if((appType == mesConstants.APP_TYPE_DISCOVER || appType == mesConstants.APP_TYPE_DISCOVER_IMS) && isBankReferralApp())
    {

      if(isBlank(bankReferralNum) || bankReferralNum.equals("***"))
      {
        addError("Please select a bank name for this bank referral application");
      }

    }
    else if((appType == mesConstants.APP_TYPE_DISCOVER || appType == mesConstants.APP_TYPE_DISCOVER_IMS) && !isBankReferralApp())
    {

      if(!isBlank(bankReferralNum))
      {
        addError("If selecting a bank referral name, please specify that this is a Bank Referral Application, else de-select the bank name");
      }

    }


    if (isBlank(businessName))
    {
      addError("Please provide a business name (DBA)");
    }

    //if bbt and business type sole proprietorship business legal name not required
    //so we just use the business name if a business legal name was not provided
    if(appType == mesConstants.APP_TYPE_BBT && businessType == 1 && isBlank(businessLegalName))
    {
      businessLegalName = businessName;
    }
    else if(isBlank(businessLegalName))
    {
      addError("Please provide a legal business name");
    }

    if(businessType != 1 && businessType != 3)
    {

      if (!isValidTaxId(taxpayerId))
      {
        addError("Please provide a valid taxpayer ID/SSN");
      }
      else if(countDigits(taxpayerId) != MAX_SSN)
      {
        addError("Please provide a valid taxpayer ID/SSN");
      }
      else if(!isBlank(taxpayerId))
      {
        taxIdValidation(taxpayerId);
      }

/*
      else if(!isBlank(taxpayerId))
      {
        //if sole proprietor or partnership.. do the ssn validation.. cause they dont have a valid fed tax id..
        if(businessType == 1 || businessType == 3)
        {
          ssnValidation(taxpayerId);
        }
        else
        {
          taxIdValidation(taxpayerId);
        }
      }
*/
    }

    if (!isValidPhone(businessPhone))
    {
      addError("Please provide a valid business phone number (with area code)");
    }

    if( !isBlank(customerServicePhone) && ! isValidPhone(customerServicePhone))
    {
      addError("Please provide a valid customer service phone number (with area code)");
    }

    if (isBlank(businessAddress1))
    {
      addError("Please provide a business address");
    }

    if (isBlank(businessCity))
    {
      addError("Please provide a business city");
    }

    if (businessState == 0)
    {
      addError("Please select a business state");
    }

    num = countDigits(businessZip);
    if (isBlank(businessZip) || (num != 5 && num != 9))
    {
      addError("Please provide a valid 5 or 9 digit business zip code");
    }
    else
    {
      if(!validZipForState(getBusinessState2(), businessZip))
      {
        addError("Business Zip not valid for Business State");
      }
    }

    if ( ( (appType == mesConstants.APP_TYPE_VERISIGN || appType == mesConstants.APP_TYPE_DEMO) || !isBlank(businessEmail) || this.validEmail ) && !isEmail(businessEmail) )
    {
      addError("Please provide a valid e-mail address");
    }

    if(isBlank(contactNameFirst))
    {
      addError("Please provide a Contact Name");
    }

    if(isBlank(contactNameLast))
    {
      addError("Please provide a Contact Name");
    }

    if (!isBlank(contactPhone) && !isValidPhone(contactPhone))
    {
      addError("Please provide a valid Contact Phone Number (with area code)");
    }
    if (!isBlank(contactEmail) && !isEmail(contactEmail))
    {
      addError("Please provide a valid Contact Email Address");
    }

    if(!isBlank(mailingName))
    {
      if(isBlank(mailingAddress1))
      {
        addError("A Mailing Address Line 1 is needed if a mailing address name is specified");
      }
      if(isBlank(mailingCity))
      {
        addError("A Mailing Address City is needed if a mailing address name is specified");
      }
      if(mailingState == 0)
      {
        addError("A Mailing Address State is needed if a mailing address name is specified");
      }

      num = countDigits(mailingZip);
      if(isBlank(mailingZip))
      {
        addError("A Mailing Address Zip is needed if a mailing address name is specified");
      }
      else if (num != 5 && num != 9)
      {
        addError("Please provide a valid 5 or 9 digit Mailing Zip Code");
      }
      else
      {
        if(!validZipForState(getMailingState2(), mailingZip))
        {
          addError("Mailing Zip not valid for Mailing State");
        }
      }
    }
    else
    {
      if(!isBlank(mailingAddress1))
      {
        addError("A Mailing Address Name is needed if a mailing address is specified");
      }
    }

    if (appType == mesConstants.APP_TYPE_TRANSCOM && isBlank(businessFax))
    {
      addError("Please provide a valid Business Fax (with area code)");
    }

    num = countDigits(businessFax);
    if (!isBlank(businessFax) && !businessFax.equals("0") && countDigits(businessFax) != 10)
    {
      addError("Please provide a valid Business Fax (with area code)");
    }

    if (businessType == 0)
    {
      addError("Please select a Business Type");
    }

    if (industryType == 0)
    {
      addError("Please select an Industry Type");
    }

    if(!isBlank(mccSicCode) && (mccSicCode.length() != 4 || !isNumber(mccSicCode)))
    {
      addError("Please provide a valid 4 digit MCC/SIC Code");
    }

    if(!isBlank(sicCode) && (sicCode.length() != 4 || !isNumber(sicCode)))
    {
      addError("Please provide a valid 4 digit SIC Code");
    }

    if (appType != mesConstants.APP_TYPE_BBT && locationType == 0)
    {
      addError("Please select a Location Type");
    }

    if(!isBlank(cbtAssNum) && !isNumber(cbtAssNum))
    {
      addError("Association Number must be a valid 6 digit number");
    }

    if(!isBlank(cbtRepCode) && !isNumber(cbtRepCode))
    {
      addError("Rep Code must be a valid 4 digit number");
    }


    if((appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_MESA) &&  industryType == 9)
    {
      //no validation
      if(!isBlank(product) && product.length() > 200)
      {
        addError("Business Description (currently " + product.length() + " characters) must be less than 200 characters." );
      }

      if(!isBlank(establishedYear))
      {
        num = parseInt(establishedYear);
        if(num < 1000 || num > 9999)
        {
          addError("Please provide a valid 4-digit year the business was established");
        }
      }

    }
    else //we validate
    {

      if (establishedMonth == 0)
      {
        addError("Please select the month the business was established");
      }

      num = parseInt(establishedYear);
      if(num < 1000 || num > 9999)
      {
        addError("Please provide a valid 4-digit year the business was established");
      }


      if (isBlank(product))
      {
        addError("Please provide a Business Description");
      }
      else if(product.length() > 200)
      {
        addError("Business Description (currently " + product.length() + " characters) must be less than 200 characters." );
      }

      if (applicationType == 0)
      {
        addError("Please select Type of Account Setup");
      }

      if (isBlank(numLocations))
      {
        addError("Please provide the Number of Locations");
      }


      if (appType != mesConstants.APP_TYPE_TRANSCOM && appType != mesConstants.APP_TYPE_BBT && isBlank(locationYears))
      {
        addError("Please provide the Number of Years at Location");
      }
    }

//*****************************  SECTION 2  ********************************
    if((appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_MESA) &&  industryType == 9)
    {
      //no validation
    }
    else //we validate
    {
      if (haveProcessed == 0)
      {
        addError("Please select whether business has accepted credit cards before");
      }
      else if(appType == mesConstants.APP_TYPE_BBT && haveProcessed == 2)
      {
        beingWon            = 2;
        statementsProvided  = 2;
        haveCanceled        = 2;
      }

      if (haveProcessed == 1 && isBlank(previousProcessor))
      {
        addError("Please provide Previous Processor");
      }

      if(appType == mesConstants.APP_TYPE_BBT)
      {
        if (beingWon == 0)
        {
          addError("Please select whether merchant is being won over from another organization");
        }

        if (beingWon == 1 && isBlank(beingWonReason))
        {
          addError("Since merchant is being won, provide name of organization, reasons for change and chargeback history");
        }
      }

      if(appType != mesConstants.APP_TYPE_BBT)
      {
        if(statementsProvided == 0)
        {
          addError("Please specify whether previous processor statements will be provided");
        }
      }

      if (haveCanceled == 0)
      {
        addError("Please select whether business has had account canceled before");
      }

      if (haveCanceled == 1)
      {
        if (isBlank(canceledProcessor))
        {
          addError("Please provide Cancelling Processor");
        }

        if (isBlank(cancelReason))
        {
          addError("Please provide reason account was cancelled");
        }

        if (cancelMonth == 0)
        {
          addError("Please select the month account was cancelled");
        }

        num = parseInt(cancelYear);
        if(num < 1000 || num > 9999)
        {
          addError("Please provide a valid 4-digit year account was cancelled");
        }
      }
    }
//*****************************************  end section 2  ******************************

    if(appType != mesConstants.APP_TYPE_BBT)
    {
      if (isBlank(bankName))
      {
        addError("Please provide a business checking account bank name");
      }

      if((appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_MESA) && industryType == 9)
      {
        //skip this validation for cbt industry type cash advance..
      }
      else
      {
        num = parseInt(yearsOpen);
        if(appType != mesConstants.APP_TYPE_TRANSCOM && num == 0 && !(yearsOpen.equals("0")))
        {
          addError("Please provide number of years business checking account open");
        }
      }

      if (isBlank(bankAddress) && appType != mesConstants.APP_TYPE_DISCOVER && appType != mesConstants.APP_TYPE_DISCOVER_IMS && appType != mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL)
      {
        addError("Please provide the business checking account bank address");
      }

      if (isBlank(bankCity))
      {
        addError("Please provide the business checking account bank city");
      }

      num = countDigits(bankZip);
      if (isBlank(bankZip) || (num != 5 && num != 9))
      {
        addError("Please provide a valid 5 or 9 digit business checking account zip code");
      }

      if (bankState == 0)
      {
        addError("Please select a business checking account state");
      }
    }
    else if(appType == mesConstants.APP_TYPE_BBT)
    {
      if(isBlank(bbtCenterNum) || !isValidCenterNumber(bbtCenterNum))
      {
        addError("Please provide a valid 7 digit center#");
      }
      if(!isBlank(bbtReferringOfficer) && !isValidOfficerNumber(bbtReferringOfficer))
      {
        addError("Please provide a valid referring officer #");
      }
    }

    if (isBlank(checkingAccount))
    {
      addError("Please provide a business checking account number");
    }
    else
    {
      try
      {
        long tempLng = Long.parseLong(checkingAccount);
      }
      catch(Exception e)
      {
        addError("Business checking account number must be all numeric");
      }
    }

    if(appType == mesConstants.APP_TYPE_BBT )
    {
      if(isBlank(transitRouting))
      {
        transitRouting = transitRoutingBbtOther;
      }
    }

    if (isBlank(transitRouting))
    {
      addError("Please provide a business checking account transit routing number");
    }
    else
    {
      try
      {
        long testL = Long.parseLong(transitRouting);
        if(countDigits(transitRouting) != 9)
        {
          addError("Business checking account transit routing number must consist of a nine digit number");
        }
        //  Transit routing numbers DO NOT comply with Mod10
        //  there was actually a bug that was preventing t/r's that began with zero from passing..
        //  i fixed this.. all valid t/r's should mod 10 check now.. according to david
        else if(!isValidMod10Check(transitRouting))
        {
          addError("Transit Routing Number did not pass the check digit routine");
        }

        else if(!validTransitRoute(transitRouting))
        {
//          if(!okToPass(mesConstants.ERROR_ABA_NOT_FOUND))
//          {
            addError("Transit Routing number was not found in our list of valid ABA Numbers.  Please validate it is correct and resend");
//            letItPass(mesConstants.ERROR_ABA_NOT_FOUND, "Questionable Data");
//          }
        }
        else //no errors so remove from map.. so it doesnt get listed as possible error
        {
          removeFromMap(mesConstants.ERROR_ABA_NOT_FOUND);
        }

        if(!isBlank(transitRouting) && (transitRouting.equals("031101114") || transitRouting.equals("061091977")))
        {
          addError("Transit Routing Numbers 031101114 and 061091977 are not allowed");
        }

      }
      catch(Exception e)
      {
        addError("Please provide a business checking account transit routing number");
      }
    }


    if(isAccountConfirmationReq())
    {

      if(isBlank(getConfirmCheckingAccount()))
      {
        addError("Please re-enter the account number of your business checking account to validate it was entered correctly");
      }
      else if(!isBlank(getCheckingAccount()) && !getCheckingAccount().equals(getConfirmCheckingAccount()))
      {
        addError("Checking Account # and Confirmation of Checking Account # do not match");
      }

      if(isBlank(getConfirmTransitRouting()))
      {
        addError("Please re-enter the transit routing number of your business checking account to validate it was entered correctly");
      }
      else if(!isBlank(getTransitRouting()) && !getTransitRouting().equals(getConfirmTransitRouting()))
      {
        addError("Transit Routing # and Confirmation of Transit Routing # do not match");
      }

    }

    if(isAccountSourceTypeReq())
    {

      if(isBlank(getTypeOfAcct()))
      {
        addError("Please select the type of account corresponding to the account number entered above");
      }
      else if(!isBlank(getTypeOfAcct()) && !getTypeOfAcct().equals(Integer.toString(mesConstants.BANK_ACCT_TYPE_CHECKING_ACCOUNT)) && !isSkipTypeError())
      {
        //this allows this error to only pop up once
        setSkipTypeError("true");
        addError("Note: Limitations may exist for this type of bank account.");
      }

      if(isBlank(getSourceOfInfo()))
      {
        addError("Please select where you obtained your account number and transit routing number information");
      }

    }

    if(appType == mesConstants.APP_TYPE_TRANSCOM && (isBlank(bankPhone) || !isValidPhone(bankPhone)))
    {
      addError("Please provide a valid bank phone number");
    }
    else if(!isBlank(bankPhone) && !isValidPhone(bankPhone))
    {
      addError("Please provide a valid bank phone number");
    }


//*****************************   SECTION 4  ************************************

    if((appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_MESA) &&  industryType == 9)
    {

      num = countDigits(owner1Zip);
      if (!isBlank(owner1Zip) && (num != 5 && num != 9))
      {
        addError("Please provide a valid 5 or 9 digit primary owner zip code");
      }

      num = countDigits(owner2Zip);
      if (!isBlank(owner2Zip) && num != 5 && num != 9)
      {
        addError("Please provide a valid 5 or 9 digit secondary owner zip code");
      }

      if (!isBlank(owner1SSN) && !isValidTaxId(owner1SSN))
      {
        addError("Please provide a valid 9 digit primary owner SSN");
      }
      else if(!isBlank(owner1SSN))
      {
        ssnValidation(owner1SSN);
      }

      num = parseInt(owner1Percent);
      if (!isBlank(owner1Percent) && (num < 0 || num > 100))
      {
        addError("Primary owner's percentage of ownership is invalid");
      }

      num = parseInt(owner2Percent);
      if (!isBlank(owner2Percent) && (num < 0 || num > 100))
      {
        addError("Secondary owner's percentage of ownership is invalid");
      }

      num = parseInt(owner1SinceYear);
      if (num > 0 && (num < 1000 || num > 9999))
      {
        addError("Please provide a valid 4-digit start of ownership year");
      }

      if(!isBlank(owner1Phone) && !owner1Phone.equals("0") && !isValidPhone(owner1Phone))
      {
        addError("Please provide an owner phone number (owner 1)");
      }
    }
    else //we validate
    {

      if (isBlank(owner1FirstName))
      {
        addError("Please provide the owner's first name");
      }

      if (isBlank(owner1LastName))
      {
        addError("Please provide the owner's last name");
      }

      if(appType != mesConstants.APP_TYPE_BBT)
      {
        if (isBlank(owner1Address1))
        {
          addError("Please provide the owner's address");
        }

        if (isBlank(owner1City))
        {
          addError("Please provide the owner's city");
        }

        if (owner1State == 0)
        {
          addError("Please select the owner's state");
        }

        num = countDigits(owner1Zip);
        if (isBlank(owner1Zip) || (num != 5 && num != 9))
        {
          addError("Please provide a valid 5 or 9 digit primary owner zip code");
        }
        else
        {
          if(!validZipForState(getOwner1State2(), owner1Zip))
          {
            addError("Primary principal Zip not valid for Primary Principal State");
          }
        }
      }

      num = countDigits(owner2Zip);
      if (!isBlank(owner2Zip) && num != 5 && num != 9)
      {
        addError("Please provide a valid 5 or 9 digit secondary owner zip code");
      }

      if (!isValidTaxId(owner1SSN))
      {
        addError("Please provide a valid 9 digit primary owner SSN");
      }
      else if(!isBlank(owner1SSN))
      {
        ssnValidation(owner1SSN);
      }

      num = parseInt(owner1Percent);
      if (!isBlank(owner1Percent) && (num < 0 || num > 100))
      {
        addError("Primary owner's percentage of ownership is invalid");
      }

      num = parseInt(owner2Percent);
      if (!isBlank(owner2Percent) && (num < 0 || num > 100))
      {
        addError("Secondary owner's percentage of ownership is invalid");
      }

      num = parseInt(owner1SinceYear);
      if (num > 0 && (num < 1000 || num > 9999))
      {
        addError("Please provide a valid 4-digit start of ownership year");
      }

      if(!isBlank(owner1Phone) && !owner1Phone.equals("0") && !isValidPhone(owner1Phone))
      {
        addError("Please provide an owner phone number (owner 1)");
      }
   }
//*******************************  END SECTION 4 ***************************************

    if(!isBlank(owner2Phone) && !owner2Phone.equals("0") && !isValidPhone(owner2Phone))
    {
      addError("Please provide a valid owner 2 phone number (with area code)");
    }

    if (!isBlank(owner2SSN) && !isValidTaxId(owner2SSN))
    {
      addError("Please provide a valid 9 digit secondary owner SSN)");
    }
    else if(!isBlank(owner2SSN))
    {
      ssnValidation(owner2SSN);
    }


//***************************** SECTION 6 **************************************

    if((appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_MESA) &&  industryType == 9)
    {
      double dol = 0;

      if(!isBlank(monthlySales))
      {
        dol = parseDouble(monthlySales);
        num = countDigits(monthlySales);
        if (dol <= 0 || num > 10)
        {
          addError("Please provide a valid projected monthly sales amount");
        }
      }

      if(!isBlank(vmcSales))
      {
        dol = parseDouble(vmcSales);
        num = countDigits(vmcSales);
        if (dol <= 0 || num > 10)
        {
          addError("Please provide a valid projected Visa/MasterCard monthly sales amount");
        }
      }

      if(!isBlank(averageTicket))
      {
        dol = parseDouble(averageTicket);
        num = countDigits(averageTicket);
        if (dol <= 0 || num > 10)
        {
          addError("Please provide a valid entry in Average Credit Card Ticket Field");
        }
      }

      if(!isBlank(motoPercentage))
      {
        num = parseInt(motoPercentage);
        if (num < 0 || num > 100)
        {
          addError("Please provide a valid mail order/telephone order percentage");
        }
        else if(num >= 50)
        {
          discMotoRate = true;
        }
      }
    }
    else
    {
      double dol = parseDouble(monthlySales);
      num = countDigits(monthlySales);
      if (dol <= 0 || num > 10)
      {
        addError("Please provide a valid projected monthly sales amount");
      }

      dol = parseDouble(vmcSales);
      num = countDigits(vmcSales);
      if (dol <= 0 || num > 10)
      {
        addError("Please provide a valid projected Visa/MasterCard monthly sales amount");
      }

      dol = parseDouble(averageTicket);
      num = countDigits(averageTicket);
      if (dol <= 0 || num > 10)
      {
        addError("Please provide a valid entry in Average Credit Card Ticket Field");
      }

      if (appType == mesConstants.APP_TYPE_TRANSCOM && isBlank(annualCount))
      {
        addError("Please provide a valid Estimated Annual Transaction Count");
      }

      num = parseInt(motoPercentage);
      if (appType != mesConstants.APP_TYPE_TRANSCOM && (num < 0 || num > 100))
      {
        addError("Please provide a valid mail order/telephone order percentage");
      }
      else if(appType != mesConstants.APP_TYPE_TRANSCOM && num >= 50)
      {
        discMotoRate = true;
      }

/*
      num = parseInt(motoPercentageMail);
      if (appType == mesConstants.APP_TYPE_TRANSCOM && (num < 0 || num > 100))
      {
        addError("Please provide a valid mail order percentage");
      }
      num = parseInt(motoPercentageTelephone);
      if (appType == mesConstants.APP_TYPE_TRANSCOM && (num < 0 || num > 100))
      {
        addError("Please provide a valid telephone order percentage");
      }
*/
      num = parseInt(motoPercentageMailTele);
      if (appType == mesConstants.APP_TYPE_TRANSCOM && (num < 0 || num > 100))
      {
        addError("Please provide a valid MO/TO percentage");
      }

      num = parseInt(motoPercentageInternet);
      if (appType == mesConstants.APP_TYPE_TRANSCOM && (num < 0 || num > 100))
      {
        addError("Please provide a valid internet order percentage");
      }

      //else if(appType == mesConstants.APP_TYPE_TRANSCOM && num > 0)
      //{
        //if(isBlank(internetSalesOption))
        //{
          //addError("Please select an Internet Sales Option");
        //}
      //}

      num = parseInt(cardholderPresent);
      if (appType == mesConstants.APP_TYPE_TRANSCOM && (num < 0 || num > 100))
      {
        addError("Please provide a valid % Sales Cardholder Present");
      }

      num = parseInt(cardholderNotPresent);
      if (appType == mesConstants.APP_TYPE_TRANSCOM && (num < 0 || num > 100))
      {
        addError("Please provide a valid % Sales Cardholder Not Present");
      }

      num = parseInt(sumUpPercents());
      if(num >= 50)
      {
        discMotoRate = true;
      }

      if (refundPolicy == 0)
      {
        addError("Please select a refund policy");
      }
   }

//********************************* END SECTION 6 **********************************


    if(appType == mesConstants.APP_TYPE_TRANSCOM && isSeasonalMerchant() && highVolumeMonths.equals("NNNNNNNNNNNN"))
    {
      addError("Please select the High Volume Months for this seasonal Merchant");
    }
    else if(appType == mesConstants.APP_TYPE_TRANSCOM && !isSeasonalMerchant() && !highVolumeMonths.equals("NNNNNNNNNNNN"))
    {
      addError("If selecting high volume Months for merchant, please select seasonal merchant or delete high volume months");
    }

    if(ebtAccepted && isBlank(fcsNumber))
    {
      addError("Please provide an fcsNumber");
    }
    else if(!isBlank(fcsNumber) && !ebtAccepted)
    {
      addError("Please check the EBT checkbox or delete the FCS Number");
    }

    if (appType == mesConstants.APP_TYPE_VERISIGN || appType == mesConstants.APP_TYPE_DEMO)
    {
      if (amexAccepted && !amexNewAccount && isBlank(amexMerchId))
      {
        addError("Please provide a valid Amex SE# or indicate new account");
      }

      if (discAccepted && !discNewAccount && isBlank(discMerchId))
      {
        //automatically applies for new discover account for verisign apps..
        if(appType == mesConstants.APP_TYPE_VERISIGN)
        {
          setDiscNewAccount("y");
        }
        else
        {
          addError("Please provide a valid Discover Merchant # or indicate new account");
        }
      }

      if (dinrAccepted && (isBlank(dinrMerchId) || dinrMerchId.length() < 10))
      {
        addError("Please provide a valid Diners Merchant # (10 digits)");
      }

      if (jcbAccepted && isBlank(jcbMerchId))
      {
        addError("Please provide a valid JCB Merchant #");
      }
    }
    else
    {
      if (amexAccepted && isBlank(amexRate) && isBlank(amexMerchId) && !amexSplitDial)
      {
        addError("Please provide a valid Amex SE# or ESA rate or select split dial");
      }
      //else if(amexAccepted && isBlank(amexRate) && isBlank(amexMerchId) && appType == mesConstants.APP_TYPE_TRANSCOM && !amexSplitDial)
      //{
        //addError("Please provide a valid Amex SE# or ESA rate or check split dial");
      //}

      if(amexSplitDial && amexPip)
      {
        addError("Please select either Amex Split Dial or Amex Pip, but not both");
      }

      if(!amexAccepted && !isBlank(amexRate))
      {
        addError("Please check the Amex Payment Box or remove the ESA Rate provided");
      }
      else if(!amexAccepted && !isBlank(amexMerchId))
      {
        addError("Please check the Amex Payment Box or remove the Amex Merchant Number provided");
      }

      if(!isBlank(amexMerchId) && amexMerchId.length() < 10)
      {
        addError("Amex Merchant Number must be atleast 10 digits long");
      }

      if (discAccepted && isBlank(discRate) && isBlank(discFee) && isBlank(discMerchId))
      {
        addError("Please provide a valid Discover Merchant # or RAP rate");
      }
      else if((!isBlank(discRate) || !isBlank(discFee))&& !isBlank(discMerchId))
      {
        addError("Please provide either a valid Discover Merchant # OR RAP rate/fee");
      }

      if(!discAccepted && (!isBlank(discRate) || !isBlank(discFee)))
      {
        addError("Please check the Discover Payment Box or remove the Rap Rate/Fee provided");
      }
      else if(!discAccepted && !isBlank(discMerchId))
      {
        addError("Please check the Discover Payment Box or remove the Discover Merchant Number provided");
      }

      if (discAccepted && !isBlank(discRate))
      {
        validateRate(discRate);
      }

      if ((appType == mesConstants.APP_TYPE_DISCOVER || appType == mesConstants.APP_TYPE_DISCOVER_IMS || appType == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL)
          && (discAccepted && !isBlank(discRate) && isBlank(discFee)))
      {
        addError("Must provide a RAP Per Item Fee if providing a rap rate.  If no fee is to be charge enter 0.");
      }

      if(!isBlank(discFee) && isBlank(discRate))
      {
        addError("Must provide RAP Rate if providing RAP Per Item Fee");
      }

      if(!isBlank(discMerchId) && discMerchId.length() < 15)
      {
        addError("Discover Merchant Number must be atleast 15 digits long");
      }

      if (dinrAccepted && (isBlank(dinrMerchId) || dinrMerchId.length() < 10))
      {
        addError("Please provide a valid Diners Merchant # (10 digits)");
      }

    }

    if(appType == mesConstants.APP_TYPE_BBT)
    {
      if(!visaAccepted && !mcAccepted && !debitAccepted && !ebtAccepted && !dinrAccepted && !jcbAccepted && !amexAccepted && !discAccepted)
      {
        addError("Atleast one payment option must be selected. (i.e. Visa, Mastercard, Amex, etc...)");
      }
    }


    if (discRate.equals("-1"))
    {
      addError("Please provide a valid RAP discount rate for a new Discover account");
    }

    if (discFee.equals("-1"))
    {
      addError("Please provide a valid RAP Per Item Fee for a new Discover account");
    }

    if (amexRate.equals("-1"))
    {
      addError("Please provide a valid ESA discount rate for a new American Express account");
    }

    if(parseDouble(monthlySales) < parseDouble(vmcSales))
    {
      addError("Total Estimated Monthly Sales cannot be less than Total Estimated Monthly Visa/MC Sales");
    }

    if(appType == mesConstants.APP_TYPE_BBT)
    {
      if(isBlank(bbtProcessor))
      {
        addError("Please select a processor from the list");
      }
      else
      {
        if(bbtProcessor.equals(Integer.toString(mesConstants.APP_PROCESSOR_TYPE_VITAL)) && productType == mesConstants.POS_GLOBAL_PC)
        {
          addError("Can not select Global PC Windows if using Vital processor");
        }
        if(bbtProcessor.equals(Integer.toString(mesConstants.APP_PROCESSOR_TYPE_GLOBAL)) && productType == mesConstants.POS_PC)
        {
          addError("Can not select Vital POS Partner if using Global processor");
        }
      }
    }


    switch (productType)
    {
      case 0:
      default:
        addError("Please select a product type");
        break;

      case mesConstants.POS_DIAL_TERMINAL:
      case mesConstants.POS_WIRELESS_TERMINAL:
      case mesConstants.POS_DIAL_AUTH:
      case mesConstants.POS_PC:
      case mesConstants.POS_STAGE_ONLY:
      case mesConstants.POS_GLOBAL_PC:
      case mesConstants.POS_GPS:
      case mesConstants.POS_CHARGE_EXPRESS:
      case mesConstants.POS_CHARGE_PRO:
        break;

      case mesConstants.POS_INTERNET:
        if (internetType == 0)
        {
          addError("Please select an internet solution type");
        }

        if (isBlank(webUrl))
        {
          addError("Please provide a web store URL");
        }

        if(appType != mesConstants.APP_TYPE_VERISIGN && (isBlank(internetEmail) || !isEmail(internetEmail)))
        {
          addError("Please provide a valid Administrator Email address for Internet");
        }
        break;

      case mesConstants.POS_OTHER:
        if(isBlank(vitalProduct))
        {
          addError("Please provide the name of the Vital Certified Product");
        }
        break;

      case mesConstants.POS_THIRD_PARTY_SOFTWARE:
        if(isBlank(thirdPartySoftware))
        {
          addError("Please provide the name of the Third Party Software");
        }
        break;

      case mesConstants.POS_VIRTUAL_TERMINAL:
        if(isBlank(vtEmail) || !isEmail(vtEmail))
        {
          addError("Please provide a valid Administrator Email address for Virtual Terminal");
        }
        break;

      case mesConstants.POS_PAYMENT_GATEWAY:
        if(isBlank(pgEmail) || !isEmail(pgEmail))
        {
          addError("Please provide a valid Administrator Email address for MES Payment Gateway");
        }
        if(isBlank(pgCertName))
        {
          addError("Please provide a valid Certified Name for MES Payment Gateway");
        }
        if(isBlank(pgCard))
        {
          addError("Please select a valid Processing Option for MES Payment Gateway");
        }
        break;
    }

    if(this.comments.length() > 4000)
    {
      addError("Comments must be less than 4000 characters long. It is currently " + this.comments.length() + " characters long");
    }

    if(appType == mesConstants.APP_TYPE_VERISIGN && yesAgreeReq)
    {
      if(!agreement.equals("Y"))
      {
        addError("You must agree with the terms of the merchant agreement in order to submit the application");
      }
    }

    // validate the Certegy check and
    // Valutec gift card fields
    validateCheckValutec();

    // validate the extended fields
    validatePOSPartnerExtendedFields();

    return(!hasErrors());
  }

  protected void validateCheckValutec()
  {
    if (checkAccepted)
    {
      if ( isBlank(checkMerchId) )
      {
        addError("Must provide a check merchant number");
      }
      if ( checkProvider.equals("CPOther") && isBlank(checkProviderOther) )
      {
        addError("Must specify name of check provider");
      }
      if ( checkProvider.equals("CertegyWelcomeCheck") && checkMerchId.length() != 10 )
      {
        addError("Welcome check requires a 10 digit Station Number");
      }
      if ( checkProvider.equals("CertegyElecCheck") && checkMerchId.length() != 10 )
      {
        addError("Certegy Elec Check requires a 10 digit Station Number");
      }
      if ( checkProvider.equals("CertegyElecCheck") && checkTermId.length() != 10 )
      {
        addError("Certegy Elec Check requires a 10 character Terminal ID");
      }
      if ( productType != mesConstants.POS_DIAL_TERMINAL )
      {
        addError("Check processing not supported with current product selection");
      }
    }

    if (valutecAccepted)
    {
      if ( isBlank(valutecTermId) )
      {
        addError("Must provide a Valutec terminal ID");
      }
      if ( productType != mesConstants.POS_DIAL_TERMINAL )
      {
        addError("Valutec Gift Card not supported with current product selection");
      }
    }
  }

  protected void validatePOSPartnerExtendedFields()
  {
    if ( productType == mesConstants.POS_PC && supportsPosPartnerExtendedFields )
    {
      if ( isBlank(posPartnerConnectivity) )
      {
        addError("Must select a POS Partner connection type");
      }
      if ( isBlank(posPartnerOS) )
      {
        addError("Must select a POS Partner operating system");
      }
    }
  }

  private void taxIdValidation (String taxid)
  {
    taxid = getDigitsStr(taxid);

    if(taxid.length() == 9)
    {
      String taxIdPrefix = taxid.substring(0,2);

      if(taxIdPrefix.equals("07") ||
         taxIdPrefix.equals("08") ||
         taxIdPrefix.equals("09") ||
         taxIdPrefix.equals("17") ||
         taxIdPrefix.equals("18") ||
         taxIdPrefix.equals("19") ||
         taxIdPrefix.equals("28") ||
         taxIdPrefix.equals("29") ||
         taxIdPrefix.equals("49") ||
         taxIdPrefix.equals("78") ||
         taxIdPrefix.equals("00") ||
         taxIdPrefix.equals("79")
        )
      {
        addError("Federal Tax Id can not begin with 00, 07, 08, 09, 17, 18, 19, 28, 29, 49, 78, or 79");
      }
      else if(repeatingOne(taxid) || repeatingTwo(taxid) || repeatingThree(taxid))
      {
        addError("Federal Tax Id can not have repeating numbers in groups of one, two and three. (ie. 111-11-1111, 121-21-2121, 123-12-3123, etc...");
      }
    }
    else
    {
      addError("Please provide a valid 9-digit Federal Tax Id");
    }

  }


  private void ssnValidation(String ssn)
  {

     try
     {
       ssn = getDigitsStr(ssn);
       if(ssn.length() == 9)
       {
         String ssnAreaNumber = ssn.substring(0,3);

         int ssnAreaNumberInt = Integer.parseInt(ssnAreaNumber);

         if(ssnAreaNumberInt == 0                                 ||
            ssnAreaNumberInt == 666                               ||
            (ssnAreaNumberInt >= 730 && ssnAreaNumberInt <= 759)  ||
            (ssnAreaNumberInt >= 780 && ssnAreaNumberInt <= 799)
           )
         {
           addError("Social Security Number first 3 numbers can not begin with 000 or 666 and can not be between 730 and 759 or between 780 and 799 ");
         }
         else if(ssnAreaNumberInt >= 800)
         {
           addError("Social Security Number with first 3 numbers 800 and above have not been assigned yet ");
         }

         if(ssn.equals("123456789"))
         {
           addError("Social Security Number can not equal 123-45-6789");
         }
         else if(repeatingOne(ssn) || repeatingTwo(ssn) || repeatingThree(ssn))
         {
           addError("Social Security Number can not have repeating numbers in groups of one, two and three. (ie. 111-11-1111, 121-21-2121, 123-12-3123, etc...");
         }
         else if( groupsOfFiveOrMore(ssn) )
         {
           addError("Social Security Number can not have five or more of the same consecutive numbers anywhere. (ie. 111-11-2222, 121-11-1111, 133-33-3333, etc...");
         }
       }
       else
       {
         addError("Please provide a valid 9-digit Social Security Number");
       }

     }
     catch(Exception e)
     {
       addError("Please provide a valid 9-digit Social Security Number");
     }

  }

  private boolean groupsOfFiveOrMore(String data)
  {
    boolean result = false;

    try
    {
      String group51 = data.substring(0,5);
      String group52 = data.substring(1,6);
      String group53 = data.substring(2,7);
      String group54 = data.substring(3,8);
      String group55 = data.substring(4);

      String group61 = data.substring(0,6);
      String group62 = data.substring(1,7);
      String group63 = data.substring(2,8);
      String group64 = data.substring(3);

      String group71 = data.substring(0,7);
      String group72 = data.substring(1,8);
      String group73 = data.substring(2);

      String group81 = data.substring(0,8);
      String group82 = data.substring(1);

      if(repeatingOne(group51) ||
         repeatingOne(group52) ||
         repeatingOne(group53) ||
         repeatingOne(group54) ||
         repeatingOne(group55) ||
         repeatingOne(group61) ||
         repeatingOne(group62) ||
         repeatingOne(group63) ||
         repeatingOne(group64) ||
         repeatingOne(group71) ||
         repeatingOne(group72) ||
         repeatingOne(group73) ||
         repeatingOne(group81) ||
         repeatingOne(group82)
        )
      {
        result = true;
      }

    }
    catch(Exception e)
    {
      System.out.println("groupsOfFiveOrMore: " + e.toString());
    }
    return result;
  }

  private boolean repeatingOne(String data)
  {
    boolean result = true;

    try
    {
      String[]  numArray = new String[data.length()];

      for(int x=0; x<numArray.length; x++)
      {
        if(x == (numArray.length - 1))
        {
          numArray[x] = data.substring(x);
        }
        else
        {
          numArray[x] = data.substring(x, x+1);
        }
      }

      for(int x=0; x<numArray.length - 1; x++)
      {
        if(!numArray[x].equals(numArray[x+1]))
        {
          result = false;
        }
      }
    }
    catch(Exception e)
    {
      System.out.println("repeatingOne: " + e.toString());
    }

    return result;

  }


  private boolean repeatingTwo(String data)
  {
    boolean result = false;

    String group1 = data.substring(0,2);
    String group2 = data.substring(2,4);
    String group3 = data.substring(4,6);
    String group4 = data.substring(6,8);

    if(group1.equals(group2) && group1.equals(group3) && group1.equals(group4))
    {
      result = true;
    }

    return result;

  }

  private boolean repeatingThree(String data)
  {
    boolean result = false;

    String group1 = data.substring(0,3);
    String group2 = data.substring(3,6);
    String group3 = data.substring(6);

    if(group1.equals(group2) && group1.equals(group3))
    {
      result = true;
    }

    return result;

  }



  private boolean okToPass(String sKey)
  {
    boolean result = false;
    result = questionableData.containsKey(sKey);
    return result;
  }

  private void letItPass(String sKey, String sValue)
  {
    questionableData.put(sKey,sValue);
  }

  private void removeFromMap(String sKey)
  {
    questionableData.remove(sKey);
  }

  public String getQuestionableList()
  {
    String  result                  = "";

    if(isQuestionableDataEmpty())
    {
      return result;
    }

    Iterator iter = getQuestionableListIterator();

    while(iter != null && iter.hasNext())
    {
      result = result + ((String)(iter.next()) + "#");
    }

    return result;
  }

  private Iterator getQuestionableListIterator()
  {
    HashSet sortedQuestionableData = new HashSet(questionableData.keySet());
    return sortedQuestionableData.iterator();
  }

  public boolean isQuestionableDataEmpty()
  {
    return this.questionableData.isEmpty();
  }

  public void setQuestionableData(String lst)
  {
    int idx     = 0;
    int nextIdx = lst.indexOf("#");
    String data = "";

    while(nextIdx != -1)
    {
      data = lst.substring(idx, nextIdx);

      letItPass(data, "Questionable Data");
      idx     = ++nextIdx;
      nextIdx = lst.indexOf("#",idx);
    }
  }

  private void submitQuestionableDataList(long appSeqNum)
  {
    StringBuffer      qs    = new StringBuffer("");
    PreparedStatement ps    = null;
    ResultSet         rs    = null;

    try
    {
      //if nothing exists dont submit anything
      if(isQuestionableDataEmpty())
      {
        return;
      }

      qs.setLength(0);
      qs.append("insert into merchant_app_errors (");
      qs.append("app_seq_num, ");
      qs.append("error_date, ");
      qs.append("error_code) ");
      qs.append("values (?, sysdate, ?)");

      ps = getPreparedStatement(qs.toString());


      String errorCode = "";
      Iterator iter = getQuestionableListIterator();

      while(iter != null && iter.hasNext())
      {

        errorCode = ((String)iter.next());
        ps.setLong  (1,  appSeqNum);
        ps.setString(2,  errorCode);

        if(!errorExists(appSeqNum,errorCode))
        {
          ps.executeUpdate();
        }

      }
      ps.close();
    }
    catch (Exception e)
    {
      logEntry("submitQuestionableDataList()", e.toString());
      addError("submitQuestionableDataList: " + e.toString());
    }
  }

  private boolean isValidDiscoverRepId(String repId)
  {

    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    boolean           result  = false;

    try
    {
      //if blank return false.. cause its required
      if(isBlank(repId))
      {
        return result;
      }

      qs.setLength(0);
      qs.append("select * from users      ");
      qs.append("where                    ");
      qs.append("type_id = ? and          ");
      qs.append("third_party_user_id = ?  ");


      ps = getPreparedStatement(qs.toString());

      ps.setLong(1, MesUsers.USER_DISCOVER);
      ps.setString(2, repId);

      rs = ps.executeQuery();

      if(rs.next())
      {
        result = true;
      }

      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      logEntry("isValidDiscoverRepId()", e.toString());
      addError("isValidDiscoverRepId: " + e.toString());
    }

    return result;
  }


  private boolean errorExists(long appSeqNum, String errorCode)
  {
    StringBuffer        qs      = new StringBuffer("");
    PreparedStatement   ps      = null;
    ResultSet           rs      = null;
    boolean             result  = false;

    try
    {
      qs.append("select app_seq_num         ");
      qs.append("from   merchant_app_errors ");
      qs.append("where  app_seq_num = ?     ");
      qs.append("and error_code     = ?     ");
      qs.append("and corrected_date is null ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,   appSeqNum);
      ps.setString(2, errorCode);

      rs = ps.executeQuery();

      if(rs.next())
      {
        result = true;
      }

      rs.close();
      ps.close();

    }
    catch(Exception e)
    {
      result = false;
      logEntry("errorExists()", e.toString());
    }

    return result;
  }



  private boolean validTransitRoute(String aba)
  {
    StringBuffer        qs      = new StringBuffer("");
    PreparedStatement   ps      = null;
    ResultSet           rs      = null;
    boolean             result  = false;

    try
    {
      qs.append("select TRANSIT_ROUTING_NUM     ");
      qs.append("from   RAP_APP_BANK_ABA        ");
      qs.append("where  TRANSIT_ROUTING_NUM = ? ");
      qs.append("       and nvl(fraud_flag, 'N') = 'N' ");

      ps = getPreparedStatement(qs.toString());
      ps.setString(1, aba);

      rs = ps.executeQuery();

      if(rs.next())
      {
        result = true;
      }

      rs.close();
      ps.close();

    }
    catch(Exception e)
    {
      result = false;
      logEntry("validTransitRoute()", e.toString());
    }

    return result;
  }


  private boolean validZipForState(String state, String zip)
  {

    StringBuffer        qs      = new StringBuffer("");
    PreparedStatement   ps      = null;
    ResultSet           rs      = null;
    boolean             result  = false;
    int                 zipNum  = 0;

    try
    {
      if(zip.length() > 5)
      {
        zip = zip.substring(0,5);
      }
      else if(zip.length() < 5)
      {
        return false;
      }

      zipNum = Integer.parseInt(zip);

      qs.append("select zip_low, zip_high ");
      qs.append("from   rapp_app_valid_zipcodes ");
      qs.append("where  state_code = ?");

      ps = getPreparedStatement(qs.toString());
      ps.setString(1, state);

      rs = ps.executeQuery();

      while (rs.next())
      {
        if(! result )
        {
          result =
            (zipNum >= rs.getInt("zip_low") && zipNum <= rs.getInt("zip_high"));
        }
      }

      rs.close();
      ps.close();

    }
    catch(Exception e)
    {
      result = false;
      System.out.println(e.toString());
    }

    return result;
  }


  private boolean isValidMod10Check(String transRoute)
  {
    boolean result = false;

    if(transRoute != null && !transRoute.equals("") && transRoute.length() == 9)
    {
      // I'm hijacking this stupid validation because it fails too freaking often
      return true;
    }

    try
    {
      String  strFirstTwo = transRoute.substring(0,2);
      int     intFirstTwo = Integer.parseInt(strFirstTwo);

      if((intFirstTwo < 1 || intFirstTwo > 12) && (intFirstTwo < 21 || intFirstTwo > 32))
      {
        return result;
      }

      StringBuffer sb = new StringBuffer(transRoute);

      int sum = 0;
      for(int m=0; m<8; m++)
      {
        int multiplier = 0;
        switch(m)
        {
          case 0:
          case 3:
          case 6:
            multiplier = 3;
          break;

          case 1:
          case 4:
          case 7:
            multiplier = 7;
          break;

          case 2:
          case 5:
            multiplier = 1;
          break;
        }
        sum += (Integer.parseInt(String.valueOf(sb.charAt(m))) * multiplier);
      }

      String  strCheckDigit     = Integer.toString(sum);

      int     checkDigitLength  = strCheckDigit.length();

      strCheckDigit     = strCheckDigit.substring(checkDigitLength - 1);

      int     intCheckDigit     = 10 - Integer.parseInt(strCheckDigit);

      if(intCheckDigit == 10)
      {
        intCheckDigit = 0;
      }

      strCheckDigit = Integer.toString(intCheckDigit);

      if(strCheckDigit.equals(String.valueOf(sb.charAt(8))))
      {
        result = true;
      }

    }
    catch(Exception e)
    {
      System.out.println("check digit routine failed");
      result = false;
    }

    return result;
  }


  private void validateRate(String discRate)
  {

    double min1 = 1.85;
    double max1 = 3.50;

    double min2 = 1.22;
    double max2 = 3.00;

    double min3 = 1.45;
    double max3 = 3.00;

    try
    {
      double tempDub = Double.parseDouble(discRate);

      if(discMotoRate)
      {
        if(tempDub < min1 || tempDub > max1)
        {
          addError("The Discover Rap Rate must be between " + Double.toString(min1) + " and " + Double.toString(max1) + " for this merchant");
        }
      }
      else if(this.industryType == 1 || this.industryType == 2)
      {
        if(tempDub < min2 || tempDub > max2)
        {
          addError("The Discover Rap Rate must be between " + Double.toString(min2) + " and " + Double.toString(max2) + " for this merchant");
        }
      }
      else
      {
        if(tempDub < min3 || tempDub > max3)
        {
          addError("The Discover Rap Rate must be between " + Double.toString(min3) + " and " + Double.toString(max3) + " for this merchant");
        }
      }
    }
    catch(Exception e)
    {
    }
  }


  private boolean isValidCenterNumber(String centerNum)
  {
    boolean result = false;
    try
    {
      long temp = Long.parseLong(centerNum);
      if(centerNum.length() == 7)
      {
        result = true;
      }
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }
  private boolean isValidOfficerNumber(String officerNum)
  {
    boolean result = true;
    try
    {
      long temp = Long.parseLong(officerNum);
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }


  public void setCardholderPresent(String cardholderPresent)
  {
    this.cardholderPresent = cardholderPresent;
  }

  public void setCardholderNotPresent(String cardholderNotPresent)
  {
    this.cardholderNotPresent = cardholderNotPresent;
  }

  public String getCardholderPresent()
  {
    return blankIfNull(this.cardholderPresent);
  }

  public String getCardholderNotPresent()
  {
    return blankIfNull(this.cardholderNotPresent);
  }

  public String getSalesRepChange()
  {
    return blankIfNull(this.salesRepChange);
  }

  public void setSalesRepChange(String salesRepChange)
  {
    this.salesRepChange = salesRepChange;
  }

  public String getCbtCreditScore()
  {
    return blankIfNull(this.cbtCreditScore);
  }

  public void setCbtCreditScore(String cbtCreditScore)
  {
    this.cbtCreditScore = cbtCreditScore;
  }

  /*
  ** ACCESSORS
  **
  ** 1. business information
  */

  public void setBusinessName(String newVal)
  {
    businessName = newVal.trim();
  }
  public String getBusinessName()
  {
    return blankIfNull(businessName);
  }

  public void setBusinessLegalName(String newVal)
  {
    businessLegalName = newVal.trim();
  }
  public String getBusinessLegalName()
  {
    return blankIfNull(businessLegalName);
  }

  public void setTaxpayerId(String newVal)
  {
    taxpayerId = newVal.trim();
  }
  public String getTaxpayerId()
  {
    if (taxpayerId.equals("0"))
    {
      return "";
    }
    return blankIfNull(taxpayerId);
  }

  public void setBusinessPhone(String newVal)
  {
    businessPhone = newVal.trim();
  }
  public String getBusinessPhone()
  {
    if (businessPhone.equals("0"))
    {
      return "";
    }
    return blankIfNull(businessPhone);
  }

  public void setCustomerServicePhone(String newVal)
  {
    customerServicePhone = newVal.trim();
  }
  public String getCustomerServicePhone()
  {
    String result = "";
    if(customerServicePhone.equals("0"))
    {
      result = "";
    }
    else
    {
      result = blankIfNull(customerServicePhone);
    }

    return ( result );
  }

  public void setBankPhone(String newVal)
  {
    bankPhone = newVal.trim();
  }
  public String getBankPhone()
  {
    if (bankPhone == null || bankPhone.equals("0"))
    {
      return "";
    }
    return blankIfNull(bankPhone);
  }

  public void setPromotionCode(String newVal)
  {
    promotionCode = newVal.trim();
  }
  public String getPromotionCode()
  {
    return blankIfNull(promotionCode);
  }
  
  public void setChannel(String newVal)
  {
    channel = newVal.trim();
  }
  public String getChannel()
  {
    return( blankIfNull(channel) );
  }

  public void setBusinessEmail(String newVal)
  {
    businessEmail = newVal.trim();
  }
  public String getBusinessEmail()
  {
    return blankIfNull(businessEmail);
  }

  public void setBusinessAddress1(String newVal)
  {
    businessAddress1 = newVal.trim();
  }
  public String getBusinessAddress1()
  {
    return blankIfNull(businessAddress1);
  }

  public void setBusinessAddress2(String newVal)
  {
    businessAddress2 = newVal.trim();
  }
  public String getBusinessAddress2()
  {
    return blankIfNull(businessAddress2);
  }

  public void setBusinessCity(String newVal)
  {
    businessCity = newVal.trim();
  }
  public String getBusinessCity()
  {
    return blankIfNull(businessCity);
  }

  public void setBusinessZip(String newVal)
  {
    businessZip = newVal.trim();
  }
  public String getBusinessZip()
  {
    return blankIfNull(businessZip);
  }

  public void setEstablishedYear(String newVal)
  {
    establishedYear = newVal.trim();
  }
  public String getEstablishedYear()
  {
    if (establishedYear.equals("0"))
    {
      return "";
    }
    return blankIfNull(establishedYear);
  }

  public void setContactNameFirst(String newVal)
  {
    contactNameFirst = newVal.trim();
  }
  public String getContactNameFirst()
  {
    return blankIfNull(contactNameFirst);
  }

  public void setContactNameLast(String newVal)
  {
    contactNameLast = newVal.trim();
  }
  public String getContactNameLast()
  {
    return blankIfNull(contactNameLast);
  }

  public void setContactPhone(String newVal)
  {
    contactPhone = newVal.trim();
  }
  public String getContactPhone()
  {
    return blankIfNull(contactPhone);
  }

  public void setContactEmail(String newVal)
  {
    contactEmail = newVal.trim();
  }
  public String getContactEmail()
  {
    return blankIfNull(contactEmail);
  }

  public void setMailingName(String newVal)
  {
    mailingName = newVal.trim();
  }
  public String getMailingName()
  {
    return blankIfNull(mailingName);
  }

  public void setMailingAddress1(String newVal)
  {
    mailingAddress1 = newVal.trim();
  }
  public String getMailingAddress1()
  {
    return blankIfNull(mailingAddress1);
  }

  public void setMailingAddress2(String newVal)
  {
    mailingAddress2 = newVal.trim();
  }
  public String getMailingAddress2()
  {
    return blankIfNull(mailingAddress2);
  }

  public void setMailingCity(String newVal)
  {
    mailingCity = newVal.trim();
  }
  public String getMailingCity()
  {
    return blankIfNull(mailingCity);
  }

  public void setMailingZip(String newVal)
  {
    mailingZip = newVal.trim();
  }
  public String getMailingZip()
  {
    return blankIfNull(mailingZip);
  }

  public void setBusinessFax(String newVal)
  {
    businessFax = newVal.trim();
  }
  public String getBusinessFax()
  {
    return blankIfNull(businessFax);
  }

  public void setProduct(String newVal)
  {
    product = newVal.trim();
  }
  public String getProduct()
  {
    return blankIfNull(product);
  }

  public void setNumLocations(String newVal)
  {
    numLocations = newVal.trim();
  }
  public String getNumLocations()
  {
    return blankIfNull(numLocations);
  }

  public void setLocationYears(String newVal)
  {
    locationYears = newVal.trim();
  }
  public String getLocationYears()
  {
    return blankIfNull(locationYears);
  }

  public void setBusinessState(String newVal)
  {
    businessState = parseInt(newVal);
  }
  public String getBusinessStateSelected(int idx)
  {
    if (businessState == idx)
    {
      return "selected";
    }

    return "";
  }

  public int getBusinessState()
  {
    return businessState;
  }

  public String getBusinessState2()
  {
    String result = "";
    result = states[businessState];

    return result;
  }


  public void setBusinessCountry(String newVal)
  {
    businessCountry = newVal;
  }

  public String getBusinessCountry()
  {
    return blankIfNull(this.businessCountry);
  }

  public void setMailingState(String newVal)
  {
    mailingState = parseInt(newVal);
  }
  public String getMailingStateSelected(int idx)
  {
    if (mailingState == idx)
    {
      return "selected";
    }

    return "";
  }
  public int getMailingState()
  {
    return mailingState;
  }
  public String getMailingState2()
  {
    String result = "";
    result = states[mailingState];

    return result;
  }

  public String getTotalNumMultiMerchant()
  {
    return blankIfNull(this.totalNumMultiMerchant);
  }
  public String getNumMultiMerchant()
  {
    return blankIfNull(this.numMultiMerchant);
  }
  public String getMultiMerchantMasterDba()
  {
    return blankIfNull(this.multiMerchantMasterDba);
  }
  public String getMultiMerchantMasterControlNum()
  {
    return blankIfNull(this.multiMerchantMasterControlNum);
  }
  public void setTotalNumMultiMerchant(String totalNumMultiMerchant)
  {
    this.totalNumMultiMerchant = totalNumMultiMerchant;
  }
  public void setNumMultiMerchant(String numMultiMerchant)
  {
    this.numMultiMerchant = numMultiMerchant;
  }
  public void setMultiMerchantMasterDba(String multiMerchantMasterDba)
  {
    this.multiMerchantMasterDba = multiMerchantMasterDba;
  }
  public void setMultiMerchantMasterControlNum(String multiMerchantMasterControlNum)
  {
    this.multiMerchantMasterControlNum = multiMerchantMasterControlNum;
  }
  public void setBusinessType(String newVal)
  {
    businessType = parseInt(newVal);
  }
  public String getBusinessTypeSelected(int idx)
  {
    if (businessType == idx)
    {
      return "selected";
    }

    return "";
  }
  public int getBusinessType()
  {
    return businessType;
  }

  public void setIndustryType(String newVal)
  {
    industryType = parseInt(newVal);
  }
  public String getIndustryTypeSelected(int idx)
  {
    if (industryType == idx)
    {
      return "selected";
    }

    return "";
  }
  public int getIndustryType()
  {
    return industryType;
  }

  public String getMccSicCode()
  {
    return blankIfNull(this.mccSicCode);
  }

  public void setMccSicCode(String mccSicCode)
  {
    this.mccSicCode = mccSicCode;
  }

  public String getSicCode()
  {
    return blankIfNull(this.sicCode);
  }

  public void setSicCode(String sicCode)
  {
    this.sicCode = sicCode;
  }


  public void setLocationType(String newVal)
  {
    locationType = parseInt(newVal);
  }
  public String getLocationTypeSelected(int idx)
  {
    if (locationType == idx)
    {
      return "selected";
    }

    return "";
  }

  public String getLocationType()
  {
    return Integer.toString(locationType);
  }

  public void setApplicationType(String newVal)
  {
    applicationType = parseInt(newVal);
  }
  public String getApplicationTypeSelected(int idx)
  {
    if (applicationType == idx)
    {
      return "selected";
    }

    return "";
  }

  public void setTranscomEbtOption(String newVal)
  {
    transcomEbtOption = parseInt(newVal);
  }
  public String getTranscomEbtOptionSelected(int idx)
  {
    if (transcomEbtOption == idx)
    {
      return "selected";
    }

    return "";
  }
  public int getTranscomEbtOption()
  {
    return transcomEbtOption;
  }


  public int getApplicationType()
  {
    return applicationType;
  }

  public void setEstablishedMonth(String newVal)
  {
    establishedMonth = parseInt(newVal);
  }
  public String getEstablishedMonthSelected(int idx)
  {
    if (establishedMonth == idx)
    {
      return "selected";
    }

    return "";
  }

  public String getEstablishedMonth()
  {
    return Integer.toString(establishedMonth);
  }

  /*
  ** 2. merchant history
  */
  public void setHaveProcessed(String newVal)
  {
    haveProcessed = parseInt(newVal);
  }
  public String getHaveProcessedSelected(int idx)
  {
    if (haveProcessed == idx)
    {
      return "selected";
    }

    return "";
  }
  public int getHaveProcessed()
  {
    return haveProcessed;
  }

  public void setPreviousProcessor(String newVal)
  {
    previousProcessor = newVal.trim();
  }
  public String getPreviousProcessor()
  {
    return blankIfNull(previousProcessor);
  }

  public void setStatementsProvided(String newVal)
  {
    statementsProvided = parseInt(newVal);
  }
  public String getStatementsProvidedSelected(int idx)
  {
    if(statementsProvided == idx)
    {
      return "selected";
    }

    return "";
  }

  public void setBeingWon(String newVal)
  {
    beingWon = parseInt(newVal);
  }
  public String getBeingWonSelected(int idx)
  {
    if(beingWon == idx)
    {
      return "selected";
    }

    return "";
  }

  public void setBeingWonReason(String newVal)
  {
    beingWonReason = newVal.trim();
  }
  public String getBeingWonReason()
  {
    return blankIfNull(beingWonReason);
  }

  public int getStatementsProvided()
  {
    return statementsProvided;
  }

  public void setHaveCanceled(String newVal)
  {
    haveCanceled = parseInt(newVal);
  }
  public String getHaveCanceledSelected(int idx)
  {
    if (haveCanceled == idx)
    {
      return "selected";
    }

    return "";
  }
  public int getHaveCanceled()
  {
    return haveCanceled;
  }

  public void setCanceledProcessor(String newVal)
  {
    canceledProcessor = newVal.trim();
  }
  public String getCanceledProcessor()
  {
    return blankIfNull(canceledProcessor);
  }

  public void setCancelReason(String newVal)
  {
    cancelReason = newVal.trim();
  }
  public String getCancelReason()
  {
    return blankIfNull(cancelReason);
  }

  public void setCancelMonth(String newVal)
  {
    cancelMonth = parseInt(newVal);
  }
  public String getCancelMonthSelected(int idx)
  {
    if (cancelMonth == idx)
    {
      return "selected";
    }

    return "";
  }
  public int getCancelMonth()
  {
    return cancelMonth;
  }

  public void setCancelYear(String newVal)
  {
    cancelYear = newVal.trim();
  }
  public String getCancelYear()
  {
    if (cancelYear.equals("0"))
    {
      return "";
    }
    return cancelYear;
  }

  /*
  ** 4. bb&t misc info
  */
  public void setBbtCenterNum(String newVal)
  {
    bbtCenterNum = newVal.trim();
  }
  public String getBbtCenterNum()
  {
    return blankIfNull(bbtCenterNum);
  }
  public void setBbtReferringOfficer(String newVal)
  {
    bbtReferringOfficer = newVal.trim();
  }
  public String getBbtReferringOfficer()
  {
    return blankIfNull(bbtReferringOfficer);
  }

  /*
  ** 3. business checking
  */
  public void setBankName(String newVal)
  {
    bankName = newVal.trim();
  }
  public String getBankName()
  {
    return blankIfNull(bankName);
  }

  public void setCheckingAccount(String newVal)
  {
    checkingAccount = newVal.trim();
  }
  public String getCheckingAccount()
  {
    return blankIfNull(checkingAccount);
  }

  public void setConfirmCheckingAccount(String newVal)
  {
    confirmCheckingAccount = newVal.trim();
  }
  public String getConfirmCheckingAccount()
  {
    return blankIfNull(confirmCheckingAccount);
  }

  public void setAccountConfirmationReq(String newVal)
  {
    this.accountConfirmationReq = true;
  }
  public boolean isAccountConfirmationReq()
  {
    return this.accountConfirmationReq;
  }

  public void setAccountSourceTypeReq(String newVal)
  {
    this.accountSourceTypeReq = true;
  }
  public boolean isAccountSourceTypeReq()
  {
    return this.accountSourceTypeReq;
  }

  public boolean isSkipTypeError()
  {
    return this.skipTypeError;
  }
  public void setSkipTypeError(String newVal)
  {
    this.skipTypeError = true;
  }

  public void setTransitRouting(String newVal)
  {
    transitRouting = newVal.trim();
  }
  public String getTransitRouting()
  {
    return blankIfNull(transitRouting);
  }

  public void setTypeOfAcct(String typeOfAcct)
  {
    this.typeOfAcct = typeOfAcct;
  }
  public String getTypeOfAcct()
  {
    return blankIfNull(this.typeOfAcct);
  }

  public void setSourceOfInfo(String sourceOfInfo)
  {
    this.sourceOfInfo = sourceOfInfo;
  }
  public String getSourceOfInfo()
  {
    return blankIfNull(this.sourceOfInfo);
  }

  public void setConfirmTransitRouting(String newVal)
  {
    confirmTransitRouting = newVal.trim();
  }
  public String getConfirmTransitRouting()
  {
    return blankIfNull(confirmTransitRouting);
  }

  public void setTransitRoutingBbtOther(String newVal)
  {
    transitRoutingBbtOther = newVal.trim();
  }
  public String getTransitRoutingBbtOther()
  {
    return blankIfNull(transitRoutingBbtOther);
  }

  public void setYearsOpen(String newVal)
  {
    yearsOpen = newVal.trim();
  }
  public String getYearsOpen()
  {
    return blankIfNull(yearsOpen);
  }

  public void setBankAddress(String newVal)
  {
    bankAddress = newVal.trim();
  }
  public String getBankAddress()
  {
    return blankIfNull(bankAddress);
  }

  public void setBankCity(String newVal)
  {
    bankCity = newVal.trim();
  }
  public String getBankCity()
  {
    return blankIfNull(bankCity);
  }

  public void setBankState(String newVal)
  {
    bankState = parseInt(newVal);
  }
  public String getBankStateSelected(int idx)
  {
    if (bankState == idx)
    {
      return "selected";
    }

    return "";
  }

  public String getBankState()
  {
    return Integer.toString(bankState);
  }

  public void setBankZip(String newVal)
  {
    bankZip = newVal.trim();
  }
  public String getBankZip()
  {
    return blankIfNull(bankZip);
  }

  public void setReferringBank(String newVal)
  {
    referringBank = newVal;
  }
  public String getReferringBank()
  {
    return blankIfNull(referringBank);
  }

  /*
  ** 4. primary owner
  */
  public void setOwner1FirstName(String newVal)
  {
    owner1FirstName = newVal.trim();
  }
  public String getOwner1FirstName()
  {
    return blankIfNull(owner1FirstName);
  }

  public void setOwner1LastName(String newVal)
  {
    owner1LastName = newVal.trim();
  }
  public String getOwner1LastName()
  {
    return blankIfNull(owner1LastName);
  }

  public void setOwner1SSN(String newVal)
  {
    owner1SSN = newVal.trim();
  }
  public String getOwner1SSN()
  {
    return blankIfNull(owner1SSN);
  }

  public void setOwner1Percent(String newVal)
  {
    owner1Percent = newVal.trim();
  }
  public String getOwner1Percent()
  {
    if (owner1Percent.equals("0"))
    {
      return "";
    }
    return blankIfNull(owner1Percent);
  }

  public void setOwner1Address1(String newVal)
  {
    owner1Address1 = newVal.trim();
  }
  public String getOwner1Address1()
  {
    return blankIfNull(owner1Address1);
  }

  public void setOwner1Address2(String newVal)
  {
    owner1Address2 = newVal.trim();
  }
  public String getOwner1Address2()
  {
    return blankIfNull(owner1Address2);
  }

  public void setOwner1City(String newVal)
  {
    owner1City = newVal.trim();
  }
  public String getOwner1City()
  {
    return blankIfNull(owner1City);
  }

  public void setOwner1Zip(String newVal)
  {
    owner1Zip = newVal.trim();
  }
  public String getOwner1Zip()
  {
    return blankIfNull(owner1Zip);
  }

  public void setOwner1State(String newVal)
  {
    owner1State = parseInt(newVal);
  }
  public String getOwner1StateSelected(int idx)
  {
    if (owner1State == idx)
    {
      return "selected";
    }

    return "";
  }

  public String getOwner1State()
  {
    return Integer.toString(owner1State);
  }
  public String getOwner1State2()
  {
    String result = "";
    result = states[owner1State];

    return result;
  }

  public void setOwner1Phone(String newVal)
  {
    owner1Phone = newVal.trim();
  }
  public String getOwner1Phone()
  {
    return blankIfNull(owner1Phone);
  }

  public void setOwner1Title(String newVal)
  {
    owner1Title = newVal.trim();
  }
  public String getOwner1Title()
  {
    return blankIfNull(owner1Title);
  }

  public void setOwner1SinceMonth(String newVal)
  {
    owner1SinceMonth = parseInt(newVal);
  }
  public String getOwner1SinceMonthSelected(int idx)
  {
    if (owner1SinceMonth == idx)
    {
      return "selected";
    }

    return "";
  }
  public int getOwner1SinceMonth()
  {
    return owner1SinceMonth;
  }

  public void setOwner1SinceYear(String newVal)
  {
    owner1SinceYear = newVal.trim();
  }
  public String getOwner1SinceYear()
  {
    return blankIfNull(owner1SinceYear);
  }

  /*
  ** 5. secondary owner
  */
  public void setOwner2FirstName(String newVal)
  {
    owner2FirstName = newVal.trim();
 }
  public String getOwner2FirstName()
  {
    return blankIfNull(owner2FirstName);
  }

  public void setOwner2LastName(String newVal)
  {
    owner2LastName = newVal.trim();
  }
  public String getOwner2LastName()
  {
    return blankIfNull(owner2LastName);
  }

  public void setOwner2SSN(String newVal)
  {
    owner2SSN = newVal.trim();
  }
  public String getOwner2SSN()
  {
    if (owner2SSN.equals("0"))
    {
      return "";
    }
    return blankIfNull(owner2SSN);
  }

  public void setOwner2Percent(String newVal)
  {
    owner2Percent = newVal.trim();
  }
  public String getOwner2Percent()
  {
    if (owner2Percent.equals("0"))
    {
      return "";
    }
    return blankIfNull(owner2Percent);
  }

  public void setOwner2Address1(String newVal)
  {
    owner2Address1 = newVal.trim();
  }
  public String getOwner2Address1()
  {
    return blankIfNull(owner2Address1);
  }

  public void setOwner2Address2(String newVal)
  {
    owner2Address2 = newVal.trim();
  }
  public String getOwner2Address2()
  {
    return blankIfNull(owner2Address2);
  }

  public void setOwner2City(String newVal)
  {
    owner2City = newVal.trim();
  }
  public String getOwner2City()
  {
    return blankIfNull(owner2City);
  }

  public void setOwner2State(String newVal)
  {
    owner2State = parseInt(newVal);
  }
  public String getOwner2StateSelected(int idx)
  {
    if (owner2State == idx)
    {
      return "selected";
    }

    return "";
  }
  public int getOwner2State()
  {
    return owner2State;
  }

  public void setOwner2Zip(String newVal)
  {
    owner2Zip = newVal.trim();
  }
  public String getOwner2Zip()
  {
    return blankIfNull(owner2Zip);
  }

  public void setOwner2Phone(String newVal)
  {
    owner2Phone = newVal.trim();
  }
  public String getOwner2Phone()
  {
    if (owner2Phone.equals("0"))
    {
      return "";
    }
    return blankIfNull(owner2Phone);
  }

  public void setOwner2Title(String newVal)
  {
    owner2Title = newVal.trim();
  }
  public String getOwner2Title()
  {
    return blankIfNull(owner2Title);
  }

  public void setOwner2SinceMonth(String newVal)
  {
    owner2SinceMonth = parseInt(newVal);
  }
  public String getOwner2SinceMonthSelected(int idx)
  {
    if (owner2SinceMonth == idx)
    {
      return "selected";
    }

    return "";
  }
  public int getOwner2SinceMonth()
  {
    return owner2SinceMonth;
  }

  public void setOwner2SinceYear(String newVal)
  {
    owner2SinceYear = newVal.trim();
  }
  public String getOwner2SinceYear()
  {
    if (owner2SinceYear.equals("0"))
    {
      return "";
    }
    return blankIfNull(owner2SinceYear);
  }

  /*
  ** 6. transaction information
  */

  public void setMonthlySales(String newVal)
  {
    double temp     = getNumber(newVal.trim());
    String result = "";

    //int          index  = newVal.indexOf(",");
    //while(index != -1)
    //{
      //temp = new StringBuffer(newVal);
      //temp.deleteCharAt(index);
      //newVal = temp.toString();
      //index  = newVal.indexOf(",");
    //}

    if(temp != 0L)
    {
      result = Double.toString(temp);
    }

    monthlySales = result.trim();
  }

  public String getMonthlySales()
  {
    String result = monthlySales;
    if(monthlySalesCom)
    {
    }
    return blankIfNull(result);
  }

  public void setSeasonalMerchant(String temp)
  {
    this.seasonalMerchant = temp.equals("Y") ? true : false;
  }

  public boolean isSeasonalMerchant()
  {
    return this.seasonalMerchant;
  }

  public boolean isSeasonalMonth(int idx)
  {
    boolean result = false;

    if(highVolumeMonths.charAt(idx) == 'Y')
    {
      result = true;
    }
    return result;
  }

  public void setSeasonalJan(String temp)
  {
    StringBuffer tempBuff = new StringBuffer(highVolumeMonths);
    tempBuff.setCharAt(0,'Y');
    highVolumeMonths = tempBuff.toString();
  }
  public void setSeasonalFeb(String temp)
  {
    StringBuffer tempBuff = new StringBuffer(highVolumeMonths);
    tempBuff.setCharAt(1,'Y');
    highVolumeMonths = tempBuff.toString();
  }
  public void setSeasonalMar(String temp)
  {
    StringBuffer tempBuff = new StringBuffer(highVolumeMonths);
    tempBuff.setCharAt(2,'Y');
    highVolumeMonths = tempBuff.toString();
  }
  public void setSeasonalApr(String temp)
  {
    StringBuffer tempBuff = new StringBuffer(highVolumeMonths);
    tempBuff.setCharAt(3,'Y');
    highVolumeMonths = tempBuff.toString();
  }
  public void setSeasonalMay(String temp)
  {
    StringBuffer tempBuff = new StringBuffer(highVolumeMonths);
    tempBuff.setCharAt(4,'Y');
    highVolumeMonths = tempBuff.toString();
  }
  public void setSeasonalJun(String temp)
  {
    StringBuffer tempBuff = new StringBuffer(highVolumeMonths);
    tempBuff.setCharAt(5,'Y');
    highVolumeMonths = tempBuff.toString();
  }
  public void setSeasonalJul(String temp)
  {
    StringBuffer tempBuff = new StringBuffer(highVolumeMonths);
    tempBuff.setCharAt(6,'Y');
    highVolumeMonths = tempBuff.toString();
  }
  public void setSeasonalAug(String temp)
  {
    StringBuffer tempBuff = new StringBuffer(highVolumeMonths);
    tempBuff.setCharAt(7,'Y');
    highVolumeMonths = tempBuff.toString();
  }
  public void setSeasonalSep(String temp)
  {
    StringBuffer tempBuff = new StringBuffer(highVolumeMonths);
    tempBuff.setCharAt(8,'Y');
    highVolumeMonths = tempBuff.toString();
  }
  public void setSeasonalOct(String temp)
  {
    StringBuffer tempBuff = new StringBuffer(highVolumeMonths);
    tempBuff.setCharAt(9,'Y');
    highVolumeMonths = tempBuff.toString();
  }
  public void setSeasonalNov(String temp)
  {
    StringBuffer tempBuff = new StringBuffer(highVolumeMonths);
    tempBuff.setCharAt(10,'Y');
    highVolumeMonths = tempBuff.toString();
  }
  public void setSeasonalDec(String temp)
  {
    StringBuffer tempBuff = new StringBuffer(highVolumeMonths);
    tempBuff.setCharAt(11,'Y');
    highVolumeMonths = tempBuff.toString();
  }

  public void setVmcSales(String newVal)
  {
    double temp     = getNumber(newVal.trim());
    String result = "";

    //int          index  = newVal.indexOf(",");
    //while(index != -1)
    //{
      //temp = new StringBuffer(newVal);
      //temp.deleteCharAt(index);
      //newVal = temp.toString();
      //index  = newVal.indexOf(",");
    //}

    if(temp != 0L)
    {
      result = Double.toString(temp);
    }

    vmcSales = result.trim();
  }
  public String getVmcSales()
  {
    String result = vmcSales;
    if(vmcSalesCom)
    {
    }
    return blankIfNull(result);
  }

  public void setAverageTicket(String newVal)
  {
    double temp     = getNumber(newVal.trim());
    String result = "";

    //int          index  = newVal.indexOf(",");
    //while(index != -1)
    //{
      //temp = new StringBuffer(newVal);
      //temp.deleteCharAt(index);
      //newVal = temp.toString();
      //index  = newVal.indexOf(",");
    //}

    if(temp != 0L)
    {
      result = Double.toString(temp);
    }

    averageTicket = result.trim();
  }
  public String getAverageTicket()
  {
    String result = averageTicket;
    if(averageTicketCom)
    {
    }
    return blankIfNull(result);
  }


  public void setAnnualCount(String newVal)
  {
    try
    {
      long temp         = Long.parseLong(newVal);
      this.annualCount  = newVal;
    }
    catch(Exception e)
    {
      this.annualCount  = "";
    }
  }

  public String getAnnualCount()
  {
    return blankIfNull(this.annualCount);
  }

  public void setBbtProcessor(String newVal)
  {
    bbtProcessor = newVal.trim();
  }
  public String getBbtProcessor()
  {
    return blankIfNull(bbtProcessor);
  }

  public void setMotoPercentage(String newVal)
  {
    motoPercentage = newVal.trim();
  }
  public String getMotoPercentage()
  {
    return blankIfNull(motoPercentage);
  }

  public void setMotoPercentageMail(String newVal)
  {
    motoPercentageMail = newVal.trim();
  }
  public String getMotoPercentageMail()
  {
    return blankIfNull(motoPercentageMail);
  }
  public void setMotoPercentageTelephone(String newVal)
  {
    motoPercentageTelephone = newVal.trim();
  }
  public String getMotoPercentageTelephone()
  {
    return blankIfNull(motoPercentageTelephone);
  }

  public void setMotoPercentageMailTele(String newVal)
  {
    this.motoPercentageMailTele = newVal;
  }
  public String getMotoPercentageMailTele()
  {
    return blankIfNull(this.motoPercentageMailTele);
  }

  public void setMotoPercentageInternet(String newVal)
  {
    motoPercentageInternet = newVal.trim();
  }
  public String getMotoPercentageInternet()
  {
    return blankIfNull(motoPercentageInternet);
  }

  public void setInternetSalesOption(String newVal)
  {
    this.internetSalesOption = newVal;
  }
  public String getInternetSalesOption()
  {
    return blankIfNull(this.internetSalesOption);
  }

  public void setRefundPolicy(String newVal)
  {
    refundPolicy = parseInt(newVal);
  }
  public String getRefundPolicySelected(int idx)
  {
    if (refundPolicy == idx)
    {
      return "selected";
    }

    return "";
  }

  public String getRefundPolicy()
  {
    return Integer.toString(refundPolicy);
  }

  public void setVisaAccepted(String newVal)
  {
    visaAccepted = newVal.trim().equals("y");
  }
  public String getVisaAccepted()
  {
    if (visaAccepted)
    {
      return "checked";
    }

    return "";
  }

  public void setMcAccepted(String newVal)
  {
    mcAccepted = newVal.trim().equals("y");
  }
  public String getMcAccepted()
  {
    if (mcAccepted)
    {
      return "checked";
    }

    return "";
  }

  public void setEbtAccepted(String newVal)
  {
    ebtAccepted = newVal.trim().equals("y");
  }
  public String getEbtAccepted()
  {
    if (ebtAccepted)
    {
      return "checked";
    }

    return "";
  }

  public void setTymeAccepted(String newVal)
  {
    tymeAccepted = newVal.trim().equals("y");
  }
  public String getTymeAccepted()
  {
    if (tymeAccepted)
    {
      return "checked";
    }

    return "";
  }


  public void setDebitAccepted(String newVal)
  {
    debitAccepted = newVal.trim().equals("y");
  }
  public String getDebitAccepted()
  {
    if (debitAccepted)
    {
      return "checked";
    }

    return "";
  }

  public void setFcsNumber(String newVal)
  {

    fcsNumber = newVal.trim();

    //ebt is its own category now
    //if(!isBlank(fcsNumber))
    //{
      //ebtAccepted = true;
    //}

  }
  public String getFcsNumber()
  {
    return blankIfNull(fcsNumber);
  }

  public void setDinrAccepted(String newVal)
  {
    dinrAccepted = newVal.trim().equals("y");
  }
  public String getDinrAccepted()
  {
    if (dinrAccepted)
    {
      return "checked";
    }

    return "";
  }

  public void setDinrMerchId(String newVal)
  {
    dinrMerchId = newVal.trim();
  }
  public String getDinrMerchId()
  {
    return blankIfNull(dinrMerchId);
  }

  public void setJcbAccepted(String newVal)
  {
    jcbAccepted = newVal.trim().equals("y");
  }
  public String getJcbAccepted()
  {
    if (jcbAccepted)
    {
      return "checked";
    }

    return "";
  }

  public void setJcbMerchId(String newVal)
  {
    jcbMerchId = newVal.trim();
  }
  public String getJcbMerchId()
  {
    return blankIfNull(jcbMerchId);
  }

  public void setAmexAccepted(String newVal)
  {
    amexAccepted = newVal.trim().equals("y");
  }
  public String getAmexAccepted()
  {
    if (amexAccepted)
    {
      return "checked";
    }

    return "";
  }

  public void setAmexMerchId(String newVal)
  {
    amexMerchId = newVal.trim();
  }
  public String getAmexMerchId()
  {
    return blankIfNull(amexMerchId);
  }

  public void setAmexRate(String newVal)
  {
    try
    {
      if(!isBlank(newVal))
        amexRate = Double.valueOf(newVal.trim()).toString();
      else
        amexRate = "";
    }
    catch(Exception e)
    {
      amexRate = "-1";
    }
  }
  public String getAmexRate()
  {
    String result = amexRate;
    if(result.equals("-1"))
    {
      result = "";
    }
    return result;
  }

  public void setAmexSplitDial(String newVal)
  {
    amexSplitDial = newVal.trim().equals("y");
  }
  public String getAmexSplitDial()
  {
    if (amexSplitDial)
    {
      return "checked";
    }

    return "";
  }

  public void setAmexPip(String newVal)
  {
    amexPip = newVal.trim().equals("y");
  }
  public String getAmexPip()
  {
    if (amexPip)
    {
      return "checked";
    }

    return "";
  }


  public void setAmexNewAccount(String newVal)
  {
    amexNewAccount = newVal.trim().equals("y");
    if (amexNewAccount)
    {
      amexRate = "3.2";
    }
    else
    {
      amexRate = "";
    }
  }
  public String getAmexNewAccount()
  {
    if (amexNewAccount)
    {
      return "checked";
    }

    return "";
  }

  public void setDiscAccepted(String newVal)
  {
    discAccepted = newVal.trim().equals("y");
  }
  public String getDiscAccepted()
  {
    if (discAccepted)
    {
      return "checked";
    }

    return "";
  }

  public void setDiscMerchId(String newVal)
  {
    discMerchId = newVal.trim();
  }
  public String getDiscMerchId()
  {
    return blankIfNull(discMerchId);
  }

  public void setDiscRate(String newVal)
  {
    try
    {
      if(!isBlank(newVal))
        discRate = Double.valueOf(newVal.trim()).toString();
      else
        discRate = "";
    }
    catch(Exception e)
    {
      discRate = "-1";
    }
  }

  public String getDiscRate()
  {
    String result = discRate;
    if(result.equals("-1"))
    {
      result = "";
    }
    return blankIfNull(result);
  }

  public void setDiscFee(String newVal)
  {
    try
    {
      if(!isBlank(newVal))
        discFee = Double.valueOf(newVal.trim()).toString();
      else
        discFee = "";
    }
    catch(Exception e)
    {
      discFee = "-1";
    }
  }

  public String getDiscFee()
  {
    String result = discFee;
    if(result.equals("-1"))
    {
      result = "";
    }
    return blankIfNull(result);
  }

  //this is to set new account for verisign apps...
  public void setDiscNewAccount(String newVal)
  {
    discNewAccount = newVal.trim().equals("y");
    if (discNewAccount)
    {
      if(appType == mesConstants.APP_TYPE_VERISIGN || appType == mesConstants.APP_TYPE_NSI)
      {
        discRate = "2.44";
      }
      else
      {
        discRate = "3.2";
      }
    }
    else
    {
      discRate = "";
    }
  }
  public String getDiscNewAccount()
  {
    if (discNewAccount)
    {
      return "checked";
    }

    return "";
  }

  public void setCheckProvider(String newVal)
  {
    checkProvider = newVal.trim();
  }
  public String getCheckProvider()
  {
    return blankIfNull(checkProvider);
  }

  public void setCheckProviderOther(String newVal)
  {
    checkProviderOther = newVal.trim();
  }
  public String getCheckProviderOther()
  {
    return( blankIfNull(checkProviderOther) );
  }

  public void setCheckMerchId(String newVal)
  {
    checkMerchId = newVal.trim();
  }
  public String getCheckMerchId()
  {
    return blankIfNull(checkMerchId);
  }

  public void setCheckTermId(String newVal)
  {
    checkTermId = newVal.trim();
  }
  public String getCheckTermId()
  {
    return blankIfNull(checkTermId);
  }

  public void setCheckAccepted(String newVal)
  {
    checkAccepted = newVal.trim().equals("y");
  }
  public String getCheckAccepted()
  {
    if (checkAccepted)
    {
      return "checked";
    }

    return "";
  }

  public void setValutecMerchId(String newVal)
  {
    valutecMerchId = newVal.trim();
  }
  public String getValutecMerchId()
  {
    return( blankIfNull(valutecMerchId) );
  }

  public void setValutecTermId(String newVal)
  {
    valutecTermId = newVal.trim();
  }
  public String getValutecTermId()
  {
    return( blankIfNull(valutecTermId) );
  }

  public void setValutecAccepted(String newVal)
  {
    valutecAccepted = newVal.trim().equals("y");
  }
  public String getValutecAccepted()
  {
    String    retVal = "";
    if (valutecAccepted)
    {
      retVal = "checked";
    }

    return( retVal );
  }

  // SVB RC numbers
  public Vector getRCNums()
  {
    return rcNums;
  }

  public String getRCSelected(int i)
  {
    String result = "";
    try
    {
      RCNum rc = (RCNum)(rcNums.elementAt(i));

      if(svbAssoc.equals(Integer.toString(rc.association)))
      {
        result = "selected";
      }
    }
    catch(Exception e)
    {
      System.out.println("getRCSelected(" + i + "): " + e.toString());
      logEntry("getRCSelected(" + i + ")", e.toString());
    }
    return result;
  }

  // POS Partner Field Accessors
  public Vector getPosPartnerOSOptions( )
  {
    return( posPartnerOSOptions );
  }

  public void setPosPartnerExtendedFieldSupport( boolean newVal )
  {
    supportsPosPartnerExtendedFields = newVal;
  }

  public void setPosPartnerConnectivity(String newVal)
  {
    posPartnerConnectivity = newVal.trim();
  }
  public String getPosPartnerConnectivity(String value)
  {
    String    retVal    = "";

    if ( value != null && posPartnerConnectivity != null && posPartnerConnectivity.equals(value) )
    {
      retVal = "checked";
    }
    return( retVal );
  }

  public void setPosPartnerLevelII(String newVal)
  {
    posPartnerLevelII = newVal.trim().equals("y");
  }

  public String getPosPartnerLevelII()
  {
    String    retVal = "";
    if (posPartnerLevelII)
    {
      retVal = "checked";
    }

    return( retVal );
  }

  public void setPosPartnerLevelIII(String newVal)
  {
    posPartnerLevelIII = newVal.trim().equals("y");
  }
  public String getPosPartnerLevelIII()
  {
    String    retVal = "";
    if (posPartnerLevelIII)
    {
      retVal = "checked";
    }

    return( retVal );
  }

  public void setPosPartnerOS(String newVal)
  {
    posPartnerOS = newVal.trim();
  }
  public String getPosPartnerOS( String value )
  {
    String      retVal    = "";

    if ( value != null && posPartnerOS != null && posPartnerOS.equals(value) )
    {
      retVal = "selected";
    }
    return( retVal );
  }

  /*
  ** 7. product selection
  */
  public void setProductType(String newVal)
  {
    productType = parseInt(newVal);
  }
  public String getProductTypeSelected(int idx)
  {
    if (productType == idx)
    {
      return "checked";
    }

    return "";
  }
  public int getProductType()
  {
    return productType;
  }

  public void setPcType(String newVal)
  {
    pcType = parseInt((String)pcTypeCodes.elementAt(parseInt(newVal)));
  }
  public String getPcTypeSelected(int idx)
  {
    if (pcType == parseInt((String)pcTypeCodes.elementAt(idx)))
    {
      return "selected";
    }

    return "";
  }

  public void setPcLicenses(String newVal)
  {
    pcLicenses = newVal;
  }
  public String getPcLicenses()
  {
    return blankIfNull(pcLicenses);
  }

  public void setInternetType(String newVal)
  {
    internetType = parseInt((String)internetTypeCodes.elementAt(parseInt(newVal)));

    if(internetType == mesConstants.APP_MPOS_ACTIVE_C_STOREFRONT_BASIC || internetType == mesConstants.APP_MPOS_ACTIVE_C_STOREFRONT_PLUS)
    {
      this.validEmail = true;
    }
  }

  public String getInternetTypeSelected(int idx)
  {
    if (internetType == parseInt((String)internetTypeCodes.elementAt(idx)))
    {
      return "selected";
    }

    return "";
  }

  public void setSvbInternetType(String newVal)
  {
    internetType = parseInt((String)svbInternetTypeCodes.elementAt(parseInt(newVal)));
  }
  public String getSvbInternetTypeSelected(int idx)
  {
    if (internetType == parseInt((String)svbInternetTypeCodes.elementAt(idx)))
    {
      return "selected";
    }

    return "";
  }

  public void setWebUrl(String newVal)
  {
    if(newVal != null)
    {
      webUrl = newVal.trim();
    }
  }
  public String getWebUrl()
  {
    return blankIfNull(webUrl);
  }
  public String getMerchantNumber()
  {
    return blankIfNull(this.merchantNumber);
  }
  public void setAppType(String newVal)
  {
    appType = parseInt(newVal);
  }
  public String getAppType()
  {
    return Integer.toString(appType);
  }
  public int getAppTypeInt()
  {
    return this.appType;
  }

  /*
  ** comments
  */
  public void setComments(String newVal)
  {
    comments = newVal.trim();
  }
  public String getComments()
  {
    return blankIfNull(comments);
  }

  /*
  ** option vectors
  */

  public Vector getDiscoverBankReferralNames()
  {
    return this.discoverBankReferralNames;
  }

  public Vector getDiscoverBankReferralNumbers()
  {
    return this.discoverBankReferralNumbers;
  }

  public Vector getDiscoverFranchiseDesc()
  {
    return this.discoverFranchiseDesc;
  }
  public Vector getDiscoverFranchiseCode()
  {
    return this.discoverFranchiseCode;
  }

  public Vector getYesNoOptions()
  {
    return yesNoOptions;
  }
  public Vector getStateOptions()
  {
    return stateOptions;
  }
  public Vector getMonthOptions()
  {
    return monthOptions;
  }
  public Vector getBusinessTypeOptions()
  {
    return businessTypeOptions;
  }

  public Vector getTranscomEbtOptions()
  {
    return transcomEbtOptions;
  }

  public Vector getBbtBusinessTypeDesc()
  {
    return bbtBusinessTypeDesc;
  }

  public Vector getBbtBusinessTypes()
  {
    return bbtBusinessTypes;
  }

  public Vector getBbtIndustryTypeDesc()
  {
    return bbtIndustryTypeDesc;
  }

  public Vector getBbtIndustryTypes()
  {
    return bbtIndustryTypes;
  }

  public Vector getExpandedIndustryTypeDesc()
  {
    return expandedIndustryTypeDesc;
  }

  public Vector getExpandedIndustryTypes()
  {
    return expandedIndustryTypes;
  }

  public Vector getTransIndustryTypeDesc()
  {
    return transIndustryTypeDesc;
  }

  public Vector getTransIndustryTypes()
  {
    return transIndustryTypes;
  }

  public Vector getTransInternetSalesOptions()
  {
    return transInternetSalesOptions;
  }

  public Vector getTransInternetSalesOptionsCodes()
  {
    return transInternetSalesOptionsCodes;
  }

  public Vector getAccountTypeDesc()
  {
    return accountTypeDesc;
  }

  public Vector getAccountTypeCode()
  {
    return accountTypeCode;
  }

  public Vector getBbtLocationTypeDesc()
  {
    return bbtLocationTypeDesc;
  }

  public Vector getInfoSource()
  {
    return infoSource;
  }


  public Vector getBbtLocationTypes()
  {
    return bbtLocationTypes;
  }

  public Vector getBbtTransitRoutingDesc()
  {
    return bbtTransitRoutingDesc;
  }

  public Vector getBbtTransitRoutingNumbers()
  {
    return bbtTransitRoutingNumbers;
  }

  public Vector getBbtProcessors()
  {
    return bbtProcessors;
  }

  public Vector getBbtProcessorCodes()
  {
    return bbtProcessorCodes;
  }

  public Vector getIndustryTypeOptions()
  {
    return industryTypeOptions;
  }
  public Vector getLocationTypeOptions()
  {
    return locationTypeOptions;
  }

  public Vector getTransLocationTypeOptions()
  {
    return transLocationTypeOptions;
  }

  public Vector getApplicationTypeOptions()
  {
    return applicationTypeOptions;
  }
  public Vector getPcTypeOptions()
  {
    return pcTypeOptions;
  }
  public Vector getRefundPolicyOptions()
  {
    return refundPolicyOptions;
  }

  public Vector getTransRefundPolicyOptions()
  {
    return transRefundPolicyOptions;
  }

  public Vector getInternetTypeOptions()
  {
    return internetTypeOptions;
  }
  public Vector getSvbInternetTypeOptions()
  {
    return svbInternetTypeOptions;
  }

  public Vector getCountryDescs()
  {
    return countryDesc;
  }
  public Vector getCountryCodes()
  {
    return countryCode;
  }

  public String getNbscBankNum()
  {
    return blankIfNull(this.nbscBankNum);
  }
  public String getNbscAssNum()
  {
    return blankIfNull(this.nbscAssNum);
  }

  public String getGoldAssNum()
  {
    return blankIfNull(this.goldAssNum);
  }

  public void setNbscBankNum(String nbscBankNum)
  {
    this.nbscBankNum = nbscBankNum;
  }
  public void setNbscAssNum(String nbscAssNum)
  {
    this.nbscAssNum = nbscAssNum;
  }

  public void setGoldAssNum(String goldAssNum)
  {
    this.goldAssNum = goldAssNum;
  }

  public void setSvbAssoc(String svbAssoc)
  {
    this.svbAssoc = svbAssoc;
  }

  public String getCbtAssNum()
  {
    return blankIfNull(this.cbtAssNum);
  }
  public void setCbtAssNum(String cbtAssNum)
  {
    this.cbtAssNum = cbtAssNum;
  }

  public String getCbtRepCode()
  {
    return blankIfNull(this.cbtRepCode);
  }
  public void setCbtRepCode(String cbtRepCode)
  {
    this.cbtRepCode = cbtRepCode;
  }

  public String getExistingVerisign()
  {
    return blankIfNull(this.existingVerisign);
  }
  public void setExistingVerisign(String existingVerisign)
  {
    this.existingVerisign = existingVerisign;
  }

  public boolean isRecalculated()
  {
    return this.recalculate;
  }

  public void setRecalculate(String recalculate)
  {
    this.recalculate = true;
  }
  public boolean getRecalcuate()
  {
    return this.recalculate;
  }
  public String getTarget()
  {
    String target = "";
    if(this.recalculate == true)
      target="calc";
    return target;
  }

  public String getAnnualSales(String monthlySales)
  {
    return NumberFormat.getCurrencyInstance(Locale.US).format((12.0 * parseDouble(monthlySales)));
  }

  public String getSvbCifNum()
  {
    return blankIfNull(this.svbCifNum);
  }
  public void setSvbCifNum(String svbCifNum)
  {
    try
    {
      long temp = Long.parseLong(svbCifNum);
      this.svbCifNum = svbCifNum;
    }
    catch(Exception e)
    {
    }
  }

  public String getSvbCdNum()
  {
    return blankIfNull(this.svbCdNum);
  }
  public void setSvbCdNum(String svbCdNum)
  {
    try
    {
      float temp = Float.parseFloat(svbCdNum);
      this.svbCdNum = svbCdNum;
    }
    catch(Exception e)
    {
    }
  }

  public String getSvbCdCollateral()
  {
    return Double.toString(svbCdCollateral);
  }
  public void setSvbCdCollateral(String svbCdCollateral)
  {
    try
    {
      this.svbCdCollateral = Double.parseDouble(svbCdCollateral);
    }
    catch(Exception e)
    {
      this.svbCdCollateral = 0.0;
    }
  }

  public String getGoldAccountType()
  {
    return blankIfNull(this.goldAccountType);
  }
  public void setGoldAccountType(String goldAccountType)
  {
    this.goldAccountType = goldAccountType;
  }

  public String getVitalProduct()
  {
    return blankIfNull(this.vitalProduct);
  }
  public void setVitalProduct(String vitalProduct)
  {
    this.vitalProduct = vitalProduct;
  }

  public String getThirdPartySoftware()
  {
    return blankIfNull(this.thirdPartySoftware);
  }
  public void setThirdPartySoftware(String thirdPartySoftware)
  {
    this.thirdPartySoftware = thirdPartySoftware;
  }

  public String getVtEmail()
  {
    return blankIfNull(vtEmail);
  }
  public void setVtEmail(String vtEmail)
  {
    this.vtEmail = vtEmail;
  }

  public String getInternetEmail()
  {
    return blankIfNull(internetEmail);
  }
  public void setInternetEmail(String internetEmail)
  {
    this.internetEmail = internetEmail;
  }

  public String getPgCertName()
  {
    return blankIfNull(pgCertName);
  }
  public void setPgCertName(String pgCertName)
  {
    this.pgCertName = pgCertName;
  }

  public String getPgEmail()
  {
    return blankIfNull(pgEmail);
  }
  public void setPgEmail(String pgEmail)
  {
    this.pgEmail = pgEmail;
  }

  public String getPgCard()
  {
    return blankIfNull(pgCard);
  }

  public void setPgCard(String pgCard)
  {
    this.pgCard = pgCard;
  }

  public String getPgCard(String value)
  {
    String    retVal    = "";

    if ( value != null && pgCard != null && pgCard.equals(value) )
    {
      retVal = "checked";
    }
    return( retVal );
  }


  public String getDiscoverRepName()
  {
    return blankIfNull(this.discoverRepName);
  }
  public void setDiscoverRepName(String discoverRepName)
  {
    this.discoverRepName = discoverRepName;
  }

  public String getDiscoverRepId()
  {
    return blankIfNull(this.discoverRepId);
  }
  public void setDiscoverRepId(String discoverRepId)
  {
    this.discoverRepId = discoverRepId;
  }

  public String getFranchiseCode()
  {
    return blankIfNull(this.franchiseCode);
  }
  public void setFranchiseCode(String franchiseCode)
  {
    this.franchiseCode = franchiseCode;
  }

  public String getBankReferralNum()
  {
    return blankIfNull(this.bankReferralNum);
  }
  public void setBankReferralNum(String bankReferralNum)
  {
    this.bankReferralNum = bankReferralNum;
  }

  public boolean isBankReferralApp()
  {
    return this.bankReferralApp;
  }

  public void setBankReferralApp(String yeaOrNea)
  {
    this.bankReferralApp = yeaOrNea.equals("Y");
  }

  public String getAgreement()
  {
    return blankIfNull(this.agreement);
  }
  public void setAgreement(String agreement)
  {
    this.agreement = agreement;
  }

  public void setYesAgreeReq(boolean well)
  {
    this.yesAgreeReq = well;
  }

  public double getNumber(String raw)
  {
    double          result      = 0L;
    StringBuffer  digits      = new StringBuffer("");

    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == '.')
        {
          digits.append(raw.charAt(i));
        }
      }

      result = parseDouble(digits.toString());
    }
    catch(Exception e)
    {
      logEntry("getNumber()", e.toString());
    }

    return result;
  }


  public String clearZero(String temp)
  {
    String result = "";
    if(!isBlank(temp) && !temp.equals("0"))
    {
      result = temp;
    }
    return result;
  }

  public String getImprinterPlates()
  {
    String result = "";
    if(this.imprinterPlates > 0)
    {
      result = String.valueOf(this.imprinterPlates);
    }

    return result;
  }
  public void setImprinterPlates(String imprinterPlates)
  {
    try
    {
      this.imprinterPlates = Integer.parseInt(imprinterPlates);
    }
    catch(Exception e)
    {
      this.imprinterPlates = 0;
    }
  }

  public boolean isShoppingCart()
  {
    return this.shoppingCart;
  }

  public void setShoppingCart()
  {
    this.shoppingCart = true;
  }

  public class RCNum
  {
    public  int     association;
    public  String  description;

    public RCNum(ResultSet rs)
    {
      try
      {
        association = rs.getInt("association");
        description = rs.getString("rc_description");
      }
      catch(Exception e)
      {
        System.out.println("Failure to instantiate RCNum: " + e.toString());
      }
    }
  }
}/*@lineinfo:generated-code*/