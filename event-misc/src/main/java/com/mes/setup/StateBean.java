/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/StateBean.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/21/00 10:32a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.setup;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import com.mes.database.DBConnect;

public class StateBean
{
  /* Private Variables */
  private String tempStr=null;
  private  Statement st=null;
  private  Connection con=null;

  /* Constructor */

  public StateBean()
  {
  }

  /* Public Methods */
  public Vector getStates() 
  {
    Vector tempVec = new Vector(60,10);
    String Query = "select * from countrystate order by countrystate_code";  
    DBConnect db = new DBConnect(this, "getStates");

    try 
    {
      con = db.getConnection("pool;ApplicationPool");
      
      if(con == null)
      {
        com.mes.support.SyncLog.LogEntry("getStates: " + db.getErrorMessage());
      }
      else
      {
        st=con.createStatement();
        ResultSet rs=st.executeQuery(Query);
        while(rs.next()) 
        { 
          tempStr=rs.getString(1);
          tempVec.add(tempStr);
        }          
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(1, this.getClass().getName(), "getStates: " + e.toString());
    }
    finally 
    {
      try 
      {
        st.close();
        db.releaseConnection();
      }/* end of try loop */
      catch (Exception e) 
      {
        com.mes.support.SyncLog.LogEntry(2, this.getClass().getName(), "getStates: " + e.toString());
      }
    }/* end of final loop */  

    return tempVec;
  }/* end of getStates */
  

}/* end of StateBean */


