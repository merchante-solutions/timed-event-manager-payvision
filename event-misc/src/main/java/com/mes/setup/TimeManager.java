/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/TimeManager.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.setup;

public class TimeManager
{
  private int       rawTime = 0;
  
  /*
  ** CONSTRUCTOR
  */
  public TimeManager()
  {
    rawTime = 0;
  }
  
  /*
  ** METHOD getHours
  */
  public int getHours()
  {
    if (rawTime >= 1300)
    {
      return (rawTime / 100) - 1200;
    }
    
    return rawTime / 100;
  }
  
  /*
  ** METHOD getMinutes
  */
  public int getMinutes()
  {
    return rawTime % 100;
  }
  
  /*
  ** METHOD getQuarter
  */
  public int getQuarter()
  {
    // this is a hack to allow quarter hours to get matched
    // up with form drop down list indexes that allow selection
    // of quarter hour increments only
    if (getHours() != 0)
    {
      int minutes = getMinutes();
      if (minutes == 0)
      {
        return 1;
      }
      else if (minutes == 15)
      {
        return 2;
      }
      else if (minutes == 30)
      {
        return 3;
      }
      else if (minutes == 45)
      {
        return 4;
      }
    }
    
    return 0;
  }
  
  /*
  ** METHOD isPM
  */
  public boolean isPM()
  {
    return (rawTime > 1299);
  }
  
  /*
  ** METHOD toString
  */
  public String toString()
  {
    return Integer.toString(rawTime);
  }
  
  /*
  ** METHOD setTime
  */
  public void setTime(int newTime)
  {
    rawTime = newTime;
  }
  
  /*
  ** METHOD setHour
  */
  public void setHours(String hours)
  {
    try
    {
      setHours(Integer.parseInt(hours));
    }
    catch(Exception e)
    {
    }
  }
  public void setHours(int newHours)
  {
    if (newHours >= 1 && newHours <= 12)
    {
      // if currently a pm time, then adjust
      // new time to be in the pm zone as well
      if (rawTime >= 1300)
      {
        newHours += 12;
      }
      
      rawTime = getMinutes() + (newHours * 100);
    }
  }
  
  /*
  ** METHOD setMinute
  */
  public void setMinutes(String minutes)
  {
    try
    {
      setMinutes(Integer.parseInt(minutes));
    }
    catch(Exception e)
    {
    }
  }
  public void setMinutes(int newMinutes)
  {
    if (newMinutes >= 0 && newMinutes <= 59)
    {
      rawTime = (getHours() * 100) + newMinutes;
    }
  }
  
  /*
  ** METHOD setAMPM
  */
  public void setIsPM(boolean isPM)
  {
    if (getHours() <= 12 && isPM)
    {
      rawTime += 1200;
    }
    else if (getHours() >= 13 && !isPM)
    {
      rawTime -= 1200;
    }
  }
  
  /*
  ** METHOD setTime
  */
  public void setTime(String stringTime)
  {
    try
    {
      int newTime = Integer.parseInt(stringTime);
      if (newTime >= 100 && newTime <= 2459)
      {
        rawTime = newTime;
      }
    }
    catch(Exception e)
    {
    }
  }
  
  /*
  ** METHOD isValid
  */
  public boolean isValid()
  {
    if (rawTime >= 100 && rawTime <= 2459)
    {
      return true;
    }
    
    return false;
  }
}
    