/*@lineinfo:filename=ValidateUser*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/ValidateUser.sqlj $

  Description:
    Base class for application data beans

    This class should maintain any data that is common to all of the
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-10-08 10:25:00 -0700 (Wed, 08 Oct 2008) $
  Version            : $Revision: 15432 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.setup;

import java.io.Serializable;
import java.sql.ResultSet;
import com.mes.constants.MesUsers;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class ValidateUser extends SQLJConnectionBase
  implements Serializable
{
  private long userId             = 0L;
  private boolean validUser       = true;
  private boolean recordExists    = true;
  
  public ValidateUser()
  {
  }

  public ValidateUser(boolean validateOn)
  {
  }

  public boolean isValidSequence(long sequenceId, long userSequenceType)
  {
    boolean result = false;
    if(sequenceId == userSequenceType)
    {
      result = true;
    }
    else
    {
      if(sequenceId == com.mes.constants.mesConstants.SEQUENCE_APPLICATION_TRANSCOM && userSequenceType == com.mes.constants.mesConstants.SEQUENCE_APPLICATION)
      {
        this.validUser = true; //let mes users see transcom sequence
      }
      else
      {
        this.validUser = false;
      }
    }
    result = true;
    return result;
  }

  public boolean isValidUser(long appSeqNum, long appType)
  {
    ResultSetIterator it          = null;
    ResultSet         rs          = null;
    boolean           result      = false;
    boolean           sales       = false;
    boolean           salesMan    = false;
    boolean           salesSuper  = false;
    boolean           wasStale    = false;
    
    //if validation is off or a new app is being started return true
    if(appSeqNum > 0L)
    {
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:92^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct(ugtr.right_id) right_id
//            from    user_group_to_right ugtr,
//                    user_to_group       utg,
//                    users               u
//            where   u.user_id = :userId and
//                    (u.user_id = utg.user_id or u.type_id = utg.user_id) and
//                    utg.group_id = ugtr.group_id and
//                    ugtr.right_id in 
//                    (
//                      :MesUsers.RIGHT_APPLICATION_PROXY,  -- 201
//                      :MesUsers.RIGHT_APPLICATION_MGR,    -- 202
//                      :MesUsers.RIGHT_APPLICATION_SUPER   -- 203
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct(ugtr.right_id) right_id\n          from    user_group_to_right ugtr,\n                  user_to_group       utg,\n                  users               u\n          where   u.user_id =  :1  and\n                  (u.user_id = utg.user_id or u.type_id = utg.user_id) and\n                  utg.group_id = ugtr.group_id and\n                  ugtr.right_id in \n                  (\n                     :2 ,  -- 201\n                     :3 ,    -- 202\n                     :4    -- 203\n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.setup.ValidateUser",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userId);
   __sJT_st.setLong(2,MesUsers.RIGHT_APPLICATION_PROXY);
   __sJT_st.setLong(3,MesUsers.RIGHT_APPLICATION_MGR);
   __sJT_st.setLong(4,MesUsers.RIGHT_APPLICATION_SUPER);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.setup.ValidateUser",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:107^9*/
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          long right = rs.getLong("right_id");
          if(right == MesUsers.RIGHT_APPLICATION_PROXY)
          {
            sales = true;
          }
          
          if(right == MesUsers.RIGHT_APPLICATION_MGR)
          {
            salesMan = true;
          }
          
          if(right ==  MesUsers.RIGHT_APPLICATION_SUPER)
          {
            salesSuper = true;
          }
        }
        
        rs.close();
        it.close();
          
        int   allowedCount = 0;
        if(salesSuper)
        {
          allowedCount = 1;
        }
        else if(salesMan)
        {
          // see if user is allowed to view app
          /*@lineinfo:generated-code*//*@lineinfo:141^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(app.app_seq_num)
//              
//              from    application   app,
//                      t_hierarchy   th,
//                      users         u1,
//                      users         u2
//              where   app.app_seq_num = :appSeqNum and
//                      app.app_user_id = u1.user_id and
//                      :userId = u2.user_id and
//                      th.ancestor = u2.hierarchy_node and
//                      th.descendent = u1.hierarchy_node
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app.app_seq_num)\n             \n            from    application   app,\n                    t_hierarchy   th,\n                    users         u1,\n                    users         u2\n            where   app.app_seq_num =  :1  and\n                    app.app_user_id = u1.user_id and\n                     :2  = u2.user_id and\n                    th.ancestor = u2.hierarchy_node and\n                    th.descendent = u1.hierarchy_node";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.setup.ValidateUser",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,userId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   allowedCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:154^11*/
          
          if(allowedCount == 0)
          {
            // see if this account has a merchant number that rolls up to this user
            /*@lineinfo:generated-code*//*@lineinfo:159^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(mr.app_seq_num)
//                
//                from    merchant mr,
//                        t_hierarchy th,
//                        mif m,
//                        users u
//                where   mr.app_seq_num = :appSeqNum and
//                        mr.merch_number = m.merchant_number and
//                        m.association_node = th.descendent and
//                        th.ancestor = u.hierarchy_node and
//                        u.user_id = :userId            
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mr.app_seq_num)\n               \n              from    merchant mr,\n                      t_hierarchy th,\n                      mif m,\n                      users u\n              where   mr.app_seq_num =  :1  and\n                      mr.merch_number = m.merchant_number and\n                      m.association_node = th.descendent and\n                      th.ancestor = u.hierarchy_node and\n                      u.user_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.setup.ValidateUser",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,userId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   allowedCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:172^13*/
          }
          /*
          #sql [Ctx]
          {
            select  count(app_seq_num)
            into    :allowedCount
            from    application
            where   app_seq_num = :appSeqNum and
                    (app_user_id = :userId or app_type = :appType)
          };
          */
        }
        else if(sales)
        {
          /*@lineinfo:generated-code*//*@lineinfo:187^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//              
//              from    application
//              where   app_seq_num = :appSeqNum and
//                      app_user_id = :userId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n             \n            from    application\n            where   app_seq_num =  :1  and\n                    app_user_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.setup.ValidateUser",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,userId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   allowedCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:194^11*/
        }
          
        if(allowedCount > 0)
        {
          validUser = true;
          result = true;
        }
        else
        {
          validUser = false;
          result = false;
        }
      }
      catch(Exception e)
      {
        logEntry("isValidUser(" + appSeqNum + ", " + userId + ")", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
    else
    {
      this.validUser = true;
      result = true;
    }
    
    return result;
  }
  
  public boolean isValidUser()
  { 
    return this.validUser;
  }
  
  public boolean isRecordExists()
  { 
    return this.recordExists;
  }

  public void resetFlag()
  {
    this.validUser    = true;
    this.recordExists = true;
  }

  public void setUserId(String userId)
  {
    try
    {
      setUserId(Long.parseLong(userId));
    }
    catch(Exception e)
    {
    }
  }
  public void setUserId(long userId)
  {
    this.userId = userId;
  }
  
  public long getUserId()
  {
    return this.userId;
  }
}/*@lineinfo:generated-code*/