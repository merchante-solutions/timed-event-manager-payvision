/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/AppTypeBean.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/17/01 5:44p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.setup;

public class AppTypeBean extends com.mes.screens.SequenceDataBean
{

  private  int  appType          = -1;
  private  long sequenceType     = -1L;
   
  private int appTypeFromUserType(int userType)
  {
    int appType = com.mes.constants.mesConstants.APP_TYPE_MES;

    if(userType == com.mes.constants.mesConstants.USER_VERISIGN_REP || userType == com.mes.constants.mesConstants.USER_VERISIGN_MANAGER)    
    {
      appType = com.mes.constants.mesConstants.APP_TYPE_VERISIGN;  
    }
    else if(userType == com.mes.constants.mesConstants.USER_DEMO)
    {
      appType = com.mes.constants.mesConstants.APP_TYPE_DEMO;  
    }
    else if(userType == com.mes.constants.mesConstants.USER_CBT)
    {
      appType = com.mes.constants.mesConstants.APP_TYPE_CBT_NEW;  
    }
    else if(userType == com.mes.constants.mesConstants.USER_CEDAR)
    {
      appType = com.mes.constants.mesConstants.APP_TYPE_CEDAR;
    }

    return appType;
  }
  
  private long sequenceTypeFromUserType(int userType)
  {
    long sequenceType = com.mes.constants.mesConstants.SEQUENCE_APPLICATION;

    if(userType == com.mes.constants.mesConstants.USER_VERISIGN_REP || userType == com.mes.constants.mesConstants.USER_VERISIGN_MANAGER)    
    {
      sequenceType = com.mes.constants.mesConstants.SEQUENCE_ID_APPLICATION;  
    }
    else if(userType == com.mes.constants.mesConstants.USER_DEMO)
    {
      sequenceType = com.mes.constants.mesConstants.SEQUENCE_ID_APPLICATION;  
    }
    else if(userType == com.mes.constants.mesConstants.USER_CBT)
    {
      sequenceType = com.mes.constants.mesConstants.SEQUENCE_APPLICATION;  
    }
    else if(userType == com.mes.constants.mesConstants.USER_CEDAR)
    {
      sequenceType = com.mes.constants.mesConstants.SEQUENCE_APPLICATION;
    }

    return sequenceType;
  }

  public int getAppType()
  {
    return this.appType;
  }

  public long getSequenceType()
  {
    return this.sequenceType;
  }

}
