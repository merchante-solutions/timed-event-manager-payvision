/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/PricingBean.java $

  Description:  

   
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-12-01 15:02:28 -0800 (Mon, 01 Dec 2008) $
  Version            : $Revision: 15576 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.setup;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.support.HttpHelper;

/**
 *  This bean is used for all the application whose source is not Merchant Direct.
 *  The options selected by the user on the Payment Options page and the equipment
 *  pages are populated with there default values.
 *  The data inserted is updated into the database.
 */
public class PricingBean extends com.mes.screens.SequenceDataBean
{
  public final static int     NUM_CARDS               = 10;
  public final static int     NUM_MISC                = 20;
  public final static int     NUM_QUESTIONS           = 6;
  private final static String OWNEDEQUIPAMT           = "5";
  private final static int    TRANSACTION_MIN_CHARGE  = 25;
  //private final static int CBT_TRANSACTION_MIN_CHARGE = 10;

  public final static String  SITE_INSP_REQUESTED     = "REQUESTED";
  public final static String  SITE_INSP_NOT_REQUESTED = "NOT REQUESTED";

  public final static String  CLUB                    = "CLUB";
  public final static String  NCLUB                   = "NCLUB";

  public final static int     CARD_DINERS             = 0;
  public final static int     CARD_DISCOVER           = 1;
  public final static int     CARD_JCB                = 2;
  public final static int     CARD_AMEX               = 3;
  public final static int     CARD_DEBIT              = 4;
  public final static int     CARD_CHECK              = 5;
  public final static int     CARD_INTERNET           = 6;
  public final static int     CARD_DIALPAY            = 7;
  public final static int     CARD_EBT                = 8;
  public final static int     CARD_VMC                = 9;

  public final static int     FEE_CHARGEBACK          = 0;
  public final static int     FEE_INTERNET_MONTHLY    = 1;
  public final static int     FEE_HELPDESK            = 2;
  public final static int     FEE_STATEMENT           = 3;
  public final static int     FEE_WEB_REPORTING       = 4;
  public final static int     FEE_NEW_APP             = 5;
  public final static int     FEE_ACCOUNT_SETUP       = 6;
  public final static int     FEE_INTERNET_SETUP      = 7;
  public final static int     FEE_VT_SETUP            = 8;
  public final static int     FEE_VT_MONTHLY          = 9;
  public final static int     FEE_MEMBERSHIP          = 10;
  public final static int     FEE_PCI_ADMIN           = 11;
  public final static int     FEE_RUSH                = 12;
  public final static int     FEE_UNIQUE              = 13;
  public final static int     FEE_GATEWAY_MONTHLY     = 14;
  public final static int     FEE_GATEWAY_SETUP       = 15;
  public final static int     FEE_TERM_REPROG         = 16;
  public final static int     FEE_PRINTER_REPROG      = 17;
  public final static int     FEE_IMPRINTER           = 18;
  public final static int     FEE_ACCOUNT_SERVICING   = 19;


  private final static int[]    cardCodes             = new int[NUM_CARDS];
  private final static int[]    miscCodes             = new int[NUM_MISC];
  private final static String[] questionDesc          = new String[NUM_QUESTIONS];

  private String[]  questionFlag                      = new String[NUM_QUESTIONS];
  private String[]  cardDesc                          = new String[NUM_CARDS];
  private String[]  cardSelectedFlag                  = new String[NUM_CARDS];
  private String[]  cardFee                           = new String[NUM_CARDS];
  private String[]  miscDesc                          = new String[NUM_MISC];
  private String[]  miscSelectedFlag                  = new String[NUM_MISC];
  private String[]  miscFee                           = new String[NUM_MISC];
  private String[]  miscDefaultFee                    = new String[NUM_MISC];
  private String[]  miscCreditAmount                  = new String[NUM_MISC];
  private String[]  miscChargeAmount                  = new String[NUM_MISC];

  private Vector    pricingGrids                      = new Vector();
  private Vector    discPricingGrids                  = new Vector();
  private Vector    discPricingGridsCodes             = new Vector();
  
  private int       appType                           = -1;
  private int       pricingGrid                       = mesConstants.PRICING_GRID_INVALID;
  
  private int       pricingScenario                   = mesConstants.APP_PS_INVALID;
  private int       interchangeType                   = mesConstants.CBT_APP_INTERCHANGE_TYPE_INVALID;
  private String    interchangeTypeFeeRetail          = "";
  private String    interchangeTypeFeeMotoLodging     = "";

  private double    discountRate                      = -1;
  private double    interchangePassthrough            = -1;
  private double    interchangeDiscountRate           = -1;
  private double    discountRate4                     = -1;
  private double    interchangePassthrough4           = -1;
  private boolean   imsSurcharge                      = false;
  private double    cbtDiscountRate5                  = -1;
  private double    cbtNonQual5                       = -1;
  private double    cbtDiscountRate6                  = -1;
  private double    cbtPerItem6                       = -1;
  private double    cbtPerAuth6                       = -1;
  private double    cbtPerItem7                       = -1;
  private double    cbtPerAuth7                       = -1;
  private double    cbtPerCap7                        = -1;
  private double    cbtDiscountRate8                  = -1;
  private double    cbtPerItem8                       = -1;
  private String    visamcFee                         = "-1";
  private String    aruFee                            = "-1";
  private String    voiceFee                          = "-1";
  
  private String    dailyDiscount                     = "";

  //used for discover discount validation
  private double    visaVol                           = -1; 
  private double    aveTic                            = -1;
  //*************************************

  private String    basisPointIncrease                = "";
  private String    uniqueFeeDesc                     = "";  
  private String    posMonthlyFee                     = "-1";
  private String    posMonthlyFeeCredit               = "-1";

  private String    pcchargeMonthlyFee                = "-1";
  private String    pcchargeMonthlyFeeCredit          = "-1";


  private String    minMonthDiscount                  = "-1";
  private boolean   minMonthSelected                  = false;

  private String    minMonthDiscountBankCredit        = "-1";
  private String    minMonthDiscountBankCharge        = "-1";

  private String    monthlyMembershipBankCredit       = "-1";
  private String    monthlyMembershipBankCharge       = "-1";

  private String    thirdPartySoftwareBankCredit      = "-1";
  private String    thirdPartySoftwareBankCharge      = "-1";

  private String    setupKitBankCredit                = "-1";
  private String    setupKitBankCharge                = "-1";

  private String    nameOfBankEquipCredit             = "";
  private boolean   nameOfBankEquipReq                = false;
  private String    nameOfBankMiscCredit              = "";
  private boolean   nameOfBankMiscReq                 = false;

  private String    offGridApprovedBy                 = "";
  private boolean   offGridApproved                   = false;

  private boolean   setupKitSelected                  = false;
  private String    setupKitFee                       = "-1";
  private String    setupKitType                      = "-1";
  private String    setupKitSize                      = "-1";

  private String    referralAuthFee                   = "-1";

  private String    numReprogTerms                    = "";
  private String    numReprogPrints                   = "";
  private String    numImprinters                     = "";

  private boolean   monthlyMembershipSelected         = false;
  private String    monthlyMembershipFee              = "-1";
  private boolean   thirdPartySoftwareSelected        = false;
  private String    thirdPartySoftwareFee             = "-1";

  private String    monthlyDialpayFee                 = ""; //discover apps
  private String    monthlyMultiMerchFee              = "";

  private String    wirelessSetupFee                  = "";
  private String    wirelessMonthlyFee                = "";
  private String    wirelessTransactionFee            = "";

  private String    wirelessSetupFeeFlag              = "";
  private String    wirelessMonthlyFeeFlag            = "";
  private String    wirelessTransactionFeeFlag        = "";

  private String    achRejectFeeFlag                  = "";
  private String    achRejectFee                      = "";

  private String    annualFee                         = "";
  private String    annualFeeFlag                     = "";

  private String    pcQuantity                        = "-1";
  private String    pcFee                             = "-1";
  private String    pcCreditFee                       = "-1";
  private String    pcModel                           = "Unknown Model";
  private String    ownedEquipQty                     = "-1";
  private String    ownedEquipAmt                     = "-1";
  private String    ownedEquipCreditAmt               = "-1";
  private String    ownedEquipChargeAmt               = "-1";
  private String    ownedEquipFlag                    = "N";
  private boolean   equipmentPurchased                = false;
  private ResultSet equipmentInfo                     = null;
  private int       equipmentInfoCount                = 0;

  private String    businessLocation                  = "-1";
  private String    businessLocationComment           = "-1";
  private String    siteInspectionStatus              = "-1";
  private String    businessAddress                   = "-1";
  private String    businessAddressStreet             = "-1";
  private String    businessAddressCity               = "-1";
  private String    businessAddressState              = "-1";
  private String    businessAddressZip                = "-1";
  private String    numEmployees                      = "-1";
  
  private String    houseName                         = "-1";
  private String    houseFlag                         = "-1";
  private String    inventoryValue                    = "-1";
  private String    inventoryAddressStreet            = "-1";
  private String    inventoryAddressCity              = "-1";
  private String    inventoryAddressState             = "-1";
  private String    inventoryAddressZip               = "-1";
  private String    houseStreet                       = "-1";
  private String    houseCity                         = "-1";
  private String    houseState                        = "-1";
  private String    houseZip                          = "-1";
  private String    softwareFlag                      = "-1";
  private String    softwareComment                   = "-1";
  private String    comment                           = "";
  private String    supplyBilling                     = "";
  private String    association                       = "";
  private int       paySolOption                      = -1;
  private double    subtotal                          = 0.0;
  private double    subtotalPur                       = 0.0;
  private double    grandtotal                        = 0.0;
  private double    tax                               = 0.0;
  private double    taxRate                           = 0.0;
  private boolean   recalculate                       = false;
  private boolean   allowSubmit                       = false;
  private boolean   amexZero                          = false;
  private boolean   amexSplit                         = false;
  private boolean   activec                           = false;
  private boolean   internetPOS                       = false;
  private boolean   cybercash                         = false;
  private boolean   verisign                          = false;
  private boolean   wirelessProduct                   = false;
  private boolean   dialPay                           = false;
  public  boolean   virTerm                           = false;
  private boolean   multiMerch                        = false;
  private boolean   seasonalMerch                     = false;
  private boolean   payflowLink                       = false;
  private boolean   payflowPro                        = false;
  private double    verisignOverAuthPerItem           = 0.0;

  /*
  ** Default Contstructor
  */      
  public PricingBean() 
  { 
    super.init();
  }
  
  public void fillDropDowns(long appSeqNum)
  {
    // fill pricing grid vector
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    StringBuffer      qs      = new StringBuffer("");
    try
    {
      qs.setLength(0);
      qs.append("select ic.grid_type,                 ");
      qs.append("       ic.grid_desc                  ");
      qs.append("from   interchange_cost ic,          ");
      qs.append("       application app               ");
      qs.append("where  app.app_seq_num = ? and       ");
      qs.append("       (                             ");
      qs.append("         ic.app_type is null or      ");
      qs.append("         ic.app_type = -1 or         ");
      qs.append("         ic.app_type = app.app_type  ");
      qs.append("       )                             ");
      qs.append("order by grid_type                   ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      pricingGrids.add("Select One");
      while(rs.next())
      {
        pricingGrids.add(rs.getString("grid_desc"));
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "fillDropDowns()" + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
    
    discPricingGrids.add("Select One");
    discPricingGridsCodes.add(Integer.toString( mesConstants.PRICING_GRID_INVALID));
    discPricingGrids.add("Retail");
    discPricingGridsCodes.add(Integer.toString(mesConstants.APP_PG_RETAIL_RESTAURANT));
    discPricingGrids.add("Restaurant");
    discPricingGridsCodes.add(Integer.toString(mesConstants.APP_PG_RESTAURANT));
    discPricingGrids.add("MOTO/Internet");
    discPricingGridsCodes.add(Integer.toString(mesConstants.APP_PG_MOTO_INTERNET));
    discPricingGrids.add("Lodging");
    discPricingGridsCodes.add(Integer.toString(mesConstants.APP_PG_LODGING));

    try
    {
      qs.setLength(0);

      qs.append("select a.pos_code,                 ");
      qs.append("       a.pos_param,                ");
      qs.append("       b.pos_type                  ");
      qs.append("from   merch_pos     a,            ");
      qs.append("       pos_category  b             ");
      qs.append("where  a.app_seq_num = ? and       ");
      qs.append("       a.pos_code    = b.pos_code  ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();

      if (rs.next())
      {
        if(mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK <= rs.getInt("pos_code") &&
           rs.getInt("pos_code") <= mesConstants.APP_MPOS_OTHER)
        {
          this.internetPOS = true;
        }
        
        if(rs.getInt("pos_code") == mesConstants.APP_MPOS_ACTIVE_C_STOREFRONT_BASIC || 
           rs.getInt("pos_code") == mesConstants.APP_MPOS_ACTIVE_C_STOREFRONT_PLUS)
        {
          this.activec = true;
        }
        else if(rs.getInt("pos_code") == mesConstants.APP_MPOS_CYBERCASH)
        {
          this.cybercash = true;
        }
        else if(rs.getInt("pos_code") == mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK || 
                rs.getInt("pos_code") == mesConstants.APP_MPOS_VERISIGN_PAYFLOW_PRO)
        {
          this.verisign = true;
        }

        if(rs.getInt("pos_type") == mesConstants.POS_WIRELESS_TERMINAL)
        {
          this.wirelessProduct = true;
        }
        else if(rs.getInt("pos_type") == mesConstants.POS_DIAL_AUTH)
        {
          this.dialPay = true;
        }
        else if(rs.getInt("pos_type") == mesConstants.POS_VIRTUAL_TERMINAL)
        {
          this.virTerm = true;
        }
      
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      addError("getMerchPosData: " + e.toString()); com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMerchPosData: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }

    for(int i=0; i<this.NUM_CARDS; i++)
    {
      cardSelectedFlag[i] = "N";
      cardFee[i]          = "-1";
      switch(i)
      {
        case CARD_DINERS:
          cardDesc[i]  = "Diners Club/Carte Blanche";
          cardCodes[i] = mesConstants.APP_CT_DINERS_CLUB;
          break;
        case CARD_DISCOVER:
          cardDesc[i]  = "Discover/Novus";
          cardCodes[i] = mesConstants.APP_CT_DISCOVER;
          break;
        case CARD_JCB: 
          cardDesc[i]  = "JCB";
          cardCodes[i] = mesConstants.APP_CT_JCB;
          break;
        case CARD_AMEX:
          cardDesc[i]  = "American Express";
          cardCodes[i] = mesConstants.APP_CT_AMEX;
          break;
        case CARD_DEBIT:
          cardDesc[i]  = "Debit Card";
          cardCodes[i] = mesConstants.APP_CT_DEBIT;
          break;
        case CARD_CHECK:
          cardDesc[i]  = "Check Verification";
          cardCodes[i] = mesConstants.APP_CT_CHECK_AUTH;
          break;
        case CARD_INTERNET:
          cardDesc[i]  = "Internet Transaction";
          cardCodes[i] = mesConstants.APP_CT_INTERNET;
          break;
        case CARD_DIALPAY:
          cardDesc[i]  = "Dial Pay Transaction";
          cardCodes[i] = mesConstants.APP_CT_DIAL_PAY;
          break;
        case CARD_EBT:
          cardDesc[i]  = "EBT";
          cardCodes[i] = mesConstants.APP_CT_EBT;
          break;
        case CARD_VMC:
          cardDesc[i] = "V/MC Authorization";
          cardCodes[i] = mesConstants.APP_CT_VISA;
          break;
        default:
          break;
      }
    }

    for(int i=0; i<this.NUM_MISC; i++)
    {
      miscSelectedFlag[i] = "N";
      miscFee[i]          = "-1"; 
      miscCreditAmount[i] = "-1";
      miscChargeAmount[i] = "-1";
      
      switch(i)
      {
        case FEE_CHARGEBACK:
          miscDesc[i]       = "Chargeback Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_CHARGEBACK;
          miscDefaultFee[i] = "-1";
          break;
        case FEE_INTERNET_MONTHLY:
          miscDesc[i]       = "Internet Service Monthly Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE;
          miscDefaultFee[i] = "-1";
          break;
        case FEE_HELPDESK:
          miscDesc[i]       = "Help Desk Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_HELP_DESK;
          miscDefaultFee[i] = "-1";
          break;
        case FEE_STATEMENT:
          miscDesc[i]       = "Monthly Service Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT;
          miscDefaultFee[i] = "-1";
          break;
        case FEE_WEB_REPORTING:
          miscDesc[i]       = "Web Reporting Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_WEB_REPORTING;
          miscDefaultFee[i] = "-1";
          break;
        case FEE_NEW_APP:
          miscDesc[i]       = "New Account Application Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_APP;
          miscDefaultFee[i] = "-1";
          break;
        case FEE_ACCOUNT_SETUP:
          miscDesc[i]       = "New Account Setup Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_SETUP;
          miscDefaultFee[i] = "-1";
          break;
        case FEE_INTERNET_SETUP: 
          miscDesc[i]       = "Internet Service Setup Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP;
          miscDefaultFee[i] = "-1";
          break;
        case FEE_VT_SETUP:
          miscDesc[i]       = "Trident Virtual Terminal Setup Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_VT_SETUP_FEE;
          miscDefaultFee[i] = "-1";
          break;
        case FEE_VT_MONTHLY:
          miscDesc[i]       = "Trdent Virtual Terminal Monthly Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_VT_MONTHLY_FEE;
          miscDefaultFee[i] = "-1";
          break;
        case FEE_MEMBERSHIP:
          miscDesc[i]       = "Annual Membership Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_MEMBERSHIP_FEES;
          miscDefaultFee[i] = "-1";
          break;
        case FEE_RUSH:
          miscDesc[i]       = "Rush Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_RUSH_FEE;
          miscDefaultFee[i] = "-1";
          break;
        case FEE_UNIQUE:
          miscDesc[i]       = "Unique Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_UNIQUE_FEE;
          miscDefaultFee[i] = "-1";
          break;
        case FEE_GATEWAY_MONTHLY:
          miscDesc[i]       = "Internet Monthly Gateway Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_MONTHLY_GATEWAY;
          miscDefaultFee[i] = "-1";
          break;
        case FEE_GATEWAY_SETUP:
          miscDesc[i]       = "Internet Gateway Setup Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_GATEWAY_SETUP;
          miscDefaultFee[i] = "-1";
          break;

        case FEE_TERM_REPROG:
          miscDesc[i]       = "Terminal Reprogramming Fee - One Time Charge";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_TERM_REPROG_FEE;
          miscDefaultFee[i] = "-1";
          break;

        case FEE_PRINTER_REPROG:
          miscDesc[i]       = "Printer Reprogramming Fee - One Time Charge";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_PRINTER_REPROG_FEE;
          miscDefaultFee[i] = "-1";
          break;

        case FEE_IMPRINTER:
          miscDesc[i]       = "Imprinter Fee - One Time Charge";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_IMPRINTER;
          miscDefaultFee[i] = "-1";
          break;

        case FEE_ACCOUNT_SERVICING:
          miscDesc[i]       = "Account Servicing Monthly Fee";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_ACCOUNT_SERVICING;
          miscDefaultFee[i] = "-1";
          break;
          
        case FEE_PCI_ADMIN:
          miscDesc[i]       = "Annual PCI Admin Fee (billed in November)";
          miscCodes[i]      = mesConstants.APP_MISC_CHARGE_PCI_COMPLIANCE;
          miscDefaultFee[i] = "-1";
          break;

        default:
          break;
      }
    }
    for(int i=0; i<this.NUM_QUESTIONS; i++)
    {
      questionFlag[i]     = "-1";
      switch(i)
      {
        case 0:
          questionDesc[i]     = "Did name posted at business match business name on application?";
          break;
        case 1:
          questionDesc[i]     = "Did location appear to have appropriate signage?";
          break;
        case 2: 
          questionDesc[i]     = "Were business hours posted?";
          break;
        case 3:
          questionDesc[i]     = "Was merchant's inventory viewed?";
          break;
        case 4:
          questionDesc[i]     = "Was inventory consistent with merchant's type of business?";
          break;
        case 5:
          questionDesc[i]     = "Did inventory appear to be adequate to support the sales volume indicated on the application?";
          break;
        default:
          break;
      }
    }
    
    try
    {
      qs.setLength(0);
     
      qs.append("select pos_code                      ");
      qs.append("from   merch_pos                     ");
      qs.append("where  app_seq_num = ?               ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        if(rs.getInt("pos_code") == mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK)
        {
          payflowLink = true;
        }
        else if(rs.getInt("pos_code") == mesConstants.APP_MPOS_VERISIGN_PAYFLOW_PRO)
        {
          payflowPro = true;
        }
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "fillDropDowns() - payflowLink" + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }
  
  public void getData(long appSeqNum)
  {
    getApplicationType(appSeqNum);

    if(this.appType == mesConstants.APP_TYPE_SVB)
    {
      // default verisign over auths to 0.07
      verisignOverAuthPerItem = 0.07;
    }
    getGrid(appSeqNum);
    getPaySolOption(appSeqNum);
    getCardInfo(appSeqNum);
    getPricingRate(appSeqNum);
    getPcInfo(appSeqNum);
    getOwnedEquipmentInfo(appSeqNum);
    getMiscCharges(appSeqNum);
    getSiteInspectionInfo(appSeqNum);
    getAmexZero(appSeqNum);
    getBasisPointInfo(appSeqNum);
  }

  private void getBasisPointInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select MERCH_BASIS_PT_INCREASE,  ");
      qs.append("       OFF_GRID_APPROVED_BY,     ");
      qs.append("       EQUIP_CREDIT_BANK_NAME,   ");
      qs.append("       MISC_CREDIT_BANK_NAME,    ");
      qs.append("       ASSO_NUMBER,              ");
      qs.append("       MERCH_DLY_DISCOUNT_FLAG,  ");
      qs.append("       MULTI_MERCH_MASTER_CNTRL, ");
      qs.append("       seasonal_flag             ");
      qs.append("from   merchant                  ");
      qs.append("where  app_seq_num = ?           ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      rs = ps.executeQuery();

      if(rs.next())
      {
        basisPointIncrease    = isBlank(rs.getString("MERCH_BASIS_PT_INCREASE"))  ? "" : rs.getString("MERCH_BASIS_PT_INCREASE");
        offGridApprovedBy     = isBlank(rs.getString("OFF_GRID_APPROVED_BY"))     ? "" : rs.getString("OFF_GRID_APPROVED_BY");
        nameOfBankEquipCredit = isBlank(rs.getString("EQUIP_CREDIT_BANK_NAME"))   ? "" : rs.getString("EQUIP_CREDIT_BANK_NAME");
        nameOfBankMiscCredit  = isBlank(rs.getString("MISC_CREDIT_BANK_NAME"))    ? "" : rs.getString("MISC_CREDIT_BANK_NAME");
        association           = isBlank(rs.getString("ASSO_NUMBER"))              ? "" : rs.getString("ASSO_NUMBER");
        dailyDiscount         = isBlank(rs.getString("MERCH_DLY_DISCOUNT_FLAG"))  ? "" : rs.getString("MERCH_DLY_DISCOUNT_FLAG");

        //if blank is not multi merch
        multiMerch            = !isBlank(rs.getString("MULTI_MERCH_MASTER_CNTRL"));

        seasonalMerch         = (!isBlank(rs.getString("seasonal_flag")) && rs.getString("seasonal_flag").equals("Y"));

        if(!isBlank(offGridApprovedBy))
        {
          offGridApproved = true;
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getBasisPointInfo: " + e.toString());
      addError("getBasisPointInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void setSalesInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select                       ");
      qs.append("MERCH_MONTH_VISA_MC_SALES,   ");
      qs.append("MERCH_AVERAGE_CC_TRAN        ");
      qs.append("from   merchant              ");
      qs.append("where  app_seq_num = ?       ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      rs = ps.executeQuery();

      if(rs.next())
      {
        visaVol = isBlank(rs.getString("MERCH_MONTH_VISA_MC_SALES"))  ? -1 : rs.getDouble("MERCH_MONTH_VISA_MC_SALES");
        aveTic  = isBlank(rs.getString("MERCH_AVERAGE_CC_TRAN"))      ? -1 : rs.getDouble("MERCH_AVERAGE_CC_TRAN");
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setSalesInfo: " + e.toString());
      addError("setSalesInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void getAmexZero(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select merchpo_split_dial,   ");
      qs.append("merchpo_pip                  ");
      qs.append("from   merchpayoption        ");
      qs.append("where  app_seq_num = ? and   ");
      qs.append("       cardtype_code = ?     ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1, appSeqNum);
      ps.setInt(2, mesConstants.APP_CT_AMEX);

      rs = ps.executeQuery();

      if(rs.next())
      {
        if(!isBlank(rs.getString("merchpo_split_dial")) && (rs.getString("merchpo_split_dial")).equals("Y"))
        {
          amexZero  = true;          
          amexSplit = true;
        }
        else if(!isBlank(rs.getString("merchpo_pip")) && (rs.getString("merchpo_pip")).equals("Y"))
        {
          amexZero = true;
        }
        else
        {
          amexZero = false;
        }
      }
      else
      {
        amexZero = false;
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getSplitDial: " + e.toString());
      addError("getSplitDial: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }
  
  private String getEbtFee(long appSeqNum)  
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    String            result  = "-1";

    try
    {
      qs.append("select tranchrg_per_tran from tranchrg where ");
      qs.append("app_seq_num = ? and cardtype_code = ? ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setInt(2, mesConstants.APP_CT_EBT);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        result = isBlank(rs.getString("tranchrg_per_tran")) ? "-1" : rs.getString("tranchrg_per_tran");
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      result = "-1";
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }

    return result;
  }
  
  /**
   *  This method takes the sequence no. as the argument and is used
   *  to get all the details of the card types selected by the merchant
   *  on the Payment Options page.
  */
  private void getCardInfo(long appSeqNum)  
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    PreparedStatement ps1     = null;
    ResultSet         rs      = null;
    ResultSet         rs1     = null;
    int               i       = 0;
    
    try 
    {
      qs.append("select cardtype_code,              ");
      qs.append("merchpo_card_merch_number          ");
      qs.append("from merchpayoption                ");
      qs.append("where app_seq_num = ?              ");
      qs.append("and cardtype_code not in (4,19)  ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();
      
      qs.setLength(0);
      qs.append("select tranchrg_per_tran, ");
      qs.append("nvl(tranchrg_per_auth, 0.0) tranchrg_per_auth ");
      qs.append("from tranchrg where      ");
      qs.append("app_seq_num = ? and      ");
      qs.append("cardtype_code = ?        ");

      while(rs.next())
      {
        ps1 = getPreparedStatement(qs.toString());
        //ps1.clearParameters();
        ps1.setLong(1, appSeqNum);
        ps1.setInt(2, rs.getInt("cardtype_code"));
      
        rs1 = ps1.executeQuery();

        //find which element to put info into
        i=0;
        while(i < this.NUM_CARDS && rs.getInt("cardtype_code") != this.cardCodes[i])
        {
          i+=1;
        }

        if(rs1.next() && i < this.NUM_CARDS)
        {
          this.cardFee[i]           = rs1.getString("tranchrg_per_tran");
          this.cardSelectedFlag[i]  = "Y";
        }
        else if(i < this.NUM_CARDS)
        {
          this.cardFee[i]           = "-1";
          this.cardSelectedFlag[i]  = "Y";
        }

        rs1.close();
        ps1.close();
      }
      
      rs.close();
      ps.close();
      
      if(this.paySolOption == 2) //Internet Transaction
      {
        this.cardSelectedFlag[CARD_INTERNET] = "Y";
        ps1 = getPreparedStatement(qs.toString());
        ps1.setLong(1, appSeqNum);
        ps1.setInt(2, mesConstants.APP_CT_INTERNET);
        rs1 = ps1.executeQuery();
        if(rs1.next())
        {
          this.cardFee[CARD_INTERNET] = rs1.getString("tranchrg_per_tran");
          this.verisignOverAuthPerItem = rs1.getDouble("tranchrg_per_auth");
        }
        rs1.close();
        ps1.close();
      }
      else if(this.paySolOption == 4) //DialPay Transaction
      {
        ps1 = getPreparedStatement(qs.toString());
        this.cardSelectedFlag[CARD_DIALPAY] = "Y";
        ps1.setLong(1, appSeqNum);
        ps1.setInt(2, mesConstants.APP_CT_DIAL_PAY);
        rs1 = ps1.executeQuery();
        if(rs1.next())
        {
          this.cardFee[CARD_DIALPAY] = rs1.getString("tranchrg_per_tran");
        }
        rs1.close();
        ps1.close();
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCardInfo: " + e.toString());
      addError("getCardInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
      try { rs1.close(); } catch(Exception e) {}
      try { ps1.close(); } catch(Exception e) {}
    }
  }

  /**
   *  This method takes the sequence no. as the argument and is used
   *  to get all the details of the equipment which he is wants to
   *  purchase or rent.
   *  It also get's quantity entered by the user and the default
   *  price for each equipment.
   */
  public ResultSet getEquipmentInfo(long appSeqNum) 
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    super.init();
    
    try 
    {
      qs.append("select                                                                     ");
      qs.append("equipment.equip_model,                                                     ");
      qs.append("equipment.equip_descriptor,                                                ");
      qs.append("equiplendtype.equiplendtype_code,                                          ");
      qs.append("equiplendtype_description,                                                 ");
      qs.append("merchequip_equip_quantity,                                                 ");
      qs.append("merchequipment.prod_option_id,                                             "); 
      qs.append("prodoption.prod_option_des,                                                ");
      qs.append("DECODE(equiplendtype.equiplendtype_code,1,equipment.equip_price,2,equipment.equip_rent,0) Amount ");
      qs.append("from                                                                       ");
      qs.append("merchequipment,                                                            ");
      qs.append("equipment,                                                                 ");
      qs.append("equiplendtype,                                                             ");
      qs.append("prodoption                                                                 ");
      qs.append("where                                                                      ");
      qs.append("app_seq_num = ?                                                            ");
      qs.append("and merchequipment.equip_model         = equipment.equip_model             ");
      qs.append("and merchequipment.prod_option_id      = prodoption.prod_option_id(+)      ");
      qs.append("and merchequipment.equiplendtype_code  = equiplendtype.equiplendtype_code  ");
      qs.append("and equiplendtype.equiplendtype_code in (?,?,?,?)                          ");
      qs.append("order by                                                                   ");
      qs.append("equiplendtype_code,                                                        ");
      qs.append("equipment.equiptype_code                                                   ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setInt(2, mesConstants.APP_EQUIP_PURCHASE);
      ps.setInt(3, mesConstants.APP_EQUIP_RENT);
      ps.setInt(4, mesConstants.APP_EQUIP_BUY_REFURBISHED);
      ps.setInt(5, mesConstants.APP_EQUIP_LEASE);

      this.equipmentInfo = ps.executeQuery();
    }
    catch(Exception e)
    {
      this.equipmentInfo = null;
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getEquipmentInfo: " + e.toString());
      addError("getEquipmentInfo: " + e.toString());
    }
    return this.equipmentInfo;
  }
  
  private void getPcInfo(long appSeqNum)
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      StringBuffer      qs      = new StringBuffer("");
          
      qs.append("select merchequip_equip_quantity,  ");
      qs.append("merchequip_amount,                 ");
      qs.append("merchequip_credit_amount           ");
      qs.append("from merchequipment                ");
      qs.append("where app_seq_num = ? and          ");
      qs.append("equip_model = ?                    ");
                    
      ps = getPreparedStatement(qs.toString());
         
      ps.setLong(1, appSeqNum);
      ps.setString(2, "PCPS");
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.pcFee        = isBlank(rs.getString("merchequip_amount"))        ? "-1" : rs.getString("merchequip_amount");
        this.pcCreditFee  = isBlank(rs.getString("merchequip_credit_amount")) ? "-1" : rs.getString("merchequip_credit_amount");
        this.pcQuantity   = rs.getString("merchequip_equip_quantity");
      }
      else
      {
        pcQuantity  = "-1";
        pcFee       = "-1";
        pcCreditFee = "-1";
      }
      rs.close();
      ps.close();
      
      qs.setLength(0);
      qs.append("select pos_desc              ");
      qs.append("from pos_category a,         ");
      qs.append("merch_pos b                  ");
      qs.append("where b.app_seq_num = ? and  ");
      qs.append("a.pos_code = b.pos_code      ");
      
      ps = getPreparedStatement(qs.toString());
         
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.pcModel = rs.getString("pos_desc");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPcInfo: " + e.toString());
      addError("getPcInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  /**
   *  This method takes the sequence no. as the argument and is used
   *  to get all the details of the equipment which he is wants to
   *  purchase or rent.
   *  It also get's quantity entered by the user and the default
   *  price for each equipment.
   */
  private void getMiscCharges(long appSeqNum) 
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    try 
    {
      qs.append("select misc_chrg_amount,         ");
      qs.append("       misc_bank_credit_amount,  ");
      qs.append("       misc_bank_chrg_amount     ");
      qs.append("from   miscchrg                  ");
      qs.append("where  app_seq_num = ? and       ");
      qs.append("       misc_code = ?             ");
     
      ps = getPreparedStatement(qs.toString());
      
      for(int i=0; i<this.NUM_MISC; i++)
      {
        ps = getPreparedStatement(qs.toString());
        ps.setLong(1,appSeqNum);
        ps.setInt(2,this.miscCodes[i]);
        
        rs = ps.executeQuery();
        
        if(rs.next())
        {
          this.miscFee[i]           = rs.getString("misc_chrg_amount");
          this.miscSelectedFlag[i]  = "Y";
        
          this.miscCreditAmount[i]  = isBlank(rs.getString("misc_bank_credit_amount"))  ? "-1" : rs.getString("misc_bank_credit_amount");
          this.miscChargeAmount[i]  = isBlank(rs.getString("misc_bank_chrg_amount"))    ? "-1" : rs.getString("misc_bank_chrg_amount");
        }
        else
        {
          this.miscFee[i] = this.miscDefaultFee[i];
        }
        
        rs.close();
        ps.close();
      }
      
      //get pos montly fee if it exists
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_MISC_CHARGE_POS_MONTHLY_SERVICE_FEE);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.posMonthlyFee        = isBlank(rs.getString("misc_chrg_amount"))         ? "-1" : rs.getString("misc_chrg_amount");
        this.posMonthlyFeeCredit  = isBlank(rs.getString("misc_bank_credit_amount"))  ? "-1" : rs.getString("misc_bank_credit_amount");
      }
      rs.close();
      ps.close();

      //get pc charge montly fee if it exists
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_MISC_CHARGE_PCCHARGE_MONTHLY_SERVICE_FEE);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.pcchargeMonthlyFee        = isBlank(rs.getString("misc_chrg_amount"))         ? "-1" : rs.getString("misc_chrg_amount");
        this.pcchargeMonthlyFeeCredit  = isBlank(rs.getString("misc_bank_credit_amount"))  ? "-1" : rs.getString("misc_bank_credit_amount");
      }
      rs.close();
      ps.close();

      //get setup kit fee if it exists
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_MISC_CHARGE_SETUP_KIT_FEE);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.setupKitFee        = isBlank(rs.getString("misc_chrg_amount"))         ? "-1" : rs.getString("misc_chrg_amount");
        this.setupKitBankCredit = isBlank(rs.getString("misc_bank_credit_amount"))  ? "-1" : rs.getString("misc_bank_credit_amount");
        this.setupKitBankCharge = isBlank(rs.getString("misc_bank_chrg_amount"))    ? "-1" : rs.getString("misc_bank_chrg_amount");
        this.setupKitSelected   = true;
      }
      rs.close();
      ps.close();
      
      //get setup kit fee if it exists
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_MISC_CHARGE_REFERRAL_AUTH_FEE);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.referralAuthFee        = isBlank(rs.getString("misc_chrg_amount"))         ? "-1" : rs.getString("misc_chrg_amount");
      }
      rs.close();
      ps.close();

      //get monthly membership fee if it exists
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_MISC_CHARGE_MONTHLY_MEMBERSHIP_FEE);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.monthlyMembershipFee         = isBlank(rs.getString("misc_chrg_amount"))         ? "-1" : rs.getString("misc_chrg_amount");
        this.monthlyMembershipBankCredit  = isBlank(rs.getString("misc_bank_credit_amount"))  ? "-1" : rs.getString("misc_bank_credit_amount");
        this.monthlyMembershipBankCharge  = isBlank(rs.getString("misc_bank_chrg_amount"))    ? "-1" : rs.getString("misc_bank_chrg_amount");
        this.monthlyMembershipSelected    = true;
      }
      rs.close();
      ps.close();

      //get third party software service fee if it exists
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_MISC_CHARGE_THIRD_PARTY_SOFTWARE_FEE);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.thirdPartySoftwareFee          = isBlank(rs.getString("misc_chrg_amount"))         ? "-1" : rs.getString("misc_chrg_amount");
        this.thirdPartySoftwareBankCredit   = isBlank(rs.getString("misc_bank_credit_amount"))  ? "-1" : rs.getString("misc_bank_credit_amount");
        this.thirdPartySoftwareBankCharge   = isBlank(rs.getString("misc_bank_chrg_amount"))    ? "-1" : rs.getString("misc_bank_chrg_amount");
        this.thirdPartySoftwareSelected     = true;
      }
      rs.close();
      ps.close();

      //get wireless setup fee
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_MISC_CHARGE_WIRELESS_SETUP_FEE);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.wirelessSetupFee          = isBlank(rs.getString("misc_chrg_amount"))         ? "-1" : rs.getString("misc_chrg_amount");
        this.wirelessSetupFeeFlag      = "Y";
      }
      rs.close();
      ps.close();

      //get wireless monthly fee
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_MISC_CHARGE_WIRELESS_MONTHLY_FEE);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.wirelessMonthlyFee          = isBlank(rs.getString("misc_chrg_amount"))         ? "-1" : rs.getString("misc_chrg_amount");
        this.wirelessMonthlyFeeFlag      = "Y";
      }
      rs.close();
      ps.close();

      //get wireless transaction fee
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_MISC_CHARGE_WIRELESS_TRANSACTION_FEE);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.wirelessTransactionFee          = isBlank(rs.getString("misc_chrg_amount"))         ? "-1" : rs.getString("misc_chrg_amount");
        this.wirelessTransactionFeeFlag      = "Y";
      }
      rs.close();
      ps.close();

      //get annual fee
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_MISC_CHARGE_ANNUAL_FEE);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.annualFee          = isBlank(rs.getString("misc_chrg_amount"))         ? "-1" : rs.getString("misc_chrg_amount");
        this.annualFeeFlag      = "Y";
      }
      rs.close();
      ps.close();

      //get ach reject fee
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      ps.setInt(2,mesConstants.APP_MISC_CHARGE_ACH_REJECT_FEE);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.achRejectFee          = isBlank(rs.getString("misc_chrg_amount"))         ? "-1" : rs.getString("misc_chrg_amount");
        this.achRejectFeeFlag      = "Y";
      }
      rs.close();
      ps.close();

    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMiscCharges: " + e.toString());
      addError("getMiscCharges: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }


  /**
   *  This method takes the sequence no. as the argument and is used
   *  to get the discount rate and the interchange passthru by using
   *  the matrix provided by MES.
   */

  private void getPricingRate(long appSeqNum) 
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try 
    {
      qs.setLength(0);
      qs.append("select tranchrg_disc_rate,     ");
      qs.append("tranchrg_mmin_chrg,            ");
      qs.append("tranchrg_mmin_bank_credit,     ");
      qs.append("tranchrg_mmin_bank_charge,     ");
      qs.append("tranchrg_per_tran,             ");
      qs.append("tranchrg_pass_thru,            ");
      qs.append("tranchrg_discrate_type,        ");
      qs.append("tranchrg_per_auth,             ");
      qs.append("tranchrg_per_capture,          ");
      qs.append("non_qualification_downgrade,   ");
      qs.append("tranchrg_aru_fee,              ");
      qs.append("tranchrg_voice_fee,            ");
      qs.append("tranchrg_interchangefee_type,  ");
      qs.append("tranchrg_interchangefee_fee    ");
      qs.append("from tranchrg                  ");
      qs.append("where app_seq_num = ?          ");
      qs.append("and cardtype_code = ?          ");
      
      ps = getPreparedStatement(qs.toString());
      ps.clearParameters();
      
      ps.setLong(1, appSeqNum);
      ps.setInt(2,  mesConstants.APP_CT_VISA);
      
      rs = ps.executeQuery();
      
      if (rs.next()) 
      {
        this.pricingScenario = rs.getInt("tranchrg_discrate_type");

        switch(this.pricingScenario)
        {
          case mesConstants.APP_PS_VARIABLE_RATE:
          case mesConstants.APP_PS_FIXED_RATE:
          case mesConstants.APP_PS_INTERCHANGE:
            if(!isBlank(rs.getString("tranchrg_pass_thru")))
            {
              this.interchangePassthrough = rs.getDouble("tranchrg_pass_thru");
            }

            if(!isBlank(rs.getString("tranchrg_disc_rate")))
            {
              if(this.pricingScenario == mesConstants.APP_PS_INTERCHANGE)
              {
                this.interchangeDiscountRate = rs.getDouble("tranchrg_disc_rate");
              }
              else
              {
                this.discountRate  = rs.getDouble("tranchrg_disc_rate");
              }
            }

            this.visamcFee        = isBlank(rs.getString("tranchrg_per_auth"))  ? "-1" : rs.getString("tranchrg_per_auth");

            //usually has discount rate type of 2, we force it to disc rate type 4 fields.. for viewing purposes on app.
            if(appType == mesConstants.APP_TYPE_VERISIGN || appType == mesConstants.APP_TYPE_NSI)
            {
              if(!isBlank(rs.getString("tranchrg_pass_thru")))
              {
                this.interchangePassthrough4 = rs.getDouble("tranchrg_pass_thru");
              }
              
              if(!isBlank(rs.getString("tranchrg_disc_rate")))
              {
                this.discountRate4  = rs.getDouble("tranchrg_disc_rate");
              }
            }

            this.minMonthDiscount = isBlank(rs.getString("tranchrg_mmin_chrg")) ? "-1" : rs.getString("tranchrg_mmin_chrg");
            
            if(!this.minMonthDiscount.equals("-1"))
            {
              this.minMonthSelected = true;
            }

            break;

          case mesConstants.APP_PS_FIXED_PLUS_PER_ITEM:
          case mesConstants.IMS_MOTO_INTERNET_DIALPAY_PLAN:
          case mesConstants.IMS_RETAIL_RESTAURANT_PLAN:
            
            if(!isBlank(rs.getString("tranchrg_pass_thru")))
            {
              this.interchangePassthrough4 = rs.getDouble("tranchrg_pass_thru");
              imsSurcharge = true;
            }

            this.visamcFee        = isBlank(rs.getString("tranchrg_per_auth"))  ? "-1" : rs.getString("tranchrg_per_auth");
            
            if(!isBlank(rs.getString("tranchrg_disc_rate")))
            {
              this.discountRate4  = rs.getDouble("tranchrg_disc_rate");
            }
            
            this.minMonthDiscount = isBlank(rs.getString("tranchrg_mmin_chrg")) ? "-1" : rs.getString("tranchrg_mmin_chrg");
            
            if(!this.minMonthDiscount.equals("-1"))
            {
              this.minMonthSelected = true;
            }
            
            break;

          case mesConstants.CBT_APP_BUNDLED_RATE_PLAN:
            this.cbtDiscountRate5 = rs.getDouble("tranchrg_disc_rate");
            this.cbtNonQual5      = rs.getDouble("non_qualification_downgrade");
            this.minMonthDiscount = isBlank(rs.getString("tranchrg_mmin_chrg")) ? "-1" : rs.getString("tranchrg_mmin_chrg");
            if(!this.minMonthDiscount.equals("-1"))
            {
              this.minMonthSelected = true;
            }
            break;

          case mesConstants.CBT_APP_UNBUNDLED_RATE_PLAN:
            this.cbtDiscountRate6 = rs.getDouble("tranchrg_disc_rate");
            this.cbtPerItem6      = rs.getDouble("tranchrg_per_tran");
            this.cbtPerAuth6      = rs.getDouble("tranchrg_per_auth");
            this.minMonthDiscount = isBlank(rs.getString("tranchrg_mmin_chrg")) ? "-1" : rs.getString("tranchrg_mmin_chrg");
            if(!this.minMonthDiscount.equals("-1"))
            {
              this.minMonthSelected = true;
            }
            break;          

          case mesConstants.CBT_APP_INTERCHANGE_PLUS_PLAN:
            this.cbtPerItem7      = rs.getDouble("tranchrg_per_tran");
            this.cbtPerAuth7      = rs.getDouble("tranchrg_per_auth");
            this.cbtPerCap7       = rs.getDouble("tranchrg_per_capture");
            this.minMonthDiscount = isBlank(rs.getString("tranchrg_mmin_chrg")) ? "-1" : rs.getString("tranchrg_mmin_chrg");
            if(!this.minMonthDiscount.equals("-1"))
            {
              this.minMonthSelected = true;
            }
            break;

          case mesConstants.CBT_APP_PERITEM_DISCRATE_PLAN:
            this.cbtDiscountRate8 = rs.getDouble("tranchrg_disc_rate");
            this.cbtPerItem8      = rs.getDouble("tranchrg_per_tran");
            
            this.visamcFee        = isBlank(rs.getString("tranchrg_per_auth"))  ? "-1" : rs.getString("tranchrg_per_auth");
            this.aruFee           = isBlank(rs.getString("tranchrg_aru_fee"))   ? "-1" : rs.getString("tranchrg_aru_fee");
            this.voiceFee         = isBlank(rs.getString("tranchrg_voice_fee")) ? "-1" : rs.getString("tranchrg_voice_fee");
            this.minMonthDiscountBankCredit = isBlank(rs.getString("tranchrg_mmin_bank_credit")) ? "-1" : rs.getString("tranchrg_mmin_bank_credit");
            this.minMonthDiscountBankCharge = isBlank(rs.getString("tranchrg_mmin_bank_charge")) ? "-1" : rs.getString("tranchrg_mmin_bank_charge");

            if(!isBlank(rs.getString("tranchrg_interchangefee_type")))
            {
              this.interchangeType    = rs.getInt("tranchrg_interchangefee_type");
             
              if(this.interchangeType == mesConstants.CBT_APP_INTERCHANGE_TYPE_RETAIL)
              {
                this.interchangeTypeFeeRetail = isBlank(rs.getString("tranchrg_interchangefee_fee")) ? "" : rs.getString("tranchrg_interchangefee_fee");
              }
              else if(this.interchangeType == mesConstants.CBT_APP_INTERCHANGE_TYPE_MOTO_LODGING)
              {
                interchangeTypeFeeMotoLodging = isBlank(rs.getString("tranchrg_interchangefee_fee")) ? "" : rs.getString("tranchrg_interchangefee_fee");
              }
            }

            this.minMonthDiscount = isBlank(rs.getString("tranchrg_mmin_chrg")) ? "-1" : rs.getString("tranchrg_mmin_chrg");

            if(!this.minMonthDiscount.equals("-1"))
            {
              this.minMonthSelected = true;
            }
          
            break;          
          
          default:
            break;
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPricingRate: " + e.toString());
      addError("getPricingRate: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }


  /**
   *  This method takes the sequence no. as the argument and is used
   *  to get all the details of the equipment which he already owns.
   *  It also get's quantity entered by the user and the default
   *  price for each equipment.
   */
  private void getOwnedEquipmentInfo(long appSeqNum) 
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try 
    {
      qs.append("select sum(merchequip_equip_quantity)  ");
      qs.append("from merchequipment                    ");
      qs.append("where app_seq_num = ? and              ");
      qs.append("equiplendtype_code = ? and             ");
      qs.append("equiptype_code != ?                    ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setInt(2, mesConstants.APP_EQUIP_OWNED);
      ps.setInt(3, mesConstants.APP_EQUIP_TYPE_IMPRINTER);

      rs = ps.executeQuery();
     
      if (rs.next() && !isBlank(rs.getString(1))) 
      {
        this.ownedEquipQty  = rs.getString(1);

        if(this.paySolOption == mesConstants.APP_PAYSOL_DIAL_TERMINAL || 
           this.paySolOption == mesConstants.APP_PAYSOL_DIAL_PAY      ||
           this.paySolOption == mesConstants.APP_PAYSOL_WIRELESS_TERMINALS)
        {
          this.ownedEquipFlag = "Y";
        }
        else
        {
          this.ownedEquipFlag = "N";
        }

        rs.close();
        ps.close();

        qs.setLength(0);
        qs.append("select                     ");
        qs.append("merchequip_amount,         ");
        qs.append("merchequip_credit_amount,  ");
        qs.append("merchequip_charge_amount   ");
        qs.append("from merchequipment        ");
        qs.append("where app_seq_num = ? and  ");
        qs.append("equiplendtype_code = ?     ");
      
        ps = getPreparedStatement(qs.toString());
        ps.setLong(1, appSeqNum);
        ps.setInt(2, mesConstants.APP_EQUIP_OWNED);

        rs = ps.executeQuery();
     
        if (rs.next()) 
        {  
          this.ownedEquipAmt        = rs.getString("merchequip_amount");
          this.ownedEquipCreditAmt  = isBlank(rs.getString("merchequip_credit_amount")) ? "" : rs.getString("merchequip_credit_amount");
          this.ownedEquipChargeAmt  = isBlank(rs.getString("merchequip_charge_amount")) ? "" : rs.getString("merchequip_charge_amount");
        }
        else
        {
          this.ownedEquipAmt        = "-1";
          this.ownedEquipCreditAmt  = "-1";
          this.ownedEquipChargeAmt  = "-1";
        }
      }
      else
      {
        this.ownedEquipFlag = "N";
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getOwnedEquipmentInfo: " + e.toString());
      addError("getOwnedEquipmentInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }// End of method.
  
  public void getGrid(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select pricing_grid    ");
      qs.append("from merchant          ");
      qs.append("where app_seq_num = ?  ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.pricingGrid = rs.getInt("pricing_grid");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getGrid: " + e.toString());
      addError("getGrid: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }
  
  public void getApplicationType(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select app_type        ");
      qs.append("from application       ");
      qs.append("where app_seq_num = ?  ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.appType = rs.getInt("app_type");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAppType: " + e.toString());
      addError("getAppType: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }
 
  private void getPaySolOption(long appSeqNum) 
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select pos_type              ");
      qs.append("from merch_pos a,            ");
      qs.append("pos_category b               ");
      qs.append("where a.app_seq_num = ? and  ");
      qs.append("a.pos_code = b.pos_code      ");

      ps = getPreparedStatement(qs.toString());

      ps.clearParameters();
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();

      while(rs.next())
      {
        this.paySolOption = rs.getInt("pos_type");
      }

      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPaySolOption: " + e.toString());
      addError("getPaySolOption: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  public String getPiAmount(long appSeqNum, String equipModel, int equipLendType)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    String            fee     = "-1";
    try 
    {
      qs.append("select merchequip_amount   ");
      qs.append("from merchequipment where  ");
      qs.append("app_seq_num = ? and        ");
      qs.append("equip_model = ? and        ");
      qs.append("equiplendtype_code = ?     ");
       
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setString(2, equipModel);
      ps.setInt(3, equipLendType);
    
      rs = ps.executeQuery();

      if(rs.next())
      {
        fee = formatCurrency(rs.getString("merchequip_amount"));
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPiAmount: " + e.toString());
      addError("getPiAmount: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
    return fee;
  }

  public String getCreditAmount(long appSeqNum, String equipModel, int equipLendType)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    String            fee     = "-1";
    try 
    {
      qs.append("select merchequip_credit_amount  ");
      qs.append("from merchequipment where        ");
      qs.append("app_seq_num = ? and              ");
      qs.append("equip_model = ? and              ");
      qs.append("equiplendtype_code = ?           ");
       
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setString(2, equipModel);
      ps.setInt(3, equipLendType);
    
      rs = ps.executeQuery();

      if(rs.next())
      {
        fee = formatCurrency(rs.getString("merchequip_credit_amount"));
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCreditAmount: " + e.toString());
      addError("getCreditAmount: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
    return fee;
  }

  public String getChargeAmount(long appSeqNum, String equipModel, int equipLendType)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    String            fee     = "-1";
    try 
    {
      qs.append("select merchequip_charge_amount  ");
      qs.append("from merchequipment where        ");
      qs.append("app_seq_num = ? and              ");
      qs.append("equip_model = ? and              ");
      qs.append("equiplendtype_code = ?           ");
       
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setString(2, equipModel);
      ps.setInt(3, equipLendType);
    
      rs = ps.executeQuery();

      if(rs.next())
      {
        fee = formatCurrency(rs.getString("merchequip_charge_amount"));
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getChargeAmount: " + e.toString());
      addError("getChargeAmount: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
    return fee;
  }

  public void getSiteInspectionInfo(long appSeqNum)
  {
    StringBuffer      qs    = new StringBuffer("");
    PreparedStatement ps    = null;
    ResultSet         rs    = null;
    
    try
    {
      qs.append("select siteinsp_comment,       ");
      qs.append("siteinsp_name_flag,            ");
      qs.append("siteinsp_bus_hours_flag,       ");
      qs.append("siteinsp_no_of_emp,            ");
      qs.append("siteinsp_inv_street,           ");
      qs.append("siteinsp_inv_city,             ");
      qs.append("siteinsp_inv_state,            ");
      qs.append("siteinsp_inv_zip,              ");
      qs.append("siteinsp_vol_flag,             ");
      qs.append("siteinsp_bus_loc,              ");
      qs.append("siteinsp_bus_loc_comment,      ");
      qs.append("siteinsp_bus_address,          ");
      qs.append("siteinsp_bus_street,           ");
      qs.append("siteinsp_bus_city,             ");
      qs.append("siteinsp_bus_state,            ");
      qs.append("siteinsp_bus_zip,              ");
      qs.append("siteinsp_inv_sign_flag,        ");
      qs.append("siteinsp_inv_viewed_flag,      ");
      qs.append("siteinsp_inv_consistant_flag,  ");
      qs.append("siteinsp_inv_value,            ");
      qs.append("siteinsp_full_flag,            ");
      qs.append("siteinsp_full_name,            ");
      qs.append("siteinsp_full_street,          ");
      qs.append("siteinsp_full_city,            ");
      qs.append("siteinsp_full_state,           ");
      qs.append("siteinsp_full_zip,             ");
      qs.append("siteinsp_soft_flag,            ");
      qs.append("siteinsp_soft_name,            ");
      qs.append("siteinsp_unique_fee_desc,      ");
      qs.append("siteinsp_setupkit_type,        ");
      qs.append("siteinsp_setupkit_size,        ");
      qs.append("siteinsp_status,               ");
      qs.append("siteinsp_num_reprog_terms,     ");
      qs.append("siteinsp_num_reprog_prints,    ");
      qs.append("siteinsp_num_imprinters,       ");
      qs.append("siteinsp_supply_billing        ");
      qs.append("from siteinspection            ");
      qs.append("where app_seq_num = ?          ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.questionFlag[0]          = isBlank(rs.getString("siteinsp_name_flag"))           ? "-1" : rs.getString("siteinsp_name_flag");
        this.questionFlag[1]          = isBlank(rs.getString("siteinsp_inv_sign_flag"))       ? "-1" : rs.getString("siteinsp_inv_sign_flag");
        this.questionFlag[2]          = isBlank(rs.getString("siteinsp_bus_hours_flag"))      ? "-1" : rs.getString("siteinsp_bus_hours_flag");
        this.questionFlag[3]          = isBlank(rs.getString("siteinsp_inv_viewed_flag"))     ? "-1" : rs.getString("siteinsp_inv_viewed_flag");
        this.questionFlag[4]          = isBlank(rs.getString("siteinsp_inv_consistant_flag")) ? "-1" : rs.getString("siteinsp_inv_consistant_flag");
        this.questionFlag[5]          = isBlank(rs.getString("siteinsp_vol_flag"))            ? "-1" : rs.getString("siteinsp_vol_flag");

        this.houseFlag                = rs.getString("siteinsp_full_flag");
        this.softwareFlag             = rs.getString("siteinsp_soft_flag");
        
        this.comment                  = rs.getString("siteinsp_comment");
        this.inventoryAddressStreet   = rs.getString("siteinsp_inv_street");
        this.inventoryAddressCity     = rs.getString("siteinsp_inv_city");
        this.inventoryAddressState    = rs.getString("siteinsp_inv_state");
        this.inventoryAddressZip      = rs.getString("siteinsp_inv_zip");
                                        
        this.houseStreet              = rs.getString("siteinsp_full_street");
        this.houseCity                = rs.getString("siteinsp_full_city");
        this.houseState               = rs.getString("siteinsp_full_state");
        this.houseZip                 = rs.getString("siteinsp_full_zip");
                                        
        this.businessAddressStreet    = rs.getString("siteinsp_bus_street");
        this.businessAddressCity      = rs.getString("siteinsp_bus_city");
        this.businessAddressState     = rs.getString("siteinsp_bus_state");
        this.businessAddressZip       = rs.getString("siteinsp_bus_zip");
                                        
        this.inventoryValue           = rs.getString("siteinsp_inv_value");
        this.houseName                = rs.getString("siteinsp_full_name");
        this.numEmployees             = rs.getString("siteinsp_no_of_emp");
                                        
        this.businessLocation         = rs.getString("siteinsp_bus_loc");
        this.businessLocationComment  = rs.getString("siteinsp_bus_loc_comment");
        this.siteInspectionStatus     = isBlank(rs.getString("siteinsp_status"))              ? "-1" : rs.getString("siteinsp_status");
        this.businessAddress          = rs.getString("siteinsp_bus_address");
        this.softwareComment          = rs.getString("siteinsp_soft_name");
        this.uniqueFeeDesc            = isBlank(rs.getString("siteinsp_unique_fee_desc"))     ? ""   : rs.getString("siteinsp_unique_fee_desc");
        this.setupKitType             = isBlank(rs.getString("siteinsp_setupkit_type"))       ? "-1" : rs.getString("siteinsp_setupkit_type");
        this.setupKitSize             = isBlank(rs.getString("siteinsp_setupkit_size"))       ? "-1" : rs.getString("siteinsp_setupkit_size");

        this.numReprogTerms           = isBlank(rs.getString("siteinsp_num_reprog_terms"))    ? ""   : rs.getString("siteinsp_num_reprog_terms");
        this.numReprogPrints          = isBlank(rs.getString("siteinsp_num_reprog_prints"))   ? ""   : rs.getString("siteinsp_num_reprog_prints");
        this.numImprinters            = isBlank(rs.getString("siteinsp_num_imprinters"))      ? ""   : rs.getString("siteinsp_num_imprinters");
        this.supplyBilling            = isBlank(rs.getString("siteinsp_supply_billing"))      ? ""   : rs.getString("siteinsp_supply_billing");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getSiteInspectionInfo: " + e.toString());
      addError("getSiteInspectionInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }
  
  public void updateData(HttpServletRequest aReq)
  {
    double temp;
    for(int i=0; i<this.NUM_CARDS; i++)
    {
        if(!isBlank(aReq.getParameter("cardSelectedFlag" + i)) && aReq.getParameter("cardSelectedFlag" + i).equals("Y"))
        {
          this.cardSelectedFlag[i] = "Y";
          this.cardFee[i] = aReq.getParameter("cardFee" + i);
        }
        else
          this.cardSelectedFlag[i] = "N"; 
    }
    
    // set verisign over auth fee
    setVerisignOverAuthPerItem(aReq.getParameter("verisignOverAuthPerItem"));
    
    String tempString;
    for(int i=0; i<this.NUM_MISC; i++)
    {
        if(!isBlank(aReq.getParameter("miscSelectedFlag" + i)) && aReq.getParameter("miscSelectedFlag" + i).equals("Y"))
        {
          this.miscSelectedFlag[i] = "Y";
        }
        else
        {
          this.miscSelectedFlag[i] = "N"; 
        }
        
        tempString = isBlank(aReq.getParameter("miscFee" + i)) ? "" : aReq.getParameter("miscFee" + i);
        tempString = tempString.replace('f','l');
        tempString = tempString.replace('F','l');
        tempString = tempString.replace('d','l');
        tempString = tempString.replace('D','l');

        try
        {
          temp = Double.parseDouble(tempString);
          if(temp < 0.0)
          {
            this.miscFee[i] = "-1";
          }
          else
          {
            this.miscFee[i] = formatCurrency(aReq.getParameter("miscFee" + i));
          }
        }
        catch(Exception e)
        {
          this.miscFee[i] = "-1";
        }

        tempString = isBlank(aReq.getParameter("miscCreditAmount" + i)) ? "" : aReq.getParameter("miscCreditAmount" + i);
        tempString = tempString.replace('f','l');
        tempString = tempString.replace('F','l');
        tempString = tempString.replace('d','l');
        tempString = tempString.replace('D','l');

        try
        {
          temp = Double.parseDouble(tempString);
          if(temp < 0.0)
          {
            this.miscCreditAmount[i] = "-1";
          }
          else
          {
            this.miscCreditAmount[i] = formatCurrency(aReq.getParameter("miscCreditAmount" + i));
          }
        }
        catch(Exception e)
        {
          this.miscCreditAmount[i] = "-1";
        }

        tempString = isBlank(aReq.getParameter("miscChargeAmount" + i)) ? "" : aReq.getParameter("miscChargeAmount" + i);
        tempString = tempString.replace('f','l');
        tempString = tempString.replace('F','l');
        tempString = tempString.replace('d','l');
        tempString = tempString.replace('D','l');

        try
        {
          temp = Double.parseDouble(tempString);
          if(temp < 0.0)
          {
            this.miscChargeAmount[i] = "-1";
          }
          else
          {
            this.miscChargeAmount[i] = formatCurrency(aReq.getParameter("miscChargeAmount" + i));
          }
        }
        catch(Exception e)
        {
          this.miscChargeAmount[i] = "-1";
        }

    }

    for(int i=0; i<this.NUM_QUESTIONS; i++)
    {
        if(!isBlank(aReq.getParameter("questionFlag" + i)) && aReq.getParameter("questionFlag" + i).equals("Y"))
        {
          this.questionFlag[i] = "Y";
        }
        else if(!isBlank(aReq.getParameter("questionFlag" + i)) && aReq.getParameter("questionFlag" + i).equals("N"))
        {
          this.questionFlag[i] = "N";
        }
        else
        {
          this.questionFlag[i] = "-1"; 
        }
    }
  }

  public boolean validate(long primaryKey)
  {
    getApplicationType(primaryKey);
    
    // check if required fields are blank
    switch(this.appType)
    {
      case mesConstants.APP_TYPE_WEST_AMERICA:
      case mesConstants.APP_TYPE_AGENT:
      case mesConstants.APP_TYPE_MES:
      case mesConstants.APP_TYPE_MESC:
        if(this.pricingGrid == mesConstants.PRICING_GRID_INVALID)
        {
          addError("Please indicate which pricing grid was used to price this merchant");
        }
        
        if(this.pricingScenario == mesConstants.APP_PS_INTERCHANGE && this.appType == mesConstants.APP_TYPE_MES)
        {
          basisPointIncrease = "00";
        }
        else if(isBlank(basisPointIncrease))
        {
          addError("Please select a basis point increase");
        }    
        break;

      case mesConstants.APP_TYPE_MESB:
        if(this.pricingGrid == mesConstants.PRICING_GRID_INVALID)
        {
          addError("Please indicate which pricing grid was used to price this merchant");
        }
        break;

      case mesConstants.APP_TYPE_CBT:
      case mesConstants.APP_TYPE_MESA:
        if(this.pricingGrid == mesConstants.PRICING_GRID_INVALID)
        {
          addError("Please indicate the type of transaction that this merchant performs most");
        }
        break;

      case mesConstants.APP_TYPE_DEMO:
      case mesConstants.APP_TYPE_CEDAR:
      case mesConstants.APP_TYPE_SUMMIT:
      case mesConstants.APP_TYPE_ORANGE:
      case mesConstants.APP_TYPE_SVB:
        if(this.pricingGrid == mesConstants.PRICING_GRID_INVALID)
        {
          addError("Please provide how this merchant was priced");
        }
        break;
      
      case mesConstants.APP_TYPE_VERISIGN:
      case mesConstants.APP_TYPE_NSI:
      case mesConstants.APP_TYPE_VISTA:
      break;

      case mesConstants.APP_TYPE_DISCOVER:
      case mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL:
      case mesConstants.APP_TYPE_DISCOVER_IMS:
        if(this.pricingGrid == mesConstants.PRICING_GRID_INVALID)
        {
          addError("Please indicate which pricing grid was used to price this merchant");
        }
      break;

      default:
        break;
    }
    
    
    // only check business location for non-verisign/nsi apps
    if(this.appType != mesConstants.APP_TYPE_VERISIGN &&
       this.appType != mesConstants.APP_TYPE_NSI &&
       this.appType != mesConstants.APP_TYPE_VISTA)
    {
      if(this.businessLocation.equals("-1"))
      {
        addError("Please select a Location of Business");
      }
      if(this.businessLocation.equals("Other") && this.businessLocationComment.equals("-1"))
      {
        addError("Please explain Other Location of Business");
      }
    
      if(appType == mesConstants.APP_TYPE_DISCOVER_IMS && this.siteInspectionStatus.equals("-1"))
      {
        addError("Please select a site inspection status");
      }
    
    }
    
    return(! hasErrors());
  }

  public boolean validate(HttpServletRequest aReq)
  {
    if( this.pricingScenario == mesConstants.APP_PS_INVALID )
    {
      addError("Please select a Pricing Scenario");
    }

    if(this.discountRate == -1 && this.pricingScenario == mesConstants.APP_PS_FIXED_RATE)
    {
      addError("Please provide a valid Discount Rate for pricing scenario 2");
    }

    if(this.pricingScenario == mesConstants.APP_PS_INTERCHANGE)
    {
      if(this.interchangeDiscountRate == -1 && this.interchangePassthrough == -1)
      {
        addError("Please provide a valid Interchange Discount Rate or Per Item Fee for pricing scenario 3");
      }
      /* why would we care if rep wants to charge both a rate AND a per-item fee?  stupid.
      else if(this.interchangeDiscountRate > 0 && this.interchangePassthrough > 0)
      {
        addError("Please provide a valid Interchange Discount Rate OR Per Item Fee for pricing scenario 3");
      }
      */
    }

    if((this.interchangePassthrough4 == -1 || this.discountRate4 == -1) &&  this.pricingScenario == mesConstants.APP_PS_FIXED_PLUS_PER_ITEM)
    {

      if((this.appType == mesConstants.APP_TYPE_DISCOVER      || 
          this.appType == mesConstants.APP_TYPE_DISCOVER_IMS  || 
          appType == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL) 
          && this.discountRate4 == -1)
      {
        addError("Please provide a valid Discount Rate for the Fixed Rate + Per Item Plan ");
      }
      else if(this.appType != mesConstants.APP_TYPE_DISCOVER && 
              this.appType != mesConstants.APP_TYPE_DISCOVER_IMS && 
              appType != mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL)
      {
        addError("Please provide a valid Discount Rate AND Interchange Passthrough for pricing scenario 4");
      }

    }
    

    if(getOffGridApproved())
    {
      if(isBlank(offGridApprovedBy))
      {
        addError("Please specify who approved the off grid pricing.");
      }    
    }
    else if(this.pricingScenario == mesConstants.APP_PS_FIXED_PLUS_PER_ITEM && (this.appType == mesConstants.APP_TYPE_DISCOVER || this.appType == mesConstants.APP_TYPE_DISCOVER_IMS || appType == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL))
    {
      long pk = HttpHelper.getLong(aReq, "primaryKey");

      setSalesInfo(pk);
      int tempAppType = this.appType;
      
      //user discover pricing grid to validate bank referral.
      //if(tempAppType == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL)
      //{
        //always make apptype discover.. cause they all use discover pricing grids now.. 
        tempAppType = mesConstants.APP_TYPE_DISCOVER;
      //}

      DiscoverPricingBean dpb = new DiscoverPricingBean(tempAppType);

      String result = dpb.validateDiscountRate(discountRate4, interchangePassthrough4, pricingGrid, visaVol, aveTic);

      if(!result.equals(dpb.VALID))
      {
        addError(result);
      }
    }
    


    if(this.cbtDiscountRate5 == -1 && this.pricingScenario == mesConstants.CBT_APP_BUNDLED_RATE_PLAN)
    {
      addError("Please provide a valid Discount Rate for Bundled Rate Plan");
    }
    if(this.cbtNonQual5 == -1 && this.pricingScenario == mesConstants.CBT_APP_BUNDLED_RATE_PLAN)
    {
      //addError("Please provide a valid Non-Qual for Bundled Rate Plan");
    }
    
    if(this.cbtDiscountRate6 == -1 && this.pricingScenario == mesConstants.CBT_APP_UNBUNDLED_RATE_PLAN)
    {
      addError("Please provide a valid Discount Rate for Unbundled Rate Plan");
    }
    if(this.cbtPerItem6 == -1 && this.pricingScenario == mesConstants.CBT_APP_UNBUNDLED_RATE_PLAN)
    {
      addError("Please provide a valid Per Item fee for Unbundled Rate Plan");
    }
    if(this.cbtPerAuth6 == -1 && this.pricingScenario == mesConstants.CBT_APP_UNBUNDLED_RATE_PLAN)
    {
      addError("Please provide a valid Per Authorization fee for Unbundled Rate Plan");
    }

    if(this.cbtPerItem7 == -1 && this.pricingScenario == mesConstants.CBT_APP_INTERCHANGE_PLUS_PLAN)
    {
      addError("Please provide a valid Per Item fee for Interchange Plus Plan");
    }
    if(this.cbtPerAuth7 == -1 && this.pricingScenario == mesConstants.CBT_APP_INTERCHANGE_PLUS_PLAN)
    {
      addError("Please provide a valid Per Authorization fee for Interchange Plus Plan");
    }
    if(this.cbtPerCap7 == -1 && this.pricingScenario == mesConstants.CBT_APP_INTERCHANGE_PLUS_PLAN)
    {
      addError("Please provide a valid Per Capture fee for Interchange Plus Plan");
    }

    if(this.cbtDiscountRate8 == -1 && this.pricingScenario == mesConstants.CBT_APP_PERITEM_DISCRATE_PLAN)
    {
      addError("Please provide a valid Discount Rate for Visa & Mastercard");
    }
    if(this.cbtPerItem8 == -1 && this.pricingScenario == mesConstants.CBT_APP_PERITEM_DISCRATE_PLAN)
    {
      //addError("Please provide a valid Per Item fee for Visa & Mastercard");
    }

    for(int i=0; i<this.NUM_CARDS; i++)
    {
      if(this.cardSelectedFlag[i].equals("Y") && this.cardFee[i].equals("-1"))
      {
        if(cardCodes[i] != mesConstants.APP_CT_INTERNET || isCybercash() || this.pricingScenario != mesConstants.CBT_APP_PERITEM_DISCRATE_PLAN)
        {
          addError("Please select a fee for " + this.cardDesc[i]);
        }
        else if(cardCodes[i] == mesConstants.APP_CT_INTERNET && !isCybercash())
        {
          this.cardFee[i] = "0.0";
        }
      }
    }

    if(this.minMonthSelected)
    {
      if(this.minMonthDiscount.equals("-1"))
      {
        addError("Please Provide a valid Minimum Monthly Discount Charge");
      }
    }
    else if(!this.minMonthDiscount.equals("-1"))
    {
      addError("Please check the box or delete the default value for Minimum Monthly Discount");  
    }
    else if(!this.minMonthSelected && this.minMonthDiscount.equals("-1"))
    {
      
      if(appType == mesConstants.APP_TYPE_SUMMIT)
      {
        addError("Please provide a Minimum Monthly Discount");
      }
      
      this.minMonthDiscountBankCredit = "-1";
      this.minMonthDiscountBankCharge = "-1";
    }

    for(int i=0; i<this.NUM_MISC; i++)
    {
      String miscFeeDesc = this.miscDesc[i];

      if(appType == mesConstants.APP_TYPE_MESB && i == FEE_ACCOUNT_SERVICING)
      {
        miscFeeDesc = "Monthly Merchant Services Fee";
      }
      
      if(this.miscSelectedFlag[i].equals("Y") && this.miscFee[i].equals("-1"))
      {
        addError("Please provide a fee for " + miscFeeDesc);
      }
      else if(this.miscSelectedFlag[i].equals("N") && !this.miscFee[i].equals("-1"))
      {
        addError("Please check the box or delete the default value for " + miscFeeDesc);
      }
      else if(this.miscSelectedFlag[i].equals("N") && this.miscFee[i].equals("-1"))
      {
        if(i == FEE_CHARGEBACK && appType == mesConstants.APP_TYPE_SUMMIT)
        {
          addError("Please provide a Chargeback Fee");
        }
        
        this.miscCreditAmount[i] = "-1";
        this.miscChargeAmount[i] = "-1";
      }
      
      if(!this.miscCreditAmount[i].equals("-1"))
      {
        this.nameOfBankMiscReq = true;
      }

      //credit whole amount for these charges
      if(appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_MESA)
      {
        if(miscCodes[i] == mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_APP   || 
           miscCodes[i] == mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_SETUP ||
           miscCodes[i] == mesConstants.APP_MISC_CHARGE_MEMBERSHIP_FEES   ||
           miscCodes[i] == mesConstants.APP_MISC_CHARGE_UNIQUE_FEE)   
        {
           this.miscCreditAmount[i] = this.miscFee[i];
        }
      
      
      }

      if((appType == mesConstants.APP_TYPE_DISCOVER     || 
          appType == mesConstants.APP_TYPE_DISCOVER_IMS || 
          appType == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL) 
          && miscCodes[i] == mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_APP)
      {
        if(!this.miscFee[i].equals("-1"))
        {
          if(Double.parseDouble(this.miscFee[i]) < 50.00)
          {
            addError("Fee for " + miscFeeDesc + " must be no less than $50.00");
          }
        }
      }
    }
    
    
    if(appType == mesConstants.APP_TYPE_DISCOVER)
    {
      try
      {
        if(Double.parseDouble(miscFee[FEE_STATEMENT]) != -1 &&
           Double.parseDouble(miscFee[FEE_STATEMENT]) < 5.00)
        {
          addError("Monthly Service fee must be at least $5.00");
        }
      }
      catch(Exception e)
      {
      }
    }

    if(appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_MESA)
    {
      if(isBlank(supplyBilling))
      {
        addError("Please select a Supply Billing option");
      }
    }

    if(appType == mesConstants.APP_TYPE_NBSC)
    {
      if(this.miscSelectedFlag[FEE_TERM_REPROG].equals("Y") && (isBlank(this.numReprogTerms) || !isNumber(this.numReprogTerms)))
      {
        addError("Please specify the number of terminals that need reprogramming");
      }
      if(this.miscSelectedFlag[FEE_PRINTER_REPROG].equals("Y") && (isBlank(this.numReprogPrints) || !isNumber(this.numReprogPrints)))
      {
        addError("Please specify the number of printers that need reprogramming");
      }
      if(this.miscSelectedFlag[FEE_IMPRINTER].equals("Y") && (isBlank(this.numImprinters) || !isNumber(this.numImprinters)))
      {
        addError("Please specify the number of imprinters to charge for");
      }
    }

    if(verisign && this.miscFee[FEE_INTERNET_MONTHLY].equals("-1") && appType != mesConstants.APP_TYPE_VERISIGN && appType != mesConstants.APP_TYPE_NSI && appType != mesConstants.APP_TYPE_SVB)
    {
      addError("Please provide an Internet Service Monthly Fee");
    }

    if(verisign && this.miscFee[FEE_INTERNET_SETUP].equals("-1") && appType != mesConstants.APP_TYPE_VERISIGN && appType != mesConstants.APP_TYPE_NSI && appType != mesConstants.APP_TYPE_SVB)
    {
      addError("Please provide an Internet Service Setup Fee");
    }
    
    if(appType == mesConstants.APP_TYPE_SVB)
    {
      // check some SVB-specific validations
      if(!dialPay && visamcFee.equals("-1"))
      {
        addError("Please select a Visa/MC Authorization Fee");
      }
      
      if(this.miscFee[FEE_CHARGEBACK].equals("-1"))
      {
        addError("Please select a Chargeback Fee");
      }
      
      if(internetPOS && this.miscFee[FEE_INTERNET_MONTHLY].equals("-1"))
      {
        addError("Please provide an Internet Service Monthly Fee");
      }
      
      if(internetPOS && this.miscFee[FEE_INTERNET_SETUP].equals("-1"))
      {
        addError("Please provide an Internet Service Setup Fee");
      }
    }

    //if wireless terminal product type selected and monthly wireless fee blank or wireless transaction fee is blank.. drop error.. 
    //if not blank.. check to see that its a real number

    if(wirelessProduct && isBlank(this.wirelessSetupFee))
    {
      addError("A wireless service setup fee must be specified for this account");
    }
    else if(wirelessProduct)
    {
      if(!isRealNumber(this.wirelessSetupFee))
      {
        addError("Wireless Service Setup Fee must be a valid number");
      }
      else
      {
        if(appType == mesConstants.APP_TYPE_MES       || 
           appType == mesConstants.APP_TYPE_MESC      ||
           appType == mesConstants.APP_TYPE_DISCOVER  || 
           appType == mesConstants.APP_TYPE_DISCOVER_IMS)
        {
          if(parseDouble(this.wirelessSetupFee) < 75.0)
          {
            addError("Wireless Service Setup Fee must not be less than $75");
          }
        }
      }
    }


    if(wirelessProduct && isBlank(this.wirelessMonthlyFee))
    {
      addError("A monthly wireless fee must be specified for this account");
    }
    else if(wirelessProduct)
    {
      if(!isRealNumber(this.wirelessMonthlyFee))
      {
        addError("Monthly Wireless Fee must be a valid number");
      }
      else
      {
        if(appType == mesConstants.APP_TYPE_MES       || 
           appType == mesConstants.APP_TYPE_MESC      ||
           appType == mesConstants.APP_TYPE_DISCOVER  || 
           appType == mesConstants.APP_TYPE_DISCOVER_IMS)
        {
          if(parseDouble(this.wirelessMonthlyFee) < 25.0)
          {
            addError("Wireless Monthly Fee must not be less than $25");
          }
        }
      }
    }

    if(wirelessProduct && isBlank(this.wirelessTransactionFee))
    {
      addError("A wireless transaction fee must be specified for this account");
    }
    else if(wirelessProduct)
    {
      if(!isRealNumber(this.wirelessTransactionFee))
      {
        addError("Wireless Transaction Fee must be a valid number");
      }
      else
      {
        if(appType == mesConstants.APP_TYPE_MES       || 
           appType == mesConstants.APP_TYPE_MESC      ||
           appType == mesConstants.APP_TYPE_DISCOVER  || 
           appType == mesConstants.APP_TYPE_DISCOVER_IMS)
        {
          if(parseDouble(this.wirelessTransactionFee) < 0.0)
          {
            addError("Wireless Transaction Fee must not be less than $0");
          }
        }
      }
    }

    if(achRejectFeeFlag.equals("Y") && isBlank(this.achRejectFee))
    {
      addError("An ACH Reject fee must be specified for this account");
    }
    else if(!isBlank(this.achRejectFee) && !achRejectFeeFlag.equals("Y"))
    {
      addError("Please check the checkbox for the ACH Reject Fee misc charge option, or delete the inserted value"); 
    }  
    else if(!isBlank(this.achRejectFee) && achRejectFeeFlag.equals("Y"))
    {  
      if(!isRealNumber(this.achRejectFee))
      {
        addError("ACH Reject Fee must be a valid number");
      }
    }

    if(annualFeeFlag.equals("Y") && isBlank(this.annualFee))
    {
      addError("An Annual Fee must be specified for this account");
    }
    else if(!isBlank(this.annualFee) && !annualFeeFlag.equals("Y"))
    {
      addError("Please check the checkbox for the Annual Fee misc charge option, or delete the inserted value"); 
    }  
    else if(!isBlank(this.annualFee) && annualFeeFlag.equals("Y"))
    {  
      if(!isRealNumber(this.annualFee))
      {
        addError("Annual Fee must be a valid number");
      }
    }


    if(this.miscSelectedFlag[FEE_UNIQUE].equals("Y") && isBlank(this.uniqueFeeDesc))
    {
      addError("Please provide a brief description of the Unique Charge");
    }

    if(this.monthlyMembershipSelected)
    {
      if(this.monthlyMembershipFee.equals("-1"))
      {
        addError("Please Provide a valid Monthly Membership Fee");
      }
    }
    else if(!this.monthlyMembershipFee.equals("-1"))
    {
      addError("Please check the box or delete the value for Monthly Membership Fee");  
    }
    else if(!this.monthlyMembershipSelected && this.monthlyMembershipFee.equals("-1"))
    {
      this.monthlyMembershipBankCredit = "-1";
      this.monthlyMembershipBankCharge = "-1";
    }
    
    if(this.thirdPartySoftwareSelected)
    {
      if(this.thirdPartySoftwareFee.equals("-1"))
      {
        addError("Please Provide a valid Third Party Software Monthly Service Fee");
      }
    }
    else if(!this.thirdPartySoftwareFee.equals("-1"))
    {
      addError("Please check the box or delete the value for Third Party Software Monthly Service Fee");  
    }
    else if(!this.thirdPartySoftwareSelected && this.thirdPartySoftwareFee.equals("-1"))
    {
      this.thirdPartySoftwareBankCredit = "-1";
      this.thirdPartySoftwareBankCharge = "-1";
    }    

    double tempDouble = 0.0;
    String tempString = "";
    String tempError  = "";

    for(int i=0; i<equipmentInfoCount; i++)
    {
      try
      {
        tempString = aReq.getParameter("equipmentAmount" + i);
        tempString = tempString.replace('f','l');
        tempString = tempString.replace('F','l');
        tempString = tempString.replace('d','l');
        tempString = tempString.replace('D','l');
        tempDouble = Double.parseDouble(tempString);

        if(tempDouble < 0.0)
        {
          if((appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_MESA) && aReq.getParameter("equipModel" + i).equals("IP5"))
          {
            tempError = "Please provide a valid Amount for the " + aReq.getParameter("equipDesc" + i) + " you want to " + aReq.getParameter("lendDesc" + i);
          }
          else
          {
            tempError = "";
            addError("Please provide a valid Amount for the " + aReq.getParameter("equipDesc" + i) + " you want to " + aReq.getParameter("lendDesc" + i));
          }
        }
        else 
        {
          validPrice(appType,aReq.getParameter("equipModel" + i),aReq.getParameter("equipDesc" + i),aReq.getParameter("lendDesc" + i),aReq.getParameter("equipProdId" + i),aReq.getParameter("lendCode" + i),tempDouble);
        }
        
        if(aReq.getParameter("lendCode" + i).equals("1") && !aReq.getParameter("equipModel" + i).equals("IP5"))
        {
          this.equipmentPurchased = true;
        }
      }
      catch(Exception e)
      {
        if((appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_MESA) && aReq.getParameter("equipModel" + i).equals("IP5"))
        {
          tempError = "Please provide a valid Amount for the " + aReq.getParameter("equipDesc" + i) + " you want to " + aReq.getParameter("lendDesc" + i);
        }
        else
        {
          tempError = "";
          addError("Please provide a valid Amount for the " + aReq.getParameter("equipDesc" + i) + " you want to " + aReq.getParameter("lendDesc" + i));
        }
      }

      try
      {
        tempString = aReq.getParameter("equipmentCreditAmount" + i);
        tempString = tempString.replace('f','l');
        tempString = tempString.replace('F','l');
        tempString = tempString.replace('d','l');
        tempString = tempString.replace('D','l');
        tempDouble = Double.parseDouble(tempString);

        if(tempDouble < 0.0)
        {
          addError("Please provide a valid Amount to credit back to the bank for the " + aReq.getParameter("equipDesc" + i) + " you want to " + aReq.getParameter("lendDesc" + i));
        }
        else
        {
          this.nameOfBankEquipReq = true;
        }

      }
      catch(Exception e)
      {
         //addError("Please provide a valid Amount to credit back to the bank for the " + aReq.getParameter("equipDesc" + i) + " you want to " + aReq.getParameter("lendDesc" + i));
      }

      try
      {
        tempString = aReq.getParameter("equipmentChargeAmount" + i);
        tempString = tempString.replace('f','l');
        tempString = tempString.replace('F','l');
        tempString = tempString.replace('d','l');
        tempString = tempString.replace('D','l');
        tempDouble = Double.parseDouble(tempString);

        if(tempDouble < 0.0)
        {
          addError("Please provide a valid Amount to charge the bank for the " + aReq.getParameter("equipDesc" + i) + " you want to " + aReq.getParameter("lendDesc" + i));
        }
        else if((appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_MESA) && aReq.getParameter("equipModel" + i).equals("IP5"))
        {
          tempError = "";
        }


      }
      catch(Exception e)
      {
         //addError("Please provide a valid Amount to credit back to the bank for the " + aReq.getParameter("equipDesc" + i) + " you want to " + aReq.getParameter("lendDesc" + i));
      }


    }

    if((appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_MESA) && !this.equipmentPurchased && !isBlank(tempError))
    {
      addError(tempError);
    }

    if(appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_MESA)
    {

      if(this.equipmentPurchased)
      {
        if(this.setupKitSelected)
        {
          if(this.setupKitFee.equals("-1"))
          {
            addError("Please Provide a valid Setup Kit Fee or uncheck the setup kit check box");
          }
          else
          {
            this.setupKitBankCredit = this.setupKitFee;
            this.nameOfBankMiscReq  = true;
          }

        }
        else if(!this.setupKitFee.equals("-1"))
        {
          addError("Please check the setup kit check box or erase the setup kit fee provided");
        }

      }
      else
      {
        if(this.setupKitSelected)
        {
          if(this.setupKitFee.equals("-1") && this.setupKitBankCharge.equals("-1"))
          {
            addError("Please Provide a valid Setup Kit Fee");
          }
          else //if(this.ownedEquipFlag.equals("N"))
          {
            try
            {
              double setupkitfee          = Double.parseDouble(this.setupKitFee);
              double setupkitfeebankchrg  = Double.parseDouble(this.setupKitBankCharge);
              
              if(setupkitfee < 0)
              {
                this.setupKitFee = "0";
                setupkitfee = 0.0;
              }
              if(setupkitfeebankchrg < 0)
              {
                setupkitfeebankchrg = 0.0;
              }

              if((setupkitfee + setupkitfeebankchrg) < 20.0)
              {
                addError("The sum of the setup kit fee charged to merchant and charged to bank must be atleast $20.00 ");
              }
              else
              {
                this.setupKitBankCredit = Double.toString((setupkitfee + setupkitfeebankchrg) - 20.0);
                this.nameOfBankMiscReq  = true;
              }

            }
            catch(Exception e)
            {
              addError("The sum of the setup kit fee charged to merchant and charged to bank must be atleast $20.00 ");
            }
          }

        }
        else if(!this.setupKitFee.equals("-1"))
        {
          this.setupKitSelected = true;
        }
        else if(!this.setupKitSelected && this.setupKitFee.equals("-1"))
        {
          addError("A Setup Kit Fee is required.  Please check the box for Setup Kit and provide a valid Fee");  
        }
      }


      if(this.setupKitSelected && !this.setupKitFee.equals("-1"))
      {
        if(isBlank(this.setupKitType) || this.setupKitType.equals("-1"))
        {
          addError("If charging setup kit fee, you must select a setup kit type");
        }
      
        /*
          if(isBlank(this.setupKitSize) || this.setupKitSize.equals("-1"))
          {
            addError("If charging setup kit fee, you must select a setup kit size");
          }
        */

      }
    }

    if(appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_MESA)
    {
      if(this.nameOfBankMiscReq && isBlank(nameOfBankMiscCredit))
      {
        addError("Please provide the name of the bank to credit for Miscellaneous Fees");
      }

      if(this.nameOfBankEquipReq && isBlank(nameOfBankEquipCredit))
      {
        addError("Please provide the name of the bank to credit for Equipment/Software Fees");
      }
    }

    if( this.interchangeType == mesConstants.CBT_APP_INTERCHANGE_TYPE_INVALID && this.pricingScenario == mesConstants.CBT_APP_PERITEM_DISCRATE_PLAN)
    {
      //addError("Please choose an Interchange Fee Plan");
    }

    if(this.interchangeType == mesConstants.CBT_APP_INTERCHANGE_TYPE_RETAIL)
    {
      if(isBlank(interchangeTypeFeeRetail))
      {
        addError("Please select a Retail Punitive Bet");
      }
    }
    else if(this.interchangeType == mesConstants.CBT_APP_INTERCHANGE_TYPE_MOTO_LODGING)
    {
      if(isBlank(interchangeTypeFeeMotoLodging))
      {
        addError("Please select a Moto/Lodging Punitive Bet");
      }
    }

    
    if(this.ownedEquipFlag.equals("Y") && this.ownedEquipAmt.equals("-1"))
    {
      addError("Please provide a valid Amount for Owned Equipment Support Fee");
    }
    
    if(this.paySolOption == mesConstants.APP_PAYSOL_PC && this.pcQuantity.equals("-1"))
    {
      addError("Please provide a valid Quantity for Number of Licenses");
    }

    if((this.paySolOption == mesConstants.APP_PAYSOL_PC || this.paySolOption == mesConstants.APP_PAYSOL_PC_CHARGE_EXPRESS || this.paySolOption == mesConstants.APP_PAYSOL_PC_CHARGE_PRO) && this.pcFee.equals("-1"))
    {
      addError("Please provide a valid Amount per License");
    }
    //else if(this.paySolOption == mesConstants.APP_PAYSOL_PC && appType == mesConstants.APP_TYPE_DISCOVER_IMS)
    //{
      //double tempDub = parseDouble(this.pcFee);
      //if(tempDub < 250.0)
      //{
        //addError("POS Partner Software License Fee must be greater than $250");
      //}
    //}
    else if(this.paySolOption == mesConstants.APP_PAYSOL_PC && appType == mesConstants.APP_TYPE_CBT)
    {
      double tempDub = parseDouble(this.pcFee);
      if(tempDub < 400.0)
      {
        addError("POS Partner Software License Fee must be no less than $400");
      }
    }
    
    if(this.paySolOption == mesConstants.APP_PAYSOL_PC  && this.posMonthlyFee.equals("-1"))
    {
      addError("Please provide a valid POS Partner Monthly Fee");
    }

    if((this.paySolOption == mesConstants.APP_PAYSOL_PC_CHARGE_EXPRESS || this.paySolOption == mesConstants.APP_PAYSOL_PC_CHARGE_PRO) && this.pcchargeMonthlyFee.equals("-1"))
    {
      addError("Please provide a valid PC Charge Monthly Fee");
    }

    if(appType == mesConstants.APP_TYPE_DISCOVER || appType == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL) 
    {
      if(this.businessAddress.equals("-1"))
      {
        addError("Please specify the address of the inspected location");
      }
    }

    if(this.businessAddress.equals("Other") && this.businessAddressStreet.equals("-1"))
    {
      addError("Please provide other Address of Location");
    }
    if(this.businessAddress.equals("Other") && this.businessAddressCity.equals("-1"))
    {
      addError("Please provide other Address of Location City");
    }
    if(this.businessAddress.equals("Other") && this.businessAddressState.equals("-1"))
    {
      addError("Please provide other Address of Location State");
    }
    
    if (!this.businessAddressZip.equals("-1"))
    {
      switch ( this.businessAddressZip.length() ) 
      {
        case 5 : 
          if (!isNumber(this.businessAddressZip)) 
          {
            this.businessAddressZip = "-1";
          }
          break;
        case 9 : 
          if (!isNumber(this.businessAddressZip)) 
          {
            this.businessAddressZip = "-1";
          }
          break;
        case 10 : 
          if (!isNumber(this.businessAddressZip.substring(0,5)) || !isNumber(this.businessAddressZip.substring(6,10)) ||
              this.businessAddressZip.charAt(5) != '-') 
          {
            this.businessAddressZip = "-1";
          }
          break;
        default : 
          this.businessAddressZip = "-1";
          break;
      }
    }

    if(appType == mesConstants.APP_TYPE_DISCOVER || appType == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL) 
    {
      if(this.numEmployees.equals("-1"))
      {
        addError("Please specify the total number of employees");
      }
    }

    for(int i=0; i<this.NUM_QUESTIONS; i++)
    {
      if(this.questionFlag[i].equals("-1"))
      {
        this.questionFlag[i] = "";
        if(appType == mesConstants.APP_TYPE_DISCOVER || appType == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL) 
        {
          addError("Question, " + this.questionDesc[i] + ", must be answered");
        }
      }
    }

    if(this.inventoryValue.equals("-2"))
    {
      addError("A valid Dollar Value of Inventory is needed");
    }
    
    if (!this.inventoryAddressZip.equals("-1"))
    {
      switch ( this.inventoryAddressZip.length() ) 
      {
        case 5 : 
          if (!isNumber(this.inventoryAddressZip)) 
          {
            this.inventoryAddressZip = "-1";
            addError("Please provide valid Address of Inventory Zip" );
          }
          break;
        case 9 : 
          if (!isNumber(this.inventoryAddressZip)) 
          {
            this.inventoryAddressZip = "-1";
            addError("Please provide valid Address of Inventory Zip" );
          }
          break;
        case 10 : 
          if (!isNumber(this.inventoryAddressZip.substring(0,5)) || !isNumber(this.inventoryAddressZip.substring(6,10)) ||
              this.inventoryAddressZip.charAt(5) != '-') 
          {
            this.inventoryAddressZip = "-1";
            addError("Please provide valid Address of Inventory Zip " );
          }
          break;
        default : 
          this.inventoryAddressZip = "-1";
          addError("Please provide valid Address of Inventory Zip" );
          break;
      }
    }
    
    if(this.houseFlag.equals("Y") && this.houseName.equals("-1"))
    {
      addError("Please provide Fullfillment House Name");
    }    
    if(this.houseFlag.equals("Y") && this.houseStreet.equals("-1"))
    {
      addError("Please provide Fullfillment House Address");
    }    
    if(this.houseFlag.equals("Y") && this.houseCity.equals("-1"))
    {
      addError("Please provide Fullfillment House Address City");
    }    
    if(this.houseFlag.equals("Y") && this.houseState.equals("-1"))
    {
      addError("Please provide Fullfillment House Address State");
    }    
    
    
    if (!this.houseZip.equals("-1"))
    {
      switch ( this.houseZip.length() ) 
      {
        case 5 : 
          if (!isNumber(this.houseZip)) 
          {
            this.houseZip = "-1";
          }
          break;
        case 9 : 
          if (!isNumber(this.houseZip)) 
          {
            this.houseZip = "-1";
          }
          break;
        case 10 : 
          if (!isNumber(this.houseZip.substring(0,5)) || !isNumber(this.houseZip.substring(6,10)) ||
              this.houseZip.charAt(5) != '-') 
          {
            this.houseZip = "-1";
          }
          break;
        default : 
          this.houseZip = "-1";
          break;
      }
    }
    
    if(this.houseFlag.equals("Y") && this.houseZip.equals("-1"))
    {
      addError("Please provide Fullfillment House Address Zip");
    }    
    
    if(this.softwareFlag.equals("Y") && this.softwareComment.equals("-1"))
    {
      addError("Please provide Name of Authentication Software");
    }    
  
    if(this.comment.length() > 400)
    {
      addError("Comments must be less than 400 characters long. It is currently " + this.comment.length() + " characters long");
    }

    if(appType == mesConstants.APP_TYPE_SUMMIT)
    {
      if(isBlank(association))
      {
        addError("Please provide an Association Number in Section 8");
      }
      else if(association.length() != 6)
      {
        addError("Association Number must be 6 digits");
      }
      else if(!isNumber(association))
      {
        addError("Association Number must be a valid 6 digit number");
      }
    }

    return(! hasErrors());
  }

  private boolean validPrice(int client, String model, String desc, String lendDesc, String prodId, String lendCode, double price)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    boolean           result  = false;

    int     lendType = -1;
    String  fields   = "";

    try
    {
      lendType = Integer.parseInt(lendCode);

      switch(lendType)
      {
        case mesConstants.APP_EQUIP_PURCHASE:
          fields = "equip_buy_min,equip_buy_max ";
        break;

        case mesConstants.APP_EQUIP_RENT:
          fields = "equip_rent_min,equip_rent_max ";
        break;

        case mesConstants.APP_EQUIP_LEASE:
          fields = "equip_lease_min,equip_lease_max ";
        break;
      }

      qs.append("select ");
      qs.append(fields);
      qs.append("from equipment_application ");
      qs.append("where app_type = ? and ");
      qs.append("equip_model = ? ");
      
      if(!isBlank(prodId))
      {
        qs.append("and prod_option_id = ? ");
      }
      else
      {
        qs.append("and prod_option_id is null ");
      }

      ps = getPreparedStatement(qs.toString());

      ps.setInt   (1, client);
      ps.setString(2, model);
      
      if(!isBlank(prodId))
      {
        ps.setString(3, prodId);
      }

      rs = ps.executeQuery();

      if(rs.next())
      {
        result = true;

        if(!isBlank(rs.getString(1)))
        {
          double min = rs.getDouble(1);
          
          if((client == mesConstants.APP_TYPE_DISCOVER || client == mesConstants.APP_TYPE_DISCOVER_IMS) && model.equals("IP5") && isDialPay())
          {
            min = 0.0;
          }
          
          
          if(price < min)
          {
            result = false;
          }
          
          if(!result)
          {
            addError(lendDesc + " price for " + desc + " must be greater than $" + formatCurrency(Double.toString(min)) );
            return result;
          }

        }


        if(!isBlank(rs.getString(2)))
        {
          double max = rs.getDouble(2);
 
          if(price > max)
          {
            result = false;
          }

          if(!result)
          {
            addError(lendDesc + " price for " + desc + " must be less than $" + formatCurrency(Double.toString(max)) );
            return result;
          }
        }
      }
      else if(client != mesConstants.APP_TYPE_MES && client != mesConstants.APP_TYPE_MESC)
      {
        result = validPrice(mesConstants.APP_TYPE_MES, model, desc, lendDesc, prodId, lendCode, price);
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {}
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  
    return result;  
  }

  public String getTotal(String lendCode, String amount, String quantity)
  {
    String temp = "";
    double total = 0.0;
    try
    {

      double qty = Double.parseDouble(quantity);
      double amt = Double.parseDouble(formatCurrency(amount));

      total = amt * qty;
      temp  = NumberFormat.getCurrencyInstance(Locale.US).format(total);

      if(lendCode.equals("1"))
      {
        this.subtotalPur += total;
      }
      this.subtotal   += total;
      this.grandtotal += total;

    }
    catch(Exception e)
    {
      temp = "$0.00";
    }
    return temp;
  }
  public String getSubtotal()
  {
    String temp = "";
    try
    {
      temp = NumberFormat.getCurrencyInstance(Locale.US).format(this.subtotal);
    }
    catch(Exception e)
    {
      temp = "$0.00";
    }
    return temp;
  }
  public String getSubtotalPur()
  {
    String temp = "";
    try
    {
      temp = NumberFormat.getCurrencyInstance(Locale.US).format(this.subtotalPur);
    }
    catch(Exception e)
    {
      temp = "$0.00";
    }
    return temp;
  }

  public boolean isAmexSplit()
  {
    return amexSplit;
  }

  public void setAmexSplit(String amexSplit)
  {
    this.amexSplit = true;
  }

  public int getPricingGrid()
  {
    return this.pricingGrid;
  }
  public void setPricingGrid(String pricingGrid)
  {
    try
    {
      this.pricingGrid = Integer.parseInt(pricingGrid);
    }
    catch(Exception e)
    {
      this.pricingGrid = mesConstants.PRICING_GRID_INVALID;
    }
  }
  public Vector getPricingGrids()
  {
    return this.pricingGrids;
  }
  public Vector getDiscPricingGrids()
  {
    return this.discPricingGrids;
  }
  public Vector getDiscPricingGridsCodes()
  {
    return this.discPricingGridsCodes;
  }

  public String getTax()
  {
    String temp = "";
    try
    {
      if(this.taxRate < 0.0)
      {
        addError("Please enter a valid Tax Rate");
        return "0.00";
      }
      this.tax = this.subtotalPur * this.taxRate/100.0;
      temp = NumberFormat.getCurrencyInstance(Locale.US).format(this.tax);
    }
    catch(Exception e)
    {
      this.tax = 0.0;
      temp = "$0.00";
    }
    return temp;
  }

  public String getGrandtotal()
  {
    String temp = "";
    try
    {
      temp = NumberFormat.getCurrencyInstance(Locale.US).format(this.grandtotal + this.tax);
    }
    catch(Exception e)
    {
      temp = "$0.00";
    }
    return temp;
  }

  public String formatCurrency(String num)
  {
    try
    {
      if(isBlank(num))
        return "";
      num = num.replace('f','l');
      num = num.replace('F','l');
      num = num.replace('d','l');
      num = num.replace('D','l');
      double check = Double.parseDouble(num);
    }
    catch(Exception e)
    {
      return "";
    }

    if(isBlank(num))
      return "";

    char last;
    int length = num.length();
    int index  = num.indexOf('.');

    if(index == -1)
      return num + ".00";

    String result = "";
    if(length - index >= 4)
    {
      result = num.substring(0,index+3);
      last = num.charAt(index+3);
      if(last > '5')
      {
        try
        {
          index  = result.indexOf('.');
          String subStr1 = result.substring(0,index);
          String subStr2 = result.substring(index+1);
          int tempInt = Integer.parseInt(subStr2);
          tempInt += 1;
          if (tempInt == 100)
          {
            int tempInt2 = Integer.parseInt(subStr1);
            tempInt2 += 1;
            result = tempInt2 + ".00";
          }
          else if(tempInt == 1)
          {
            result = subStr1 + ".01";
          }
          else
          {
            result = subStr1 + "." + tempInt;
          }
        }
        catch(Exception e)
        {
          return "";
        }
      }
    }
    else if(length - index == 3)
    {
      result = num;
    }
    else if(length - index == 2)
    {
      result = num + "0";
    }
    else if(length - index == 1)
    {
      result = num + "00";
    }

    return result;
  }

  public boolean isActivec()
  {
    return activec;
  }
  public boolean isCybercash()
  {
    return this.cybercash;
  }
  public boolean isVerisign()
  {
    return this.verisign;
  }
  public boolean isWirelessProduct()
  {
    return this.wirelessProduct;
  }

  public String getCardDesc(int i)
  {
    return this.cardDesc[i];
  }
  public String getCardSelectedFlag(int i)
  {
    return this.cardSelectedFlag[i];
  }
  public int getCardCode(int i)
  {
    return this.cardCodes[i];
  }
  public String getCardFee(int i)
  {
    return this.cardFee[i];
  }

  public String getMiscDesc(int i)
  {
    return this.miscDesc[i];
  }
  public String getMiscSelectedFlag(int i)
  {
    return this.miscSelectedFlag[i];
  }
  public String getMiscFee(int i)
  {
    return this.miscFee[i];
  }

  public String getMiscCreditAmount(int i)
  {
    return this.miscCreditAmount[i];
  }

  public String getMiscChargeAmount(int i)
  {
    return this.miscChargeAmount[i];
  }


  public int getEquipmentInfoCount()
  {
    return this.equipmentInfoCount;
  }
  public void setEquipmentInfoCount(String equipmentInfoCount)
  {
    try
    {
      setEquipmentInfoCount(Integer.parseInt(equipmentInfoCount));
    }
    catch(Exception e)
    {
    }
  }
  public void setEquipmentInfoCount(int equipmentInfoCount)
  {
    this.equipmentInfoCount = equipmentInfoCount;
  }

  public String getDiscountRate()
  {
    String result = "";
    if(this.discountRate != -1)
    {
      result = String.valueOf(discountRate);
    }
    return result;
  }
  public void setDiscountRate(String discountRate)
  {
    try
    {
      this.discountRate = Double.parseDouble(discountRate);
    }
    catch(Exception e)
    {
     this.discountRate = -1;
    }
  }

  public String getMonthlyDialpayFee()
  {
    return this.monthlyDialpayFee;
  }

  public void setMonthlyDialpayFee(String monthlyDialpayFee)
  {
    this.monthlyDialpayFee = monthlyDialpayFee;
  }

  public String getMonthlyMultiMerchFee()
  {
    return this.monthlyMultiMerchFee;
  }

  public void setMonthlyMultiMerchFee(String monthlyMultiMerchFee)
  {
    this.monthlyMultiMerchFee = monthlyMultiMerchFee;
  }

  public String getWirelessMonthlyFee()
  {
    return this.wirelessMonthlyFee;
  }

  public void setWirelessMonthlyFee(String wirelessMonthlyFee)
  {
    this.wirelessMonthlyFee = wirelessMonthlyFee;
  }

  public String getWirelessSetupFee()
  {
    return this.wirelessSetupFee;
  }

  public void setWirelessSetupFee(String wirelessSetupFee)
  {
    this.wirelessSetupFee = wirelessSetupFee;
  }

  public String getWirelessTransactionFee()
  {
    return this.wirelessTransactionFee;
  }

  public void setWirelessTransactionFee(String wirelessTransactionFee)
  {
    this.wirelessTransactionFee = wirelessTransactionFee;
  }



  public String getWirelessMonthlyFeeFlag()
  {
    return this.wirelessMonthlyFeeFlag;
  }

  public void setWirelessMonthlyFeeFlag(String wirelessMonthlyFeeFlag)
  {
    this.wirelessMonthlyFeeFlag = wirelessMonthlyFeeFlag;
  }

  public String getWirelessSetupFeeFlag()
  {
    return this.wirelessSetupFeeFlag;
  }

  public void setWirelessSetupFeeFlag(String wirelessSetupFeeFlag)
  {
    this.wirelessSetupFeeFlag = wirelessSetupFeeFlag;
  }

  public String getAchRejectFeeFlag()
  {
    return this.achRejectFeeFlag;
  }

  public void setAchRejectFeeFlag(String achRejectFeeFlag)
  {
    this.achRejectFeeFlag = achRejectFeeFlag;
  }

  public String getAchRejectFee()
  {
    return this.achRejectFee;
  }

  public void setAchRejectFee(String achRejectFee)
  {
    this.achRejectFee = achRejectFee;
  }

  public String getAnnualFeeFlag()
  {
    return this.annualFeeFlag;
  }

  public void setAnnualFeeFlag(String annualFeeFlag)
  {
    this.annualFeeFlag = annualFeeFlag;
  }

  public String getAnnualFee()
  {
    return this.annualFee;
  }

  public void setAnnualFee(String annualFee)
  {
    this.annualFee = annualFee;
  }



  public String getWirelessTransactionFeeFlag()
  {
    return this.wirelessTransactionFeeFlag;
  }

  public void setWirelessTransactionFeeFlag(String wirelessTransactionFeeFlag)
  {
    this.wirelessTransactionFeeFlag = wirelessTransactionFeeFlag;
  }


  public String getMinMonthDiscount()
  {
    String result = "";
    if(!this.minMonthDiscount.equals("-1"))
    {
      result = this.minMonthDiscount;
    }
    return result;
  }

  public void setMinMonthDiscount(String minMonthDiscount)
  {
    try
    {
      double temp = Double.parseDouble(minMonthDiscount);
      this.minMonthDiscount = minMonthDiscount;
    }
    catch(Exception e)
    {
     this.minMonthDiscount = "-1";
    }
  }

  public String getMinMonthDiscountBankCredit()
  {
    String result = "";
    if(!this.minMonthDiscountBankCredit.equals("-1"))
    {
      result = this.minMonthDiscountBankCredit;
    }
    return result;
  }

  public String getMinMonthDiscountBankCharge()
  {
    String result = "";
    if(!this.minMonthDiscountBankCharge.equals("-1"))
    {
      result = this.minMonthDiscountBankCharge;
    }
    return result;
  }

  public String getMonthlyMembershipBankCredit()
  {
    String result = "";
    if(!this.monthlyMembershipBankCredit.equals("-1"))
    {
      result = this.monthlyMembershipBankCredit;
    }
    return result;
  }

  public String getMonthlyMembershipBankCharge()
  {
    String result = "";
    if(!this.monthlyMembershipBankCharge.equals("-1"))
    {
      result = this.monthlyMembershipBankCharge;
    }
    return result;
  }

  public String getThirdPartySoftwareBankCredit()
  {
    String result = "";
    if(!this.thirdPartySoftwareBankCredit.equals("-1"))
    {
      result = this.thirdPartySoftwareBankCredit;
    }
    return result;
  }

  public String getThirdPartySoftwareBankCharge()
  {
    String result = "";
    if(!this.thirdPartySoftwareBankCharge.equals("-1"))
    {
      result = this.thirdPartySoftwareBankCharge;
    }
    return result;
  }

  public String getSetupKitBankCredit()
  {
    String result = "";
    if(!this.setupKitBankCredit.equals("-1"))
    {
      result = this.setupKitBankCredit;
    }
    return result;
  }

  public String getSetupKitBankCharge()
  {
    String result = "";
    if(!this.setupKitBankCharge.equals("-1"))
    {
      result = this.setupKitBankCharge;
    }
    return result;
  }

  public void setMinMonthDiscountBankCredit(String minMonthDiscountBankCredit)
  {
    try
    {
      double temp = Double.parseDouble(minMonthDiscountBankCredit);
      this.minMonthDiscountBankCredit = minMonthDiscountBankCredit;
    }
    catch(Exception e)
    {
     this.minMonthDiscountBankCredit = "-1";
    }
  }

  public void setMinMonthDiscountBankCharge(String minMonthDiscountBankCharge)
  {
    try
    {
      double temp = Double.parseDouble(minMonthDiscountBankCharge);
      this.minMonthDiscountBankCharge = minMonthDiscountBankCharge;
    }
    catch(Exception e)
    {
     this.minMonthDiscountBankCharge = "-1";
    }
  }

  public void setMonthlyMembershipBankCredit(String monthlyMembershipBankCredit)
  {
    try
    {
      double temp = Double.parseDouble(monthlyMembershipBankCredit);
      this.monthlyMembershipBankCredit = monthlyMembershipBankCredit;
    }
    catch(Exception e)
    {
     this.monthlyMembershipBankCredit = "-1";
    }
  }

  public void setMonthlyMembershipBankCharge(String monthlyMembershipBankCharge)
  {
    try
    {
      double temp = Double.parseDouble(monthlyMembershipBankCharge);
      this.monthlyMembershipBankCharge = monthlyMembershipBankCharge;
    }
    catch(Exception e)
    {
     this.monthlyMembershipBankCharge = "-1";
    }
  }

  public void setThirdPartySoftwareBankCredit(String thirdPartySoftwareBankCredit)
  {
    try
    {
      double temp = Double.parseDouble(thirdPartySoftwareBankCredit);
      this.thirdPartySoftwareBankCredit = thirdPartySoftwareBankCredit;
    }
    catch(Exception e)
    {
     this.thirdPartySoftwareBankCredit = "-1";
    }
  }

  public void setThirdPartySoftwareBankCharge(String thirdPartySoftwareBankCharge)
  {
    try
    {
      double temp = Double.parseDouble(thirdPartySoftwareBankCharge);
      this.thirdPartySoftwareBankCharge = thirdPartySoftwareBankCharge;
    }
    catch(Exception e)
    {
     this.thirdPartySoftwareBankCharge = "-1";
    }
  }

  public void setSetupKitBankCredit(String setupKitBankCredit)
  {
    try
    {
      double temp = Double.parseDouble(setupKitBankCredit);
      this.setupKitBankCredit = setupKitBankCredit;
    }
    catch(Exception e)
    {
     this.setupKitBankCredit = "-1";
    }
  }

  public void setSetupKitBankCharge(String setupKitBankCharge)
  {
    try
    {
      double temp = Double.parseDouble(setupKitBankCharge);
      this.setupKitBankCharge = setupKitBankCharge;
    }
    catch(Exception e)
    {
     this.setupKitBankCharge = "-1";
    }
  }


  public String getMonthlyMembershipFee()
  {
    String result = "";
    if(!this.monthlyMembershipFee.equals("-1"))
    {
      result = this.monthlyMembershipFee;
    }
    return result;
  }

  public void setMonthlyMembershipFee(String monthlyMembershipFee)
  {
    try
    {
      double temp = Double.parseDouble(monthlyMembershipFee);
      this.monthlyMembershipFee = monthlyMembershipFee;
    }
    catch(Exception e)
    {
     this.monthlyMembershipFee = "-1";
    }
  }

  public String getThirdPartySoftwareFee()
  {
    String result = "";
    if(!this.thirdPartySoftwareFee.equals("-1"))
    {
      result = this.thirdPartySoftwareFee;
    }
    return result;
  }

  public void setThirdPartySoftwareFee(String thirdPartySoftwareFee)
  {
    try
    {
      double temp = Double.parseDouble(thirdPartySoftwareFee);
      this.thirdPartySoftwareFee = thirdPartySoftwareFee;
    }
    catch(Exception e)
    {
     this.thirdPartySoftwareFee = "-1";
    }
  }

  public String getSetupKitFee()
  {
    String result = "";
    if(!this.setupKitFee.equals("-1"))
    {
      result = this.setupKitFee;
    }
    return result;
  }

  public void setSetupKitFee(String setupKitFee)
  {
    try
    {
      double temp = Double.parseDouble(setupKitFee);
      this.setupKitFee = setupKitFee;
    }
    catch(Exception e)
    {
     this.setupKitFee = "-1";
    }
  }

  public String getReferralAuthFee()
  {
    String result = "";
    if(!this.referralAuthFee.equals("-1"))
    {
      result = this.referralAuthFee;
    }
    return result;
  }

  public void setReferralAuthFee(String referralAuthFee)
  {
    try
    {
      double temp = Double.parseDouble(referralAuthFee);
      this.referralAuthFee = referralAuthFee;
    }
    catch(Exception e)
    {
     this.referralAuthFee = "-1";
    }
  }

  public String getNameOfBankEquipCredit()
  {
    return this.nameOfBankEquipCredit;
  }

  public String getNameOfBankMiscCredit()
  {
    return this.nameOfBankMiscCredit;
  }

  public void setNameOfBankEquipCredit(String nameOfBankEquipCredit)
  {
    this.nameOfBankEquipCredit = nameOfBankEquipCredit;
  }

  public void setNameOfBankMiscCredit(String nameOfBankMiscCredit)
  {
    this.nameOfBankMiscCredit = nameOfBankMiscCredit;
  }

  public String getSetupKitType()
  {
    return this.setupKitType;
  }

  public void setSetupKitType(String setupKitType)
  {
    this.setupKitType = setupKitType;
  }

  public String getDailyDiscount()
  {
    return this.dailyDiscount;
  }

  public void setDailyDiscount(String dailyDiscount)
  {
    this.dailyDiscount = dailyDiscount;
  }

  public String getSetupKitSize()
  {
    return this.setupKitSize;
  }

  public void setSetupKitSize(String setupKitSize)
  {
    this.setupKitSize = setupKitSize;
  }

  public void setNumImprinters(String numImprinters)
  {
    this.numImprinters = numImprinters;
  }

  public void setNumReprogTerms(String numReprogTerms)
  {
    this.numReprogTerms = numReprogTerms;
  }

  public void setNumReprogPrints(String numReprogPrints)
  {
    this.numReprogPrints = numReprogPrints;
  }


  public String getNumImprinters()
  {
    return this.numImprinters;
  }

  public String getNumReprogTerms()
  {
    return this.numReprogTerms;
  }

  public String getNumReprogPrints()
  {
    return this.numReprogPrints;
  }


  public void setSetupKitSelected(String temp)
  {
    this.setupKitSelected = true;
  }
  public boolean getSetupKitSelected()
  {
    return this.setupKitSelected;
  }

  public void setMonthlyMembershipSelected(String temp)
  {
    this.monthlyMembershipSelected = true;
  }
  public boolean getMonthlyMembershipSelected()
  {
    return this.monthlyMembershipSelected;
  }

  public void setThirdPartySoftwareSelected(String temp)
  {
    this.thirdPartySoftwareSelected = true;
  }
  public boolean getThirdPartySoftwareSelected()
  {
    return this.thirdPartySoftwareSelected;
  }

  public void setMinMonthSelected(String temp)
  {
    if(temp != null && temp.equals("N"))
    {
      this.minMonthSelected = false;
    }
    else
    {
      this.minMonthSelected = true;
    }
  }
  public boolean getMinMonthSelected()
  {
    return this.minMonthSelected;
  }


  public String getOffGridApprovedBy()
  {
    return this.offGridApprovedBy;
  }

  public void setOffGridApprovedBy(String offGridApprovedBy)
  {
    this.offGridApprovedBy = offGridApprovedBy;
  }

  public boolean getOffGridApproved()
  {
    return this.offGridApproved;
  }

  public void setOffGridApproved(String offGridApproved)
  {
    this.offGridApproved = true;
  }



  public String getDiscountRate4()
  {
    String result = "";
    if(this.discountRate4 != -1)
    {
      result = String.valueOf(discountRate4);
    }
    return result;
  }
  public void setDiscountRate4(String discountRate4)
  {
    try
    {
      this.discountRate4 = Double.parseDouble(discountRate4);
    }
    catch(Exception e)
    {
     this.discountRate4 = -1;
    }
  }

  public String getCbtDiscountRate5()
  {
    String result = "";
    if(this.cbtDiscountRate5 != -1)
    {
      result = String.valueOf(cbtDiscountRate5);
    }
    return result;
  }
  public void setCbtDiscountRate5(String cbtDiscountRate5)
  {
    try
    {
      this.cbtDiscountRate5 = Double.parseDouble(cbtDiscountRate5);
    }
    catch(Exception e)
    {
     this.cbtDiscountRate5 = -1;
    }
  }
  public String getCbtDiscountRate6()
  {
    String result = "";
    if(this.cbtDiscountRate6 != -1)
    {
      result = String.valueOf(cbtDiscountRate6);
    }
    return result;
  }
  public void setCbtDiscountRate6(String cbtDiscountRate6)
  {
    try
    {
      this.cbtDiscountRate6 = Double.parseDouble(cbtDiscountRate6);
    }
    catch(Exception e)
    {
     this.cbtDiscountRate6 = -1;
    }
  }
  public String getCbtDiscountRate8()
  {
    String result = "";
    if(this.cbtDiscountRate8 != -1)
    {
      result = String.valueOf(cbtDiscountRate8);
    }
    return result;
  }
  public void setCbtDiscountRate8(String cbtDiscountRate8)
  {
    try
    {
      this.cbtDiscountRate8 = Double.parseDouble(cbtDiscountRate8);
    }
    catch(Exception e)
    {
     this.cbtDiscountRate8 = -1;
    }
  }
  
  
  public String getCbtNonQual5()
  {
    String result = "";
    if(this.cbtNonQual5 != -1)
    {
      result = String.valueOf(cbtNonQual5);
    }
    return result;
  }
  public void setCbtNonQual5(String cbtNonQual5)
  {
    try
    {
      this.cbtNonQual5 = Double.parseDouble(cbtNonQual5);
    }
    catch(Exception e)
    {
     this.cbtNonQual5 = -1;
    }
  }
  public String getCbtPerItem6()
  {
    String result =  "";
    if(this.cbtPerItem6 != -1)
    {
      result = String.valueOf(cbtPerItem6);
    }
    return result;
  }
  public void setCbtPerItem6(String cbtPerItem6)
  {
    try
    {
      cbtPerItem6 = formatCurrency(cbtPerItem6);
      this.cbtPerItem6 = Double.parseDouble(cbtPerItem6);
    }
    catch(Exception e)
    {
     this.cbtPerItem6 = -1;
    }
  }
  public String getCbtPerItem8()
  {
    String result =  "";
    if(this.cbtPerItem8 != -1)
    {
      result = String.valueOf(cbtPerItem8);
    }
    return result;
  }
  public void setCbtPerItem8(String cbtPerItem8)
  {
    try
    {
      cbtPerItem8 = formatCurrency(cbtPerItem8);
      this.cbtPerItem8 = Double.parseDouble(cbtPerItem8);
    }
    catch(Exception e)
    {
      this.cbtPerItem8 = -1;
    }
  }

  public String getCbtPerItem7()
  {
    String result =  "";
    if(this.cbtPerItem7 != -1)
    {
      result = String.valueOf(cbtPerItem7);
    }
    return result;
  }
  public void setCbtPerItem7(String cbtPerItem7)
  {
    try
    {
      cbtPerItem7 = formatCurrency(cbtPerItem7);
      this.cbtPerItem7 = Double.parseDouble(cbtPerItem7);
    }
    catch(Exception e)
    {
     this.cbtPerItem7 = -1;
    }
  }

  public String getVisamcFee()
  {
    String result =  "";
    if(!this.visamcFee.equals("-1"))
    {
      result = this.visamcFee;
    }
    return result;
  }
  public void setVisamcFee(String visamcFee)
  {
    this.visamcFee  = visamcFee;
  }


  public String getAruFee()
  {
    String result =  "";
    if(!this.aruFee.equals("-1"))
    {
      result = this.aruFee;
    }
    return result;
  }
  public void setAruFee(String aruFee)
  {
    this.aruFee  = aruFee;
  }

  public String getVoiceFee()
  {
    String result =  "";
    if(!this.voiceFee.equals("-1"))
    {
      result = this.voiceFee;
    }
    return result;
  }
  public void setVoiceFee(String voiceFee)
  {
    this.voiceFee  = voiceFee;
  }


  public void setBasisPointIncrease(String basisPointIncrease)
  {
    this.basisPointIncrease = basisPointIncrease;
  }

  public String getBasisPointIncrease()
  {
    return this.basisPointIncrease;
  }

  public String getCbtPerAuth6()
  {
    String result =  "";
    if(this.cbtPerAuth6 != -1)
    {
      result = String.valueOf(cbtPerAuth6);
    }
    return result;
  }
  public void setCbtPerAuth6(String cbtPerAuth6)
  {
    try
    {
      cbtPerAuth6 = formatCurrency(cbtPerAuth6);
      this.cbtPerAuth6 = Double.parseDouble(cbtPerAuth6);
    }
    catch(Exception e)
    {
     this.cbtPerAuth6 = -1;
    }
  }
  public String getCbtPerAuth7()
  {
    String result =  "";
    if(this.cbtPerAuth7 != -1)
    {
      result = String.valueOf(cbtPerAuth7);
    }
    return result;
  }
  public void setCbtPerAuth7(String cbtPerAuth7)
  {
    try
    {
      cbtPerAuth7      = formatCurrency(cbtPerAuth7);
      this.cbtPerAuth7 = Double.parseDouble(cbtPerAuth7);
    }
    catch(Exception e)
    {
     this.cbtPerAuth7 = -1;
    }
  }
  public String getCbtPerCap7()
  {
    String result =  "";
    if(this.cbtPerCap7 != -1)
    {
      result = String.valueOf(cbtPerCap7);
    }
    return result;
  }
  public void setCbtPerCap7(String cbtPerCap7)
  {
    try
    {
      cbtPerCap7      = formatCurrency(cbtPerCap7);
      this.cbtPerCap7 = Double.parseDouble(cbtPerCap7);
    }
    catch(Exception e)
    {
     this.cbtPerCap7 = -1;
    }
  }

  public String getInterchangePassthrough()
  {
    String result =  "";
    if(this.interchangePassthrough != -1)
    {
      result = String.valueOf(interchangePassthrough);
    }
    return result;
  }
  public void setInterchangePassthrough(String interchangePassthrough)
  {
    try
    {
      interchangePassthrough = formatCurrency(interchangePassthrough);
      this.interchangePassthrough = Double.parseDouble(interchangePassthrough);
    }
    catch(Exception e)
    {
     this.interchangePassthrough = -1;
    }
  }

  public String getInterchangeDiscountRate()
  {
    String result =  "";
    if(this.interchangeDiscountRate != -1)
    {
      result = String.valueOf(interchangeDiscountRate);
    }
    return result;
  }
  public void setInterchangeDiscountRate(String interchangeDiscountRate)
  {
    try
    {
      interchangeDiscountRate = formatCurrency(interchangeDiscountRate);
      this.interchangeDiscountRate = Double.parseDouble(interchangeDiscountRate);
    }
    catch(Exception e)
    {
     this.interchangeDiscountRate = -1;
    }
  }

  public String getInterchangePassthrough4()
  {
    String result =  "";
    if(this.interchangePassthrough4 != -1)
    {
      result = String.valueOf(interchangePassthrough4);
    }
    return result;
  }
  
  public void setInterchangePassthrough4(String interchangePassthrough4)
  {
    try
    {
      interchangePassthrough4 = formatCurrency(interchangePassthrough4);
      this.interchangePassthrough4 = Double.parseDouble(interchangePassthrough4);
    }
    catch(Exception e)
    {
     this.interchangePassthrough4 = -1;
    }
  }
  
  public String getOwnedEquipFlag()
  {
    return this.ownedEquipFlag;
  }
  public void setOwnedEquipFlag(String ownedEquipFlag)
  {
      this.ownedEquipFlag = ownedEquipFlag;
  }

  public String getOwnedEquipQty()
  {
    String result = "";
    if(!this.ownedEquipQty.equals("-1"))
    {
      result = this.ownedEquipQty;
    }
    return result;
  }
  public void setOwnedEquipQty(String ownedEquipQty)
  {
    try
    {
      int tempInt = Integer.parseInt(ownedEquipQty);
      this.ownedEquipQty = ownedEquipQty;
    }
    catch(Exception e)
    {
      this.ownedEquipQty = "-1";
    }
  }

  public String getOwnedEquipAmt()
  {
    String result = "";

    if(!isBlank(this.ownedEquipAmt) && !this.ownedEquipAmt.equals("-1"))
    {
      result = this.ownedEquipAmt;
    }
    return result;
  }
  public void setOwnedEquipAmt(String ownedEquipAmt)
  {
    try
    {
      double tempDouble = Double.parseDouble(ownedEquipAmt);
      if(tempDouble < 0.0)
      {
        this.ownedEquipAmt = "-1";
      }
      else
      {
        this.ownedEquipAmt = formatCurrency(ownedEquipAmt);
      }
    }
    catch(Exception e)
    {
      this.ownedEquipAmt = "-1";
    }
  }



  public String getOwnedEquipCreditAmt()
  {
    String result = "";

    if(!isBlank(this.ownedEquipCreditAmt) && !this.ownedEquipCreditAmt.equals("-1"))
    {
      result = this.ownedEquipCreditAmt;
    }
    return result;
  }
  public void setOwnedEquipCreditAmt(String ownedEquipCreditAmt)
  {
    try
    {
      double tempDouble = Double.parseDouble(ownedEquipCreditAmt);
      if(tempDouble < 0.0)
      {
        this.ownedEquipCreditAmt = "-1";
      }
      else
      {
        this.ownedEquipCreditAmt = formatCurrency(ownedEquipCreditAmt);
      }
    }
    catch(Exception e)
    {
      this.ownedEquipCreditAmt = "-1";
    }
  }


  public String getOwnedEquipChargeAmt()
  {
    String result = "";

    if(!isBlank(this.ownedEquipChargeAmt) && !this.ownedEquipChargeAmt.equals("-1"))
    {
      result = this.ownedEquipChargeAmt;
    }
    return result;
  }
  public void setOwnedEquipChargeAmt(String ownedEquipChargeAmt)
  {
    try
    {
      double tempDouble = Double.parseDouble(ownedEquipChargeAmt);
      if(tempDouble < 0.0)
      {
        this.ownedEquipChargeAmt = "-1";
      }
      else
      {
        this.ownedEquipChargeAmt = formatCurrency(ownedEquipChargeAmt);
      }
    }
    catch(Exception e)
    {
      this.ownedEquipChargeAmt = "-1";
    }
  }


  public String getPosMonthlyFee()
  {
    String result = "";

    if(!isBlank(this.posMonthlyFee) && !this.posMonthlyFee.equals("-1"))
    {
      result = this.posMonthlyFee;
    }
    return result;
  }
  public void setPosMonthlyFee(String posMonthlyFee)
  {
    try
    {
      double tempDouble = Double.parseDouble(posMonthlyFee);
      if(tempDouble < 0.0)
      {
        this.posMonthlyFee = "-1";
      }
      else
      {
        this.posMonthlyFee = formatCurrency(posMonthlyFee);
      }
    }
    catch(Exception e)
    {
      this.posMonthlyFee = "-1";
    }
  }

  public String getPosMonthlyFeeCredit()
  {
    String result = "";

    if(!isBlank(this.posMonthlyFeeCredit) && !this.posMonthlyFeeCredit.equals("-1"))
    {
      result = this.posMonthlyFeeCredit;
    }
    return result;
  }
  public void setPosMonthlyFeeCredit(String posMonthlyFeeCredit)
  {
    try
    {
      double tempDouble = Double.parseDouble(posMonthlyFeeCredit);
      if(tempDouble < 0.0)
      {
        this.posMonthlyFeeCredit = "-1";
      }
      else
      {
        this.posMonthlyFeeCredit = formatCurrency(posMonthlyFeeCredit);
      }
    }
    catch(Exception e)
    {
      this.posMonthlyFeeCredit = "-1";
    }
  }

//pc charge
  public String getPcchargeMonthlyFee()
  {
    String result = "";

    if(!isBlank(this.pcchargeMonthlyFee) && !this.pcchargeMonthlyFee.equals("-1"))
    {
      result = this.pcchargeMonthlyFee;
    }
    return result;
  }
  public void setPcchargeMonthlyFee(String pcchargeMonthlyFee)
  {
    try
    {
      double tempDouble = Double.parseDouble(pcchargeMonthlyFee);
      if(tempDouble < 0.0)
      {
        this.pcchargeMonthlyFee = "-1";
      }
      else
      {
        this.pcchargeMonthlyFee = formatCurrency(pcchargeMonthlyFee);
      }
    }
    catch(Exception e)
    {
      this.pcchargeMonthlyFee = "-1";
    }
  }

  public String getPcchargeMonthlyFeeCredit()
  {
    String result = "";

    if(!isBlank(this.pcchargeMonthlyFeeCredit) && !this.pcchargeMonthlyFeeCredit.equals("-1"))
    {
      result = this.pcchargeMonthlyFeeCredit;
    }
    return result;
  }
  public void setPcchargeMonthlyFeeCredit(String pcchargeMonthlyFeeCredit)
  {
    try
    {
      double tempDouble = Double.parseDouble(pcchargeMonthlyFeeCredit);
      if(tempDouble < 0.0)
      {
        this.pcchargeMonthlyFeeCredit = "-1";
      }
      else
      {
        this.pcchargeMonthlyFeeCredit = formatCurrency(pcchargeMonthlyFeeCredit);
      }
    }
    catch(Exception e)
    {
      this.pcchargeMonthlyFeeCredit = "-1";
    }
  }

  public int getPricingScenario()
  {
    return this.pricingScenario;
  }

  public void setPricingScenario(String pricingScenario)
  {
    try
    {
      this.pricingScenario = Integer.parseInt(pricingScenario);

      if(this.pricingScenario == mesConstants.IMS_RETAIL_RESTAURANT_PLAN)
      {
        discountRate4           = 1.79;
        //interchangePassthrough4 = .10;
      }
      else if(this.pricingScenario == mesConstants.IMS_MOTO_INTERNET_DIALPAY_PLAN)
      {
        discountRate4           = 2.29;
        //interchangePassthrough4 = .10;
      }
    }
    catch(Exception e)
    {
      this.pricingScenario = mesConstants.APP_PS_INVALID;
    }
  }

  public void setImsSurcharge()
  {
    this.imsSurcharge = true;
  }

  public boolean isImsSurcharge()
  {
    return this.imsSurcharge;
  }

  public int getInterchangeType()
  {
    return this.interchangeType;
  }
  public void setInterchangeType(String interchangeType)
  {
    try
    {
      this.interchangeType = Integer.parseInt(interchangeType);
    }
    catch(Exception e)
    {
      this.interchangeType = mesConstants.CBT_APP_INTERCHANGE_TYPE_INVALID;
    }
  }

  public String getInterchangeTypeFeeRetail()
  {
    return this.interchangeTypeFeeRetail;
  }
  public void setInterchangeTypeFeeRetail(String interchangeTypeFeeRetail)
  {
    this.interchangeTypeFeeRetail = interchangeTypeFeeRetail;
  }
  public String getInterchangeTypeFeeMotoLodging()
  {
    return this.interchangeTypeFeeMotoLodging;
  }
  public void setInterchangeTypeFeeMotoLodging(String interchangeTypeFeeMotoLodging)
  {
    this.interchangeTypeFeeMotoLodging = interchangeTypeFeeMotoLodging;
  }


                
  public String getSupplyBilling()
  {
    return this.supplyBilling;
  }
  public void setSupplyBilling(String supplyBilling)
  {
    this.supplyBilling = supplyBilling;
  }

  public int getPaySolOption()
  {
    return this.paySolOption;
  }
  public void setPaySolOption(String paySolOption)
  {
    try
    {
      setPaySolOption(Integer.parseInt(paySolOption));
    }
    catch(Exception e)
    {
    }
  }
  public void setPaySolOption(int paySolOption)
  {
    this.paySolOption = paySolOption;
  }

  public boolean isDialPay()
  {
    return (getPaySolOption() == mesConstants.APP_PAYSOL_DIAL_PAY);
  }

  public boolean isInternet()
  {
    return (getPaySolOption() == mesConstants.APP_PAYSOL_INTERNET);
  }

  public String getUniqueFeeDesc()
  {
    return this.uniqueFeeDesc;
  }
  public void setUniqueFeeDesc(String uniqueFeeDesc)
  {
    this.uniqueFeeDesc = uniqueFeeDesc;
  }

  public String getPcQuantity()
  {
    String result = "";
    if(!this.pcQuantity.equals("-1"))
    {
      result = this.pcQuantity;
    }
    return result;
  }

  public void setPcQuantity(String pcQuantity)
  {
    try
    {
      int tempInt = Integer.parseInt(pcQuantity);
      if(tempInt < 0)
      {
        this.pcQuantity = "-1";
      }
      else
      {
        this.pcQuantity = pcQuantity;
      }
    }
    catch(Exception e)
    {
      this.pcQuantity = "-1";
    }
  }
  public String getPcModel()
  {
    return this.pcModel;
  }

  public void setPcModel(String pcModel)
  {
    this.pcModel = pcModel;
  }

  public String getPcFee()
  {
    String result = "";
    if(!this.pcFee.equals("-1"))
    {
      result = this.pcFee;
    }
    return result;
  }
  public void setPcFee(String pcFee)
  {
    try
    {
      double tempDouble = Double.parseDouble(pcFee);
      if(tempDouble < 0.0)
      {
        this.pcFee = "-1";
      }
      else
      {
        this.pcFee = formatCurrency(pcFee);
      }
    }
    catch(Exception e)
    {
      this.pcFee = "-1";
    }
  }


  public String getPcCreditFee()
  {
    String result = "";
    if(!this.pcCreditFee.equals("-1"))
    {
      result = this.pcCreditFee;
    }
    return result;
  }
  public void setPcCreditFee(String pcCreditFee)
  {
    try
    {
      double tempDouble = Double.parseDouble(pcCreditFee);
      if(tempDouble < 0.0)
      {
        this.pcCreditFee = "-1";
      }
      else
      {
        this.pcCreditFee = formatCurrency(pcCreditFee);
      }
    }
    catch(Exception e)
    {
      this.pcCreditFee = "-1";
    }
  }



  public String getBusinessAddressZip()
  {
    String result = "";
    if(!this.businessAddressZip.equals("-1"))
    {
      result = this.businessAddressZip;
    }
    return result;
  }
  public void setBusinessAddressZip(String businessAddressZip)
  {
    if(!isBlank(businessAddressZip))
      this.businessAddressZip = businessAddressZip;
    else
      this.businessAddressZip = "-1";
  }
  public String getNumEmployees()
  {
    String result = "";
    if(!this.numEmployees.equals("-1"))
    {
      result = this.numEmployees;
    }
    return result;
  }
  public void setNumEmployees(String numEmployees)
  {
    try
    {
      int temp = Integer.parseInt(numEmployees);
      this.numEmployees = numEmployees;
    }
    catch(Exception e)
    {
      this.numEmployees = "-1";
    }
  }

  public String getBusinessLocationComment()
  {
    String result = "";
    if(!this.businessLocationComment.equals("-1"))
    {
      result = this.businessLocationComment;
    }
    return result;
  }
  public void setBusinessLocationComment(String businessLocationComment)
  {
    if(!isBlank(businessLocationComment))
      this.businessLocationComment = businessLocationComment;
    else
      this.businessLocationComment = "-1";
  }

  public String getBusinessAddressCity()
  {
    String result = "";
    if(!this.businessAddressCity.equals("-1"))
    {
      result = this.businessAddressCity;
    }
    return result;
  }
  public void setBusinessAddressCity(String businessAddressCity)
  {
    if(!isBlank(businessAddressCity))
      this.businessAddressCity = businessAddressCity;
    else
      this.businessAddressCity = "-1";
  }


  public String getBusinessAddressStreet()
  {
    String result = "";
    if(!this.businessAddressStreet.equals("-1"))
    {
      result = this.businessAddressStreet;
    }
    return result;
  }
  public void setBusinessAddressStreet(String businessAddressStreet)
  {
    if(!isBlank(businessAddressStreet))
      this.businessAddressStreet = businessAddressStreet;
    else
      this.businessAddressStreet = "-1";
  }

  public String getBusinessLocation()
  {
    return this.businessLocation;
  }
  public void setBusinessLocation(String businessLocation)
  {
    this.businessLocation = businessLocation;
  }

  public String getSiteInspectionStatus()
  {
    return this.siteInspectionStatus;
  }
  public void setSiteInspectionStatus(String siteInspectionStatus)
  {
    this.siteInspectionStatus = siteInspectionStatus;
  }

  public String getBusinessAddress()
  {
    return this.businessAddress;
  }
  public void setBusinessAddress(String businessAddress)
  {
    this.businessAddress = businessAddress;
  }
  public String getBusinessAddressState()
  {
    return this.businessAddressState;
  }
  public void setBusinessAddressState(String businessAddressState)
  {
    this.businessAddressState = businessAddressState;
  }

  public String getQuestionDesc(int i)
  {
    return this.questionDesc[i];
  }
  public String getQuestionFlag(int i)
  {
    return this.questionFlag[i];
  }

  public String getInventoryAddressStreet()
  {
    String result = "";
    if(!this.inventoryAddressStreet.equals("-1"))
    {
      result = this.inventoryAddressStreet;
    }
    return result;
  }
  public void setInventoryAddressStreet(String inventoryAddressStreet)
  {
    if(!isBlank(inventoryAddressStreet))
      this.inventoryAddressStreet = inventoryAddressStreet;
    else
      this.inventoryAddressStreet = "-1";
  }

  public String getInventoryAddressCity()
  {
    String result = "";
    if(!this.inventoryAddressCity.equals("-1"))
    {
      result = this.inventoryAddressCity;
    }
    return result;
  }
  public void setInventoryAddressCity(String inventoryAddressCity)
  {
    if(!isBlank(inventoryAddressCity))
      this.inventoryAddressCity = inventoryAddressCity;
    else
      this.inventoryAddressCity = "-1";
  }

  public String getInventoryAddressState()
  {
    return this.inventoryAddressState;
  }
  public void setInventoryAddressState(String inventoryAddressState)
  {
    this.inventoryAddressState = inventoryAddressState;
  }

  public String getInventoryAddressZip()
  {
    String result = "";
    if(!this.inventoryAddressZip.equals("-1"))
    {
      result = this.inventoryAddressZip;
    }
    return result;
  }
  public void setInventoryAddressZip(String inventoryAddressZip)
  {
    if(!isBlank(inventoryAddressZip))
      this.inventoryAddressZip = inventoryAddressZip;
    else
      this.inventoryAddressZip = "-1";
  }


  public String getHouseStreet()
  {
    String result = "";
    if(!this.houseStreet.equals("-1"))
    {
      result = this.houseStreet;
    }
    return result;
  }
  public void setHouseStreet(String houseStreet)
  {
    if(!isBlank(houseStreet))
      this.houseStreet = houseStreet;
    else
      this.houseStreet = "-1";
  }

  public String getComment()
  {
    return this.comment;
  }
  public void setComment(String comment)
  {
    this.comment = comment;
  }

  public String getHouseCity()
  {
    String result = "";
    if(!this.houseCity.equals("-1"))
    {
      result = this.houseCity;
    }
    return result;
  }
  public void setHouseCity(String houseCity)
  {
    if(!isBlank(houseCity))
      this.houseCity = houseCity;
    else
      this.houseCity = "-1";
  }

  public String getHouseState()
  {
    return this.houseState;
  }
  public void setHouseState(String houseState)
  {
    this.houseState = houseState;
  }

  public String getHouseZip()
  {
    String result = "";
    if(!this.houseZip.equals("-1"))
    {
      result = this.houseZip;
    }
    return result;
  }
  public void setHouseZip(String houseZip)
  {
    if(!isBlank(houseZip))
      this.houseZip = houseZip;
    else
      this.houseZip = "-1";
  }


  public String getHouseFlag()
  {
    String result = "";
    if(!this.houseFlag.equals("-1"))
    {
      result = this.houseFlag;
    }
    return result;
  }
  public void setHouseFlag(String houseFlag)
  {
    if(!isBlank(houseFlag))
      this.houseFlag = houseFlag;
    else
      this.houseFlag = "-1";
  }

  public String getSoftwareFlag()
  {
    String result = "";
    if(!this.softwareFlag.equals("-1"))
    {
      result = this.softwareFlag;
    }
    return result;
  }
  public void setSoftwareFlag(String softwareFlag)
  {
    if(!isBlank(softwareFlag))
      this.softwareFlag = softwareFlag;
    else
      this.softwareFlag = "-1";
  }

  public String getHouseName()
  {
    String result = "";
    if(!this.houseName.equals("-1"))
    {
      result = this.houseName;
    }
    return result;
  }
  public void setHouseName(String houseName)
  {
    if(!isBlank(houseName))
      this.houseName = houseName;
    else
      this.houseName = "-1";
  }

  public String getSoftwareComment()
  {
    String result = "";
    if(!this.softwareComment.equals("-1"))
    {
      result = this.softwareComment;
    }
    return result;
  }
  public void setSoftwareComment(String softwareComment)
  {
    if(!isBlank(softwareComment))
      this.softwareComment = softwareComment;
    else
      this.softwareComment = "-1";
  }

  public String getInventoryValue()
  {
    String result = "";
    if(!this.inventoryValue.equals("-1") && !this.inventoryValue.equals("-2"))
    {
      result = this.inventoryValue;
    }
    return result;
  }

  public String getAssociation()
  {
    return this.association;
  }

  public void setAssociation(String association)
  {
    this.association = association;
  }

  public void setInventoryValue(String inventoryValue)
  {
    try
    {
      if(isBlank(inventoryValue))
        inventoryValue = "-1";
      double temp = Double.parseDouble(inventoryValue);
      if(temp < 0.0)
      {
        this.inventoryValue = "-2";
      }
      else
      {
        this.inventoryValue = formatCurrency(inventoryValue);
      }
    }
    catch(Exception e)
    {
      this.inventoryValue = "-2";
    }
  }
  public void setTaxRate(String taxRate)
  {
    try
    {
      if(isBlank(taxRate))
        taxRate    = "0";
      this.taxRate = Double.parseDouble(taxRate);
    }
    catch(Exception e)
    {
      this.taxRate = -2;
    }
  }
  public String getTaxRate()
  {
    String result = "";
    if(this.taxRate != 0 && this.taxRate != -2)
    {
      result = String.valueOf(this.taxRate);
    }
    return result;
  }

  public boolean isRecalculated()
  {
    return this.recalculate;
  }

  public int getAppType()
  {
    return this.appType;
  }

  public void setRecalculate(String recalculate)
  {
    this.recalculate = true;
  }
  public boolean getRecalculate()
  {
    return this.recalculate;
  }
  public String getTarget()
  {
    String target = "";
    if(this.recalculate == true)
      target="equip";
    return target;
  }
  
  public void setAllowSubmit(String temp)
  {
    this.allowSubmit = true;
  }
  public boolean allowSubmit()
  {
    return this.allowSubmit;
  }

  public boolean isAmexZero()
  {
    return amexZero;
  }

  public void setAmexZero(String amexZero)
  {
    this.amexZero = true;
  }

  public boolean isMultiMerch()
  {
    return this.multiMerch;
  }

  public void setMultiMerch()
  {
    this.multiMerch = true;
  }


  public boolean isSeasonalMerch()
  {
    return this.seasonalMerch;
  }

  public void setSeasonalMerch()
  {
    this.seasonalMerch = true;
  }


  public void submitData(HttpServletRequest aReq, long appSeqNum)
  {
    submitGrid              (appSeqNum);
    submitCardInfo          (aReq, appSeqNum);
    submitMiscInfo          (aReq, appSeqNum);
    submitEquipInfo         (aReq, appSeqNum);
    submitSiteInspectionInfo(appSeqNum);
    submitBasisPointInfo    (appSeqNum);
  }

  private void submitBasisPointInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    
    try
    {
      qs.append("update merchant                  ");
      qs.append("set MERCH_BASIS_PT_INCREASE = ?, ");
      qs.append("    OFF_GRID_APPROVED_BY    = ?, ");
      qs.append("    EQUIP_CREDIT_BANK_NAME  = ?, ");
      qs.append("    MISC_CREDIT_BANK_NAME   = ?, ");
      qs.append("    ASSO_NUMBER             = ?, ");
      qs.append("    MERCH_DLY_DISCOUNT_FLAG = ?  ");
      qs.append("where app_seq_num           = ?  ");

      ps = getPreparedStatement(qs.toString());
      ps.setString(1, basisPointIncrease);
      ps.setString(2, offGridApprovedBy);
      ps.setString(3, nameOfBankEquipCredit);
      ps.setString(4, nameOfBankMiscCredit);
      ps.setString(5, association);
      ps.setString(6, dailyDiscount);
      ps.setLong(7,   appSeqNum);

      ps.executeUpdate();
      
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitBasisPointInfo: " + e.toString());
      addError("submitBasisPointInfo: " + e.toString());
    }
    finally
    {
      try { ps.close(); } catch(Exception e) {}
    }
  }


  private void submitGrid(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;

    try
    {
      qs.append("update merchant ");
      qs.append("set pricing_grid = ? ");
      qs.append("where app_seq_num = ?");

      ps = getPreparedStatement(qs.toString());
      ps.setInt(1, this.pricingGrid);
      ps.setLong(2, appSeqNum);

      ps.executeUpdate();

      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitGrid: " + e.toString());
      addError("submitGrid: " + e.toString());
    }
    finally
    {
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void submitEquipInfo(HttpServletRequest aReq, long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;

    try
    {
      switch(this.paySolOption)
      {
        case mesConstants.APP_PAYSOL_DIAL_TERMINAL :
        case mesConstants.APP_PAYSOL_DIAL_PAY:
        case mesConstants.APP_PAYSOL_WIRELESS_TERMINALS:
        case mesConstants.APP_PAYSOL_VIRTUAL_TERMINAL:
          qs.setLength(0);
          qs.append("update merchequipment set        ");
          qs.append("merchequip_amount          = ?,  ");
          qs.append("merchequip_credit_amount   = ?,  ");
          qs.append("merchequip_charge_amount   = ?,  ");
          qs.append("merchequip_equip_quantity  = ?   ");
          qs.append("where app_seq_num          = ?   ");
          qs.append("and equiplendtype_code     = ?   ");
          qs.append("and equip_model            = ?   ");

          ps = getPreparedStatement(qs.toString());

          for ( i = 0; i < this.equipmentInfoCount; i++ )
          {
            ps.clearParameters();
            ps.setString(1, (isBlank(aReq.getParameter("equipmentAmount" + i)) ? "0" : aReq.getParameter("equipmentAmount" + i)));
            ps.setString(2, (isBlank(aReq.getParameter("equipmentCreditAmount" + i)) ? "" : aReq.getParameter("equipmentCreditAmount" + i)));
            ps.setString(3, (isBlank(aReq.getParameter("equipmentChargeAmount" + i)) ? "" : aReq.getParameter("equipmentChargeAmount" + i)));
            ps.setString(4, aReq.getParameter("equipmentQuantity" + i));
            ps.setLong(5,   appSeqNum);
            ps.setString(6, aReq.getParameter("lendCode" + i));
            ps.setString(7, aReq.getParameter("equipModel" + i));

            if(ps.executeUpdate() != 1)
            {
              com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "merchequipment: insert failed");
              addError("merchequipment: Unable to insert/update record");
            }

          }
          ps.close();

          if (this.ownedEquipFlag.equals("Y"))
          {
            qs.setLength(0);
            qs.append("update merchequipment          ");
            qs.append("set merchequip_amount    = ?,  ");
            qs.append("merchequip_credit_amount = ?,  ");
            qs.append("merchequip_charge_amount = ?   ");
            qs.append("where app_seq_num        = ?   ");
            qs.append("and equiplendtype_code   = ?   ");

            ps = getPreparedStatement(qs.toString());
            ps.clearParameters();

            ps.setString(1,this.ownedEquipAmt);
            ps.setString(2, (this.ownedEquipCreditAmt.equals("-1") ? "" : this.ownedEquipCreditAmt));
            ps.setString(3, (this.ownedEquipChargeAmt.equals("-1") ? "" : this.ownedEquipChargeAmt));
            ps.setLong(4,appSeqNum);
            ps.setInt(5,mesConstants.APP_EQUIP_OWNED);
            ps.executeUpdate();
            ps.close();
          }

          //  Update the data entered here into the merchequipship table.
          updateMerchEquipShip(appSeqNum);

          break;

        case mesConstants.APP_PAYSOL_PC:
        case mesConstants.APP_PAYSOL_PC_CHARGE_EXPRESS:
        case mesConstants.APP_PAYSOL_PC_CHARGE_PRO:

          qs.setLength(0);
          qs.append("delete from merchequipment ");
          qs.append("where app_seq_num = ? and  ");
          qs.append("equip_model = ?            ");

          ps = getPreparedStatement(qs.toString());
          ps.setLong  (1, appSeqNum);
          ps.setString(2,"PCPS");

          ps.executeUpdate();
          ps.close();

          qs.setLength(0);
          qs.append("insert into merchequipment(  ");
          qs.append("app_seq_num,                 ");
          qs.append("equip_model,                 ");
          qs.append("equiplendtype_code,          ");
          qs.append("merchequip_amount,           ");
          qs.append("merchequip_credit_amount,    ");
          qs.append("merchequip_equip_quantity,   ");
          qs.append("equiptype_code)              ");
          qs.append("values (?,?,?,?,?,?,?)       ");

          ps = getPreparedStatement(qs.toString());
          ps.clearParameters();

          ps.setLong(1,   appSeqNum);
          ps.setString(2, "PCPS");
          ps.setInt(3,    mesConstants.APP_EQUIP_PURCHASE);
          ps.setString(4, this.pcFee);
          ps.setString(5, (this.pcCreditFee.equals("-1") ? "" : this.pcCreditFee));
          ps.setString(6, this.pcQuantity);
          ps.setInt(7,    mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE);

          if(ps.executeUpdate() != 1)
          {
            com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitPC: insert failed");
            addError("submitPC Unable to insert/update record");
          }

          ps.close();
          break;

        case mesConstants.APP_PAYSOL_INTERNET:
        default:
          break;
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
      addError("submitData: " + e.toString());
    }
    finally
    {
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void submitMiscInfo(HttpServletRequest aReq, long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;

    try
    {
      //delete all miscchrg info from miscchrg -> insert new info
      qs.append("delete from miscchrg ");
      qs.append("where app_seq_num = ? ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong  (1, appSeqNum);

      ps.executeUpdate();
      ps.close();

      qs.setLength(0);
      qs.append("insert into miscchrg(    ");
      qs.append("misc_chrg_amount,        ");
      qs.append("app_seq_num,             ");
      qs.append("misc_code,               ");
      qs.append("misc_bank_credit_amount, ");
      qs.append("misc_bank_chrg_amount,   ");
      qs.append("MISC_CHRGBASIS_CODE,     ");
      qs.append("MISC_CHRGBASIS_DESCR)    ");
      qs.append("values (?,?,?,?,?,?,?)   ");

      ps = getPreparedStatement(qs.toString());

      for (i=0; i < this.NUM_MISC; i++)
      {
        if(this.miscSelectedFlag[i].equals("Y"))
        {
          ps.clearParameters();
          ps.setString(1, this.miscFee[i]);
          ps.setLong(2,   appSeqNum);
          ps.setInt(3,    this.miscCodes[i]);
          ps.setString(4, (this.miscCreditAmount[i].equals("-1") ? "" :  this.miscCreditAmount[i]) );
          ps.setString(5, (this.miscChargeAmount[i].equals("-1") ? "" :  this.miscChargeAmount[i]) );

          if(i == FEE_UNIQUE)
          {
            ps.setString(6, "MC"); //we just always default it to monthly charge
            ps.setString(7, this.uniqueFeeDesc);
          }
          else
          {
            ps.setString(6, "");
            ps.setString(7, "");
          }

          if(ps.executeUpdate() != 1)
          {
            com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
            addError("submitData4: Unable to insert/update record");
          }
        }
      }
      
      if(!isBlank(monthlyDialpayFee))
      {
        ps.clearParameters();
        ps.setString(1, this.monthlyDialpayFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    mesConstants.APP_MISC_CHARGE_DIALPAY_MONTLY_FEE);
        ps.setString(4, "");
        ps.setString(5, "");
        ps.setString(6, "");
        ps.setString(7, "");
      
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
          addError("submitData4: Unable to insert/update record");
        }
      }

      if(!isBlank(monthlyMultiMerchFee))
      {
        ps.clearParameters();
        ps.setString(1, this.monthlyMultiMerchFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    mesConstants.APP_MISC_CHARGE_MULTI_MERCHANT_FEE);
        ps.setString(4, "");
        ps.setString(5, "");
        ps.setString(6, "");
        ps.setString(7, "");
      
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
          addError("submitData4: Unable to insert/update record");
        }
      }

      if(!isBlank(wirelessMonthlyFee))
      {
        ps.clearParameters();
        ps.setString(1, this.wirelessMonthlyFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    mesConstants.APP_MISC_CHARGE_WIRELESS_MONTHLY_FEE);
        ps.setString(4, "");
        ps.setString(5, "");
        ps.setString(6, "");
        ps.setString(7, "");
      
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
          addError("submitData4: Unable to insert/update record");
        }
      }

      if(!isBlank(wirelessSetupFee))
      {
        ps.clearParameters();
        ps.setString(1, this.wirelessSetupFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    mesConstants.APP_MISC_CHARGE_WIRELESS_SETUP_FEE);
        ps.setString(4, "");
        ps.setString(5, "");
        ps.setString(6, "");
        ps.setString(7, "");
      
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
          addError("submitData4: Unable to insert/update record");
        }
      }

      if(!isBlank(wirelessTransactionFee))
      {
        ps.clearParameters();
        ps.setString(1, this.wirelessTransactionFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    mesConstants.APP_MISC_CHARGE_WIRELESS_TRANSACTION_FEE);
        ps.setString(4, "");
        ps.setString(5, "");
        ps.setString(6, "");
        ps.setString(7, "");
      
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
          addError("submitData4: Unable to insert/update record");
        }
      }

      if(!isBlank(achRejectFee))
      {
        ps.clearParameters();
        ps.setString(1, this.achRejectFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    mesConstants.APP_MISC_CHARGE_ACH_REJECT_FEE);
        ps.setString(4, "");
        ps.setString(5, "");
        ps.setString(6, "");
        ps.setString(7, "");
      
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
          addError("submitData4: Unable to insert/update record");
        }
      }

      if(!isBlank(annualFee))
      {
        ps.clearParameters();
        ps.setString(1, this.annualFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    mesConstants.APP_MISC_CHARGE_ANNUAL_FEE);
        ps.setString(4, "");
        ps.setString(5, "");
        ps.setString(6, "");
        ps.setString(7, "");
      
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
          addError("submitData4: Unable to insert/update record");
        }
      }

      if(!isBlank(posMonthlyFee) && !posMonthlyFee.equals("-1"))
      {
        ps.clearParameters();
        ps.setString(1, this.posMonthlyFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    mesConstants.APP_MISC_CHARGE_POS_MONTHLY_SERVICE_FEE);
        ps.setString(4, (this.posMonthlyFeeCredit.equals("-1") ? "" : this.posMonthlyFeeCredit));
        ps.setString(5, "");
        ps.setString(6, "");
        ps.setString(7, "");
      
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
          addError("submitData4: Unable to insert/update record");
        }
      }

      if(!isBlank(pcchargeMonthlyFee) && !pcchargeMonthlyFee.equals("-1"))
      {
        ps.clearParameters();
        ps.setString(1, this.pcchargeMonthlyFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    mesConstants.APP_MISC_CHARGE_PCCHARGE_MONTHLY_SERVICE_FEE);
        ps.setString(4, (this.pcchargeMonthlyFeeCredit.equals("-1") ? "" : this.pcchargeMonthlyFeeCredit));
        ps.setString(5, "");
        ps.setString(6, "");
        ps.setString(7, "");
      
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
          addError("submitData4: Unable to insert/update record");
        }
      }

      if(!isBlank(setupKitFee) && !setupKitFee.equals("-1"))
      {
        ps.clearParameters();
        ps.setString(1, this.setupKitFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    mesConstants.APP_MISC_CHARGE_SETUP_KIT_FEE);
        ps.setString(4, (this.setupKitBankCredit.equals("-1") ? "" :  this.setupKitBankCredit));
        ps.setString(5, (this.setupKitBankCharge.equals("-1") ? "" :  this.setupKitBankCharge));
        ps.setString(6, "");
        ps.setString(7, "");
      
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
          addError("submitData4(setup kit fee): Unable to insert/update record");
        }
      }

      if(!isBlank(referralAuthFee) && !referralAuthFee.equals("-1"))
      {
        ps.clearParameters();
        ps.setString(1, this.referralAuthFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    mesConstants.APP_MISC_CHARGE_REFERRAL_AUTH_FEE);
        ps.setString(4, "");
        ps.setString(5, "");
        ps.setString(6, "");
        ps.setString(7, "");
      
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
          addError("submitData4(referralAuthFee): Unable to insert/update record");
        }
      }

      if(!isBlank(monthlyMembershipFee) && !monthlyMembershipFee.equals("-1"))
      {
        ps.clearParameters();
        ps.setString(1, this.monthlyMembershipFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    mesConstants.APP_MISC_CHARGE_MONTHLY_MEMBERSHIP_FEE);
        ps.setString(4, (this.monthlyMembershipBankCredit.equals("-1") ? "" :  this.monthlyMembershipBankCredit));
        ps.setString(5, (this.monthlyMembershipBankCharge.equals("-1") ? "" :  this.monthlyMembershipBankCharge));
        ps.setString(6, "");
        ps.setString(7, "");
      
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
          addError("submitData4(monthlyMembershipFee): Unable to insert/update record");
        }
      }

      if(!isBlank(thirdPartySoftwareFee) && !thirdPartySoftwareFee.equals("-1"))
      {
        ps.clearParameters();
        ps.setString(1, this.thirdPartySoftwareFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    mesConstants.APP_MISC_CHARGE_THIRD_PARTY_SOFTWARE_FEE);
        ps.setString(4, (this.thirdPartySoftwareBankCredit.equals("-1") ? "" :  this.thirdPartySoftwareBankCredit));
        ps.setString(5, (this.thirdPartySoftwareBankCharge.equals("-1") ? "" :  this.thirdPartySoftwareBankCharge));
        ps.setString(6, "");
        ps.setString(7, "");
      
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
          addError("submitData4(thirdPartySoftwareFee): Unable to insert/update record");
        }
      }

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: " + e.toString());
      addError("submitData4: " + e.toString());
    }
    finally
    {
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void submitCardInfo(HttpServletRequest aReq, long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;

    try
    {
      //delete all card info from tranchrg -> insert new info
      System.out.println("deleting everything from tranchrg");
      qs.append("delete from tranchrg ");
      qs.append("where app_seq_num = ? ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong  (1, appSeqNum);

      ps.executeUpdate();
      ps.close();

      qs.setLength(0);
      qs.append("insert into tranchrg(                    ");
      qs.append("tranchrg_mmin_chrg,                      ");
      qs.append("tranchrg_discrate_type,                  ");
      qs.append("tranchrg_disc_rate,                      ");
      qs.append("tranchrg_pass_thru,                      ");
      qs.append("tranchrg_per_tran,                       ");
      qs.append("tranchrg_float_disc_flag,                ");
      qs.append("app_seq_num,                             ");
      qs.append("cardtype_code,                           ");
      qs.append("tranchrg_per_auth,                       ");
      qs.append("tranchrg_per_capture,                    ");
      qs.append("non_qualification_downgrade,             ");
      qs.append("tranchrg_interchangefee_type,            ");
      qs.append("tranchrg_interchangefee_fee,             ");
      qs.append("tranchrg_aru_fee,                        ");
      qs.append("tranchrg_voice_fee,                      ");
      qs.append("tranchrg_mmin_bank_credit,               ");
      qs.append("tranchrg_mmin_bank_charge)               ");
      qs.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

      ps = getPreparedStatement(qs.toString());

      // The values for updating the data for Visa card.
      ps.setLong(7, appSeqNum);
      ps.setInt(8,mesConstants.APP_CT_VISA); //cardtype code
      ps.setInt(2,this.pricingScenario); //discount rate type

      String tempMinMonth = Integer.toString(TRANSACTION_MIN_CHARGE);
      switch(this.pricingScenario)
      {
        case mesConstants.APP_PS_VARIABLE_RATE: // floating discount rate
          if(appType == mesConstants.APP_TYPE_SUMMIT || 
             appType == mesConstants.APP_TYPE_GOLD   ||
             appType == mesConstants.APP_TYPE_MESB)
          {
            tempMinMonth = this.minMonthDiscount.equals("-1") ? "" : this.minMonthDiscount; //transaction min charge
          }
  
          ps.setString(1, tempMinMonth); //transaction min charge
          ps.setString(6,  "Y");
          ps.setNull  (3,  java.sql.Types.FLOAT );
          ps.setNull  (4,  java.sql.Types.FLOAT );
          if( appType == mesConstants.APP_TYPE_MES )
          {
            ps.setString(5, this.cardFee[CARD_VMC]);
          }
          else
          {
            ps.setString(5,  "0"); //charge per transaction
          }
          ps.setNull  (9,  java.sql.Types.FLOAT );
          ps.setNull  (10, java.sql.Types.FLOAT );
          ps.setNull  (11, java.sql.Types.FLOAT );
          ps.setString(12, "");
          ps.setString(13, "");
          ps.setNull  (14,java.sql.Types.FLOAT );
          ps.setNull  (15,java.sql.Types.FLOAT );
          ps.setNull  (16,java.sql.Types.FLOAT );
          ps.setNull  (17,java.sql.Types.FLOAT );
          break;

        case mesConstants.APP_PS_FIXED_RATE: // discount rate
          if(appType == mesConstants.APP_TYPE_SUMMIT || 
             appType == mesConstants.APP_TYPE_GOLD   ||
             appType == mesConstants.APP_TYPE_MESB)
          {
            tempMinMonth = this.minMonthDiscount.equals("-1") ? "" : this.minMonthDiscount; //transaction min charge
          }
  
          ps.setString(1, tempMinMonth); //transaction min charge
          ps.setString(6,  "N");
          ps.setDouble(3,  this.discountRate);
          ps.setNull  (4,  java.sql.Types.FLOAT );
          if( appType == mesConstants.APP_TYPE_MES )
          {
            ps.setString(5, this.cardFee[CARD_VMC]);
          }
          else
          {
            ps.setString(5,  "0"); //charge per transaction
          }

          if(this.visamcFee.equals("-1"))
          {
            ps.setNull(9,java.sql.Types.FLOAT );
          }
          else
          {
            ps.setString(9,this.visamcFee);
          }

          ps.setNull  (10, java.sql.Types.FLOAT );
          ps.setNull  (11, java.sql.Types.FLOAT );
          ps.setString(12, "");
          ps.setString(13, "");
          ps.setNull  (14,java.sql.Types.FLOAT );
          ps.setNull  (15,java.sql.Types.FLOAT );
          ps.setNull  (16,java.sql.Types.FLOAT );
          ps.setNull  (17,java.sql.Types.FLOAT );
          break;

        case mesConstants.APP_PS_INTERCHANGE: // interchange pass through
          if(appType == mesConstants.APP_TYPE_SUMMIT || 
             appType == mesConstants.APP_TYPE_GOLD   ||
             appType == mesConstants.APP_TYPE_MESB)
          {
            tempMinMonth = this.minMonthDiscount.equals("-1") ? "" : this.minMonthDiscount; //transaction min charge
          }
  
          ps.setString(1, tempMinMonth); //transaction min charge
          ps.setString(6,  "N");

          if(this.interchangeDiscountRate != -1)
          {
            ps.setDouble(3,  this.interchangeDiscountRate );
          }
          else
          {
            ps.setNull(3, java.sql.Types.FLOAT);
          }

          ps.setDouble(4,  this.interchangePassthrough);
          if( appType == mesConstants.APP_TYPE_MES )
          {
            ps.setString(5, this.cardFee[CARD_VMC]);
          }
          else
          {
            ps.setString(5,  "0"); //charge per transaction
          }

          if(this.visamcFee.equals("-1"))
          {
            ps.setNull(9,java.sql.Types.FLOAT );
          }
          else
          {
            ps.setString(9,this.visamcFee);
          }

          ps.setNull  (10, java.sql.Types.FLOAT );
          ps.setNull  (11, java.sql.Types.FLOAT );
          ps.setString(12, "");
          ps.setString(13, "");
          ps.setNull  (14,java.sql.Types.FLOAT );
          ps.setNull  (15,java.sql.Types.FLOAT );
          ps.setNull  (16,java.sql.Types.FLOAT );
          ps.setNull  (17,java.sql.Types.FLOAT );
          break;

        case mesConstants.APP_PS_FIXED_PLUS_PER_ITEM: // interchange pass through
        case mesConstants.IMS_MOTO_INTERNET_DIALPAY_PLAN:
        case mesConstants.IMS_RETAIL_RESTAURANT_PLAN:
          
          if(appType == mesConstants.APP_TYPE_DISCOVER                || 
             appType == mesConstants.APP_TYPE_DISCOVER_IMS            || 
             appType == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL  || 
             appType == mesConstants.APP_TYPE_SUMMIT                  || 
             appType == mesConstants.APP_TYPE_GOLD                    ||
             appType == mesConstants.APP_TYPE_MESB)
          {
            tempMinMonth = this.minMonthDiscount.equals("-1") ? "" : this.minMonthDiscount; //transaction min charge
          }
          
          ps.setString(1, tempMinMonth); //transaction min charge
          ps.setString(6, "N");
          ps.setDouble(3, this.discountRate4 );

          if(this.interchangePassthrough4 == -1)// || appType == mesConstants.APP_TYPE_VERISIGN || appType == mesConstants.APP_TYPE_NSI)
          {
            ps.setNull(4,java.sql.Types.FLOAT );
          }
          else
          {
            ps.setDouble(4,this.interchangePassthrough4);
          }

          if( appType == mesConstants.APP_TYPE_MES )
          {
            ps.setString(5, this.cardFee[CARD_VMC]);
          }
          else
          {
            ps.setString(5,  "0"); //charge per transaction
          }
          
          if(this.visamcFee.equals("-1"))
          {
            ps.setNull(9,java.sql.Types.FLOAT );
          }
          else
          {
            ps.setString(9,this.visamcFee);
          }

          ps.setNull  (10, java.sql.Types.FLOAT );
          ps.setNull  (11, java.sql.Types.FLOAT );
          ps.setString(12, "");
          ps.setString(13, "");
          ps.setNull  (14,java.sql.Types.FLOAT );
          ps.setNull  (15,java.sql.Types.FLOAT );
          ps.setNull  (16,java.sql.Types.FLOAT );
          ps.setNull  (17,java.sql.Types.FLOAT );
          break;

        //cbt pricing scenarios

        case mesConstants.CBT_APP_BUNDLED_RATE_PLAN: // discount rate
          ps.setString(1,  (this.minMonthDiscount.equals("-1") ? "" : this.minMonthDiscount)); //transaction min charge
          ps.setString(6,  "N");
          ps.setDouble(3,  this.cbtDiscountRate5);
          ps.setNull  (4,  java.sql.Types.FLOAT );
          ps.setString(5,  "0"); //charge per transaction
          ps.setNull  (9,  java.sql.Types.FLOAT );
          ps.setNull  (10, java.sql.Types.FLOAT );
          ps.setNull  (11, java.sql.Types.FLOAT );
          ps.setString(12, "");
          ps.setString(13, "");
          ps.setNull  (14,java.sql.Types.FLOAT );
          ps.setNull  (15,java.sql.Types.FLOAT );
          ps.setNull  (16,java.sql.Types.FLOAT );
          ps.setNull  (17,java.sql.Types.FLOAT );
          break;

        case mesConstants.CBT_APP_UNBUNDLED_RATE_PLAN: // interchange pass through
          ps.setString(1,   (this.minMonthDiscount.equals("-1") ? "" : this.minMonthDiscount)); //transaction min charge
          ps.setString(6,   "N");
          ps.setDouble(3,   this.cbtDiscountRate6 );
          ps.setNull  (4,   java.sql.Types.FLOAT );
          ps.setDouble(5,   this.cbtPerItem6); //charge per transaction
          ps.setDouble(9,   this.cbtPerAuth6 );
          ps.setNull  (10,  java.sql.Types.FLOAT );
          ps.setNull  (11,  java.sql.Types.FLOAT );
          ps.setString(12,  "");
          ps.setString(13, "");
          ps.setNull  (14,java.sql.Types.FLOAT );
          ps.setNull  (15,java.sql.Types.FLOAT );
          ps.setNull  (16,java.sql.Types.FLOAT );
          ps.setNull  (17,java.sql.Types.FLOAT );
          break;

        case mesConstants.CBT_APP_INTERCHANGE_PLUS_PLAN: // interchange pass through
          ps.setString(1,  (this.minMonthDiscount.equals("-1") ? "" : this.minMonthDiscount)); //transaction min charge
          ps.setString(6,  "N");
          ps.setNull  (3,  java.sql.Types.FLOAT );
          ps.setNull  (4,  java.sql.Types.FLOAT );
          ps.setDouble(5,  this.cbtPerItem7); //charge per transaction
          ps.setDouble(9,  this.cbtPerAuth7 );
          ps.setDouble(10, this.cbtPerCap7 );
          ps.setNull  (11, java.sql.Types.FLOAT );
          ps.setString(12, "");
          ps.setString(13, "");
          ps.setNull  (14,java.sql.Types.FLOAT );
          ps.setNull  (15,java.sql.Types.FLOAT );
          ps.setNull  (16,java.sql.Types.FLOAT );
          ps.setNull  (17,java.sql.Types.FLOAT );
          break;

        case mesConstants.CBT_APP_PERITEM_DISCRATE_PLAN: // disc rate & per item
          ps.setString(1,  (this.minMonthDiscount.equals("-1") ? "" : this.minMonthDiscount)); //transaction min charge
          ps.setString(6,  "N");
          ps.setDouble(3,  this.cbtDiscountRate8 );
          ps.setNull  (4,  java.sql.Types.FLOAT );

          if(this.cbtPerItem8 == -1)
          {
            ps.setString(5,"0.0");
          }
          else
          {
            ps.setDouble(5, this.cbtPerItem8); //charge per transaction
          }

          if(this.visamcFee.equals("-1"))
          {
            ps.setNull(9,java.sql.Types.FLOAT );
          }
          else
          {
            ps.setString(9,this.visamcFee);
          }

          ps.setNull(10,  java.sql.Types.FLOAT );
          ps.setNull(11,  java.sql.Types.FLOAT );
          ps.setInt (12,  this.interchangeType);

          
          if(this.interchangeType == mesConstants.CBT_APP_INTERCHANGE_TYPE_RETAIL)
          {
            if(isBlank(this.interchangeTypeFeeRetail))
            {
              ps.setNull(13,java.sql.Types.FLOAT );
            }
            else
            {
              ps.setString(13,this.interchangeTypeFeeRetail);
            }
          }
          else if(this.interchangeType == mesConstants.CBT_APP_INTERCHANGE_TYPE_MOTO_LODGING)
          {
            if(isBlank(this.interchangeTypeFeeMotoLodging))
            {
              ps.setNull(13,java.sql.Types.FLOAT );
            }
            else
            {
              ps.setString(13,this.interchangeTypeFeeMotoLodging);
            }
          }
          else
          {
            ps.setNull(13,java.sql.Types.FLOAT );
          }


          if(this.aruFee.equals("-1"))
          {
            ps.setNull(14,java.sql.Types.FLOAT );
          }
          else
          {
            ps.setString(14,this.aruFee);
          }

          if(this.voiceFee.equals("-1"))
          {
            ps.setNull(15,java.sql.Types.FLOAT );
          }
          else
          {
            ps.setString(15,this.voiceFee);
          }

          ps.setString  (16, (this.minMonthDiscountBankCredit.equals("-1") ? "" : this.minMonthDiscountBankCredit));
          ps.setString  (17, (this.minMonthDiscountBankCharge.equals("-1") ? "" : this.minMonthDiscountBankCharge));

          break;

        default:
          break;
      }

      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData1: insert failed");
        addError("submitData1: Unable to insert/update record");
      }

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData1: " + e.toString());
      addError("submitData1: " + e.toString());
    }
    try
    {
      // The values for updating data for Master Card.
      ps.setInt(8,mesConstants.APP_CT_MC); //  change card type code, all else same
      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData2: insert failed");
        addError("submitData2: Unable to insert/update record");
      }
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData2: " + e.toString());
      addError("submitData2: " + e.toString());
    }
    try
    {
      qs.setLength(0);
      qs.append("Insert into tranchrg(  ");
      qs.append("tranchrg_per_tran,     ");
      qs.append("app_seq_num,           ");
      qs.append("cardtype_code,         ");
      qs.append("tranchrg_per_auth)     ");
      qs.append("values(?,?,?,?)          ");

      ps = getPreparedStatement(qs.toString());

      for ( i = 0; i < this.NUM_CARDS-1; i++ )
      {
        ps.clearParameters();
        if(this.cardSelectedFlag[i].equals("Y"))
        {
          ps.setString(1, this.cardFee[i]);
          ps.setLong(2, appSeqNum);
          ps.setInt(3, this.cardCodes[i]);
          
          if(i == CARD_INTERNET)
          {
            ps.setDouble(4, verisignOverAuthPerItem);
          }
          else
          {
            ps.setDouble(4, 0.0);
          }

          if(ps.executeUpdate() != 1)
          {
            com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData3: insert failed");
            addError("submitData3: Unable to insert/update record");
          }
        }
      }
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData3: " + e.toString());
      addError("submitData3: " + e.toString());
    }
  }
  private void updateMerchEquipShip(long appSeqNum)
  {
    StringBuffer      qs = new StringBuffer("");
    PreparedStatement ps = null;
    ResultSet         rs = null;
    try
    {
      qs.append("delete from merchequipship ");
      qs.append("where app_seq_num = ? ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong  (1, appSeqNum);

      ps.executeUpdate();
      ps.close();

      qs.setLength(0);
      qs.append("select app_seq_num,          ");
      qs.append("equip_model,                 ");
      qs.append("merchequip_equip_quantity,   ");
      qs.append("equiplendtype_code           ");
      qs.append("from merchequipment          ");
      qs.append("where app_seq_num = ? and    ");
      qs.append("equiplendtype_code in (1,2)  ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();

      ps.clearParameters();

      qs.setLength(0);
      qs.append("insert into merchequipship(  ");
      qs.append("app_seq_num,                 ");
      qs.append("equip_model,                 ");
      qs.append("merch_equipitem_num,         ");
      qs.append("equiplendtype_code)          ");
      qs.append("values(?,?,?,?)              ");

      ps = getPreparedStatement(qs.toString());

      while (rs.next())
      {
        for (int i = 1; i <= rs.getInt("merchequip_equip_quantity"); i++ )
        {
          ps.setLong  (1, appSeqNum);
          ps.setString(2, rs.getString("equip_model"));
          ps.setInt   (3, i);
          ps.setInt   (4, rs.getInt("equiplendtype_code"));

          if(ps.executeUpdate() != 1)
          {
            com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "updateMerchEquipShip: insert failed");
            addError("updateMerchEquipShip: Unable to insert/update record");
          }

          ps.clearParameters();
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "updateMerchEquipShip: " + e.toString());
      addError("updateMerchEquipShip: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  public void submitSiteInspectionInfo(long appSeqNum)
  {
    StringBuffer      qs    = new StringBuffer("");
    PreparedStatement ps    = null;
    ResultSet         rs    = null;

    try
    {
      qs.append("select app_seq_num     ");
      qs.append("from siteinspection    ");
      qs.append("where app_seq_num = ?  ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();
      qs.setLength(0);
      if(rs.next())
      {
        qs.append("update siteinspection set         ");
        qs.append("siteinsp_comment             = ?, ");
        qs.append("siteinsp_name_flag           = ?, ");
        qs.append("siteinsp_inv_sign_flag       = ?, ");
        qs.append("siteinsp_bus_hours_flag      = ?, ");
        qs.append("siteinsp_inv_viewed_flag     = ?, ");
        qs.append("siteinsp_inv_consistant_flag = ?, ");
        qs.append("siteinsp_vol_flag            = ?, ");
        qs.append("siteinsp_full_flag           = ?, ");
        qs.append("siteinsp_soft_flag           = ?, ");
        qs.append("siteinsp_inv_street          = ?, ");
        qs.append("siteinsp_inv_city            = ?, ");
        qs.append("siteinsp_inv_state           = ?, ");
        qs.append("siteinsp_inv_zip             = ?, ");
        qs.append("siteinsp_full_street         = ?, ");
        qs.append("siteinsp_full_city           = ?, ");
        qs.append("siteinsp_full_state          = ?, ");
        qs.append("siteinsp_full_zip            = ?, ");
        qs.append("siteinsp_bus_street          = ?, ");
        qs.append("siteinsp_bus_city            = ?, ");
        qs.append("siteinsp_bus_state           = ?, ");
        qs.append("siteinsp_bus_zip             = ?, ");
        qs.append("siteinsp_inv_value           = ?, ");
        qs.append("siteinsp_full_name           = ?, ");
        qs.append("siteinsp_no_of_emp           = ?, ");
        qs.append("siteinsp_bus_loc             = ?, ");
        qs.append("siteinsp_bus_loc_comment     = ?, ");
        qs.append("siteinsp_bus_address         = ?, ");
        qs.append("siteinsp_soft_name           = ?, ");
        qs.append("siteinsp_unique_fee_desc     = ?, ");
        qs.append("siteinsp_setupkit_type       = ?, ");
        qs.append("siteinsp_setupkit_size       = ?, ");
        qs.append("siteinsp_status              = ?, ");
        qs.append("siteinsp_num_reprog_terms    = ?, ");
        qs.append("siteinsp_num_reprog_prints   = ?, ");
        qs.append("siteinsp_num_imprinters      = ?, ");
        qs.append("siteinsp_supply_billing      = ?  ");
        qs.append("where app_seq_num            = ?  ");
      }
      else
      {
        qs.append("insert into siteinspection(    ");
        qs.append("siteinsp_comment,              ");
        qs.append("siteinsp_name_flag,            ");
        qs.append("siteinsp_inv_sign_flag,        ");
        qs.append("siteinsp_bus_hours_flag,       ");
        qs.append("siteinsp_inv_viewed_flag,      ");
        qs.append("siteinsp_inv_consistant_flag,  ");
        qs.append("siteinsp_vol_flag,             ");
        qs.append("siteinsp_full_flag,            ");
        qs.append("siteinsp_soft_flag,            ");
        qs.append("siteinsp_inv_street,           ");
        qs.append("siteinsp_inv_city,             ");
        qs.append("siteinsp_inv_state,            ");
        qs.append("siteinsp_inv_zip,              ");
        qs.append("siteinsp_full_street,          ");
        qs.append("siteinsp_full_city,            ");
        qs.append("siteinsp_full_state,           ");
        qs.append("siteinsp_full_zip,             ");
        qs.append("siteinsp_bus_street,           ");
        qs.append("siteinsp_bus_city,             ");
        qs.append("siteinsp_bus_state,            ");
        qs.append("siteinsp_bus_zip,              ");
        qs.append("siteinsp_inv_value,            ");
        qs.append("siteinsp_full_name,            ");
        qs.append("siteinsp_no_of_emp,            ");
        qs.append("siteinsp_bus_loc,              ");
        qs.append("siteinsp_bus_loc_comment,      ");
        qs.append("siteinsp_bus_address,          ");
        qs.append("siteinsp_soft_name,            ");
        qs.append("siteinsp_unique_fee_desc,      ");
        qs.append("siteinsp_setupkit_type,        ");
        qs.append("siteinsp_setupkit_size,        ");
        qs.append("siteinsp_status,               ");
        qs.append("siteinsp_num_reprog_terms,     ");
        qs.append("siteinsp_num_reprog_prints,    ");
        qs.append("siteinsp_num_imprinters,       ");
        qs.append("siteinsp_supply_billing,       ");
        qs.append("app_seq_num)                   ");
        qs.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
      }
      rs.close();
      ps.close();

      ps = getPreparedStatement(qs.toString());
      ps.setString(1, this.comment);
      ps.setString(2, this.questionFlag[0]);
      ps.setString(3, this.questionFlag[1]);
      ps.setString(4, this.questionFlag[2]);
      ps.setString(5, this.questionFlag[3]);
      ps.setString(6, this.questionFlag[4]);
      ps.setString(7, this.questionFlag[5]);

      if(this.houseFlag.equals("-1"))
      {
        this.houseFlag = "N";
      }
      
      ps.setString(8, this.houseFlag);

      if(this.softwareFlag.equals("-1"))
      {
        this.softwareFlag = "N";
      }

      ps.setString(9, this.softwareFlag);

      ps.setString(10, this.inventoryAddressStreet);
      ps.setString(11, this.inventoryAddressCity);
      ps.setString(12, this.inventoryAddressState);
      ps.setString(13, this.inventoryAddressZip);
      ps.setString(14, this.houseStreet);
      ps.setString(15, this.houseCity);
      ps.setString(16, this.houseState);
      ps.setString(17, this.houseZip);
      ps.setString(18, this.businessAddressStreet);
      ps.setString(19, this.businessAddressCity);
      ps.setString(20, this.businessAddressState);
      ps.setString(21, this.businessAddressZip);
      ps.setString(22, this.inventoryValue);
      ps.setString(23, this.houseName);
      ps.setString(24, this.numEmployees);
      ps.setString(25, this.businessLocation);
      ps.setString(26, this.businessLocationComment);
      ps.setString(27, this.businessAddress);
      ps.setString(28, this.softwareComment);
      ps.setString(29, this.uniqueFeeDesc);
      ps.setString(30, this.setupKitType);
      ps.setString(31, this.setupKitSize);
      ps.setString(32, this.siteInspectionStatus);
      ps.setString(33, this.numReprogTerms);
      ps.setString(34, this.numReprogPrints);
      ps.setString(35, this.numImprinters);
      ps.setString(36, this.supplyBilling);
      ps.setLong(37,   appSeqNum);

      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitSiteInspectionInfo: update failed");
        addError("submitSiteInspectionInfo: Unable to insert/update record");
      }

      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitSiteInspectionInfo: " + e.toString());
      addError("submitSiteInspectionInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }
  
  public boolean isPayflowLink()
  {
    return payflowLink;
  }
  public boolean isPayflowPro()
  {
    return payflowPro;
  }
  
  public String getVerisignOverAuthPerItem()
  {
    String result =  "";
    
    result = Double.toString(verisignOverAuthPerItem);
    
    return result;
  }
  public void setVerisignOverAuthPerItem(String perItem)
  {
    try
    {
      if(!isBlank(perItem))
      {
        verisignOverAuthPerItem = Double.parseDouble(perItem);
      }
    }
    catch(Exception e)
    {
      verisignOverAuthPerItem = 0.0;
    }
  }
  
}
