/*@lineinfo:filename=VsMerchInfoBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/VsMerchInfoBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/03/03 3:29p $
  Version            : $Revision: 28 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.setup;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;

public class VsMerchInfoBean extends MerchInfoBean
{
  
  private   int       pageNum          = 0;
  private   int       completed        = 0;
  private   HashMap   errorMap         = new HashMap();
  private   String    login            = "";
  private   String    pass1            = "";
  private   String    pass2            = "";
  private   boolean   alreadyUsed      = false;
  /*
  ** METHOD submitData
  */
  public void submitData(HttpServletRequest req, long appSeqNum)
  {
      //submitApplicationData(req, appSeqNum);
    switch(this.pageNum)
    {
      case 1:
        submitMerchantData1(req, appSeqNum);
        submitAddressData(appSeqNum,mesConstants.ADDR_TYPE_BUSINESS);
        submitAddressData(appSeqNum,mesConstants.ADDR_TYPE_MAILING);
        submitMerchContactData(appSeqNum);
        submitMerchPosData(appSeqNum);
        //updateLogin(appSeqNum);
        break;
      case 2:
        submitAddressData(appSeqNum,mesConstants.ADDR_TYPE_OWNER1);
        submitAddressData(appSeqNum,mesConstants.ADDR_TYPE_OWNER2);
        submitAddressData(appSeqNum,mesConstants.ADDR_TYPE_CHK_ACCT_BANK);
        submitBusinessOwnerData(appSeqNum,1);
        submitBusinessOwnerData(appSeqNum,2);
        submitMerchBankData(appSeqNum);
        break;
      case 3:
        submitMerchantData2(req, appSeqNum);
        submitMerchPayOptionData(appSeqNum);
        submitMerchEquipmentData(appSeqNum);

        //if selected shopping cart.. change pos_code to reflect that
        if(isShoppingCart())
        {
          submitShoppingCartInfo(appSeqNum);
        }

        break;
      case 4:
        submitMerchantPass(appSeqNum);
        break;
      case 5:
        submitMerchantAgreement(appSeqNum);
        MesPricingBean pricingBean = new MesPricingBean();
        pricingBean.getData(appSeqNum);
        pricingBean.cleanUp();
        break;
    }
  }
  
  /*
  ** METHOD validate
  */  
  public boolean validate()
  {
    int num;
    long longNum;
    boolean result = false;
    String tempAdd = "";

    switch(pageNum)
    {
      case 1:
        if (isBlank(getBusinessName()))
        {
          errorMap.put("businessName", "Please provide a business name (DBA)");
        }
  
        if (isBlank(getBusinessLegalName()))
        {
          errorMap.put("businessLegalName", "Please provide a legal business name");    
        }
  
        if (!isValidTaxId(getTaxpayerId()))
        {
          errorMap.put("taxpayerId", "Please provide a valid taxpayer ID/SSN (e.g. 142-12-9874)");
        }
        else if(countDigits(getTaxpayerId()) != MAX_SSN)
        {
          errorMap.put("taxpayerId", "Please provide a valid 9 digit taxpayer ID/SSN (e.g. 142-12-9874)");
        }
  
        if (!isValidPhone(getBusinessPhone()))
        {
          errorMap.put("businessPhone", "Please provide a valid business phone number with area code (e.g. 650-555-0909)");
        }
    
        if (isBlank(getBusinessAddress1()))
        {
          errorMap.put("businessAddress1", "Please provide a business address");
        }
        else
        {
          tempAdd = ((getBusinessAddress1()).toLowerCase()).trim();
          if(tempAdd.startsWith("p.o. box") || tempAdd.startsWith("p.o box") || tempAdd.startsWith("po box") || tempAdd.startsWith("p.o.box") || tempAdd.startsWith("p.obox") || tempAdd.startsWith("pobox") || tempAdd.startsWith("po. box") || tempAdd.startsWith("po.box"))
          {
            errorMap.put("businessAddress1", "No P.O. Boxes please");
          }
        }

        if (!isBlank(getBusinessAddress2()))
        {
          tempAdd = ((getBusinessAddress2()).toLowerCase()).trim();
          if(tempAdd.startsWith("p.o. box") || tempAdd.startsWith("p.o box") || tempAdd.startsWith("po box") || tempAdd.startsWith("p.o.box") || tempAdd.startsWith("p.obox") || tempAdd.startsWith("pobox") || tempAdd.startsWith("po. box") || tempAdd.startsWith("po.box"))
          {
            errorMap.put("businessAddress2", "No P.O. Boxes please");
          }
        }
        
        if (isBlank(getBusinessCity()))
        {
          errorMap.put("businessCity", "Please provide a business city");
        }
  
        if (getBusinessState() == 0)
        {
          errorMap.put("businessState", "Please select a business state");
        }
  
        num = countDigits(getBusinessZip());
        if (isBlank(getBusinessZip()) || (num != 5 && num != 9))
        {
          errorMap.put("businessZip", "Please provide a valid 5 or 9 digit business zip (e.g. 94015 or 94015-4587)");
        }
     
        if (isBlank(getBusinessEmail()) || !isEmail(getBusinessEmail()))
        {
          errorMap.put("businessEmail", "Please provide a valid e-mail address (e.g. joe@aol.com)");
        }

        num = parseInt(getEstablishedYear());
        if (getEstablishedMonth().equals("0") || (num < 1000 || num > 9999))
        {
          errorMap.put("establishedDate", "Please select the month and provide a valid 4-digit year the business was established");
        }
  
        if(isBlank(getContactNameFirst()))
        {
          errorMap.put("contactNameFirst", "Please provide a contact first name");
        }

        if(isBlank(getContactNameLast()))
        {
          errorMap.put("contactNameLast", "Please provide a contact last name");
        }

        if (!isValidPhone(getContactPhone()))
        {
          errorMap.put("contactPhone", "Please provide a valid contact phone number with area code (e.g. 650-555-0909)");
        }
  
        if (!isBlank(getMailingAddress1()))
        {
          tempAdd = ((getMailingAddress1()).toLowerCase()).trim();
          if(tempAdd.startsWith("p.o. box") || tempAdd.startsWith("p.o box") || tempAdd.startsWith("po box") || tempAdd.startsWith("p.o.box") || tempAdd.startsWith("p.obox") || tempAdd.startsWith("pobox") || tempAdd.startsWith("po. box") || tempAdd.startsWith("po.box"))
          {
            errorMap.put("mailingAddress1", "No P.O. Boxes please");
          }
        }
        
        if (!isBlank(getMailingAddress2()))
        {
          tempAdd = ((getMailingAddress2()).toLowerCase()).trim();
          if(tempAdd.startsWith("p.o. box") || tempAdd.startsWith("p.o box") || tempAdd.startsWith("po box") || tempAdd.startsWith("p.o.box") || tempAdd.startsWith("p.obox") || tempAdd.startsWith("pobox") || tempAdd.startsWith("po. box") || tempAdd.startsWith("po.box"))
          {
            errorMap.put("mailingAddress2", "No P.O. Boxes please");
          }
        }

        num = countDigits(getMailingZip());
        if (!isBlank(getMailingZip()) && num != 5 && num != 9)
        {
          errorMap.put("mailingZip", "Please provide a valid 5 or 9 digit business zip (e.g. 94015 or 94015-4587)");
        }
  
        num = countDigits(getBusinessFax());
        if (!isBlank(getBusinessFax()) && !(getBusinessFax()).equals("0") && countDigits(getBusinessFax()) != 10)
        {
          errorMap.put("businessFax", "Please provide a valid business fax number with area code (e.g. 650-555-0909)");
        }

        if (getBusinessType() == 0)
        {
          errorMap.put("businessType", "Please select a Business Type");
        }
  
        if (getIndustryType() == 0)
        {
          errorMap.put("industryType", "Please select an Industry Type");
        }
  
        if (isBlank(getProduct()))
        {
          errorMap.put("product", "Please provide a business description (max 200 characters)");
        }
        else if((getProduct()).length() > 200)
        {
          errorMap.put("product", ("Business Description (currently " + (getProduct()).length() + " characters) must be less than 200 characters"));
        }
  
        if (getApplicationType() == 0)
        {
          addError("Please select Type of Account Setup");
        }
  
        if (isBlank(getNumLocations()))
        {
          addError("Please provide the Number of Locations");
        }
  
        if (getLocationType().equals("0"))
        {
          addError("Please select a Location Type");
        }
  
        if (isBlank(getLocationYears()))
        {
          errorMap.put("locationYears", "Please provide the number of years at location");
        }
        
        if (isBlank(getWebUrl()))
        {
          errorMap.put("webUrl", "Please provide the Business Store URL (e.g. http://www.example.com)");
        }
        else
        {
          String tempURL = (getWebUrl()).trim();
          tempURL = tempURL.toLowerCase();
          if(tempURL.startsWith("http://"))
          {}
          else if(tempURL.startsWith("https://"))
          {}
          else
          {
            setWebUrl("http://" + getWebUrl());
            //errorMap.put("webUrl", "Business Store URL must inlcude http:// or https:// (e.g. http://www.example.com or https://www.example.com)");
          }
        }
        break;
      case 2:

        if (isBlank(getBankName()))
        {
          errorMap.put("bankName", "Please provide the name of the bank your business checking account resides in");
        }
  
        if (isBlank(getCheckingAccount()))
        {
          errorMap.put("checkingAccount", "Please provide the account number of your business checking account.  This number can be found on your business check.");
        }
        
        num = parseInt(getTransitRouting());  
        if (isBlank(getTransitRouting()) || num == 0 || (getTransitRouting()).length() != 9)
        {
          errorMap.put("transitRouting", "Please provide the 9 digit transit routing number of your business checking account.  This number can be found on your business check.");
        }
        else if(getTransitRouting().equals("031101114") || getTransitRouting().equals("061091977"))
        {
          errorMap.put("transitRouting", "Please provide a valid transit routing number.");
        }

        if(isAccountConfirmationReq())
        {
          
          if(isBlank(getConfirmCheckingAccount()))
          {
            errorMap.put("confirmCheckingAccount", "Please re-enter the account number of your business checking account to validate it was entered correctly");
          }
          else if(!isBlank(getCheckingAccount()) && !getCheckingAccount().equals(getConfirmCheckingAccount()))
          {
            errorMap.put("confirmCheckingAccount", "Checking Account # and Confirmation of Checking Account # do not match");
          }

          if(isBlank(getConfirmTransitRouting()))
          {
            errorMap.put("confirmTransitRouting", "Please re-enter the transit routing number of your business checking account to validate it was entered correctly");
          }
          else if(!isBlank(getTransitRouting()) && !getTransitRouting().equals(getConfirmTransitRouting()))
          {
            errorMap.put("confirmTransitRouting", "Transit Routing # and Confirmation of Transit Routing # do not match");
          }
       
        }

        if(isAccountSourceTypeReq())
        {
          
          if(isBlank(getTypeOfAcct()))
          {
            errorMap.put("typeOfAcct", "Please select the type of account corresponding to the account number entered above");
          }
          /*
          else if(!isBlank(getCheckingAccount()) && !getCheckingAccount().equals(Integer.toString(mesConstants.BANK_ACCT_TYPE_CHECKING_ACCOUNT)) && !isSkipTypeError())
          {
            //this allows this error to only pop up once
            setSkipTypeError("true");
            errorMap.put("typeOfAcct", "Note: Limitations may exist for this type of account.");
          }
          */

          if(isBlank(getSourceOfInfo()))
          {
            errorMap.put("sourceOfInfo", "Please select where you obtained your account number and transit routing number information");
          }
       
        }



        num = parseInt(getYearsOpen());
        if(num == 0 && !((getYearsOpen()).equals("0")))
        {
          errorMap.put("yearsOpen", "Please provide the number of years your business checking account has been opened");
        }
  
        if (isBlank(getBankAddress()))
        {
          errorMap.put("bankAddress", "Please provide the address of the bank that holds your business checking account");
        }
  
        if (isBlank(getBankCity()))
        {
          errorMap.put("bankCity", "Please provide the City of the bank that holds your business checking account");
        }
  
        num = countDigits(getBankZip());
        if (isBlank(getBankZip()) || (num != 5 && num != 9))
        {
          errorMap.put("bankZip", "Please provide a valid 5 or 9 digit zip for the bank that holds your business checking account (e.g. 94015 or 94015-4587)");
        }
  
        if (getBankState().equals("0"))
        {
          errorMap.put("bankState", "Please select the state for the bank that holds your business checking account");
        }

        if (isBlank(getOwner1FirstName()))
        {
          errorMap.put("owner1FirstName", "Please provide the owner's first name");
        }
  
        if (isBlank(getOwner1LastName()))
        {
          errorMap.put("owner1LastName", "Please provide the owner's last name");
        }
  
        if (isBlank(getOwner1Address1()))
        {
          errorMap.put("owner1Address1", "Please provide the owner's residential address");
        }
  
        if (isBlank(getOwner1City()))
        {
          errorMap.put("owner1City", "Please provide the owner's city of residence");
        }

        num = countDigits(getOwner1Zip());    
        if (isBlank(getOwner1Zip()) || (num != 5 && num != 9))
        {
          errorMap.put("owner1Zip", "Please provide a valid 5 or 9 digit zip (e.g. 94015 or 94015-4587)");
        }
  
        num = countDigits(getOwner2Zip());
        if (!isBlank(getOwner2Zip()) && num != 5 && num != 9)
        {
          errorMap.put("owner2Zip", "Please provide a valid 5 or 9 digit zip (e.g. 94015 or 94015-4587)");
        }
    
        if (getOwner1State().equals("0"))
        {
          errorMap.put("owner1State", "Please select the owner's state of residence");
        }
  
        if (!isValidTaxId(getOwner1SSN()))
        {
          errorMap.put("owner1SSN", "Please provide a valid 9 digit SSN (e.g. 555-12-4788)");
        }
  
        num = parseInt(getOwner1Percent());
        if (isBlank(getOwner1Percent()) || (!isBlank(getOwner1Percent()) && (num <= 0 || num > 100)))
        {
          errorMap.put("owner1Percent", "Please provide a valid percentage of ownership (value must be between 0 and 100)");
        }
  
        num = parseInt(getOwner2Percent());
        if (!isBlank(getOwner2Percent()) && (num <= 0 || num > 100))
        {
          errorMap.put("owner2Percent", "Please provide a valid percentage of ownership (value must be between 0 and 100)");
        }
  
        num = parseInt(getOwner1SinceYear());
        if ((!isBlank(getOwner1SinceYear()) && num == 0) || (!isBlank(getOwner1SinceYear()) && (num < 1000 || num > 9999)))
        {
          errorMap.put("owner1SinceYear", "Please provide a valid 4-digit year");
        }

        num = parseInt(getOwner2SinceYear());
        if ((!isBlank(getOwner2SinceYear()) && num == 0) || (!isBlank(getOwner2SinceYear()) && (num < 1000 || num > 9999)))
        {
          errorMap.put("owner2SinceYear", "Please provide a valid 4-digit year");
        }
  
        if(!isBlank(getOwner1Phone()) && !(getOwner1Phone()).equals("0") && !isValidPhone(getOwner1Phone()))
        {
          errorMap.put("owner1Phone", "Please provide a valid phone number with area code (e.g. 650-555-0909)");
        }
  
        if(!isBlank(getOwner2Phone()) && !(getOwner2Phone()).equals("0") && !isValidPhone(getOwner2Phone()))
        {
          errorMap.put("owner2Phone", "Please provide a valid phone number with area code (e.g. 650-555-0909)");
        }
  
        if (!isBlank(getOwner2SSN()) && !isValidTaxId(getOwner2SSN()))
        {
          errorMap.put("owner2SSN", "Please provide a valid 9 digit SSN (e.g. 555-12-4788)");
        }
        break;    
      case 3:
        if (getHaveProcessed() == 0)
        {
          errorMap.put("haveProcessed", "Please select whether the business has accepted credit cards before");
        }
  
        if (getHaveProcessed() == 1 && isBlank(getPreviousProcessor()))
        {
          errorMap.put("previousProcessor", "Please provide the name of the previous processor");
        }
  
        if (getHaveCanceled() == 0)
        {
          errorMap.put("haveCanceled", "Please select whether the business has had an account canceled before");
        }
  
        if (getHaveCanceled() == 1)
        {
          if (isBlank(getCanceledProcessor()))
          {
            errorMap.put("canceledProcessor", "Please provide the name of the cancelled processor");
          }
    
          if (isBlank(getCancelReason()))
          {
            errorMap.put("cancelReason", "Please provide the reason the account was cancelled");
          }

          num = parseInt(getCancelYear());
          if (getCancelMonth() == 0 || (num < 1000 || num > 9999))
          {
            errorMap.put("cancelDate", "Please select the month, and provide a valid 4-digit year, the account was cancelled");
          }
        }

        double dol = parseDouble(getMonthlySales());
        num = countDigits(getMonthlySales());
        if (dol <= 0 || num > 10)
        {
          errorMap.put("monthlySales", "Please provide a valid projected monthly sales amount");
        }
  
        dol = parseDouble(getVmcSales());
        num = countDigits(getVmcSales());
        if (dol <= 0 || num > 10)
        {
          errorMap.put("vmcSales", "Please provide a valid projected Visa/MasterCard monthly sales amount");
        }
  
        dol = parseDouble(getAverageTicket());
        num = countDigits(getAverageTicket());
        if (dol <= 0 || num > 10)
        {
          errorMap.put("averageTicket", "Please provide a valid entry in average credit card ticket field");
        }
  
        num = parseInt(getMotoPercentage());
        if (num < 0 || num > 100)
        {
          addError("Please provide a valid mail order/telephone order percentage");
        }
  
        if (getRefundPolicy().equals("0"))
        {
          errorMap.put("refundPolicy", "Please select a refund policy");
        }
  
        if (!isBlank(getAmexAccepted()) && isBlank(getAmexNewAccount()) && isBlank(getAmexMerchId()))
        {
          errorMap.put("amex", "Please provide a valid amex SE# or indicate you would like to open a new account");
        }
        else if (!isBlank(getAmexNewAccount()) && !isBlank(getAmexMerchId()))
        {
          errorMap.put("amex", "You must either provide a valid amex SE# OR indicate you would like to open a new account");
        }
        else if (isBlank(getAmexAccepted()) && (!isBlank(getAmexNewAccount()) || !isBlank(getAmexMerchId())))
        {
          errorMap.put("amex", "Please check the American Express checkbox");
        }
        else if(!isBlank(getAmexMerchId()))
        {
          longNum = parseLong(getAmexMerchId());
          if (longNum < 1000000000)
          {
            errorMap.put("amex", "SE# must be 10 digits long");
          }
        }

        if (!isBlank(getDiscAccepted()) && isBlank(getDiscNewAccount()) && isBlank(getDiscMerchId()))
        {
          if( getAppTypeInt() == mesConstants.APP_TYPE_VERISIGN)
          {
            setDiscNewAccount("y"); //automatically applies for new discover account for verisign apps
          }
          else
          {
            errorMap.put("discover", "Please provide a valid discover Merchant # or indicate you would like to open a new account");
          }
        }
        else if (!isBlank(getDiscNewAccount()) && !isBlank(getDiscMerchId()))
        {
          errorMap.put("discover", "You must either provide a valid discover Merchant # OR indicate you would like to open a new account");
        }
        else if (isBlank(getDiscAccepted()) && (!isBlank(getDiscNewAccount()) || !isBlank(getDiscMerchId())))
        {
          errorMap.put("discover", "Please check the Discover checkbox");
        }
        else if(!isBlank(getDiscMerchId()))
        {
          longNum = parseLong(getDiscMerchId());
          if (longNum < 0X5AF3107A4000L)
          {
            errorMap.put("discover", "Discover Merchant # must be 15 digits long");
          }
        }

  
        if (!isBlank(getDinrAccepted()) && isBlank(getDinrMerchId()))
        {
          errorMap.put("diner", "Please provide a valid Diners Merchant #");
        }
        else if (isBlank(getDinrAccepted()) && !isBlank(getDinrMerchId()))
        {
          errorMap.put("diner", "Please check the Diners checkbox");
        }
  
        if (!isBlank(getJcbAccepted()) && isBlank(getJcbMerchId()))
        {
          errorMap.put("jcb", "Please provide a valid JCB Merchant #");
        }
        else if (isBlank(getJcbAccepted()) && !isBlank(getJcbMerchId()))
        {
          errorMap.put("jcb", "Please check the JCB checkbox");
        }
        
        if((getComments()).length() > 4000)
        {
          errorMap.put("comments", ("Comments must be less than 4000 characters long. It is currently " + (getComments()).length() + " characters long"));
        }

        break;
      case 4:
        if(isBlank(login) || !isEmail(login))
        {
          addError("<b><u>Please correct the following errors:</u></b>");
          addError("User Name must be a valid Email Address. (e.g. joe@aol.com)");
        }
        if(isBlank(pass1) || isBlank(pass2))
        {
          if(!hasErrors())
          {
            addError("<b><u>Please correct the following errors:</u></b>");
          }
          addError("Please select a password and re-enter it.<br>&nbsp;");
        }
        else if(!pass1.equals(pass2))
        {
          if(!hasErrors())
          {
            addError("<b><u>Please correct the following errors:</u></b>");
          }
          addError("Passwords did not match! Please enter a password and re-enter it exactly as the first.<br>&nbsp;");
        }
        else if(pass1.length() < 6)
        {
          if(!hasErrors())
          {
            addError("<b><u>Please correct the following errors:</u></b>");
          }
          addError("Password must be atleast 6 characters long.<br>&nbsp;");
        }
        if(!hasErrors() && alreadyUsed)
        {
          addError("<b><u>Please correct the following errors:</u></b>");
          addError("Please choose a different password.");
        }
        break;
      case 5:
        if(completed < 15)
        {
          addError("<b><u>Please correct the following errors:</u></b><br>");
          addError("You must complete the Business Profile, Ownership/Bank Details, <br> and Transaction Types sections of the application before submitting this form.<br>");
        }
        else if(isBlank(getAgreement()))
        {
          addError("<b><u>Please correct the following errors:</u></b><br>");
          addError("You must Agree or Disagree with the terms <br>of this contract before submitting this form.<br>");
        }
        break;
      default:
        break;
    }
    if(!errorMap.isEmpty())
    {
      addError("<b><u>Please correct the following errors indicated by the red arrow</u></b> &nbsp; " + "<img src=\"/images/arrow1_right.gif\" width=\"10\" height=\"10\" border=\"0\">");
      addError("Place the mouse pointer on each red arrow to see additional");
      addError("information pertaining to that field.");
    }
    if(hasErrors() || !errorMap.isEmpty())
    {
      result = false;
    }
    else
    {
      result = true;
    }
    return result;
  }


  public String getErrorFlag(String key)
  {
    String result = "";
    if(errorMap.get(key) != null)
    {
      result = "<img src=\"/images/arrow1_right.gif\" width=\"10\" height=\"10\" alt=\"" + (String)errorMap.get(key) + "\" border=\"0\">";
    }
    return result;
  }

  private void submitMerchantPass(long appSeqNum)
  {
    PreparedStatement   ps                = null;
    ResultSet           rs                = null;
    StringBuffer        qs                = new StringBuffer("");
    
    try
    {

      // this is a new (insert) scenario
      qs.append("update merchant set ");
      qs.append("merch_login = ?, ");
      qs.append("merch_password = ? ");
      qs.append("where app_seq_num = ? ");
      
      ps = getPreparedStatement(qs.toString());
      
      ps.setString(1, login.trim());
      ps.setString(2, pass1.trim());
      ps.setLong  (3, appSeqNum);
      ps.executeUpdate();
    }
    catch(Exception e)
    {
      addError(e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitMerchantPass: " + e.toString());
    }
  }

  private void submitShoppingCartInfo(long appSeqNum)
  {
    PreparedStatement   ps                = null;
    ResultSet           rs                = null;
    StringBuffer        qs                = new StringBuffer("");
    
    try
    {

      qs.append("update merch_pos set     ");
      qs.append("pos_code           = ?   ");
      qs.append("where app_seq_num  = ?   ");
      
      ps = getPreparedStatement(qs.toString());
      
      ps.setInt (1, mesConstants.APP_MPOS_ESTOREFRONT_SHOPPING_CART_LINK);
      ps.setLong(2, appSeqNum);
      ps.executeUpdate();
    }
    catch(Exception e)
    {
      addError(e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitShoppingCartInfo: " + e.toString());
    }
  }

  private boolean alreadyUsed()   //test to see if username and password already exists
  {
    PreparedStatement   ps                = null;
    ResultSet           rs                = null;
    StringBuffer        qs                = new StringBuffer("");
    boolean             result            = true;
    
    try
    {
      qs.append("select app_seq_num from merchant ");
      qs.append("where ");
      qs.append("merch_login = ? and ");
      qs.append("merch_password = ? ");
      
      ps = getPreparedStatement(qs.toString());

      ps.setString(1, login.trim());
      ps.setString(2, pass1.trim());
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        result = true;
      }      
      else
      {
        result = false;
      }
    }
    catch(Exception e)
    {
      addError(e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "alreadyUsed: " + e.toString());
    }
    finally
    {
    }
    return result;
  }

  private void submitMerchantAgreement(long appSeqNum)
  {
    PreparedStatement   ps                = null;
    ResultSet           rs                = null;
    StringBuffer        qs                = new StringBuffer("");
    
    try
    {

      // this is a new (insert) scenario
      qs.append("update merchant set ");
      qs.append("merch_agreement = ? ");
      qs.append("where app_seq_num = ? ");
      
      ps = getPreparedStatement(qs.toString());
      
      ps.setString(1, getAgreement());
      ps.setLong  (2, appSeqNum);
      ps.executeUpdate();
    }
    catch(Exception e)
    {
      addError(e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitMerchantAgreement: " + e.toString());
    }
  }

  /*
  ** METHOD submitData
     puts a new record in application table, userId is stored here and app_created_date
     everything else is irrelevant.
  */
  public void submitApplicationData(long appSeqNum, long userId, String userLogin, int appType)
  {
    PreparedStatement   ps                = null;
    ResultSet           rs                = null;
    StringBuffer        qs                = new StringBuffer("");
    String              appSrcTypeCode    = "USER";
    long                screenSequenceId  = 0;
    
    try
    {
      
      //@System.out.println("appSeqNum = " + appSeqNum);
      //@System.out.println("userId    = " + userId);
      //@System.out.println("userLogin = " + userLogin);
      //@System.out.println("appType   = " + appType);
      //@System.out.println("blank");

      ps = getPreparedStatement("select app_seq_num from application where app_seq_num = ?");
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        //if already exists dont do anything... 
      }
      else
      {
        ps = getPreparedStatement("select appsrctype_code from org_app where app_type = ?");
        ps.setInt(1,appType);
        rs = ps.executeQuery();
        
        if(rs.next())
        {
          appSrcTypeCode    = rs.getString("appsrctype_code");
          screenSequenceId  = mesConstants.SEQUENCE_VERISIGN_APPLICATION;
        }

        // this is a new (insert) scenario
        qs.append("insert into application (");
        qs.append("app_seq_num, ");
        qs.append("appsrctype_code, ");
        qs.append("app_created_date, ");
        qs.append("app_scrn_code, ");
        qs.append("app_user_id, ");
        qs.append("app_user_login, ");
        qs.append("app_type, ");
        qs.append("screen_sequence_id) ");
        qs.append("values (?, ?, sysdate, ?, ?, ?, ?, ?)");
        
        ps = getPreparedStatement(qs.toString());
        
        ps.setLong  (1,appSeqNum);
        //@System.out.println("appSeqNum = " + appSeqNum);
        ps.setString(2,appSrcTypeCode);
        //@System.out.println("appSrcTypeCode = " + appSrcTypeCode);
        ps.setNull  (3,java.sql.Types.VARCHAR);
        ps.setLong  (4,userId);
        //@System.out.println("userId = " + userId);
        ps.setString(5,userLogin);
        //@System.out.println("userLogin = " + userLogin);
        ps.setInt   (6,appType);
        //@System.out.println("appType = " + appType);
        ps.setLong  (7,screenSequenceId);
        //@System.out.println("screenSequenceId = " + screenSequenceId);
        ps.executeUpdate();
      }
    }
    catch(Exception e)
    {
      addError(e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitApplicationData: " + e.toString());
    }
  }

  
  private String verifyLength(String field, int fieldLength)
  {
    if(isBlank(field))
    {
      return "";
    }
    
    try
    {
      StringBuffer  sb    = new StringBuffer(field);
      sb.setLength(fieldLength);
      field = (sb.toString()).trim();
    }
    catch(Exception e)
    {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(),"verifyLength:");
    }

    return field;
  }
  
  /*
  ** METHOD submitMerchantData
  */
  public void submitMerchantData(String businessName, String businessEmail, String loginName, long appSeqNum, long appControlNum)
  {
    boolean           isStale   = false;
    
    try
    {
      businessName  = verifyLength(businessName,  25);
      businessEmail = verifyLength(businessEmail, 75);
      loginName     = verifyLength(loginName,     75);
      
      if(isConnectionStale())
      {
        isStale = true;
        connect();
      }
      
      int appCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:927^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.setup.VsMerchInfoBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:933^7*/
      
      if(appCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:937^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_business_name   = :businessName,
//                    merch_login           = :loginName,
//                    merch_email_address   = :businessEmail
//            where   app_seq_num           = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n          set     merch_business_name   =  :1 ,\n                  merch_login           =  :2 ,\n                  merch_email_address   =  :3 \n          where   app_seq_num           =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.setup.VsMerchInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,businessName);
   __sJT_st.setString(2,loginName);
   __sJT_st.setString(3,businessEmail);
   __sJT_st.setLong(4,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:944^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:948^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant
//            (
//              merc_cntrl_number,
//              merch_business_name,
//              merch_login,
//              merch_email_address,
//              app_seq_num
//            )
//            values
//            (
//              :appControlNum,
//              :businessName,
//              :loginName,
//              :businessEmail,
//              :appSeqNum
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchant\n          (\n            merc_cntrl_number,\n            merch_business_name,\n            merch_login,\n            merch_email_address,\n            app_seq_num\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.setup.VsMerchInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appControlNum);
   __sJT_st.setString(2,businessName);
   __sJT_st.setString(3,loginName);
   __sJT_st.setString(4,businessEmail);
   __sJT_st.setLong(5,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:966^9*/
      }
    }
    catch (Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(),
        "submitMerchantData: " + e.toString());
      addError("submitMerchantData: " + e.toString());
    }
    finally
    {
      if(isStale)
      {
        cleanUp();
      }
    }
  }
  
  /*
  ** METHOD submitAddressData
  */
  public void submitAddressData(long appSeqNum, String address1, String address2, String city, String state, String zip, String businessFax)
  {
    StringBuffer      qs    = new StringBuffer("");
    PreparedStatement ps    = null;
    ResultSet         rs    = null;

    address1    = verifyLength(address1,    32);
    address2    = verifyLength(address2,    32);
    city        = verifyLength(city,        25);
    state       = verifyLength(state,       2);
    zip         = verifyLength(zip,         10);
    //businessFax = verifyLength(businessFax, 10);

    try
    {
      qs.append("select app_seq_num from address ");
      qs.append("where app_seq_num = ? and addresstype_code = ?");
      ps = getPreparedStatement(qs.toString());
      
      ps.setLong  (1, appSeqNum);
      ps.setInt   (2, 1);
      
      rs = ps.executeQuery();
      
      qs.setLength(0);
      
      if (rs.next())
      {
        qs.append("update address set ");
        qs.append("address_line1 = ?, ");
        qs.append("address_line2 = ?, ");
        qs.append("address_city = ?, ");
        qs.append("countrystate_code = ?, ");
        qs.append("address_zip = ?, ");
        qs.append("address_fax = ? ");
        qs.append("where app_seq_num = ? and addresstype_code = ?");
      }
      else
      {
        qs.append("insert into address (");
        qs.append("address_line1, ");
        qs.append("address_line2, ");
        qs.append("address_city, ");
        qs.append("countrystate_code, ");
        qs.append("address_zip, ");
        qs.append("address_fax, ");
        qs.append("app_seq_num, ");
        qs.append("addresstype_code) ");
        qs.append("values (?, ?, ?, ?, ?, ?, ?, ?)");
      }
      
      rs.close();
      ps.close();
      
      ps = getPreparedStatement(qs.toString());
      
      ps.setString(1,address1);
      ps.setString(2,address2);
      ps.setString(3,city);
      ps.setString(4,state);
      ps.setString(5,zip);
      ps.setLong  (6,getDigits(businessFax));
      ps.setLong  (7,appSeqNum);
      ps.setInt   (8,1);
      
      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitAddressData: insert failed");
        addError("submitAddressData: Unable to insert/update record");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitAddressData: " + e.toString());
      addError("submitAddressData: " + e.toString());
    }
  }
      
  
  /*
  ** METHOD submitMerchContactData
  */
  public void submitMerchContactData(long appSeqNum, String contactFirstName, String contactLastName, String contactPhone)
  {
    StringBuffer      qs                = new StringBuffer("");
    PreparedStatement ps                = null;
    ResultSet         rs                = null;
    Vector            vs                = new Vector();
    int               i                 = 0;

    try
    {
      
/*
      StringTokenizer tok = new StringTokenizer(contactName, " ");
      while (tok.hasMoreTokens()) 
      {
        vs.add(" ");
        vs.add(tok.nextToken());
      }

      if(vs.size() > 0)
      {    
        if(vs.size() == 2)
        {
          contactFirstName = (String)vs.elementAt(1);
        }
        else
        {
          for(i=0; i < vs.size() - 1; ++i)
          {
            qs.append((String)vs.elementAt(i));
          }
          contactFirstName = (qs.toString()).trim();
          qs.setLength(0);
          contactLastName  = (String)vs.elementAt(i);
        }
      }
*/
      contactFirstName    = verifyLength(contactFirstName,   20);
      contactLastName     = verifyLength(contactLastName,    20);
      //contactPhone      = verifyLength(contactPhone, 10);
      
      qs.append("select app_seq_num from merchcontact ");
      qs.append("where app_seq_num = ?");
      ps = getPreparedStatement(qs.toString());
      ps.setLong  (1,appSeqNum);
      
      rs = ps.executeQuery();
      
      qs.setLength(0);
      
      if(rs.next())
      {
        qs.append("update merchcontact set ");
        qs.append("merchcont_prim_first_name = ?, ");
        qs.append("merchcont_prim_last_name = ?, ");
        qs.append("merchcont_prim_phone = ? ");
        qs.append("where app_seq_num = ?");
      }
      else
      {
        qs.append("insert into merchcontact (");
        qs.append("merchcont_prim_first_name, ");
        qs.append("merchcont_prim_last_name, ");
        qs.append("merchcont_prim_phone, ");
        qs.append("app_seq_num) ");
        qs.append("values (?, ?, ?, ?)");
      }
      
      rs.close();
      ps.close();
      
      ps = getPreparedStatement(qs.toString());

      ps.setString(1,contactFirstName);
      ps.setString(2,contactLastName);
      ps.setLong  (3,getDigits(contactPhone));
      ps.setLong  (4,appSeqNum);      
      
      if(ps.executeUpdate() != 1)
      {
        addError("submitMerchContactData: Unable to insert/update record");
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(),
                 "submitMerchContactData: insert failed");
      }
    }
    catch(Exception e)
    {
      addError("submitMerchContactData: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(),
               "submitMerchContactData: " + e.toString());
    }
  }

  public String formatPhone(String phone)
  {
    if(isBlank(phone))
    {
      return "";
    }
    
    String        result = phone;
    StringBuffer  s      = new StringBuffer(phone);

    int index = phone.indexOf("-");

    while(index != -1)
    {
      s.deleteCharAt(index);
      phone = s.toString();
      index = phone.indexOf("-");    
    }

    if(s.length() == 10)
    {
      s.insert(3, "-");
      s.insert(7, "-");
      result = s.toString();
    }

    return result;
  }

  public String formatSSN(String ssn)
  {

    if(isBlank(ssn))
    {
      return "";
    }
    
    String        result = ssn;
    StringBuffer  s      = new StringBuffer(ssn);

    int index = ssn.indexOf("-");

    while(index != -1)
    {
      s.deleteCharAt(index);
      ssn = s.toString();
      index = ssn.indexOf("-");    
    }

    if(s.length() == 9)
    {
      s.insert(3, "-");
      s.insert(6, "-");
      result = s.toString();
    }

    return result;
  }

  public boolean canSubmit()
  {
    boolean result = true;

    if(isBlank(login) || !isEmail(login))
    {
      result = false;
    }
    if(isBlank(pass1) || isBlank(pass2))
    {
      result = false;
    }
    else if(!pass1.equals(pass2))
    {
      result = false;
    }
    else if(pass1.length() < 6)
    {
      result = false;
    }
    if(result && alreadyUsed())
    {
      alreadyUsed = true;
      result      = false;
    }

    return result;
  }
  
  public void setPageNum(String pageNum)
  {
    this.pageNum = parseInt(pageNum);
  }
  
  public void setCompleted(String completed)
  {
    this.completed = parseInt(completed);
  }
  
  public void setLogin(String login)
  {
    this.login = login;
  }
  public String getLogin()
  {
    return this.login;
  }

  public void setPass1(String pass1)
  {
    this.pass1 = pass1;
  }

  public void setPass2(String pass2)
  {
    this.pass2 = pass2;
  }
  public String getPass1()
  {
    return this.pass1;
  }

  public String getPass2()
  {
    return this.pass2;
  }


}/*@lineinfo:generated-code*/