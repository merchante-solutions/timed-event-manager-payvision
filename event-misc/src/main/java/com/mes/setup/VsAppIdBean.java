/*@lineinfo:filename=VsAppIdBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/VsAppIdBean.sqlj $

  Description:
    Base class for application data beans

    This class should maintain any data that is common to all of the
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 30 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.setup;

import java.io.Serializable;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import sqlj.runtime.ResultSetIterator;

public class VsAppIdBean extends com.mes.screens.SequenceIdBean
  implements Serializable
{
  public static final int     SAVED_APP               = 1;
  public static final int     FORGOT_PASSWORD         = 2;
  public static final int     AGREE_TO_TERMS          = 3;
  public static final int     DO_NOT_AGREE_TO_TERMS   = 4;
  public static final int     APPLICATION_COMPLETED   = 5;
  
  public static final String  DEFAULT_RETURN_URL      = "http://www.merchante-solutions.com";

  private long          appControlNum     = 0L;
  private int           appType           = -1;
  private long          vid               = 0L;
  private String        loginDefault      = "";
  private String        returnURL         = "";
  private String        businessURL       = "";
  private int           partnerId         = 0;
  private String        partnerName       = "";
  private boolean       exist             = false;
  private boolean       loggedIn          = false;
  private boolean       newApp            = false;
  private boolean       saved             = false;
  private boolean       done              = false;
  private boolean       superUser         = false;
  private boolean       passExist         = true;

  public synchronized void resetSequenceId()
  {
    appControlNum     = 0L;
    appType           = -1;
    vid               = 0L;
    loginDefault      = "";
    returnURL         = "";
    businessURL       = "";
    exist             = false;
    loggedIn          = false;
    newApp            = false;
    saved             = false;
    done              = false;
    superUser         = false;
    passExist         = true;
    super.init();
  }
  public synchronized void setSuperUser()
  {
    loggedIn          = true;
    newApp            = false;
    superUser         = true;
  }
  public synchronized void getData(long sequenceId, long primaryKey)
  {
    if(isSuperUser())
    {
      if(primaryKey != getPrimaryKey())
      {
        //if you want to validate user.. do it here
        getSessionData(primaryKey);
      }
    }
    super.getData(sequenceId, primaryKey);
    if(isSuperUser() && getCompletedScreens() == 0L)
    {
      setCompletedScreens(1L);
    }
  }

  public synchronized void getSessionData(long primaryKey)
  {
    ResultSet         rs      = null;
    ResultSetIterator it      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:132^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch.merc_cntrl_number,
//                  link.verisign_id,
//                  link.return_url,
//                  link.business_url,
//                  nvl(link.pid, 0)  pid,
//                  nvl(link.partner, 'unknown')  partner,
//                  app.app_type
//          from    merchant merch,
//                  vs_linking_table link,
//                  application app
//          where   merch.app_seq_num = :primaryKey and
//                  merch.app_seq_num = link.app_seq_num(+) and
//                  merch.app_seq_num = app.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch.merc_cntrl_number,\n                link.verisign_id,\n                link.return_url,\n                link.business_url,\n                nvl(link.pid, 0)  pid,\n                nvl(link.partner, 'unknown')  partner,\n                app.app_type\n        from    merchant merch,\n                vs_linking_table link,\n                application app\n        where   merch.app_seq_num =  :1  and\n                merch.app_seq_num = link.app_seq_num(+) and\n                merch.app_seq_num = app.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.setup.VsAppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.setup.VsAppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:147^7*/
      
      rs = it.getResultSet();
      
      if (rs.next())
      {
        this.loggedIn       = true;
        this.appControlNum  = rs.getLong("merc_cntrl_number");
        this.vid            = rs.getLong("verisign_id");
        this.returnURL      = rs.getString("return_url");
        this.businessURL    = rs.getString("business_url");
        this.appType        = rs.getInt("app_type");
        this.partnerId      = rs.getInt("pid");
        this.saved          = true;
        this.exist          = true;
        this.newApp         = false;
        this.superUser      = true;
        this.done           = false;
      }
      else
      {
        resetSequenceId();
      }
      
      rs.close(); 
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getSessionData: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public synchronized void startNewSession(int     appType, 
                              long    sequenceId, 
                              long    vid, 
                              String  enterpriseId, 
                              String  smbid, 
                              String  quickStoreLogin,
                              String  returnURL,
                              String  businessURL,
                              String  partner,
                              String  pid,
                              int     product)
  {
    resetSequenceId();

    long primaryKey = 0L;

    //starting new session so put in all info that you need

    this.vid       = vid;
    this.appType   = appType;  
    this.loggedIn  = true;
    this.newApp    = true;
    this.exist     = false;
    primaryKey     = issuePrimaryKey(primaryKey); // gets new primary key and sets merch control number
    super.getData(sequenceId, primaryKey);
    submitLinkingData(enterpriseId, smbid, quickStoreLogin, returnURL, businessURL, partner, pid, product);
  }

  //looks for existence in db, if found gets login name to prepopulate the login page
  //we set booleans and vid and login name for session... then once they log on we can set loggedin to true
  //  and let them finish filling out the form
  
  public synchronized void findExisting(long vid)
  {
    ResultSet         rs      = null;
    ResultSetIterator it      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:227^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch.merch_login, 
//                  merch.merch_password
//          
//          from    merchant merch,
//                  vs_linking_table link
//          
//          where   link.verisign_id = :vid and
//                  link.app_seq_num = merch.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch.merch_login, \n                merch.merch_password\n        \n        from    merchant merch,\n                vs_linking_table link\n        \n        where   link.verisign_id =  :1  and\n                link.app_seq_num = merch.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.setup.VsAppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,vid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.setup.VsAppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:237^7*/
      
      rs = it.getResultSet();
      
      if (rs.next())
      {
        this.exist         = true;
        this.newApp        = false;
        this.loginDefault  = isBlank(rs.getString("merch_login")) ? "" : rs.getString("merch_login");
        
        if(isBlank(rs.getString("merch_login")) || isBlank(rs.getString("merch_password")))
        {
          this.passExist  = false;
          this.loggedIn   = true;
          this.newApp     = true;
          validate(vid);
        }
      }
      else
      {
        this.exist         = false;
        this.newApp        = true;
        this.passExist     = false;
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "findExisting: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  private void validate(long vid)
  {
    ResultSet         rs      = null;
    ResultSetIterator it      = null;
    
    try
    {
      // get connection
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:286^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch.merch_login,
//                  merch.merc_cntrl_number,
//                  merch.merch_agreement,
//                  merch.app_seq_num,
//                  link.verisign_id,
//                  link.return_url,
//                  link.business_url,
//                  app.app_type
//  
//          from    merchant merch,
//                  vs_linking_table link,
//                  application app
//  
//          where   link.verisign_id = :vid and
//                  link.app_seq_num = merch.app_seq_num and
//                  link.app_seq_num = app.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch.merch_login,\n                merch.merc_cntrl_number,\n                merch.merch_agreement,\n                merch.app_seq_num,\n                link.verisign_id,\n                link.return_url,\n                link.business_url,\n                app.app_type\n\n        from    merchant merch,\n                vs_linking_table link,\n                application app\n\n        where   link.verisign_id =  :1  and\n                link.app_seq_num = merch.app_seq_num and\n                link.app_seq_num = app.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.setup.VsAppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,vid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.setup.VsAppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:304^7*/
      
      rs = it.getResultSet();
      
      if (rs.next())
      {
        if(rs.getString("merch_agreement") != null && (rs.getString("merch_agreement")).equals("Y"))
        {
          this.done          = true;
          this.returnURL     = rs.getString("return_url");
        }
        else
        {
          this.loggedIn      = true;
          this.vid           = rs.getLong   ("verisign_id");
          this.appControlNum = rs.getLong   ("merc_cntrl_number");
          this.appType       = rs.getInt    ("app_type");
          this.returnURL     = rs.getString ("return_url");
          this.businessURL   = rs.getString ("business_url");
          this.saved         = true;
          this.exist         = true;
          this.newApp        = false;
          setPrimaryKey(rs.getLong("app_seq_num"));
        }
      }
      else
      {
        this.loggedIn      = false;
      }
      
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "validate: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public synchronized long findVerisignId(long cntrlnum)
  {
    long              result  = -1234;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:354^7*/

//  ************************************************************
//  #sql [Ctx] { select  verisign_id 
//          from    vs_linking_table
//          where   merc_cntrl_number = :cntrlnum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  verisign_id  \n        from    vs_linking_table\n        where   merc_cntrl_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.setup.VsAppIdBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,cntrlnum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:359^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "findVerisignId: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public synchronized boolean validate(String login, String pass)
  {
    ResultSet         rs      = null;
    ResultSetIterator it      = null;
    
    try
    {
      if(login.trim() == "" || login.trim() == null || pass.trim() == "" || pass.trim() == null)
      {
        return false;
      }
      // get connection
      
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:388^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch.merch_login,
//                  merch.merc_cntrl_number,
//                  merch.merch_agreement,
//                  merch.app_seq_num,
//                  link.verisign_id,
//                  link.return_url,
//                  link.business_url,
//                  app.app_type
//          from    merchant merch,
//                  vs_linking_table link,
//                  application app
//          where   merch.merch_login = :login.trim() and
//                  merch.merch_password = :pass.trim() and
//                  link.app_seq_num = merch.app_seq_num and
//                  merch.app_seq_num = app.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1548 = login.trim();
 String __sJT_1549 = pass.trim();
  try {
   String theSqlTS = "select  merch.merch_login,\n                merch.merc_cntrl_number,\n                merch.merch_agreement,\n                merch.app_seq_num,\n                link.verisign_id,\n                link.return_url,\n                link.business_url,\n                app.app_type\n        from    merchant merch,\n                vs_linking_table link,\n                application app\n        where   merch.merch_login =  :1  and\n                merch.merch_password =  :2  and\n                link.app_seq_num = merch.app_seq_num and\n                merch.app_seq_num = app.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.setup.VsAppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1548);
   __sJT_st.setString(2,__sJT_1549);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.setup.VsAppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:405^7*/
      
      rs = it.getResultSet();
      
      if (rs.next())
      {
        if(rs.getString("merch_agreement") != null)
        {
          if((rs.getString("merch_agreement")).equals("Y"))
          {
            this.done          = true;
            this.returnURL     = rs.getString("return_url");
          }
          else
          {
            this.loggedIn      = true;
            this.vid           = rs.getLong("verisign_id");
            this.appControlNum = rs.getLong("merc_cntrl_number");
            this.appType       = rs.getInt("app_type");
            this.returnURL     = rs.getString("return_url");
            this.businessURL   = rs.getString("business_url");
            this.saved         = true;
            this.exist         = true;
            this.newApp        = false;
            setPrimaryKey(rs.getLong("app_seq_num"));
          }
        }
        else
        {
          this.loggedIn      = true;
          this.vid           = rs.getLong("verisign_id");
          this.appControlNum = rs.getLong("merc_cntrl_number");
          this.appType       = rs.getInt("app_type");
          this.returnURL     = rs.getString("return_url");
          this.businessURL   = rs.getString("business_url");
          this.saved         = true;
          this.exist         = true;
          this.newApp        = false;
          setPrimaryKey(rs.getLong("app_seq_num"));
        }
      }
      else
      {
        this.loggedIn      = false;
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "validate: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return (this.loggedIn || this.done);
  }

  public synchronized boolean findPassword(String login, String email)
  {
    ResultSet         rs      = null;
    ResultSetIterator it      = null;
    boolean           result  = false;

    try
    {
      if(isBlank(login.trim()) && isBlank(email.trim()))
      {
        return false;
      }
      // get connection
      
      connect();
      
      if(!isBlank(login.trim()) && !isBlank(email.trim()))
      {
        /*@lineinfo:generated-code*//*@lineinfo:486^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num,merch_email_address
//            from    merchant
//            where   merch_login = :login.trim() and
//                    merch_email_address = :email.trim()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1550 = login.trim();
 String __sJT_1551 = email.trim();
  try {
   String theSqlTS = "select  app_seq_num,merch_email_address\n          from    merchant\n          where   merch_login =  :1  and\n                  merch_email_address =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.setup.VsAppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1550);
   __sJT_st.setString(2,__sJT_1551);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.setup.VsAppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:492^9*/
      }
      else if(!isBlank(login.trim()))
      {
        /*@lineinfo:generated-code*//*@lineinfo:496^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num,merch_email_address
//            from    merchant
//            where   merch_login = :login.trim() 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1552 = login.trim();
  try {
   String theSqlTS = "select  app_seq_num,merch_email_address\n          from    merchant\n          where   merch_login =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.setup.VsAppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1552);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.setup.VsAppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:501^9*/
      }
      else if(!isBlank(email.trim()))
      {
        /*@lineinfo:generated-code*//*@lineinfo:505^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num,merch_email_address
//            from    merchant
//            where   merch_email_address = :email.trim() 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1553 = email.trim();
  try {
   String theSqlTS = "select  app_seq_num,merch_email_address\n          from    merchant\n          where   merch_email_address =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.setup.VsAppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1553);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.setup.VsAppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:510^9*/
      }

      rs = it.getResultSet();
      
      //if it returns more than one record, we just return false.. cause we dont know which one is correct
      while(rs.next())
      {
        if(!result)
        {
          if(!isBlank(rs.getString("merch_email_address")))
          {
            result = true;
            setPrimaryKey(rs.getLong("app_seq_num"));
          }
          else
          {
            result = false;
          }
        }
        else
        {
          result = false;
          init(); //reset primary key incase it was set above
          break;
        }
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "validate: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }

  public synchronized String getLoginName()
  {
    String            result  = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:562^7*/

//  ************************************************************
//  #sql [Ctx] { select  merch_login 
//          from    merchant
//          where   app_seq_num = :getPrimaryKey()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1554 = getPrimaryKey();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_login  \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.setup.VsAppIdBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1554);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:567^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getLoginName: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  //looks for existence in db, if found gets login name to prepopulate the login page
  //we set booleans and vid and login name for session... then once they log on we can set loggedin to true
  //  and let them finish filling out the form
  
  private void submitLinkingData( String enterpriseId, 
                                  String smbid, 
                                  String quickStoreLogin,
                                  String returnURL,
                                  String businessURL,
                                  String partner,
                                  String pid,
                                  int    product)
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:598^7*/

//  ************************************************************
//  #sql [Ctx] { insert into vs_linking_table
//          (
//            app_seq_num,
//            verisign_id,
//            merc_cntrl_number,
//            nsi_enterprise_id,
//            nsi_smbid,
//            nsi_login_id,
//            return_url,
//            business_url,
//            partner,
//            pid,
//            product
//          )
//          values
//          (
//            :getPrimaryKey(),
//            :vid,
//            :appControlNum,
//            :enterpriseId,
//            :smbid,
//            :quickStoreLogin,
//            :returnURL,
//            :businessURL,
//            :partner,
//            :pid,
//            :product
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1555 = getPrimaryKey();
   String theSqlTS = "insert into vs_linking_table\n        (\n          app_seq_num,\n          verisign_id,\n          merc_cntrl_number,\n          nsi_enterprise_id,\n          nsi_smbid,\n          nsi_login_id,\n          return_url,\n          business_url,\n          partner,\n          pid,\n          product\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.setup.VsAppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1555);
   __sJT_st.setLong(2,vid);
   __sJT_st.setLong(3,appControlNum);
   __sJT_st.setString(4,enterpriseId);
   __sJT_st.setString(5,smbid);
   __sJT_st.setString(6,quickStoreLogin);
   __sJT_st.setString(7,returnURL);
   __sJT_st.setString(8,businessURL);
   __sJT_st.setString(9,partner);
   __sJT_st.setString(10,pid);
   __sJT_st.setInt(11,product);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:628^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitLinkingData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

 
  /*
    this method checks to see if an entry has been made into the screen_progress table.
    if it has and this current session belongs to a different sequence type, e.g., 
    a verisign app being viewed by the mes application, then we bypass the submit data method in 
   the base class. This avoids two entries in the screen_progress table for one application.
  */
  public synchronized void submitData(long screenId)
  {
    ResultSet           rs      = null;
    ResultSetIterator   it      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:656^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  screen_pk,
//                  screen_sequence_id
//          from    screen_progress
//          where   screen_pk = :getPrimaryKey()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1556 = getPrimaryKey();
  try {
   String theSqlTS = "select  screen_pk,\n                screen_sequence_id\n        from    screen_progress\n        where   screen_pk =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.setup.VsAppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1556);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.setup.VsAppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:662^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        if(rs.getLong("screen_sequence_id") == getSequenceId())
        {
          super.submitData(screenId);  
        }
      }
      else
      {
        super.submitData(screenId);
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  //either gets new primary key and control num and starts new session or
  //validates userId and sets primary key and control num and stats new session
  private long issuePrimaryKey(long primaryKey)
  {
    if(primaryKey <= 0L) // if no seqnum generate new one
    {
      primaryKey = setSeqNum();
    }
    else if(primaryKey > 0L)//else get control number
    {
      //this.appControlNum   = getControlNumber(primaryKey);
    }
    return primaryKey;
  }
  
  /*
  ** METHOD setSeqNum
  */
  private long setSeqNum()
  {
    long                controlSeq  = 10000000000L;
    long                primaryKey  = 0L;
    
    try
    {
      connect();
      
      // get new sequence number
      /*@lineinfo:generated-code*//*@lineinfo:721^7*/

//  ************************************************************
//  #sql [Ctx] { select  application_sequence.nextval 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  application_sequence.nextval  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.setup.VsAppIdBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   primaryKey = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:725^7*/

      // get a new control number
      Calendar      cal         = new GregorianCalendar();
      String        YYYY        = String.valueOf(cal.get(Calendar.YEAR));
      String        YY          = YYYY.substring(2);
      int           day         = cal.get(Calendar.DAY_OF_YEAR);

      // add the year
      controlSeq += ((cal.get(Calendar.YEAR) - 2000) * 100000000L);

      // add the julian day
      controlSeq += (cal.get(Calendar.DAY_OF_YEAR) * 100000L);

      // add the sequence number
      int tempVal = 0;
      /*@lineinfo:generated-code*//*@lineinfo:741^7*/

//  ************************************************************
//  #sql [Ctx] { select  control_num_sequence.nextVal 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  control_num_sequence.nextVal  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.setup.VsAppIdBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   tempVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:745^7*/
      
      controlSeq += tempVal;

      this.appControlNum = controlSeq + tempVal;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setSeqNum: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return primaryKey;
  }

  /*
    this method checks to see if an entry has been made into the screen_progress table.
    if it has and this current session belongs to a different sequence type, e.g., 
    a verisign app being viewed by the mes application, then we set the completed screens 
    to the highest number for that sequence, so that the entire application can be navigated 
    through
  */
  public synchronized void checkCompletedScreens()
  {
    ResultSet           rs      = null;
    ResultSetIterator   it      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:779^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  screen_pk,
//                  screen_sequence_id
//          from    screen_progress
//          where   screen_pk = :getPrimaryKey()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1557 = getPrimaryKey();
  try {
   String theSqlTS = "select  screen_pk,\n                screen_sequence_id\n        from    screen_progress\n        where   screen_pk =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.setup.VsAppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1557);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.setup.VsAppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:785^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        if(rs.getLong("screen_sequence_id") != getSequenceId())
        {
          if(getSequenceId() == com.mes.constants.mesConstants.SEQUENCE_ID_APPLICATION)
          {
            setCompletedScreens(1);
          }
          else if(getSequenceId() == com.mes.constants.mesConstants.SEQUENCE_APPLICATION)
          {
            setCompletedScreens(7);
          }
        }
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "checkCompletedScreens: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public synchronized void sendEmail(int sendMessage, HttpServletRequest request)
  {
    MailMessage         msg         = null;
    SimpleDateFormat    simpleDate  = new SimpleDateFormat("MM/dd/yyyy");
    String              comDate     = simpleDate.format(new Date());
    StringBuffer        message     = new StringBuffer("");
    String              subject     = "";
    
    long                cntrlNum    = 0L;
    String              dba         = "";
    String              login       = "";
    String              password    = "";
    String              emailAdd    = "";
    String              merchName   = "";
    String              conPhone    = "";

    ResultSet           rs          = null;
    ResultSetIterator   it          = null;
    
    String              to          = "";
    String              host        = request.getHeader("HOST");
    String              protocol    = "http://";
    int                 emailType   = MesEmails.MSG_ADDRS_VS_APP_SUBMITTED;
    
    if(host.equals("www.merchante-solutions.com"))
    {
      protocol = "https://";
    }

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:852^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch.merch_login,
//                  merch.merch_legal_name,
//                  merch.merch_business_name,
//                  merch.merch_password,
//                  merch.merch_email_address,
//                  merch.merc_cntrl_number,
//                  contact.merchcont_prim_first_name,
//                  contact.merchcont_prim_last_name,
//                  contact.merchcont_prim_phone
//          from    merchant merch,
//                  merchcontact contact
//          where   merch.app_seq_num = :getPrimaryKey() and
//                  merch.app_seq_num = contact.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1558 = getPrimaryKey();
  try {
   String theSqlTS = "select  merch.merch_login,\n                merch.merch_legal_name,\n                merch.merch_business_name,\n                merch.merch_password,\n                merch.merch_email_address,\n                merch.merc_cntrl_number,\n                contact.merchcont_prim_first_name,\n                contact.merchcont_prim_last_name,\n                contact.merchcont_prim_phone\n        from    merchant merch,\n                merchcontact contact\n        where   merch.app_seq_num =  :1  and\n                merch.app_seq_num = contact.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.setup.VsAppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1558);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.setup.VsAppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:867^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        /*
        merchName = rs.getString("merchcont_prim_first_name") != null ? rs.getString("merchcont_prim_first_name") : "";
        merchName += rs.getString("merchcont_prim_last_name") != null ? (" " + rs.getString("merchcont_prim_last_name")) : "";
        */
        
        merchName = rs.getString("merch_legal_name");
        conPhone  = rs.getString("merchcont_prim_phone");
        dba       = rs.getString("merch_business_name");
        cntrlNum  = rs.getLong("merc_cntrl_number");
        login     = rs.getString("merch_login");
        password  = rs.getString("merch_password");
        emailAdd  = rs.getString("merch_email_address");
        if(emailAdd == null || emailAdd.equals(""))
        {
          emailAdd = login;
        }
      }

      to = emailAdd;
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("sendEmail(1)", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    boolean sendOK = true;
    switch(sendMessage)
    {
      case SAVED_APP:
        subject = "Application Information for your merchant account application";
        message.append("Your application for an Internet Merchant Account with Merchant e-Solutions has been saved.  Please use this url to return to your application:\n\n");
        message.append(protocol);
        message.append(host);
        message.append("/jsp/secure/vsLogin.jsp?cntrlnum=" + cntrlNum + "\n\n");
        message.append("Should you have any questions please call 1-888-288-2692 or send an e-mail to help@merchante-solutions.com.\n\n");
        message.append("Sincerely,\n\n");
        message.append("Application Review Team\n\n");
        message.append("Merchant e-Solutions working in partnership with VeriSign\n");
        break;
      case FORGOT_PASSWORD:
        subject = "Login Name and Password";
        message.append("Please use this url to return to your application: \n\n");
        message.append(protocol);
        message.append(host);
        message.append("/jsp/secure/vsLogin.jsp?cntrlnum=" + cntrlNum + "\n\n");
        message.append("Should you have any questions please call 1-888-288-2692.\n\n");
        message.append("Sincerely,\n\n");
        message.append("Application Review Team");
        break;
      case AGREE_TO_TERMS:
        subject = "Your application for a merchant account has been received";
        message.append("The application you have submitted has been received and is being processed.  By the end of the next business day, we will have reviewed the information you have provided and inform you by e-mail of the outcome of your merchant account request.\n\n");
        message.append("Part of the evaluation process includes a review of your web site.  If your web site is not currently operational, we will continue with the credit evalutaion process, but will not be able to activate your merchant account until this step has been completed.  If your web site is active and your application is approved, you will be advised by e-mail when you can start accepting credit card transactions.\n\n");
        message.append("To contact Merchant e-Solutions you may call 1-888-288-2692 or send an e-mail to help@merchante-solutions.com <mailto:help@merchante-solutions.com>.\n\n");
        message.append("Again, we thank you for your interest in a merchant account with Merchant e-Solutions, and we look forward to servicing your credit card acceptance needs.\n\n");
        message.append("Sincerely,\n\n");
        message.append("Application Review Team\n\n");
        message.append("Merchant e-Solutions working in partnership with VeriSign\n");
        break;
      case DO_NOT_AGREE_TO_TERMS:
        subject = "Your application for a merchant account is Pending";
        message.append("Thank you for taking the time to complete our application.  Unfortunately, you did not agree to the terms of our merchant agreement so your application is not being processed.  Should you change your decision, please use this url to return to your application and agree to the terms of the merchant agreement:\n\n");
        message.append(protocol);
        message.append(host);
        message.append("/jsp/secure/vsLogin.jsp?cntrlnum=" + cntrlNum + "\n\n");
        message.append("Should you have any questions please call 1-888-288-2692 or send an e-mail to help@merchante-solutions.com <mailto:help@merchante-solutions.com>.\n\n");
        message.append("Sincerely,\n\n");
        message.append("Application Review Team\n\n");
        message.append("Merchant e-Solutions working in partnership with VeriSign\n");
        break;
      case APPLICATION_COMPLETED:
        subject = merchName + " " + "Merchant E Application Completed";
        message.append("Merchant Account Completed by:\n\n");
        message.append("Date Completed: " + comDate + "\n");
        message.append("Merchant Name: " + merchName +"\n");
        message.append("DBA: " + dba + "\n");
        message.append("Phone: " + conPhone + "\n");
        message.append("Email Address: " + emailAdd + "\n");
        emailType = MesEmails.MSG_ADDRS_NSI_APP_SUBMITTED;
        break;
      default:
        break;
    }

    try
    {
      if(sendOK)
      {
        msg = new MailMessage();
        msg.setAddresses(emailType);
        if(emailType == MesEmails.MSG_ADDRS_VS_APP_SUBMITTED)
        {
          msg.addTo(to);
        }
        msg.setSubject(subject);
        msg.setText(message.toString());
        msg.send();
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(),"sendEmail2: " + e.toString());
    }
  }


  public synchronized String sendPasswordEmail(HttpServletRequest request)
  {
    MailMessage         msg         = null;
    SimpleDateFormat    simpleDate  = new SimpleDateFormat("MM/dd/yyyy");
    String              comDate     = simpleDate.format(new Date());
    StringBuffer        message     = new StringBuffer("");
    String              subject     = "";
    
    long                cntrlNum    = 0L;
    String              dba         = "";
    String              login       = "";
    String              password    = "";
    String              emailAdd    = "";
    String              merchName   = "";
    String              conPhone    = "";

    ResultSet           rs          = null;
    ResultSetIterator   it          = null;
    
    String              to          = "";
    String              host        = request.getHeader("HOST");
    String              protocol    = "http://";
    int                 emailType   = MesEmails.MSG_ADDRS_VS_APP_SUBMITTED;
    
    if(host.equals("www.merchante-solutions.com"))
    {
      protocol = "https://";
    }

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1021^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_login,
//                  merch_legal_name,
//                  merch_business_name,
//                  merch_password,
//                  merch_email_address,
//                  merc_cntrl_number
//          from    merchant
//          where   app_seq_num = :getPrimaryKey()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1559 = getPrimaryKey();
  try {
   String theSqlTS = "select  merch_login,\n                merch_legal_name,\n                merch_business_name,\n                merch_password,\n                merch_email_address,\n                merc_cntrl_number\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.setup.VsAppIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1559);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.setup.VsAppIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1031^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        
        merchName = rs.getString("merch_legal_name");
        dba       = rs.getString("merch_business_name");
        cntrlNum  = rs.getLong("merc_cntrl_number");
        login     = rs.getString("merch_login");
        password  = rs.getString("merch_password");
        emailAdd  = rs.getString("merch_email_address");
        if(emailAdd == null || emailAdd.equals(""))
        {
          emailAdd = login;
        }
      }

      to = emailAdd;
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("sendPasswordEmail(1)", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    boolean sendOK = true;
    switch(FORGOT_PASSWORD)
    {
      case FORGOT_PASSWORD:
        subject = "Login Name and Password";
        message.append("Please use this url to return to your application: \n\n");
        message.append(protocol);
        message.append(host);
        message.append("/jsp/secure/vsLogin.jsp?cntrlnum=" + cntrlNum + "\n\n");
        message.append("Login    = " + login + "\n");
        message.append("Password = "  + password + "\n\n");
        message.append("Should you have any questions please call 1-888-288-2692.\n\n");
        message.append("Sincerely,\n\n");
        message.append("Application Review Team");
        break;
      default:
        break;
    }

    try
    {
      if(sendOK)
      {
        msg = new MailMessage();
        msg.setAddresses(emailType);
        if(emailType == MesEmails.MSG_ADDRS_VS_APP_SUBMITTED)
        {
          msg.addTo(to);
        }
        msg.setSubject(subject);
        msg.setText(message.toString());
        msg.send();
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(),"sendPasswordEmail2: " + e.toString());
    }
    return emailAdd;
  }


  public synchronized long getAppControlNum()
  {
    return this.appControlNum;
  }
  public synchronized long getVid()
  {
    return this.vid;
  }

  public synchronized int getAppType()
  {
    return appType;
  }
  public void setAppType(String appType)
  {
    try
    {
      setAppType(Integer.parseInt(appType));
    }
    catch(Exception e)
    {
      logEntry("setAppType(" + appType + ")", e.toString());
    }
  }
  public synchronized void setAppType(int appType)
  {
    this.appType = appType;
  }
  
  public boolean isEnforceOrder()
  {
    boolean result = false;
    return result;
  }

  public synchronized String getLoginDefault()
  {
    return this.loginDefault;
  }
  public synchronized boolean isLoggedIn()
  {
    return this.loggedIn;
  }
  public synchronized boolean doesExist()
  {
    return this.exist;
  }

  public synchronized boolean isNewApp()
  {
    return this.newApp;
  }
  public synchronized boolean isSuperUser()
  {
    return this.superUser;
  }

  public synchronized boolean isSaved()
  {
    return saved;
  }
  public synchronized boolean isDone()
  {
    return this.done;
  }
  
  public synchronized String getReturnURL()
  {
    return this.returnURL;
  }
  
  public synchronized String getReturnURL(long primaryKey)
  {
    String result = DEFAULT_RETURN_URL;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1187^7*/

//  ************************************************************
//  #sql [Ctx] { select  return_url 
//          from    vs_linking_table
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  return_url  \n        from    vs_linking_table\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.setup.VsAppIdBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1192^7*/
    }
    catch(Exception e)
    {
      logEntry("getReturnURL(" + primaryKey + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public synchronized boolean passwordExist()
  {
    return this.passExist;
  }

  public synchronized boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }

  public synchronized String getBusinessURL(long primaryKey)
  {
    String result = "";
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1231^7*/

//  ************************************************************
//  #sql [Ctx] { select  business_url 
//          from    vs_linking_table
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  business_url  \n        from    vs_linking_table\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.setup.VsAppIdBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1236^7*/
    }
    catch(Exception e)
    {
      logEntry("getBusinessURL(" + primaryKey + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
}/*@lineinfo:generated-code*/