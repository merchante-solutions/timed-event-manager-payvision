/*@lineinfo:filename=EquipmentHelperBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/EquipmentHelperBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.setup;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class EquipmentHelperBean extends SQLJConnectionBase
{
  private static final int  EQUIPMENT_NEEDED        = 1;
  private static final int  EQUIPMENT_OWNED         = 2;
  private static final int  ALL_EQUIPMENT_APP_TYPE  = -1;

  private boolean           needSuccess             = false;
  private boolean           ownSuccess              = false;

  private Vector            needPeripherals         = new Vector();
  private Vector            needTerminals           = new Vector();
  private Vector            needPrinters            = new Vector();
  private Vector            needPinpads             = new Vector();
  private Vector            needImprinters          = new Vector();
  private Vector            needTermPrints          = new Vector();
  private Vector            needTermPrintPins       = new Vector();
  
  private Vector            ownPeripherals          = new Vector();
  private Vector            ownTerminals            = new Vector();
  private Vector            ownPrinters             = new Vector();
  private Vector            ownPinpads              = new Vector();
  private Vector            ownImprinters           = new Vector();
  private Vector            ownTermPrints           = new Vector();
  private Vector            ownTermPrintPins        = new Vector();

  private HashMap           ownEquipDesc            = new HashMap();
  private HashMap           needEquipDesc           = new HashMap();


  public EquipmentHelperBean()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    String              tempModel   = "";
    String              tempProdId  = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:84^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   equip_type,equip_description,equip_model,prod_option_id 
//          from     equipment_application
//          where    app_type = :ALL_EQUIPMENT_APP_TYPE and equip_section = :EQUIPMENT_OWNED
//          order by equip_type,equip_sort_order asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   equip_type,equip_description,equip_model,prod_option_id \n        from     equipment_application\n        where    app_type =  :1  and equip_section =  :2 \n        order by equip_type,equip_sort_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.setup.EquipmentHelperBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,ALL_EQUIPMENT_APP_TYPE);
   __sJT_st.setInt(2,EQUIPMENT_OWNED);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.setup.EquipmentHelperBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:90^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        ownSuccess   = true;

        switch(rs.getInt("equip_type"))
        {
          case mesConstants.APP_EQUIP_TYPE_PERIPHERAL:
            tempModel  = "";
            tempProdId = "";

            tempModel   = isBlank(rs.getString("equip_model"))      ? "" : rs.getString("equip_model");
            tempProdId  = isBlank(rs.getString("prod_option_id"))   ? "" : rs.getString("prod_option_id");

            if(!isBlank(tempModel))
            {

              if(!isBlank(tempProdId))
              {
                tempModel = tempModel + "*" + tempProdId;
              }
              
              ownPeripherals.add(tempModel.trim());
              ownEquipDesc.put(tempModel.trim(),rs.getString("equip_description"));

              //@System.out.println("Just added " + tempModel + " to the ownPeripherals vector");
            }
            else
            {
              //@System.out.println(rs.getString("equip_description") + " had a null model number");
            }
          break;
          case mesConstants.APP_EQUIP_TYPE_TERMINAL:
            tempModel  = "";
            tempProdId = "";

            tempModel   = isBlank(rs.getString("equip_model"))    ? "" : rs.getString("equip_model");
            tempProdId  = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

            if(!isBlank(tempModel))
            {

              if(!isBlank(tempProdId))
              {
                tempModel = tempModel + "*" + tempProdId;
              }
              
              ownTerminals.add(tempModel.trim());
              ownEquipDesc.put(tempModel.trim(),rs.getString("equip_description"));

              //@System.out.println("Just added " + tempModel + " to the ownTerminals vector");
            }
            else
            {
              //@System.out.println(rs.getString("equip_description") + " had a null model number");
            }
          break;
          case mesConstants.APP_EQUIP_TYPE_PRINTER:
            tempModel  = "";
            tempProdId = "";

            tempModel   = isBlank(rs.getString("equip_model"))    ? "" : rs.getString("equip_model");
            tempProdId  = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

            if(!isBlank(tempModel))
            {

              if(!isBlank(tempProdId))
              {
                tempModel = tempModel + "*" + tempProdId;
              }
              
              ownPrinters.add(tempModel.trim());
              ownEquipDesc.put(tempModel.trim(),rs.getString("equip_description"));

              //@System.out.println("Just added " + tempModel + " to the ownPrinters vector");
            }
            else
            {
              //@System.out.println(rs.getString("equip_description") + " had a null model number");
            }
          break;
          case mesConstants.APP_EQUIP_TYPE_PINPAD:
            tempModel  = "";
            tempProdId = "";

            tempModel   = isBlank(rs.getString("equip_model"))    ? "" : rs.getString("equip_model");
            tempProdId  = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

            if(!isBlank(tempModel))
            {

              if(!isBlank(tempProdId))
              {
                tempModel = tempModel + "*" + tempProdId;
              }
              
              ownPinpads.add(tempModel.trim());
              ownEquipDesc.put(tempModel.trim(),rs.getString("equip_description"));

              //@System.out.println("Just added " + tempModel + " to the ownPinpads vector");
            }
            else
            {
              //@System.out.println(rs.getString("equip_description") + " had a null model number");
            }
          break;
          case mesConstants.APP_EQUIP_TYPE_IMPRINTER:
            tempModel  = "";
            tempProdId = "";

            tempModel   = isBlank(rs.getString("equip_model"))    ? "" : rs.getString("equip_model");
            tempProdId  = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

            if(!isBlank(tempModel))
            {

              if(!isBlank(tempProdId))
              {
                tempModel = tempModel + "*" + tempProdId;
              }
              
              ownImprinters.add(tempModel.trim());
              ownEquipDesc.put(tempModel.trim(),rs.getString("equip_description"));

              //@System.out.println("Just added " + tempModel + " to the ownImprinters vector");
            }
            else
            {
              //@System.out.println(rs.getString("equip_description") + " had a null model number");
            }
          break;
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINTER:
            tempModel  = "";
            tempProdId = "";

            tempModel   = isBlank(rs.getString("equip_model"))    ? "" : rs.getString("equip_model");
            tempProdId  = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

            if(!isBlank(tempModel))
            {

              if(!isBlank(tempProdId))
              {
                tempModel = tempModel + "*" + tempProdId;
              }
              
              ownTermPrints.add(tempModel.trim());
              ownEquipDesc.put(tempModel.trim(),rs.getString("equip_description"));

              //@System.out.println("Just added " + tempModel + " to the ownTermPrints vector");
            }
            else
            {
              //@System.out.println(rs.getString("equip_description") + " had a null model number");
            }
          break;
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD:
            tempModel  = "";
            tempProdId = "";

            tempModel   = isBlank(rs.getString("equip_model"))    ? "" : rs.getString("equip_model");
            tempProdId  = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

            if(!isBlank(tempModel))
            {

              if(!isBlank(tempProdId))
              {
                tempModel = tempModel + "*" + tempProdId;
              }
              
              ownTermPrintPins.add(tempModel.trim());
              ownEquipDesc.put(tempModel.trim(),rs.getString("equip_description"));

              //@System.out.println("Just added " + tempModel + " to the ownTermPrintPins vector");
            }
            else
            {
              //@System.out.println(rs.getString("equip_description") + " had a null model number");
            }
          break;
        }
      }
 
      it.close();
      
    }
    catch(Exception e)
    {
      logEntry("EquipmentHelperBean()", e.toString());
      //@System.out.println("Crashed while getting owned equipment from equipment_application");
      ownSuccess = false;
    }
    finally
    {
      cleanUp();
    }

  }
  
  public void setNeededEquipmentList(String appType)
  {
    try
    {
      setNeededEquipmentList(Integer.parseInt(appType));
    }
    catch(Exception e)
    {
      logEntry("setNeededEquipmentList(" + appType + ")", e.toString());
    }
  }

  public void setNeededEquipmentList(int appType)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             notFound    = true;
    String              tempModel   = "";
    String              tempProdId  = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:318^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   equip_type,equip_model,prod_option_id,equip_description 
//          from     equipment_application
//          where    app_type = :appType and equip_section = :EQUIPMENT_NEEDED
//          order by equip_type,equip_sort_order asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   equip_type,equip_model,prod_option_id,equip_description \n        from     equipment_application\n        where    app_type =  :1  and equip_section =  :2 \n        order by equip_type,equip_sort_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.setup.EquipmentHelperBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   __sJT_st.setInt(2,EQUIPMENT_NEEDED);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.setup.EquipmentHelperBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:324^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        notFound      = false;
        needSuccess   = true;

        switch(rs.getInt("equip_type"))
        {
          case mesConstants.APP_EQUIP_TYPE_PERIPHERAL:
            tempModel  = "";
            tempProdId = "";

            tempModel   = isBlank(rs.getString("equip_model"))    ? "" : rs.getString("equip_model");
            tempProdId  = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

            if(!isBlank(tempModel))
            {

              if(!isBlank(tempProdId))
              {
                tempModel = tempModel + "*" + tempProdId;
              }
              
              needPeripherals.add(tempModel.trim());
              needEquipDesc.put(tempModel.trim(),rs.getString("equip_description"));

              //@System.out.println("Just added " + tempModel + " to the needPeripherals vector");
            }
            else
            {
              //@System.out.println(rs.getString("equip_description") + " had a null model number");
            }
          break;

          case mesConstants.APP_EQUIP_TYPE_TERMINAL:
            tempModel  = "";
            tempProdId = "";

            tempModel   = isBlank(rs.getString("equip_model"))    ? "" : rs.getString("equip_model");
            tempProdId  = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

            if(!isBlank(tempModel))
            {

              if(!isBlank(tempProdId))
              {
                tempModel = tempModel + "*" + tempProdId;
              }
              
              needTerminals.add(tempModel.trim());
              needEquipDesc.put(tempModel.trim(),rs.getString("equip_description"));

              //@System.out.println("Just added " + tempModel + " to the needTerminals vector");
            }
            else
            {
              //@System.out.println(rs.getString("equip_description") + " had a null model number");
            }
          break;

          case mesConstants.APP_EQUIP_TYPE_PRINTER:
            tempModel  = "";
            tempProdId = "";

            tempModel   = isBlank(rs.getString("equip_model"))    ? "" : rs.getString("equip_model");
            tempProdId  = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

            if(!isBlank(tempModel))
            {

              if(!isBlank(tempProdId))
              {
                tempModel = tempModel + "*" + tempProdId;
              }
              
              needPrinters.add(tempModel.trim());
              needEquipDesc.put(tempModel.trim(),rs.getString("equip_description"));

              //@System.out.println("Just added " + tempModel + " to the needPrinters vector");
            }
            else
            {
              //@System.out.println(rs.getString("equip_description") + " had a null model number");
            }
          break;

          case mesConstants.APP_EQUIP_TYPE_PINPAD:
            tempModel  = "";
            tempProdId = "";

            tempModel   = isBlank(rs.getString("equip_model"))    ? "" : rs.getString("equip_model");
            tempProdId  = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

            if(!isBlank(tempModel))
            {

              if(!isBlank(tempProdId))
              {
                tempModel = tempModel + "*" + tempProdId;
              }
              
              needPinpads.add(tempModel.trim());
              needEquipDesc.put(tempModel.trim(),rs.getString("equip_description"));

              //@System.out.println("Just added " + tempModel + " to the needPinpads vector");
            }
            else
            {
              //@System.out.println(rs.getString("equip_description") + " had a null model number");
            }
          break;

          case mesConstants.APP_EQUIP_TYPE_IMPRINTER:
            tempModel  = "";
            tempProdId = "";

            tempModel   = isBlank(rs.getString("equip_model"))    ? "" : rs.getString("equip_model");
            tempProdId  = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

            if(!isBlank(tempModel))
            {

              if(!isBlank(tempProdId))
              {
                tempModel = tempModel + "*" + tempProdId;
              }
              
              needImprinters.add(tempModel.trim());
              needEquipDesc.put(tempModel.trim(),rs.getString("equip_description"));

              //@System.out.println("Just added " + tempModel + " to the needImprinters vector");
            }
            else
            {
              //@System.out.println(rs.getString("equip_description") + " had a null model number");
            }
          break;

          case mesConstants.APP_EQUIP_TYPE_TERM_PRINTER:
            tempModel  = "";
            tempProdId = "";

            tempModel   = isBlank(rs.getString("equip_model"))    ? "" : rs.getString("equip_model");
            tempProdId  = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

            if(!isBlank(tempModel))
            {

              if(!isBlank(tempProdId))
              {
                tempModel = tempModel + "*" + tempProdId;
              }
              
              needTermPrints.add(tempModel.trim());
              needEquipDesc.put(tempModel.trim(),rs.getString("equip_description"));

              //@System.out.println("Just added " + tempModel + " to the needTermPrints vector");
            }
            else
            {
              //@System.out.println(rs.getString("equip_description") + " had a null model number");
            }
          break;

          case mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD:
            tempModel  = "";
            tempProdId = "";

            tempModel   = isBlank(rs.getString("equip_model"))    ? "" : rs.getString("equip_model");
            tempProdId  = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

            if(!isBlank(tempModel))
            {

              if(!isBlank(tempProdId))
              {
                tempModel = tempModel + "*" + tempProdId;
              }
              
              needTermPrintPins.add(tempModel.trim());
              needEquipDesc.put(tempModel.trim(),rs.getString("equip_description"));

              //@System.out.println("Just added " + tempModel + " to the needTermPrintPins vector");
            }
            else
            {
              //@System.out.println(rs.getString("equip_description") + " had a null model number");
            }
          break;
        }
      }
 
      it.close();
      
      if(notFound && appType != mesConstants.APP_TYPE_MES)//then use mes equipment list
      {
        setNeededEquipmentList(mesConstants.APP_TYPE_MES);
      }
      else if(notFound && appType == mesConstants.APP_TYPE_MES)//we got problems
      {
        needSuccess = false;
        logEntry("setEquipmentList()", "Couldn't find any equipment for drop downs on application");
      }
  
    }
    catch(Exception e)
    {
      logEntry("setEquipmentList()", e.toString());
      //@System.out.println("Crashed while getting equipment from equipment_application");
      needSuccess = false;
    }
    finally
    {
      cleanUp();
    }
  }

  public boolean isNeedSuccessFull()
  {
    return this.needSuccess;
  }

  public boolean isOwnSuccessFull()
  {
    return this.ownSuccess;
  }

  public String getNeedDesc(String needModel)
  {
    String result = "";

    try
    {
        result = isBlank((String)needEquipDesc.get(needModel)) ? "" : (String)needEquipDesc.get(needModel);
    }
    catch(Exception e)
    {
    }
    return result;
  }

  public String getOwnDesc(String ownModel)
  {
    String result = "";

    try
    {
        result = isBlank((String)ownEquipDesc.get(ownModel)) ? "" : (String)ownEquipDesc.get(ownModel);
    }
    catch(Exception e)
    {
    }
    return result;
  }

  public String decodeModel(String code)
  {
    String result = "";
    
    if(isBlank(code))
    {
      return result;
    }

    try
    {

      int idx = code.indexOf("*");

      if(idx >= 0)
      {
        result = (code.substring(0,idx)).trim();
      }
      else if(idx == -1)
      {
        result = code.trim();
      }

    }
    catch(Exception e)
    {
    }
    return result;
  }

  public String decodeProdId(String code)
  {
    String result = "";
    
    if(isBlank(code))
    {
      return result;
    }

    try
    {

      int idx = code.indexOf("*");

      if(idx >= 0)
      {
        result = (code.substring(idx+1)).trim();
      }
      else if(idx == -1)
      {
        result = "";
      }

    }
    catch(Exception e)
    {
    }
    return result;
  }


  public String[] getNeedPeripherals()
  {
    String[] tempArr = new String[1];

    try
    {
      if(needPeripherals.size() == 0)
      {
        return null;
      }
      
      tempArr = (String[])(needPeripherals.toArray(tempArr));    
    }
    catch(Exception e)
    {
      //@System.out.println("getNeedPeripherals crashed: " + e.toString());
    }

    return tempArr;
  }
  
  public String[] getNeedTerminals()
  {
    String[] tempArr = new String[1];

    try
    {
      if(needTerminals.size() == 0)
      {
        return null;
      }
      tempArr = (String[])(needTerminals.toArray(tempArr));
    }
    catch(Exception e)
    {
      //@System.out.println("getNeedTerminals crashed: " + e.toString());
    }

    return tempArr;
  }

  public String[] getNeedPrinters()
  {
    String[] tempArr = new String[1];

    try
    {
      if(needPrinters.size() == 0)
      {
        return null;
      }
      tempArr = (String[])(needPrinters.toArray(tempArr));    
    }
    catch(Exception e)
    {
      //@System.out.println("getNeedPrinters crashed: " + e.toString());
    }

    return tempArr;
  }

  public String[] getNeedPinpads()
  {
    String[] tempArr = new String[1];

    try
    {
      if(needPinpads.size() == 0)
      {
        return null;
      }
      tempArr = (String[])(needPinpads.toArray(tempArr));    
    }
    catch(Exception e)
    {
      //@System.out.println("getNeedPinpads crashed: " + e.toString());
    }

    return tempArr;
  }

  public String[] getNeedImprinters()
  {
    String[] tempArr = new String[1];

    try
    {
      if(needImprinters.size() == 0)
      {
        return null;
      }
      tempArr = (String[])(needImprinters.toArray(tempArr));    
    }
    catch(Exception e)
    {
      //@System.out.println("getNeedImprinters crashed: " + e.toString());
    }

    return tempArr;
  }

  public String[] getNeedTermPrints()
  {
    String[] tempArr = new String[1];

    try
    {
      if(needTermPrints.size() == 0)
      {
        return null;
      }
      tempArr = (String[])(needTermPrints.toArray(tempArr));    
    }
    catch(Exception e)
    {
      //@System.out.println("getNeedTermPrints crashed: " + e.toString());
    }

    return tempArr;
  }

  public String[] getNeedTermPrintPins()
  {
    String[] tempArr = new String[1];

    try
    {
      if(needTermPrintPins.size() == 0)
      {
        return null;
      }
      tempArr = (String[])(needTermPrintPins.toArray(tempArr));    
    }
    catch(Exception e)
    {
      //@System.out.println("getNeedTermPrintPins crashed: " + e.toString());
    }

    return tempArr;
  }





  public String[] getOwnPeripherals()
  {
    String[] tempArr = new String[1];

    try
    {
      if(ownPeripherals.size() == 0)
      {
        return null;
      }
      tempArr = (String[])(ownPeripherals.toArray(tempArr));    
    }
    catch(Exception e)
    {
      //@System.out.println("getOwnPeripherals crashed: " + e.toString());
    }

    return tempArr;
  }
  
  public String[] getOwnTerminals()
  {
    String[] tempArr = new String[1];

    try
    {
      if(ownTerminals.size() == 0)
      {
        return null;
      }
      tempArr = (String[])(ownTerminals.toArray(tempArr));
    }
    catch(Exception e)
    {
      //@System.out.println("getOwnTerminals crashed: " + e.toString());
    }

    return tempArr;
  }

  public String[] getOwnPrinters()
  {
    String[] tempArr = new String[1];

    try
    {
      if(ownPrinters.size() == 0)
      {
        return null;
      }
      tempArr = (String[])(ownPrinters.toArray(tempArr));    
    }
    catch(Exception e)
    {
      //@System.out.println("getOwnPrinters crashed: " + e.toString());
    }

    return tempArr;
  }

  public String[] getOwnPinpads()
  {
    String[] tempArr = new String[1];

    try
    {
      if(ownPinpads.size() == 0)
      {
        return null;
      }
      tempArr = (String[])(ownPinpads.toArray(tempArr));    
    }
    catch(Exception e)
    {
      //@System.out.println("getOwnPinpads crashed: " + e.toString());
    }

    return tempArr;
  }

  public String[] getOwnImprinters()
  {
    String[] tempArr = new String[1];

    try
    {
      if(ownImprinters.size() == 0)
      {
        return null;
      }
      tempArr = (String[])(ownImprinters.toArray(tempArr));    
    }
    catch(Exception e)
    {
      //@System.out.println("getOwnImprinters crashed: " + e.toString());
    }

    return tempArr;
  }

  public String[] getOwnTermPrints()
  {
    String[] tempArr = new String[1];

    try
    {
      if(ownTermPrints.size() == 0)
      {
        return null;
      }
      tempArr = (String[])(ownTermPrints.toArray(tempArr));    
    }
    catch(Exception e)
    {
      //@System.out.println("getOwnTermPrints crashed: " + e.toString());
    }

    return tempArr;
  }

  public String[] getOwnTermPrintPins()
  {
    String[] tempArr = new String[1];

    try
    {
      if(ownTermPrintPins.size() == 0)
      {
        return null;
      }
      tempArr = (String[])(ownTermPrintPins.toArray(tempArr));    
    }
    catch(Exception e)
    {
      //@System.out.println("getOwnTermPrintPins crashed: " + e.toString());
    }

    return tempArr;
  }

  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }



}/*@lineinfo:generated-code*/