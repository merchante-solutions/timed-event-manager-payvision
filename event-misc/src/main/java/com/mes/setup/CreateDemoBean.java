/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/CreateDemoBean.java $

  Description:  

   
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-05-31 14:24:22 -0700 (Thu, 31 May 2012) $
  Version            : $Revision: 20222 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.setup;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.DBConnect;

public class CreateDemoBean
{

  private String  discountFee      = "-1";
  private String  transactionFee   = "-1";
  private String  midqualDg        = "-1";
  private String  nonqualDg        = "-1";
  private String  miscFee1         = "-1";
  private String  miscFee2         = "-1";
  private String  miscFee3         = "-1";
  private String  miscFee4         = "-1";
  private String  miscLable1       = "-1";
  private String  miscLable2       = "-1";
  private String  miscLable3       = "-1";
  private String  miscLable4       = "-1";
  private String  companyName      = "-1";
  private String  userLogin        = "-1";
  private String  userPassword     = "-1";
  private String  demoGif          = "-1";
  private String  demoWidth        = "-1";
  private String  demoHeight       = "-1";
  private Vector  errors           = new Vector();
  private boolean submit           = false;
  private int     userId           = 0;

  public boolean validated()
  {
    
    if(this.companyName.equals("-1"))
      addError("Please enter a Company Name");
    if(this.userLogin.equals("-1"))
      addError("please enter a User Login Id");
    if(this.userPassword.equals("-1")) 
      addError("Please enter a User Login Password");
    if(!this.demoGif.equals("-1") && (this.demoWidth.equals("-1") || this.demoWidth.equals("-2")) && (this.demoHeight.equals("-1") || this.demoHeight.equals("-2")))
      addError("Please enter a valid width and height for Company Logo image");
    if(this.discountFee.equals("-2"))
      addError("Please enter a valid Discount Rate Fee");
    if(this.transactionFee.equals("-2"))
      addError("Please enter a valid Transaction Fee");
    if(this.midqualDg.equals("-2"))
      addError("Please enter a valid Mid-Qualification Downgrade");
    if(this.nonqualDg.equals("-2"))
      addError("Please enter a valid Non-Qualification Downgrade");
    if(this.miscFee1.equals("-2"))
      addError("Please enter a valid Setup Fee");
    if(this.miscFee2.equals("-2"))
      addError("Please enter a valid Chargeback Fee");
    if(this.miscFee3.equals("-2"))
      addError("Please enter a valid Monthly Statement Fee");
    if(this.miscFee4.equals("-2"))
      addError("Please enter a valid Minimum Monthly Fee");

    if(!hasErrors())
      createUser();
    if(!hasErrors())
      setSequenceDemoInfo();

    return !hasErrors();
  }

  private void setSequenceDemoInfo()
  {
    DBConnect         db  = new DBConnect(this, "setSequenceDemoInfo");
    StringBuffer      qs  = new StringBuffer("");
    Connection        con = null;
    PreparedStatement ps  = null;
    ResultSet         rs  = null;

    try 
    {
      qs.append("insert into sequence_demo_info (");
      qs.append("transaction_fee, ");
      qs.append("discount_rate_fee, ");
      qs.append("midqual_dg, ");
      qs.append("nonqual_dg, ");
      qs.append("misc_lable1, ");
      qs.append("misc_lable2, ");
      qs.append("misc_lable3, ");
      qs.append("misc_lable4, ");
      qs.append("misc_fee1, ");
      qs.append("misc_fee2, ");
      qs.append("misc_fee3, ");
      qs.append("misc_fee4, ");
      qs.append("demo_gif, ");
      qs.append("demo_width, ");
      qs.append("demo_height, user_id) ");
      qs.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

      con = db.getConnection("pool;ApplicationPool");

      ps = con.prepareStatement(qs.toString());
      
      ps.setString(1,this.transactionFee.equals("-1")  ? "" : this.transactionFee);
      ps.setString(2,this.discountFee.equals("-1")     ? "" : this.discountFee);
      ps.setString(3,this.midqualDg.equals("-1")       ? "" : this.midqualDg);
      ps.setString(4,this.nonqualDg.equals("-1")       ? "" : this.nonqualDg);
      ps.setString(5,this.miscLable1.equals("-1")      ? "" : this.miscLable1);
      ps.setString(6,this.miscLable2.equals("-1")      ? "" : this.miscLable2);
      ps.setString(7,this.miscLable3.equals("-1")      ? "" : this.miscLable3);
      ps.setString(8,this.miscLable4.equals("-1")      ? "" : this.miscLable4);
      ps.setString(9,this.miscFee1.equals("-1")        ? "" : this.miscFee1);
      ps.setString(10,this.miscFee2.equals("-1")       ? "" : this.miscFee2);
      ps.setString(11,this.miscFee3.equals("-1")       ? "" : this.miscFee3);
      ps.setString(12,this.miscFee4.equals("-1")       ? "" : this.miscFee4);
      ps.setString(13,this.demoGif.equals("-1")        ? "" : "/images/" + this.demoGif);
      ps.setString(14,this.demoWidth);
      ps.setString(15,this.demoHeight);
      ps.setInt(16,this.userId);
      
        
      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setSequenceDemoInfo: unable to add to SequenceDemoInfo");
        addError("setSequenceDemoInfo: unable to add to SequenceDemoInfo");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setSequenceDemoInfo: " + e.toString());
      addError("setSequenceDemoInfo: " + e.toString());
    }
    finally
    {
      try
      {
        db.releaseConnection();
      }
      catch(Exception e)
      {
      }
    }
  }
  private void createUser()
  {
    int                 paramIndex        = 1;
    StringBuffer        sqlStatement      = new StringBuffer("");
    CallableStatement   statement         = null;
    
    try
    {
      DBConnect           DbConnectionManager = new DBConnect(this, "createUser");
      Connection          connection        = DbConnectionManager.getConnection( "pool;reportPool" );
    
      statement = connection.prepareCall("{? = call create_user(?,?,?,?,?,?,?) }");
      
      //statement.registerOutParameter( paramIndex++, oracle.jdbc.driver.OracleTypes.NUMBER );

      statement.setLong   ( paramIndex++, 0L );
      statement.setLong   ( paramIndex++, 8L );
      statement.setString ( paramIndex++, this.userLogin );
      statement.setString ( paramIndex++, this.userPassword );
      statement.setLong   ( paramIndex++, com.mes.constants.mesConstants.FUNC_APPLICATION_DEMO );
      statement.setString ( paramIndex++, this.companyName );
      statement.setString ( paramIndex++, "demo@demo.com" );
      
      statement.execute();

      this.userId = ((BigDecimal)statement.getObject(1)).intValue();
      if ( userId < 0 )
      {
        switch( userId )
        {
          case -1:
            addError("This User Login Id already exists!  Please select a new one.");
            break;
          default:
            addError("Unable to create Demo user!  Seek Technical Advice.");
            break;
        }
      }

      statement.close();
    }
    catch(java.sql.SQLException e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "createUser: " + e.toString());
      addError("createUser: " + e.toString());
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "createUser: " + e.toString());
      addError("createUser: " + e.toString());
    }
  }
  
  public String getDiscountFee()
  {
    String result = "";
    if(!this.discountFee.equals("-1") && !this.discountFee.equals("-2"))
      result = this.discountFee;
    return result;
  }    
  public String getTransactionFee()
  {
    String result = "";
    if(!this.transactionFee.equals("-1") && !this.transactionFee.equals("-2"))
      result = this.transactionFee;
    return result;
  }
  public String getMidqualDg()
  {
    String result = "";
    if(!this.midqualDg.equals("-1") && !this.midqualDg.equals("-2"))
      result = this.midqualDg;
    return result;
  }
  public String getNonqualDg()
  {
    String result = "";
    if(!this.nonqualDg.equals("-1") && !this.nonqualDg.equals("-2"))
      result = this.nonqualDg;
    return result;
  }
  public String getMiscFee1()
  {
    String result = "";
    if(!this.miscFee1.equals("-1") && !this.miscFee1.equals("-2"))
      result = this.miscFee1;
    return result;
  }    
  public String getMiscFee2()
  {
    String result = "";
    if(!this.miscFee2.equals("-1") && !this.miscFee2.equals("-2"))
      result = this.miscFee2;
    return result;
  }    
  public String getMiscFee3()
  {
    String result = "";
    if(!this.miscFee3.equals("-1") && !this.miscFee3.equals("-2"))
      result = this.miscFee3;
    return result;
  }    
  public String getMiscFee4()
  {
    String result = "";
    if(!this.miscFee4.equals("-1") && !this.miscFee4.equals("-2"))
      result = this.miscFee4;
    return result;
  }    
  public String getMiscLable1()
  {
    String result = "";
    if(!this.miscLable1.equals("-1"))
      result = this.miscLable1;
    return result;
  }    
  public String getMiscLable2()
  {
    String result = "";
    if(!this.miscLable2.equals("-1"))
      result = this.miscLable2;
    return result;
  }    
  public String getMiscLable3()
  {
    String result = "";
    if(!this.miscLable3.equals("-1"))
      result = this.miscLable3;
    return result;
  }    
  public String getMiscLable4()
  {
    String result = "";
    if(!this.miscLable4.equals("-1"))
      result = this.miscLable4;
    return result;
  }    

  public String getCompanyName()
  {
    String result = "";
    if(!this.companyName.equals("-1"))
      result = this.companyName;
    return result;
  }    
  public String getUserLogin()
  {
    String result = "";
    if(!this.userLogin.equals("-1"))
      result = this.userLogin;
    return result;
  }    
  public String getUserPassword()
  {
    String result = "";
    if(!this.userPassword.equals("-1"))
      result = this.userPassword;
    return result;
  }    
  public String getDemoGif()
  {
    String result = "";
    if(!this.demoGif.equals("-1"))
      result = this.demoGif;
    return result;
  }    
  public String getDemoWidth()
  {
    String result = "";
    if(!this.demoWidth.equals("-1") && !this.demoWidth.equals("-2"))
      result = this.demoWidth;
    return result;
  }
  public String getDemoHeight()
  {
    String result = "";
    if(!this.demoHeight.equals("-1") && !this.demoHeight.equals("-2"))
      result = this.demoHeight;
    return result;
  }    

  public void setDiscountFee(String discountFee)
  {
    if(!isBlank(discountFee))
    {
      if(isFloat(discountFee))
        this.discountFee = discountFee;
      else
        this.discountFee = "-2";
    }
  }    
  public void setTransactionFee(String transactionFee)
  {
    if(!isBlank(transactionFee))
    {
      if(isFloat(transactionFee))
        this.transactionFee = transactionFee;
      else
        this.transactionFee = "-2";
    }
  }
  public void setMidqualDg(String midqualDg)
  {
    if(!isBlank(midqualDg))
    {
      if(isFloat(midqualDg))
        this.midqualDg = midqualDg;
      else
        this.midqualDg = "-2";
    }
  }
  public void setNonqualDg(String nonqualDg)
  {
    if(!isBlank(nonqualDg))
    {
      if(isFloat(nonqualDg))
        this.nonqualDg = nonqualDg;
      else
        this.nonqualDg = "-2";
    }
  }
  public void setMiscFee1(String miscFee1)
  {
    if(!isBlank(miscFee1))
    {
      if(isFloat(miscFee1))
        this.miscFee1 = miscFee1;
      else
        this.miscFee1 = "-2";
    }
  }    
  public void setMiscFee2(String miscFee2)
  {
    if(!isBlank(miscFee2))
    {
      if(isFloat(miscFee2))
        this.miscFee2 = miscFee2;
      else
        this.miscFee2 = "-2";
    }
  }    
  public void setMiscFee3(String miscFee3)
  {
    if(!isBlank(miscFee3))
    {
      if(isFloat(miscFee3))
        this.miscFee3 = miscFee3;
      else
        this.miscFee3 = "-2";
    }
  }    
  public void setMiscFee4(String miscFee4)
  {                           
    if(!isBlank(miscFee4))
    {
      if(isFloat(miscFee4))
        this.miscFee4 = miscFee4;
      else
        this.miscFee4 = "-2";
    }
  }
  
  public void setCompanyName(String companyName)
  {
    this.companyName = companyName;
  }    
  public void setUserLogin(String userLogin)
  {
    this.userLogin = userLogin;
  }    
  public void setUserPassword(String userPassword)
  {
    this.userPassword = userPassword;
  }    
  public void setDemoGif(String demoGif)
  {
    this.demoGif = demoGif;
  }    
  public void setDemoWidth(String demoWidth)
  {
    if(!isBlank(demoWidth))
    {
      if(isInt(demoWidth))
        this.demoWidth = demoWidth;
      else
        this.demoWidth = "-2";
    }
  }
  public void setDemoHeight(String demoHeight)
  {
    if(!isBlank(demoHeight))
    {
      if(isInt(demoHeight))
        this.demoHeight = demoHeight;
      else
        this.demoHeight = "-2";
    }
  }    
      
  public void setMiscLable1(String miscLable1)
  {
    this.miscLable1 = miscLable1;
  }    
  public void setMiscLable2(String miscLable2)
  {
    this.miscLable2 = miscLable2;
  }    
  public void setMiscLable3(String miscLable3)
  {
    this.miscLable3 = miscLable3;
  }    
  public void setMiscLable4(String miscLable4)
  {
    this.miscLable4 = miscLable4;
  }    
  
  public boolean isFloat(String temp)
  {
    boolean retVal = true;
    try
    {
      float tempFloat = Float.parseFloat(temp);
      if(tempFloat < 0.0)
        retVal = false;
    }
    catch(Exception e)
    {
      retVal = false;  
    }
    return retVal;
  }
  public boolean isInt(String temp)
  {
    boolean retVal = true;
    try
    {
      int tempInt = Integer.parseInt(temp);
      if(tempInt < 1)
        retVal = false;
    }
    catch(Exception e)
    {
      retVal = false;  
    }
    return retVal;
  }
  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }
  protected void addError(String error)
  {
    try
    {
      errors.add(error);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "addError: " + e.toString());
    }
  }
  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }
  public Vector getErrors()
  {
    return this.errors;
  }
  public int getUserId()
  {
    return this.userId;
  }
  public boolean isSubmitted()
  {
    return this.submit;
  }
  public void setSubmit(String submit)
  {
    this.submit = true;
  }
  public boolean getSubmit()
  {
    return this.submit;
  }
}
