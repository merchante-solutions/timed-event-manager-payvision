/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/TransPricingBean.java $

  Description:  

   
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-05-21 15:33:20 -0700 (Wed, 21 May 2008) $
  Version            : $Revision: 14888 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.setup;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;

/**
 *  This bean is used for all the application whose source is not Merchant Direct.
 *  The options selected by the user on the Payment Options page and the equipment
 *  pages are populated with there default values.
 *  The data inserted is updated into the database.
 */
public class TransPricingBean extends com.mes.screens.SequenceDataBean
{
  public final static int NUM_CARDS               = 10;
  public final static int NUM_QUESTIONS           = 5;
  private final static int TRANSACTION_MIN_CHARGE = 25;

  public final static String STATEMENT_BREAKDOWN_DETAIL       = "D";
  public final static String STATEMENT_BREAKDOWN_BUCKET       = "B";
  public final static String CHECK_RECEIVED_YES               = "Y";
  public final static String CHECK_RECEIVED_NO                = "N";
  public final static String MISC_FEE_MONTHLY                 = "MC";
  public final static String MISC_FEE_ONETIME                 = "OT";

  public final static int CARD_DINERS    = 0;
  public final static int CARD_DISCOVER  = 1;
  public final static int CARD_JCB       = 2;
  public final static int CARD_AMEX      = 3;
  public final static int CARD_DEBIT     = 4;
  public final static int CARD_CHECK     = 5;
  public final static int CARD_INTERNET  = 6;
  public final static int CARD_DIALPAY   = 7;
  public final static int CARD_EBT       = 8;
  public final static int CARD_TYME      = 9;

  public final static int FEE_CHARGEBACK            = mesConstants.APP_MISC_CHARGE_CHARGEBACK;
  public final static int FEE_RETRIEVAL             = mesConstants.APP_MISC_CHARGE_RETRIEVAL_FEE;
  public final static int FEE_STATEMENT             = mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT;
  public final static int FEE_INTERNET_REPORTING    = mesConstants.APP_MISC_CHARGE_WEB_REPORTING;
  public final static int FEE_APPLICATION           = mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_APP;
  public final static int FEE_TERMINAL_REPROGRAM    = mesConstants.APP_MISC_CHARGE_TERM_REPROG_FEE;
  public final static int FEE_PINPAD_SWAP           = mesConstants.APP_MISC_CHARGE_PINPAD_SWAP_FEE;
  public final static int FEE_ACH_DEPOSIT           = mesConstants.APP_MISC_CHARGE_ACH_DEPOSIT_FEE;
  public final static int FEE_DEBIT_ACCESS          = mesConstants.APP_MISC_CHARGE_DEBIT_ACCESS_FEE;
  public final static int FEE_INTERNET_STARTUP      = mesConstants.APP_MISC_CHARGE_GATEWAY_SETUP;
  public final static int FEE_GATEWAY_MONTHLY       = mesConstants.APP_MISC_CHARGE_MONTHLY_GATEWAY;
  public final static int FEE_ANNUAL                = mesConstants.APP_MISC_CHARGE_ANNUAL_FEE;
  public final static int FEE_TRAINING              = mesConstants.APP_MISC_CHARGE_TRAINING_FEE;
  public final static int FEE_MISCELLANEOUS1        = mesConstants.APP_MISC_CHARGE_MISC1_FEE;
  public final static int FEE_MISCELLANEOUS2        = mesConstants.APP_MISC_CHARGE_MISC2_FEE;
  public final static int FEE_VOICE_AUTH            = mesConstants.APP_MISC_CHARGE_VOICE_AUTH_FEE;
  public final static int FEE_POS_PARTNER_MONTHLY   = mesConstants.APP_MISC_CHARGE_POS_MONTHLY_SERVICE_FEE;
  public final static int FEE_WIRELESS_SETUP        = mesConstants.APP_MISC_CHARGE_WIRELESS_SETUP_FEE;
  public final static int FEE_WIRELESS_MONTHLY      = mesConstants.APP_MISC_CHARGE_WIRELESS_MONTHLY_FEE;
  public final static int FEE_WIRELESS_TRANSACTION  = mesConstants.APP_MISC_CHARGE_WIRELESS_TRANSACTION_FEE;
  public final static int FEE_VT_SETUP              = mesConstants.APP_MISC_CHARGE_VT_SETUP_FEE;
  public final static int FEE_VT_MONTHLY            = mesConstants.APP_MISC_CHARGE_VT_MONTHLY_FEE;
  public final static int FEE_SUPPLY                = mesConstants.APP_MISC_CHARGE_MONTHLY_SUPPLIES;

  private final static int[]    cardCodes         = new int[NUM_CARDS];
  private final static String[] questionDesc      = new String[NUM_QUESTIONS];

  //CHARGES FOR TRANSCOM
  private String chargebackFee                    = "20";
  private String retrievalRequestFee              = "";
  private String statementFee                     = "";
  private String internetReportFee                = "";
  private String applicationFee                   = "";
  private String terminalReprogrammingFee         = "";
  private String numTermsReprogramming            = "";
  private String pinpadSwapFee                    = "";
  private String minMonthDiscount                 = "";
  private String achDepositFee                    = "0.25";
  private String debitAccessFee                   = "";
  private String internetStartupFee               = "";
  private String gatewayFee                       = "";
  private String annualFee                        = "";
  private String supplyFee                        = "";
  private String trainingFee                      = "";
  private String miscellaneous1Fee                = "";
  private String miscellaneous1ChargeBasis        = "";
  private String miscellaneous1FeeDesc            = "";
  private String miscellaneous2Fee                = "";
  private String miscellaneous2ChargeBasis        = "";
  private String miscellaneous2FeeDesc            = "";
  private String wirelessSetupFee                 = "";
  private String wirelessMonthlyFee               = "";
  private String wirelessTransactionFee           = "";


  private String chargebackFeeFlag                = "Y";
  private String retrievalRequestFeeFlag          = "N";
  private String statementFeeFlag                 = "N";
  private String internetReportFeeFlag            = "N";
  private String applicationFeeFlag               = "N";
  private String terminalReprogrammingFeeFlag     = "N";
  private String numTermsReprogrammingFlag        = "N";
  private String pinpadSwapFeeFlag                = "N";
  private String minMonthDiscountFlag             = "N";
  private String achDepositFeeFlag                = "Y";
  private String debitAccessFeeFlag               = "N";
  private String internetStartupFeeFlag           = "N";
  private String gatewayFeeFlag                   = "N";
  private String annualFeeFlag                    = "N";
  private String trainingFeeFlag                  = "N";
  private String miscellaneous1FeeFlag            = "N";
  private String miscellaneous2FeeFlag            = "N";

  private String wirelessSetupFeeFlag             = "N";
  private String wirelessMonthlyFeeFlag           = "N";
  private String wirelessTransactionFeeFlag       = "N";

  private String vtSetupFeeFlag                   = "N";
  private String vtSetupFee                       = "";
  private String vtMonthlyFeeFlag                 = "N";
  private String vtMonthlyFee                     = "";

  private String checkReceivedChrgFlag            = "N";
  private String checkReceivedChrgAmount          = "";
  private String checkReceivedEquipFlag           = "N";
  private String checkReceivedEquipAmount         = "";
  private String achDebitEquipFlag                = "N";
  private String achDebitEquipAmount              = "";

  private String voiceAuthFee                     = ".85";
  private String posPartnerMonthlyFee             = "";

  private String[]  questionFlag          = new String[NUM_QUESTIONS];
  private String[]  cardDesc              = new String[NUM_CARDS];
  private String[]  cardSelectedFlag      = new String[NUM_CARDS];
  private String[]  cardFee               = new String[NUM_CARDS];
  
  private double    vmcAuthFee            = 0.0;
  
  private Vector    pricingGrids            = new Vector();
  private Vector    pricingGridsCode        = new Vector();
  private Vector    billingElementTableDesc = new Vector();
  private Vector    billingElementTableCode = new Vector();
  
  private int       appType               = -1;
  private String    pricingGrid           = "";
  private String    statementBreakdown    = "";
  private String    repCode               = "803";
  private Date      dateReceived          = null;
  private Date      dateSubmitted         = null;

  private int pricingScenario             = mesConstants.TRANS_APP_BUCKET_WITH_PERITEM_PLAN;

  private double transDiscountRate9       = -1;
  private String transSurcharge9          = "0.5";
  private double transDiscountRate10      = -1;
  private double transPerItem10           = -1;
  private String transSurcharge10         = "0.5";
  private double transDiscountRate11      = -1;
  private double transPerItem11           = -1;
  private double transDiscountRate12      = -1;
  private double transPerItem12           = -1;
  
  private String billingElementTable      = "";

  private String pcQuantity               = "-1";
  private String pcFee                    = "-1";
  private String pcModel                  = "Unknown Model";

  private String ownedEquipQty            = "-1";
  private String ownedEquipAmt            = "-1";
  private String ownedEquipFlag           = "N";

  private boolean rental                  = false;
  private boolean sales                   = false;
  private boolean lease                   = false;

  private String rentalPrice              = "";
  private String salesPrice               = "";
  private String leasePrice               = "";

  private ResultSet equipmentInfo         = null;
  private int equipmentInfoCount          = 0;

  private String businessLocation         = "-1";
  private String businessLocationComment  = "-1";
  private String businessAddress          = "-1";
  private String businessAddressStreet    = "-1";
  private String businessAddressCity      = "-1";
  private String businessAddressState     = "-1";
  private String businessAddressZip       = "-1";
  private String numEmployees             = "-1";
  
  private String houseName                = "-1";
  private String houseFlag                = "-1";
  private String inventoryValue           = "-1";
  private String inventoryAddressStreet   = "-1";
  private String inventoryAddressCity     = "-1";
  private String inventoryAddressState    = "-1";
  private String inventoryAddressZip      = "-1";
  private String houseStreet              = "-1";
  private String houseCity                = "-1";
  private String houseState               = "-1";
  private String houseZip                 = "-1";
  private String softwareFlag             = "-1";
  private String softwareComment          = "-1";
  private String supplyConfig             = "NCLUB";
  private String comment                  = "";

  private int paySolOption                = -1;
  private double subtotal                 = 0.0;
  private double subtotalPur              = 0.0;
  private double grandtotal               = 0.0;
  private double tax                      = 0.0;
  private double taxRate                  = 0.0;
  private boolean recalculate             = false;
  private boolean allowSubmit             = false;
  private boolean amexZero                = false;
  private boolean amexSplit               = false;
  private boolean activec                 = false;
  private boolean cybercash               = false;
  private boolean wirelessEquip           = false;

  /*
  ** Default Contstructor
  */      
  public TransPricingBean() 
  { 
    super.init();
  }
  
  public void fillDropDowns(long appSeqNum)
  {
    // fill pricing grid vector
    try
    {
      billingElementTableDesc.add("Select One");
      billingElementTableCode.add("");
      
      billingElementTableDesc.add("4000/5000 (no downgrade assessments)");
      billingElementTableCode.add("4000");

      billingElementTableDesc.add("4040/5001 (interchange pass through)");
      billingElementTableCode.add("4040");

      billingElementTableDesc.add("4010/5010 (retail - bucket - no points)");
      billingElementTableCode.add("4010");

      billingElementTableDesc.add("4011/5011 (retail - bucket - 10 points)");
      billingElementTableCode.add("4011");

      billingElementTableDesc.add("4012/5012 (retail - bucket - 20 points)");
      billingElementTableCode.add("4012");

      billingElementTableDesc.add("4013/5013 (retail - bucket - 30 points)");
      billingElementTableCode.add("4013");

      billingElementTableDesc.add("4014/5014 (retail - bucket - 40 points)");
      billingElementTableCode.add("4014");

      billingElementTableDesc.add("4015/5015 (retail - bucket - 50 points)");
      billingElementTableCode.add("4015");

      billingElementTableDesc.add("4072/5072 (retail - detailed statement)");
      billingElementTableCode.add("4072");

      billingElementTableDesc.add("4082/5082 (retail - detailed, 50% debit rebate statement)");
      billingElementTableCode.add("4082");

      billingElementTableDesc.add("4092/5092 (retail - detailed, 100% debit rebate statement)");
      billingElementTableCode.add("4092");

      billingElementTableDesc.add("4022/5022 (moto - bucket - no points)");
      billingElementTableCode.add("4022");

      billingElementTableDesc.add("4023/5023 (moto - bucket - 10 points)");
      billingElementTableCode.add("4023");

      billingElementTableDesc.add("4024/5024 (moto - bucket - 20 points)");
      billingElementTableCode.add("4024");

      billingElementTableDesc.add("4025/5025 (moto - bucket - 30 points)");
      billingElementTableCode.add("4025");

      billingElementTableDesc.add("4026/5026 (moto - bucket - 40 points)");
      billingElementTableCode.add("4026");

      billingElementTableDesc.add("4027/5027 (moto - bucket - 50 points)");
      billingElementTableCode.add("4027");

      billingElementTableDesc.add("4073/5073 (moto - detailed w/out debit rebate)");
      billingElementTableCode.add("4073");

      billingElementTableDesc.add("4093/5093 (moto - detailed w/ debit rebate)");
      billingElementTableCode.add("4093");
      
    
      pricingGrids.add("Select One");
      pricingGridsCode.add("");
      pricingGrids.add("Retail");
      pricingGridsCode.add(Integer.toString(mesConstants.APP_PG_RETAIL));
      pricingGrids.add("Moto");
      pricingGridsCode.add(Integer.toString(mesConstants.APP_PG_MOTO));
      pricingGrids.add("Internet");
      pricingGridsCode.add(Integer.toString(mesConstants.APP_PG_INTERNET));
      pricingGrids.add("Supermarket");
      pricingGridsCode.add(Integer.toString(mesConstants.APP_PG_SUPERMARKET));
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "fillDropDowns()" + e.toString());
    }
    
    try
    {
      StringBuffer      qs      = new StringBuffer("");
      PreparedStatement ps      = null;
      ResultSet         rs      = null;

      qs.append("select a.pos_code, a.pos_param, b.pos_type ");
      qs.append("from merch_pos a, pos_category b ");
      qs.append("where a.app_seq_num = ? and a.pos_code = b.pos_code");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      if (rs.next())
      {
        if(rs.getInt("pos_code") == mesConstants.APP_MPOS_ACTIVE_C_STOREFRONT_BASIC || rs.getInt("pos_code") == mesConstants.APP_MPOS_ACTIVE_C_STOREFRONT_PLUS)
        {
          this.activec = true;
        }
        else if(rs.getInt("pos_code") == mesConstants.APP_MPOS_CYBERCASH)
        {
          this.cybercash = true;
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      addError("getMerchPosData: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(),
               "getMerchPosData: " + e.toString());
    }

    for(int i=0; i<this.NUM_CARDS; i++)
    {
      cardSelectedFlag[i] = "N";
      cardFee[i]          = "-1";
      switch(i)
      {
        case CARD_DINERS:
          cardDesc[i]  = "Diners Club/Carte Blanche";
          cardCodes[i] = mesConstants.APP_CT_DINERS_CLUB;
          break;
        case CARD_DISCOVER:
          cardDesc[i]  = "Discover/Novus";
          cardCodes[i] = mesConstants.APP_CT_DISCOVER;
          break;
        case CARD_JCB: 
          cardDesc[i]  = "JCB";
          cardCodes[i] = mesConstants.APP_CT_JCB;
          break;
        case CARD_AMEX:
          cardDesc[i]  = "American Express";
          cardCodes[i] = mesConstants.APP_CT_AMEX;
          break;
        case CARD_DEBIT:
          cardDesc[i]  = "Debit";
          cardCodes[i] = mesConstants.APP_CT_DEBIT;
          break;
        case CARD_CHECK:
          cardDesc[i]  = "Check Verification";
          cardCodes[i] = mesConstants.APP_CT_CHECK_AUTH;
          break;
        case CARD_INTERNET:
          cardDesc[i]  = "Internet Transaction";
          cardCodes[i] = mesConstants.APP_CT_INTERNET;
          break;
        case CARD_DIALPAY:
          cardDesc[i]  = "Dial Pay Transaction";
          cardCodes[i] = mesConstants.APP_CT_DIAL_PAY;
          break;
        case CARD_EBT:
          cardDesc[i]  = "EBT";
          cardCodes[i] = mesConstants.APP_CT_EBT;
          break;
        case CARD_TYME:
          cardDesc[i]  = "Tyme Network";
          cardCodes[i] = mesConstants.APP_CT_TYME_NETWORK;
          break;
        default:
          break;
      }
    }
    for(int i=0; i<this.NUM_QUESTIONS; i++)
    {
      questionFlag[i]     = "-1";
      switch(i)
      {
        case 0:
          questionDesc[i]     = "Did name posted at business match business name on application?";
          break;
        case 1:
          questionDesc[i]     = "Did location appear to have appropriate signage?";
          break;
        case 2: 
          questionDesc[i]     = "Were business hours posted?";
          break;
        case 3:
          questionDesc[i]     = "Was merchant's inventory viewed?";
          break;
        case 4:
          questionDesc[i]     = "Was inventory consistent with merchant's type of business?";
          break;
        case 5:
          //questionDesc[i]     = "Did inventory appear to be adequate to support the sales volume indicated on the application?";
          break;
        default:
          break;
      }
    }
  }
  
  public void getData(long appSeqNum)
  {
    getTranscomInfo(appSeqNum);
    getGrid(appSeqNum);
    getPaySolOption(appSeqNum);
    getCardInfo(appSeqNum);
    getPricingRate(appSeqNum);
    getPcInfo(appSeqNum);
    //getOwnedEquipmentInfo(appSeqNum);
    getMiscCharges(appSeqNum);
    getSiteInspectionInfo(appSeqNum);
    getAmexZero(appSeqNum);
    getEquipmentInfo(appSeqNum);
  }

  private void getTranscomInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select MISC_FEE1_BASIS,          ");
      qs.append("       MISC_FEE1_DESC,           ");
      qs.append("       MISC_FEE2_BASIS,          ");
      qs.append("       MISC_FEE2_DESC,           ");
      qs.append("       CHECK_AMOUNT_MISC,        ");
      qs.append("       CHECK_AMOUNT_EQUIP,       ");
      qs.append("       ACH_DEBIT_AMOUNT_EQUIP,   ");
      qs.append("       DATE_APP_RECEIVED,        ");
      qs.append("       DATE_APP_SUBMITTED,       ");
      qs.append("       REP_CODE,                 "); //not the same rep code that goes into tsys..tsys accepts 4 digit this is 6 digit
      qs.append("       NUM_REPROGRMMING,         ");
      qs.append("       BET_TABLE                 ");
      qs.append("from   transcom_merchant         ");
      qs.append("where  app_seq_num = ?           ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      rs = ps.executeQuery();

      if(rs.next())
      {
        miscellaneous1ChargeBasis   = isBlank(rs.getString("MISC_FEE1_BASIS"))  ? "" : rs.getString("MISC_FEE1_BASIS");
        miscellaneous1FeeDesc       = isBlank(rs.getString("MISC_FEE1_DESC"))   ? "" : rs.getString("MISC_FEE1_DESC");
        miscellaneous2ChargeBasis   = isBlank(rs.getString("MISC_FEE2_BASIS"))  ? "" : rs.getString("MISC_FEE2_BASIS");
        miscellaneous2FeeDesc       = isBlank(rs.getString("MISC_FEE2_DESC"))   ? "" : rs.getString("MISC_FEE2_DESC");

        checkReceivedChrgAmount     = isBlank(rs.getString("CHECK_AMOUNT_MISC"))   ? "" : rs.getString("CHECK_AMOUNT_MISC");
        if(!isBlank(checkReceivedChrgAmount))
        {
          checkReceivedChrgFlag     = "Y";
        }

        checkReceivedEquipAmount     = isBlank(rs.getString("CHECK_AMOUNT_EQUIP"))   ? "" : rs.getString("CHECK_AMOUNT_EQUIP");
        if(!isBlank(checkReceivedEquipAmount))
        {
          checkReceivedEquipFlag     = "Y";
        }

        achDebitEquipAmount     = isBlank(rs.getString("ACH_DEBIT_AMOUNT_EQUIP"))   ? "" : rs.getString("ACH_DEBIT_AMOUNT_EQUIP");
        if(!isBlank(achDebitEquipAmount))
        {
          achDebitEquipFlag     = "Y";
        }

        numTermsReprogramming     = isBlank(rs.getString("NUM_REPROGRMMING"))   ? "" : rs.getString("NUM_REPROGRMMING");
        if(!isBlank(numTermsReprogramming))
        {
          numTermsReprogrammingFlag     = "Y";
        }

        billingElementTable = isBlank(rs.getString("BET_TABLE"))   ? "" : rs.getString("BET_TABLE");

        repCode       = isBlank(rs.getString("REP_CODE"))   ? "803" : rs.getString("REP_CODE");
        dateReceived  = rs.getDate("DATE_APP_RECEIVED");
        dateSubmitted = rs.getDate("DATE_APP_SUBMITTED");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getTranscomInfo: " + e.toString());
      addError("getTranscomInfo: " + e.toString());
    }
  }


  private void getAmexZero(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select merchpo_split_dial, merchpo_pip ");
      qs.append("from   merchpayoption ");
      qs.append("where  app_seq_num = ? and ");
      qs.append("       cardtype_code = ?");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setInt(2, mesConstants.APP_CT_AMEX);
      rs = ps.executeQuery();
      if(rs.next())
      {
        if(!isBlank(rs.getString("merchpo_split_dial")) && (rs.getString("merchpo_split_dial")).equals("Y"))
        {
          amexZero  = true;          
          amexSplit = true;
        }
        else if(!isBlank(rs.getString("merchpo_pip")) && (rs.getString("merchpo_pip")).equals("Y"))
        {
          amexZero = true;
        }
        else
        {
          amexZero  = false;
          amexSplit = false;
        }
      }
      else
      {
        amexZero  = false;
        amexSplit = false;
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getSplitDial: " + e.toString());
      addError("getSplitDial: " + e.toString());
    }
  }
  
  private String getEbtFee(long appSeqNum)  
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    String            result  = "-1";

    try
    {
      qs.append("select tranchrg_per_tran from tranchrg where ");
      qs.append("app_seq_num = ? and cardtype_code = ? ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setInt(2, mesConstants.APP_CT_EBT);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        result = isBlank(rs.getString("tranchrg_per_tran")) ? "-1" : rs.getString("tranchrg_per_tran");
      }
    }
    catch(Exception e)
    {
      result = "-1";
    }

    return result;
  }
  
  /**
   *  This method takes the sequence no. as the argument and is used
   *  to get all the details of the card types selected by the merchant
   *  on the Payment Options page.
  */
  private void getCardInfo(long appSeqNum)  
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    PreparedStatement ps1     = null;
    ResultSet         rs      = null;
    ResultSet         rs1     = null;
    int               i       = 0;
    
    try 
    {
        
      qs.append("select cardtype_code, merchpo_card_merch_number from merchpayoption ");
      qs.append("where app_seq_num = ? and cardtype_code not in (1,4,19) ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();
      
      qs.setLength(0);
      qs.append("select tranchrg_per_tran from tranchrg where ");
      qs.append("app_seq_num = ? and cardtype_code = ? ");
      ps1 = getPreparedStatement(qs.toString());
    
      while(rs.next())
      {
        ps1.clearParameters();
        ps1.setLong(1, appSeqNum);
        ps1.setInt(2, rs.getInt("cardtype_code"));
      
        rs1 = ps1.executeQuery();
        //find which element to put info into
        i=0;
        while(i < this.NUM_CARDS && rs.getInt("cardtype_code") != this.cardCodes[i])
        {
          i+=1;
        }
        if(rs1.next() && i < this.NUM_CARDS)
        {
          this.cardFee[i] = rs1.getString("tranchrg_per_tran");
          this.cardSelectedFlag[i] = "Y";
        }
        else if(i < this.NUM_CARDS)
        {
          this.cardFee[i] = "-1";
          this.cardSelectedFlag[i] = "Y";
        }

        rs1.close();
      }
      
      if(this.paySolOption == 2) //Internet Transaction
      {
        this.cardSelectedFlag[CARD_INTERNET] = "Y";
        ps1.clearParameters();
        ps1.setLong(1, appSeqNum);
        ps1.setInt(2, mesConstants.APP_CT_INTERNET);
        rs1 = ps1.executeQuery();
        if(rs1.next())
        {
          this.cardFee[CARD_INTERNET] = rs1.getString("tranchrg_per_tran");
        }
      }
      else if(this.paySolOption == 4) //DialPay Transaction
      {
        this.cardSelectedFlag[CARD_DIALPAY] = "Y";
        ps1.clearParameters();
        ps1.setLong(1, appSeqNum);
        ps1.setInt(2, mesConstants.APP_CT_DIAL_PAY);
        rs1 = ps1.executeQuery();
        if(rs1.next())
        {
          this.cardFee[CARD_DIALPAY] = rs1.getString("tranchrg_per_tran");
        }
      }
      ps.close();
      rs.close();
      ps1.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCardInfo: " + e.toString());
      addError("getCardInfo: " + e.toString());
    }
  }

  /**
   *  This method takes the sequence no. as the argument and is used
   *  to get all the details of the equipment which he is wants to
   *  purchase or rent or buy.
   *  It also get's quantity entered by the user and the default
   *  price for each equipment.
   */
  private void getEquipmentInfo(long appSeqNum) 
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
        
    try 
    {
      qs.append("select equip_model, equiplendtype_code, merchequip_amount ");
      qs.append("from merchequipment ");
      qs.append("where app_seq_num = ? and equiplendtype_code in (1,2,5) ");
      qs.append("order by equiplendtype_code ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
    
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        if(rs.getString("equip_model").equals("800") || rs.getString("equip_model").equals("801"))
        {
          wirelessEquip = true;
        }

        switch(rs.getInt("equiplendtype_code"))
        {
          case mesConstants.APP_EQUIP_PURCHASE:
            this.sales = true;
          break;
          case mesConstants.APP_EQUIP_RENT:
            this.rental = true;
          break;
          case mesConstants.APP_EQUIP_LEASE:
            this.lease = true;
          break;
        }
      }
      rs.close();
      ps.close();

      if(this.sales)
      {
        this.salesPrice   = getLendAmount(appSeqNum,mesConstants.APP_EQUIP_PURCHASE);
      }
      if(this.rental)
      {
        this.rentalPrice  = getLendAmount(appSeqNum,mesConstants.APP_EQUIP_RENT);
      }
      if(this.lease)
      {
        this.leasePrice   = getLendAmount(appSeqNum,mesConstants.APP_EQUIP_LEASE);
      }
    
    }
    catch(Exception e)
    {
      this.equipmentInfo = null;
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getEquipmentInfo: " + e.toString());
      addError("getEquipmentInfo: " + e.toString());
    }
  }
  
  public void setVmcAuthFee(String fee)
  {
    try
    {
      vmcAuthFee = Double.parseDouble(fee);
    }
    catch(Exception e)
    {
    }
  }
  
  public String getVmcAuthFee()
  {
    return Double.toString(vmcAuthFee);
  }

  private void getPcInfo(long appSeqNum)
  {
    try
    {
      StringBuffer      qs      = new StringBuffer("");
      PreparedStatement ps      = null;
      ResultSet         rs      = null;
          
      qs.append("select merchequip_equip_quantity,merchequip_amount from merchequipment ");
      qs.append("where app_seq_num = ? and equip_model = ? ");
                    
      ps = getPreparedStatement(qs.toString());
         
      ps.setLong(1, appSeqNum);
      ps.setString(2, "PCPS");
      rs = ps.executeQuery();
      if(rs.next())
      {
        this.pcFee      = rs.getString("merchequip_amount");
        if(isBlank(this.pcFee))
          this.pcFee = "-1";

        this.pcQuantity = rs.getString("merchequip_equip_quantity");
      }
      else
      {
        pcQuantity = "-1";
        pcFee      = "-1";
      }
      rs.close();
      ps.close();
      
      qs.setLength(0);
      qs.append("select pos_desc from pos_category a, merch_pos b ");
      qs.append("where b.app_seq_num = ? and a.pos_code = b.pos_code and a.pos_type = 3 ");
      
      ps = getPreparedStatement(qs.toString());
         
      ps.setLong(1, appSeqNum);
      rs = ps.executeQuery();
      if(rs.next())
      {
        this.pcModel = rs.getString("pos_desc");
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPcInfo: " + e.toString());
      addError("getPcInfo: " + e.toString());
    }
  }

  /**
   *  This method takes the sequence no. as the argument and is used
   *  to get all the details of the equipment which he is wants to
   *  purchase or rent.
   *  It also get's quantity entered by the user and the default
   *  price for each equipment.
   */
  private void getMiscCharges(long appSeqNum) 
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    try 
    {
      qs.append("select * from miscchrg ");
      qs.append("where app_seq_num = ?  ");
     
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      while(rs.next())
      {
        switch(rs.getInt("misc_code"))
        {
          case FEE_CHARGEBACK:
            this.chargebackFee                = rs.getString("misc_chrg_amount");
            this.chargebackFeeFlag            = "Y";
            break;

          case FEE_RETRIEVAL:
            this.retrievalRequestFee          = rs.getString("misc_chrg_amount");
            this.retrievalRequestFeeFlag      = "Y";
            break;

          case FEE_STATEMENT:
            this.statementFee                 = rs.getString("misc_chrg_amount");
            this.statementFeeFlag             = "Y";
            break;

          case FEE_INTERNET_REPORTING:
            this.internetReportFee            = rs.getString("misc_chrg_amount");
            this.internetReportFeeFlag        = "Y";
            break;

          case FEE_APPLICATION:
            this.applicationFee               = rs.getString("misc_chrg_amount");
            this.applicationFeeFlag           = "Y";
            break;

          case FEE_TERMINAL_REPROGRAM:
            this.terminalReprogrammingFee     = rs.getString("misc_chrg_amount");
            this.terminalReprogrammingFeeFlag = "Y";
            break;

          case FEE_PINPAD_SWAP:
            this.pinpadSwapFee                = rs.getString("misc_chrg_amount");
            this.pinpadSwapFeeFlag            = "Y";
            break;

          case FEE_ACH_DEPOSIT:
            this.achDepositFee                = rs.getString("misc_chrg_amount");
            this.achDepositFeeFlag            = "Y";
            break;

          case FEE_DEBIT_ACCESS:
            this.debitAccessFee               = rs.getString("misc_chrg_amount");
            this.debitAccessFeeFlag           = "Y";
            break;

          case FEE_INTERNET_STARTUP:
            this.internetStartupFee           = rs.getString("misc_chrg_amount");
            this.internetStartupFeeFlag       = "Y";
            break;

          case FEE_GATEWAY_MONTHLY:
            this.gatewayFee                   = rs.getString("misc_chrg_amount");
            this.gatewayFeeFlag               = "Y";
            break;

          case FEE_ANNUAL:
            this.annualFee                    = rs.getString("misc_chrg_amount");
            this.annualFeeFlag                = "Y";
            break;
          
          case FEE_SUPPLY:
            this.supplyFee                    = rs.getString("misc_chrg_amount");
            this.supplyConfig                 = "CLUB";
            break;
          
          case FEE_VT_SETUP:
            vtSetupFee                        = rs.getString("misc_chrg_amount");
            vtSetupFeeFlag                    = "Y";
            break;
            
          case FEE_VT_MONTHLY:
            vtMonthlyFee                      = rs.getString("misc_chrg_amount");
            vtMonthlyFeeFlag                  = "Y";
            break;

          case FEE_TRAINING:
            this.trainingFee                  = rs.getString("misc_chrg_amount");
            this.trainingFeeFlag              = "Y";
            break;

          case FEE_MISCELLANEOUS1:
            this.miscellaneous1Fee            = rs.getString("misc_chrg_amount");
            this.miscellaneous1FeeFlag        = "Y";
            //get this info from transcom merchant
            //this.miscellaneous1ChargeBasis    = isBlank(rs.getString("MISC_CHRGBASIS_CODE"))  ? "" : rs.getString("MISC_CHRGBASIS_CODE");
            //this.miscellaneous1FeeDesc        = isBlank(rs.getString("MISC_CHRGBASIS_DESCR")) ? "" : rs.getString("MISC_CHRGBASIS_DESCR");
            break;

          case FEE_MISCELLANEOUS2:
            this.miscellaneous2Fee            = rs.getString("misc_chrg_amount");
            this.miscellaneous2FeeFlag        = "Y";
            //get this info from transcom_merchant
            //this.miscellaneous2ChargeBasis    = isBlank(rs.getString("MISC_CHRGBASIS_CODE"))  ? "" : rs.getString("MISC_CHRGBASIS_CODE");
            //this.miscellaneous2FeeDesc        = isBlank(rs.getString("MISC_CHRGBASIS_DESCR")) ? "" : rs.getString("MISC_CHRGBASIS_DESCR");
            break;

          case FEE_VOICE_AUTH:
            this.voiceAuthFee                 = rs.getString("misc_chrg_amount");
            break;

          case FEE_POS_PARTNER_MONTHLY:
            this.posPartnerMonthlyFee         = rs.getString("misc_chrg_amount");
            break;

          case FEE_WIRELESS_SETUP:
            this.wirelessSetupFee             = rs.getString("misc_chrg_amount");
            this.wirelessSetupFeeFlag         = "Y";
            break;

          case FEE_WIRELESS_MONTHLY:
            this.wirelessMonthlyFee           = rs.getString("misc_chrg_amount");
            this.wirelessMonthlyFeeFlag       = "Y";
            break;

          case FEE_WIRELESS_TRANSACTION:
            this.wirelessTransactionFee       = rs.getString("misc_chrg_amount");
            this.wirelessTransactionFeeFlag   = "Y";
            break;

        }
      }
      rs.close();
      ps.close();
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMiscCharges: " + e.toString());
      addError("getMiscCharges: " + e.toString());
    }
  }


  /**
   *  This method takes the sequence no. as the argument and is used
   *  to get the discount rate and the interchange passthru by using
   *  the matrix provided by MES.
   */

  private void getPricingRate(long appSeqNum) 
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try 
    {
      qs.setLength(0);
      qs.append("select tranchrg_disc_rate,tranchrg_mmin_chrg,tranchrg_per_tran,tranchrg_pass_thru,tranchrg_discrate_type,nvl(tranchrg_per_auth,0) tranchrg_per_auth,tranchrg_per_item,tranchrg_per_capture,non_qualification_downgrade,tranchrg_interchangefee_type ");
      qs.append("from tranchrg where app_seq_num = ? and cardtype_code = ? ");
      
      ps = getPreparedStatement(qs.toString());
      ps.clearParameters();
      
      ps.setLong(1, appSeqNum);
      ps.setInt(2, mesConstants.APP_CT_VISA);
      
      rs = ps.executeQuery();
      
      if (rs.next()) 
      {
        this.pricingScenario      = rs.getInt("tranchrg_discrate_type");
        this.minMonthDiscount     = isBlank(rs.getString("tranchrg_mmin_chrg")) ? "" : rs.getString("tranchrg_mmin_chrg");
        this.minMonthDiscountFlag = isBlank(this.minMonthDiscount) ? "N" : "Y";

        switch(this.pricingScenario)
        {
          case mesConstants.TRANS_APP_BUCKET_ONLY_PLAN:
            if(rs.getString("tranchrg_disc_rate") != null)
            {
              this.transDiscountRate9 = rs.getDouble("tranchrg_disc_rate");
            }
            if(rs.getString("tranchrg_per_item") != null)
            {
              this.transSurcharge9 = rs.getString("tranchrg_per_item");
            }
            break;
          case mesConstants.TRANS_APP_BUCKET_WITH_PERITEM_PLAN:
            if(rs.getString("tranchrg_per_tran") != null)
            {
              this.transPerItem10 = rs.getDouble("tranchrg_per_tran");
            }
            if(rs.getString("tranchrg_disc_rate") != null)
            {
              this.transDiscountRate10  = rs.getDouble("tranchrg_disc_rate");
            }
            if(rs.getString("tranchrg_per_item") != null)
            {
              this.transSurcharge10 = rs.getString("tranchrg_per_item");
            }
            break;
          case mesConstants.TRANS_APP_DETAILED_STATEMENT_PLAN:
            if(rs.getString("tranchrg_per_tran") != null)
            {
              this.transPerItem11 = rs.getDouble("tranchrg_per_tran");
            }
            if(rs.getString("tranchrg_disc_rate") != null)
            {
              this.transDiscountRate11  = rs.getDouble("tranchrg_disc_rate");
            }
            break;
          case mesConstants.TRANS_APP_INTER_PASSTHRU_PLAN:
            if(rs.getString("tranchrg_per_tran") != null)
            {
              this.transPerItem12 = rs.getDouble("tranchrg_per_tran");
            }
            if(rs.getString("tranchrg_disc_rate") != null)
            {
              this.transDiscountRate12  = rs.getDouble("tranchrg_disc_rate");
            }
            break;
          default:
            break;
        }
        
        vmcAuthFee = rs.getDouble("tranchrg_per_auth");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPricingRate: " + e.toString());
      addError("getPricingRate: " + e.toString());
    }
  }


  /**
   *  This method takes the sequence no. as the argument and is used
   *  to get all the details of the equipment which he already owns.
   *  It also get's quantity entered by the user and the default
   *  price for each equipment.
   */
  private void getOwnedEquipmentInfo(long appSeqNum) 
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try 
    {

      qs.append("select sum(merchequip_equip_quantity) from merchequipment ");
      qs.append("where app_seq_num = ? and equiplendtype_code = 3 and equiptype_code != 4 ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
    
      rs = ps.executeQuery();
     
      if (rs.next() && !isBlank(rs.getString(1))) 
      {
        this.ownedEquipQty  = rs.getString(1);

        if(this.paySolOption == mesConstants.APP_PAYSOL_DIAL_TERMINAL || this.paySolOption == mesConstants.APP_PAYSOL_DIAL_PAY)
          this.ownedEquipFlag = "Y";
        else
          this.ownedEquipFlag = "N";
        
        rs.close();
        ps.close();

        qs.setLength(0);
        qs.append("select merchequip_amount from merchequipment ");
        qs.append("where app_seq_num = ? and equiplendtype_code = 3 ");
      
        ps = getPreparedStatement(qs.toString());
        ps.setLong(1, appSeqNum);
    
        rs = ps.executeQuery();
     
        if (rs.next()) 
        {  
          this.ownedEquipAmt  = rs.getString("merchequip_amount");
        }
        else
        {
          this.ownedEquipAmt  = "-1";
        }
        rs.close();
        ps.close();
      }
      else
      {
        this.ownedEquipFlag = "N";
      }
      
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getOwnedEquipmentInfo: " + e.toString());
      addError("getOwnedEquipmentInfo: " + e.toString());
    }
  }// End of method.
  
  public void getGrid(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select pricing_grid ");
      qs.append("from merchant ");
      qs.append("where app_seq_num = ?");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.pricingGrid = isBlank(rs.getString("pricing_grid")) ? "" : rs.getString("pricing_grid");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getGrid: " + e.toString());
      addError("getGrid: " + e.toString());
    }
  }
  
  public void getApplicationType(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select app_type ");
      qs.append("from application ");
      qs.append("where app_seq_num = ?");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.appType = rs.getInt("app_type");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAppType: " + e.toString());
      addError("getAppType: " + e.toString());
    }
  }
 
  private void getPaySolOption(long appSeqNum) 
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select pos_type ");
      qs.append("from merch_pos a, pos_category b ");
      qs.append("where a.app_seq_num = ? and a.pos_code = b.pos_code");

      ps = getPreparedStatement(qs.toString());

      ps.clearParameters();
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      while(rs.next())
      {
        this.paySolOption = rs.getInt("pos_type");
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPaySolOption: " + e.toString());
      addError("getPaySolOption: " + e.toString());
    }
  }
  public String getPiAmount(long appSeqNum, String equipModel, int equipLendType)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    String            fee     = "-1";
    try 
    {
      qs.append("select merchequip_amount from merchequipment where ");
      qs.append("app_seq_num = ? and equip_model = ? and equiplendtype_code = ? ");
       
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setString(2, equipModel);
      ps.setInt(3, equipLendType);
    
      rs = ps.executeQuery();
      if(rs.next())
        fee = formatCurrency(rs.getString("merchequip_amount"));
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPiAmount: " + e.toString());
      addError("getPiAmount: " + e.toString());
    }
    return fee;
  }

  public String getLendAmount(long appSeqNum, int equipLendType)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    String            fee     = "";

    try 
    {
      qs.append("select merchequip_amount from merchequipment where ");
      qs.append("app_seq_num = ? and equiplendtype_code = ? ");
       
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setInt(2,  equipLendType);
    
      rs = ps.executeQuery();
      if(rs.next())
      {
        fee = formatCurrency(rs.getString("merchequip_amount"));
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getLendAmount: " + e.toString());
      addError("getLendAmount: " + e.toString());
    }
    return fee;
  }


  public void getSiteInspectionInfo(long appSeqNum)
  {
    StringBuffer      qs    = new StringBuffer("");
    PreparedStatement ps    = null;
    ResultSet         rs    = null;
    
    try
    {
      
      qs.append("select siteinsp_comment,siteinsp_name_flag,siteinsp_bus_hours_flag,siteinsp_no_of_emp,siteinsp_inv_street,siteinsp_inv_city, ");
      qs.append("siteinsp_inv_state,siteinsp_inv_zip,siteinsp_vol_flag,siteinsp_bus_loc,siteinsp_bus_loc_comment,siteinsp_bus_address, ");
      qs.append("siteinsp_bus_street,siteinsp_bus_city,siteinsp_bus_state,siteinsp_bus_zip,siteinsp_inv_sign_flag,siteinsp_inv_viewed_flag, ");
      qs.append("siteinsp_inv_consistant_flag,siteinsp_inv_value,siteinsp_full_flag,siteinsp_full_name,siteinsp_full_street,siteinsp_full_city, ");
      qs.append("siteinsp_full_state,siteinsp_full_zip,siteinsp_soft_flag,siteinsp_soft_name,siteinsp_supply_billing ");
      qs.append("from siteinspection ");
      qs.append("where app_seq_num = ?");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.questionFlag[0]          = isBlank(rs.getString("siteinsp_name_flag")) ? "-1" : rs.getString("siteinsp_name_flag");
        this.questionFlag[1]          = isBlank(rs.getString("siteinsp_inv_sign_flag")) ? "-1" : rs.getString("siteinsp_inv_sign_flag");
        this.questionFlag[2]          = isBlank(rs.getString("siteinsp_bus_hours_flag")) ? "-1" : rs.getString("siteinsp_bus_hours_flag");
        this.questionFlag[3]          = isBlank(rs.getString("siteinsp_inv_viewed_flag")) ? "-1" : rs.getString("siteinsp_inv_viewed_flag");
        this.questionFlag[4]          = isBlank(rs.getString("siteinsp_inv_consistant_flag")) ? "-1" : rs.getString("siteinsp_inv_consistant_flag");
        //this.questionFlag[5]          = isBlank(rs.getString("siteinsp_vol_flag")) ? "-1" : rs.getString("siteinsp_vol_flag");
        this.houseFlag                = rs.getString("siteinsp_full_flag");
        this.softwareFlag             = rs.getString("siteinsp_soft_flag");
        
        this.comment                  = rs.getString("siteinsp_comment");
        this.inventoryAddressStreet   = rs.getString("siteinsp_inv_street");
        this.inventoryAddressCity     = rs.getString("siteinsp_inv_city");
        this.inventoryAddressState    = rs.getString("siteinsp_inv_state");
        this.inventoryAddressZip      = rs.getString("siteinsp_inv_zip");
                                        
        this.houseStreet              = rs.getString("siteinsp_full_street");
        this.houseCity                = rs.getString("siteinsp_full_city");
        this.houseState               = rs.getString("siteinsp_full_state");
        this.houseZip                 = rs.getString("siteinsp_full_zip");
                                        
        this.businessAddressStreet    = rs.getString("siteinsp_bus_street");
        this.businessAddressCity      = rs.getString("siteinsp_bus_city");
        this.businessAddressState     = rs.getString("siteinsp_bus_state");
        this.businessAddressZip       = rs.getString("siteinsp_bus_zip");
                                        
        this.inventoryValue           = rs.getString("siteinsp_inv_value");
        this.houseName                = rs.getString("siteinsp_full_name");
        this.numEmployees             = rs.getString("siteinsp_no_of_emp");
                                        
        this.businessLocation         = rs.getString("siteinsp_bus_loc");
        this.businessLocationComment  = rs.getString("siteinsp_bus_loc_comment");
        this.businessAddress          = rs.getString("siteinsp_bus_address");
        this.softwareComment          = rs.getString("siteinsp_soft_name");
        this.supplyConfig             = rs.getString("siteinsp_supply_billing");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getSiteInspectionInfo: " + e.toString());
      addError("getSiteInspectionInfo: " + e.toString());
    }
  }



  public void updateData(HttpServletRequest aReq)
  {
    double temp;
    for(int i=0; i<this.NUM_CARDS; i++)
    {
        if(!isBlank(aReq.getParameter("cardSelectedFlag" + i)) && aReq.getParameter("cardSelectedFlag" + i).equals("Y"))
        {
          this.cardSelectedFlag[i] = "Y";
          this.cardFee[i] = aReq.getParameter("cardFee" + i);
        }
        else
          this.cardSelectedFlag[i] = "N"; 
    }
    
    for(int i=0; i<this.NUM_QUESTIONS; i++)
    {
        if(!isBlank(aReq.getParameter("questionFlag" + i)) && aReq.getParameter("questionFlag" + i).equals("Y"))
        {
          this.questionFlag[i] = "Y";
        }
        else if(!isBlank(aReq.getParameter("questionFlag" + i)) && aReq.getParameter("questionFlag" + i).equals("N"))
        {
          this.questionFlag[i] = "N";
        }
        else
        {
          this.questionFlag[i] = "-1"; 
        }
    }
  }
 
  public boolean validate(HttpServletRequest aReq)
  {
    if( this.pricingScenario == mesConstants.APP_PS_INVALID )
    {
      addError("Please select a Pricing Scenario");
    }
    
    if(this.transDiscountRate9 == -1 && this.pricingScenario == mesConstants.TRANS_APP_BUCKET_ONLY_PLAN)
    {
      addError("Please provide a valid Discount Rate for Bucket Only pricing scenario");
    }
    if((this.transPerItem10 == -1 || this.transDiscountRate10 == -1) &&  this.pricingScenario == mesConstants.TRANS_APP_BUCKET_WITH_PERITEM_PLAN)
    {
      addError("Please provide a valid Discount Rate AND Per Item Fee for Bucket with Per Item Fee pricing scenario");
    }
    if((this.transPerItem11 == -1 || this.transDiscountRate11 == -1) && this.pricingScenario == mesConstants.TRANS_APP_DETAILED_STATEMENT_PLAN)
    {
      addError("Please provide a valid Discount Rate AND Per Item Fee for Detailed Statement pricing scenario");
    }
    if((this.transPerItem12 == -1 || this.transDiscountRate12 == -1) && this.pricingScenario == mesConstants.TRANS_APP_INTER_PASSTHRU_PLAN)
    {
      addError("Please provide a valid Discount Rate AND Per Item Fee for Interchange Pass Through pricing scenario");
    }


    if(isBlank(this.pricingGrid))
    {
      addError("Please select a Type of Merchant");
    }

    if(isBlank(this.billingElementTable))
    {
      addError("Please select a Billing Element Table");
    }

    for(int i=0; i<this.NUM_CARDS; i++)
    {
      if(this.cardSelectedFlag[i].equals("Y") && this.cardFee[i].equals("-1"))
      {
        addError("Please select a fee for " + this.cardDesc[i]);
      }
    }

    //CHARGES FOR TRANSCOM
    if(!isBlank(this.voiceAuthFee))
    {
      try
      {
        double temp = Double.parseDouble(this.voiceAuthFee);
      }
      catch(Exception e)
      {
        addError("Please provide a valid Voice Authorization Fee");
      }
    }
                                                                  
    if(!isBlank(this.chargebackFee))
    {
      try
      {
        double temp = Double.parseDouble(this.chargebackFee);
        if(this.chargebackFeeFlag.equals("N"))
        {
          addError("Please check the Chargeback checkbox");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Chargeback Fee");
      }
    }
    else if(this.chargebackFeeFlag.equals("Y"))
    {
        addError("Please provide a valid Chargeback Fee or uncheck the Chargeback Fee checkbox");
    }

    if(!isBlank(this.achDepositFee))
    {
      try
      {
        double temp = Double.parseDouble(this.achDepositFee);
        if(this.achDepositFeeFlag.equals("N"))
        {
          addError("Please check the ACH Deposit checkbox");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid ACH Deposit Fee");
      }
    }
    else if(this.achDepositFeeFlag.equals("Y"))
    {
        addError("Please provide a valid ACH Deposit Fee or uncheck the ACH Deposit Fee checkbox");
    }

    if(!isBlank(this.retrievalRequestFee))
    {
      try
      {
        double temp = Double.parseDouble(this.retrievalRequestFee);
        if(this.retrievalRequestFeeFlag.equals("N"))
        {
          addError("Please check the Retrieval Request checkbox or delete the Retrieval Request Fee");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Retrieval Request Fee");
      }
    }
    else if(this.retrievalRequestFeeFlag.equals("Y"))
    {
        addError("Please provide a valid Retrieval Request Fee or uncheck the Retrieval Request Fee checkbox");
    }

    if(!isBlank(this.statementFee))
    {
      try
      {
        double temp = Double.parseDouble(this.statementFee);
        if(this.statementFeeFlag.equals("N"))
        {
          addError("Please check the Merchant Statement checkbox or delete the Merchant Statement Fee");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Merchant Statement Fee");
      }
    }
    else if(this.statementFeeFlag.equals("Y"))
    {
        addError("Please provide a valid Merchant Statement Fee or uncheck the Merchant Statement Fee checkbox");
    }

    if(!isBlank(this.internetReportFee))
    {
      try
      {
        double temp = Double.parseDouble(this.internetReportFee);
        if(this.internetReportFeeFlag.equals("N"))
        {
          addError("Please check the Internet Report checkbox or delete the Internet Report Fee");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Internet Report Fee");
      }
    }
    else if(this.internetReportFeeFlag.equals("Y"))
    {
        addError("Please provide a valid Internet Report Fee or uncheck the Internet Report Fee checkbox");
    }

    if(!isBlank(this.applicationFee))
    {
      try
      {
        double temp = Double.parseDouble(this.applicationFee);
        if(this.applicationFeeFlag.equals("N"))
        {
          addError("Please check the Application Fee checkbox or delete the Application Fee");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Application Fee");
      }
    }
    else if(this.applicationFeeFlag.equals("Y"))
    {
        addError("Please provide a valid Application Fee or uncheck the Application Fee checkbox");
    }

  
    if(this.terminalReprogrammingFeeFlag.equals(CHECK_RECEIVED_NO) && !isBlank(this.terminalReprogrammingFee))
    {
      addError("Please check the terminal reprogramming fee checkbox if a fee is provided");
    }
    else if(this.terminalReprogrammingFeeFlag.equals(CHECK_RECEIVED_YES) && isBlank(this.terminalReprogrammingFee))
    {
      addError("Please provide terminal reprogramming fee if terminal reprogramming fee checkbox is checked");
    }
    else if(!isBlank(this.terminalReprogrammingFee))
    {
      try
      {
        double temp = Double.parseDouble(this.terminalReprogrammingFee);
        if(!isBlank(this.numTermsReprogramming))
        {
          try
          {
            int tempInt = Integer.parseInt(this.numTermsReprogramming);
            if(this.numTermsReprogrammingFlag.equals("N"))
            {
              addError("Please check the Number of Terminals for Reprogramming checkbox");
            }
          }
          catch(Exception e)
          {
            addError("Please provide a valid number of terminals to be reprogrammed");
          }
        }
        else if(this.numTermsReprogrammingFlag.equals("Y"))
        {
          addError("If specifying a terminal reprogramming fee, please specify number of terminals to reprogram");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Terminal Reprogramming Fee");
      }
    }
    else if(!isBlank(this.numTermsReprogramming))
    {
      addError("Please specify a terminal reprogramming fee if specifying number of terminals to reprogram");
    }



    if(!isBlank(this.pinpadSwapFee))
    {
      try
      {
        double temp = Double.parseDouble(this.pinpadSwapFee);
        if(this.pinpadSwapFeeFlag.equals("N"))
        {
          addError("Please check the Pin Pad Swap checkbox or delete the Pin Pad Swap Fee");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Pin Pad Swap Fee");
      }
    }
    else if(this.pinpadSwapFeeFlag.equals("Y"))
    {
        addError("Please provide a valid Pin Pad Swap Fee or uncheck the Pin Pad Swap Fee checkbox");
    }

    if(!isBlank(this.minMonthDiscount))
    {
      try
      {
        double temp = Double.parseDouble(this.minMonthDiscount);
        if(this.minMonthDiscountFlag.equals("N"))
        {
          addError("Please check the Minimum Monthly Discount Charge checkbox or delete the Minimum Monthly Discount Charge");
        }
      }
      catch(Exception e)
      {
        addError("Please Provide a valid Minimum Monthly Discount Charge");
      }
    }
    else if(this.minMonthDiscountFlag.equals("Y"))
    {
        addError("Please provide a valid Minimum Monthly Discount Charge or uncheck the Minimum Monthly Discount Charge checkbox");
    }

    if(!isBlank(this.debitAccessFee))
    {
      try
      {
        double temp = Double.parseDouble(this.debitAccessFee);
        if(this.debitAccessFeeFlag.equals("N"))
        {
          addError("Please check the Debit Access checkbox or delete the Debit Access Fee");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Debit Access Fee");
      }
    }
    else if(this.debitAccessFeeFlag.equals("Y"))
    {
        addError("Please provide a valid Debit Access Fee or uncheck the Debit Access Fee checkbox");
    }

    if(!isBlank(this.internetStartupFee))
    {
      try
      {
        double temp = Double.parseDouble(this.internetStartupFee);
        if(this.internetStartupFeeFlag.equals("N"))
        {
          addError("Please check the Internet Startup Fee checkbox or delete the Internet Startup Fee");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Internet Startup Fee");
      }
    }
    else if(this.internetStartupFeeFlag.equals("Y"))
    {
        addError("Please provide a valid Internet Startup Fee or uncheck the Internet Startup Fee checkbox");
    }

    if(!isBlank(this.gatewayFee))
    {
      try
      {
        double temp = Double.parseDouble(this.gatewayFee);
        if(this.gatewayFeeFlag.equals("N"))
        {
          addError("Please check the Internet Gateway Fee checkbox or delete the Internet Gateway Fee");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Internet Gateway Fee");
      }
    }
    else if(this.gatewayFeeFlag.equals("Y"))
    {
        addError("Please provide a valid Internet Gateway Fee or uncheck the Internet Gateway Fee checkbox");
    }

    if(!isBlank(this.annualFee))
    {
      try
      {
        double temp = Double.parseDouble(this.annualFee);
        if(this.annualFeeFlag.equals("N"))
        {
          addError("Please check the Annual Fee checkbox or delete the Annual Fee");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Annual Fee");
      }
    }
    else if(this.annualFeeFlag.equals("Y"))
    {
        addError("Please provide a valid Annual Fee or uncheck the Annual Fee checkbox");
    }
  
    if(!isBlank(vtSetupFee))
    {
      try
      {
        double temp = Double.parseDouble(vtSetupFee);
        if(vtSetupFeeFlag.equals("N"))
        {
          addError("Please check the Virtual Terminal Setup Fee checkbox or delete the Virtual Terminal Setup Fee");
        }
      }
      catch(Exception e)
      {
        addError("Please privide a valid Virtual Terminal Setup Fee");
      }
    }
    else if(vtSetupFeeFlag.equals("Y"))
    {
      addError("Please provide a valid Virtual Terminal Setup Fee or uncheck the Virtual Terminal Setup Fee checkbox");
    }
    if(!isBlank(vtMonthlyFee))
    {
      try
      {
        double temp = Double.parseDouble(vtMonthlyFee);
        if(vtMonthlyFeeFlag.equals("N"))
        {
          addError("Please check the Virtual Terminal Monthly Fee checkbox or delete the Virtual Terminal Monthly Fee");
        }
      }
      catch(Exception e)
      {
        addError("Please privide a valid Virtual Terminal Monthly Fee");
      }
    }
    else if(vtMonthlyFeeFlag.equals("Y"))
    {
      addError("Please provide a valid Virtual Terminal Monthly Fee or uncheck the Virtual Terminal Monthly Fee checkbox");
    }

    if(!isBlank(this.trainingFee))
    {
      try
      {
        double temp = Double.parseDouble(this.trainingFee);
        if(this.trainingFeeFlag.equals("N"))
        {
          addError("Please check the Training Fee checkbox or delete the Training Fee");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a Training Fee");
      }
    }
    else if(this.trainingFeeFlag.equals("Y"))
    {
        addError("Please provide a valid Training Fee or uncheck the Training Fee checkbox");
    }

    if(this.miscellaneous1FeeFlag.equals(CHECK_RECEIVED_NO) && !isBlank(this.miscellaneous1Fee))
    {
      addError("Please check the Miscellaneous Fee 1 checkbox if a fee is provided");
    }
    else if(this.miscellaneous1FeeFlag.equals(CHECK_RECEIVED_YES) && isBlank(this.miscellaneous1Fee))
    {
      addError("Please provide a fee for Miscellaneous Fee 1 if it's checkbox is checked");
    }
    else if(!isBlank(this.miscellaneous1Fee) || !isBlank(this.miscellaneous1FeeDesc))
    {
      try
      {
        double temp = Double.parseDouble(this.miscellaneous1Fee);
        if(isBlank(this.miscellaneous1ChargeBasis))
        {
            addError("Please select Monthly or One time for Miscellaneous Fee 1");
        }
        if(isBlank(this.miscellaneous1FeeDesc))
        {
          addError("Please provide a Miscellaneous Fee 1 Description");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Miscellaneous Fee 1 ");
      }
    }
    else 
    {
      this.miscellaneous1ChargeBasis = "";
    }

    if(this.miscellaneous2FeeFlag.equals(CHECK_RECEIVED_NO) && !isBlank(this.miscellaneous2Fee))
    {
      addError("Please check the Miscellaneous Fee 2 checkbox if a fee is provided");
    }
    else if(this.miscellaneous2FeeFlag.equals(CHECK_RECEIVED_YES) && isBlank(this.miscellaneous2Fee))
    {
      addError("Please provide a fee for Miscellaneous Fee 2 if it's checkbox is checked");
    }
    else if(!isBlank(this.miscellaneous2Fee) || !isBlank(this.miscellaneous2FeeDesc))
    {
      try
      {
        double temp = Double.parseDouble(this.miscellaneous2Fee);
        if(isBlank(this.miscellaneous2ChargeBasis))
        {
            addError("Please select Monthly or One time for Miscellaneous Fee 2");
        }
        if(isBlank(this.miscellaneous2FeeDesc))
        {
          addError("Please provide a Miscellaneous Fee 2 Description");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Miscellaneous Fee 2");
      }
    }
    else 
    {
      this.miscellaneous2ChargeBasis = "";
    }

    if(this.checkReceivedChrgFlag.equals(CHECK_RECEIVED_NO) && !isBlank(this.checkReceivedChrgAmount))
    {
      addError("Please check the 'Check Received' checkbox if Amount of Check is provided in the Miscellaneous Fees section");
    }
    else if(this.checkReceivedChrgFlag.equals(CHECK_RECEIVED_YES) && isBlank(this.checkReceivedChrgAmount))
    {
      addError("Please provide Amount of Check if 'Checked Received' checkbox is checked in the Miscellaneous Fees section");
    }
    else if(this.checkReceivedChrgFlag.equals(CHECK_RECEIVED_YES) && !isBlank(this.checkReceivedChrgAmount))
    {
      try
      {
        double temp = Double.parseDouble(this.checkReceivedChrgAmount);
      }
      catch(Exception e)
      {
        addError("Please provide a valid Amount of Check in the Miscellaneous Fees section");
      }
    }

    if(!isBlank(this.wirelessSetupFee))
    {
      try
      {
        double temp = Double.parseDouble(this.wirelessSetupFee);
        if(this.wirelessSetupFeeFlag.equals("N"))
        {
          addError("Please check the Wireless Setup Fee checkbox");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Wireless Setup Fee");
      }
    }
    else if(this.wirelessSetupFeeFlag.equals("Y"))
    {
      addError("Please provide a valid Wireless Setup Fee or uncheck the Wireless Setup Fee checkbox");
    }
    else if(paySolOption == mesConstants.POS_WIRELESS_TERMINAL || isWirelessEquip())
    {
      addError("A valid Wireless Setup Fee is required");    
    }


    if(!isBlank(this.wirelessMonthlyFee))
    {
      try
      {
        double temp = Double.parseDouble(this.wirelessMonthlyFee);
        if(this.wirelessMonthlyFeeFlag.equals("N"))
        {
          addError("Please check the Wireless Monthly Fee checkbox");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Wireless Monthly Fee");
      }
    }
    else if(this.wirelessMonthlyFeeFlag.equals("Y"))
    {
        addError("Please provide a valid Wireless Monthly Fee or uncheck the Wireless Monthly Fee checkbox");
    }
    else if(paySolOption == mesConstants.POS_WIRELESS_TERMINAL || isWirelessEquip())
    {
      addError("A valid Wireless Monthly Fee is required.");    
    }


    if(!isBlank(this.wirelessTransactionFee))
    {
      try
      {
        double temp = Double.parseDouble(this.wirelessTransactionFee);
        if(this.wirelessTransactionFeeFlag.equals("N"))
        {
          addError("Please check the Wireless Transaction Fee checkbox");
        }
      }
      catch(Exception e)
      {
        addError("Please provide a valid Wireless Transaction Fee");
      }
    }
    else if(this.wirelessTransactionFeeFlag.equals("Y"))
    {
        addError("Please provide a valid Wireless Transaction Fee or uncheck the Wireless Transaction Fee checkbox");
    }
    else if(paySolOption == mesConstants.POS_WIRELESS_TERMINAL || isWirelessEquip())
    {
      addError("A valid Wireless Transaction Fee is required.");    
    }

    double tempDouble = 0.0;
    String tempString = "";
    
    if(this.rental)
    {
      if(isBlank(this.rentalPrice))
      {
        addError("Please provide a valid Equipment Monthly Rental Price");
      }
      else
      {
        try
        {
          double td = Double.parseDouble(this.rentalPrice);
        }
        catch(Exception e)
        {
          addError("Please provide a valid Equipment Monthly Rental Price");
        }
      }
    }
    
    
    if(this.sales)
    {
      if(isBlank(this.salesPrice))
      {
        addError("Please provide a valid Equipment Sales Price");
      }
      else
      {
        try
        {
          double td = Double.parseDouble(this.salesPrice);
        }
        catch(Exception e)
        {
          addError("Please provide a valid Equipment Sales Price");
        }
      }
    }

    if(this.lease)
    {
      if(isBlank(this.leasePrice))
      {
        addError("Please provide a valid Equipment Monthly Lease Price");
      }
      else
      {
        try
        {
          double td = Double.parseDouble(this.leasePrice);
        }
        catch(Exception e)
        {
          addError("Please provide a valid Equipment Monthly Lease Price");
        }
      }
    }

    if(this.paySolOption == mesConstants.APP_PAYSOL_PC && this.pcQuantity.equals("-1"))
    {
      addError("Please provide a valid Quantity for Number of Licenses");
    }
    if(this.paySolOption == mesConstants.APP_PAYSOL_PC && this.pcFee.equals("-1"))
    {
      addError("Please provide a valid POS Partner Onetime License Fee ");
    }
    if(this.paySolOption == mesConstants.APP_PAYSOL_PC && isBlank(this.posPartnerMonthlyFee))
    {
      addError("Please provide a valid POS Partner Monthly Service Fee");
    }

    if(!isBlank(this.posPartnerMonthlyFee))
    {
      try
      {
        double temp = Double.parseDouble(this.posPartnerMonthlyFee);
      }
      catch(Exception e)
      {
        addError("Please provide a valid POS Partner Monthly Service Fee");
      }
    }
  
  if(this.paySolOption == mesConstants.POS_VIRTUAL_TERMINAL && this.vtSetupFee.equals("-1"))
  {
    addError("Please provide a valid Trident Virtual Terminal Setup Fee");
  }
  if(!isBlank(this.vtSetupFee))
  {
    try
    {
      double temp = Double.parseDouble(this.vtSetupFee);
    }
    catch(Exception e)
    {
      addError("Please provide a valid Trident Virtual Terminal Setup Fee");
    }
  }
  if(this.paySolOption == mesConstants.POS_VIRTUAL_TERMINAL && this.vtMonthlyFee.equals("-1"))
  {
    addError("Please provide a valid Trident Virtual Terminal Monthly Fee");
  }
  if(!isBlank(this.vtMonthlyFee))
  {
    try
    {
      double temp = Double.parseDouble(this.vtMonthlyFee);
    }
    catch(Exception e)
    {
      addError("Please provide a valid Trident Virtual Terminal Monthly Fee");
    }
  }

  if(this.checkReceivedEquipFlag.equals(CHECK_RECEIVED_NO) && !isBlank(this.checkReceivedEquipAmount))
  {
    addError("Please check the 'Check Received' checkbox if Amount of Check is provided in the Equipment/Software section");
  }
  else if(this.checkReceivedEquipFlag.equals(CHECK_RECEIVED_YES) && isBlank(this.checkReceivedEquipAmount))
  {
    addError("Please provide Amount of Check if 'Checked Received' checkbox is checked in the Equipment/Software section");
  }
  else if(this.checkReceivedEquipFlag.equals(CHECK_RECEIVED_YES) && !isBlank(this.checkReceivedEquipAmount))
  {
    try
    {
      double temp = Double.parseDouble(this.checkReceivedEquipAmount);
    }
    catch(Exception e)
    {
      addError("Please provide a valid Amount of Check in the Equipment/Software section");
    }
  }

  if(this.achDebitEquipFlag.equals(CHECK_RECEIVED_NO) && !isBlank(this.achDebitEquipAmount))
  {
    addError("Please check the 'ACH Debit' checkbox if Amount of ACH Debit is provided in the Equipment/Software section");
  }
  else if(this.achDebitEquipFlag.equals(CHECK_RECEIVED_YES) && isBlank(this.achDebitEquipAmount))
  {
    addError("Please provide Amount of ACH Debit if 'ACH Debit' checkbox is checked in the Equipment/Software section");
  }
  else if(this.achDebitEquipFlag.equals(CHECK_RECEIVED_YES) && !isBlank(this.achDebitEquipAmount))
  {
    try
    {
      double temp = Double.parseDouble(this.achDebitEquipAmount);
    }
    catch(Exception e)
    {
      addError("Please provide a valid Amount of ACH Debit in the Equipment/Software section");
    }
  }



    // only check business location for non-verisign/nsi apps
    if(this.businessLocation.equals("-1"))
    {
      addError("Please select a Location of Business");
    }
    if(this.businessLocation.equals("Other") && this.businessLocationComment.equals("-1"))
    {
      addError("Please explain Other Location of Business");
    }

    if(this.businessAddress.equals("Other") && this.businessAddressStreet.equals("-1"))
    {
      addError("Please provide other Address of Location");
    }
    if(this.businessAddress.equals("Other") && this.businessAddressCity.equals("-1"))
    {
      addError("Please provide other Address of Location City");
    }
    if(this.businessAddress.equals("Other") && this.businessAddressState.equals("-1"))
    {
      addError("Please provide other Address of Location State");
    }
    
    if (!this.businessAddressZip.equals("-1"))
    {
      switch ( this.businessAddressZip.length() ) 
      {
        case 5 : 
          if (!isNumber(this.businessAddressZip)) 
          {
            this.businessAddressZip = "-1";
          }
          break;
        case 9 : 
          if (!isNumber(this.businessAddressZip)) 
          {
            this.businessAddressZip = "-1";
          }
          break;
        case 10 : 
          if (!isNumber(this.businessAddressZip.substring(0,5)) || !isNumber(this.businessAddressZip.substring(6,10)) ||
              this.businessAddressZip.charAt(5) != '-') 
          {
            this.businessAddressZip = "-1";
          }
          break;
        default : 
          this.businessAddressZip = "-1";
          break;
      }
    }

    for(int i=0; i<this.NUM_QUESTIONS; i++)
    {
        if(this.questionFlag[i].equals("-1"))
        {
          this.questionFlag[i] = "";
        }
    }

    if(this.inventoryValue.equals("-2"))
    {
      addError("A valid Dollar Value of Inventory is needed");
    }
    
    if (!this.inventoryAddressZip.equals("-1"))
    {
      switch ( this.inventoryAddressZip.length() ) 
      {
        case 5 : 
          if (!isNumber(this.inventoryAddressZip)) 
          {
            this.inventoryAddressZip = "-1";
            addError("Please provide valid Address of Inventory Zip" );
          }
          break;
        case 9 : 
          if (!isNumber(this.inventoryAddressZip)) 
          {
            this.inventoryAddressZip = "-1";
            addError("Please provide valid Address of Inventory Zip" );
          }
          break;
        case 10 : 
          if (!isNumber(this.inventoryAddressZip.substring(0,5)) || !isNumber(this.inventoryAddressZip.substring(6,10)) ||
              this.inventoryAddressZip.charAt(5) != '-') 
          {
            this.inventoryAddressZip = "-1";
            addError("Please provide valid Address of Inventory Zip " );
          }
          break;
        default : 
          this.inventoryAddressZip = "-1";
          addError("Please provide valid Address of Inventory Zip" );
          break;
      }
    }
    
    if(this.houseFlag.equals("Y") && this.houseName.equals("-1"))
    {
      addError("Please provide Fullfillment House Name");
    }    
    if(this.houseFlag.equals("Y") && this.houseStreet.equals("-1"))
    {
      addError("Please provide Fullfillment House Address");
    }    
    if(this.houseFlag.equals("Y") && this.houseCity.equals("-1"))
    {
      addError("Please provide Fullfillment House Address City");
    }    
    if(this.houseFlag.equals("Y") && this.houseState.equals("-1"))
    {
      addError("Please provide Fullfillment House Address State");
    }    
    
    
    if (!this.houseZip.equals("-1"))
    {
      switch ( this.houseZip.length() ) 
      {
        case 5 : 
          if (!isNumber(this.houseZip)) 
          {
            this.houseZip = "-1";
          }
          break;
        case 9 : 
          if (!isNumber(this.houseZip)) 
          {
            this.houseZip = "-1";
          }
          break;
        case 10 : 
          if (!isNumber(this.houseZip.substring(0,5)) || !isNumber(this.houseZip.substring(6,10)) ||
              this.houseZip.charAt(5) != '-') 
          {
            this.houseZip = "-1";
          }
          break;
        default : 
          this.houseZip = "-1";
          break;
      }
    }
    
    if(this.houseFlag.equals("Y") && this.houseZip.equals("-1"))
    {
      addError("Please provide Fullfillment House Address Zip");
    }    
    
    if(this.softwareFlag.equals("Y") && this.softwareComment.equals("-1"))
    {
      addError("Please provide Name of Authentication Software");
    }    
  
    if(this.comment.length() > 400)
    {
      addError("Comments must be less than 400 characters long. It is currently " + this.comment.length() + " characters long");
    }

    if(this.dateReceived == null)
    {
      addError("A valid Date Application Received is needed");
    }
    if(this.dateSubmitted == null)
    {
      addError("A valid Date Application Submitted is needed");
    }
    if(isBlank(repCode))
    {
      addError("A valid Rep Code is needed");
    }
    else if(repCode.length() != 6 || !isReallyNumber(repCode))
    {
      addError("A valid 6 digit Rep Code number is needed");
    }

    return(! hasErrors());

  }
  
  private boolean isReallyNumber(String temp)
  {
    boolean result = false;
    try
    {
      int tempint = Integer.parseInt(temp);
      result = true;
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }

  public String getTotal(String lendCode, String amount, String quantity)
  {
    String temp = "";
    double total = 0.0;
    try
    {

      double qty = Double.parseDouble(quantity);
      double amt = Double.parseDouble(formatCurrency(amount));

      total = amt * qty;
      temp  = NumberFormat.getCurrencyInstance(Locale.US).format(total);

      if(lendCode.equals("1"))
      {
        this.subtotalPur += total;
      }
      this.subtotal   += total;
      this.grandtotal += total;

    }
    catch(Exception e)
    {
      temp = "$0.00";
    }
    return temp;
  }
  public String getSubtotal()
  {
    String temp = "";
    try
    {
      temp = NumberFormat.getCurrencyInstance(Locale.US).format(this.subtotal);
    }
    catch(Exception e)
    {
      temp = "$0.00";
    }
    return temp;
  }
  public String getSubtotalPur()
  {
    String temp = "";
    try
    {
      temp = NumberFormat.getCurrencyInstance(Locale.US).format(this.subtotalPur);
    }
    catch(Exception e)
    {
      temp = "$0.00";
    }
    return temp;
  }

  public String getPricingGrid()
  {
    return this.pricingGrid;
  }
  public void setPricingGrid(String pricingGrid)
  {
    this.pricingGrid = pricingGrid;
  }
  public Vector getPricingGrids()
  {
    return this.pricingGrids;
  }
  public Vector getPricingGridsCode()
  {
    return this.pricingGridsCode;
  }

  public Vector getBillingElementTableDesc()
  {
    return this.billingElementTableDesc;
  }

  public Vector getBillingElementTableCode()
  {
    return this.billingElementTableCode;
  }

  public String getTax()
  {
    String temp = "";
    try
    {
      if(this.taxRate < 0.0)
      {
        addError("Please enter a valid Tax Rate");
        return "0.00";
      }
      this.tax = this.subtotalPur * this.taxRate / 100.0;
      temp = NumberFormat.getCurrencyInstance(Locale.US).format(this.tax);
    }
    catch(Exception e)
    {
      this.tax = 0.0;
      temp = "$0.00";
    }
    return temp;
  }

  public String getGrandtotal()
  {
    String temp = "";
    try
    {
      temp = NumberFormat.getCurrencyInstance(Locale.US).format(this.grandtotal + this.tax);
    }
    catch(Exception e)
    {
      temp = "$0.00";
    }
    return temp;
  }

  public String formatCurrency(String num)
  {
    try
    {
      if(isBlank(num))
        return "";
      num = num.replace('f','l');
      num = num.replace('F','l');
      num = num.replace('d','l');
      num = num.replace('D','l');
      double check = Double.parseDouble(num);
    }
    catch(Exception e)
    {
      return "";
    }

    if(isBlank(num))
      return "";

    char last;
    int length = num.length();
    int index  = num.indexOf('.');

    if(index == -1)
      return num + ".00";

    String result = "";
    if(length - index >= 4)
    {
      result = num.substring(0,index+3);
      last = num.charAt(index+3);
      if(last > '5')
      {
        try
        {
          index  = result.indexOf('.');
          String subStr1 = result.substring(0,index);
          String subStr2 = result.substring(index+1);
          int tempInt = Integer.parseInt(subStr2);
          tempInt += 1;
          if (tempInt == 100)
          {
            int tempInt2 = Integer.parseInt(subStr1);
            tempInt2 += 1;
            result = tempInt2 + ".00";
          }
          else if(tempInt == 1)
          {
            result = subStr1 + ".01";
          }
          else
          {
            result = subStr1 + "." + tempInt;
          }
        }
        catch(Exception e)
        {
          return "";
        }
      }
    }
    else if(length - index == 3)
    {
      result = num;
    }
    else if(length - index == 2)
    {
      result = num + "0";
    }
    else if(length - index == 1)
    {
      result = num + "00";
    }

    return result;
  }

  public boolean isActivec()
  {
    return activec;
  }
  public boolean isCybercash()
  {
    return this.cybercash;
  }

  public String getStatementBreakdown()
  {
    return this.statementBreakdown;
  }

  public void setStatementBreakdown(String statementBreakdown)
  {
    this.statementBreakdown = statementBreakdown;
  }

  public String getCardDesc(int i)
  {
    return this.cardDesc[i];
  }
  public String getCardSelectedFlag(int i)
  {
    return this.cardSelectedFlag[i];
  }
  public String getCardFee(int i)
  {
    return this.cardFee[i];
  }
  
  public String getVmcAuthFeeSelected(double value)
  {
    String result = "";
    
    if(vmcAuthFee == value)
    {
      result = "selected";
    }
    
    return result;
  }

  public int getEquipmentInfoCount()
  {
    return this.equipmentInfoCount;
  }
  public void setEquipmentInfoCount(String equipmentInfoCount)
  {
    try
    {
      setEquipmentInfoCount(Integer.parseInt(equipmentInfoCount));
    }
    catch(Exception e)
    {
    }
  }
  public void setEquipmentInfoCount(int equipmentInfoCount)
  {
    this.equipmentInfoCount = equipmentInfoCount;
  }


  public String getMinMonthDiscount()
  {
    return this.minMonthDiscount;
  }

  public void setMinMonthDiscount(String minMonthDiscount)
  {
    this.minMonthDiscount = minMonthDiscount;
  }

  public String getTransDiscountRate9()
  {
    String result = "";
    if(this.transDiscountRate9 != -1)
    {
      result = String.valueOf(this.transDiscountRate9);
    }
    return result;
  }
  public void setTransDiscountRate9(String transDiscountRate9)
  {
    try
    {
      this.transDiscountRate9 = Double.parseDouble(transDiscountRate9);
    }
    catch(Exception e)
    {
     this.transDiscountRate9 = -1;
    }
  }


  public String getTransSurcharge9()
  {
    return this.transSurcharge9;
  }
  public void setTransSurcharge9(String transSurcharge9)
  {
    this.transSurcharge9 = transSurcharge9;
  }



  public String getTransDiscountRate10()
  {
    String result = "";
    if(this.transDiscountRate10 != -1)
    {
      result = String.valueOf(this.transDiscountRate10);
    }
    return result;
  }
  public void setTransDiscountRate10(String transDiscountRate10)
  {
    try
    {
      this.transDiscountRate10 = Double.parseDouble(transDiscountRate10);
    }
    catch(Exception e)
    {
     this.transDiscountRate10 = -1;
    }
  }
  
  public String getTransSurcharge10()
  {
    return this.transSurcharge10;
  }
  public void setTransSurcharge10(String transSurcharge10)
  {
    this.transSurcharge10 = transSurcharge10;
  }



  public String getTransPerItem10()
  {
    String result =  "";
    if(this.transPerItem10 != -1)
    {
      result = String.valueOf(transPerItem10);
    }
    return result;
  }
  public void setTransPerItem10(String transPerItem10)
  {
    try
    {
      transPerItem10 = transPerItem10;
      this.transPerItem10 = Double.parseDouble(transPerItem10);
    }
    catch(Exception e)
    {
     this.transPerItem10 = -1;
    }
  }
  public String getTransPerItem11()
  {
    String result =  "";
    if(this.transPerItem11 != -1)
    {
      result = String.valueOf(transPerItem11);
    }
    return result;
  }
  public void setTransPerItem11(String transPerItem11)
  {
    try
    {
      transPerItem11 = transPerItem11;
      this.transPerItem11 = Double.parseDouble(transPerItem11);
    }
    catch(Exception e)
    {
      this.transPerItem11 = -1;
    }
  }

  public String getTransDiscountRate11()
  {
    String result =  "";
    if(this.transDiscountRate11 != -1)
    {
      result = String.valueOf(transDiscountRate11);
    }
    return result;
  }
  public void setTransDiscountRate11(String transDiscountRate11)
  {
    try
    {
      transDiscountRate11 = transDiscountRate11;
      this.transDiscountRate11 = Double.parseDouble(transDiscountRate11);
    }
    catch(Exception e)
    {
      this.transDiscountRate11 = -1;
    }
  }


  public String getTransPerItem12()
  {
    String result =  "";
    if(this.transPerItem12 != -1)
    {
      result = String.valueOf(transPerItem12);
    }
    return result;
  }
  public void setTransPerItem12(String transPerItem12)
  {
    try
    {
      transPerItem12 = transPerItem12;
      this.transPerItem12 = Double.parseDouble(transPerItem12);
    }
    catch(Exception e)
    {
      this.transPerItem12 = -1;
    }
  }

  public String getTransDiscountRate12()
  {
    String result =  "";
    if(this.transDiscountRate12 != -1)
    {
      result = String.valueOf(transDiscountRate12);
    }
    return result;
  }
  public void setTransDiscountRate12(String transDiscountRate12)
  {
    try
    {
      transDiscountRate12 = transDiscountRate12;
      this.transDiscountRate12 = Double.parseDouble(transDiscountRate12);
    }
    catch(Exception e)
    {
      this.transDiscountRate12 = -1;
    }
  }

  //CHARGES FOR TRANSCOM
  public String getChargebackFee()
  {
    return this.chargebackFee;
  }
  public void setChargebackFee(String chargebackFee)
  {
    this.chargebackFee = chargebackFee;
  }

  public String getRetrievalRequestFee()
  {
    return this.retrievalRequestFee;
  }
  public void setRetrievalRequestFee(String retrievalRequestFee)
  {
    this.retrievalRequestFee = retrievalRequestFee;
  }

  public String getStatementFee()
  {
    return this.statementFee;
  }
  public void setStatementFee(String statementFee)
  {
    this.statementFee = statementFee;
  }

  public String getInternetReportFee()
  {
    return this.internetReportFee;
  }
  public void setInternetReportFee(String internetReportFee)
  {
    this.internetReportFee = internetReportFee;
  }

  public String getApplicationFee()
  {
    return this.applicationFee;
  }
  public void setApplicationFee(String applicationFee)
  {
    this.applicationFee = applicationFee;
  }

  public String getTerminalReprogrammingFee()
  {
    return this.terminalReprogrammingFee;
  }
  public void setTerminalReprogrammingFee(String terminalReprogrammingFee)
  {
    this.terminalReprogrammingFee = terminalReprogrammingFee;
  }

  public String getNumTermsReprogramming()
  {
    return this.numTermsReprogramming;
  }
  public void setNumTermsReprogramming(String numTermsReprogramming)
  {
    this.numTermsReprogramming = numTermsReprogramming;
  }

  public String getPinpadSwapFee()
  {
    return this.pinpadSwapFee;
  }
  public void setPinpadSwapFee(String pinpadSwapFee)
  {
    this.pinpadSwapFee = pinpadSwapFee;
  }

  public String getAchDepositFee()
  {
    return this.achDepositFee;
  }
  public void setAchDepositFee(String achDepositFee)
  {
    this.achDepositFee = achDepositFee;
  }

  public String getDebitAccessFee()
  {
    return this.debitAccessFee;
  }
  public void setDebitAccessFee(String debitAccessFee)
  {
    this.debitAccessFee = debitAccessFee;
  }

  public String getInternetStartupFee()
  {
    return this.internetStartupFee;
  }
  public void setInternetStartupFee(String internetStartupFee)
  {
    this.internetStartupFee = internetStartupFee;
  }

  public String getGatewayFee()
  {
    return this.gatewayFee;
  }
  public void setGatewayFee(String gatewayFee)
  {
    this.gatewayFee = gatewayFee;
  }

  public String getAnnualFee()
  {
    return this.annualFee;
  }
  public void setAnnualFee(String annualFee)
  {
    this.annualFee = annualFee;
  }
  
  public String getSupplyFee()
  {
    return this.supplyFee;
  }
  public void setSupplyFee(String supplyFee)
  {
    this.supplyFee = supplyFee;
  }
  
  public String getVtSetupFee()
  {
    return vtSetupFee;
  }
  public void setVtSetupFee(String vtSetupFee)
  {
    this.vtSetupFee = vtSetupFee;
  }
  public String getVtMonthlyFee()
  {
    return vtMonthlyFee;
  }
  public void setVtMonthlyFee(String vtMonthlyFee)
  {
    this.vtMonthlyFee = vtMonthlyFee;
  }

  public String getTrainingFee()
  {
    return this.trainingFee;
  }
  public void setTrainingFee(String trainingFee)
  {
    this.trainingFee = trainingFee;
  }

  public String getMiscellaneous1Fee()
  {
    return this.miscellaneous1Fee;
  }
  public void setMiscellaneous1Fee(String miscellaneous1Fee)
  {
    this.miscellaneous1Fee = miscellaneous1Fee;
  }

  public String getMiscellaneous1ChargeBasis()
  {
    return this.miscellaneous1ChargeBasis;
  }
  public void setMiscellaneous1ChargeBasis(String miscellaneous1ChargeBasis)
  {
    this.miscellaneous1ChargeBasis = miscellaneous1ChargeBasis;
  }
  public String getMiscellaneous1FeeDesc()
  {
    return this.miscellaneous1FeeDesc;
  }
  public void setMiscellaneous1FeeDesc(String miscellaneous1FeeDesc)
  {
    this.miscellaneous1FeeDesc = miscellaneous1FeeDesc;
  }


  public String getMiscellaneous2Fee()
  {
    return this.miscellaneous2Fee;
  }
  public void setMiscellaneous2Fee(String miscellaneous2Fee)
  {
    this.miscellaneous2Fee = miscellaneous2Fee;
  }

  public String getMiscellaneous2ChargeBasis()
  {
    return this.miscellaneous2ChargeBasis;
  }
  public void setMiscellaneous2ChargeBasis(String miscellaneous2ChargeBasis)
  {
    this.miscellaneous2ChargeBasis = miscellaneous2ChargeBasis;
  }
  public String getMiscellaneous2FeeDesc()
  {
    return this.miscellaneous2FeeDesc;
  }
  public void setMiscellaneous2FeeDesc(String miscellaneous2FeeDesc)
  {
    this.miscellaneous2FeeDesc = miscellaneous2FeeDesc;
  }


  public String getAchDebitEquipFlag()
  {
    return this.achDebitEquipFlag;
  }
  public void setAchDebitEquipFlag(String achDebitEquipFlag)
  {
    this.achDebitEquipFlag = achDebitEquipFlag;
  }

  public String getAchDebitEquipAmount()
  {
    return this.achDebitEquipAmount;
  }
  public void setAchDebitEquipAmount(String achDebitEquipAmount)
  {
    this.achDebitEquipAmount = achDebitEquipAmount;
  }



  public String getCheckReceivedChrgFlag()
  {
    return this.checkReceivedChrgFlag;
  }
  public void setCheckReceivedChrgFlag(String checkReceivedChrgFlag)
  {
    this.checkReceivedChrgFlag = checkReceivedChrgFlag;
  }

  public String getCheckReceivedChrgAmount()
  {
    return this.checkReceivedChrgAmount;
  }
  public void setCheckReceivedChrgAmount(String checkReceivedChrgAmount)
  {
    this.checkReceivedChrgAmount = checkReceivedChrgAmount;
  }

  public String getCheckReceivedEquipFlag()
  {
    return this.checkReceivedEquipFlag;
  }
  public void setCheckReceivedEquipFlag(String checkReceivedEquipFlag)
  {
    this.checkReceivedEquipFlag = checkReceivedEquipFlag;
  }

  public String getCheckReceivedEquipAmount()
  {
    return this.checkReceivedEquipAmount;
  }
  public void setCheckReceivedEquipAmount(String checkReceivedEquipAmount)
  {
    this.checkReceivedEquipAmount = checkReceivedEquipAmount;
  }


  public String getVoiceAuthFee()
  {
    return this.voiceAuthFee;
  }
  public void setVoiceAuthFee(String voiceAuthFee)
  {
    this.voiceAuthFee = voiceAuthFee;
  }
  
  public String getPosPartnerMonthlyFee()
  {
    return this.posPartnerMonthlyFee;
  }
  public void setPosPartnerMonthlyFee(String posPartnerMonthlyFee)
  {
    this.posPartnerMonthlyFee = posPartnerMonthlyFee;
  }

  public String getChargebackFeeFlag()
  {
    return this.chargebackFeeFlag;
  }
  public void setChargebackFeeFlag(String chargebackFeeFlag)
  {
    this.chargebackFeeFlag = chargebackFeeFlag;
  }

  public String getWirelessSetupFeeFlag()
  {
    return this.wirelessSetupFeeFlag;
  }
  public void setWirelessSetupFeeFlag(String wirelessSetupFeeFlag)
  {
    this.wirelessSetupFeeFlag = wirelessSetupFeeFlag;
  }

  public String getWirelessMonthlyFeeFlag()
  {
    return this.wirelessMonthlyFeeFlag;
  }
  public void setWirelessMonthlyFeeFlag(String wirelessMonthlyFeeFlag)
  {
    this.wirelessMonthlyFeeFlag = wirelessMonthlyFeeFlag;
  }

  public String getWirelessTransactionFeeFlag()
  {
    return this.wirelessTransactionFeeFlag;
  }
  public void setWirelessTransactionFeeFlag(String wirelessTransactionFeeFlag)
  {
    this.wirelessTransactionFeeFlag = wirelessTransactionFeeFlag;
  }

  public String getWirelessSetupFee()
  {
    return this.wirelessSetupFee;
  }
  public void setWirelessSetupFee(String wirelessSetupFee)
  {
    this.wirelessSetupFee = wirelessSetupFee;
  }

  public String getWirelessMonthlyFee()
  {
    return this.wirelessMonthlyFee;
  }
  public void setWirelessMonthlyFee(String wirelessMonthlyFee)
  {
    this.wirelessMonthlyFee = wirelessMonthlyFee;
  }

  public String getWirelessTransactionFee()
  {
    return this.wirelessTransactionFee;
  }
  public void setWirelessTransactionFee(String wirelessTransactionFee)
  {
    this.wirelessTransactionFee = wirelessTransactionFee;
  }

  public String getRetrievalRequestFeeFlag()
  {
    return this.retrievalRequestFeeFlag;
  }
  public void setRetrievalRequestFeeFlag(String retrievalRequestFeeFlag)
  {
    this.retrievalRequestFeeFlag = retrievalRequestFeeFlag;
  }

  public String getStatementFeeFlag()
  {
    return this.statementFeeFlag;
  }
  public void setStatementFeeFlag(String statementFeeFlag)
  {
    this.statementFeeFlag = statementFeeFlag;
  }

  public String getInternetReportFeeFlag()
  {
    return this.internetReportFeeFlag;
  }
  public void setInternetReportFeeFlag(String internetReportFeeFlag)
  {
    this.internetReportFeeFlag = internetReportFeeFlag;
  }

  public String getApplicationFeeFlag()
  {
    return this.applicationFeeFlag;
  }
  public void setApplicationFeeFlag(String applicationFeeFlag)
  {
    this.applicationFeeFlag = applicationFeeFlag;
  }

  public String getTerminalReprogrammingFeeFlag()
  {
    return this.terminalReprogrammingFeeFlag;
  }
  public void setTerminalReprogrammingFeeFlag(String terminalReprogrammingFeeFlag)
  {
    this.terminalReprogrammingFeeFlag = terminalReprogrammingFeeFlag;
  }

  public String getNumTermsReprogrammingFlag()
  {
    return this.numTermsReprogrammingFlag;
  }
  public void setNumTermsReprogrammingFlag(String numTermsReprogrammingFlag)
  {
    this.numTermsReprogrammingFlag = numTermsReprogrammingFlag;
  }

  public String getPinpadSwapFeeFlag()
  {
    return this.pinpadSwapFeeFlag;
  }
  public void setPinpadSwapFeeFlag(String pinpadSwapFeeFlag)
  {
    this.pinpadSwapFeeFlag = pinpadSwapFeeFlag;
  }

  public String getMinMonthDiscountFlag()
  {
    return this.minMonthDiscountFlag;
  }
  public void setMinMonthDiscountFlag(String minMonthDiscountFlag)
  {
    this.minMonthDiscountFlag = minMonthDiscountFlag;
  }

  public String getAchDepositFeeFlag()
  {
    return this.achDepositFeeFlag;
  }
  public void setAchDepositFeeFlag(String achDepositFeeFlag)
  {
    this.achDepositFeeFlag = achDepositFeeFlag;
  }

  public String getDebitAccessFeeFlag()
  {
    return this.debitAccessFeeFlag;
  }
  public void setDebitAccessFeeFlag(String debitAccessFeeFlag)
  {
    this.debitAccessFeeFlag = debitAccessFeeFlag;
  }

  public String getInternetStartupFeeFlag()
  {
    return this.internetStartupFeeFlag;
  }
  public void setInternetStartupFeeFlag(String internetStartupFeeFlag)
  {
    this.internetStartupFeeFlag = internetStartupFeeFlag;
  }

  public String getGatewayFeeFlag()
  {
    return this.gatewayFeeFlag;
  }
  public void setGatewayFeeFlag(String gatewayFeeFlag)
  {
    this.gatewayFeeFlag = gatewayFeeFlag;
  }

  public String getAnnualFeeFlag()
  {
    return this.annualFeeFlag;
  }
  public void setAnnualFeeFlag(String annualFeeFlag)
  {
    this.annualFeeFlag = annualFeeFlag;
  }
  
  public void setSupplyConfig(String supplyConfig)
  {
    this.supplyConfig = supplyConfig;
  }
  public String getSupplyConfig()
  {
    return ( supplyConfig == null ? "" : supplyConfig );  
  }
  
  public String getVtSetupFeeFlag()
  {
    return vtSetupFeeFlag;
  }
  public void setVtSetupFeeFlag(String vtSetupFeeFlag)
  {
    this.vtSetupFeeFlag = vtSetupFeeFlag;
  }
  public String getVtMonthlyFeeFlag()
  {
    return vtMonthlyFeeFlag;
  }
  public void setVtMonthlyFeeFlag(String vtMonthlyFeeFlag)
  {
    this.vtMonthlyFeeFlag = vtMonthlyFeeFlag;
  }

  public String getTrainingFeeFlag()
  {
    return this.trainingFeeFlag;
  }
  public void setTrainingFeeFlag(String trainingFeeFlag)
  {
    this.trainingFeeFlag = trainingFeeFlag;
  }

  public String getMiscellaneous1FeeFlag()
  {
    return this.miscellaneous1FeeFlag;
  }
  public void setMiscellaneous1FeeFlag(String miscellaneous1FeeFlag)
  {
    this.miscellaneous1FeeFlag = miscellaneous1FeeFlag;
  }

  public String getMiscellaneous2FeeFlag()
  {
    return this.miscellaneous2FeeFlag;
  }
  public void setMiscellaneous2FeeFlag(String miscellaneous2FeeFlag)
  {
    this.miscellaneous2FeeFlag = miscellaneous2FeeFlag;
  }



  public String getOwnedEquipFlag()
  {
    return this.ownedEquipFlag;
  }
  public void setOwnedEquipFlag(String ownedEquipFlag)
  {
      this.ownedEquipFlag = ownedEquipFlag;
  }

  public String getOwnedEquipQty()
  {
    String result = "";
    if(!this.ownedEquipQty.equals("-1"))
    {
      result = this.ownedEquipQty;
    }
    return result;
  }
  public void setOwnedEquipQty(String ownedEquipQty)
  {
    try
    {
      int tempInt = Integer.parseInt(ownedEquipQty);
      this.ownedEquipQty = ownedEquipQty;
    }
    catch(Exception e)
    {
      this.ownedEquipQty = "-1";
    }
  }

  public String getOwnedEquipAmt()
  {
    String result = "";

    if(!isBlank(this.ownedEquipAmt) && !this.ownedEquipAmt.equals("-1"))
    {
      result = this.ownedEquipAmt;
    }
    return result;
  }
  public void setOwnedEquipAmt(String ownedEquipAmt)
  {
    try
    {
      double tempDouble = Double.parseDouble(ownedEquipAmt);
      if(tempDouble < 0.0)
      {
        this.ownedEquipAmt = "-1";
      }
      else
      {
        this.ownedEquipAmt = formatCurrency(ownedEquipAmt);
      }
    }
    catch(Exception e)
    {
      this.ownedEquipAmt = "-1";
    }
  }

  public int getPricingScenario()
  {
    return this.pricingScenario;
  }
  public void setPricingScenario(String pricingScenario)
  {
    try
    {
      this.pricingScenario = Integer.parseInt(pricingScenario);
    }
    catch(Exception e)
    {
      this.pricingScenario = mesConstants.APP_PS_INVALID;
    }
  }


  public int getPaySolOption()
  {
    return this.paySolOption;
  }
  public void setPaySolOption(int paySolOption)
  {
    this.paySolOption = paySolOption;
  }

  public void setPaySolOption(String paySolOption)
  {
    try
    {
      int tempInt = Integer.parseInt(paySolOption);
      setPaySolOption(tempInt);
    }
    catch(Exception e)
    {
      setPaySolOption(-1);
    }
  }

  public String getPcQuantity()
  {
    String result = "";
    if(!this.pcQuantity.equals("-1"))
    {
      result = this.pcQuantity;
    }
    return result;
  }

  public void setPcQuantity(String pcQuantity)
  {
    try
    {
      int tempInt = Integer.parseInt(pcQuantity);
      if(tempInt < 0)
      {
        this.pcQuantity = "-1";
      }
      else
      {
        this.pcQuantity = pcQuantity;
      }
    }
    catch(Exception e)
    {
      this.pcQuantity = "-1";
    }
  }
  public String getPcModel()
  {
    return this.pcModel;
  }

  public void setPcModel(String pcModel)
  {
    this.pcModel = pcModel;
  }

  public String getBillingElementTable()
  {
    return this.billingElementTable;
  }

  public void setBillingElementTable(String billingElementTable)
  {
    this.billingElementTable = billingElementTable;
  }


  public String getPcFee()
  {
    String result = "";
    if(!this.pcFee.equals("-1"))
    {
      result = this.pcFee;
    }
    return result;
  }
  public void setPcFee(String pcFee)
  {
    try
    {
      double tempDouble = Double.parseDouble(pcFee);
      if(tempDouble < 0.0)
      {
        this.pcFee = "-1";
      }
      else
      {
        this.pcFee = formatCurrency(pcFee);
      }
    }
    catch(Exception e)
    {
      this.pcFee = "-1";
    }
  }

  public String getBusinessAddressZip()
  {
    String result = "";
    if(!this.businessAddressZip.equals("-1"))
    {
      result = this.businessAddressZip;
    }
    return result;
  }
  public void setBusinessAddressZip(String businessAddressZip)
  {
    if(!isBlank(businessAddressZip))
      this.businessAddressZip = businessAddressZip;
    else
      this.businessAddressZip = "-1";
  }
  public String getNumEmployees()
  {
    String result = "";
    if(!this.numEmployees.equals("-1"))
    {
      result = this.numEmployees;
    }
    return result;
  }
  public void setNumEmployees(String numEmployees)
  {
    try
    {
      int temp = Integer.parseInt(numEmployees);
      this.numEmployees = numEmployees;
    }
    catch(Exception e)
    {
      this.numEmployees = "-1";
    }
  }

  public String getBusinessLocationComment()
  {
    String result = "";
    if(!this.businessLocationComment.equals("-1"))
    {
      result = this.businessLocationComment;
    }
    return result;
  }
  public void setBusinessLocationComment(String businessLocationComment)
  {
    if(!isBlank(businessLocationComment))
      this.businessLocationComment = businessLocationComment;
    else
      this.businessLocationComment = "-1";
  }

  public String getBusinessAddressCity()
  {
    String result = "";
    if(!this.businessAddressCity.equals("-1"))
    {
      result = this.businessAddressCity;
    }
    return result;
  }
  public void setBusinessAddressCity(String businessAddressCity)
  {
    if(!isBlank(businessAddressCity))
      this.businessAddressCity = businessAddressCity;
    else
      this.businessAddressCity = "-1";
  }


  public String getBusinessAddressStreet()
  {
    String result = "";
    if(!this.businessAddressStreet.equals("-1"))
    {
      result = this.businessAddressStreet;
    }
    return result;
  }
  public void setBusinessAddressStreet(String businessAddressStreet)
  {
    if(!isBlank(businessAddressStreet))
      this.businessAddressStreet = businessAddressStreet;
    else
      this.businessAddressStreet = "-1";
  }

  public String getBusinessLocation()
  {
    return this.businessLocation;
  }
  public void setBusinessLocation(String businessLocation)
  {
    this.businessLocation = businessLocation;
  }
  public String getBusinessAddress()
  {
    return this.businessAddress;
  }
  public void setBusinessAddress(String businessAddress)
  {
    this.businessAddress = businessAddress;
  }
  public String getBusinessAddressState()
  {
    return this.businessAddressState;
  }
  public void setBusinessAddressState(String businessAddressState)
  {
    this.businessAddressState = businessAddressState;
  }

  public String getQuestionDesc(int i)
  {
    return this.questionDesc[i];
  }
  public String getQuestionFlag(int i)
  {
    return this.questionFlag[i];
  }

  public String getInventoryAddressStreet()
  {
    String result = "";
    if(!this.inventoryAddressStreet.equals("-1"))
    {
      result = this.inventoryAddressStreet;
    }
    return result;
  }
  public void setInventoryAddressStreet(String inventoryAddressStreet)
  {
    if(!isBlank(inventoryAddressStreet))
      this.inventoryAddressStreet = inventoryAddressStreet;
    else
      this.inventoryAddressStreet = "-1";
  }

  public String getInventoryAddressCity()
  {
    String result = "";
    if(!this.inventoryAddressCity.equals("-1"))
    {
      result = this.inventoryAddressCity;
    }
    return result;
  }
  public void setInventoryAddressCity(String inventoryAddressCity)
  {
    if(!isBlank(inventoryAddressCity))
      this.inventoryAddressCity = inventoryAddressCity;
    else
      this.inventoryAddressCity = "-1";
  }

  public String getInventoryAddressState()
  {
    return this.inventoryAddressState;
  }
  public void setInventoryAddressState(String inventoryAddressState)
  {
    this.inventoryAddressState = inventoryAddressState;
  }

  public String getInventoryAddressZip()
  {
    String result = "";
    if(!this.inventoryAddressZip.equals("-1"))
    {
      result = this.inventoryAddressZip;
    }
    return result;
  }
  public void setInventoryAddressZip(String inventoryAddressZip)
  {
    if(!isBlank(inventoryAddressZip))
      this.inventoryAddressZip = inventoryAddressZip;
    else
      this.inventoryAddressZip = "-1";
  }


  public String getHouseStreet()
  {
    String result = "";
    if(!this.houseStreet.equals("-1"))
    {
      result = this.houseStreet;
    }
    return result;
  }
  public void setHouseStreet(String houseStreet)
  {
    if(!isBlank(houseStreet))
      this.houseStreet = houseStreet;
    else
      this.houseStreet = "-1";
  }

  public String getComment()
  {
    return this.comment;
  }
  public void setComment(String comment)
  {
    this.comment = comment;
  }

  public String getHouseCity()
  {
    String result = "";
    if(!this.houseCity.equals("-1"))
    {
      result = this.houseCity;
    }
    return result;
  }
  public void setHouseCity(String houseCity)
  {
    if(!isBlank(houseCity))
      this.houseCity = houseCity;
    else
      this.houseCity = "-1";
  }

  public String getHouseState()
  {
    return this.houseState;
  }
  public void setHouseState(String houseState)
  {
    this.houseState = houseState;
  }

  public String getHouseZip()
  {
    String result = "";
    if(!this.houseZip.equals("-1"))
    {
      result = this.houseZip;
    }
    return result;
  }
  public void setHouseZip(String houseZip)
  {
    if(!isBlank(houseZip))
      this.houseZip = houseZip;
    else
      this.houseZip = "-1";
  }


  public String getHouseFlag()
  {
    String result = "";
    if(!this.houseFlag.equals("-1"))
    {
      result = this.houseFlag;
    }
    return result;
  }
  public void setHouseFlag(String houseFlag)
  {
    if(!isBlank(houseFlag))
      this.houseFlag = houseFlag;
    else
      this.houseFlag = "-1";
  }

  public String getSoftwareFlag()
  {
    String result = "";
    if(!this.softwareFlag.equals("-1"))
    {
      result = this.softwareFlag;
    }
    return result;
  }
  public void setSoftwareFlag(String softwareFlag)
  {
    if(!isBlank(softwareFlag))
      this.softwareFlag = softwareFlag;
    else
      this.softwareFlag = "-1";
  }

  public String getHouseName()
  {
    String result = "";
    if(!this.houseName.equals("-1"))
    {
      result = this.houseName;
    }
    return result;
  }
  public void setHouseName(String houseName)
  {
    if(!isBlank(houseName))
      this.houseName = houseName;
    else
      this.houseName = "-1";
  }

  public String getSoftwareComment()
  {
    String result = "";
    if(!this.softwareComment.equals("-1"))
    {
      result = this.softwareComment;
    }
    return result;
  }
  public void setSoftwareComment(String softwareComment)
  {
    if(!isBlank(softwareComment))
      this.softwareComment = softwareComment;
    else
      this.softwareComment = "-1";
  }

  public String getInventoryValue()
  {
    String result = "";
    if(!this.inventoryValue.equals("-1") && !this.inventoryValue.equals("-2"))
    {
      result = this.inventoryValue;
    }
    return result;
  }
  public void setInventoryValue(String inventoryValue)
  {
    try
    {
      if(isBlank(inventoryValue))
        inventoryValue = "-1";
      double temp = Double.parseDouble(inventoryValue);
      if(temp < 0.0)
      {
        this.inventoryValue = "-2";
      }
      else
      {
        this.inventoryValue = formatCurrency(inventoryValue);
      }
    }
    catch(Exception e)
    {
      this.inventoryValue = "-2";
    }
  }

  public String getRepCode()
  {
    return this.repCode;
  }
  public void setRepCode(String repCode)
  {
    this.repCode = repCode;
  }

  private boolean validDate(String date)
  {
    int index1  = date.indexOf('/');
    int index2  = date.lastIndexOf('/');
    String mon  = date.substring(0,index1);
    String day  = date.substring(index1+1,index2);
    String year = date.substring(index2+1);
    boolean result = true;
    try
    {
      int dayInt = Integer.parseInt(day);
      int monInt = Integer.parseInt(mon);
      int yearInt = Integer.parseInt(year);
      if(dayInt > 31 || dayInt < 1)
        result = false;
      if(monInt > 12 || monInt < 1)
        result = false;
      if(yearInt < 0)
        result = false;
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }

  public String getDateReceived()
  {
    DateFormat df   = new SimpleDateFormat("MM/dd/yyyy");
    String result = "";
    if(this.dateReceived != null)
    {
      result = df.format(this.dateReceived);
    }
    return result;
  }  
  public String getDateSubmitted()
  {
    DateFormat df   = new SimpleDateFormat("MM/dd/yyyy");
    String result = "";
    if(this.dateSubmitted != null)
    {
      result = df.format(this.dateSubmitted);
    }
    return result;
  }  
  public void setDateReceived(String dateReceived)
  {
    String dateReceivedStr = dateReceived.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(dateReceived.trim());
      try
      {
        dateReceived          = dateReceived.replace('/','0');
        long tempLong         = Long.parseLong(dateReceived.trim());
        if(validDate(dateReceivedStr.trim()))
        {
          this.dateReceived = aDate;
        }
        else
        {
          this.dateReceived = null;
        }
      }
      catch(Exception e)
      {
        this.dateReceived    = null;
      }
    }
    catch(ParseException e)
    {
      this.dateReceived      = null;
    }
  }

  public void setDateSubmitted(String dateSubmitted)
  {
    String dateSubmittedStr = dateSubmitted.trim();
    Date aDate;
    DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
    try
    {
      aDate = fmt.parse(dateSubmitted.trim());
      try
      {
        dateSubmitted         = dateSubmitted.replace('/','0');
        long tempLong         = Long.parseLong(dateSubmitted.trim());
        if(validDate(dateSubmittedStr.trim()))
        {
          this.dateSubmitted = aDate;
        }
        else
        {
          this.dateSubmitted = null;
        }
      }
      catch(Exception e)
      {
        this.dateSubmitted    = null;
      }
    }
    catch(ParseException e)
    {
      this.dateSubmitted      = null;
    }
  }

  public void setTaxRate(String taxRate)
  {
    try
    {
      if(isBlank(taxRate))
        taxRate    = "0";
      this.taxRate = Double.parseDouble(taxRate);
    }
    catch(Exception e)
    {
      this.taxRate = -2;
    }
  }
  public String getTaxRate()
  {
    String result = "";
    if(this.taxRate != 0 && this.taxRate != -2)
    {
      result = String.valueOf(this.taxRate);
    }
    return result;
  }

  public boolean isWirelessEquip()
  {
    return this.wirelessEquip;
  }
  
  public void setWirelessEquip()
  {
    this.wirelessEquip = true;
  }

  public boolean isRecalculated()
  {
    return this.recalculate;
  }

  public int getAppType()
  {
    return this.appType;
  }

  public boolean isRental()
  {
    return this.rental;
  }
  public void setRental(String temp)
  {
    this.rental = true;
  }

  public void setRentalPrice(String rentalPrice)
  {
    this.rentalPrice = rentalPrice;
  }
  public String getRentalPrice()
  {
    return this.rentalPrice;
  }

  public boolean isSales()
  {
    return this.sales;
  }
  public void setSales(String temp)
  {
    this.sales = true;
  }
  
  public void setSalesPrice(String salesPrice)
  {
    this.salesPrice = salesPrice;
  }
  public String getSalesPrice()
  {
    return this.salesPrice;
  }

  public boolean isLease()
  {
    return this.lease;
  }
  public void setLease(String temp)
  {
    this.lease = true;
  }

  public void setLeasePrice(String leasePrice)
  {
    this.leasePrice = leasePrice;
  }
  public String getLeasePrice()
  {
    return this.leasePrice;
  }

  public void setRecalculate(String recalculate)
  {
    this.recalculate = true;
  }
  public boolean getRecalculate()
  {
    return this.recalculate;
  }
  public String getTarget()
  {
    String target = "";
    if(this.recalculate == true)
      target="equip";
    return target;
  }
  
  public void setAllowSubmit(String temp)
  {
    this.allowSubmit = true;
  }
  public boolean allowSubmit()
  {
    return this.allowSubmit;
  }

  public boolean isAmexZero()
  {
    return amexZero;
  }
  public void setAmexZero(String amexZero)
  {
    this.amexZero = true;
  }

  public boolean isAmexSplit()
  {
    return amexSplit;
  }

  public void setAmexSplit(String amexSplit)
  {
    this.amexSplit = true;
  }

  public void submitData(HttpServletRequest aReq, long appSeqNum)
  {
    submitGrid(appSeqNum);
    submitCardInfo(aReq, appSeqNum);
    submitMiscInfo(aReq, appSeqNum);
    submitEquipInfo(aReq, appSeqNum);
    submitSiteInspectionInfo(appSeqNum);
    submitTranscomInfo(appSeqNum);
  }

  private void submitTranscomInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    
    try
    {
      qs.append("update transcom_merchant set ");
      qs.append("MISC_FEE1_BASIS        = ?, ");
      qs.append("MISC_FEE1_DESC         = ?, ");
      qs.append("MISC_FEE2_BASIS        = ?, ");
      qs.append("MISC_FEE2_DESC         = ?, ");
      qs.append("CHECK_AMOUNT_MISC      = ?, ");
      qs.append("CHECK_AMOUNT_EQUIP     = ?, ");
      qs.append("ACH_DEBIT_AMOUNT_EQUIP = ?, ");
      qs.append("DATE_APP_RECEIVED      = ?, ");
      qs.append("DATE_APP_SUBMITTED     = ?, ");
      qs.append("REP_CODE               = ?,  ");
      qs.append("NUM_REPROGRMMING       = ?,  ");
      qs.append("BET_TABLE              = ?   ");      
      qs.append("where  app_seq_num = ? ");

      ps = getPreparedStatement(qs.toString());
      
      ps.setString(1, miscellaneous1ChargeBasis);
      ps.setString(2, miscellaneous1FeeDesc);
      ps.setString(3, miscellaneous2ChargeBasis);
      ps.setString(4, miscellaneous2FeeDesc);
      ps.setString(5, checkReceivedChrgAmount);
      ps.setString(6, checkReceivedEquipAmount);
      ps.setString(7, achDebitEquipAmount);
      ps.setDate(8, new java.sql.Date(this.dateReceived.getTime()));
      ps.setDate(9, new java.sql.Date(this.dateSubmitted.getTime()));
      ps.setString(10, repCode);
      ps.setString(11, numTermsReprogramming);
      ps.setString(12, billingElementTable);
      ps.setLong(13, appSeqNum);

      ps.executeUpdate();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitTranscomInfo: " + e.toString());
      addError("submitTranscomInfo: " + e.toString());
    }
  }

  private void submitGrid(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;

    try
    {
      qs.append("update merchant ");
      qs.append("set pricing_grid = ?, ");
      qs.append("merch_dly_discount_flag = 'N' ");
      qs.append("where app_seq_num = ?");

      ps = getPreparedStatement(qs.toString());
      ps.setString(1, this.pricingGrid);
      ps.setLong(2, appSeqNum);

      ps.executeUpdate();

      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitGrid: " + e.toString());
      addError("submitGrid: " + e.toString());
    }
  }

  private void submitEquipInfo(HttpServletRequest aReq, long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;

    try
    {
      switch(this.paySolOption)
      {
        case mesConstants.APP_PAYSOL_DIAL_TERMINAL :
        case mesConstants.APP_PAYSOL_DIAL_PAY:

          qs.setLength(0);
          qs.append("update merchequipment set merchequip_amount = ? ");
          qs.append("where equiplendtype_code = ? and app_seq_num = ? ");

          ps = getPreparedStatement(qs.toString());

          if(this.sales)
          {
            ps.setString(1,this.salesPrice);
            ps.setInt(2,mesConstants.APP_EQUIP_PURCHASE);
            ps.setLong(3,appSeqNum);
            ps.executeUpdate();
          }
          if(this.rental)
          {
            ps.setString(1,this.rentalPrice);
            ps.setInt(2,mesConstants.APP_EQUIP_RENT);
            ps.setLong(3,appSeqNum);
            ps.executeUpdate();
          }
          if(this.lease)
          {
            ps.setString(1,this.leasePrice);
            ps.setInt(2,mesConstants.APP_EQUIP_LEASE);
            ps.setLong(3,appSeqNum);
            ps.executeUpdate();
          }
          
          ps.close();

          break;

        case mesConstants.APP_PAYSOL_PC:

          qs.setLength(0);
          qs.append("delete from merchequipment ");
          qs.append("where app_seq_num = ? and equip_model = ? ");

          ps = getPreparedStatement(qs.toString());
          ps.setLong  (1, appSeqNum);
          ps.setString(2,"PCPS");

          ps.executeUpdate();
          ps.close();

          qs.setLength(0);
          qs.append("insert into merchequipment(app_seq_num,equip_model, ");
          qs.append("equiplendtype_code,merchequip_amount, ");
          qs.append("merchequip_equip_quantity,equiptype_code) values (?,?,?,?,?,?) ");

          ps = getPreparedStatement(qs.toString());
          ps.clearParameters();

          ps.setLong(1, appSeqNum);
          ps.setString(2, "PCPS");
          ps.setInt(3, mesConstants.APP_EQUIP_PURCHASE);
          ps.setString(4, this.pcFee);
          ps.setString(5, this.pcQuantity);
          ps.setInt(6, mesConstants.APP_EQUIP_TYPE_PC_SOFTWARE);
          if(ps.executeUpdate() != 1)
          {
            com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitPC: insert failed");
            addError("submitPC Unable to insert/update record");
          }
          ps.close();
          break;

        case mesConstants.APP_PAYSOL_INTERNET:
        default:
          break;
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
      addError("submitData: " + e.toString());
    }
  }

  private void submitMiscInfo(HttpServletRequest aReq, long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;

    System.out.println("Submitting misc info");
    try
    {
      //delete all miscchrg info from miscchrg -> insert new info
      qs.append("delete from miscchrg ");
      qs.append("where app_seq_num = ? ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong  (1, appSeqNum);

      ps.executeUpdate();
      ps.close();

      qs.setLength(0);
      qs.append("insert into miscchrg(misc_chrg_amount,app_seq_num,misc_code,MISC_CHRGBASIS_CODE,MISC_CHRGBASIS_DESCR) values (?,?,?,?,?)");

      ps = getPreparedStatement(qs.toString());


      if(!isBlank(this.chargebackFee))
      {
        ps.clearParameters();
        ps.setString(1, this.chargebackFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_CHARGEBACK);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "chargebackFee: insert failed");
          addError("chargebackFee: Unable to insert/update record");
        }
      }

      if(!isBlank(this.wirelessMonthlyFee))
      {
        ps.clearParameters();
        ps.setString(1, this.wirelessMonthlyFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_WIRELESS_SETUP);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "wirelessMonthlyFee: insert failed");
          addError("wirelessMonthlyFee: Unable to insert/update record");
        }
      }

      if(!isBlank(this.wirelessMonthlyFee))
      {
        ps.clearParameters();
        ps.setString(1, this.wirelessMonthlyFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_WIRELESS_MONTHLY);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "wirelessMonthlyFee: insert failed");
          addError("wirelessMonthlyFee: Unable to insert/update record");
        }
      }

      if(!isBlank(this.wirelessTransactionFee))
      {
        ps.clearParameters();
        ps.setString(1, this.wirelessTransactionFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_WIRELESS_TRANSACTION);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "wirelessTransactionFee: insert failed");
          addError("wirelessTransactionFee: Unable to insert/update record");
        }
      }


      if(!isBlank(this.retrievalRequestFee))
      {
        ps.clearParameters();
        ps.setString(1, this.retrievalRequestFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_RETRIEVAL);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "retrievalRequestFee: insert failed");
          addError("retrievalRequestFee: Unable to insert/update record");
        }
      }
      if(!isBlank(this.statementFee))
      {
        ps.clearParameters();
        ps.setString(1, this.statementFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_STATEMENT);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "statementFee: insert failed");
          addError("statementFee: Unable to insert/update record");
        }
      }
      
      if(!isBlank(this.internetReportFee))
      {
        ps.clearParameters();
        ps.setString(1, this.internetReportFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_INTERNET_REPORTING);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "internetReportFee: insert failed");
          addError("internetReportFee: Unable to insert/update record");
        }
      }


      if(!isBlank(this.applicationFee))
      {
        ps.clearParameters();
        ps.setString(1, this.applicationFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_APPLICATION);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
          addError("submitData4: Unable to insert/update record");
        }
      }
      if(!isBlank(this.terminalReprogrammingFee))
      {
        ps.clearParameters();
        ps.setString(1, this.terminalReprogrammingFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_TERMINAL_REPROGRAM);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "terminalReprogrammingFee: insert failed");
          addError("terminalReprogrammingFee: Unable to insert/update record");
        }
      }
    
    
      if(!isBlank(this.pinpadSwapFee))
      {
        ps.clearParameters();
        ps.setString(1, this.pinpadSwapFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_PINPAD_SWAP);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "pinpadSwapFee: insert failed");
          addError("pinpadSwapFee: Unable to insert/update record");
        }
      }

      if(!isBlank(this.achDepositFee))
      {
        ps.clearParameters();
        ps.setString(1, this.achDepositFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_ACH_DEPOSIT);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "achDepositFee: insert failed");
          addError("achDepositFee: Unable to insert/update record");
        }
      }
      if(!isBlank(this.debitAccessFee))
      {
        ps.clearParameters();
        ps.setString(1, this.debitAccessFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_DEBIT_ACCESS);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "debitAccessFee: insert failed");
          addError("debitAccessFee: Unable to insert/update record");
        }
      }
    
      if(!isBlank(this.internetStartupFee))
      {
        ps.clearParameters();
        ps.setString(1, this.internetStartupFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_INTERNET_STARTUP);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "internetStartupFee: insert failed");
          addError("internetStartupFee: Unable to insert/update record");
        }
      }
      if(!isBlank(this.gatewayFee))
      {
        ps.clearParameters();
        ps.setString(1, this.gatewayFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_GATEWAY_MONTHLY);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "gatewayFee: insert failed");
          addError("gatewayFee: Unable to insert/update record");
        }
      }

      System.out.println("submitting annual fee");
      if(!isBlank(this.annualFee))
      {
        ps.clearParameters();
        ps.setString(1, this.annualFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_ANNUAL);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "annualFee: insert failed");
          addError("annualFee: Unable to insert/update record");
        }
      }
      
      if(!isBlank(this.supplyFee) && this.supplyConfig.equals("CLUB"))
      {
        ps.clearParameters();
        ps.setString(1, this.supplyFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_SUPPLY);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "supplyFee: insert failed");
          addError("supplyFee: Unable to insert/update record");
        }
      }
      
      System.out.println("submitting vtSetupFee");
      if(!isBlank(vtSetupFee))
      {
        ps.clearParameters();
        ps.setString(1, vtSetupFee);
        ps.setLong(2, appSeqNum);
        ps.setInt(3, FEE_VT_SETUP);
        ps.setString(4, "");
        ps.setString(5, "");
        
        System.out.println("  adding vtSetupFee");
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "vtSetupFee: insert failed");
          addError("vtSetupFee: Unable to insert/update record");
        }
      }
    
      System.out.println("submitting vtMonthlyFee");
      if(!isBlank(vtMonthlyFee))
      {
        ps.clearParameters();
        ps.setString(1, vtMonthlyFee);
        ps.setLong(2, appSeqNum);
        ps.setInt(3, FEE_VT_MONTHLY);
        ps.setString(4, "");
        ps.setString(5, "");
        
        System.out.println("  adding vtMonthlyFee");
        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "vtMonthlyFee: insert failed");
          addError("vtMonthlyFee: Unable to insert/update record");
        }
      }
    
      System.out.println("got past vt fees");
      if(!isBlank(this.trainingFee))
      {
        ps.clearParameters();
        ps.setString(1, this.trainingFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_TRAINING);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "trainingFee: insert failed");
          addError("trainingFee: Unable to insert/update record");
        }
      }
      if(!isBlank(this.miscellaneous1Fee))
      {
        ps.clearParameters();
        ps.setString(1, this.miscellaneous1Fee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_MISCELLANEOUS1);
        ps.setString(4, this.miscellaneous1ChargeBasis);
        ps.setString(5, this.miscellaneous1FeeDesc);

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "miscellaneous1Fee: insert failed");
          addError("miscellaneous1Fee: Unable to insert/update record");
        }
      }
      if(!isBlank(this.miscellaneous2Fee))
      {
        ps.clearParameters();
        ps.setString(1, this.miscellaneous2Fee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_MISCELLANEOUS2);
        ps.setString(4, this.miscellaneous2ChargeBasis);
        ps.setString(5, this.miscellaneous2FeeDesc);

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "miscellaneous2Fee: insert failed");
          addError("miscellaneous2Fee: Unable to insert/update record");
        }
      }
      if(!isBlank(this.voiceAuthFee))
      {
        ps.clearParameters();
        ps.setString(1, this.voiceAuthFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_VOICE_AUTH);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "voiceAuthFee: insert failed");
          addError("voiceAuthFee: Unable to insert/update record");
        }
      }
      if(!isBlank(this.posPartnerMonthlyFee))
      {
        ps.clearParameters();
        ps.setString(1, this.posPartnerMonthlyFee);
        ps.setLong(2,   appSeqNum);
        ps.setInt(3,    FEE_POS_PARTNER_MONTHLY);
        ps.setString(4, "");
        ps.setString(5, "");

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "posPartnerMonthlyFee: insert failed");
          addError("posPartnerMonthlyFee: Unable to insert/update record");
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: " + e.toString());
      addError("submitData4: " + e.toString());
    }
  }

  private void submitCardInfo(HttpServletRequest aReq, long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;

    try
    {
      //delete all card info from tranchrg -> insert new info
      qs.append("delete from tranchrg ");
      qs.append("where app_seq_num = ? ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong  (1, appSeqNum);

      ps.executeUpdate();
      ps.close();

      qs.setLength(0);
      qs.append("Insert into tranchrg( ");
      qs.append("tranchrg_mmin_chrg,");
      qs.append("tranchrg_discrate_type,");
      qs.append("tranchrg_disc_rate,");
      qs.append("tranchrg_pass_thru,");
      qs.append("tranchrg_per_tran,");
      qs.append("tranchrg_float_disc_flag,");
      qs.append("app_seq_num,");
      qs.append("cardtype_code,");
      qs.append("tranchrg_per_item,");
      qs.append("tranchrg_per_capture,");
      qs.append("non_qualification_downgrade,");
      qs.append("tranchrg_interchangefee_type,");
      qs.append("tranchrg_per_auth) ");
      qs.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?) ");

      ps = getPreparedStatement(qs.toString());

      // The values for updating the data for Visa card.
      ps.setLong(7, appSeqNum);
      ps.setInt(8,mesConstants.APP_CT_VISA); //cardtype code
      ps.setInt(2,this.pricingScenario); //discount rate type

      switch(this.pricingScenario)
      {
        case mesConstants.TRANS_APP_BUCKET_ONLY_PLAN: // discount rate
          ps.setString(1,this.minMonthDiscount); //transaction min charge
          ps.setString(6,"N");//floating disc y or n
          ps.setDouble(3,this.transDiscountRate9); //disc rate
          ps.setNull(4,java.sql.Types.FLOAT ); //tranch_pass_thru
          ps.setString(5,"0"); //charge per transaction
          ps.setString(9,this.transSurcharge9); //charge per item.. used for surcharge
          ps.setNull(10,java.sql.Types.FLOAT );
          ps.setNull(11,java.sql.Types.FLOAT );
          ps.setString(12, "");
          ps.setDouble(13, vmcAuthFee);
          break;

        case mesConstants.TRANS_APP_BUCKET_WITH_PERITEM_PLAN: // disc + per item
          ps.setString(1,this.minMonthDiscount); //transaction min charge
          ps.setString(6,"N");
          ps.setDouble(3,this.transDiscountRate10);
          ps.setNull(4,java.sql.Types.FLOAT ); //tranch_pass_thru
          ps.setDouble(5,this.transPerItem10); //charge per tran
          ps.setString(9,this.transSurcharge10);
          ps.setNull(10,java.sql.Types.FLOAT );
          ps.setNull(11,java.sql.Types.FLOAT );
          ps.setString(12, "");
          ps.setDouble(13, vmcAuthFee);
          break;

        case mesConstants.TRANS_APP_DETAILED_STATEMENT_PLAN: // interchange pass through
          ps.setString(1,this.minMonthDiscount); //transaction min charge
          ps.setString(6,"N");
          ps.setDouble(3,this.transDiscountRate11);
          ps.setNull(4,java.sql.Types.FLOAT ); //tranch_pass_thru
          ps.setDouble(5,this.transPerItem11); //charge per transaction
          ps.setNull(9,java.sql.Types.FLOAT ); //tranch_pass_thru
          ps.setNull(10,java.sql.Types.FLOAT );
          ps.setNull(11,java.sql.Types.FLOAT );
          ps.setString(12, "");
          ps.setDouble(13, vmcAuthFee);
          break;

        case mesConstants.TRANS_APP_INTER_PASSTHRU_PLAN: // interchange pass through
          ps.setString(1,this.minMonthDiscount); //transaction min charge
          ps.setString(6,"N");
          ps.setDouble(3,this.transDiscountRate12);
          ps.setNull(4,java.sql.Types.FLOAT ); //tranch_pass_thru
          ps.setDouble(5,this.transPerItem12); //charge per transaction
          ps.setNull(9,java.sql.Types.FLOAT ); //tranch_pass_thru
          ps.setNull(10,java.sql.Types.FLOAT );
          ps.setNull(11,java.sql.Types.FLOAT );
          ps.setString(12, "");
          ps.setDouble(13, vmcAuthFee);
          break;

        default:
          break;
      }

      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData1: insert failed");
        addError("submitData1: Unable to insert/update record");
      }

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData1: " + e.toString());
      addError("submitData1: " + e.toString());
    }
    try
    {
      // The values for updating data for Master Card.
      ps.setInt(8,mesConstants.APP_CT_MC); //  change card type code, all else same
      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData2: insert failed");
        addError("submitData2: Unable to insert/update record");
      }
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData2: " + e.toString());
      addError("submitData2: " + e.toString());
    }

    try
    {
      qs.setLength(0);
      qs.append("Insert into tranchrg(tranchrg_per_tran,app_seq_num,cardtype_code) ");
      qs.append("values(?,?,?)");

      ps = getPreparedStatement(qs.toString());

      for ( i = 0; i < this.NUM_CARDS; i++ )
      {
         ps.clearParameters();
         if(this.cardSelectedFlag[i].equals("Y"))
         {
            ps.setString(1, this.cardFee[i]);
            ps.setLong(2, appSeqNum);
            ps.setInt(3, this.cardCodes[i]);

            if(ps.executeUpdate() != 1)
            {
              com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData3: insert failed");
              addError("submitData3: Unable to insert/update record");
            }
         }
      }
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData3: " + e.toString());
      addError("submitData3: " + e.toString());
    }
  }
  private void updateMerchEquipShip(long appSeqNum)
  {
    StringBuffer      qs = new StringBuffer("");
    PreparedStatement ps = null;
    ResultSet         rs = null;
    try
    {
      qs.append("delete from merchequipship ");
      qs.append("where app_seq_num = ? ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong  (1, appSeqNum);

      ps.executeUpdate();
      ps.close();

      qs.setLength(0);
      qs.append("select app_seq_num,equip_model,merchequip_equip_quantity,equiplendtype_code ");
      qs.append("from merchequipment where app_seq_num = ? and equiplendtype_code in (1,2) ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();

      ps.clearParameters();

      qs.setLength(0);
      qs.append("insert into merchequipship(app_seq_num,equip_model,merch_equipitem_num,equiplendtype_code) ");
      qs.append("values(?,?,?,?) ");

      ps = getPreparedStatement(qs.toString());

      while (rs.next())
      {
        for (int i = 1; i <= rs.getInt("merchequip_equip_quantity"); i++ )
        {
          ps.setLong(1,appSeqNum);
          ps.setString(2,rs.getString("equip_model"));
          ps.setInt(3,i);
          ps.setInt(4,rs.getInt("equiplendtype_code"));
          if(ps.executeUpdate() != 1)
          {
            com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "updateMerchEquipShip: insert failed");
            addError("updateMerchEquipShip: Unable to insert/update record");
          }
          ps.clearParameters();
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "updateMerchEquipShip: " + e.toString());
      addError("updateMerchEquipShip: " + e.toString());
    }
  }

  public void submitSiteInspectionInfo(long appSeqNum)
  {
    StringBuffer      qs    = new StringBuffer("");
    PreparedStatement ps    = null;
    ResultSet         rs    = null;

    try
    {
      qs.append("select app_seq_num from siteinspection where app_seq_num = ? ");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();
      qs.setLength(0);
      if(rs.next())
      {
        qs.append("update siteinspection set ");
        qs.append("siteinsp_comment = ?, ");
        qs.append("siteinsp_name_flag = ?, ");
        qs.append("siteinsp_inv_sign_flag = ?, ");
        qs.append("siteinsp_bus_hours_flag = ?, ");
        qs.append("siteinsp_inv_viewed_flag = ?, ");
        qs.append("siteinsp_inv_consistant_flag = ?, ");
        qs.append("siteinsp_vol_flag = ?, ");
        qs.append("siteinsp_full_flag = ?, ");
        qs.append("siteinsp_soft_flag = ?, ");
        qs.append("siteinsp_inv_street = ?, ");
        qs.append("siteinsp_inv_city = ?, ");
        qs.append("siteinsp_inv_state = ?, ");
        qs.append("siteinsp_inv_zip = ?, ");
        qs.append("siteinsp_full_street = ?, ");
        qs.append("siteinsp_full_city = ?, ");
        qs.append("siteinsp_full_state = ?, ");
        qs.append("siteinsp_full_zip = ?, ");
        qs.append("siteinsp_bus_street = ?, ");
        qs.append("siteinsp_bus_city = ?, ");
        qs.append("siteinsp_bus_state = ?, ");
        qs.append("siteinsp_bus_zip = ?, ");
        qs.append("siteinsp_inv_value = ?, ");
        qs.append("siteinsp_full_name = ?, ");
        qs.append("siteinsp_no_of_emp = ?, ");
        qs.append("siteinsp_bus_loc = ?, ");
        qs.append("siteinsp_bus_loc_comment = ?, ");
        qs.append("siteinsp_bus_address = ?, ");
        qs.append("siteinsp_soft_name = ?, ");
        qs.append("siteinsp_unique_fee_desc = ?, ");
        qs.append("siteinsp_supply_billing = ? ");
        qs.append("where app_seq_num = ? ");
      }
      else
      {
        qs.append("insert into siteinspection(");
        qs.append("siteinsp_comment, ");
        qs.append("siteinsp_name_flag, ");
        qs.append("siteinsp_inv_sign_flag, ");
        qs.append("siteinsp_bus_hours_flag, ");
        qs.append("siteinsp_inv_viewed_flag, ");
        qs.append("siteinsp_inv_consistant_flag, ");
        qs.append("siteinsp_vol_flag, ");
        qs.append("siteinsp_full_flag, ");
        qs.append("siteinsp_soft_flag, ");
        qs.append("siteinsp_inv_street, ");
        qs.append("siteinsp_inv_city, ");
        qs.append("siteinsp_inv_state, ");
        qs.append("siteinsp_inv_zip, ");
        qs.append("siteinsp_full_street, ");
        qs.append("siteinsp_full_city, ");
        qs.append("siteinsp_full_state, ");
        qs.append("siteinsp_full_zip, ");
        qs.append("siteinsp_bus_street, ");
        qs.append("siteinsp_bus_city, ");
        qs.append("siteinsp_bus_state, ");
        qs.append("siteinsp_bus_zip, ");
        qs.append("siteinsp_inv_value, ");
        qs.append("siteinsp_full_name, ");
        qs.append("siteinsp_no_of_emp, ");
        qs.append("siteinsp_bus_loc, ");
        qs.append("siteinsp_bus_loc_comment, ");
        qs.append("siteinsp_bus_address, ");
        qs.append("siteinsp_soft_name, ");
        qs.append("siteinsp_unique_fee_desc, ");
        qs.append("siteinsp_supply_billing, ");
        qs.append("app_seq_num) ");
        qs.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
      }
      rs.close();
      ps.close();

      ps = getPreparedStatement(qs.toString());
      ps.setString(1, this.comment);
      ps.setString(2, this.questionFlag[0]);
      ps.setString(3, this.questionFlag[1]);
      ps.setString(4, this.questionFlag[2]);
      ps.setString(5, this.questionFlag[3]);
      ps.setString(6, this.questionFlag[4]);
      //ps.setString(7, this.questionFlag[5]);
      ps.setString(7, "");

      if(this.houseFlag.equals("-1"))
        this.houseFlag = "N";
      ps.setString(8, this.houseFlag);

      if(this.softwareFlag.equals("-1"))
        this.softwareFlag = "N";
      ps.setString(9, this.softwareFlag);

      ps.setString(10, this.inventoryAddressStreet);
      ps.setString(11, this.inventoryAddressCity);
      ps.setString(12, this.inventoryAddressState);
      ps.setString(13, this.inventoryAddressZip);
      ps.setString(14, this.houseStreet);
      ps.setString(15, this.houseCity);
      ps.setString(16, this.houseState);
      ps.setString(17, this.houseZip);
      ps.setString(18, this.businessAddressStreet);
      ps.setString(19, this.businessAddressCity);
      ps.setString(20, this.businessAddressState);
      ps.setString(21, this.businessAddressZip);
      ps.setString(22, this.inventoryValue);
      ps.setString(23, this.houseName);
      ps.setString(24, this.numEmployees);
      ps.setString(25, this.businessLocation);
      ps.setString(26, this.businessLocationComment);
      ps.setString(27, this.businessAddress);
      ps.setString(28, this.softwareComment);
      ps.setString(29, "");
      ps.setString(30, this.supplyConfig);
      ps.setLong(31, appSeqNum);

      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitSiteInspectionInfo: update failed");
        addError("submitSiteInspectionInfo: Unable to insert/update record");
      }
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitSiteInspectionInfo: " + e.toString());
      addError("submitSiteInspectionInfo: " + e.toString());
    }
  }
}
