/*@lineinfo:filename=Card*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/Card.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/17/01 11:27a $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.setup;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class Card extends com.mes.database.SQLJConnection
{
  
  private   boolean   errorOccurred         = false;
  private   Vector    cardPlans             = new Vector();

  private   String    visaBet               = "0000";
  private   String    mcBet                 = "0000";
  private   String    private1Bet           = "0000";
  private   String    dinersBet             = "0000";
  private   String    discoverBet           = "0000";
  private   String    jcbBet                = "0000";
  private   String    amexBet               = "0000";
  private   String    debitBet              = "0000";

  //not currently used
  private   String    visaCashBet           = "0000";
  private   String    visaLargeBet          = "0000";
  private   String    mcCashBet             = "0000";
  private   String    mcLargeBet            = "0000";
  private   String    private2Bet           = "0000";
  private   String    private3Bet           = "0000";

  public Card(long appSeqNum)
  {
    getCards(appSeqNum);
  }
  
  public void setBet(String visa, String mc, String priv1, String diners, String discover, String jcb, String amex, String debit)
  {
    if(!isBlank(visa))
    {
      this.visaBet = visa; 
    }
    if(!isBlank(mc))
    {
      this.mcBet = mc; 
    }
    if(!isBlank(priv1))
    {
      this.private1Bet = priv1; 
    }
    if(!isBlank(diners))
    {
      this.dinersBet = diners; 
    }
    if(!isBlank(discover))
    {
      this.discoverBet = discover; 
    }
    if(!isBlank(jcb))
    {
      this.jcbBet = jcb; 
    }
    if(!isBlank(amex))
    {
      this.amexBet = amex; 
    }
    if(!isBlank(debit))
    {
      this.debitBet = debit; 
    }
  }

  private void getCards(long appSeqNum)  
  {
    ResultSetIterator     it    = null;
    ResultSet             rs    = null;
    CardPlan           cards    = null;
    int             numCards    = 0;

    try 
    {
      
      /*@lineinfo:generated-code*//*@lineinfo:107^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    billcard
//          where   app_seq_num = :appSeqNum
//          and     billcard_sr_number < 6
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    billcard\n        where   app_seq_num =  :1 \n        and     billcard_sr_number < 6";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.setup.Card",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.setup.Card",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:113^7*/
      
      rs = it.getResultSet();

      if(rs.next())
      {
        do
        {
          cards = new CardPlan(rs.getInt("cardtype_code"));
          cards.setCardPlanBet(getBet(rs.getInt("cardtype_code")));
          cards.setCardDiscType( (isBlank(rs.getString("billcard_disc_type"))      ? "" : rs.getString("billcard_disc_type")) );
          cards.setCardDiscRate( (isBlank(rs.getString("billcard_disc_rate"))      ? "" : rs.getString("billcard_disc_rate")) );
          cards.setCardDrtTable( (isBlank(rs.getString("billcard_drt_number"))     ? "" : rs.getString("billcard_drt_number")) );
          cards.setCardDiscMethod( (isBlank(rs.getString("billcard_disc_method"))  ? "" : rs.getString("billcard_disc_method")) );
          cards.setCardPerItem( (isBlank(rs.getString("billcard_peritem_fee"))     ? "" : rs.getString("billcard_peritem_fee")) );
          cards.setCardMinDisc("0000");
          cards.setCardFloorLim( (isBlank(rs.getString("billcard_flr_limit"))      ? "0000" : rs.getString("billcard_flr_limit")) );
          cardPlans.add(cards);
          numCards++;
        }while(rs.next());
      }
    
      while (numCards < 6)
      {
        cards = new CardPlan();
        cards.setCardMinDisc("0000");
        cards.setCardFloorLim("0000");
        cards.setCardPlanBet("0000");
        cardPlans.add(cards);
        numCards++;
      }
      
      if(numCards > 6)
      {
        errorOccurred = true;
      }


      try
      {
        it.close();
      }
      catch(Exception e)
      {}
    
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCardInfo: " + e.toString());
      errorOccurred = true;
    }
    finally
    {
      cleanUp();
    }
  }

  private String getBet(int cardType)
  {
    String result = "0000";

    switch(cardType)
    {
      case mesConstants.APP_CT_VISA:
        result = visaBet;
      break;
      case mesConstants.APP_CT_VISA_CASH_ADVANCE:
        result = visaCashBet;
      break;
      case mesConstants.APP_CT_VISA_LARGE_TICKET:
        result = visaLargeBet;
      break;
      case mesConstants.APP_CT_MC:
        result = mcBet;
      break;
      case mesConstants.APP_CT_MC_CASH_ADVANCE:
        result = mcCashBet;
      break;
      case mesConstants.APP_CT_MC_LARGE_TICKET:
        result = mcLargeBet;
      break;
      case mesConstants.APP_CT_PRIVATE_LABEL_1:
        result = private1Bet;
      break;
      case mesConstants.APP_CT_PRIVATE_LABEL_2:
        result = private2Bet;
      break;
      case mesConstants.APP_CT_PRIVATE_LABEL_3:
        result = private3Bet;
      break;
      case mesConstants.APP_CT_DINERS_CLUB:
        result = dinersBet;
      break;
      case mesConstants.APP_CT_DISCOVER:
        result = discoverBet;
      break;
      case mesConstants.APP_CT_JCB:
        result = jcbBet;
      break;
      case mesConstants.APP_CT_AMEX:
        result = amexBet;
      break;
      case mesConstants.APP_CT_DEBIT:
        result = debitBet;
      break;
    }

    return result;
  }
  
  public boolean isNumber(String test)
  {
    boolean pass = true;
    
    try
    {
      int num = Integer.parseInt(test);
    }
    catch(Exception e)
    {
      pass = false;
    }
    
    return pass;
  }
  
  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }

  public boolean didErrorOccur()
  {
    return errorOccurred;
  }

  public String getCardDiscType(int idx)
  {
    CardPlan cards = (CardPlan)cardPlans.elementAt(idx);
    return(cards.getCardDiscType());
  }
  public String getCardDiscRate(int idx)
  {
    CardPlan cards = (CardPlan)cardPlans.elementAt(idx);
    return(cards.getCardDiscRate());
  }
  public String getCardDrtTable(int idx)
  {
    CardPlan cards = (CardPlan)cardPlans.elementAt(idx);
    return(cards.getCardDrtTable());
  }
  public String getCardDiscMethod(int idx)
  {
    CardPlan cards = (CardPlan)cardPlans.elementAt(idx);
    return(cards.getCardDiscMethod());
  }
  public String getCardPerItem(int idx)
  {
    CardPlan cards = (CardPlan)cardPlans.elementAt(idx);
    return(cards.getCardPerItem());
  }
  public String getCardMinDisc(int idx)
  {
    CardPlan cards = (CardPlan)cardPlans.elementAt(idx);
    return(cards.getCardMinDisc());
  }
  public String getCardFloorLim(int idx)
  {
    CardPlan cards = (CardPlan)cardPlans.elementAt(idx);
    return(cards.getCardFloorLim());
  }
  public String getCardPlan(int idx)
  {
    CardPlan cards = (CardPlan)cardPlans.elementAt(idx);
    return(cards.getCardPlan());
  }
  public String getCardPlanBet(int idx)
  {
    CardPlan cards = (CardPlan)cardPlans.elementAt(idx);
    return(cards.getCardPlanBet());
  }
  public String getAuthOnlyOpt(int idx)
  {
    CardPlan cards = (CardPlan)cardPlans.elementAt(idx);
    return(cards.getAuthOnlyOpt());
  }
  public String getAchOption(int idx)
  {
    CardPlan cards = (CardPlan)cardPlans.elementAt(idx);
    return(cards.getAchOption());
  }
    
  public class CardPlan
  {
    private int    cardType       = 0;
    private String cardPlan       = "";
    private String discType       = "";
    private String discRate       = "";
    private String drtTable       = "";
    private String discMethod     = "";
    private String perItem        = "";
    private String minDisc        = "";
    private String floorLim       = "";
    private String cardBet        = "";

    private String achOption      = "";
    private String authOnlyOpt    = "";

    CardPlan(int cardType)
    {
      this.cardType = cardType;
      setCardPlan();
    }

    CardPlan()
    {
    }

    public void setCardDiscType(String discType)
    {
      this.discType = discType;  
    }
    public void setCardDiscRate(String discRate)
    {
      this.discRate = discRate;      
    }
    public void setCardDrtTable(String drtTable)
    {
      this.drtTable = drtTable;  
    }
    public void setCardDiscMethod(String discMethod)
    {
      this.discMethod = discMethod;
    }
    public void setCardPerItem(String perItem)
    {
      this.perItem = perItem;
    }
    public void setCardMinDisc(String minDisc)
    {
      this.minDisc = minDisc;
    }
    public void setCardFloorLim(String floorLim)
    {
      this.floorLim = floorLim;      
    }
    
    public void setCardPlanBet(String cardBet)
    {
      this.cardBet = cardBet;
    }

    public void setCardPlan()
    {
      switch(this.cardType)
      {
        case mesConstants.APP_CT_VISA:
          cardPlan    = "VS";
          authOnlyOpt = "N";
          achOption   = "C";
        break;
        case mesConstants.APP_CT_VISA_CASH_ADVANCE:
          cardPlan    = "V$";
          authOnlyOpt = "N";
          achOption   = "";
        break;
        case mesConstants.APP_CT_VISA_LARGE_TICKET:
          cardPlan    = "VL";
          authOnlyOpt = "N";
          achOption   = "";
        break;
        case mesConstants.APP_CT_MC:
          cardPlan    = "MC";
          authOnlyOpt = "N";
          achOption   = "C";
        break;
        case mesConstants.APP_CT_MC_CASH_ADVANCE:
          cardPlan    = "M$";
          authOnlyOpt = "N";
          achOption   = "";
        break;
        case mesConstants.APP_CT_MC_LARGE_TICKET:
          cardPlan    = "ML";
          authOnlyOpt = "N";
          achOption   = "";
        break;
        case mesConstants.APP_CT_PRIVATE_LABEL_1:
          cardPlan    = "P1";
          authOnlyOpt = "N";
          achOption   = "N";
        break;
        case mesConstants.APP_CT_PRIVATE_LABEL_2:
          cardPlan    = "P2";
          authOnlyOpt = "N";
          achOption   = "N";
        break;
        case mesConstants.APP_CT_PRIVATE_LABEL_3:
          cardPlan    = "P3";
          authOnlyOpt = "N";
          achOption   = "N";
        break;
        case mesConstants.APP_CT_DINERS_CLUB:
          cardPlan    = "DC";
          authOnlyOpt = "N";
          achOption   = "N";
        break;
        case mesConstants.APP_CT_DISCOVER:
          cardPlan    = "DS";
          authOnlyOpt = "N";
          achOption   = "N";
        break;
        case mesConstants.APP_CT_JCB:
          cardPlan    = "JC";
          authOnlyOpt = "N";
          achOption   = "N";
        break;
        case mesConstants.APP_CT_AMEX:
          cardPlan    = "AM";
          authOnlyOpt = "N";
          achOption   = "N";
        break;
        case mesConstants.APP_CT_DEBIT:
          cardPlan    = "DB";
          authOnlyOpt = "N";
          achOption   = "C";
        break;
      }
    }

    public String getAuthOnlyOpt()
    {
      return authOnlyOpt;
    }
    public String getAchOption()
    {
      return achOption;
    }

    public int getCardType()
    {
      return this.cardType;  
    }
    public String getCardDiscType()
    {
      return this.discType;  
    }
    public String getCardDiscRate()
    {
      return this.discRate;  
    }
    public String getCardDrtTable()
    {
      return this.drtTable;  
    }
    public String getCardDiscMethod()
    {
      return this.discMethod;  
    }
    public String getCardPerItem()
    {
      return this.perItem;  
    }
    public String getCardMinDisc()
    {
      return this.minDisc;  
    }
    public String getCardFloorLim()
    {
      return this.floorLim;  
    }
    public String getCardPlan()
    {
      return this.cardPlan;      
    }
    public String getCardPlanBet()
    {
      return this.cardBet;      
    }

  }
}/*@lineinfo:generated-code*/