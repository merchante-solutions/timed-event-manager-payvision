/*@lineinfo:filename=Charge*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/Charge.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 5/21/01 11:24a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.setup;

import java.sql.ResultSet;
import java.util.Vector;
import sqlj.runtime.ResultSetIterator;

public class Charge extends com.mes.database.SQLJConnection
{
  private   boolean   errorOccurred         = false;
  private   Vector    chargeRecs            = new Vector();

  public Charge(long appSeqNum)
  {
    getCharges(appSeqNum);
  }
  
  
  private void getCharges(long appSeqNum)  
  {
    ResultSetIterator     it    = null;
    ResultSet             rs    = null;
    ChargeRec        charges    = null;
    int              numCharges = 0;
    try 
    {

      /*@lineinfo:generated-code*//*@lineinfo:52^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    billother
//          where   app_seq_num = :appSeqNum
//          and     billoth_sr_num < 5
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    billother\n        where   app_seq_num =  :1 \n        and     billoth_sr_num < 5";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.setup.Charge",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.setup.Charge",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:58^7*/
      
      rs = it.getResultSet();

      if(rs.next())
      {
        do
        {
          charges = new ChargeRec();
          
          charges.setChargeDesc( (isBlank(rs.getString("billoth_charge_msg"))          ? "" : rs.getString("billoth_charge_msg")) );
          charges.setChargeType( (isBlank(rs.getString("billoth_charge_type"))         ? "" : rs.getString("billoth_charge_type")) );
          charges.setChargeStart( (isBlank(rs.getString("billoth_charge_startdate"))   ? "" : rs.getString("billoth_charge_startdate")) );
          charges.setChargeExpire( (isBlank(rs.getString("billoth_charge_expiredate")) ? "" : rs.getString("billoth_charge_expiredate")) );
          charges.setChargeFreq( (isBlank(rs.getString("billoth_charge_frq"))          ? "NNNNNNNNNNNN" : rs.getString("billoth_charge_frq")) );
          charges.setChargeNum( (isBlank(rs.getString("billoth_charge_times"))         ? "1" : rs.getString("billoth_charge_times")) );
          charges.setChargeMethod( (isBlank(rs.getString("billoth_charge_method"))     ? "D" : rs.getString("billoth_charge_method")) );
          charges.setChargeAmt( (isBlank(rs.getString("billoth_charge_amt"))           ? "" : rs.getString("billoth_charge_amt")) );
          charges.setChargeTax( (isBlank(rs.getString("billoth_charge_tax_ind"))       ? "" : rs.getString("billoth_charge_tax_ind")) );
          
          chargeRecs.add(charges);
          numCharges++;
        }while(rs.next());
      }
      
      while (numCharges < 5)
      {
        charges = new ChargeRec();
        chargeRecs.add(charges);
        numCharges++;
      }
      
      if(numCharges > 5)
      {
        errorOccurred = true;
      }

      try
      {
        it.close();
      }
      catch(Exception e)
      {}

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getChrg: " + e.toString());
      errorOccurred = true;
    }
    finally
    {
      cleanUp();
    }
  }
  
  public boolean isNumber(String test)
  {
    boolean pass = true;
    
    try
    {
      int num = Integer.parseInt(test);
    }
    catch(Exception e)
    {
      pass = false;
    }
    
    return pass;
  }
  
  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }

  public boolean didErrorOccur()
  {
    return errorOccurred;
  }

  public String getChargeDesc(int idx)
  {
    ChargeRec charges = (ChargeRec)chargeRecs.elementAt(idx);
    return(charges.getChargeDesc());
  }
  public String getChargeType(int idx)
  {
    ChargeRec charges = (ChargeRec)chargeRecs.elementAt(idx);
    return(charges.getChargeType());
  }
  public String getChargeStart(int idx)
  {
    ChargeRec charges = (ChargeRec)chargeRecs.elementAt(idx);
    return(charges.getChargeStart());
  }
  public String getChargeExpire(int idx)
  {
    ChargeRec charges = (ChargeRec)chargeRecs.elementAt(idx);
    return(charges.getChargeExpire());
  }
  public String getChargeFreq(int idx)
  {
    ChargeRec charges = (ChargeRec)chargeRecs.elementAt(idx);
    return(charges.getChargeFreq());
  }
  public String getChargeNum(int idx)
  {
    ChargeRec charges = (ChargeRec)chargeRecs.elementAt(idx);
    return(charges.getChargeNum());
  }
  public String getChargeMethod(int idx)
  {
    ChargeRec charges = (ChargeRec)chargeRecs.elementAt(idx);
    return(charges.getChargeMethod());
  }
  public String getChargeAmt(int idx)
  {
    ChargeRec charges = (ChargeRec)chargeRecs.elementAt(idx);
    return(charges.getChargeAmt());
  }
  public String getChargeTax(int idx)
  {
    ChargeRec charges = (ChargeRec)chargeRecs.elementAt(idx);
    return(charges.getChargeTax());
  }

  public class ChargeRec
  {
    private String chargeDesc     = "";
    private String chargeType     = "";
    private String chargeStart    = "";
    private String chargeExpire   = "";
    private String chargeFreq     = "";
    private String chargeNum      = "";
    private String chargeMethod   = "";
    private String chargeAmt      = "";
    private String chargeTax      = "";

    ChargeRec()
    {}

    public void setChargeDesc(String chargeDesc)
    {
      this.chargeDesc = chargeDesc;  
    }
    public void setChargeType(String chargeType)
    {
      this.chargeType = chargeType;      
    }
    public void setChargeStart(String chargeStart)
    {
      this.chargeStart = chargeStart;  
    }
    public void setChargeExpire(String chargeExpire)
    {
      this.chargeExpire = chargeExpire;
    }
    public void setChargeFreq(String chargeFreq)
    {
      this.chargeFreq = chargeFreq;
    }
    public void setChargeNum(String chargeNum)
    {
      this.chargeNum = chargeNum;
    }
    public void setChargeAmt(String chargeAmt)
    {
      this.chargeAmt = chargeAmt;      
    }
    public void setChargeTax(String chargeTax)
    {
      this.chargeTax = chargeTax;
    }
    public void setChargeMethod(String chargeMethod)
    {
      this.chargeMethod = chargeMethod;      
    }

    public String getChargeDesc()
    {
      return this.chargeDesc;  
    }
    public String getChargeType()
    {
      return this.chargeType;      
    }
    public String getChargeStart()
    {
      return this.chargeStart;  
    }
    public String getChargeExpire()
    {
      return this.chargeExpire;
    }
    public String getChargeFreq()
    {
      return this.chargeFreq;
    }
    public String getChargeNum()
    {
      return this.chargeNum;
    }
    public String getChargeAmt()
    {
      return this.chargeAmt;      
    }
    public String getChargeTax()
    {
      return this.chargeTax;
    }
    public String getChargeMethod()
    {
      return this.chargeMethod;      
    }

  }

}/*@lineinfo:generated-code*/