/*@lineinfo:filename=EquipmentBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/EquipmentBean.sqlj $

  Description:


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-09-19 11:13:32 -0700 (Wed, 19 Sep 2007) $
  Version            : $Revision: 14159 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.setup;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.screens.SequenceDataBean;
import sqlj.runtime.ResultSetIterator;

public class EquipmentBean extends SequenceDataBean
{

  private final static String   NO_PRINTER                      = "No Printer";

  public  final static String   YES                             = "Y";
  public  final static String   NO                              = "N";

  public  final static String   TRANS_PHONE_TRAINING            = "T";
  public  final static String   MES_PHONE_TRAINING              = "M";
  public  final static String   OTHER_PHONE_TRAINING            = "O";

  public  final static String   PHONE_TRAINING_MES_HELPDESK     = "M";
  public  final static String   PHONE_TRAINING_SALESBANK_REP    = "R";
  public  final static String   PHONE_TRAINING_NO_TRAINING      = "N";

  public  final static String   DEPLOY_WITH_TRAINING            = "1";
  public  final static String   DEPLOY_WITHOUT_TRAINING         = "2";
  public  final static String   TRAINING_ONLY                   = "3";
  public  final static String   CONVERSION                      = "4";

  public  final static int      FRAUD_LAST4                     = 1;
  public  final static int      FRAUD_FULL                      = 2;

  public  final static int      NUM_OF_EQUIP                    = 147;
  public  final static int      NUM_OF_MODS                     = 74;
  public  final static int      NUM_OF_TYPES                    = 7;
  public  final static int      NUM_OF_APPS                     = 18;

  //order type of equipment is entered into need vector
  public  final static int      TERMINAL_PRINTER                = 0;
  public  final static int      TERMINAL_PINPAD                 = 1;
  public  final static int      TERMINALS                       = 2;
  public  final static int      PRINTERS                        = 3;
  public  final static int      PINPADS                         = 4;
  public  final static int      PERIPHERALS                     = 5;
  public  final static int      IMPRINTERS                      = 6;

  public  final static int EQUIP_TYPE_IMPRINTER_PLATE           = 9;

  public  final static String   BUSINESS_ADD_TYPE               = "1";
  public  final static String   MAILING_ADD_TYPE                = "2";
  public  final static String   EQUIP_ADD_TYPE                  = "3";
  public  final static String   INVALID_ADD_TYPE                = "-1";

  private final static Integer  RETAIL                          = 1;
  private final static Integer  RESTAURANT                      = 2;
  private final static Integer  FINE_DINING                     = 3;
  private final static Integer  HOTEL                           = 4;
  private final static Integer  CASH_ADVANCE                    = 10;
  private final static Integer  MOTEL                           = 11;
  private final static Integer  DEBIT_TRACK1                    = 8;
  private final static Integer  DEBIT_TRACK2                    = 9;
  private final static Integer  RETAIL_TRACK1                   = 12;
  private final static Integer  RETAIL_TRACK2                   = 13;
  private final static Integer  LAN                             = 14;
  private final static Integer  RESTAURANT_TRACK1               = 15;
  private final static Integer  RESTAURANT_TRACK2               = 16;
  private final static Integer  MOTEL_TRACK1                    = 17;
  private final static Integer  MOTEL_TRACK2                    = 18;
  private final static Integer  CASH_ADVANCE_TRACK2_ONLY        = 19;
  private final static Integer  RETAIL_NO_DEBIT_CDPD_ONLY       = 20;
  private final static Integer  RETAIL_CDPD_WITH_DEBIT          = 21;

  private final static int      BUY                             = mesConstants.APP_EQUIP_PURCHASE;
  private final static int      RENT                            = mesConstants.APP_EQUIP_RENT;
  private final static int      OWN                             = mesConstants.APP_EQUIP_OWNED;
  private final static int      LEASE                           = mesConstants.APP_EQUIP_LEASE;
  private final static int      BUY_REFURB                      = mesConstants.APP_EQUIP_BUY_REFURBISHED;

  private String[] equipDesc                      = new String[NUM_OF_EQUIP];
  private String[] equipDesc2                     = new String[NUM_OF_MODS];
  private String[] equipMod                       = new String[NUM_OF_MODS];
  private String[] appDesc                        = new String[NUM_OF_APPS];

  public String[] terminalPrinterNeed             = new String[27];
  public String[] terminalPinpadNeed              = new String[2];
  public String[] terminalNeed                    = new String[11];
  public String[] printersNeed                    = new String[3];
  public String[] pinpadsNeed                     = new String[5];
  public String[] imprintersNeed                  = new String[1];
  public String[] peripheralsNeed                 = new String[3];

  public String[] terminalPrinterNeedDisc         = new String[15];
  public String[] terminalPinpadNeedDisc          = new String[1];
  public String[] terminalNeedDisc                = new String[11];
  public String[] printersNeedDisc                = new String[2];
  public String[] peripheralsNeedDisc             = new String[1];

  public String[] terminalPrinterNeedCbt          = new String[43];
  public String[] terminalNeedCbt                 = new String[22];
  public String[] printersNeedCbt                 = new String[5];
  public String[] pinpadsNeedCbt                  = new String[6];

  public String[] terminalPrinterNeedBbt          = new String[21];
  public String[] terminalNeedBbt                 = new String[15];
  public String[] printersNeedBbt                 = new String[3];
  public String[] pinpadsNeedBbt                  = new String[2];
  public String[] imprintersNeedBbt               = new String[2];

  public String[] terminalPrinterOwn              = new String[45];
  public String[] terminalPinpadOwn               = new String[5];
  public String[] terminalOwn                     = new String[33];
  public String[] printersOwn                     = new String[16];
  public String[] pinpadsOwn                      = new String[7];
  public String[] imprintersOwn                   = new String[1];
  public String[] peripheralsOwn                  = new String[3];

  private String[] equipType                      = new String[NUM_OF_TYPES];
  private String[] equipTypeCode                  = new String[NUM_OF_TYPES];
  //variables store form data
  private String[] equipNeeded                    = new String[NUM_OF_TYPES];
  private String[] equipOwned                     = new String[NUM_OF_TYPES];
  private String[] equipNumRent                   = new String[NUM_OF_TYPES];
  private String[] equipNumBuy                    = new String[NUM_OF_TYPES];
  private String[] equipNumRefurb                 = new String[NUM_OF_TYPES];
  private String[] equipNumLease                  = new String[NUM_OF_TYPES];
  private String[] equipNumOwn                    = new String[NUM_OF_TYPES];

  private String[] equipAmtRent                   = new String[NUM_OF_TYPES];
  private String[] equipAmtBuy                    = new String[NUM_OF_TYPES];
  private String[] equipAmtRefurb                 = new String[NUM_OF_TYPES];
  private String[] equipAmtOwn                    = new String[NUM_OF_TYPES];
  private String[] equipAmtLease                  = new String[NUM_OF_TYPES];


  public Vector need                              = new Vector();
  public Vector own                               = new Vector();

  private String accessCodeFlag                   = "N";
  private String accessCodeComment                = "-1";
  private String autoBatchFlag                    = "N";
  private String autoBatchHhComment               = "-1";
  private String autoBatchMmComment               = "-1";
  private String autoBatchAmPmComment             = "-1";

  private String headerLine4Flag                  = "N";
  private String headerLine4Comment               = "-1";

  private String headerLine5Flag                  = "N";
  private String headerLine5Comment               = "-1";

  private String footerFlag                       = "N";
  private String footerComment                    = "-1";

  private String resetReferenceFlag               = "N";
  private String invoicePromptFlag                = "N";
  private String fraudControlFlag                 = "Y";
  private String fraudControlComment              = "-1";

  private String avsOnFlag                        = "Y";
  private String cvv2Flag                         = "N";
  private String cashBackFlag                     = "N";
  private String debitSurchargeFlag               = "N";

  private String purchasingCardFlag               = "Y";
  private String truncationCardFlag               = "N";

  private String passwordProtectFlag              = "N";
  private String phoneTrainingFlag                = "N";
  private String terminalReminderFlag             = "N";
  private String terminalReminderHhComment        = "-1";
  private String terminalReminderMmComment        = "-1";
  private String terminalReminderAmPmComment      = "-1";
  private String equipmentComment                 = "-1";
  private String tipOptionFlag                    = "N";
  private String clerkEnabledFlag                 = "N";

  private String wirelessEsnFlag                  = "N";
  private String wirelessEsnNumber                = "-1";
  private String wirelessLliFlag                  = "N";
  private String wirelessLliNumber                = "-1";


  private String phoneTraining                    = "";
  private String cashBackLimit                    = "";
  private String debitSurchargeAmount             = "";
  private String terminalInsurance                = "N";
  private String terminalTrack                    = "";
  private String terminalPrinter                  = "";
  private String customerServicePhone             = "";

  private String useAddress                       = INVALID_ADD_TYPE;
  private String shippingName                     = "";
  private String shippingContactName              = "";
  private String contactName                      = "";
  private String shippingAddress1                 = "";
  private String shippingAddress2                 = "";
  private String shippingCity                     = "";
  private String shippingState                    = "-1";
  private String shippingZip                      = "";
  private String shippingPhone                    = "";
  private String shippingMethod                   = "";
  private String shippingComment                  = "";
  private boolean dialPay                         = false;
  private boolean noTraining                      = false;
  private boolean retail                          = false;
  private boolean moto                            = false;
  private boolean cashAdvance                     = false;
  private boolean recalculate                     = false;
  private boolean virtualTerminal                 = false;

  private String  businessPhone                   = "";
  private String  businessAddress                 = "";
  private String  businessName                    = "";
  private String  businessReturnPolicy            = "";

  private int     imprinterPlates                 = 0;
  private String  longImpPlateQty                 = "";
  private String  longImpPlateDesc                = "";
  private String  shortImpPlateQty                = "";
  private String  shortImpPlateDesc               = "";
  private int     appType                         = 0;

  private int     ipTerminal                      = mesConstants.TERM_COMM_TYPE_DIAL;

  //leasing info...
  private String  leaseCompany                    = "";
  private String  leaseActNum                     = "";
  private String  leaseAuthNum                    = "";
  private String  leaseMonths                     = "";
  private String  fundingAmt                      = "";
  private String  leaseAmt                        = "";

  private boolean leasingEquip                    = false;
  private boolean noPrinterNeed                   = false;
  private boolean noPrinterOwn                    = false;

  private Vector  monthOptions                    = new Vector();
  private Vector  companyOptions                  = new Vector();
  private Vector  companyOptionCode               = new Vector();
  public  Vector  method                          = new Vector();
  public  Vector  methodDesc                      = new Vector();

  private EquipmentHelperBean ehb                 = new EquipmentHelperBean();

  public EquipmentBean()
  {
    equipType[0]                = "Terminal/Printer Sets";
    equipType[1]                = "Terminal/Printer/Pinpad Sets";
    equipType[2]                = "Terminals";
    equipType[3]                = "Printers";
    equipType[4]                = "Pinpads";
    equipType[5]                = "Peripherals";
    equipType[6]                = "Imprinters";

    equipTypeCode[0]            = Integer.toString(mesConstants.APP_EQUIP_TYPE_TERM_PRINTER);       //"5";
    equipTypeCode[1]            = Integer.toString(mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD);  //"6";
    equipTypeCode[2]            = Integer.toString(mesConstants.APP_EQUIP_TYPE_TERMINAL);           //"1";
    equipTypeCode[3]            = Integer.toString(mesConstants.APP_EQUIP_TYPE_PRINTER);            //"2";
    equipTypeCode[4]            = Integer.toString(mesConstants.APP_EQUIP_TYPE_PINPAD);             //"3";
    equipTypeCode[5]            = Integer.toString(mesConstants.APP_EQUIP_TYPE_PERIPHERAL);         //"0";
    equipTypeCode[6]            = Integer.toString(mesConstants.APP_EQUIP_TYPE_IMPRINTER);          //"4";

    for(int i=0; i<this.NUM_OF_TYPES; i++)
    {
      equipOwned[i]     = "-1";
      equipNumOwn[i]    = "-1";
      equipAmtOwn[i]    = "-1";

      equipNeeded[i]    = "-1";
      equipNumRent[i]   = "-1";
      equipNumBuy[i]    = "-1";
      equipNumRefurb[i] = "-1";
      equipNumLease[i]  = "-1";
      equipAmtRent[i]   = "-1";
      equipAmtBuy[i]    = "-1";
      equipAmtRefurb[i] = "-1";
      equipAmtLease[i]  = "-1";
    }


    monthOptions.add("12");
    monthOptions.add("24");
    monthOptions.add("36");
    monthOptions.add("48");


    try
    {
      connect();

      ResultSetIterator it    = null;
      ResultSet         rs    = null;

      /*@lineinfo:generated-code*//*@lineinfo:327^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    equip_lease_company
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    equip_lease_company";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.setup.EquipmentBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.setup.EquipmentBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:331^7*/
      rs = it.getResultSet();

      while (rs.next())
      {
        companyOptions.add(rs.getString("lease_company_desc"));
        companyOptionCode.add(rs.getString("lease_company_code"));
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
    }
    finally
    {
      cleanUp();
    }
  }

  public String getNeedDesc(String needModel)
  {
    String result = "";

    try
    {
      result = ehb.getNeedDesc(needModel);
    }
    catch(Exception e)
    {
    }
    return result;
  }

  public String getOwnDesc(String ownModel)
  {
    String result = "";

    try
    {
      result = ehb.getOwnDesc(ownModel);
    }
    catch(Exception e)
    {
    }
    return result;
  }


  public void getApplicationType(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select app_type        ");
      qs.append("from application       ");
      qs.append("where app_seq_num = ?  ");

      ps  = getPreparedStatement(qs.toString());

      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        this.appType = rs.getInt("app_type");
        ehb.setNeededEquipmentList(this.appType);
      }
      else//use mes list cause apptype could not be determined
      {
        ehb.setNeededEquipmentList(mesConstants.APP_TYPE_MES);
      }

      rs.close();
      ps.close();

      if(ehb.isNeedSuccessFull())
      {
        need = new Vector();

        //@System.out.println("equipment helper bean was successful.. getting need arrays..");

        need.add(ehb.getNeedTermPrints());
        need.add(ehb.getNeedTermPrintPins());
        need.add(ehb.getNeedTerminals());
        need.add(ehb.getNeedPrinters());
        need.add(ehb.getNeedPinpads());
        need.add(ehb.getNeedPeripherals());
        need.add(ehb.getNeedImprinters());
      }
      else
      {
        addError("Couldn't get Needed Equipment list");
      }

      if(ehb.isOwnSuccessFull())
      {
        own = new Vector();

        //@System.out.println("equipment helper bean was successful.. getting own arrays..");

        own.add(ehb.getOwnTermPrints());
        own.add(ehb.getOwnTermPrintPins());
        own.add(ehb.getOwnTerminals());
        own.add(ehb.getOwnPrinters());
        own.add(ehb.getOwnPinpads());
        own.add(ehb.getOwnPeripherals());
        own.add(ehb.getOwnImprinters());
      }
      else
      {
        addError("Couldn't get Owned Equipment list");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAppType: " + e.toString());
      addError("getAppType: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  public void updateData(long appSeqNum)
  {
    getApplicationType(appSeqNum);
  }


  private String getDataFromRequest(String reqData)
  {
    String result = "";

    if(!isBlank(reqData) && !reqData.equals("-1"))
    {
      result = reqData;
    }
    else
    {
      result = "-1";
    }

    return result;
  }



  public void updateData(HttpServletRequest aReq)
  {
    for(int i=0; i<this.NUM_OF_TYPES; i++)
    {

      this.equipNeeded[i]     = getDataFromRequest(aReq.getParameter("equipNeeded"    + i));
      this.equipNumRent[i]    = getDataFromRequest(aReq.getParameter("equipNumRent"   + i));
      this.equipNumBuy[i]     = getDataFromRequest(aReq.getParameter("equipNumBuy"    + i));
      this.equipNumRefurb[i]  = getDataFromRequest(aReq.getParameter("equipNumRefurb" + i));
      this.equipOwned[i]      = getDataFromRequest(aReq.getParameter("equipOwned"     + i));
      this.equipNumOwn[i]     = getDataFromRequest(aReq.getParameter("equipNumOwn"    + i));
      this.equipNumLease[i]   = getDataFromRequest(aReq.getParameter("equipNumLease"  + i));

      if(!isBlank(aReq.getParameter("equipNumLease" + i)))
      {
        this.leasingEquip = true;
      }

    }
  }


  private String intGrtZeroCheck(String temp)
  {
    String result = temp;

    try
    {
      int tempInt = Integer.parseInt(temp);

      if(tempInt <= 0)
      {
        result = "-1";
      }
    }
    catch(Exception e)
    {
      result = "-1";
    }

    return result;
  }

  private boolean intGrtOneCheck(String temp)
  {
    boolean result = false;

    try
    {
      int tempInt = Integer.parseInt(temp);

      if(tempInt > 1)
      {
        result = true;
      }
    }
    catch(Exception e)
    {
      result = false;
    }

    return result;
  }

  public boolean validate( long appSeqNum )
  {
    String    equipModel      = null;
    int       recCount        = 0;
    int       supported       = 0;
    int       temp            = 0;

    try
    {
      connect();

      getPaySolOption(appSeqNum);

      for(int i=0; i<this.NUM_OF_TYPES; i++)
      {
        this.equipNumRent[i]    = intGrtZeroCheck(this.equipNumRent[i]);
        this.equipNumBuy[i]     = intGrtZeroCheck(this.equipNumBuy[i]);
        this.equipNumRefurb[i]  = intGrtZeroCheck(this.equipNumRefurb[i]);
        this.equipNumLease[i]   = intGrtZeroCheck(this.equipNumLease[i]);
        this.equipNumOwn[i]     = intGrtZeroCheck(this.equipNumOwn[i]);

        switch(i)
        {
          case TERMINAL_PRINTER:
          case TERMINAL_PINPAD:
          case TERMINALS:
            for ( int modelType = 0; modelType < 2; ++modelType )
            {
              switch( modelType )
              {
                case 0:   // owned
                  if( isBlank(this.equipOwned[i]) ||
                      this.equipOwned[i].equals("-1") )
                  {
                    continue;
                  }
                  equipModel = ehb.decodeModel(this.equipOwned[i]);
                  break;

                case 1:   // needed
                  if( isBlank(this.equipNeeded[i]) ||
                      this.equipNeeded[i].equals("-1") )
                  {
                    continue;
                  }
                  equipModel = ehb.decodeModel(this.equipNeeded[i]);
                  break;
              }

              System.out.println(modelType + " " + equipModel);//@

              /*@lineinfo:generated-code*//*@lineinfo:600^15*/

//  ************************************************************
//  #sql [Ctx] { select  count(mpo.app_seq_num) 
//                  from    merchpayoption    mpo
//                  where   mpo.app_seq_num = :appSeqNum and
//                          mpo.cardtype_code = :mesConstants.APP_CT_CHECK_AUTH and
//                          mpo.merchpo_provider_name in
//                          (
//                            'CertegyWelcomeCheck',
//                            'CertegyECheck'
//                          )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mpo.app_seq_num)  \n                from    merchpayoption    mpo\n                where   mpo.app_seq_num =  :1  and\n                        mpo.cardtype_code =  :2  and\n                        mpo.merchpo_provider_name in\n                        (\n                          'CertegyWelcomeCheck',\n                          'CertegyECheck'\n                        )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.setup.EquipmentBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_CHECK_AUTH);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:611^15*/

              // Certegy programs only support certain equipment
              if ( recCount > 0 )
              {
                supported = 0;

                /*@lineinfo:generated-code*//*@lineinfo:618^17*/

//  ************************************************************
//  #sql [Ctx] { select  decode( nvl(eq.certegy_check_supported,'N'),
//                                    'Y', 1, 0 )
//                    
//                    from    equipment   eq
//                    where   eq.equip_model = :equipModel
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode( nvl(eq.certegy_check_supported,'N'),\n                                  'Y', 1, 0 )\n                   \n                  from    equipment   eq\n                  where   eq.equip_model =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.setup.EquipmentBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,equipModel);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   supported = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:625^17*/

                if ( supported == 0 )
                {
                  addError("Check provider does not support this type of equipment");
                }
              }

              // check for valutec
              /*@lineinfo:generated-code*//*@lineinfo:634^15*/

//  ************************************************************
//  #sql [Ctx] { select  count(mpo.app_seq_num) 
//                  from    merchpayoption    mpo
//                  where   mpo.app_seq_num = :appSeqNum and
//                          mpo.cardtype_code = :mesConstants.APP_CT_VALUTEC_GIFT_CARD
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mpo.app_seq_num)  \n                from    merchpayoption    mpo\n                where   mpo.app_seq_num =  :1  and\n                        mpo.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.setup.EquipmentBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VALUTEC_GIFT_CARD);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:640^15*/

              // valutec only supports certain equipment
              if ( recCount > 0 )
              {
                supported = 0;

                /*@lineinfo:generated-code*//*@lineinfo:647^17*/

//  ************************************************************
//  #sql [Ctx] { select  decode( nvl(eq.valutec_gift_card_supported,'N'),
//                                    'Y', 1, 0 )
//                    
//                    from    equipment   eq
//                    where   eq.equip_model = :equipModel
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode( nvl(eq.valutec_gift_card_supported,'N'),\n                                  'Y', 1, 0 )\n                   \n                  from    equipment   eq\n                  where   eq.equip_model =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.setup.EquipmentBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,equipModel);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   supported = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:654^17*/

                if ( supported == 0 )
                {
                  addError("Valutec Gift Card program does not support this type of equipment");
                }
              }
            }
            break;
        }

        if(i == IMPRINTERS && appType == mesConstants.APP_TYPE_DISCOVER && isDialPay() && intGrtOneCheck(this.equipNumBuy[i]))
        {
          addError("You can only purchase one imprinter");
        }

        if(i == PRINTERS && isNoPrinterNeed() && (!this.equipNeeded[i].equals("-1") || !this.equipNumRent[i].equals("-1") || !this.equipNumBuy[i].equals("-1") || !this.equipNumRefurb[i].equals("-1") || !this.equipNumLease[i].equals("-1")))
        {
          addError("If checking No Printer under equipment needed, no other printer selection or number printers needed is allowed");
        }
        else
        {
          if(this.equipNeeded[i].equals("NOPRINTER"))
          {
            this.equipNumRent[i]  = "-1";
            this.equipNumLease[i] = "-1";
            this.equipNumBuy[i]   = "0";
          }

          if(!this.equipNeeded[i].equals("-1") && this.equipNumRent[i].equals("-1") && this.equipNumBuy[i].equals("-1") && this.equipNumRefurb[i].equals("-1") && this.equipNumLease[i].equals("-1") )
          {
            addError("Please indicate how many " + getNeedDesc(this.equipNeeded[i]) + "'s you would like to Buy, Rent, Lease, or Buy/Refurbished");
          }

          if(this.equipNeeded[i].equals("-1") && (!this.equipNumRent[i].equals("-1") || !this.equipNumBuy[i].equals("-1") || !this.equipNumRefurb[i].equals("-1") || !this.equipNumLease[i].equals("-1")))
          {
            addError("Please select an item from " + this.equipType[i] + " or delete the quantity you would like to Buy, Rent, Lease, or Buy/Refurbished");
          }
        }

        if(i == PRINTERS && isNoPrinterOwn() && (!this.equipOwned[i].equals("-1") || !this.equipNumOwn[i].equals("-1")))
        {
          addError("If checking No Printer under equipment owned, no other printer selection or number printers owned is allowed");
        }
        else
        {
          if(this.equipOwned[i].equals("NOPRINTER"))
          {
            this.equipNumOwn[i]   = "0";
          }

          if(!this.equipOwned[i].equals("-1") && this.equipNumOwn[i].equals("-1"))
          {

            addError("Please indicate how many " + getOwnDesc(this.equipOwned[i]) + "'s you already Own");
          }
          if(this.equipOwned[i].equals("-1") && !this.equipNumOwn[i].equals("-1"))
          {
            addError("Please select an item from " + this.equipType[i] + " or delete the quantity you already Own");
          }
        }
      }

      if(!isBlank(this.longImpPlateQty))
      {
        try
        {
          int tempInty = Integer.parseInt(this.longImpPlateQty);
          if(isBlank(this.longImpPlateDesc))
          {
            addError("Please provide the message you would like on your long imprinter plates");
          }
        }
         catch(Exception e)
        {
          addError("Please provide a valid quantity of long imprinter plates you would like to order");
        }
      }
      else if(!isBlank(this.longImpPlateDesc))
      {
          addError("Please provide the quantity of long imprinter plates you would like to order");
      }

      if(!isBlank(this.shortImpPlateQty))
      {
        try
        {
          int tempInty = Integer.parseInt(this.shortImpPlateQty);
          if(isBlank(this.shortImpPlateDesc))
          {
            addError("Please provide the message you would like on your short imprinter plates");
          }
        }
        catch(Exception e)
        {
          addError("Please provide a valid quantity of short imprinter plates you would like to order");
        }
      }
      else if(!isBlank(this.shortImpPlateDesc))
      {
          addError("Please provide the quantity of short imprinter plates you would like to order");
      }


      if(isLeasingEquip() && appType != mesConstants.APP_TYPE_BBT && appType != mesConstants.APP_TYPE_DISCOVER && appType != mesConstants.APP_TYPE_DISCOVER_IMS && appType != mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL)
      {
        //System.out.println("checking lease stuff apptype = " + appType);
        if(isBlank(this.leaseCompany))
        {
          addError("If leasing equipment, please select a Lease Company");
        }
        if(isBlank(this.leaseActNum))
        {
          addError("If leasing equipment, please provide a valid Lease Account Number");
        }
        if(isBlank(leaseAuthNum))
        {
          addError("If leasing equipment, please provide a valid Lease Authorization Number");
        }
        if(isBlank(this.leaseMonths))
        {
          addError("If leasing equipment, please select the Number of Months of the lease");
        }

        //not required for now
        if(!isBlank(this.leaseAmt) && !isValidAmount(this.leaseAmt))
        {
          addError("If leasing equipment, please provide a valid Monthly Lease Amount");
        }

        if(!isBlank(this.fundingAmt) && !isValidAmount(this.fundingAmt))
        {
          addError("If leasing equipment, please provide a valid Lease Funding Amount");
        }

      }


      if(this.accessCodeFlag.equals("Y") && this.accessCodeComment.equals("-1"))
      {
        addError("Please provide a valid Access Code ");
      }
      else if(!this.accessCodeFlag.equals("Y") && !this.accessCodeComment.equals("-1"))
      {
        addError("Please check the Access Code Check Box or delete the entered Access Code");
      }
      if(this.autoBatchFlag.equals("Y") && (this.autoBatchHhComment.equals("-1") || this.autoBatchMmComment.equals("-1") || this.autoBatchAmPmComment.equals("-1")))
      {
        addError("Please provide the hour(HH), minute(MM), and AM/PM of when the auto batch close will occur");
      }
      else if(!this.autoBatchFlag.equals("Y") && (!this.autoBatchHhComment.equals("-1") || !this.autoBatchMmComment.equals("-1")))
      {
        addError("Please check the Auto Batch Close check box or delete the entered hour(HH) and minute(MM)");
      }
      else if(this.autoBatchFlag.equals("Y") && !this.autoBatchHhComment.equals("-1") && !this.autoBatchMmComment.equals("-1") && !this.autoBatchAmPmComment.equals("-1") && this.autoBatchMmComment.length() != 2)
      {
        addError("Auto Batch Close Minute(MM) must be two digits long");
      }
      if(this.headerLine4Flag.equals("Y")  && this.headerLine4Comment.equals("-1"))
      {
        addError("Please provide Line 4 for the Receipt Header");
      }
      else if(!this.headerLine4Flag.equals("Y") && !this.headerLine4Comment.equals("-1"))
      {
        addError("Please check the Receipt Header Line 4 check box or delete the entered Line 4");
      }
      if(this.headerLine5Flag.equals("Y")  && this.headerLine5Comment.equals("-1"))
      {
        addError("Please provide Line 5 for the Receipt Header");
      }
      else if(!this.headerLine5Flag.equals("Y") && !this.headerLine5Comment.equals("-1"))
      {
        addError("Please check the Receipt Header Line 5 check box or delete the entered Line 5");
      }
      if(this.footerFlag.equals("Y")  && this.footerComment.equals("-1"))
      {
        addError("Please provide a Receipt Footer");
      }
      else if(!this.footerFlag.equals("Y")  && !this.footerComment.equals("-1"))
      {
        addError("Please check the Receipt Footer check box or delete the entered Footer");
      }

      if(this.fraudControlFlag.equals("Y")  && this.fraudControlComment.equals("-1") && appType == mesConstants.APP_TYPE_BBT)
      {
        addError("Please select last 4 digits or full card number for fraud control option");
      }

      if(cashBackFlag.equals("Y"))
      {
        try
        {
          double tempdub = Double.parseDouble(cashBackLimit);
        }
        catch(Exception e)
        {
          addError("Please enter a valid cash back limit");
        }
      }
      else if(!isBlank(cashBackLimit))
      {
        addError("Please select cash back with debit checkbox or delete cash back limit");
      }

      if(debitSurchargeFlag.equals("Y"))
      {
        try
        {
          double tempdub = Double.parseDouble(debitSurchargeAmount);
        }
        catch(Exception e)
        {
          addError("Please enter a valid debit surcharge amount");
        }
      }
      else if(!isBlank(debitSurchargeAmount))
      {
        addError("Please select debit surcharge checkbox or delete debit surcharge amount");
      }

      if(wirelessEsnFlag.equals("Y") && wirelessEsnNumber.equals("-1"))
      {
        addError("Please enter a wireless ESN Number for Motient");
      }
      else if(wirelessEsnFlag.equals("N") && !wirelessEsnNumber.equals("-1"))
      {
        addError("Please select wireless ESN Number for Motient checkbox or delete the number provided");
      }

      if(wirelessLliFlag.equals("Y") && wirelessLliNumber.equals("-1"))
      {
        addError("Please enter a wireless LLI Number for Motient");
      }
      else if(wirelessLliFlag.equals("N") && !wirelessLliNumber.equals("-1"))
      {
        addError("Please select wireless LLI Number for Motient checkbox or delete the number provided");
      }

      if(!isBlank(customerServicePhone) && !isValidPhone(customerServicePhone))
      {
        addError("Please provide a valid customer service phone #");
      }

      if(this.terminalReminderFlag.equals("Y") && (this.terminalReminderHhComment.equals("-1") || this.terminalReminderMmComment.equals("-1") || this.terminalReminderAmPmComment.equals("-1")))
      {
        addError("Please provide the hour(HH), minute(MM), and AM/PM of Terminal Reminder to Check Totals");
      }
      else if(!this.terminalReminderFlag.equals("Y") && (!this.terminalReminderHhComment.equals("-1") || !this.terminalReminderMmComment.equals("-1")))
      {
        addError("Please check the Terminal Reminder to Check Totals check box or delete the entered hour(HH) and minute(MM)");
      }
      else if(this.terminalReminderFlag.equals("Y") && !this.terminalReminderHhComment.equals("-1") && !this.terminalReminderMmComment.equals("-1") && !this.terminalReminderAmPmComment.equals("-1") && this.terminalReminderMmComment.length() != 2)
      {
        addError("Terminal Reminder to Check Totals Minute(MM) must be two digits long");
      }

      if(! virtualTerminal)
      {
        if(isBlank(shippingName))
        {
          addError("Please provide a shipping name");
        }
        if(isBlank(shippingContactName))
        {
          addError("Please provide a shipping contact name");
        }
        if(isBlank(shippingAddress1))
        {
          addError("Please provide shipping address line 1");
        }
        if(isBlank(shippingCity))
        {
          addError("Please provide shipping city");
        }
        if(shippingState.equals("-1"))
        {
          addError("Please provide shipping state");
        }

        int num = countDigits(shippingZip);
        if(isBlank(shippingZip) || (num != 5 && num != 9))
        {
          addError("Please provide a valid shipping zip");
        }

        if(!isBlank(shippingPhone) && !isValidPhone(shippingPhone))
        {
          addError("Please provide a valid shipping phone");
        }
      }

      if(this.equipmentComment.length() > 250)
      {
        addError("Equipment Comments must be less than 250 characters long. It is currently " + this.equipmentComment.length() + " characters long");
      }

      if(this.shippingComment.length() > 70)
      {
        addError("Shipping Comments must be less than 70 characters long. It is currently " + this.shippingComment.length() + " characters long");
      }
    }
    catch( Exception e )
    {
      logEntry("validate()",e.toString());
    }
    finally
    {
      cleanUp();
    }

    return(!hasErrors());
  }

  private boolean isValidAmount(String amount)
  {
    boolean result = false;

    try
    {
      double tempdub = Double.parseDouble(amount);
      result = true;
    }
    catch(Exception e)
    {
      result = false;
    }

    return result;
  }

  public void getData(long appSeqNum)
  {
    getApplicationType(appSeqNum);
    getEquipmentInfo(appSeqNum);
    getFeatureInfo(appSeqNum);
    getPaySolOption(appSeqNum);
    getIndustryType(appSeqNum);
    if(! virtualTerminal)
    {
      getReturnPolicy(appSeqNum);
    }
    getBusinessAddress(appSeqNum);
    getBusinessName(appSeqNum);
    getContactName(appSeqNum);
    getLeaseInfo(appSeqNum);
  }


  public void getAddressSelected(long appSeqNum)
  {
    StringBuffer      qs    = new StringBuffer("");
    PreparedStatement ps    = null;
    ResultSet         rs    = null;

    try
    {
      if(isRecalculated())
      {
        qs.append("select address_name,       ");
        qs.append("address_phone,             ");
        qs.append("address_line1,             ");
        qs.append("address_line2,             ");
        qs.append("address_city,              ");
        qs.append("countrystate_code,         ");
        qs.append("address_zip                ");
        qs.append("from address               ");
        qs.append("where app_seq_num = ? and  ");
        qs.append("addresstype_code = ?       ");

        if(useAddress.equals(EQUIP_ADD_TYPE))
        {
          this.shippingAddress1 = "";
          this.shippingAddress2 = "";
          this.shippingCity     = "";
          this.shippingState    = "";
          this.shippingZip      = "";
          this.shippingPhone    = "";
        }
        else
        {
          ps = getPreparedStatement(qs.toString());
          ps.setLong(1,appSeqNum);

          if(useAddress.equals(BUSINESS_ADD_TYPE))
          {
            ps.setString(2,BUSINESS_ADD_TYPE);
          }
          else if(useAddress.equals(MAILING_ADD_TYPE))
          {
            ps.setString(2,MAILING_ADD_TYPE);
          }
          else //just exit out of method and do nothing
          {
            return;
          }

          rs = ps.executeQuery();

          if(rs.next())
          {
            this.shippingAddress1 = isBlank(rs.getString("address_line1"))      ? "" : rs.getString("address_line1");
            this.shippingAddress2 = isBlank(rs.getString("address_line2"))      ? "" : rs.getString("address_line2");
            this.shippingCity     = isBlank(rs.getString("address_city"))       ? "" : rs.getString("address_city");
            this.shippingState    = isBlank(rs.getString("countrystate_code"))  ? "" : rs.getString("countrystate_code");
            this.shippingZip      = isBlank(rs.getString("address_zip"))        ? "" : rs.getString("address_zip");
            this.shippingPhone    = isBlank(rs.getString("address_phone"))      ? "" : rs.getString("address_phone");
          }

          rs.close();
          ps.close();
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAddressSelected: " + e.toString());
      addError("getAddressSelected: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void getLeaseInfo(long appSeqNum)
  {
    StringBuffer      qs    = new StringBuffer("");
    PreparedStatement ps    = null;
    ResultSet         rs    = null;

    try
    {
      qs.append("select *               ");
      qs.append("from merchequiplease   ");
      qs.append("where app_seq_num = ?  ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      if (rs.next())
      {
        leaseCompany  = isBlank(rs.getString("lease_company_code"))    ? "" : rs.getString("lease_company_code");
        leaseActNum   = isBlank(rs.getString("lease_account_num"))     ? "" : rs.getString("lease_account_num");
        leaseAuthNum  = isBlank(rs.getString("lease_auth_num"))        ? "" : rs.getString("lease_auth_num");
        leaseMonths   = isBlank(rs.getString("lease_months_num"))      ? "" : rs.getString("lease_months_num");
        leaseAmt      = isBlank(rs.getString("lease_monthly_amount"))  ? "" : rs.getString("lease_monthly_amount");
        fundingAmt    = isBlank(rs.getString("lease_funding_amount"))  ? "" : rs.getString("lease_funding_amount");
      }

      rs.close();
      ps.close();
    }
    catch (Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getLeaseInfo: " + e.toString());
      addError("getLeaseInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void getEquipmentInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select equiplendtype_code, ");
      qs.append("equip_model,               ");
      qs.append("merchequip_equip_quantity, ");
      qs.append("prod_option_id             ");
      qs.append("from merchequipment        ");
      qs.append("where app_seq_num = ? and  ");
      qs.append("equiptype_code = ?         ");

      for(int i=0; i<this.NUM_OF_TYPES; i++)
      {
        ps = getPreparedStatement(qs.toString());
        ps.setLong(1,appSeqNum);
        ps.setString(2,this.equipTypeCode[i]);
        rs = ps.executeQuery();

        while(rs.next())
        {
          int     lendType  = rs.getInt("equiplendtype_code");
          String  desc      = isBlank(rs.getString("equip_model"))     ? "" : rs.getString("equip_model");
          String  prod      = isBlank(rs.getString("prod_option_id"))  ? "" : rs.getString("prod_option_id");

          switch(lendType)
          {

            case BUY:
              this.equipNumBuy[i]     = rs.getString("merchequip_equip_quantity");
              this.equipNeeded[i]     = (desc + (isBlank(prod) ? "" : ("*" + prod))).trim();
            break;

            case BUY_REFURB:
              this.equipNumRefurb[i]  = rs.getString("merchequip_equip_quantity");
              this.equipNeeded[i]     = (desc + (isBlank(prod) ? "" : ("*" + prod))).trim();
            break;

            case RENT:
              this.equipNumRent[i]    = rs.getString("merchequip_equip_quantity");
              this.equipNeeded[i]     = (desc + (isBlank(prod) ? "" : ("*" + prod))).trim();
            break;

            case LEASE:
              this.equipNumLease[i]   = rs.getString("merchequip_equip_quantity");
              this.equipNeeded[i]     = (desc + (isBlank(prod) ? "" : ("*" + prod))).trim();
            break;

            case OWN:
              this.equipNumOwn[i]     = rs.getString("merchequip_equip_quantity");
              this.equipOwned[i]        = (desc + (isBlank(prod) ? "" : ("*" + prod))).trim();
            break;

          }

        }
        rs.close();
        ps.close();
      }

      //get the number of imprinter plates ordered
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setInt(2, this.EQUIP_TYPE_IMPRINTER_PLATE);
      rs = ps.executeQuery();

      if(rs.next())
      {
        this.imprinterPlates = rs.getInt("merchequip_equip_quantity");
      }

      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getEquipmentInfo: " + e.toString());
      addError("getEquipmentInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }
  private void getFeatureInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select                   ");
      qs.append("access_code_flag,        ");
      qs.append("access_code,             ");
      qs.append("auto_batch_close_flag,   ");
      qs.append("auto_batch_close_time,   ");
      qs.append("header_line4_flag,       ");
      qs.append("header_line4,            ");
      qs.append("header_line5_flag,       ");
      qs.append("header_line5,            ");
      qs.append("footer_flag,footer,      ");
      qs.append("reset_ref_flag,          ");
      qs.append("invoice_prompt_flag,     ");
      qs.append("fraud_control_flag,      ");
      qs.append("password_protect_flag,   ");
      qs.append("phone_training_flag,     ");
      qs.append("terminal_reminder_flag,  ");
      qs.append("terminal_reminder_time,  ");
      qs.append("avs_flag,                ");
      qs.append("cvv2_flag,               ");
      qs.append("phone_training,          ");
      qs.append("cash_back_flag,          ");
      qs.append("cash_back_limit,         ");
      qs.append("purchasing_card_flag,    ");
      qs.append("card_truncation_flag,    ");
      qs.append("debit_surcharge_flag,    ");
      qs.append("debit_surcharge_amount,  ");
      qs.append("wireless_lli_flag,       ");
      qs.append("wireless_lli_number,     ");
      qs.append("wireless_esn_flag,       ");
      qs.append("wireless_esn_number,     ");
      qs.append("customer_service_phone,  ");
      qs.append("tip_option_flag,         ");
      qs.append("clerk_enabled_flag,      ");
      qs.append("addresstype_code,        ");
      qs.append("equipment_comment,       ");
      qs.append("fraud_control_option,    ");
      qs.append("no_printer_needed,       ");
      qs.append("term_comm_type,          ");
      qs.append("no_printer_owned         ");
      qs.append("from pos_features where app_seq_num = ? ");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();

      if(rs.next())
      {
        if(!isBlank(rs.getString("access_code_flag")) && rs.getString("access_code_flag").equals("Y"))
        {
          this.accessCodeFlag         = rs.getString("access_code_flag");
        }

        if(!isBlank(rs.getString("access_code")))
        {
          this.accessCodeComment      =  rs.getString("access_code");
        }

        if(!isBlank(rs.getString("auto_batch_close_flag")) && rs.getString("auto_batch_close_flag").equals("Y"))
        {
          this.autoBatchFlag          = rs.getString("auto_batch_close_flag");
        }
        if(!isBlank(rs.getString("auto_batch_close_time")))
        {
          setBatchTime(rs.getString("auto_batch_close_time"));
        }
        if(!isBlank(rs.getString("header_line4_flag")) && rs.getString("header_line4_flag").equals("Y"))
        {
          this.headerLine4Flag        = rs.getString("header_line4_flag");
        }
        if(!isBlank(rs.getString("header_line4")))
        {
          this.headerLine4Comment     = rs.getString("header_line4");
        }
        if(!isBlank(rs.getString("header_line5_flag")) && rs.getString("header_line5_flag").equals("Y"))
        {
          this.headerLine5Flag        = rs.getString("header_line5_flag");
        }
        if(!isBlank(rs.getString("header_line5")))
        {
          this.headerLine5Comment     = rs.getString("header_line5");
        }
        if(!isBlank(rs.getString("footer_flag")) && rs.getString("footer_flag").equals("Y"))
        {
          this.footerFlag             = rs.getString("footer_flag");
        }
        if(!isBlank(rs.getString("footer")))
        {
          this.footerComment          = rs.getString("footer");
        }
        if(!isBlank(rs.getString("reset_ref_flag")) && rs.getString("reset_ref_flag").equals("Y"))
        {
          this.resetReferenceFlag     = rs.getString("reset_ref_flag");
        }
        if(!isBlank(rs.getString("invoice_prompt_flag")) && rs.getString("invoice_prompt_flag").equals("Y"))
        {
          this.invoicePromptFlag      = rs.getString("invoice_prompt_flag");
        }
        if(!isBlank(rs.getString("fraud_control_flag")) && rs.getString("fraud_control_flag").equals("Y"))
        {
          this.fraudControlFlag       = rs.getString("fraud_control_flag");
        }
        if(!isBlank(rs.getString("fraud_control_option")))
        {
          this.fraudControlComment    = rs.getString("fraud_control_option");
        }
        if(!isBlank(rs.getString("password_protect_flag")) && rs.getString("password_protect_flag").equals("Y"))
        {
          this.passwordProtectFlag    = rs.getString("password_protect_flag");
        }
        if(!isBlank(rs.getString("phone_training_flag")) && rs.getString("phone_training_flag").equals("Y"))
        {
          this.phoneTrainingFlag      = rs.getString("phone_training_flag");
        }
        if(!isBlank(rs.getString("terminal_reminder_flag")) && rs.getString("terminal_reminder_flag").equals("Y"))
        {
          this.terminalReminderFlag   = rs.getString("terminal_reminder_flag");
        }
        if(!isBlank(rs.getString("terminal_reminder_time")))
        {
          setReminderTime(rs.getString("terminal_reminder_time"));
        }
        if(!isBlank(rs.getString("tip_option_flag")) && rs.getString("tip_option_flag").equals("Y"))
        {
          this.tipOptionFlag          = rs.getString("tip_option_flag");
        }
        if(!isBlank(rs.getString("clerk_enabled_flag")) && rs.getString("clerk_enabled_flag").equals("Y"))
        {
          this.clerkEnabledFlag       = rs.getString("clerk_enabled_flag");
        }
        if(!isBlank(rs.getString("equipment_comment")))
        {
          this.equipmentComment       = rs.getString("equipment_comment");
        }
        if(!isBlank(rs.getString("addresstype_code")))
        {
          this.useAddress             = rs.getString("addresstype_code");
        }
        if(!isBlank(rs.getString("no_printer_needed")))
        {
          this.noPrinterNeed          = (rs.getString("no_printer_needed")).equals("Y") ? true : false;
        }
        if(!isBlank(rs.getString("no_printer_owned")))
        {
          this.noPrinterOwn           = (rs.getString("no_printer_owned")).equals("Y") ? true : false;
        }

        if(!isBlank(rs.getString("avs_flag")))
        {
          this.avsOnFlag = rs.getString("avs_flag");
        }

        if(!isBlank(rs.getString("cvv2_flag")))
        {
          this.cvv2Flag = rs.getString("cvv2_flag");
        }

        if(!isBlank(rs.getString("phone_training")))
        {
          this.phoneTraining = rs.getString("phone_training");
        }
        if(!isBlank(rs.getString("cash_back_flag")))
        {
          this.cashBackFlag = rs.getString("cash_back_flag");
        }
        if(!isBlank(rs.getString("cash_back_limit")))
        {
          this.cashBackLimit = rs.getString("cash_back_limit");
        }
        if(!isBlank(rs.getString("purchasing_card_flag")))
        {
          this.purchasingCardFlag = rs.getString("purchasing_card_flag");
        }
        if(!isBlank(rs.getString("card_truncation_flag")))
        {
          this.truncationCardFlag = rs.getString("card_truncation_flag");
        }
        if(!isBlank(rs.getString("debit_surcharge_flag")))
        {
          this.debitSurchargeFlag = rs.getString("debit_surcharge_flag");
        }
        if(!isBlank(rs.getString("debit_surcharge_amount")))
        {
          this.debitSurchargeAmount = rs.getString("debit_surcharge_amount");
        }

        if(!isBlank(rs.getString("wireless_esn_flag")))
        {
          this.wirelessEsnFlag = rs.getString("wireless_esn_flag");
        }
        if(!isBlank(rs.getString("wireless_esn_number")))
        {
          this.wirelessEsnNumber = rs.getString("wireless_esn_number");
        }

        if(!isBlank(rs.getString("wireless_lli_flag")))
        {
          this.wirelessLliFlag = rs.getString("wireless_lli_flag");
        }
        if(!isBlank(rs.getString("wireless_lli_number")))
        {
          this.wirelessLliNumber = rs.getString("wireless_lli_number");
        }

        if(!isBlank(rs.getString("customer_service_phone")))
        {
          this.customerServicePhone = rs.getString("customer_service_phone");
        }

        if(!isBlank(rs.getString("term_comm_type")))
        {
          this.ipTerminal = rs.getInt("term_comm_type");
        }

        if(!this.useAddress.equals(INVALID_ADD_TYPE))
        {
          getShippingAddress(appSeqNum);
        }
      }

      rs.close();
      ps.close();

      qs.setLength(0);
      qs.append("select                   ");
      qs.append("lease_term,              ");
      qs.append("avs_flag,                ");
      qs.append("phone_training,          ");
      qs.append("cash_back_flag,          ");
      qs.append("cash_back_limit,         ");
      qs.append("shipping_comment,        ");
      qs.append("terminal_insurance,      ");
      qs.append("terminal_track,          ");
      qs.append("terminal_printer,        ");
      qs.append("purchasing_card_flag,    ");
      qs.append("card_truncation_flag,    ");
      qs.append("debit_surcharge_flag,    ");
      qs.append("debit_surcharge_amount,  ");
      qs.append("wireless_lli_flag,       ");
      qs.append("wireless_lli_number,     ");
      qs.append("wireless_esn_flag,       ");
      qs.append("wireless_esn_number,     ");
      qs.append("customer_service_phone   ");
      qs.append("from transcom_merchant where app_seq_num = ?");
      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        if(!isBlank(rs.getString("lease_term")))
        {
          this.leaseMonths = rs.getString("lease_term");
        }
        if(!isBlank(rs.getString("avs_flag")))
        {
          this.avsOnFlag = rs.getString("avs_flag");
        }
        if(!isBlank(rs.getString("phone_training")))
        {
          this.phoneTraining = rs.getString("phone_training");
        }
        if(!isBlank(rs.getString("cash_back_flag")))
        {
          this.cashBackFlag = rs.getString("cash_back_flag");
        }
        if(!isBlank(rs.getString("cash_back_limit")))
        {
          this.cashBackLimit = rs.getString("cash_back_limit");
        }
        if(!isBlank(rs.getString("terminal_insurance")))
        {
          this.terminalInsurance = rs.getString("terminal_insurance");
        }
        if(!isBlank(rs.getString("terminal_track")))
        {
          this.terminalTrack = rs.getString("terminal_track");
        }
        if(!isBlank(rs.getString("terminal_printer")))
        {
          this.terminalPrinter = rs.getString("terminal_printer");
        }
        if(!isBlank(rs.getString("purchasing_card_flag")))
        {
          this.purchasingCardFlag = rs.getString("purchasing_card_flag");
        }
        if(!isBlank(rs.getString("card_truncation_flag")))
        {
          this.truncationCardFlag = rs.getString("card_truncation_flag");
        }
        if(!isBlank(rs.getString("debit_surcharge_flag")))
        {
          this.debitSurchargeFlag = rs.getString("debit_surcharge_flag");
        }
        if(!isBlank(rs.getString("debit_surcharge_amount")))
        {
          this.debitSurchargeAmount = rs.getString("debit_surcharge_amount");
        }

        if(!isBlank(rs.getString("wireless_esn_flag")))
        {
          this.wirelessEsnFlag = rs.getString("wireless_esn_flag");
        }
        if(!isBlank(rs.getString("wireless_esn_number")))
        {
          this.wirelessEsnNumber = rs.getString("wireless_esn_number");
        }

        if(!isBlank(rs.getString("wireless_lli_flag")))
        {
          this.wirelessLliFlag = rs.getString("wireless_lli_flag");
        }
        if(!isBlank(rs.getString("wireless_lli_number")))
        {
          this.wirelessLliNumber = rs.getString("wireless_lli_number");
        }

        if(!isBlank(rs.getString("customer_service_phone")))
        {
          this.customerServicePhone = rs.getString("customer_service_phone");
        }
        if(!isBlank(rs.getString("shipping_comment")))
        {
          this.shippingComment = rs.getString("shipping_comment");
        }
      }

      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getFeatureInfo: " + e.toString());
      addError("getFeatureInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void getShippingAddress(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select                     ");
      qs.append("address_name,              ");
      qs.append("address_phone,             ");
      qs.append("address_line1,             ");
      qs.append("address_line2,             ");
      qs.append("address_city,              ");
      qs.append("countrystate_code,         ");
      qs.append("address_zip                ");
      qs.append("from address               ");
      qs.append("where app_seq_num = ? and  ");
      qs.append("addresstype_code = ?       ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);
      ps.setString(2, EQUIP_ADD_TYPE);

      rs = ps.executeQuery();

      if(rs.next())
      {
        if(!isBlank(rs.getString("address_name")))
        {
          this.shippingName     = rs.getString("address_name");
        }
        if(!isBlank(rs.getString("address_line1")))
        {
          this.shippingAddress1 = rs.getString("address_line1");
        }
        if(!isBlank(rs.getString("address_line2")))
        {
          this.shippingAddress2 = rs.getString("address_line2");
        }
        if(!isBlank(rs.getString("address_city")))
        {
          this.shippingCity     = rs.getString("address_city");
        }
        if(!isBlank(rs.getString("countrystate_code")))
        {
          this.shippingState    = rs.getString("countrystate_code");
        }
        if(!isBlank(rs.getString("address_zip")))
        {
          this.shippingZip      = rs.getString("address_zip");
        }
        if(!isBlank(rs.getString("address_phone")))
        {
          this.shippingPhone    = rs.getString("address_phone");
        }
      }

      rs.close();
      ps.close();

      qs.setLength(0);
      qs.append("select                 ");
      qs.append("shipping_contact_name, ");
      qs.append("shipping_method        ");
      qs.append("from transcom_merchant ");
      qs.append("where app_seq_num = ?  ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        if(!isBlank(rs.getString("shipping_contact_name")))
        {
          this.shippingContactName = rs.getString("shipping_contact_name");
        }
        if(!isBlank(rs.getString("shipping_method")))
        {
          this.shippingMethod = rs.getString("shipping_method");
        }
      }

      rs.close();
      ps.close();

      qs.setLength(0);
      qs.append("select                 ");
      qs.append("shipping_contact_name, ");
      qs.append("shipping_method,       ");
      qs.append("shipping_comment       ");
      qs.append("from pos_features      ");
      qs.append("where app_seq_num = ?  ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        if(!isBlank(rs.getString("shipping_contact_name")))
        {
          this.shippingContactName = rs.getString("shipping_contact_name");
        }
        if(!isBlank(rs.getString("shipping_method")))
        {
          this.shippingMethod = rs.getString("shipping_method");
        }
        if(!isBlank(rs.getString("shipping_comment")))
        {
          this.shippingComment = rs.getString("shipping_comment");
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getShippingAddress: " + e.toString());
      addError("getShippingAddress: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void getPaySolOption(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select pos_type ");
      qs.append("from merch_pos a, pos_category b ");
      qs.append("where a.app_seq_num = ? and a.pos_code = b.pos_code");

      ps = getPreparedStatement(qs.toString());

      ps.clearParameters();
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      if(rs.next())
      {
        if(rs.getInt("pos_type") == mesConstants.APP_PAYSOL_DIAL_PAY || rs.getInt("pos_type") == mesConstants.POS_GPS)
        {
          dialPay = true;
        }
        else if(rs.getInt("pos_type") == mesConstants.APP_PAYSOL_INTERNET || rs.getInt("pos_type") == mesConstants.APP_PAYSOL_OTHER_PRODUCT)
        {
          noTraining = true;
        }
        else if(rs.getInt("pos_type") == mesConstants.POS_VIRTUAL_TERMINAL)
        {
          virtualTerminal = true;
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPaySolOption: " + e.toString());
      addError("getPaySolOption: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void getIndustryType(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select industype_code from merchant where app_seq_num = ? ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      if(rs.next())
      {
        if(rs.getInt("industype_code") != mesConstants.APP_INDUSTYPE_RESTAURANT)
        {
          retail = true;
        }

        if(rs.getInt("industype_code") == mesConstants.APP_INDUSTYPE_INTERNET || rs.getInt("industype_code") == mesConstants.APP_INDUSTYPE_DIRECTMARKET)
        {
          moto = true;
        }

        if(rs.getInt("industype_code") == mesConstants.APP_INDUSTYPE_CASH_ADVANCE)
        {
          cashAdvance = true;
        }

      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getIndustryType: " + e.toString());
      addError("getIndustryType: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void getReturnPolicy(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      // build base statement
      qs.append("select rft.refundtype_desc from refundtype rft, merchant merch where merch.app_seq_num = ? and ");
      qs.append("merch.refundtype_code = rft.refundtype_code");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      rs = ps.executeQuery();
      if(rs.next())
      {
        this.businessReturnPolicy = rs.getString("refundtype_desc");
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getReturnPolicy: " + e.toString());
      addError("getReturnPolicy: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void getBusinessName(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      // build base statement
      qs.append("select merch_business_name from merchant merch where app_seq_num = ? ");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      rs = ps.executeQuery();
      if(rs.next())
      {
        this.businessName = rs.getString("merch_business_name");
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getBusinessName: " + e.toString());
      addError("getBusinessName: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void getContactName(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      // build base statement
      qs.append("select MERCHCONT_PRIM_FIRST_NAME,MERCHCONT_PRIM_LAST_NAME,MERCHCONT_PRIM_PHONE from MERCHCONTACT merch where app_seq_num = ? ");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        this.contactName = rs.getString("MERCHCONT_PRIM_FIRST_NAME") + " " + rs.getString("MERCHCONT_PRIM_LAST_NAME");
      }

      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getContactName: " + e.toString());
      addError("getContactName: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }


  private void getBusinessAddress(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      // build base statement
      qs.append("select * from address where app_seq_num = ? and addresstype_code = ? ");
      ps = getPreparedStatement(qs.toString());

      ps.setLong(1, appSeqNum);
      ps.setInt (2, mesConstants.ADDR_TYPE_BUSINESS); // business address

      rs = ps.executeQuery();

      if(rs.next())
      {
        this.businessAddress = rs.getString("address_line1");
        this.businessPhone = rs.getString("address_phone");
        this.businessPhone = formatPhone(this.businessPhone);
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getBusinessAddress: " + e.toString());
      addError("getBusinessAddress: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }


  private String formatPhone(String phone)
  {
    if(isBlank(phone))
    {
      return "";
    }
    if(phone.length() != 10)
    {
      return "";
    }
    String areaCode   = phone.substring(0,3);
    String firstThree = phone.substring(3,6);
    String lastFour   = phone.substring(6);
    return ("(" + areaCode + ")" + " " + firstThree + "-" + lastFour);
  }

  private void getCurrentAmounts(long appSeqNum)
  {
    StringBuffer      qs       = new StringBuffer("");
    PreparedStatement ps       = null;
    ResultSet         rs       = null;
    int               lendType = 0;

    try
    {
      qs.append("select merchequip_amount,    ");
      qs.append("equip_model,                 ");
      qs.append("equiplendtype_code           ");
      qs.append("from merchequipment          ");
      qs.append("where app_seq_num = ?        ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();

      while(rs.next())
      {
        lendType = rs.getInt("equiplendtype_code");

        switch(lendType)
        {
          case BUY:
            for(int i=0; i<this.NUM_OF_TYPES; i++)
            {
              if(!isBlank(this.equipNeeded[i]) && (ehb.decodeModel(this.equipNeeded[i])).equals(rs.getString("equip_model")))
              {
                if(!isBlank(rs.getString("merchequip_amount")))
                {
                  this.equipAmtBuy[i] = rs.getString("merchequip_amount");
                }
              }
            }
            break;
          case BUY_REFURB:
            for(int i=0; i<this.NUM_OF_TYPES; i++)
            {
              if(!isBlank(this.equipNeeded[i]) && (ehb.decodeModel(this.equipNeeded[i])).equals(rs.getString("equip_model")))
              {
                if(!isBlank(rs.getString("merchequip_amount")))
                {
                  this.equipAmtRefurb[i] = rs.getString("merchequip_amount");
                }
              }
            }
            break;
          case RENT:
            for(int i=0; i<this.NUM_OF_TYPES; i++)
            {
              if(!isBlank(this.equipNeeded[i]) && (ehb.decodeModel(this.equipNeeded[i])).equals(rs.getString("equip_model")))
              {
                if(!isBlank(rs.getString("merchequip_amount")))
                {
                  this.equipAmtRent[i] = rs.getString("merchequip_amount");
                }
              }
            }
            break;
          case OWN:
            for(int i=0; i<this.NUM_OF_TYPES; i++)
            {
              if(!isBlank(this.equipOwned[i]) && (ehb.decodeModel(this.equipOwned[i])).equals(rs.getString("equip_model")))
              {
                if(!isBlank(rs.getString("merchequip_amount")))
                {
                  this.equipAmtOwn[i] = rs.getString("merchequip_amount");
                }
              }
            }
            break;
          case LEASE:
            for(int i=0; i<this.NUM_OF_TYPES; i++)
            {
              if(!isBlank(this.equipNeeded[i]) && (ehb.decodeModel(this.equipNeeded[i])).equals(rs.getString("equip_model")))
              {
                if(!isBlank(rs.getString("merchequip_amount")))
                {
                  this.equipAmtLease[i] = rs.getString("merchequip_amount");
                }
              }
            }
            break;
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCurrentAmounts: " + e.toString());
      addError("getCurrentAmounts: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  public void submitData(HttpServletRequest request, long appSeqNum)
  {
    submitEquipmentInfo(appSeqNum);
    submitFeatureInfo(appSeqNum);

    if(appType == mesConstants.APP_TYPE_TRANSCOM)
    {
      submitTranscomInfo(appSeqNum);
    }

    if(isLeasingEquip())
    {
      submitLeaseInfo(appSeqNum);
    }
    else
    {
      deleteLeaseInfo(appSeqNum);
    }
  }

  private void deleteLeaseInfo(long appSeqNum)
  {
    PreparedStatement ps      = null;

    try
    {
      ps = getPreparedStatement("delete merchequiplease where app_seq_num = ?");
      ps.setLong(1,appSeqNum);
      ps.executeUpdate();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "deleteLeaseInfo: " + e.toString());
      addError("deleteLeaseInfo: " + e.toString());
    }
  }

  private void submitLeaseInfo(long appSeqNum)
  {
    StringBuffer      qs    = new StringBuffer("");
    PreparedStatement ps    = null;
    ResultSet         rs    = null;

    try
    {
      qs.append("select app_seq_num from merchequiplease where app_seq_num = ? ");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();

      qs.setLength(0);
      if(rs.next())
      {
          qs.append("update merchequiplease set ");
          qs.append("lease_company_code   = ?, ");
          qs.append("lease_account_num    = ?, ");
          qs.append("lease_auth_num       = ?, ");
          qs.append("lease_months_num     = ?, ");
          qs.append("lease_monthly_amount = ?, ");
          qs.append("lease_funding_amount = ?  ");
          qs.append("where app_seq_num    = ?  ");
      }
      else
      {
          qs.append("insert into merchequiplease (");
          qs.append("lease_company_code, ");
          qs.append("lease_account_num, ");
          qs.append("lease_auth_num, ");
          qs.append("lease_months_num, ");
          qs.append("lease_monthly_amount, ");
          qs.append("lease_funding_amount, ");
          qs.append("app_seq_num) ");
          qs.append("values(?,?,?,?,?,?,?)");
      }
      rs.close();
      ps.close();

      ps = getPreparedStatement(qs.toString());

      ps.setString(1,this.leaseCompany);
      ps.setString(2,this.leaseActNum);
      ps.setString(3,this.leaseAuthNum);
      ps.setString(4,this.leaseMonths);
      ps.setString(5,this.leaseAmt);
      ps.setString(6,this.fundingAmt);
      ps.setLong(7,appSeqNum);

      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitLeaseInfo: insert failed");
        addError("submitLeaseInfo: Unable to insert/update record");
      }
    }
    catch (Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(),
        "submitLeaseInfo: " + e.toString());
      addError("submitLeaseInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void submitTranscomInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select app_seq_num from transcom_merchant where app_seq_num = ? ");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      qs.setLength(0);

      if(rs.next())
      {
          qs.append("update transcom_merchant set ");
          qs.append("avs_flag               = ?,  ");
          qs.append("phone_training         = ?,  ");
          qs.append("cash_back_flag         = ?,  ");
          qs.append("cash_back_limit        = ?,  ");
          qs.append("terminal_insurance     = ?,  ");
          qs.append("terminal_track         = ?,  ");
          qs.append("terminal_printer       = ?,  ");
          qs.append("shipping_contact_name  = ?,  ");
          qs.append("shipping_method        = ?,  ");
          qs.append("shipping_comment       = ?,  ");
          qs.append("lease_term             = ?,  ");
          qs.append("purchasing_card_flag   = ?,  ");
          qs.append("card_truncation_flag   = ?,  ");
          qs.append("debit_surcharge_flag   = ?,  ");
          qs.append("debit_surcharge_amount = ?,  ");
          qs.append("wireless_lli_flag      = ?,  ");
          qs.append("wireless_lli_number    = ?,  ");
          qs.append("wireless_esn_flag      = ?,  ");
          qs.append("wireless_esn_number    = ?,  ");
          qs.append("customer_service_phone = ?   ");
          qs.append("where app_seq_num      = ?   ");
      }
      else
      {
          qs.append("insert into transcom_merchant (  ");
          qs.append("avs_flag,                        ");
          qs.append("phone_training,                  ");
          qs.append("cash_back_flag,                  ");
          qs.append("cash_back_limit,                 ");
          qs.append("terminal_insurance,              ");
          qs.append("terminal_track,                  ");
          qs.append("terminal_printer,                ");
          qs.append("shipping_contact_name,           ");
          qs.append("shipping_method,                 ");
          qs.append("shipping_comment,                ");
          qs.append("lease_term,                      ");
          qs.append("purchasing_card_flag,            ");
          qs.append("card_truncation_flag,            ");
          qs.append("debit_surcharge_flag,            ");
          qs.append("debit_surcharge_amount,          ");
          qs.append("wireless_lli_flag,               ");
          qs.append("wireless_lli_number,             ");
          qs.append("wireless_esn_flag,               ");
          qs.append("wireless_esn_number,             ");
          qs.append("customer_service_phone,          ");
          qs.append("app_seq_num)                     ");
          qs.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      }
      rs.close();
      ps.close();

      ps = getPreparedStatement(qs.toString());

      ps.setString(1,avsOnFlag);
      ps.setString(2,phoneTraining);
      ps.setString(3,cashBackFlag);
      ps.setString(4,cashBackLimit);
      ps.setString(5,terminalInsurance);
      ps.setString(6,terminalTrack);
      ps.setString(7,terminalPrinter);
      ps.setString(8,shippingContactName);
      ps.setString(9,shippingMethod);
      ps.setString(10,shippingComment);
      ps.setString(11,leaseMonths);
      ps.setString(12,purchasingCardFlag);
      ps.setString(13,truncationCardFlag);
      ps.setString(14,debitSurchargeFlag);
      ps.setString(15,debitSurchargeAmount);
      ps.setString(16,wirelessLliFlag);
      ps.setString(17,getWirelessLliNumber());
      ps.setString(18,wirelessEsnFlag);
      ps.setString(19,getWirelessEsnNumber());
      ps.setString(20,customerServicePhone);
      ps.setLong(21,appSeqNum);

      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitTranscomInfo: insert failed");
        addError("submitTranscomInfo: Unable to insert/update record");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitTranscomInfo: " + e.toString());
      addError("submitTranscomInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void submitEquipmentInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      getCurrentAmounts(appSeqNum);

      qs.append("delete from merchequipment where app_seq_num = ? and equip_model != 'PCPS' ");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      ps.executeUpdate();
      ps.close();

      qs.setLength(0);
      qs.append("insert into merchequipment ( ");
      qs.append("app_seq_num,               ");
      qs.append("equiptype_code,            ");
      qs.append("equiplendtype_code,        ");
      qs.append("merchequip_amount,         ");
      qs.append("equip_model,               ");
      qs.append("merchequip_equip_quantity, ");
      qs.append("prod_option_id,            ");
      qs.append("quantity_deployed)         ");
      qs.append("values(?,?,?,?,?,?,?,?)");
      ps = getPreparedStatement(qs.toString());

      for(int i=0; i<this.NUM_OF_TYPES; i++)
      {
        if(!this.equipNumBuy[i].equals("-1"))
        {
          ps.setLong(1,appSeqNum);
          ps.setString(2,this.equipTypeCode[i]);
          ps.setInt(3,this.BUY);
          if(isBlank(this.equipAmtBuy[i]) || this.equipAmtBuy[i].equals("-1"))
          {
            ps.setNull(4,java.sql.Types.FLOAT);
          }
          else
          {
            ps.setString(4,this.equipAmtBuy[i]);
          }

          ps.setString(5,ehb.decodeModel(this.equipNeeded[i]));
          ps.setString(6,this.equipNumBuy[i]);

          ps.setString(7,ehb.decodeProdId(this.equipNeeded[i]));

          ps.setInt(8,0); //set quantity deployed = zero

          if(ps.executeUpdate() != 1)
          {
            com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitEquipmentInfoBuy: insert failed");
            addError("submitEquipmentInfoBuy: Unable to insert/update record");
          }
        }

        if(!this.equipNumRefurb[i].equals("-1"))
        {
          ps.setLong(1,appSeqNum);
          ps.setString(2,this.equipTypeCode[i]);
          ps.setInt(3,this.BUY_REFURB);
          if(isBlank(this.equipAmtRefurb[i]) || this.equipAmtRefurb[i].equals("-1"))
          {
            ps.setNull(4,java.sql.Types.FLOAT);
          }
          else
          {
            ps.setString(4,this.equipAmtRefurb[i]);
          }

          ps.setString(5,ehb.decodeModel(this.equipNeeded[i]));
          ps.setString(6,this.equipNumRefurb[i]);
          ps.setString(7,ehb.decodeProdId(this.equipNeeded[i]));

          ps.setInt(8,0); //set quantity deployed = zero

          if(ps.executeUpdate() != 1)
          {
            com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitEquipmentInfoRefurb: insert failed");
            addError("submitEquipmentInfoRefurb: Unable to insert/update record");
          }
        }


        if(!this.equipNumRent[i].equals("-1"))
        {
          ps.setLong(1,appSeqNum);
          ps.setString(2,this.equipTypeCode[i]);
          ps.setInt(3,this.RENT);

          if(isBlank(this.equipAmtRent[i]) || this.equipAmtRent[i].equals("-1"))
          {
            ps.setNull(4,java.sql.Types.FLOAT);
          }
          else
          {
            ps.setString(4,this.equipAmtRent[i]);
          }

          ps.setString(5,ehb.decodeModel(this.equipNeeded[i]));
          ps.setString(6,this.equipNumRent[i]);
          ps.setString(7,ehb.decodeProdId(this.equipNeeded[i]));

          ps.setInt(8,0); //set quantity deployed = zero

          if(ps.executeUpdate() != 1)
          {
            com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitEquipmentInfoRent: insert failed");
            addError("submitEquipmentInfoRent: Unable to insert/update record");
          }
        }

        if(!this.equipNumLease[i].equals("-1"))
        {
          ps.setLong(1,appSeqNum);
          ps.setString(2,this.equipTypeCode[i]);
          ps.setInt(3,this.LEASE);

          if(isBlank(this.equipAmtLease[i]) || this.equipAmtLease[i].equals("-1"))
          {
            ps.setNull(4,java.sql.Types.FLOAT);
          }
          else
          {
            ps.setString(4,this.equipAmtLease[i]);
          }

          ps.setString(5,ehb.decodeModel(this.equipNeeded[i]));
          ps.setString(6,this.equipNumLease[i]);
          ps.setString(7,ehb.decodeProdId(this.equipNeeded[i]));

          ps.setInt(8,0); //set quantity deployed = zero

          if(ps.executeUpdate() != 1)
          {
            com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitEquipmentInfoLease: insert failed");
            addError("submitEquipmentInfoLease: Unable to insert/update record");
          }
        }


        if(!this.equipNumOwn[i].equals("-1"))
        {
          ps.setLong(1,appSeqNum);
          ps.setString(2,this.equipTypeCode[i]);
          ps.setInt(3,this.OWN);

          if(isBlank(this.equipAmtOwn[i]) || this.equipAmtOwn[i].equals("-1"))
          {
            ps.setNull(4,java.sql.Types.FLOAT);
          }
          else
          {
            ps.setString(4,this.equipAmtOwn[i]);
          }


          ps.setString(5,ehb.decodeModel(this.equipOwned[i]));
          ps.setString(6,this.equipNumOwn[i]);
          ps.setString(7,ehb.decodeProdId(this.equipOwned[i]));

          ps.setInt(8,0); //set quantity deployed = zero

          if(ps.executeUpdate() != 1)
          {
            com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitEquipmentInfoOwn: insert failed");
            addError("submitEquipmentInfoOwn: Unable to insert/update record");
          }
        }
      }

      if(this.imprinterPlates > 0)
      {
        ps.setLong(1, appSeqNum);
        ps.setInt(2, this.EQUIP_TYPE_IMPRINTER_PLATE);
        ps.setInt(3, this.BUY);
        ps.setNull(4, java.sql.Types.FLOAT);
        ps.setString(5, "IPPL");
        ps.setInt(6, this.imprinterPlates);
        ps.setNull(7,java.sql.Types.INTEGER);
        ps.setInt(8,0); //set quantity deployed = zero

        if(ps.executeUpdate() != 1)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitEquipmentInfo - Imprinter Plates: insert failed");
          addError("unable to insert imprinter plates quantity");
        }
      }
      ps.close();

      if(!this.equipNumBuy[PINPADS].equals("-1")  || !this.equipNumRefurb[PINPADS].equals("-1") ||
         !this.equipNumRent[PINPADS].equals("-1") || !this.equipNumLease[PINPADS].equals("-1")  ||
         !this.equipNumOwn[PINPADS].equals("-1"))
      {
        //since a pinpad was selected we make sure debit is selected, if not we selected it manually...
        makeSureDebitIsSelected(appSeqNum);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitEquipmentInfo: " + e.toString());
      addError("submitEquipmentInfo: " + e.toString());
    }
  }


  public void loadShippingMethods( long appSeqNum )
  {
    ResultSetIterator   it              = null;
    ResultSet           rs              = null;
    String              shippingMethod  = null;

    try
    {
      connect();

      // clear the existing items
      method.removeAllElements();
      methodDesc.removeAllElements();

      try
      {
        // get the shipping method from the application
        /*@lineinfo:generated-code*//*@lineinfo:2492^9*/

//  ************************************************************
//  #sql [Ctx] { select  pf.shipping_method 
//            from    pos_features  pf
//            where   pf.app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pf.shipping_method  \n          from    pos_features  pf\n          where   pf.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.setup.EquipmentBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   shippingMethod = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2497^9*/
      }
      catch( Exception ee )
      {
        // ignore, shipping method not set yet
      }

      // new airborne shipping methods
      /*@lineinfo:generated-code*//*@lineinfo:2505^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  code,
//                  (description || ': ' || long_description)    as code_desc
//          from    shipping_methods
//          where   trunc(nvl( ( select  app.app_created_date
//                               from    application app
//                               where   app.app_seq_num(+) = :appSeqNum ),
//                             sysdate
//                           )
//                       ) between from_date and to_date or
//                  code = nvl(:shippingMethod,'-1')
//          order by display_order, from_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  code,\n                (description || ': ' || long_description)    as code_desc\n        from    shipping_methods\n        where   trunc(nvl( ( select  app.app_created_date\n                             from    application app\n                             where   app.app_seq_num(+) =  :1  ),\n                           sysdate\n                         )\n                     ) between from_date and to_date or\n                code = nvl( :2 ,'-1')\n        order by display_order, from_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.setup.EquipmentBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,shippingMethod);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.setup.EquipmentBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2518^7*/
      rs = it.getResultSet();

      while( rs.next() )
      {
        method.add(rs.getString("code"));
        methodDesc.add(rs.getString("code_desc"));
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadShippingMethods()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void makeSureDebitIsSelected(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    boolean           exists  = false;

    try
    {
      ps = getPreparedStatement("select app_seq_num from merchpayoption where app_seq_num = ? and cardtype_code = ? ");
      ps.setLong(1,appSeqNum);
      ps.setInt(2, mesConstants.APP_CT_DEBIT);

      rs = ps.executeQuery();

      if(rs.next())
      {
        exists = true;
      }

      rs.close();
      ps.close();


      if(!exists)
      {
        qs.setLength(0);
        qs.append("insert into                ");
        qs.append("merchpayoption (           ");
        qs.append("app_seq_num,               ");
        qs.append("cardtype_code,             ");
        qs.append("card_sr_number,            ");
        qs.append("merchpo_split_dial,        ");
        qs.append("merchpo_pip,               ");
        qs.append("MERCHPO_CARD_MERCH_NUMBER) ");
        qs.append("values(?,?,?,?,?,?)    ");

        ps = getPreparedStatement(qs.toString());

        ps.setLong(1,   appSeqNum);
        ps.setInt(2,    mesConstants.APP_CT_DEBIT);
        ps.setInt(3,    getCardSrNumber(appSeqNum));
        ps.setString(4, "N");
        ps.setString(5, "N");
        ps.setInt(6,    0);

        ps.executeUpdate();

        ps.close();
      }

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "makeSureDebitIsSelected: " + e.toString());
      addError("makeSureDebitIsSelected: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }


  private int getCardSrNumber(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               result  = 99;

    try
    {
      ps = getPreparedStatement("select max(card_sr_number) from merchpayoption where app_seq_num = ? ");
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        result = rs.getInt(1) + 1;
      }

      rs.close();
      ps.close();

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCardSrNumber: " + e.toString());
      addError("getCardSrNumber: " + e.toString());
    }

    return result;
  }


  private void deleteShippingAddress(long appSeqNum)
  {
    PreparedStatement ps      = null;

    try
    {
      ps = getPreparedStatement("delete address where app_seq_num = ? and addresstype_code = ? ");
      ps.setLong(1,appSeqNum);
      ps.setString(2, EQUIP_ADD_TYPE);

      ps.executeUpdate();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "deleteShippingAddress: " + e.toString());
      addError("deleteShippingAddress: " + e.toString());
    }
  }

  private void submitShippingAddress(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      ps = getPreparedStatement("select app_seq_num from address where app_seq_num = ? and addresstype_code = ? ");
      ps.setLong(1,appSeqNum);
      ps.setString(2, EQUIP_ADD_TYPE);

      rs = ps.executeQuery();
      qs.setLength(0);

      if(rs.next())
      {
          qs.append("update address set         ");
          qs.append("address_name       = ?,    ");
          qs.append("address_line1      = ?,    ");
          qs.append("address_line2      = ?,    ");
          qs.append("address_city       = ?,    ");
          qs.append("countrystate_code  = ?,    ");
          qs.append("address_zip        = ?,    ");
          qs.append("address_phone      = ?     ");
          qs.append("where app_seq_num  = ?     ");
          qs.append("and addresstype_code = ?   ");
      }
      else
      {
          qs.append("insert into address (      ");
          qs.append("address_name,              ");
          qs.append("address_line1,             ");
          qs.append("address_line2,             ");
          qs.append("address_city,              ");
          qs.append("countrystate_code,         ");
          qs.append("address_zip,               ");
          qs.append("address_phone,             ");
          qs.append("app_seq_num,               ");
          qs.append("addresstype_code)          ");
          qs.append("values(?,?,?,?,?,?,?,?,?)  ");
      }
      rs.close();
      ps.close();

      ps = getPreparedStatement(qs.toString());

      ps.setString(1, shippingName);
      ps.setString(2, shippingAddress1);
      ps.setString(3, shippingAddress2);
      ps.setString(4, shippingCity);
      ps.setString(5, shippingState);
      ps.setString(6, shippingZip);
      ps.setLong  (7, getDigits(shippingPhone));
      ps.setLong  (8, appSeqNum);
      ps.setString(9, EQUIP_ADD_TYPE);

      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitShippingAddress: insert failed");
        addError("submitShippingAddress: Unable to insert/update record");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitShippingAddress: " + e.toString());
      addError("submitShippingAddress: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  private void submitFeatureInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      qs.append("select app_seq_num from pos_features where app_seq_num = ? ");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      qs.setLength(0);

      if(rs.next())
      {
          qs.append("update pos_features set ");
          qs.append("access_code_flag       = ?,  ");
          qs.append("access_code            = ?,  ");
          qs.append("auto_batch_close_flag  = ?,  ");
          qs.append("auto_batch_close_time  = ?,  ");
          qs.append("header_line4_flag      = ?,  ");
          qs.append("header_line4           = ?,  ");
          qs.append("header_line5_flag      = ?,  ");
          qs.append("header_line5           = ?,  ");
          qs.append("footer_flag            = ?,  ");
          qs.append("footer                 = ?,  ");
          qs.append("reset_ref_flag         = ?,  ");
          qs.append("invoice_prompt_flag    = ?,  ");
          qs.append("fraud_control_flag     = ?,  ");
          qs.append("password_protect_flag  = ?,  ");
          qs.append("phone_training_flag    = ?,  ");
          qs.append("terminal_reminder_flag = ?,  ");
          qs.append("terminal_reminder_time = ?,  ");
          qs.append("tip_option_flag        = ?,  ");
          qs.append("clerk_enabled_flag     = ?,  ");
          qs.append("equipment_comment      = ?,  ");
          qs.append("addresstype_code       = ?,  ");
          qs.append("fraud_control_option   = ?,  ");
          qs.append("no_printer_needed      = ?,  ");
          qs.append("no_printer_owned       = ?,  ");
          qs.append("cvv2_flag              = ?,  ");
          qs.append("avs_flag               = ?,  ");
          qs.append("phone_training         = ?,  ");
          qs.append("cash_back_flag         = ?,  ");
          qs.append("cash_back_limit        = ?,  ");
          qs.append("purchasing_card_flag   = ?,  ");
          qs.append("card_truncation_flag   = ?,  ");
          qs.append("debit_surcharge_flag   = ?,  ");
          qs.append("debit_surcharge_amount = ?,  ");
          qs.append("wireless_lli_flag      = ?,  ");
          qs.append("wireless_lli_number    = ?,  ");
          qs.append("wireless_esn_flag      = ?,  ");
          qs.append("wireless_esn_number    = ?,  ");
          qs.append("customer_service_phone = ?,  ");
          qs.append("shipping_contact_name  = ?,  ");
          qs.append("shipping_method        = ?,  ");
          qs.append("shipping_comment       = ?,  ");
          qs.append("term_comm_type         = ?   ");
          qs.append("where app_seq_num      = ?   ");
      }
      else
      {
          qs.append("insert into pos_features (     ");
          qs.append("access_code_flag,access_code,  ");
          qs.append("auto_batch_close_flag,         ");
          qs.append("auto_batch_close_time,         ");
          qs.append("header_line4_flag,             ");
          qs.append("header_line4,                  ");
          qs.append("header_line5_flag,             ");
          qs.append("header_line5,                  ");
          qs.append("footer_flag,                   ");
          qs.append("footer,                        ");
          qs.append("reset_ref_flag,                ");
          qs.append("invoice_prompt_flag,           ");
          qs.append("fraud_control_flag,            ");
          qs.append("password_protect_flag,         ");
          qs.append("phone_training_flag,           ");
          qs.append("terminal_reminder_flag,        ");
          qs.append("terminal_reminder_time,        ");
          qs.append("tip_option_flag,               ");
          qs.append("clerk_enabled_flag,            ");
          qs.append("equipment_comment,             ");
          qs.append("addresstype_code,              ");
          qs.append("fraud_control_option,          ");
          qs.append("no_printer_needed,             ");
          qs.append("no_printer_owned,              ");
          qs.append("cvv2_flag,                     ");
          qs.append("avs_flag,                      ");
          qs.append("phone_training,                ");
          qs.append("cash_back_flag,                ");
          qs.append("cash_back_limit,               ");
          qs.append("purchasing_card_flag,          ");
          qs.append("card_truncation_flag,          ");
          qs.append("debit_surcharge_flag,          ");
          qs.append("debit_surcharge_amount,        ");
          qs.append("wireless_lli_flag,             ");
          qs.append("wireless_lli_number,           ");
          qs.append("wireless_esn_flag,             ");
          qs.append("wireless_esn_number,           ");
          qs.append("customer_service_phone,        ");
          qs.append("shipping_contact_name,         ");
          qs.append("shipping_method,               ");
          qs.append("shipping_comment,              ");
          qs.append("term_comm_type,                ");
          qs.append("app_seq_num)                   ");
          qs.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      }
      rs.close();
      ps.close();

      ps = getPreparedStatement(qs.toString());

      ps.setString(1,getAccessCodeFlag());
      ps.setString(2,getAccessCodeComment());
      ps.setString(3,getAutoBatchFlag());
      ps.setString(4,getBatchTime());
      ps.setString(5,getHeaderLine4Flag());
      ps.setString(6,getHeaderLine4Comment());
      ps.setString(7,getHeaderLine5Flag());
      ps.setString(8,getHeaderLine5Comment());
      ps.setString(9,getFooterFlag());
      ps.setString(10,getFooterComment());
      ps.setString(11,getResetReferenceFlag());
      ps.setString(12,getInvoicePromptFlag());
      ps.setString(13,getFraudControlFlag());
      ps.setString(14,getPasswordProtectFlag());
      ps.setString(15,getPhoneTrainingFlag());
      ps.setString(16,getTerminalReminderFlag());
      ps.setString(17,getReminderTime());
      ps.setString(18,getTipOptionFlag());
      ps.setString(19,getClerkEnabledFlag());
      ps.setString(20,getEquipmentComment());
      ps.setString(21,this.useAddress);
      ps.setString(22,getFraudControlComment());
      ps.setString(23,(isNoPrinterNeed() ? "Y" : "N"));
      ps.setString(24,(isNoPrinterOwn() ? "Y" : "N"));
      ps.setString(25,cvv2Flag);
      ps.setString(26,avsOnFlag);
      ps.setString(27,phoneTraining);
      ps.setString(28,cashBackFlag);
      ps.setString(29,cashBackLimit);
      ps.setString(30,purchasingCardFlag);
      ps.setString(31,truncationCardFlag);
      ps.setString(32,debitSurchargeFlag);
      ps.setString(33,debitSurchargeAmount);
      ps.setString(34,wirelessLliFlag);
      ps.setString(35,getWirelessLliNumber());
      ps.setString(36,wirelessEsnFlag);
      ps.setString(37,getWirelessEsnNumber());
      ps.setString(38,customerServicePhone);
      ps.setString(39,shippingContactName);
      ps.setString(40,shippingMethod);
      ps.setString(41,shippingComment);
      ps.setInt(42,ipTerminal);
      ps.setLong(43,appSeqNum);

      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitFeatureInfo: insert failed");
        addError("submitFeatureInfo: Unable to insert/update record");
      }

      if(!this.useAddress.equals(INVALID_ADD_TYPE))
      {
        submitShippingAddress(appSeqNum);
      }
      else
      {
        deleteShippingAddress(appSeqNum);
      }

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitFeatureInfo: " + e.toString());
      addError("submitFeatureInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
  }

  public String getEquipType(int i)
  {
    String result = "";
    if(!this.equipType[i].equals("-1"))
    {
      result = this.equipType[i];
    }
    return result;
  }

  public String getEquipNeeded(int i)
  {
    return this.equipNeeded[i];
  }
  public String getEquipOwned(int i)
  {
    return this.equipOwned[i];
  }

  public String getEquipNumRent(int i)
  {
    String result = "";
    if(!this.equipNumRent[i].equals("-1"))
    {
      result = this.equipNumRent[i];
    }
    return result;
  }

  public String getEquipNumBuy(int i)
  {
    String result = "";
    if(!this.equipNumBuy[i].equals("-1"))
    {
      result = this.equipNumBuy[i];
    }
    return result;
  }

  public String getEquipNumRefurb(int i)
  {
    String result = "";
    if(!this.equipNumRefurb[i].equals("-1"))
    {
      result = this.equipNumRefurb[i];
    }
    return result;
  }

  public String getEquipNumLease(int i)
  {
    String result = "";
    if(!this.equipNumLease[i].equals("-1"))
    {
      result = this.equipNumLease[i];
    }
    return result;
  }

  public String getEquipNumOwn(int i)
  {
    String result = "";
    if(!this.equipNumOwn[i].equals("-1"))
    {
      result = this.equipNumOwn[i];
    }
    return result;
  }

  public void setAccessCodeFlag(String accessCodeFlag)
  {
    this.accessCodeFlag = accessCodeFlag;
  }
  public String getAccessCodeFlag()
  {
    return this.accessCodeFlag;
  }
  public void setAutoBatchFlag(String autoBatchFlag)
  {
    this.autoBatchFlag = autoBatchFlag;
  }
  public String getAutoBatchFlag()
  {
    return this.autoBatchFlag;
  }
  public void setHeaderLine4Flag(String headerLine4Flag)
  {
    this.headerLine4Flag = headerLine4Flag;
  }
  public String getHeaderLine4Flag()
  {
    return this.headerLine4Flag;
  }
  public void setHeaderLine5Flag(String headerLine5Flag)
  {
    this.headerLine5Flag = headerLine5Flag;
  }
  public String getHeaderLine5Flag()
  {
    return this.headerLine5Flag;
  }
  public void setFooterFlag(String footerFlag)
  {
    this.footerFlag = footerFlag;
  }
  public String getFooterFlag()
  {
    return this.footerFlag;
  }
  public void setResetReferenceFlag(String resetReferenceFlag)
  {
    this.resetReferenceFlag = resetReferenceFlag;
  }
  public String getResetReferenceFlag()
  {
    return this.resetReferenceFlag;
  }
  public void setInvoicePromptFlag(String invoicePromptFlag)
  {
    this.invoicePromptFlag = invoicePromptFlag;
  }
  public String getInvoicePromptFlag()
  {
    return this.invoicePromptFlag;
  }
  public void setFraudControlFlag(String fraudControlFlag)
  {
    this.fraudControlFlag = fraudControlFlag;
  }
  public String getFraudControlFlag()
  {
    return this.fraudControlFlag;
  }
  public void setPasswordProtectFlag(String passwordProtectFlag)
  {
    this.passwordProtectFlag = passwordProtectFlag;
  }
  public String getPasswordProtectFlag()
  {
    return this.passwordProtectFlag;
  }
  public void setPhoneTrainingFlag(String phoneTrainingFlag)
  {
    this.phoneTrainingFlag = phoneTrainingFlag;
  }
  public String getPhoneTrainingFlag()
  {
    return this.phoneTrainingFlag;
  }
  public void setTerminalReminderFlag(String terminalReminderFlag)
  {
    this.terminalReminderFlag = terminalReminderFlag;
  }
  public String getTerminalReminderFlag()
  {
    return this.terminalReminderFlag;
  }
  public void setTipOptionFlag(String tipOptionFlag)
  {
    this.tipOptionFlag = tipOptionFlag;
  }
  public String getTipOptionFlag()
  {
    return this.tipOptionFlag;
  }
  public void setClerkEnabledFlag(String clerkEnabledFlag)
  {
    this.clerkEnabledFlag = clerkEnabledFlag;
  }
  public String getClerkEnabledFlag()
  {
    return this.clerkEnabledFlag;
  }

  public void setAvsOnFlag(String avsOnFlag)
  {
    this.avsOnFlag = avsOnFlag;
  }
  public String getAvsOnFlag()
  {
    return this.avsOnFlag;
  }

  public void setCvv2Flag(String cvv2Flag)
  {
    this.cvv2Flag = cvv2Flag;
  }
  public String getCvv2Flag()
  {
    return this.cvv2Flag;
  }

  public void setCashBackFlag(String cashBackFlag)
  {
    this.cashBackFlag = cashBackFlag;
  }
  public String getCashBackFlag()
  {
    return this.cashBackFlag;
  }

  public void setDebitSurchargeFlag(String debitSurchargeFlag)
  {
    this.debitSurchargeFlag = debitSurchargeFlag;
  }
  public String getDebitSurchargeFlag()
  {
    return this.debitSurchargeFlag;
  }

  public void setPurchasingCardFlag(String purchasingCardFlag)
  {
    this.purchasingCardFlag = purchasingCardFlag;
  }
  public String getPurchasingCardFlag()
  {
    return this.purchasingCardFlag;
  }

  public void setTruncationCardFlag(String truncationCardFlag)
  {
    this.truncationCardFlag = truncationCardFlag;
  }
  public String getTruncationCardFlag()
  {
    return this.truncationCardFlag;
  }

  public String getPhoneTraining()
  {
    return this.phoneTraining;
  }
  public void setPhoneTraining(String phoneTraining)
  {
    this.phoneTraining = phoneTraining;
  }

  public String getTerminalInsurance()
  {
    return this.terminalInsurance;
  }
  public void setTerminalInsurance(String terminalInsurance)
  {
    this.terminalInsurance = terminalInsurance;
  }

  public String getTerminalTrack()
  {
    return this.terminalTrack;
  }
  public void setTerminalTrack(String terminalTrack)
  {
    this.terminalTrack = terminalTrack;
  }

  public String getTerminalPrinter()
  {
    return this.terminalPrinter;
  }
  public void setTerminalPrinter(String terminalPrinter)
  {
    this.terminalPrinter = terminalPrinter;
  }

  public String getCashBackLimit()
  {
    return this.cashBackLimit;
  }
  public void setCashBackLimit(String cashBackLimit)
  {
    this.cashBackLimit = cashBackLimit;
  }

  public String getDebitSurchargeAmount()
  {
    return this.debitSurchargeAmount;
  }
  public void setDebitSurchargeAmount(String debitSurchargeAmount)
  {
    this.debitSurchargeAmount = debitSurchargeAmount;
  }

  public void setAccessCodeComment(String accessCodeComment)
  {
    try
    {
      long temp = Long.parseLong(accessCodeComment);
      this.accessCodeComment = accessCodeComment;
    }
    catch(Exception e)
    {
      this.accessCodeComment = "-1";
    }
  }
  public String getAccessCodeComment()
  {
    String result = "";
    if(!this.accessCodeComment.equals("-1"))
    {
      result = this.accessCodeComment;
    }
    return result;
  }

  public void setWirelessLliNumber(String wirelessLliNumber)
  {
    this.wirelessLliNumber = wirelessLliNumber;
  }
  public String getWirelessLliNumber()
  {
    String result = "";
    if(!this.wirelessLliNumber.equals("-1"))
    {
      result = this.wirelessLliNumber;
    }
    return result;
  }

  public void setWirelessLliFlag(String wirelessLliFlag)
  {
    this.wirelessLliFlag = wirelessLliFlag;
  }
  public String getWirelessLliFlag()
  {
    String result = "";
    if(!this.wirelessLliFlag.equals("-1"))
    {
      result = this.wirelessLliFlag;
    }
    return result;
  }

  public void setIpTerminal(String ipTerminal)
  {
    if(ipTerminal != null && ipTerminal.equals("Y"))
    {
      this.ipTerminal = mesConstants.TERM_COMM_TYPE_IP;
    }
  }
  public String getIpTerminal()
  {
    return (this.ipTerminal == mesConstants.TERM_COMM_TYPE_IP ? "Y" : "N");
  }

  public void setWirelessEsnNumber(String wirelessEsnNumber)
  {
    this.wirelessEsnNumber = wirelessEsnNumber;
  }
  public String getWirelessEsnNumber()
  {
    String result = "";
    if(!this.wirelessEsnNumber.equals("-1"))
    {
      result = this.wirelessEsnNumber;
    }
    return result;
  }

  public void setWirelessEsnFlag(String wirelessEsnFlag)
  {
    this.wirelessEsnFlag = wirelessEsnFlag;
  }
  public String getWirelessEsnFlag()
  {
    String result = "";
    if(!this.wirelessEsnFlag.equals("-1"))
    {
      result = this.wirelessEsnFlag;
    }
    return result;
  }

  public void setAutoBatchHhComment(String autoBatchHhComment)
  {
    try
    {
      int temp = Integer.parseInt(autoBatchHhComment);
      if(temp > 12 || temp <= 0)
      {
        this.autoBatchHhComment = "-1";
      }
      else
      {
        this.autoBatchHhComment = autoBatchHhComment;
      }
    }
    catch(Exception e)
    {
      this.autoBatchHhComment = "-1";
    }
  }
  public String getAutoBatchHhComment()
  {
    String result = "";
    if(!this.autoBatchHhComment.equals("-1"))
    {
      result = this.autoBatchHhComment;
    }
    return result;
  }
  public void setAutoBatchMmComment(String autoBatchMmComment)
  {
    try
    {
      int temp = Integer.parseInt(autoBatchMmComment);
      if(temp > 59 || temp < 0)
      {
        this.autoBatchMmComment = "-1";
      }
      else
      {
        this.autoBatchMmComment = autoBatchMmComment;
      }
    }
    catch(Exception e)
    {
      this.autoBatchMmComment = "-1";
    }
  }
  public String getAutoBatchMmComment()
  {
    String result = "";
    if(!this.autoBatchMmComment.equals("-1"))
    {
      result = this.autoBatchMmComment;
    }
    return result;
  }


  public void setTerminalReminderHhComment(String terminalReminderHhComment)
  {
    try
    {
      int temp = Integer.parseInt(terminalReminderHhComment);
      if(temp > 12 || temp <= 0)
      {
        this.terminalReminderHhComment = "-1";
      }
      else
      {
        this.terminalReminderHhComment = terminalReminderHhComment;
      }
    }
    catch(Exception e)
    {
      this.terminalReminderHhComment = "-1";
    }
  }
  public String getTerminalReminderHhComment()
  {
    String result = "";
    if(!this.terminalReminderHhComment.equals("-1"))
    {
      result = this.terminalReminderHhComment;
    }
    return result;
  }
  public void setTerminalReminderMmComment(String terminalReminderMmComment)
  {
    try
    {
      int temp = Integer.parseInt(terminalReminderMmComment);
      if(temp > 59 || temp < 0)
      {
        this.terminalReminderMmComment = "-1";
      }
      else
      {
        this.terminalReminderMmComment = terminalReminderMmComment;
      }
    }
    catch(Exception e)
    {
      this.terminalReminderMmComment = "-1";
    }
  }
  public String getTerminalReminderMmComment()
  {
    String result = "";
    if(!this.terminalReminderMmComment.equals("-1"))
    {
      result = this.terminalReminderMmComment;
    }
    return result;
  }

  public void setTerminalReminderAmPmComment(String terminalReminderAmPmComment)
  {
    if(!isBlank(terminalReminderAmPmComment))
    {
      this.terminalReminderAmPmComment = terminalReminderAmPmComment;
    }
    else
    {
      this.terminalReminderAmPmComment = "-1";
    }
  }
  public String getTerminalReminderAmPmComment()
  {
    return this.terminalReminderAmPmComment;
  }

  public void setAutoBatchAmPmComment(String autoBatchAmPmComment)
  {
    if(!isBlank(autoBatchAmPmComment))
    {
      this.autoBatchAmPmComment = autoBatchAmPmComment;
    }
    else
    {
      this.autoBatchAmPmComment = "-1";
    }
  }
  public String getAutoBatchAmPmComment()
  {
    return this.autoBatchAmPmComment;
  }

  public void setHeaderLine4Comment(String headerLine4Comment)
  {
    if(!isBlank(headerLine4Comment))
    {
      this.headerLine4Comment = headerLine4Comment;
    }
    else
    {
      this.headerLine4Comment = "-1";
    }
  }
  public String getHeaderLine4Comment()
  {
    String result = "";
    if(!this.headerLine4Comment.equals("-1"))
    {
      result = this.headerLine4Comment;
    }
    return result;
  }
  public void setHeaderLine5Comment(String headerLine5Comment)
  {
    if(!isBlank(headerLine5Comment))
    {
      this.headerLine5Comment = headerLine5Comment;
    }
    else
    {
      this.headerLine5Comment = "-1";
    }
  }
  public String getHeaderLine5Comment()
  {
    String result = "";
    if(!this.headerLine5Comment.equals("-1"))
    {
      result = this.headerLine5Comment;
    }
    return result;
  }
  public void setFooterComment(String footerComment)
  {
    if(!isBlank(footerComment))
    {
      this.footerComment = footerComment;
    }
    else
    {
      this.footerComment = "-1";
    }
  }
  public String getFooterComment()
  {
    String result = "";
    if(!this.footerComment.equals("-1"))
    {
      result = this.footerComment;
    }
    return result;
  }

  public void setFraudControlComment(String fraudControlComment)
  {
    if(!isBlank(fraudControlComment))
    {
      this.fraudControlComment = fraudControlComment;
    }
    else
    {
      this.fraudControlComment = "-1";
    }
  }
  public String getFraudControlComment()
  {
    String result = "";
    if(!this.fraudControlComment.equals("-1"))
    {
      result = this.fraudControlComment;
    }
    return result;
  }


  public void setEquipmentComment(String equipmentComment)
  {
    if(!isBlank(equipmentComment))
    {
      this.equipmentComment = equipmentComment;
    }
    else
    {
      this.equipmentComment = "-1";
    }
  }
  public String getEquipmentComment()
  {
    String result = "";
    if(!this.equipmentComment.equals("-1"))
    {
      result = this.equipmentComment;
    }
    return result;
  }

  private String getBatchTime()
  {
    String result = "";
    if(!this.autoBatchHhComment.equals("-1") && !this.autoBatchMmComment.equals("-1") && !this.autoBatchAmPmComment.equals("-1"))
    {
      result = this.autoBatchHhComment + ":" + this.autoBatchMmComment + " " + this.autoBatchAmPmComment;
    }
    return result;
  }
  private String getReminderTime()
  {
    String result = "";
    if(!this.terminalReminderHhComment.equals("-1") && !this.terminalReminderMmComment.equals("-1") && !this.terminalReminderAmPmComment.equals("-1"))
    {
      result = this.terminalReminderHhComment + ":" + this.terminalReminderMmComment + " " + this.terminalReminderAmPmComment;
    }
    return result;
  }

  private void setBatchTime(String time)
  {
    if(isBlank(time))
    {
      return;
    }
    if(time.length() >= 7)
    {
      int index   = 0;

      index = time.indexOf(':');
      this.autoBatchHhComment   = time.substring(0,index);
      this.autoBatchMmComment   = time.substring(index+1, index+3);
      this.autoBatchAmPmComment = time.substring(index+4);
    }
  }
  private void setReminderTime(String time)
  {
    if(isBlank(time))
    {
      return;
    }
    if(time.length() >= 7)
    {
      int index   = 0;

      index = time.indexOf(':');
      this.terminalReminderHhComment   = time.substring(0,index);
      this.terminalReminderMmComment   = time.substring(index+1, index+3);
      this.terminalReminderAmPmComment = time.substring(index+4);
    }
  }

  public String getLeaseMonths()
  {
    return this.leaseMonths;
  }
  public String getLeaseCompany()
  {
    return this.leaseCompany;
  }
  public String getLeaseActNum()
  {
    return this.leaseActNum;
  }
  public String getLeaseAuthNum()
  {
    return this.leaseAuthNum;
  }

  public String getLeaseAmt()
  {
    //return formatCurr(this.leaseAmt);
    return this.leaseAmt;
  }
  public String getFundingAmt()
  {
    //return formatCurr(this.fundingAmt);
    return this.fundingAmt;
  }

  public String formatCurr(String curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }

  public String getCurr(String num)
  {
    String result = "";
    try
    {
      if(validCurr(num))
      {
        if(!num.startsWith("$"))
        {
          num = "$" + num;
        }
        NumberFormat  nf       = NumberFormat.getCurrencyInstance(Locale.US);
        double        temp     = (nf.parse(num)).doubleValue();
                      result   = Double.toString(temp);
      }
     else
      {
        return num;
      }
    }
    catch(Exception e)
    {}

    return result;
  }

  public boolean validCurr(String raw)
  {
    boolean result  = true;
    int     sign    = 0;
    int     decimal = 0;

    try
    {
      if(!raw.startsWith("$"))
      {
        raw = "$" + raw;
      }

      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == ',') //let digits and commas slide
        {
        }
        else if(raw.charAt(i) == '$')
        {
          sign++;
        }
        else if(raw.charAt(i) == '.')
        {
          decimal++;
        }
        else
        {
          result = false;
        }
      }

      if(sign > 1 || decimal > 1)
      {
        result = false;
      }
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }

  public void setLeaseAmt(String leaseAmt)
  {
    this.leaseAmt = getCurr(leaseAmt);
  }
  public void setFundingAmt(String fundingAmt)
  {
    this.fundingAmt = getCurr(fundingAmt);
  }

  public void setLeaseCompany(String leaseCompany)
  {
    this.leaseCompany = leaseCompany;
  }

  public void setLeaseAuthNum(String leaseAuthNum)
  {
    this.leaseAuthNum = leaseAuthNum;
  }

  public void setLeaseActNum(String leaseActNum)
  {
    String result = "";
    if(isNumber(leaseActNum))
    {
      result = leaseActNum;
    }
    this.leaseActNum = result;
  }

  public void setLeaseMonths(String leaseMonths)
  {
    try
    {
      int temp = Integer.parseInt(leaseMonths);
      this.leaseMonths = leaseMonths;
    }
    catch(Exception e)
    {
      this.leaseMonths = "";
    }
  }

  public String getCustomerServicePhone()
  {
    return formatPhone(this.customerServicePhone);
  }
  public void setCustomerServicePhone(String customerServicePhone)
  {
    this.customerServicePhone = removeNonDigits(customerServicePhone);
  }

  public String removeNonDigits(String raw)
  {
    StringBuffer  digits      = new StringBuffer("");

    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }
    }
    catch(Exception e)
    {
    }

    return digits.toString();
  }


  public String getUseAddress()
  {
    return this.useAddress;
  }
  public String getShippingName()
  {
    return this.shippingName;
  }
  public String getShippingContactName()
  {
    return this.shippingContactName;
  }
  public String getShippingPhone()
  {
    return this.shippingPhone;
  }
  public String getShippingMethod()
  {
    return this.shippingMethod;
  }
  public String getShippingComment()
  {
    return this.shippingComment;
  }


  public String getShippingAddress1()
  {
    return this.shippingAddress1;
  }
  public String getShippingAddress2()
  {
    return this.shippingAddress2;
  }
  public String getShippingCity()
  {
    return this.shippingCity;
  }
  public String getShippingState()
  {
    return this.shippingState;
  }
  public String getShippingZip()
  {
    return this.shippingZip;
  }

  public void setUseAddress(String temp)
  {
    this.useAddress = temp;
  }
  public void setShippingName(String temp)
  {
    this.shippingName = temp;
  }

  public void setShippingContactName(String shippingContactName)
  {
    this.shippingContactName = shippingContactName;
  }
  public void setShippingPhone(String shippingPhone)
  {
    this.shippingPhone = shippingPhone;
  }
  public void setShippingMethod(String shippingMethod)
  {
    this.shippingMethod =  shippingMethod;
  }
  public void setShippingComment(String shippingComment)
  {
    this.shippingComment = shippingComment;
  }

  public void setShippingAddress1(String temp)
  {
    this.shippingAddress1 = temp;
  }
  public void setShippingAddress2(String temp)
  {
    this.shippingAddress2 = temp;
  }
  public void setShippingCity(String temp)
  {
    this.shippingCity = temp;
  }
  public void setShippingState(String temp)
  {
    this.shippingState = temp;
  }
  public void setShippingZip(String temp)
  {
    this.shippingZip = temp;
  }
  public boolean isDialPay()
  {
    return dialPay;
  }

  public boolean isVirtualTerminal()
  {
    return virtualTerminal;
  }

  public boolean isNoTraining()
  {
    return noTraining;
  }

  public void setDialPay(String temp)
  {
    dialPay = true;
  }
  public boolean isRetail()
  {
    return retail;
  }
  public boolean isMoto()
  {
    return moto;
  }

  public boolean isCashAdvance()
  {
    return cashAdvance;
  }

  public String getLongImpPlateQty()
  {
    return this.longImpPlateQty;
  }
  public void setLongImpPlateQty(String longImpPlateQty)
  {
    this.longImpPlateQty = longImpPlateQty;
  }

  public String getLongImpPlateDesc()
  {
    return this.longImpPlateDesc;
  }
  public void setLongImpPlateDesc(String longImpPlateDesc)
  {
    this.longImpPlateDesc = longImpPlateDesc;
  }

  public String getShortImpPlateQty()
  {
    return this.shortImpPlateQty;
  }
  public void setShortImpPlateQty(String shortImpPlateQty)
  {
    this.shortImpPlateQty = shortImpPlateQty;
  }

  public String getShortImpPlateDesc()
  {
    return this.shortImpPlateDesc;
  }
  public void setShortImpPlateDesc(String shortImpPlateDesc)
  {
    this.shortImpPlateDesc = shortImpPlateDesc;
  }


  public String getBusinessPhone()
  {
    return this.businessPhone;
  }
  public String getBusinessReturnPolicy()
  {
    return this.businessReturnPolicy;
  }
  public String getBusinessName()
  {
    return this.businessName;
  }

  public String getContactName()
  {
    return this.contactName;
  }

  public String getBusinessAddress()
  {
    return this.businessAddress;
  }

  public boolean isLeasingEquip()
  {
    return leasingEquip;
  }

  public Vector getLeaseComOptions()
  {
    return companyOptions;
  }

  public Vector getLeaseComOptionCode()
  {
    return companyOptionCode;
  }

  public Vector getLeaseMonOptions()
  {
    return monthOptions;
  }

  public String getImprinterPlates()
  {
    String result = "";
    if(this.imprinterPlates > 0)
    {
      result = String.valueOf(this.imprinterPlates);
    }

    return result;
  }
  public void setImprinterPlates(String imprinterPlates)
  {
    try
    {
      this.imprinterPlates = Integer.parseInt(imprinterPlates);
    }
    catch(Exception e)
    {
      this.imprinterPlates = 0;
    }
  }
  public void setAppType(String appType)
  {
    try
    {
      this.appType = Integer.parseInt(appType);
    }
    catch(Exception e)
    {
      this.appType = 0;
    }
  }

  public boolean isNoPrinterNeed()
  {
    return this.noPrinterNeed;
  }
  public boolean isNoPrinterOwn()
  {
    return this.noPrinterOwn;
  }

  public void setNoPrinterNeed(String temp)
  {
    this.noPrinterNeed = true;
  }
  public void setNoPrinterOwn(String temp)
  {
    this.noPrinterOwn = true;
  }

  public boolean isRecalculated()
  {
    return this.recalculate;
  }

  public void setRecalculate(String recalculate)
  {
    this.recalculate = true;
  }
  public boolean getRecalculate()
  {
    return this.recalculate;
  }
  public String getTarget()
  {
    String target = "";
    if(this.recalculate == true)
      target="address";
    return target;
  }



}/*@lineinfo:generated-code*/