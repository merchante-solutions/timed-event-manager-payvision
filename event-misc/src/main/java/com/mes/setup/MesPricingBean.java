/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/MesPricingBean.java $

  Description:  

   
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-09-24 11:58:34 -0700 (Wed, 24 Sep 2008) $
  Version            : $Revision: 15385 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.setup;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;

public class MesPricingBean extends com.mes.screens.SequenceDataBean
{
  public final static int NUM_CARDS               = 4;
  public final static int NUM_MISC                = 4;
  private final static int[] cardCodes            = new int[NUM_CARDS];
  private final static int[] miscCodes            = new int[NUM_MISC];

  public final static int MISC_FEE_SETUP              = 0;
  public final static int MISC_FEE_CHARGEBACK         = 1;
  public final static int MISC_FEE_MONTHLY_STATEMENT  = 2;
  public final static int MISC_FEE_MIN_MONTHLY        = 3;
  public final static int CARD_FEE_DINERS             = 0;
  public final static int CARD_FEE_DISCOVER           = 1;
  public final static int CARD_FEE_JCB                = 2;
  public final static int CARD_FEE_AMEX               = 3;

  public final static String DEFAULT_PARTNER_ID       = "1026";

  private String[]  cardDesc                      = new String[NUM_CARDS];
  private String[]  cardFee                       = new String[NUM_CARDS];
  private String[]  cardSelectedFlag              = new String[NUM_CARDS];
  private String[]  miscDesc                      = new String[NUM_MISC];
  private String[]  miscDesc2                     = new String[NUM_MISC];
  private String[]  miscFee                       = new String[NUM_MISC];
  
  private String discountRate             = "-1";
  private String vmTransFee               = "-1";
  private String midQualDg                = "-1";
  private String qualComm                 = "-1";
  private String commDgAdd                = "-1";
  private String nonQualDg                = "-1";
  private String nonQualDgAdd             = "-1";
  private String agreeMerch               = "N";
  private String agreePrice               = "N";
  private String owner1LastName           = "";
  private String owner1FirstName          = "";
  private String owner1Title              = "";
  private String owner2LastName           = "";
  private String owner2FirstName          = "";
  private String owner2Title              = "";

/*
  * Default Constructor .
*/      

  public MesPricingBean() 
  { 
    super.init();

    //****************** static stuff *********************
    this.midQualDg    = "0.80";
    this.nonQualDg    = "1.95";
    this.nonQualDgAdd = "0.10";
    this.qualComm     = "0.55";
    this.commDgAdd    = "0.10";

    miscDesc[MISC_FEE_SETUP]              = "Setup Fee";
    miscDesc2[MISC_FEE_SETUP]             = "&nbsp;";
    miscCodes[MISC_FEE_SETUP]             = mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_SETUP;
    miscFee[MISC_FEE_SETUP]               = "-1"; //initialize

    miscDesc[MISC_FEE_CHARGEBACK]         = "Chargeback Fee";
    miscDesc2[MISC_FEE_CHARGEBACK]        = "per item";
    miscCodes[MISC_FEE_CHARGEBACK]        = mesConstants.APP_MISC_CHARGE_CHARGEBACK;
    miscFee[MISC_FEE_CHARGEBACK]          = "-1"; //initialize

    miscDesc[MISC_FEE_MONTHLY_STATEMENT]  = "Monthly Service Fee";
    miscDesc2[MISC_FEE_MONTHLY_STATEMENT] = "&nbsp;";
    miscCodes[MISC_FEE_MONTHLY_STATEMENT] = mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT;
    miscFee[MISC_FEE_MONTHLY_STATEMENT]   = "-1"; //initialize

    miscDesc[MISC_FEE_MIN_MONTHLY]        = "Minimum Monthly Fee";
    miscDesc2[MISC_FEE_MIN_MONTHLY]       = "&nbsp;";
    miscCodes[MISC_FEE_MIN_MONTHLY]       = 0; //not a misc fee so we use an invalid misc fee type... 
    miscFee[MISC_FEE_MIN_MONTHLY]         = "-1"; //initialize

    cardSelectedFlag[CARD_FEE_DINERS]     = "N";
    cardDesc[CARD_FEE_DINERS]             = "Diners Club/Carte Blanche";
    cardCodes[CARD_FEE_DINERS]            = mesConstants.APP_CT_DINERS_CLUB;
    cardFee[CARD_FEE_DINERS]              = "-1"; //initialize

    cardSelectedFlag[CARD_FEE_DISCOVER]   = "N";
    cardDesc[CARD_FEE_DISCOVER]           = "Discover/Novus";
    cardCodes[CARD_FEE_DISCOVER]          = mesConstants.APP_CT_DISCOVER;
    cardFee[CARD_FEE_DISCOVER]            = "-1"; //initialize

    cardSelectedFlag[CARD_FEE_JCB]        = "N";
    cardDesc[CARD_FEE_JCB]                = "JCB";
    cardCodes[CARD_FEE_JCB]               = mesConstants.APP_CT_JCB;
    cardFee[CARD_FEE_JCB]                 = "-1"; //initialize

    cardSelectedFlag[CARD_FEE_AMEX]       = "N";
    cardDesc[CARD_FEE_AMEX]               = "American Express";
    cardCodes[CARD_FEE_AMEX]              = mesConstants.APP_CT_AMEX;
    cardFee[CARD_FEE_AMEX]                = "-1"; //initialize
    //******************* end static stuff *********************


    //get default pricing from vs_partner_pricing table.. based on default partner id
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
   
    try 
    {
      qs.append("select * from vs_partner_pricing ");
      qs.append("where partner_id = ? and active = ? ");

      ps = getPreparedStatement(qs.toString());
      ps.setString(1, DEFAULT_PARTNER_ID);
      ps.setString(2, "Y");

      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.discountRate                   = isBlank(rs.getString("vmc_discount"))   ? "" : rs.getString("vmc_discount");
        this.vmTransFee                     = isBlank(rs.getString("vmc_pertran"))    ? "" : rs.getString("vmc_pertran");
        miscFee[MISC_FEE_SETUP]             = isBlank(rs.getString("setup_fee"))      ? "" : rs.getString("setup_fee");
        miscFee[MISC_FEE_CHARGEBACK]        = isBlank(rs.getString("chargeback_fee")) ? "" : rs.getString("chargeback_fee");
        miscFee[MISC_FEE_MONTHLY_STATEMENT] = isBlank(rs.getString("mon_svc_fee"))    ? "" : rs.getString("mon_svc_fee");
        miscFee[MISC_FEE_MIN_MONTHLY]       = isBlank(rs.getString("mon_min_fee"))    ? "" : rs.getString("mon_min_fee");

        cardFee[CARD_FEE_DINERS]            = isBlank(rs.getString("te_pertran"))     ? "" : rs.getString("te_pertran");
        cardFee[CARD_FEE_DISCOVER]          = isBlank(rs.getString("te_pertran"))     ? "" : rs.getString("te_pertran");
        cardFee[CARD_FEE_JCB]               = isBlank(rs.getString("te_pertran"))     ? "" : rs.getString("te_pertran");
        cardFee[CARD_FEE_AMEX]              = isBlank(rs.getString("te_pertran"))     ? "" : rs.getString("te_pertran");
      }
  
    }
    catch (Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::constructor()", e.toString());
    }
  }
  
  private void getPricing(long appSeqNum) 
  { 
      
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
   
    try 
    {
      qs.append("select price.* from vs_partner_pricing price, vs_linking_table link ");
      qs.append("where link.app_seq_num = ? and link.pid = price.partner_id and price.active = ? ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setString(2, "Y");

      rs = ps.executeQuery();
      
      if(rs.next()) //if partner has separate pricing we replace default pricing with the partner specific pricing
      {
        this.discountRate                   = isBlank(rs.getString("vmc_discount"))   ? "" : rs.getString("vmc_discount");
        this.vmTransFee                     = isBlank(rs.getString("mc_pertran"))    ? "" : rs.getString("vmc_pertran");

        miscFee[MISC_FEE_SETUP]             = isBlank(rs.getString("setup_fee"))      ? "" : rs.getString("setup_fee");
        miscFee[MISC_FEE_CHARGEBACK]        = isBlank(rs.getString("chargeback_fee")) ? "" : rs.getString("chargeback_fee");
        miscFee[MISC_FEE_MONTHLY_STATEMENT] = isBlank(rs.getString("mon_svc_fee"))    ? "" : rs.getString("mon_svc_fee");
        miscFee[MISC_FEE_MIN_MONTHLY]       = isBlank(rs.getString("mon_min_fee"))    ? "" : rs.getString("mon_min_fee");

        cardFee[CARD_FEE_DINERS]            = isBlank(rs.getString("te_pertran"))     ? "" : rs.getString("te_pertran");
        cardFee[CARD_FEE_DISCOVER]          = isBlank(rs.getString("te_pertran"))     ? "" : rs.getString("te_pertran");
        cardFee[CARD_FEE_JCB]               = isBlank(rs.getString("te_pertran"))     ? "" : rs.getString("te_pertran");
        cardFee[CARD_FEE_AMEX]              = isBlank(rs.getString("te_pertran"))     ? "" : rs.getString("te_pertran");
      }
    }
    catch (Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getPricing(" + appSeqNum + ")", e.toString());
    }

  }

  public int getAppType(long appSeqNum)
  {
    int appType = 0;
    
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    
    try
    {
      qs.append("select app_type ");
      qs.append("from   application ");
      qs.append("where  app_seq_num = ?");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ResultSet rs = ps.executeQuery();
      
      if(rs.next())
      {
        appType = rs.getInt("app_type");
      }
      
      rs.close();
      ps.close();
    }
    catch (Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getAppType(" + appSeqNum + ")", e.toString());
    }
    
    return appType;
  }

  //this method gets the pricing and misc fee description for a particular demo..
  public void getDemoInfo(long userId)  
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
   
    try 
    {
      qs.append("select ");
      qs.append("transaction_fee, ");
      qs.append("discount_rate_fee, ");
      qs.append("midqual_dg, ");
      qs.append("nonqual_dg, ");
      qs.append("misc_lable1, ");
      qs.append("misc_lable2, ");
      qs.append("misc_lable3, ");
      qs.append("misc_lable4, ");
      qs.append("misc_fee1, ");
      qs.append("misc_fee2, ");
      qs.append("misc_fee3, ");
      qs.append("misc_fee4 ");
      qs.append("from sequence_demo_info ");
      qs.append("where user_id = ?");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, userId);

      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.discountRate = isBlank(rs.getString("discount_rate_fee")) ? this.discountRate : rs.getString("discount_rate_fee");
        this.vmTransFee   = isBlank(rs.getString("transaction_fee")) ? this.vmTransFee : rs.getString("transaction_fee");
        this.midQualDg    = isBlank(rs.getString("midqual_dg")) ? this.midQualDg : rs.getString("midqual_dg");
        this.nonQualDg    = isBlank(rs.getString("nonqual_dg")) ? this.nonQualDg : rs.getString("nonqual_dg");

        for(int i=0; i<this.NUM_MISC; i++)
        {
          switch(i)
          {
            case 0:
              miscDesc[i]       = isBlank(rs.getString("misc_lable1")) ? this.miscDesc[i] : rs.getString("misc_lable1");
              miscDesc2[i]      = "&nbsp;";
              miscCodes[i]      = 2;
              miscFee[i]        = isBlank(rs.getString("misc_fee1")) ? this.miscFee[i] : rs.getString("misc_fee1");
              break;
            case 1:
              miscDesc[i]       = isBlank(rs.getString("misc_lable2")) ? this.miscDesc[i] : rs.getString("misc_lable2");
              miscDesc2[i]      = "per item";
              miscCodes[i]      = 5;
              miscFee[i]        = isBlank(rs.getString("misc_fee2")) ? this.miscFee[i] : rs.getString("misc_fee2");
              break;
            case 2:
              miscDesc[i]       = isBlank(rs.getString("misc_lable3")) ? this.miscDesc[i] : rs.getString("misc_lable3");
              miscDesc2[i]      = "&nbsp;";
              miscCodes[i]      = 7;
              miscFee[i]        = isBlank(rs.getString("misc_fee3")) ? this.miscFee[i] : rs.getString("misc_fee3");
              break;
            case 3:
              miscDesc[i]       = isBlank(rs.getString("misc_lable4")) ? this.miscDesc[i] : rs.getString("misc_lable4");
              miscDesc2[i]      = "&nbsp;";
              miscCodes[i]      = 0;
              miscFee[i]        = isBlank(rs.getString("misc_fee4")) ? this.miscFee[i] : rs.getString("misc_fee4");
              break;
            default:
              break;
          }
        }
      }
      rs.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDemoInfo: " + e.toString());
      addError("getDemoInfo: " + e.toString());
    }
  }
  
  
  public void getData(long appSeqNum)
  {
    getPricing(appSeqNum); //sets up pricing for given partner id.. should come first.. before submit methods
    getCardInfo(appSeqNum);
    getBusinessOwnerData(appSeqNum);
    submitCardInfo(appSeqNum);
    submitMiscInfo(appSeqNum);
  }


  /**
   *  This method takes the sequence no. as the argument and is used
   *  to get all the details of the card types selected by the merchant
   *  on the Payment Options page.
  */
  private void getCardInfo(long appSeqNum)  
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    PreparedStatement ps1     = null;
    ResultSet         rs      = null;
    ResultSet         rs1     = null;
    int               i       = 0;
    
    try 
    {
      qs.append("select cardtype_code from merchpayoption ");
      qs.append("where app_seq_num = ? and cardtype_code not in (1,4,19) ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();
      
      while(rs.next())
      {
        i=0;
        while(i < this.NUM_CARDS && rs.getInt("cardtype_code") != this.cardCodes[i])
        {
          i+=1;
        }
        if(i < this.NUM_CARDS)
        {
          this.cardSelectedFlag[i] = "Y";
        }
      }
      rs.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCardInfo: " + e.toString());
      addError("getCardInfo: " + e.toString());
    }
  }
  /*
  ** METHOD getBusinessOwnerData
  */
  private void getBusinessOwnerData(long appSeqNum)
  {
    StringBuffer      qs  = new StringBuffer("");
    PreparedStatement ps  = null;
    ResultSet         rs  = null;
    
    try
    {
      qs.append("select busowner_last_name, busowner_first_name, busowner_title ");
      qs.append("from businessowner where ");
      qs.append("app_seq_num = ? and busowner_num = ?");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      // business owner 1
      ps.setInt (2, 1);
      rs = ps.executeQuery();
      if (rs.next())
      {
        owner1LastName    = rs.getString("busowner_last_name");
        owner1FirstName   = rs.getString("busowner_first_name");
        owner1Title       = isBlank(rs.getString("busowner_title")) ? "-1" : rs.getString("busowner_title");
      }
      rs.close();
      
      // business owner 2
      ps.setInt(2, 2);
      rs = ps.executeQuery();
      if (rs.next())
      {
        owner2LastName    = isBlank(rs.getString("busowner_last_name")) ? "-1" :  rs.getString("busowner_last_name");
        owner2FirstName   = isBlank(rs.getString("busowner_first_name")) ? "-1" : rs.getString("busowner_first_name");
        owner2Title       = isBlank(rs.getString("busowner_title")) ? "-1" : rs.getString("busowner_title");
      }
      rs.close();
      ps.close();
    }
    catch (Exception e)
    {
      addError("getBusinessOwnerData: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
               "getBusinessOwnerData: " + e.toString());
    }
  }
  
  private void submitMiscInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;
    
    try
    {
      //delete all miscchrg info from miscchrg -> insert new info
      qs.append("delete from miscchrg ");
      qs.append("where app_seq_num = ? ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong  (1, appSeqNum);
       
      ps.executeUpdate();
      ps.close();
 
      qs.setLength(0);
      qs.append("insert into miscchrg(misc_chrg_amount,app_seq_num,misc_code) values (?,?,?)");

      ps = getPreparedStatement(qs.toString());
      
      int appType = getAppType(appSeqNum);

      for (i=0; i < this.NUM_MISC-1; i++) 
      {
        // NSI doesn't get the $50 setup fee!!
        if(this.miscCodes[i] != mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_SETUP || appType != mesConstants.APP_TYPE_NSI)
        {
          ps.clearParameters();
          ps.setString(1, this.miscFee[i]);
          ps.setLong(2, appSeqNum);
          ps.setInt(3, this.miscCodes[i]);
      
          //if the fee is zero.. dont add charge to database
          if(feeGreaterThanZero(this.miscFee[i]) && ps.executeUpdate() != 1)
          {
            com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: insert failed");
            addError("submitData4: Unable to insert/update record");
          }
        }
      }
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData4: " + e.toString());
      addError("submitData4: " + e.toString());
    }
  }

  private boolean feeGreaterThanZero(String fee)
  {
    boolean result = false;
    try
    {
      double temp = Double.parseDouble(fee);
      if(temp > 0.0)
      {
        result = true;
      }
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }

  private void submitCardInfo(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;

    try 
    {
      //delete all card info from tranchrg -> insert new info
      qs.append("delete from tranchrg ");
      qs.append("where app_seq_num = ? ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong  (1, appSeqNum);
       
      ps.executeUpdate();
      ps.close();
 
      qs.setLength(0);
      qs.append("Insert into tranchrg(          ");
      qs.append("tranchrg_mmin_chrg,            ");
      qs.append("tranchrg_discrate_type,        ");
      qs.append("tranchrg_disc_rate,            ");
      //qs.append("tranchrg_pass_thru,            ");
      qs.append("tranchrg_per_tran,             ");
      qs.append("tranchrg_float_disc_flag,      ");
      qs.append("app_seq_num,                   ");
      qs.append("cardtype_code,                 ");
      qs.append("mid_qualification_downgrade,   ");
      qs.append("non_qualification_downgrade )  ");
      qs.append("values(?,?,?,?,?,?,?,?,?)      ");
      
      ps = getPreparedStatement(qs.toString());

      // The values for updating the data for Visa card.
      ps.setString(1,getMiscFee(MISC_FEE_MIN_MONTHLY)); //transaction min charge
      ps.setInt(2,mesConstants.APP_PS_FIXED_RATE); //discount rate type
      ps.setString(3,this.discountRate );
      
      //ps.setString(4,this.vmTransFee);
      
      ps.setString(4,"0");
      ps.setString(5,"N");
      ps.setLong(6, appSeqNum);
      ps.setInt(7,mesConstants.APP_CT_VISA); //cardtype code
      ps.setString(8,this.midQualDg);
      ps.setString(9,this.nonQualDg);

      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData1: insert failed");
        addError("submitData1: Unable to insert/update record");
      }
    
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData1: " + e.toString());
      addError("submitData1: " + e.toString());
    }
    try
    {
      // The values for updating data for Master Card.
      ps.setInt(7,mesConstants.APP_CT_MC); //  change card type code, all else same
      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData2: insert failed");
        addError("submitData2: Unable to insert/update record");
      }
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData2: " + e.toString());
      addError("submitData2: " + e.toString());
    }
    try
    {
      qs.setLength(0);
      qs.append("Insert into tranchrg(tranchrg_per_tran,app_seq_num,cardtype_code) ");
      qs.append("values(?,?,?)");
       
      ps = getPreparedStatement(qs.toString());
      
      for ( i = 0; i < this.NUM_CARDS; i++ ) 
      {
         ps.clearParameters();
         if(this.cardSelectedFlag[i].equals("Y"))
         {
            ps.setString(1, this.cardFee[i]);
            ps.setLong(2, appSeqNum);
            ps.setInt(3, this.cardCodes[i]);
        
            if(ps.executeUpdate() != 1)
            {
              com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData3: insert failed");
              addError("submitData3: Unable to insert/update record");
            }
         }
      }
      ps.clearParameters();
      ps.setString(1,this.vmTransFee);
      ps.setLong(2, appSeqNum);
      ps.setInt(3,mesConstants.APP_CT_INTERNET);
  
      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData3: insert failed");
        addError("submitData3: Unable to insert/update record");
      }
      
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData3: " + e.toString());
      addError("submitData3: " + e.toString());
    }
  }

  public void updateData(HttpServletRequest aReq)
  {
    int temp = 0;
    for(int i=0; i<this.NUM_CARDS; i++)
    {
      if(!isBlank(aReq.getParameter("cardCode" + i)))
      {
        try
        {
          temp = Integer.parseInt(aReq.getParameter("cardCode" + i));
          switch(temp)
          { 
            case mesConstants.APP_CT_DINERS_CLUB:
              this.cardSelectedFlag[0] = "Y";
              break;
            case mesConstants.APP_CT_DISCOVER:
              this.cardSelectedFlag[1] = "Y";   
              break;
            case mesConstants.APP_CT_JCB:
              this.cardSelectedFlag[2] = "Y";
              break;
            case mesConstants.APP_CT_AMEX:
              this.cardSelectedFlag[3] = "Y";
              break;
            default:
              break;
          }
        }
        catch(Exception e)
        {
        }
      }
    }
  }
        
  public boolean validate(HttpServletRequest aReq)
  {
    return(! hasErrors());
  }
  
  public String getCardDesc(int i)
  {
    return this.cardDesc[i];
  }
  public int getCardCode(int i)
  {
    return this.cardCodes[i];
  }

  public String getCardSelectedFlag(int i)
  {
    return this.cardSelectedFlag[i];
  }
  public String getCardFee(int i)
  {
    return this.cardFee[i];
  }

  public String getMiscDesc(int i)
  {
    return this.miscDesc[i];
  }
  public String getMiscDesc2(int i)
  {
    return this.miscDesc2[i];
  }

  public String getMiscFee(int i)
  {
    return this.miscFee[i];
  }

  public String getOwner1LastName()
  {
    return this.owner1LastName;
  }
  public String getOwner1FirstName()
  {
    return this.owner1FirstName;
  }
  public String getOwner1Title()
  {
    return this.owner1Title;
  }

  public String getOwner2LastName()
  {
    return this.owner2LastName;
  }
  public String getOwner2FirstName()
  {
    return this.owner2FirstName;
  }
  public String getOwner2Title()
  {
    return this.owner2Title;
  }
  public String getDiscountRate()
  {
    return this.discountRate;
  }    
  public String getVmTransFee()
  {
    String result = this.vmTransFee;

    if(vmTransFee.length() == 2 || (vmTransFee.startsWith("0") && vmTransFee.length() == 3) )
    {
      result = this.vmTransFee + "0";
    }
    
    return result;
  }
  public String getMidQualDg()
  {
    return this.midQualDg;
  }
  public String getNonQualDg()
  {
    return this.nonQualDg;
  }

  public String getQualComm()
  {
    return this.qualComm;
  }
  
  public String getNonQualDgAdd()
  {
    return this.nonQualDgAdd;
  }
  
  public String getCommDgAdd()
  {
    return this.commDgAdd;
  }

  
  public void setAgreeMerch(String agreeMerch)
  { 
    this.agreeMerch = isBlank(agreeMerch) ? "N" : "Y";
  }
  public void setAgreePrice(String agreePrice)
  { 
    this.agreePrice = isBlank(agreePrice) ? "N" : "Y";
  }

}
