package com.mes.mvc;

public interface Downloadable
{

  public void startDownload();

  public String getDlHeader();

  public String getDlNext();

  public void finishDownload();

  public String getDlFilename();
}