package com.mes.mvc;

import com.mes.forms.HtmlLink;

public interface Link
{
  public void setServletUrl(String servletUrl);
  public void setActionDef(ActionDef actionDef);
  public ActionDef getActionDef();
  public void setView(View view);
  public View getView();
  public void addArg(String argName);
  public void addArg(String argName, String argValue);
  public void addArgStr(String argStr);
  public void setText(String text);
  public void setPrefix(String prefix);

  public void setPopTarget(String popTarget);
  public void setPopWidth(int popWidth);
  public void setPopHeight(int popHeight);

  public String renderUrl();
  public String renderHtml();

  public HtmlLink getHtmlLink();
}
