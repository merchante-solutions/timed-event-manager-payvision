package com.mes.mvc;

public class ActionMenuItem extends MenuItem
{
  private String actionName;
  private String otherArgs;

  public ActionMenuItem(String title, String actionName, String otherArgs)
  {
    super(title);
    this.actionName = actionName;
    this.otherArgs = otherArgs;
  }

  public ActionMenuItem(String title, String actionName)
  {
    this(title,actionName,null);
  }

  public String renderLink(RequestHandler handler)
  {
    return getLink(handler).renderHtml();
  }
  public String renderLink(RequestHandler handler, String targetIcon)
  {
    // for now don't support target windows on action menu items
    return renderLink(handler);
  }

  public Link getLink(RequestHandler handler)
  {
    return handler.getLink(actionName,otherArgs,title);
  }
}