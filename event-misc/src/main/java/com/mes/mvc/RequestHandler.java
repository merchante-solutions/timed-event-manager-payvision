package com.mes.mvc;

import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.mes.user.UserBean;

public interface RequestHandler
{
  public HttpServletRequest getRequest();
  public HttpServletResponse getResponse();
  public HttpSession getSession();
  public SessionManager getSessionManager();
  public UserBean getUser();
  public Map getMenuMap();
  public String getServletUrl();
  public ServletContext getServletContext();
  
  public String getProperty(String name);

  // main entry point
  public void doRequest(Controller controller, HttpServletRequest request, 
    HttpServletResponse response);

  public void doAction(String actionName);
  public ActionDef getActionDef(String actionName);
  public View getView(String viewName);

  // routing (to actions, views, links, etc.)
  public void doView(String viewName, String title);
  public void doView(String viewName);
  public void doForward(View view, String title);
  public void doForward(View view);
  public void doRedirect(String path);
  public void doActionRedirect(String actionName);
  public void doActionRedirect(String actionName, String otherArgs);
  public void doLinkRedirect(Link link);
  public void doRender(String viewName);

  // link handling
  public Link getLink();
  public Link getLink(String actionName);
  public Link getLink(String actionName, String otherArgs);
  public Link getLink(String actionName, String otherArgs, String text);
  public Link getLink(String actionName, String viewName, String otherArgs,
    String prefix);
  public void setBackLink(String fromActName, Link backLink);
  public void setBackLink(String fromActName, String toActName, String toViewName,
    String otherArgs);
  public Link getBackLink(String actionName);
  public Link getBackLink();
  public void autoSetBackLinkFor(String actionName);

  public void disableBackLink(String actionName);
  public void disableBackLink();
  public boolean backLinkEnabled();
  public boolean backLinkEnabled(String actionName);


  public Link generateLink(String actionName, String otherArgs, String text);
  public Link generateLink(String actionName, String viewName, String otherArgs,
    String prefix);

  public Link getCurLink(String prefix);
  public Link getCurLink();
  public Action getCurAction();
  public ActionDef getCurActionDef();

  public boolean userHasPermission(String actionName);
  public boolean userHasPermission(UserBean user, String actionName);
}
