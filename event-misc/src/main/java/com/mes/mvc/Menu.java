package com.mes.mvc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

public class Menu
{
  static Logger log = Logger.getLogger(Menu.class);

  private String name;
  private String title;
  private List items = new ArrayList();
  private boolean autoBackLinkFlag;
  private MenuItem backItem;

  public Menu()
  {
    name = title = "not set";
  }

  public Menu(String name, String title)
  {
    this.name = name;
    this.title = title;
  }

  public void setName(String name)
  {
    this.name = name;
  }
  public String getName()
  {
    return name;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }
  public String getTitle()
  {
    return title;
  }

  public void addItem(MenuItem item)
  {
    if (item.isBackLink())
    {
      setBackItem(item);
    }
    else
    {
      items.add(item);
    }
  }
  public List getItems()
  {
    return items;
  }

  public void setAutoBackLinkFlag(boolean autoBackLinkFlag)
  {
    this.autoBackLinkFlag = autoBackLinkFlag;
  }
  public boolean hasAutoBackLink()
  {
    return autoBackLinkFlag;
  }

  public void setBackItem(MenuItem backItem)
  {
    this.backItem = backItem;
  }
  public MenuItem getBackItem()
  {
    return backItem;
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("Menu [ ");
    buf.append("name: " + name);
    buf.append(", title: " + title);
    buf.append(", auto back: " + autoBackLinkFlag);
    buf.append(", items: (" + title);
    for (Iterator i = items.iterator(); i.hasNext();)
    {
      buf.append(" "+i.next());
    }
    buf.append(" )");
    buf.append(backItem != null ? " back: " + backItem : "");
    buf.append(" ]");
    return buf.toString();
  }
}