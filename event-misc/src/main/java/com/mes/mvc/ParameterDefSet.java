package com.mes.mvc;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ParameterDefSet
{
  private Map reqDefs = new HashMap();
  private Map rspDefs = new HashMap();

  public ParameterDefSet()
  {
  }

  public void addDef(ParameterDef def)
  {
    if (def.isRequest())
    {
      reqDefs.put(def.getName(),def);
    }
    else
    {
      rspDefs.put(def.getName(),def);
    }
  }

  public ParameterDef getRequestDef(String name)
  {
    return (ParameterDef)reqDefs.get(name);
  }

  public ParameterDef getResponseDef(String name)
  {
    return (ParameterDef)rspDefs.get(name);
  }

  public ParameterDef getDef(String name)
  {
    ParameterDef reqDef = getRequestDef(name);
    ParameterDef rspDef = getResponseDef(name);
    if (reqDef != null && rspDef != null)
    {
      throw new RuntimeException("Ambiguous parameter name, use more specific accessor");
    }
    if (reqDef != null)
    {
      return reqDef;
    }
    return rspDef;
  }

  public void addDefs(ParameterDefSet defs)
  {
    if (defs.reqDefs != null)
    {
      for (Iterator i = defs.reqDefs.values().iterator(); i.hasNext();)
      {
        addDef((ParameterDef)i.next());
      }
    }
    if (defs.rspDefs != null)
    {
      for (Iterator i = defs.rspDefs.values().iterator(); i.hasNext();)
      {
        addDef((ParameterDef)i.next());
      }
    }
  }

  /**
   * Scans the given def map for a match with the name.
   */
  private boolean matchesDef(Map defs, String name)
  {
    // look for quick exact name match
    if (defs.get(name) != null)
    {
      return true;
    }

    // scan for regex match...
    for (Iterator i = defs.values().iterator(); i.hasNext();)
    {
      ParameterDef def = (ParameterDef)i.next();
      if (def.matches(name))
      {
        return true;
      }
    }

    return false;
  }

  /**
   * Determines if name matches any request parameter def.
   */
  public boolean matchesRequestDef(String name)
  {
    return matchesDef(reqDefs,name);
  }

  /**
   * Determines if all names in the collection match a request
   * parameter def.
   */
  public boolean matchesRequestDefs(Collection parms)
  {
    for (Iterator i = parms.iterator(); i.hasNext();)
    {
      String name = ""+i.next();
      if (!matchesRequestDef(name))
      {
        return false;
      }
    }
    return true;
  }

  /**
   * Returns collection of request defs.
   */
  public Collection getRequestDefs()
  {
    return reqDefs.values();
  }

  /**
   * Determines if name matches any response parameter def.
   */
  public boolean matchesResponseDef(String name)
  {
    return matchesDef(rspDefs,name);
  }

  /**
   * Determines if all names in the collection match a response
   * parameter def.
   */
  public boolean matchesResponseDefs(Collection parms)
  {
    for (Iterator i = parms.iterator(); i.hasNext();)
    {
      String name = ""+i.next();
      if (!matchesResponseDef(name))
      {
        return false;
      }
    }
    return true;
  }

  /**
   * Returns collection of response defs.
   */
  public Collection getResponseDefs()
  {
    return rspDefs.values();
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("ParameterDefSet [ reqDefs: ");
    for (Iterator i = reqDefs.values().iterator(); i.hasNext();)
    {
      buf.append(""+i.next());
      buf.append((i.hasNext() ? ", " : ""));
    }
    buf.append(", rspDefs: ");
    for (Iterator i = rspDefs.values().iterator(); i.hasNext();)
    {
      buf.append(""+i.next());
      buf.append((i.hasNext() ? ", " : ""));
    }
    buf.append(" ]");

    return buf.toString();
  }
}