package com.mes.mvc;

import java.util.Date;
import java.util.Map;

public interface ApiRequest extends ApiMultipartFileHandler
{
  public void setMultipartFileHandler(ApiMultipartFileHandler fileHandler);
  public void setParameterDefSet(ParameterDefSet defs);
  public void setParameter(String name, String value);
  public String getParameter(String name);
  public long getParameterAsLong(String name);
  public int getParameterAsInt(String name);
  public Date getParameterAsDate(String name);
  public Map getParameterMap();
  public boolean hasParameter(String name);
  public void receive();
  public boolean isMultipartRequest();
}
