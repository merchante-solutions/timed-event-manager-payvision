package com.mes.mvc;

import java.util.List;

public interface View
{
  public void setTarget(String target);
  public String getTarget();

  public void setTitle(String title);
  public String getTitle();

  public void setTemplate(String template);
  public String getTemplate();

  public void addInclude(String include);
  public List getIncludes();

  public boolean isTitled();
  public boolean isTemplate();
  public boolean isDefault();
}