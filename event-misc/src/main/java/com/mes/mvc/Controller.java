package com.mes.mvc;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;
import java.util.Map;
import java.util.Properties;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

public class Controller extends HttpServlet
{
  public static final String IP_CONFIG_FILE       = "config-file";
  public static final String IP_XML_CONFIG_PARSER = "xml-config-parser";

  static Logger log = Logger.getLogger(Controller.class);
  
  // config property handling
  private String          configFilePath;
  private URL             configUrl;
  private File            configFile;
  private XmlConfigParser xmlConfigParser;
  private long            lastLoaded = -1L;
  private Properties      properties;

  private Class           handlerClass;

  private Map             menuMap;
  private Map             viewMap;
  private Map             actionMap;
  private ApiRcDefSet     rcDefs;

  /**
   * Load the init-param config file path.
   */
  private String getConfigFilePath()
  {
    // fetch servlet init parameter containing config file location
    String filePath = getInitParameter(IP_CONFIG_FILE);
    if (filePath == null)
    {
      String errorText = "Servlet init-param '" + IP_CONFIG_FILE 
        + "' missing, check servlet registration in web.xml";
      log.error(errorText);
      throw new RuntimeException(errorText);
    }
    return filePath;
  }

  /**
   * Attempts to setup a file reference to the config file so that live reloads
   * can be triggered by updates to the config file.  If the config data is
   * inside a deployed WAR than reloads are obviously not needed.
   */
  private File getConfigFile(String filePath)
  {
    // try and get a File reference to the config file resource, it may
    // not be possible if it's in a war
    String sep = filePath.startsWith(File.separator) ? ""
      : File.separator;
    String realPath = getServletContext().getRealPath("/");
    return realPath != null ? new File(realPath + sep + filePath) : null;
  }

  /**
   * Initializes the config file URL object that will be used to load
   * config file data.
   */
  private URL getConfigUrl(String filePath)
  {
    URL url = null;
    try
    {
      url = getServletContext().getResource(filePath);
    }
    catch (Exception e)
    {
      String errorText = "Error accessing file " + filePath + ": " + e;
      log.error(errorText);
      throw new RuntimeException(errorText);
    }
    if (url == null)
    {
      String errorText = "Config file " + filePath + " not found";
      log.error(errorText);
      throw new NullPointerException(errorText);
    }
    return url;
  }

  /**
   * Attempts to initialize xml config parsing if a parser class is
   * specified in web.xml init-param for the controller servlet.
   */
  private XmlConfigParser getConfigParser()
  {
    String parserClassName = getInitParameter(IP_XML_CONFIG_PARSER);
    XmlConfigParser parser = null;
    if (parserClassName != null)
    {
      try
      {
        parser = (XmlConfigParser)(Class.forName(parserClassName)).newInstance();
        log.debug("Xml Config parser set to " + parserClassName);
      }
      catch (Exception e)
      {
        String errorText = "Failed to instantiate xml config parser of type "
          + parserClassName + " (" + e + ")";
        log.error(errorText);
        throw new RuntimeException(errorText);
      }
    }
    return parser;
  }

  /**
   * Fetches config file from init parameters in web.xml.  Looks for an xml
   * config parser class name, if present an instance is created of the xml
   * config parser.
   */
  private void initConfigParsing()
  {
    configFilePath  = getConfigFilePath();
    configFile      = getConfigFile(configFilePath);
    configUrl       = getConfigUrl(configFilePath);
    xmlConfigParser = getConfigParser();
  }

  /**
   * Tries to determine if a config file has been modified since the last load
   * of config data.  This may not be possible if config file is inside an
   * unexploded WAR file, in which case only a redeploy can update config data.
   * Returns true if properties have never been loaded or config file can is
   * determined to be newer than most recent config load time.
   */
  private boolean propertyRefreshNeeded()
  {
    return properties == null 
          || (configFile != null && configFile.lastModified() > lastLoaded);
  }

  /**
   * Loads xml configuration file using configUrl and xmlConfigParser.
   * Menus may only be loaded via xml.
   */
  private void loadXmlConfigFile()
  {
    if (xmlConfigParser == null)
    {
      String errorText = "No xml config parser has been set for controller"
        + " config file " + configFile + ", xml-config-parser init-param"
        + " must be set servlet in web.xml";
      log.error(errorText);
      throw new NullPointerException(errorText);
    }
    properties = xmlConfigParser.parseXmlUrl(configUrl);
    menuMap = xmlConfigParser.getMenuMap();
    viewMap = xmlConfigParser.getViewMap();
    actionMap = xmlConfigParser.getActionMap();
    rcDefs = xmlConfigParser.getRcDefs();
  }

  /**
   * Loads old style name/value property file formatted config file.
   */
  private void loadOldConfigFile()
  {
    InputStream is = null;
    try
    {
      is = getServletContext().getResourceAsStream(configFilePath);
      if (is == null)
      {
        String errorText = "Controller config file " + configFilePath
          + " does not exist";
        log.error(errorText);
        throw new RuntimeException(errorText);
      }
      properties = new Properties();
      properties.load(is);
    }
    catch (IOException ie)
    {
      String errorText = "Failed to load config file " + configFilePath
        + " (" + ie.toString() + ")";
      log.error(errorText);
      throw new RuntimeException(errorText);
    }
    finally
    {
      try { is.close(); } catch (Exception e) { }
    }
  }

  /**
   * This is synchronized since multiple request handler threads may be trying
   * to reload config data simultaneously.
   */
  private synchronized void refreshProperties()
  {
    // do another quick check in case another thread has refreshed recently
    if (!propertyRefreshNeeded()) return;

    // xml configuration file
    if (configFilePath.toLowerCase().endsWith(".xml"))
    {
      loadXmlConfigFile();
    }
    // non-xml name/value pair configuration file
    else
    {
      loadOldConfigFile();
    }
   
    // note last loaded timestamp
    log.debug("Config data loaded from " + configFilePath);
    lastLoaded = Calendar.getInstance().getTimeInMillis();
  }

  /**
   * Loads the property file located at the config path.  Currently this does
   * not store the properties, this is convenient for testing as changes to
   * the property file will be reflected at runtime whenever a handler uses
   * this method to get properties.
   */
  public Properties getProperties()
  {
    if (propertyRefreshNeeded())
    {
      refreshProperties();
    }
    return properties;
  }

  /**
   * Returns menu map.  This is generated by xml parser object during a property
   * load.
   */
  public Map getMenuMap()
  {
    return menuMap;
  }

  public Map getViewMap()
  {
    return viewMap;
  }

  public Map getActionMap()
  {
    return actionMap;
  }

  public ApiRcDefSet getRcDefs()
  {
    return rcDefs;
  }

  /**
   * Loads configuration file name from servlet init param and then loads
   * the request handler class name from it.  This method is called once 
   * when the web app is starting up and registering servlets declared in 
   * WEB-INF/web.xml.
   */
  public void init()
  {
    // initialize config file and xml parser
    initConfigParsing();

    // load properties
    refreshProperties();

    // get handler class name
    String handlerName = properties.getProperty(Mvc.PN_REQUEST_HANDLER);
    if (handlerName == null)
    {
      String errorText = "Servlet controller " + getServletName()
        + " missing property '" + Mvc.PN_REQUEST_HANDLER + "' in config file "
        + configFile;
      log.error(errorText);
      throw new NullPointerException(errorText);
    }

    // get handler class from name
    try
    {
      handlerClass = Class.forName(handlerName);
    }
    catch (Exception e)
    {
      log.error("Failed to get handler class " + handlerName + ": " + e);
      throw new RuntimeException(""+e);
    }
  }
  
  /**
   * Processes a request using the designated request handler.
   */
  private void doRequest(HttpServletRequest request,
    HttpServletResponse response)
  {
    try
    {
      // create a request handler and handle the request
      RequestHandler handler = (RequestHandler)handlerClass.newInstance();
      handler.doRequest(this,request,response);
    }
    catch (Exception e)
    {
      String errorText = "Failed to instantiate handler object of type "
        + handlerClass.getName() + " (" + e + ")";
      log.error(errorText);
      e.printStackTrace();
      throw new RuntimeException(errorText);
    }
  }
  
  /**
   * Handles http post, calls processRequest with request and response.
   */
  public void doPost( HttpServletRequest request, HttpServletResponse response)
  {
    doRequest(request,response);
  }

  /**
   * Handles http get, calls processRequest with request and response.
   */
  public void doGet( HttpServletRequest request, HttpServletResponse response)
  {
    doRequest(request,response);
  }
}