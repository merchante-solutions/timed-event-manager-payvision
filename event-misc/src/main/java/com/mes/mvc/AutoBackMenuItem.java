package com.mes.mvc;

public class AutoBackMenuItem extends MenuItem
{
  public AutoBackMenuItem()
  {
    super("Back");
  }

  public String renderLink(RequestHandler handler)
  {
    return getLink(handler).renderHtml();
  }
  public String renderLink(RequestHandler handler, String targetIcon)
  {
    // for now, target icon not supported in auto back menu items
    return renderLink(handler);
  }

  public Link getLink(RequestHandler handler)
  {
    return handler.getBackLink();
  }
}