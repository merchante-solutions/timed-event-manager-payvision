package com.mes.mvc.mes;

import org.apache.log4j.Logger;
import com.mes.mvc.Menu;
import com.mes.mvc.Mvc;

public class MenuAction extends MesAction
{
  static Logger log = Logger.getLogger(MenuAction.class);

  public static final String FN_MENU_NAME = "menuName";

  public void execute()
  {
    // determine menu name
    String menuName = null;
    if (name.equals("menu"))
    {
      // get the requested menu name
      menuName = getParm(FN_MENU_NAME,null);

      // fall back to default menu if no menu name given
      if (menuName == null)
      {
        menuName = handler.getProperty(Mvc.PN_DEFAULT_MENU_NAME);
      }
    }
    else
    {
      // menu name is the action name
      menuName = name;
    }

    // add menu to the request and do menu view
    Menu menu = null;
    if ((menu = (Menu)menuMap.get(menuName)) != null)
    {
      setRequestAttr("menu",menu);
      doView(Mvc.PN_MENU_VIEW,menu.getTitle());
    }
    else
    {
      //addFeedback("Attempt to access menu '" + menuName + "' failed");
      //doView(Mvc.PN_MENU_VIEW,"Invalid menu request");
      redirectWithFeedback(handler.getBackLink(),"Attempt to access menu '" 
        + menuName + "' failed");
    }
  }
}