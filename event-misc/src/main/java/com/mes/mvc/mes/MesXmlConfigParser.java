package com.mes.mvc.mes;

import java.util.Iterator;
import org.apache.log4j.Logger;
import org.jdom.Element;
import com.mes.mvc.MenuItem;
import com.mes.mvc.View;
import com.mes.mvc.XmlConfigParser;
import com.mes.user.Permissions;

public class MesXmlConfigParser extends XmlConfigParser
{
  static Logger log = Logger.getLogger(MesXmlConfigParser.class);

  protected String getViewName(Element viewEl)
  {
    return viewEl.getAttribute("name").getValue();
  }

  protected View getView(Element viewEl)
  {
    String path     = viewEl.getAttribute("path").getValue();
    String title    = viewEl.getAttribute("title") != null ?
                        viewEl.getAttribute("title").getValue() : null;
    String template = viewEl.getAttribute("template") != null ? 
                        viewEl.getAttribute("template").getValue() : null;

    // can't allow null title with non-null template
    if (title == null && template != null)
    {
      throw new NullPointerException("Cannot set template for view " 
        + viewEl.getAttribute("name") + " with null title");
    }

    MesView view = new MesView();
    view.setTarget(path);
    view.setTitle(title);
    view.setTemplate(template);
    for (Iterator i = viewEl.getChildren("include").iterator(); i.hasNext();)
    {
      Element incEl = (Element)i.next();
      view.addInclude(incEl.getAttribute("path").getValue());
    }

    return view;
  }

  protected String getPermissionsValue(Element root)
  {
    StringBuffer permBuf = new StringBuffer();
    for (Iterator i = root.getChildren("permission").iterator(); i.hasNext();)
    {
      Element permEl = (Element)i.next();
      if (permBuf.length() > 0) permBuf.append(";");
      String actionName = permEl.getAttribute("action").getValue();
      String rightId = permEl.getAttribute("right-id").getValue();
      permBuf.append(actionName + " = " + rightId + ";" + actionName 
        + "Action=" + rightId);
    }
    return permBuf.toString();
  }

  protected MenuItem getMenuItemWrapper(MenuItem item)
  {
    return new MesMenuItem(item);
  }

  protected void loadMenuItemPermissions(Element permEl, MenuItem item)
  {
    Element rsEl = permEl.getChild("right-set");
    boolean isRequired = rsEl.getAttribute("type").getValue().equals("required");
    Permissions permissions = new Permissions();
    for (Iterator i = rsEl.getChildren("right").iterator(); i.hasNext();)
    {
      String rightStr = ((Element)i.next()).getText();
      long rightId = Long.parseLong(rightStr);
      if (isRequired)
      {
        permissions.requireRight(rightId);
      }
      else
      {
        permissions.allowRight(rightId);
      }
    }
    ((MesMenuItem)item).setPermissions(permissions);
  }
}