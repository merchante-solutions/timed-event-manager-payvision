package com.mes.mvc.mes;

import org.apache.log4j.Logger;
import com.mes.mvc.Mvc;

public class ConfirmAction extends MesAction
{
  static Logger log = Logger.getLogger(ConfirmAction.class);

  public void execute()
  {
    // setup ConfirmBean, process any submit data (user confirm or cancel)
    ConfirmBean bean = new ConfirmBean(this);

    // look for confirm data request attributes (present if first time through)
    String confirmMsg = (String)getRequestAttr(FN_CONFIRM_MSG,null);
    if (confirmMsg != null)
    {
      String confirmAction = (String)getRequestAttr(FN_CONFIRM_ACTION,null);
      bean.setData(FN_CONFIRM_ACTION,confirmAction);
    }

    // see if user submitted response
    if (!bean.isAutoSubmitOk())
    {
      // place the confirm bean in the request and 
      // forward user to the confirm dialog view
      setRequestAttr("viewBean",bean);
      doView(Mvc.PN_CONFIRM_VIEW);
    }
    // user chose to confirm or cancel the original 
    // action, return to the original action
    else
    {
      // route user back to originating action with user choice in confirm flag
      String confirmAction = bean.getData(FN_CONFIRM_ACTION);
      restoreRequestData(confirmAction);
      setRequestAttr(getCfName(confirmAction),bean.isConfirmed() ? "yes" : "no");
      doAction(confirmAction);
    }
  }
}