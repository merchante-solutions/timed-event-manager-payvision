package com.mes.mvc.mes;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.forms.HtmlLink;
import com.mes.mvc.ActionDef;
import com.mes.mvc.Link;
import com.mes.mvc.View;

public class MesLink implements Link
{
  static Logger log = Logger.getLogger(MesLink.class);

  private String    servletUrl;
  private ActionDef actionDef;
  private String    text;
  private String    prefix;
  private View      view;
  private List      args = new ArrayList();
  private String    popTarget;
  private int       popWidth;
  private int       popHeight;

  public MesLink()
  {
  }

  public void setServletUrl(String servletUrl)
  {
    this.servletUrl = servletUrl;
  }

  public void setActionDef(ActionDef actionDef)
  {
    this.actionDef = actionDef;
  }
  public ActionDef getActionDef()
  {
    return actionDef;
  }

  public void setView(View view)
  {
    this.view = view;
  }
  public View getView()
  {
    return view;
  }

  public void addArg(String argName)
  {
    if (argName == null) return;
    args.add(argName);
  }

  public void addArg(String argName, String argValue)
  {
    if (argName == null) return;
    args.add(argName + "=" + argValue);
  }

  public void addArgStr(String argStr)
  {
    if (argStr == null) return;
    String[] argPairs = argStr.split("&");
    for (int i = 0; i < argPairs.length; ++i)
    {
      String[] nameVal = argPairs[i].split("=",2);
      if (nameVal.length != 2)
      {
        throw new RuntimeException("Invalid arg pair: '" + argPairs[i] + "'");
      }
      addArg(nameVal[0],nameVal[1]);
    }
  }

  public void setText(String text)
  {
    this.text = text;
  }

  public void setPrefix(String prefix)
  {
    this.prefix = prefix;
  }

  private String getLinkText()
  {
    StringBuffer linkText = new StringBuffer();
    if (prefix != null)
    {
      linkText.append(prefix + " ");
    }

    // hmm, changed text to override view title...
    if (text != null)
    {
      linkText.append(text);
    }
    else if (view != null)
    {
      linkText.append(view.isTitled() ? view.getTitle() : view.getTarget());
    }

    return linkText.length() == 0 ? 
      "Action: " + actionDef.getName() : linkText.toString();
  }

  public void setPopTarget(String popTarget)
  {
    this.popTarget = popTarget;
  }

  public void setPopWidth(int popWidth)
  {
    this.popWidth = popWidth;
  }

  public void setPopHeight(int popHeight)
  {
    this.popHeight = popHeight;
  }

  private String getShortActionName()
  {
    String actionName = actionDef.getName();
    if (actionName.endsWith("Action"))
    {
      actionName = actionName.substring(0,actionName.length() - 6);
    }
    return actionName;
  }

  public String renderUrl()
  {
    if (servletUrl == null)
      throw new NullPointerException("Servlet URL has not been set in link");

    if (actionDef == null)
      throw new NullPointerException("Action def not set in link");

    StringBuffer linkUrl = new StringBuffer();
    linkUrl.append(servletUrl + "/" + getShortActionName());
    if (args.size() > 0)
    {
      linkUrl.append("?");
    }
    for (Iterator i = args.iterator(); i.hasNext();)
    {
      String arg = (String)i.next();
      String[] nameVal = arg.split("=");

      try
      {
        linkUrl.append(URLEncoder.encode(nameVal[0],"UTF-8"));
        if (nameVal.length > 1)
        {
          linkUrl.append("=" + URLEncoder.encode(nameVal[1],"UTF-8"));
        }
        if (i.hasNext())
        {
          linkUrl.append("&");
        }
      }
      catch (Exception e)
      {
        throw new RuntimeException("Unable to encode url arg value " 
          + nameVal[0] + "='" + nameVal[1] + "': " + e);
      }
    }
    return linkUrl.toString();
  }

  /*
    Popup code example:

      <script language="JavaScript">
        <!--

        function launchSicBrowser()
        {
          open('/jsp/app/sic_lookup.jsp', 'sicbrowser',
            'resizable=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, width=400, height=400')
        }
  
        function unlinkSicBrowser()
        {
          if (window.sicbrowser && window.sicbrowser.open && !window.sicbrowser.closed)
            window.sicbrowser.opener = null;
        }

        document.write('<a href="javascript:launchSicBrowser()">Pick...</a>')
  
        -->
      </script>
      <noscript>

        <a target="sicbrowser" href="/jsp/app/sic_lookup.jsp">Browse...</a>
  
      </noscript>

  */

  private String renderPopupHtml()
  {
    if (popWidth == 0)
    {
      throw new RuntimeException(
        "Popup target set in link but popup width not given");
    }
    if (popHeight == 0)
    {
      throw new RuntimeException(
        "Popup target set in link but popup height not given");
    }
    String popUrl     = renderUrl();
    String popFunc    = "pop_" + popTarget + "()";
    String unlinkFunc = "unlink_" + popTarget + "()";

    StringBuffer linkHtml = new StringBuffer();
    linkHtml.append("<script language=\"JavaScript\">\n");
    linkHtml.append("  <!--\n\n");
    linkHtml.append("  function " + popFunc + "\n");
    linkHtml.append("  {\n");
    linkHtml.append("    open('" + popUrl + "', '" + popTarget + "',\n");
    linkHtml.append("      'resizable=yes, toolbar=no, location=no, "
      + "directories=no, status=no, menubar=no, scrollbars=yes, "
      + "width=" + popWidth + ", height=" + popHeight + "')\n");
    linkHtml.append("  }\n\n");
    linkHtml.append("  function " + unlinkFunc + "\n");
    linkHtml.append("  {\n");
    linkHtml.append("    if (window." + popTarget + " && window." + popTarget 
      + ".open && !window." + popTarget + ".closed)\n");
    linkHtml.append("      window." + popTarget + ".opener = null;\n");
    linkHtml.append("  }\n\n");
    linkHtml.append("  document.write('<a href=\"javascript:" + popFunc + "\">"
      + getLinkText() + "</a>')\n\n");
    linkHtml.append("  -->\n");
    linkHtml.append("</script>\n");
    linkHtml.append("<noscript>\n\n");
    linkHtml.append("  <a target=\"" + popTarget + "\" href=\"" + popUrl + "\">"
      + getLinkText() + "</a>\n\n");
    linkHtml.append("</noscript>\n");

    return linkHtml.toString();
  }

  public HtmlLink getHtmlLink()
  {
    HtmlLink hl = new HtmlLink(renderUrl(),getLinkText());
    hl.setSuppressFormatting(true);
    return hl;
  }

  public String renderHtml()
  {
    if (view == null && text == null)
      throw new NullPointerException(
        "View and title text not set in link (at least one must be set)");
      
    if (popTarget != null) return renderPopupHtml();

    return getHtmlLink().renderHtml();
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("MesLink [ ");
    buf.append("servletUrl: " + servletUrl);
    buf.append(actionDef != null ? ", action: " + actionDef.getName() : "");
    buf.append(text != null ? ", text: " + text : "");
    buf.append(view != null ? ", view: " + view.getTarget() : "");
    if (!args.isEmpty())
    {
      buf.append(", args [ ");
      for (Iterator i = args.iterator(); i.hasNext();)
      {
        buf.append((String)i.next());
        if (i.hasNext()) buf.append(", ");
      }
      buf.append(" ]");
    }
    buf.append(" ]");
    if (popTarget != null)
    {
      buf.append(", target: " + popTarget);
      buf.append(", width: " + popWidth);
      buf.append(", height: " + popHeight);
    }
    return buf.toString();
  }
}
