package com.mes.mvc.mes;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.menus.MenuDataBean;
import com.mes.mvc.ActionMenuItem;
import com.mes.mvc.Menu;
import com.mes.mvc.MenuItem;
import com.mes.mvc.RequestHandler;
import com.mes.mvc.UrlMenuItem;
import com.mes.user.Permissions;
import com.mes.user.Right;

public class MesDbMenuLoader
{
  static Logger log = Logger.getLogger(MesDbMenuLoader.class);

  private RequestHandler handler;
  private int menuId;

  private MesDbMenuLoader(RequestHandler handler, int menuId)
  {
    this.handler = handler;
    this.menuId = menuId;
  }

  private void applyPermissions(MenuItem mvcItem, 
    com.mes.menus.MenuItem mesItem)
  {
    // apply permissions to mes menu items
    if (mvcItem != null && mvcItem instanceof MesMenuItem &&
        mesItem.getRequiredRights().size() > 0)
    {
      MesMenuItem mmItem = (MesMenuItem)mvcItem;
      Permissions perms = new Permissions();
      for (Iterator ri = mesItem.getRequiredRights().iterator(); 
           ri.hasNext();)
      {
        Right right = (Right)ri.next();
        perms.allow(right);
      }
      mmItem.setPermissions(perms);
    }
  }

  private MenuItem generateMvcMenuItem(String itemName, String itemUrl)
  {
    MenuItem mvcItem = null;

    // determine if item is an action or url link
    // url links will start with '/'
    if (itemUrl.startsWith("/"))
    {
      // add the menu item to the mvc menu as 
      mvcItem = new UrlMenuItem(itemName,itemUrl);
    }
    else
    {
      // add as action item, separate parm string
      String[] urlParts = itemUrl.split("\\?");
      String actionName = urlParts[0];

      try
      {
        // fetch action def to verify it exists
        // this will throw null pointer exception
        // if action name is invalid
        handler.getActionDef(actionName);
        String otherArgs = null;
        if (urlParts.length > 1)
        {
          otherArgs = urlParts[1];
        }
        mvcItem = new ActionMenuItem(itemName,actionName,otherArgs);
      }
      catch (NullPointerException ne)
      {
        log.error("Invalid action name specified in menu with id " 
          + menuId + ": '" + actionName + "'");
        return null;
      }
    }

    // wrap the generated menu item in the mes menu item wrapper
    return new MesMenuItem(mvcItem);
  }

  private MenuItem convertMesMenuItem(com.mes.menus.MenuItem mesItem)
  {

    // extract menu item data from mes menu item
    String itemName = mesItem.getDisplayName();
    String itemUrl = mesItem.getBaseUrl();
    MenuItem mvcItem = generateMvcMenuItem(itemName,itemUrl);
    if (mvcItem != null)
    {
      applyPermissions(mvcItem,mesItem);
    }
    return mvcItem;
  }

  private MenuItem extractBackItem(com.mes.menus.MenuItem mesItem)
  {
    // detect back menu item specification in 
    // the description field of a menu item
    // (back::link text::link url in mes_items.description)
    MenuItem backItem = null;
    String mesDesc = mesItem.getDescription();
    if (mesDesc != null && mesDesc.startsWith("back"))
    {
      String[] backStrs = mesDesc.split("::");
      if (backStrs.length == 3)
      {
        // set the back item for the menu
        // from the back data specified
        backItem = generateMvcMenuItem(backStrs[1],backStrs[2]);
        if (backItem != null)
        {
          backItem.setBackLinkFlag(true);
          backItem = new MesMenuItem(backItem);
        }
      }
    }
    return backItem;
  }

  private Menu convertMesMenu(MenuDataBean mdb, List items)
  {
    // create an mvc menu object
    String menuTitle = mdb.getMenuTitle();
    menuTitle = menuTitle != null ? menuTitle : "Menu";
    Menu mvcMenu = new Menu("mesMenu" + menuId,menuTitle);

    // convert mes menu items to mvc menu items
    for (Iterator i = items.iterator(); i.hasNext();)
    {
      com.mes.menus.MenuItem mesItem = (com.mes.menus.MenuItem)i.next();
      MenuItem mvcItem = convertMesMenuItem(mesItem);
      if (mvcItem != null)
      {
        mvcMenu.addItem(mvcItem);
        MenuItem mvcBackItem = extractBackItem(mesItem);
        if (mvcBackItem != null)
        {
          mvcMenu.setBackItem(mvcBackItem);
        }
      }
    }

    return mvcMenu;
  }

  private Menu _load()
  {
    Menu mvcMenu = null;
    boolean backSet = false;

    // load mes menu item data
    MenuDataBean mdb = new MenuDataBean();      
    List items = mdb.loadMenu(menuId);

    // convert the mes menu data to an mvc style menu
    if (items != null)
    {
      return convertMesMenu(mdb,items);
    }
    return null;
  }

  public static Menu load(RequestHandler handler, int menuId)
  {
    return (new MesDbMenuLoader(handler,menuId))._load();
  }
}
