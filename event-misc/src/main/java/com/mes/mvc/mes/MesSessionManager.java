package com.mes.mvc.mes;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.mes.mvc.SessionManager;

public class MesSessionManager implements SessionManager, Serializable
{
  static Logger log = Logger.getLogger(MesSessionManager.class);

  private HashMap items = new HashMap();
  private HttpSession session;

  public class ItemData
  {
    private Serializable  item;
    private String        name;
    private int           lastHash;

    public void setName(String name)
    {
      this.name = name;
    }
    public String getName()
    {
      return name;
    }

    private int getItemHash()
    {
      return item.toString().hashCode();
    }

    public void clean()
    {
      lastHash = getItemHash();
    }
      
    public void setItem(Serializable item)
    {
      this.item = item;
      clean();
    }
    public Serializable getItem()
    {
      return item;
    }

    public boolean isDirty()
    {
      return lastHash != getItemHash();
    }

    public String toString()
    {
      return "ItemData [ name: " + name + ", item: " + item + " ]";
    }
  }

  public void setSession(HttpSession session)
  {
    this.session = session;
  }

  private HttpSession getSession()
  {
    if (session == null)
    {
      throw new NullPointerException("Attempt to use session manager without"
        + " setting session");
    }
    return session;
  }

  public void set(String name, Serializable item)
  {
    log.debug("setting session attribute " + name + ": " + item);
    getSession().setAttribute(name,item);
    ItemData id = new ItemData();
    id.setItem(item);
    id.setName(name);
    items.put(item,id);
    items.put(name,id);
  }

  public Object get(String name)
  {
    return getSession().getAttribute(name);
  }

  public void remove(String name)
  {
    getSession().removeAttribute(name);
    ItemData id = (ItemData)items.get(name);
    if (id != null)
    {
      items.remove(id.getName());
      items.remove(id.getItem());
    }
  }

  public boolean isDirty()
  {
    for (Iterator i = items.values().iterator(); i.hasNext();)
    {
      ItemData id = (ItemData)i.next();
      if (id.isDirty())
      {
        log.debug("found dirty item data: " + id);
        return true;
      }
    }
    return false;
  }

  public void clean()
  {
    for (Iterator i = items.values().iterator(); i.hasNext();)
    {
      ItemData id = (ItemData)i.next();
      if (id.isDirty())
      {
        log.debug("resetting session attribute for item data: " + id);
        getSession().setAttribute(id.getName(),id.getItem());
        id.clean();
      }
    }
  }
}