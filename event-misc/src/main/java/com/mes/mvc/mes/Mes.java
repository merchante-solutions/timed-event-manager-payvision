package com.mes.mvc.mes;

public class Mes
{
  public static final String PN_DEFAULT_TEMPLATE_PATH = "defaultTemplatePath";
  public static final String PN_DEFAULT_ACTION_NAME   = "defaultActionName";
  public static final String PN_DEFAULT_VIEW_NAME     = "defaultViewName";
  public static final String PN_TARGET_WIN_ICON       = "targetWinIcon";

  public static final String ACTION_DB_MENU           = "mesMenu";

  public static final String PN_DEFAULT_LOGIN_VIEW    = "defaultLogin";
  public static final String PN_INVALID_REQUEST_VIEW  = "invalidRequest";
  public static final String PN_BAD_RIGHTS_VIEW       = "badRights";
}