package com.mes.mvc.mes;

import com.mes.mvc.ApiException;

public class MesApiInvalidParmException extends ApiException
{
  public MesApiInvalidParmException(String name)
  {
    super(MesApiResponse.RC_INVALID_PARM,"Invalid parameter " + name);
  }
}
