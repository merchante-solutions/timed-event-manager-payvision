package com.mes.mvc.mes;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.mes.mvc.Action;
import com.mes.mvc.ActionDef;
import com.mes.mvc.Controller;
import com.mes.mvc.Link;
import com.mes.mvc.Mvc;
import com.mes.mvc.RequestHandler;
import com.mes.mvc.SessionManager;
import com.mes.mvc.View;
import com.mes.support.RequestedPageBean;
import com.mes.tags.TemplatePageParameter;
import com.mes.tags.TemplateStack;
import com.mes.tags.TemplateStackFrame;
import com.mes.user.ClientLoginCookie;
import com.mes.user.PermissionsMap;
import com.mes.user.UserBean;

public abstract class MesRequestHandler implements RequestHandler
{
  static Logger log = Logger.getLogger(MesRequestHandler.class);
  
  protected Controller          controller;
  protected HttpServletRequest  request;
  protected HttpServletResponse response;
  protected HttpSession         session;
  protected UserBean            user;
  protected String              servletUrl;
  protected Map                 menuMap;
  
  protected Properties          properties;
  protected PermissionsMap      permissions;
  protected Map                 viewMap;
  protected Map                 actionMap;

  protected SessionManager      sessionMgr;
  protected Action              curAction;
  protected View                curView;
  protected Link                curLink;
  
  public MesRequestHandler()
  {
  }
  
  public HttpServletRequest getRequest()
  {
    return request;
  }
  
  public HttpServletResponse getResponse()
  {
    return response;
  }
  
  public HttpSession getSession()
  {
    return session;
  }
  
  public SessionManager getSessionManager()
  {
    return sessionMgr;
  }

  public UserBean getUser()
  {
    return user;
  }

  public Map getMenuMap()
  {
    return menuMap;
  }
  
  public String getServletUrl()
  {
    return servletUrl;
  }

  public ServletContext getServletContext()
  {
    return controller.getServletContext();
  }

  /**
   * Returns config properties object.
   */
  private Properties getProperties()
  {
    return properties;
  }
    
  /**
   * Tries to fetch a property with the given name.
   */
  public String getProperty(String name)
  {
    String property = getProperties().getProperty(name);

    // if property not found throw a null pointer exception
    if (property == null)
    {
      throw new NullPointerException("Property '" + name + "' not found");
    }
      
    return property;
  }
  
  /**
   * Does handler specific initialization.
   */
  protected abstract void initializeHandler();
  

  /**************************************************************************
   *
   *  Request handling
   *
   *************************************************************************/

  /**
   * Loads properties from the controller, requires controller to have been
   * set.
   */
  protected void loadConfiguration()
  {
    try
    {
      properties  = controller.getProperties();
      menuMap     = controller.getMenuMap();
      viewMap     = controller.getViewMap();
    }
    catch (Exception e)
    {
      throw new RuntimeException("Failed to load properties: " + e);
    }
  }  

  /**
   * Load rights into a PermissionsMap from the config file properties.
   */
  protected void loadPermissions()
  {
    permissions = new PermissionsMap(getProperty(Mvc.PN_PERMISSIONS));
  }
  
  /**
   * Determine if user is valid.  Returns true if user is valid.
   */
  private boolean validateUser(UserBean user)
  {
    boolean isValid = false;
    
    if (user != null)
    {
      // if auto logoff is indicated invalidate user, clear session
      if (user.autoLogoff())
      {
        user.invalidate();
    
        Enumeration names = session.getAttributeNames();
        while(names.hasMoreElements())
        {
          String name = (String)names.nextElement();
          session.removeAttribute(name);
        }
      }
      else
      {
        isValid = user.isValid();
      }
    }
    
    return isValid;
  }

  /**
   * Load action definitions.
   */
  protected void loadActions()
  {
    actionMap = controller.getActionMap();

    // add any built in actions
    actionMap.put(Mvc.ACTION_CONFIRM,
      new ActionDef("core",Mvc.ACTION_CONFIRM,ConfirmAction.class,false));

    // original menu handling action
    actionMap.put(Mvc.ACTION_MENU,
      new ActionDef("core",Mvc.ACTION_MENU,MenuAction.class,false));

    // support for mes db menus (menu_items table in oracle)
    // the action handle is built into the mes base action handling class
    actionMap.put(Mes.ACTION_DB_MENU,
      new ActionDef("core",Mes.ACTION_DB_MENU,MesAction.class,false));

    // load menu names as actions
    for (Iterator i = controller.getMenuMap().keySet().iterator(); i.hasNext();)
    {
      String menuName = (String)i.next();
      actionMap.put(menuName,
        new ActionDef("core",menuName,MenuAction.class,false));
    }
  }

  /**
   * Retrieve or create a session manager that can be useful maintaining
   * session data within a clustered application server environment.  The
   * session manager handler is set to this handler
   */
  public void loadSessionManager()
  {
    sessionMgr = (SessionManager)session.getAttribute("sessionManager");
    if (sessionMgr == null)
    {
      sessionMgr = new MesSessionManager();
      sessionMgr.setSession(session);
      session.setAttribute("sessionManager",sessionMgr);
    }
  }
  
  /**
   * Main entry point: processes an http request.
   *
   * Basic flow:
   *
   *    HTTP request -> controller servlet -> request handler 
   *     -> action handler -> view target (via request handler forwarding)
   *
   * Request handlers are intended to be used once per request and then 
   * discarded -- so this method is not thread safe...
   */
  public void doRequest(Controller controller, HttpServletRequest request, 
    HttpServletResponse response)
  {
    try
    {
      // store some refs locally
      this.controller = controller;
      this.request    = request;
      this.response   = response;
      this.session    = request.getSession();
      this.user       = (UserBean)session.getAttribute("UserLogin");
      this.servletUrl = request.getServletPath();

      // do some per-request initializations

      // TODO: reduce amount of overhead in the following 
      // (i.e. make views and actions only reload if properties are changed)
      loadConfiguration();
      loadActions();
      loadPermissions();
      loadSessionManager();
      initializeHandler();

      // place this handler object into the request
      request.setAttribute("handler",this);
      
      // if user is not logged in forward to login page
      if (!validateUser(user))
      {
        doLoginForward();
      }
      // valid user so handle the requested action
      else
      {
        doAction();
        sessionMgr.clean();
      }
    }
    catch (Exception e)
    {
      doErrorForward(e);
    }
  }


  /**************************************************************************
   *
   *  Action handling
   *
   *************************************************************************/
  
  /**
   * Fetches the action def associated with the given name.
   */
  public ActionDef getActionDef(String actionName)
  {
    String findName = actionName;
    int qsStartIdx = findName.indexOf("?");
    if (qsStartIdx >= 0)
    {
      findName = findName.substring(0,qsStartIdx);
    }
    ActionDef actionDef = (ActionDef)actionMap.get(findName);
    if (actionDef == null)
    {
      throw new NullPointerException("No action with name " + findName
        + " registered");
    }
    return actionDef;
  }

  /**
   * The named action class is retrieved from the action map, an instance
   * is created, configured and returned.
   */
  protected Action getAction(String actionName)
  {
    // retrieve action def, get the action class from the def
    ActionDef actionDef = getActionDef(actionName);
    if (actionDef != null)
    {
      // instantiate action
      Class actionClass = actionDef.getActionClass();
      Action action = null;
      try
      {
        action = (Action)actionClass.newInstance();
      }
      catch (Exception e)
      {
        if (e instanceof RuntimeException)
        {
          throw (RuntimeException)e;
        }
        throw new RuntimeException("Error: " + e,e);
      }
      log.debug("about to setHandler in for action of type " + action.getClass().getName());
      action.setHandler(this,actionName);
      return action;
    }
    return null;
  }
  
  protected void setCurAction(Action curAction)
  {
    this.curAction = curAction;
    getCurLink().setActionDef(curAction.getActionDef());
  }

  /**
   * Fetches the action with the given action name and executes it.  User
   * permissions are checked to make sure user can perform the action.
   */
  public void doAction(String actionName)
  {
    // get an action handler with the action name, make it available in the request
    setCurAction(getAction(actionName));

    // make sure user has rights to do action      
    if (!permissions.hasPermissions(user,actionName))
    {
      // send user off to insufficient rights screen
      doInsufficientRightsForward(actionName);
    }
    else
    {
      // exceute the action
      curAction.execute();
    }
  }

  protected void doAction()
  {
    // look for new style action name 
    // .../xyz/actionName?parm=abc...
    String actionName = null;
    String pathInfo = request.getPathInfo();
    if (pathInfo != null)
    {
      // remove leading '/'
      actionName = pathInfo.substring(1);
    }
    // look for old style action name in query parameters
    // i.e. .../xyz?action=actionName&parm=abc...
    else
    {
      actionName = request.getParameter("action");
    }

    // do the found action
    if (actionName != null && actionName.length() > 0)
    {
      doAction(actionName);
    }
    // or default action if none was specified
    else
    {
      doAction(getProperty(Mes.PN_DEFAULT_ACTION_NAME));
    }

  }


  /**************************************************************************
   *
   *  Routing (to views, to actions, to actions via a redirect, 
   *  to a link target)
   *
   *************************************************************************/

  /**
   * Retrieves the named view from the view map.  Throws NullPointerException
   * if view is not found.
   */
  public View getView(String viewName)
  {
    View view = (View)viewMap.get(viewName);
    if (view == null)
    {
      throw new NullPointerException("View " + viewName + " does not exist");
    }
    return view;
  }

  /**
   * Redirect browser to the indicated path.
   */
  public void doRedirect(String redirectPath)
  {
    try
    {
      response.sendRedirect(redirectPath);
    }
    catch (Exception e)
    {
      throw new RuntimeException("Redirect failed in request handler " + 
        this.getClass().getName() + ": " + e);
    }
  }
  
  /**
   * Use request dispatcher to forward request to the indicated path.  Used
   * to forward to view targets where the view does not have an associated
   * template.  (Or by the doTemplateForward(...) after loading template data 
   * into the request).
   */
  protected void doDirectForward(String forwardPath)
  {
    try
    {
      controller.getServletConfig().getServletContext()
        .getRequestDispatcher(forwardPath).forward(request,response);
    }
    catch (Exception e)
    {
      throw new RuntimeException("Forward failed: " + e);
    }
  }
  
  /**
   * Displays some error info at the console, forwards to the designated
   * error page with a generic error message.
   */
  private void doErrorForward(Exception e)
  {
    log.error("Error in " + this.getClass().getName() + ": " + e);
    e.printStackTrace();
    // TODO: log this message in java_log table...
    // TODO: or make a log4j appender that does...
    // TODO: enhance view handling to allow query strings...
    curView = getView(Mes.PN_INVALID_REQUEST_VIEW);
    doDirectForward(curView.getTarget()
      + "?error=Unable to process request due to the following error: " + e);
  }
  
  /**
   * Forwards the browser to the login screen, attempts to store the
   * original request info so that user will be automatically returned
   * after login.
   */
  private void doLoginForward()
  {
    try
    {
      log.debug("User not logged in...");

      // look for the requested page object in the session
      RequestedPageBean requestedPage
        = (RequestedPageBean)session.getAttribute("RequestedPage");
      
      // create the requested page object if it isn't 
      // present in the session
      if (requestedPage == null)
      {
        requestedPage = new RequestedPageBean();
        session.setAttribute("RequestedPage",requestedPage);
      }
      
      // store the requested url in the requeste page url
      // so that the login page can redirect here if login
      // is required and is successful
      requestedPage.setRequestedPage(request);
      
      // try to determine the users login page from a client cookie object
      ClientLoginCookie clientCookie = ClientLoginCookie
        .getLoginCookie(request);
      StringBuffer loginPage = new StringBuffer("");
      if(clientCookie != null)
      {
        // client cookie found, use cookie login page
        loginPage.append(clientCookie.getLoginPage());
      }
      else
      {
        // no login cookie, use default login page
        loginPage.append(getView(Mes.PN_DEFAULT_LOGIN_VIEW).getTarget());
      }
      
      // append requested parameters to allow auto-login
      if (requestedPage.getQueryString() != null 
          && !requestedPage.getQueryString().equals(""))
      {
        loginPage.append("?");
        loginPage.append(requestedPage.getQueryString());
      }
      
      // forward to the login page
      doDirectForward(loginPage.toString());
    }
    catch (Exception e)
    {
      throw new RuntimeException("Login forward failed: " + e);
    }
  }
  
  /**
   * Forward request to a template handler with a target path as the main
   * page content.  This essentially emulates the functionality of the template 
   * use tag that would normally be used by jsp's that wish to invoke a 
   * template expansion.  See com.mes.tags.TemplateUseTag.doStartTag() and
   * .doEndTag().
   */
  protected void doTemplateForward(String targetPath, String targetTitle,
    String templatePath, List includes)
  {
    /*
    if (targetTitle == null)
    {
      targetTitle = targetPath;
      log.warn("No title provided for template target " + targetPath);
    }
    */

    targetTitle = targetTitle != null ? targetTitle : "";
    //  ? "<div class=\"title\">" + targetTitle + "</div>" : "";

    if (templatePath == null)
    {
      templatePath = getProperty(Mes.PN_DEFAULT_TEMPLATE_PATH);
    }

    // place a template stack in the request
    TemplateStack stack = new TemplateStack();
    request.setAttribute("template_stack",stack);
    
    // create a template stack frame to contain template parameters and push it
    // onto the template stack
    TemplateStackFrame frame =
      new TemplateStackFrame(targetPath + ".controller");

    // these will be retrieved in a layout jsp that includes the target
    // jsp and sets the target title to the given title
    // "parent" in this case refers to the target page
    frame.put("parent_page",new TemplatePageParameter(targetPath,false));
    frame.put("parent_title",new TemplatePageParameter(targetTitle,true));
    stack.push(frame);
    
    // place cur path attr in request to satisfy template expand tag
    request.setAttribute("template_cur_path",templatePath);

    // place copy of includes list in request 
    // (so further additions aren't permanantely added to the view's include list)
    request.setAttribute("includeList",new ArrayList(includes));
    
    // forward the request to the template handler jsp
    doDirectForward(templatePath);
  }
  
  /**
   * Forwards to designated insufficient rights notification screen.
   */
  private void doInsufficientRightsForward(String actionName)
  {
    log.debug(user.getLoginName() + " does not have rights to do "
      + actionName);
    doView(Mes.PN_BAD_RIGHTS_VIEW);
  }

  /**
   * Sets the current view, updates the current link's text with override
   * title if not null.
   */
  private void setCurView(View curView, String title)
  {  
    this.curView = curView;
    Link curLink = getCurLink();
    curLink.setView(curView);
    if (title != null)
    {
      curLink.setText(title);
    }
  }
  private void setCurView(View curView)
  {
    setCurView(curView,null);
  }
  
  /**
   * Forwards to the view, if title is not null it overrides the view's title.
   * Titles are only possible on views with template targets, titles given for
   * such views will be ignored.
   */
  public void doForward(View view, String title)
  {
    // set current view
    setCurView(view,title);

    // forward request directly to target
    if (!view.isTemplate())
    {
      if (title != null)
      {
        log.warn("Attempt to specify a title on non-template view '" 
          + view.getTarget() + "'");
      }
      doDirectForward(view.getTarget());
    }
    // forward request to a template
    else
    {
      // determine title to use with the target...
      // either override title or view's defined title
      title = title == null && view.isTitled() ? view.getTitle() : title;

      // forward to the template using view template if available
      doTemplateForward(view.getTarget(),title,
        view.isDefault() ? null : view.getTemplate(),view.getIncludes());
    }
  }

  /**
   * Forwards to the target specified by a view object.
   */
  public void doForward(View view)
  {
    doForward(view,null);
  }

  /**
   * Forwards request to the named view with the specified page title.
   */
  public void doView(String viewName, String title)
  {
    doForward(getView(viewName),title);
  }
  
  /**
   * Forwards request to the named view.
   */  
  public void doView(String viewName)
  {
    doView(viewName,null);
  }

  /**
   * Forwards request to the view contained in a link.
   */
  public void doLink(Link link)
  {
    View view = link.getView();
    if (view == null)
    {
      throw new NullPointerException("Attempt to forward to link with no view");
    }
    doForward(link.getView());
  }
    

  /**
   * Redirects the user's browser to the link.
   */
  public void doLinkRedirect(Link link)
  {
    doRedirect(link.renderUrl());
  }

  /**
   * Forces the user's browser to redirect to the controller servlet with
   * a request for the given action.  This allows simple action requests only,
   * i.e. additional request parameters are not supported with this currently.
   */
  public void doActionRedirect(String actionName)
  {
    doLinkRedirect(getLink(actionName));
  }

  /**
   * Same as doActionRedirect above, but adds otherArgs to the query string.
   */
  public void doActionRedirect(String actionName, String otherArgs)
  {
    doLinkRedirect(getLink(actionName,otherArgs));
  }

  /**
   * Forwards the request directly to a view without wrapping it in a template.
   * Used for responding to ajax requests.
   */
  public void doRender(String viewName)
  {
    View view = getView(viewName);
    log.debug("render fwd -> " + view);
    response.setHeader("Cache-Control","no-cache");
    response.setHeader("Pragma","no-cache");
    doDirectForward(view.getTarget());
  }


  /**************************************************************************
   *
   *  Link handling
   *
   *************************************************************************/

  private Link getLink(ActionDef actionDef, View view, String text, String otherArgs, 
    String prefix)
  {
    Link link = new MesLink();
    link.setServletUrl(servletUrl);
    link.setActionDef(actionDef);
    link.addArgStr(otherArgs);
    if (view != null)
    {
      link.setView(view);
    }
    else
    {
      link.setText(text);
    }
    link.setPrefix(prefix);
    return link;
  }
    
  private Link getLink(ActionDef actionDef, String text, String otherArgs)
  {
    return getLink(actionDef,null,text,otherArgs,null);
  }
    
  private Link getLink(ActionDef actionDef, View view, String otherArgs, 
    String prefix)
  {
    return getLink(actionDef,view,null,otherArgs,prefix);
  }

  public Link getDefaultLink()
  {
    return getLink(getActionDef(getProperty(Mes.PN_DEFAULT_ACTION_NAME)),
                   getView(getProperty(Mes.PN_DEFAULT_VIEW_NAME)),null,null);
  }

  /**
   * Generate a name based on an action name suitable for storing a back link
   * in the session.
   */
  private String getBackLinkName(String actionName)
  {
    return "backLink-" + actionName;
  }

  /**
   * Generate a name based on the back link name to store in session to
   * signal an action's back linking is disabled.
   */
  private String getDisableBackLinkName(String actionName)
  {
    return "disable-" + getBackLinkName(actionName);
  }

  /**
   * Used internally to remove back link disable flag from session.
   */
  private void enableBackLink(String actionName)
  {
    session.removeAttribute(getDisableBackLinkName(actionName));
  }

  /**
   * Stores a back link in the session.  Takes a from action used to name the
   * back link in the session such that it can be retrieved during that actions
   * execution (see getBackLink(...)).
   *
   * (root back link setter)
   */
  private void setBackLink(ActionDef fromActionDef, Link backLink)
  {
    // re-enable back link
    enableBackLink(getDisableBackLinkName(fromActionDef.getName()));

    // set back link
    session.setAttribute(getBackLinkName(fromActionDef.getName()),backLink);
  }

  /**
   * Generates a back link for the given to action and view and sets it as the
   * back link for the given from action.
   */
  private void setBackLink(ActionDef fromActionDef, ActionDef toActionDef, 
    View toView, String otherArgs)
  {
    setBackLink(fromActionDef,getLink(toActionDef,toView,otherArgs,"Back to"));
  }

  /**
   * Used to manually generate a back link in the session when a back link
   * target does not directly link to the from action it is associated with.
   * Intended to be used during action execution.
   */
  public void setBackLink(String fromActionName, String toActionName, 
    String toViewName, String otherArgs)
  {
    setBackLink(getActionDef(fromActionName),
                getActionDef(toActionName),
                getView(toViewName),
                otherArgs);
  }

  /**
   * Sets the back link for a from action to the given link.
   */
  public void setBackLink(String fromActionName, Link backLink)
  {
    setBackLink(getActionDef(fromActionName),backLink);
  }

  /**
   * Generate action request link anchor tags with given text.
   * Versions allow for a view title to be used as link text and a prefix 
   * for the main link text (useful in conjunction with view titles).
   * Args versions provided to allow additional query string args to be
   * added to the URL.
   */

  /**
   * Generate action request links suitable for rendering an action url 
   * (i.e. for a form post url).  These do not set a view or link text so they
   * may only be used for url rendering (not html).
   */
  public Link getLink(String actionName, String otherArgs)
  {
    return getLink(getActionDef(actionName),null,otherArgs);
  }
  public Link getLink(String actionName)
  {
    return getLink(actionName,null);
  }
  public Link getLink()
  {
    return getLink(curAction.getName());
  }

  /*
   * These generate links without automatically creating a back link for the
   * current action.
   */
  public Link generateLink(String actionName, String otherArgs, String text)
  {
    return getLink(getActionDef(actionName),text,otherArgs);
  }
  public Link generateLink(String actionName, String viewName, String otherArgs,
    String prefix)
  {
    return getLink(getActionDef(actionName),getView(viewName),otherArgs,prefix);
  }

  /**
   * Constructs a link object that represents the current request for use
   * in generating back links, etc.  If cur action is handling an ajax request
   * then treat the back link as the current link, preventing ajax requests
   * from being used as back links, etc.
   */
  public Link getCurLink(String prefix)
  {
    // generate a link object representing the current request
    if (curLink == null)
    {
      // scan through parms, pull out action name, generate "otherArgs" with rest
      StringBuffer otherBuf = null;
      boolean discardFlag = false;
      for (Enumeration e = request.getParameterNames(); 
           e.hasMoreElements() && !discardFlag;)
      {
        String parmName = (String)e.nextElement();
        discardFlag = parmName.equals(Mvc.DISCARD_ARGS_FLAG);
        if (!discardFlag)
        {
          String parmValue = request.getParameter(parmName);
          if (!parmName.equals("action") && parmValue != null 
              && parmValue.length() > 0)
          {
            if (otherBuf == null)
            {
              otherBuf = new StringBuffer();
            }
            else
            {
              otherBuf.append("&");
            }
            try
            {
              parmName = URLDecoder.decode(parmName,"UTF-8");
              parmName = URLEncoder.encode(parmName,"UTF-8");
              parmValue = URLDecoder.decode(parmValue,"UTF-8");
              parmValue = URLEncoder.encode(parmValue,"UTF-8");
            }
            catch (Exception ee) { }
            otherBuf.append(parmName + "=" + parmValue);
          }
        }
      }
      String otherArgs = !discardFlag && otherBuf != null ? ""+otherBuf : null;
      curLink = getLink(curAction.getActionDef(),curView,otherArgs,prefix);
    }
    return curLink;
  }
  public Link getCurLink()
  {
    return getCurLink("Back to");
  }

  /**
   * Fetches back link using the given action name from the session, if it exists.
   */
  public Link getBackLink(String actionName)
  {
    return (Link)session.getAttribute(getBackLinkName(actionName));
  }
  public Link getBackLink()
  {
    return getBackLink(curAction.getName());
    /*
    Link backLink = getBackLink(curAction.getName());
    if (backLink == null)
    {
      backLink = getDefaultLink();
    }
    return backLink;
    */
  }

  /**
   * Stores a back link in the session from the named action to the current
   * action and view.  Used to automatically create session back links during
   * "forward" link generation.
   */
  public void autoSetBackLinkFor(String actionName)
  {
    Link backLink 
      = curAction.getActionDef().isAjax() ? getBackLink() : getCurLink();
    setBackLink(getActionDef(actionName),backLink);
  }

  /**
   * Disables back link for the named action.  Setting back link for the
   * action will re-enable back linking.
   */
  public void disableBackLink(String actionName)
  {
    session.setAttribute(getDisableBackLinkName(actionName),"disable");
  }
  public void disableBackLink()
  {
    disableBackLink(curAction.getName());
  }

  /**
   * Template layout pages can use this to determine if back link has been 
   * disabled for a particular action.
   */
  public boolean backLinkEnabled(String actionName)
  {
    return session.getAttribute(getDisableBackLinkName(actionName)) == null;
  }
  public boolean backLinkEnabled()
  {
    return backLinkEnabled(curAction.getName());
  }

  /*
   * Generate action request links.  First variation takes action name and link
   * text, second takes action name with a view name.  Both also take other args
   * strings (optional) and the view link allows an optional prefix to be prepended
   * to the view title.  These automatically generate a back link for the named
   * action and store them in the session.  These are suitable for html anchor tag
   * rendering.
   */
  public Link getLink(String actionName, String otherArgs, String text)
  {
    autoSetBackLinkFor(actionName);
    return generateLink(actionName,otherArgs,text);
  }
  public Link getLink(String actionName, String viewName, String otherArgs,
    String prefix)
  {
    autoSetBackLinkFor(actionName);
    return generateLink(actionName,viewName,otherArgs,prefix);
  }

  public Action getCurAction()
  {
    return curAction;
  }
  public ActionDef getCurActionDef()
  {
    return getCurAction().getActionDef();
  }

  public boolean userHasPermission(UserBean user, String actionName)
  {
    return permissions.hasPermissions(user,actionName);
  }
  public boolean userHasPermission(String actionName)
  {
    return userHasPermission(user,actionName);
  }

}
