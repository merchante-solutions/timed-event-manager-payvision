package com.mes.mvc.mes;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.apache.log4j.Logger;
import com.mes.mvc.ApiException;
import com.mes.mvc.ApiMultipartFileHandler;
import com.mes.mvc.ApiRequest;
import com.mes.mvc.ApiResponse;
import com.mes.mvc.RequestHandler;
import com.mes.user.UserBean;

public class MesApiAction extends MesAction
{
  static Logger log = Logger.getLogger(MesApiAction.class);

  protected ApiRequest  apiRequest;
  protected ApiResponse apiResponse;
  
  /**
   * Child classes should override this to provide their own custom
   * multipart file handling.
   */
  protected ApiMultipartFileHandler getMultipartFileHandler()
  {
    return null;
  }

  /**
   * Overrides parent class to do some api specific tasks prior to the
   * action handling method being invoked.  Primarily this is calling
   * the requests receive method and authenticating user credentials.
   */
  public void setHandler(RequestHandler handler, String name)
  {
    super.setHandler(handler,name);

    // fetch api request object, register file handling and parm def set
    this.apiRequest = ((MesApiRequestHandler)handler).getApiRequest();
    if (actionDef.getParameterDefSet() != null)
    {
      apiRequest.setParameterDefSet(actionDef.getParameterDefSet());
    }
    
    // before receive, need to fetch any custom multi part file handlers
    // from child classes if they need it
    ApiMultipartFileHandler mph = getMultipartFileHandler();
    if (mph != null)
    {
      apiRequest.setMultipartFileHandler(mph);
    }

    // receive and validate request data
    // (needs to happen before authenticate user is called)
    apiRequest.receive();

    // validate user from request data
    authenticateUser();

    this.apiResponse = ((MesApiRequestHandler)handler).getApiResponse();
  }

  /**
   * Create and authenticate user bean from id and password in request
   * parameters.
   *
   * This is a bit of a hack, normally user authentication and the 
   * UserBean are loaded in the request handler (from the session)
   * and passed to the action handler during initialization.  This 
   * hack allows user authentication to occur from parameter data 
   * uploaded in multipart encoded requests.  Its also more in line 
   * with the api model where user authentication occurs in each 
   * request rather than once at the beginning of a session.
   */
  protected void authenticateUser()
  {
    // get user login name
    String userName =  apiRequest.getParameter(MesApiRequest.PARM_USER_ID);
    if (userName == null)
    {
      throw new MesApiMissingParmException(MesApiRequest.PARM_USER_ID);
    }

    // get password
    String password = apiRequest.getParameter(MesApiRequest.PARM_USER_PASS);
    if (password == null)
    {
      throw new MesApiMissingParmException(MesApiRequest.PARM_USER_PASS);
    }

    // create user bean, authenticate user credentials
    user = new UserBean();
    if (!user.validate(userName,password,request))
    {
      throw new ApiException(MesApiResponse.RC_AUTHENTICATE_ERROR);
    }
  }

  /**
   * Overridden to pass through ApiExceptions.
   */
  protected boolean executeByName()
  {
    try
    {
      Method method = null;
      try
      {
        String methodName = "do" + name.substring(0,1).toUpperCase() 
          + name.substring(1);
        method = this.getClass().getDeclaredMethod(methodName,null);
        method.invoke(this,null);
        return true;
      }
      catch (NoSuchMethodException nsme) { }
    }
    catch (InvocationTargetException ite)
    {
      if (ite.getCause() instanceof ApiException)
      {
        throw (ApiException)(ite.getCause());
      }
      log.error("Error in invoked method: " + ite.getCause());
      ite.getCause().printStackTrace();
      throw new RuntimeException(ite);
    }
    catch (Exception e)
    {
      log.error("Error invoking action method by name: " + e);
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    return false;
  }
}