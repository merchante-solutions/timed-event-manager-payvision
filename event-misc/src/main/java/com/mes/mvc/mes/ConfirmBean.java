package com.mes.mvc.mes;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.HiddenField;
import com.mes.mvc.Action;

public class ConfirmBean extends MesViewBean
{
  static Logger log = Logger.getLogger(ConfirmBean.class);
  
  public static final String FN_CONFIRM_ACTION  = MesAction.FN_CONFIRM_ACTION;
  public static final String FN_NO_BTN          = "noBtn";
  public static final String FN_YES_BTN         = "yesBtn";

  public ConfirmBean(Action action)
  {
    super(action);
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_CONFIRM_ACTION));
      fields.add(new ButtonField(FN_NO_BTN,"No"));
      fields.add(new ButtonField(FN_YES_BTN,"Yes"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public boolean isConfirmed()
  {
    return getData(FN_YES_BTN).length() > 0;
  }

  public boolean isCanceled()
  {
    return getData(FN_NO_BTN).length() > 0;
  }
}