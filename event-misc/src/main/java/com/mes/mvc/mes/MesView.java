package com.mes.mvc.mes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.log4j.Logger;
import com.mes.mvc.View;

public class MesView implements View
{
  static Logger log = Logger.getLogger(MesView.class);
  
  private String target;
  private String title;
  private String template;
  private List includes = new ArrayList();
  
  /**
   * Given a string and a delimiter string an array is generated containing
   * all substrings separated by the delimiter.
   *
   * NOTE: this should be replaced with java.lang.String.split() once we are
   *       all on jdk 1.4
   */
  private String[] split(String str, String delim)
  {
    StringTokenizer tok = new StringTokenizer(str,delim);
    String[] strs = new String[tok.countTokens()];
    for (int i = 0; i < strs.length; ++i)
    {
      strs[i] = tok.nextToken().trim();
    }
    return strs;
  }
  
  /**
   * Parses a view property value, determines a path and possibly a title
   * if it's present.
   */
  public void parseProperty(String property)
  {
    String[] strs = split(property,":");
    if (strs.length == 1)
    {
      target    = strs[0].trim();
    }
    else if (strs.length == 2)
    {
      target    = strs[0].trim();
      title     = strs[1].trim();
    }
    else if (strs.length == 3)
    {
      target    = strs[0].trim();
      title     = strs[1].trim();
      template  = strs[2].trim();
    }
    else
    {
      throw new RuntimeException("Invalid view property value: '" + property + "'");
    }
  }
  
  public void setTarget(String target)
  {
    this.target = target;
  }
  public String getTarget()
  {
    return target;
  }
  
  public void setTitle(String title)
  {
    this.title = title;
  }
  public String getTitle()
  {
    if (title == null)
    {
      throw new NullPointerException("View title is null");
    }
    return title;
  }
  
  public String getTemplate()
  {
    if (template == null)
    {
      throw new NullPointerException("View template is null");
    }
    return template;
  }
  public void setTemplate(String template)
  {
    this.template = template;
  }

  public List getIncludes()
  {
    return includes;
  }
  public void addInclude(String include)
  {
    // discard duplicates...
    for (Iterator i = includes.iterator(); i.hasNext();)
    {
      if (i.next().equals(include))
      {
        return;
      }
    }
    includes.add(include);
  }

  public boolean isTitled()
  {
    return title != null;
  }

  /**
   * Returns true if this view uses a template.
   */
  public boolean isTemplate()
  {
    return (title != null);
  }

  /**
   * Returns true if this view uses a default template.
   */
  public boolean isDefault()
  {
    return (template == null);
  }

  public String toString()
  {
    return "View [ "
      + "target: " + target
      + (isTemplate() ? ", title: " + title : "")
      + (!isDefault() ? ", template: " + template : "")
      + " ]";
  }
}
  