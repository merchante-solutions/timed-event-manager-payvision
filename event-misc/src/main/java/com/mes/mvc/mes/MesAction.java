package com.mes.mvc.mes;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldError;
import com.mes.mvc.Action;
import com.mes.mvc.ActionDef;
import com.mes.mvc.Downloadable;
import com.mes.mvc.Link;
import com.mes.mvc.Menu;
import com.mes.mvc.Mvc;
import com.mes.mvc.RequestHandler;
import com.mes.mvc.SessionManager;
import com.mes.user.UserBean;

//public abstract class MesAction implements Action
public class MesAction implements Action
{
  static Logger log = Logger.getLogger(MesAction.class);

  // confirmation bean field names
  public static final String FN_CONFIRM_MSG     = "confirmMsg";
  public static final String FN_CONFIRM_ACTION  = "confirmAction";  

  protected RequestHandler      handler;
  protected HttpServletRequest  request;
  protected HttpServletResponse response;
  protected HttpSession         session;
  protected SessionManager      sessionMgr;
  protected String              name;
  protected ActionDef           actionDef;
  protected UserBean            user;
  protected Map                 menuMap;

  public void setHandler(RequestHandler handler, String name)
  {
    this.handler  = handler;
    this.name     = name;
    request       = handler.getRequest();
    response      = handler.getResponse();
    session       = handler.getSession();
    sessionMgr    = handler.getSessionManager();
    user          = handler.getUser();
    menuMap       = handler.getMenuMap();
    actionDef     = handler.getActionDef(name);
  }

  public RequestHandler getHandler()
  {
    return handler;
  }

  public HttpServletRequest getRequest()
  {
    return request;
  }

  public HttpServletResponse getResponse()
  {
    return response;
  }

  public HttpSession getSession()
  {
    return session;
  }

  public String getName()
  {
    return name;
  }

  public ActionDef getActionDef()
  {
    return actionDef;
  }

  public boolean isAjax()
  {
    return actionDef.isAjax();
  }

  public UserBean getUser()
  {
    return user;
  }

  public Map getMenuMap()
  {
    return menuMap;
  }
  
  public String toString()
  {
    return "Action [ name: " + name + ", class: " 
      + this.getClass().getName() + ", ajax: " + isAjax() + " ]";
  }

  protected String getProperty(String name)
  {
    return handler.getProperty(name);
  }


  /**************************************************************************
   *
   *  Request data storage and retrieval 
   *    (see confirmation for how this is used, attribute/session accessors
   *     below to see how it's accessed)
   *
   *************************************************************************/

  public static final String RESTORED_REQUEST_DATA = "restoredRequestData";

  private String getRdName(String actionName)
  {
    return "requestData-" + actionName;
  }

  /**
   * Stores request data for a particular action in the session for later 
   * retrieval.
   */
  protected void saveRequestData(String actionName)
  {
    session.setAttribute(getRdName(actionName),new RequestData(request));
  }

  /**
   * Retrieves action request data that has been stored in the session under
   * the given action name.  The request data object is removed from the session
   * if found.  The restored data is placed in the request object so that
   * it is accessible by this action and any other action handlers chained to 
   * by this one.
   */
  protected void restoreRequestData(String actionName)
  {
    String rdName = getRdName(actionName);

    RequestData rd = (RequestData)session.getAttribute(rdName);

    if (rd == null)
    {
      throw new NullPointerException("Stored request data for " + actionName 
        + " not found");
    }

    session.removeAttribute(rdName);
    request.setAttribute(RESTORED_REQUEST_DATA,rd);
  }


  /**************************************************************************
   *
   *  Request and Session data handling, these will find data in restored
   *  action request data if available.
   *
   *************************************************************************/

  /**
   * Request parameter accessors
   *
   * Accessors for grabbing parameter data out of the request.  Will find 
   * parameters in restored request data if not found in actual request object.
   * Default values can be given to return if no request data is found.  When
   * default values aren't given and request data is not found an exception
   * is thrown.  Some string-to-primitive converters may be provided if 
   * convenient.
   */

  public String getParm(String name, String defaultVal)
  {
    String parm = request.getParameter(name);
    if (parm == null)
    {
      RequestData rd = (RequestData)request.getAttribute(RESTORED_REQUEST_DATA);
      if (rd != null)
      {
        parm = rd.getRequest().getParameter(name);
      }
    }
    return parm == null ? defaultVal : parm;
  }
  public String getParm(String name)
  {
    String parm = getParm(name,null);
    if (parm == null)
    {
      throw new RuntimeException("Invalid request, missing parameter '" + name
        + "'");
    }
    return parm;
  }


  private long parmToLong(String name, String strval)
  {
    try
    {
      return Long.parseLong(strval);
    }
    catch (NumberFormatException nfe)
    {
      throw new RuntimeException("Invalid long value '" + strval 
        + "' in parameter " + name + " (" + nfe + ")");
    }
  }
  public long getParmLong(String name, long defaultVal)
  {
    return parmToLong(name,getParm(name,String.valueOf(defaultVal)));
  }
  public long getParmLong(String name)
  {
    return parmToLong(name,getParm(name));
  }


  private int parmToInt(String name, String strval)
  {
    try
    {
      return Integer.parseInt(strval);
    }
    catch (NumberFormatException nfe)
    {
      throw new RuntimeException("Invalid long value '" + strval 
        + "' in parameter " + name + " (" + nfe + ")");
    }
  }
  public int getParmInt(String name, int defaultVal)
  {
    return parmToInt(name,getParm(name,String.valueOf(defaultVal)));
  }
  public int getParmInt(String name)
  {
    return parmToInt(name,getParm(name));
  }


  /**
   * Request attribute accessors
   *
   * Accessors for grabbing attribute data out of the request.  Will find 
   * attributes in restored request data if not found in actual request object.
   * Default values can be given to return if no request data is found.  When
   * default values aren't given and request data is not found an exception
   * is thrown.
   */
  public Object getRequestAttr(String name, Object defaultVal)
  {
    Object attribute = request.getAttribute(name);
    if (attribute == null)
    {
      RequestData rd = (RequestData)request.getAttribute(RESTORED_REQUEST_DATA);
      if (rd != null)
      {
        attribute = rd.getRequest().getAttribute(name);
      }
    }
    return attribute == null ? defaultVal : attribute;
  }

  public Object getRequestAttr(String name)
  {
    Object attribute = getRequestAttr(name,null);
    if (attribute == null)
    {
      throw new RuntimeException("Missing attribute '" + name + "'");
    }
    return attribute;
  }

  public void setRequestAttr(String name, Object attribute)
  {
    request.setAttribute(name,attribute);
  }

  /**
   * Session attribute accessors
   *
   * Sets and fetches attributes in the session.  Default value may be provided,
   * if not given and not found exception will be thrown.
   */
  public Object getSessionAttr(String name, Object defaultVal)
  {
    //Object attr = sessionMgr.get(name);
    Object attr = session.getAttribute(name);
    return attr == null ? defaultVal : attr;
  }

  public Object getSessionAttr(String name)
  {
    Object attr = getSessionAttr(name,null);
    if (attr == null)
    {
      throw new RuntimeException("Attribute '" + name + "' not found in session");
    }
    return attr;
  }

  public void setSessionAttr(String name, Object val)
  {
    /*
    if (val instanceof Serializable)
    {
      sessionMgr.set(name,(Serializable)val);
    }
    else
    {
      session.setAttribute(name,val);
    }
    */
    session.setAttribute(name,val);
  }

  public void removeSessionAttr(String name)
  {
    //sessionMgr.remove(name);
    session.removeAttribute(name);
  }


  /**************************************************************************
   *
   *  Confirmation handling (see ConfirmAction for additional info)
   *
   *************************************************************************/

  /**
   * Generates an attribute name to use to store the confirmation flag for an
   * action.
   */
  protected String getCfName(String actionName)
  {
    return "confirmFlag-" + actionName;
  }

  /**
   * Looks for a positive confirmation flag in the request attributes, returns
   * true if the user has confirmed the originally requested action.
   */
  public boolean isConfirmed()
  {
    return getRequestAttr(getCfName(name),"").toString().equals("yes");
  }

  /**
   * Returns true if a user hit the no button in response to a confirmation dialog.
   */
  public boolean isCanceled()
  {
    return getRequestAttr(getCfName(name),"").toString().equals("no");
  }

  /**
   * Forward user to the confirmation dialog action handler with the provided
   * confirmation message to prompt the user for approval to complete the
   * requested action.
   */
  public void confirm(String confirmMsg)
  {
    saveRequestData(name);
    setRequestAttr(FN_CONFIRM_MSG,confirmMsg);
    setRequestAttr(FN_CONFIRM_ACTION,name);
    doAction(Mvc.ACTION_CONFIRM);
  }


  /**************************************************************************
   *
   *  Feedback handling
   *
   *************************************************************************/

  public void addFeedback(String message)
  {
    FeedbackContainer fc = (FeedbackContainer)getSessionAttr("feedback",null);
    if (fc == null)
    {
      fc = new FeedbackContainer();
      setSessionAttr("feedback",fc);
    }
    fc.addMessage(message);
  }

  public void addFeedback(FieldBean bean)
  {
    if (bean.hasErrors())
    {
      for (Iterator i = bean.getErrors().iterator(); i.hasNext();)
      {
        Object err = i.next();
        if (err instanceof FieldError && 
            ((FieldError)err).getField().getShowErrorText())
        {
          continue;
        }
        addFeedback(""+err);
      }
    }
  }


  /**************************************************************************
   *
   *  Back link handling
   *
   *************************************************************************/

  protected void setBackLink(String fromActName, String toActName, 
    String toViewName, String otherArgs)
  {
    handler.setBackLink(fromActName,toActName,toViewName,otherArgs);
  }

  protected void setBackLink(String fromActName, Link backLink)
  {
    handler.setBackLink(fromActName,backLink);
  }

  protected Link getBackLink()
  {
    return handler.getBackLink(name);
  }

 
  /**************************************************************************
   *
   * Routing (to views, to actions, to actions via a redirect, to a link 
   * target).  These are intended to be invoked internally by the action 
   * handler during the execution cycle as the final step.
   *
   *************************************************************************/

  protected void doView(String viewName)
  {
    handler.doView(viewName);
  }

  protected void doView(String viewName, String title)
  {
    handler.doView(viewName,title);
  }

  protected void doAction(String actionName)
  {
    handler.doAction(actionName);
  }

  protected void doActionRedirect(String actionName)
  {
    handler.doActionRedirect(actionName);
  }

  protected void doActionRedirect(String actionName, String otherArgs)
  {
    handler.doActionRedirect(actionName,otherArgs);
  }

  protected void doLinkRedirect(Link link)
  {
    handler.doLinkRedirect(link);
  }

  /**
   * New type of response, renders a view without attempting to inject it into
   * a template.  Used for ajax responses.
   */
  protected void doRender(String viewName)
  {
    handler.doRender(viewName);
  }

  /**************************************************************************
   *
   * Auto feedback generation
   *
   *************************************************************************/

  /**
   * Ovverride to provide a descriptor of a bean, etc. being manipulated
   * by a particular action i.e. "Part Type"
   */
  public String getActionObject(String actionName)
  {
    return null;
  }
  public String getActionResult(String actionName)
  {
    return null;
  }

  /**
   * Feedback generation for simple actions.
   */
  protected String getActionResult()
  {
    String actionResult = getActionResult(name);
    if (actionResult == null)
    {
      log.warn("Action result not defined for action " + name);
      actionResult = "";
    }
    return actionResult;
  }

  protected String getActionObject()
  {
    String actionObject = getActionObject(name);
    if (actionObject == null)
    {
      log.warn("Action object not defined for action " + name);
      actionObject = "";
    }
    return actionObject;
  }
    
  /**
   * These generate feedback text suitable for displaying to the user and/or
   * logging in the action log.
   */
  private String getFeedback(String description, String modifier)
  {
    return getActionObject() + " " + description + " " 
      + (modifier != null ? modifier + " " : "" ) + getActionResult();
  }
  protected String getFeedback(String description)
  {
    return getFeedback(description,null);
  }
  protected String getFeedback(long id)
  {
    return getFeedback(String.valueOf(id));
  }
  protected String getNegFeedback(String description)
  {
    return getFeedback(description,"not");
  }
  protected String getNegFeedback(long id)
  {
    return getNegFeedback(String.valueOf(id));
  }
  protected String getIllFeedback(String description)
  {
    return getFeedback(description,"cannot be");
  }
  protected String getIllFeedback(long id)
  {
    return getIllFeedback(String.valueOf(id));
  }


  /**
   * Automatically generate feedback and log actions in addition to doing
   * a redirect.
   */
  protected void redirectWithFeedback(String toName, String feedback)
  {
    addFeedback(feedback);
    doActionRedirect(toName);
  }
  protected void redirectWithFeedback(Link toLink, String feedback)
  {
    addFeedback(feedback);
    doLinkRedirect(toLink);
  }

  /**
   * File download support.
   */

  protected void doCsvDownload(Downloadable bean)
  {
    String filename = bean.getDlFilename();
    try
    {
      bean.startDownload();

      response.setContentType("application/csv");
      String cd = "attachment; filename=" + filename
        + (filename.endsWith(".csv") ? "" : ".csv");
      response.setHeader("Content-Disposition",cd);
     
      ServletOutputStream out = response.getOutputStream();

      String line = bean.getDlHeader();
      if (line != null)
      {
        out.println(bean.getDlHeader());
      }

      while ((line = bean.getDlNext()) != null)
      {
        out.println(line);
      }
    }
    catch (Exception e)
    {
      String errorMsg = "Error downloading file '" + filename + "' from data in " 
        + bean.getClass().getName();
      log.error(errorMsg);
      throw new RuntimeException(errorMsg);
    }
    finally
    {
      try { bean.finishDownload(); } catch (Exception e) { }
    }
  }

  /**
   * DB menu support (menu_items driven menus)
   */
  protected void doMesMenu()
  {
    int menuId = getParmInt("menuId",-1);
    if (menuId != -1)
    {
      Menu menu = MesDbMenuLoader.load(handler,menuId);
      if (menu != null)
      {
        setRequestAttr("menu",menu);
        doView(Mvc.PN_MENU_VIEW,menu.getTitle());
      }
      else
      {
        redirectWithFeedback(handler.getBackLink(),"Unable to access menu with id " + menuId);
      }
    }
    else
    {
      redirectWithFeedback(handler.getBackLink(),"Missing menu id");
    }
  }

  protected boolean executeByName()
  {
    try
    {
      Method method = null;
      try
      {
        String methodName = "do" + name.substring(0,1).toUpperCase() 
          + name.substring(1);
        method = this.getClass().getDeclaredMethod(methodName,null);
        method.invoke(this,null);
        return true;
      }
      catch (NoSuchMethodException nsme) { }
    }
    catch (InvocationTargetException ite)
    {
      log.error("Error in invoked method: " + ite.getCause());
      ite.printStackTrace();
      throw new RuntimeException(ite);
    }
    catch (Exception e)
    {
      log.error("Error invoking action method by name: " + e);
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    return false;
  }

  protected void doDummyAction()
  {
    log.warn("No action handler implementation for action named '" + name + "'");
    redirectWithFeedback(handler.getBackLink(),
      "Action handler under construction: " + name);
  }

  protected void sendAjaxResponse(String ajaxData)
  {
    try
    {
      response.getWriter().println(ajaxData);
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  public void execute()
  {
    if (!executeByName())
    {
      doDummyAction();
    }
  }
}