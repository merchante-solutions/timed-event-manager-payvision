package com.mes.mvc.mes;

import com.mes.mvc.ApiException;

public class MesApiMissingParmException extends ApiException
{
  public MesApiMissingParmException(String name)
  {
    super(MesApiResponse.RC_MISSING_PARM,"Missing parameter " + name);
  }
}
