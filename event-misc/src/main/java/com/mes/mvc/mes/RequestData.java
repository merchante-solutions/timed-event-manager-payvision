package com.mes.mvc.mes;

import javax.servlet.http.HttpServletRequest;

public class RequestData
{
  private HttpServletRequest request;

  public RequestData(HttpServletRequest request)
  {
    this.request = request;
  }

  public HttpServletRequest getRequest()
  {
    return request;
  }
}
