package com.mes.mvc.mes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.mes.mvc.ActionDef;
import com.mes.mvc.ApiException;
import com.mes.mvc.ApiRcDefSet;
import com.mes.mvc.ApiRequest;
import com.mes.mvc.ApiResponse;
import com.mes.mvc.Controller;

public class MesApiRequestHandler extends GenericRequestHandler
{
  static Logger log = Logger.getLogger(MesApiRequestHandler.class);

  private ApiRequest  apiRequest;
  private ApiResponse apiResponse;
  private ApiRcDefSet rcDefs;

  public void initializeHandler()
  {
    rcDefs = controller.getRcDefs();
  }

  /**
   * Create ApiRequest wrapper, process data (extract parameters, receive 
   * file upload data).
   */
  private void generateApiRequest()
  {
    apiRequest = new MesApiRequest(request);
  }

  /**
   * Create ApiResponse wrapper.
   */
  private void generateApiResponse()
  {
    apiResponse = new MesApiResponse(response,rcDefs);
  }

  /**
   * Send the api response.
   */
  private void sendResponse()
  {
    apiResponse.send();
  }

  /**
   * Set response code, send response.
   */
  private void sendResponse(String code, String msg)
  {
    apiResponse.setResponseCode(code,msg);
    sendResponse();
  }
  private void sendResponse(String code)
  {
    sendResponse(code,null);
  }
  private void sendResponse(ApiException ae)
  {
    sendResponse(ae.getCode(),ae.getMsg());
  }

  public ApiRequest getApiRequest()
  {
    return apiRequest;
  }

  public ApiResponse getApiResponse()
  {
    return apiResponse;
  }

  /**
   * Fetches the action def associated with the given name.
   */
  public ActionDef getActionDef(String actionName)
  {
    ActionDef actionDef = (ActionDef)actionMap.get(actionName);
    if (actionDef == null)
    {
      throw new MesApiInvalidRequestException(actionName);
    }
    return actionDef;
  }

  /**
   * Overrides MesRequestHandler so parameter defs can be injected into
   * the apiRequest object if they are present in the action def.
   */
  public void doAction(String actionName)
  {
    // get an action handler with the action name, make it available in the request
    setCurAction(getAction(actionName));

    // make sure user has rights to do action      
    if (!permissions.hasPermissions(user,actionName))
    {
      throw new ApiException(MesApiResponse.RC_AUTHENTICATE_ERROR);
    }

    // exceute the action
    curAction.execute();
  }

  /**
   * Main entry point: processes an http request.  Overrides 
   * MesRequestHandler.
   *
   * Basic flow:
   *
   *    HTTP request -> controller servlet -> request handler 
   *     -> action handler -> generate response data
   *
   * Request handlers are intended to be used once per request and then 
   * discarded -- so this method is not thread safe...
   */
  public void doRequest(Controller controller, HttpServletRequest request, 
    HttpServletResponse response)
  {
    try
    {
      // store some refs locally
      this.controller   = controller;
      this.request      = request;
      this.response     = response;
      this.session      = request.getSession();
      this.servletUrl   = request.getServletPath();

      // do some per-request initializations
      // TODO: reduce amount of overhead in the following 
      // (i.e. make views and actions only reload if properties are changed)
      loadConfiguration();
      loadActions();
      loadPermissions();
      loadSessionManager();
      initializeHandler();

      // create api request/response wrappers
      generateApiRequest();
      generateApiResponse();

      // place this handler object into the request
      request.setAttribute("handler",this);
      
      doAction();
      sessionMgr.clean();

      sendResponse();
    }
    catch (ApiException ae)
    {
      log.error("Api error: " + ae);
      sendResponse(ae);
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      sendResponse(MesApiResponse.RC_SYSTEM_ERROR);
      // notify developer...
    }
  }
}
