package com.mes.mvc.mes;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.mes.aus.file.GenUtil;
import com.mes.support.BaseModel;

public class ApiLogRecord extends BaseModel
{
  private long    logId;
  private Date    logDate;
  private String  serverName;
  private String  userName;
  private String  apiType;
  private String  action;
  private String  requestData;
  private String  responseData;
  private String  responseCode;
  private String  responseMsg;
  private String  merchNum;
  private String  profileId;
  private long    requestDuration;
  private String  ipAddress;

  public void setLogId(long logId)
  {
    this.logId = logId;
  }
  public long getLogId()
  {
    return logId;
  }

  public void setLogDate(Date logDate)
  {
    this.logDate = logDate;
  }
  public Date getLogDate()
  {
    return logDate;
  }
  public void setLogTs(Timestamp logTs)
  {
    logDate = toDate(logTs);
  }
  public Timestamp getLogTs()
  {
    return toTimestamp(logDate);
  }
  
  /**
   * Format log timestamp for display
   */
  public String getLogDateForDisplay()
  {
    return forDisplay(formatHtmlDateSecs(logDate));
  }

  public void setServerName(String serverName)
  {
    this.serverName = serverName;
  }
  public String getServerName()
  {
    return serverName;
  }
  
  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setApiType(String apiType)
  {
    this.apiType = apiType;
  }
  public String getApiType()
  {
    return apiType;
  }

  public void setAction(String action)
  {
    this.action = action;
  }
  public String getAction()
  {
    return action;
  }
  
  public void setResponseCode(String responseCode)
  {
    this.responseCode = responseCode;
  }
  public String getResponseCode()
  {
    return responseCode;
  }
  
  public void setResponseMsg(String responseMsg)
  {
    this.responseMsg = responseMsg;
  }
  public String getResponseMsg()
  {
    return responseMsg;
  }
  
  /** 
   * Format response code (zero pad to 3 digits)
   */
  public String getResponseCodeForDisplay()
  {
    return GenUtil.zeroPad(responseCode,3);
  }
  
  /** 
   * Format response code and message for display.
   */
  public String getResponseTextForDisplay()
  {
    return getResponseCodeForDisplay() + " - " + responseMsg;
  }

  public static class Parameter
  {
    private String name;
    private String value;
    
    public Parameter(String name, String value)
    {
      this.name = name;
      this.value = value;
    }
    
    public String getName()
    {
      return name;
    }
    
    public String getValue()
    {
      return value;
    }
  }  

  /**
   * Insert newlines at specified intervals if input data is long enough.
   */
  private String breakLongData(String data, int maxWidth)
  {
    StringBuffer buf = new StringBuffer();
    data = data == null ? "(no data)" : data;
    for (int i = 0; i < data.length(); i += maxWidth)
    {
      if (data.substring(i).length() > maxWidth)
      {
        buf.append(data.substring(i,i + maxWidth) + "<br>");
      }
      else
      {
        buf.append(data.substring(i));
      }
    }
    return ""+buf;
  }
  
  /**
   * Generates a list of ParameterDef objects from the given data string.
   * Used to parse request and response query strings.
   */
  private List getParameters(String data)
  {
    List l = new ArrayList();
    if (data != null)
    {
      Pattern p = Pattern.compile("([^=&]*)=([^&]*)");
      Matcher m = p.matcher(data);
      while (m.find())
      {
        l.add(new Parameter(m.group(1),m.group(2)));
      }
    }
    return l;
  }
  
  public void setRequestData(String requestData)
  {
    this.requestData = requestData;
  }
  public String getRequestData()
  {
    return requestData;
  }
  
  /**
   * Returns list of parameter objects parsed from request data.
   */
  public List getRequestParameters()
  {
    return getParameters(requestData);
  }

  /**
   * Break over long request data string into multiple lines by
   * inserting newlines.
   */
  public String getRequestDataForDisplay(int maxWidth)
  {
    return breakLongData(requestData,maxWidth);
  }
  
  public void setResponseData(String responseData)
  {
    this.responseData = responseData;
  }
  public String getResponseData()
  {
    return responseData;
  }
  
  /**
   * Returns list of parameter objects parsed from response data.
   */
  public List getResponseParameters()
  { 
    return getParameters(responseData);
  }
  
  /**
   * Break over long response data string into multiple lines by
   * inserting newlines.
   */
  public String getResponseDataForDisplay(int maxWidth)
  {
    return breakLongData(responseData,maxWidth);
  }
  
  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }
  
  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }
  
  public void setRequestDuration(long requestDuration)
  {
    this.requestDuration = requestDuration;
  }
  public long getRequestDuration()
  {
    return requestDuration;
  }
  
  /**
   * Convert duration milliseconds to seconds suitable for displaying.
   *
   * 246 milliseconds -> '0.246 secs'
   */
  public String getDurationForDisplay()
  {
    BigDecimal duration = new BigDecimal(""+requestDuration).setScale(3);
    duration = duration.divide(new BigDecimal("1000"), RoundingMode.HALF_UP);
    return ""+duration + " secs";
  }
  
  public void setIpAddress(String ipAddress)
  {
    this.ipAddress = ipAddress;
  }
  public String getIpAddress()
  {
    return ipAddress;
  }
  
  public String toString()
  {
    return "ApiLogRecord ["
      + "logId: " + logId
      + ", logDate: " + logDate
      + ", serverName: " + serverName
      + ", apiType: " + apiType
      + ", action: " + action
      + ", requestData: " + requestData
      + ", responseCode: " + responseCode
      + ", responseMsg: " + responseMsg
      + ", responseData: " + responseData
      + ", merchNum: " + merchNum
      + ", profileId: " + profileId
      + ", requestDuration: " + requestDuration
      + ", ipAddress: " + ipAddress
      + " ]";
  }
}