package com.mes.mvc.mes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FeedbackContainer
{
  private List messages = new ArrayList();

  /**
   * Adds message to the messages list.
   */
  public void addMessage(String message)
  {
    messages.add(message);
  }

  /**
   * Returns a string containing all messages separated by '<br>, suitable for
   * embedding in an html doc.  All messages are deleted.
   */
  public String displayMessages()
  {
    StringBuffer buf = new StringBuffer();
    for (Iterator i = messages.iterator(); i.hasNext();)
    {
      if (buf.length() > 0) buf.append("<br>");
      buf.append(i.next().toString());
      i.remove();
    }
    return buf.toString();
  }
}