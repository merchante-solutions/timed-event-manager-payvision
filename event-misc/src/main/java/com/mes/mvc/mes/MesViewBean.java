package com.mes.mvc.mes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HtmlBlock;
import com.mes.forms.HtmlRenderable;
import com.mes.forms.HtmlText;
import com.mes.mvc.Action;
import com.mes.mvc.Link;
import com.mes.mvc.Mvc;
import com.mes.mvc.RequestHandler;
import com.mes.mvc.tags.Viewable;

public abstract class MesViewBean extends FieldBean implements Viewable
{
  static Logger log = Logger.getLogger(MesViewBean.class);

  public static final String AN_SET_ORDER_HEADER = "actionSetOrderHeader";

  protected RequestHandler    handler;
  protected Action            action;
  protected String            beanName;
  protected int               beanIdx;

  public MesViewBean()
  {
  }
  public MesViewBean(Action action, String beanName, int beanIdx)
  {
    this.beanIdx = beanIdx;
    enableMultipart();
    setAction(action,beanName);
  }
  public MesViewBean(Action action, String beanName)
  {
    this(action,beanName,-1);
  }
  public MesViewBean(Action action)
  {
    this(action,null);
  }

  public int getBeanIdx()
  {
    return beanIdx;
  }

  public void setRequestHandler(RequestHandler handler)
  {
    this.handler = handler;
  }

  public void setBeanName(String beanName)
  {
    if (beanName != null)
    {
      this.beanName = beanName;
    }
  }
  public String getBeanName()
  {
    return beanName != null ? beanName : "viewBean";
  }

  public void setAction(Action action, String requestName)
  {
    this.action = action;
    setRequestHandler(action.getHandler());
    resetErrorState();
    autoSetFields(action.getRequest());
    setBeanName(requestName);
    action.getRequest().setAttribute(getBeanName(),this);
    if (isAutoSubmit() && !isAutoValid() && showFeedback())
    {
      ((MesAction)action).addFeedback(this);
      resetAutoSubmit();
    }
  }
  public void setAction(Action action)
  {
    setAction(action,getBeanName());
  }

  /**
   * Indexed name handling, overrides base field bean field accessors.
   */
  protected String indexedName(String name)
  {
    return beanIdx != -1 ? name + beanIdx : name;
  }

  public Field getField(String name)
  {
    return super.getField(indexedName(name));
  }

  public String getData(String name)
  {
    return super.getData(indexedName(name));
  }

  public void setData(String name, String data)
  {
    super.setData(indexedName(name),data);
  }

  /**
   * Viewable implementation
   */

  public List getRows()
  {
    log.warn("getRows() not implemented in child, returning empty row list");
    return new ArrayList();
  }

  public boolean isReversed()
  {
    return false;
  }

  public int getLeadCount()
  {
    return 0;
  }

  public int getTrailCount()
  {
    return 0;
  }

  public boolean isExpanded()
  {
    return true;
  }

  public boolean exceedsLimits()
  {
    return getRows().size() > getLeadCount() + getTrailCount();
  }

  /**
   * Order handling
   */

  private Map orderMap = new HashMap();

  private String curHeaderName;
  private boolean orderDirFlag;

  public class OrderInfo
  {
    public String orderName;
    public boolean defaultDirFlag;
    public OrderInfo(String orderName, boolean defaultDirFlag)
    {
      this.orderName = orderName;
      this.defaultDirFlag = defaultDirFlag;
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    fields.add(new Field(AN_SET_ORDER_HEADER,100,100,true));
  }

  protected void putOrderHeader(String headerName, String orderName, 
    boolean defaultDirFlag)
  {
    orderMap.put(headerName,new OrderInfo(orderName,defaultDirFlag));
  }
  protected void putOrderHeader(String headerName, String orderName)
  {
    putOrderHeader(headerName,orderName,false);
  }

  protected boolean setOrderHeader(String headerName)
  {
    boolean setOk = false;
    if (curHeaderName == null || !curHeaderName.equals(headerName))
    {
      if (orderMap.get(headerName) != null)
      {
        curHeaderName = headerName;
        OrderInfo oi = (OrderInfo)orderMap.get(headerName);
        orderDirFlag = oi.defaultDirFlag;
        setOk = true;
      }
    }
    else
    {
      orderDirFlag = !orderDirFlag;
      setOk = true;
    }
    log.debug("Order set to " + curHeaderName + " (dir flag " + orderDirFlag + ")");
    return setOk;
  }

  protected String getOrderName()
  {
    OrderInfo oi = (OrderInfo)orderMap.get(curHeaderName);
    return oi != null ? oi.orderName : null;
  }

  protected boolean getOrderDirFlag(boolean defaultDirFlag)
  {
    return getOrderName() != null ? orderDirFlag : defaultDirFlag;
  }

  protected String getOrderName(String defaultOrderName)
  {
    String orderName = getOrderName();
    return orderName != null ? orderName : defaultOrderName;
  }

  public HtmlRenderable getOrderHeader(String headerName)
  {
    if (orderMap.get(headerName) == null)
    {
      return new HtmlText(headerName);
    }

    Link link = handler.getLink(action.getName());
    link.addArg(AN_SET_ORDER_HEADER,headerName);
    link.addArg(Mvc.DISCARD_ARGS_FLAG);
    link.setText(headerName);

    HtmlBlock block = new HtmlBlock();
    block.add(link.getHtmlLink());
    if (curHeaderName != null && curHeaderName.equals(headerName))
    {
      block.add(orderDirFlag ? "v" : "^");
    }

    return block;
  }

  protected void notifyNewOrder(boolean newOrderOk)
  {
    // override in child class to trigger actions such 
    // as a reload of data after a new order is set
  }

  protected boolean autoAct()
  {
    String actionVal = getData(autoActionName);
    setData(autoActionName,"");
    if (autoActionName.equals(AN_SET_ORDER_HEADER))
    {
      boolean newOrderOk = setOrderHeader(actionVal);
      notifyNewOrder(newOrderOk);
      return newOrderOk;
    }
    return false;
  }

  protected boolean showFeedback()
  {
    return true;
  }
}
