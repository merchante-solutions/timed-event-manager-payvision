/**
 * ApiRequest
 *
 * Represents an incoming api request.
 *
 * Can handle multipart encoded requests.  To use this feature a 
 * MultiPartFileHandler must be registered via setMultiPartFileHandler().
 * If one is not then the default handling internal to this class is used.
 * Once a file handler is set the receive function should be called to
 * scan the multipart formatted request.  File handlers can also be
 * injected via constructor.
 *
 * The receive function should also be called for non-multipart requests
 * as it causes request parameters to be loaded.
 *
 * Another optional feature is parameter verification.  If a ParameterDefs
 * is registered, missing and invalid parameters will be detected during the
 * receive() process.
 *
 * The receive() method can be automatically triggered via certain
 * constructors, or manually triggered later or not used at all if
 * no multipart data is to be received and no parameter validation
 * is desired.
 */
package com.mes.mvc.mes;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.mvc.ApiException;
import com.mes.mvc.ApiMultipartFileHandler;
import com.mes.mvc.ApiRequest;
import com.mes.mvc.ParameterDef;
import com.mes.mvc.ParameterDefSet;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;

public class MesApiRequest implements ApiRequest
{
  static Logger log = Logger.getLogger(MesApiRequest.class);

  // max file upload size 50MB
  public static final int MAX_FILE_UPLOAD_SIZE = 100 * 1024 * 1024;

  // standard request parameters
  public static final String PARM_USER_ID   = "userId";
  public static final String PARM_USER_PASS = "userPass";
  public static final String PARM_MERCH_NUM = "merchId";
  public static final String PARM_PROFILE_ID = "profileId";

  protected HttpServletRequest request;

  private Map parmMap = new HashMap();

  private ApiMultipartFileHandler fileHandler;
  private ParameterDefSet defs;
  private List uploads = new ArrayList();

  /**
   * Constructor options: allow various combinations of parameter defs
   * and file handlers and the ability to disable auto receiving.
   */
  public MesApiRequest(HttpServletRequest request, ParameterDefSet defs, 
    ApiMultipartFileHandler fileHandler, boolean doReceive)
  {
    this.request = request;
    this.defs = defs;
    this.fileHandler = fileHandler;
    if (this.fileHandler == null)
    {
      this.fileHandler = this;
    }
    if (doReceive)
    {
      receive();
    }
  }
  public MesApiRequest(HttpServletRequest request, ParameterDefSet defs,
    boolean doReceive)
  {
    this(request,defs,null,doReceive);
  }
  public MesApiRequest(HttpServletRequest request, 
    ApiMultipartFileHandler fileHandler, boolean doReceive)
  {
    this(request,null,fileHandler,doReceive);
  }
  public MesApiRequest(HttpServletRequest request, ParameterDefSet defs)
  {
    this(request,defs,true);
  }
  public MesApiRequest(HttpServletRequest request, 
    ApiMultipartFileHandler fileHandler)
  {
    this(request,fileHandler,true);
  }
  public MesApiRequest(HttpServletRequest request, boolean doReceive)
  {
    this(request,null,null,doReceive);
  }
  public MesApiRequest(HttpServletRequest request)
  {
    this(request,false);
  }
  
  /**
   * Allows access to http servlet request wrapped by this class.
   */
  public HttpServletRequest getRequest()
  {
    return request;
  }

  /**
   * Sets a handler to receive multipart file parts.
   */
  public void setMultipartFileHandler(ApiMultipartFileHandler fileHandler)
  {
    this.fileHandler = fileHandler;
  }

  /**
   * Set parameter defs object.  Can also be passed in constructor.
   */
  public void setParameterDefSet(ParameterDefSet defs)
  {
    this.defs = defs;
  }

  /**
   * Sets request parameter value.
   */
  public void setParameter(String name, String value)
  {
    parmMap.put(name,value);
  }

  /**
   * Returns parameter value if it exists, or null.
   */
  public String getParameter(String name)
  {
    return (String)parmMap.get(name);
  }

  /**
   * Convert parameter data to long value.  Returns -1 if not present or
   * long parse fails.
   */
  public long getParameterAsLong(String name)
  {
    String valStr = getParameter(name);
    long val = -1L;
    if (valStr != null)
    {
      try { val = Long.parseLong(valStr); } catch (Exception e) { }
    }
    return val;
  }

  /**
   * Convert parameter data to int value.  Returns -1 if not present or
   * int parse fails.
   */
  public int getParameterAsInt(String name)
  {
    String valStr = getParameter(name);
    int val = -1;
    if (valStr != null)
    {
      try { val = Integer.parseInt(valStr); } catch (Exception e) { }
    }
    return val;
  }
  
  /**
   * Tries to parse a date object given the simple date format string and
   * a date string.  Returns null if parse fails.
   */
  private Date parseDate(String fmt, String dateStr)
  {
    Date date = null;
    
    try
    {
      SimpleDateFormat sdf = new SimpleDateFormat(fmt);
      sdf.setLenient(false);
      date = sdf.parse(dateStr);
    }
    catch (Exception e) { }
    
    return date;
  }

  /**
   * Convert parameter data to java.util.Date.  Returns null 
   * if not present or parse fails.
   */
  public Date getParameterAsDate(String name) 
  {
    String dateStr = getParameter(name);
    Date date = null;
    if (dateStr != null)
    {
      String[] fmts = new String[] { "MM/dd/yy", "M/d/yyyy" };
      for (int i = 0; date == null && i < fmts.length; ++i)
      {
        date = parseDate(fmts[i],dateStr);
      }
    }
    return date;
  }
  
  /**
   * Determine if parameter is present.
   */
  public boolean hasParameter(String name)
  {
    return getParameter(name) != null;
  }
  
  /**
   * Return the parameter map.
   */
  public Map getParameterMap()
  {
    return parmMap;
  }

  /**
   * Attempts to verify parameters present in the request.  Called
   * by receive().  If parameter def object is present it is used
   * to detect invalid parameters that are not defined in the defs,
   * and to find missing parameters that are defined as required.
   */
  private void verifyParameters()
  {
    if (defs == null)
    {
      log.warn("request parameter verification disabled");
      return;
    }

    // get parameter names present in request
    Collection parms = parmMap.keySet();

    // look for invalid parameters present in request
    for (Iterator i = parms.iterator(); i.hasNext();)
    {
      String name = ""+i.next();
      if (!defs.matchesRequestDef(name))
      {
        throw new MesApiInvalidParmException(name);
      }
    }

    // look for missing parameters in request
    Collection reqDefs = defs.getRequestDefs();
    for (Iterator i = reqDefs.iterator(); i.hasNext();)
    {
      ParameterDef def = (ParameterDef)i.next();
      if (def.isRequired())
      {
        boolean isPresent = false;
        for (Iterator i2 = parms.iterator(); !isPresent && i2.hasNext();)
        {
          String name = ""+i2.next();
          if (def.matches(name))
          {
            isPresent = true;
          }
        }
        if (!isPresent)
        {
          throw new MesApiMissingParmException(def.getName());
        }
      }
    }
  }
  
  /**
   * Determines if the request objects content type indicates multipart
   * form data is present.
   */
  public boolean isMultipartRequest()
  {
    try
    {
      String contentType = request.getContentType();
      return contentType != null && 
             contentType.startsWith("multipart/form-data");
    }
    catch (Exception e) { }
    return false;
  }

  /**
   * Processes incoming request data available in the HttpServletRequest 
   * object.  If request is multipart, received files will be handled
   * by the registered multi part file handler.  If none has been set
   * an exception is thrown.  Parameter data is placed in the parameter
   * map.  If parameter defs have been set then the parameters are
   * validated against them.
   */
  public void receive()
  {
    try
    {
      if (isMultipartRequest())
      {

        if (fileHandler == null)
        {
          log.error("No file handler registered for multipart request");
          throw new ApiException(MesApiResponse.RC_UPLOAD_ERROR);
        }

        MultipartParser parser = 
          new MultipartParser(request,MAX_FILE_UPLOAD_SIZE);
        com.oreilly.servlet.multipart.Part part = null;
        while ((part = parser.readNextPart()) != null)
        {
          String name = part.getName();
          if (part.isParam())
          {
            String value = ((ParamPart)part).getStringValue();
            setParameter(name,value);
          }
          else if (part.isFile())
          {
            fileHandler.handleFilePart((FilePart)part);
          }
          else
          {
            log.warn("Unhandled multi part " + name 
              + " with unknown part type: " + part);
          }
        }
      }
      else
      {
        // for non-multipart, load parameters to enable validation
        for (Enumeration e = request.getParameterNames(); e.hasMoreElements();)
        {
          String name = (String)e.nextElement();
          setParameter(name,request.getParameter(name));
        }
      }

      // check for invalid or missing parameters
      verifyParameters();
    }
    catch (ApiException ae)
    {
      throw ae;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      throw new MesApiSystemErrorException("Error: " + e);
    }
  }

  /***********************************************************************
   *
   * Mutlipart file handling
   *
   ***********************************************************************/

  /**
   * Used to represent uploaded files.
   */
  private class FileUpload
  {
    public String name;
    public byte[] data;
    
    public FileUpload(String name, byte[] data)
    {
      this.name = name;
      this.data = data;
    }

    public String toString()
    {
      int length = (data != null ? data.length : 0);
      return "MesApiAction.FileUpload ["
        + " name: " + name
        + ", size: " + length
        + (length > 100 ? 
           ", data: '" + new String(data,0,100) + "...'" : "")
        + (length <= 100 && length > 0 ? 
           ", data: '" + new String(data) + "'" : "")
        + " ]";
    }
  }

  /**
   * Add a file upload with the file name and data.
   */
  private void addUpload(String name, byte[] data)
  {
    FileUpload upload = new FileUpload(name,data);
    uploads.add(upload);
    log.debug("received file data:\n" + upload);
  }

  /**
   * True if one or more upload objects exists.
   */
  public boolean uploadReceived()
  {
    return uploadCount() > 0;
  }

  /**
   * How many uploads are present.
   */
  public int uploadCount()
  {
    return uploads.size();
  }

  /**
   * Retrieves a file upload by index from the upload list.  Starts at 0.
   */
  private FileUpload getFileUpload(int index)
  {
    if (index >= 0 && index < uploads.size())
    {
      return (FileUpload)uploads.get(index);
    }
    log.warn("Index " + index + " out of bounds on upload list");
    return null;
  }

  /**
   * Retrieves a file upload name by index. Starts at 0.
   */
  public String getUploadName(int index)
  {
    FileUpload upload = getFileUpload(index);
    if (upload != null)
    {
      return upload.name;
    }
    return null;
  }

  /** 
   * Retrieves uploaded file data by index.  Starts at 0.
   */
  public byte[] getUploadData(int index)
  {
    FileUpload upload = getFileUpload(index);
    if (upload != null)
    {
      return upload.data;
    }
    return null;
  }

  /**
   * During initialization (setHandler(...)), files may be uploaded
   * into memory from multipart form requests.  This captures the file
   * data in memory and makes it available to action handler methods.
   * Action handlers can also override this to create there own 
   * functionality...
   */  
  public void handleFilePart(FilePart filePart)
  {
    try
    {
      if (filePart.getFileName() == null)
      {
        throw new ApiException(MesApiResponse.RC_UPLOAD_ERROR,
          "Upload error, missing file name");
      }

      // store file data in memory as FileUpload object
      InputStream in = filePart.getInputStream();
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      byte[] inBuf = new byte[1024];
      int readCount = 0;
      int readTotal = 0;
      while ((readCount = in.read(inBuf)) != -1)
      {
        baos.write(inBuf,0,readCount);
        readTotal += readCount;
      }
      addUpload(filePart.getFileName(),baos.toByteArray());
    }
    catch (ApiException ae)
    {
      throw ae;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      throw new ApiException(MesApiResponse.RC_UPLOAD_ERROR);
    }
  }
}
