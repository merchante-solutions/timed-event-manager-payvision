package com.mes.mvc.mes;

import com.mes.mvc.Link;
import com.mes.mvc.MenuItem;
import com.mes.mvc.RequestHandler;
import com.mes.user.Permissions;

public class MesMenuItem extends MenuItem
{
  private MenuItem item;
  private Permissions permissions = new Permissions();

  public MesMenuItem(MenuItem item)
  {
    this.item = item;
  }

  public void setTitle(String title)
  {
    item.setTitle(title);
  }
  public String getTitle()
  {
    return item.getTitle();
  }

  public void setDisabledFlag(boolean disabledFlag)
  {
    item.setDisabledFlag(disabledFlag);
  }
  public boolean isDisabled()
  {
    return item.isDisabled();
  }

  public void setBackLinkFlag(boolean backLinkFlag)
  {
    item.setBackLinkFlag(backLinkFlag);
  }
  public boolean isBackLink()
  {
    return item.isBackLink();
  }

  public String renderLink(RequestHandler handler)
  {
    return item.renderLink(handler,
      handler.getProperty(Mes.PN_TARGET_WIN_ICON));
  }
  public String renderLink(RequestHandler handler, String targetIcon)
  {
    return item.renderLink(handler,targetIcon);
  }

  public Link getLink(RequestHandler handler)
  {
    return item.getLink(handler);
  }
  
  public void setPermissions(Permissions permissions)
  {
    this.permissions = permissions;
  }
  public Permissions getPermissions()
  {
    return permissions;
  }

  public String toString()
  {
    return this.getClass().getName() + " [ "
      + "internal item: " + item
      + ", permissions: " + permissions
      + " ]";
  }
}