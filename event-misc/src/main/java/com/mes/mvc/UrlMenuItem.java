package com.mes.mvc;

public class UrlMenuItem extends MenuItem
{
  private String url;
  private String targetWin;

  public UrlMenuItem(String title, String url, String targetWin)
  {
    super(title);
    this.url = url;
    this.targetWin = targetWin;
  }
  public UrlMenuItem(String title, String url)
  {
    this(title,url,null);
  }

  public String renderLink(RequestHandler handler, String targetIcon)
  {
    String href = " href='" + url + "'";
    String target = (targetWin == null ? "" : " target='" + targetWin + "'");
    return "<a" + href + target + ">" + title + 
      (target.length() > 0 && targetIcon != null 
        ? " <img border=0 src='" + targetIcon + "'>" : "") + "</a>";
  }
  public String renderLink(RequestHandler handler)
  {
    return renderLink(handler,null);
  }

  public Link getLink(RequestHandler handler)
  {
    throw new RuntimeException("Cannot generate Link for url menu items");
  }
}