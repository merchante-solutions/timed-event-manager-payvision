package com.mes.mvc;

import java.util.regex.Pattern;

public class ParameterDef
{
  public static final String T_REQUEST  = "request";
  public static final String T_RESPONSE = "response";

  private String  name;
  private String  parmType;
  private Pattern pat;
  private boolean requireFlag;

  // package accessible constructor
  ParameterDef()
  {
  }

  public ParameterDef(String name, String parmType, boolean requireFlag, 
    String match)
  {
    setName(name);
    setType(parmType);
    setRequireFlag(requireFlag);
    setMatch(match);
  }
  public ParameterDef(String name, String parmType, boolean requireFlag)
  {
    this(name,parmType,requireFlag,null);
  }
  public ParameterDef(String name, String parmType)
  {
    this(name,parmType,false);
  }

  // package accessible accessors, these allow config 
  // parser to instantiate and set members
  void setName(String name)
  {
    this.name = name;
  }
  void setType(String parmType)
  {
    if (!T_REQUEST.equals(parmType) && ! T_RESPONSE.equals(parmType))
    {
      throw new RuntimeException("Invalid parameter type '" + parmType + "'");
    }
    this.parmType = parmType;
  }
  void setRequireFlag(boolean requireFlag)
  {
    this.requireFlag = requireFlag;
  }
  void setMatch(String match)
  {
    pat = match != null ? Pattern.compile(match) : null;
  }

  // public accessors
  public String getName()
  {
    return name;
  }

  public boolean isRequest()
  {
    return T_REQUEST.equals(parmType);
  }

  public boolean isRequired()
  {
    return requireFlag;
  }

  public boolean matches(String str)
  {
    return pat != null ? pat.matcher(str).matches() : name.equals(str);
  }
  
  public String toString()
  {
    return "ParameterDef [ " + 
      "name: " + name +
      ", parmType: " + parmType +
      ", requireFlag: " + requireFlag +
      ", pattern: " + (pat != null ? pat.pattern() : "--") +
      " ]";
  }
}