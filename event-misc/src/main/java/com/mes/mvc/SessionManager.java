package com.mes.mvc;

import java.io.Serializable;
import javax.servlet.http.HttpSession;

public interface SessionManager
{
  public void setSession(HttpSession session);
  public void set(String name, Serializable item);
  public Object get(String name);
  public void remove(String name);
  public boolean isDirty();
  public void clean();
}