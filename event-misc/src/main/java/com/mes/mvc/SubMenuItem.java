package com.mes.mvc;

public class SubMenuItem extends MenuItem
{
  private String subName;

  public SubMenuItem(String title, String subName)
  {
    super(title);
    this.subName = subName;
  }

  public String renderLink(RequestHandler handler)
  {
    return getLink(handler).renderHtml();
  }

  public String renderLink(RequestHandler handler, String targetIcon)
  {
    // for now don't support target icon's on sub menu links
    return renderLink(handler);
  }

  public Link getLink(RequestHandler handler)
  {
    return handler.getLink(Mvc.ACTION_MENU,"menuName="+subName,title);
  }
}