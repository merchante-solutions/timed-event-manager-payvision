package com.mes.mvc;

public class ActionDef
{
  private String    setName;
  private String    name;
  private Class     actionClass;
  private boolean   ajaxFlag;
  private ParameterDefSet defs;

  public ActionDef(String setName, String name, Class actionClass, 
    boolean ajaxFlag, ParameterDefSet defs)
  {
    this.setName = setName;
    this.name = name;
    this.actionClass = actionClass;
    this.ajaxFlag = ajaxFlag;
    this.defs = defs;
  }
  public ActionDef(String setName, String name, Class actionClass, 
    boolean ajaxFlag)
  {
    this(setName,name,actionClass,ajaxFlag,null);
  }
  
  public String getSetName()
  {
    return setName;
  }

  public String getName()
  {
    return name;
  }

  public Class getActionClass()
  {
    return actionClass;
  }

  public boolean isAjax()
  {
    return ajaxFlag;
  }

  public ParameterDefSet getParameterDefSet()
  {
    return defs;
  }
}