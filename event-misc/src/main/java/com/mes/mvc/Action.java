package com.mes.mvc;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.mes.user.UserBean;

public interface Action
{
  public void setHandler(RequestHandler handler, String name);

  public RequestHandler getHandler();
  public HttpServletRequest getRequest();
  public HttpServletResponse getResponse();
  public HttpSession getSession();
  public String getName();
  public UserBean getUser();
  public Map getMenuMap();
  public ActionDef getActionDef();
  public boolean isAjax();

  // main entry point
  public void execute();

  // request parameter accessors
  public String getParm(String name, String defaultVal);
  public String getParm(String name);
  public long getParmLong(String name, long defaultVal);
  public long getParmLong(String name);

  // request attribute accessors
  public Object getRequestAttr(String name, Object defaultVal);
  public Object getRequestAttr(String name);
  public void setRequestAttr(String name, Object value);
  
  // session data accessors
  public Object getSessionAttr(String name, Object defaultVal);
  public Object getSessionAttr(String name);
  public void setSessionAttr(String name, Object val);
  public void removeSessionAttr(String name);

  // confirmation handling
  public boolean isCanceled();
  public boolean isConfirmed();
  public void confirm(String confirmMsg);

  // feeback handling
  public void addFeedback(String message);
}