package com.mes.mvc;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public abstract class XmlConfigParser
{
  static Logger log = Logger.getLogger(XmlConfigParser.class);

  private Map menuMap   = new HashMap();
  private Map viewMap   = new HashMap();
  private Map actionMap = new HashMap();

  private ParameterDefSet globalDefs;
  private ApiRcDefSet     rcDefs;

  /**
   * View loading
   */

  protected abstract String getViewName(Element viewEl);
  protected abstract View getView(Element viewEl);

  private void loadView(String viewName, Element viewEl)
  {
    viewMap.put(viewName,getView(viewEl));
  }

  private void loadView(Element viewEl)
  {
    viewMap.put(getViewName(viewEl),getView(viewEl));
  }
    
  private void loadViews(Element el)
  {
    // load all view elements 
    for (Iterator i = el.getChildren("view").iterator(); i.hasNext();)
    {
      loadView((Element)i.next());
    }
  }

  public Map getViewMap()
  {
    return viewMap;
  }

  private ParameterDef getParameterDef(Element parmEl)
  {
    String name = parmEl.getAttribute("name").getValue();
    String parmType = parmEl.getAttribute("param-type").getValue();
    boolean requireFlag = 
      parmEl.getAttribute("required").getValue().equals("true");
    Attribute patternAttr = parmEl.getAttribute("pattern");
    String pattern = patternAttr != null ? patternAttr.getValue() : null;
    return new ParameterDef(name,parmType,requireFlag,pattern);
  }

  private ParameterDefSet loadParmDefs(Element defsEl)
  {
    ParameterDefSet defs = new ParameterDefSet();
    for (Iterator i = defsEl.getChildren("parameter").iterator(); 
      i.hasNext();)
    {
      defs.addDef(getParameterDef((Element)i.next()));
    }
    return defs;
  }

  private ApiRcDef getRcDef(Element defEl)
  {
    String code = defEl.getAttribute("code").getValue();
    String msg = defEl.getAttribute("msg").getValue();
    return new ApiRcDef(code,msg);
  }

  private ApiRcDefSet loadRcDefs(Element defsEl)
  {
    ApiRcDefSet defs = new ApiRcDefSet();
    for (Iterator i = defsEl.getChildren("api-response").iterator(); 
      i.hasNext();)
    {
      defs.addDef(getRcDef((Element)i.next()));
    }
    return defs;
  }

  private void loadController(Element root, Properties properties)
  {
    Element ctrlEl = root.getChild("controller");

    // request-handler
    Element subEl = ctrlEl.getChild("request-handler");
    properties.setProperty(Mvc.PN_REQUEST_HANDLER,
                            subEl.getAttribute("class").getValue());

    // load optional web app specific elements
    Element wcEl = ctrlEl.getChild("web-config");
    if (wcEl != null)
    {
      // confirm-view
      subEl = wcEl.getChild("confirm-view");
      loadView("confirmView",subEl);

      // menu-view
      subEl = wcEl.getChild("menu-view");
      properties.setProperty(Mvc.PN_DEFAULT_MENU_NAME,
                              subEl.getAttribute("default-menu").getValue());
      loadView("menuView",subEl);
    }

    // properties
    for (Iterator i = ctrlEl.getChildren("property").iterator(); i.hasNext();)
    {
      subEl = (Element)i.next();
      properties.setProperty(subEl.getAttribute("name").getValue(),
                              subEl.getAttribute("value").getValue());
    }

    // views
    loadViews(ctrlEl);

    // global parameters (loaded into all actions that define parm def set)
    Element defsEl = ctrlEl.getChild("global-parameter-set");
    globalDefs = defsEl != null ? loadParmDefs(defsEl) : null;

    // api response codes and messages
    Element rcDefsEl = ctrlEl.getChild("api-responses");
    rcDefs = rcDefsEl != null ? loadRcDefs(rcDefsEl) : null;
  }

  private ActionDef getActionDef(Element actEl, String setName, 
    String defaultClassName)
  {
    String actionName = null;
    String className = null;
    try
    {
      actionName = actEl.getAttribute("name").getValue();
      Attribute classAttr = actEl.getAttribute("class");
      className = classAttr != null ? classAttr.getValue() : defaultClassName;
      Attribute ajaxAttr = actEl.getAttribute("ajax");
      boolean ajaxFlag = ajaxAttr != null && ajaxAttr.getValue().equals("true");
      Element defsEl = actEl.getChild("parameter-set");
      ParameterDefSet defs = defsEl != null ? loadParmDefs(defsEl) : null;
      if (defs != null && globalDefs != null)
      {
        defs.addDefs(globalDefs);
      }
      return 
        new ActionDef(
          setName,actionName,Class.forName(className),ajaxFlag,defs);
    }
    catch (Exception e)
    {
      log.error("Error creating action def for action named '" + actionName + "'");
      e.printStackTrace();
    }
    return null;
  }

  private void loadActions(Element setEl)
  {
    // load all action elements
    String setName = setEl.getAttribute("name").getValue();
    String defaultClass = setEl.getAttribute("default-class").getValue();
    for (Iterator i = setEl.getChildren("action").iterator(); i.hasNext();)
    {
      Element actEl = (Element)i.next();
      ActionDef actionDef = getActionDef(actEl,setName,defaultClass);
      actionMap.put(actionDef.getName(),actionDef);
    }
  }

  private void loadActionSets(Element root, Properties properties)
  {
    for (Iterator i = root.getChildren("action-set").iterator(); i.hasNext();)
    {
      Element setEl = (Element)i.next();
      loadActions(setEl);
      loadViews(setEl);
    }
  }

  public Map getActionMap()
  {
    return actionMap;
  }

  protected abstract String getPermissionsValue(Element root);

  private void loadPermissions(Element root, Properties properties)
  {
    properties.setProperty(Mvc.PN_PERMISSIONS,getPermissionsValue(root));
  }

  protected abstract MenuItem getMenuItemWrapper(MenuItem item);

  protected abstract void loadMenuItemPermissions(Element permEl, MenuItem item);

  private void loadMenus(Element root)
  {
    for (Iterator i = root.getChildren("menu").iterator(); i.hasNext();)
    {
      Element menuEl = (Element)i.next();
      String name = menuEl.getAttribute("name").getValue();
      String title = menuEl.getAttribute("title").getValue();
      Menu menu = new Menu(name,title);
      for (Iterator j = menuEl.getChildren("menu-item").iterator(); j.hasNext();)
      {
        Element itemEl = (Element)j.next();
        String itemTitle = itemEl.getChild("title").getText();
        Element linkEl = null;
        MenuItem item = null;
        if ((linkEl = itemEl.getChild("menu-link")) != null)
        {
          String subName = linkEl.getText();
          item = new SubMenuItem(itemTitle,subName);
        }
        else if ((linkEl = itemEl.getChild("action-link")) != null)
        {
          String actionName = linkEl.getText();
          item = new ActionMenuItem(itemTitle,actionName);
        }
        else if ((linkEl = itemEl.getChild("url-link")) != null)
        {
          String targetWin = null;
          if (itemEl.getAttribute("target-win") != null)
          {
            targetWin = itemEl.getAttribute("target-win").getValue();
          }
          String url = linkEl.getText();
          item = new UrlMenuItem(itemTitle,url,targetWin);
        }
        else
        {
          throw new RuntimeException("Menu " + name + " could not be processed,"
            + " unknown link type encountered");
        }
        item = getMenuItemWrapper(item);

        item.setDisabledFlag(
          itemEl.getAttribute("disabled").getValue().equals("true"));
        item.setBackLinkFlag(
          itemEl.getAttribute("is-back-link").getValue().equals("true"));

        Element permEl = itemEl.getChild("permissions");
        if (permEl != null)
        {
          loadMenuItemPermissions(permEl,item);
        }

        menu.addItem(item);
      }

      if (menuEl.getAttribute("auto-back-link").getValue().equals("true"))
      {
        menu.setAutoBackLinkFlag(true);
        menu.setBackItem(new AutoBackMenuItem());
      }

      menuMap.put(name,menu);
    }
  }

  public Map getMenuMap()
  {
    return menuMap;
  }

  public ApiRcDefSet getRcDefs()
  {
    return rcDefs;
  }

  private Properties loadProperties(Document doc)
  {
    Properties properties = null;

    try
    {
      Element root = doc.getRootElement();
      properties = new Properties();
      loadController(root,properties);
      loadActionSets(root,properties);
      loadPermissions(root,properties);
      loadMenus(root);
    }
    catch (Exception e)
    {
      log.error("Error in generateSchema(): " + e);
      e.printStackTrace();
    }

    return properties;
  }
  
  public Properties parseXmlFile(File xmlFile)
  {
    Properties properties = null;

    try
    {
      SAXBuilder builder = new SAXBuilder("org.apache.xerces.parsers.SAXParser",true);
      Document doc = builder.build(xmlFile);
      properties = loadProperties(doc);
      log.info("MVC config xml " + xmlFile + " is valid.");
    }
    catch (Exception e)
    {
      log.error("Error parsing config file" + xmlFile + ": " + e);
      e.printStackTrace();
    }

    return properties;
  }

  public Properties parseXmlUrl(URL xmlUrl)
  {
    Properties properties = null;

    try
    {
      SAXBuilder builder = new SAXBuilder("org.apache.xerces.parsers.SAXParser",true);
      Document doc = builder.build(xmlUrl);
      properties = loadProperties(doc);
      log.info("MVC config xml " + xmlUrl + " is valid.");
    }
    catch (Exception e)
    {
      log.error("Error parsing config file" + xmlUrl + ": " + e);
      e.printStackTrace();
    }

    return properties;
  }

  public Properties parseXmlStream(InputStream xmlStream)
  {
    Properties properties = null;

    try
    {
      SAXBuilder builder = new SAXBuilder("org.apache.xerces.parsers.SAXParser",true);
      Document doc = builder.build(xmlStream);
      properties = loadProperties(doc);
      log.info("MVC config xml is valid.");
    }
    catch (Exception e)
    {
      log.error("Error parsing config file: " + e);
      e.printStackTrace();
    }

    return properties;
  }

}