package com.mes.mvc;

public abstract class MenuItem
{
  protected String title;
  protected boolean disabledFlag;
  protected boolean backLinkFlag;

  public MenuItem()
  {
  }

  public MenuItem(String title)
  {
    this.title = title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }
  public String getTitle()
  {
    return title;
  }

  public void setDisabledFlag(boolean disabledFlag)
  {
    this.disabledFlag = disabledFlag;
  }
  public boolean isDisabled()
  {
    return disabledFlag;
  }

  public void setBackLinkFlag(boolean backLinkFlag)
  {
    this.backLinkFlag = backLinkFlag;
  }
  public boolean isBackLink()
  {
    return backLinkFlag;
  }

  public abstract String renderLink(RequestHandler handler, 
    String targetIcon);

  public abstract String renderLink(RequestHandler handler);

  public abstract Link getLink(RequestHandler handler);

  public String toString()
  {
    return this.getClass().getName() + " [ "
      + "title: " + title
      + ", disabled: " + disabledFlag
      + ", back link: " + backLinkFlag
      + " ]";
  }
}