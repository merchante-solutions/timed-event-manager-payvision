/**
 * Defines a "wrapper" custom tag for use in forms and fields.  This allows
 * a wrapper tag (such as a span) to be defined that encloses a form input
 * field.  The wrapper tag can be defined at the form level, which causes it
 * to be used on all fields in the form, or in a specific field, which causes
 * it to appear only around that field.
 *
 * The wrapper should contain one or more wrapper attribute tags that are 
 * added as tag attributes to the enclosing field wrapper tag, such as
 * "style",  "onmouseover", etc.
 *
 * Originally implemented to allow popup tooltips to be created around 
 * form fields by enclosing them in a span and invoking javascript functions 
 * via an onmouseover event.
 */
package com.mes.mvc.tags;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import com.mes.forms.Field;

public class ViewFieldWrapperTag extends ViewTagBase
{
  static Logger log = Logger.getLogger(ViewFieldWrapperTag.class);

  private ViewFormTag formTag;
  private ViewFieldTag fieldTag;
  private String tagName;
  private String errorOnly = "true";
  private List attributes = new ArrayList();

  public void setTagName(String tagName)
  {
    this.tagName = tagName;
  }
  public String getTagName()
  {
    return tagName;
  }

  public void setErrorOnly(String errorOnly)
  {
    this.errorOnly = errorOnly.toLowerCase();
    if (!this.errorOnly.equals("true") && !this.errorOnly.equals("false"))
    {
      throw new RuntimeException("Invalid value, errorOnly must be 'true'"
        + " or 'false'");
    }
  }
  public String getErrorOnly()
  {
    return errorOnly;
  }

  public boolean isErrorOnly()
  {
    return errorOnly.equals("true");
  }

  public ViewFieldTag getFieldTag()
  {
    return fieldTag;
  }

  public ViewFormTag getFormTag()
  {
    return formTag;
  }

  public boolean isFieldLevel()
  {
    return fieldTag != null;
  }

  public class Attr
  {
    String name;
    String data;

    public Attr(String name, String data)
    {
      this.name = name;
      this.data = data;
    }

    private String renderData(Field field, String data)
    {
      StringBuffer errorText = new StringBuffer(field.getErrorText());
      for (int i = 0; i < errorText.length(); ++i)
      {
        if (errorText.charAt(i) == '$')
        {
          errorText.insert(i++,"\\");
        }
      }
      Pattern pattern = Pattern.compile("@field.errorText");
      Matcher matcher = pattern.matcher(data);
      data = matcher.replaceAll(""+errorText);
      return data;
    }

    public String render(Field field)
    {
      return name + "='" + renderData(field,data) + "'";
    }

  }

  public void addAttribute(ViewFieldWrapperAttributeTag attribute)
  {
    attributes.add(new Attr(attribute.getName(),attribute.getData()));
  }

  public int doStartTag()
  {
    try
    {
      fieldTag 
        = (ViewFieldTag)findAncestorWithClass(this,ViewFieldTag.class);
      if (fieldTag != null)
      {
        fieldTag.addWrapper(this);
      }
      else
      {
        formTag 
          = (ViewFormTag)findAncestorWithClass(this,ViewFormTag.class);
        formTag.addWrapper(this);
      }
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
      return SKIP_BODY;
    }
    
    return EVAL_BODY_INCLUDE;
  }

  public String doWrapperStart(Field field)
  {
    StringBuffer buf = new StringBuffer("<" + tagName);
    for (Iterator i = attributes.iterator(); i.hasNext();)
    {
      Attr attr = (Attr)i.next();
      buf.append(" " + attr.render(field));
    }
    buf.append(">");
    return buf.toString();
  }

  public String doWrapperEnd()
  {
    return "</" + tagName + ">";
  }
}
