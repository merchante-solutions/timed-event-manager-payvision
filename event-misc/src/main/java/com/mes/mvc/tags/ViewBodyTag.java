package com.mes.mvc.tags;

import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;
import org.apache.log4j.Logger;
import com.mes.mvc.RequestHandler;

public class ViewBodyTag extends BodyTagSupport
{
  static Logger log = Logger.getLogger(ViewBodyTag.class);

  private String idAttr = null;

  public void setId(String idAttr)
  {
    this.idAttr = idAttr;
  }
  public String getId()
  {
    return idAttr;
  }

  public int doStartTag()
  {
    pageContext.setAttribute("user",getHandler().getUser(),
      PageContext.REQUEST_SCOPE);
    return EVAL_BODY_INCLUDE;
  }

  protected RequestHandler getHandler()
  {
    RequestHandler handler = (RequestHandler)pageContext
      .getAttribute("handler",PageContext.REQUEST_SCOPE);
    if (handler == null)
    {
      throw new NullPointerException("Request handler not found in request");
    }
    return handler;
  }
}
