package com.mes.mvc.tags;

import java.lang.reflect.Method;
import java.util.List;
import java.util.ListIterator;
import org.apache.log4j.Logger;

public class ViewTableListTag extends ViewTableRowBaseTag
{
  static Logger log = Logger.getLogger(ViewTableListTag.class);

  private String varId;
  private String varType;
  private String accName;
  private String shaderCssClass;
  private String order;
  private String leading;
  private String trailing;

  private ListIterator rowIter;
  private int rowCount;
  private int rowNum;
  private int leadCount;
  private int trailCount;
  private boolean reverseFlag;
  private boolean hiddenRowFlag;
  private boolean rowStarted;
  private boolean isShaded;
  private boolean isExpanded = true;

  public void setId(String id)
  {
    varId = id;
  }
  public String getId()
  {
    return varId;
  }

  public void setType(String type)
  {
    varType = type;
  }
  public String getType()
  {
    return varType;
  }

  public void setAccName(String accName)
  {
    this.accName = accName;
  }
  public String getAccName()
  {
    return accName;
  }

  public void setShaderCssClass(String shaderCssClass)
  {
    this.shaderCssClass = shaderCssClass;
  }
  public String getShaderCssClass()
  {
    return shaderCssClass;
  }

  public void setOrder(String order)
  {
    if (!order.equals("reverse"))
    {
      throw new RuntimeException("Invalid order attribute: '" + order + "'");
    }
    reverseFlag = true;
  }
  public String getOrder()
  {
    return reverseFlag ? "reverse" : "";
  }

  public void setLeading(String leading)
  {
    try
    {
      leadCount = Integer.parseInt(leading);
    }
    catch (Exception e)
    {
      throw new RuntimeException("Invalid leading attribute: '" + leading + "'");
    }
  }
  public String getLeading()
  {
    return String.valueOf(leadCount);
  }

  public void setTrailing(String trailing)
  {
    try
    {
      trailCount = Integer.parseInt(trailing);
    }
    catch (Exception e)
    {
      throw new RuntimeException("Invalid trailing attribute: '" + trailing + "'");
    }
  }
  public String getTrailing()
  {
    return String.valueOf(trailCount);
  }

  public boolean inHiddenRow()
  {
    return hiddenRowFlag;
  }

  private String getShaderClass()
  {
    String addClass = isShaded ? shaderCssClass : null;
    isShaded = !isShaded;
    return addClass;
  }

  private void startNewRow() throws Exception
  {
    pageContext.getOut().print("<tr" 
      + renderCssAttributes(null,getShaderClass()) + ">");
    rowStarted = true;
  }

  private void startHidingRows() throws Exception
  {
    pageContext.getOut().print("<tr><td " + renderCssAttributes(
      "line-height: 1.5em; text-align: center; font-size: 80%",null)
      + " colspan=\"" + colCount + "\">(expand to view more)</td></tr>\n\n");
  }

  private void processNextRow() throws Exception
  {
    // fetch next/previous row item based on reverse flag
    pageContext.setAttribute(varId,
      reverseFlag ? rowIter.previous() : rowIter.next());

    // increment/decrement rowNum based on reverse flag
    rowNum += reverseFlag ? -1 : 1;
    pageContext.setAttribute("rownum",""+rowNum);
    pageContext.setAttribute("rowidx",""+(rowNum - 1));

    // determine if rowNum is to be shown based on 
    // leading and trailing and isExpanded flag
    if (isExpanded || rowNum < leadCount + 1 || rowNum > rowCount - trailCount)
    {
      // visible row, start it
      startNewRow();
      hiddenRowFlag = false;
      rowStarted = true;
    }
    else 
    {
      // first hidden row, start hiding rows
      if (!hiddenRowFlag)
      {
        startHidingRows();
        hiddenRowFlag = true;
      }
      rowStarted = false;
    }

    resetColCount();
  }

  private void finishRow() throws Exception
  {
    pageContext.getOut().print("</tr>\n\n");
  }

  private List getTableRowList()
  {
    // new special case, row-sets may now use an accessor method name
    // to access lists in an id object within the table tag
    if (accName != null && tableTag.getId() != null)
    {
      try
      {
        Object idObj = tableTag.getIdObj();
        Class idClass = Class.forName(tableTag.getType());
        Method method = idClass.getDeclaredMethod(accName,null);
        return (List)method.invoke(idObj,null);
      }
      catch (Exception e)
      {
        throw new RuntimeException(e);
      }
    }

    // no acc/id obj combo available so do regular row list generation
    return tableTag.getRowList();
  }

  /**
   * Initialize iteration over rows list.  If reverse flag is turned on needs
   * to start at end of list rather than beginning.  The rowCount is initialized
   * and rowNum is set to first or last element based on the reverse flag.\
   */
  private void initIteration()
  {
    // set some internals based on table tag's viewable object 
    // if not overridden by local attributes
    Viewable bean = tableTag.getViewable();
    if (bean != null)
    {
      isExpanded = bean.isExpanded();
      if (leadCount == 0 && bean.getLeadCount() > 0)
      {
        leadCount = bean.getLeadCount();
      }
      if (trailCount == 0 && bean.getTrailCount() > 0)
      {
        trailCount = bean.getTrailCount();
      }
      if (!reverseFlag && bean.isReversed())
      {
        reverseFlag = true;
      }
    }

    List rows = getTableRowList();

    // start at end of list when reversing
    if (reverseFlag)
    {
      rowIter = rows.listIterator(rows.size());
      rowNum = rows.size() + 1;
    }
    // else start at beginning of list
    else
    {
      rowIter = rows.listIterator();
      rowNum = 0;
    }
    rowCount = rows.size();

    // reset flags
    hiddenRowFlag = false;
    isShaded = false;
  }

  private boolean hasMoreRows()
  {
    if (reverseFlag)
    {
      return rowIter.hasPrevious();
    }
    return rowIter.hasNext();
  }

  public int doStartTag()
  {
    try
    {
      initIteration();
      if (hasMoreRows())
      {
        processNextRow();
        return EVAL_BODY_INCLUDE;
      }
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }

    return SKIP_BODY;
  }

  public int doAfterBody()
  {
    try
    {
      if (rowStarted)
      {
        finishRow();
      }
      if (hasMoreRows())
      {
        processNextRow();
        return EVAL_BODY_AGAIN;
      }
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return EVAL_PAGE;
  }
}
