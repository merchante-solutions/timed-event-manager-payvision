package com.mes.mvc.tags;

import javax.servlet.jsp.tagext.BodyTagSupport;
import org.apache.log4j.Logger;

public class AjaxParameterTag extends BodyTagSupport
{
  static Logger log = Logger.getLogger(AjaxParameterTag.class);

  private String name;
  private String value;
  private String rtExpr = "false";

  public void setName(String name)
  {
    this.name = name;
  }
  public String getName()
  {
    return name;
  }

  public void setValue(String value)
  {
    this.value = value;
  }
  public String getValue()
  {
    return value;
  }

  public void setRtExpr(String rtExpr)
  {
    this.rtExpr = rtExpr;
  }
  public String getRtExpr()
  {
    return rtExpr;
  }

  public int doStartTag()
  {
    try
    {
      AjaxParameterContainer container = 
        (AjaxParameterContainer)findAncestorWithClass(this,
                                                      AjaxParameterContainer.class);
      container.addAjaxParm(
        new AjaxParameter(name,value,rtExpr.toLowerCase().equals("true")));
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return SKIP_BODY;
  }
}
