package com.mes.mvc.tags;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.VariableInfo;
import org.apache.log4j.Logger;

public class ViewFormTEI extends TagExtraInfo
{
  static Logger log = Logger.getLogger(ViewFormTEI.class);
  
  public VariableInfo[] getVariableInfo(TagData tagData)
  {
    VariableInfo[] vars = new VariableInfo[1];
    String varName = (String)tagData.getAttribute("varName");
    varName = varName == null ? "bean" : varName;
    vars[0] = new VariableInfo(varName,(String)tagData.getAttribute("type"),true,VariableInfo.AT_BEGIN);
    return vars;
  }
}
