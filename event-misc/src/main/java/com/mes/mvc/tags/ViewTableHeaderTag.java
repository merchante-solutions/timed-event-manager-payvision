package com.mes.mvc.tags;

import javax.servlet.jsp.JspWriter;
import org.apache.log4j.Logger;

public class ViewTableHeaderTag extends ViewTableRowBaseTag
{
  static Logger log = Logger.getLogger(ViewTableHeaderTag.class);

  private boolean endTagNeeded;

  public int doStartTag()
  {
    try
    {
      resetColCount();
      endTagNeeded = false;
      if (!tableTag.hasList() || !tableTag.getRowList().isEmpty())
      {
        JspWriter out = pageContext.getOut();
        pageContext.getOut().print("<tr" + renderCssAttributes() + ">");
        endTagNeeded = true;
        return EVAL_BODY_INCLUDE;
      }
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return SKIP_BODY;
  }

  public int doEndTag()
  {
    try
    {
      if (endTagNeeded)
      {
        pageContext.getOut().print("</tr>");
      }
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return EVAL_PAGE;
  }
}
