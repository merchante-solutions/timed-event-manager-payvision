package com.mes.mvc.tags;

public class AjaxParameter
{
  private String name;
  private String value;
  private boolean rtExprFlag;

  public AjaxParameter(String name, String value, boolean rtExprFlag)
  {
    this.name = name;
    this.value = value;
    this.rtExprFlag = rtExprFlag;
  }

  public String getName()
  {
    return name;
  }

  public String getValue()
  {
    return value;
  }

  public boolean isRtExpr()
  {
    return rtExprFlag;
  }

  public String toString()
  {
    return "AjaxParameter [ "
      + "name: " + name 
      + ", value: " + value 
      + ", rtexpr: " + rtExprFlag 
      + " ]";
  }
}
