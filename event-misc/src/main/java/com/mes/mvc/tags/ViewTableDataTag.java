package com.mes.mvc.tags;

import org.apache.log4j.Logger;

public class ViewTableDataTag extends ViewTableRowBaseTag
{
  static Logger log = Logger.getLogger(ViewTableDataTag.class);

  public int doStartTag()
  {
    try
    {
      resetColCount();
      pageContext.getOut().print("<tr" + renderCssAttributes() + ">");
      return EVAL_BODY_INCLUDE;
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }

    return SKIP_BODY;
  }

  public int doAfterBody()
  {
    try
    {
      pageContext.getOut().print("</tr>");
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return EVAL_PAGE;
  }
}
