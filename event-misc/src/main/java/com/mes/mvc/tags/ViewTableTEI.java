package com.mes.mvc.tags;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.VariableInfo;
import org.apache.log4j.Logger;

public class ViewTableTEI extends TagExtraInfo
{
  static Logger log = Logger.getLogger(ViewTableTEI.class);
  
  public VariableInfo[] getVariableInfo(TagData tagData)
  {
    if (tagData.getAttribute("bean") != null)
    {
      VariableInfo[] vars = new VariableInfo[1];
      vars[0] = new VariableInfo(ViewTableTag.VIEWABLE_BEAN_NAME,
        Viewable.class.getName(),true,VariableInfo.NESTED);
      return vars;
    }
    else if (tagData.getAttribute("id") != null)
    {
      VariableInfo[] vars = new VariableInfo[1];
      vars[0] = new VariableInfo((String)tagData.getAttribute("id"),
        (String)tagData.getAttribute("type"),true,VariableInfo.NESTED);
      return vars;
    }
    return new VariableInfo[0];
  }
}
