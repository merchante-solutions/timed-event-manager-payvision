package com.mes.mvc.tags;

import javax.servlet.jsp.JspWriter;
import org.apache.log4j.Logger;

public class ViewBoxTag extends ViewTagBase
{
  static Logger log = Logger.getLogger(ViewBoxTag.class);

  private String typeAttr = null;
  private String frame = null;

  public void setType(String typeAttr)
  {
    if (!typeAttr.equals("table") && !typeAttr.equals("div"))
    {
      throw new RuntimeException("Box type must by 'table' or 'div'");
    }
    this.typeAttr = typeAttr;
  }
  public String getType()
  {
    return typeAttr;
  }

  public void setFrame(String frame)
  {
    this.frame = frame;
  }
  public String getFrame()
  {
    return frame;
  }

  private void doStartFraming() throws Exception
  {
    JspWriter out = pageContext.getOut();
    out.print("<b class='" + frame + "-hdr'><b class='" + frame 
      + "-s1'></b><b\n");
    out.print(" class='" + frame + "-s2'></b><b class='" + frame 
      + "-s3'></b><b class='" + frame + "-s4'></b></b><div\n");
    out.print(" class='" + frame + "-body'>");
  }

  private void doEndFraming() throws Exception
  {
    JspWriter out = pageContext.getOut();
    out.print("</div><b class='" + frame + "-ftr'><b class='" + frame 
      + "-s4'></b><b class='" + frame + "-s3'></b><b\n");
    out.print(" class='" + frame + "-s2'></b><b class='" + frame 
      + "-s1'></b></b>");
  }

  public int doStartTag()
  {
    try
    {
      JspWriter out = pageContext.getOut();
      if (typeAttr.equals("table"))
      {
        out.print("<table" + renderCssAttributes(null,frame) + "><tr><td>");
      }
      else
      {
        out.print("<div" + renderCssAttributes(null,frame) + ">");
      }
      doStartFraming();
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
      return SKIP_BODY;
    }
    
    return EVAL_BODY_INCLUDE;
  }

  public int doEndTag()
  {
    try
    {
      doEndFraming();
      JspWriter out = pageContext.getOut();
      if (typeAttr.equals("table"))
      {
        out.print("</td></tr></table>\n\n");
      }
      else
      {
        out.print("</div>\n\n");
      }
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return EVAL_PAGE;
  }
}
