package com.mes.mvc.tags;

import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;
import com.mes.mvc.RequestHandler;

public class ViewTagBase extends BodyTagSupport
{
  protected ViewBodyTag     bodyTag;
  protected RequestHandler  handler;

  protected String          cssClass;
  protected String          style;
  protected String          tagId;
  protected String          onChange;
  protected String          onClick;
  protected String          onSubmit;

  public void setCssClass(String cssClass)
  {
    this.cssClass = cssClass;
  }
  public String getCssClass()
  {
    return cssClass;
  }

  public void setStyle(String style)
  {
    this.style = style;
  }
  public String getStyle()
  {
    return style;
  }

  public void setTagId(String tagId)
  {
    this.tagId = tagId;
  }
  public String getTagId()
  {
    return tagId;
  }

  public void setOnChange(String onChange)
  {
    this.onChange = onChange;
  }
  public String getOnChange()
  {
    return onChange;
  }

  public void setOnClick(String onClick)
  {
    this.onClick = onClick;
  }
  public String getOnClick()
  {
    return onClick;
  }

  public void setOnSubmit(String onSubmit)
  {
    this.onSubmit = onSubmit;
  }
  public String getOnSubmit()
  {
    return onSubmit;
  }

  protected RequestHandler getHandler()
  {
    if (handler == null)
    {
      if (bodyTag != null)
      {
        handler = bodyTag.getHandler();
      }
      else
      {
        handler = (RequestHandler)pageContext
          .getAttribute("handler",PageContext.REQUEST_SCOPE);
      }
    }
    if (handler == null)
    {
      throw new NullPointerException("Handler not found");
    }
    return handler;
  }

  /**
   * Uses the setParent method as a hook for doing some initialization chores...
   */
  public void setParent(Tag parentTag)
  {
    super.setParent(parentTag);
    bodyTag = (ViewBodyTag)findAncestorWithClass(this,ViewBodyTag.class);
  }

  protected String renderCssAttributes(String addStyles, String addClasses)
  {
    StringBuffer cssBuf = new StringBuffer();

    // generate tag id attribute
    if (tagId != null)
    {
      cssBuf.append(" id=\"" + tagId + "\"");
    }

    // generate class attribute
    StringBuffer classBuf = new StringBuffer();
    if (cssClass != null)
    {
      classBuf.append(cssClass);
    }
    if (addClasses != null)
    {
      if (classBuf.length() > 0)
      {
        classBuf.append(" ");
      }
      classBuf.append(addClasses);
    }
    if (classBuf.length() > 0)
    {
      cssBuf.append(" class=\"" + classBuf + "\"");
    }

    // generate style attribute
    StringBuffer styleBuf = new StringBuffer();
    if (style != null) 
    {
      styleBuf.append(style);
    }
    if (addStyles != null)
    {
      if (styleBuf.length() > 0)
      {
        styleBuf.append("; ");
      }
      styleBuf.append(addStyles);
    }
    if (styleBuf.length() > 0)
    {
      cssBuf.append(" style=\"" + styleBuf + "\"");
    }

    // supported events
    if (onChange != null)
    {
      cssBuf.append(" onChange=\"" + onChange + "\"");
    }
    if (onClick != null)
    {
      cssBuf.append(" onClick=\"" + onClick + "\"");
    }

    return cssBuf.toString();
  }
  protected String renderCssAttributes(String addStyles)
  {
    return renderCssAttributes(addStyles,null);
  }
  protected String renderCssAttributes()
  {
    return renderCssAttributes(null);
  }
}