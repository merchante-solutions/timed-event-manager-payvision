package com.mes.mvc.tags;

public interface AjaxParameterContainer
{
  public void addAjaxParm(AjaxParameter ajaxParm);
  public AjaxParameter getAjaxParm(String name);
}