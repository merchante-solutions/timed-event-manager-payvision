package com.mes.mvc.tags;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;
import org.apache.log4j.Logger;
import com.mes.mvc.RequestHandler;

public class AjaxUpdateTag extends BodyTagSupport implements AjaxParameterContainer
{
  static Logger log = Logger.getLogger(AjaxUpdateTag.class);

  private String  target;
  private String  url;
  private String  method;
  private String  function;
  private String  insertion;
  private String  formId;
  private List    parameters = new ArrayList();

  public void setTarget(String target)
  {
    this.target = target;
  }
  public String getTarget()
  {
    return target;
  }

  public void setUrl(String url)
  {
    this.url = url;
  }
  public String getUrl()
  {
    return url;
  }

  public void addAjaxParm(AjaxParameter ajaxParm)
  {
    parameters.add(ajaxParm);
  }
  public AjaxParameter getAjaxParm(String name)
  {
    for (Iterator i = parameters.iterator(); i.hasNext();)
    {
      AjaxParameter parm = (AjaxParameter)i.next();
      if (parm.getName().equals(name)) return parm;
    }
    return null;
  }

  public void setAction(String action)
  {
    addAjaxParm(new AjaxParameter("action",action,false));
  }
  public String getAction()
  {
    return getAjaxParm("action").getValue();
  }

  public void setMethod(String method)
  {
    this.method = method;
  }
  public String getMethod()
  {
    return method;
  }

  public void setFunction(String function)
  {
    this.function = function;
  }
  public String getFunction()
  {
    return function;
  }

  public void setInsertion(String insertion)
  {
    this.insertion = insertion;
  }
  public String getInsertion()
  {
    return insertion;
  }

  public void setFormId(String formId)
  {
    this.formId = formId;
  }
  public String getFormId()
  {
    return formId;
  }

  public int doStartTag()
  {
    return EVAL_BODY_INCLUDE;
  }

  private String getFunctionExpression()
  {
    StringBuffer funcBuf = new StringBuffer();
    funcBuf.append("  ");
    String[] parts = function.split("\\(");
    if (parts.length > 1)
    {
      funcBuf.append(parts[0]);
      funcBuf.append(" = function(" + parts[1]);
    }
    else
    {
      funcBuf.append(function + " = function()");
    }
    return funcBuf.toString();
  }

  private String getInsertionClass()
  {
    StringBuffer buf = new StringBuffer("insertion:    ");
    if (insertion.equals("top"))
    {
      buf.append("Insertion.Top");
    }
    else if (insertion.equals("bottom"))
    {
      buf.append("Insertion.Bottom");
    }
    else if (insertion.equals("before"))
    {
      buf.append("Insertion.Before");
    }
    else if (insertion.equals("after"))
    {
      buf.append("Insertion.After");
    }
    else
    {
      buf.append("null");
    }
    return buf.toString();
  }

  private String getParmData()
  {
    StringBuffer parmBuf = new StringBuffer();
    if (parameters != null)
    {
      parmBuf.append("'");
      for (Iterator i = parameters.iterator(); i.hasNext();)
      {
        AjaxParameter parm = (AjaxParameter)i.next();
        parmBuf.append(parm.getName() + "=");
        if (parm.isRtExpr())
        {
          parmBuf.append("' + " + parm.getValue());
        }
        else
        {
          parmBuf.append(parm.getValue());
        }
        if (i.hasNext())
        {
          if (parm.isRtExpr())
          {
            parmBuf.append(" + '");
          }
          parmBuf.append("&");
        }
        else if (!parm.isRtExpr())
        {
          parmBuf.append("'");
        }
      }
    }
    if (formId != null)
    {
      if (parmBuf.length() > 0)
      {
        parmBuf.append(" + '&' + ");
      }
      parmBuf.append("Form.serialize('" + formId + "')");
    }
    return parmBuf.length() > 0 ? parmBuf.toString() : null;
  }

  public int doEndTag()
  {
    try
    {
      // set the back link for the specified action as though we are
      // generating a link to it (for use by ajax request time link
      // generation as back link)
      RequestHandler handler = (RequestHandler)pageContext
        .getAttribute("handler",PageContext.REQUEST_SCOPE);
      handler.autoSetBackLinkFor(getAjaxParm("action").getValue());
      StringBuffer tagBuf = new StringBuffer();

      // if function given, generate script start tag and function declaration
      if (function != null)
      {
        tagBuf.append("\n<script>\n");
        tagBuf.append(getFunctionExpression());
        tagBuf.append("\n  {");
      }

      // create prototype updater call:
      //
      //  new Ajax.Updater('message',
      //                   '/srv/dst',  
      //                   { 
      //                     method:      'get',  
      //                     parameters:  'action=testAjax',
      //                     evalScripts: true
      //                   }) 

      tagBuf.append("\n    new Ajax.Updater('" + target + "',\n");
      tagBuf.append("                '" 
        + (url != null ? url : handler.getServletUrl()) + "',\n");
      tagBuf.append("                { \n");
      if (method != null)
      {
        tagBuf.append("                   method:       '" + method + "',\n");
      }
      String parms = getParmData();
      if (parms != null)
      {
        tagBuf.append("                   parameters:   " + parms + ",\n");
      }
      if (insertion != null)
      {
        tagBuf.append("                   " + getInsertionClass() + ",\n");
      }
      tagBuf.append("                   evalScripts:  true\n");
      tagBuf.append("                })\n");

      // closing function brace and script tag if needed
      if (function != null)
      {
        tagBuf.append("\n  }\n</script>\n");
      }

      pageContext.getOut().print(tagBuf.toString());
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return EVAL_PAGE;
  }
}
