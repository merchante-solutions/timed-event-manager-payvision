package com.mes.mvc.tags;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.VariableInfo;
import org.apache.log4j.Logger;

public class ViewTableListTEI extends TagExtraInfo
{
  static Logger log = Logger.getLogger(ViewTableListTEI.class);
  
  public VariableInfo[] getVariableInfo(TagData tagData)
  {
    VariableInfo[] vars = new VariableInfo[3];
    vars[0] = new VariableInfo((String)tagData.getAttribute("id"),
      (String)tagData.getAttribute("type"),true,VariableInfo.AT_BEGIN);
    vars[1] = new VariableInfo("rownum",
      "java.lang.String",true,VariableInfo.AT_BEGIN);
    vars[2] = new VariableInfo("rowidx",
      "java.lang.String",true,VariableInfo.AT_BEGIN);
    return vars;
  }
}
