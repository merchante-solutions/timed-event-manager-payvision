package com.mes.mvc.tags;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.VariableInfo;
import org.apache.log4j.Logger;

public class ViewTableCellTEI extends TagExtraInfo
{
  static Logger log = Logger.getLogger(ViewTableCellTEI.class);
  
  public VariableInfo[] getVariableInfo(TagData tagData)
  {
    VariableInfo[] vars = new VariableInfo[2];
    vars[0] = new VariableInfo("colnum","java.lang.String",true,
      VariableInfo.AT_BEGIN);
    vars[1] = new VariableInfo("coltotal","java.lang.String",true,
      VariableInfo.AT_BEGIN);
    return vars;
  }
}
