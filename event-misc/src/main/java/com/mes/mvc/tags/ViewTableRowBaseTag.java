package com.mes.mvc.tags;

import javax.servlet.jsp.tagext.Tag;
import org.apache.log4j.Logger;

public class ViewTableRowBaseTag extends ViewTagBase
{
  static Logger log = Logger.getLogger(ViewTableRowBaseTag.class);

  protected ViewTableTag  tableTag;
  protected int           colCount;
  
  public void incrementColCount(int incAmount)
  {
    colCount += incAmount;
    tableTag.updateColTotal(colCount);
  }
  public int getColCount()
  {
    return colCount;
  }
  public void resetColCount()
  {
    colCount = 0;
  }

  /**
   * Uses the setParent method as a hook for doing some initialization chores...
   */
  public void setParent(Tag parentTag)
  {
    super.setParent(parentTag);
    tableTag = (ViewTableTag)findAncestorWithClass(this,ViewTableTag.class);
  }
}
