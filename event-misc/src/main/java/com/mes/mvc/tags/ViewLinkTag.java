package com.mes.mvc.tags;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.jsp.PageContext;
import org.apache.log4j.Logger;
import com.mes.forms.HtmlLink;
import com.mes.mvc.Link;
import com.mes.mvc.RequestHandler;

public class ViewLinkTag extends ViewTagBase
{
  static Logger log = Logger.getLogger(ViewLinkTag.class);

  private String  action;
  private String  view;
  private String  text;
  private String  prefix;
  private String  target;
  private List    args;
  private boolean showFlag = true;

  public void setAction(String action)
  {
    this.action = action;
  }
  public String getAction()
  {
    return action;
  }

  public void setView(String view)
  {
    this.view = view;
  }
  public String getView()
  {
    return view;
  }

  public void setText(String text)
  {
    this.text = text;
  }
  public String getText()
  {
    return text;
  }

  public void setPrefix(String prefix)
  {
    this.prefix = prefix;
  }
  public String getPrefix()
  {
    return prefix;
  }

  public void setCondition(String condition)
  {
    showFlag = condition.toLowerCase().equals("true");
  }
  public String getCondition()
  {
    return ""+showFlag;
  }

  public void setTarget(String target)
  {
    this.target = target;
  }
  public String getTarget()
  {
    return target;
  }

  public class NameValuePair
  {
    public String name;
    public String value;
    public NameValuePair(String name, String value)
    {
      this.name = name;
      this.value = value;
    }
  }

  public void addArg(String name, String value)
  {
    args.add(new NameValuePair(name,value));
  }

  public int doStartTag()
  {
    args = new ArrayList();
    return showFlag ? EVAL_BODY_INCLUDE : SKIP_BODY;
  }

  private void generateLink(HtmlLink hl) throws Exception
  {
    if (cssClass != null) hl.setProperty("class",cssClass);
    if (style != null) hl.setProperty("style",style);
    if (onClick != null) hl.setProperty("onClick",onClick + "; return false");
    if (target != null) hl.setProperty("target",target);
    pageContext.getOut().print(hl.renderHtml());
  }

  private void doGenericLink() throws Exception
  {
    generateLink(new HtmlLink("#",text));
  }

  private void doActionLink() throws Exception
  {
    RequestHandler handler = (RequestHandler)pageContext
      .getAttribute("handler",PageContext.REQUEST_SCOPE);

    Link link = handler.getLink(action);
    if (view != null)
    {
      link.setView(handler.getView(view));
    }
    if (text != null)
    {
      link.setText(text);
    }
    if (prefix != null)
    {
      link.setPrefix(prefix);
    }
    for (Iterator i = args.iterator(); i.hasNext();)
    {
      NameValuePair nv = (NameValuePair)i.next();
      link.addArg(nv.name,nv.value);
    }

    // manually set back link since getLink() method used doesn't automatically
    handler.autoSetBackLinkFor(action);

    generateLink(link.getHtmlLink());
  }

  public int doEndTag()
  {
    try
    {
      if (showFlag)
      {
        if (action != null)
        {
          doActionLink();
        }
        else
        {
          doGenericLink();
        }
      }
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }

    return EVAL_PAGE;
  }

  public void release()
  {
    action = null;
    view = null;
    text = null;
    prefix = null;
    target = null;
  }
}
