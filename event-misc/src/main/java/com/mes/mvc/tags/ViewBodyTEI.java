package com.mes.mvc.tags;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.VariableInfo;
import org.apache.log4j.Logger;

public class ViewBodyTEI extends TagExtraInfo
{
  static Logger log = Logger.getLogger(ViewBodyTEI.class);
  
  public VariableInfo[] getVariableInfo(TagData tagData)
  {
    VariableInfo[] vars = new VariableInfo[2];
    vars[0] = new VariableInfo((String)tagData.getAttribute("id"),
      "com.mes.mvc.RequestHandler",true,VariableInfo.AT_BEGIN);
    vars[1] = new VariableInfo("user","com.mes.user.UserBean",
      true,VariableInfo.NESTED);
    return vars;
  }
}
