package com.mes.mvc.tags;

import javax.servlet.jsp.PageContext;
import org.apache.log4j.Logger;
import com.mes.forms.HtmlLink;
import com.mes.mvc.Link;
import com.mes.mvc.RequestHandler;

public class ViewBackLinkTag extends ViewTagBase
{
  static Logger log = Logger.getLogger(ViewBackLinkTag.class);

  private String text = null;

  public void setText(String text)
  {
    this.text = text;
  }
  public String getText()
  {
    return text;
  }

  public int doStartTag()
  {
    try
    {
      RequestHandler handler = (RequestHandler)pageContext
        .getAttribute("handler",PageContext.REQUEST_SCOPE);

      Link link = handler.getBackLink();
      if (link != null)
      {
        if (text != null)
        {
          link.setText(text);
        }

        HtmlLink hl = link.getHtmlLink();
        if (cssClass != null) hl.setProperty("class",cssClass);
        if (style != null) hl.setProperty("style",style);
        pageContext.getOut().print(hl.renderHtml());
      }
      else
      {
        log.info("No back link found for " + handler.getCurAction());
      }
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return SKIP_BODY;
  }
}
