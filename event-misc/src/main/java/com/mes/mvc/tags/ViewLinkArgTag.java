package com.mes.mvc.tags;

import org.apache.log4j.Logger;

public class ViewLinkArgTag extends ViewTagBase
{
  static Logger log = Logger.getLogger(ViewLinkArgTag.class);

  private String name;
  private String value;

  public void setName(String name)
  {
    this.name = name;
  }
  public String getName()
  {
    return name;
  }

  public void setValue(String value)
  {
    this.value = value;
  }
  public String getValue()
  {
    return value;
  }

  public int doStartTag()
  {
    try
    {
      ViewLinkTag linkTag = 
        (ViewLinkTag)findAncestorWithClass(this,ViewLinkTag.class);
      linkTag.addArg(name,value);
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return SKIP_BODY;
  }

}
