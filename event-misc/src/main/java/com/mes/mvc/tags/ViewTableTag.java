package com.mes.mvc.tags;

import java.util.List;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import org.apache.log4j.Logger;
import com.mes.forms.FieldBean;

public class ViewTableTag extends ViewTagBase
{
  static Logger log = Logger.getLogger(ViewTableTag.class);

  public static final String VIEWABLE_BEAN_NAME = "tableBeanXyz";

  private String  beanAttr;
  private String  listAttr;
  private String  varId;
  private String  varType;
  private String  emptyText;
  private String  order;
  private String  leading;
  private String  trailing;

  private boolean overrideEmptyFlag;
  private boolean reverseFlag;
  private int     leadingRows;
  private int     trailingRows;

  private boolean needsEndTag;
  private int     colTotal;

  public void updateColTotal(int colCount)
  {
    if (colTotal < colCount)
    {
      colTotal = colCount;
    }
  }
  public int getColTotal()
  {
    return colTotal;
  }

  public void setId(String id)
  {
    varId = id;
  }
  public String getId()
  {
    return varId;
  }

  public void setType(String type)
  {
    varType = type;
  }
  public String getType()
  {
    return varType;
  }

  public void setBean(String beanAttr)
  {
    this.beanAttr = beanAttr;

    // copy the named bean to a named attribute for use as a scripting variable
    // that won't conflict with a scripting variable declared outside the table
    // for the same attribute (see ViewTableTEI)
    pageContext.setAttribute(VIEWABLE_BEAN_NAME,
      pageContext.getAttribute(beanAttr,PageContext.REQUEST_SCOPE));
  }
  public String getBean()
  {
    if (listAttr != null)
    {
      doBeanAndListError();
    }
    return beanAttr;
  }

  public void setList(String listAttr)
  {
    if (beanAttr != null)
    {
      doBeanAndListError();
    }
    this.listAttr = listAttr;
  }
  public String getList()
  {
    return listAttr;
  }

  public void setEmptyCondition(String condition)
  {
    overrideEmptyFlag = condition.toLowerCase().equals("true");
  }
  public String getEmptyCondition()
  {
    return ""+overrideEmptyFlag;
  }

  public void setEmptyText(String emptyText)
  {
    this.emptyText = emptyText;
  }
  public String getEmptyText()
  {
    return emptyText;
  }

  private void doBeanAndListError()
  {
    String errorMsg = "Illegal attempt to set both bean and list in "
      + this.getClass().getName();
    log.error(errorMsg);
    throw new RuntimeException(errorMsg);
  }

  public List getRowList()
  {
    if (beanAttr != null)
    {
      Viewable viewable =
        (Viewable)pageContext.getAttribute(beanAttr,PageContext.REQUEST_SCOPE);
      return viewable.getRows();
    }
    else if (listAttr != null)
    {
      return (List)pageContext.getAttribute(listAttr,PageContext.REQUEST_SCOPE);
    }
    return null;
  }

  public Viewable getViewable()
  {
    if (beanAttr != null)
    {
      return
        (Viewable)pageContext.getAttribute(beanAttr,PageContext.REQUEST_SCOPE);
    }
    return null;
  }

  public Object getIdObj()
  {
    if (varId != null)
    {
      return pageContext.getAttribute(varId,PageContext.REQUEST_SCOPE);
    }
    return null;
  }

  public boolean hasData()
  {
    return varId != null;
  }

  public boolean hasList()
  {
    return hasBean() || listAttr != null;
  }

  public boolean hasBean()
  {
    return beanAttr != null;
  }

  private String renderStartTag()
  {
    return "<table" + renderCssAttributes() + ">";
  }

  private String renderEndTag()
  {
    return "</table>";
  }

  private String renderEmptyMarkup()
  {
    return renderStartTag() + "\n  <tr><td>" + emptyText + "</td></tr>\n" + renderEndTag();
  }

  /**
   *  table takes inputs from attributes: id, list, bean
   *
   *  emptyCondition attr (optional) gives explicit way for determining if 
   *  table is considered empty, overrides list emptiness, it is stored in 
   *  overrideEmptyFlag
   *
   *  id is considered empty only if overrideEmptyFlag is set
   *  list/bean are empty if override flag or the list itself is empty
   */
  private boolean isEmpty()
  {
    return overrideEmptyFlag || 
            ((hasBean() || hasList()) && getRowList().isEmpty());
  }

  private boolean needsEmptyNotice()
  {
    // if empty text has been set then return true unless 
    // there is a bean and it has validation errors...
    if (emptyText == null)
    {
      return false;
    }
    if (hasBean())
    {
      Viewable v = getViewable();
      if (v instanceof FieldBean)
      {
        return ((FieldBean)v).isAutoSubmitOk();
      }
    }
    return true;
  }

  public int doStartTag()
  {
    try
    {
      needsEndTag = false;
      JspWriter out = pageContext.getOut();

      if (isEmpty())
      {
        if (needsEmptyNotice())
        {
          out.print(renderEmptyMarkup());
        }
        return SKIP_BODY;
      }

      out.print(renderStartTag());
      needsEndTag = true;    
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
      return SKIP_BODY;
    }

    return EVAL_BODY_INCLUDE;
  }

  public int doEndTag()
  {
    try
    {
      if (needsEndTag)
      {
        pageContext.getOut().print(renderEndTag());
      }
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return EVAL_PAGE;
  }
}
