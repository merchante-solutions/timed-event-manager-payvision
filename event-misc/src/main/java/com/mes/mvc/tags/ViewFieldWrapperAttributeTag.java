package com.mes.mvc.tags;

import org.apache.log4j.Logger;

public class ViewFieldWrapperAttributeTag extends ViewTagBase
{
  static Logger log = Logger.getLogger(ViewFieldWrapperAttributeTag.class);

  private ViewFieldWrapperTag wrapperTag;

  private String name;
  private String data;

  public void setName(String name)
  {
    this.name = name;
  }
  public String getName()
  {
    return name;
  }

  public void setData(String data)
  {
    this.data = data;
  }
  public String getData()
  {
    return data;
  }

  public int doStartTag()
  {
    try
    {
      wrapperTag = (ViewFieldWrapperTag)
        findAncestorWithClass(this,ViewFieldWrapperTag.class);
      wrapperTag.addAttribute(this);
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return SKIP_BODY;
  }
}
