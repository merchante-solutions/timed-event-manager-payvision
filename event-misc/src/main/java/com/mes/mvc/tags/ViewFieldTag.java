package com.mes.mvc.tags;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.jsp.JspWriter;
import org.apache.log4j.Logger;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;

public class ViewFieldTag extends ViewTagBase
{
  static Logger log = Logger.getLogger(ViewFieldTag.class);

  private ViewFormTag formTag;
  private FieldBean bean;
  private Field field;

  private List wrappers = new ArrayList();

  private String name = null;
  private String tabIndex = null;

  public void setName(String name)
  {
    // HACK: have tagId set to name on fields 
    // if not set otherwise or if it is the same
    // as name (in the case of tag object reuse)
    if (tagId == null || tagId.equals(this.name))
    {
      tagId = name;
    }
    this.name = name;
  }
  public String getName()
  {
    return name;
  }

  public void setTabIndex(String tabIndex)
  {
    this.tabIndex = tabIndex;
  }
  public String getTabIndex()
  {
    return tabIndex;
  }

  private String renderField()
  {
    // TODO: fix this to not overwrite existing html extra 
    // this is to allow tag base css attributes to be present
    // in the form fields that are not naturally supported
    // by the old forms.field class
    // FIXED?
    field.setHtmlExtra(field.getHtmlExtra() + renderCssAttributes());
    StringBuffer buf = new StringBuffer(field.renderHtml());
    List w = wrappers;
    if (w.isEmpty())
    {
      w = formTag.getWrappers();
    }
    if (!w.isEmpty())
    {
      for (Iterator i = w.iterator();i.hasNext();)
      {
        ViewFieldWrapperTag wrapper = (ViewFieldWrapperTag)i.next();
        if (!wrapper.isErrorOnly() || field.getHasError())
        {
          buf.insert(0,wrapper.doWrapperStart(field));
          buf.append(wrapper.doWrapperEnd());
        }
      }
    }
    return ""+buf;
  }

  public void addWrapper(ViewFieldWrapperTag wrapper)
  {
    wrappers.add(wrapper);
  }

  public List getWrappers()
  {
    return wrappers;
  }

  public int doStartTag()
  {
    try
    {
      formTag 
        = (ViewFormTag)findAncestorWithClass(this,ViewFormTag.class);
      bean = formTag.getFieldBean();
      if (bean != null)
      {
        field = bean.getField(name);
        if (field != null)
        {
          if (tabIndex != null)
          {
            try
            {
              field.setTabIndex(Integer.parseInt(tabIndex));
            }
            catch (Exception e)
            {
              log.warn("Unable to set tab index: " + e);
            }
          }
          field.setHtmlExtra(renderCssAttributes());
        }
        else
        {
          log.warn("Field with name '" + name + "' not found in field bean");
        }
      }
      else
      {
        log.warn("Field bean not found");
      }
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
      return SKIP_BODY;
    }
    
    return EVAL_BODY_INCLUDE;
  }

  public int doEndTag()
  {
    try
    {
      JspWriter out = pageContext.getOut();
      out.print(renderField());
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return EVAL_PAGE;
  }
}
