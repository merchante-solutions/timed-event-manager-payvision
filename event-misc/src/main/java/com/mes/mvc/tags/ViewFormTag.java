package com.mes.mvc.tags;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import org.apache.log4j.Logger;
import com.mes.forms.FieldBean;
import com.mes.mvc.RequestHandler;

public class ViewFormTag extends ViewTagBase
{
  static Logger log = Logger.getLogger(ViewFormTag.class);

  private String method;
  private String action;
  private String beanAttr;
  private String typeAttr;
  private String scope = "request";
  private String frame;
  private String enctype;
  private String varName = "bean";
  private String name;

  private boolean useParentBack = true;

  public void setName(String name)
  {
    this.name = name;
  }
  public String getName()
  {
    return name;
  }

  public void setMethod(String method)
  {
    this.method = method;
  }
  public String getMethod()
  {
    return method;
  }

  public void setAction(String action)
  {
    this.action = action;
  }
  public String getAction()
  {
    return action;
  }

  public void setBean(String beanAttr)
  {
    this.beanAttr = beanAttr;
  }
  public String getBean()
  {
    return beanAttr;
  }

  public void setVarName(String varName)
  {
    this.varName = varName;
  }
  public String getVarName()
  {
    return varName;
  }

  public void setType(String typeAttr)
  {
    Class beanClass = null;
    Class fbClass = null;
    try
    {
      beanClass = Class.forName(typeAttr);
      fbClass = Class.forName("com.mes.forms.FieldBean");
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      throw new RuntimeException("" + e);
    }
    if (!fbClass.isAssignableFrom(beanClass))
    {
      String errorMsg = "Form bean class must be FieldBean";
      log.error(errorMsg);
      throw new ClassCastException(errorMsg);
    }
    this.typeAttr = typeAttr;
  }
  public String getType()
  {
    return typeAttr;
  }

  public void setScope(String scope)
  {
    if (!scope.equals("request") && !scope.equals("session"))
    {
      String errorMsg = "Attribute scope must be request or session";
      log.error(errorMsg);
      throw new RuntimeException(errorMsg);
    }
    this.scope = scope;
  }
  public String getScope()
  {
    return scope;
  }

  public void setFrame(String frame)
  {
    this.frame = frame;
  }
  public String getFrame()
  {
    return frame;
  }

  public void setEnctype(String enctype)
  {
    this.enctype = enctype;
  }
  public String getEnctype()
  {
    return enctype;
  }

  public void setUseParentBack(String useParentBackStr)
  {
    this.useParentBack = useParentBackStr.toLowerCase().equals("true");
  }
  public String getUseParentBack()
  {
    return useParentBack ? "true" : "false";
  }

  public FieldBean getFieldBean()
  {
    int scopeVal = scope.equals("request") ? 
                   PageContext.REQUEST_SCOPE : 
                   PageContext.SESSION_SCOPE;
    FieldBean bean = (FieldBean)pageContext.getAttribute(beanAttr,scopeVal);
    if (bean == null)
    {
      throw new RuntimeException("Failed to find form FieldBean named '" 
        + beanAttr + "' in scope " + scope);
    }
    return bean;
  }


  private List wrappers = new ArrayList();

  public void addWrapper(ViewFieldWrapperTag wrapper)
  {
    wrappers.add(wrapper);
  }

  public List getWrappers()
  {
    return wrappers;
  }
  private boolean isKnownFrame(String frame)
  {
    return frame != null && (frame.equals("rb") || frame.equals("rbe"));
  }

  private void doStartFraming() throws Exception
  {
    if (isKnownFrame(frame))
    {
      JspWriter out = pageContext.getOut();
      out.print("  <table class=\"" + frame + "\"><tr><td><b class=\"" + frame 
        + "-hdr\"><b class=\"" + frame + "-s1\"></b><b\n");
      out.print("   class=\"" + frame + "-s2\"></b><b class=\"" + frame 
        + "-s3\"></b><b class=\"" + frame + "-s4\"></b></b><div\n");
      out.print("   class=\"" + frame + "-body\">");
    }
  }

  private void doEndFraming() throws Exception
  {
    if (isKnownFrame(frame))
    {
      JspWriter out = pageContext.getOut();
      out.print("  </div><b class=\"" + frame + "-ftr\"><b class=\"" + frame 
        + "-s4\"></b><b class=\"" + frame + "-s3\"></b><b\n");
      out.print("   class=\"" + frame + "-s2\"></b><b class=\"" + frame 
        + "-s1\"></b></b></td></tr></table>\n\n");
    }
  }

  private String getActionUrl()
  {
    String urlStr;
    RequestHandler handler = getHandler();
    if (action != null)
    {
      urlStr = handler.getLink(action).renderUrl();
      handler.setBackLink(action,
        useParentBack ? handler.getBackLink() : handler.getCurLink());
    }
    else
    {
      urlStr = handler.getLink().renderUrl();
    }
    return urlStr;
  }

  public int doStartTag()
  {
    try
    {
      pageContext.setAttribute("bean",getFieldBean());
      JspWriter out = pageContext.getOut();
      out.print("<form method='" + method + "'");
      String actionUrl = getActionUrl();
      out.print((actionUrl != null ? " action='" + actionUrl + "'" : ""));
      out.print((name != null ? " name='" + name + "'" : ""));
      out.print((tagId != null ? " id='" + tagId + "'" : ""));
      out.print((onSubmit != null ? " onSubmit='" + onSubmit + "; return false'" : ""));
      out.print((enctype != null ? " enctype='" + enctype + "'" : "") + ">\n");
      out.print(getFieldBean().renderHiddenFields() + "\n");
      doStartFraming();
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
      return SKIP_BODY;
    }
    
    return EVAL_BODY_INCLUDE;
  }

  public int doEndTag()
  {
    try
    {
      doEndFraming();
      JspWriter out = pageContext.getOut();
      out.print("</form>");
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return EVAL_PAGE;
  }
}
