package com.mes.mvc.tags;

import org.apache.log4j.Logger;

public class ViewTableCellTag extends ViewTagBase
{
  static Logger log = Logger.getLogger(ViewTableCellTag.class);

  private ViewTableTag        tableTag;
  private ViewTableRowBaseTag rowTag;
  private String              justify;
  private String              colSpan;

  public void setColSpan(String colSpan)
  {
    this.colSpan = colSpan;
  }
  public String getColSpan()
  {
    return colSpan;
  }

  private String renderColSpan()
  {
    int spanNum = 1;
    int colNum = rowTag.getColCount() + 1;
    if (colSpan != null)
    {
      if (colSpan.toLowerCase().equals("all"))
      {
        if (colNum != 1)
        {
          throw new RuntimeException("Can only use 'all' keyword in first cell"
            + " colspan attribute");
        }
        spanNum = tableTag.getColTotal();
      }
      else
      {
        spanNum = Integer.parseInt(colSpan);
      }
    }

    rowTag.incrementColCount(spanNum);
      
    if (spanNum != 1) 
    {
      return " colspan=\"" + spanNum + "\"";
    }
    return "";
  }

  public void setJustify(String justify)
  {
    if (!justify.equals("left") && 
        !justify.equals("right") && 
        !justify.equals("center"))
    {
      String errorMsg = "Justify must be left, right or center, '" + justify 
        + "' not allowed";
      log.error(errorMsg);
      throw new RuntimeException(errorMsg);
    }
    this.justify = justify;
  }

  private boolean inHiddenRow()
  {
    ViewTableListTag listTag = 
    (ViewTableListTag)findAncestorWithClass(this,ViewTableListTag.class);
    return listTag != null && listTag.inHiddenRow();
  }

  public int doStartTag()
  {
    try
    {
      if (inHiddenRow())
      {
        return SKIP_BODY;
      }
      tableTag = (ViewTableTag)findAncestorWithClass(this,ViewTableTag.class);
      rowTag = 
        (ViewTableRowBaseTag)findAncestorWithClass(this,ViewTableRowBaseTag.class);
      pageContext.setAttribute("coltotal",""+tableTag.getColTotal());
      pageContext.setAttribute("colnum",""+(rowTag.getColCount() + 1));
      String addStyles = justify != null ? "text-align: " + justify : null;
      pageContext.getOut().print("<td" + renderCssAttributes(addStyles) 
        + renderColSpan() + ">");
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return EVAL_BODY_INCLUDE;
  }

  public int doEndTag()
  {
    try
    {
      if (!inHiddenRow())
      {
        pageContext.getOut().print("</td>");
      }
    }
    catch(Exception e)
    {
      log.error("error: " + e);
      e.printStackTrace();
    }
    
    return EVAL_PAGE;
  }
}
