package com.mes.mvc;

import com.oreilly.servlet.multipart.FilePart;

public interface ApiMultipartFileHandler
{
  public int uploadCount();
  public boolean uploadReceived();
  public String getUploadName(int index);
  public byte[] getUploadData(int index);
  public void handleFilePart(FilePart filePart);
}