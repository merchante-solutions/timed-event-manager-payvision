package com.mes.mvc;

public class Mvc
{
  public static final String PN_REQUEST_HANDLER   = "requestHandler";
  public static final String PN_CONFIRM_VIEW      = "confirmView";
  public static final String PN_MENU_VIEW         = "menuView";
  public static final String PN_PERMISSIONS       = "permissions";
  public static final String PN_DEFAULT_MENU_NAME = "defaultMenuName";

  public static final String ACTION_CONFIRM       = "confirm";
  public static final String ACTION_MENU          = "menu";

  public static final String DISCARD_ARGS_FLAG    = "@@disc-arg";
}