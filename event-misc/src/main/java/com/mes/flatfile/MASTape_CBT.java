/*@lineinfo:filename=MASTape_CBT*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/flatfile/MASTape_CBT.sqlj $

  Description:

    MASTape_CBT

    Extension of MASTape specifically for CB&T.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-04-05 17:05:50 -0700 (Thu, 05 Apr 2007) $
  Version            : $Revision: 13642 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.flatfile;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class MASTape_CBT extends MASTape
{
  public MASTape_CBT()
  {
    super();
  }
  
  public int getBankNumber()
  {
    return mesConstants.BANK_ID_CBT;
  }
  
  protected void buildDetailClientSpecific(int appSeqNum)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    // all CB&T accounts have special values in the first data capture bet slot
    try
    {
      setFieldData("dc_bet_1",                  "VP");
      setFieldData("dc_bet_num_1",              "5000");
      setFieldData("dc_bet_dda_1",              "*");
      setFieldData("dc_bet_tr_1",               "*");
      setFieldData("dc_bet_billing_method_1",   "*");
      setFieldData("dc_bet_name_addr_ind_1",    "00");
      setFieldData("dc_bet_bank_officer_1",     "*");
      
      // blank out assistant manager
      setFieldData("merchcontact_name",         "");
      
      // make sure merchant number has a leading zero
      /*@lineinfo:generated-code*//*@lineinfo:66^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  lpad(m.merch_number, 9, '0')    merch_number,
//                  rpad(cbt_credit_score, 15, '0') dun_bradstreet_number
//          from    merchant  m
//          where   m.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  lpad(m.merch_number, 9, '0')    merch_number,\n                rpad(cbt_credit_score, 15, '0') dun_bradstreet_number\n        from    merchant  m\n        where   m.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.flatfile.MASTape_CBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.flatfile.MASTape_CBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:72^7*/
      
      rs = it.getResultSet();
      if(rs.next())
      {
        setAllFieldData(rs);
      }
      
      rs.close();
      it.close();
      
      // make sure address #00 uses the merchant legal name
      /*@lineinfo:generated-code*//*@lineinfo:84^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.merch_legal_name          address_name_00
//          from    merchant  m
//          where   m.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.merch_legal_name          address_name_00\n        from    merchant  m\n        where   m.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.flatfile.MASTape_CBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.flatfile.MASTape_CBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:89^7*/
      
      rs = it.getResultSet();
      if(rs.next())
      {
        setAllFieldData(rs);
      }
      
      rs.close();
      it.close();
      
      // make sure rep code is accurate -- either all zeroes or what was entered on app
      /*@lineinfo:generated-code*//*@lineinfo:101^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(merch_rep_code,
//                    null, '0000',
//                    merch_rep_code)                     merch_rep_code
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(merch_rep_code,\n                  null, '0000',\n                  merch_rep_code)                     merch_rep_code\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.flatfile.MASTape_CBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.flatfile.MASTape_CBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:108^7*/
      
      rs = it.getResultSet();
      if(rs.next())
      {
        setAllFieldData(rs);
      }
      
      rs.close();
      it.close();
      
      // set draft storage flag equal to edc flag
      /*@lineinfo:generated-code*//*@lineinfo:120^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_edc_flag    merch_paper_draft_flag
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_edc_flag    merch_paper_draft_flag\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.flatfile.MASTape_CBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.flatfile.MASTape_CBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^7*/
      
      rs = it.getResultSet();
      if(rs.next())
      {
        setAllFieldData(rs);
      }
      
      rs.close();
      it.close();
      
      // reset owner, manager, and asst. manager names
      setFieldData("busowner2_name", "");
      setFieldData("merchcontact_name", "");
      
      /*@lineinfo:generated-code*//*@lineinfo:140^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(bo2.busowner_first_name,
//                    null, 'NONE',
//                    upper(bo2.busowner_first_name||' '||bo2.busowner_last_name))            busowner2_name,
//                  decode(mc.merchcont_prim_first_name,
//                    null, 'NONE',
//                    upper(mc.merchcont_prim_first_name||' '||mc.merchcont_prim_last_name))  contact_name
//          from    merchant mr,
//                  businessowner bo2,
//                  merchcontact mc
//          where   mr.app_seq_num  = :appSeqNum and
//                  mr.app_seq_num = bo2.app_seq_num(+) and
//                  bo2.busowner_num(+) = 2 and
//                  mr.app_seq_num = mc.app_seq_num(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(bo2.busowner_first_name,\n                  null, 'NONE',\n                  upper(bo2.busowner_first_name||' '||bo2.busowner_last_name))            busowner2_name,\n                decode(mc.merchcont_prim_first_name,\n                  null, 'NONE',\n                  upper(mc.merchcont_prim_first_name||' '||mc.merchcont_prim_last_name))  contact_name\n        from    merchant mr,\n                businessowner bo2,\n                merchcontact mc\n        where   mr.app_seq_num  =  :1  and\n                mr.app_seq_num = bo2.app_seq_num(+) and\n                bo2.busowner_num(+) = 2 and\n                mr.app_seq_num = mc.app_seq_num(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.flatfile.MASTape_CBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.flatfile.MASTape_CBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:155^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        if(rs.getString("busowner2_name").equals("NONE"))
        {
          // put contact name in manager's spot
          setFieldData("busowner2_name", rs.getString("contact_name"));
        }
        else
        {
          setFieldData("busowner2_name", rs.getString("busowner2_name"));
          setFieldData("merchcontact_name", rs.getString("contact_name"));
        }
      }
    }
    catch(Exception e)
    {
      masLogEntry("buildDetailClientSpecific(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
}/*@lineinfo:generated-code*/