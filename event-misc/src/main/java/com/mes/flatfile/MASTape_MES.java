/*@lineinfo:filename=MASTape_MES*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/flatfile/MASTape_CBT.sqlj $

  Description:

    MASTape_MES

    Extension of MASTape specifically for MES (3941)
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/29/04 10:13a $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.flatfile;

import com.mes.constants.mesConstants;

public class MASTape_MES extends MASTape
{
  public MASTape_MES()
  {
    super();
  }
  
  public int getBankNumber()
  {
    return mesConstants.BANK_ID_MES;
  }
  
  protected void buildDetailClientSpecific(int appSeqNum)
  {
    int appType = 0;
    try
    {
      // don't filter this out for Sabre because they still need the m#s at TSYS
      /*@lineinfo:generated-code*//*@lineinfo:49^7*/

//  ************************************************************
//  #sql [Ctx] { select  app.app_type
//          
//          from    application
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app.app_type\n         \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.flatfile.MASTape_MES",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:55^7*/
      
      if(appType != mesConstants.APP_TYPE_SABRE)
      {
        // MES no longer sends non-bank merchant numbers
        setFieldData("merch_amex_pcid_num", "000000");
        setFieldData("amex_settlement_flag", "");
        setFieldData("amex_merchant_number", "");
      
        setFieldData("discover_merchant_number", "");
        setFieldData("diners_merchant_number", "");
        setFieldData("jcb_merchant_number", "");
      }
    }
    catch(Exception e)
    {
      masLogEntry("buildDetailClientSpecific(" + appSeqNum + ")", e.toString());
    }
  }
}/*@lineinfo:generated-code*/