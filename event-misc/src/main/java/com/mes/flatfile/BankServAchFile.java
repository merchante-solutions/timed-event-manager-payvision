/*@lineinfo:filename=BankServAchFile*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/flatfile/BankServAchFile.sqlj $

  Description:

    FlatFileRecordField

    Construcs a BankServ-formatted ach batch file using the processId to
    identify records in the bankserv_ach_detail table.  After successful
    completion of buildACHFile() the entire file is available in a
    StringBuffer accessible using getFile().

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-01-10 11:28:45 -0800 (Wed, 10 Jan 2007) $
  Version            : $Revision: 13279 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.flatfile;

import java.sql.ResultSet;
import java.util.Calendar;
import com.mes.constants.MesFlatFiles;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class BankServAchFile extends FlatFileRecord
{
  private int           processId;
  private int           recordCount   = 0;
  private double        recordTotal   = 0.0;
  private String        fileBase      = "";
  private StringBuffer  achFile       = new StringBuffer("");
  private String        errorMsg      = "";
  private int           groupId       = 0;

  public BankServAchFile()
  {
    super();
  }
  
  public BankServAchFile(DefaultContext Ctx)
  {
    super(Ctx);
  }

  public String getFile()
  {
    return achFile.toString();
  }

  public String getFileBase()
  {
    return fileBase;
  }
  
  public String getErrorMsg()
  {
    return errorMsg;
  }

  private void generateFileName()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:79^7*/

//  ************************************************************
//  #sql [Ctx] { select  to_char(sysdate, 'YYYYMMDDHH24MI')
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_char(sysdate, 'YYYYMMDDHH24MI')\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.flatfile.BankServAchFile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   fileBase = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^7*/
    }
    catch(Exception e)
    {
      logEntry("generateFileName()", e.toString());
    }
  }
  
  public void buildHeader(int _groupId)
  {
    if(_groupId > 0)
    {
      groupId = _groupId;
    }
    
    buildHeader();
  }

  public void buildHeader()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;

    try
    {
      // generate new file name based on time & date
      generateFileName();

      setDefType(MesFlatFiles.DEF_TYPE_BANKSERV_HEADER);

      /*@lineinfo:generated-code*//*@lineinfo:114^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sysdate     file_date
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sysdate     file_date\n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.flatfile.BankServAchFile",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.flatfile.BankServAchFile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:118^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        setAllFieldData(rs);
      }

      // set file id using fileBase
      setFieldData("file_id", fileBase);
      
      // override group id if nonzero
      if(groupId > 0)
      {
        setFieldData("group_number", groupId);
      }

      achFile.append(spew());
      achFile.append("\n");


      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("buildHeader(" + processId + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** buildDetailRecords
  **
  ** Builds BankServ detail records from a resultSet.  Also tallies
  ** record counts and amounts for trailer.
  */  
  public boolean buildDetailRecords(ResultSet rs)
  {
    boolean result = false;
    try
    {
      setDefType(MesFlatFiles.DEF_TYPE_BANKSERV_DETAIL);
      
      while(rs.next())
      {
        resetAllFields();
        
        ++recordCount;
        recordTotal += rs.getDouble("target_amount");

        setAllFieldData(rs);

        if(rs.getDate("transaction_date") == null)
        {
          // set transaction date to today
          setFieldData("transaction_date", Calendar.getInstance().getTime());
        }

        achFile.append(spew());
        achFile.append("\n");
      }
      
      result = true;
    }
    catch(Exception e)
    {
      errorMsg = "buildDetailRecords(): " + e.toString();
      logEntry("buildDetailRecords()", e.toString());
    }
    
    return result;
  }

  private void buildDetails()
  {
    ResultSetIterator     it        = null;
    ResultSet             rs        = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:204^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bad.merchant_number                 debtor_id_1,
//                  bad.ach_seq_num                     debtor_id_2,
//                  bad.target_routing_number           target_routing_number,
//                  bad.target_account_number           target_account_number,
//                  bad.amount                          target_amount,
//                  bad.credit_debit_ind                credit_debit_indicator,
//                  bad.transaction_date                transaction_date,
//                  bad.entry_description               entry_description,
//                  bad.transaction_number              transaction_number,
//                  bad.nsf_service_fee                 nsf_return_service_fee_amount,
//                  bad.transaction_fee                 transaction_fee_amount,
//                  bad.deferred_settlement_days        deferred_settlement_days,
//                  bad.tran_fee_routing_number         transaction_fee_routing_number,
//                  bad.tran_fee_account_number         transaction_fee_account_number,
//                  bad.tran_fee_account_type           transaction_fee_account_type,
//                  bad.clearing_routing_number         clearing_routing_number,
//                  bad.clearing_account_number         clearing_account_number,
//                  bad.number_of_retries               number_of_retries,
//                  bad.comments                        comments,
//                  nvl(bat.account_type, 'CK')         target_account_type,
//                  badd.customer_id                    customer_id
//          from    bankserv_ach_detail       bad,
//                  bankserv_ach_descriptions badd,
//                  bankserv_account_types    bat
//          where   bad.process_sequence = :processId and
//                  bad.entry_description = badd.description and
//                  bad.merchant_number = bat.merchant_number(+)
//          order by bad.credit_debit_ind desc, bad.entry_description asc      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bad.merchant_number                 debtor_id_1,\n                bad.ach_seq_num                     debtor_id_2,\n                bad.target_routing_number           target_routing_number,\n                bad.target_account_number           target_account_number,\n                bad.amount                          target_amount,\n                bad.credit_debit_ind                credit_debit_indicator,\n                bad.transaction_date                transaction_date,\n                bad.entry_description               entry_description,\n                bad.transaction_number              transaction_number,\n                bad.nsf_service_fee                 nsf_return_service_fee_amount,\n                bad.transaction_fee                 transaction_fee_amount,\n                bad.deferred_settlement_days        deferred_settlement_days,\n                bad.tran_fee_routing_number         transaction_fee_routing_number,\n                bad.tran_fee_account_number         transaction_fee_account_number,\n                bad.tran_fee_account_type           transaction_fee_account_type,\n                bad.clearing_routing_number         clearing_routing_number,\n                bad.clearing_account_number         clearing_account_number,\n                bad.number_of_retries               number_of_retries,\n                bad.comments                        comments,\n                nvl(bat.account_type, 'CK')         target_account_type,\n                badd.customer_id                    customer_id\n        from    bankserv_ach_detail       bad,\n                bankserv_ach_descriptions badd,\n                bankserv_account_types    bat\n        where   bad.process_sequence =  :1  and\n                bad.entry_description = badd.description and\n                bad.merchant_number = bat.merchant_number(+)\n        order by bad.credit_debit_ind desc, bad.entry_description asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.flatfile.BankServAchFile",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,processId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.flatfile.BankServAchFile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:234^7*/

      rs = it.getResultSet();
      
      buildDetailRecords(rs);
    }
    catch(Exception e)
    {
      logEntry("buildDetails(" + processId + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  public void buildTrailer()
  {
    try
    {
      setDefType(MesFlatFiles.DEF_TYPE_BANKSERV_TRAILER);

      // set record count and total
      setFieldData("file_detail_record_count", recordCount);
      setFieldData("file_dollar_amount", recordTotal);

      achFile.append(spew());
      achFile.append("\n");
    }
    catch(Exception e)
    {
      logEntry("buildTrailer(" + processId + ")", e.toString());
    }
  }

  public void buildACHFile(int pid)
  {
    try
    {
      connect();

      this.processId = pid;

      achFile.setLength(0);

      // build header record
      buildHeader();

      // build details including headers and trailers
      buildDetails();

      // build trailer
      buildTrailer();
    }
    catch(Exception e)
    {
      logEntry("buildACHFile(" + processId + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/