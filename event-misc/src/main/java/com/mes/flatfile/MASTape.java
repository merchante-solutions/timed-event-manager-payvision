/*@lineinfo:filename=MASTape*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/flatfile/MASTape.sqlj $

  Description:

    MasTape

    Extension of FlatFileRecord specifically for constructing MAS tape
    files.  The base version (this file) is a complete implementation
    of the MES-style MAS file.  Extensions to this file should be used
    for clients that differ from how MES constructs the tape file.
    
  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-11-12 15:27:32 -0800 (Thu, 12 Nov 2015) $
  Version            : $Revision: 23997 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.flatfile;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Vector;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class MASTape extends FlatFileRecord
{
  private int             procSequence    = 0;
  private StringBuffer    masFile         = new StringBuffer("");
  private Vector          apps            = null;
  
  private boolean         errors          = false;
  private Vector          errorList       = new Vector();
  
  public MASTape()
  {
    super();
  }
  
  public int getBankNumber()
  {
    // must be overridden by extending classes
    return mesConstants.BANK_ID_MES;
  }
  
  private long getBatchNumber()
  {
    return(100000000 + procSequence);
  }
  
  public String getMasFile()
  {
    return masFile.toString();
  }
  public boolean getFileSuccess()
  {
    return !errors;
  }
  public Vector getErrorList()
  {
    return errorList;
  }
  public int getProcSequence()
  {
    return procSequence;
  }
  
  protected void masLogEntry(String source, String msg)
  {
    try
    {
      // log in java_log
      logEntry(source, msg);
      
      errors = true;
      errorList.add(source + ": " + msg);
    }
    catch(Exception e)
    {
      logEntry("masLogEntry()", e.toString());
    }
  }
  
  public boolean buildMASTapeApps(Vector _apps)
  {
    boolean result = false;
    
    try
    {
      connect();
      
      this.apps = _apps;
      
      setDefType(MesFlatFiles.DEF_TYPE_MAS_DETAIL_BRG);
      
      for(int i = 0; i < apps.size(); ++i)
      {
        resetAllFields();
        buildDetailRecord(i);
        masFile.append(spew());
        masFile.append("\n");
      }
      
      if(apps.size() > 0)
      {
        // build trailer
        setDefType(MesFlatFiles.DEF_TYPE_MAS_TRAILER);
        buildTrailer(apps.size() + 1);
      
        masFile.append(spew());
        masFile.append("\n");
      }
      else
      {
        masLogEntry("buildMASTape()", "No records for bank: " + getBankNumber());
      }
      
      result = true;
    }
    catch(Exception e)
    {
      masLogEntry("buildMASTapeApps()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  /*
  **  String buildMASTape()
  **
  **  Constructs a complete MAS tape file in memory and returns a String
  **  reference to the file
  */
  public boolean buildMASTape()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             result  = false;
    
    try
    {
      // make sure we're connected
      connect();
      
      apps = new Vector();
      
      // get process sequence
      /*@lineinfo:generated-code*//*@lineinfo:170^7*/

//  ************************************************************
//  #sql [Ctx] { select  mas_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mas_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.flatfile.MASTape",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procSequence = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:175^7*/
      
      // update waiting records in app_setup_complete
      /*@lineinfo:generated-code*//*@lineinfo:178^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_setup_complete
//          set     process_sequence = :procSequence
//          where   process_sequence is null and
//                  app_seq_num in 
//                  (
//                    select  ac.app_seq_num
//                    from    app_setup_complete  ac,
//                            application         app,
//                            app_type            atype
//                    where   ac.app_seq_num = app.app_seq_num and
//                            app.app_type = atype.app_type_code and
//                            atype.app_bank_number = :getBankNumber()
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_467 = getBankNumber();
   String theSqlTS = "update  app_setup_complete\n        set     process_sequence =  :1 \n        where   process_sequence is null and\n                app_seq_num in \n                (\n                  select  ac.app_seq_num\n                  from    app_setup_complete  ac,\n                          application         app,\n                          app_type            atype\n                  where   ac.app_seq_num = app.app_seq_num and\n                          app.app_type = atype.app_type_code and\n                          atype.app_bank_number =  :2 \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   __sJT_st.setInt(2,__sJT_467);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:193^7*/
      
      // retrieve app_seq_nums for apps and place into vector
      /*@lineinfo:generated-code*//*@lineinfo:196^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct app_seq_num
//          from    app_setup_complete
//          where   process_sequence = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct app_seq_num\n        from    app_setup_complete\n        where   process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:201^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        apps.add(rs.getInt("app_seq_num"));
      }
      
      rs.close();
      it.close();
      
      if(buildMASTapeApps(apps))
      {
        // update records to show processed
        /*@lineinfo:generated-code*//*@lineinfo:216^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_setup_complete
//            set     date_processed = sysdate
//            where   process_sequence = :procSequence
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_setup_complete\n          set     date_processed = sysdate\n          where   process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:221^9*/
      }
      
      result = (errors == false);
    }
    catch(Exception e)
    {
      masLogEntry("buildMASTape()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  private void buildTrailer(int sequence)
  {
    try
    {
      setFieldData("batch_number", getBatchNumber());
      setFieldData("record_sequence", sequence);
      setFieldData("bank_number", getBankNumber());
    }
    catch(Exception e)
    {
      masLogEntry("buildTrailer()", e.toString());
    }
  }
  
  private void buildDetailRecord(int idx)
  {
    int appSeqNum = 0;
    try
    {
      appSeqNum = ((Integer)(apps.elementAt(idx))).intValue();
      buildDetailMerchantData(appSeqNum, idx + 1);
      buildDetailUserData(appSeqNum);
      buildDetailInterchangeBets(appSeqNum);
      buildDetailNonBankCard(appSeqNum);
      buildDetailAddresses(appSeqNum);
      buildDetailChargeRecords(appSeqNum);
      buildDetailPlanTypes(appSeqNum);
      buildDetailDCBets(appSeqNum);
      buildDetailAuthBets(appSeqNum);

/*      
      // default MVV to all blanks to avoid rejects
      if(getBankNumber() == mesConstants.BANK_ID_MES ||
         getBankNumber() == mesConstants.BANK_ID_CBT )
      {
        setFieldData("merchant_verification_value", "          ");
      }
*/
      
      // must be done last so the customized version can override existing data
      buildDetailClientSpecific(appSeqNum);
    }
    catch(Exception e)
    {
      masLogEntry("buildDetailRecord(" + idx + ")", e.toString());
    }
  }
  
  /*
  ** buildDetailClientSpecific
  */
  protected void buildDetailClientSpecific(int appSeqNum)
  {
  }
  
  /*
  ** buildDetailAuthBets
  */
  protected void buildDetailAuthBets(int appSeqNum)
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:305^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bab.vendor_id             auth_vendor_id_,
//                  bab.media_type            auth_media_type_,
//                  bab.plan_type             auth_plan_type_,
//                  bab.bet                   auth_bet_
//          from    billcard_auth_bet bab
//          where   app_seq_num = :appSeqNum
//          order by bab.num_of_bet asc        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bab.vendor_id             auth_vendor_id_,\n                bab.media_type            auth_media_type_,\n                bab.plan_type             auth_plan_type_,\n                bab.bet                   auth_bet_\n        from    billcard_auth_bet bab\n        where   app_seq_num =  :1 \n        order by bab.num_of_bet asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:314^7*/
      
      rs = it.getResultSet();
      
      int idx = 1;
      
      while(rs.next())
      {
        if(idx > 36)
        {
          break;
        }
        
        ResultSetMetaData   md = rs.getMetaData();
        
        // set data for columns (with chargeId appended to column name)
        for(int i=1; i <= md.getColumnCount(); ++i)
        {
          String columnName = md.getColumnName(i).toLowerCase();
          
          StringBuffer fieldName = new StringBuffer(columnName);
          fieldName.append(Integer.toString(idx));
          
          switch(findFieldByName(fieldName.toString()).getType())
          {
            case FlatFileRecordField.FIELD_TYPE_NUMERIC:
              setFieldData(fieldName.toString(), rs.getDouble(columnName));
              break;
              
            case FlatFileRecordField.FIELD_TYPE_DATE:
              setFieldData(fieldName.toString(), new java.sql.Date(rs.getTimestamp(columnName).getTime()));
              break;

            case FlatFileRecordField.FIELD_TYPE_ALPHA:
            default:
              setFieldData(fieldName.toString(), rs.getString(columnName));
              break;
          }
        }

        ++idx;
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      masLogEntry("buildDetailAuthBets(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  /*
  ** buildDetailDCBets
  */
  protected void buildDetailDCBets(int appSeqNum)
  {
    // base class (MES version) doesn't use data capture bets
  }
  
  /*
  ** setPlanData
  */
  protected void setPlanData(int idx, String cardPlan, CardPlan cp)
  {
    try
    {
      setFieldData("plan_type_" + idx,            cardPlan);
      setFieldData("billcard_disc_type_" + idx,   cp.discType);
      setFieldData("billcard_disc_method_" + idx, cp.discMethod);
      setFieldData("billcard_drt_number_" + idx,  cp.drtNumber);
      setFieldData("billcard_disc_rate_" + idx,   cp.discRate);
      setFieldData("billcard_peritem_fee_" + idx, cp.perItem);
      setFieldData("billcard_flr_limit_" + idx,   cp.flrLimit);
      setFieldData("billcard_ach_option_" + idx,  cp.achOption);
      setFieldData("billcard_auth_option_" + idx, cp.authOption);
      setFieldData("card_plan_bet_" + idx,        cp.indPlanBet);
    }
    catch(Exception e)
    {
      logEntry("setPlanData(" + idx + ", " + cardPlan + ")", e.toString());
    }
  }
  
  /*
  ** buildDetailPlanTypes
  */
  protected void buildDetailPlanTypes(int appSeqNum)
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    int                   appType = -1;
    
    try
    {
      // get app type for special situations
      /*@lineinfo:generated-code*//*@lineinfo:416^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//          
//          from    application
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n         \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.flatfile.MASTape",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:422^7*/
      
      if(appType == mesConstants.APP_TYPE_SABRE)
      {
        // sabre has very specific individual plan type numbers
        /*@lineinfo:generated-code*//*@lineinfo:427^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(bc.cardtype_code,
//                      1, 'VS',
//                      4, 'MC',
//                      7, 'P1',
//                      10,'DC',
//                      14,'DS',
//                      16,'AM',
//                      'P1')                           plan_type_,
//                    bc.billcard_disc_type             billcard_disc_type_,
//                    bc.billcard_disc_method           billcard_disc_method_,
//                    bc.billcard_drt_number            billcard_drt_number_,
//                    bc.billcard_disc_rate             billcard_disc_rate_,
//                    bc.billcard_peritem_fee           billcard_peritem_fee_,
//                    bc.billcard_flr_limit             billcard_flr_limit_,
//                    bc.billcard_ach_option            billcard_ach_option_,
//                    bc.billcard_auth_option           billcard_auth_option_,
//                    decode(bc.cardtype_code,
//                      1, '0011',
//                      4, '0012',
//                      7, '0017',
//                      10,'0015',
//                      14,'0016',
//                      16,'0013',
//                      '*')                            card_plan_bet_,
//                    nvl(bc.billcard_brg,'00000')      billcard_brg_
//            from    billcard  bc
//            where   bc.app_seq_num = :appSeqNum
//            order by bc.cardtype_code asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(bc.cardtype_code,\n                    1, 'VS',\n                    4, 'MC',\n                    7, 'P1',\n                    10,'DC',\n                    14,'DS',\n                    16,'AM',\n                    'P1')                           plan_type_,\n                  bc.billcard_disc_type             billcard_disc_type_,\n                  bc.billcard_disc_method           billcard_disc_method_,\n                  bc.billcard_drt_number            billcard_drt_number_,\n                  bc.billcard_disc_rate             billcard_disc_rate_,\n                  bc.billcard_peritem_fee           billcard_peritem_fee_,\n                  bc.billcard_flr_limit             billcard_flr_limit_,\n                  bc.billcard_ach_option            billcard_ach_option_,\n                  bc.billcard_auth_option           billcard_auth_option_,\n                  decode(bc.cardtype_code,\n                    1, '0011',\n                    4, '0012',\n                    7, '0017',\n                    10,'0015',\n                    14,'0016',\n                    16,'0013',\n                    '*')                            card_plan_bet_,\n                  nvl(bc.billcard_brg,'00000')      billcard_brg_\n          from    billcard  bc\n          where   bc.app_seq_num =  :1 \n          order by bc.cardtype_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:457^9*/
      }
      else
      {
        // normal 'rolled-up' pricing
        /*@lineinfo:generated-code*//*@lineinfo:462^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(bc.cardtype_code,
//                      1, 'VS',
//                      2, 'V$',
//                      11,'VB',
//                      12,'VD',
//                      4, 'MC',
//                      5, 'M$',
//                      25,'MD',
//                      13,'MB',
//                      10,'DC',
//                      14,'DS',
//                      15,'JC',
//                      16,'AM',
//                      17,'DB',
//                      18,'CK',
//                      'P1')                           plan_type_,
//                    bc.billcard_disc_type             billcard_disc_type_,
//                    bc.billcard_disc_method           billcard_disc_method_,
//                    bc.billcard_drt_number            billcard_drt_number_,
//                    bc.billcard_disc_rate             billcard_disc_rate_,
//                    bc.billcard_peritem_fee           billcard_peritem_fee_,
//                    bc.billcard_flr_limit             billcard_flr_limit_,
//                    bc.billcard_ach_option            billcard_ach_option_,
//                    bc.billcard_auth_option           billcard_auth_option_,
//                    nvl(bc.billcard_bet_number, '*')  card_plan_bet_,
//                    nvl(bc.billcard_brg,'00000')      billcard_brg_
//            from    billcard  bc
//            where   bc.app_seq_num = :appSeqNum
//            order by bc.cardtype_code asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(bc.cardtype_code,\n                    1, 'VS',\n                    2, 'V$',\n                    11,'VB',\n                    12,'VD',\n                    4, 'MC',\n                    5, 'M$',\n                    25,'MD',\n                    13,'MB',\n                    10,'DC',\n                    14,'DS',\n                    15,'JC',\n                    16,'AM',\n                    17,'DB',\n                    18,'CK',\n                    'P1')                           plan_type_,\n                  bc.billcard_disc_type             billcard_disc_type_,\n                  bc.billcard_disc_method           billcard_disc_method_,\n                  bc.billcard_drt_number            billcard_drt_number_,\n                  bc.billcard_disc_rate             billcard_disc_rate_,\n                  bc.billcard_peritem_fee           billcard_peritem_fee_,\n                  bc.billcard_flr_limit             billcard_flr_limit_,\n                  bc.billcard_ach_option            billcard_ach_option_,\n                  bc.billcard_auth_option           billcard_auth_option_,\n                  nvl(bc.billcard_bet_number, '*')  card_plan_bet_,\n                  nvl(bc.billcard_brg,'00000')      billcard_brg_\n          from    billcard  bc\n          where   bc.app_seq_num =  :1 \n          order by bc.cardtype_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:493^9*/
      }
    
      rs = it.getResultSet();
  
      int idx = 1;
      while(rs.next())
      {
        if(idx > 14)
        {
          break;
        }
    
        ResultSetMetaData   md = rs.getMetaData();
    
        // set data for columns (with chargeId appended to column name)
        for(int i=1; i <= md.getColumnCount(); ++i)
        {
          String columnName = md.getColumnName(i).toLowerCase();
      
          StringBuffer fieldName = new StringBuffer(columnName);
          fieldName.append(Integer.toString(idx));
      
          switch(findFieldByName(fieldName.toString()).getType())
          {
            case FlatFileRecordField.FIELD_TYPE_NUMERIC:
              setFieldData(fieldName.toString(), rs.getDouble(columnName));
              break;
          
            case FlatFileRecordField.FIELD_TYPE_DATE:
              setFieldData(fieldName.toString(), new java.sql.Date(rs.getTimestamp(columnName).getTime()));
              break;

            case FlatFileRecordField.FIELD_TYPE_ALPHA:
            default:
              setFieldData(fieldName.toString(), rs.getString(columnName));
              break;
          }
        }

        ++idx;
      }
  
      rs.close();
      it.close();
      
      // set other card type pricing if necessary
      int vdCount = 0;
      int vbCount = 0;
      int mdCount = 0;
      int mbCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:544^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(bc1.cardtype_code),
//                  count(bc2.cardtype_code),
//                  count(bc3.cardtype_code),
//                  count(bc4.cardtype_code)
//          
//          from    billcard bc1,
//                  billcard bc2,
//                  billcard bc3,
//                  billcard bc4
//          where   bc1.app_seq_num = :appSeqNum and
//                  bc1.cardtype_code = 12 and
//                  bc2.app_seq_num(+) = bc1.app_seq_num and
//                  bc2.cardtype_code(+) = 11 and
//                  bc3.app_seq_num(+) = bc1.app_seq_num and
//                  bc3.cardtype_code(+) = 25 and
//                  bc4.app_seq_num(+) = bc1.app_seq_num and
//                  bc4.cardtype_code(+) = 13
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(bc1.cardtype_code),\n                count(bc2.cardtype_code),\n                count(bc3.cardtype_code),\n                count(bc4.cardtype_code)\n         \n        from    billcard bc1,\n                billcard bc2,\n                billcard bc3,\n                billcard bc4\n        where   bc1.app_seq_num =  :1  and\n                bc1.cardtype_code = 12 and\n                bc2.app_seq_num(+) = bc1.app_seq_num and\n                bc2.cardtype_code(+) = 11 and\n                bc3.app_seq_num(+) = bc1.app_seq_num and\n                bc3.cardtype_code(+) = 25 and\n                bc4.app_seq_num(+) = bc1.app_seq_num and\n                bc4.cardtype_code(+) = 13";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.flatfile.MASTape",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vdCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vbCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mdCount = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mbCount = __sJT_rs.getInt(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:566^7*/
      
      if(vdCount > 0 || vbCount > 0 || mdCount > 0 || mbCount > 0)
      {
        // if we are here then we need to build all non-consumer card plans
        if(vbCount == 0)
        {
          // set visa business card equal to visa consumer card
          /*@lineinfo:generated-code*//*@lineinfo:574^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  bc.billcard_disc_type         billcard_disc_type,
//                      bc.billcard_disc_method       billcard_disc_method,
//                      bc.billcard_drt_number        billcard_drt_number,
//                      bc.billcard_disc_rate         billcard_disc_rate,
//                      bc.billcard_peritem_fee       billcard_peritem_fee,
//                      bc.billcard_flr_limit         billcard_flr_limit,
//                      bc.billcard_ach_option        billcard_ach_option,
//                      bc.billcard_auth_option       billcard_auth_option,
//                      bc.billcard_bet_number        billcard_bet_number,
//                      nvl(bc.billcard_brg,'00000')  billcard_brg
//              from    billcard    bc
//              where   bc.app_seq_num = :appSeqNum and
//                      bc.cardtype_code = :mesConstants.APP_CT_VISA
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bc.billcard_disc_type         billcard_disc_type,\n                    bc.billcard_disc_method       billcard_disc_method,\n                    bc.billcard_drt_number        billcard_drt_number,\n                    bc.billcard_disc_rate         billcard_disc_rate,\n                    bc.billcard_peritem_fee       billcard_peritem_fee,\n                    bc.billcard_flr_limit         billcard_flr_limit,\n                    bc.billcard_ach_option        billcard_ach_option,\n                    bc.billcard_auth_option       billcard_auth_option,\n                    bc.billcard_bet_number        billcard_bet_number,\n                    nvl(bc.billcard_brg,'00000')  billcard_brg\n            from    billcard    bc\n            where   bc.app_seq_num =  :1  and\n                    bc.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:589^11*/
      
          rs = it.getResultSet();
      
          CardPlan cp = null;
      
          if(rs.next())
          {
            cp = new CardPlan(rs);
          }
      
          rs.close();
          it.close();
      
          setPlanData(idx++, "VB", cp);
        }
        
        if(mdCount == 0)
        {
          // set mc check card equal to visa check card
          /*@lineinfo:generated-code*//*@lineinfo:609^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  bc.billcard_disc_type         billcard_disc_type,
//                      bc.billcard_disc_method       billcard_disc_method,
//                      bc.billcard_drt_number        billcard_drt_number,
//                      bc.billcard_disc_rate         billcard_disc_rate,
//                      bc.billcard_peritem_fee       billcard_peritem_fee,
//                      bc.billcard_flr_limit         billcard_flr_limit,
//                      bc.billcard_ach_option        billcard_ach_option,
//                      bc.billcard_auth_option       billcard_auth_option,
//                      bc.billcard_bet_number        billcard_bet_number,
//                      nvl(bc.billcard_brg,'00000')  billcard_brg
//              from    billcard    bc
//              where   bc.app_seq_num = :appSeqNum and
//                      bc.cardtype_code = :mesConstants.APP_CT_VISA_CHECK_CARD
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bc.billcard_disc_type         billcard_disc_type,\n                    bc.billcard_disc_method       billcard_disc_method,\n                    bc.billcard_drt_number        billcard_drt_number,\n                    bc.billcard_disc_rate         billcard_disc_rate,\n                    bc.billcard_peritem_fee       billcard_peritem_fee,\n                    bc.billcard_flr_limit         billcard_flr_limit,\n                    bc.billcard_ach_option        billcard_ach_option,\n                    bc.billcard_auth_option       billcard_auth_option,\n                    bc.billcard_bet_number        billcard_bet_number,\n                    nvl(bc.billcard_brg,'00000')  billcard_brg\n            from    billcard    bc\n            where   bc.app_seq_num =  :1  and\n                    bc.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA_CHECK_CARD);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:624^11*/
      
          rs = it.getResultSet();
      
          CardPlan cp = null;
      
          if(rs.next())
          {
            cp = new CardPlan(rs);
          }
      
          rs.close();
          it.close();
      
          setPlanData(idx++, "MD", cp);
        }
        
        if(mbCount == 0)
        {
          // set mc business card equal to mc consumer card
          /*@lineinfo:generated-code*//*@lineinfo:644^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  bc.billcard_disc_type         billcard_disc_type,
//                      bc.billcard_disc_method       billcard_disc_method,
//                      bc.billcard_drt_number        billcard_drt_number,
//                      bc.billcard_disc_rate         billcard_disc_rate,
//                      bc.billcard_peritem_fee       billcard_peritem_fee,
//                      bc.billcard_flr_limit         billcard_flr_limit,
//                      bc.billcard_ach_option        billcard_ach_option,
//                      bc.billcard_auth_option       billcard_auth_option,
//                      bc.billcard_bet_number        billcard_bet_number,
//                      nvl(bc.billcard_brg,'00000')  billcard_brg
//              from    billcard    bc
//              where   bc.app_seq_num = :appSeqNum and
//                      bc.cardtype_code = :mesConstants.APP_CT_MC
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bc.billcard_disc_type         billcard_disc_type,\n                    bc.billcard_disc_method       billcard_disc_method,\n                    bc.billcard_drt_number        billcard_drt_number,\n                    bc.billcard_disc_rate         billcard_disc_rate,\n                    bc.billcard_peritem_fee       billcard_peritem_fee,\n                    bc.billcard_flr_limit         billcard_flr_limit,\n                    bc.billcard_ach_option        billcard_ach_option,\n                    bc.billcard_auth_option       billcard_auth_option,\n                    bc.billcard_bet_number        billcard_bet_number,\n                    nvl(bc.billcard_brg,'00000')  billcard_brg\n            from    billcard    bc\n            where   bc.app_seq_num =  :1  and\n                    bc.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_MC);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:659^11*/
      
          rs = it.getResultSet();
      
          CardPlan cp = null;
      
          if(rs.next())
          {
            cp = new CardPlan(rs);
          }
      
          rs.close();
          it.close();
      
          setPlanData(idx++, "MB", cp);
        }
      }
    }
    catch(Exception e)
    {
      masLogEntry("buildDetailPlanTypes(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** buildDetailChargeRecords
  */
  protected void buildDetailChargeRecords(int appSeqNum)
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      java.sql.Date chargeMonth     = null;
      String        dda             = null;
      long          transitRouting  = 0L;
      
      // first get charge month for minimum discount charge record
      /*@lineinfo:generated-code*//*@lineinfo:703^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  trunc(add_months(sysdate,
//                    -- calculate the starting month based on current day
//                    decode( trunc((trunc(sysdate,'DD')-trunc(sysdate,'MM'))/15),
//                            0, 0,
//                            1 ) + 
//                    -- add any months requested by this app type
//                    nvl(apt.min_discount_delay_months,0) +
//                    -- add 2 months if the app type delays conversion fees
//                    decode( nvl(apt.delay_monthly_fees_conversion,'N'),
//                            'N', 0, 
//                            decode(mr.account_type,'C',2,0) )
//                                  ),'MM')         charge_month,
//                  mb.merchbank_acct_num           dda,
//                  mb.merchbank_transit_route_num  transit_routing
//          from    merchbank     mb,
//                  application   app,
//                  app_type      apt,
//                  merchant      mr
//          where   app.app_seq_num = :appSeqNum and
//                  app.app_type = apt.app_type_code and
//                  app.app_seq_num = mb.app_seq_num and
//                  mb.merchbank_acct_srnum = 1 and
//                  mr.app_seq_num = app.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  trunc(add_months(sysdate,\n                  -- calculate the starting month based on current day\n                  decode( trunc((trunc(sysdate,'DD')-trunc(sysdate,'MM'))/15),\n                          0, 0,\n                          1 ) + \n                  -- add any months requested by this app type\n                  nvl(apt.min_discount_delay_months,0) +\n                  -- add 2 months if the app type delays conversion fees\n                  decode( nvl(apt.delay_monthly_fees_conversion,'N'),\n                          'N', 0, \n                          decode(mr.account_type,'C',2,0) )\n                                ),'MM')         charge_month,\n                mb.merchbank_acct_num           dda,\n                mb.merchbank_transit_route_num  transit_routing\n        from    merchbank     mb,\n                application   app,\n                app_type      apt,\n                merchant      mr\n        where   app.app_seq_num =  :1  and\n                app.app_type = apt.app_type_code and\n                app.app_seq_num = mb.app_seq_num and\n                mb.merchbank_acct_srnum = 1 and\n                mr.app_seq_num = app.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:728^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        chargeMonth     = rs.getDate("charge_month");
        dda             = rs.getString("dda");
        transitRouting  = rs.getLong("transit_routing");
      }
      
      rs.close();
      it.close();
      
      // set discount record charge month
      setFieldData("charge_start_date_1", chargeMonth);
      setFieldData("charge_dda_1", dda);
      setFieldData("charge_transit_route_1", transitRouting);
      
      // get rest of charge records
      /*@lineinfo:generated-code*//*@lineinfo:748^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bo.billoth_charge_type                                billoth_charge_type_,
//                  nvl(bo.billoth_charge_method,'D')                     billoth_charge_method_,
//                  decode(upper(bo.billoth_charge_msg),
//                    'ANNUAL PCI COMPLIANCE FEE', 'NNNNNNNNNNYN',
//                    bo.billoth_charge_frq)                              billoth_charge_frq_,
//                  add_months(to_date(bo.billoth_charge_startdate,'MM/dd/yy'), nvl(waive.waive_months,0)) billoth_charge_startdate_,
//                  decode(bo.billoth_charge_expiredate,
//                    null, '999999',
//                    '999999', '999999',
//                    to_char(
//                      add_months(to_date(bo.billoth_charge_expiredate, 'MM/dd/yy'), nvl(waive_months,0)),
//                      'MMddyy'))                                        billoth_charge_expiredate_,
//                  bo.billoth_charge_amt                                 billoth_charge_amt_,
//                  bo.billoth_charge_type                                billoth_charge_type_,
//                  bo.billoth_charge_times                               billoth_charge_times_,
//                  bo.billoth_charge_tax_ind                             billoth_charge_tax_ind_,
//                  upper(bo.billoth_charge_msg)                          billoth_charge_msg_,
//                  :dda                                                  billoth_charge_dda_,
//                  :transitRouting                                       billoth_charge_transit_route_
//          from    billother     bo,
//                  (
//                    select  apw.charge_type,
//                            apw.waive_months  
//                    from    application app,
//                            app_promotion_waivers apw
//                    where   app.app_seq_num =:appSeqNum and
//                            app.app_type = apw.app_type and
//                            lower(apw.enabled) = 'y' and
//                            sysdate between apw.start_date and apw.end_date           
//                  ) waive
//          where   bo.app_seq_num = :appSeqNum and
//                  bo.misc_code = waive.charge_type(+)
//          order by bo.billoth_sr_num asc      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bo.billoth_charge_type                                billoth_charge_type_,\n                nvl(bo.billoth_charge_method,'D')                     billoth_charge_method_,\n                decode(upper(bo.billoth_charge_msg),\n                  'ANNUAL PCI COMPLIANCE FEE', 'NNNNNNNNNNYN',\n                  bo.billoth_charge_frq)                              billoth_charge_frq_,\n                add_months(to_date(bo.billoth_charge_startdate,'MM/dd/yy'), nvl(waive.waive_months,0)) billoth_charge_startdate_,\n                decode(bo.billoth_charge_expiredate,\n                  null, '999999',\n                  '999999', '999999',\n                  to_char(\n                    add_months(to_date(bo.billoth_charge_expiredate, 'MM/dd/yy'), nvl(waive_months,0)),\n                    'MMddyy'))                                        billoth_charge_expiredate_,\n                bo.billoth_charge_amt                                 billoth_charge_amt_,\n                bo.billoth_charge_type                                billoth_charge_type_,\n                bo.billoth_charge_times                               billoth_charge_times_,\n                bo.billoth_charge_tax_ind                             billoth_charge_tax_ind_,\n                upper(bo.billoth_charge_msg)                          billoth_charge_msg_,\n                 :1                                                   billoth_charge_dda_,\n                 :2                                        billoth_charge_transit_route_\n        from    billother     bo,\n                (\n                  select  apw.charge_type,\n                          apw.waive_months  \n                  from    application app,\n                          app_promotion_waivers apw\n                  where   app.app_seq_num = :3  and\n                          app.app_type = apw.app_type and\n                          lower(apw.enabled) = 'y' and\n                          sysdate between apw.start_date and apw.end_date           \n                ) waive\n        where   bo.app_seq_num =  :4  and\n                bo.misc_code = waive.charge_type(+)\n        order by bo.billoth_sr_num asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,dda);
   __sJT_st.setLong(2,transitRouting);
   __sJT_st.setInt(3,appSeqNum);
   __sJT_st.setInt(4,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:783^7*/
      
      rs = it.getResultSet();
      
      int idx=2;    // start with 2 because the first record is defaulted
      
      while(rs.next())
      {
        if(idx > 10)
        {
          break;
        }
        
        ResultSetMetaData   md = rs.getMetaData();
        
        // set data for columns (with chargeId appended to column name)
        for(int i=1; i <= md.getColumnCount(); ++i)
        {
          String columnName = md.getColumnName(i).toLowerCase();
          
          StringBuffer fieldName = new StringBuffer(columnName);
          fieldName.append(Integer.toString(idx));
          
          switch(findFieldByName(fieldName.toString()).getType())
          {
            case FlatFileRecordField.FIELD_TYPE_NUMERIC:
              setFieldData(fieldName.toString(), rs.getDouble(columnName));
              break;
              
            case FlatFileRecordField.FIELD_TYPE_DATE:
              setFieldData(fieldName.toString(), new java.sql.Date(rs.getTimestamp(columnName).getTime()));
              break;

            case FlatFileRecordField.FIELD_TYPE_ALPHA:
            default:
              setFieldData(fieldName.toString(), rs.getString(columnName));
              break;
          }
        }

        ++idx;
      }
    }
    catch(Exception e)
    {
      masLogEntry("buildDetailChargeRecords(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { it.close(); } catch (Exception e) {}
    }
  }
  
  /*
  ** buildDetailAddresses
  */
  protected void buildDetailAddresses(int appSeqNum)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:847^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ai.ind_dis                  ind_dis,
//                  ai.ind_rcl                  ind_rcl,
//                  ai.ind_crb                  ind_crb,
//                  ai.ind_mlr                  ind_mlr,
//                  ai.ind_ccd                  ind_ccd,
//                  ai.ind_imp                  ind_imp,
//                  ai.ind_mem                  ind_mem,
//                  ai.ind_pos                  ind_pos,
//                  ai.ind_mis                  ind_mis,
//                  ai.ind_nsg                  ind_nsg,
//                  ai.ind_ajf                  ind_ajf,
//                  ai.ind_cbf                  ind_cbf,
//                  ai.ind_bt1                  ind_bt1,
//                  ai.ind_bt2                  ind_bt2,
//                  ai.ind_bt3                  ind_bt3
//          from    address_indicator_usages    ai
//          where   ai.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ai.ind_dis                  ind_dis,\n                ai.ind_rcl                  ind_rcl,\n                ai.ind_crb                  ind_crb,\n                ai.ind_mlr                  ind_mlr,\n                ai.ind_ccd                  ind_ccd,\n                ai.ind_imp                  ind_imp,\n                ai.ind_mem                  ind_mem,\n                ai.ind_pos                  ind_pos,\n                ai.ind_mis                  ind_mis,\n                ai.ind_nsg                  ind_nsg,\n                ai.ind_ajf                  ind_ajf,\n                ai.ind_cbf                  ind_cbf,\n                ai.ind_bt1                  ind_bt1,\n                ai.ind_bt2                  ind_bt2,\n                ai.ind_bt3                  ind_bt3\n        from    address_indicator_usages    ai\n        where   ai.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:866^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        setAllFieldData(rs);
      }
      
      rs.close();
      it.close();
      
      /*  replaced this query with one that doesn't strip out special characters
      #sql [Ctx] it =
      {
        select  lpad(asa.address_ind,2,'0')     name_addr_ind_,
                ltrim(upper(asa.address_name))  address_name_,
                ltrim(
                  replace(translate(upper(asa.address_line1),',./:#!@&''-', '^'),
                    '^', ''))                   address_line1_,
                ltrim(
                  replace(translate(upper(asa.address_line2),',./:#!@&''-', '^'),
                    '^', ''))                   address_line2_,
                ltrim(upper(asa.address_city))  address_city_,
                ltrim(upper(asa.address_state)) address_state_,
                rpad(replace(
                  translate(asa.address_zip,' -','X'), 
                  'X'),9,'0')                   address_zip_
        from    app_setup_address           asa
        where   asa.app_seq_num = :appSeqNum
      };
      */
      
      /*@lineinfo:generated-code*//*@lineinfo:899^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  lpad(asa.address_ind,2,'0')     name_addr_ind_,
//                  ltrim(upper(asa.address_name))  address_name_,
//                  ltrim(upper(asa.address_line1)) address_line1_,
//                  ltrim(upper(asa.address_line2)) address_line2_,
//                  ltrim(upper(asa.address_city))  address_city_,
//                  ltrim(upper(asa.address_state)) address_state_,
//                  rpad(replace(
//                    translate(asa.address_zip,' -','X'), 
//                    'X'),9,'0')                   address_zip_
//          from    app_setup_address           asa
//          where   asa.app_seq_num = :appSeqNum
//          order by asa.address_ind
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  lpad(asa.address_ind,2,'0')     name_addr_ind_,\n                ltrim(upper(asa.address_name))  address_name_,\n                ltrim(upper(asa.address_line1)) address_line1_,\n                ltrim(upper(asa.address_line2)) address_line2_,\n                ltrim(upper(asa.address_city))  address_city_,\n                ltrim(upper(asa.address_state)) address_state_,\n                rpad(replace(\n                  translate(asa.address_zip,' -','X'), \n                  'X'),9,'0')                   address_zip_\n        from    app_setup_address           asa\n        where   asa.app_seq_num =  :1 \n        order by asa.address_ind";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:913^7*/
      
      rs = it.getResultSet();
      
      int idx = 1;
      while(rs.next())
      {
        ResultSetMetaData   md = rs.getMetaData();
        
        // set data for columns (with chargeId appended to column name)
        for(int i=1; i <= md.getColumnCount(); ++i)
        {
          String columnName = md.getColumnName(i).toLowerCase();
          
          StringBuffer fieldName = new StringBuffer(columnName);
          fieldName.append(Integer.toString(idx));
          
          switch(findFieldByName(fieldName.toString()).getType())
          {
            case FlatFileRecordField.FIELD_TYPE_NUMERIC:
              setFieldData(fieldName.toString(), rs.getDouble(columnName));
              break;
              
            case FlatFileRecordField.FIELD_TYPE_DATE:
              setFieldData(fieldName.toString(), new java.sql.Date(rs.getTimestamp(columnName).getTime()));
              break;

            case FlatFileRecordField.FIELD_TYPE_ALPHA:
            default:
              setFieldData(fieldName.toString(), rs.getString(columnName));
              break;
          }
        }

        ++idx;
      }
    }
    catch(Exception e)
    {
      masLogEntry("buildDetailAddresses(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
    
  }
  
  
  /*
  ** buildDetailNonBankCard
  */
  protected void buildDetailNonBankCard(int appSeqNum)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:973^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(amexcard.cardtype_code,
//                    null, null,
//                    m.merch_amex_pcid_num)          merch_amex_pcid_num,
//                  decode(amexcard.cardtype_code,
//                    null, null,
//                    amex.merchpo_card_merch_number) amex_merchant_number,
//                  decode(amexcard.billcard_ach_option,
//                    null, null,
//                    'N', 'N',
//                    'Y')                            amex_settlement_flag,
//                  decode(disccard.cardtype_code,
//                    null, null,
//                    disc.merchpo_card_merch_number) discover_merchant_number,
//                  decode(dinrcard.cardtype_code,
//                    null, null,
//                    dinr.merchpo_card_merch_number) diners_merchant_number,
//                  decode(jcbcard.cardtype_code,
//                    null, null,
//                    jcb.merchpo_card_merch_number)  jcb_merchant_number
//          from    merchant    m,
//                  merchpayoption  amex,
//                  billcard        amexcard,
//                  merchpayoption  disc,
//                  billcard        disccard,
//                  merchpayoption  dinr,
//                  billcard        dinrcard,
//                  merchpayoption  jcb,
//                  billcard        jcbcard
//          where   m.app_seq_num         = :appSeqNum                and
//                  m.app_seq_num         = amex.app_seq_num(+)       and
//                  amex.cardtype_code(+) = 16                        and
//                  amex.app_seq_num      = amexcard.app_seq_num(+)   and
//                  amex.cardtype_code    = amexcard.cardtype_code(+) and
//                  m.app_seq_num         = disc.app_seq_num(+)       and
//                  disc.cardtype_code(+) = 14                        and
//                  disc.app_seq_num      = disccard.app_seq_num(+)   and
//                  disc.cardtype_code    = disccard.cardtype_code(+) and
//                  m.app_seq_num         = dinr.app_seq_num(+)       and
//                  dinr.cardtype_code(+) = 10                        and
//                  dinr.app_seq_num      = dinrcard.app_seq_num(+)   and
//                  dinr.cardtype_code    = dinrcard.cardtype_code(+) and
//                  m.app_seq_num         = jcb.app_seq_num(+)        and
//                  jcb.cardtype_code(+)  = 15                        and
//                  jcb.app_seq_num       = jcbcard.app_seq_num(+)    and
//                  jcb.cardtype_code     = jcbcard.cardtype_code(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(amexcard.cardtype_code,\n                  null, null,\n                  m.merch_amex_pcid_num)          merch_amex_pcid_num,\n                decode(amexcard.cardtype_code,\n                  null, null,\n                  amex.merchpo_card_merch_number) amex_merchant_number,\n                decode(amexcard.billcard_ach_option,\n                  null, null,\n                  'N', 'N',\n                  'Y')                            amex_settlement_flag,\n                decode(disccard.cardtype_code,\n                  null, null,\n                  disc.merchpo_card_merch_number) discover_merchant_number,\n                decode(dinrcard.cardtype_code,\n                  null, null,\n                  dinr.merchpo_card_merch_number) diners_merchant_number,\n                decode(jcbcard.cardtype_code,\n                  null, null,\n                  jcb.merchpo_card_merch_number)  jcb_merchant_number\n        from    merchant    m,\n                merchpayoption  amex,\n                billcard        amexcard,\n                merchpayoption  disc,\n                billcard        disccard,\n                merchpayoption  dinr,\n                billcard        dinrcard,\n                merchpayoption  jcb,\n                billcard        jcbcard\n        where   m.app_seq_num         =  :1                 and\n                m.app_seq_num         = amex.app_seq_num(+)       and\n                amex.cardtype_code(+) = 16                        and\n                amex.app_seq_num      = amexcard.app_seq_num(+)   and\n                amex.cardtype_code    = amexcard.cardtype_code(+) and\n                m.app_seq_num         = disc.app_seq_num(+)       and\n                disc.cardtype_code(+) = 14                        and\n                disc.app_seq_num      = disccard.app_seq_num(+)   and\n                disc.cardtype_code    = disccard.cardtype_code(+) and\n                m.app_seq_num         = dinr.app_seq_num(+)       and\n                dinr.cardtype_code(+) = 10                        and\n                dinr.app_seq_num      = dinrcard.app_seq_num(+)   and\n                dinr.cardtype_code    = dinrcard.cardtype_code(+) and\n                m.app_seq_num         = jcb.app_seq_num(+)        and\n                jcb.cardtype_code(+)  = 15                        and\n                jcb.app_seq_num       = jcbcard.app_seq_num(+)    and\n                jcb.cardtype_code     = jcbcard.cardtype_code(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1020^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        setAllFieldData(rs);
      }
    }
    catch(Exception e)
    {
      masLogEntry("buildDetailNonBankCard(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  /*
  ** buildDetailInterchangeBets
  */
  protected void buildDetailInterchangeBets(int appSeqNum)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1050^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bb.interchange_bet_visa       interchange_bet_visa,
//                  bb.interchange_bet_mc         interchange_bet_mc,
//                  bb.system_generated_bet       system_generated_bet,
//                  decode(bb.debit_net_bet,
//                    null, '0000',
//                    bb.debit_net_bet)           debit_net_bet,
//                  bb.data_capture_vendor1       dc_bet_1,
//                  bb.data_capture_bet1          dc_bet_num_1
//          from    billcard_bet      bb
//          where   bb.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bb.interchange_bet_visa       interchange_bet_visa,\n                bb.interchange_bet_mc         interchange_bet_mc,\n                bb.system_generated_bet       system_generated_bet,\n                decode(bb.debit_net_bet,\n                  null, '0000',\n                  bb.debit_net_bet)           debit_net_bet,\n                bb.data_capture_vendor1       dc_bet_1,\n                bb.data_capture_bet1          dc_bet_num_1\n        from    billcard_bet      bb\n        where   bb.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1062^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        setAllFieldData(rs);
      }
    }
    catch(Exception e)
    {
      masLogEntry("buildDetailInterchangeBets(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  /*
  ** buildDetailUserData
  */
  protected void buildDetailUserData(int appSeqNum)
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1092^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  upper(md.user_data_1)             user_data_1,
//                  upper(md.user_data_2)             user_data_2,
//                  upper(md.user_data_3)             user_data_3,
//                  upper(md.user_data_4)             user_data_4,
//                  upper(md.user_data_5)             user_data_5,
//                  rpad(md.user_account_1, 16, '0')  user_account_1,
//                  rpad(md.user_account_2, 16, '0')  user_account_2,
//                  md.dest_global                    dest_global,
//                  md.dest_deposits                  dest_deposits,
//                  md.dest_adjustments               dest_adjustments,
//                  md.dest_chargebacks               dest_chargebacks,
//                  md.dest_reversals                 dest_reversals,
//                  md.dest_chargeback_reversals      dest_chargeback_reversals,
//                  md.dest_dda_adjustments           dest_dda_adjustments,
//                  md.dest_batch_rc                  dest_batch_rc,
//                  md.dest_other_tran_type1          dest_other_tran_type1,
//                  md.dest_other_tran_type2          dest_other_tran_type2,
//                  md.dest_other_tran_type3          dest_other_tran_type3
//          from    merchant_data     md
//          where   md.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  upper(md.user_data_1)             user_data_1,\n                upper(md.user_data_2)             user_data_2,\n                upper(md.user_data_3)             user_data_3,\n                upper(md.user_data_4)             user_data_4,\n                upper(md.user_data_5)             user_data_5,\n                rpad(md.user_account_1, 16, '0')  user_account_1,\n                rpad(md.user_account_2, 16, '0')  user_account_2,\n                md.dest_global                    dest_global,\n                md.dest_deposits                  dest_deposits,\n                md.dest_adjustments               dest_adjustments,\n                md.dest_chargebacks               dest_chargebacks,\n                md.dest_reversals                 dest_reversals,\n                md.dest_chargeback_reversals      dest_chargeback_reversals,\n                md.dest_dda_adjustments           dest_dda_adjustments,\n                md.dest_batch_rc                  dest_batch_rc,\n                md.dest_other_tran_type1          dest_other_tran_type1,\n                md.dest_other_tran_type2          dest_other_tran_type2,\n                md.dest_other_tran_type3          dest_other_tran_type3\n        from    merchant_data     md\n        where   md.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1114^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        setAllFieldData(rs);
      }
      
      // set user_bank_number
      setFieldData("user_bank_number", getBankNumber());
    }
    catch(Exception e)
    {
      masLogEntry("buildDetailUserData(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  /*
  ** buildDetailMerchantData
  */
  protected void buildDetailMerchantData(int appSeqNum, int sequence)
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;
    
    try
    {
      // get basic merchant data
      /*@lineinfo:generated-code*//*@lineinfo:1148^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.merch_number                        merch_number,
//                  m.asso_number                         asso_number,
//                  m.sic_code                            sic_code,
//                  m.merch_mcc                           merch_mcc,
//                  m.merch_rep_code                      merch_rep_code,
//                  m.merch_dly_ach_flag                  merch_dly_ach_flag,
//                  m.merch_stat_hold_flag                merch_stat_hold_flag,
//                  m.merch_stat_print_flag               merch_stat_print_flag,
//                  nvl(m.merch_edc_flag,'Y')             merch_edc_flag,
//                  m.merch_paper_draft_flag              merch_paper_draft_flag,
//                  decode(m.bustype_code,
//                    '1','3',
//                    '2','1',
//                    '3','4',
//                    '4','1',
//                    '5','5',
//                    '6','6',
//                    '7','6',
//                    '8','8',
//                    '9','7',
//                    '3')
//                    || decode(m.merch_gender,'M','1','F','2','0')
//                    || '00'                             merch_gender_bustype_code, 
//                  m.merch_interchg_dollar_flag          merch_interchg_dollar_flag,
//                  m.merch_interchg_number_flag          merch_interchg_number_flag,
//                  decode(m.bustype_code,
//                    '2', '3',
//                    '3', '2',
//                    '6', '7',
//                    '5', '6',
//                    '8', '9',
//                    '9', '8',
//                    m.bustype_code)                     bustype_code,
//                  m.merch_vrs_flag                      merch_vrs_flag,
//                  decode(m.merch_dly_discount_flag,
//                    null, 'N',
//                    'Y',  'D',
//                    m.merch_dly_discount_flag)          merch_dly_discount_flag,
//                  decode(m.merch_dly_discount_flag,
//                    'I',  'S',
//                    'B',  'S',
//                    ' ')                                ach_daily_interchange,
//                  upper(m.merch_business_name)          merch_business_name,
//                  m.merch_met_table_number              merch_met_table_number,
//                  m.merch_invg_code                     merch_invg_code,
//                  m.merch_deposit_flag                  merch_deposit_flag,
//                  m.merch_dly_excpt_flag                merch_dly_excpt_flag,
//                  m.merch_fraud_flag                    merch_fraud_flag,
//                  m.merch_visa_cps_ind                  merch_visa_cps_ind,
//                  m.merch_visa_psirf_flag               merch_visa_psirf_flag,
//                  m.merch_visa_smkt_flag                merch_visa_smkt_flag,
//                  m.merch_visa_eps_flag                 merch_visa_eps_flag,
//                  m.merch_visa_eirf_flag                merch_visa_eirf_flag,
//                  m.merch_mc_merit_ind                  merch_mc_merit_ind,
//                  m.merch_mc_smkt_flag                  merch_mc_smkt_flag,
//                  m.merch_mc_premier_flag               merch_mc_premier_flag,
//                  m.merch_mc_petrocat_flag              merch_mc_petrocat_flag,
//                  m.merch_mc_warehouse_flag             merch_mc_warehouse_flag,
//                  m.merch_amex_pcid_num                 merch_amex_pcid_num,
//                  m.merch_min_disc_charge               merch_min_disc_charge,
//                  decode(m.merch_tax_disc_ind,
//                    null, 'N',
//                    m.merch_tax_disc_ind)               merch_tax_disc_ind,
//                  m.merch_federal_tax_id                merch_federal_tax_id,
//                  upper(bo1.busowner_first_name)||' '|| 
//                    upper(bo1.busowner_last_name)       busowner_name,
//                  upper(bo2.busowner_first_name)||' '|| 
//                    upper(bo2.busowner_last_name)       busowner2_name,
//                  nvl(dukpt_decrypt_wrapper(bo1.busowner_ssn_enc), bo1.busowner_ssn)  busowner_ssn,
//                  nvl(dukpt_decrypt_wrapper(bo2.busowner_ssn_enc), bo2.busowner_ssn)  busowner2_ssn,
//                  a1.address_phone                      address_phone,
//                  a1.address_fax                        address_fax,
//                  upper(a1.address_city)                address_city,
//                  upper(a1.countrystate_code)           countrystate_code,
//                  rpad(
//                    replace(translate(a1.address_zip,' -','X'),'X'),
//                    9,'0')                              address_zip,
//                  mcr.merch_debit_achpost_days          merch_debit_achpost_days,
//                  mcr.merch_credit_achpost_days         merch_credit_achpost_days,
//                  mcr.merch_days_suspend                merch_days_suspend,
//                  decode(m.customer_service_phone,
//                    0, decode(a1.address_phone, 0, null, a1.address_phone),
//                    null, decode(a1.address_phone, 0, null, a1.address_phone),
//                    m.customer_service_phone)           customer_service_phone,
//                  sysdate                               current_date
//          from    merchant          m,
//                  businessowner     bo1,
//                  businessowner     bo2,
//                  address           a1,
//                  merchcredit       mcr
//          where   m.app_seq_num = :appSeqNum and
//                  m.app_seq_num = bo1.app_seq_num(+) and
//                  bo1.busowner_num(+) = 1 and
//                  m.app_seq_num = bo2.app_seq_num(+) and
//                  bo2.busowner_num(+) = 2 and
//                  m.app_seq_num = a1.app_seq_num(+) and
//                  a1.addresstype_code(+) = 1 and
//                  m.app_seq_num = mcr.app_seq_num(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.merch_number                        merch_number,\n                m.asso_number                         asso_number,\n                m.sic_code                            sic_code,\n                m.merch_mcc                           merch_mcc,\n                m.merch_rep_code                      merch_rep_code,\n                m.merch_dly_ach_flag                  merch_dly_ach_flag,\n                m.merch_stat_hold_flag                merch_stat_hold_flag,\n                m.merch_stat_print_flag               merch_stat_print_flag,\n                nvl(m.merch_edc_flag,'Y')             merch_edc_flag,\n                m.merch_paper_draft_flag              merch_paper_draft_flag,\n                decode(m.bustype_code,\n                  '1','3',\n                  '2','1',\n                  '3','4',\n                  '4','1',\n                  '5','5',\n                  '6','6',\n                  '7','6',\n                  '8','8',\n                  '9','7',\n                  '3')\n                  || decode(m.merch_gender,'M','1','F','2','0')\n                  || '00'                             merch_gender_bustype_code, \n                m.merch_interchg_dollar_flag          merch_interchg_dollar_flag,\n                m.merch_interchg_number_flag          merch_interchg_number_flag,\n                decode(m.bustype_code,\n                  '2', '3',\n                  '3', '2',\n                  '6', '7',\n                  '5', '6',\n                  '8', '9',\n                  '9', '8',\n                  m.bustype_code)                     bustype_code,\n                m.merch_vrs_flag                      merch_vrs_flag,\n                decode(m.merch_dly_discount_flag,\n                  null, 'N',\n                  'Y',  'D',\n                  m.merch_dly_discount_flag)          merch_dly_discount_flag,\n                decode(m.merch_dly_discount_flag,\n                  'I',  'S',\n                  'B',  'S',\n                  ' ')                                ach_daily_interchange,\n                upper(m.merch_business_name)          merch_business_name,\n                m.merch_met_table_number              merch_met_table_number,\n                m.merch_invg_code                     merch_invg_code,\n                m.merch_deposit_flag                  merch_deposit_flag,\n                m.merch_dly_excpt_flag                merch_dly_excpt_flag,\n                m.merch_fraud_flag                    merch_fraud_flag,\n                m.merch_visa_cps_ind                  merch_visa_cps_ind,\n                m.merch_visa_psirf_flag               merch_visa_psirf_flag,\n                m.merch_visa_smkt_flag                merch_visa_smkt_flag,\n                m.merch_visa_eps_flag                 merch_visa_eps_flag,\n                m.merch_visa_eirf_flag                merch_visa_eirf_flag,\n                m.merch_mc_merit_ind                  merch_mc_merit_ind,\n                m.merch_mc_smkt_flag                  merch_mc_smkt_flag,\n                m.merch_mc_premier_flag               merch_mc_premier_flag,\n                m.merch_mc_petrocat_flag              merch_mc_petrocat_flag,\n                m.merch_mc_warehouse_flag             merch_mc_warehouse_flag,\n                m.merch_amex_pcid_num                 merch_amex_pcid_num,\n                m.merch_min_disc_charge               merch_min_disc_charge,\n                decode(m.merch_tax_disc_ind,\n                  null, 'N',\n                  m.merch_tax_disc_ind)               merch_tax_disc_ind,\n                m.merch_federal_tax_id                merch_federal_tax_id,\n                upper(bo1.busowner_first_name)||' '|| \n                  upper(bo1.busowner_last_name)       busowner_name,\n                upper(bo2.busowner_first_name)||' '|| \n                  upper(bo2.busowner_last_name)       busowner2_name,\n                nvl(dukpt_decrypt_wrapper(bo1.busowner_ssn_enc), bo1.busowner_ssn)  busowner_ssn,\n                nvl(dukpt_decrypt_wrapper(bo2.busowner_ssn_enc), bo2.busowner_ssn)  busowner2_ssn,\n                a1.address_phone                      address_phone,\n                a1.address_fax                        address_fax,\n                upper(a1.address_city)                address_city,\n                upper(a1.countrystate_code)           countrystate_code,\n                rpad(\n                  replace(translate(a1.address_zip,' -','X'),'X'),\n                  9,'0')                              address_zip,\n                mcr.merch_debit_achpost_days          merch_debit_achpost_days,\n                mcr.merch_credit_achpost_days         merch_credit_achpost_days,\n                mcr.merch_days_suspend                merch_days_suspend,\n                decode(m.customer_service_phone,\n                  0, decode(a1.address_phone, 0, null, a1.address_phone),\n                  null, decode(a1.address_phone, 0, null, a1.address_phone),\n                  m.customer_service_phone)           customer_service_phone,\n                sysdate                               current_date\n        from    merchant          m,\n                businessowner     bo1,\n                businessowner     bo2,\n                address           a1,\n                merchcredit       mcr\n        where   m.app_seq_num =  :1  and\n                m.app_seq_num = bo1.app_seq_num(+) and\n                bo1.busowner_num(+) = 1 and\n                m.app_seq_num = bo2.app_seq_num(+) and\n                bo2.busowner_num(+) = 2 and\n                m.app_seq_num = a1.app_seq_num(+) and\n                a1.addresstype_code(+) = 1 and\n                m.app_seq_num = mcr.app_seq_num(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1248^7*/
      
      rs = it.getResultSet();
      
      // set all one-to-one merchant fields
      if(rs.next())
      {
        setAllFieldData(rs);
      }
      
      rs.close();
      it.close();
      
      // get merchcontact_name
      /*@lineinfo:generated-code*//*@lineinfo:1262^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  upper(merchcont_prim_first_name)  merchcont_prim_first_name,
//                  upper(merchcont_prim_last_name)   merchcont_prim_last_name
//          from    merchcontact
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  upper(merchcont_prim_first_name)  merchcont_prim_first_name,\n                upper(merchcont_prim_last_name)   merchcont_prim_last_name\n        from    merchcontact\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.flatfile.MASTape",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.flatfile.MASTape",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1268^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        setFieldData("merchcontact_name", 
          getOwnerName( rs.getString("merchcont_prim_first_name"),
                        rs.getString("merchcont_prim_last_name")));
      }
      
      // set some manual fields
      setFieldData("merch_bank_number", getBankNumber());
      setFieldData("batch_number", getBatchNumber());
      setFieldData("record_sequence", sequence);
    }
    catch(Exception e)
    {
      masLogEntry("buildDetailMerchantData(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  protected String getOwnerName(String firstName, String lastName)
  {
    String result = "";
    
    try
    {
      firstName = firstName == null ? "" : firstName;
      lastName  = lastName == null  ? "" : lastName;

      if(firstName.length() + lastName.length() < 25)
      {
        result = firstName + " " + lastName;
      }
      else
      {
        int i = 25 - lastName.length();
        if(i > 0)
        {
          result = firstName.substring(0,i-1) + " " + lastName;
        }
        else if(i == 0)
        {
          result = lastName;
        }
        else if(i < 0)
        {
          result = lastName.substring(0,25);
        }
      }        
    }
    catch(Exception e)
    {
      masLogEntry("getOwnerName(" + firstName + ", " + lastName + ")", e.toString());
    }
    
    return result;
  }
  
  public static void main(String[] args)
  {
    try
    {
      SQLJConnectionBase.initStandalone();
      int appSeqNum = Integer.parseInt(args[0]);
      
      Vector testApp = new Vector();
      testApp.add(appSeqNum);
      
      MASTape mt = null;
      if(args.length > 1)
      {
        mt = (MASTape)Class.forName(args[1]).newInstance();
      }
      else
      {
        mt = new MASTape();
      }
      
      mt.buildMASTapeApps(testApp);
      
      System.out.println(mt.getMasFile());
    }
    catch(Exception e)
    {
      System.out.println("MASTape::main(): " + e.toString());
    }
  }
  
  public class CardPlan
  {
    public String   discType;
    public String   discMethod;
    public int      drtNumber;
    public double   discRate;
    public double   perItem;
    public int      flrLimit;
    public String   achOption;
    public String   authOption;
    public String   indPlanBet;
    public String   brgNumber;
    
    public CardPlan(ResultSet rs)
    {
      try
      {
        discType      = rs.getString("billcard_disc_type");
        discMethod    = rs.getString("billcard_disc_method");
        drtNumber     = rs.getInt("billcard_drt_number");
        discRate      = rs.getDouble("billcard_disc_rate");
        perItem       = rs.getDouble("billcard_peritem_fee");
        flrLimit      = rs.getInt("billcard_flr_limit");
        achOption     = rs.getString("billcard_ach_option");
        authOption    = rs.getString("billcard_auth_option");
        indPlanBet    = rs.getString("billcard_bet_number");
        brgNumber     = rs.getString("billcard_brg");
      }
      catch(Exception e)
      {
        logEntry("CardPlan()::constructor", e.toString());
      }
    }
        
  }
}/*@lineinfo:generated-code*/