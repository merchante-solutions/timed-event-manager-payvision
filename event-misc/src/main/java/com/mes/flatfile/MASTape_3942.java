/*@lineinfo:filename=MASTape_3942*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/flatfile/MASTape_CBT.sqlj $

  Description:

    MASTape_CBT

    Extension of MASTape specifically for CB&T.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.flatfile;

import com.mes.constants.mesConstants;

public class MASTape_3942 extends MASTape
{
  public MASTape_3942()
  {
    super();
  }
  
  public int getBankNumber()
  {
    return mesConstants.BANK_ID_STERLING;
  }
}/*@lineinfo:generated-code*/