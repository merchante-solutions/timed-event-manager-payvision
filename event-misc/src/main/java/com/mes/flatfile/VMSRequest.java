/*@lineinfo:filename=VMSRequest*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/flatfile/VMSRequest.sqlj $

  Description:

    VMSRequest

    Extension of FlatFileRecord specifically for constructing MAS tape
    files.  The base version (this file) is a complete implementation
    of the MES-style MAS file.  Extensions to this file should be used
    for clients that differ from how MES constructs the tape file.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/26/04 1:53p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.flatfile;

import sqlj.runtime.ref.DefaultContext;

public class VMSRequest extends FlatFileRecord
{
  private int             procSequence    = 0;
  private StringBuffer    file            = new StringBuffer("");
  
  public VMSRequest(DefaultContext newCtx)
  {
    if(newCtx != null)
    {
      this.Ctx = newCtx;
    }
  }
  
  /*
  **  String buildVMSRequest()
  **
  */
  public String buildVMSRequest()
  {
    try
    {
      // get process sequence
      /*@lineinfo:generated-code*//*@lineinfo:62^7*/

//  ************************************************************
//  #sql [Ctx] { select  mas_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mas_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.flatfile.VMSRequest",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procSequence = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:67^7*/
      
    }
    catch(Exception e)
    {
      logEntry("buildVMSRequest()", e.toString());
    }
    
    return file.toString();
  }
}/*@lineinfo:generated-code*/