/*@lineinfo:filename=MASTape_BBT*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/flatfile/MASTape_BBT.sqlj $

  Description:

    MASTape_CBT

    Extension of MASTape specifically for CB&T.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/21/04 2:29p $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.flatfile;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class MASTape_BBT extends MASTape
{
  public MASTape_BBT()
  {
    super();
  }
  
  public int getBankNumber()
  {
    return mesConstants.BANK_ID_BBT;
  }
  
  protected void buildDetailClientSpecific(int appSeqNum)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    // all CB&T accounts have special values in the first data capture bet slot
    try
    {

      //take the contact name and put it into the managers name field, which in the base class we 
      //use to put the second business owners name into the managers name field
      setFieldData("busowner2_name", getFieldRawData("merchcontact_name"));

      //we set assistant managers name field to blank... we use to put contact name here.. but for bbt we leave it blank
      setFieldData("merchcontact_name", "");

      //they want this blank. this maps to drivers license number field# 15
      setFieldData("busowner2_ssn", "");

      //investigator code is always 0000 for bbt
      setFieldData("merch_invg_code", "0000");

      String taxTypeId      = null;
      String seasonalMonths = null;
      int    businessNature = -1;
      int    posType        = -1;
      int    productType    = -1;
      String visaIncStatus  = "";

      /*@lineinfo:generated-code*//*@lineinfo:76^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.tax_type_id,
//                  m.merch_visainc_status_ind,
//                  decode(m.seasonal_months, null, 'YYYYYYYYYYYY', 'NNNNNNNNNNNN', 'YYYYYYYYYYYY', m.seasonal_months) seasonal_months,
//                  decode(m.business_nature, null, -1, m.business_nature) business_nature
//          from    merchant  m
//          where   m.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.tax_type_id,\n                m.merch_visainc_status_ind,\n                decode(m.seasonal_months, null, 'YYYYYYYYYYYY', 'NNNNNNNNNNNN', 'YYYYYYYYYYYY', m.seasonal_months) seasonal_months,\n                decode(m.business_nature, null, -1, m.business_nature) business_nature\n        from    merchant  m\n        where   m.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.flatfile.MASTape_BBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.flatfile.MASTape_BBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^7*/
      
      rs = it.getResultSet();
      if(rs.next())
      {
        taxTypeId      =  rs.getString("tax_type_id");
        seasonalMonths =  rs.getString("seasonal_months");
        businessNature =  rs.getInt("business_nature");
        visaIncStatus  =  rs.getString("merch_visainc_status_ind");
      }
      
      rs.close();
      it.close();

      if(taxTypeId != null)
      {
        setFieldData("merch_gender_bustype_code", taxTypeId);
      }

      if(visaIncStatus != null)
      {
        setFieldData("bustype_code", visaIncStatus);
      }

      /*@lineinfo:generated-code*//*@lineinfo:108^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(mp.pos_code, null, -1, mp.pos_code) pos_type,
//                  decode(pc.pos_type, null, -1, pc.pos_type) product_type
//          from    merch_pos     mp,
//                  pos_category  pc
//          where   mp.app_seq_num  = :appSeqNum and
//                  pc.pos_code     = mp.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(mp.pos_code, null, -1, mp.pos_code) pos_type,\n                decode(pc.pos_type, null, -1, pc.pos_type) product_type\n        from    merch_pos     mp,\n                pos_category  pc\n        where   mp.app_seq_num  =  :1  and\n                pc.pos_code     = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.flatfile.MASTape_BBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.flatfile.MASTape_BBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:116^7*/
      
      rs = it.getResultSet();
      if(rs.next())
      {
        posType        = rs.getInt("pos_type");
        productType    = rs.getInt("product_type");
      }
      
      rs.close();
      it.close();

      //we put the address phone number in the dba city field if its not dialpay or touch tone cature 
      //and business nature is keytail, moto, or internet
      if(productType !=4 && productType != 13)  //not dial pay accounts and not touch tone capture accounts
      {
        if(businessNature == 4 || businessNature == 5 || businessNature == 6 || businessNature == 7) //if gov, keytail, moto, or internet
        {
          String phone = getFieldRawData("address_phone");
          if(phone.length() == 10)
          {
            setFieldData("address_city", (phone.substring(0,3) + "-" + phone.substring(3)));
          }
        }
      }

      boolean inquiryOnly = false;

      /*@lineinfo:generated-code*//*@lineinfo:144^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(internet_inquiry_only, null, 'N', internet_inquiry_only) internet_inquiry_only
//          from    app_merch_bbt
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(internet_inquiry_only, null, 'N', internet_inquiry_only) internet_inquiry_only\n        from    app_merch_bbt\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.flatfile.MASTape_BBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.flatfile.MASTape_BBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:149^7*/
      
      rs = it.getResultSet();
      if(rs.next())
      {
        inquiryOnly = rs.getString("internet_inquiry_only").toUpperCase().equals("Y");  
      }

      rs.close();
      it.close();

      //if inquiry only is true then we make it all 2's
      //if not inquiry only and business nature is internet then we make business license no all 1's
      if(inquiryOnly)
      {
        setFieldData("business_license_no", "2222222222");
      }
      else if(businessNature == 7)
      {
        setFieldData("business_license_no", "1111111111");
      }


      // get bank account type.. if savings put a 'B' in user flag 1 field
      /*@lineinfo:generated-code*//*@lineinfo:173^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(mb.bankacc_type, 1, 'B', '')  user_flag_1
//          from    merchbank mb
//          where   mb.app_seq_num = :appSeqNum and
//                  mb.merchbank_acct_srnum = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(mb.bankacc_type, 1, 'B', '')  user_flag_1\n        from    merchbank mb\n        where   mb.app_seq_num =  :1  and\n                mb.merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.flatfile.MASTape_BBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.flatfile.MASTape_BBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:179^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        setAllFieldData(rs);
      }
      
      rs.close();
      it.close();

      // set last credit check to today date and date of next call to next months date
      /*@lineinfo:generated-code*//*@lineinfo:192^7*/

//  ************************************************************
//  #sql [Ctx] it = { select sysdate                            date_last_credit_check,
//                 add_months(sysdate,1)              date_next_call
//          from   dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select sysdate                            date_last_credit_check,\n               add_months(sysdate,1)              date_next_call\n        from   dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.flatfile.MASTape_BBT",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.flatfile.MASTape_BBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        setAllFieldData(rs);
      }
      
      rs.close();
      it.close();

      //get FNS From merchpayoption ebt field.. if available and put into fns_number
      /*@lineinfo:generated-code*//*@lineinfo:210^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    MERCHPO_CARD_MERCH_NUMBER   fns_number
//          from      MERCHPAYOPTION 
//          where     app_seq_num   = :appSeqNum  and
//                    CARDTYPE_CODE = :mesConstants.APP_CT_EBT
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    MERCHPO_CARD_MERCH_NUMBER   fns_number\n        from      MERCHPAYOPTION \n        where     app_seq_num   =  :1   and\n                  CARDTYPE_CODE =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.flatfile.MASTape_BBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_EBT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.flatfile.MASTape_BBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:216^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        setAllFieldData(rs);
      }
      
      rs.close();
      it.close();

      //because the base file right pads these two fields and bbt needs them left padded, we overwrite them
      /*@lineinfo:generated-code*//*@lineinfo:229^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  md.user_account_1  user_account_1,
//                  md.user_account_2  user_account_2
//          from    merchant_data     md
//          where   md.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  md.user_account_1  user_account_1,\n                md.user_account_2  user_account_2\n        from    merchant_data     md\n        where   md.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.flatfile.MASTape_BBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.flatfile.MASTape_BBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:235^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        setAllFieldData(rs);
      }

      rs.close();
      it.close();

      // set discount record
      //if table has two different dda/tr numbers the second one gets put into the discount charge record,
      //else only the first one gets put into the discount charge record
      /*@lineinfo:generated-code*//*@lineinfo:250^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    mb.merchbank_acct_num                   charge_dda_1,
//                    mb.merchbank_transit_route_num          charge_transit_route_1,
//                    decode(mb.billing_method, null,  'D', 
//                                            'Debit', 'D', 
//                                            'Remit', 'R')   charge_billing_method_1,
//                    :seasonalMonths                         charge_billing_frequency_1,
//                    '9999'                                  charge_bank_officer_1,
//                    trunc(add_months(sysdate,1), 'MM')      charge_start_date_1
//          from      merchbank mb
//          where     mb.app_seq_num = :appSeqNum  
//          order by  mb.merchbank_acct_srnum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mb.merchbank_acct_num                   charge_dda_1,\n                  mb.merchbank_transit_route_num          charge_transit_route_1,\n                  decode(mb.billing_method, null,  'D', \n                                          'Debit', 'D', \n                                          'Remit', 'R')   charge_billing_method_1,\n                   :1                          charge_billing_frequency_1,\n                  '9999'                                  charge_bank_officer_1,\n                  trunc(add_months(sysdate,1), 'MM')      charge_start_date_1\n        from      merchbank mb\n        where     mb.app_seq_num =  :2   \n        order by  mb.merchbank_acct_srnum";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.flatfile.MASTape_BBT",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,seasonalMonths);
   __sJT_st.setInt(2,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.flatfile.MASTape_BBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:263^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        setAllFieldData(rs);
      }
      
      rs.close();
      it.close();


      //fix other charge records
      /*@lineinfo:generated-code*//*@lineinfo:277^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bo.billoth_charge_type                                billoth_charge_type_,
//                  bo.billoth_charge_method                              billoth_charge_method_,
//                  bo.billoth_charge_frq                                 billoth_charge_frq_,
//                  to_date(bo.billoth_charge_startdate,'MM/dd/yy')       billoth_charge_startdate_,
//                  decode(bo.billoth_charge_expiredate,
//                    null, '999999',
//                    '999999', '999999',
//                    to_char(
//                      to_date(bo.billoth_charge_expiredate, 'MM/dd/yy'),
//                      'MMddyy'))                                        billoth_charge_expiredate_,
//                  bo.billoth_charge_amt                                 billoth_charge_amt_,
//                  bo.billoth_charge_type                                billoth_charge_type_,
//                  bo.billoth_charge_times                               billoth_charge_times_,
//                  bo.billoth_charge_tax_ind                             billoth_charge_tax_ind_,
//                  upper(bo.billoth_charge_msg)                          billoth_charge_msg_,
//                  bo.billoth_charge_acct_num                            billoth_charge_dda_,
//                  bo.billoth_charge_trans_rte_num                       billoth_charge_transit_route_,
//                  '9999'                                                charge_bank_officer_
//          from    billother     bo
//          where   bo.app_seq_num = :appSeqNum
//          order by bo.billoth_sr_num asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bo.billoth_charge_type                                billoth_charge_type_,\n                bo.billoth_charge_method                              billoth_charge_method_,\n                bo.billoth_charge_frq                                 billoth_charge_frq_,\n                to_date(bo.billoth_charge_startdate,'MM/dd/yy')       billoth_charge_startdate_,\n                decode(bo.billoth_charge_expiredate,\n                  null, '999999',\n                  '999999', '999999',\n                  to_char(\n                    to_date(bo.billoth_charge_expiredate, 'MM/dd/yy'),\n                    'MMddyy'))                                        billoth_charge_expiredate_,\n                bo.billoth_charge_amt                                 billoth_charge_amt_,\n                bo.billoth_charge_type                                billoth_charge_type_,\n                bo.billoth_charge_times                               billoth_charge_times_,\n                bo.billoth_charge_tax_ind                             billoth_charge_tax_ind_,\n                upper(bo.billoth_charge_msg)                          billoth_charge_msg_,\n                bo.billoth_charge_acct_num                            billoth_charge_dda_,\n                bo.billoth_charge_trans_rte_num                       billoth_charge_transit_route_,\n                '9999'                                                charge_bank_officer_\n        from    billother     bo\n        where   bo.app_seq_num =  :1 \n        order by bo.billoth_sr_num asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.flatfile.MASTape_BBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.flatfile.MASTape_BBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:300^7*/
      
      rs = it.getResultSet();
      
      int idx=2;    // start with 2 because the first record is defaulted
      
      while(rs.next())
      {
        if(idx > 10)
        {
          break;
        }
        
        ResultSetMetaData   md = rs.getMetaData();
        
        // set data for columns (with chargeId appended to column name)
        for(int i=1; i <= md.getColumnCount(); ++i)
        {
          String columnName = md.getColumnName(i).toLowerCase();
          
          StringBuffer fieldName = new StringBuffer(columnName);
          fieldName.append(Integer.toString(idx));
          
          switch(findFieldByName(fieldName.toString()).getType())
          {
            case FlatFileRecordField.FIELD_TYPE_NUMERIC:
              setFieldData(fieldName.toString(), rs.getDouble(columnName));
              break;
              
            case FlatFileRecordField.FIELD_TYPE_DATE:
              setFieldData(fieldName.toString(), new java.sql.Date(rs.getTimestamp(columnName).getTime()));
              break;

            case FlatFileRecordField.FIELD_TYPE_ALPHA:
            default:
              setFieldData(fieldName.toString(), rs.getString(columnName));
              break;
          }
        }

        ++idx;
      }
    }
    catch(Exception e)
    {
      masLogEntry("buildDetailClientSpecific(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
}/*@lineinfo:generated-code*/