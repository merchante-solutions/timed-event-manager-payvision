package com.mes.events;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.config.DbProperties;
import com.mes.database.ConnectionBase;
import com.mes.support.LoggingConfigurator;
import com.mes.support.PropertiesFile;

public abstract class AbstractEvent extends ConnectionBase implements Event {

  static { LoggingConfigurator.requireConfiguration(); } // initialize log4j
  static Logger log = Logger.getLogger(AbstractEvent.class);
  
  static { DbProperties.requireConfiguration(); } // initialize database properties

  protected List argList;
  protected PropertiesFile properties;

  public void setEventArgs(String argStr) {
    argList = new ArrayList();
    String[] argsArray = argStr.split(",");
    for (int i = 0; i < argsArray.length; ++i) {
      argList.add(argsArray[i].trim());
    }
  }
  
  public void setEventProperties(PropertiesFile properties) {
    this.properties = properties;
  }
  
  public abstract boolean execute();
}