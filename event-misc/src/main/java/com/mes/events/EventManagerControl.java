/*@lineinfo:filename=EventManagerControl*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/events/EventManagerControl.sqlj $

  Description:
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-07-29 13:51:34 -0700 (Wed, 29 Jul 2015) $
  Version            : $Revision: 23755 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.events;

import java.net.InetAddress;
import java.net.Socket;
import java.util.Vector;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.HttpHelper;

public class EventManagerControl extends SQLJConnectionBase
{
  public Vector   events    = null;
  
  private Socket  socket    = null; 
  
  private String  managerAddr = "";
  private int     managerPort = 0;
  
  public EventManagerControl(String addr, int port)
  {
    managerAddr = addr;
    managerPort = port;
  }
  
  public void getEventStatus()
  {
    try
    {
      // contact the server to get the events and their statuses
      if(getServerConnection(managerAddr, managerPort))
      {      
        EventMessage em = new EventMessage.StatusEventMessage();
      
        em.send(socket);
      
        // get Vector of events back from server
        events = (Vector)(new EventMessage(socket).data);
      }
    }
    catch(Exception e)
    {
      logEntry("getEventStatus()", e.toString());
    }
    finally
    {
      closeServerConnection();
    }
  }
  
  public String getEventManagerDisplayStatus()
  {
    String      retVal      = "Unknown";
    
    try
    {
      // contact the server to tell it to shut down
      if( getServerConnection(managerAddr, managerPort) )
      {    
        // send enable/disable message
        EventMessage em = new EventMessage.ShutdownStatusMessage();
        em.send( socket );
      
        // get the response
        em = new EventMessage(socket);
        
        // data will hold a Boolean
        if ( ((Boolean)em.data).booleanValue() == true )
        {
          // server is shutting down
          retVal = "Shutting Down";
        }
        else
        {
          retVal = "Running";
        }
      }
      else
      {
        retVal = "Not Running";
      }
    }
    catch(Exception e)
    {
      retVal = ("ERROR: " + e.toString());
      logEntry("isEventManagerRunning()", e.toString());
    }
    finally
    {
      closeServerConnection();
    }
    return( retVal );
  }
  
  public long getEventManagerUpTime()
  {
    long        retVal    = -1L;
    
    try
    {
      // contact the server to tell it to shut down
      if( getServerConnection(managerAddr, managerPort) )
      {    
        // send enable/disable message
        EventMessage em = new EventMessage( EventMessage.ACTION_UP_TIME, null );
        em.send( socket );
      
        // get the response
        em = new EventMessage(socket);
        
        // data will hold a Long
        retVal = ((Long)em.data).longValue();
      }
    }
    catch(Exception e)
    {
      logEntry("isEventManagerRunning()", e.toString());
    }
    finally
    {
      closeServerConnection();
    }
    return( retVal );
  }
  
  public void setEventEnableFlag( int eventId, boolean enabled )
  {
    try
    {
      // contact the server to tell it to shut down
      if( getServerConnection(managerAddr, managerPort) )
      {    
        // send enable/disable message
        EventMessage em = new EventMessage.EventEnableMessage(eventId,enabled);
      
        em.send( socket );
      
        // get the ack
        em = new EventMessage(socket);
      }
    }
    catch(Exception e)
    {
      logEntry("setEventEnableFlag()", e.toString());
    }
    finally
    {
      closeServerConnection();
    }
  }
  
  public void setShutdown()
  {
    try
    {
      // contact the server to tell it to shut down
      if(getServerConnection(managerAddr, managerPort))
      {    
        // send shutdown action message
      
        EventMessage  em = new EventMessage.ShutdownEventMessage();
      
        em.send(socket);
      
        // get the ack
        em = new EventMessage(socket);
      }
    }
    catch(Exception e)
    {
      logEntry("setShutdown()", e.toString());
    }
    finally
    {
      closeServerConnection();
    }
  }
  
  public void setRestart()
  {
    try
    {
      if(getServerConnection(managerAddr, managerPort))
      {
        // send restart message
        EventMessage  em = new EventMessage.RestartEventMessage();
      
        em.send(socket);
      
        // get ack
        em = new EventMessage(socket);
      }
    }
    catch(Exception e)
    {
      logEntry("setRestart()", e.toString());
    }
    finally
    {
      closeServerConnection();
    }
  }
  
  private void closeServerConnection()
  {
    try
    {
      if(socket != null)
      {
        socket.close();
      }
    }
    catch(Exception e)
    {
      logEntry("closeServerConnection()", e.toString());
    }
  }
  
  private boolean getServerConnection(String managerAddr, int port)
  {
    boolean result = false;
    
    try
    {
      String  serverIp    = managerAddr;
      int     serverPort  = port == 0 ? MesDefaults.getInt(MesDefaults.DK_EVENT_SERVER_LISTEN_PORT) : port;
      
      // get the address from the system 
      connect();
      
      if( ("").equals(serverIp) || serverIp == null )
      {
        /*@lineinfo:generated-code*//*@lineinfo:261^9*/

//  ************************************************************
//  #sql [Ctx] { select  server_address,
//                    nvl(server_port,:serverPort)
//            
//            from    timed_event_config
//            where   (
//                      :HttpHelper.isDevServer(null) ? "Y" : "N" = 'N' and
//                      dev_name is null
//                    ) or
//                    (
//                      :HttpHelper.isDevServer(null) ? "Y" : "N" = 'Y' and
//                      dev_name = :getHostName()
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3634 = HttpHelper.isDevServer(null) ? "Y" : "N";
 String __sJT_3635 = HttpHelper.isDevServer(null) ? "Y" : "N";
 String __sJT_3636 = getHostName();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  server_address,\n                  nvl(server_port, :1 )\n           \n          from    timed_event_config\n          where   (\n                     :2  = 'N' and\n                    dev_name is null\n                  ) or\n                  (\n                     :3  = 'Y' and\n                    dev_name =  :4 \n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.events.EventManagerControl",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,serverPort);
   __sJT_st.setString(2,__sJT_3634);
   __sJT_st.setString(3,__sJT_3635);
   __sJT_st.setString(4,__sJT_3636);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   serverIp = (String)__sJT_rs.getString(1);
   serverPort = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:275^9*/
      }
      
      socket = new Socket(InetAddress.getByName(serverIp), serverPort);
      result = true;
    }
    catch(Exception e)
    {
      logEntry("getServerConnection()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public static void main(String[] args)
  {
    try
    {
      com.mes.database.SQLJConnectionBase.initStandalone("DEBUG");
    
      EventManagerControl eventController = new EventManagerControl(args[0], Integer.parseInt(args[1]));
    
      System.out.println("shutting down...");
      eventController.setShutdown();
      System.out.println("finished shutdown command");
    }
    catch(Exception e)
    {
    }
  }
}/*@lineinfo:generated-code*/