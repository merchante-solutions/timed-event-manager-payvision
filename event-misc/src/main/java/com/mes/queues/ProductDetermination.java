/*@lineinfo:filename=ProductDetermination*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ClientReviewQueue.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-05-15 11:45:51 -0700 (Thu, 15 May 2008) $
  Version            : $Revision: 14856 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.forms.ComboDateField;
import com.mes.forms.Field;
import com.mes.forms.RadioButtonField;
import com.mes.forms.TextareaField;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;

public class ProductDetermination extends QueueBase
{
  public boolean dateRangeEnabled()
  {
    // disable date range for now
    return true;
  }
  
  protected void loadQueueData(UserBean user)
  {
    try
    {
      java.sql.Date fromDate = 
        ((ComboDateField)fields.getField("fromDate")).getSqlDate();
      java.sql.Date toDate = 
        ((ComboDateField)fields.getField("toDate")).getSqlDate();
        
      /*@lineinfo:generated-code*//*@lineinfo:54^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    qd.id                 id,
//                    qd.type               type,
//                    qd.item_type          item_type,
//                    qd.owner              owner,
//                    qd.date_created       date_created,
//                    qd.source             source,
//                    qd.affiliate          affiliate,
//                    qd.last_changed       last_changed,
//                    qd.last_user          last_user,
//                    m.merch_number        merchant_number,
//                    m.merch_business_name description,
//                    u.name                rep_name,
//                    qd.id                 control,
//                    qd.locked_by          locked_by,
//                    nvl(qn.note_count,0)  note_count,
//                    qt.status             status
//          from      q_data                qd,
//                    q_types               qt,
//                    ( select    qn.id,
//                                count(qn.id)  note_count
//                      from      q_notes qn,
//                                q_data qd
//                      where     qd.type = :this.type and
//                                qd.id = qn.id(+)
//                      group by qn.id )    qn,
//                    merchant              m,
//                    application           app,
//                    users                 u
//          where     qd.type = :this.type
//                    and qd.type = qt.type
//                    and qd.id = qn.id(+)
//                    and qd.id = m.app_seq_num
//                    and qd.id = app.app_seq_num
//                    and app.app_user_login = u.login_name
//          order by  qd.date_created asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    qd.id                 id,\n                  qd.type               type,\n                  qd.item_type          item_type,\n                  qd.owner              owner,\n                  qd.date_created       date_created,\n                  qd.source             source,\n                  qd.affiliate          affiliate,\n                  qd.last_changed       last_changed,\n                  qd.last_user          last_user,\n                  m.merch_number        merchant_number,\n                  m.merch_business_name description,\n                  u.name                rep_name,\n                  qd.id                 control,\n                  qd.locked_by          locked_by,\n                  nvl(qn.note_count,0)  note_count,\n                  qt.status             status\n        from      q_data                qd,\n                  q_types               qt,\n                  ( select    qn.id,\n                              count(qn.id)  note_count\n                    from      q_notes qn,\n                              q_data qd\n                    where     qd.type =  :1  and\n                              qd.id = qn.id(+)\n                    group by qn.id )    qn,\n                  merchant              m,\n                  application           app,\n                  users                 u\n        where     qd.type =  :2 \n                  and qd.type = qt.type\n                  and qd.id = qn.id(+)\n                  and qd.id = m.app_seq_num\n                  and qd.id = app.app_seq_num\n                  and app.app_user_login = u.login_name\n        order by  qd.date_created asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.ProductDetermination",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setInt(2,this.type);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.queues.ProductDetermination",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:91^7*/
    }
    catch(Exception e)
    {
      logEntry("getQueueData(" + user.getLoginName() + ")", e.toString());
    }
  }
  
  protected void loadQueueItem(long id)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:103^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    qd.id                 id,
//                    qd.type               type,
//                    qd.item_type          item_type,
//                    qd.owner              owner,
//                    qd.date_created       date_created,
//                    qd.source             source,
//                    qd.affiliate          affiliate,
//                    qd.last_changed       last_changed,
//                    qd.last_user          last_user,
//                    m.merch_number        merchant_number,
//                    m.merch_business_name description,
//                    u.name                rep_name,
//                    qd.id                 control,
//                    qd.locked_by          locked_by,
//                    nvl(qn.note_count,0)  note_count,
//                    qt.status             status
//          from      q_data                qd,
//                    q_types               qt,
//                    (
//                      select  count(qn.id)  note_count,
//                              qn.id
//                      from    q_notes qn
//                      where   qn.id = :id
//                      group by qn.id
//                    ) qn,
//                    merchant              m,
//                    users                 u
//          where     qd.id = :id
//                    and qd.type = :this.type
//                    and qd.type = qt.type
//                    and qd.id = qn.id(+)
//                    and qd.id = m.app_seq_num
//                    and qd.source = u.login_name(+)      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    qd.id                 id,\n                  qd.type               type,\n                  qd.item_type          item_type,\n                  qd.owner              owner,\n                  qd.date_created       date_created,\n                  qd.source             source,\n                  qd.affiliate          affiliate,\n                  qd.last_changed       last_changed,\n                  qd.last_user          last_user,\n                  m.merch_number        merchant_number,\n                  m.merch_business_name description,\n                  u.name                rep_name,\n                  qd.id                 control,\n                  qd.locked_by          locked_by,\n                  nvl(qn.note_count,0)  note_count,\n                  qt.status             status\n        from      q_data                qd,\n                  q_types               qt,\n                  (\n                    select  count(qn.id)  note_count,\n                            qn.id\n                    from    q_notes qn\n                    where   qn.id =  :1 \n                    group by qn.id\n                  ) qn,\n                  merchant              m,\n                  users                 u\n        where     qd.id =  :2 \n                  and qd.type =  :3 \n                  and qd.type = qt.type\n                  and qd.id = qn.id(+)\n                  and qd.id = m.app_seq_num\n                  and qd.source = u.login_name(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.ProductDetermination",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setLong(2,id);
   __sJT_st.setInt(3,this.type);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.queues.ProductDetermination",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:138^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueItem(" + this.type + ", " + id + ")", e.toString());
    }
  }
  
  public String getDescriptionURL()
  {
    return "/jsp/setup/merchinfo4.jsp?primaryKey=";
  }
  
  public int getMenuId()
  {
    return MesMenus.MENU_ID_PRODUCT_DETERMINATION;
  }
  
  public String getBackLink()
  {
    return "/jsp/menus/cbt_queue_menu.jsp?com.mes.ReportMenuId=" + getMenuId();
  }

  public String getBackLinkDesc()
  {
    String result = "";

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:169^7*/

//  ************************************************************
//  #sql [Ctx] { select  menu_title
//          
//          from    menu_types
//          where   menu_id = :getMenuId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_512 = getMenuId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  menu_title\n         \n        from    menu_types\n        where   menu_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.queues.ProductDetermination",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,__sJT_512);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:175^7*/
    }
    catch(Exception e)
    {
      logEntry("getBackLinkDesc()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public void setType(int newType)
  {
    super.setType(newType);
    
    String [][] buttons = null;
    
    switch(newType)
    {
      case MesQueues.Q_PRODUCT_DETERMINE_NEW:
        buttons = new String[][]
        {
          { "Move to POS Setup", Integer.toString(MesQueues.Q_MMS_NEW)  },
          { "Cancel",            Integer.toString(MesQueues.Q_PRODUCT_DETERMINE_CANCELED) }
        };
        break;
      case MesQueues.Q_PRODUCT_DETERMINE_CANCELED:
        buttons = new String[][]
        {
          { "Move to POS Setup", Integer.toString(MesQueues.Q_MMS_NEW)  }
        };
        break;
        
      default:
        break;
    }
    
    // if there are actions provide a way to select and provide comments
    if (buttons != null)
    {
      fields.add( new RadioButtonField( "action", buttons, -1, false, "Must select an action" ) );
      fields.add( new TextareaField(    "actionComments",500,8,80,true));
    }
  }
  
  public void doAction(HttpServletRequest request)
  {
    // do queue move
    Field   comments    = getField("actionComments");
    int     destQueueId = HttpHelper.getInt(request, "action", -1);
    long    itemId      = HttpHelper.getLong(request, "id", -1);
    
    // get user id of app submitter
    QueueTools.moveQueueItem(itemId, this.type, destQueueId, this.getUser(), comments.getData());
  }
  
}/*@lineinfo:generated-code*/