/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/QueueFunctionBase.java $

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 4/05/02 3:07p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import com.mes.database.SQLJConnectionBase;

public class QueueFunctionBase extends SQLJConnectionBase
{
  protected   long      id          = 0L;
  protected   int       type        = 0;
  protected   int       itemType    = 0;
  
  public QueueFunctionBase()
  {
  }
  
  public void setid(String id)
  {
    try
    {
      if(id != null)
      {
        this.id = Long.parseLong(id);
      }
    }
    catch(Exception e)
    {
      logEntry("setId(" + id + ")", e.toString());
    }
  }
  public long getId()
  {
    return this.id;
  }
  
  public void setType(String type)
  {
    try
    {
      if(type != null)
      {
        this.type = Integer.parseInt(type);
      }
    }
    catch(Exception e)
    {
      logEntry("setType(" + type + ")", e.toString());
    }
  }
  public int getType()
  {
    return this.type;
  }
  
  public void setItemType(String itemType)
  {
    try
    {
      if(itemType != null)
      {
        this.itemType = Integer.parseInt(itemType);
      }
    }
    catch(Exception e)
    {
      logEntry("setItemType(" + type + ")", e.toString());
    }
  }
  
}
