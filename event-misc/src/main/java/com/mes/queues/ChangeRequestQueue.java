/*@lineinfo:filename=ChangeRequestQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/ChangeRequestQueue.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-10-22 16:10:59 -0700 (Mon, 22 Oct 2007) $
  Version            : $Revision: 14231 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.sql.Date;
import java.sql.ResultSet;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.net.GetClosestParent;
import com.mes.queues.ExtraColumnBean.LinkData;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class ChangeRequestQueue extends QueueBase
{
  // create class log category
  static Logger log = Logger.getLogger(ChangeRequestQueue.class);

  private int QUEUE_CLASS = MesMenus.MENU_ID_ACR_QUEUES;
  //can also be MesMenus.MENU_ID_BBT_ACR_QUEUES;

  public ChangeRequestQueue()
  {
    super();

    createSearchFields();
  }

  private void createSearchFields()
  {
    FieldGroup      searchFields = initSearchFields();

    // fields will appear in this order when displayed
    searchFields.add( new DateField( "searchBeginDate","Submit Date From: ",true ) );
    ((DateField)searchFields.getField("searchBeginDate")).setDateFormat("MM/dd/yyyy");
    searchFields.add( new DateField( "searchEndDate","To:", true ) );
    ((DateField)searchFields.getField("searchEndDate")).setDateFormat("MM/dd/yyyy");
    searchFields.add( new Field    ( "searchValue","Search Criteria:", 50, 50, true ) );

    searchFields.add( new DropDownField("acrType","ACR Type",(new AcrTypeDropDownTable()),true) );
    searchFields.add( new DropDownField("accountType","[Account] Type",(new AccountTypeDropDownTable(getUser())),true) );

  }

  public void setUser(UserBean v)
  {
    super.setUser(v);
    //rebuild the accounttype dropdown
    FieldGroup      searchFields = (FieldGroup)getField("searchFields");
    searchFields.add( new DropDownField("accountType","[Account] Type",(new AccountTypeDropDownTable(getUser())),true) );
  }

  public void setType(int type)
  {
    super.setType(type);

    if(type == MesQueues.Q_CHANGE_REQUEST_COMPLETED ||
       type == MesQueues.Q_CHANGE_REQUEST_CANCELLED ||
       type == MesQueues.Q_CHANGE_REQUEST_BBT_CANCELLED ||
       type == MesQueues.Q_CHANGE_REQUEST_BBT_COMPLETED) {

      FieldGroup      searchFields  = (FieldGroup)fields.getField("searchFields");

      java.util.Date now = new java.util.Date();
      ((DateField)searchFields.getField( "searchBeginDate" )).setUtilDate(now);
      ((DateField)searchFields.getField( "searchEndDate" )).setUtilDate(now);

    }

    if(isBBT(type))
    {
      QUEUE_CLASS = MesMenus.MENU_ID_BBT_ACR_QUEUES;
    }
  }

  private boolean isBBT(int type)
  {
    switch(type)
    {
      case  MesQueues.Q_CHANGE_REQUEST_BBT_PENDING:
      case  MesQueues.Q_CHANGE_REQUEST_BBT_DATA_ENTRY:
      case  MesQueues.Q_CHANGE_REQUEST_BBT_QA:
      case  MesQueues.Q_CHANGE_REQUEST_BBT_OUTBOUND_CALL:
      case  MesQueues.Q_CHANGE_REQUEST_BBT_DATA_ENTRY_EXCP:
      case  MesQueues.Q_CHANGE_REQUEST_BBT_CLIENT_SERVICE_EXCP:
      case  MesQueues.Q_CHANGE_REQUEST_BBT_ACCOUNTING:
      case  MesQueues.Q_CHANGE_REQUEST_BBT_MGMT:
      case  MesQueues.Q_CHANGE_REQUEST_BBT_COMPLETED:
      case  MesQueues.Q_CHANGE_REQUEST_BBT_CANCELLED:
      case  MesQueues.Q_CHANGE_REQUEST_BBT_ACCT_MGR:
        return true;
      default:
        return false;
    }
  }

  private class AcrTypeDropDownTable extends DropDownTable
  {
    public AcrTypeDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      ResultSetIterator   it      = null;
      ResultSet           rs      = null;

      try
      {
        addElement("","All ACR Types");

        connect();

        /*@lineinfo:generated-code*//*@lineinfo:139^9*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//                      NAME
//                      ,SHORT_NAME
//            FROM
//                      ACRDEF
//            WHERE
//                      IS_ON = 1
//            ORDER BY
//                      NAME
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                    NAME\n                    ,SHORT_NAME\n          FROM\n                    ACRDEF\n          WHERE\n                    IS_ON = 1\n          ORDER BY\n                    NAME";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.ChangeRequestQueue",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.queues.ChangeRequestQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:150^9*/

        rs = it.getResultSet();

        String val,desc;

        while (rs.next()) {
          val = rs.getString("SHORT_NAME");
          desc = rs.getString("NAME");
          addElement(val,desc);
        }

        it.close();
      }
      catch (Exception e) {
        logEntry("AcrTypeDropDownTable.getData(" + user.getLoginName() + ")", e.toString());
      }
      finally {
        cleanUp();
      }
    }

  } // AcrTypeDropDownTable

  private class AccountTypeDropDownTable extends DropDownTable
  {
    UserBean user = null;

    public AccountTypeDropDownTable(UserBean user)
    {
      this.user = user;
      getData();
    }

    private void getData()
    {
      ResultSetIterator   it      = null;
      ResultSet           rs      = null;
      try
      {

        addElement("","All Account Types");

        connect();
        long userNode = 9999999999L;

        if(user!=null)
        {
          userNode = user.getHierarchyNode();
        }

        if(userNode == 9999999999L)
        {
           // just get all app types
           /*@lineinfo:generated-code*//*@lineinfo:204^12*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct  oa.appsrctype_code   appsrctype_code,
//                       oa.app_name                    app_name
//               from    org_app oa,
//                       app_type apt
//               where   oa.app_type = apt.app_type_code and
//                       nvl(apt.restrict_mes, 'N') = 'N'
//               order by oa.app_name asc
//              };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct  oa.appsrctype_code   appsrctype_code,\n                     oa.app_name                    app_name\n             from    org_app oa,\n                     app_type apt\n             where   oa.app_type = apt.app_type_code and\n                     nvl(apt.restrict_mes, 'N') = 'N'\n             order by oa.app_name asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.ChangeRequestQueue",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.queues.ChangeRequestQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:213^12*/
        }
        else
        {
          // get closest parent in the app_status_app_types table
          long appNode = GetClosestParent.get("app_status_app_types", userNode, Ctx);

          // get all app types that match the appNode
          /*@lineinfo:generated-code*//*@lineinfo:221^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct  oa.appsrctype_code appsrctype_code,
//                     oa.app_name                  app_name
//             from    org_app   oa,
//                     app_status_app_types  asat
//             where   asat.hierarchy_node = :appNode and
//                     asat.app_type = oa.app_type
//             order by oa.appsrctype_code asc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct  oa.appsrctype_code appsrctype_code,\n                   oa.app_name                  app_name\n           from    org_app   oa,\n                   app_status_app_types  asat\n           where   asat.hierarchy_node =  :1  and\n                   asat.app_type = oa.app_type\n           order by oa.appsrctype_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.queues.ChangeRequestQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.queues.ChangeRequestQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:230^11*/
        }

        rs = it.getResultSet();

        String val,desc;
        while(rs.next())
        {
          val = rs.getString("appsrctype_code");
          desc = rs.getString("app_name");
          addElement(val,desc);
        }

        rs.close();
        it.close();

      }
      catch (Exception e) {
        logEntry("AccountTypeDropDownTable.getData(" + user.getLoginName() + ")", e.toString());
      }
      finally {
        cleanUp();
      }

    }

  } // AccountTypeDropDownTable

  protected void loadQueueData(UserBean user)
  {
    try
    {
      FieldGroup      searchFields  = (FieldGroup)fields.getField("searchFields");

      // Extract params here
      long    longVal           = 0L;
      Date    searchBeginDate   = ((DateField)searchFields.getField( "searchBeginDate" )).getSqlDate();
      Date    searchEndDate     = ((DateField)searchFields.getField( "searchEndDate" )).getSqlDate();
      String  searchValue       = searchFields.getField( "searchValue" ).getData();

      String  acrType           = searchFields.getField( "acrType" ).getData();
      String  accountType       = searchFields.getField( "accountType" ).getData();
      boolean  filter           = false;

      // get the search value as a long if possible
      try{ longVal = Long.parseLong(searchValue); } catch( Exception e ) { longVal = 0L; }

      // note that the class member SearchSubmitted can be used
      // if your code needs to know when the user has pressed
      // the search button on the UI.

      if ( SearchSubmitted )
      {
        //accountType is null when none selected = always filter
        //otherwise only filter if not BBT
        if(accountType == null)
        {
          if(user.getOrgBankId() != mesConstants.BANK_ID_BBT)
          {
            filter = true;
          }
        }
        else
        {
          filter = true;
        }

      }

      // run the query using the search params - if it's not a BBT user,
      // ensure that the query run removes all BBT items.
      if(filter)
      {
        //two types of identifiers for BBT (legacy)
        String bbtTypeInt = ""+mesConstants.BANK_ID_BBT;
        String bbtType = "BBT";

        /*@lineinfo:generated-code*//*@lineinfo:307^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   id,
//                    qd.id                   control,
//                    qd.type                 type,
//                    qd.item_type            item_type,
//                    qd.owner                owner,
//                    qd.date_created         date_created,
//                    qd.source               source,
//                    qd.affiliate            affiliate,
//                    qd.last_changed         last_changed,
//                    qd.last_user            last_user,
//                    qd.is_rush              is_rush,
//                    m.dba_name              description,
//                    merch.merc_cntrl_number app_control,
//                    merch.app_seq_num       ec_link_data1,
//                    qd.locked_by            locked_by,
//                    count(qn.id)            note_count,
//                    qt.status               status,
//                    m.merchant_number       merchant_number,
//                    count_service_calls(a.merch_number, 15)   num_of_assignments,
//                    ad.name                 acr_name
//            from    q_data                  qd,
//                    q_notes                 qn,
//                    q_types                 qt,
//                    merchant                merch,
//                    mif                     m,
//                    acr                     a,
//                    acrdef                  ad,
//                    t_hierarchy             th
//            where   qd.type         = :this.type and
//                    qd.id           = a.acr_seq_num and
//                    a.merch_number  = m.merchant_number(+) and
//                    a.merch_number  = merch.merch_number(+) and
//                    a.acrdefid      = ad.acrdef_seq_num and
//                    qd.type         = qt.type and
//                    a.acr_seq_num   = qn.id(+)
//                    and
//                    (
//                          (( :searchBeginDate is null ) or ( trunc(qd.date_created) between :searchBeginDate and :searchEndDate ))
//                      and (
//                            ( :searchValue is null ) or
//                            ( m.merchant_number = :longVal ) or
//                            ( qd.id = :longVal ) or
//                            ( a.acr_seq_num = :longVal ) or
//                            ( lower(m.dba_name) like lower('%' || :searchValue || '%') )
//                          )
//                      and (ad.short_name = :acrType or :acrType is null)
//                      and (qd.affiliate = :accountType or :accountType is null)
//                      and (qd.affiliate != :bbtType or qd.affiliate != :bbtTypeInt)
//                    ) and
//                    m.association_node = th.descendent and
//                    th.ancestor = :user.getHierarchyNode()
//            group by qd.id,
//                     qd.id,
//                     qd.is_rush,
//                     qd.type,
//                     qd.item_type,
//                     qd.owner,
//                     qd.date_created,
//                     qd.source,
//                     qd.affiliate,
//                     qd.last_changed,
//                     qd.last_user,
//                     m.dba_name,
//                     merch.merc_cntrl_number,
//                     merch.app_seq_num,
//                     qd.locked_by,
//                     qt.status,
//                     m.merchant_number,
//                     ad.name,
//                     a.merch_number
//            order by qd.is_rush desc,qd.date_created asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_485 = user.getHierarchyNode();
  try {
   String theSqlTS = "select  qd.id                   id,\n                  qd.id                   control,\n                  qd.type                 type,\n                  qd.item_type            item_type,\n                  qd.owner                owner,\n                  qd.date_created         date_created,\n                  qd.source               source,\n                  qd.affiliate            affiliate,\n                  qd.last_changed         last_changed,\n                  qd.last_user            last_user,\n                  qd.is_rush              is_rush,\n                  m.dba_name              description,\n                  merch.merc_cntrl_number app_control,\n                  merch.app_seq_num       ec_link_data1,\n                  qd.locked_by            locked_by,\n                  count(qn.id)            note_count,\n                  qt.status               status,\n                  m.merchant_number       merchant_number,\n                  count_service_calls(a.merch_number, 15)   num_of_assignments,\n                  ad.name                 acr_name\n          from    q_data                  qd,\n                  q_notes                 qn,\n                  q_types                 qt,\n                  merchant                merch,\n                  mif                     m,\n                  acr                     a,\n                  acrdef                  ad,\n                  t_hierarchy             th\n          where   qd.type         =  :1  and\n                  qd.id           = a.acr_seq_num and\n                  a.merch_number  = m.merchant_number(+) and\n                  a.merch_number  = merch.merch_number(+) and\n                  a.acrdefid      = ad.acrdef_seq_num and\n                  qd.type         = qt.type and\n                  a.acr_seq_num   = qn.id(+)\n                  and\n                  (\n                        ((  :2  is null ) or ( trunc(qd.date_created) between  :3  and  :4  ))\n                    and (\n                          (  :5  is null ) or\n                          ( m.merchant_number =  :6  ) or\n                          ( qd.id =  :7  ) or\n                          ( a.acr_seq_num =  :8  ) or\n                          ( lower(m.dba_name) like lower('%' ||  :9  || '%') )\n                        )\n                    and (ad.short_name =  :10  or  :11  is null)\n                    and (qd.affiliate =  :12  or  :13  is null)\n                    and (qd.affiliate !=  :14  or qd.affiliate !=  :15 )\n                  ) and\n                  m.association_node = th.descendent and\n                  th.ancestor =  :16 \n          group by qd.id,\n                   qd.id,\n                   qd.is_rush,\n                   qd.type,\n                   qd.item_type,\n                   qd.owner,\n                   qd.date_created,\n                   qd.source,\n                   qd.affiliate,\n                   qd.last_changed,\n                   qd.last_user,\n                   m.dba_name,\n                   merch.merc_cntrl_number,\n                   merch.app_seq_num,\n                   qd.locked_by,\n                   qt.status,\n                   m.merchant_number,\n                   ad.name,\n                   a.merch_number\n          order by qd.is_rush desc,qd.date_created asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.queues.ChangeRequestQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setDate(2,searchBeginDate);
   __sJT_st.setDate(3,searchBeginDate);
   __sJT_st.setDate(4,searchEndDate);
   __sJT_st.setString(5,searchValue);
   __sJT_st.setLong(6,longVal);
   __sJT_st.setLong(7,longVal);
   __sJT_st.setLong(8,longVal);
   __sJT_st.setString(9,searchValue);
   __sJT_st.setString(10,acrType);
   __sJT_st.setString(11,acrType);
   __sJT_st.setString(12,accountType);
   __sJT_st.setString(13,accountType);
   __sJT_st.setString(14,bbtType);
   __sJT_st.setString(15,bbtTypeInt);
   __sJT_st.setLong(16,__sJT_485);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.queues.ChangeRequestQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:380^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:384^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   id,
//                    qd.id                   control,
//                    qd.type                 type,
//                    qd.item_type            item_type,
//                    qd.owner                owner,
//                    qd.date_created         date_created,
//                    qd.source               source,
//                    qd.affiliate            affiliate,
//                    qd.last_changed         last_changed,
//                    qd.last_user            last_user,
//                    qd.is_rush              is_rush,
//                    m.dba_name              description,
//                    merch.merc_cntrl_number app_control,
//                    merch.app_seq_num       ec_link_data1,
//                    qd.locked_by            locked_by,
//                    count(qn.id)            note_count,
//                    qt.status               status,
//                    m.merchant_number       merchant_number,
//                    count_service_calls(a.merch_number, 15)   num_of_assignments,
//                    ad.name                 acr_name
//            from    q_data                  qd,
//                    q_notes                 qn,
//                    q_types                 qt,
//                    merchant                merch,
//                    mif                     m,
//                    acr                     a,
//                    acrdef                  ad,
//                    t_hierarchy             th
//            where   qd.type         = :this.type and
//                    qd.id           = a.acr_seq_num and
//                    a.merch_number  = m.merchant_number(+) and
//                    a.merch_number  = merch.merch_number(+) and
//                    a.acrdefid      = ad.acrdef_seq_num and
//                    qd.type         = qt.type and
//                    a.acr_seq_num   = qn.id(+)
//                    and
//                    (
//                          (( :searchBeginDate is null ) or ( trunc(qd.date_created) between :searchBeginDate and :searchEndDate ))
//                      and (
//                            ( :searchValue is null ) or
//                            ( m.merchant_number = :longVal ) or
//                            ( qd.id = :longVal ) or
//                            ( a.acr_seq_num = :longVal ) or
//                            ( lower(m.dba_name) like lower('%' || :searchValue || '%') )
//                          )
//                      and (ad.short_name = :acrType or :acrType is null)
//                      and (qd.affiliate = :accountType or :accountType is null)
//                    ) and
//                    m.association_node = th.descendent and
//                    th.ancestor = :user.getHierarchyNode()
//            group by qd.id,
//                     qd.id,
//                     qd.is_rush,
//                     qd.type,
//                     qd.item_type,
//                     qd.owner,
//                     qd.date_created,
//                     qd.source,
//                     qd.affiliate,
//                     qd.last_changed,
//                     qd.last_user,
//                     m.dba_name,
//                     merch.merc_cntrl_number,
//                     merch.app_seq_num,
//                     qd.locked_by,
//                     qt.status,
//                     m.merchant_number,
//                     ad.name,
//                     a.merch_number
//            order by qd.is_rush desc,qd.date_created asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_486 = user.getHierarchyNode();
  try {
   String theSqlTS = "select  qd.id                   id,\n                  qd.id                   control,\n                  qd.type                 type,\n                  qd.item_type            item_type,\n                  qd.owner                owner,\n                  qd.date_created         date_created,\n                  qd.source               source,\n                  qd.affiliate            affiliate,\n                  qd.last_changed         last_changed,\n                  qd.last_user            last_user,\n                  qd.is_rush              is_rush,\n                  m.dba_name              description,\n                  merch.merc_cntrl_number app_control,\n                  merch.app_seq_num       ec_link_data1,\n                  qd.locked_by            locked_by,\n                  count(qn.id)            note_count,\n                  qt.status               status,\n                  m.merchant_number       merchant_number,\n                  count_service_calls(a.merch_number, 15)   num_of_assignments,\n                  ad.name                 acr_name\n          from    q_data                  qd,\n                  q_notes                 qn,\n                  q_types                 qt,\n                  merchant                merch,\n                  mif                     m,\n                  acr                     a,\n                  acrdef                  ad,\n                  t_hierarchy             th\n          where   qd.type         =  :1  and\n                  qd.id           = a.acr_seq_num and\n                  a.merch_number  = m.merchant_number(+) and\n                  a.merch_number  = merch.merch_number(+) and\n                  a.acrdefid      = ad.acrdef_seq_num and\n                  qd.type         = qt.type and\n                  a.acr_seq_num   = qn.id(+)\n                  and\n                  (\n                        ((  :2  is null ) or ( trunc(qd.date_created) between  :3  and  :4  ))\n                    and (\n                          (  :5  is null ) or\n                          ( m.merchant_number =  :6  ) or\n                          ( qd.id =  :7  ) or\n                          ( a.acr_seq_num =  :8  ) or\n                          ( lower(m.dba_name) like lower('%' ||  :9  || '%') )\n                        )\n                    and (ad.short_name =  :10  or  :11  is null)\n                    and (qd.affiliate =  :12  or  :13  is null)\n                  ) and\n                  m.association_node = th.descendent and\n                  th.ancestor =  :14 \n          group by qd.id,\n                   qd.id,\n                   qd.is_rush,\n                   qd.type,\n                   qd.item_type,\n                   qd.owner,\n                   qd.date_created,\n                   qd.source,\n                   qd.affiliate,\n                   qd.last_changed,\n                   qd.last_user,\n                   m.dba_name,\n                   merch.merc_cntrl_number,\n                   merch.app_seq_num,\n                   qd.locked_by,\n                   qt.status,\n                   m.merchant_number,\n                   ad.name,\n                   a.merch_number\n          order by qd.is_rush desc,qd.date_created asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.queues.ChangeRequestQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setDate(2,searchBeginDate);
   __sJT_st.setDate(3,searchBeginDate);
   __sJT_st.setDate(4,searchEndDate);
   __sJT_st.setString(5,searchValue);
   __sJT_st.setLong(6,longVal);
   __sJT_st.setLong(7,longVal);
   __sJT_st.setLong(8,longVal);
   __sJT_st.setString(9,searchValue);
   __sJT_st.setString(10,acrType);
   __sJT_st.setString(11,acrType);
   __sJT_st.setString(12,accountType);
   __sJT_st.setString(13,accountType);
   __sJT_st.setLong(14,__sJT_486);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.queues.ChangeRequestQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:456^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("loadQueueData(" + user.getLoginName() + ")", e.toString());
    }
  }

  protected void loadQueueItem(long id)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:469^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   id,
//                  qd.id                   control,
//                  qd.type                 type,
//                  qd.item_type            item_type,
//                  qd.owner                owner,
//                  qd.date_created         date_created,
//                  qd.source               source,
//                  qd.affiliate            affiliate,
//                  qd.last_changed         last_changed,
//                  qd.last_user            last_user,
//                  qd.is_rush              is_rush,
//                  m.dba_name              description,
//                  merch.merc_cntrl_number app_control,
//                  merch.app_seq_num       ec_link_data1,
//                  qd.locked_by            locked_by,
//                  count(qn.id)            note_count,
//                  qt.status               status,
//                  m.merchant_number       merchant_number,
//                  count_service_calls(a.merch_number, 15)   num_of_assignments,
//                  ad.name                 acr_name
//          from    q_data                  qd,
//                  q_notes                 qn,
//                  merchant                merch,
//                  mif                     m,
//                  q_types                 qt,
//                  acr                     a,
//                  acrdef                  ad
//          where   qd.type         = :this.type and
//                  qd.id           = :id and
//                  qd.id           = a.acr_seq_num and
//                  a.merch_number  = m.merchant_number(+) and
//                  a.merch_number  = merch.merch_number(+) and
//                  a.acrdefid      = ad.acrdef_seq_num and
//                  qd.type         = qt.type and
//                  a.acr_seq_num   = qn.id(+)
//          group by qd.id,
//                   qd.id,
//                   qd.is_rush,
//                   qd.type,
//                   qd.item_type,
//                   qd.owner,
//                   qd.date_created,
//                   qd.source,
//                   qd.affiliate,
//                   qd.last_changed,
//                   qd.last_user,
//                   m.dba_name,
//                   merch.merc_cntrl_number,
//                   merch.app_seq_num,
//                   qd.locked_by,
//                   qt.status,
//                   m.merchant_number,
//                   ad.name,
//                   a.merch_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                   id,\n                qd.id                   control,\n                qd.type                 type,\n                qd.item_type            item_type,\n                qd.owner                owner,\n                qd.date_created         date_created,\n                qd.source               source,\n                qd.affiliate            affiliate,\n                qd.last_changed         last_changed,\n                qd.last_user            last_user,\n                qd.is_rush              is_rush,\n                m.dba_name              description,\n                merch.merc_cntrl_number app_control,\n                merch.app_seq_num       ec_link_data1,\n                qd.locked_by            locked_by,\n                count(qn.id)            note_count,\n                qt.status               status,\n                m.merchant_number       merchant_number,\n                count_service_calls(a.merch_number, 15)   num_of_assignments,\n                ad.name                 acr_name\n        from    q_data                  qd,\n                q_notes                 qn,\n                merchant                merch,\n                mif                     m,\n                q_types                 qt,\n                acr                     a,\n                acrdef                  ad\n        where   qd.type         =  :1  and\n                qd.id           =  :2  and\n                qd.id           = a.acr_seq_num and\n                a.merch_number  = m.merchant_number(+) and\n                a.merch_number  = merch.merch_number(+) and\n                a.acrdefid      = ad.acrdef_seq_num and\n                qd.type         = qt.type and\n                a.acr_seq_num   = qn.id(+)\n        group by qd.id,\n                 qd.id,\n                 qd.is_rush,\n                 qd.type,\n                 qd.item_type,\n                 qd.owner,\n                 qd.date_created,\n                 qd.source,\n                 qd.affiliate,\n                 qd.last_changed,\n                 qd.last_user,\n                 m.dba_name,\n                 merch.merc_cntrl_number,\n                 merch.app_seq_num,\n                 qd.locked_by,\n                 qt.status,\n                 m.merchant_number,\n                 ad.name,\n                 a.merch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.queues.ChangeRequestQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setLong(2,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.queues.ChangeRequestQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:525^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueItem(" + this.type + ", " + id + ")", e.toString());
    }
  }

  protected void loadExtraColumns(ExtraColumnBean extraColumns, UserBean user)
  {
    try
    {
      // add the extra columns
      LinkData linkData = null;

      linkData = extraColumns.getNewLinkData();
      if(this.type == MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS)
      {
        //NOTE: this accounts for the above service call groupings... won't really make sense in
        //any other ACR Queue...
        linkData.addLink("merchant","merchant_number");
        extraColumns.addColumn("# Calls", "num_of_assignments", "/jsp/maintenance/viewcalls.jsp?", "blank", "", linkData);
      }
      linkData.addLink("primaryKey","ec_link_data1");
      extraColumns.addColumn("App Cntrl #", "app_control", "/jsp/setup/merchinfo4.jsp", "blank", "", linkData);
      extraColumns.addColumn("Merch #", "merchant_number");
      extraColumns.addColumn("ACR Type", "acr_name");
    }
    catch(Exception e)
    {
      logEntry("loadExtraColumns()", e.toString());
    }

  }

  public String getDescriptionURL()
  {
    String result = "/jsp/acr/queue.jsp?type=" + this.type + "&queueId=";
    return result;
  }

  public int getMenuId()
  {
    return QUEUE_CLASS;
  }

  public String getBackLink()
  {
    StringBuffer result = new StringBuffer();

    result.append("/jsp/menus/generic_menu.jsp?com.mes.ReportMenuId=");
    result.append(QUEUE_CLASS);

    return result.toString();
  }

  public String getGenericSearchDesc()
  {
    StringBuffer          desc      = new StringBuffer();

    desc.append("<ul>");
    desc.append("  <li>Merchant Number</li>");
    desc.append("  <li>Merchant DBA Name (partial is OK)</li>");
    desc.append("  <li>ACR Control Number</li>");
    desc.append("</ul>");

    return( desc.toString() );
  }

}/*@lineinfo:generated-code*/