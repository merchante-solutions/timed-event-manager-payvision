/*@lineinfo:filename=QueueBaseTools*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.queues;

import com.mes.database.SQLJConnectionBase;
import com.mes.user.UserBean;


public class QueueBaseTools extends SQLJConnectionBase
{
  private QueueBaseTools()
  {
  }

  public static QueueBase buildQueueBean(int itemType, UserBean user)
  {
    return (new QueueBaseTools())._buildQueueBean(itemType, user);
  }
  private QueueBase _buildQueueBean(int itemType, UserBean user)
  {
    String  className;
    boolean useCleanUp = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }

      /*@lineinfo:generated-code*//*@lineinfo:30^7*/

//  ************************************************************
//  #sql [Ctx] { select  QUEUE_BEAN_CLASS_NAME
//          
//          from    q_item_types
//          where   item_type = :itemType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  QUEUE_BEAN_CLASS_NAME\n         \n        from    q_item_types\n        where   item_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.QueueBaseTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,itemType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   className = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:36^7*/
    }
    catch(Exception e)
    {
      logEntry("_buildQueueBean(" + itemType + ")", e.toString());
      className = "com.mes.queues.QueueBase";
    }
    finally
    {
      if(useCleanUp)
        cleanUp();
    }

    QueueBase queueBean;
    try
    {
      queueBean = (QueueBase)(Class.forName(className).newInstance());
      queueBean.setUser(user);
    }
    catch(Exception e) {
      queueBean = null;
      logEntry("_buildQueueBean(" + itemType + ")", e.toString());
    }

    return queueBean;
  }


}/*@lineinfo:generated-code*/