/*@lineinfo:filename=BbtRequiredDocs*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/BbtRequiredDocs.sqlj $

  Last Modified By   : $Author: Jkirton $
  Last Modified Date : $Date: 2/24/04 2:50p $
  Version            : $Revision: 2 $

  BbtRequiredDocs

  Used to manage the required documentation queue.

  Change History:
     See VSS database

  Copyright (C) 2003 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.queues;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class BbtRequiredDocs extends FieldBean
{
  private boolean isQueuePage;
  
  public class LabelField extends Field
  {
    public LabelField(String fname, String labelText)
    {
      super(fname,0,0,true);
      setData(labelText);
    }
    
    protected String renderHtmlField()
    {
      StringBuffer html = new StringBuffer();
      html.append("<span " + getHtmlExtra() + ">");
      html.append(getRenderData());
      html.append("</span>");
      return html.toString();
    }
  }
  
  /*
  ** public BbtRequiredDocs(UserBean user)
  **
  ** Constructor.
  **
  ** Initializes fields used by credit decision screen form.
  */
  public BbtRequiredDocs(UserBean user,boolean isQueuePage)
  {
    super(user);
    this.isQueuePage = isQueuePage;
    
    fields.add(new HiddenField("type"));
    fields.add(new HiddenField("id"));
    fields.add(new HiddenField("itemType"));
    fields.add(new HiddenField("startType"));
    
    fields.add(new ButtonField("update"));

    fields.setGroupHtmlExtra("class=\"smallText\"");
    
    fields.add(new LabelField("businessDescription","Unknown"));
    
  }
  
  /*
  ** public void createRequiredDocumentFields(String appSeqNum)
  **
  ** Creates required document fields based on business type and annual
  ** V/MC volume.
  */
  public void createRequiredDocumentFields(String appSeqNum)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      connect();
      
      // determine merchant app conditions
      double  annualVolume;
      int     businessType;
      String  businessTypeDesc;
      /*@lineinfo:generated-code*//*@lineinfo:106^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(0,m.merch_month_visa_mc_sales) * 12,
//                  m.bustype_code,
//                  b.bustype_desc
//          
//          from    merchant m,
//                  bustype b
//          where   app_seq_num = :appSeqNum
//                  and m.bustype_code = b.bustype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(0,m.merch_month_visa_mc_sales) * 12,\n                m.bustype_code,\n                b.bustype_desc\n         \n        from    merchant m,\n                bustype b\n        where   app_seq_num =  :1 \n                and m.bustype_code = b.bustype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.BbtRequiredDocs",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   annualVolume = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   businessType = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   businessTypeDesc = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:118^7*/
      boolean isCorp    = (businessType == 2);
      boolean isLlc     = (businessType == 8);
      boolean isPart    = (businessType == 3);
      boolean is250Plus = (annualVolume > 250000 && isCorp);
      boolean is100Plus = (annualVolume > 100000 && !isCorp);
      
      // generate business description
      StringBuffer busDesc = new StringBuffer();
      busDesc.append("Business Type: " + businessTypeDesc + ", ");
      if (!isCorp)
      {
        if (is100Plus)
        {
          busDesc.append("Greater than $100,000");
        }
        else
        {
          busDesc.append("Less than $100,000");
        }
      }
      else
      {
        if (is250Plus)
        {
          busDesc.append("Greater than $250,000");
        }
        else
        {
          busDesc.append("Less than $250,000");
        }
      }
      fields.setData("businessDescription",busDesc.toString());
      
      // load doc items
      /*@lineinfo:generated-code*//*@lineinfo:153^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  doc_id,
//                  description
//          from    q_doc_items
//          order by doc_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  doc_id,\n                description\n        from    q_doc_items\n        order by doc_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.BbtRequiredDocs",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.queues.BbtRequiredDocs",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:159^7*/
      
      // determine which might apply to this merchant
      FieldGroup docGroups = new FieldGroup("docGroups");
      rs = it.getResultSet();
      while (rs.next())
      {
        int docId = rs.getInt("doc_id");
        boolean docNeeded = false;
        boolean orNeeded = false;
        switch (docId)
        {
          case MesQueues.QDI_MERCHANT_AGREEMENT:
          case MesQueues.QDI_VMC_VOLUME_TICKET:
          case MesQueues.QDI_ONSITE_INSPECTION:
          case MesQueues.QDI_PRIVACY_POLICY:
          case MesQueues.QDI_PRIOR_ACTIVITY:
          case MesQueues.QDI_WEBSITE_COPY:
            docNeeded = true;
            break;
            
          case MesQueues.QDI_CREDIT_REPORT:
          case MesQueues.QDI_PERSONAL_GUARANTY:
            docNeeded = (!isCorp || !is250Plus);
            break;
            
          default:
          case MesQueues.QDI_LLC_PARTNERSHIP_RESOLUTION:
          case MesQueues.QDI_2_YEARS_FINANCIALS:
          case MesQueues.QDI_3_YEARS_FINANCIALS:
          case MesQueues.QDI_CORPORATE_RESOLUTION:
            docNeeded = false;
            break;
        }
        String description = rs.getString("description");
        if (orNeeded)
        {
          description = description + ", or";
        }

        // create relevant fields for this merchant app
        if (docNeeded)
        {
          FieldGroup docGroup = new FieldGroup("docGroup_" + docId);
          docGroup.add(
            new LabelField("doclabel_" + docId,description));
          docGroup.add(new CheckboxField("docrequired_" + docId,false));
          docGroup.add(new CheckboxField("docreceived_" + docId,false));
          docGroups.add(docGroup);
        }
      }
      fields.add(docGroups);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::createRequiredDocumentFields(" + appSeqNum + "): " + e);
      logEntry("createRequiredDocumentFields(" + appSeqNum + ")",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  /*
  ** protected void preHandlRequest(HttpServletRequest)
  **
  ** Create additional doc fields based on the id found in the request.
  */
  protected void preHandleRequest(HttpServletRequest request)
  {
    String appSeqNum = request.getParameter("id");

    createRequiredDocumentFields(appSeqNum);
  }
  
  /*
  ** protected void postHandleRequest(HttpServletRequest request)
  **
  ** Set startType if it's blank.
  */
  protected void postHandleRequest(HttpServletRequest request)
  {
    // copy type into startType if startType is not set
    if (isQueuePage && fields.getField("startType").isBlank())
    {
      fields.setData("startType",fields.getData("type"));
    }
  }

  /*
  ** public Field[][][] getDocFields()
  **
  ** Creates a multi-dimensional array of doc group fields.  First dimension
  ** indicates which category the requirement is (0-application checklist,
  ** 1-credit documentation).  Next dimension is an index within that category.
  ** Last dimension indicates which field it is (0-description label, 1-required
  ** checkbox, 2-received checkbox)
  **
  ** RETURNS: array of doc group fields.
  */
  public Field[][][] getDocFields()
  {
    // create arrays based on number of groups
    FieldGroup docGroups = (FieldGroup)fields.getField("docGroups");
    Field[][][] docFields = new Field[2][][];
    Field[][] appFields = new Field[6][3];
    Field[][] credFields = new Field[docGroups.size() - 6][3];
    docFields[0] = appFields;
    docFields[1] = credFields;
    
    // iterate through the groups, keep track of indexes for
    // both categories, place fields in array according to category
    // and type
    int[] fieldIndex = new int[2];
    for (Iterator i = docGroups.iterator(); i.hasNext();)
    {
      // determine the docId from the docGroup name,
      // and category from the docId
      FieldGroup docGroup = (FieldGroup)i.next();
      StringTokenizer tok = new StringTokenizer(docGroup.getName(),"_");
      tok.nextToken();
      int docId = 0;
      try
      {
        docId = Integer.parseInt(tok.nextToken());
      }
      catch (Exception e) {}
      int category = (docId > MesQueues.QDI_WEBSITE_COPY ? 1 : 0);
      
      // place fields in array
      docFields[category][fieldIndex[category]][0]
        = docGroup.getField("doclabel_" + docId);
      docFields[category][fieldIndex[category]][1]
        = docGroup.getField("docrequired_" + docId);
      docFields[category][fieldIndex[category]][2]
        = docGroup.getField("docreceived_" + docId);
      ++fieldIndex[category];
    }
    
    return docFields;
  }
  
  /*
  ** protected boolean autoSubmit()
  **
  ** Submits current documentation queue data.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      connect();
      
      // clear out current set of doc data
      /*@lineinfo:generated-code*//*@lineinfo:323^7*/

//  ************************************************************
//  #sql [Ctx] { delete  from q_doc_data
//          where   id = : fields.getData("id")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_474 =  fields.getData("id");
   String theSqlTS = "delete  from q_doc_data\n        where   id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.queues.BbtRequiredDocs",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_474);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:327^7*/
      
      // iterate through doc groups
      FieldGroup docGroups = (FieldGroup)fields.getField("docGroups");
      int receivedCount = 0;
      int requiredCount = 0;
      for (Iterator i = docGroups.iterator(); i.hasNext();)
      {
        // extract doc group data
        FieldGroup docGroup = (FieldGroup)i.next();
        StringTokenizer tok = new StringTokenizer(docGroup.getName(),"_");
        tok.nextToken();
        String docId = tok.nextToken();
        String docRequired = docGroup.getData("docrequired_" + docId);
        String docReceived = docGroup.getData("docreceived_" + docId);
        if (docRequired.equals("y"))
        {
          ++requiredCount;
        }
        if (docReceived.equals("y"))
        {
          ++receivedCount;
        }
        
        // store group data as a record in q_doc_data
        /*@lineinfo:generated-code*//*@lineinfo:352^9*/

//  ************************************************************
//  #sql [Ctx] { insert into q_doc_data
//            ( id,
//              doc_id,
//              doc_required,
//              doc_received )
//            values
//            ( : fields.getData("id"),
//              :docId,
//              :docRequired,
//              :docReceived )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_475 =  fields.getData("id");
   String theSqlTS = "insert into q_doc_data\n          ( id,\n            doc_id,\n            doc_required,\n            doc_received )\n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.queues.BbtRequiredDocs",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_475);
   __sJT_st.setString(2,docId);
   __sJT_st.setString(3,docRequired);
   __sJT_st.setString(4,docReceived);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:364^9*/
      }
      
      // determine current doc queue type
      int oldDocType = -1;
      /*@lineinfo:generated-code*//*@lineinfo:369^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  type
//          from    q_data
//          where   id = : fields.getData("id")
//                  and type in ( : MesQueues.Q_CBT_DOC_WORKING, 
//                    : MesQueues.Q_CBT_DOC_COMPLETE )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_476 =  fields.getData("id");
  try {
   String theSqlTS = "select  type\n        from    q_data\n        where   id =  :1 \n                and type in (  :2 , \n                   :3  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.queues.BbtRequiredDocs",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_476);
   __sJT_st.setInt(2, MesQueues.Q_CBT_DOC_WORKING);
   __sJT_st.setInt(3, MesQueues.Q_CBT_DOC_COMPLETE);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.queues.BbtRequiredDocs",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:376^7*/
      rs = it.getResultSet();
      if (rs.next())
      {
        oldDocType = rs.getInt("type");
      }

      // make sure doc item exists in proper queue (complete or not)
      int newDocType = (requiredCount > receivedCount 
                          ? MesQueues.Q_CBT_DOC_WORKING
                          : MesQueues.Q_CBT_DOC_COMPLETE);
                          
      // if move needed, do it
      if (oldDocType != newDocType && oldDocType != -1)
      {
        // move from old to new queue
        long id = Long.parseLong(fields.getData("id"));
        QueueTools.moveQueueItem(id,oldDocType,newDocType,user,"");

        // don't muck with queue type parameters unless bean
        // is being used by actual doc queue function page
        if (isQueuePage)
        {
          // set the queue type in the type field to be the new queue
          String newTypeString = Integer.toString(newDocType);
          fields.setData("type",newTypeString);

          // add a queue type override flag to the request
          request.setAttribute("overrideType",newTypeString);
        }
      }

      submitOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::autoSubmit(" + fields.getData("id") + "): " + e);
      logEntry("autoSubmit(" + fields.getData("id") + ")",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
      cleanUp();
    }
    
    return submitOk;
  }
  
  /*
  ** protected boolean autoLoad()
  **
  ** Loads current documentation queue data.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:443^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  'docrequired_'||doc_id  required_name,
//                  'docreceived_'||doc_id  received_name,
//                  doc_required,
//                  doc_received
//          from    q_doc_data
//          where   id = : fields.getData("id")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_477 =  fields.getData("id");
  try {
   String theSqlTS = "select  'docrequired_'||doc_id  required_name,\n                'docreceived_'||doc_id  received_name,\n                doc_required,\n                doc_received\n        from    q_doc_data\n        where   id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.queues.BbtRequiredDocs",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_477);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.queues.BbtRequiredDocs",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:451^7*/
      
      rs = it.getResultSet();
      while (rs.next())
      {
        String docRequired = rs.getString("doc_required").toLowerCase();
        String docReceived = rs.getString("doc_received").toLowerCase();
        String receivedName = rs.getString("received_name");
        String requiredName = rs.getString("required_name");
        fields.setData(receivedName,docReceived);
        fields.setData(requiredName,docRequired);
      }
      
      loadOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::autoLoad(" + fields.getData("id") + "): " + e);
      logEntry("autoLoad(" + fields.getData("id") + ")",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
      cleanUp();
    }
    
    return loadOk;
  }
}/*@lineinfo:generated-code*/