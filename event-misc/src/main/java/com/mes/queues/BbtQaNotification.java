/*@lineinfo:filename=BbtQaNotification*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/BbtQaNotification.sqlj $

  BbtQaNotification

  Supports BB&T qa notification screen.

  Last Modified By   : $Author: Jkirton $
  Last Modified Date : $Date: 2/24/04 2:43p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.queues;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import com.mes.config.MesDefaults;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.ImageButtonField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TextareaField;
import com.mes.forms.ZipField;
import com.mes.net.EmailQueue;
import com.mes.net.EmailQueueItem;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class BbtQaNotification extends FieldBean
{
  private String[][] receiptTypeList =
  {
    { "TO","to" },
    { "CC","cc" },
    { "BCC","bcc" },
    { "N/A","na" }
  };
  
  public BbtQaNotification(UserBean user)
  {
    super(user);
    
    // queue id fields
    fields.add(new HiddenField("type"));
    fields.add(new HiddenField("id"));
    fields.add(new HiddenField("itemType"));
    fields.add(new HiddenField("startType"));
    
    // common recipient types
    FieldGroup emailFields = new FieldGroup("email");
    emailFields.add(new RadioButtonField("merchRcptType",receiptTypeList,-1,false,
      "Please specify a recipient type"));
    emailFields.add(new RadioButtonField("repRcptType",receiptTypeList,-1,false,
      "Please specify a recipient type"));
      
    // recipient fields (others will be dynamically created later)
    emailFields.add(new HiddenField("toCount"));
    emailFields.add(new HiddenField("ccCount"));
    emailFields.add(new HiddenField("bccCount"));
    emailFields.add(new HiddenField("merchEmail"));
    emailFields.add(new HiddenField("merchName"));
    emailFields.add(new HiddenField("repEmail"));
    emailFields.add(new HiddenField("repName"));
    emailFields.add(new ImageButtonField("toAdd","/images/add_address.gif",12,67));
    emailFields.add(new ImageButtonField("ccAdd","/images/add_address.gif",12,67));
    emailFields.add(new ImageButtonField("bccAdd","/images/add_address.gif",12,67));
    emailFields.add(new Field("emailSubject",200,80,false));
    emailFields.add(new FieldGroup("recipients"));
    emailFields.add(new ButtonField("update"));
    emailFields.add(new ButtonField("preview"));
    emailFields.add(new ButtonField("send"));
    fields.add(emailFields);
    
    // shared fields
    fields.add(new TextareaField("emailBody",4000,10,110,false));
    fields.add(new ButtonField("default"));
    
    // print message fields
    FieldGroup snailFields = new FieldGroup("snail");
    snailFields.add(new Field("snailName",40,40,true));
    snailFields.add(new Field("snailAddress1",40,40,true));
    snailFields.add(new Field("snailAddress2",40,40,true));
    snailFields.add(new Field("snailCity",40,40,true));
    snailFields.add(new DropDownField("snailState",new StateDropDownTable(),true));
    snailFields.add(new ZipField("snailZip",true));
    snailFields.add(new Field("snailSubject",200,40,true));
    snailFields.add(new ButtonField("generate"));
    snailFields.add(new ButtonField("merchant fill"));
    fields.add(snailFields);
    
    fields.setGroupHtmlExtra("class=\"smallText\"");
    fields.setShowErrorText(true);
    
    // default recipient counts to 1
    fields.setData("toCount","0");
    fields.setData("ccCount","0");
    fields.setData("bccCount","0");
    
    fields.setData("merchRcptType","to");
    fields.setData("repRcptType","na");
    fields.setData("emailSubject","Notice from Merchant Services");
    fields.setData("snailSubject","Notice from Merchant Services");
  }
  
  /*
  ** private void createRecipientFields(String prefix,HttpServletRequest request)
  **
  ** Creates two groups of fields, each containing rCount fields, one
  ** group containing email fields, the other name fields. Fields and
  ** groups are named using the prefix and an index value.  rCount is
  ** pulled from the request using the prefix.  If the request also
  ** contains an add or remove button push for this prefix, then rCount
  ** is incremented/decremented as appropriate.
  */
  private void createRecipientFields(String prefix,HttpServletRequest request)
  {
    // get recipient wrapper field group
    FieldGroup recipients = (FieldGroup)fields.getField("recipients");
      
    // determine number of fields needed
    int rCount = 0;
    try
    {
      rCount = Integer.parseInt(fields.getData(prefix + "Count"));
      if (fields.getData(prefix + "Add").length() > 0)
      {
        ++rCount;
      }
    }
    catch (Exception e) {}
    
    // create fields
    FieldGroup nameGroup  = new FieldGroup(prefix + "Names");
    FieldGroup emailGroup = new FieldGroup(prefix + "Emails");
    FieldGroup removeGroup  = new FieldGroup(prefix + "Removes");
    int curIdx = 1;
    for (int i = 1; i <= rCount; ++i)
    {
      // if the remove button wasn't hit for this entry, create fields for it
      if (request.getParameter(prefix + "Remove_" + i + ".x") == null)
      {
        // create fields with current index
        Field nameField   = new Field(prefix + "Name_" + curIdx,45,20,false);
        Field emailField  = new EmailField(prefix + "Email_" + curIdx,45,30,false);
        Field removeField = new ImageButtonField(prefix + "Remove_" + curIdx,
            "/images/remove_address.gif",12,86);
            
        nameGroup.add(nameField);
        emailGroup.add(emailField);
        removeGroup.add(removeField);
        
        // set the name and email to request data
        nameField.setData(request.getParameter(prefix + "Name_" + i));
        emailField.setData(request.getParameter(prefix + "Email_" + i));
        
        ++curIdx;
      }
    }
    
    nameGroup.setGroupHtmlExtra("class=\"smallText\"");
    emailGroup.setGroupHtmlExtra("class=\"smallText\"");
    
    recipients.add(nameGroup);
    recipients.add(emailGroup);
    recipients.add(removeGroup);
    
    // update the field count
    try
    {
      fields.setData(prefix + "Count",Integer.toString(nameGroup.size()));
    }
    catch (Exception e) {}
  }
  
  /*
  ** private String expandReasonTag(String letter)
  **
  ** Expands reason tag to list of reason descriptions based on the queue type
  ** and the reason codes in q_data for this item.
  **
  ** RETURNS: the text of the letter body with any reason tag replaced by the
  **          reason descriptions.
  */
  
  private String expandReasonTag(String letter)
  {
    StringBuffer newLetter = new StringBuffer(letter);
    int tagIndex = letter.indexOf("<reasons>");
    if (tagIndex >= 0)
    {
      StringBuffer reasons = new StringBuffer();
      
      try
      {
        connect();
        
        String codes;
        /*@lineinfo:generated-code*//*@lineinfo:211^9*/

//  ************************************************************
//  #sql [Ctx] { select  codes 
//            from    q_credit_data
//            where   id = : fields.getData("id")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_468 =  fields.getData("id");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  codes  \n          from    q_credit_data\n          where   id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.BbtQaNotification",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_468);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   codes = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:216^9*/
        
        
        String qType = fields.getData("type");
        StringTokenizer tok = new StringTokenizer(codes,",");
        while (tok.hasMoreTokens())
        {
          String description;
          /*@lineinfo:generated-code*//*@lineinfo:224^11*/

//  ************************************************************
//  #sql [Ctx] { select  cr.description 
//              from    q_credit_reasons  cr,
//                      q_credit_types    ct
//              where   ct.type = :qType
//                      and ct.reason_type = cr.type
//                      and cr.code = :tok.nextToken()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_469 = tok.nextToken();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cr.description  \n            from    q_credit_reasons  cr,\n                    q_credit_types    ct\n            where   ct.type =  :1 \n                    and ct.reason_type = cr.type\n                    and cr.code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.BbtQaNotification",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,qType);
   __sJT_st.setString(2,__sJT_469);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   description = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:232^11*/
          
          reasons.append(" - " + description + "\n");
        }
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName()
          + "::expandReasonTag(): " + e);
        logEntry("expandReasonTag()",e.toString());
      }
      finally
      {
        cleanUp();
      }
      
      newLetter.replace(tagIndex,tagIndex + 9,reasons.toString());
    }
    
    return newLetter.toString();
  }
  
  /*
  ** private void loadMessageTemplate()
  **
  ** Loads a template message from the database based on the letter type field.
  ** Reason tags in the template are expanded to a list of reasons.
  */
  private void loadMessageTemplate()
  {
    // load a message template if the email body is
    // empty or a new message type has been selected
    Field body = fields.getField("emailBody");
    String letter = null;
    String subject = null;
    if (fields.getData("default").length() > 0 || body.getData().length() == 0)
    {
      // load template body from database
      try
      {
        connect();
        
        String qType = fields.getData("type");
        /*@lineinfo:generated-code*//*@lineinfo:275^9*/

//  ************************************************************
//  #sql [Ctx] { select  cl.letter,
//                    cl.subject
//            
//            from    q_credit_letters cl,
//                    q_credit_types   ct
//            where   ct.type = :qType
//                    and ct.letter_type = cl.type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cl.letter,\n                  cl.subject\n           \n          from    q_credit_letters cl,\n                  q_credit_types   ct\n          where   ct.type =  :1 \n                  and ct.letter_type = cl.type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.queues.BbtQaNotification",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,qType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   letter = (String)__sJT_rs.getString(1);
   subject = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:285^9*/
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName()
          + "::loadMessageTemplate(): " + e);
        logEntry("loadMessageTemplate()",e.toString());
      }
      finally
      {
        cleanUp();
      }

      // expand reason tag if present
      letter = expandReasonTag(letter);
      
      // set the textarea field to the letter text
      body.setData(letter);
      fields.setData("snailSubject",subject);
      fields.setData("emailSubject",subject);
    }
  }
  
  /*
  ** private void loadSnailMailFields()
  **
  ** If snail mail fields are empty or user has asked for default population
  ** then load merchant recipient information into the fields.
  */
  private void loadSnailMailFields()
  {
    if (fields.getData("merchant fill").length() > 0
        || fields.getData("snailName").length() == 0)
    {
      // load template body from database
      
      ResultSetIterator it  = null;
      ResultSet rs          = null;
      try
      {
        connect();
        
        String qId = fields.getData("id");
        
        // look for mailing address
        /*@lineinfo:generated-code*//*@lineinfo:330^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.merch_business_name,
//                    a.address_line1,
//                    a.address_line2,
//                    a.address_city,
//                    a.countrystate_code,
//                    a.address_zip
//            from    merchant m,
//                    address a
//            where   m.app_seq_num = :qId
//                    and m.app_seq_num = a.app_seq_num
//                    and a.addresstype_code = 2
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.merch_business_name,\n                  a.address_line1,\n                  a.address_line2,\n                  a.address_city,\n                  a.countrystate_code,\n                  a.address_zip\n          from    merchant m,\n                  address a\n          where   m.app_seq_num =  :1 \n                  and m.app_seq_num = a.app_seq_num\n                  and a.addresstype_code = 2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.queues.BbtQaNotification",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,qId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.queues.BbtQaNotification",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:343^9*/
        rs = it.getResultSet();
        if (!rs.next())
        {
          // use business address as backup if mailing address not available
          /*@lineinfo:generated-code*//*@lineinfo:348^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.merch_business_name,
//                      a.address_line1,
//                      a.address_line2,
//                      a.address_city,
//                      a.countrystate_code,
//                      a.address_zip
//              from    merchant m,
//                      address a
//              where   m.app_seq_num = :qId
//                      and m.app_seq_num = a.app_seq_num
//                      and a.addresstype_code = 1
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.merch_business_name,\n                    a.address_line1,\n                    a.address_line2,\n                    a.address_city,\n                    a.countrystate_code,\n                    a.address_zip\n            from    merchant m,\n                    address a\n            where   m.app_seq_num =  :1 \n                    and m.app_seq_num = a.app_seq_num\n                    and a.addresstype_code = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.queues.BbtQaNotification",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,qId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.queues.BbtQaNotification",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:361^11*/
          rs = it.getResultSet();
          rs.next();
        }

        fields.setData("snailName",rs.getString("merch_business_name"));
        fields.setData("snailAddress1",rs.getString("address_line1"));
        fields.setData("snailAddress2",rs.getString("address_line2"));
        fields.setData("snailCity",rs.getString("address_city"));
        fields.setData("snailState",rs.getString("countrystate_code"));
        fields.setData("snailZip",rs.getString("address_zip"));
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName()
          + "::loadSnailMailFields(): " + e);
        logEntry("loadSnailMailFields()",e.toString());
      }
      finally
      {
        try { it.close(); } catch (Exception e) {}
        try { rs.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
    
  }
  
  /*
  ** private void loadMerchAndRep()
  **
  ** Loads the names and emails of the merchant and the sales rep.
  */
  private void loadMerchAndRep()
  {
    try
    {
      connect();
      
      String merchEmail;
      String merchName;
      String repEmail;
      String repName;
      
      /*@lineinfo:generated-code*//*@lineinfo:405^7*/

//  ************************************************************
//  #sql [Ctx] { select  m.merch_email_address,
//                  m.merch_business_name,
//                  u.name,
//                  u.email
//                  
//          
//          
//          from    merchant  m,
//                  users     u,
//                  q_data    qd
//                  
//          where   qd.type = :fields.getData("type")
//                  and qd.id = :fields.getData("id")
//                  and qd.id = m.app_seq_num
//                  and qd.source = u.login_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_470 = fields.getData("type");
 String __sJT_471 = fields.getData("id");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  m.merch_email_address,\n                m.merch_business_name,\n                u.name,\n                u.email\n                \n         \n        \n        from    merchant  m,\n                users     u,\n                q_data    qd\n                \n        where   qd.type =  :1 \n                and qd.id =  :2 \n                and qd.id = m.app_seq_num\n                and qd.source = u.login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.queues.BbtQaNotification",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_470);
   __sJT_st.setString(2,__sJT_471);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchEmail = (String)__sJT_rs.getString(1);
   merchName = (String)__sJT_rs.getString(2);
   repName = (String)__sJT_rs.getString(3);
   repEmail = (String)__sJT_rs.getString(4);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:425^7*/
      
      fields.setData("merchEmail",merchEmail);
      fields.setData("merchName",merchName);
      fields.setData("repEmail",repEmail);
      fields.setData("repName",repName);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::loadMerchAndRep(): " + e);
      logEntry("loadMerchAndRep()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** public void postHandleRequest(HttpServletRequest request)
  **
  ** Uses request parameters to create recipient fields.
  */
  protected void postHandleRequest(HttpServletRequest request)
  {
    // generate recipient fields
    createRecipientFields("to",request);
    createRecipientFields("cc",request);
    createRecipientFields("bcc",request);
    
    // load merch and rep fields
    loadMerchAndRep();
    
    // fill message body textarea
    loadMessageTemplate();
    
    // auto populate snail mail fields
    loadSnailMailFields();
  }
  
  /*
  ** public Field[] getRecipientFields(String rType)
  **
  ** Finds all recipient fields of the specified type and puts them
  ** into an array of type Field.  rType specifies the general category
  ** of recipient ("to", "cc" or "bcc").  fType specifies whether "Email"
  ** or "Name" or "Remove" fields are being sought.
  **
  ** RETURNS: array of Field objects, null if none found matching the
  **          rType/fType combination.
  */  
  public Field[] getRecipientFields(String rType, String fType)
  {
    Field[] rFields = null;

    try
    {
      FieldGroup fg = (FieldGroup)fields.getField(rType + fType + "s");
      rFields = new Field[fg.size()];
      for (Iterator i = fg.iterator(); i.hasNext();)
      {
        Field f = (Field)i.next();
        String name = f.getName();
        StringTokenizer tok = new StringTokenizer(name,"_");
        tok.nextToken();
        if (tok.hasMoreTokens())
        {
          try
          {
            int idx = Integer.parseInt(tok.nextToken()) - 1;
            if (idx >= 0 && idx < rFields.length)
            {
              rFields[idx] = f;
            }
          }
          catch (Exception e)
          {
            System.out.println("error in getRecipientFields() inner: " + e);
          }
        }
      }
    }
    catch (Exception e)
    {
      System.out.println("error in getRecipientFields() outer: " + e);
    }
    
    return rFields;
  }
  
  public boolean isSend()
  {
    return fields.getData("send").length() > 0;
  }
  public boolean isGenerate()
  {
    return fields.getData("generate").length() > 0;
  }
  public boolean previewRequested()
  {
    return fields.getData("preview").length() > 0;
  }
  
  /*
  ** private String getRecipientString(String rType)
  **
  ** Generates a list of email addresses based on the type given
  ** ("to", "cc", "bcc").  Will include the merchant and sales rep
  ** addresses if they are set to this recipient type.
  **
  ** RETURNS: String containing a list of the email addresses of the
  **          recipient type.
  */
  private String getRecipientString(String rType)
  {
    StringBuffer rString = new StringBuffer();
    int rCount = 0;
    
    if (fields.getData("merchRcptType").equals(rType))
    {
      rString.append(fields.getData("merchName") + " &lt;"
        + fields.getData("merchEmail") + "&gt;");
      ++rCount;
    }
    
    if (fields.getData("repRcptType").equals(rType))
    {
      rString.append(fields.getData("repName") + " &lt;"
        + fields.getData("repEmail") + "&gt;");
      ++rCount;
    }
    
    try
    {
      FieldGroup emails = (FieldGroup)fields.getField(rType + "Emails");
      for (Iterator i = emails.iterator(); i.hasNext();)
      {
        if (rCount > 0)
        {
          rString.append("; ");
        }
      
        Field email = (Field)i.next();
        Field name = null;
        StringTokenizer tok = new StringTokenizer(email.getName(),"_");
        if (tok.hasMoreTokens() && tok.nextToken() != null && tok.hasMoreTokens())
        {
          String idx = tok.nextToken();
          name = fields.getField(rType + "Name_" + idx);
        }
      
        if (name != null && name.getData().length() > 0)
        {
          rString.append(name.getData() + " &lt;" + email.getData() + "&gt;");
        }
        else if (email.getData().length() > 0)
        {
          rString.append(email.getData());
        }
      
        ++rCount;
      }
    }
    catch (Exception e)
    {
      System.out.println("getRecipientString(rType = " + rType + "): " + e);
    }
    
    return rString.toString(); 
  }
  
  /*
  ** private String convertTextToHtml(String text)
  **
  ** Scans a text string and generates a string to return with end of line chars
  ** converted to html <br> tags.
  **
  ** RETURNS: a String containing the html equivalent of text.
  */
  private String convertTextToHtml(String text)
  {
    StringBuffer html = new StringBuffer();
    for (int i = 0; i < text.length(); ++i)
    {
      char ch = text.charAt(i);
      if (ch == '\n')
      {
        html.append("<br>");
      }
      else if (ch != '\r')
      {
        html.append(ch);
      }
    }
    return html.toString();
  }
  
  /*
  ** public String getPreviewHtml()
  **
  ** Generates an html document displaying the contents of email that would be sent
  ** with the current settings.
  **
  ** RETURNS: String containing the html of the email.
  */
  public String getPreviewHtml(String title)
  {
    StringBuffer html = new StringBuffer();
    
    html.append("<html><head><title>" + title + "</title><body>");
    html.append("<table border=0>");
    
    // to recipients
    String rString = getRecipientString("to");
    if (rString.length() > 0)
    {
      html.append("<tr>");
      html.append("<td><b>To:</b></td>");
      html.append("<td>" + rString + "</td>");
      html.append("</tr>");
    }

    // cc recipients
    rString = getRecipientString("cc");
    if (rString.length() > 0)
    {
      html.append("<tr>");
      html.append("<td><b>Cc:</b></td>");
      html.append("<td>" + rString + "</td>");
      html.append("</tr>");
    }
    
    // bcc recipients
    rString = getRecipientString("bcc");
    if (rString.length() > 0)
    {
      html.append("<tr>");
      html.append("<td><b>Bcc:</b></td>");
      html.append("<td>" + rString + "<br></td>");
      html.append("</tr>");
    }

    // from address
    html.append("<tr>");
    html.append("<td><b>From:</b></td>");
    html.append("<td>Merchant Services<br></td>");
    html.append("</tr>");
    
    // subject
    html.append("<tr>");
    html.append("<td valign=top><b>Subject:</b></td>");
    html.append("<td>" + fields.getData("emailSubject") + "</td>");
    html.append("</tr>");
    
    // body
    html.append("<tr>");
    html.append("<td valign=top><b>Body:</b></td>");
    html.append("<td>" + convertTextToHtml(fields.getData("emailBody")) + "</td>");
    html.append("</tr>");
    html.append("</table>");
    html.append("</body>");
    
    // close button
    
    return html.toString();
  }
  
  /*
  ** private void logNotification(String letterType, String letter, String subject)
  **
  ** Logs a credit notification letter in q_credit_letter_log.
  */
  private void logNotification(String letterType, String letter, String subject)
  {
    try
    {
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:703^7*/

//  ************************************************************
//  #sql [Ctx] { insert into q_credit_letter_log
//          ( log_seq_num,
//            id,
//            send_date,
//            sender,
//            letter_type,
//            subject,
//            letter )
//          values
//          ( q_credit_letter_sequence.nextval,
//            :fields.getData("id"),
//            sysdate,
//            :user.getLoginName(),
//            :letterType,
//            :subject,
//            :letter )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_472 = fields.getData("id");
 String __sJT_473 = user.getLoginName();
   String theSqlTS = "insert into q_credit_letter_log\n        ( log_seq_num,\n          id,\n          send_date,\n          sender,\n          letter_type,\n          subject,\n          letter )\n        values\n        ( q_credit_letter_sequence.nextval,\n           :1 ,\n          sysdate,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.queues.BbtQaNotification",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_472);
   __sJT_st.setString(2,__sJT_473);
   __sJT_st.setString(3,letterType);
   __sJT_st.setString(4,subject);
   __sJT_st.setString(5,letter);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:721^7*/
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::logNotification: " + e);
      logEntry("logNotification()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** public String getGenerateHtml()
  **
  ** Generates a printable html document that can be printed for mailing to
  ** a recipient.  Also, logs the generated message.
  **
  ** RETURNS: String containing the html of a printable letter to send.
  */
  public String getGenerateHtml()
  {
    StringBuffer html = new StringBuffer();
    
    html.append("<html><head><title>Notification Letter</title><body>");
    html.append("<table border=0>");
    
    // white space
    html.append("<tr><td><br><br><br><br><br><br><br></td></tr>");

    // address
    html.append("<tr><td>");
    html.append(fields.getData("snailName") + "<br>");
    html.append(fields.getData("snailAddress1") + "<br>");
    String fieldData = fields.getData("snailAddress2");
    if (fieldData.length() > 0)
    {
      html.append(fieldData + "<br>");
    }
    html.append(fields.getData("snailCity") + ", ");
    html.append(fields.getData("snailState") + "  " + fields.getData("snailZip"));
    html.append("</td></tr>");
    
    // subject line
    fieldData = fields.getData("snailSubject");
    if (fieldData.length() > 0)
    {
      html.append("<tr><td>&nbsp;</td></tr>");
      html.append("<tr><td><b>" + fieldData + "</b></td></tr>");
    }
    
    // body
    html.append("<tr><td>&nbsp;</td></tr>");
    html.append("<tr><td>" + convertTextToHtml(fields.getData("emailBody")));
    html.append("</td></tr>");
    
    // close button
    
    // print button
    
    // log the email
    logNotification("S",html.toString(),fieldData);
    
    return html.toString();
  }
  
  private void loadEmailQueueItemAddresses(EmailQueueItem qi,String rType)
    throws Exception
  {
    int addrType = -1;
    if (rType.equals("to"))
    {
      addrType = EmailQueueItem.AT_TO;
    }
    else if (rType.equals("cc"))
    {
      addrType = EmailQueueItem.AT_CC;
    }
    else if (rType.equals("bcc"))
    {
      addrType = EmailQueueItem.AT_BCC;
    }
    else
    {
      throw new Exception("Invalid recipient type");
    }

    StringBuffer addrs = new StringBuffer(getRecipientString(rType));
    if (addrs.length() > 0)
    {
      int idx = -1;
      while ((idx = addrs.toString().indexOf("&lt;")) >= 0)
      {
        addrs.replace(idx,idx + 4,"<");
      }
      while ((idx = addrs.toString().indexOf("&gt;")) >= 0)
      {
        addrs.replace(idx,idx + 4,">");
      }
      StringTokenizer tok = new StringTokenizer(addrs.toString(),";");
      while (tok.hasMoreTokens())
      {
        qi.addAddress(tok.nextToken(),addrType);
      }
    }
  }
  
  /*
  ** public boolean sendEmail()
  **
  ** Sends an email message.
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean sendEmail()
  {
    boolean sendOk = false;
    
    try
    {
      long id = -1L;
      try
      {
        id = Long.parseLong(fields.getData("id"));
      }
      catch (Exception e) { }
        
      EmailQueueItem qi
        = new EmailQueueItem("BbtQaNotification","cbtcreditnotify",id);

      String fromAddress
        = MesDefaults.getString(MesDefaults.DK_CBT_CREDIT_EMAIL_FROM);
      qi.setFrom(fromAddress);
      
      loadEmailQueueItemAddresses(qi,"to");
      loadEmailQueueItemAddresses(qi,"cc");
      loadEmailQueueItemAddresses(qi,"bcc");
      
      String subject = fields.getData("emailSubject");
      qi.setSubject(subject);
      qi.setText(fields.getData("emailBody"));
      EmailQueue.push(qi);
      
      // log the email
      logNotification("E",getPreviewHtml("Sent Email"),subject);
      
      sendOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::sendEmail: " + e);
      logEntry("sendEmail()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return sendOk;
  }
}/*@lineinfo:generated-code*/