/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/CbtReviewQueue.java $

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 2/10/04 2:08p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import com.mes.constants.MesMenus;

public class CbtReviewQueue extends CbtCreditQueue
{
  public int getMenuId()
  {
    return MesMenus.MENU_ID_CBT_REVIEW_QUEUES;
  }
}
