/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/QueueSessionBean.java $

  Last Modified By   : $Author: Jkirton $
  Last Modified Date : $Date: 7/16/03 5:44p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.util.Vector;

public class QueueSessionBean
{
  private Vector    queueSessions     = new Vector();
  
  public QueueSessionBean()
  {
  }
  
  public void updateQueueSession(int type, int begin)
  {
    updateQueueSession(type,begin,null);
  }
  
  public void updateQueueSession(int type, int begin, QueueBase queueBean)
  {
    boolean       sessionFound    = false;
    QueueSession  qs              = null;
    try
    {
      // find the queue session in the session list
      for(int i=0; i < queueSessions.size(); ++i)
      {
        qs = (QueueSession)(queueSessions.elementAt(i));
        if(qs.getType() == type)
        {
          sessionFound = true;
          break;
        }
      }
      
      // if no session was found, create one and add it to the vector
      if( ! sessionFound )
      {
        qs = new QueueSession(type);
        queueSessions.add(qs);
      }
      
      if(qs != null)
      {
        // update the current begin page for this session
        qs.setBegin(begin);
        
        // set the queueBean for this session
        qs.setQueueBean(queueBean);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::setType(" + type + ")", e.toString());
    }
  }
  
  public int getBegin(int type)
  {
    int   result      = 0;
    
    try
    {
      for(int i=0; i < queueSessions.size(); ++i)
      {
        QueueSession qs = (QueueSession)(queueSessions.elementAt(i));
        
        if(qs.getType() == type)
        {
          result = qs.getBegin();
          break;
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getBegin(" + type + ")", e.toString());
    }
    
    return result;
  }
  
  public QueueBase getQueueBean(int type)
  {
    QueueBase   result      = null;
    
    try
    {
      for(int i=0; i < queueSessions.size(); ++i)
      {
        QueueSession qs = (QueueSession)(queueSessions.elementAt(i));
        
        if(qs.getType() == type)
        {
          result = qs.getQueueBean();
          break;
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getQueueBean(" + type + ")", e.toString());
    }
    
    return result;
  }

  public class QueueSession
  {
    private   int         type        = 0;
    public    int         begin       = 0;
    private   QueueBase   queueBean   = null;
    
    public QueueSession()
    {
    }
    public QueueSession(int type)
    {
      this.type       = type;
      this.begin      = 0;
      this.queueBean  = null;
    }
    public void setType(int type)
    {
      this.type = type;
    }
    public int getType()
    {
      return this.type;
    }
    public void setBegin(int begin)
    {
      this.begin = begin;
    }
    public int getBegin()
    {
      return this.begin;
    }
    public void setQueueBean(QueueBase queueBean)
    {
      this.queueBean = queueBean;
    }
    public QueueBase getQueueBean()
    {
      return this.queueBean;
    }
  }
}