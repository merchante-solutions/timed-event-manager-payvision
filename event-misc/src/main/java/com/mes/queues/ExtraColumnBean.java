/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/ExtraColumnBean.java $

  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 7/16/03 1:46p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.support.MesMath;

public class ExtraColumnBean
{
  
  public final static String DATA_TYPE_CURRENCY = "C";

  public Vector   columnHeaders   = null;
  public Vector   columnLinks     = null;
  public Vector   columnTargets   = null;
  public Vector   columnDataType  = null;
  public Vector   queryColumns    = null;
  public Vector   linkData        = null;
  public Vector   extraColumnRows = null;
  
  public boolean  hasExtraColumns = false;
  
  public ExtraColumnBean()
  {
    columnHeaders   = new Vector();
    columnLinks     = new Vector();
    columnTargets   = new Vector();
    queryColumns    = new Vector();
    linkData        = new Vector();
    extraColumnRows = new Vector();
    columnDataType  = new Vector();
  }
  
  /*
  ** METHOD addColumn
  **
  ** This method is used to add a column header and link attributes to the 
  ** queue page.  
  **
  ** header is displayed as a column header description on the queue page.
  **
  ** queryColumn is the name of the column in the resultSet.
  **
  ** The linkURL will be applied to the column data if necessary.
  **
  ** The target is used if the link needs to open in a new window.
  **
  ** This method should be called once per extra column, in the order
  ** desired on the queue page.
  */

  public void addColumn(String header, String queryColumn, String linkURL, String target, String dataType, LinkData ld)
  {
    try
    {
      columnHeaders.add(header);
      queryColumns.add(queryColumn);
      linkData.add(ld);
      columnLinks.add(linkURL           == null ? "" : linkURL);
      columnTargets.add(target          == null ? "" : target);
      columnDataType.add(dataType       == null ? "" : dataType);
      
      // set flag to show that the user has set up at least one extra column
      hasExtraColumns = true;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::addColumn(" + header + ", " + linkURL + ", " + target + ")", e.toString());
    }
  }

  public void addColumn(String header, String queryColumn, String linkURL, String target, String dataType)
  {
    addColumn(header, queryColumn, linkURL, target, dataType, null);
  }

  public void addColumn(String header, String queryColumn, String linkURL, String target)
  {
    addColumn(header, queryColumn, linkURL, target, "", null);
  }
  
  public void addColumn(String header, String queryColumn, String linkURL)
  {
    addColumn(header, queryColumn, linkURL, "");
  }
  
  public void addColumn(String header, String queryColumn)
  {
    addColumn(header, queryColumn, "", "");
  }
  
  /*
  ** METHOD setExtraColumnData
  **
  ** This method extracts one row of extra column data from the resultSet.
  ** This method depends on previous initialization of the queryColumns vector
  ** using successive calls to addQueryColumn.
  */
  public void setExtraColumnData(ResultSet rs)
  {
    try
    {
      if(hasExtraColumns)
      {
        extraColumnRows.add(new ExtraColumnRow(rs, queryColumns, columnLinks, columnTargets, linkData));
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getExtraColumnData", e.toString());
    }
  }
  
  public LinkData getNewLinkData()
  {
    return new LinkData();
  }

  public class LinkData
  {
    private Vector varDesc                = new Vector();
    private Vector varVal                 = new Vector();
    private String ValidationColumnName   = null;
    
    public void addLink(String desc, String queryCol)
    {
      if(desc != null && queryCol != null && !desc.equals("") && !queryCol.equals(""))
      {
        varDesc.add(desc.trim());
        varVal.add(queryCol.trim());
      }
    }

    public int size()
    {
      return this.varDesc.size();
    }

    public String getVarDesc(int idx)
    {
      return (String)this.varDesc.elementAt(idx);
    }

    public String getVarVal(int idx)
    {
      return (String)this.varVal.elementAt(idx);
    }
    
    public String getValidationColName()
    {
      return( ValidationColumnName );
    }
    
    public void setValidationColName( String colName )
    {
      ValidationColumnName = colName;
    }
  }


  /*
  ** SUBCLASS ExtraColumnRow
  **
  ** Allows easy creation and initialization of simple "extra" columns in a row
  */
  public class ExtraColumnRow
  {
    public Vector   extraColumnRowFields   = new Vector();
    
    public ExtraColumnRow(ResultSet rs, Vector queryColumns, Vector columnLinks, Vector columnTargets, Vector lnkData)
    {
      try
      {
        for(int i = 0; i < queryColumns.size(); ++i)
        {
          String data = "";

          //if there is nothing in the request that matches, 
          //we just use the queryColumn data as the data for this column
          try
          {
            data = rs.getString((String)queryColumns.elementAt(i));
            
            // prevent display errors
            if( data == null )
            {
              data = "";
            }
          }
          catch(Exception e)
          {
            data = (String)queryColumns.elementAt(i);
          }
          
          //check the datatype here and make format adjustments to the column data, if neccessary
          if(((String)columnDataType.elementAt(i)).equals(DATA_TYPE_CURRENCY))
          {
            data = MesMath.toCurrency(data);  
          }

          String constructedLink = (String)columnLinks.elementAt(i);
          
          try
          {
            if ( rs.getInt("link_data_valid") == 0 )
            {
              constructedLink = "";
            }
          }
          catch(Exception e)
          {
          }
          
          if(constructedLink != null && !constructedLink.equals(""))
          {
            //add field data to end of string first... this is always present
            if(constructedLink.indexOf("?") > -1)
            {
              if(constructedLink.endsWith("&") || constructedLink.endsWith("?"))
              {
                constructedLink += ("fieldData=" + data);
              }
              else
              {
                constructedLink += ("&fieldData=" + data);
              }
            }
            else
            {
              constructedLink += ("?fieldData=" + data);
            }


            //here we need to build the rest of the link...
            LinkData ld = (LinkData)lnkData.elementAt(i);
            if(ld != null)
            {
              // if there is a validation column for this 
              // link and it is invalid, then set the 
              // constructed link to null to disable the
              // link.
              try
              {
                if ( rs.getInt( ld.getValidationColName() ) == 0 )
                {
                  constructedLink = null;
                }
              }                
              catch( Exception e ) 
              {
              }
              
              // continue unless the validation column is 0
              if ( constructedLink != null )
              {
                for(int idx=0; idx<ld.size(); idx++)
                {
                  String var = "&" + ld.getVarDesc(idx) + "=";

                  try
                  {
                    var += rs.getString(ld.getVarVal(idx));
                  }
                  catch(Exception e)
                  {
                    var += ld.getVarVal(idx);
                  }

                  constructedLink += var;
                }
              }                
            }
          }

          ExtraColumnRowField  field = new ExtraColumnRowField(data, constructedLink, (String)columnTargets.elementAt(i));

          extraColumnRowFields.add(field);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::constructor", e.toString());
      }
    }
    
    /*
    ** SUB-SUBCLASS ExtraColumnRowField
    **
    ** This class contains one field of data inside of a single row 
    */
    public class ExtraColumnRowField
    {
      public  String    data          = "";
      public  String    link          = "";
      public  String    target        = "";
    
      public ExtraColumnRowField(String data, String link, String target)
      {
        this.data         = data;
        this.link         = link;
        this.target       = target;
      }
    }

  }

}
