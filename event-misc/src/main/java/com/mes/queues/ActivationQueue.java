/*@lineinfo:filename=ActivationQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/ActivationQueue.sqlj $

  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 11/21/02 4:40p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import com.mes.constants.MesMenus;
import com.mes.user.UserBean;

public class ActivationQueue extends QueueBase
{
  public ActivationQueue()
  {
  }
  
  protected void loadQueueData(UserBean user)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:36^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   id,
//                  qd.type                 type,
//                  qd.item_type            item_type,
//                  qd.owner                owner,
//                  qd.date_created         date_created,
//                  qd.source               source,
//                  qd.affiliate            affiliate,
//                  qd.last_changed         last_changed,
//                  qd.last_user            last_user,
//                  m.merch_business_name   description,
//                  m.merc_cntrl_number     control,
//                  qd.locked_by            locked_by,
//                  count(qn.id)            note_count,
//                  qt.status               status,
//                  m.merch_number          merchant_number,
//                  m.sic_code              sic_code
//          from    q_data                  qd,
//                  q_notes                 qn,
//                  q_types                 qt,
//                  merchant                m
//          where   qd.type = :this.type and
//                  qd.id = m.app_seq_num and
//                  qd.type = qt.type and
//                  qd.id = qn.id(+)
//          group by qd.id,
//                   qd.type,
//                   qd.item_type,
//                   qd.owner,
//                   qd.date_created,
//                   qd.source,
//                   qd.affiliate,
//                   qd.last_changed,
//                   qd.last_user,
//                   m.merch_business_name,
//                   m.merc_cntrl_number,
//                   qd.locked_by,
//                   qt.status,
//                   m.merch_number,
//                   m.sic_code
//          order by qd.date_created asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                   id,\n                qd.type                 type,\n                qd.item_type            item_type,\n                qd.owner                owner,\n                qd.date_created         date_created,\n                qd.source               source,\n                qd.affiliate            affiliate,\n                qd.last_changed         last_changed,\n                qd.last_user            last_user,\n                m.merch_business_name   description,\n                m.merc_cntrl_number     control,\n                qd.locked_by            locked_by,\n                count(qn.id)            note_count,\n                qt.status               status,\n                m.merch_number          merchant_number,\n                m.sic_code              sic_code\n        from    q_data                  qd,\n                q_notes                 qn,\n                q_types                 qt,\n                merchant                m\n        where   qd.type =  :1  and\n                qd.id = m.app_seq_num and\n                qd.type = qt.type and\n                qd.id = qn.id(+)\n        group by qd.id,\n                 qd.type,\n                 qd.item_type,\n                 qd.owner,\n                 qd.date_created,\n                 qd.source,\n                 qd.affiliate,\n                 qd.last_changed,\n                 qd.last_user,\n                 m.merch_business_name,\n                 m.merc_cntrl_number,\n                 qd.locked_by,\n                 qt.status,\n                 m.merch_number,\n                 m.sic_code\n        order by qd.date_created asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.ActivationQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.queues.ActivationQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:78^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueData(" + user.getLoginName() + ")", e.toString());
    }
  }
  
  protected void loadQueueItem(long id)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:90^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   id,
//                  qd.type                 type,
//                  qd.item_type            item_type,
//                  qd.owner                owner,
//                  qd.date_created         date_created,
//                  qd.source               source,
//                  qd.affiliate            affiliate,
//                  qd.last_changed         last_changed,
//                  qd.last_user            last_user,
//                  m.merch_business_name   description,
//                  m.merc_cntrl_number     control,
//                  qd.locked_by            locked_by,
//                  count(qn.id)            note_count,
//                  qt.status               status,
//                  m.merch_number          merchant_number
//          from    q_data                  qd,
//                  q_notes                 qn,
//                  merchant                m,
//                  q_types                 qt
//          where   qd.type = :this.type and
//                  qd.id = :id and
//                  qd.id = m.app_seq_num and
//                  qd.type = qt.type and
//                  qd.id = qn.id(+)
//          group by qd.id,
//                   qd.type,
//                   qd.item_type,
//                   qd.owner,
//                   qd.date_created,
//                   qd.source,
//                   qd.affiliate,
//                   qd.last_changed,
//                   qd.last_user,
//                   m.merch_business_name,
//                   m.merc_cntrl_number,
//                   qd.locked_by,
//                   qt.status,
//                   m.merch_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                   id,\n                qd.type                 type,\n                qd.item_type            item_type,\n                qd.owner                owner,\n                qd.date_created         date_created,\n                qd.source               source,\n                qd.affiliate            affiliate,\n                qd.last_changed         last_changed,\n                qd.last_user            last_user,\n                m.merch_business_name   description,\n                m.merc_cntrl_number     control,\n                qd.locked_by            locked_by,\n                count(qn.id)            note_count,\n                qt.status               status,\n                m.merch_number          merchant_number\n        from    q_data                  qd,\n                q_notes                 qn,\n                merchant                m,\n                q_types                 qt\n        where   qd.type =  :1  and\n                qd.id =  :2  and\n                qd.id = m.app_seq_num and\n                qd.type = qt.type and\n                qd.id = qn.id(+)\n        group by qd.id,\n                 qd.type,\n                 qd.item_type,\n                 qd.owner,\n                 qd.date_created,\n                 qd.source,\n                 qd.affiliate,\n                 qd.last_changed,\n                 qd.last_user,\n                 m.merch_business_name,\n                 m.merc_cntrl_number,\n                 qd.locked_by,\n                 qt.status,\n                 m.merch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.ActivationQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setLong(2,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.queues.ActivationQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:130^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueItem(" + this.type + ", " + id + ")", e.toString());
    }
  }
  
  protected void loadExtraColumns(ExtraColumnBean extraColumns, UserBean user)
  {
    try
    {
      // add the extra columns
      extraColumns.addColumn("Sic Code", "sic_code", "/jsp/get_header.jsp", "__blank");
    }
    catch(Exception e)
    {
      logEntry("loadExtraColumns()", e.toString());
    }
    
  }
  
  public String getDescriptionURL()
  {
    String result = "/jsp/setup/merchinfo4.jsp?primaryKey=";
    
    return result;
  }
  
  public int getMenuId()
  {
    return MesMenus.MENU_ID_ACTIVATION_QUEUE;
  }
  
  public String getBackLink()
  {
    StringBuffer result = new StringBuffer();
    
    result.append("/jsp/menus/generic_menu.jsp?com.mes.ReportMenuId=");
    result.append(MesMenus.MENU_ID_ACTIVATION_QUEUE);
    
    return result.toString();
  }
}/*@lineinfo:generated-code*/