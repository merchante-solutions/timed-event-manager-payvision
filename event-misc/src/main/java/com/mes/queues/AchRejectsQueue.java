/*@lineinfo:filename=AchRejectsQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/AchRejectsQueue.sqlj $

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 8/13/04 1:53p $
  Version            : $Revision: 23 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.queues.ExtraColumnBean.LinkData;
import com.mes.user.UserBean;

public class AchRejectsQueue extends QueueBase
{
  public AchRejectsQueue()
  {
  }
  
  protected void loadQueueData(UserBean user)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:38^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   id,
//                  qd.type                 type,
//                  qd.item_type            item_type,
//                  qd.owner                owner,
//                  qd.source               source,
//                  qd.affiliate            affiliate,
//                  qd.last_changed         last_changed,
//                  qd.last_user            last_user,
//                  qd.locked_by            locked_by,
//                  get_queue_note_count(ar.reject_seq_num,qd.type) note_count,
//                  check_dda_change(m.dda_num, ar.dda)                        dda_updated,
//                  check_tr_change(m.transit_routng_num, ar.transit_routing)  tr_updated,
//                  qt.status               status,
//                  ar.settled_date         date_created,
//                  ar.merchant_name        description,
//                  ar.entry_desc           entry_desc,
//                  ar.reject_seq_num       control,
//                  ar.adden_1              addend1,
//                  ar.adden_2              addend2,
//                  ar.merchant_number      merchant_number,
//                  ar.reason_code          reason_code,
//                  ar.amount               amount,
//                  arrc.description        code_description,
//                  decode(ars.reject_status, null, 'NEW', ars.reject_status) reject_status,
//                  m.dmagent               association,
//                  m.transit_routng_num    transit_routing_num,
//                  m.dda_num               dda_num
//          from    q_data                  qd,
//                  q_types                 qt,
//                  ach_rejects             ar,
//                  ach_reject_reason_codes arrc,
//                  ach_reject_status       ars,
//                  mif                     m
//          where   qd.type             = :this.type                        and
//                  qd.id               = ar.reject_seq_num                   and
//                  ar.reject_seq_num   = ars.reject_seq_num(+)               and
//                  ar.reason_code      = arrc.reason_code(+)                 and
//                  ar.merchant_number  = m.merchant_number                   and
//                  qd.type             = qt.type
//          group by qd.id,
//                   qd.type,
//                   qd.item_type,
//                   qd.owner,
//                   qd.source,
//                   qd.affiliate,
//                   qd.last_changed,
//                   qd.last_user,
//                   qd.locked_by,
//                   check_dda_change(m.dda_num, ar.dda),
//                   check_tr_change(m.transit_routng_num, ar.transit_routing),
//                   qt.status,
//                   ar.settled_date,
//                   ar.merchant_name,
//                   ar.entry_desc,
//                   ar.reject_seq_num,
//                   ar.adden_1,
//                   ar.adden_2,
//                   ar.merchant_number,
//                   ar.reason_code,
//                   ar.amount,
//                   arrc.description,
//                   decode(ars.reject_status, null, 'NEW', ars.reject_status),
//                   m.dmagent,
//                   m.transit_routng_num,
//                   m.dda_num
//          order by ar.settled_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                   id,\n                qd.type                 type,\n                qd.item_type            item_type,\n                qd.owner                owner,\n                qd.source               source,\n                qd.affiliate            affiliate,\n                qd.last_changed         last_changed,\n                qd.last_user            last_user,\n                qd.locked_by            locked_by,\n                get_queue_note_count(ar.reject_seq_num,qd.type) note_count,\n                check_dda_change(m.dda_num, ar.dda)                        dda_updated,\n                check_tr_change(m.transit_routng_num, ar.transit_routing)  tr_updated,\n                qt.status               status,\n                ar.settled_date         date_created,\n                ar.merchant_name        description,\n                ar.entry_desc           entry_desc,\n                ar.reject_seq_num       control,\n                ar.adden_1              addend1,\n                ar.adden_2              addend2,\n                ar.merchant_number      merchant_number,\n                ar.reason_code          reason_code,\n                ar.amount               amount,\n                arrc.description        code_description,\n                decode(ars.reject_status, null, 'NEW', ars.reject_status) reject_status,\n                m.dmagent               association,\n                m.transit_routng_num    transit_routing_num,\n                m.dda_num               dda_num\n        from    q_data                  qd,\n                q_types                 qt,\n                ach_rejects             ar,\n                ach_reject_reason_codes arrc,\n                ach_reject_status       ars,\n                mif                     m\n        where   qd.type             =  :1                         and\n                qd.id               = ar.reject_seq_num                   and\n                ar.reject_seq_num   = ars.reject_seq_num(+)               and\n                ar.reason_code      = arrc.reason_code(+)                 and\n                ar.merchant_number  = m.merchant_number                   and\n                qd.type             = qt.type\n        group by qd.id,\n                 qd.type,\n                 qd.item_type,\n                 qd.owner,\n                 qd.source,\n                 qd.affiliate,\n                 qd.last_changed,\n                 qd.last_user,\n                 qd.locked_by,\n                 check_dda_change(m.dda_num, ar.dda),\n                 check_tr_change(m.transit_routng_num, ar.transit_routing),\n                 qt.status,\n                 ar.settled_date,\n                 ar.merchant_name,\n                 ar.entry_desc,\n                 ar.reject_seq_num,\n                 ar.adden_1,\n                 ar.adden_2,\n                 ar.merchant_number,\n                 ar.reason_code,\n                 ar.amount,\n                 arrc.description,\n                 decode(ars.reject_status, null, 'NEW', ars.reject_status),\n                 m.dmagent,\n                 m.transit_routng_num,\n                 m.dda_num\n        order by ar.settled_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.AchRejectsQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.queues.AchRejectsQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:106^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueData(" + user.getLoginName() + ")", e.toString());
    }
  }
  
  protected void loadQueueItem(long id)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:118^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   id,
//                  qd.type                 type,
//                  qd.item_type            item_type,
//                  qd.owner                owner,
//                  ar.settled_date         date_created,
//                  qd.source               source,
//                  qd.affiliate            affiliate,
//                  qd.last_changed         last_changed,
//                  qd.last_user            last_user,
//                  ar.merchant_name        description,
//                  ar.reject_seq_num       control,
//                  qd.locked_by            locked_by,
//                  get_queue_note_count(ar.reject_seq_num,qd.type) note_count,
//                  check_dda_change(m.dda_num, ar.dda)             dda_updated,
//                  qt.status               status,
//                  ar.merchant_number      merchant_number,
//                  ar.reason_code          reason_code,
//                  arrc.description        code_description,
//                  ar.amount               amount,
//                  ar.merchant_number      merchant_number,
//                  decode(ars.reject_status, null, 'NEW', ars.reject_status) reject_status,
//                  m.dmagent               association,
//                  m.transit_routng_num    transit_routing_num
//          from    q_data                  qd,
//                  q_types                 qt,
//                  ach_rejects             ar,
//                  ach_reject_reason_codes arrc,
//                  ach_reject_status       ars,
//                  mif                     m
//          where   qd.type             = :this.type          and
//                  qd.id               = :id                   and
//                  qd.id               = ar.reject_seq_num     and
//                  ar.reject_seq_num   = ars.reject_seq_num(+) and
//                  ar.reason_code      = arrc.reason_code(+)   and
//                  ar.merchant_number  = m.merchant_number     and
//                  qd.type             = qt.type
//          group by qd.id,
//                   qd.type,
//                   qd.item_type,
//                   qd.owner,
//                   ar.settled_date,
//                   qd.source,
//                   qd.affiliate,
//                   qd.last_changed,
//                   qd.last_user,
//                   ar.merchant_name,
//                   ar.reject_seq_num,
//                   qd.locked_by,
//                   check_dda_change(m.dda_num, ar.dda),
//                   qt.status,
//                   ar.merchant_number,
//                   ar.reason_code,
//                   arrc.description,
//                   ar.amount,
//                   ar.merchant_number,
//                   ars.reject_status,
//                   m.dmagent,
//                   m.transit_routng_num
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                   id,\n                qd.type                 type,\n                qd.item_type            item_type,\n                qd.owner                owner,\n                ar.settled_date         date_created,\n                qd.source               source,\n                qd.affiliate            affiliate,\n                qd.last_changed         last_changed,\n                qd.last_user            last_user,\n                ar.merchant_name        description,\n                ar.reject_seq_num       control,\n                qd.locked_by            locked_by,\n                get_queue_note_count(ar.reject_seq_num,qd.type) note_count,\n                check_dda_change(m.dda_num, ar.dda)             dda_updated,\n                qt.status               status,\n                ar.merchant_number      merchant_number,\n                ar.reason_code          reason_code,\n                arrc.description        code_description,\n                ar.amount               amount,\n                ar.merchant_number      merchant_number,\n                decode(ars.reject_status, null, 'NEW', ars.reject_status) reject_status,\n                m.dmagent               association,\n                m.transit_routng_num    transit_routing_num\n        from    q_data                  qd,\n                q_types                 qt,\n                ach_rejects             ar,\n                ach_reject_reason_codes arrc,\n                ach_reject_status       ars,\n                mif                     m\n        where   qd.type             =  :1           and\n                qd.id               =  :2                    and\n                qd.id               = ar.reject_seq_num     and\n                ar.reject_seq_num   = ars.reject_seq_num(+) and\n                ar.reason_code      = arrc.reason_code(+)   and\n                ar.merchant_number  = m.merchant_number     and\n                qd.type             = qt.type\n        group by qd.id,\n                 qd.type,\n                 qd.item_type,\n                 qd.owner,\n                 ar.settled_date,\n                 qd.source,\n                 qd.affiliate,\n                 qd.last_changed,\n                 qd.last_user,\n                 ar.merchant_name,\n                 ar.reject_seq_num,\n                 qd.locked_by,\n                 check_dda_change(m.dda_num, ar.dda),\n                 qt.status,\n                 ar.merchant_number,\n                 ar.reason_code,\n                 arrc.description,\n                 ar.amount,\n                 ar.merchant_number,\n                 ars.reject_status,\n                 m.dmagent,\n                 m.transit_routng_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.AchRejectsQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setLong(2,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.queues.AchRejectsQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:179^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueItem(" + this.type + ", " + id + ")", e.toString());
    }
  }
  
  protected void loadExtraColumns(ExtraColumnBean extraColumns, UserBean user)
  {
    try
    {
      // add the extra columns

      LinkData linkData = null;
      
      linkData = extraColumns.getNewLinkData();
      linkData.addLink("merchant","merchant_number");
      
      extraColumns.addColumn("Tran Description",  "entry_desc");
      extraColumns.addColumn("Merchant Number",   "merchant_number", "/jsp/maintenance/view_account.jsp?action=1", "blank", "", linkData);
      extraColumns.addColumn("Association",       "association");
      extraColumns.addColumn("Transit Rtg #",     "transit_routing_num");
      extraColumns.addColumn("DDA #",             "dda_num");
      extraColumns.addColumn("Reason Code",       "reason_code");
      extraColumns.addColumn("Code Description",  "code_description");
      extraColumns.addColumn("Amount", "amount",  "", "", ExtraColumnBean.DATA_TYPE_CURRENCY);
      extraColumns.addColumn("Reject Status",     "reject_status");

      if(this.type == MesQueues.Q_ACH_REJECT_CATEGORY_2 || this.type == MesQueues.Q_ACH_REJECT_CATEGORY_3)
      {
        extraColumns.addColumn("DDA Updated",  "dda_updated");
        extraColumns.addColumn("T/R Updated",  "tr_updated");
      }
      else if(this.type == MesQueues.Q_ACH_REJECT_CATEGORY_MISC)
      {
        extraColumns.addColumn("Adden 1",  "addend1");
        extraColumns.addColumn("Adden 2",  "addend2");
      }

    }
    catch(Exception e)
    {
      logEntry("loadExtraColumns()", e.toString());
    }
    
  }
  
  public String getDescriptionURL()
  {
    String result = "";
    
    //result = "/jsp/credit/org_ach_reject.jsp?reject_seq_num=";
    
    return result;
  }
  
  public int getMenuId()
  {
    return MesMenus.MENU_ID_ACH_REJECT_QUEUES;
  }
  
  public String getBackLink()
  {
    StringBuffer result = new StringBuffer();
    
    result.append("/jsp/menus/generic_menu.jsp?com.mes.ReportMenuId=");
    result.append(MesMenus.MENU_ID_ACH_REJECT_QUEUES);
    
    return result.toString();
  }
}/*@lineinfo:generated-code*/