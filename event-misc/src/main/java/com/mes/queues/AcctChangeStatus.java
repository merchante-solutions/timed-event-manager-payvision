/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/AcctChangeStatus.java $

  Description:
  
    StatementRecord
  
    Allows storage and retrieval of statement records.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 3/05/03 6:41p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.util.Vector;
import com.mes.constants.MesQueues;
import com.mes.user.UserBean;

public class AcctChangeStatus extends QueueFunctionBase
{
  public static final int     ACTION_PEND         = 1;
  public static final int     ACTION_CANCEL       = 2;
  public static final int     ACTION_COMPLETE     = 3;
  public static final int     ACTION_CREDIT       = 4;
  public static final int     ACTION_SETUP        = 5;
  public static final int     ACTION_DEPLOYMENT   = 6;
  public static final int     ACTION_ACTIVATION   = 7;
  public static final int     ACTION_ADJUSTMENTS  = 8;
  public static final int     ACTION_ACCT_ENTRY   = 9;
  public static final int     ACTION_MMS          = 10;
  
  private int     action              = 0;
  private String  pendComments        = "";
  private String  cancelComments      = "";
  private String  completeComments    = "";
  private String  creditComments      = "";
  private String  setupComments       = "";
  private String  deploymentComments  = "";
  private String  activationComments  = "";
  private String  adjustmentComments  = "";
  private String  acctEntryComments   = "";
  private String  mmsComments         = "";
  private String  queueDescription    = "";
  private boolean submitted           = false;
  private Vector  errors              = new Vector();
  
  public AcctChangeStatus()
  {
  }
  
  public void submitData(UserBean user)
  {
    try
    {
      switch(action)
      {
        case ACTION_PEND:
          QueueTools.moveQueueItem(id, type, MesQueues.Q_ACCT_CHANGE_PEND, user, pendComments);
          break;
          
        case ACTION_CANCEL:
          QueueTools.moveQueueItem(id, type, MesQueues.Q_ACCT_CHANGE_CANCEL, user, cancelComments);
          break;
          
        case ACTION_COMPLETE:
          QueueTools.moveQueueItem(id, type, MesQueues.Q_ACCT_CHANGE_COMPLETE, user, completeComments);
          break;
          
        case ACTION_CREDIT:
          QueueTools.moveQueueItem(id, type, MesQueues.Q_ACCT_CHANGE_CREDIT, user, creditComments);
          break;
        
        case ACTION_SETUP:
          QueueTools.moveQueueItem(id, type, MesQueues.Q_ACCT_CHANGE_SETUP, user, setupComments);
          break;
        
        case ACTION_DEPLOYMENT:
          QueueTools.moveQueueItem(id, type, MesQueues.Q_ACCT_CHANGE_DEPLOYMENT, user, deploymentComments);
          break;
          
        case ACTION_ACTIVATION:
          QueueTools.moveQueueItem(id, type, MesQueues.Q_ACCT_CHANGE_ACTIVATION, user, activationComments);
          break;
          
        case ACTION_ADJUSTMENTS:
          QueueTools.moveQueueItem(id, type, MesQueues.Q_ACCT_CHANGE_ADJUSTMENTS, user, adjustmentComments);
          break;
          
        case ACTION_ACCT_ENTRY:
          QueueTools.moveQueueItem(id, type, MesQueues.Q_ACCT_CHANGE_ACCT_ENTRY, user, acctEntryComments);
          break;
          
        case ACTION_MMS:
          QueueTools.moveQueueItem(id, type, MesQueues.Q_ACCT_CHANGE_MMS, user, mmsComments);
          break;
        
        default:
          errors.add("Please choose an action for this item");
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("submitData()", e.toString());
    }
  }
  
  public void setAction(String action)
  {
    try
    {
      if(action != null)
      {
        this.action = Integer.parseInt(action);
      }
    }
    catch(Exception e)
    {
      logEntry("setAction(" + action + ")", e.toString());
    }
  }
  public int getAction()
  {
    return this.action;
  }
  
  public void setPendComments(String pendComments)
  {
    this.pendComments = "PENDED: " + pendComments;
  }
  public String getPendComments()
  {
    return this.pendComments;
  }
  
  public void setCancelComments(String cancelComments)
  {
    this.cancelComments = "CANCELLED: " + cancelComments;
  }
  public String getCancelComments()
  {
    return this.cancelComments;
  }
  
  public void setCompleteComments(String completeComments)
  {
    this.completeComments = "COMPLETED: " + completeComments;
  }
  public String getCompleteComments()
  {
    return this.completeComments;
  }
  
  public void setCreditComments(String creditComments)
  {
    this.creditComments = creditComments;
  }
  public String getCreditComments()
  {
    return this.creditComments;
  }
  
  public void setSetupComments(String setupComments)
  {
    this.setupComments = setupComments;
  }
  public String getSetupComments()
  {
    return this.setupComments;
  }
  
  public void setDeploymentComments(String deploymentComments)
  {
    this.deploymentComments = deploymentComments;
  }
  public String getDeploymentComments()
  {
    return this.deploymentComments;
  }
  
  public void setActivationComments(String activationComments)
  {
    this.activationComments = activationComments;
  }
  public String getActivationComments()
  {
    return this.activationComments;
  }                                          
  
  public void setAdjustmentComments(String adjustmentComments)
  {
    this.adjustmentComments = adjustmentComments;
  }
  public String getAdjustmentComments()
  {
    return this.adjustmentComments;
  }
  
  public void setAcctEntryComments(String acctEntryComments)
  {
    this.acctEntryComments = acctEntryComments;
  }
  public String getAcctEntryComments()
  {
    return this.acctEntryComments;
  }
  
  public void setMmsComments(String mmsComments)
  {
    this.mmsComments = mmsComments;
  }
  public String getMmsComments()
  {
    return this.mmsComments;
  }
  
  public String getQueueDescription()
  {
    return this.queueDescription;
  }
  
  public void setSubmit(String submit)
  {
    this.submitted = true;
  }
  
  public boolean isSubmitted()
  {
    return this.submitted;
  }
  
  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }
  public Vector getErrors()
  {
    return this.errors;
  }
}
