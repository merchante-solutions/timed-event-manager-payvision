/*@lineinfo:filename=DebitExceptionQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/DebitExceptionQueue.sqlj $

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2007-03-22 16:29:22 -0700 (Thu, 22 Mar 2007) $
  Version            : $Revision: 13582 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.sql.Date;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.constants.MesUsers;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.RadioButtonField;
import com.mes.queues.ExtraColumnBean.LinkData;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.SyncLog;
import com.mes.user.UserBean;

public class DebitExceptionQueue extends QueueBase
{
  public static final int       AS_NONE     = 0;
  public static final int       AS_WEB      = 1;
  
  public static class DebitExceptionQueueData extends QueueData
  {
    public  String        CardNumber              = null;
    public  Date          MatchDate               = null;
    public  String        ReferenceNumber         = null;
    public  String        SaleUrl                 = null;
    public  String        TraceNumber             = null;
    public  String        TranId                  = null;
    public  double        TranAmount              = 0.0;
    public  Date          TranDate                = null;
    
    public void setExtendedData(ResultSet resultSet)
    {
      StringBuffer      temp       = new StringBuffer();
      
      try
      {
        CardNumber          = processString(resultSet.getString("card_number"));
        MatchDate           = resultSet.getDate("dt_batch_date");
        ReferenceNumber     = processString(resultSet.getString("ref_num"));
        TraceNumber         = resultSet.getString("trace_number");
        TranAmount          = resultSet.getDouble("tran_amount");
        TranDate            = resultSet.getDate("tran_date");
        TranId              = resultSet.getString("tran_id");
        
        if ( resultSet.getInt("tran_data_valid") == 1 )
        {
          temp.setLength(0);
          temp.append("/jsp/utils/find_transaction.jsp?dataType=0&reportType=1&findTran=true");
          temp.append("&findHierarchyNode=");
          temp.append(resultSet.getLong("merchant_number"));
          temp.append("&reportDateBegin=");
          temp.append(DateTimeFormatter.getFormattedDate(MatchDate,"MM/dd/yyyy"));
          temp.append("&reportDateEnd=");
          temp.append(DateTimeFormatter.getFormattedDate(MatchDate,"MM/dd/yyyy"));
          temp.append("&findCardNum=");
          temp.append(CardNumber);
          
          SaleUrl = temp.toString();
        }
      }
      catch(Exception e)
      {
        SyncLog.LogEntry(this.getClass().getName() + "::setExtendedData()", e.toString());
      }
    }
  }     
  
  public DebitExceptionQueue( )
  {
    FieldGroup      searchFields  = initSearchFields();
    
    searchFields.add( new DateField( "searchBeginDate","Incoming Date From: ",true ) );
    ((DateField)searchFields.getField("searchBeginDate")).setDateFormat("MM/dd/yyyy");
    searchFields.add( new DateField( "searchEndDate","To:", true ) );
    ((DateField)searchFields.getField("searchEndDate")).setDateFormat("MM/dd/yyyy");
    searchFields.add( new Field    ( "searchValue","Search Criteria:", 50, 50, true ) );
  }
  
  public boolean isSubmitAllowed( UserBean user )
  {
    boolean result = true;
    
    try
    {
      // if user has restriction then only allow access to Held for Finance queue
      if( user.hasRight(MesUsers.RIGHT_RESTRICT_DEBIT_EXCEPTIONS) )
      {
        if( getType() != MesQueues.Q_DEBIT_EXCEPTION_HELD )
        {
          result = false;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("isSubmitAllowed(" + getType() +")", e.toString());
      result = true;
    }
    
    return( result );
  }
  
  public void doAction( HttpServletRequest request )
  {
    String            actionCode    = null;
    RadioButtonField  actionField   = null;
    int               docInd        = 0;
    long              itemId        = HttpHelper.getLong(request, "id", -1);
    String            fcode         = null;
    Field             field         = null;
    StringBuffer      msg           = new StringBuffer();
    int               recCount      = 0;
    String            reprCode      = "0";
    String            userLogin     = null;
    int               workedId      = 0;
    boolean           queryActionCode = true;

    super.doAction(request);

    try
    {
      connect();

      try
      {
        userLogin = user.getLoginName();
      }
      catch(Exception e)
      {
        userLogin = "WebQueue";
      }

      actionField = (RadioButtonField)getField("action");

      if( actionField.isValid() )
      {
        int toQueue = actionField.asInteger();
        
        // queue type all is used to re-pend worked items that
        // have not been processed (sent to TSYS or Visa).
        if ( toQueue == MesQueues.Q_DEBIT_EXCEPTION_ALL )
        {
          // try to move this item back to pending
          pendException(itemId);
          
          // update with the new queue type
          /*@lineinfo:generated-code*//*@lineinfo:173^11*/

//  ************************************************************
//  #sql [Ctx] { select  qd.type   
//              from    q_data  qd
//              where   qd.id = :itemId and
//                      qd.item_type = :MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS -- 39
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  qd.type    \n            from    q_data  qd\n            where   qd.id =  :1  and\n                    qd.item_type =  :2  -- 39";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.DebitExceptionQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   __sJT_st.setInt(2,MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   toQueue = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:179^11*/
          actionField.setData(Integer.toString(toQueue));
        }
        else    // standard queue handling
        {
          /*@lineinfo:generated-code*//*@lineinfo:184^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(tda.rec_id)   
//              from    trident_debit_activity  tda
//              where   tda.rec_id = :itemId and
//                      tda.action_code = :actionCode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(tda.rec_id)    \n            from    trident_debit_activity  tda\n            where   tda.rec_id =  :1  and\n                    tda.action_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.DebitExceptionQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   __sJT_st.setString(2,actionCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:190^11*/
        
          int validCount = 0;
          /*@lineinfo:generated-code*//*@lineinfo:193^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(ac.short_action_code)
//              
//              from    trident_debit_action_codes ac
//              where   ac.queue_type = :toQueue and
//                      nvl( ac.queue_action, 'N') = 'Y'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ac.short_action_code)\n             \n            from    trident_debit_action_codes ac\n            where   ac.queue_type =  :1  and\n                    nvl( ac.queue_action, 'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.queues.DebitExceptionQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,toQueue);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   validCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:200^11*/
          
          if( validCount > 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:204^13*/

//  ************************************************************
//  #sql [Ctx] { select  ac.short_action_code 
//                from    trident_debit_action_codes    ac
//                where   ac.queue_type = :toQueue
//                        and nvl( ac.queue_action,'N' ) = 'Y'
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ac.short_action_code  \n              from    trident_debit_action_codes    ac\n              where   ac.queue_type =  :1 \n                      and nvl( ac.queue_action,'N' ) = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.queues.DebitExceptionQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,toQueue);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   actionCode = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:210^13*/

            if ( recCount == 0 && actionCode != null )
            {
              workedId = 0;   // reset
              do
              {
                workedId++;

                /*@lineinfo:generated-code*//*@lineinfo:219^17*/

//  ************************************************************
//  #sql [Ctx] { select  count(tda.rec_id) 
//                    from    trident_debit_activity tda
//                    where   tda.rec_id = :itemId and
//                            tda.action_rec_id = :workedId
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(tda.rec_id)  \n                  from    trident_debit_activity tda\n                  where   tda.rec_id =  :1  and\n                          tda.action_rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.queues.DebitExceptionQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   __sJT_st.setInt(2,workedId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:225^17*/
              } while ( recCount != 0 );

              /*@lineinfo:generated-code*//*@lineinfo:228^15*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_debit_activity
//                  (
//                    rec_id,
//                    action_rec_id,
//                    action_code,
//                    action_date,
//                    user_login,
//                    action_source
//                  )
//                  values
//                  (
//                    :itemId,
//                    :workedId,
//                    :actionCode,
//                    trunc(sysdate),
//                    :userLogin,
//                    :AS_WEB
//                  )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into trident_debit_activity\n                (\n                  rec_id,\n                  action_rec_id,\n                  action_code,\n                  action_date,\n                  user_login,\n                  action_source\n                )\n                values\n                (\n                   :1 ,\n                   :2 ,\n                   :3 ,\n                  trunc(sysdate),\n                   :4 ,\n                   :5 \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.queues.DebitExceptionQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   __sJT_st.setInt(2,workedId);
   __sJT_st.setString(3,actionCode);
   __sJT_st.setString(4,userLogin);
   __sJT_st.setInt(5,AS_WEB);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:248^15*/

              /*@lineinfo:generated-code*//*@lineinfo:250^15*/

//  ************************************************************
//  #sql [Ctx] { commit
//                 };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:253^15*/
            }   // if ( validCount > 0 )
          }     // if ( toQueue == MesQueues.Q_DEBIT_EXCEPTION_ALL ) 
        }       // if ( recCount == 0 && actionCode != null )
      }         // if ( actionField.isValid() )
    }
    catch( Exception e )
    {
      e.printStackTrace();
      logEntry( "doAction()", e.toString() );
    }
    finally
    {
      cleanUp();
    }
  }
  
  public String getActionDisplayName( )
  {
    return( "Debit Exception Status" );
  }
  
  public String getBackLink()
  {
    return("/jsp/reports/index.jsp?com.mes.ReportMenuId=" + getMenuId());
  }
  
  public String getDescriptionURL(QueueData qd)
  {
    long              merchantId  = 0L;
    String            retVal      = null;
    StringBuffer      temp        = new StringBuffer();
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:290^7*/

//  ************************************************************
//  #sql [Ctx] { select  dbt.merchant_number  
//          from    trident_debit_financial dbt
//          where   dbt.rec_id = :qd.getId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_498 = qd.getId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  dbt.merchant_number   \n        from    trident_debit_financial dbt\n        where   dbt.rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.queues.DebitExceptionQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_498);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:295^7*/
      
      if ( merchantId != 0L )
      {
        temp.append("/jsp/maintenance/view_account.jsp?merchant=");
        temp.append(merchantId);
        retVal = temp.toString();
      }
    }
    catch( Exception e )
    {
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected QueueData getEmptyQueueData()
  {
    QueueData       retVal = new DebitExceptionQueueData();
    retVal.setDateFormat("MM/dd/yyyy");
    return( retVal );
  }
  
  public String getGenericSearchDesc()
  {
    StringBuffer          desc      = new StringBuffer();
    
    
    desc.append("<ul>");
    desc.append("  <li>Merchant Number</li>");
    desc.append("  <li>Merchant DBA Name (partial is OK)</li>");
    desc.append("  <li>Case #</li>");
    desc.append("  <li>Reference Number</li>");
    desc.append("  <li>Transaction Amount</li>");
    desc.append("  <li>Card Number (min first 6)</li>");
    desc.append("</ul>");
    
    return( desc.toString() );
  }
  
  public int getMenuId()
  {
    return( MesMenus.MENU_ID_DEBIT_EXCEPTION_QUEUES );
  }
  
  protected void loadExtraColumns(ExtraColumnBean extraColumns, UserBean user)
  {
    LinkData        linkData    = null;
  
    try
    {
      // add the extra columns
      if ( getType() == MesQueues.Q_DEBIT_EXCEPTION_ALL )
      {
        extraColumns.addColumn("Queue", "queue_desc");
      }
      extraColumns.addColumn("Merch #", "merchant_number");
      extraColumns.addColumn("Card #", "card_number");
      extraColumns.addColumn("Amount","tran_amount","","","C");
      extraColumns.addColumn("Tran Date","tran_date_str");
      
      // setup the tran ref # column as a link to 
      // the find transaction screen.
      linkData = extraColumns.getNewLinkData();
      linkData.addLink("findHierarchyNode","merchant_number");
      linkData.addLink("findAuthCode","auth_code");
      linkData.addLink("reportDateBegin","tran_date_str");
      linkData.addLink("reportDateEnd","tran_date_str");
      linkData.addLink("findCardNum","card_number");
      extraColumns.addColumn("Auth Code", "auth_code", "/jsp/utils/find_auth.jsp?reportType=1&findAuth=true", "_findAuth", null, linkData);
      
      extraColumns.addColumn("Retrieval Ref #","ref_num");
      extraColumns.addColumn("Tran ID","tran_id");
      extraColumns.addColumn("Trace #","trace_number");
    }
    catch(Exception e)
    {
      logEntry("loadExtraColumns()", e.toString());
    }
  }
  
  protected void loadQueueData(UserBean user)
  {
    int             bankNumber    = 0; 
    double          doubleVal     = 0.0;
    long            longVal       = -1L;
    FieldGroup      searchFields  = (FieldGroup)fields.getField("searchFields");
    
    try
    {
      bankNumber  = Integer.parseInt(Long.toString(user.getHierarchyNode()).substring(0,4));
      
      Date    searchBeginDate   = ((DateField)searchFields.getField( "searchBeginDate" )).getSqlDate();
      Date    searchEndDate     = ((DateField)searchFields.getField( "searchEndDate" )).getSqlDate();
      String  searchValue       = searchFields.getField( "searchValue" ).getData();
      
      // get the value as a long if possible
      try{ longVal = Long.parseLong(searchValue); } catch( Exception e ) { longVal = -1L; }
      try{ doubleVal = Double.parseDouble(searchValue); } catch( Exception e ) { doubleVal = 0.0; }
      
      /*@lineinfo:generated-code*//*@lineinfo:398^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    -- required fields
//                    qd.id                                   as id,
//                    qd.type                                 as type,
//                    qd.item_type                            as item_type,
//                    qd.owner                                as owner,
//                    qd.id                                   as control,
//                    dbt.card_acceptor_name                  as description,
//                    trunc(qd.date_created)                  as date_created,
//                    qd.source                               as source,
//                    qd.affiliate                            as affiliate,
//                    qd.last_changed                         as last_changed,
//                    qd.last_user                            as last_user,
//                    qd.locked_by                            as locked_by,
//                    nvl(qn.note_count,0)                    as note_count,
//                    qt.status                               as status,
//                    qt.short_desc                           as queue_desc,
//                    -- options fields
//                    dbt.merchant_number                     as merchant_number,
//                    -- extended fields
//                    dbt.card_number                         as card_number,
//                    to_char(dbt.local_transaction_date,'mm/dd/yyyy')  as tran_date_str,
//                    dbt.local_transaction_date              as tran_date,
//                    dbt.authorization_id_resp_code          as auth_code,
//                    dbt.retrieval_reference_number          as ref_num,                  
//                    dbt.transaction_amount                  as tran_amount,
//                    dbt.transaction_identifier              as tran_id,
//                    lpad(dbt.trace_number,6,'0')            as trace_number,
//                    dbt.rec_id                              as load_sec,
//                    decode( dbt.dt_batch_date,
//                            null, 0, 1 )                    as tran_data_valid,
//                    dbt.dt_batch_date                       as dt_batch_date,
//                    dbt.dt_batch_number                     as dt_batch_number,
//                    dbt.dt_ddf_dt_id                        as dt_ddf_dt_id
//          from      q_data                  qd,
//                    ( select    /*+ use_nl(qg qnote) */
//                                qnote.id,
//                                count(*)  note_count
//                      from      q_group_to_types    qg,
//                                q_notes             qnote
//                      where     qg.group_type = :MesQueues.QG_DEBIT_EXCEPTION_QUEUES and -- 13 and
//                                qnote.type = qg.queue_type and
//                                qnote.item_type = :MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS
//                      group by  qnote.id )  qn,
//                    q_types                 qt,
//                    trident_debit_financial dbt,
//                    users                   u,
//                    mif                     mf,
//                    t_hierarchy             th
//          where     ( 
//                      qd.type = :this.type or
//                      ( 
//                        :this.type = :MesQueues.Q_DEBIT_EXCEPTION_ALL and -- 1800 and
//                        not :searchValue is null and
//                        qd.item_type = :MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS -- 39
//                      )
//                    ) and 
//                    ( 
//                      ( :searchBeginDate is null and
//                        trunc(qd.date_created) >= (trunc(sysdate)-180) ) or
//                      ( trunc(qd.date_created) between :searchBeginDate and :searchEndDate )
//                    ) and
//                    (
//                      ( :searchValue is null ) or
//                      ( dbt.merchant_number = :longVal ) or
//                      ( qd.id = :longVal ) or
//                      ( to_char(dbt.retrieval_reference_number) = :searchValue  ) or
//                      ( dbt.transaction_amount = :doubleVal ) or 
//                      ( lower(dbt.card_acceptor_name) like lower('%' || :searchValue || '%') ) or
//                      ( dbt.card_number like ( substr(:searchValue,1,6) || 'xxxxxx' ||
//                                               decode( length(:searchValue),
//                                                       16, substr(:searchValue,-4),
//                                                       '%' ) ) )
//                    ) and
//                    qd.type = qt.type and 
//                    qn.id(+) = qd.id and
//                    dbt.rec_id = qd.id and
//                    u.login_name(+) = qd.source and
//                    dbt.merchant_number = mf.merchant_number and
//                    mf.association_node = th.descendent and
//                    th.ancestor = :user.getHierarchyNode()
//          order by  (trunc(sysdate) - trunc(qd.date_created)) desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_499 = user.getHierarchyNode();
  try {
   String theSqlTS = "select    -- required fields\n                  qd.id                                   as id,\n                  qd.type                                 as type,\n                  qd.item_type                            as item_type,\n                  qd.owner                                as owner,\n                  qd.id                                   as control,\n                  dbt.card_acceptor_name                  as description,\n                  trunc(qd.date_created)                  as date_created,\n                  qd.source                               as source,\n                  qd.affiliate                            as affiliate,\n                  qd.last_changed                         as last_changed,\n                  qd.last_user                            as last_user,\n                  qd.locked_by                            as locked_by,\n                  nvl(qn.note_count,0)                    as note_count,\n                  qt.status                               as status,\n                  qt.short_desc                           as queue_desc,\n                  -- options fields\n                  dbt.merchant_number                     as merchant_number,\n                  -- extended fields\n                  dbt.card_number                         as card_number,\n                  to_char(dbt.local_transaction_date,'mm/dd/yyyy')  as tran_date_str,\n                  dbt.local_transaction_date              as tran_date,\n                  dbt.authorization_id_resp_code          as auth_code,\n                  dbt.retrieval_reference_number          as ref_num,                  \n                  dbt.transaction_amount                  as tran_amount,\n                  dbt.transaction_identifier              as tran_id,\n                  lpad(dbt.trace_number,6,'0')            as trace_number,\n                  dbt.rec_id                              as load_sec,\n                  decode( dbt.dt_batch_date,\n                          null, 0, 1 )                    as tran_data_valid,\n                  dbt.dt_batch_date                       as dt_batch_date,\n                  dbt.dt_batch_number                     as dt_batch_number,\n                  dbt.dt_ddf_dt_id                        as dt_ddf_dt_id\n        from      q_data                  qd,\n                  ( select    /*+ use_nl(qg qnote) */\n                              qnote.id,\n                              count(*)  note_count\n                    from      q_group_to_types    qg,\n                              q_notes             qnote\n                    where     qg.group_type =  :1  and -- 13 and\n                              qnote.type = qg.queue_type and\n                              qnote.item_type =  :2 \n                    group by  qnote.id )  qn,\n                  q_types                 qt,\n                  trident_debit_financial dbt,\n                  users                   u,\n                  mif                     mf,\n                  t_hierarchy             th\n        where     ( \n                    qd.type =  :3  or\n                    ( \n                       :4  =  :5  and -- 1800 and\n                      not  :6  is null and\n                      qd.item_type =  :7  -- 39\n                    )\n                  ) and \n                  ( \n                    (  :8  is null and\n                      trunc(qd.date_created) >= (trunc(sysdate)-180) ) or\n                    ( trunc(qd.date_created) between  :9  and  :10  )\n                  ) and\n                  (\n                    (  :11  is null ) or\n                    ( dbt.merchant_number =  :12  ) or\n                    ( qd.id =  :13  ) or\n                    ( to_char(dbt.retrieval_reference_number) =  :14   ) or\n                    ( dbt.transaction_amount =  :15  ) or \n                    ( lower(dbt.card_acceptor_name) like lower('%' ||  :16  || '%') ) or\n                    ( dbt.card_number like ( substr( :17 ,1,6) || 'xxxxxx' ||\n                                             decode( length( :18 ),\n                                                     16, substr( :19 ,-4),\n                                                     '%' ) ) )\n                  ) and\n                  qd.type = qt.type and \n                  qn.id(+) = qd.id and\n                  dbt.rec_id = qd.id and\n                  u.login_name(+) = qd.source and\n                  dbt.merchant_number = mf.merchant_number and\n                  mf.association_node = th.descendent and\n                  th.ancestor =  :20 \n        order by  (trunc(sysdate) - trunc(qd.date_created)) desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.queues.DebitExceptionQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.QG_DEBIT_EXCEPTION_QUEUES);
   __sJT_st.setInt(2,MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS);
   __sJT_st.setInt(3,this.type);
   __sJT_st.setInt(4,this.type);
   __sJT_st.setInt(5,MesQueues.Q_DEBIT_EXCEPTION_ALL);
   __sJT_st.setString(6,searchValue);
   __sJT_st.setInt(7,MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS);
   __sJT_st.setDate(8,searchBeginDate);
   __sJT_st.setDate(9,searchBeginDate);
   __sJT_st.setDate(10,searchEndDate);
   __sJT_st.setString(11,searchValue);
   __sJT_st.setLong(12,longVal);
   __sJT_st.setLong(13,longVal);
   __sJT_st.setString(14,searchValue);
   __sJT_st.setDouble(15,doubleVal);
   __sJT_st.setString(16,searchValue);
   __sJT_st.setString(17,searchValue);
   __sJT_st.setString(18,searchValue);
   __sJT_st.setString(19,searchValue);
   __sJT_st.setLong(20,__sJT_499);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.queues.DebitExceptionQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:481^7*/
    }
    catch(Exception e)
    {
      logEntry("getQueueData(" + user.getLoginName() + ")", e.toString());
    }
  }
  
  protected void loadQueueItem(long id)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:493^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    -- required fields
//                    qd.id                                   as id,
//                    qd.type                                 as type,
//                    qd.item_type                            as item_type,
//                    qd.owner                                as owner,
//                    qd.id                                   as control,
//                    dbt.card_acceptor_name                  as description,
//                    trunc(qd.date_created)                  as date_created,
//                    qd.source                               as source,
//                    qd.affiliate                            as affiliate,
//                    qd.last_changed                         as last_changed,
//                    qd.last_user                            as last_user,
//                    qd.locked_by                            as locked_by,
//                    nvl(qn.note_count,0)                    as note_count,
//                    qt.status                               as status,
//                    qt.short_desc                           as queue_desc,
//                    -- options fields
//                    dbt.merchant_number                     as merchant_number,
//                    -- extended fields
//                    dbt.card_number                         as card_number,
//                    to_char(dbt.local_transaction_date,'mm/dd/yyyy') as tran_date_str,
//                    dbt.local_transaction_date              as tran_date,
//                    dbt.authorization_id_resp_code          as auth_code,
//                    dbt.retrieval_reference_number          as ref_num,                  
//                    dbt.transaction_amount                  as tran_amount,
//                    dbt.transaction_identifier              as tran_id,
//                    lpad(dbt.trace_number,6,'0')            as trace_number,
//                    dbt.rec_id                              as load_sec,
//                    decode( dbt.dt_batch_date,
//                            null, 0, 1 )                    as tran_data_valid,
//                    dbt.dt_batch_date                       as dt_batch_date,
//                    dbt.dt_batch_number                     as dt_batch_number,
//                    dbt.dt_ddf_dt_id                        as dt_ddf_dt_id
//          from      q_data                  qd,
//                    ( select    qnote.id,
//                                count(*)  note_count
//                      from      q_group_to_types    qg,
//                                q_notes             qnote
//                      where     qg.group_type = :MesQueues.QG_DEBIT_EXCEPTION_QUEUES and -- 13 and
//                                qnote.type = qg.queue_type
//                      group by  qnote.id )  qn,
//                    q_types                 qt,
//                    trident_debit_financial dbt,
//                    users                   u
//          where     ( 
//                      qd.type = :this.type or
//                      ( 
//                        :this.type = :MesQueues.Q_DEBIT_EXCEPTION_ALL and -- 1800 and
//                        qd.item_type = :MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS -- 39
//                      )
//                    ) and 
//                    qd.id = :id and
//                    qd.type = qt.type and 
//                    qn.id(+) = qd.id and
//                    dbt.rec_id = qd.id and
//                    u.login_name(+) = qd.source
//          order by  (trunc(sysdate) - trunc(qd.date_created)) desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    -- required fields\n                  qd.id                                   as id,\n                  qd.type                                 as type,\n                  qd.item_type                            as item_type,\n                  qd.owner                                as owner,\n                  qd.id                                   as control,\n                  dbt.card_acceptor_name                  as description,\n                  trunc(qd.date_created)                  as date_created,\n                  qd.source                               as source,\n                  qd.affiliate                            as affiliate,\n                  qd.last_changed                         as last_changed,\n                  qd.last_user                            as last_user,\n                  qd.locked_by                            as locked_by,\n                  nvl(qn.note_count,0)                    as note_count,\n                  qt.status                               as status,\n                  qt.short_desc                           as queue_desc,\n                  -- options fields\n                  dbt.merchant_number                     as merchant_number,\n                  -- extended fields\n                  dbt.card_number                         as card_number,\n                  to_char(dbt.local_transaction_date,'mm/dd/yyyy') as tran_date_str,\n                  dbt.local_transaction_date              as tran_date,\n                  dbt.authorization_id_resp_code          as auth_code,\n                  dbt.retrieval_reference_number          as ref_num,                  \n                  dbt.transaction_amount                  as tran_amount,\n                  dbt.transaction_identifier              as tran_id,\n                  lpad(dbt.trace_number,6,'0')            as trace_number,\n                  dbt.rec_id                              as load_sec,\n                  decode( dbt.dt_batch_date,\n                          null, 0, 1 )                    as tran_data_valid,\n                  dbt.dt_batch_date                       as dt_batch_date,\n                  dbt.dt_batch_number                     as dt_batch_number,\n                  dbt.dt_ddf_dt_id                        as dt_ddf_dt_id\n        from      q_data                  qd,\n                  ( select    qnote.id,\n                              count(*)  note_count\n                    from      q_group_to_types    qg,\n                              q_notes             qnote\n                    where     qg.group_type =  :1  and -- 13 and\n                              qnote.type = qg.queue_type\n                    group by  qnote.id )  qn,\n                  q_types                 qt,\n                  trident_debit_financial dbt,\n                  users                   u\n        where     ( \n                    qd.type =  :2  or\n                    ( \n                       :3  =  :4  and -- 1800 and\n                      qd.item_type =  :5  -- 39\n                    )\n                  ) and \n                  qd.id =  :6  and\n                  qd.type = qt.type and \n                  qn.id(+) = qd.id and\n                  dbt.rec_id = qd.id and\n                  u.login_name(+) = qd.source\n        order by  (trunc(sysdate) - trunc(qd.date_created)) desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.queues.DebitExceptionQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.QG_DEBIT_EXCEPTION_QUEUES);
   __sJT_st.setInt(2,this.type);
   __sJT_st.setInt(3,this.type);
   __sJT_st.setInt(4,MesQueues.Q_DEBIT_EXCEPTION_ALL);
   __sJT_st.setInt(5,MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS);
   __sJT_st.setLong(6,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.queues.DebitExceptionQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:552^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueItem(" + this.type + ", " + id + ")", e.toString());
    }
  }
  
  protected void pendException( long itemId )
  {
    int       moveOk    = 0;
    int       toQueue   = -1;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:567^7*/

//  ************************************************************
//  #sql [Ctx] { select  decode(dt.dt_batch_date,
//                         null,:MesQueues.Q_DEBIT_EXCEPTION_MERCH_NOT_FUNDED,
//                         :MesQueues.Q_DEBIT_EXCEPTION_MES_NOT_FUNDED),
//                  decode(tda.load_filename,null,1,0)
//          
//          from    trident_debit_financial dt,
//                  trident_debit_activity  tda
//          where   dt.rec_id = :itemId and
//                  tda.rec_id(+) = dt.rec_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode(dt.dt_batch_date,\n                       null, :1 ,\n                        :2 ),\n                decode(tda.load_filename,null,1,0)\n         \n        from    trident_debit_financial dt,\n                trident_debit_activity  tda\n        where   dt.rec_id =  :3  and\n                tda.rec_id(+) = dt.rec_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.queues.DebitExceptionQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_DEBIT_EXCEPTION_MERCH_NOT_FUNDED);
   __sJT_st.setInt(2,MesQueues.Q_DEBIT_EXCEPTION_MES_NOT_FUNDED);
   __sJT_st.setLong(3,itemId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   toQueue = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   moveOk = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:578^7*/
      
      if ( moveOk == 1 )
      {
        // remove unprocessed entry from activity table
        /*@lineinfo:generated-code*//*@lineinfo:583^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    trident_debit_activity tda
//            where   tda.rec_id = :itemId and
//                    tda.load_filename is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    trident_debit_activity tda\n          where   tda.rec_id =  :1  and\n                  tda.load_filename is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.queues.DebitExceptionQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,itemId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:589^9*/
    
        // move the entry back to the incoming queue
        /*@lineinfo:generated-code*//*@lineinfo:592^9*/

//  ************************************************************
//  #sql [Ctx] { update  q_data
//            set     type = :toQueue
//            where   id = :itemId and
//                    item_type = :MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS -- 39
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  q_data\n          set     type =  :1 \n          where   id =  :2  and\n                  item_type =  :3  -- 39";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.queues.DebitExceptionQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,toQueue);
   __sJT_st.setLong(2,itemId);
   __sJT_st.setInt(3,MesQueues.Q_ITEM_TYPE_DEBIT_EXCEPTIONS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:598^9*/
      }
    }
    catch( Exception e )
    {
      logEntry("pendException( " + itemId + " )", e.toString());
    }
  }
  
  public void setType( int newType )
  {
    String[][]          buttons     = null;
    
    super.setType( newType );
    
    // setup the options based on type
    switch( newType )
    {
      case MesQueues.Q_DEBIT_EXCEPTION_MERCH_NOT_FUNDED:
        buttons = 
          new String[][]
          {
            { "Fund Merchant (DCE)",      Integer.toString(MesQueues.Q_DEBIT_EXCEPTION_MERCHANT_CREDIT) },
            { "Refund Cardholder (VEX)",  Integer.toString(MesQueues.Q_DEBIT_EXCEPTION_CH_CREDIT) },
            { "Refer to Finance",         Integer.toString(MesQueues.Q_DEBIT_EXCEPTION_HELD) },
          };
        break;
        
      case MesQueues.Q_DEBIT_EXCEPTION_MES_NOT_FUNDED:
        buttons = 
          new String[][]
          {
            { "Debit Merchant (DCE)",   Integer.toString(MesQueues.Q_DEBIT_EXCEPTION_MERCHANT_DEBIT) },
            { "Debit Cardholder (VEX)", Integer.toString(MesQueues.Q_DEBIT_EXCEPTION_CH_DEBIT) },
            { "Write Off (LOSS)",       Integer.toString(MesQueues.Q_DEBIT_EXCEPTION_LOSS) },
          };
        break;
        
      case MesQueues.Q_DEBIT_EXCEPTION_MERCHANT_CREDIT:
      case MesQueues.Q_DEBIT_EXCEPTION_MERCHANT_DEBIT:
      case MesQueues.Q_DEBIT_EXCEPTION_CH_CREDIT:
      case MesQueues.Q_DEBIT_EXCEPTION_CH_DEBIT:
        buttons = 
          new String[][]
          {
            { "Pend (PEND) * Same day only", Integer.toString(MesQueues.Q_DEBIT_EXCEPTION_ALL) }
          };
        break;
        
      case MesQueues.Q_DEBIT_EXCEPTION_HELD:
        buttons = 
          new String[][]
          {
            { "Move Back to Incoming Queue", Integer.toString(MesQueues.Q_DEBIT_EXCEPTION_MERCH_NOT_FUNDED) },
            { "Write Off (LOSS)",       Integer.toString(MesQueues.Q_DEBIT_EXCEPTION_LOSS) },
          };
        break;
        
//      case MesQueues.Q_DEBIT_EXCEPTION_LOSS:
      default:
        break;
    }

    // if there are actions provide a way to select and provide comments
    if ( buttons != null )
    {
      fields.add( new RadioButtonField("action", buttons, -1, false, "Must select an action" ) );
//@      fields.add(new TextareaField("actionComments",500,8,80,true));
    }
  }
}/*@lineinfo:generated-code*/