/*@lineinfo:filename=CbtCreditQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/CbtCreditQueue.sqlj $

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 4/03/03 5:18p $
  Version            : $Revision: 10 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.sql.ResultSet;
import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.forms.ComboDateField;
import com.mes.support.SyncLog;
import com.mes.user.UserBean;

public class CbtCreditQueue extends QueueBase
{
  public boolean dateRangeEnabled()
  {
    return true;
  }
  
  protected void loadQueueData(UserBean user)
  {
    try
    {
      java.sql.Date fromDate = 
        ((ComboDateField)fields.getField("fromDate")).getSqlDate();
      java.sql.Date toDate = 
        ((ComboDateField)fields.getField("toDate")).getSqlDate();
        
      /*@lineinfo:generated-code*//*@lineinfo:47^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    qd.id                 id,
//                    m.merch_number        merchant_number,
//                    qd.type               type,
//                    qd.item_type          item_type,
//                    qd.owner              owner,
//                    qd.date_created       date_created,
//                    qd.source             source,
//                    qd.affiliate          affiliate,
//                    qd.last_changed       last_changed,
//                    qd.last_user          last_user,
//                    m.merch_business_name description,
//                    m.merch_email_address merch_email,
//                    u.name                rep_name,
//                    u.email               rep_email,
//                    qd.id                 control,
//                    qd.locked_by          locked_by,
//                    nvl(qn.note_count,0)  note_count,
//                    qt.status             status,
//                    m.cbt_credit_score    score,
//                    to_char( m.merch_month_visa_mc_sales * 12, 'L9G999G999D99' )
//                                          volume
//  
//          from      q_data                qd,
//                    ( select    id,
//                                count(*)  note_count
//                      from      q_notes
//                      group by id )       qn,
//                    q_types               qt,
//                    merchant              m,
//                    users                 u
//  
//          where     qd.type = :this.type
//                    and qd.type = qt.type
//                    and qd.id = qn.id(+)
//                    and m.app_seq_num = qd.id
//                    and qd.source = u.login_name(+)
//                    and trunc(qd.date_created) between :fromDate and :toDate
//  
//          order by  qd.date_created asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    qd.id                 id,\n                  m.merch_number        merchant_number,\n                  qd.type               type,\n                  qd.item_type          item_type,\n                  qd.owner              owner,\n                  qd.date_created       date_created,\n                  qd.source             source,\n                  qd.affiliate          affiliate,\n                  qd.last_changed       last_changed,\n                  qd.last_user          last_user,\n                  m.merch_business_name description,\n                  m.merch_email_address merch_email,\n                  u.name                rep_name,\n                  u.email               rep_email,\n                  qd.id                 control,\n                  qd.locked_by          locked_by,\n                  nvl(qn.note_count,0)  note_count,\n                  qt.status             status,\n                  m.cbt_credit_score    score,\n                  to_char( m.merch_month_visa_mc_sales * 12, 'L9G999G999D99' )\n                                        volume\n\n        from      q_data                qd,\n                  ( select    id,\n                              count(*)  note_count\n                    from      q_notes\n                    group by id )       qn,\n                  q_types               qt,\n                  merchant              m,\n                  users                 u\n\n        where     qd.type =  :1 \n                  and qd.type = qt.type\n                  and qd.id = qn.id(+)\n                  and m.app_seq_num = qd.id\n                  and qd.source = u.login_name(+)\n                  and trunc(qd.date_created) between  :2  and  :3 \n\n        order by  qd.date_created asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.CbtCreditQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setDate(2,fromDate);
   __sJT_st.setDate(3,toDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.queues.CbtCreditQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:88^7*/
    }
    catch(Exception e)
    {
      logEntry("getQueueData(" + user.getLoginName() + ")", e.toString());
    }
  }
  
  protected void loadQueueItem(long id)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:100^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    qd.id                 id,
//                    m.merch_number        merchant_number,
//                    qd.type               type,
//                    qd.item_type          item_type,
//                    qd.owner              owner,
//                    qd.date_created       date_created,
//                    qd.source             source,
//                    qd.affiliate          affiliate,
//                    qd.last_changed       last_changed,
//                    qd.last_user          last_user,
//                    m.merch_business_name description,
//                    m.merch_email_address merch_email,
//                    u.name                rep_name,
//                    u.email               rep_email,
//                    qd.id                 control,
//                    qd.locked_by          locked_by,
//                    nvl(qn.note_count,0)  note_count,
//                    qt.status             status,
//                    m.cbt_credit_score    score,
//                    to_char( m.merch_month_visa_mc_sales * 12, 'L9G999G999D99' )
//                                          volume
//                    
//          from      q_data                qd,
//                    ( select    id,
//                                count(*)  note_count
//                      from      q_notes
//                      group by id )       qn,
//                    q_types               qt,
//                    merchant              m,
//                    users                 u
//                    
//          where     qd.type = :this.type
//                    and qd.type = qt.type
//                    and qd.id = qn.id(+)
//                    and qd.id = :id
//                    and m.app_seq_num = qd.id
//                    and qd.source = u.login_name(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    qd.id                 id,\n                  m.merch_number        merchant_number,\n                  qd.type               type,\n                  qd.item_type          item_type,\n                  qd.owner              owner,\n                  qd.date_created       date_created,\n                  qd.source             source,\n                  qd.affiliate          affiliate,\n                  qd.last_changed       last_changed,\n                  qd.last_user          last_user,\n                  m.merch_business_name description,\n                  m.merch_email_address merch_email,\n                  u.name                rep_name,\n                  u.email               rep_email,\n                  qd.id                 control,\n                  qd.locked_by          locked_by,\n                  nvl(qn.note_count,0)  note_count,\n                  qt.status             status,\n                  m.cbt_credit_score    score,\n                  to_char( m.merch_month_visa_mc_sales * 12, 'L9G999G999D99' )\n                                        volume\n                  \n        from      q_data                qd,\n                  ( select    id,\n                              count(*)  note_count\n                    from      q_notes\n                    group by id )       qn,\n                  q_types               qt,\n                  merchant              m,\n                  users                 u\n                  \n        where     qd.type =  :1 \n                  and qd.type = qt.type\n                  and qd.id = qn.id(+)\n                  and qd.id =  :2 \n                  and m.app_seq_num = qd.id\n                  and qd.source = u.login_name(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.CbtCreditQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setLong(2,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.queues.CbtCreditQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:139^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueItem(" + this.type + ", " + id + ")", e.toString());
    }
  }
  
  public static class CbtQueueData extends QueueData
  {
    private int     score;
    private String  volume;
    private String  merchEmail;
    private String  repName;
    private String  repEmail;
    
    public void setExtendedData(ResultSet rs)
    {
      try
      {
        setScore      (rs.getInt("score"));
        setVolume     (rs.getString("volume"));
        setMerchEmail (rs.getString("merch_email"));
        setRepName    (rs.getString("rep_name"));
        setRepEmail   (rs.getString("rep_email"));
      }
      catch(Exception e)
      {
        SyncLog.LogEntry(this.getClass().getName() + "::setData()", e.toString());
      }
    }
    
    public void setScore(int score)
    {
      this.score = score;
    }
    public int getScore()
    {
      return score;
    }
    
    public void setVolume(String volume)
    {
      this.volume = volume;
    }
    public String getVolume()
    {
      return volume;
    }
    
    public void setMerchEmail(String merchEmail)
    {
      this.merchEmail = merchEmail;
    }
    public String getMerchEmail()
    {
      return merchEmail;
    }
    
    public void setRepName(String repName)
    {
      this.repName = repName;
    }
    public String getRepName()
    {
      return repName;
    }
    
    public void setRepEmail(String repEmail)
    {
      this.repEmail = repEmail;
    }
    public String getRepEmail()
    {
      return repEmail;
    }
  }
  
  protected void loadExtraColumns(ExtraColumnBean extraColumns, UserBean user)
  {
    try
    {
      // add the extra columns
      extraColumns.addColumn("Volume", "volume", "", "", "C");
      extraColumns.addColumn("Score", "score");
    }
    catch(Exception e)
    {
      logEntry("loadExtraColumns()", e.toString());
    }
  }
  
  protected QueueData getEmptyQueueData()
  {
    return new CbtQueueData();
  }
  
  public String getDescriptionURL()
  {
    return "/jsp/setup/merchinfo4.jsp?primaryKey=";
  }
  
  public int getMenuId()
  {
    int result = 0;
    
    switch (type)
    {
      case MesQueues.Q_CBT_NEW:
      case MesQueues.Q_CBT_PENDING:
      case MesQueues.Q_CBT_DECLINED:
      case MesQueues.Q_CBT_CANCELLED:
      case MesQueues.Q_CBT_APPROVED:
        result = MesMenus.MENU_ID_CBT_CREDIT_QUEUES_LV;
        break;
      
      case MesQueues.Q_CBT_NEW_HIGH_VOLUME:
      case MesQueues.Q_CBT_PENDING_HIGH_VOLUME:
      case MesQueues.Q_CBT_CANCELLED_HIGH_VOLUME:
      case MesQueues.Q_CBT_DECLINED_HIGH_VOLUME:
      case MesQueues.Q_CBT_APPROVED_HIGH_VOLUME:
        result = MesMenus.MENU_ID_CBT_CREDIT_QUEUES_HV;
        break;

      // new cb&t review queues
      case MesQueues.Q_CBT_DATA_ASSIGN_NEW:
      case MesQueues.Q_CBT_DATA_ASSIGN_COMPLETED:
      case MesQueues.Q_CBT_DATA_ASSIGN_CANCELED:
        result = MesMenus.MENU_ID_CBT_DATA_ASSIGN_QUEUES;
        break;

      case MesQueues.Q_CBT_RISKY_SIC_NEW:
      case MesQueues.Q_CBT_RISKY_SIC_APPROVED:
      case MesQueues.Q_CBT_RISKY_SIC_DECLINED:
      case MesQueues.Q_CBT_RISKY_SIC_CANCELED:
        result = MesMenus.MENU_ID_CBT_RISKY_SIC_QUEUES;
        break;

      case MesQueues.Q_CBT_PULL_CREDIT_NEW:
      case MesQueues.Q_CBT_PULL_CREDIT_COMPLETED:
      case MesQueues.Q_CBT_PULL_CREDIT_CANCELED:
        result = MesMenus.MENU_ID_CBT_PULL_CREDIT_QUEUES;
        break;

      case MesQueues.Q_CBT_LOW_SCORE_NEW:
      case MesQueues.Q_CBT_LOW_SCORE_PENDED:
      case MesQueues.Q_CBT_LOW_SCORE_APPROVED:
      case MesQueues.Q_CBT_LOW_SCORE_DECLINED:
      case MesQueues.Q_CBT_LOW_SCORE_CANCELED:
        result = MesMenus.MENU_ID_CBT_LOW_SCORE_QUEUES;
        break;

      case MesQueues.Q_CBT_HIGH_VOLUME_NEW:
      case MesQueues.Q_CBT_HIGH_VOLUME_PENDED:
      case MesQueues.Q_CBT_HIGH_VOLUME_APPROVED:
      case MesQueues.Q_CBT_HIGH_VOLUME_DECLINED:
      case MesQueues.Q_CBT_HIGH_VOLUME_CANCELED:
        result = MesMenus.MENU_ID_CBT_HIGH_VOLUME_QUEUES;
        break;

      case MesQueues.Q_CBT_LARGE_TICKET_NEW:
      case MesQueues.Q_CBT_LARGE_TICKET_PENDED:
      case MesQueues.Q_CBT_LARGE_TICKET_APPROVED:
      case MesQueues.Q_CBT_LARGE_TICKET_DECLINED:
      case MesQueues.Q_CBT_LARGE_TICKET_CANCELED:
        result = MesMenus.MENU_ID_CBT_LARGE_TICKET_QUEUES;
        break;

      case MesQueues.Q_CBT_DOC_NEW:
      case MesQueues.Q_CBT_DOC_COMPLETED:
      case MesQueues.Q_CBT_DOC_CANCELED:
      case MesQueues.Q_CBT_DOC_PENDED:
        result = MesMenus.MENU_ID_CBT_DOC_QUEUES;
        break;
    }

    return result;
  }

  public String getBackLink()
  {
    return "/jsp/menus/cbt_queue_menu.jsp?com.mes.ReportMenuId=" + getMenuId();
  }

  public String getBackLinkDesc()
  {
    String result = "";

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:331^7*/

//  ************************************************************
//  #sql [Ctx] { select  menu_title
//          
//          from    menu_types
//          where   menu_id = :getMenuId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_484 = getMenuId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  menu_title\n         \n        from    menu_types\n        where   menu_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.queues.CbtCreditQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,__sJT_484);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:337^7*/
    }
    catch(Exception e)
    {
      logEntry("getBackLinkDesc()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

}/*@lineinfo:generated-code*/