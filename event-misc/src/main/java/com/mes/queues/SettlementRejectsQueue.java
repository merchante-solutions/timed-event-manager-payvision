/*@lineinfo:filename=SettlementRejectsQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: $

  Last Modified By   : $$
  Last Modified Date : $$
  Version            : $$

  Change History:
     See VSS database

  Copyright (C) 2000-2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.TextareaField;
import com.mes.queues.ExtraColumnBean.LinkData;
import com.mes.support.HttpHelper;
import com.mes.support.StringUtilities;
import com.mes.support.SyncLog;
import com.mes.user.UserBean;

public class SettlementRejectsQueue extends QueueBase
{

  static Logger log = Logger.getLogger(SettlementRejectsQueue.class);

  //Q_ITEM_TYPE_PACKAGE_REJECTS 44
  private final int ALL     = 0;
  private final int MC      = 1;
  private final int VS      = 2;

  private int ccType  = ALL;

  public SettlementRejectsQueue()
  {
    FieldGroup      searchFields  = initSearchFields();

    searchFields.add( new DateField( "searchBeginDate","Incoming Date From: ",true ) );
    ((DateField)searchFields.getField("searchBeginDate")).setDateFormat("MM/dd/yyyy");

    searchFields.add( new DateField( "searchEndDate","To:", true ) );
    ((DateField)searchFields.getField("searchEndDate")).setDateFormat("MM/dd/yyyy");

    searchFields.add( new Field    ( "searchValue","Search Criteria:", 50, 50, true ) );

    // initialize the multi-move module
    //initMultiMoveFields();
  }


  protected QueueData getEmptyQueueData()
  {
    SettlementRejectData       retVal;

    switch(ccType)
    {
      case(MC):
      case(VS):
       retVal = new _SettlementRejectData();
       break;

      case(ALL):
      default:
       retVal = new SettlementRejectData();
       break;
    }

    retVal.setDateFormat("MM/dd/yyyy");

    //fields.add(retVal.elements);

    return( retVal );

  }


  protected void loadQueueData(UserBean user)
  {

    double          doubleVal     = 0.0;
    long            longVal       = -1L;
    FieldGroup      searchFields  = (FieldGroup)fields.getField("searchFields");

    try
    {
      int bankNumber  = Integer.parseInt(Long.toString(user.getHierarchyNode()).substring(0,4));

      Date    searchBeginDate   = ((DateField)searchFields.getField( "searchBeginDate" )).getSqlDate();
      Date    searchEndDate     = ((DateField)searchFields.getField( "searchEndDate" )).getSqlDate();
      String  searchValue       = searchFields.getField( "searchValue" ).getData();

      // get the value as a long if possible
      try{ longVal = Long.parseLong(searchValue); } catch( Exception e ) { longVal = -1L; }
      //try{ doubleVal = Double.parseDouble(searchValue); } catch( Exception e ) { doubleVal = 0.0; }

      /*@lineinfo:generated-code*//*@lineinfo:119^7*/

//  ************************************************************
//  #sql [Ctx] it = { -- Visa data
//          select
//                  qd.id                   as id,
//                  qd.type                 as type,
//                  qd.item_type            as item_type,
//                  qd.owner                as owner,
//                  qd.id                   as control,
//                  'VS Reject'             as description,
//                  trunc(qd.date_created)  as date_created,
//                  qd.source               as source,
//                  qd.affiliate            as affiliate,
//                  qd.last_changed         as last_changed,
//                  qd.last_user            as last_user,
//                  nvl(qd.locked_by,'')    as locked_by,
//                  0                       as note_count,-- nvl(qn.note_count,0)
//                  nvl(qt.status,'')       as status,
//                  vs.merchant_number      as merchant_number,
//                  rr.reject_id            as reject_code,
//                  'VS'                    as reject_type,
//                  nvl(rt.title,'')        as reject_title,
//                  nvl(rt.description,'')  as reject_desc,
//                  nvl(rt.action,'')       as reject_action,
//                  nvl(rt.fields_affected,'') as fields
//          from    q_data                  qd,
//                  q_types                 qt,
//                  visa_settlement         vs,
//                  reject_record           rr,
//                  reject_type             rt
//          where   qd.type             = :this.type
//                  and qd.type         = qt.type
//                  and qt.item_type    = :MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS
//                  and vs.rec_id(+)    = qd.id
//                  and qd.id           = rr.rec_id(+)
//                  and rr.reject_type  = 'VS'
//                  and
//                    (
//                      ( :searchBeginDate is null and
//                        trunc(qd.date_created) >= (trunc(sysdate)-180) ) or
//                      ( trunc(qd.date_created) between :searchBeginDate and :searchEndDate )
//                    ) and
//                    (
//                      ( :searchValue is null ) or
//                      ( vs.merchant_number = :longVal ) or
//                      ( qd.id = :longVal ) or
//                      ( rr.reject_id like '%'||:searchValue||'%') or
//                      ( rr.reject_type = :searchValue)
//                    )
//                  and rr.reject_id = rt.id(+)
//          UNION
//  -- MC data
//          select
//                  qd.id                   as id,
//                  qd.type                 as type,
//                  qd.item_type            as item_type,
//                  qd.owner                as owner,
//                  qd.id                   as control,
//                  'MC Reject'             as description,
//                  trunc(qd.date_created)  as date_created,
//                  qd.source               as source,
//                  qd.affiliate            as affiliate,
//                  qd.last_changed         as last_changed,
//                  qd.last_user            as last_user,
//                  nvl(qd.locked_by,'')    as locked_by,
//                  0                       as note_count,-- nvl(qn.note_count,0)
//                  nvl(qt.status,'')       as status,
//                  mc.merchant_number      as merchant_number,
//                  rr.reject_id            as reject_code,
//                  'MC'                    as reject_type,
//                  nvl(rt.title,'')        as reject_title,
//                  nvl(rt.description,'')  as reject_desc,
//                  nvl(rt.action,'')       as reject_action,
//                  nvl(rt.fields_affected,'') as fields
//          from    q_data                  qd,
//                  q_types                 qt,
//                  mc_settlement           mc,
//                  reject_record           rr,
//                  reject_type             rt
//          where   qd.type             = :this.type
//                  and qd.type         = qt.type
//                  and qt.item_type    = :MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS
//                  and mc.rec_id(+)    = qd.id
//                  and qd.id           = rr.rec_id(+)
//                  and rr.reject_type  = 'MC'
//                  and
//                    (
//                      ( :searchBeginDate is null and
//                        trunc(qd.date_created) >= (trunc(sysdate)-180) ) or
//                      ( trunc(qd.date_created) between :searchBeginDate and :searchEndDate )
//                    ) and
//                    (
//                      ( :searchValue is null ) or
//                      ( mc.merchant_number = :longVal ) or
//                      ( qd.id = :longVal ) or
//                      ( rr.reject_id like '%'||:searchValue||'%') or
//                      ( rr.reject_type = :searchValue)
//                    )
//                  and rr.reject_id = rt.id(+)
//           order by 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "-- Visa data\n        select\n                qd.id                   as id,\n                qd.type                 as type,\n                qd.item_type            as item_type,\n                qd.owner                as owner,\n                qd.id                   as control,\n                'VS Reject'             as description,\n                trunc(qd.date_created)  as date_created,\n                qd.source               as source,\n                qd.affiliate            as affiliate,\n                qd.last_changed         as last_changed,\n                qd.last_user            as last_user,\n                nvl(qd.locked_by,'')    as locked_by,\n                0                       as note_count,-- nvl(qn.note_count,0)\n                nvl(qt.status,'')       as status,\n                vs.merchant_number      as merchant_number,\n                rr.reject_id            as reject_code,\n                'VS'                    as reject_type,\n                nvl(rt.title,'')        as reject_title,\n                nvl(rt.description,'')  as reject_desc,\n                nvl(rt.action,'')       as reject_action,\n                nvl(rt.fields_affected,'') as fields\n        from    q_data                  qd,\n                q_types                 qt,\n                visa_settlement         vs,\n                reject_record           rr,\n                reject_type             rt\n        where   qd.type             =  :1 \n                and qd.type         = qt.type\n                and qt.item_type    =  :2 \n                and vs.rec_id(+)    = qd.id\n                and qd.id           = rr.rec_id(+)\n                and rr.reject_type  = 'VS'\n                and\n                  (\n                    (  :3  is null and\n                      trunc(qd.date_created) >= (trunc(sysdate)-180) ) or\n                    ( trunc(qd.date_created) between  :4  and  :5  )\n                  ) and\n                  (\n                    (  :6  is null ) or\n                    ( vs.merchant_number =  :7  ) or\n                    ( qd.id =  :8  ) or\n                    ( rr.reject_id like '%'|| :9 ||'%') or\n                    ( rr.reject_type =  :10 )\n                  )\n                and rr.reject_id = rt.id(+)\n        UNION\n-- MC data\n        select\n                qd.id                   as id,\n                qd.type                 as type,\n                qd.item_type            as item_type,\n                qd.owner                as owner,\n                qd.id                   as control,\n                'MC Reject'             as description,\n                trunc(qd.date_created)  as date_created,\n                qd.source               as source,\n                qd.affiliate            as affiliate,\n                qd.last_changed         as last_changed,\n                qd.last_user            as last_user,\n                nvl(qd.locked_by,'')    as locked_by,\n                0                       as note_count,-- nvl(qn.note_count,0)\n                nvl(qt.status,'')       as status,\n                mc.merchant_number      as merchant_number,\n                rr.reject_id            as reject_code,\n                'MC'                    as reject_type,\n                nvl(rt.title,'')        as reject_title,\n                nvl(rt.description,'')  as reject_desc,\n                nvl(rt.action,'')       as reject_action,\n                nvl(rt.fields_affected,'') as fields\n        from    q_data                  qd,\n                q_types                 qt,\n                mc_settlement           mc,\n                reject_record           rr,\n                reject_type             rt\n        where   qd.type             =  :11 \n                and qd.type         = qt.type\n                and qt.item_type    =  :12 \n                and mc.rec_id(+)    = qd.id\n                and qd.id           = rr.rec_id(+)\n                and rr.reject_type  = 'MC'\n                and\n                  (\n                    (  :13  is null and\n                      trunc(qd.date_created) >= (trunc(sysdate)-180) ) or\n                    ( trunc(qd.date_created) between  :14  and  :15  )\n                  ) and\n                  (\n                    (  :16  is null ) or\n                    ( mc.merchant_number =  :17  ) or\n                    ( qd.id =  :18  ) or\n                    ( rr.reject_id like '%'|| :19 ||'%') or\n                    ( rr.reject_type =  :20 )\n                  )\n                and rr.reject_id = rt.id(+)\n         order by 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.SettlementRejectsQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setInt(2,MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS);
   __sJT_st.setDate(3,searchBeginDate);
   __sJT_st.setDate(4,searchBeginDate);
   __sJT_st.setDate(5,searchEndDate);
   __sJT_st.setString(6,searchValue);
   __sJT_st.setLong(7,longVal);
   __sJT_st.setLong(8,longVal);
   __sJT_st.setString(9,searchValue);
   __sJT_st.setString(10,searchValue);
   __sJT_st.setInt(11,this.type);
   __sJT_st.setInt(12,MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS);
   __sJT_st.setDate(13,searchBeginDate);
   __sJT_st.setDate(14,searchBeginDate);
   __sJT_st.setDate(15,searchEndDate);
   __sJT_st.setString(16,searchValue);
   __sJT_st.setLong(17,longVal);
   __sJT_st.setLong(18,longVal);
   __sJT_st.setString(19,searchValue);
   __sJT_st.setString(20,searchValue);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.queues.SettlementRejectsQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:219^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueData(" + user.getLoginName() + ")", e.toString());
    }

  }

  protected void loadQueueItem(long id)
  {
    //determine proper table to look at - ccType
    determineSettlementTable(id);

    try
    {
      switch(ccType)
      {
        case(MC):

          //MC
          /*@lineinfo:generated-code*//*@lineinfo:240^11*/

//  ************************************************************
//  #sql [Ctx] it = { select
//                      qd.id                   as id,
//                      qd.type                 as type,
//                      qd.item_type            as item_type,
//                      qd.owner                as owner,
//                      qd.id                   as control,
//                      'MC Reject'             as description,
//                      trunc(qd.date_created)  as date_created,
//                      qd.source               as source,
//                      qd.affiliate            as affiliate,
//                      qd.last_changed         as last_changed,
//                      qd.last_user            as last_user,
//                      nvl(qd.locked_by,'')    as locked_by,
//                      0                       as note_count,
//                      nvl(qt.status,'')       as status,
//                      rr.reject_id            as reject_code,
//                      'MC'                    as reject_type,
//                      nvl(rt.title,'')        as reject_title,
//                      nvl(rt.description,'')  as reject_desc,
//                      nvl(rt.action,'')       as reject_action,
//                      nvl(rt.fields_affected,'') as fields,
//                      mc.*
//              from    q_data                  qd,
//                      q_types                 qt,
//                      mc_settlement           mc,
//                      reject_record           rr,
//                      reject_type             rt
//              where   qd.type                 = :this.type
//                      and qd.id               = :id
//                      and qd.type             = qt.type
//                      and mc.rec_id(+)        = qd.id
//                      and qd.id               = rr.rec_id(+)
//                      and rr.reject_id        = rt.id(+)
//              order by
//                      rr.reject_id asc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n                    qd.id                   as id,\n                    qd.type                 as type,\n                    qd.item_type            as item_type,\n                    qd.owner                as owner,\n                    qd.id                   as control,\n                    'MC Reject'             as description,\n                    trunc(qd.date_created)  as date_created,\n                    qd.source               as source,\n                    qd.affiliate            as affiliate,\n                    qd.last_changed         as last_changed,\n                    qd.last_user            as last_user,\n                    nvl(qd.locked_by,'')    as locked_by,\n                    0                       as note_count,\n                    nvl(qt.status,'')       as status,\n                    rr.reject_id            as reject_code,\n                    'MC'                    as reject_type,\n                    nvl(rt.title,'')        as reject_title,\n                    nvl(rt.description,'')  as reject_desc,\n                    nvl(rt.action,'')       as reject_action,\n                    nvl(rt.fields_affected,'') as fields,\n                    mc.*\n            from    q_data                  qd,\n                    q_types                 qt,\n                    mc_settlement           mc,\n                    reject_record           rr,\n                    reject_type             rt\n            where   qd.type                 =  :1 \n                    and qd.id               =  :2 \n                    and qd.type             = qt.type\n                    and mc.rec_id(+)        = qd.id\n                    and qd.id               = rr.rec_id(+)\n                    and rr.reject_id        = rt.id(+)\n            order by\n                    rr.reject_id asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.SettlementRejectsQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setLong(2,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.queues.SettlementRejectsQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:277^11*/
          break;

          case(VS):

          //VISA
          /*@lineinfo:generated-code*//*@lineinfo:283^11*/

//  ************************************************************
//  #sql [Ctx] it = { select
//                      qd.id                   as id,
//                      qd.type                 as type,
//                      qd.item_type            as item_type,
//                      qd.owner                as owner,
//                      qd.id                   as control,
//                      'VS Reject'             as description,
//                      trunc(qd.date_created)  as date_created,
//                      qd.source               as source,
//                      qd.affiliate            as affiliate,
//                      qd.last_changed         as last_changed,
//                      qd.last_user            as last_user,
//                      nvl(qd.locked_by,'')    as locked_by,
//                      0                       as note_count,
//                      nvl(qt.status,'')       as status,
//                      rr.reject_id            as reject_code,
//                      'VS'                    as reject_type,
//                      nvl(rt.title,'')        as reject_title,
//                      nvl(rt.description,'')  as reject_desc,
//                      nvl(rt.action,'')       as reject_action,
//                      nvl(rt.fields_affected,'') as fields,
//                      vss.*
//              from    q_data                  qd,
//                      q_types                 qt,
//                      visa_settlement         vss,
//                      reject_record           rr,
//                      reject_type             rt
//              where   qd.type                 = :this.type
//                      and qd.id               = :id
//                      and qd.type             = qt.type
//                      and vss.rec_id(+)       = qd.id
//                      and qd.id               = rr.rec_id(+)
//                      and rr.reject_id        = rt.id(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n                    qd.id                   as id,\n                    qd.type                 as type,\n                    qd.item_type            as item_type,\n                    qd.owner                as owner,\n                    qd.id                   as control,\n                    'VS Reject'             as description,\n                    trunc(qd.date_created)  as date_created,\n                    qd.source               as source,\n                    qd.affiliate            as affiliate,\n                    qd.last_changed         as last_changed,\n                    qd.last_user            as last_user,\n                    nvl(qd.locked_by,'')    as locked_by,\n                    0                       as note_count,\n                    nvl(qt.status,'')       as status,\n                    rr.reject_id            as reject_code,\n                    'VS'                    as reject_type,\n                    nvl(rt.title,'')        as reject_title,\n                    nvl(rt.description,'')  as reject_desc,\n                    nvl(rt.action,'')       as reject_action,\n                    nvl(rt.fields_affected,'') as fields,\n                    vss.*\n            from    q_data                  qd,\n                    q_types                 qt,\n                    visa_settlement         vss,\n                    reject_record           rr,\n                    reject_type             rt\n            where   qd.type                 =  :1 \n                    and qd.id               =  :2 \n                    and qd.type             = qt.type\n                    and vss.rec_id(+)       = qd.id\n                    and qd.id               = rr.rec_id(+)\n                    and rr.reject_id        = rt.id(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.queues.SettlementRejectsQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setLong(2,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.queues.SettlementRejectsQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:318^11*/
          break;

        default:
          throw new RuntimeException("invalid card type");

      };

    }
    catch(Exception e)
    {
      e.printStackTrace();
      //logEntry("loadQueueItem(" + this.type + ", " + id + ")", e.toString());
    }
  }

  private void determineSettlementTable(long id)
  {
    int count;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:339^7*/

//  ************************************************************
//  #sql [Ctx] { select count(rec_id), decode(reject_type, 'MC',1, 'VS', 2, 0)
//          
//          from reject_record
//          where rec_id = :id
//          group by decode(reject_type, 'MC',1, 'VS', 2, 0)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(rec_id), decode(reject_type, 'MC',1, 'VS', 2, 0)\n         \n        from reject_record\n        where rec_id =  :1 \n        group by decode(reject_type, 'MC',1, 'VS', 2, 0)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.queues.SettlementRejectsQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   ccType = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:346^7*/
    }
    catch(Exception e)
    {
      ccType = ALL;
    }
  }

  protected void loadExtraColumns(ExtraColumnBean extraColumns, UserBean user)
  {
    try
    {
      // add the extra columns

      LinkData linkData = null;

      linkData = extraColumns.getNewLinkData();
      linkData.addLink("merchant","merchant_number");

      extraColumns.addColumn("Reject Code",  "reject_code");
      extraColumns.addColumn("Reject Type",  "reject_type");
      extraColumns.addColumn("Merchant Number",   "merchant_number", "/jsp/maintenance/view_account.jsp?action=1", "blank", "", linkData);

    }
    catch(Exception e)
    {
      logEntry("loadExtraColumns()", e.toString());
    }

  }

  public String getGenericSearchDesc()
  {
    StringBuffer          desc      = new StringBuffer();

    desc.append("<ul>");
    desc.append("  <li>Merchant Number</li>");
    desc.append("  <li>Reject Reason Code (partial OK)</li>");
    desc.append("  <li>VS or MC</li>");
    desc.append("  <li>Control Number</li>");
    desc.append("</ul>");

    return( desc.toString() );
  }
/*
  public boolean isMultiMoveEnabled()
  {
    boolean     retVal      = false;

    switch(getType())
    {
      case MesQueues.Q_REJECTS_RAW:
        retVal = true;
        break;
    }
    return(retVal);
  }
*/

  //it's here we'll process the field changes to correct and represent the reject
  public void doAction( HttpServletRequest request )
  {
    String            actionCode    = null;
    RadioButtonField  actionField   = null;
    long              itemId        = HttpHelper.getLong(request, "id", -1);
    Field             field         = null;
    StringBuffer      msg           = new StringBuffer();
    String            userLogin     = null;

    super.doAction(request);

    try
    {
      connect();

      try
      {
        userLogin = user.getLoginName();
      }
      catch(Exception e)
      {
        userLogin = "WebQueue";
      }

      actionField = (RadioButtonField)getField("action");

      if( actionField.isValid() )
      {

        int toQueue = actionField.asInteger();

        //need to check the queue change
        switch(toQueue)
        {
          case MesQueues.Q_REJECTS_UNWORKED:
            log.debug("moving to Q_REJECTS_UNWORKED");
            if(getType()==MesQueues.Q_REJECTS_PENDING_CLEAR)
            {
              //moving back from pending queue, so update status
              updateRejectStatus(-1);
            }
            break;

          case MesQueues.Q_REJECTS_PENDING_CLEAR:
            log.debug("moving to Q_REJECTS_PENDING_CLEAR");
            //if moving to clearing queue pending, we need to insert a new entry into
            //the appropriate _settlement_activity table
            updateSettlementData(request);
            updateRejectStatus(2);
            break;

          case MesQueues.Q_REJECTS_PENDING_KILL:
            log.debug("moving to Q_REJECTS_PENDING_KILL");
            updateRejectStatus(0);
            break;

          case MesQueues.Q_REJECTS_KILL:
          case MesQueues.Q_REJECTS_COMPLETED:
          default:
            log.debug("EXCEPTION - toQueue :: undefined or inaccessible.");
        };
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
      logEntry( "doAction()", e.toString() );
    }
    finally
    {
      cleanUp();
    }
  }

  private void updateSettlementData(HttpServletRequest request)
  throws Exception
  {

    log.debug("in updateSettlementData...");

    //we'll set request data here into the field structure to do
    //validation
    SettlementRejectData _queueItem = (SettlementRejectData)queueItem;

    _queueItem.setFields(request);

    switch(ccType)
    {
      case MC:

        //need to define all standard field data
        //String bin_number             = _queueItem.getFieldData("");
        //String ica_number             = _queueItem.getFieldData("");
        //String merchant_number        = _queueItem.getFieldData("");
        //String dba_name               = _queueItem.getFieldData("");
        //String legal_corp_name        = _queueItem.getFieldData("");

        //String batch_date                        = _queueItem.getFieldData("batch_date");
        //String batch_number                      = _queueItem.getFieldData("batch_number");
        //String batch_id                          = _queueItem.getFieldData("batch_id");
        //String batch_record_id                   = _queueItem.getFieldData("batch_record_id");

        //String recurring_payment_number = _queueItem.getFieldData("recurringpaymentnumber");
        //String recurring_payment_count  = _queueItem.getFieldData("recurringpaymentcount");
        //String alternate_tax_amount     = _queueItem.getFieldData("alternatetaxamount");
        //String tax_amount               = _queueItem.getFieldData("taxamount");

        java.sql.Timestamp transaction_date         = ((DateField)_queueItem.getField("transactiondate")).getSqlTimestamp();
        java.sql.Timestamp transaction_time         = ((DateField)_queueItem.getField("transactiontime")).getSqlTimestamp();
        java.sql.Timestamp auth_date                = ((DateField)_queueItem.getField("authdate")).getSqlTimestamp();
        java.sql.Timestamp statement_date_begin     = ((DateField)_queueItem.getField("statementdatebegin")).getSqlTimestamp();
        java.sql.Timestamp statement_date_end       = ((DateField)_queueItem.getField("statementdateend")).getSqlTimestamp();

        String dba_addr                 = _queueItem.getFieldData("dbaaddr");
        String dba_city                 = _queueItem.getFieldData("dbacity");
        String dba_state                = _queueItem.getFieldData("dbastate");
        String dba_zip                  = _queueItem.getFieldData("dbazip");
        String sic_code                 = _queueItem.getFieldData("siccode");
        String card_acceptor_type       = _queueItem.getFieldData("cardacceptortype");
        String moto_ecommerce_merchant  = _queueItem.getFieldData("motoecommercemerchant");
        String mc_assigned_id           = _queueItem.getFieldData("mcassignedid");
        String country_code             = _queueItem.getFieldData("countrycode");
        String phone_number             = _queueItem.getFieldData("phonenumber");
        String card_number              = _queueItem.getFieldData("cardnumber");
        String card_type                = _queueItem.getFieldData("cardtype");
        String gcms_product_id          = _queueItem.getFieldData("gcmsproductid");
        String card_program_id          = _queueItem.getFieldData("cardprogramid");
        String product_class            = _queueItem.getFieldData("productclass");
        String us_issuer                = _queueItem.getFieldData("usissuer");
        String cash_disbursement        = _queueItem.getFieldData("cashdisbursement");
        String debit_credit_indicator   = _queueItem.getFieldData("debitcreditindicator");
        String transaction_amount       = _queueItem.getFieldData("transactionamount");
        String currency_code            = _queueItem.getFieldData("currencycode");
        String ic_cat                   = _queueItem.getFieldData("iccat");
        String ic_cat_billing           = _queueItem.getFieldData("iccatbilling");
        String ic_cat_downgrade         = _queueItem.getFieldData("iccatdowngrade");
        String auth_amount              = _queueItem.getFieldData("authamount");
        String auth_amount_total        = _queueItem.getFieldData("authamounttotal");
        String auth_currency_code       = _queueItem.getFieldData("authcurrencycode");
        String auth_code                = _queueItem.getFieldData("authcode");
        String auth_source_code         = _queueItem.getFieldData("authsourcecode");
        String auth_response_code       = _queueItem.getFieldData("authresponsecode");
        String auth_retrieval_ref_num   = _queueItem.getFieldData("authretrievalrefnum");
        String auth_banknet_ref_num     = _queueItem.getFieldData("authbanknetrefnum");
        String auth_banknet_date        = _queueItem.getFieldData("authbanknetdate");
        String auth_avs_response        = _queueItem.getFieldData("authavsresponse");
        String auth_cvv2_response       = _queueItem.getFieldData("authcvv2response");
        String auth_rec_id              = _queueItem.getFieldData("authrecid");
        String trident_tran_id          = _queueItem.getFieldData("tridenttranid");
        String cat_indicator            = _queueItem.getFieldData("catindicator");
        String alm_code                 = _queueItem.getFieldData("almcode");
        String bsa_type                 = _queueItem.getFieldData("bsatype");
        String bsa_id_code              = _queueItem.getFieldData("bsaidcode");
        String ird                      = _queueItem.getFieldData("ird");
        String addendum_required        = _queueItem.getFieldData("addendumrequired");
        String reference_number         = _queueItem.getFieldData("referencenumber");
        String acq_reference_number     = _queueItem.getFieldData("acqreferencenumber");
        String purchase_id              = _queueItem.getFieldData("purchaseid");
        String purchase_id_format       = _queueItem.getFieldData("purchaseidformat");
        String ecomm_security_level_ind = _queueItem.getFieldData("ecommsecuritylevelind");
        String switch_settled_ind       = _queueItem.getFieldData("switchsettledind");
        String pos_data_code            = _queueItem.getFieldData("posdatacode");
        String processing_code          = _queueItem.getFieldData("processingcode");
        String cardholder_from_type     = _queueItem.getFieldData("cardholderfromtype");
        String cardholder_to_type       = _queueItem.getFieldData("cardholdertotype");
        String program_registration_id  = _queueItem.getFieldData("programregistrationid");
        String transaction_category_ind = _queueItem.getFieldData("transactioncategoryind");
        String service_code             = _queueItem.getFieldData("servicecode");
        String poi_amount               = _queueItem.getFieldData("poiamount");
        String poi_currency_code        = _queueItem.getFieldData("poicurrencycode");
        String customer_code            = _queueItem.getFieldData("customercode");
        String merchant_type_code       = _queueItem.getFieldData("merchanttypecode");
        String alternate_tax_amount_ind = _queueItem.getFieldData("alternatetaxamountind");
        String merchant_tax_id          = _queueItem.getFieldData("merchanttaxid");
        String merchant_reference_number= _queueItem.getFieldData("merchantreferencenumber");
        String tax_indicator            = _queueItem.getFieldData("taxindicator");
        String pos_entry_mode           = _queueItem.getFieldData("posentrymode");
        String card_present             = _queueItem.getFieldData("cardpresent");
        String moto_ecommerce_ind       = _queueItem.getFieldData("motoecommerceind");
        String recurring_payment_ind    = _queueItem.getFieldData("recurringpaymentind");
        String ship_from_zip            = _queueItem.getFieldData("shipfromzip");
        String ship_to_zip              = _queueItem.getFieldData("shiptozip");
        String dest_country_code        = _queueItem.getFieldData("destcountrycode");
        String shipping_amount          = _queueItem.getFieldData("shippingamount");
        String cashback_amount          = _queueItem.getFieldData("cashbackamount");
        String tip_amount               = _queueItem.getFieldData("tipamount");
        String duty_amount              = _queueItem.getFieldData("dutyamount");
        String customer_service_phone   = _queueItem.getFieldData("customerservicephone");

        /*@lineinfo:generated-code*//*@lineinfo:595^9*/

//  ************************************************************
//  #sql [Ctx] { update
//              mc_settlement
//            set
//              --bin_number                  = :(),
//              --ica_number                  = :(),
//              --merchant_number             = :(),
//              --dba_name                    = :(),
//              --legal_corp_name             = :(),
//              --batch_date                  = :(),
//              --batch_number                = :(),
//              --batch_id                    = :(),
//              --batch_record_id             = :(),
//              --recurring_payment_number    = :(),
//              --recurring_payment_count     = :(),
//              --alternate_tax_amount        = :(),
//              --tax_amount                  = :(),
//              dba_addr                      = :dba_addr,
//              dba_city                      = :dba_city,
//              dba_state                     = :dba_state,
//              dba_zip                       = :dba_zip,
//              sic_code                      = :sic_code,
//              card_acceptor_type            = :card_acceptor_type,
//              moto_ecommerce_merchant       = :moto_ecommerce_merchant,
//              mc_assigned_id                = :mc_assigned_id,
//              country_code                  = :country_code,
//              phone_number                  = :phone_number,
//              card_number                   = :card_number,
//              card_type                     = :card_type,
//              gcms_product_id               = :gcms_product_id,
//              card_program_id               = :card_program_id,
//              product_class                 = :product_class,
//              us_issuer                     = :us_issuer,
//              cash_disbursement             = :cash_disbursement,
//              transaction_date              = :transaction_date,
//              transaction_time              = :transaction_time,
//              debit_credit_indicator        = :debit_credit_indicator,
//              transaction_amount            = :transaction_amount,
//              currency_code                 = :currency_code,
//              ic_cat                        = :ic_cat,
//              ic_cat_billing                = :ic_cat_billing,
//              ic_cat_downgrade              = :ic_cat_downgrade,
//              auth_amount                   = :auth_amount,
//              auth_amount_total             = :auth_amount_total,
//              auth_currency_code            = :auth_currency_code,
//              auth_code                     = :auth_code,
//              auth_source_code              = :auth_source_code,
//              auth_response_code            = :auth_response_code,
//              auth_date                     = :auth_date,
//              auth_retrieval_ref_num        = :auth_retrieval_ref_num,
//              auth_banknet_ref_num          = :auth_banknet_ref_num,
//              auth_banknet_date             = :auth_banknet_date,
//              auth_avs_response             = :auth_avs_response,
//              auth_cvv2_response            = :auth_cvv2_response,
//              auth_rec_id                   = :auth_rec_id,
//              trident_tran_id               = :trident_tran_id,
//              cat_indicator                 = :cat_indicator,
//              alm_code                      = :alm_code,
//              bsa_type                      = :bsa_type,
//              bsa_id_code                   = :bsa_id_code,
//              ird                           = :ird,
//              addendum_required             = :addendum_required,
//              reference_number              = :reference_number,
//              acq_reference_number          = :acq_reference_number,
//              purchase_id                   = :purchase_id,
//              purchase_id_format            = :purchase_id_format,
//              statement_date_begin          = :statement_date_begin,
//              statement_date_end            = :statement_date_end,
//              ecomm_security_level_ind      = :ecomm_security_level_ind,
//              switch_settled_ind            = :switch_settled_ind,
//              pos_data_code                 = :pos_data_code,
//              processing_code               = :processing_code,
//              cardholder_from_type          = :cardholder_from_type,
//              cardholder_to_type            = :cardholder_to_type,
//              program_registration_id       = :program_registration_id,
//              transaction_category_ind      = :transaction_category_ind,
//              service_code                  = :service_code,
//              poi_amount                    = :poi_amount,
//              poi_currency_code             = :poi_currency_code,
//              customer_code                 = :customer_code,
//              merchant_type_code            = :merchant_type_code,
//              alternate_tax_amount_ind      = :alternate_tax_amount_ind,
//              merchant_tax_id               = :merchant_tax_id,
//              merchant_reference_number     = :merchant_reference_number,
//              tax_indicator                 = :tax_indicator,
//              pos_entry_mode                = :pos_entry_mode,
//              card_present                  = :card_present,
//              moto_ecommerce_ind            = :moto_ecommerce_ind,
//              recurring_payment_ind         = :recurring_payment_ind,
//              ship_from_zip                 = :ship_from_zip,
//              ship_to_zip                   = :ship_to_zip,
//              dest_country_code             = :dest_country_code,
//              shipping_amount               = :shipping_amount,
//              cashback_amount               = :cashback_amount,
//              tip_amount                    = :tip_amount,
//              duty_amount                   = :duty_amount,
//              customer_service_phone        = :customer_service_phone
//            where
//              rec_id = :queueItem.getId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_524 = queueItem.getId();
  try {
   String theSqlTS = "update\n            mc_settlement\n          set\n            --bin_number                  = :(),\n            --ica_number                  = :(),\n            --merchant_number             = :(),\n            --dba_name                    = :(),\n            --legal_corp_name             = :(),\n            --batch_date                  = :(),\n            --batch_number                = :(),\n            --batch_id                    = :(),\n            --batch_record_id             = :(),\n            --recurring_payment_number    = :(),\n            --recurring_payment_count     = :(),\n            --alternate_tax_amount        = :(),\n            --tax_amount                  = :(),\n            dba_addr                      =  :1 ,\n            dba_city                      =  :2 ,\n            dba_state                     =  :3 ,\n            dba_zip                       =  :4 ,\n            sic_code                      =  :5 ,\n            card_acceptor_type            =  :6 ,\n            moto_ecommerce_merchant       =  :7 ,\n            mc_assigned_id                =  :8 ,\n            country_code                  =  :9 ,\n            phone_number                  =  :10 ,\n            card_number                   =  :11 ,\n            card_type                     =  :12 ,\n            gcms_product_id               =  :13 ,\n            card_program_id               =  :14 ,\n            product_class                 =  :15 ,\n            us_issuer                     =  :16 ,\n            cash_disbursement             =  :17 ,\n            transaction_date              =  :18 ,\n            transaction_time              =  :19 ,\n            debit_credit_indicator        =  :20 ,\n            transaction_amount            =  :21 ,\n            currency_code                 =  :22 ,\n            ic_cat                        =  :23 ,\n            ic_cat_billing                =  :24 ,\n            ic_cat_downgrade              =  :25 ,\n            auth_amount                   =  :26 ,\n            auth_amount_total             =  :27 ,\n            auth_currency_code            =  :28 ,\n            auth_code                     =  :29 ,\n            auth_source_code              =  :30 ,\n            auth_response_code            =  :31 ,\n            auth_date                     =  :32 ,\n            auth_retrieval_ref_num        =  :33 ,\n            auth_banknet_ref_num          =  :34 ,\n            auth_banknet_date             =  :35 ,\n            auth_avs_response             =  :36 ,\n            auth_cvv2_response            =  :37 ,\n            auth_rec_id                   =  :38 ,\n            trident_tran_id               =  :39 ,\n            cat_indicator                 =  :40 ,\n            alm_code                      =  :41 ,\n            bsa_type                      =  :42 ,\n            bsa_id_code                   =  :43 ,\n            ird                           =  :44 ,\n            addendum_required             =  :45 ,\n            reference_number              =  :46 ,\n            acq_reference_number          =  :47 ,\n            purchase_id                   =  :48 ,\n            purchase_id_format            =  :49 ,\n            statement_date_begin          =  :50 ,\n            statement_date_end            =  :51 ,\n            ecomm_security_level_ind      =  :52 ,\n            switch_settled_ind            =  :53 ,\n            pos_data_code                 =  :54 ,\n            processing_code               =  :55 ,\n            cardholder_from_type          =  :56 ,\n            cardholder_to_type            =  :57 ,\n            program_registration_id       =  :58 ,\n            transaction_category_ind      =  :59 ,\n            service_code                  =  :60 ,\n            poi_amount                    =  :61 ,\n            poi_currency_code             =  :62 ,\n            customer_code                 =  :63 ,\n            merchant_type_code            =  :64 ,\n            alternate_tax_amount_ind      =  :65 ,\n            merchant_tax_id               =  :66 ,\n            merchant_reference_number     =  :67 ,\n            tax_indicator                 =  :68 ,\n            pos_entry_mode                =  :69 ,\n            card_present                  =  :70 ,\n            moto_ecommerce_ind            =  :71 ,\n            recurring_payment_ind         =  :72 ,\n            ship_from_zip                 =  :73 ,\n            ship_to_zip                   =  :74 ,\n            dest_country_code             =  :75 ,\n            shipping_amount               =  :76 ,\n            cashback_amount               =  :77 ,\n            tip_amount                    =  :78 ,\n            duty_amount                   =  :79 ,\n            customer_service_phone        =  :80 \n          where\n            rec_id =  :81";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.queues.SettlementRejectsQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,dba_addr);
   __sJT_st.setString(2,dba_city);
   __sJT_st.setString(3,dba_state);
   __sJT_st.setString(4,dba_zip);
   __sJT_st.setString(5,sic_code);
   __sJT_st.setString(6,card_acceptor_type);
   __sJT_st.setString(7,moto_ecommerce_merchant);
   __sJT_st.setString(8,mc_assigned_id);
   __sJT_st.setString(9,country_code);
   __sJT_st.setString(10,phone_number);
   __sJT_st.setString(11,card_number);
   __sJT_st.setString(12,card_type);
   __sJT_st.setString(13,gcms_product_id);
   __sJT_st.setString(14,card_program_id);
   __sJT_st.setString(15,product_class);
   __sJT_st.setString(16,us_issuer);
   __sJT_st.setString(17,cash_disbursement);
   __sJT_st.setTimestamp(18,transaction_date);
   __sJT_st.setTimestamp(19,transaction_time);
   __sJT_st.setString(20,debit_credit_indicator);
   __sJT_st.setString(21,transaction_amount);
   __sJT_st.setString(22,currency_code);
   __sJT_st.setString(23,ic_cat);
   __sJT_st.setString(24,ic_cat_billing);
   __sJT_st.setString(25,ic_cat_downgrade);
   __sJT_st.setString(26,auth_amount);
   __sJT_st.setString(27,auth_amount_total);
   __sJT_st.setString(28,auth_currency_code);
   __sJT_st.setString(29,auth_code);
   __sJT_st.setString(30,auth_source_code);
   __sJT_st.setString(31,auth_response_code);
   __sJT_st.setTimestamp(32,auth_date);
   __sJT_st.setString(33,auth_retrieval_ref_num);
   __sJT_st.setString(34,auth_banknet_ref_num);
   __sJT_st.setString(35,auth_banknet_date);
   __sJT_st.setString(36,auth_avs_response);
   __sJT_st.setString(37,auth_cvv2_response);
   __sJT_st.setString(38,auth_rec_id);
   __sJT_st.setString(39,trident_tran_id);
   __sJT_st.setString(40,cat_indicator);
   __sJT_st.setString(41,alm_code);
   __sJT_st.setString(42,bsa_type);
   __sJT_st.setString(43,bsa_id_code);
   __sJT_st.setString(44,ird);
   __sJT_st.setString(45,addendum_required);
   __sJT_st.setString(46,reference_number);
   __sJT_st.setString(47,acq_reference_number);
   __sJT_st.setString(48,purchase_id);
   __sJT_st.setString(49,purchase_id_format);
   __sJT_st.setTimestamp(50,statement_date_begin);
   __sJT_st.setTimestamp(51,statement_date_end);
   __sJT_st.setString(52,ecomm_security_level_ind);
   __sJT_st.setString(53,switch_settled_ind);
   __sJT_st.setString(54,pos_data_code);
   __sJT_st.setString(55,processing_code);
   __sJT_st.setString(56,cardholder_from_type);
   __sJT_st.setString(57,cardholder_to_type);
   __sJT_st.setString(58,program_registration_id);
   __sJT_st.setString(59,transaction_category_ind);
   __sJT_st.setString(60,service_code);
   __sJT_st.setString(61,poi_amount);
   __sJT_st.setString(62,poi_currency_code);
   __sJT_st.setString(63,customer_code);
   __sJT_st.setString(64,merchant_type_code);
   __sJT_st.setString(65,alternate_tax_amount_ind);
   __sJT_st.setString(66,merchant_tax_id);
   __sJT_st.setString(67,merchant_reference_number);
   __sJT_st.setString(68,tax_indicator);
   __sJT_st.setString(69,pos_entry_mode);
   __sJT_st.setString(70,card_present);
   __sJT_st.setString(71,moto_ecommerce_ind);
   __sJT_st.setString(72,recurring_payment_ind);
   __sJT_st.setString(73,ship_from_zip);
   __sJT_st.setString(74,ship_to_zip);
   __sJT_st.setString(75,dest_country_code);
   __sJT_st.setString(76,shipping_amount);
   __sJT_st.setString(77,cashback_amount);
   __sJT_st.setString(78,tip_amount);
   __sJT_st.setString(79,duty_amount);
   __sJT_st.setString(80,customer_service_phone);
   __sJT_st.setLong(81,__sJT_524);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:695^9*/
        break;

      case VS:

        transaction_date         = ((DateField)_queueItem.getField("transactiondate")).getSqlTimestamp();
        auth_date                = ((DateField)_queueItem.getField("authdate")).getSqlTimestamp();
        statement_date_begin     = ((DateField)_queueItem.getField("statementdatebegin")).getSqlTimestamp();
        statement_date_end       = ((DateField)_queueItem.getField("statementdateend")).getSqlTimestamp();

        /*@lineinfo:generated-code*//*@lineinfo:705^9*/

//  ************************************************************
//  #sql [Ctx] { update visa_settlement
//            set
//              --rec_id                              =:(_queueItem.getFieldData("recid")),
//              --bin_number                          =:(_queueItem.getFieldData("binnumber")),
//              acquirer_business_id                =:_queueItem.getFieldData("acquirerbusinessid"),
//              merchant_number                     =:_queueItem.getFieldData("merchantnumber"),
//              dba_name                            =:_queueItem.getFieldData("dbaname"),
//              dba_city                            =:_queueItem.getFieldData("dbacity"),
//              dba_state                           =:_queueItem.getFieldData("dbastate"),
//              dba_zip                             =:_queueItem.getFieldData("dbazip"),
//              sic_code                            =:_queueItem.getFieldData("siccode"),
//              moto_ecommerce_merchant             =:_queueItem.getFieldData("motoecommercemerchant"),
//              mvv                                 =:_queueItem.getFieldData("mvv"),
//              country_code                        =:_queueItem.getFieldData("countrycode"),
//              phone_number                        =:_queueItem.getFieldData("phonenumber"),
//              card_number                         =:_queueItem.getFieldData("cardnumber"),
//              card_type                           =:_queueItem.getFieldData("cardtype"),
//              card_type_enhanced                  =:_queueItem.getFieldData("cardtypeenhanced"),
//              us_issuer                           =:_queueItem.getFieldData("usissuer"),
//              cash_disbursement                   =:_queueItem.getFieldData("cashdisbursement"),
//              --batch_date                          =:(_queueItem.getFieldData("batchdate")),
//              --batch_number                        =:(_queueItem.getFieldData("batchnumber")),
//              --batch_id                            =:(_queueItem.getFieldData("batchid")),
//              --batch_record_id                     =:(_queueItem.getFieldData("batchrecordid")),
//              transaction_date                    =:transaction_date,
//              debit_credit_indicator              =:_queueItem.getFieldData("debitcreditindicator"),
//              transaction_amount                  =:_queueItem.getFieldData("transactionamount"),
//              currency_code                       =:_queueItem.getFieldData("currencycode"),
//              ic_cat                              =:_queueItem.getFieldData("iccat"),
//              ic_cat_billing                      =:_queueItem.getFieldData("iccatbilling"),
//              ic_cat_downgrade                    =:_queueItem.getFieldData("iccatdowngrade"),
//              auth_amount                         =:_queueItem.getFieldData("authamount"),
//              auth_amount_total                   =:_queueItem.getFieldData("authamounttotal"),
//              auth_currency_code                  =:_queueItem.getFieldData("authcurrencycode"),
//              auth_code                           =:_queueItem.getFieldData("authcode"),
//              auth_source_code                    =:_queueItem.getFieldData("authsourcecode"),
//              auth_response_code                  =:_queueItem.getFieldData("authresponsecode"),
//              auth_date                           =:auth_date,
//              auth_returned_aci                   =:_queueItem.getFieldData("authreturnedaci"),
//              auth_settled_aci                    =:_queueItem.getFieldData("authsettledaci"),
//              auth_retrieval_ref_num              =:_queueItem.getFieldData("authretrievalrefnum"),
//              auth_tran_id                        =:_queueItem.getFieldData("authtranid"),
//              auth_val_code                       =:_queueItem.getFieldData("authvalcode"),
//              auth_avs_response                   =:_queueItem.getFieldData("authavsresponse"),
//              auth_cvv2_response                  =:_queueItem.getFieldData("authcvv2response"),
//              auth_rec_id                         =:_queueItem.getFieldData("authrecid"),
//              trident_tran_id                     =:_queueItem.getFieldData("tridenttranid"),
//              product_id                          =:_queueItem.getFieldData("productid"),
//              cardholder_id_method                =:_queueItem.getFieldData("cardholderidmethod"),
//              special_conditions_ind              =:_queueItem.getFieldData("specialconditionsind"),
//              fee_program_indicator               =:_queueItem.getFieldData("feeprogramindicator"),
//              requested_payment_service           =:_queueItem.getFieldData("requestedpaymentservice"),
//              reimbursement_attribute             =:_queueItem.getFieldData("reimbursementattribute"),
//              reference_number                    =:_queueItem.getFieldData("referencenumber"),
//              acq_reference_number                =:_queueItem.getFieldData("acqreferencenumber"),
//              purchase_id                         =:_queueItem.getFieldData("purchaseid"),
//              purchase_id_format                  =:_queueItem.getFieldData("purchaseidformat"),
//              statement_date_begin                =:statement_date_begin,
//              statement_date_end                  =:statement_date_end,
//              tax_amount                          =:_queueItem.getFieldData("taxamount"),
//              tax_indicator                       =:_queueItem.getFieldData("taxindicator"),
//              pos_entry_mode                      =:_queueItem.getFieldData("posentrymode"),
//              card_present                        =:_queueItem.getFieldData("cardpresent"),
//              moto_ecommerce_ind                  =:_queueItem.getFieldData("motoecommerceind"),
//              pos_term_cap                        =:_queueItem.getFieldData("postermcap"),
//              recurring_payment_ind               =:_queueItem.getFieldData("recurringpaymentind"),
//              multiple_clearing_seq_num           =:_queueItem.getFieldData("multipleclearingseqnum"),
//              multiple_clearing_seq_count         =:_queueItem.getFieldData("multipleclearingseqcount"),
//              ship_to_zip                         =:_queueItem.getFieldData("shiptozip"),
//              shipping_amount                     =:_queueItem.getFieldData("shippingamount"),
//              cashback_amount                     =:_queueItem.getFieldData("cashbackamount"),
//              tip_amount                          =:_queueItem.getFieldData("tipamount"),
//              market_specific_data_ind            =:_queueItem.getFieldData("marketspecificdataind"),
//              information_indicator               =:_queueItem.getFieldData("informationindicator"),
//              merchant_vol_indicator              =:_queueItem.getFieldData("merchantvolindicator"),
//              ecommerce_goods_indicator           =:_queueItem.getFieldData("ecommercegoodsindicator"),
//              enhanced_data_1                     =:_queueItem.getFieldData("enhanceddata1"),
//              enhanced_data_2                     =:_queueItem.getFieldData("enhanceddata2"),
//              enhanced_data_3                     =:_queueItem.getFieldData("enhanceddata3"),
//              level_iii_data_present              =:_queueItem.getFieldData("leveliiidatapresent"),
//              eligibility_flags                   =:_queueItem.getFieldData("eligibilityflags"),
//              no_show_indicator                   =:_queueItem.getFieldData("noshowindicator"),
//              hotel_rental_days                   =:_queueItem.getFieldData("hotelrentaldays"),
//              extra_charges                       =:_queueItem.getFieldData("extracharges"),
//              rate_daily                          =:_queueItem.getFieldData("ratedaily"),
//              rate_weekly                         =:_queueItem.getFieldData("rateweekly"),
//              one_way_drop_off_charges            =:_queueItem.getFieldData("onewaydropoffcharges"),
//              insurance_charges                   =:_queueItem.getFieldData("insurancecharges"),
//              fuel_charges                        =:_queueItem.getFieldData("fuelcharges"),
//              car_class_code                      =:_queueItem.getFieldData("carclasscode"),
//              renter_name                         =:_queueItem.getFieldData("rentername"),
//              total_room_tax                      =:_queueItem.getFieldData("totalroomtax"),
//              folio_cash_advances                 =:_queueItem.getFieldData("foliocashadvances"),
//              food_beverage_charges               =:_queueItem.getFieldData("foodbeveragecharges"),
//              prepaid_expenses                    =:_queueItem.getFieldData("prepaidexpenses")
//              --card_number_enc                     =:(_queueItem.getFieldData("cardnumberenc")),
//              --test_flag                           =:(_queueItem.getFieldData("testflag")),
//              --last_modified_date                  =:(_queueItem.getFieldData("lastmodifieddate")),
//              --last_modified_user                  =:(_queueItem.getFieldData("lastmodifieduser")),
//              --load_file_id                        =:(_queueItem.getFieldData("loadfileid")),
//              --load_filename                       =:(_queueItem.getFieldData("loadfilename")),
//              --output_file_id                      =:(_queueItem.getFieldData("outputfileid")),
//              --output_filename                     =:(_queueItem.getFieldData("outputfilename")),
//            where
//            ..
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_525 = _queueItem.getFieldData("acquirerbusinessid");
 String __sJT_526 = _queueItem.getFieldData("merchantnumber");
 String __sJT_527 = _queueItem.getFieldData("dbaname");
 String __sJT_528 = _queueItem.getFieldData("dbacity");
 String __sJT_529 = _queueItem.getFieldData("dbastate");
 String __sJT_530 = _queueItem.getFieldData("dbazip");
 String __sJT_531 = _queueItem.getFieldData("siccode");
 String __sJT_532 = _queueItem.getFieldData("motoecommercemerchant");
 String __sJT_533 = _queueItem.getFieldData("mvv");
 String __sJT_534 = _queueItem.getFieldData("countrycode");
 String __sJT_535 = _queueItem.getFieldData("phonenumber");
 String __sJT_536 = _queueItem.getFieldData("cardnumber");
 String __sJT_537 = _queueItem.getFieldData("cardtype");
 String __sJT_538 = _queueItem.getFieldData("cardtypeenhanced");
 String __sJT_539 = _queueItem.getFieldData("usissuer");
 String __sJT_540 = _queueItem.getFieldData("cashdisbursement");
 String __sJT_541 = _queueItem.getFieldData("debitcreditindicator");
 String __sJT_542 = _queueItem.getFieldData("transactionamount");
 String __sJT_543 = _queueItem.getFieldData("currencycode");
 String __sJT_544 = _queueItem.getFieldData("iccat");
 String __sJT_545 = _queueItem.getFieldData("iccatbilling");
 String __sJT_546 = _queueItem.getFieldData("iccatdowngrade");
 String __sJT_547 = _queueItem.getFieldData("authamount");
 String __sJT_548 = _queueItem.getFieldData("authamounttotal");
 String __sJT_549 = _queueItem.getFieldData("authcurrencycode");
 String __sJT_550 = _queueItem.getFieldData("authcode");
 String __sJT_551 = _queueItem.getFieldData("authsourcecode");
 String __sJT_552 = _queueItem.getFieldData("authresponsecode");
 String __sJT_553 = _queueItem.getFieldData("authreturnedaci");
 String __sJT_554 = _queueItem.getFieldData("authsettledaci");
 String __sJT_555 = _queueItem.getFieldData("authretrievalrefnum");
 String __sJT_556 = _queueItem.getFieldData("authtranid");
 String __sJT_557 = _queueItem.getFieldData("authvalcode");
 String __sJT_558 = _queueItem.getFieldData("authavsresponse");
 String __sJT_559 = _queueItem.getFieldData("authcvv2response");
 String __sJT_560 = _queueItem.getFieldData("authrecid");
 String __sJT_561 = _queueItem.getFieldData("tridenttranid");
 String __sJT_562 = _queueItem.getFieldData("productid");
 String __sJT_563 = _queueItem.getFieldData("cardholderidmethod");
 String __sJT_564 = _queueItem.getFieldData("specialconditionsind");
 String __sJT_565 = _queueItem.getFieldData("feeprogramindicator");
 String __sJT_566 = _queueItem.getFieldData("requestedpaymentservice");
 String __sJT_567 = _queueItem.getFieldData("reimbursementattribute");
 String __sJT_568 = _queueItem.getFieldData("referencenumber");
 String __sJT_569 = _queueItem.getFieldData("acqreferencenumber");
 String __sJT_570 = _queueItem.getFieldData("purchaseid");
 String __sJT_571 = _queueItem.getFieldData("purchaseidformat");
 String __sJT_572 = _queueItem.getFieldData("taxamount");
 String __sJT_573 = _queueItem.getFieldData("taxindicator");
 String __sJT_574 = _queueItem.getFieldData("posentrymode");
 String __sJT_575 = _queueItem.getFieldData("cardpresent");
 String __sJT_576 = _queueItem.getFieldData("motoecommerceind");
 String __sJT_577 = _queueItem.getFieldData("postermcap");
 String __sJT_578 = _queueItem.getFieldData("recurringpaymentind");
 String __sJT_579 = _queueItem.getFieldData("multipleclearingseqnum");
 String __sJT_580 = _queueItem.getFieldData("multipleclearingseqcount");
 String __sJT_581 = _queueItem.getFieldData("shiptozip");
 String __sJT_582 = _queueItem.getFieldData("shippingamount");
 String __sJT_583 = _queueItem.getFieldData("cashbackamount");
 String __sJT_584 = _queueItem.getFieldData("tipamount");
 String __sJT_585 = _queueItem.getFieldData("marketspecificdataind");
 String __sJT_586 = _queueItem.getFieldData("informationindicator");
 String __sJT_587 = _queueItem.getFieldData("merchantvolindicator");
 String __sJT_588 = _queueItem.getFieldData("ecommercegoodsindicator");
 String __sJT_589 = _queueItem.getFieldData("enhanceddata1");
 String __sJT_590 = _queueItem.getFieldData("enhanceddata2");
 String __sJT_591 = _queueItem.getFieldData("enhanceddata3");
 String __sJT_592 = _queueItem.getFieldData("leveliiidatapresent");
 String __sJT_593 = _queueItem.getFieldData("eligibilityflags");
 String __sJT_594 = _queueItem.getFieldData("noshowindicator");
 String __sJT_595 = _queueItem.getFieldData("hotelrentaldays");
 String __sJT_596 = _queueItem.getFieldData("extracharges");
 String __sJT_597 = _queueItem.getFieldData("ratedaily");
 String __sJT_598 = _queueItem.getFieldData("rateweekly");
 String __sJT_599 = _queueItem.getFieldData("onewaydropoffcharges");
 String __sJT_600 = _queueItem.getFieldData("insurancecharges");
 String __sJT_601 = _queueItem.getFieldData("fuelcharges");
 String __sJT_602 = _queueItem.getFieldData("carclasscode");
 String __sJT_603 = _queueItem.getFieldData("rentername");
 String __sJT_604 = _queueItem.getFieldData("totalroomtax");
 String __sJT_605 = _queueItem.getFieldData("foliocashadvances");
 String __sJT_606 = _queueItem.getFieldData("foodbeveragecharges");
 String __sJT_607 = _queueItem.getFieldData("prepaidexpenses");
   String theSqlTS = "update visa_settlement\n          set\n            --rec_id                              =:(_queueItem.getFieldData(\"recid\")),\n            --bin_number                          =:(_queueItem.getFieldData(\"binnumber\")),\n            acquirer_business_id                = :1 ,\n            merchant_number                     = :2 ,\n            dba_name                            = :3 ,\n            dba_city                            = :4 ,\n            dba_state                           = :5 ,\n            dba_zip                             = :6 ,\n            sic_code                            = :7 ,\n            moto_ecommerce_merchant             = :8 ,\n            mvv                                 = :9 ,\n            country_code                        = :10 ,\n            phone_number                        = :11 ,\n            card_number                         = :12 ,\n            card_type                           = :13 ,\n            card_type_enhanced                  = :14 ,\n            us_issuer                           = :15 ,\n            cash_disbursement                   = :16 ,\n            --batch_date                          =:(_queueItem.getFieldData(\"batchdate\")),\n            --batch_number                        =:(_queueItem.getFieldData(\"batchnumber\")),\n            --batch_id                            =:(_queueItem.getFieldData(\"batchid\")),\n            --batch_record_id                     =:(_queueItem.getFieldData(\"batchrecordid\")),\n            transaction_date                    = :17 ,\n            debit_credit_indicator              = :18 ,\n            transaction_amount                  = :19 ,\n            currency_code                       = :20 ,\n            ic_cat                              = :21 ,\n            ic_cat_billing                      = :22 ,\n            ic_cat_downgrade                    = :23 ,\n            auth_amount                         = :24 ,\n            auth_amount_total                   = :25 ,\n            auth_currency_code                  = :26 ,\n            auth_code                           = :27 ,\n            auth_source_code                    = :28 ,\n            auth_response_code                  = :29 ,\n            auth_date                           = :30 ,\n            auth_returned_aci                   = :31 ,\n            auth_settled_aci                    = :32 ,\n            auth_retrieval_ref_num              = :33 ,\n            auth_tran_id                        = :34 ,\n            auth_val_code                       = :35 ,\n            auth_avs_response                   = :36 ,\n            auth_cvv2_response                  = :37 ,\n            auth_rec_id                         = :38 ,\n            trident_tran_id                     = :39 ,\n            product_id                          = :40 ,\n            cardholder_id_method                = :41 ,\n            special_conditions_ind              = :42 ,\n            fee_program_indicator               = :43 ,\n            requested_payment_service           = :44 ,\n            reimbursement_attribute             = :45 ,\n            reference_number                    = :46 ,\n            acq_reference_number                = :47 ,\n            purchase_id                         = :48 ,\n            purchase_id_format                  = :49 ,\n            statement_date_begin                = :50 ,\n            statement_date_end                  = :51 ,\n            tax_amount                          = :52 ,\n            tax_indicator                       = :53 ,\n            pos_entry_mode                      = :54 ,\n            card_present                        = :55 ,\n            moto_ecommerce_ind                  = :56 ,\n            pos_term_cap                        = :57 ,\n            recurring_payment_ind               = :58 ,\n            multiple_clearing_seq_num           = :59 ,\n            multiple_clearing_seq_count         = :60 ,\n            ship_to_zip                         = :61 ,\n            shipping_amount                     = :62 ,\n            cashback_amount                     = :63 ,\n            tip_amount                          = :64 ,\n            market_specific_data_ind            = :65 ,\n            information_indicator               = :66 ,\n            merchant_vol_indicator              = :67 ,\n            ecommerce_goods_indicator           = :68 ,\n            enhanced_data_1                     = :69 ,\n            enhanced_data_2                     = :70 ,\n            enhanced_data_3                     = :71 ,\n            level_iii_data_present              = :72 ,\n            eligibility_flags                   = :73 ,\n            no_show_indicator                   = :74 ,\n            hotel_rental_days                   = :75 ,\n            extra_charges                       = :76 ,\n            rate_daily                          = :77 ,\n            rate_weekly                         = :78 ,\n            one_way_drop_off_charges            = :79 ,\n            insurance_charges                   = :80 ,\n            fuel_charges                        = :81 ,\n            car_class_code                      = :82 ,\n            renter_name                         = :83 ,\n            total_room_tax                      = :84 ,\n            folio_cash_advances                 = :85 ,\n            food_beverage_charges               = :86 ,\n            prepaid_expenses                    = :87 \n            --card_number_enc                     =:(_queueItem.getFieldData(\"cardnumberenc\")),\n            --test_flag                           =:(_queueItem.getFieldData(\"testflag\")),\n            --last_modified_date                  =:(_queueItem.getFieldData(\"lastmodifieddate\")),\n            --last_modified_user                  =:(_queueItem.getFieldData(\"lastmodifieduser\")),\n            --load_file_id                        =:(_queueItem.getFieldData(\"loadfileid\")),\n            --load_filename                       =:(_queueItem.getFieldData(\"loadfilename\")),\n            --output_file_id                      =:(_queueItem.getFieldData(\"outputfileid\")),\n            --output_filename                     =:(_queueItem.getFieldData(\"outputfilename\")),\n          where\n          ..";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.queues.SettlementRejectsQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_525);
   __sJT_st.setString(2,__sJT_526);
   __sJT_st.setString(3,__sJT_527);
   __sJT_st.setString(4,__sJT_528);
   __sJT_st.setString(5,__sJT_529);
   __sJT_st.setString(6,__sJT_530);
   __sJT_st.setString(7,__sJT_531);
   __sJT_st.setString(8,__sJT_532);
   __sJT_st.setString(9,__sJT_533);
   __sJT_st.setString(10,__sJT_534);
   __sJT_st.setString(11,__sJT_535);
   __sJT_st.setString(12,__sJT_536);
   __sJT_st.setString(13,__sJT_537);
   __sJT_st.setString(14,__sJT_538);
   __sJT_st.setString(15,__sJT_539);
   __sJT_st.setString(16,__sJT_540);
   __sJT_st.setTimestamp(17,transaction_date);
   __sJT_st.setString(18,__sJT_541);
   __sJT_st.setString(19,__sJT_542);
   __sJT_st.setString(20,__sJT_543);
   __sJT_st.setString(21,__sJT_544);
   __sJT_st.setString(22,__sJT_545);
   __sJT_st.setString(23,__sJT_546);
   __sJT_st.setString(24,__sJT_547);
   __sJT_st.setString(25,__sJT_548);
   __sJT_st.setString(26,__sJT_549);
   __sJT_st.setString(27,__sJT_550);
   __sJT_st.setString(28,__sJT_551);
   __sJT_st.setString(29,__sJT_552);
   __sJT_st.setTimestamp(30,auth_date);
   __sJT_st.setString(31,__sJT_553);
   __sJT_st.setString(32,__sJT_554);
   __sJT_st.setString(33,__sJT_555);
   __sJT_st.setString(34,__sJT_556);
   __sJT_st.setString(35,__sJT_557);
   __sJT_st.setString(36,__sJT_558);
   __sJT_st.setString(37,__sJT_559);
   __sJT_st.setString(38,__sJT_560);
   __sJT_st.setString(39,__sJT_561);
   __sJT_st.setString(40,__sJT_562);
   __sJT_st.setString(41,__sJT_563);
   __sJT_st.setString(42,__sJT_564);
   __sJT_st.setString(43,__sJT_565);
   __sJT_st.setString(44,__sJT_566);
   __sJT_st.setString(45,__sJT_567);
   __sJT_st.setString(46,__sJT_568);
   __sJT_st.setString(47,__sJT_569);
   __sJT_st.setString(48,__sJT_570);
   __sJT_st.setString(49,__sJT_571);
   __sJT_st.setTimestamp(50,statement_date_begin);
   __sJT_st.setTimestamp(51,statement_date_end);
   __sJT_st.setString(52,__sJT_572);
   __sJT_st.setString(53,__sJT_573);
   __sJT_st.setString(54,__sJT_574);
   __sJT_st.setString(55,__sJT_575);
   __sJT_st.setString(56,__sJT_576);
   __sJT_st.setString(57,__sJT_577);
   __sJT_st.setString(58,__sJT_578);
   __sJT_st.setString(59,__sJT_579);
   __sJT_st.setString(60,__sJT_580);
   __sJT_st.setString(61,__sJT_581);
   __sJT_st.setString(62,__sJT_582);
   __sJT_st.setString(63,__sJT_583);
   __sJT_st.setString(64,__sJT_584);
   __sJT_st.setString(65,__sJT_585);
   __sJT_st.setString(66,__sJT_586);
   __sJT_st.setString(67,__sJT_587);
   __sJT_st.setString(68,__sJT_588);
   __sJT_st.setString(69,__sJT_589);
   __sJT_st.setString(70,__sJT_590);
   __sJT_st.setString(71,__sJT_591);
   __sJT_st.setString(72,__sJT_592);
   __sJT_st.setString(73,__sJT_593);
   __sJT_st.setString(74,__sJT_594);
   __sJT_st.setString(75,__sJT_595);
   __sJT_st.setString(76,__sJT_596);
   __sJT_st.setString(77,__sJT_597);
   __sJT_st.setString(78,__sJT_598);
   __sJT_st.setString(79,__sJT_599);
   __sJT_st.setString(80,__sJT_600);
   __sJT_st.setString(81,__sJT_601);
   __sJT_st.setString(82,__sJT_602);
   __sJT_st.setString(83,__sJT_603);
   __sJT_st.setString(84,__sJT_604);
   __sJT_st.setString(85,__sJT_605);
   __sJT_st.setString(86,__sJT_606);
   __sJT_st.setString(87,__sJT_607);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:812^9*/
        break;

      case ALL:
      default:
        log.debug("We can't process this, since we don't know what fields to get");
    }

  }

  private void updateRejectStatus(int status)
  throws Exception
  {
    try
    {

      /*@lineinfo:generated-code*//*@lineinfo:828^7*/

//  ************************************************************
//  #sql [Ctx] { update reject_record
//          set
//          status=:status
//          where
//          rec_id = :queueItem.getId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_608 = queueItem.getId();
   String theSqlTS = "update reject_record\n        set\n        status= :1 \n        where\n        rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.queues.SettlementRejectsQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,status);
   __sJT_st.setLong(2,__sJT_608);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:835^7*/

    }
    catch(Exception e)
    {
      log.debug("updateRejectStatus(): " + e.toString() );
      throw e;
    }
  }

  /*
  Q_REJECTS_RAW                   = 370
  Q_REJECTS_UNWORKED              = 371
  Q_REJECTS_PENDING_CLEAR         = 372
  Q_REJECTS_PENDING_KILL          = 373
  Q_REJECTS_KILL                  = 374
  Q_REJECTS_COMPLETED             = 375
  */
  public void setType( int newType )
  {
    String[][]          buttons     = null;

    super.setType( newType );

    //FieldGroup group = new FieldGroup("optionalFields");
    //fields.add(new FieldGroup("optionalFields"));

    // setup the options based on type
    switch( newType )
    {
      //Q_REJECTS_RAW for test only...
      case MesQueues.Q_REJECTS_RAW:
        buttons =
          new String[][]
          {
            { "Move to Unworked",    Integer.toString(MesQueues.Q_REJECTS_UNWORKED) }
          };
        break;

      case MesQueues.Q_REJECTS_UNWORKED:
        buttons =
          new String[][]
          {
            { "Clear Transaction (Pending)",   Integer.toString(MesQueues.Q_REJECTS_PENDING_CLEAR) },
            { "Kill Transaction (Pending)",    Integer.toString(MesQueues.Q_REJECTS_PENDING_KILL) }
          };
        break;

      case MesQueues.Q_REJECTS_PENDING_CLEAR:
        buttons =
          new String[][]
          {
            { "Back to Unworked",             Integer.toString(MesQueues.Q_REJECTS_UNWORKED) },
            { "Kill Transaction (Pending)",   Integer.toString(MesQueues.Q_REJECTS_PENDING_KILL) }
          };
        break;

      case MesQueues.Q_REJECTS_PENDING_KILL:
        buttons =
          new String[][]
          {
            //{ "Clear Transaction (Pending)",  Integer.toString(MesQueues.Q_REJECTS_PENDING_CLEAR) },
            { "Back to Unworked",             Integer.toString(MesQueues.Q_REJECTS_UNWORKED) }
          };
        break;

      default:
        break;
    }

    // if there are actions provide a way to select and provide comments
    if ( buttons != null )
    {
      fields.add( new RadioButtonField("action", buttons, -1, false, "Must select an action" ) );
      fields.add( new TextareaField("actionComments",500,8,80,true));
    }
  }

  public int getMenuId()
  {
    return MesMenus.MENU_ID_EDIT_PKG_REJECTS;
  }

  public String getBackLink()
  {
    StringBuffer result = new StringBuffer();

    result.append("/jsp/menus/generic_menu.jsp?com.mes.ReportMenuId=");
    result.append(MesMenus.MENU_ID_EDIT_PKG_REJECTS);

    return result.toString();
  }

  //base QueueData class here, for queue list
  public static class SettlementRejectData extends QueueData
  {
    protected List rejects          = new LinkedList();
    public FieldGroup elements      = new FieldGroup("optionalFields");

    protected void generateFields(ResultSet rs)
    {

      if(rs != null)
      {
        int col = 1;

        try
        {

          ResultSetMetaData  metaData    = rs.getMetaData();

          //log.debug("Column type = "+metaData.getColumnTypeName(col)+"::id = "+metaData.getColumnType(col));
          for (col=1; col <= metaData.getColumnCount(); col++)
          {
              String name[] = removeUnderscores(metaData.getColumnName(col).toLowerCase());

              //log.debug("building..."+metaData.getColumnName(col));
              Field field;

              switch(metaData.getColumnType(col))
              {
                case 1:
                  //Char=1
                  field = new Field(name[0],name[1],2,2,true);
                  break;

                case 2:
                  //Number=2
                  field = new NumberField(name[0],name[1],20,20,true, metaData.getPrecision(col));
                  break;

                case 91:
                  //Date = 91
                  field = new DateField(name[0],name[1], 20, 20, true);
                  ((DateField)field).setDateFormatInput("MM/dd/yyyy hh:mm:ss");
                  ((DateField)field).setDateFormat("MM/dd/yyyy hh:mm:ss");
                  break;

                case 12:
                default:
                  //Varchar2=12 and everything else
                  field = new Field(name[0],name[1],500,30,true);
                  break;
              };

              //field.makeReadOnly();
              elements.add(field);
          }
        }
        catch(Exception e)
        {
          log.debug("skipping "+col+" :: "+e.getMessage());
        }
      }

    }

    //this is done to allow access to field-specific functions,
    //allows us to handle the insert more cleanly
    public void setFields(HttpServletRequest request)
    throws Exception
    {

      log.debug("in setFields...");

      Field field;
      for (Enumeration names = request.getParameterNames();
           names.hasMoreElements(); )
      {
        String name = (String)names.nextElement();
        String value = request.getParameter(name);
        //log.debug("name  = "+ name);
        //log.debug("value = "+ value);

        field = elements.getField(name);

        if(field != null)
        {
          //check to see if data has changed
          if(!value.equals(field.getData()))
          {

            log.debug("r name  = "+name);
            log.debug("r value = "+value);
            log.debug("F name  = "+field.getName());
            log.debug("F value = "+field.getData());

            //if it has, set it
            field.setData(value);

            //validate it and either move on, or throw
            if(!field.isValid())
            {
              throw new Exception("Invalid field data: "+ field.getLabel());
            }

          }
        }
      }

    }

    protected String[] removeUnderscores(String dbName)
    {
      String a = StringUtilities.strip(dbName,'_');
      String b = StringUtilities.replace(dbName,"_"," ");
      return new String[]{ a , b };
    }

    public String getFieldData(String fieldName)
    {
       String data;

       if( (data = elements.getData(fieldName)) != null)
       {
         return data;
       }
       else
       {
         return "";
       }
    }

    public Field getField(String name)
    {
      return elements.getField(name);
    }

    public void setExtendedData(ResultSet resultSet)
    {
      setExtendedData(resultSet, false);
    }

    public void setExtendedData(ResultSet resultSet, boolean recursive)
    {
      Reject reject;
      try
      {
        reject = new Reject( resultSet.getString("reject_code"),
                             resultSet.getString("reject_type"),
                             resultSet.getString("reject_title"),
                             resultSet.getString("reject_desc"),
                             resultSet.getString("reject_action"),
                             resultSet.getString("fields")
                           );
        rejects.add(reject);

        if(recursive)
        {
          generateFields(resultSet);

          FieldBean.setFields(elements, resultSet, false, true);

          //ask the reject to identify its respective fields
          reject.highlightFields(elements);

          //add any remaining rejects from the result set
          while(resultSet.next())
          {
            reject = new Reject( resultSet.getString("reject_code"),
                                 resultSet.getString("reject_type"),
                                 resultSet.getString("reject_title"),
                                 resultSet.getString("reject_desc"),
                                 resultSet.getString("reject_action"),
                                 resultSet.getString("fields")
                               );
            rejects.add(reject);

            //ask the reject to identify its respective fields
            reject.highlightFields(elements);
          }
        }

      }
      catch(Exception e)
      {
        log.debug("Unable to set fields from result set");
        SyncLog.LogEntry(this.getClass().getName() + "::setExtendedData()", e.toString());
      }

    }

    public ListIterator fieldIterator()
    {
      return elements.getFieldsVector().listIterator();
    }

    public Iterator rejectIterator()
    {
      return rejects.iterator();
    }

  }

  //specialized QueueData class here, for queue list item, basically set recursive
  public static class _SettlementRejectData extends SettlementRejectData
  {
    public void setExtendedData(ResultSet resultSet)
    {
      setExtendedData(resultSet, true);
    }
  }

  public static class Reject
  {
    public  String        RejectCode              = "";
    public  String        RejectType              = "";
    public  String        RejectTitle             = "";
    public  String        RejectDesc              = "";
    public  String        RejectAction            = "";

    public  String[]      FieldNameMap            = null;

    public Reject(){}

    public Reject(  String code,
                    String type,
                    String title,
                    String desc,
                    String action)
    {
      this(code, type, title, desc,action,null);
    }

    public Reject(  String code,
                    String type,
                    String title,
                    String desc,
                    String action,
                    String fields)
    {
      RejectCode              = code==null?"":code;
      RejectType              = type==null?"":type;
      RejectTitle             = title==null?"":title;
      RejectDesc              = desc==null?"":desc;
      RejectAction            = action==null?"":action;

      mapFields(fields);
    }

    private void mapFields(String fields)
    {
      if(fields != null)
      {
        FieldNameMap = fields.split(",");
      }
        /*
      if("0112".equals(RejectCode))
      {
        FieldNameMap = "countryCode";
      }
      */
    }

    public void highlightFields(Field field)
    {

      if(FieldNameMap!=null)
      {
        //look at each field name referenced by this reject
        for(int i=0;i<FieldNameMap.length;i++)
        {
          if(field instanceof FieldGroup)
          {
            //this is a fieldgroup, so get the field in question
            //and add a background for the user
            try
            {
              Field x = ((FieldGroup)field).getField(FieldNameMap[i]);
              //x.unmakeReadOnly();
              x.addHtmlExtra("class='tableBackground'");
            }
            catch(Exception e)
            {
              log.debug("Unable to find field name: " + FieldNameMap[i]);
            }
          }
          else
          {
            //simply check that this field matches the name in question
            //before you place a background in
            if(FieldNameMap[i].equals(field.getName()))
            {
              //field.unmakeReadOnly();
              field.addHtmlExtra("class='tableBackground'");
            }

          }
        }//for

      }

    }

  }

}/*@lineinfo:generated-code*/