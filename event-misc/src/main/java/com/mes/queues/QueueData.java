/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/QueueData.java $

  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 6/25/03 5:28p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.support.DateTimeFormatter;
import com.mes.support.SyncLog;

public class QueueData
{
  private long            id                = 0;
  private int             type              = 0;
  private int             itemType          = 0;
  private int             owner             = 0;
  private String          control           = "";
  private String          description       = "";
  private String          dateCreated       = "";
  private String          source            = "";
  private String          affiliate         = "";
  private String          lastChangedDate   = "";
  private String          lastUser          = "";
  private String          lockedBy          = "";
  private int             noteCount         = 0;
  private String          status            = "";
  private String          merchantNumber    = "";
  private boolean         isRush            = false;
  private String          DateFormatString  = "MM/dd/yy hh:mm:ss a";
  
  /*
  ** CONSTRUCTOR QueueData
  */
  public QueueData()
  {
  }
  
  protected String blankIfNull(String data)
  {
    String result = "";
    
    result = (data == null) ? "" : data;
    
    return result;
  }
  
  /*
  ** METHOD setData
  **
  ** Sets all the internal data from a result set
  */
  public final void setData(ResultSet rs)
  {
    try
    {
      // set internal variables from the result set
      setId(rs.getLong("id"));
      setType(rs.getInt("type"));
      setItemType(rs.getInt("item_type"));
      setOwner(rs.getInt("owner"));
      setControl(rs.getString("control"));
      setDescription(rs.getString("description"));
      setDateCreated(rs.getTimestamp("date_created"));
      setSource(rs.getString("source"));
      setAffiliate(rs.getString("affiliate"));
      setLastChangedDate(rs.getTimestamp("last_changed"));
      setLastUser(rs.getString("last_user"));
      setLockedBy(rs.getString("locked_by"));
      setNoteCount(rs.getInt("note_count"));
      setStatus(rs.getString("status"));
      
      if(rs.getString("merchant_number") != null)
      {
        setMerchantNumber(rs.getString("merchant_number"));
      }
      
      try {
        setIsRush(rs.getInt("is_rush")==1);
      }
      catch(SQLException sqle) {}

      // call the extended class setData
      setExtendedData(rs);
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::setData()", e.toString());
    }
  }
  
  /*
  ** METHOD setExtendedData
  **
  ** Allows an extending class to retrieve more data from the ResultSet
  */
  public void setExtendedData(ResultSet rs)
  {
  }
  
  /*
  ** ACCESSORS
  */
  public void setId(String id)
  {
    try
    {
      setId(Long.parseLong(id));
    }
    catch(Exception e)
    {
    }
  }
  public void setId(long id)
  {
    this.id = id;
  }
  public long getId()
  {
    return this.id;
  }
  
  public void setType(String type)
  {
    try
    {
      setType(Integer.parseInt(type));
    }
    catch(Exception e)
    {
    }
  }
  public void setType(int type)
  {
    this.type = type;
  }
  public int getType()
  {
    return this.type;
  }
  
  public void setItemType(String itemType)
  {
    try
    {
      setItemType(Integer.parseInt(itemType));
    }
    catch(Exception e)
    {
    }
  }
  public void setItemType(int itemType)
  {
    this.itemType = itemType;
  }
  public int getItemType()
  {
    return this.itemType;
  }
  
  public void setOwner(String owner)
  {
    try
    {
      setOwner(Integer.parseInt(owner));
    }
    catch(Exception e)
    {
    }
  }
  public void setOwner(int owner)
  {
    this.owner = owner;
  }
  public int getOwner()
  {
    return this.owner;
  }
  
  public void setControl(String control)
  {
    this.control = control;
  }
  public String getControl()
  {
    return blankIfNull(this.control);
  }
  
  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return blankIfNull(this.description);
  }
  
  public void setDateCreated(Timestamp dateCreated)
  {
    try
    {
      this.dateCreated = DateTimeFormatter.getFormattedDate(dateCreated, DateFormatString );
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::setDateCreated()", e.toString());
    }
  }
  public String getDateCreated()
  {
    return blankIfNull(this.dateCreated);
  }
  
  public void setSource(String source)
  {
    this.source = source;
  }
  public String getSource()
  {
    return blankIfNull(this.source);
  }
  
  public void setAffiliate(String affiliate)
  {
    this.affiliate = affiliate;
  }
  public String getAffiliate()
  {
    return blankIfNull(this.affiliate);
  }
  
  public void setLastChangedDate(Timestamp lastChangedDate)
  {
    try
    {
      this.lastChangedDate = DateTimeFormatter.getFormattedDate(lastChangedDate, DateFormatString);
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::setLastChangedDate()", e.toString());
    }
  }
  public String getLastChangedDate()
  {
    return blankIfNull(this.lastChangedDate);
  }
  
  public void setLastUser(String lastUser)
  {
    this.lastUser = lastUser;
  }
  public String getLastUser()
  {
    return blankIfNull(getLastUser(false));
  }
  public String getLastUser(boolean ignoreLock)
  {
    String result;
    
    if(!ignoreLock) {
      if(this.lockedBy == null || this.lockedBy.equals(""))
      {
        result = this.lastUser;
      }
      else
      {
        result = this.lockedBy;
      }
    } else
      result = this.lastUser;
    
    return result;
  }
  
  public void setLockedBy(String lockedBy)
  {
    this.lockedBy = lockedBy;
  }
  public String getLockedBy()
  {
    return blankIfNull(this.lockedBy);
  }
  
  public void setNoteCount(String noteCount)
  {
    try
    {
      setNoteCount(Integer.parseInt(noteCount));
    }
    catch(Exception e)
    {
    }
  }
  public void setNoteCount(int noteCount)
  {
    this.noteCount = noteCount;
  }
  public int getNoteCount()
  {
    return this.noteCount;
  }
  
  public void setMerchantNumber(String merchantNumber)
  {
    this.merchantNumber = merchantNumber;
  }
  public String getMerchantNumber()
  {
    return blankIfNull(this.merchantNumber);
  }
  
  public void setIsRush(boolean v)
  {
    isRush=v;
  }
  public boolean getIsRush()
  {
    return this.isRush;
  }

  public void setStatus(String status)
  {
    this.status = status;
  }
  public String getStatus()
  {
    return blankIfNull(this.status);
  }
  
  public void setDateFormat( String newString )
  {
    String      testVal     = null;
    
    testVal = DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),newString);
    
    if( testVal != null && !testVal.equals("") )
    {
      // format is valid store it
      DateFormatString = newString;
    }
  }
}
