/*@lineinfo:filename=CallTagQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.queues;

import java.sql.Date;
import java.sql.ResultSet;
import com.mes.constants.MesMenus;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.queues.ExtraColumnBean.LinkData;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;


public class CallTagQueue extends QueueBase
{
  public CallTagQueue()
  {
    super();
    
    createSearchFields();
  }
  
  private void createSearchFields()
  {
    FieldGroup      searchFields = initSearchFields();

    // fields will appear in this order when displayed
    searchFields.add( new DateField( "searchBeginDate","Submit Date From: ",true ) );
    ((DateField)searchFields.getField("searchBeginDate")).setDateFormat("MM/dd/yyyy");
    searchFields.add( new DateField( "searchEndDate","To:", true ) );
    ((DateField)searchFields.getField("searchEndDate")).setDateFormat("MM/dd/yyyy");
    searchFields.add( new Field    ( "searchValue","Search Criteria:", 50, 50, true ) );
    
    searchFields.add( new DropDownField("calltagSource","Call Tag Source",(new EquipActionTypeDropDownTable()),true) );
    searchFields.add( new DropDownField("accountType","Account Type",(new AccountTypeDropDownTable()),true) );
  }

  private class AccountTypeDropDownTable extends DropDownTable
  {
    public AccountTypeDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      ResultSetIterator   it      = null;
      ResultSet           rs      = null;

      try
      {
        addElement("","All Account Types");
        
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:55^9*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT distinct     
//                      APP_NAME
//                      ,APPSRCTYPE_CODE
//            FROM      
//                      ORG_APP
//            ORDER BY  
//                      APP_NAME
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT distinct     \n                    APP_NAME\n                    ,APPSRCTYPE_CODE\n          FROM      \n                    ORG_APP\n          ORDER BY  \n                    APP_NAME";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.CallTagQueue",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.queues.CallTagQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:64^9*/

        rs = it.getResultSet();
        
        String val,desc;

        while (rs.next()) {
          val = rs.getString("APPSRCTYPE_CODE");
          desc = rs.getString("APP_NAME");
          addElement(val,desc);
        }

        it.close();
      }
      catch (Exception e) {
        logEntry("AccountTypeDropDownTable.getData(" + user.getLoginName() + ")", e.toString());
      }
      finally {
        cleanUp();
      }

    }

  } // AccountTypeDropDownTable
  
  private class EquipActionTypeDropDownTable extends DropDownTable
  {
    public EquipActionTypeDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      ResultSetIterator   it      = null;
      ResultSet           rs      = null;

      try
      {
        addElement("","All Call Tag Sources");
        
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:107^9*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT    description         dsc
//                      ,code               code
//            FROM      EQUIP_ACTION_TYPE
//            ORDER BY  sort_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT    description         dsc\n                    ,code               code\n          FROM      EQUIP_ACTION_TYPE\n          ORDER BY  sort_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.CallTagQueue",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.queues.CallTagQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:113^9*/

        rs = it.getResultSet();
        
        String val,desc;

        while (rs.next()) {
          val = rs.getString("code");
          desc = rs.getString("dsc");
          addElement(val,desc);
        }

        it.close();
      }
      catch (Exception e) {
        logEntry("EquipActionTypeDropDownTable.getData(" + user.getLoginName() + ")", e.toString());
      }
      finally {
        cleanUp();
      }
    }
  
  } // EquipActionTypeDropDownTable
  
  protected void loadQueueData(UserBean user)
  {
    try
    {
      FieldGroup      searchFields  = (FieldGroup)fields.getField("searchFields");
      
      // Extract params here
      long    longVal           = 0L;
      Date    searchBeginDate   = ((DateField)searchFields.getField( "searchBeginDate" )).getSqlDate();
      Date    searchEndDate     = ((DateField)searchFields.getField( "searchEndDate" )).getSqlDate();
      String  searchValue       = searchFields.getField( "searchValue" ).getData();
      
      String  calltagSource     = searchFields.getField( "calltagSource" ).getData();
      String  accountType       = searchFields.getField( "accountType" ).getData();
      
      // get the search value as a long if possible
      try{ longVal = Long.parseLong(searchValue); } catch( Exception e ) { longVal = 0L; }
      
      // note that the class member SearchSubmitted can be used
      // if your code needs to know when the user has pressed
      // the search button on the UI.
      if ( SearchSubmitted )
      {
        // do any required handling as a 
        // result of the user pressing the
        // search button.
      }

      /*@lineinfo:generated-code*//*@lineinfo:165^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   id,
//                  qd.id                   control,
//                  qd.is_rush              is_rush,
//                  qd.type                 type,
//                  qd.item_type            item_type,
//                  qd.owner                owner,
//  				        decode(ad.name,null,eat.description,'ACR - ' || ad.name) description,
//                  m.dba_name              dba_name,
//                  qd.date_created         date_created,
//                  qd.source               source,
//                  qd.affiliate            affiliate,
//                  qd.last_changed         last_changed,
//                  qd.last_user            last_user,
//                  qd.locked_by            locked_by,
//                  count(qn.id)            note_count,
//                  qt.status               status,
//                  ct.merch_number         merchant_number,
//                  ctn.pkg_trk_num         ptn
//  
//          from    q_data                  qd,
//                  q_notes                 qn,
//                  q_types                 qt,
//                  equip_calltag           ct,
//                  equip_calltag_notification ctn,
//                  EQUIP_ACTION_TYPE       eat,
//                  mif                     m,
//                  acr                     a,
//                  acrdef                  ad
//  
//          where   qd.type         = :this.type and
//                  qd.id           = ct.ct_seq_num and
//                  qd.type         = qt.type and
//                  qd.id           = qn.id(+) and
//                  ct.parent_type  = eat.code(+) and 
//                  ct.ct_seq_num = ctn.ctid(+) and
//                  ct.merch_number  = m.merchant_number(+)
//                  and ct.parentkey_refnum = a.acr_seq_num(+)
//                  and a.acrdefid = ad.acrdef_seq_num(+)
//  
//                  and
//                  (
//                        (( :searchBeginDate is null ) or ( trunc(qd.date_created) between :searchBeginDate and :searchEndDate ))
//                    and (
//                          ( :searchValue is null ) or
//                          ( m.merchant_number = :longVal ) or
//                          ( qd.id = :longVal ) or
//                          ( lower(m.dba_name) like lower('%' || :searchValue || '%') ) or
//                          ( ctn.pkg_trk_num = :searchValue )
//                        )
//                    and (ct.parent_type = :calltagSource or :calltagSource is null)
//                    and (qd.affiliate = :accountType or :accountType is null)
//                  )
//  
//          group by qd.id,
//                   qd.id,
//          				 qd.is_rush,
//                   qd.type,
//                   qd.item_type,
//                   qd.owner,
//          				 eat.description,
//                   m.dba_name,
//                   qd.date_created,
//                   qd.source,
//                   qd.affiliate,
//                   qd.last_changed,
//                   qd.last_user,
//                   qd.locked_by,
//                   qt.status,
//                   ct.merch_number,
//                   m.dba_name,
//                   ad.name,
//                   ctn.pkg_trk_num
//  
//          order by qd.is_rush desc,qd.date_created asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                   id,\n                qd.id                   control,\n                qd.is_rush              is_rush,\n                qd.type                 type,\n                qd.item_type            item_type,\n                qd.owner                owner,\n\t\t\t\t        decode(ad.name,null,eat.description,'ACR - ' || ad.name) description,\n                m.dba_name              dba_name,\n                qd.date_created         date_created,\n                qd.source               source,\n                qd.affiliate            affiliate,\n                qd.last_changed         last_changed,\n                qd.last_user            last_user,\n                qd.locked_by            locked_by,\n                count(qn.id)            note_count,\n                qt.status               status,\n                ct.merch_number         merchant_number,\n                ctn.pkg_trk_num         ptn\n\n        from    q_data                  qd,\n                q_notes                 qn,\n                q_types                 qt,\n                equip_calltag           ct,\n                equip_calltag_notification ctn,\n                EQUIP_ACTION_TYPE       eat,\n                mif                     m,\n                acr                     a,\n                acrdef                  ad\n\n        where   qd.type         =  :1  and\n                qd.id           = ct.ct_seq_num and\n                qd.type         = qt.type and\n                qd.id           = qn.id(+) and\n                ct.parent_type  = eat.code(+) and \n                ct.ct_seq_num = ctn.ctid(+) and\n                ct.merch_number  = m.merchant_number(+)\n                and ct.parentkey_refnum = a.acr_seq_num(+)\n                and a.acrdefid = ad.acrdef_seq_num(+)\n\n                and\n                (\n                      ((  :2  is null ) or ( trunc(qd.date_created) between  :3  and  :4  ))\n                  and (\n                        (  :5  is null ) or\n                        ( m.merchant_number =  :6  ) or\n                        ( qd.id =  :7  ) or\n                        ( lower(m.dba_name) like lower('%' ||  :8  || '%') ) or\n                        ( ctn.pkg_trk_num =  :9  )\n                      )\n                  and (ct.parent_type =  :10  or  :11  is null)\n                  and (qd.affiliate =  :12  or  :13  is null)\n                )\n\n        group by qd.id,\n                 qd.id,\n        \t\t\t\t qd.is_rush,\n                 qd.type,\n                 qd.item_type,\n                 qd.owner,\n        \t\t\t\t eat.description,\n                 m.dba_name,\n                 qd.date_created,\n                 qd.source,\n                 qd.affiliate,\n                 qd.last_changed,\n                 qd.last_user,\n                 qd.locked_by,\n                 qt.status,\n                 ct.merch_number,\n                 m.dba_name,\n                 ad.name,\n                 ctn.pkg_trk_num\n\n        order by qd.is_rush desc,qd.date_created asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.queues.CallTagQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setDate(2,searchBeginDate);
   __sJT_st.setDate(3,searchBeginDate);
   __sJT_st.setDate(4,searchEndDate);
   __sJT_st.setString(5,searchValue);
   __sJT_st.setLong(6,longVal);
   __sJT_st.setLong(7,longVal);
   __sJT_st.setString(8,searchValue);
   __sJT_st.setString(9,searchValue);
   __sJT_st.setString(10,calltagSource);
   __sJT_st.setString(11,calltagSource);
   __sJT_st.setString(12,accountType);
   __sJT_st.setString(13,accountType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.queues.CallTagQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:241^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueData(" + user.getLoginName() + ")", e.toString());
    }
  }
  
  protected void loadQueueItem(long id)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:253^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   id,
//                  qd.id                   control,
//                  qd.type                 type,
//                  qd.item_type            item_type,
//                  qd.owner                owner,
//  				        decode(ad.name,null,eat.description,'ACR - ' || ad.name) description,
//                  qd.date_created         date_created,
//                  qd.source               source,
//                  qd.affiliate            affiliate,
//                  qd.last_changed         last_changed,
//                  qd.last_user            last_user,
//                  qd.is_rush              is_rush,
//                  qd.locked_by            locked_by,
//                  count(qn.id)            note_count,
//                  qt.status               status,
//                  ct.merch_number         merchant_number,
//                  ctn.pkg_trk_num         ptn
//  
//          from    q_data                      qd,
//                  q_notes                     qn,
//                  q_types                     qt,
//                  equip_calltag               ct,
//                  equip_calltag_notification  ctn,
//                  EQUIP_ACTION_TYPE           eat,
//                  acr                         a,
//                  acrdef                      ad
//  
//          where   qd.type         = :this.type and
//                  qd.id           = :id and
//                  qd.id           = ct.ct_seq_num and
//                  ct.ct_seq_num = ctn.ctid(+) and
//                  qd.type         = qt.type and
//                  qd.id           = qn.id(+) and
//                  ct.parent_type  = eat.code(+)
//                  and ct.parentkey_refnum = a.acr_seq_num(+)
//                  and a.acrdefid = ad.acrdef_seq_num(+)
//  
//          group by qd.id,
//                   qd.id,
//          				 qd.is_rush,
//                   qd.type,
//                   qd.item_type,
//                   qd.owner,
//          				 eat.description,
//                   qd.date_created,
//                   qd.source,
//                   qd.affiliate,
//                   qd.last_changed,
//                   qd.last_user,
//                   qd.locked_by,
//                   qt.status,
//                   ct.merch_number,
//                   ad.name,
//                   ctn.pkg_trk_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                   id,\n                qd.id                   control,\n                qd.type                 type,\n                qd.item_type            item_type,\n                qd.owner                owner,\n\t\t\t\t        decode(ad.name,null,eat.description,'ACR - ' || ad.name) description,\n                qd.date_created         date_created,\n                qd.source               source,\n                qd.affiliate            affiliate,\n                qd.last_changed         last_changed,\n                qd.last_user            last_user,\n                qd.is_rush              is_rush,\n                qd.locked_by            locked_by,\n                count(qn.id)            note_count,\n                qt.status               status,\n                ct.merch_number         merchant_number,\n                ctn.pkg_trk_num         ptn\n\n        from    q_data                      qd,\n                q_notes                     qn,\n                q_types                     qt,\n                equip_calltag               ct,\n                equip_calltag_notification  ctn,\n                EQUIP_ACTION_TYPE           eat,\n                acr                         a,\n                acrdef                      ad\n\n        where   qd.type         =  :1  and\n                qd.id           =  :2  and\n                qd.id           = ct.ct_seq_num and\n                ct.ct_seq_num = ctn.ctid(+) and\n                qd.type         = qt.type and\n                qd.id           = qn.id(+) and\n                ct.parent_type  = eat.code(+)\n                and ct.parentkey_refnum = a.acr_seq_num(+)\n                and a.acrdefid = ad.acrdef_seq_num(+)\n\n        group by qd.id,\n                 qd.id,\n        \t\t\t\t qd.is_rush,\n                 qd.type,\n                 qd.item_type,\n                 qd.owner,\n        \t\t\t\t eat.description,\n                 qd.date_created,\n                 qd.source,\n                 qd.affiliate,\n                 qd.last_changed,\n                 qd.last_user,\n                 qd.locked_by,\n                 qt.status,\n                 ct.merch_number,\n                 ad.name,\n                 ctn.pkg_trk_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.queues.CallTagQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setLong(2,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.queues.CallTagQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:309^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueItem(" + this.type + ", " + id + ")", e.toString());
    }
  }
  
  protected void loadExtraColumns(ExtraColumnBean extraColumns, UserBean user)
  {
    try
    {
      // add the extra columns
      LinkData linkData = null;
      linkData = extraColumns.getNewLinkData();
      linkData.addLink("merchant","merchant_number");
      
      extraColumns.addColumn("DBA Name", "dba_name");
      extraColumns.addColumn("Merchant Number", "merchant_number", "/jsp/maintenance/view_account.jsp", "blank", "", linkData);
    }
    catch(Exception e)
    {
      logEntry("loadExtraColumns()", e.toString());
    }
  }
  
  public String getDescriptionURL()
  {
    String result = "";
    
    //result = "/jsp/setup/merchinfo4.jsp?primaryKey=";
    
    return result;
  }
  
  public int getMenuId()
  {
    return MesMenus.MENU_ID_CALLTAG_QUEUES;
  }
  
  public String getBackLink()
  {
    StringBuffer result = new StringBuffer();
    
    result.append("/jsp/calltag/calltagmenu.jsp?com.mes.CallTagMenuId=");
    result.append(MesMenus.MENU_ID_CALLTAG_QUEUES);
    
    return result.toString();
  }
  
  public String getGenericSearchDesc()
  {
    StringBuffer          desc      = new StringBuffer();
    
    desc.append("<ul>");
    desc.append("  <li>Merchant Number</li>");
    desc.append("  <li>Merchant DBA Name (partial is OK)</li>");
    desc.append("  <li>Call Tag Control Number</li>");
    desc.append("  <li>Package Tracking Number</li>");
    desc.append("</ul>");
    
    return( desc.toString() );
  }
}/*@lineinfo:generated-code*/