/*@lineinfo:filename=CbtCreditHistory*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/CbtCreditHistory.sqlj $

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/15/04 4:54p $
  Version            : $Revision: 3 $

  CbtCreditDecision

  Supports CB&T credit queue decision screen (cbt_credit_decision.jsp).

  Change History:
     See VSS database

  Copyright (C) 2003 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.queues;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Vector;
import com.mes.constants.MesQueues;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class CbtCreditHistory extends SQLJConnectionBase
{
  private Vector rows = new Vector();
  
  public CbtCreditHistory(long qId, int qType)
  {
    load(qId,qType);
  }
  
  public class Row
  {
    private String logDate;
    private String eventDescription;
    private String userLogin;
    
    public Row(ResultSet rs)
    {
      loadData(rs);
    }
    
    private void loadData(ResultSet rs)
    {
      try
      {
        logDate             = DateTimeFormatter
                                .getFormattedDate(rs.getTimestamp("log_date"),
                                  "MM/dd/yy hh:mm:ss a");
        userLogin           = rs.getString("user_login");
      
        String eventDesc    = rs.getString("event_description");
        int oldQueue        = rs.getInt   ("old_type");
        int newQueue        = rs.getInt   ("new_type");
        String oldQueueDesc = rs.getString("old_description");
        String newQueueDesc = rs.getString("new_description");
        int intAction       = rs.getInt   ("action");

        StringBuffer actionBuf = new StringBuffer();
        int relevantQueue;
        switch (intAction)
        {
          case QueueLogger.QLA_ADD:
            actionBuf.append("Created in " + newQueueDesc);
            break;
            
          case QueueLogger.QLA_COPY:
            actionBuf.append("Copied to " + newQueueDesc + " from " + oldQueueDesc);
            break;
            
          case QueueLogger.QLA_MOVE:
            if (newQueue != oldQueue)
            {
              switch (newQueue)
              {
                case MesQueues.Q_CBT_NEW:
                case MesQueues.Q_CBT_NEW_HIGH_VOLUME:
                case MesQueues.Q_CBT_DOC_WORKING:
                  actionBuf.append("Moved to ");
                  break;
              
                case MesQueues.Q_CBT_PENDING:
                case MesQueues.Q_CBT_PENDING_HIGH_VOLUME:
                  actionBuf.append("Pended, moved to ");
                  break;
              
                case MesQueues.Q_CBT_DECLINED:
                case MesQueues.Q_CBT_DECLINED_HIGH_VOLUME:
                  actionBuf.append("Declined, moved to ");
                  break;
              
                case MesQueues.Q_CBT_CANCELLED:
                case MesQueues.Q_CBT_CANCELLED_HIGH_VOLUME:
                  actionBuf.append("Canceled, moved to ");
                  break;
              
                case MesQueues.Q_CBT_APPROVED:
                case MesQueues.Q_CBT_APPROVED_HIGH_VOLUME:
                  actionBuf.append("Approved, moved to ");
                  break;
                
                case MesQueues.Q_CBT_DOC_COMPLETE:
                  actionBuf.append("Completed, moved to ");
                  break;
              }
              actionBuf.append(newQueueDesc + " from " + oldQueueDesc);
            }
            else
            {
              actionBuf.append("Updated in " + oldQueueDesc);
            }
            break;
          
          case QueueLogger.QLA_DELETE:
            actionBuf.append("Deleted from " + oldQueueDesc);
            break;
        }
      
        eventDescription = actionBuf.toString();
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName()
          + "::loadData(): " + e);
        logEntry("loadData()",e.toString());
      }
    }
    
    public String getLogDate()
    {
      return logDate;
    }
    public String getEventDescription()
    {
      return eventDescription;
    }
    public String getUserLogin()
    {
      return userLogin;
    }
  }
  
  private void load(long qId, int qType)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:168^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    l.log_seq_num,
//                    l.log_date                  log_date,
//                    l.action                    action,
//                    l.old_type                  old_type,
//                    l.new_type                  new_type,
//                    l.description               event_description,
//                    l.user_name                 user_login,
//                    nvl(t_old.description,'')   old_description,
//                    nvl(t_new.description,'')   new_description
//          from      q_log             l,
//                    q_types           t_old,
//                    q_types           t_new,
//                    q_group_to_types  gt1,
//                    q_group_to_types  gt2
//          where     l.id = :qId
//                    and l.old_type = t_old.type(+)
//                    and l.user_name not like 'q_data%'
//                    and l.new_type = t_new.type(+)
//                    and ( l.old_type = gt1.queue_type
//                          or l.new_type = gt1.queue_type )
//                    and gt1.group_type = gt2.group_type
//                    and gt2.queue_type = :qType
//          group by  l.log_seq_num,
//                    l.log_date,
//                    l.action,
//                    l.old_type,              
//                    l.new_type,               
//                    l.description,
//                    l.user_name,
//                    t_old.description,
//                    t_new.description
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    l.log_seq_num,\n                  l.log_date                  log_date,\n                  l.action                    action,\n                  l.old_type                  old_type,\n                  l.new_type                  new_type,\n                  l.description               event_description,\n                  l.user_name                 user_login,\n                  nvl(t_old.description,'')   old_description,\n                  nvl(t_new.description,'')   new_description\n        from      q_log             l,\n                  q_types           t_old,\n                  q_types           t_new,\n                  q_group_to_types  gt1,\n                  q_group_to_types  gt2\n        where     l.id =  :1 \n                  and l.old_type = t_old.type(+)\n                  and l.user_name not like 'q_data%'\n                  and l.new_type = t_new.type(+)\n                  and ( l.old_type = gt1.queue_type\n                        or l.new_type = gt1.queue_type )\n                  and gt1.group_type = gt2.group_type\n                  and gt2.queue_type =  :2 \n        group by  l.log_seq_num,\n                  l.log_date,\n                  l.action,\n                  l.old_type,              \n                  l.new_type,               \n                  l.description,\n                  l.user_name,\n                  t_old.description,\n                  t_new.description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.CbtCreditHistory",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,qId);
   __sJT_st.setInt(2,qType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.queues.CbtCreditHistory",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:201^7*/
      rs = it.getResultSet();
      while (rs.next())
      {
        rows.add(new Row(rs));
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::load(): " + e);
      logEntry("load()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
      cleanUp();
    }
  }
  
  public Iterator iterator()
  {
    return rows.iterator();
  }
}/*@lineinfo:generated-code*/