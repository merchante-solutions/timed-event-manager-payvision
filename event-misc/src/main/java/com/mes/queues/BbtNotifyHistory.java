/*@lineinfo:filename=BbtNotifyHistory*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/BbtNotifyHistory.sqlj $

  Last Modified By   : $Author: Jkirton $
  Last Modified Date : $Date: 2/24/04 2:43p $
  Version            : $Revision: 1 $

  BbtNotifyHistory

  Supports BB&T qa queue setup screen

  Change History:
     See VSS database

  Copyright (C) 2003 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.queues;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class BbtNotifyHistory extends SQLJConnectionBase
{
  private Vector rows = new Vector();
  
  public BbtNotifyHistory(long qId)
  {
    load(qId);
  }
  
  public class Row
  {
    private String  sendDate;
    private String  letterType;
    private String  userLogin;
    private String  subject;
    private String  letter;
    
    public Row(ResultSet rs)
    {
      loadData(rs);
    }
    
    private void loadData(ResultSet rs)
    {
      try
      {
        sendDate    = DateTimeFormatter
                        .getFormattedDate(rs.getTimestamp("send_date"),
                          "MM/dd/yy hh:mm:ss a");
        userLogin   = rs.getString("sender");
        subject     = rs.getString("subject");
        letter      = rs.getString("letter");
        letterType  = rs.getString("letter_type");
        if (letterType.equals("E"))
        {
          letterType = "EMAIL";
        }
        else
        {
          letterType = "PRINT";
        }
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName()
          + "::loadData(): " + e);
        logEntry("loadData()",e.toString());
      }
    }
    
    public String getSendDate()
    {
      return sendDate;
    }
    public String getLetterType()
    {
      return letterType;
    }
    public String getUserLogin()
    {
      return userLogin;
    }
    public String getSubject()
    {
      return subject;
    }
    public String getLetter()
    {
      return letter;
    }
  }
  
  private void load(long qId)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:120^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    send_date,
//                    sender,
//                    letter_type,
//                    subject,
//                    letter
//                    
//          from      q_credit_letter_log
//          
//          where     id = :qId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    send_date,\n                  sender,\n                  letter_type,\n                  subject,\n                  letter\n                  \n        from      q_credit_letter_log\n        \n        where     id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.BbtNotifyHistory",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,qId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.queues.BbtNotifyHistory",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:131^7*/
      rs = it.getResultSet();
      while (rs.next())
      {
        rows.add(new Row(rs));
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::load(): " + e);
      logEntry("load()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
      cleanUp();
    }
  }
  
  public Iterator iterator()
  {
    return rows.iterator();
  }
}/*@lineinfo:generated-code*/