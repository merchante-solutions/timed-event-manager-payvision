/*@lineinfo:filename=RetrievalQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/RetrievalQueue.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-08-31 17:05:36 -0700 (Wed, 31 Aug 2011) $
  Version            : $Revision: 19222 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.RadioButtonField;
import com.mes.forms.TextareaField;
import com.mes.queues.ExtraColumnBean.LinkData;
import com.mes.support.HttpHelper;
import com.mes.support.SyncLog;
import com.mes.tools.DocumentTool;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class RetrievalQueue extends QueueBase
{
  // create class log category
  static Logger log = Logger.getLogger(RetrievalQueue.class);

  public static class RetrievalQueueData extends QueueData
  {
    public  String        CardNumber              = null;
    public  String        CreditDate              = null;
    public  String        CreditUrl               = null;
    public  String        InquiryType             = null;
    public  String        ReferenceNumber         = null;
    public  String        RetrievalRequestId      = null;
    public  String        SaleUrl                 = null;
    public  String        DocAttachedFlag         = null;
    public  double        TranAmount              = 0.0;
    public  Date          TranDate                = null;
    private long          id                      = 0L;

    public void setExtendedData(ResultSet resultSet)
    {
      StringBuffer      temp       = new StringBuffer();

      try
      {
        CardNumber          = processString(resultSet.getString("card_number"));
        CreditDate          = processString(resultSet.getString("credit_batch_date"));
        ReferenceNumber     = processString(resultSet.getString("ref_num"));
        RetrievalRequestId  = processString(resultSet.getString("retrieval_request_id"));
        TranAmount          = resultSet.getDouble("tran_amount");
        TranDate            = resultSet.getDate("tran_date");
        id                  = resultSet.getLong("retr_load_sec");
        DocAttachedFlag     = processString(resultSet.getString("docs_attached"));
        InquiryType         = processString(resultSet.getString("inquiry_type"));

        if ( resultSet.getInt("tran_data_valid") == 1 )
        {
          temp.setLength(0);
          temp.append("/jsp/utils/find_transaction.jsp?dataType=0&reportType=1&findTran=true");
          temp.append("&findHierarchyNode=");
          temp.append(resultSet.getLong("merchant_number"));
          temp.append("&reportDateBegin=");
          temp.append(resultSet.getString("dt_batch_date"));
          temp.append("&reportDateEnd=");
          temp.append(resultSet.getString("dt_batch_date"));
          temp.append("&findCardNum=");
          temp.append(CardNumber);

          SaleUrl = temp.toString();
        }

        if ( resultSet.getInt("credit_issued") == 1 )
        {
          temp.setLength(0);
          temp.append("/jsp/utils/find_transaction.jsp?dataType=0&reportType=1&findTran=true");
          temp.append("&findHierarchyNode=");
          temp.append(resultSet.getLong("merchant_number"));
          temp.append("&reportDateBegin=");
          temp.append(resultSet.getString("credit_batch_date"));
          temp.append("&reportDateEnd=");
          temp.append(resultSet.getString("credit_batch_date"));
          temp.append("&findCardNum=");
          temp.append(CardNumber);

          CreditUrl = temp.toString();
        }
      }
      catch(Exception e)
      {
        SyncLog.LogEntry(this.getClass().getName() + "::setExtendedData()", e.toString());
      }
    }

    public long getId()
    {
      return id;
    }

    /************************************************
     * new DOCS section for Amex Chargebacks - handled while considered Retrievals
     * copied from ChargebackQueue.ChargebackQueueData
     ************************************************/
    private ArrayList documents = null; //contains ../tools/DocData.java for now

    public void buildDocuments()
    {
      DocumentTool  docTool = new DocumentTool();
      ArrayList     temp    = null;

      documents = docTool.buildSimpleDocList(getId());
      /*
      if ( OriginalLoadSec != 0L )
      {
        temp = docTool.buildSimpleDocList(OriginalLoadSec);
        if ( documents == null )
        {
          documents = temp;
        }
        else if ( temp != null )
        {
          documents.addAll( temp );
        }
      }
      */
    }

    public void attachDocument(String fileName, String docType, String newFileName, byte[] body)
    throws Exception
    {

      log.debug("fileName = "+fileName);
      log.debug("docType = "+docType);
      log.debug("newFileName = "+newFileName);

      //fileName cannot be null
      if(null==fileName)
      {
        throw new Exception("Filename is invalid.");
      }

      //cbDocType = 1 = merchant support doc
      int cbDocType=1;
      try
      {

        cbDocType = Integer.parseInt(docType);
        DocumentTool docTool = new DocumentTool();
        docTool.attachCBDocument(this.getId(), cbDocType, fileName, newFileName, body);
      }
      catch (NumberFormatException nfe)
      {
        throw new Exception("Unable to attach document type selected");
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public boolean removeDocument(String[] docIds)
    {
      if(docIds==null)
      {
        return false;
      }

      DocumentTool docTool = new DocumentTool();
      long dId;
      boolean result = true;

      for(int i=0;i<docIds.length;i++){
        try
        {
          dId = Long.parseLong(docIds[i]);
        }
        catch(Exception e)
        {
          //all or nothing
          return false;
        }
        result = docTool.removeCBDocument(this.getId(), dId);
        if(!result)
        {
          break;
        }
      }

      return result;

    }

    public Iterator getDocuments()
    {
      try
      {
        return documents.iterator();
      }
      catch( Exception e)
      {
        return null;
      }
    }

    public boolean hasDocuments()
    {
      return documents != null;
    }


 }

  public RetrievalQueue( )
  {
    FieldGroup      searchFields  = initSearchFields();

    searchFields.add( new DateField( "searchBeginDate","Incoming Date From: ",true ) );
    ((DateField)searchFields.getField("searchBeginDate")).setDateFormat("MM/dd/yyyy");
    searchFields.add( new DateField( "searchEndDate","To:", true ) );
    ((DateField)searchFields.getField("searchEndDate")).setDateFormat("MM/dd/yyyy");
    searchFields.add( new Field    ( "searchValue","Search Criteria:", 50, 50, true ) );

    // initialize the multi-move module
    initMultiMoveFields();
  }

  public void doAction( HttpServletRequest request )
  {
    int   destQueueId   = HttpHelper.getInt(request,  "action", -1);
    long  itemId        = HttpHelper.getLong(request, "id",     -1);

    // allow the base class to act
    super.doAction(request);

    try
    {
      connect();

      switch( destQueueId )
      {
        case MesQueues.Q_RETRIEVALS_VISA_COMPLETED:
        case MesQueues.Q_RETRIEVALS_MC_COMPLETED:
          /*@lineinfo:generated-code*//*@lineinfo:266^11*/

//  ************************************************************
//  #sql [Ctx] { update  network_retrievals
//              set     received_date = trunc(sysdate)
//              where   retr_load_sec = :itemId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  network_retrievals\n            set     received_date = trunc(sysdate)\n            where   retr_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.queues.RetrievalQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,itemId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:271^11*/
          /*@lineinfo:generated-code*//*@lineinfo:272^11*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:272^31*/
          break;

        default:
          break;
      }
    }
    catch( Exception e )
    {
      logEntry( "doAction()", e.toString() );
    }
    finally
    {
      cleanUp();
    }
  }

  public String getActionDisplayName( )
  {
    return( "Retrieval Request Status" );
  }

  public String getBackLink()
  {
    return("/jsp/reports/index.jsp?com.mes.ReportMenuId=" + getMenuId());
  }

  public String getDescriptionURL(QueueData qd)
  {
    long              merchantId  = 0L;
    String            retVal      = null;
    StringBuffer      temp        = new StringBuffer();

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:309^7*/

//  ************************************************************
//  #sql [Ctx] { select  rt.merchant_number 
//          from    network_retrievals rt
//          where   rt.retr_load_sec = :qd.getId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_523 = qd.getId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  rt.merchant_number  \n        from    network_retrievals rt\n        where   rt.retr_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.RetrievalQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_523);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:314^7*/

      if ( merchantId != 0L )
      {
        temp.append("/jsp/maintenance/view_account.jsp?merchant=");
        temp.append(merchantId);
        retVal = temp.toString();
      }
    }
    catch( Exception e )
    {
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }

  protected QueueData getEmptyQueueData()
  {
    QueueData       retVal = new RetrievalQueueData();
    retVal.setDateFormat("MM/dd/yyyy");
    return( retVal );
  }

  public String getGenericSearchDesc()
  {
    StringBuffer          desc      = new StringBuffer();


    desc.append("<ul>");
    desc.append("  <li>Merchant Number</li>");
    desc.append("  <li>Merchant DBA Name (partial is OK)</li>");
    desc.append("  <li>Case #</li>");
    desc.append("  <li>Retrieval Request ID</li>");
    desc.append("  <li>Reference Number</li>");
    desc.append("  <li>Transaction Amount</li>");
    desc.append("  <li>Card Number (min first 6)</li>");
    desc.append("</ul>");

    return( desc.toString() );
  }

  public int getMenuId()
  {
    return( MesMenus.MENU_ID_RETRIEVAL_QUEUES );
  }

  public String getMultiMoveColumnHeader()
  {
    return("Pend");
  }

  public void handleMultiMove( )
  {
    CheckboxField   field       = null;
    boolean         moveAll     = false;
    Vector          moveFields  = getMultiMoveFields().getFieldsVector();
    int             queueDest   = 0;
    long            queueId     = 0L;
    int             queueType   = getType();
    String          userName    = null;

    try
    {
      field = (CheckboxField)fields.getField("multiMoveAll");
      moveAll = field.isChecked();
    }
    catch(Exception e)
    {
    }

    for( int i = 0; i < moveFields.size(); ++i )
    {
      try
      {
        field = (CheckboxField)moveFields.elementAt(i);

        if ( field.getName().equals("multiMoveAll") )
        {
          continue;   // skip the all checkbox
        }
      }
      catch(ClassCastException e)
      {
        // only interested in the checkboxes
        continue;
      }

      if ( field.isChecked() || moveAll )
      {
        // field names will be "id1234" where 1234 is the id of
        // the row to be moved.
        queueId   = Long.parseLong(field.getName().substring(2));

        switch( queueType )
        {
          case MesQueues.Q_RETRIEVALS_VISA_INCOMING:
            queueDest = MesQueues.Q_RETRIEVALS_VISA_PENDING;
            break;

          case MesQueues.Q_RETRIEVALS_MC_INCOMING:
            queueDest = MesQueues.Q_RETRIEVALS_MC_PENDING;
            break;

          case MesQueues.Q_RETRIEVALS_AMEX_INCOMING:
            queueDest = MesQueues.Q_RETRIEVALS_AMEX_PENDING;
            break;
            
          case MesQueues.Q_RETRIEVALS_DS_INCOMING:
            queueDest = MesQueues.Q_RETRIEVALS_DS_PENDING;
            break;

          default:
            continue;
        }
        QueueTools.moveQueueItem(queueId, queueType, queueDest, this.user, "");
      }
    }
  }

  //
  // overload the default handling of the enable flag
  // to determine it based on the specific queue type
  //
  public boolean isMultiMoveEnabled()
  {
    boolean     retVal      = false;

    switch(getType())
    {
      case MesQueues.Q_RETRIEVALS_VISA_INCOMING:
      case MesQueues.Q_RETRIEVALS_MC_INCOMING:
      case MesQueues.Q_RETRIEVALS_AMEX_INCOMING:
      case MesQueues.Q_RETRIEVALS_DS_INCOMING:
        retVal = true;
        break;
    }
    return(retVal);
  }

  protected void loadExtraColumns(ExtraColumnBean extraColumns, UserBean user)
  {
    LinkData        linkData    = null;

    try
    {
      // add the extra columns
      if ( getType() == MesQueues.Q_RETRIEVALS_ALL )
      {
        extraColumns.addColumn("Queue", "queue_desc");
      }
      linkData = extraColumns.getNewLinkData();
      linkData.setValidationColName("merchant_number_missing");
      linkData.addLink("loadSec","id");
      extraColumns.addColumn("Merch #", "merchant_number", "/jsp/admin/cb_assign_mid.jsp?itemType=R", null, null, linkData);

      extraColumns.addColumn("Card #", "card_number");
      extraColumns.addColumn("Amount","tran_amount","","","C");
      extraColumns.addColumn("Retr Req ID","retrieval_request_id");
      extraColumns.addColumn("Tran Date","tran_date_str");

      // setup the tran ref # column as a link to
      // the find transaction screen.
      linkData = extraColumns.getNewLinkData();
      linkData.setValidationColName("tran_data_valid");
      linkData.addLink("findHierarchyNode","merchant_number");
      linkData.addLink("reportDateBegin","dt_batch_date");
      linkData.addLink("reportDateEnd","dt_batch_date");
      linkData.addLink("findCardNum","card_number");
      extraColumns.addColumn("Tran Ref #", "ref_num", "/jsp/utils/find_transaction.jsp?dataType=0&reportType=1&findTran=true", "_findTran", null, linkData);
      extraColumns.addColumn("Inquiry Type", "inquiry_type");

      // setup the credit date as a link to the
      // find transaction screen for the credit
      linkData = extraColumns.getNewLinkData();
      linkData.setValidationColName("credit_issued");
      linkData.addLink("findHierarchyNode","merchant_number");
      linkData.addLink("reportDateBegin","credit_batch_date");
      linkData.addLink("reportDateEnd","credit_batch_date");
      linkData.addLink("findCardNum","card_number");
      extraColumns.addColumn("Credit Date", "credit_batch_date", "/jsp/utils/find_transaction.jsp?dataType=0&reportType=1&findTran=true", "_findTran", null, linkData);
      extraColumns.addColumn("Docs rec'd", "docs_attached");
      extraColumns.addColumn("Days Out","days_out");
    }
    catch(Exception e)
    {
      logEntry("loadExtraColumns()", e.toString());
    }
  }

  //
  // Overload the base class load method because
  // retrievals use the bank number from the user
  // login to mask some chargebacks.
  //
  protected void createMultiMoveFields(HttpServletRequest request)
  {
    int                       bankNumber    = 0;
    FieldGroup                fgroup        = null;
    ResultSetIterator         it            = null;
    ResultSet                 rs            = null;

    try
    {
      connect();

      // get the bank number from the hierarchy node
      bankNumber  = Integer.parseInt(Long.toString(user.getHierarchyNode()).substring(0,4));

      // create the necessary checkbox fields
      fgroup = (FieldGroup)fields.getField( "multiMoveFields" );

      /*@lineinfo:generated-code*//*@lineinfo:528^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id   as id
//          from    q_data                qd,
//                  network_retrievals    rt
//          where   qd.type = :type and
//                  rt.retr_load_sec = qd.id and
//                  (
//                    ( :bankNumber = 9999 and
//                      rt.bank_number in (select bank_number from mbs_banks where owner != 0) ) or
//                    ( rt.bank_number = :bankNumber )
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id   as id\n        from    q_data                qd,\n                network_retrievals    rt\n        where   qd.type =  :1  and\n                rt.retr_load_sec = qd.id and\n                (\n                  (  :2  = 9999 and\n                    rt.bank_number in (select bank_number from mbs_banks where owner != 0) ) or\n                  ( rt.bank_number =  :3  )\n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.queues.RetrievalQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,type);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setInt(3,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.queues.RetrievalQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:540^7*/
      rs = it.getResultSet();

      while(rs.next())
      {
        fgroup.add( new CheckboxField("id"+rs.getLong("id"),false) );
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("createMultiMoveFields()",e.toString());
    }
    finally
    {
      try{it.close();}catch(Exception e){}
      cleanUp();
    }
  }

  protected void loadQueueData(UserBean user)
  {
    int             bankNumber    = 0;
    double          doubleVal     = 0.0;
    long            longVal       = -1L;
    FieldGroup      searchFields  = (FieldGroup)fields.getField("searchFields");

    try
    {
      bankNumber  = Integer.parseInt(Long.toString(user.getHierarchyNode()).substring(0,4));

      Date    searchBeginDate   = ((DateField)searchFields.getField( "searchBeginDate" )).getSqlDate();
      Date    searchEndDate     = ((DateField)searchFields.getField( "searchEndDate" )).getSqlDate();
      String  searchValue       = searchFields.getField( "searchValue" ).getData();

      // get the value as a long if possible
      try{ longVal = Long.parseLong(searchValue); } catch( Exception e ) { longVal = -1L; }
      try{ doubleVal = Double.parseDouble(searchValue); } catch( Exception e ) { doubleVal = -1.0; }

      //log.debug("searchValue = " +searchValue);
      //log.debug("longVal = " +longVal);
      //log.debug("doubleVal = " +doubleVal);

      /*@lineinfo:generated-code*//*@lineinfo:584^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    -- required fields
//                    qd.id                                   as id,
//                    qd.type                                 as type,
//                    qd.item_type                            as item_type,
//                    qd.owner                                as owner,
//                    qd.id                                   as control,
//                    rt.merchant_name                        as description,
//                    trunc(qd.date_created)                  as date_created,
//                    qd.source                               as source,
//                    qd.affiliate                            as affiliate,
//                    qd.last_changed                         as last_changed,
//                    qd.last_user                            as last_user,
//                    qd.locked_by                            as locked_by,
//                    nvl(qn.note_count,0)                    as note_count,
//                    qt.status                               as status,
//                    qt.short_desc                           as queue_desc,
//                    -- options fields
//                    rt.merchant_number                      as merchant_number,
//                    decode(nvl(rt.merchant_number,0),
//                           0,1,0)                           as merchant_number_missing,
//                    -- extended fields
//                    rt.inquiry_type                         as inquiry_type,
//                    rt.card_number                          as card_number,
//                    to_char(rt.tran_date,'mm/dd/yyyy')      as tran_date_str,
//                    rt.tran_date                            as tran_date,
//                    rt.reference_number                     as ref_num,
//                    rt.tran_amount                          as tran_amount,
//                    nvl(rt.retrieval_request_id,' ')        as retrieval_request_id,
//                    rt.retr_load_sec                        as retr_load_sec,
//                    trunc(sysdate)-trunc(qd.date_created)   as days_out,
//                    decode( rt.dt_batch_date,
//                            null, 0, 1 )                    as tran_data_valid,
//                    to_char(rt.dt_batch_date,'mm/dd/yyyy')  as dt_batch_date,
//                    rt.dt_batch_number                      as dt_batch_number,
//                    rt.dt_ddf_dt_id                         as dt_ddf_dt_id,
//                    to_char(rt.credit_batch_date,'mm/dd/yyyy') as credit_batch_date,
//                    rt.credit_batch_number                  as credit_batch_number,
//                    rt.credit_ddf_dt_id                     as credit_ddf_dt_id,
//                    decode(rt.credit_batch_date,
//                           null, 0, 1 )                     as credit_issued,
//                    decode(dd.docCount,null,' ',0, ' ', 'Y')        as docs_attached
//          from      q_data                  qd,
//                    ( select    qnote.id,
//                                count(*)  note_count
//                      from      q_group_to_types    qg,
//                                q_notes             qnote
//                      where     qg.group_type = :MesQueues.QG_RETRIEVAL_QUEUES and
//                                qnote.type = qg.queue_type
//                      group by  qnote.id )  qn,
//                    q_types                 qt,
//                    network_retrievals      rt,
//                    users                   u,
//                    (select cbd.cb_load_sec as load_sec,
//                     count(cbd.doc_id) as docCount from
//                     chargeback_docs cbd, document d
//                     where cbd.doc_id = d.doc_id
//                     and decode(d.can_remove, 'Y', 0, 1) > 0
//                     group by cbd.cb_load_sec
//                    )  dd
//          where     (
//                      qd.type = :this.type or
//                      (
//                        :this.type = :MesQueues.Q_RETRIEVALS_ALL and -- 1700 and
//                        not :searchValue is null and
//                        qd.item_type = :MesQueues.Q_ITEM_TYPE_RETRIEVAL -- 19
//                      )
//                    ) and
//                    (
//                      ( :searchBeginDate is null and
//                        trunc(qd.date_created) >= (trunc(sysdate)-180) ) or
//                      ( trunc(qd.date_created) between :searchBeginDate and :searchEndDate )
//                    ) and
//                    (
//                      ( :searchValue is null ) or
//                      ( rt.merchant_number = :longVal ) or
//                      ( qd.id = :longVal ) or
//                      ( to_char(rt.reference_number) = :searchValue  ) or
//                      ( rt.retrieval_request_id = :searchValue ) or
//                      ( rt.tran_amount = :doubleVal ) or
//                      ( lower(rt.merchant_name) like lower('%' || :searchValue || '%') ) or
//                      ( rt.card_number like ( substr(:searchValue,1,6) || 'xxxxxx' ||
//                                              decode( length(:searchValue),
//                                                      16, substr(:searchValue,-4),
//                                                      '%' ) ) )
//                    ) and
//                    qd.type = qt.type and
//                    qn.id(+) = qd.id and
//                    rt.retr_load_sec = qd.id and
//                    (
//                      ( :bankNumber = 9999 and
//                        rt.bank_number in (select bank_number from mbs_banks where owner != 0) ) or
//                      ( rt.bank_number = :bankNumber )
//                    ) and
//                    u.login_name(+) = qd.source
//                    and dd.load_sec(+) = qd.id
//          order by  (trunc(sysdate) - trunc(qd.date_created)) desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    -- required fields\n                  qd.id                                   as id,\n                  qd.type                                 as type,\n                  qd.item_type                            as item_type,\n                  qd.owner                                as owner,\n                  qd.id                                   as control,\n                  rt.merchant_name                        as description,\n                  trunc(qd.date_created)                  as date_created,\n                  qd.source                               as source,\n                  qd.affiliate                            as affiliate,\n                  qd.last_changed                         as last_changed,\n                  qd.last_user                            as last_user,\n                  qd.locked_by                            as locked_by,\n                  nvl(qn.note_count,0)                    as note_count,\n                  qt.status                               as status,\n                  qt.short_desc                           as queue_desc,\n                  -- options fields\n                  rt.merchant_number                      as merchant_number,\n                  decode(nvl(rt.merchant_number,0),\n                         0,1,0)                           as merchant_number_missing,\n                  -- extended fields\n                  rt.inquiry_type                         as inquiry_type,\n                  rt.card_number                          as card_number,\n                  to_char(rt.tran_date,'mm/dd/yyyy')      as tran_date_str,\n                  rt.tran_date                            as tran_date,\n                  rt.reference_number                     as ref_num,\n                  rt.tran_amount                          as tran_amount,\n                  nvl(rt.retrieval_request_id,' ')        as retrieval_request_id,\n                  rt.retr_load_sec                        as retr_load_sec,\n                  trunc(sysdate)-trunc(qd.date_created)   as days_out,\n                  decode( rt.dt_batch_date,\n                          null, 0, 1 )                    as tran_data_valid,\n                  to_char(rt.dt_batch_date,'mm/dd/yyyy')  as dt_batch_date,\n                  rt.dt_batch_number                      as dt_batch_number,\n                  rt.dt_ddf_dt_id                         as dt_ddf_dt_id,\n                  to_char(rt.credit_batch_date,'mm/dd/yyyy') as credit_batch_date,\n                  rt.credit_batch_number                  as credit_batch_number,\n                  rt.credit_ddf_dt_id                     as credit_ddf_dt_id,\n                  decode(rt.credit_batch_date,\n                         null, 0, 1 )                     as credit_issued,\n                  decode(dd.docCount,null,' ',0, ' ', 'Y')        as docs_attached\n        from      q_data                  qd,\n                  ( select    qnote.id,\n                              count(*)  note_count\n                    from      q_group_to_types    qg,\n                              q_notes             qnote\n                    where     qg.group_type =  :1  and\n                              qnote.type = qg.queue_type\n                    group by  qnote.id )  qn,\n                  q_types                 qt,\n                  network_retrievals      rt,\n                  users                   u,\n                  (select cbd.cb_load_sec as load_sec,\n                   count(cbd.doc_id) as docCount from\n                   chargeback_docs cbd, document d\n                   where cbd.doc_id = d.doc_id\n                   and decode(d.can_remove, 'Y', 0, 1) > 0\n                   group by cbd.cb_load_sec\n                  )  dd\n        where     (\n                    qd.type =  :2  or\n                    (\n                       :3  =  :4  and -- 1700 and\n                      not  :5  is null and\n                      qd.item_type =  :6  -- 19\n                    )\n                  ) and\n                  (\n                    (  :7  is null and\n                      trunc(qd.date_created) >= (trunc(sysdate)-180) ) or\n                    ( trunc(qd.date_created) between  :8  and  :9  )\n                  ) and\n                  (\n                    (  :10  is null ) or\n                    ( rt.merchant_number =  :11  ) or\n                    ( qd.id =  :12  ) or\n                    ( to_char(rt.reference_number) =  :13   ) or\n                    ( rt.retrieval_request_id =  :14  ) or\n                    ( rt.tran_amount =  :15  ) or\n                    ( lower(rt.merchant_name) like lower('%' ||  :16  || '%') ) or\n                    ( rt.card_number like ( substr( :17 ,1,6) || 'xxxxxx' ||\n                                            decode( length( :18 ),\n                                                    16, substr( :19 ,-4),\n                                                    '%' ) ) )\n                  ) and\n                  qd.type = qt.type and\n                  qn.id(+) = qd.id and\n                  rt.retr_load_sec = qd.id and\n                  (\n                    (  :20  = 9999 and\n                      rt.bank_number in (select bank_number from mbs_banks where owner != 0) ) or\n                    ( rt.bank_number =  :21  )\n                  ) and\n                  u.login_name(+) = qd.source\n                  and dd.load_sec(+) = qd.id\n        order by  (trunc(sysdate) - trunc(qd.date_created)) desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.queues.RetrievalQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.QG_RETRIEVAL_QUEUES);
   __sJT_st.setInt(2,this.type);
   __sJT_st.setInt(3,this.type);
   __sJT_st.setInt(4,MesQueues.Q_RETRIEVALS_ALL);
   __sJT_st.setString(5,searchValue);
   __sJT_st.setInt(6,MesQueues.Q_ITEM_TYPE_RETRIEVAL);
   __sJT_st.setDate(7,searchBeginDate);
   __sJT_st.setDate(8,searchBeginDate);
   __sJT_st.setDate(9,searchEndDate);
   __sJT_st.setString(10,searchValue);
   __sJT_st.setLong(11,longVal);
   __sJT_st.setLong(12,longVal);
   __sJT_st.setString(13,searchValue);
   __sJT_st.setString(14,searchValue);
   __sJT_st.setDouble(15,doubleVal);
   __sJT_st.setString(16,searchValue);
   __sJT_st.setString(17,searchValue);
   __sJT_st.setString(18,searchValue);
   __sJT_st.setString(19,searchValue);
   __sJT_st.setInt(20,bankNumber);
   __sJT_st.setInt(21,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.queues.RetrievalQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:682^7*/
    }
    catch(Exception e)
    {
      e.printStackTrace();
      //logEntry("getQueueData(" + user.getLoginName() + ")", e.toString());
    }
  }

  protected void loadQueueItem(long id)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:695^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    -- required fields
//                    qd.id                                   as id,
//                    qd.type                                 as type,
//                    qd.item_type                            as item_type,
//                    qd.owner                                as owner,
//                    qd.id                                   as control,
//                    rt.merchant_name                        as description,
//                    trunc(qd.date_created)                  as date_created,
//                    qd.source                               as source,
//                    qd.affiliate                            as affiliate,
//                    qd.last_changed                         as last_changed,
//                    qd.last_user                            as last_user,
//                    qd.locked_by                            as locked_by,
//                    nvl(qn.note_count,0)                    as note_count,
//                    qt.status                               as status,
//                    qt.short_desc                           as queue_desc,
//                    -- options fields
//                    rt.merchant_number                      as merchant_number,
//                    -- extended fields
//                    rt.inquiry_type                         as inquiry_type,
//                    rt.card_number                          as card_number,
//                    to_char(rt.tran_date,'mm/dd/yyyy')      as tran_date_str,
//                    rt.tran_date                            as tran_date,
//                    rt.reference_number                     as ref_num,
//                    rt.tran_amount                          as tran_amount,
//                    nvl(rt.retrieval_request_id,' ')        as retrieval_request_id,
//                    rt.retr_load_sec                        as retr_load_sec,
//                    decode( rt.dt_batch_date,
//                            null, 0, 1 )                    as tran_data_valid,
//                    to_char(rt.dt_batch_date,'mm/dd/yyyy')  as dt_batch_date,
//                    rt.dt_batch_number                      as dt_batch_number,
//                    rt.dt_ddf_dt_id                         as dt_ddf_dt_id,
//                    to_char(rt.credit_batch_date,'mm/dd/yyyy') as credit_batch_date,
//                    rt.credit_batch_number                  as credit_batch_number,
//                    rt.credit_ddf_dt_id                     as credit_ddf_dt_id,
//                    decode(rt.credit_batch_date,
//                           null, 0, 1 )                     as credit_issued,
//                    decode(dd.docCount,null,' ',0, ' ', 'Y')        as docs_attached
//          from      q_data                  qd,
//                    ( select    qnote.id,
//                                count(*)  note_count
//                      from      q_group_to_types    qg,
//                                q_notes             qnote
//                      where     qg.group_type = :MesQueues.QG_RETRIEVAL_QUEUES and
//                                qnote.type = qg.queue_type
//                      group by  qnote.id )  qn,
//                    q_types                 qt,
//                    network_retrievals      rt,
//                    users                   u,
//                    (select count(cbd.doc_id) as docCount from
//                     chargeback_docs cbd, document d
//                     where cbd.cb_load_sec = :id
//                     and cbd.doc_id = d.doc_id
//                     and decode(d.can_remove, 'Y', 0, 1) > 0
//                    )  dd
//          where     (
//                      qd.type = :this.type or
//                      (
//                        :this.type = :MesQueues.Q_RETRIEVALS_ALL and -- 1700 and
//                        qd.item_type = :MesQueues.Q_ITEM_TYPE_RETRIEVAL -- 19
//                      )
//                    ) and
//                    qd.id = :id and
//                    qd.type = qt.type and
//                    qn.id(+) = qd.id and
//                    rt.retr_load_sec = qd.id and
//                    u.login_name(+) = qd.source
//  
//          order by  (trunc(sysdate) - trunc(qd.date_created)) desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    -- required fields\n                  qd.id                                   as id,\n                  qd.type                                 as type,\n                  qd.item_type                            as item_type,\n                  qd.owner                                as owner,\n                  qd.id                                   as control,\n                  rt.merchant_name                        as description,\n                  trunc(qd.date_created)                  as date_created,\n                  qd.source                               as source,\n                  qd.affiliate                            as affiliate,\n                  qd.last_changed                         as last_changed,\n                  qd.last_user                            as last_user,\n                  qd.locked_by                            as locked_by,\n                  nvl(qn.note_count,0)                    as note_count,\n                  qt.status                               as status,\n                  qt.short_desc                           as queue_desc,\n                  -- options fields\n                  rt.merchant_number                      as merchant_number,\n                  -- extended fields\n                  rt.inquiry_type                         as inquiry_type,\n                  rt.card_number                          as card_number,\n                  to_char(rt.tran_date,'mm/dd/yyyy')      as tran_date_str,\n                  rt.tran_date                            as tran_date,\n                  rt.reference_number                     as ref_num,\n                  rt.tran_amount                          as tran_amount,\n                  nvl(rt.retrieval_request_id,' ')        as retrieval_request_id,\n                  rt.retr_load_sec                        as retr_load_sec,\n                  decode( rt.dt_batch_date,\n                          null, 0, 1 )                    as tran_data_valid,\n                  to_char(rt.dt_batch_date,'mm/dd/yyyy')  as dt_batch_date,\n                  rt.dt_batch_number                      as dt_batch_number,\n                  rt.dt_ddf_dt_id                         as dt_ddf_dt_id,\n                  to_char(rt.credit_batch_date,'mm/dd/yyyy') as credit_batch_date,\n                  rt.credit_batch_number                  as credit_batch_number,\n                  rt.credit_ddf_dt_id                     as credit_ddf_dt_id,\n                  decode(rt.credit_batch_date,\n                         null, 0, 1 )                     as credit_issued,\n                  decode(dd.docCount,null,' ',0, ' ', 'Y')        as docs_attached\n        from      q_data                  qd,\n                  ( select    qnote.id,\n                              count(*)  note_count\n                    from      q_group_to_types    qg,\n                              q_notes             qnote\n                    where     qg.group_type =  :1  and\n                              qnote.type = qg.queue_type\n                    group by  qnote.id )  qn,\n                  q_types                 qt,\n                  network_retrievals      rt,\n                  users                   u,\n                  (select count(cbd.doc_id) as docCount from\n                   chargeback_docs cbd, document d\n                   where cbd.cb_load_sec =  :2 \n                   and cbd.doc_id = d.doc_id\n                   and decode(d.can_remove, 'Y', 0, 1) > 0\n                  )  dd\n        where     (\n                    qd.type =  :3  or\n                    (\n                       :4  =  :5  and -- 1700 and\n                      qd.item_type =  :6  -- 19\n                    )\n                  ) and\n                  qd.id =  :7  and\n                  qd.type = qt.type and\n                  qn.id(+) = qd.id and\n                  rt.retr_load_sec = qd.id and\n                  u.login_name(+) = qd.source\n\n        order by  (trunc(sysdate) - trunc(qd.date_created)) desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.queues.RetrievalQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.QG_RETRIEVAL_QUEUES);
   __sJT_st.setLong(2,id);
   __sJT_st.setInt(3,this.type);
   __sJT_st.setInt(4,this.type);
   __sJT_st.setInt(5,MesQueues.Q_RETRIEVALS_ALL);
   __sJT_st.setInt(6,MesQueues.Q_ITEM_TYPE_RETRIEVAL);
   __sJT_st.setLong(7,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.queues.RetrievalQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:766^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueItem(" + this.type + ", " + id + ")", e.toString());
    }
  }

  public void setType( int newType )
  {
    String[][]          buttons     = null;

    super.setType( newType );

    // setup the options based on type
    switch( newType )
    {
      case MesQueues.Q_RETRIEVALS_VISA_INCOMING:
        buttons =
          new String[][]
          {
            { "1st Letter Sent", Integer.toString(MesQueues.Q_RETRIEVALS_VISA_PENDING) },
            { "Cancel Request",  Integer.toString(MesQueues.Q_RETRIEVALS_VISA_CANCELLED) },
          };
        break;

      case MesQueues.Q_RETRIEVALS_MC_INCOMING:
        buttons =
          new String[][]
          {
            { "1st Letter Sent", Integer.toString(MesQueues.Q_RETRIEVALS_MC_PENDING) },
            { "Cancel Request",  Integer.toString(MesQueues.Q_RETRIEVALS_MC_CANCELLED) },
          };
        break;

      case MesQueues.Q_RETRIEVALS_VISA_PENDING:
        buttons =
          new String[][]
          {
            { "Fulfilled",        Integer.toString(MesQueues.Q_RETRIEVALS_VISA_COMPLETED) },
            { "2nd Letter Sent",  Integer.toString(MesQueues.Q_RETRIEVALS_VISA_SECOND_LETTER) },
            { "Non-Fulfilled",    Integer.toString(MesQueues.Q_RETRIEVALS_VISA_NON_FULFILLED) },
          };
        break;

      case MesQueues.Q_RETRIEVALS_MC_PENDING:
        buttons =
          new String[][]
          {
            { "Fulfilled",        Integer.toString(MesQueues.Q_RETRIEVALS_MC_COMPLETED) },
            { "2nd Letter Sent",  Integer.toString(MesQueues.Q_RETRIEVALS_MC_SECOND_LETTER) },
            { "Non-Fulfilled",    Integer.toString(MesQueues.Q_RETRIEVALS_MC_NON_FULFILLED) },
          };
        break;

      case MesQueues.Q_RETRIEVALS_VISA_SECOND_LETTER:
        buttons =
          new String[][]
          {
            { "Fulfilled",      Integer.toString(MesQueues.Q_RETRIEVALS_VISA_COMPLETED) },
            { "Non-Fulfilled",  Integer.toString(MesQueues.Q_RETRIEVALS_VISA_NON_FULFILLED) },
          };
        break;

      case MesQueues.Q_RETRIEVALS_MC_SECOND_LETTER:
        buttons =
          new String[][]
          {
            { "Fulfilled",      Integer.toString(MesQueues.Q_RETRIEVALS_MC_COMPLETED) },
            { "Non-Fulfilled",  Integer.toString(MesQueues.Q_RETRIEVALS_MC_NON_FULFILLED) },
          };
        break;

      case MesQueues.Q_RETRIEVALS_AMEX_INCOMING:
        buttons =
          new String[][]
          {
            { "1st Letter Sent (Pending)", Integer.toString(MesQueues.Q_RETRIEVALS_AMEX_PENDING) },
          };
        break;

      case MesQueues.Q_RETRIEVALS_AMEX_PENDING:
        buttons =
          new String[][]
          {
            { "Responded To",      Integer.toString(MesQueues.Q_RETRIEVALS_AMEX_COMPLETED) },
            { "Non-Fulfilled",  Integer.toString(MesQueues.Q_RETRIEVALS_AMEX_NON_FULFILLED) }
          };
        break;

      case MesQueues.Q_RETRIEVALS_AMEX_COMPLETED:
        buttons =
          new String[][]
          {
            { "Return to Pending", Integer.toString(MesQueues.Q_RETRIEVALS_AMEX_PENDING) },
          };
        break;

      case MesQueues.Q_RETRIEVALS_AMEX_NON_FULFILLED:
        buttons =
          new String[][]
          {
            { "Responded To",      Integer.toString(MesQueues.Q_RETRIEVALS_AMEX_COMPLETED) },
            { "Return to Pending", Integer.toString(MesQueues.Q_RETRIEVALS_AMEX_PENDING) },
          };
        break;
        
      case MesQueues.Q_RETRIEVALS_DS_INCOMING:
        buttons =
          new String[][]
          {
            { "1st Letter Sent (Pending)", Integer.toString(MesQueues.Q_RETRIEVALS_DS_PENDING) },
          };
        break;

      case MesQueues.Q_RETRIEVALS_DS_PENDING:
        buttons =
          new String[][]
          {
            { "Responded To",   Integer.toString(MesQueues.Q_RETRIEVALS_DS_COMPLETED)     },
            { "Non-Fulfilled",  Integer.toString(MesQueues.Q_RETRIEVALS_DS_NON_FULFILLED) }
          };
        break;

      case MesQueues.Q_RETRIEVALS_DS_COMPLETED:
        buttons =
          new String[][]
          {
            { "Return to Pending", Integer.toString(MesQueues.Q_RETRIEVALS_DS_PENDING) },
          };
        break;

      case MesQueues.Q_RETRIEVALS_DS_NON_FULFILLED:
        buttons =
          new String[][]
          {
            { "Responded To",      Integer.toString(MesQueues.Q_RETRIEVALS_DS_COMPLETED) },
            { "Return to Pending", Integer.toString(MesQueues.Q_RETRIEVALS_DS_PENDING) },
          };
        break;

      default:
        break;
    }

    // if there are actions provide a way to select and provide comments
    if ( buttons != null )
    {
      fields.add( new RadioButtonField("action", buttons, -1, false, "Must select an action" ) );
      fields.add(new TextareaField("actionComments",500,8,80,true));
    }
  }
}/*@lineinfo:generated-code*/