/*@lineinfo:filename=ProgrammingQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/ProgrammingQueue.sqlj $

  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 2/05/04 12:13p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.sql.Date;
import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.queues.ExtraColumnBean.LinkData;
import com.mes.user.UserBean;


public class ProgrammingQueue extends QueueBase
{
  public ProgrammingQueue()
  {
    super();
    createSearchFields();
  }
  
  public static boolean isCompletedProgrammingQueue(int q)
  {
    return (q==MesQueues.Q_PROGRAMMING_COMPLETE 
         || q==MesQueues.Q_PROGRAMMING_TM_COMPLETED
         || q==MesQueues.Q_PROGRAMMING_VC_COMPLETED);
  }

  public static int getCompletedQueue(int type)
  {
    int cmpltdQueue = MesQueues.Q_NONE;

    switch(type) {
      case MesQueues.Q_PROGRAMMING_TM_NEW:
      case MesQueues.Q_PROGRAMMING_TM_PENDING:
      case MesQueues.Q_PROGRAMMING_TM_COMPLETED:
        cmpltdQueue = MesQueues.Q_PROGRAMMING_TM_COMPLETED;
        break;
      case MesQueues.Q_PROGRAMMING_VC_NEW:
      case MesQueues.Q_PROGRAMMING_VC_PENDING:
      case MesQueues.Q_PROGRAMMING_VC_COMPLETED:
        cmpltdQueue = MesQueues.Q_PROGRAMMING_VC_COMPLETED;
        break;
      default:
        cmpltdQueue = MesQueues.Q_PROGRAMMING_COMPLETE;
        break;
    }

    return cmpltdQueue;
  }

  public static int getPendingQueue(int type)
  {
    int q = MesQueues.Q_NONE;

    switch(type) {
      case MesQueues.Q_PROGRAMMING_TM_NEW:
      case MesQueues.Q_PROGRAMMING_TM_PENDING:
      case MesQueues.Q_PROGRAMMING_TM_COMPLETED:
        q = MesQueues.Q_PROGRAMMING_TM_PENDING;
        break;
      case MesQueues.Q_PROGRAMMING_VC_NEW:
      case MesQueues.Q_PROGRAMMING_VC_PENDING:
      case MesQueues.Q_PROGRAMMING_VC_COMPLETED:
        q = MesQueues.Q_PROGRAMMING_VC_PENDING;
        break;
      default:
        q = MesQueues.Q_PROGRAMMING_REGULAR;
        break;
    }

    return q;
  }

  public static String getTypeDescriptor(int type)
  {
    switch(type) {
      case MesQueues.Q_PROGRAMMING_REGULAR:
        return "Regular Programming";
      case MesQueues.Q_PROGRAMMING_SPECIAL:
        return "Special Programming";
      case MesQueues.Q_PROGRAMMING_COMPLETE:
        return "Programming - Complete";
      case MesQueues.Q_PROGRAMMING_TM_NEW:
        return "Programming - New TermMaster";
      case MesQueues.Q_PROGRAMMING_TM_PENDING:
        return "Programming - Pending TermMaster";
      case MesQueues.Q_PROGRAMMING_TM_COMPLETED:
        return "Programming - Completed TermMaster";
      case MesQueues.Q_PROGRAMMING_VC_NEW:
        return "Programming - New Vericentre";
      case MesQueues.Q_PROGRAMMING_VC_PENDING:
        return "Programming - Pending Vericentre";
      case MesQueues.Q_PROGRAMMING_VC_COMPLETED:
        return "Programming - Completed Vericentre";
      default:
        return "";
    }
  }

  public boolean isCompleted()
  {
    return (getCompletedQueue(type) == type);
  }

  public void setType(int type)
  {
    super.setType(type);

    if(isCompleted()) {

      FieldGroup      searchFields  = (FieldGroup)fields.getField("searchFields");

      java.util.Date now = new java.util.Date();
      ((DateField)searchFields.getField( "searchBeginDate" )).setUtilDate(now);
      ((DateField)searchFields.getField( "searchEndDate" )).setUtilDate(now);

    }
  }

  private void createSearchFields()
  {
    FieldGroup      searchFields = initSearchFields();

    // fields will appear in this order when displayed
    searchFields.add( new DateField( "searchBeginDate","Submit Date From: ",true ) );
    ((DateField)searchFields.getField("searchBeginDate")).setDateFormat("MM/dd/yyyy");
    searchFields.add( new DateField( "searchEndDate","To:", true ) );
    ((DateField)searchFields.getField("searchEndDate")).setDateFormat("MM/dd/yyyy");
    searchFields.add( new Field    ( "searchValue","Search Criteria:", 50, 50, true ) );
  }

  public String getGenericSearchDesc()
  {
    StringBuffer          desc      = new StringBuffer();
    
    desc.append("<ul>");
    desc.append("  <li>Merchant Control Number</li>");
    desc.append("</ul>");
    
    return( desc.toString() );
  }

  protected void loadQueueData(UserBean user)
  {
    try
    {
      FieldGroup      searchFields  = (FieldGroup)fields.getField("searchFields");

      // Extract params here
      long    longVal           = 0L;
      Date    searchBeginDate   = ((DateField)searchFields.getField( "searchBeginDate" )).getSqlDate();
      Date    searchEndDate     = ((DateField)searchFields.getField( "searchEndDate" )).getSqlDate();
      String  searchValue       = searchFields.getField( "searchValue" ).getData();

      // get the search value as a long if possible
      try{ longVal = Long.parseLong(searchValue); } catch( Exception e ) { longVal = 0L; }
      
      // note that the class member SearchSubmitted can be used
      // if your code needs to know when the user has pressed
      // the search button on the UI.
      if ( SearchSubmitted )
      {
        // do any required handling as a 
        // result of the user pressing the
        // search button.
      }

      // run the query using the search params
      /*@lineinfo:generated-code*//*@lineinfo:188^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   qd.id                   id,
//                   qd.type                 type,
//                   qd.item_type            item_type,
//                   qd.owner                owner,
//                   qd.date_created         date_created,
//                   qd.source               source,
//                   qd.affiliate            affiliate,
//                   qd.last_changed         last_changed,
//                   qd.last_user            last_user,
//                   m.merch_business_name   description,
//                   m.merc_cntrl_number     control,
//                   qd.locked_by            locked_by,
//                   count(qn.id)            note_count,
//                   qt.status               status,
//                   m.merch_number          merchant_number,
//                   m.app_seq_num           primary_key,
//                   msi.request_id          request_id,
//                   msi.vnum                vnum,
//                   mv.tid                  tid,
//                   msi.term_application    term_application,
//                   nvl(msi.term_application_profgen,msi.term_application)    term_application_profgen,
//                   decode(tp.catid,
//                    null, to_char(m.app_seq_num),
//                    'TRDT'||tp.catid)      coded_id
//          from     q_data                  qd,
//                   q_notes                 qn,
//                   q_types                 qt,
//                   merchant                m,
//                   mms_stage_info          msi,
//                   merch_vnumber           mv,
//                   trident_profile         tp
//          where    qd.type          = :this.type    and
//                   qd.id            = msi.request_id  and
//                   msi.app_seq_num  = m.app_seq_num   and
//                   qd.type          = qt.type         and
//                   qd.id            = qn.id(+)        and
//                   msi.vnum is not null               and 
//                   msi.vnum = tp.catid(+)             and
//                   qd.id            = mv.request_id(+) and
//                   decode(qd.type
//                    ,:MesQueues.Q_PROGRAMMING_TM_NEW,'success'
//                    ,:MesQueues.Q_PROGRAMMING_VC_NEW,'success'
//                    ,:MesQueues.Q_PROGRAMMING_REGULAR,'success'
//                    ,msi.process_response) = nvl(msi.process_response,'')
//                  and
//                  (
//                        (( :searchBeginDate is null ) or ( trunc(qd.date_created) between :searchBeginDate and :searchEndDate ))
//                    and (
//                          ( :searchValue is null ) or
//                          ( m.merc_cntrl_number = :longVal ) or
//                          ( qd.id = :longVal )
//                        )
//                  )
//          group by qd.id,
//                   qd.type,
//                   qd.item_type,
//                   qd.owner,
//                   qd.date_created,
//                   qd.source,
//                   qd.affiliate,
//                   qd.last_changed,
//                   qd.last_user,
//                   m.merch_business_name,
//                   m.merc_cntrl_number,
//                   qd.locked_by,
//                   qt.status,
//                   m.merch_number,
//                   m.app_seq_num,
//                   msi.request_id,
//                   msi.vnum,
//                   msi.term_application,
//                   mv.tid,
//                   msi.term_application_profgen,
//                   decode(tp.catid,
//                    null, to_char(m.app_seq_num),
//                    'TRDT'||tp.catid)
//          order by qd.date_created asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   qd.id                   id,\n                 qd.type                 type,\n                 qd.item_type            item_type,\n                 qd.owner                owner,\n                 qd.date_created         date_created,\n                 qd.source               source,\n                 qd.affiliate            affiliate,\n                 qd.last_changed         last_changed,\n                 qd.last_user            last_user,\n                 m.merch_business_name   description,\n                 m.merc_cntrl_number     control,\n                 qd.locked_by            locked_by,\n                 count(qn.id)            note_count,\n                 qt.status               status,\n                 m.merch_number          merchant_number,\n                 m.app_seq_num           primary_key,\n                 msi.request_id          request_id,\n                 msi.vnum                vnum,\n                 mv.tid                  tid,\n                 msi.term_application    term_application,\n                 nvl(msi.term_application_profgen,msi.term_application)    term_application_profgen,\n                 decode(tp.catid,\n                  null, to_char(m.app_seq_num),\n                  'TRDT'||tp.catid)      coded_id\n        from     q_data                  qd,\n                 q_notes                 qn,\n                 q_types                 qt,\n                 merchant                m,\n                 mms_stage_info          msi,\n                 merch_vnumber           mv,\n                 trident_profile         tp\n        where    qd.type          =  :1     and\n                 qd.id            = msi.request_id  and\n                 msi.app_seq_num  = m.app_seq_num   and\n                 qd.type          = qt.type         and\n                 qd.id            = qn.id(+)        and\n                 msi.vnum is not null               and \n                 msi.vnum = tp.catid(+)             and\n                 qd.id            = mv.request_id(+) and\n                 decode(qd.type\n                  , :2 ,'success'\n                  , :3 ,'success'\n                  , :4 ,'success'\n                  ,msi.process_response) = nvl(msi.process_response,'')\n                and\n                (\n                      ((  :5  is null ) or ( trunc(qd.date_created) between  :6  and  :7  ))\n                  and (\n                        (  :8  is null ) or\n                        ( m.merc_cntrl_number =  :9  ) or\n                        ( qd.id =  :10  )\n                      )\n                )\n        group by qd.id,\n                 qd.type,\n                 qd.item_type,\n                 qd.owner,\n                 qd.date_created,\n                 qd.source,\n                 qd.affiliate,\n                 qd.last_changed,\n                 qd.last_user,\n                 m.merch_business_name,\n                 m.merc_cntrl_number,\n                 qd.locked_by,\n                 qt.status,\n                 m.merch_number,\n                 m.app_seq_num,\n                 msi.request_id,\n                 msi.vnum,\n                 msi.term_application,\n                 mv.tid,\n                 msi.term_application_profgen,\n                 decode(tp.catid,\n                  null, to_char(m.app_seq_num),\n                  'TRDT'||tp.catid)\n        order by qd.date_created asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.ProgrammingQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setInt(2,MesQueues.Q_PROGRAMMING_TM_NEW);
   __sJT_st.setInt(3,MesQueues.Q_PROGRAMMING_VC_NEW);
   __sJT_st.setInt(4,MesQueues.Q_PROGRAMMING_REGULAR);
   __sJT_st.setDate(5,searchBeginDate);
   __sJT_st.setDate(6,searchBeginDate);
   __sJT_st.setDate(7,searchEndDate);
   __sJT_st.setString(8,searchValue);
   __sJT_st.setLong(9,longVal);
   __sJT_st.setLong(10,longVal);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.queues.ProgrammingQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:267^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueData(" + user.getLoginName() + ")", e.toString());
    }
  }
  
  protected void loadQueueItem(long id)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:279^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   qd.id                   id,
//                   qd.type                 type,
//                   qd.item_type            item_type,
//                   qd.owner                owner,
//                   qd.date_created         date_created,
//                   qd.source               source,
//                   qd.affiliate            affiliate,
//                   qd.last_changed         last_changed,
//                   qd.last_user            last_user,
//                   m.merch_business_name   description,
//                   m.merc_cntrl_number     control,
//                   qd.locked_by            locked_by,
//                   count(qn.id)            note_count,
//                   qt.status               status,
//                   m.merch_number          merchant_number,
//                   m.app_seq_num           primary_key,
//                   msi.request_id          request_id,
//                   msi.vnum                vnum,
//                   mv.tid                  tid,
//                   msi.term_application    term_application,
//                   nvl(msi.term_application_profgen,msi.term_application)    term_application_profgen
//  
//          from     q_data                  qd,
//                   q_notes                 qn,
//                   merchant                m,
//                   q_types                 qt,
//                   mms_stage_info          msi,
//                   merch_vnumber           mv
//  
//          where    qd.type          = :this.type      and
//                   qd.id            = :id               and
//                   qd.id            = msi.request_id    and
//                   msi.app_seq_num  = m.app_seq_num     and
//                   qd.type          = qt.type           and
//                   qd.id            = qn.id(+)          and
//                   qd.id            = mv.request_id(+)
//  
//          group by qd.id,
//                   qd.type,
//                   qd.item_type,
//                   qd.owner,
//                   qd.date_created,
//                   qd.source,
//                   qd.affiliate,
//                   qd.last_changed,
//                   qd.last_user,
//                   m.merch_business_name,
//                   m.merc_cntrl_number,
//                   qd.locked_by,
//                   qt.status,
//                   m.merch_number,
//                   m.app_seq_num,
//                   msi.request_id,
//                   msi.vnum,
//                   msi.term_application,
//                   mv.tid,
//                   msi.term_application_profgen
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   qd.id                   id,\n                 qd.type                 type,\n                 qd.item_type            item_type,\n                 qd.owner                owner,\n                 qd.date_created         date_created,\n                 qd.source               source,\n                 qd.affiliate            affiliate,\n                 qd.last_changed         last_changed,\n                 qd.last_user            last_user,\n                 m.merch_business_name   description,\n                 m.merc_cntrl_number     control,\n                 qd.locked_by            locked_by,\n                 count(qn.id)            note_count,\n                 qt.status               status,\n                 m.merch_number          merchant_number,\n                 m.app_seq_num           primary_key,\n                 msi.request_id          request_id,\n                 msi.vnum                vnum,\n                 mv.tid                  tid,\n                 msi.term_application    term_application,\n                 nvl(msi.term_application_profgen,msi.term_application)    term_application_profgen\n\n        from     q_data                  qd,\n                 q_notes                 qn,\n                 merchant                m,\n                 q_types                 qt,\n                 mms_stage_info          msi,\n                 merch_vnumber           mv\n\n        where    qd.type          =  :1       and\n                 qd.id            =  :2                and\n                 qd.id            = msi.request_id    and\n                 msi.app_seq_num  = m.app_seq_num     and\n                 qd.type          = qt.type           and\n                 qd.id            = qn.id(+)          and\n                 qd.id            = mv.request_id(+)\n\n        group by qd.id,\n                 qd.type,\n                 qd.item_type,\n                 qd.owner,\n                 qd.date_created,\n                 qd.source,\n                 qd.affiliate,\n                 qd.last_changed,\n                 qd.last_user,\n                 m.merch_business_name,\n                 m.merc_cntrl_number,\n                 qd.locked_by,\n                 qt.status,\n                 m.merch_number,\n                 m.app_seq_num,\n                 msi.request_id,\n                 msi.vnum,\n                 msi.term_application,\n                 mv.tid,\n                 msi.term_application_profgen";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.ProgrammingQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setLong(2,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.queues.ProgrammingQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:338^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueItem(" + this.type + ", " + id + ")", e.toString());
    }
  }
  
  protected void loadExtraColumns(ExtraColumnBean extraColumns, UserBean user)
  {
    try
    {
      LinkData linkData = null;
      
      extraColumns.addColumn("Merch #", "merchant_number");


      linkData = extraColumns.getNewLinkData();
      linkData.addLink("primaryKey","primary_key");
      extraColumns.addColumn("V#", "vnum", "/jsp/credit/mms_management_screen.jsp", "blank", "", linkData);

      extraColumns.addColumn("TID", "tid");

      extraColumns.addColumn("Term App", "term_application");

      if(getType() != MesQueues.Q_PROGRAMMING_SPECIAL)
      {
        linkData = extraColumns.getNewLinkData();
        linkData.addLink("primaryKey","primary_key");
        extraColumns.addColumn("Disc Cover Sheet", "View", "/jsp/credit/discovercoversheet.jsp", "blank", "", linkData);
        linkData = extraColumns.getNewLinkData();
        linkData.addLink("primaryKey","primary_key");
        extraColumns.addColumn("Disc Pricing Schedule", "View", "/jsp/credit/discoverpricingschedule.jsp", "blank", "", linkData);
      }

      linkData = extraColumns.getNewLinkData();
      
      linkData.addLink("primaryKey","coded_id");
      extraColumns.addColumn("Var Form", "View", "/jsp/credit/varform.jsp", "blank", "", linkData);

      String lnk = "/srv/QueueTool?type=" + getType() + "&action=" + MesQueues.Q_ACTION_MOVE + "&dest=" + getCompletedQueue(this.type) + "&user=" + user.getLoginName();
      linkData = extraColumns.getNewLinkData();
      linkData.addLink("id","request_id");
      linkData.addLink("primaryKey","primary_key");
      extraColumns.addColumn("Move To", "Completed", lnk, "","", linkData);

    }
    catch(Exception e)
    {
      logEntry("loadExtraColumns()", e.toString());
    }
    
  }
  
  public String getDescriptionURL()
  {
    String result = "";
    
    return result;
  }
  
  public int getMenuId()
  {
    switch(type) {
      default:
      case MesQueues.Q_PROGRAMMING_REGULAR:
      case MesQueues.Q_PROGRAMMING_SPECIAL:
      case MesQueues.Q_PROGRAMMING_COMPLETE:
        //return MesMenus.MENU_ID_PROGRAMMING_QUEUES;
        return MesMenus.MENU_ID_MMS_SETUP;
      case MesQueues.Q_PROGRAMMING_TM_NEW:
      case MesQueues.Q_PROGRAMMING_TM_PENDING:
      case MesQueues.Q_PROGRAMMING_TM_COMPLETED:
        return MesMenus.MENU_ID_TM_PROGRAMMING_QUEUES;
      case MesQueues.Q_PROGRAMMING_VC_NEW:
      case MesQueues.Q_PROGRAMMING_VC_PENDING:
      case MesQueues.Q_PROGRAMMING_VC_COMPLETED:
        return MesMenus.MENU_ID_VC_PROGRAMMING_QUEUES;
    }
  }
  
}/*@lineinfo:generated-code*/