/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/QueuePageData.java $

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/28/01 4:08p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

public class QueuePageData
{
  public int      beginVal      = 0;
  public int      pageNumber    = 0;
  public int      rowStart      = 0;
  public int      rowEnd        = 0;
  public int      prevBegin     = 0;
  
  public QueuePageData( int beginVal, 
                        int pageNumber, 
                        int rowStart, 
                        int rowEnd,
                        int span)
  {
    this.beginVal = beginVal;
    this.pageNumber = pageNumber;
    this.rowStart = rowStart;
    this.rowEnd = rowEnd;
    
    if(this.beginVal > 0)
    {
      this.prevBegin = (beginVal - span) < 0 ? 0 : beginVal - span;
    }
  }
  
  public boolean isCurrentPage(int beginVal)
  {
    boolean result = false;
    
    if(beginVal == this.beginVal)
    {
      result = true;
    }
    else
    {
      result = false;
    }
    
    return result;
  }
}
