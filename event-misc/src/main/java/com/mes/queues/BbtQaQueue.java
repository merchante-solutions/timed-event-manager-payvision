/*@lineinfo:filename=BbtQaQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.queues;

import java.sql.Date;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.RadioButtonField;
import com.mes.forms.TextareaField;
import com.mes.queues.ExtraColumnBean.LinkData;
import com.mes.support.HttpHelper;
import com.mes.support.SyncLog;
import com.mes.user.UserBean;


public final class BbtQaQueue extends QueueBase
{
  public BbtQaQueue()
  {
    super();
    
    createSearchFields();
  }
  
  private void createSearchFields()
  {
    FieldGroup      searchFields = initSearchFields();

    // fields will appear in this order when displayed
    searchFields.add( new DateField( "searchBeginDate","Submit Date From: ",true ) );
    ((DateField)searchFields.getField("searchBeginDate")).setDateFormat("MM/dd/yyyy");
    searchFields.add( new DateField( "searchEndDate","To:", true ) );
    ((DateField)searchFields.getField("searchEndDate")).setDateFormat("MM/dd/yyyy");
    searchFields.add( new Field    ( "searchValue","Search Criteria:", 50, 50, true ) );
  }

  protected void loadQueueData(UserBean user)
  {
    try
    {
      FieldGroup      searchFields  = (FieldGroup)fields.getField("searchFields");
      
      // Extract params here
      long    longVal           = 0L;
      Date    searchBeginDate   = ((DateField)searchFields.getField( "searchBeginDate" )).getSqlDate();
      Date    searchEndDate     = ((DateField)searchFields.getField( "searchEndDate" )).getSqlDate();
      String  searchValue       = searchFields.getField( "searchValue" ).getData();
      
      // get the search value as a long if possible
      try{ longVal = Long.parseLong(searchValue); } catch( Exception e ) { longVal = 0L; }
      
      // note that the class member SearchSubmitted can be used
      // if your code needs to know when the user has pressed
      // the search button on the UI.
      if ( SearchSubmitted )
      {
        // do any required handling as a 
        // result of the user pressing the
        // search button.
      }

      /*@lineinfo:generated-code*//*@lineinfo:63^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   id,
//                  m.merc_cntrl_number     control,
//                  qd.is_rush              is_rush,
//                  qd.type                 type,
//                  qd.item_type            item_type,
//                  qd.owner                owner,
//                  m.merch_business_name   description,
//                  qd.date_created         date_created,
//                  qd.source               source,
//                  qd.affiliate            affiliate,
//                  qd.last_changed         last_changed,
//                  qd.last_user            last_user,
//                  qd.locked_by            locked_by,
//                  nvl(qn.note_count,0)    note_count,
//                  qt.status               status,
//                  m.merch_number          merchant_number,
//                  m.business_nature       business_nature,
//                  u.name                  rep_name,
//                  u.email                 rep_email
//  
//  		    from      q_data                  qd,
//                    q_types                 qt,
//                    merchant                m,
//                    users                   u,
//                    ( select    id,
//                                count(*)  note_count
//                      from      q_notes
//                      group by id 
//                    )                       qn
//  
//          where   qd.type         = :this.type and
//                  qd.id           = m.app_seq_num and
//                  qd.type         = qt.type and
//                  qd.id           = qn.id(+) and 
//                  qd.source       = u.login_name(+)
//  
//                  and
//                  (
//                        (( :searchBeginDate is null ) or ( trunc(qd.date_created) between :searchBeginDate and :searchEndDate ))
//                    and (
//                          ( :searchValue is null ) or
//                          ( m.merch_number = :longVal ) or
//                          ( m.merc_cntrl_number = :longVal ) or
//                          ( qd.id = :longVal ) or
//                          ( lower(m.merch_business_name) like lower('%' || :searchValue || '%') )
//                        )
//                  )
//         
//          order by qd.is_rush desc
//                  ,qd.date_created asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                   id,\n                m.merc_cntrl_number     control,\n                qd.is_rush              is_rush,\n                qd.type                 type,\n                qd.item_type            item_type,\n                qd.owner                owner,\n                m.merch_business_name   description,\n                qd.date_created         date_created,\n                qd.source               source,\n                qd.affiliate            affiliate,\n                qd.last_changed         last_changed,\n                qd.last_user            last_user,\n                qd.locked_by            locked_by,\n                nvl(qn.note_count,0)    note_count,\n                qt.status               status,\n                m.merch_number          merchant_number,\n                m.business_nature       business_nature,\n                u.name                  rep_name,\n                u.email                 rep_email\n\n\t\t    from      q_data                  qd,\n                  q_types                 qt,\n                  merchant                m,\n                  users                   u,\n                  ( select    id,\n                              count(*)  note_count\n                    from      q_notes\n                    group by id \n                  )                       qn\n\n        where   qd.type         =  :1  and\n                qd.id           = m.app_seq_num and\n                qd.type         = qt.type and\n                qd.id           = qn.id(+) and \n                qd.source       = u.login_name(+)\n\n                and\n                (\n                      ((  :2  is null ) or ( trunc(qd.date_created) between  :3  and  :4  ))\n                  and (\n                        (  :5  is null ) or\n                        ( m.merch_number =  :6  ) or\n                        ( m.merc_cntrl_number =  :7  ) or\n                        ( qd.id =  :8  ) or\n                        ( lower(m.merch_business_name) like lower('%' ||  :9  || '%') )\n                      )\n                )\n       \n        order by qd.is_rush desc\n                ,qd.date_created asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.BbtQaQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setDate(2,searchBeginDate);
   __sJT_st.setDate(3,searchBeginDate);
   __sJT_st.setDate(4,searchEndDate);
   __sJT_st.setString(5,searchValue);
   __sJT_st.setLong(6,longVal);
   __sJT_st.setLong(7,longVal);
   __sJT_st.setLong(8,longVal);
   __sJT_st.setString(9,searchValue);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.queues.BbtQaQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:115^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueData(" + user.getLoginName() + ")", e.toString());
    }
  }
  
  protected void loadQueueItem(long id)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:127^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   id,
//                  m.merc_cntrl_number     control,
//                  qd.is_rush              is_rush,
//                  qd.type                 type,
//                  qd.item_type            item_type,
//                  qd.owner                owner,
//                  m.merch_business_name   description,
//                  qd.date_created         date_created,
//                  qd.source               source,
//                  qd.affiliate            affiliate,
//                  qd.last_changed         last_changed,
//                  qd.last_user            last_user,
//                  qd.locked_by            locked_by,
//                  nvl(qn.note_count,0)    note_count,
//                  qt.status               status,
//                  m.merch_number          merchant_number,
//                  m.business_nature       business_nature,
//                  u.name                  rep_name,
//                  u.email                 rep_email
//  
//          from    q_data                  qd,
//                  q_types                 qt,
//                  merchant                m,
//                  users                   u,
//                  ( select    id,
//                              count(*)  note_count
//                    from      q_notes
//                    group by id 
//                  )                       qn
//  
//          where   qd.type         = :this.type and
//                  qd.id           = m.app_seq_num and
//                  qd.type         = qt.type and
//                  qd.id           = qn.id(+) and 
//                  qd.source       = u.login_name(+) and
//                  qd.id           = :id
//          
//          order by qd.is_rush desc
//                  ,qd.date_created asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                   id,\n                m.merc_cntrl_number     control,\n                qd.is_rush              is_rush,\n                qd.type                 type,\n                qd.item_type            item_type,\n                qd.owner                owner,\n                m.merch_business_name   description,\n                qd.date_created         date_created,\n                qd.source               source,\n                qd.affiliate            affiliate,\n                qd.last_changed         last_changed,\n                qd.last_user            last_user,\n                qd.locked_by            locked_by,\n                nvl(qn.note_count,0)    note_count,\n                qt.status               status,\n                m.merch_number          merchant_number,\n                m.business_nature       business_nature,\n                u.name                  rep_name,\n                u.email                 rep_email\n\n        from    q_data                  qd,\n                q_types                 qt,\n                merchant                m,\n                users                   u,\n                ( select    id,\n                            count(*)  note_count\n                  from      q_notes\n                  group by id \n                )                       qn\n\n        where   qd.type         =  :1  and\n                qd.id           = m.app_seq_num and\n                qd.type         = qt.type and\n                qd.id           = qn.id(+) and \n                qd.source       = u.login_name(+) and\n                qd.id           =  :2 \n        \n        order by qd.is_rush desc\n                ,qd.date_created asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.BbtQaQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setLong(2,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.queues.BbtQaQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:168^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueItem(" + this.type + ", " + id + ")", e.toString());
    }
  }

  public void setType( int newType )
  {
    super.setType( newType );
    
    String[][]          buttons     = null;
    
    // setup the options based on type
    switch( newType )
    {
      case MesQueues.Q_BBT_QA_PREP:
        buttons = 
          new String[][]
          {
             { "Relinquish, send to Data Entry", Integer.toString(MesQueues.Q_BBT_QA_DATAENTRY) }
            ,{ "Cancel Application",  Integer.toString(MesQueues.Q_BBT_QA_CANCELLED) }
          };
        break;
        
      case MesQueues.Q_BBT_QA_DATAENTRY:
        buttons = 
          new String[][]
          {
             { "Forward to QA", Integer.toString(MesQueues.Q_BBT_QA_SETUP) }
            ,{ "Cancel Application",  Integer.toString(MesQueues.Q_BBT_QA_CANCELLED) }
          };
        break;

      default:
        break;
    }

    // if there are actions provide a way to select and provide comments
    if ( buttons != null )
    {
      fields.add( new RadioButtonField( "action", buttons, -1, false, "Must select an action" ) );
      fields.add( new TextareaField(    "actionComments",500,8,80,true));
    }
  }
  
  public void doAction( HttpServletRequest request )
  {
    // do queue move op
    Field       comments      = getField("actionComments");
    //Field       action        = getField("action");
    int         destQueueId   = HttpHelper.getInt(request,  "action", -1);
    long        itemId        = HttpHelper.getLong(request, "id", -1);

System.out.println("doAction(bbt) - destQueueId:"+destQueueId);
System.out.println("doAction(bbt) - itemId:"+itemId);

    switch(destQueueId) {
      
      case MesQueues.Q_BBT_QA_DATAENTRY:
        // fall through
      case MesQueues.Q_BBT_QA_SETUP:
        QueueTools.moveQueueItem(itemId, this.type, destQueueId, this.getUser(), comments.getData());
        break;
      
      default:
        break;
    
    }
    
  }
  
  protected QueueData getEmptyQueueData()
  {
    return new BbtQueueData();
  }
  
  public static class BbtQueueData extends QueueData
  {
    //private int     score;
    //private String  volume;
    //private String  merchEmail;
    private String  repName;
    private String  repEmail;
    
    public void setExtendedData(ResultSet rs)
    {
      try
      {
        //setScore      (rs.getInt("score"));
        //setVolume     (rs.getString("volume"));
        //setMerchEmail (rs.getString("merch_email"));
        setRepName    (rs.getString("rep_name"));
        setRepEmail   (rs.getString("rep_email"));
      }
      catch(Exception e)
      {
        SyncLog.LogEntry(this.getClass().getName() + "::setExtendedData()", e.toString());
      }
    }

    public void setStatus(String status)
    {
      try {
        String s = "";
        switch(status.charAt(0)) {
          case 'E':
            s = "Data Entry";
            break;
          case 'H':
            s = "Preparation";
            break;
          case 'D':
            s = "Declined";
            break;
          case 'A':
            s = "Approved";
            break;
          case 'C':
            s = "Cancelled";
            break;
          case 'P':
            s = "Pended";
            break;
          case 'Q':
            s = "New";
            break;
          default:
            s = "Unknown";
            break;
        }
        super.setStatus(s);
      }
      catch(Exception e) {
      }
    }
    
    /*
    public void setScore(int score)
    {
      this.score = score;
    }
    public int getScore()
    {
      return score;
    }
    
    public void setVolume(String volume)
    {
      this.volume = volume;
    }
    public String getVolume()
    {
      return volume;
    }

    public void setMerchEmail(String merchEmail)
    {
      this.merchEmail = merchEmail;
    }
    public String getMerchEmail()
    {
      return merchEmail;
    }
    */
    
    public void setRepName(String repName)
    {
      this.repName = repName;
    }
    public String getRepName()
    {
      return repName;
    }
    
    public void setRepEmail(String repEmail)
    {
      this.repEmail = repEmail;
    }
    public String getRepEmail()
    {
      return repEmail;
    }
  }
  
  protected void loadExtraColumns(ExtraColumnBean extraColumns, UserBean user)
  {
    try
    {
      LinkData linkData = null;

      // move to data entry link
      if(getType() == MesQueues.Q_BBT_QA_PREP) {
        String lnk = "/srv/QueueTool?type=" + getType() + "&action=" + MesQueues.Q_ACTION_MOVE + "&dest=" + MesQueues.Q_BBT_QA_DATAENTRY + "&user=" + user.getLoginName();
        linkData = extraColumns.getNewLinkData();
        linkData.addLink("id","id");
        linkData.addLink("primaryKey","app_seq_num");
        extraColumns.addColumn("Move To", "Data Entry", lnk, "","", linkData);
      }
    }
    catch(Exception e)
    {
      logEntry("loadExtraColumns()", e.toString());
    }
  }
  
  public String getDescriptionURL()
  {
    String result = "";
    
    result = "/jsp/setup/merchinfo4.jsp?primaryKey=";
    
    return result;
  }
  
  public int getMenuId()
  {
    return MesMenus.MENU_ID_BBT_QA_QUEUES;
  }
  
  public String getGenericSearchDesc()
  {
    StringBuffer          desc      = new StringBuffer();
    
    desc.append("<ul>");
    desc.append("  <li>Merchant Number</li>");
    desc.append("  <li>Merchant Control Number</li>");
    desc.append("  <li>App Seq Num</li>");
    desc.append("  <li>Merchant DBA Name (partial is OK)</li>");
    desc.append("</ul>");
    
    return( desc.toString() );
  }
}/*@lineinfo:generated-code*/