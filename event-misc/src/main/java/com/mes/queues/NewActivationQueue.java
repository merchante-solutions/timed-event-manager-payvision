/*@lineinfo:filename=NewActivationQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/NewActivationQueue.sqlj $

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 4/26/04 8:49a $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.queues.ExtraColumnBean.LinkData;
import com.mes.user.UserBean;

public class NewActivationQueue extends QueueBase
{
  public NewActivationQueue()
  {
  }
  
  protected void loadQueueData(UserBean user)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:38^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   qd.id                      id,
//                   qd.type                    type,
//                   qd.item_type               item_type,
//                   qd.owner                   owner,
//                   qd.date_created            date_created,
//                   qd.source                  source,
//                   qd.affiliate               affiliate,
//                   qd.last_changed            last_changed,
//                   qd.last_user               last_user,
//                   m.merch_business_name      description,
//                   m.merc_cntrl_number        control,
//                   qd.locked_by               locked_by,
//                   count(qn.id)               note_count,
//                   qt.status                  status,
//                   m.merch_number             merchant_number,
//                   m.app_seq_num              primary_key,
//                   app.app_user_login         sales_rep,
//                   count(sc.merchant_number)  num_of_assignments,
//                   max(sdat.id)               last_id
//          from     q_data                   qd,
//                   q_notes                  qn,
//                   q_types                  qt,
//                   merchant                 m,
//                   application              app,
//                   schedule_data            sdat,
//                   service_calls            sc,
//                   t_hierarchy              th,
//                   users                    u
//          where    qd.type                  = :this.type            and
//                   qd.id                    = m.app_seq_num           and
//                   to_char(m.merch_number)  = sdat.function_id(+)     and
//                   qd.id                    = app.app_seq_num(+)      and
//                   qd.type                  = qt.type                 and
//                   qd.id                    = qn.id(+)                and
//                   m.merch_number           = sc.merchant_number(+)   and
//                   sc.type(+)               = 15                      and
//                   app.app_user_login       = u.login_name            and
//                   u.hierarchy_node         = th.descendent           and
//                   th.ancestor              = :user.getHierarchyNode()
//          group by qd.id,
//                   qd.type,
//                   qd.item_type,
//                   qd.owner,
//                   qd.date_created,
//                   qd.source,
//                   qd.affiliate,
//                   qd.last_changed,
//                   qd.last_user,
//                   m.merch_business_name,
//                   m.merc_cntrl_number,
//                   qd.locked_by,
//                   qt.status,
//                   m.merch_number,
//                   m.app_seq_num,
//                   app.app_user_login
//  
//          order by qd.date_created asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_511 = user.getHierarchyNode();
  try {
   String theSqlTS = "select   qd.id                      id,\n                 qd.type                    type,\n                 qd.item_type               item_type,\n                 qd.owner                   owner,\n                 qd.date_created            date_created,\n                 qd.source                  source,\n                 qd.affiliate               affiliate,\n                 qd.last_changed            last_changed,\n                 qd.last_user               last_user,\n                 m.merch_business_name      description,\n                 m.merc_cntrl_number        control,\n                 qd.locked_by               locked_by,\n                 count(qn.id)               note_count,\n                 qt.status                  status,\n                 m.merch_number             merchant_number,\n                 m.app_seq_num              primary_key,\n                 app.app_user_login         sales_rep,\n                 count(sc.merchant_number)  num_of_assignments,\n                 max(sdat.id)               last_id\n        from     q_data                   qd,\n                 q_notes                  qn,\n                 q_types                  qt,\n                 merchant                 m,\n                 application              app,\n                 schedule_data            sdat,\n                 service_calls            sc,\n                 t_hierarchy              th,\n                 users                    u\n        where    qd.type                  =  :1             and\n                 qd.id                    = m.app_seq_num           and\n                 to_char(m.merch_number)  = sdat.function_id(+)     and\n                 qd.id                    = app.app_seq_num(+)      and\n                 qd.type                  = qt.type                 and\n                 qd.id                    = qn.id(+)                and\n                 m.merch_number           = sc.merchant_number(+)   and\n                 sc.type(+)               = 15                      and\n                 app.app_user_login       = u.login_name            and\n                 u.hierarchy_node         = th.descendent           and\n                 th.ancestor              =  :2 \n        group by qd.id,\n                 qd.type,\n                 qd.item_type,\n                 qd.owner,\n                 qd.date_created,\n                 qd.source,\n                 qd.affiliate,\n                 qd.last_changed,\n                 qd.last_user,\n                 m.merch_business_name,\n                 m.merc_cntrl_number,\n                 qd.locked_by,\n                 qt.status,\n                 m.merch_number,\n                 m.app_seq_num,\n                 app.app_user_login\n\n        order by qd.date_created asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.NewActivationQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setLong(2,__sJT_511);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.queues.NewActivationQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:97^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueData(" + user.getLoginName() + ")", e.toString());
    }
  }
  
  protected void loadQueueItem(long id)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:109^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   qd.id                      id,
//                   qd.type                    type,
//                   qd.item_type               item_type,
//                   qd.owner                   owner,
//                   qd.date_created            date_created,
//                   qd.source                  source,
//                   qd.affiliate               affiliate,
//                   qd.last_changed            last_changed,
//                   qd.last_user               last_user,
//                   m.merch_business_name      description,
//                   m.merc_cntrl_number        control,
//                   qd.locked_by               locked_by,
//                   count(qn.id)               note_count,
//                   qt.status                  status,
//                   m.merch_number             merchant_number,
//                   m.app_seq_num              primary_key,
//                   app.app_user_login         sales_rep,
//                   count(sc.merchant_number)  num_of_assignments,
//                   max(sdat.id)               last_id
//  
//          from     q_data                   qd,
//                   q_notes                  qn,
//                   merchant                 m,
//                   application              app,
//                   q_types                  qt,
//                   schedule_data            sdat,
//                   service_calls            sc
//  
//          where    qd.type                  = :this.type            and
//                   qd.id                    = :id                     and
//                   qd.id                    = m.app_seq_num           and
//                   to_char(m.merch_number)  = sdat.function_id(+)     and
//                   qd.id                    = app.app_seq_num(+)      and
//                   qd.type                  = qt.type                 and
//                   qd.id                    = qn.id(+)                and
//                   m.merch_number           = sc.merchant_number(+)   and
//                   sc.type                  = 15
//  
//          group by qd.id,
//                   qd.type,
//                   qd.item_type,
//                   qd.owner,
//                   qd.date_created,
//                   qd.source,
//                   qd.affiliate,
//                   qd.last_changed,
//                   qd.last_user,
//                   m.merch_business_name,
//                   m.merc_cntrl_number,
//                   qd.locked_by,
//                   qt.status,
//                   m.merch_number,
//                   m.app_seq_num,
//                   app.app_user_login
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   qd.id                      id,\n                 qd.type                    type,\n                 qd.item_type               item_type,\n                 qd.owner                   owner,\n                 qd.date_created            date_created,\n                 qd.source                  source,\n                 qd.affiliate               affiliate,\n                 qd.last_changed            last_changed,\n                 qd.last_user               last_user,\n                 m.merch_business_name      description,\n                 m.merc_cntrl_number        control,\n                 qd.locked_by               locked_by,\n                 count(qn.id)               note_count,\n                 qt.status                  status,\n                 m.merch_number             merchant_number,\n                 m.app_seq_num              primary_key,\n                 app.app_user_login         sales_rep,\n                 count(sc.merchant_number)  num_of_assignments,\n                 max(sdat.id)               last_id\n\n        from     q_data                   qd,\n                 q_notes                  qn,\n                 merchant                 m,\n                 application              app,\n                 q_types                  qt,\n                 schedule_data            sdat,\n                 service_calls            sc\n\n        where    qd.type                  =  :1             and\n                 qd.id                    =  :2                      and\n                 qd.id                    = m.app_seq_num           and\n                 to_char(m.merch_number)  = sdat.function_id(+)     and\n                 qd.id                    = app.app_seq_num(+)      and\n                 qd.type                  = qt.type                 and\n                 qd.id                    = qn.id(+)                and\n                 m.merch_number           = sc.merchant_number(+)   and\n                 sc.type                  = 15\n\n        group by qd.id,\n                 qd.type,\n                 qd.item_type,\n                 qd.owner,\n                 qd.date_created,\n                 qd.source,\n                 qd.affiliate,\n                 qd.last_changed,\n                 qd.last_user,\n                 m.merch_business_name,\n                 m.merc_cntrl_number,\n                 qd.locked_by,\n                 qt.status,\n                 m.merch_number,\n                 m.app_seq_num,\n                 app.app_user_login";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.NewActivationQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setLong(2,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.queues.NewActivationQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:165^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueItem(" + this.type + ", " + id + ")", e.toString());
    }
  }
  
  protected void loadExtraColumns(ExtraColumnBean extraColumns, UserBean user)
  {
    try
    {
      LinkData linkData = null;
      
      linkData = extraColumns.getNewLinkData();
      linkData.addLink("merchant","merchant_number");
      extraColumns.addColumn("Merch #", "merchant_number", "/jsp/maintenance/view_account.jsp?action=1", "blank", "", linkData);

      if(getType() == MesQueues.Q_ACTIVATION_SCHEDULED)
      {
        
        linkData = extraColumns.getNewLinkData();
        linkData.addLink("primaryKey","primary_key");
        extraColumns.addColumn("MMS Screen", "View", "/jsp/credit/mms_management_screen.jsp", "blank", "", linkData);

        linkData = extraColumns.getNewLinkData();
        linkData.addLink("funcid","merchant_number");
        linkData.addLink("funcdesc","description");
        linkData.addLink("id","last_id");
        extraColumns.addColumn("Schedule Activation", "Scheduled", "/jsp/credit/schedule.jsp?type=1", "blank", "", linkData);

        linkData = extraColumns.getNewLinkData();
        linkData.addLink("merchant","merchant_number");
        extraColumns.addColumn("# Calls", "num_of_assignments", "/jsp/maintenance/viewcalls.jsp?", "blank", "", linkData);

        String lnk = "/srv/QueueTool?type=" + getType() + "&action=" + MesQueues.Q_ACTION_MOVE + "&dest=" + MesQueues.Q_ACTIVATION_PENDING + "&user=" + user.getLoginName();
        linkData = extraColumns.getNewLinkData();
        linkData.addLink("id","primary_key");
        extraColumns.addColumn("Move To", "Pending", lnk, "","", linkData);
      }
      else
      {
        if(getType() == MesQueues.Q_ACTIVATION_REFERRED)
        {
          extraColumns.addColumn("Sales Rep", "sales_rep");
        }
        else
        {
          linkData = extraColumns.getNewLinkData();
          linkData.addLink("primaryKey","primary_key");
          extraColumns.addColumn("Packing Slip", "View", "/jsp/credit/packingslip.jsp", "blank", "", linkData);

          linkData = extraColumns.getNewLinkData();
          linkData.addLink("funcid","merchant_number");
          linkData.addLink("funcdesc","description");
          extraColumns.addColumn("Schedule Activation", (getType() == MesQueues.Q_ACTIVATION_NEW ? "Assign" : "Re-Assign"), "/jsp/credit/schedule.jsp?type=1", "blank", "", linkData);
        }

        linkData = extraColumns.getNewLinkData();
        linkData.addLink("merchant","merchant_number");
        extraColumns.addColumn("# Calls", "num_of_assignments", "/jsp/maintenance/viewcalls.jsp", "blank", "", linkData);

        String lnk = "/srv/QueueTool?type=" + getType() + "&action=" + MesQueues.Q_ACTION_MOVE + "&dest=" + MesQueues.Q_ACTIVATION_COMPLETED + "&user=" + user.getLoginName();
        linkData = extraColumns.getNewLinkData();
        linkData.addLink("id","primary_key");
        extraColumns.addColumn("Move To", "Completed", lnk, "","", linkData);
      }



    }
    catch(Exception e)
    {
      logEntry("loadExtraColumns()", e.toString());
    }
    
  }
  
  public String getDescriptionURL()
  {
    String result = "/jsp/setup/merchinfo4.jsp?primaryKey=";
    
    return result;
  }
  
  public int getMenuId()
  {
    return MesMenus.MENU_ID_ACCOUNT_SERVICING;
  }
  
  public String getBackLink()
  {
    StringBuffer result = new StringBuffer();
    
    result.append("/jsp/reports/index.jsp?com.mes.ReportMenuId=");
    result.append(MesMenus.MENU_ID_ACCOUNT_SERVICING);
    
    return result.toString();
  }
}/*@lineinfo:generated-code*/