/*@lineinfo:filename=ChargebackQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/ChargebackQueue.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-08-31 17:05:36 -0700 (Wed, 31 Aug 2011) $
  Version            : $Revision: 19222 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesChargebacks;
import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.constants.MesUsers;
import com.mes.forms.CheckboxField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HierarchyNodeField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.queues.ExtraColumnBean.LinkData;
import com.mes.support.HttpHelper;
import com.mes.support.MesCalendar;
import com.mes.support.SyncLog;
import com.mes.support.TridentTools;
import com.mes.tools.ChargebackTools;
import com.mes.tools.DBGrunt;
import com.mes.tools.DocumentTool;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class ChargebackQueue extends QueueBase
{

  // create class log category
  static Logger log = Logger.getLogger(ChargebackQueue.class);

  /**
   * for the Completed Queue override concept
   *
   * need to keep track of two types, in case this
   * is a completed item, which means that it gets reconfigured
   * below, and any action on that reconfigured item needs to be
   * able to recognize it's shell.
   */
  private boolean useOverrideType = false;

  private int overrideType = 0;

  public static final int _DEFAULT = 0;

  public int getType()
  {
    //log.debug("calling getType... ");
    if(useOverrideType)
    {
      return overrideType;
    }
    else
    {
      return super.getType();
    }
  }

  public ChargebackQueue( )
  {
    FieldGroup      searchFields  = initSearchFields();

    searchFields.add( new DateField( "searchBeginDate","Incoming Date From: ",true ) );
    ((DateField)searchFields.getField("searchBeginDate")).setDateFormat("MM/dd/yyyy");
    searchFields.add( new DateField( "searchEndDate","To:", true ) );
    ((DateField)searchFields.getField("searchEndDate")).setDateFormat("MM/dd/yyyy");
    searchFields.add( new Field    ( "searchValue","Search Criteria:", 50, 50, true ) );
    searchFields.add( new CurrencyField( "searchAmountLow","Search Amount Low:", 10, 10, true ) );
    searchFields.add( new CurrencyField( "searchAmountHigh","Search Amount High:", 10, 10, true ) );
  }

  protected class MCFunctionCodesTable extends DropDownTable
  {
    public MCFunctionCodesTable( )
    {
      addElement("","select one");
      addElement("205", "205 Second Presentment - Full Amount");
      addElement("282", "282 Second Presentment - Partial Amount");
    }
  }

  protected class RepresentReasonCodesTable extends DropDownTable
  {
    public RepresentReasonCodesTable( long id )
    {
      ResultSetIterator         it          = null;
      ResultSet                 resultSet   = null;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:124^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  rd.repr_reason_code,
//                    rd.repr_reason_code || ' ' || rd.repr_reason_desc
//            from    network_chargebacks         cb,
//                    chargeback_repr_reason_desc rd
//            where   cb.cb_load_sec = :id and
//                    rd.card_type = nvl(cb.card_type,
//                                       decode(substr(cb.card_number,1,1),
//                                              '4','VS','5','MC','ER')) and
//                    rd.reason_code = cb.reason_code
//            order by rd.repr_reason_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rd.repr_reason_code,\n                  rd.repr_reason_code || ' ' || rd.repr_reason_desc\n          from    network_chargebacks         cb,\n                  chargeback_repr_reason_desc rd\n          where   cb.cb_load_sec =  :1  and\n                  rd.card_type = nvl(cb.card_type,\n                                     decode(substr(cb.card_number,1,1),\n                                            '4','VS','5','MC','ER')) and\n                  rd.reason_code = cb.reason_code\n          order by rd.repr_reason_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.ChargebackQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.queues.ChargebackQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:136^9*/
        resultSet = it.getResultSet();

        if ( resultSet.next() )
        {
          // value/name pairs
          addElement("","select one");

          do
          {
            addElement(resultSet);
          }
          while(resultSet.next());
        }
        else
        {
          String        rcode   = null;
          String        rdesc   = null;

          /*@lineinfo:generated-code*//*@lineinfo:155^11*/

//  ************************************************************
//  #sql [Ctx] { select  cb.reason_code,
//                      cb.reason_code || ' ' || rd.reason_desc
//              
//              from    network_chargebacks     cb,
//                      chargeback_reason_desc  rd
//              where   cb.cb_load_sec = :id and
//                      rd.card_type(+) = nvl(cb.card_type,
//                                             decode(substr(cb.card_number,1,1),
//                                                    '4','VS','5','MC','ER')) and
//                      rd.reason_code(+) = cb.reason_code and
//                      rd.item_type(+) = 'C'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cb.reason_code,\n                    cb.reason_code || ' ' || rd.reason_desc\n             \n            from    network_chargebacks     cb,\n                    chargeback_reason_desc  rd\n            where   cb.cb_load_sec =  :1  and\n                    rd.card_type(+) = nvl(cb.card_type,\n                                           decode(substr(cb.card_number,1,1),\n                                                  '4','VS','5','MC','ER')) and\n                    rd.reason_code(+) = cb.reason_code and\n                    rd.item_type(+) = 'C'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.ChargebackQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rcode = (String)__sJT_rs.getString(1);
   rdesc = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:168^11*/
          // value/name pairs
          addElement(rcode,rdesc);
        }
        resultSet.close();
        it.close();
      }
      catch(Exception e)
      {
        logEntry("constructor(" + id + ")",e.toString());
      }
      finally
      {
        try{it.close();}catch(Exception e){}
        cleanUp();
      }
    }
  }

  public class MerchantOnlyValidation implements Validation
  {
    int       BankNumber    = 0;
    String    ErrorMessage  = null;

    public MerchantOnlyValidation( int bankNum )
    {
      BankNumber = bankNum;
    }

    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate(String fieldData)
    {
      int         recCount      = 0;

      try
      {
        connect();

        ErrorMessage = null;

        /*@lineinfo:generated-code*//*@lineinfo:212^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(mf.merchant_number) 
//            from    mif                   mf
//            where   mf.bank_number = :BankNumber and
//                    mf.merchant_number = :fieldData
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mf.merchant_number)  \n          from    mif                   mf\n          where   mf.bank_number =  :1  and\n                  mf.merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.queues.ChargebackQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,BankNumber);
   __sJT_st.setString(2,fieldData);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:218^9*/
        if ( recCount == 0 )
        {
          ErrorMessage = "Invalid merchant account number";
        }
      }
      catch( Exception e )
      {
        // ingore and consider this to be failed validation
        ErrorMessage = "Error: " + e.toString();
      }
      finally
      {
        cleanUp();
      }
      return( ErrorMessage == null );
    }
  }

  public class RepresentRequiredValidation implements Validation
  {
    Field           ActionField       = null;
    String          ErrorMessage      = null;

    public RepresentRequiredValidation( Field action )
    {
      ActionField = action;
    }

    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate(String fieldData)
    {
      try
      {
        ErrorMessage = null;

        switch(ActionField.asInteger())
        {
          case MesQueues.Q_CHARGEBACKS_REPR:
          case MesQueues.Q_CHARGEBACKS_REMC:
            if ( fieldData == null || fieldData.equals("") )
            {
              ErrorMessage = "Field required for representment";
            }
            break;

          default:
            break;
        }
      }
      catch( Exception e )
      {
        // ingore and consider this to be failed validation
        ErrorMessage = "Error: " + e.toString();
      }
      return( ErrorMessage == null );
    }
  }

  public boolean isSubmitAllowed(UserBean user)
  {
    if( queueItem!=null && ((ChargebackQueueData)queueItem).RevDate != null)
    {
      return false;
    }
    return true;
  }

  public static class ChargebackQueueData extends QueueData
  {
    public  String        CardNumber              = null;
    public  String        CBRefNum                = null;
    public  String        CreditDate              = null;
    public  String        CreditUrl               = null;
    public  String        FirstTime               = null;
    public  long          OriginalLoadSec         = 0L;
    public  String        ReasonCode              = null;
    public  String        ReasonDesc              = null;
    public  String        ReferenceNumber         = null;
    public  String        SaleUrl                 = null;
    public  double        TranAmount              = 0.0;
    public  Date          TranDate                = null;
    public  Date          RevDate                 = null;
    public  Date          incomingDate            = null;
    public  Date          reviewDate              = null;
    public  int           merchantRiskFlag        = -1;

    public void setExtendedData(ResultSet resultSet)
    {
      StringBuffer        temp      = new StringBuffer();
      try
      {
        reviewDate      = resultSet.getDate("review_date");
        CardNumber      = processString(resultSet.getString("card_number"));
        CBRefNum        = processString(resultSet.getString("cb_ref_num"));
        CreditDate      = processString(resultSet.getString("credit_batch_date"));
        FirstTime       = processString(resultSet.getString("first_time"));
        OriginalLoadSec = resultSet.getLong("org_load_sec");
        ReasonCode      = processString(resultSet.getString("reason_code"));
        ReasonDesc      = processString(resultSet.getString("reason_desc"));
        ReferenceNumber = processString(resultSet.getString("ref_num"));
        TranAmount      = resultSet.getDouble("tran_amount");
        TranDate        = resultSet.getDate("tran_date");
        RevDate         = resultSet.getDate("rev_date");
        incomingDate    = resultSet.getDate("incoming_date");
        merchantRiskFlag = resultSet.getInt("merchant_risk_flag");//==1? flagged as risk

        log.debug("RevDate = "+ RevDate);

        if ( resultSet.getInt("tran_data_valid") == 1 )
        {
          temp.setLength(0);
          temp.append("/jsp/utils/find_transaction.jsp?dataType=0&reportType=1&findTran=true");
          temp.append("&findHierarchyNode=");
          temp.append(resultSet.getLong("merchant_number"));
          temp.append("&reportDateBegin=");
          temp.append(resultSet.getString("dt_batch_date"));
          temp.append("&reportDateEnd=");
          temp.append(resultSet.getString("dt_batch_date"));
          temp.append("&findCardNum=");
          temp.append(CardNumber);

          SaleUrl = temp.toString();
        }

        if ( resultSet.getInt("credit_issued") == 1 )
        {
          temp.setLength(0);
          temp.append("/jsp/utils/find_transaction.jsp?dataType=0&reportType=1&findTran=true");
          temp.append("&findHierarchyNode=");
          temp.append(resultSet.getLong("merchant_number"));
          temp.append("&reportDateBegin=");
          temp.append(resultSet.getString("credit_batch_date"));
          temp.append("&reportDateEnd=");
          temp.append(resultSet.getString("credit_batch_date"));
          temp.append("&findCardNum=");
          temp.append(CardNumber);

          CreditUrl = temp.toString();
        }
      }
      catch(Exception e)
      {
        log.info("exception - "+e.getMessage());
        SyncLog.LogEntry(this.getClass().getName() + "::setExtendedData()", e.toString());
      }
    }

    /**
     * Indicates that the CB is over a certain # of days overdue
     * in this case, hard-coded at 40
     */
    public boolean isOverdue()
    {
      Date _date = null;

      /*
      if(FirstTime.equalsIgnoreCase("y"))
      {
        _date = incomingDate;
      }
      else
      {
        _date = TranDate;
      }

      if(_date==null)
      {
        return false;
      }
      */

      _date = incomingDate;

      java.util.Date now = DBGrunt.getCurrentDate();

      if(MesCalendar.daysBetween(_date, now) > 40d)
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    /************************************************
     * new DOCS section for Chargebacks -
     * this may move elsewhere, but for now it's here
     ************************************************/
    private ArrayList documents = null; //contains ../tools/DocData.java for now

    public void buildDocuments()
    {
      DocumentTool  docTool = new DocumentTool();
      ArrayList     temp    = null;

      documents = docTool.buildSimpleDocList(getId());

      if ( OriginalLoadSec != 0L )
      {
        temp = docTool.buildSimpleDocList(OriginalLoadSec);
        if ( documents == null )
        {
          documents = temp;
        }
        else if ( temp != null )
        {
          documents.addAll( temp );
        }
      }
    }

    public void attachDocument(String fileName, String docType, String newFileName, byte[] body)
    throws Exception
    {

      log.debug("fileName = "+fileName);
      log.debug("docType = "+docType);
      log.debug("newFileName = "+newFileName);

      //fileName cannot be null
      if(null==fileName){
        throw new Exception("Filename is invalid.");
      }

      //cbDocType = 1 = merchant support doc
      int cbDocType=1;
      try
      {

        cbDocType = Integer.parseInt(docType);
        DocumentTool docTool = new DocumentTool();
        docTool.attachCBDocument(this.getId(), cbDocType, fileName, newFileName, body);
      }
      catch (NumberFormatException nfe)
      {
        throw new Exception("Unable to attach document type selected");
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public boolean removeDocument(String[] docIds)
    {
      if(docIds==null)
      {
        return false;
      }

      DocumentTool docTool = new DocumentTool();
      long dId;
      boolean result = true;

      for(int i=0;i<docIds.length;i++){
        try
        {
          dId = Long.parseLong(docIds[i]);
        }
        catch(Exception e)
        {
          //all or nothing
          return false;
        }
        result = docTool.removeCBDocument(this.getId(), dId);
        if(!result)
        {
          break;
        }
      }

      return result;

    }

    public Iterator getDocuments()
    {
      try
      {
        return documents.iterator();
      }
      catch( Exception e)
      {
        return null;
      }
    }

    public boolean hasDocuments()
    {
      return documents != null;
    }

  }

  public void doAction( HttpServletRequest request )
  {
    String            actionCode    = null;
    RadioButtonField  actionField   = null;
    int               docInd        = 0;
    long              itemId        = HttpHelper.getLong(request, "id", -1);
    String            fcode         = null;
    Field             field         = null;
    StringBuffer      msg           = new StringBuffer();
    int               recCount      = 0;
    String            reprCode      = "0";
    String            userLogin     = null;
    int               workedId      = 0;
    boolean           queryActionCode = true;

    super.doAction(request);

    try
    {
      connect();

      try
      {
        userLogin = user.getLoginName();
      }
      catch(Exception e)
      {
        userLogin = "WebQueue";
      }

      actionField = (RadioButtonField)getField("action");

      log.debug("MesQueues.Q_CHARGEBACKS_SECOND_TIME = " + MesQueues.Q_CHARGEBACKS_SECOND_TIME);
      log.debug("toQueue = " + actionField.asInteger());

      if( actionField.isValid() )
      {
        // update the merchant number if necessary
        if ( (field = getField("merchantId")) != null )
        {
          /*@lineinfo:generated-code*//*@lineinfo:558^11*/

//  ************************************************************
//  #sql [Ctx] { update network_chargebacks cb
//              set merchant_number = :field.getData()
//              where cb_load_sec = :itemId and
//                    merchant_number = 0
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_487 = field.getData();
   String theSqlTS = "update network_chargebacks cb\n            set merchant_number =  :1 \n            where cb_load_sec =  :2  and\n                  merchant_number = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.queues.ChargebackQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_487);
   __sJT_st.setLong(2,itemId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:564^11*/
        }

        try
        {
          int toQueue = actionField.asInteger();

          // ALL is used to re-pend a chargeback that has
          // not been processed (sent to TSYS).
          if ( toQueue == MesQueues.Q_CHARGEBACKS_ALL )
          {
            // if the item has not been settled, erase the pending item and
            // move the items back to the appropriate pending queue
            ChargebackTools.pendChargeback(Ctx,itemId);

            // update with the new queue type
            /*@lineinfo:generated-code*//*@lineinfo:580^13*/

//  ************************************************************
//  #sql [Ctx] { select  qd.type  
//                from    q_data  qd
//                where   qd.item_type = 18 and
//                        qd.id = :itemId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  qd.type   \n              from    q_data  qd\n              where   qd.item_type = 18 and\n                      qd.id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.queues.ChargebackQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   toQueue = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:586^13*/
            actionField.setData(Integer.toString(toQueue));
          }
          else if ( toQueue == MesQueues.Q_CHARGEBACKS_MERCH_REB ||
                    toQueue == MesQueues.Q_CHARGEBACKS_MERCH_REB_HIGH)
          {
            //shift the queue and introduce the date, the actual
            //action code - MCHB - stays the same
            MerchantRebuttal mReb = new MerchantRebuttal(itemId, user);

            //ensure that it's going to the right Queue now
            toQueue = mReb.determineQueue();

            try
            {
              mReb.processRebuttal();
            }
            catch (Exception e)
            {
              //e.printStackTrace();
            }

          }

          //Want to force all aged accounts to the mgmt queue
          else if(  toQueue == MesQueues.Q_CHARGEBACKS_REPR ||
                    toQueue == MesQueues.Q_CHARGEBACKS_REMC )
          {
              int cbCount=0;

              /*@lineinfo:generated-code*//*@lineinfo:616^15*/

//  ************************************************************
//  #sql [Ctx] { select
//                    count(cb.cb_load_sec) 
//                  from
//                    network_chargebacks cb
//                  where
//                    cb.cb_load_sec = :itemId
//                    and trunc(cb.incoming_date) >= (trunc(sysdate)-40)
//                  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select\n                  count(cb.cb_load_sec)  \n                from\n                  network_chargebacks cb\n                where\n                  cb.cb_load_sec =  :1 \n                  and trunc(cb.incoming_date) >= (trunc(sysdate)-40)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.queues.ChargebackQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cbCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:625^16*/

              //if cb is valid, cbCount > 0
              //otherwise, it's too old...
              if (cbCount==0)
              {
                //only if the user is NOT allowed to process
                if(!user.hasRight(MesUsers.RIGHT_QUEUE_CHARGEBACK_ADMIN))
                {
                  toQueue = MesQueues.Q_CHARGEBACKS_MGMT;
                  queryActionCode = false;
                }
              }
          }

          //need to determine 2nd time queue based on card type
          else if( toQueue == MesQueues.Q_CHARGEBACKS_SECOND_TIME )

          {
            log.debug("determining second time queue type...");

            /*@lineinfo:generated-code*//*@lineinfo:646^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(cb.cb_load_sec) 
//                from    network_chargebacks   cb
//                where   cb.original_load_sec = :itemId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cb.cb_load_sec)  \n              from    network_chargebacks   cb\n              where   cb.original_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.queues.ChargebackQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:651^13*/

            // only create a second time chargeback
            // if there is not already a second time
            // entry for this chargeback.
            if ( recCount != 0 )
            {
              // second time already exists,
              // leave existing item in same queue
              toQueue = this.getType();
            }
            else    // second time does not exist, create new record
            {
              // generate a new control number and then
              // create a new entry for this second time
              // chargeback.  the new control number is
              // required in order to be able to MCHB the
              // item a second time.
              long newLoadSec = ChargebackTools.getNewLoadSec();
              if ( newLoadSec <= 0L )
              {
                throw new Exception("Chargeback control number generation failed");
              }

              //if this is a request for an item that has been worked
              //need to ensure that it is removed from cba if not already
              //processed (load_filename is null)
              if( this.getType() == MesQueues.Q_CHARGEBACKS_COMPLETED )
              {
                // removing any items waiting to be sent
                ChargebackTools.deletePendingCBActivity(Ctx,itemId);
              }

              //define queues based on constants for sqlj
              String cardType = getCardType(itemId);
              String status   = "N";
              if ( cardType.equals("VS") )
              {
                toQueue = MesQueues.Q_CHARGEBACKS_PRE_ARB;
                status  = "P";    // switch status to pre-arb
              }
              else if ( cardType.equals("MC") )
              {
                toQueue = MesQueues.Q_CHARGEBACKS_SECOND_TIME_MC;
              }
              else if ( cardType.equals("DC") )
              {
                //@ should these go into their own queue?
                toQueue = MesQueues.Q_CHARGEBACKS_SECOND_TIME_MC;
              }

              // create a new chargeback
              /*@lineinfo:generated-code*//*@lineinfo:703^15*/

//  ************************************************************
//  #sql [Ctx] { call copy_chargeback(:itemId,:newLoadSec,:status)
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN copy_chargeback( :1 , :2 , :3 )\n              \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.queues.ChargebackQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   __sJT_st.setLong(2,newLoadSec);
   __sJT_st.setString(3,status);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:706^15*/

              // make a queue entry for the next 2nd time item
              // the item will immediately be moved to the
              // correct second time queue by the queueTools servlet.
              /*@lineinfo:generated-code*//*@lineinfo:711^15*/

//  ************************************************************
//  #sql [Ctx] { insert into q_data
//                  (
//                    id,
//                    type,
//                    item_type,
//                    owner,
//                    date_created,
//                    source,
//                    affiliate
//                  )
//                  select  cb.cb_load_sec,
//                          :this.getType(),
//                          18,       -- items are chargebacks
//                          1,        -- owner, default to 1
//                          cb.incoming_date,
//                          :userLogin,
//                          cb.bank_number
//                  from    network_chargebacks   cb
//                  where   cb.cb_load_sec = :newLoadSec
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_488 = this.getType();
   String theSqlTS = "insert into q_data\n                (\n                  id,\n                  type,\n                  item_type,\n                  owner,\n                  date_created,\n                  source,\n                  affiliate\n                )\n                select  cb.cb_load_sec,\n                         :1 ,\n                        18,       -- items are chargebacks\n                        1,        -- owner, default to 1\n                        cb.incoming_date,\n                         :2 ,\n                        cb.bank_number\n                from    network_chargebacks   cb\n                where   cb.cb_load_sec =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.queues.ChargebackQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_488);
   __sJT_st.setString(2,userLogin);
   __sJT_st.setLong(3,newLoadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:732^15*/

              // note to link new CB with original CB
              insertNote(newLoadSec,"Original (First Time) Chargeback Control # " + String.valueOf(itemId));

              // update to queue item to use the newly create loadSec
              // so the QueueTools will move the new 2nd time item
              // into the appropriate 2nd time cb queue.
              getQueueItem().setId(newLoadSec);
            }

            //no short action code for these,
            //so skip the sql below
            queryActionCode = false;
          }
          else if( toQueue == MesQueues.Q_CHARGEBACKS_MGMT ||
                   toQueue == MesQueues.Q_CHARGEBACKS_AMEX )
          {
            queryActionCode = false;
          }

          if( queryActionCode )
          {
            /*@lineinfo:generated-code*//*@lineinfo:755^13*/

//  ************************************************************
//  #sql [Ctx] { select  ac.short_action_code 
//                from    chargeback_action_codes   ac
//                where   ac.queue_type = :toQueue
//                        -- and nvl(ac.worked_item,'N') = 'Y'
//                        and nvl( ac.queue_action,'N' ) = 'Y'
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ac.short_action_code  \n              from    chargeback_action_codes   ac\n              where   ac.queue_type =  :1 \n                      -- and nvl(ac.worked_item,'N') = 'Y'\n                      and nvl( ac.queue_action,'N' ) = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.queues.ChargebackQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,toQueue);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   actionCode = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:762^13*/
          }

          /************************************************************
           * QUEUE REDIRECTION
           * New queue system for chargebacks - want network_chargeback_activity
           * to remain the same, just the end result queue placement changes
           * NOTE: toQueue remains the same for action code processing
           * actionField is used by JSP to define chargeback queue
           ************************************************************/
          actionField.setData(Integer.toString(redirectQueue(toQueue)));
        }
        catch( java.sql.SQLException sqe )
        {
          log.debug("SQLException = " + sqe.getMessage());
          actionCode = null;
        }

        /*@lineinfo:generated-code*//*@lineinfo:780^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(cba.cb_load_sec)  
//            from    network_chargeback_activity cba
//            where   cba.cb_load_sec = :itemId and
//                    cba.action_code = :actionCode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cba.cb_load_sec)   \n          from    network_chargeback_activity cba\n          where   cba.cb_load_sec =  :1  and\n                  cba.action_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.queues.ChargebackQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   __sJT_st.setString(2,actionCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:786^9*/

        if ( recCount == 0 && actionCode != null )
        {
          // extract the extra fields required
          // to represent the item to the network
          if ( isRepresentment(actionCode) )
          {
            // build the user message for visa representments
            // mc representments just use the text (if any)
            // from the operator.
            String    ct;
            String    reasonCode;
            /*@lineinfo:generated-code*//*@lineinfo:799^13*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                        cb.reason_code
//                
//                from    network_chargebacks   cb
//                where   cb.cb_load_sec = :itemId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                      cb.reason_code\n               \n              from    network_chargebacks   cb\n              where   cb.cb_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.queues.ChargebackQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   ct = (String)__sJT_rs.getString(1);
   reasonCode = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:807^13*/

            if ( ct.equals("VS") )
            {
              // if the user specified a sub-reason that is not the
              // default (incoming reason) then add the code as the
              // first data in the user message field.
              if ( !reasonCode.equals( getField("representReasonCode") ) )
              {
                msg.append(getField("representReasonCode").getData());
                msg.append(" ");
              }
              // visa uses the same reason code
              // for both the incoming chargeback
              // and the representment.
              reprCode = reasonCode;
            }
            else if ( ct.equals("MC") )
            {
              // mc supports separate reason codes for representments
              reprCode  = getField("representReasonCode").getData();
              fcode     = getField("functionCode").getData();
            }
            docInd = (getData("docInd").equals("y") ? 1 : 0 );

            // setup the user message
            msg.append(getData("userMsg"));
          }

          workedId = 0;   // reset
          do
          {
            workedId++;

            /*@lineinfo:generated-code*//*@lineinfo:841^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(cba.cb_load_sec) 
//                from    network_chargeback_activity cba
//                where   cba.cb_load_sec = :itemId and
//                        cba.file_load_sec = :workedId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cba.cb_load_sec)  \n              from    network_chargeback_activity cba\n              where   cba.cb_load_sec =  :1  and\n                      cba.file_load_sec =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.queues.ChargebackQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   __sJT_st.setInt(2,workedId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:847^13*/
          } while ( recCount != 0 );

          /*@lineinfo:generated-code*//*@lineinfo:850^11*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargeback_activity
//              (
//                cb_load_sec,
//                file_load_sec,
//                action_code,
//                action_date,
//                repr_reason_code,
//                function_code,
//                document_indicator,
//                user_message,
//                user_login,
//                action_source
//              )
//              values
//              (
//                :itemId,
//                :workedId,
//                :actionCode,
//                trunc(sysdate),
//                :reprCode,
//                :fcode,
//                :docInd,
//                :msg.toString(),
//                :userLogin,
//                :MesChargebacks.AS_WEB
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_489 = msg.toString();
   String theSqlTS = "insert into network_chargeback_activity\n            (\n              cb_load_sec,\n              file_load_sec,\n              action_code,\n              action_date,\n              repr_reason_code,\n              function_code,\n              document_indicator,\n              user_message,\n              user_login,\n              action_source\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n              trunc(sysdate),\n               :4 ,\n               :5 ,\n               :6 ,\n               :7 ,\n               :8 ,\n               :9 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.queues.ChargebackQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   __sJT_st.setInt(2,workedId);
   __sJT_st.setString(3,actionCode);
   __sJT_st.setString(4,reprCode);
   __sJT_st.setString(5,fcode);
   __sJT_st.setInt(6,docInd);
   __sJT_st.setString(7,__sJT_489);
   __sJT_st.setString(8,userLogin);
   __sJT_st.setInt(9,MesChargebacks.AS_WEB);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:878^11*/

          /*@lineinfo:generated-code*//*@lineinfo:880^11*/

//  ************************************************************
//  #sql [Ctx] { commit
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:883^11*/
        }//if ( recCount == 0 && actionCode != null )
      }//if( actionField.isValid() )
    }
    catch( Exception e )
    {
      e.printStackTrace();
      logEntry( "doAction()", e.toString() );
    }
    finally
    {
      cleanUp();
    }
  }

  public String getActionDisplayName( )
  {
    return( "Chargeback Status" );
  }

  public String getBackLink()
  {
    return("/jsp/reports/index.jsp?com.mes.ReportMenuId=" + getMenuId());
  }

  protected String getCardType( long itemId )
  {
    String        cardNumber    = null;
    String        cardType      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:915^7*/

//  ************************************************************
//  #sql [Ctx] { select  cb.card_type, cb.card_number
//          
//          from    network_chargebacks   cb
//          where   cb.cb_load_sec = :itemId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cb.card_type, cb.card_number\n         \n        from    network_chargebacks   cb\n        where   cb.cb_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.queues.ChargebackQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cardType = (String)__sJT_rs.getString(1);
   cardNumber = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:921^7*/

      if ( cardType == null )
      {
        cardType = TridentTools.decodeVisakCardType(cardNumber);
      }
    }
    catch(Exception e)
    {
      logEntry("getCardType(" + itemId + ")", e.toString());
    }
    return( cardType );
  }

  public String getDescriptionURL(QueueData qd)
  {
    long              merchantId  = 0L;
    String            retVal      = null;
    StringBuffer      temp        = new StringBuffer();

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:945^7*/

//  ************************************************************
//  #sql [Ctx] { select  cb.merchant_number 
//          from    network_chargebacks cb
//          where   cb.cb_load_sec = :qd.getId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_490 = qd.getId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cb.merchant_number  \n        from    network_chargebacks cb\n        where   cb.cb_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.queues.ChargebackQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_490);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:950^7*/

      if ( merchantId != 0L )
      {
        temp.append("/jsp/maintenance/view_account.jsp?merchant=");
        temp.append(merchantId);
        retVal = temp.toString();
      }
    }
    catch( Exception e )
    {
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }

  protected QueueData getEmptyQueueData()
  {
    QueueData       retVal = new ChargebackQueueData();
    retVal.setDateFormat("MM/dd/yyyy");
    return( retVal );
  }

  public String getGenericSearchDesc()
  {
    StringBuffer          desc      = new StringBuffer();


    desc.append("<ul>");
    desc.append("  <li>Merchant Number</li>");
    desc.append("  <li>Merchant DBA Name (partial is OK)</li>");
    desc.append("  <li>Case #</li>");
    desc.append("  <li>Reference Number</li>");
    desc.append("  <li>Reason Code</li>");
    desc.append("  <li>Chargeback Reference Number</li>");
    desc.append("  <li>Card Number (min first 6)</li>");
    desc.append("</ul>");

    return( desc.toString() );
  }

  public int getMenuId()
  {
    return( MesMenus.MENU_ID_CHARGEBACK_QUEUES );
  }

  protected boolean isRepresentment( String actionCode )
  {
    boolean       retVal      = false;

    switch( actionCode.charAt(0) )
    {
      case 'C':     // REMC
      case 'S':     // REPR
        retVal = true;
        break;
    }
    return( retVal );
  }

  protected void loadExtraColumns(ExtraColumnBean extraColumns, UserBean user)
  {

    LinkData        linkData    = null;

    try
    {
      if( getType() == MesQueues.Q_CHARGEBACKS_COMPLETED)
      {
        extraColumns.addColumn("Type","detail_desc");
      }

      // add the extra columns
      if (  getType() == MesQueues.Q_CHARGEBACKS_ALL )
      {
        extraColumns.addColumn("Queue", "detail_desc");
      }

      // setup the merchant number column as a link to
      // the mid assignment screen
      linkData = extraColumns.getNewLinkData();
      linkData.setValidationColName("merchant_number_missing");
      linkData.addLink("loadSec","id");
      extraColumns.addColumn("Merch #", "merchant_number", "/jsp/admin/cb_assign_mid.jsp?itemType=C", null, null, linkData);

      extraColumns.addColumn("Card #", "card_number");
      extraColumns.addColumn("Amount","tran_amount","","","C");
      extraColumns.addColumn("Reason","reason_code");
      extraColumns.addColumn("First Time","first_time");
      extraColumns.addColumn("CB Ref #","cb_ref_num");
      extraColumns.addColumn("Tran Date","tran_date_str");

      // setup the tran ref # column as a link to
      // the find transaction screen.
      linkData = extraColumns.getNewLinkData();
      linkData.setValidationColName("tran_data_valid");
      linkData.addLink("findHierarchyNode","merchant_number");
      linkData.addLink("reportDateBegin","dt_batch_date");
      linkData.addLink("reportDateEnd","dt_batch_date");
      linkData.addLink("findCardNum","card_number");
      extraColumns.addColumn("Tran Ref #", "ref_num", "/jsp/utils/find_transaction.jsp?dataType=0&reportType=1&findTran=true", "_findTran", null, linkData);

      // setup the credit date as a link to the
      // find transaction screen for the credit
      linkData = extraColumns.getNewLinkData();
      linkData.setValidationColName("credit_issued");
      linkData.addLink("findHierarchyNode","merchant_number");
      linkData.addLink("reportDateBegin","credit_batch_date");
      linkData.addLink("reportDateEnd","credit_batch_date");
      linkData.addLink("findCardNum","card_number");
      extraColumns.addColumn("Credit Date", "credit_batch_date", "/jsp/utils/find_transaction.jsp?dataType=0&reportType=1&findTran=true", "_findTran", null, linkData);

      //added 11/11/10
      extraColumns.addColumn("Rev Date", "rev_date_str");

      //added 8/27/2004
      extraColumns.addColumn("Trans ID", "tran_id");
      extraColumns.addColumn("Mo/To", "moto");
      extraColumns.addColumn("Category Code", "cat_code");

      //added 9/8/2004
      extraColumns.addColumn("Docs Rec'd", "docs_rec");
      extraColumns.addColumn("Reviewed", "review_date_str");
      extraColumns.addColumn("Accepted", "cb_accepted");

    }
    catch(Exception e)
    {
      logEntry("loadExtraColumns()", e.toString());
    }
  }

  protected void loadQueueData(UserBean user)
  {
    log.info("loading Queue DATA...");
    int             bankNumber    = 0;
    double          doubleVal     = 0.0;
    long            longVal       = -1L;
    FieldGroup      searchFields  = (FieldGroup)fields.getField("searchFields");
    int             workedQueue   = 0;

    try
    {
      // determine if the current queue is a worked chargeback queue
      workedQueue = ( MesQueues.isWorkedChargebackQueue( getType() ) ? 1 : 0 );

      bankNumber  = Integer.parseInt(Long.toString(user.getHierarchyNode()).substring(0,4));

      Date    searchBeginDate   = ((DateField)searchFields.getField( "searchBeginDate" )).getSqlDate();
      Date    searchEndDate     = ((DateField)searchFields.getField( "searchEndDate" )).getSqlDate();
      String  searchValue       = searchFields.getField( "searchValue" ).getData();
      double  searchAmountLow   = searchFields.getField( "searchAmountLow" ).asDouble();
      double  searchAmountHigh  = searchFields.getField( "searchAmountHigh" ).asDouble();
      int     searchEnabled     = (searchValue.equals("") ? 0 : 1);

      if( searchAmountLow == 0.0 )
      {
        searchAmountLow = -999999999.99;
      }
      else
      {
        searchEnabled = 1;
      }

      if( searchAmountHigh == 0.0 )
      {
        searchAmountHigh = 999999999.99;
      }
      else
      {
        searchEnabled = 1;
      }

      // get the value as a long and double if possible
      try{ longVal = Long.parseLong(searchValue); } catch( Exception e ) { longVal = -1L; }

      /*@lineinfo:generated-code*//*@lineinfo:1129^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    -- required fields
//                    qd.id                                   as id,
//                    qd.type                                 as type,
//                    qd.item_type                            as item_type,
//                    qd.owner                                as owner,
//                    qd.id                                   as control,
//                    cb.merchant_name                        as description,
//                    trunc(qd.date_created)                  as date_created,
//                    qd.source                               as source,
//                    qd.affiliate                            as affiliate,
//                    qd.last_changed                         as last_changed,
//                    qd.last_user                            as last_user,
//                    qd.locked_by                            as locked_by,
//                    0                                       as note_count,
//                    qt.status                               as status,
//                    qt.short_desc                           as queue_desc,
//                    -- options fields
//                    cb.merchant_number                      as merchant_number,
//                    decode( nvl(cb.merchant_number,0),
//                            0, 1, 0 )                       as merchant_number_missing,
//                    -- extended fields
//                    cb.original_load_sec                    as org_load_sec,
//                    cb.card_number                          as card_number,
//                    to_char(cb.tran_date,'mm/dd/yyyy')      as tran_date_str,
//                    cb.tran_date                            as tran_date,
//                    cb.reference_number                     as ref_num,
//                    cb.tran_amount                          as tran_amount,
//                    cb.reversal_date                        as rev_date,
//                    nvl(to_char(cb.reversal_date,'mm/dd/yyyy'),'')
//                                                            as rev_date_str,
//                    cb.reason_code                          as reason_code,
//                    rd.reason_desc                          as reason_desc,
//                    cb.cb_ref_num                           as cb_ref_num,
//                    trunc(sysdate)-trunc(qd.date_created)   as days_out,
//                    decode( cb.dt_batch_date,
//                            null, 0, 1 )                    as tran_data_valid,
//                    to_char(cb.dt_batch_date,'mm/dd/yyyy')  as dt_batch_date,
//                    cb.dt_batch_number                      as dt_batch_number,
//                    cb.dt_ddf_dt_id                         as dt_ddf_dt_id,
//                    to_char(cb.credit_batch_date,'mm/dd/yyyy') as credit_batch_date,
//                    cb.credit_batch_number                  as credit_batch_number,
//                    cb.credit_ddf_dt_id                     as credit_ddf_dt_id,
//                    decode(cb.credit_batch_date,
//                           null, 0, 1 )                     as credit_issued,
//                    cb.first_time_chargeback                as first_time,
//                    cb.incoming_date                        as incoming_date,
//                    get_cb_queue_desc(qd.type, qd.id, max(cba.file_load_sec))
//                                                            as detail_desc,
//                    cbv.trans_id                            as tran_id,
//                    cbv.mail_phone_order_ind                as moto,
//                    mf.sic_code                             as cat_code,
//                    decode(mrb.doc_status,'R','yes','no')   as docs_rec,
//                    cb.review_date                          as review_date,
//                    decode(cb.review_date,null,'no',to_char(cb.review_date,'mm/dd/yyyy'))
//                                                            as review_date_str,
//                    decode(mrb.cbr_status,'A','yes','no')   as cb_accepted,
//                    decode( nvl(mf.processor_id,0),
//                            1,0,    -- do not set risk flag for MBS backend
//                            decode(mf.dmacctst,null,0,1) )  as merchant_risk_flag
//          from      q_data                  qd,
//                    q_types                 qt,
//                    network_chargebacks     cb,
//                    chargeback_reason_desc  rd,
//                    network_chargeback_activity cba,
//                    network_chargeback_visa cbv,
//                    mif                     mf,
//                    chargeback_rebuttal     mrb
//          where     (
//                      (
//                        -- in this queue and this is not a worked queue
//                        qd.type = :this.type and
//                        :workedQueue = 0
//                      ) or
//                      (
//                        -- only show worked items when searched
//                        :workedQueue = 1 and
//                        :searchEnabled = 1 and  -- search only
//                        qd.type = :this.type
//                      ) or
//                      (
//                        -- allow for an all type to search all cb queues
//                        :this.type = :MesQueues.Q_CHARGEBACKS_ALL and -- 1701 and
//                        :searchEnabled = 1 and  -- search only
//                        qd.item_type = :MesQueues.Q_ITEM_TYPE_CHARGEBACK -- 18
//                      )
//                    ) and
//                    (
//                      ( :searchBeginDate is null and
//                        trunc(qd.date_created) >= (trunc(sysdate)-180) ) or -- changed from 90
//                      ( trunc(qd.date_created) between :searchBeginDate and :searchEndDate )
//                    ) and
//                    (
//                      ( :searchValue is null ) or
//                      ( cb.merchant_number = :longVal ) or
//                      ( qd.id = :longVal ) or
//                      ( lower(to_char(cb.cb_ref_num)) like  lower('%' || :searchValue || '%') ) or
//                      ( to_char(cb.reference_number) = :searchValue  ) or
//                      ( to_char(cb.reason_code) = :searchValue ) or
//                      ( lower(cb.merchant_name) like lower('%' || :searchValue || '%') ) or
//                      ( cb.card_number like ( substr(:searchValue,1,6) || 'xxxxxx' ||
//                                              decode( length(:searchValue),
//                                                      16, substr(:searchValue,-4),
//                                                      '%' ) ) )
//                    ) and
//                    cb.tran_amount between :searchAmountLow and :searchAmountHigh and
//                    qd.type = qt.type and
//                    cb.cb_load_sec = qd.id and
//                    (
//                      ( :bankNumber = 9999 and
//                        cb.bank_number in (select bank_number from mbs_banks where owner != 0) ) or
//                      ( cb.bank_number = :bankNumber )
//                    ) and
//                    rd.card_type(+) = nvl(cb.card_type,
//                                          decode(substr(cb.card_number,1,1),
//                                                 '4','VS','5','MC','ER')) and
//                    rd.item_type(+) = 'C' and -- chargeback reason
//                    rd.reason_code(+) = cb.reason_code
//                    and cba.cb_load_sec(+) = cb.cb_load_sec
//                    and cbv.load_sec(+) = cb.cb_load_sec
//                    and mf.merchant_number(+) = cb.merchant_number
//                    and mrb.cb_load_sec(+) = cb.cb_load_sec
//          group by
//                    qd.id,
//                    qd.type,
//                    qd.item_type,
//                    qd.owner,
//                    qd.id,
//                    cb.merchant_name,
//                    trunc(qd.date_created),
//                    qd.source,
//                    qd.affiliate,
//                    qd.last_changed,
//                    qd.last_user,
//                    qd.locked_by,
//                    qt.status,
//                    qt.short_desc,
//                    -- options fields
//                    cb.merchant_number,
//                    decode( nvl(cb.merchant_number,0),
//                            0, 1, 0 ),
//                    -- extended fields
//                    cb.original_load_sec,
//                    cb.card_number,
//                    to_char(cb.tran_date,'mm/dd/yyyy'),
//                    cb.tran_date,
//                    cb.reference_number,
//                    cb.tran_amount,
//                    cb.reversal_date,
//                    nvl(to_char(cb.reversal_date,'mm/dd/yyyy'),''),
//                    cb.reason_code,
//                    rd.reason_desc,
//                    cb.cb_ref_num,
//                    trunc(sysdate)-trunc(qd.date_created),
//                    decode( cb.dt_batch_date,
//                            null, 0, 1 ),
//                    to_char(cb.dt_batch_date,'mm/dd/yyyy'),
//                    cb.dt_batch_number,
//                    cb.dt_ddf_dt_id,
//                    to_char(cb.credit_batch_date,'mm/dd/yyyy'),
//                    cb.credit_batch_number,
//                    cb.credit_ddf_dt_id,
//                    decode(cb.credit_batch_date,
//                           null, 0, 1 ),
//                    cb.first_time_chargeback,
//                    cb.incoming_date,
//                    cbv.trans_id,
//                    cbv.mail_phone_order_ind,
//                    mf.sic_code,
//                    decode(mrb.doc_status,'R','yes','no'),
//                    cb.review_date,
//                    decode(cb.review_date,null,'no',to_char(cb.review_date,'mm/dd/yyyy')),
//                    decode(mrb.cbr_status,'A','yes','no'),
//                    decode( nvl(mf.processor_id,0),1,0,decode(mf.dmacctst,null,0,1) )
//          order by  (trunc(sysdate) - trunc(qd.date_created)) desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    -- required fields\n                  qd.id                                   as id,\n                  qd.type                                 as type,\n                  qd.item_type                            as item_type,\n                  qd.owner                                as owner,\n                  qd.id                                   as control,\n                  cb.merchant_name                        as description,\n                  trunc(qd.date_created)                  as date_created,\n                  qd.source                               as source,\n                  qd.affiliate                            as affiliate,\n                  qd.last_changed                         as last_changed,\n                  qd.last_user                            as last_user,\n                  qd.locked_by                            as locked_by,\n                  0                                       as note_count,\n                  qt.status                               as status,\n                  qt.short_desc                           as queue_desc,\n                  -- options fields\n                  cb.merchant_number                      as merchant_number,\n                  decode( nvl(cb.merchant_number,0),\n                          0, 1, 0 )                       as merchant_number_missing,\n                  -- extended fields\n                  cb.original_load_sec                    as org_load_sec,\n                  cb.card_number                          as card_number,\n                  to_char(cb.tran_date,'mm/dd/yyyy')      as tran_date_str,\n                  cb.tran_date                            as tran_date,\n                  cb.reference_number                     as ref_num,\n                  cb.tran_amount                          as tran_amount,\n                  cb.reversal_date                        as rev_date,\n                  nvl(to_char(cb.reversal_date,'mm/dd/yyyy'),'')\n                                                          as rev_date_str,\n                  cb.reason_code                          as reason_code,\n                  rd.reason_desc                          as reason_desc,\n                  cb.cb_ref_num                           as cb_ref_num,\n                  trunc(sysdate)-trunc(qd.date_created)   as days_out,\n                  decode( cb.dt_batch_date,\n                          null, 0, 1 )                    as tran_data_valid,\n                  to_char(cb.dt_batch_date,'mm/dd/yyyy')  as dt_batch_date,\n                  cb.dt_batch_number                      as dt_batch_number,\n                  cb.dt_ddf_dt_id                         as dt_ddf_dt_id,\n                  to_char(cb.credit_batch_date,'mm/dd/yyyy') as credit_batch_date,\n                  cb.credit_batch_number                  as credit_batch_number,\n                  cb.credit_ddf_dt_id                     as credit_ddf_dt_id,\n                  decode(cb.credit_batch_date,\n                         null, 0, 1 )                     as credit_issued,\n                  cb.first_time_chargeback                as first_time,\n                  cb.incoming_date                        as incoming_date,\n                  get_cb_queue_desc(qd.type, qd.id, max(cba.file_load_sec))\n                                                          as detail_desc,\n                  cbv.trans_id                            as tran_id,\n                  cbv.mail_phone_order_ind                as moto,\n                  mf.sic_code                             as cat_code,\n                  decode(mrb.doc_status,'R','yes','no')   as docs_rec,\n                  cb.review_date                          as review_date,\n                  decode(cb.review_date,null,'no',to_char(cb.review_date,'mm/dd/yyyy'))\n                                                          as review_date_str,\n                  decode(mrb.cbr_status,'A','yes','no')   as cb_accepted,\n                  decode( nvl(mf.processor_id,0),\n                          1,0,    -- do not set risk flag for MBS backend\n                          decode(mf.dmacctst,null,0,1) )  as merchant_risk_flag\n        from      q_data                  qd,\n                  q_types                 qt,\n                  network_chargebacks     cb,\n                  chargeback_reason_desc  rd,\n                  network_chargeback_activity cba,\n                  network_chargeback_visa cbv,\n                  mif                     mf,\n                  chargeback_rebuttal     mrb\n        where     (\n                    (\n                      -- in this queue and this is not a worked queue\n                      qd.type =  :1  and\n                       :2  = 0\n                    ) or\n                    (\n                      -- only show worked items when searched\n                       :3  = 1 and\n                       :4  = 1 and  -- search only\n                      qd.type =  :5 \n                    ) or\n                    (\n                      -- allow for an all type to search all cb queues\n                       :6  =  :7  and -- 1701 and\n                       :8  = 1 and  -- search only\n                      qd.item_type =  :9  -- 18\n                    )\n                  ) and\n                  (\n                    (  :10  is null and\n                      trunc(qd.date_created) >= (trunc(sysdate)-180) ) or -- changed from 90\n                    ( trunc(qd.date_created) between  :11  and  :12  )\n                  ) and\n                  (\n                    (  :13  is null ) or\n                    ( cb.merchant_number =  :14  ) or\n                    ( qd.id =  :15  ) or\n                    ( lower(to_char(cb.cb_ref_num)) like  lower('%' ||  :16  || '%') ) or\n                    ( to_char(cb.reference_number) =  :17   ) or\n                    ( to_char(cb.reason_code) =  :18  ) or\n                    ( lower(cb.merchant_name) like lower('%' ||  :19  || '%') ) or\n                    ( cb.card_number like ( substr( :20 ,1,6) || 'xxxxxx' ||\n                                            decode( length( :21 ),\n                                                    16, substr( :22 ,-4),\n                                                    '%' ) ) )\n                  ) and\n                  cb.tran_amount between  :23  and  :24  and\n                  qd.type = qt.type and\n                  cb.cb_load_sec = qd.id and\n                  (\n                    (  :25  = 9999 and\n                      cb.bank_number in (select bank_number from mbs_banks where owner != 0) ) or\n                    ( cb.bank_number =  :26  )\n                  ) and\n                  rd.card_type(+) = nvl(cb.card_type,\n                                        decode(substr(cb.card_number,1,1),\n                                               '4','VS','5','MC','ER')) and\n                  rd.item_type(+) = 'C' and -- chargeback reason\n                  rd.reason_code(+) = cb.reason_code\n                  and cba.cb_load_sec(+) = cb.cb_load_sec\n                  and cbv.load_sec(+) = cb.cb_load_sec\n                  and mf.merchant_number(+) = cb.merchant_number\n                  and mrb.cb_load_sec(+) = cb.cb_load_sec\n        group by\n                  qd.id,\n                  qd.type,\n                  qd.item_type,\n                  qd.owner,\n                  qd.id,\n                  cb.merchant_name,\n                  trunc(qd.date_created),\n                  qd.source,\n                  qd.affiliate,\n                  qd.last_changed,\n                  qd.last_user,\n                  qd.locked_by,\n                  qt.status,\n                  qt.short_desc,\n                  -- options fields\n                  cb.merchant_number,\n                  decode( nvl(cb.merchant_number,0),\n                          0, 1, 0 ),\n                  -- extended fields\n                  cb.original_load_sec,\n                  cb.card_number,\n                  to_char(cb.tran_date,'mm/dd/yyyy'),\n                  cb.tran_date,\n                  cb.reference_number,\n                  cb.tran_amount,\n                  cb.reversal_date,\n                  nvl(to_char(cb.reversal_date,'mm/dd/yyyy'),''),\n                  cb.reason_code,\n                  rd.reason_desc,\n                  cb.cb_ref_num,\n                  trunc(sysdate)-trunc(qd.date_created),\n                  decode( cb.dt_batch_date,\n                          null, 0, 1 ),\n                  to_char(cb.dt_batch_date,'mm/dd/yyyy'),\n                  cb.dt_batch_number,\n                  cb.dt_ddf_dt_id,\n                  to_char(cb.credit_batch_date,'mm/dd/yyyy'),\n                  cb.credit_batch_number,\n                  cb.credit_ddf_dt_id,\n                  decode(cb.credit_batch_date,\n                         null, 0, 1 ),\n                  cb.first_time_chargeback,\n                  cb.incoming_date,\n                  cbv.trans_id,\n                  cbv.mail_phone_order_ind,\n                  mf.sic_code,\n                  decode(mrb.doc_status,'R','yes','no'),\n                  cb.review_date,\n                  decode(cb.review_date,null,'no',to_char(cb.review_date,'mm/dd/yyyy')),\n                  decode(mrb.cbr_status,'A','yes','no'),\n                  decode( nvl(mf.processor_id,0),1,0,decode(mf.dmacctst,null,0,1) )\n        order by  (trunc(sysdate) - trunc(qd.date_created)) desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.queues.ChargebackQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setInt(2,workedQueue);
   __sJT_st.setInt(3,workedQueue);
   __sJT_st.setInt(4,searchEnabled);
   __sJT_st.setInt(5,this.type);
   __sJT_st.setInt(6,this.type);
   __sJT_st.setInt(7,MesQueues.Q_CHARGEBACKS_ALL);
   __sJT_st.setInt(8,searchEnabled);
   __sJT_st.setInt(9,MesQueues.Q_ITEM_TYPE_CHARGEBACK);
   __sJT_st.setDate(10,searchBeginDate);
   __sJT_st.setDate(11,searchBeginDate);
   __sJT_st.setDate(12,searchEndDate);
   __sJT_st.setString(13,searchValue);
   __sJT_st.setLong(14,longVal);
   __sJT_st.setLong(15,longVal);
   __sJT_st.setString(16,searchValue);
   __sJT_st.setString(17,searchValue);
   __sJT_st.setString(18,searchValue);
   __sJT_st.setString(19,searchValue);
   __sJT_st.setString(20,searchValue);
   __sJT_st.setString(21,searchValue);
   __sJT_st.setString(22,searchValue);
   __sJT_st.setDouble(23,searchAmountLow);
   __sJT_st.setDouble(24,searchAmountHigh);
   __sJT_st.setInt(25,bankNumber);
   __sJT_st.setInt(26,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.queues.ChargebackQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1305^7*/
    }
    catch(Exception e)
    {
      System.out.println("Exception: "+ e.getMessage());
      logEntry("getQueueData(" + user.getLoginName() + ")", e.toString());
    }
  }

  protected void loadQueueItem(long id)
  {
    log.info("loading Queue ITEM...");
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1319^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    -- required fields
//                    qd.id                                   as id,
//                    qd.type                                 as type,
//                    qd.item_type                            as item_type,
//                    qd.owner                                as owner,
//                    qd.id                                   as control,
//                    cb.merchant_name                        as description,
//                    trunc(qd.date_created)                  as date_created,
//                    qd.source                               as source,
//                    qd.affiliate                            as affiliate,
//                    qd.last_changed                         as last_changed,
//                    qd.last_user                            as last_user,
//                    qd.locked_by                            as locked_by,
//                    nvl(qn.note_count,0)                    as note_count,
//                    qt.status                               as status,
//                    qt.short_desc                           as queue_desc,
//                    -- options fields
//                    cb.merchant_number                      as merchant_number,
//                    -- extended fields
//                    cb.original_load_sec                    as org_load_sec,
//                    cb.card_number                          as card_number,
//                    to_char(cb.tran_date,'mm/dd/yyyy')      as tran_date_str,
//                    cb.tran_date                            as tran_date,
//                    cb.reversal_date                        as rev_date,
//                    nvl(to_char(cb.reversal_date,'mm/dd/yyyy'),'')
//                                                            as rev_date_str,
//                    cb.reference_number                     as ref_num,
//                    cb.tran_amount                          as tran_amount,
//                    cb.reason_code                          as reason_code,
//                    rd.reason_desc                          as reason_desc,
//                    cb.cb_ref_num                           as cb_ref_num,
//                    trunc(sysdate)-trunc(qd.date_created)   as days_out,
//                    decode( cb.dt_batch_date,
//                            null, 0, 1 )                    as tran_data_valid,
//                    to_char(cb.dt_batch_date,'mm/dd/yyyy')  as dt_batch_date,
//                    cb.dt_batch_number                      as dt_batch_number,
//                    cb.dt_ddf_dt_id                         as dt_ddf_dt_id,
//                    to_char(cb.credit_batch_date,'mm/dd/yyyy') as credit_batch_date,
//                    cb.credit_batch_number                  as credit_batch_number,
//                    cb.credit_ddf_dt_id                     as credit_ddf_dt_id,
//                    decode(cb.credit_batch_date,
//                           null, 0, 1 )                     as credit_issued,
//                    cb.first_time_chargeback                as first_time,
//                    cb.incoming_date                        as incoming_date,
//                    cb.review_date                          as review_date,
//                    decode( nvl(mf.processor_id,0),
//                            1,0,    -- do not set risk flag for MBS backend
//                            decode(mf.dmacctst,null,0,1) )  as merchant_risk_flag
//          from      q_data                  qd,
//                    ( select    qnote.id,
//                                count(*)  note_count
//                      from      q_group_to_types    qg,
//                                q_notes             qnote
//                      where     qg.group_type = :MesQueues.QG_CHARGEBACK_QUEUES and
//                                qnote.type = qg.queue_type and
//                                qnote.id = :id
//                      group by  qnote.id )  qn,
//                    q_types                 qt,
//                    network_chargebacks     cb,
//                    chargeback_reason_desc  rd,
//                    mif                     mf
//          where     (
//                      qd.type = :this.type or
//                      (
//                        :this.type = :MesQueues.Q_CHARGEBACKS_ALL and -- 1701 and
//                        qd.item_type = :MesQueues.Q_ITEM_TYPE_CHARGEBACK -- 18
//                      )
//                    ) and
//                    qd.id   = :id and
//                    cb.cb_load_sec = qd.id and
//                    qn.id(+) = qd.id and
//                    qt.type = qd.type and
//                    rd.card_type(+) = nvl(cb.card_type,
//                                          decode(substr(cb.card_number,1,1),
//                                                 '4','VS','5','MC','ER')) and
//                    rd.item_type(+) = 'C' and -- chargeback reason
//                    rd.reason_code(+) = cb.reason_code and
//                    mf.merchant_number(+) = cb.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    -- required fields\n                  qd.id                                   as id,\n                  qd.type                                 as type,\n                  qd.item_type                            as item_type,\n                  qd.owner                                as owner,\n                  qd.id                                   as control,\n                  cb.merchant_name                        as description,\n                  trunc(qd.date_created)                  as date_created,\n                  qd.source                               as source,\n                  qd.affiliate                            as affiliate,\n                  qd.last_changed                         as last_changed,\n                  qd.last_user                            as last_user,\n                  qd.locked_by                            as locked_by,\n                  nvl(qn.note_count,0)                    as note_count,\n                  qt.status                               as status,\n                  qt.short_desc                           as queue_desc,\n                  -- options fields\n                  cb.merchant_number                      as merchant_number,\n                  -- extended fields\n                  cb.original_load_sec                    as org_load_sec,\n                  cb.card_number                          as card_number,\n                  to_char(cb.tran_date,'mm/dd/yyyy')      as tran_date_str,\n                  cb.tran_date                            as tran_date,\n                  cb.reversal_date                        as rev_date,\n                  nvl(to_char(cb.reversal_date,'mm/dd/yyyy'),'')\n                                                          as rev_date_str,\n                  cb.reference_number                     as ref_num,\n                  cb.tran_amount                          as tran_amount,\n                  cb.reason_code                          as reason_code,\n                  rd.reason_desc                          as reason_desc,\n                  cb.cb_ref_num                           as cb_ref_num,\n                  trunc(sysdate)-trunc(qd.date_created)   as days_out,\n                  decode( cb.dt_batch_date,\n                          null, 0, 1 )                    as tran_data_valid,\n                  to_char(cb.dt_batch_date,'mm/dd/yyyy')  as dt_batch_date,\n                  cb.dt_batch_number                      as dt_batch_number,\n                  cb.dt_ddf_dt_id                         as dt_ddf_dt_id,\n                  to_char(cb.credit_batch_date,'mm/dd/yyyy') as credit_batch_date,\n                  cb.credit_batch_number                  as credit_batch_number,\n                  cb.credit_ddf_dt_id                     as credit_ddf_dt_id,\n                  decode(cb.credit_batch_date,\n                         null, 0, 1 )                     as credit_issued,\n                  cb.first_time_chargeback                as first_time,\n                  cb.incoming_date                        as incoming_date,\n                  cb.review_date                          as review_date,\n                  decode( nvl(mf.processor_id,0),\n                          1,0,    -- do not set risk flag for MBS backend\n                          decode(mf.dmacctst,null,0,1) )  as merchant_risk_flag\n        from      q_data                  qd,\n                  ( select    qnote.id,\n                              count(*)  note_count\n                    from      q_group_to_types    qg,\n                              q_notes             qnote\n                    where     qg.group_type =  :1  and\n                              qnote.type = qg.queue_type and\n                              qnote.id =  :2 \n                    group by  qnote.id )  qn,\n                  q_types                 qt,\n                  network_chargebacks     cb,\n                  chargeback_reason_desc  rd,\n                  mif                     mf\n        where     (\n                    qd.type =  :3  or\n                    (\n                       :4  =  :5  and -- 1701 and\n                      qd.item_type =  :6  -- 18\n                    )\n                  ) and\n                  qd.id   =  :7  and\n                  cb.cb_load_sec = qd.id and\n                  qn.id(+) = qd.id and\n                  qt.type = qd.type and\n                  rd.card_type(+) = nvl(cb.card_type,\n                                        decode(substr(cb.card_number,1,1),\n                                               '4','VS','5','MC','ER')) and\n                  rd.item_type(+) = 'C' and -- chargeback reason\n                  rd.reason_code(+) = cb.reason_code and\n                  mf.merchant_number(+) = cb.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.queues.ChargebackQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.QG_CHARGEBACK_QUEUES);
   __sJT_st.setLong(2,id);
   __sJT_st.setInt(3,this.type);
   __sJT_st.setInt(4,this.type);
   __sJT_st.setInt(5,MesQueues.Q_CHARGEBACKS_ALL);
   __sJT_st.setInt(6,MesQueues.Q_ITEM_TYPE_CHARGEBACK);
   __sJT_st.setLong(7,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.queues.ChargebackQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1399^7*/

    }
    catch(Exception e)
    {
      logEntry("loadQueueItem(" + this.type + ", " + id + ")", e.toString());
    }
  }

  protected void finishLoadItem()
  {
    //want to remove any options in the event this item has been reversed
    if( queueItem!=null && ((ChargebackQueueData)queueItem).RevDate != null )
    {
      deleteActionFields();
    }

  }

  public void setFields( HttpServletRequest request )
  {
    Field   action      = null;
    int     bankNumber  = 0;
    String  ct          = null;
    long    itemId      = HttpHelper.getLong(request, "id", -1);
    long    merchantId  = 0L;

    try
    {
      connect();

      if ( itemId != -1L )
      {
        FieldGroup fgroup = (FieldGroup)fields.getField("optionalFields");

        /*@lineinfo:generated-code*//*@lineinfo:1434^9*/

//  ************************************************************
//  #sql [Ctx] { select  cb.bank_number,
//                    nvl(cb.merchant_number,0),
//                    nvl( cb.card_type,
//                         decode( substr(cb.card_number,1,1),'4','VS','5','MC','UK' ) )
//            
//            from    network_chargebacks cb
//            where   cb.cb_load_sec = :itemId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cb.bank_number,\n                  nvl(cb.merchant_number,0),\n                  nvl( cb.card_type,\n                       decode( substr(cb.card_number,1,1),'4','VS','5','MC','UK' ) )\n           \n          from    network_chargebacks cb\n          where   cb.cb_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.queues.ChargebackQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   merchantId = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   ct = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1443^9*/

        if ( merchantId == 0L )
        {
          fgroup.add(new HierarchyNodeField(Ctx,
                                            ((long)bankNumber * 100000L),
                                            "merchantId",
                                            "Merchant Number",
                                            false ) );
          fgroup.getField("merchantId").addValidation( new MerchantOnlyValidation(bankNumber) );
        }

        action = fields.getField("action");

        if ( ct.equals("VS") )
        {
          fgroup.add(new DropDownField("representReasonCode", "Represent Reason Code",new RepresentReasonCodesTable(itemId),true));
          fgroup.add(new Field("userMsg", "Message",50,50,true));
          fgroup.add(new CheckboxField("docInd", "Documentation",false));

          // add the validations
          fgroup.getField("representReasonCode").addValidation( new RepresentRequiredValidation(action) );
          fgroup.getField("userMsg").addValidation( new RepresentRequiredValidation(action) );
        }
        else if ( ct.equals("MC") )
        {
          fgroup.add(new DropDownField("representReasonCode", "Represent Reason Code",new RepresentReasonCodesTable(itemId),true));
          fgroup.add(new DropDownField("functionCode", "Function Code", new MCFunctionCodesTable(),true));
          fgroup.add(new Field("userMsg", "Message",36,50,true));
          fgroup.add(new CheckboxField("docInd", "Documentation",false));

          // add validations
          fgroup.getField("representReasonCode").addValidation( new RepresentRequiredValidation(action) );
          fgroup.getField("functionCode").addValidation( new RepresentRequiredValidation(action) );
          fgroup.getField("userMsg").addValidation( new RepresentRequiredValidation(action) );
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setFields()",e.toString());
    }
    finally
    {
      cleanUp();
    }

    super.setFields( request );
  }

  public void setType( int newType )
  {

    super.setType( newType );

    overrideType = newType;

    log.debug("setType: queue type = "+ getType());

    String[][] buttons = setupButtons(newType);
    // if there are actions provide a way to select and provide comments

    if ( buttons != null )
    {
      fields.add( new RadioButtonField("action", buttons, -1, false, "Must select an action" ) );
      fields.add(new TextareaField("actionComments",500,8,80,true));
      // add the optional fields group
      fields.add(new FieldGroup("optionalFields"));
    }
  }

  protected String[][] setupButtons (int newType)
  {
    return setupButtons(newType, null);
  }

  protected String[][] setupButtons (int newType, Date reverseDate)
  {

    log.debug("calling setupButtons..."+reverseDate);

    String[][]          buttons     = null;

    //only allow actions to be implemented on non-reversed items
    if(reverseDate == null)
    {
      // setup the options based on type
      switch( newType )
      {

        case MesQueues.Q_CHARGEBACKS_VISA_WITH_DOC:
        case MesQueues.Q_CHARGEBACKS_VISA_NO_DOC:
        case MesQueues.Q_CHARGEBACKS_VISA_RESEARCH:
        case MesQueues.Q_CHARGEBACKS_MC_WITH_DOC:
        case MesQueues.Q_CHARGEBACKS_MC_NO_DOC:
        case MesQueues.Q_CHARGEBACKS_MC_RESEARCH:
        case MesQueues.Q_CHARGEBACKS_NEW:
        case MesQueues.Q_CHARGEBACKS_EXCEPTION:
        case MesQueues.Q_CHARGEBACKS_OVER_125:
          buttons =
            new String[][]
            {
              { "Management Queue" , Integer.toString(MesQueues.Q_CHARGEBACKS_MGMT) },
              { "Merchant Chargeback (MCHB)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_MCHB) },
              { "Move to 2nd Time Queue"      , Integer.toString(MesQueues.Q_CHARGEBACKS_SECOND_TIME) },
              { "Represent To Network (REPR)" , Integer.toString(MesQueues.Q_CHARGEBACKS_REPR) },
              { "Paid in Full (FPMT)"         , Integer.toString(MesQueues.Q_CHARGEBACKS_FULL_PMT) },
              { "Send To Collections (COLL)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_COLL) },
              { "Final Review (Completed)"    , Integer.toString(MesQueues.Q_CHARGEBACKS_COMPLETED) }
            };
          break;

        case MesQueues.Q_CHARGEBACKS_AMEX:
          buttons =
            new String[][]
            {
              { "Merchant Chargeback (MCHB)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_MCHB) },
              { "Completed (COMP)"            , Integer.toString(MesQueues.Q_CHARGEBACKS_COMPLETED) }
            };
          break;
          
        case MesQueues.Q_CHARGEBACKS_DISCOVER:
          buttons =
            new String[][]
            {
              { "Merchant Chargeback (MCHB)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_MCHB) },
              { "Represent To Network (REPR)" , Integer.toString(MesQueues.Q_CHARGEBACKS_REPR) },
              { "Completed (COMP)"            , Integer.toString(MesQueues.Q_CHARGEBACKS_COMPLETED) }
            };
          break;

        case MesQueues.Q_CHARGEBACKS_MCHB:
          buttons =
            new String[][]
            {
              { "Initiate Merchant Rebuttal"                      , Integer.toString(MesQueues.Q_CHARGEBACKS_MERCH_REB) },
              { "Credit Merchant and Represent Chargeback (REMC)" , Integer.toString(MesQueues.Q_CHARGEBACKS_REMC) },
              { "Credit Merchant and Take Loss (MCLS)"            , Integer.toString(MesQueues.Q_CHARGEBACKS_MCLS) },
              { "Pend (PEND) * Same day only"                     , Integer.toString(MesQueues.Q_CHARGEBACKS_ALL) }
            };
          break;

        case MesQueues.Q_CHARGEBACKS_COLL:
          buttons =
            new String[][]
            {
              { "Represent To Network (REPR)" , Integer.toString(MesQueues.Q_CHARGEBACKS_REPR) },
              { "Partial Payment (PPMT)",       Integer.toString(MesQueues.Q_CHARGEBACKS_PARTIAL_PMT) },
              { "Chargeback is a Loss (LOSS)" , Integer.toString(MesQueues.Q_CHARGEBACKS_LOSS) }
            };
          break;

        case MesQueues.Q_CHARGEBACKS_PRE_ARB:
          buttons =
            new String[][]
            {
              { "Accept - Merchant Chargeback (MCHB)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_MCHB) },
              { "Decline - Completed (COMP)"         , Integer.toString(MesQueues.Q_CHARGEBACKS_COMPLETED) },
              { "Paid in Full (FPMT)"         , Integer.toString(MesQueues.Q_CHARGEBACKS_FULL_PMT) },
              { "Send To Collections (COLL)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_COLL) }
            };
          break;

        case MesQueues.Q_CHARGEBACKS_SECOND_TIME:
        case MesQueues.Q_CHARGEBACKS_SECOND_TIME_MC:
          buttons =
            new String[][]
            {
              { "Merchant Chargeback (MCHB)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_MCHB) },
              { "Paid in Full (FPMT)"         , Integer.toString(MesQueues.Q_CHARGEBACKS_FULL_PMT) },
              { "Send To Collections (COLL)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_COLL) },
              { "Move to Completed (COMP)"    , Integer.toString(MesQueues.Q_CHARGEBACKS_COMPLETED) }
            };
          break;

        case MesQueues.Q_CHARGEBACKS_REPR:
          buttons =
            new String[][]
            {
              { "Pend (PEND) * Same day only" , Integer.toString(MesQueues.Q_CHARGEBACKS_ALL) }
            };
          break;

        case MesQueues.Q_CHARGEBACKS_MERCH_REB:
        case MesQueues.Q_CHARGEBACKS_MERCH_REB_HIGH:
          buttons =
            new String[][]
            {
              { "Management Queue" , Integer.toString(MesQueues.Q_CHARGEBACKS_MGMT) },
              { "Return to Merchant Chargeback (MCHB)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_MCHB) },
              { "Credit Merchant and Represent Chargeback (REMC)" , Integer.toString(MesQueues.Q_CHARGEBACKS_REMC) },
              { "Send To Collections (COLL)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_COLL) },
              { "Final Review (Completed)"    , Integer.toString(MesQueues.Q_CHARGEBACKS_COMPLETED) }
           };
          break;

        case MesQueues.Q_CHARGEBACKS_COMPLETED:
          buttons =
            new String[][]
            {
              { "Initiate Merchant Rebuttal"  , Integer.toString(MesQueues.Q_CHARGEBACKS_MERCH_REB) },
              { "Represent To Network (REPR)" , Integer.toString(MesQueues.Q_CHARGEBACKS_REPR) },
              { "Credit Merchant and Represent Chargeback (REMC)" , Integer.toString(MesQueues.Q_CHARGEBACKS_REMC) },
              { "Merchant Chargeback (MCHB)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_MCHB) },
              { "Paid in Full (FPMT)"         , Integer.toString(MesQueues.Q_CHARGEBACKS_FULL_PMT) },
              { "Partial Payment (PPMT)",       Integer.toString(MesQueues.Q_CHARGEBACKS_PARTIAL_PMT) },
              { "Chargeback is a Loss (LOSS)" , Integer.toString(MesQueues.Q_CHARGEBACKS_LOSS) },
              { "Send To Collections (COLL)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_COLL) },
              { "Move to 2nd Time Queue (Will 'Pend' item if same day)"      , Integer.toString(MesQueues.Q_CHARGEBACKS_SECOND_TIME) },
              { "Pend (PEND) * Same day only" , Integer.toString(MesQueues.Q_CHARGEBACKS_ALL) },
              { "Management Queue" , Integer.toString(MesQueues.Q_CHARGEBACKS_MGMT) }
            };
          break;

        case MesQueues.Q_CHARGEBACKS_MGMT:
          buttons =
            new String[][]
            {
              { "Move to Amex Queue"  , Integer.toString(MesQueues.Q_CHARGEBACKS_AMEX) },
              { "Initiate Merchant Rebuttal"  , Integer.toString(MesQueues.Q_CHARGEBACKS_MERCH_REB) },
              { "Return to Merchant Chargeback (MCHB)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_MCHB) },
              { "Represent To Network (REPR)" , Integer.toString(MesQueues.Q_CHARGEBACKS_REPR) },
              { "Credit Merchant and Represent Chargeback (REMC)" , Integer.toString(MesQueues.Q_CHARGEBACKS_REMC) },
              { "Send To Collections (COLL)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_COLL) },
              { "Pend (PEND) * Same day only" , Integer.toString(MesQueues.Q_CHARGEBACKS_ALL) },
              { "Final Review (Completed)"    , Integer.toString(MesQueues.Q_CHARGEBACKS_COMPLETED) }
            };
          break;

        case MesQueues.Q_CHARGEBACKS_REMC:
          buttons =
            new String[][]
            {
              { "Move to 2nd Time Queue (Will 'Pend' item if same day)"      , Integer.toString(MesQueues.Q_CHARGEBACKS_SECOND_TIME) },
              { "Pend (PEND) * Same day only" , Integer.toString(MesQueues.Q_CHARGEBACKS_ALL) }
            };
          break;

        case MesQueues.Q_CHARGEBACKS_PRE_COMPLIANCE:
          buttons =
            new String[][]
            {
              { "Merchant Chargeback (MCHB)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_MCHB) },
              { "Decline - Completed (COMP)"  , Integer.toString(MesQueues.Q_CHARGEBACKS_COMPLETED) }
            };
          break;

        //case MesQueues.Q_CHARGEBACKS_MCLS:
        //case MesQueues.Q_CHARGEBACKS_LOSS:
        case _DEFAULT:
          buttons =
            new String[][]
            {
              { "Pend (PEND) * Same day only" , Integer.toString(MesQueues.Q_CHARGEBACKS_ALL) },
              { "Move to 1st Time Queue"  , Integer.toString(MesQueues.Q_CHARGEBACKS_NEW) },
              { "Move to 2nd Time Queue"  , Integer.toString(MesQueues.Q_CHARGEBACKS_SECOND_TIME) },
            };
            break;

        default:
          break;
      }
    }
    else
    {
      //in the event that revdate is not null, make sure to remove any actions available
      log.debug("Removing action options");

      deleteActionFields();
    }

    return buttons;
  }

  /**************************************
   * redirectQueue
   * created to allow this particular class
   * the ability to change its queue direction
   * when path is built in queue_action.jsp
   * necessary as Q_CHARGEBACKS_COMPLETED now contains
   * MCHB, REPR & REMC - need to retain the Action Code
   * of each, but the all end up in the same place
   ***************************************/

  private int redirectQueue(int toQueue)
  {
    //anything designated for these queues will be changed into the Q_CHARGEBACKS_COMPLETED queue.
    int[] equalsCompleted = { MesQueues.Q_CHARGEBACKS_MCHB,
                              MesQueues.Q_CHARGEBACKS_REPR,
                              MesQueues.Q_CHARGEBACKS_REMC };

    for (int i=0;i<equalsCompleted.length;i++)
    {
      if(toQueue == equalsCompleted[i])
      {
        toQueue = MesQueues.Q_CHARGEBACKS_COMPLETED;
        break;
      }
    }

    return toQueue;
  }

  /**************************************
   * reconfigureOptions
   * created to allow this particular class
   * the ability to change its action choices
   * outside of the generic load in queue_item.jsp
   * necessary as Q_CHARGEBACKS_COMPLETED now contains
   * MCHB, REPR & REMC - but each of those has different
   * actions available to them.
   ***************************************/
  public void reconfigureOptions(ChargebackQueueData cbqd)
  {

    log.debug("Reconfiguring radio buttons...");

    ResultSetIterator         it   = null;
    ResultSet                 rs   = null;
    String[][]                buttons = null;

    String cbLoadSec = cbqd.getControl();
    try
    {
      //two types of reconfigure...not mutually exclusive

      //1. based on a 'completed' queue and it's unique sub-group
      if(this.getType()==MesQueues.Q_CHARGEBACKS_COMPLETED)
      {
        //will need to override item type
        useOverrideType = true;

        //get the most recent action code for this item
        String actionCode = null;
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:1779^9*/

//  ************************************************************
//  #sql [Ctx] it = { select action_code
//            from network_chargeback_activity
//            where cb_load_sec = :cbLoadSec
//            order by action_date desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select action_code\n          from network_chargeback_activity\n          where cb_load_sec =  :1 \n          order by action_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.queues.ChargebackQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,cbLoadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.queues.ChargebackQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1785^9*/

        rs = it.getResultSet();

        if(rs.next())
        {
          actionCode = rs.getString("action_code");
        }

        if(null != actionCode)
        {
          //rebuild button group according to this actionCode
          //as 'Completed' queue contains different categories,
          //all which have different functionality
          switch( actionCode.charAt(0) )
          {
            case 'D':     // MCHB
            case 'B':     // 2nd time MCHB
              buttons = setupButtons(MesQueues.Q_CHARGEBACKS_MCHB,cbqd.RevDate);
              overrideType = MesQueues.Q_CHARGEBACKS_MCHB;
              break;

            case 'S':     // REPR
              buttons = setupButtons(MesQueues.Q_CHARGEBACKS_REPR,cbqd.RevDate);
              overrideType = MesQueues.Q_CHARGEBACKS_REPR;
              break;

            case 'C':     // REMC
              buttons = setupButtons(MesQueues.Q_CHARGEBACKS_REMC,cbqd.RevDate);
              overrideType = MesQueues.Q_CHARGEBACKS_REMC;
              break;

            case 'W':     // COLL
            case 'L':     // LOSS
              try
              {
                //remove existing fields, if they exist
                deleteActionFields();
              }
              catch(Exception e)
              {
                //do nothing - fall through to default
              }

            default:
              //removes current "catch-all"
              //completed queue button array...
              buttons = setupButtons(_DEFAULT,cbqd.RevDate);
              break;

          }
          if(null!=buttons)
          {
            log.debug("adding new button field");

            RadioButtonField rbf = (RadioButtonField)fields.getField("action");
            rbf.resetRadioButtons(buttons);

          }
        }
      }

      //2. based on a merchant risk flag -
      //   a value of 1 means the item is flagged

      int riskFlag = cbqd.merchantRiskFlag;
      log.debug("riskFlag = "+riskFlag);
      if( riskFlag > 0 && cbqd.RevDate == null)
      {
        //OK, this CB item is flagged as risk, so REMC is not allowed as an option
        //need to check current button field and remove the REMC option
        RadioButtonField rbf = (RadioButtonField)fields.getField("action");
        int buttonCount = rbf.getButtonCount();
        for(int button=0;button<buttonCount;button++)
        {

          String value = rbf.getValue(button);

          if(value.equals(Integer.toString(MesQueues.Q_CHARGEBACKS_REMC)))
          {
            //one of the radio button options is REMC - need to disable it
             rbf.updateButtonInfo(button,
                                  "DISABLED due to risk - Credit Merchant and Represent (REMC)",
                                  ""+this.getType());

          }
          else if(value.equals(Integer.toString(MesQueues.Q_CHARGEBACKS_MCLS)))
          {
            //one of the radio button options is REMC - need to disable it
             rbf.updateButtonInfo(button,
                                  "DISABLED due to risk - Credit Merchant and take loss (MCLS)",
                                  ""+this.getType());
          }

        }
      }

    }
    catch (Exception e)
    {
      //nothing - any kind of error and we leave it as is
      log.debug("Exception - "+e.getMessage());
    }
    finally
    {
      try{it.close();}catch(Exception e){};
      try{rs.close();}catch(Exception e){};
      cleanUp();
    }
  }


  /**************************************
   * markAsReviewed
   * adds date of review to chargeback
   ***************************************/
  public void markAsReviewed(ChargebackQueueData cbqd, UserBean user)
  {
    try
    {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:1907^9*/

//  ************************************************************
//  #sql [Ctx] { update network_chargebacks
//            set review_date = sysdate
//            where cb_load_sec = :cbqd.getControl()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_491 = cbqd.getControl();
   String theSqlTS = "update network_chargebacks\n          set review_date = sysdate\n          where cb_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.queues.ChargebackQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_491);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1912^9*/

        Date newDate;

        /*@lineinfo:generated-code*//*@lineinfo:1916^9*/

//  ************************************************************
//  #sql [Ctx] { select review_date 
//            from network_chargebacks
//            where cb_load_sec = :cbqd.getControl()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_492 = cbqd.getControl();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select review_date  \n          from network_chargebacks\n          where cb_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.queues.ChargebackQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_492);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   newDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1921^9*/

        cbqd.reviewDate = newDate;

       /*@lineinfo:generated-code*//*@lineinfo:1925^8*/

//  ************************************************************
//  #sql [Ctx] { insert into chargeback_review
//           values
//           ( :user.getLoginName(), :newDate, :cbqd.getControl() )
//          };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_493 = user.getLoginName();
 String __sJT_494 = cbqd.getControl();
   String theSqlTS = "insert into chargeback_review\n         values\n         (  :1 ,  :2 ,  :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.queues.ChargebackQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_493);
   __sJT_st.setDate(2,newDate);
   __sJT_st.setString(3,__sJT_494);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1930^8*/

    }
    catch (Exception e)
    {
      log.debug("markAsReviewed Exception: "+e.getMessage());
    }
    finally
    {
      cleanUp();
    }
  }

  /**************************************
   * checkAutoNotifyStatus
   * checks network_chargeback_auto_notify table
   * for a given cb/merch
   ***************************************/
  public boolean checkAutoNotifyStatus(ChargebackQueueData cbqd)
  {

    boolean autoNotifyIsOn = false;

    try
    {
        connect();

        long orgGroup = 0L;

        /*@lineinfo:generated-code*//*@lineinfo:1959^9*/

//  ************************************************************
//  #sql [Ctx] { SELECT  o.org_group 
//            FROM    network_chargeback_auto_notify  an,
//                    organization                    o,
//                    group_merchant                  gm
//            WHERE   o.org_group = an.hierarchy_node and
//                    gm.org_num = o.org_num and
//                    gm.merchant_number = :cbqd.getMerchantNumber() and
//                    :cbqd.incomingDate between an.valid_date_begin and
//                                          an.valid_date_end
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_495 = cbqd.getMerchantNumber();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  o.org_group  \n          FROM    network_chargeback_auto_notify  an,\n                  organization                    o,\n                  group_merchant                  gm\n          WHERE   o.org_group = an.hierarchy_node and\n                  gm.org_num = o.org_num and\n                  gm.merchant_number =  :1  and\n                   :2  between an.valid_date_begin and\n                                        an.valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.queues.ChargebackQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_495);
   __sJT_st.setDate(2,cbqd.incomingDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orgGroup = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1970^9*/

        if(orgGroup > 0L)
        {
          autoNotifyIsOn = true;
        }
    }
    catch (Exception e)
    {
      log.debug("checkAutoNotifyStatus Exception: "+e.getMessage());
    }
    finally
    {
      cleanUp();
    }

    return autoNotifyIsOn;

  }

  /**************************************
   * toggleAutoNotifyStatus
   * uses merchant number as hierarchy node
   * NOTE: if merchant is controlled by a higher
   * association, this cannot be manipulated at
   * this level and 'toggle off' will never work
   ***************************************/
  public void toggleAutoNotifyStatus(String state, ChargebackQueueData cbqd)
  {
    log.debug("in toggle...");
    log.debug("state="+state);
    try
    {

      connect();

      if(state.indexOf("on")>-1)
      {
        /*@lineinfo:generated-code*//*@lineinfo:2008^9*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargeback_auto_notify
//            (hierarchy_node, valid_date_begin, valid_date_end)
//            values
//            (:cbqd.getMerchantNumber(),:cbqd.incomingDate,'31-DEC-9999')
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_496 = cbqd.getMerchantNumber();
   String theSqlTS = "insert into network_chargeback_auto_notify\n          (hierarchy_node, valid_date_begin, valid_date_end)\n          values\n          ( :1 , :2 ,'31-DEC-9999')";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.queues.ChargebackQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_496);
   __sJT_st.setDate(2,cbqd.incomingDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2014^9*/
      }
      else if (state.indexOf("off")>-1)
      {
        /*@lineinfo:generated-code*//*@lineinfo:2018^9*/

//  ************************************************************
//  #sql [Ctx] { DELETE FROM
//              network_chargeback_auto_notify
//            WHERE
//              hierarchy_node = :cbqd.getMerchantNumber()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_497 = cbqd.getMerchantNumber();
   String theSqlTS = "DELETE FROM\n            network_chargeback_auto_notify\n          WHERE\n            hierarchy_node =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.queues.ChargebackQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_497);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2024^9*/
      }

    }
    catch (Exception e)
    {
      log.debug("toggleAutoNotifyStatus Exception: "+e.getMessage());
    }
    finally
    {
      cleanUp();
    }
  }

  public String getDestinationURL()
  {
    //send every chargeback user back to the all chargeback queue
    //since that's the one they currently use, and there is no mechanism
    //available to capture queue history...
    //ChargebackQueue rebuilds itself every time
    return ( "/jsp/queues/queue.jsp?type=1701" );
  }

  public void deleteActionFields()
  {
    fields.deleteField("action");
    fields.deleteField("actionComments");
    fields.deleteField("optionalFields");
  }

}/*@lineinfo:generated-code*/