/*@lineinfo:filename=QueueBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/QueueBase.sqlj $

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.sql.ResultSet;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.forms.CheckboxField;
import com.mes.forms.DateField;
import com.mes.forms.DateRangeReport;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;


public class QueueBase extends DateRangeReport
{                   
  protected int                   type          = 0;
  protected int                   itemType      = 0;
  protected String                description   = "";
  protected String                functionURL   = "";
  protected boolean               userAllowed   = false;
  
  // extra column headers
  protected ExtraColumnBean       extraColumns  = null;
  
  protected Vector                rows              = new Vector();
  protected Vector                pages             = new Vector();
  protected QueueData             queueItem         = null;
  protected ResultSetIterator     it                = null;
  protected boolean               SearchSubmitted   = false;
  protected int                   CurrentPageBegin  = 0;
  private   boolean               MultiMoveEnabled  = false;
  
  public QueueBase()
  {
  }
  
  public QueueBase(UserBean user)
  {
    super(user);
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    if ( isMultiMoveEnabled() )
    {
      createMultiMoveFields(request);
    }
  }
  
  //
  //  loadMultiMoveFields( )
  //
  //  The default method assumes that all of the items in the queue
  //  should be prepared to have a check box.
  // 
  protected void createMultiMoveFields(HttpServletRequest request)
  {
    ResultSetIterator         it    = null;
    ResultSet                 rs    = null;
    
    try
    {
      connect();
      
      // create the necessary checkbox fields
      FieldGroup fgroup = (FieldGroup)fields.getField( "multiMoveFields" );
    
      /*@lineinfo:generated-code*//*@lineinfo:97^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id   as id
//          from    q_data    qd
//          where   qd.type = :this.type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id   as id\n        from    q_data    qd\n        where   qd.type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.QueueBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.queues.QueueBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:102^7*/
      rs = it.getResultSet();
  
      while(rs.next())
      {
        fgroup.add( new CheckboxField("id"+rs.getLong("id"),false) );
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("createMultiMoveFields()",e.toString());
    }      
    finally
    {
      try{it.close();}catch(Exception e){}
      cleanUp();
    }
  }
  
  public void doAction( HttpServletRequest request )
  {
    Field       comments      = getField("actionComments");
    long        itemId        = HttpHelper.getLong(request, "id", -1);
    
    try
    {
      if ( comments != null && !comments.getData().equals("") )
      {
        insertNote( itemId, comments.getData() );
      }
    }
    catch( Exception e )
    {
      logEntry( "doAction()", e.toString() );
    }      
    finally
    {
    }
  }

  public boolean dateRangeEnabled()
  {
    return false;
  }
  
  public boolean encodeData( int fileFormat, HttpServletResponse response )
    throws java.io.IOException
  {
    ExtraColumnBean                 extraColumns  = getExtraColumns();
    ExtraColumnBean.ExtraColumnRow.ExtraColumnRowField  
                                    field         = null;
    Vector                          fields        = null;
    StringBuffer                    line          = new StringBuffer();
    ServletOutputStream             out           = response.getOutputStream();
    QueueData                       qd            = null;
    boolean                         retVal        = true;
    int                             rowBegin      = 0;
    int                             rowEnd        = rows.size();
    String                          temp          = null;
    
    try
    {
      switch( fileFormat )
      {
        case mesConstants.FF_CSV:
          // setup the download filename, replace spaces with '_'
          line.append(getShortDescription());
          for( int i = 0; i < line.length(); ++i )
          {
            if( line.charAt(i) == ' ' ) 
            {
              line.setCharAt(i,'_');
            }
          }
          response.setContentType("application/csv");
          response.setHeader("Content-Disposition", ( "attachment; filename=" + line.toString() + ".csv" ) );
          
          // first time, output header
          line.setLength(0);
          line.append("\"Date Originated\",");
          line.append("\"Control\",");
          line.append("\"Description\",");
          line.append("\"Source\",");
          line.append("\"Type\",");
          line.append("\"Last Worked\",");
          line.append("\"Worked By\",");
          line.append("\"Notes\"");
        
          // show extra column headers
          for( int i=0; i < extraColumns.columnHeaders.size();  ++i )
          {
            line.append(",\"");
            line.append( (String)extraColumns.columnHeaders.elementAt(i) );
            line.append("\"");
          }
          out.print( line.toString() );
          out.print("\n" );
          
          // if CurrentPageBegin is negative then export all pages
          if ( CurrentPageBegin >= 0 )
          {
            QueuePageData curPage = getCurrentPage(CurrentPageBegin);
            rowBegin  = curPage.beginVal;
            rowEnd    = curPage.rowEnd;
          }            
          
          for( int i = rowBegin; i < rowEnd; ++i )
          {
            line.setLength(0);
            qd = (QueueData)rows.elementAt(i);
      
            line.append("\"");
            line.append(qd.getDateCreated());
            line.append("\",");
            line.append(qd.getControl());
            line.append(",\"");
            line.append(qd.getDescription());
            line.append("\",\"");
            line.append(qd.getSource());
            line.append("\",\"");
            line.append(qd.getAffiliate());
            line.append("\",\"");
            line.append(qd.getLastChangedDate());
            line.append("\",\"");
            line.append(qd.getLastUser());
            line.append("\",");
            line.append(qd.getNoteCount());
            
            // add extra column row data
            if( extraColumns.hasExtraColumns )
            {
              fields = ((ExtraColumnBean.ExtraColumnRow)(extraColumns.extraColumnRows.elementAt(i))).extraColumnRowFields;
              
              for(int j = 0; j < fields.size(); ++j)
              {
                field = (ExtraColumnBean.ExtraColumnRow.ExtraColumnRowField)(fields.elementAt(j));
                
                if ( field.data == null )
                {
                  temp = "";
                }
                else
                {
                  temp = field.data;
                  try
                  {
                    double dtemp = Double.parseDouble(temp.trim());
                    
                    // Excel will truncate these even though
                    // they are within quotations.  Adding
                    // a character at the beginning forces 
                    // Excel to consider this a text field
                    if ( temp.length() > 15 )
                    {
                      temp = "'" + field.data;
                    }
                  }
                  catch( NumberFormatException ne )
                  {
                    // ignore
                  }
                }                    
                line.append(",\"");
                line.append( temp );
                line.append("\"");
              }
            }
            
            if ( line.length() > 0 )
            {
              out.print( line.toString() );
              out.print("\n" );
            }              
          }
          break;
          
         // not supported
//        case mesConstants.FF_PDF:
//        case mesConstants.FF_FIXED:
//        case mesConstants.FF_CSV_STATIC:   // pre-loaded download temp file
        default:      
          retVal = false;
          break;
      }
    }
    catch( Exception e )
    {
      logEntry( "encodeData()",e.toString() );
      addError( "encodeData(): " + e.toString() );
      retVal = false;
    }      
    finally
    {
    }
    return( retVal );
  }
  
  public void encodeQueueUrl( StringBuffer buffer )
  {
    encodeQueueUrl( buffer, CurrentPageBegin );
  }
  
  public void encodeQueueUrl( StringBuffer buffer, int pageNum )
  {
    String      temp      = null;
    
    buffer.append( ( ( buffer.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
    buffer.append( "type=" );
    buffer.append( getType() );
    buffer.append( "&begin=" );
    buffer.append( pageNum);
    
    if( dateRangeEnabled() )
    {
      temp = getDatesAsParms();
      if( temp != null && temp.length() > 0 )
      {
        buffer.append( "&" );
        buffer.append( temp );
      }        
    }
    
    if( isSearchEnabled() )
    {
      temp = getSearchParamsQueryString();
      
      if( temp != null && temp.length() > 0 )
      {
        buffer.append("&");
        buffer.append( temp );
      }        
    }
  }
  
  public String getBackLink()
  {
    StringBuffer result = new StringBuffer();
    
    result.append("/jsp/menus/generic_menu.jsp?com.mes.ReportMenuId=");
    result.append(getMenuId());
    
    return result.toString();
  }

  // default implementation expects the description URL
  // to provide the necessary "name=" at the end 
  public String getDescriptionURL(QueueData qd)
  {
    StringBuffer        retVal      = new StringBuffer();
    
    retVal.append( getDescriptionURL() );
    retVal.append( qd.getId() );
    
    return( retVal.toString() );
  }

  public String getDescriptionURL()
  {
    String result = "/jsp/queues/queue.jsp?type=" + this.type + "&queueId=";
    return result;
  }
  
  public String getDestinationURL()
  {
    return( "/jsp/queues/queue.jsp?type=" + this.type );
  }

  /***************************************************************************/

  /*
  ** ACCESSORS
  */
  public Vector getRows()
  {
    return this.rows;
  }

  public Vector getPages()
  {
    return this.pages;
  }

  public QueueData getQueueItem()
  {
    return this.queueItem;
  }
  
  public String getActionDisplayName()
  {
    return( getMenuTitle() );     // default is just menu title
  }
  
  public String getBackLinkDesc()
  {
    StringBuffer  result  = new StringBuffer();

    try
    {
      result.append("<span class=\"smallText\">");
      result.append( getMenuTitle() );
      result.append("</span>");
    }
    catch(Exception e)
    {
      logEntry("getBackLinkDesc()", e.toString());
    }
    return( result.toString() );
  }

  public final QueuePageData getCurrentPage(int begin)
  {
    QueuePageData result = null;
    try
    {
      for(int i=0; i < pages.size(); ++i)
      {
        QueuePageData curPage = (QueuePageData)pages.elementAt(i);
        if(curPage.isCurrentPage(begin))
        {
          result = curPage;
          break;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("getCurrentPage(" + begin + ")", e.toString());
    }

    return result;
  }

  public String getDescription()
  {
    return this.description;
  }
  
  /*
  ** METHOD getEmptyQueueData()
  **
  ** Extending classes may override getEmptyQueueData() to return their own
  ** version of QueueData.
  */
  protected QueueData getEmptyQueueData()
  {
    return new QueueData();
  }
  
  public ExtraColumnBean getExtraColumns()
  {
    return this.extraColumns;
  }
  
  public String getFunctionURL()
  {
    return this.functionURL;
  }
  
  public String getGenericSearchDesc()
  {
    return(null);
  }
  
  public int getMenuId()
  {
    int result = 0;

    return result;
  }
  
  public String getMenuTitle( )
  {
    return( getMenuTitle( getMenuId() ) );
  }
  
  public String getMenuTitle( int menuId )
  {
    String      menuTitle   = "";
  
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:487^7*/

//  ************************************************************
//  #sql [Ctx] { select  menu_title 
//          from    menu_types
//          where   menu_id = :menuId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  menu_title  \n        from    menu_types\n        where   menu_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.QueueBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,menuId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   menuTitle = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:492^7*/
    }
    catch(Exception e)
    {
      logEntry("getMenuTitle()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( menuTitle );
  }
  
  public String getMultiMoveColumnHeader()
  {
    return("Move");   // default
  }
  
  public FieldGroup getMultiMoveFields()
  {
    FieldGroup        retVal      = null;
    
    try
    {
      retVal = (FieldGroup) fields.getField("multiMoveFields");
    }
    catch( Exception e )
    {
      // ingore and return null
    }
    return( retVal );
  }
  
  public FieldGroup getSearchFields()
  {
    FieldGroup        retVal      = null;
    
    try
    {
      retVal = (FieldGroup) fields.getField("searchFields");
    }
    catch( Exception e )
    {
      // ingore and return null
    }
    return( retVal );
  }
  
  /*
  ** METHOD getSearchParamsQueryString
  **
  ** This function "query string-izes" the search parameters held in the 'searchFields' field group.
  ** This query string is ammended to the url in the page links href in queue.jsp in order to persist the search paramters.
  **
  ** By default this method will add all the values in the searchFields group
  */
  public String getSearchParamsQueryString()
  {
    StringBuffer    buffer        = new StringBuffer();
    String          data          = null;
    Field           field         = null;
    Vector          searchFields  = null;
    
    if ( isSearchEnabled() )
    {
      searchFields  = getSearchFields().getFieldsVector();

      for( int i = 0; i < searchFields.size(); ++i )
      { 
        field = (Field)searchFields.elementAt(i);
        
        if ( field instanceof DateField )
        {
          data = DateTimeFormatter.getFormattedDate( ((DateField)field).getUtilDate(),"MM/dd/yyyy" );
        }
        else
        {
          data = field.getData();
        }
        
        if( data != null && data.length() > 0 ) 
        {
          // skip first '&'
          buffer.append( ( ( i == 0 ) ? "" : "&" ) );
          buffer.append( field.getName() );
          buffer.append( "=" );
          buffer.append( HttpHelper.urlEncode(data) );
        }          
      }
    }        
    return( buffer.toString() );
  }
  
  public String getShortDescription( )
  {
    return( getShortDescription( getType() ) );
  }
  
  public String getShortDescription( int type )
  {
    String        retVal      = null;
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:597^7*/

//  ************************************************************
//  #sql [Ctx] { select qt.short_desc 
//          from    q_types qt
//          where   qt.type = :type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select qt.short_desc  \n        from    q_types qt\n        where   qt.type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.queues.QueueBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,type);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:602^7*/
    }
    catch( Exception e )
    {
      logEntry( "getShortDescription("+type+")", e.toString() );
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  public int getType()
  {
    return this.type;
  }
  
  public boolean getUserAllowed()
  {
    return this.userAllowed;
  }
  
  // 
  //  This methods allow a derived class to support bulk movement of
  //  items in a queue to another queue.  If multiMove is supported 
  //  then the JSP will draw a single check box for each control number
  //  in the queue and provide a submit button to allow the user to update
  //  all the records with a single click.
  //
  //  The derived class is responsible for all details of the multi-move
  //  handling.
  //
  public void handleMultiMove( )
  {
  }
  
  protected FieldGroup initMultiMoveFields( )
  {
    FieldGroup        fgroup      = null;
    
    try
    {
      try
      {
        fgroup = (FieldGroup)fields.getField( "multiMoveFields" );
        fgroup.removeAllFields();   // clear all existing fields
      }
      catch( Exception e )
      {
        // ignore
      }      
    
      if ( fgroup == null )
      {
        // create a new field group for the search
        fgroup = new FieldGroup("multiMoveFields");
        fields.add( fgroup );
      }
    
      // add the processing flag and all checkbox
      fgroup.add( new HiddenField("multiMove") );
      fgroup.getField("multiMove").setData("true");
    
      fgroup.add( new CheckboxField("multiMoveAll",(getMultiMoveColumnHeader() + " All Items"), false) );
    }
    catch( Exception e )
    {
      logEntry("initMultiMoveFields()",e.toString());
    }
    finally
    {
    }
    return( fgroup );
  }
  
  protected FieldGroup initSearchFields( )
  {
    FieldGroup        retVal      = null;
    
    try
    {
      retVal = (FieldGroup)fields.getField( "searchFields" );
      retVal.removeAllFields();   // clear all existing fields
    }
    catch( Exception e )
    {
      // ignore
    }      
    
    if ( retVal == null )
    {
      // create a new field group for the search
      retVal = new FieldGroup("searchFields");
      fields.add( retVal );
    }
    return( retVal );
  }
  
  public void insertNote( long itemId, String noteText )
  {
    int         itemType      = 0;
    
    try
    {
      connect();
      
      if ( noteText != null && !noteText.equals("") )
      {
        /*@lineinfo:generated-code*//*@lineinfo:711^9*/

//  ************************************************************
//  #sql [Ctx] { select  qt.item_type    
//            from    q_types     qt
//            where   qt.type = :this.getType()
//            
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_513 = this.getType();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  qt.item_type     \n          from    q_types     qt\n          where   qt.type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.queues.QueueBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,__sJT_513);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:717^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:719^9*/

//  ************************************************************
//  #sql [Ctx] { insert into q_notes
//            (
//              id,
//              type,
//              item_type,
//              note_source,
//              note
//            )
//            values
//            (
//              :itemId, 
//              :this.getType(),
//              :itemType,
//              :this.user.getLoginName(),
//              substr(:noteText,1,500)
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_514 = this.getType();
 String __sJT_515 = this.user.getLoginName();
   String theSqlTS = "insert into q_notes\n          (\n            id,\n            type,\n            item_type,\n            note_source,\n            note\n          )\n          values\n          (\n             :1 , \n             :2 ,\n             :3 ,\n             :4 ,\n            substr( :5 ,1,500)\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.queues.QueueBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   __sJT_st.setInt(2,__sJT_514);
   __sJT_st.setInt(3,itemType);
   __sJT_st.setString(4,__sJT_515);
   __sJT_st.setString(5,noteText);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:737^9*/
      }
    }
    catch( Exception e )
    {
      logEntry( "insertNote(" + String.valueOf(itemId) + ")", e.toString() );
    }      
    finally
    {
      cleanUp();
    }
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case mesConstants.FF_CSV:      // all queues support CSV download (page and full)
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public boolean isMultiMoveEnabled()
  {
    return( (fields.getField("multiMoveFields") != null) );
  }
  
  public boolean isSearchEnabled()
  {
    return( (fields.getField("searchFields") != null) );
  }
  
  public final void loadData(UserBean user)
  {
    try
    {
      // unlock all queue entries for this user
      QueueTools.releaseLock(user);

      connect();

      extraColumns = new ExtraColumnBean();

      loadExtraColumns(extraColumns, user);

      loadQueueData(user);
      
      if(it != null)
      {
        ResultSet rs = it.getResultSet();

        while(rs.next())
        {
          QueueData qd = getEmptyQueueData();
          qd.setData(rs);
          extraColumns.setExtraColumnData(rs);
          rows.add(qd);
        }

        it.close();
      }

      loadQueuePages(MesDefaults.getInt(MesDefaults.DK_MAX_QUEUE_DISPLAY));
    }
    catch(Exception e)
    {
      logEntry("loadData(" + user.getHierarchyNode() + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** METHOD loadItem
  **
  ** Loads a single queue item into the queueItem data structure.  Extending
  ** classes must overload the method loadQueueItem() in order to make this work.
  */
  public final void loadItem(long id)
  {
    try
    {
      connect();

      loadQueueItem(id);

      if(it != null)
      {
        ResultSet rs = it.getResultSet();

        if(rs.next())
        {
          queueItem = getEmptyQueueData();
          queueItem.setData(rs);
        }

        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("loadItem(" + this.type + ", " + id + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public final void loadQueuePages(int span)
  {
    try
    {
      if(rows != null && span > 0)
      {
        int numPages = rows.size() / span;
        if(rows.size() % span > 0)
        {
          ++numPages;
        }

        numPages = (numPages == 0) ? 1 : numPages;

        for (int i=0; i < numPages; ++i)
        {
          int beginVal = span * i;
          int endVal = (beginVal + span) > rows.size() ? rows.size() : beginVal + span;

          QueuePageData qpd = new QueuePageData(beginVal,
                                                i+1,
                                                beginVal + 1,
                                                endVal,
                                                span);
          pages.add(qpd);
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setQueuePages("+ span + ")", e.toString());
    }
  }
  
  /***************************************************************************/
  /*  METHODS THAT MUST BE OVERRIDDEN BY EXTENDING CLASS                     */
  /***************************************************************************/
  protected void loadQueueData(UserBean user)
  {
    // used to load the contents of the queue (including extra columns)
  }

  protected void loadExtraColumns(ExtraColumnBean ecb, UserBean user)
  {
    // used to load the extra column bean with header names, etc.

    // Essentially, this method needs to call ExtraColumnBean.addColumn once
    // for each extra column that must be displayed on the queue page
  }

  protected void loadQueueItem(long id)
  {
    // used to load the data for a single queue item
  }
  
  public boolean isSubmitAllowed(UserBean user)
  {
    return( true );
  }
  
  public void setFields( HttpServletRequest request )
  {
    super.setFields( request );
    
    SearchSubmitted   = HttpHelper.getBoolean(request,"searchSubmit",false);
    CurrentPageBegin  = HttpHelper.getInt(request, "begin", 0);
    
    // if the queue has multi-move and the user
    // has pressed the submit button then move 
    // the items to the destination queue.
    if( isMultiMoveEnabled() == true &&
        HttpHelper.getBoolean(request,"multiMove",false) == true )
    {
      handleMultiMove();    // move the items
    }
    
    if ( SearchSubmitted )
    { 
      FieldGroup searchFields   = getSearchFields();
      
      if ( searchFields != null )
      {
        // force validation
        searchFields.isValid();
      }
    }
  }    

  public void setType(String type)
  {
    try
    {
      setType(Integer.parseInt(type));
    }
    catch(Exception e)
    {
      logEntry("setType(" + type + ")", e.toString());
    }
  }
  
  public void setType(int type)
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;

    this.type = type;

    // get queue information from table Q_TYPES
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:963^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  description,
//                  function_url,
//                  item_type,
//                  required_right
//          from    q_types
//          where   type = :type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  description,\n                function_url,\n                item_type,\n                required_right\n        from    q_types\n        where   type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.queues.QueueBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,type);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.queues.QueueBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:971^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.description  = rs.getString("description");
        this.functionURL  = rs.getString("function_url");
        this.itemType     = rs.getInt("item_type");
        
        // scan the required rights to determine if 
        // user is allowed to access this queue
        if (user != null)
        {
          userAllowed = false;
          StringTokenizer tok 
            = new StringTokenizer(rs.getString("required_right"),",");
          while (tok.hasMoreTokens() && !userAllowed)
          {
            try
            {
              userAllowed = user.hasRight(Long.parseLong(tok.nextToken()));
            }
            catch (Exception e)
            {
              logEntry("setType(" + type +"), rights check",e.toString());
            }
          }
        }
        else
        {
          userAllowed = true;
        }
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("setType(" + type + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/