/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/CbtDocumentQueue.java $

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 3/05/03 6:50p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.user.UserBean;

public class CbtDocumentQueue extends CbtCreditQueue
{
  /*
  ** public int getMenuId()
  **
  ** Overrides CbtCreditQueue method to return a documentation queue
  ** menu id for use in back links and descriptions.
  */
  public int getMenuId()
  {
    return MesMenus.MENU_ID_CBT_DOCUMENT_QUEUES;
  }
  
  /*
  ** public void createQueueItem(long id)
  **
  ** Create a documentation queue item based on the required doc codes in the
  ** corresponding q_credit_data codes field.
  */
  public void createQueueItem(long id, UserBean user)
  {
      // make sure there isn't a queue item for this id in the
      // completed queue
      QueueTools.removeQueue(id,MesQueues.Q_CBT_DOC_COMPLETE);
    
      // create a new working item
      QueueTools.insertQueue(id,MesQueues.Q_CBT_DOC_WORKING);
  }

  /*
  ** public static void insertIntoQueue(long id)
  **
  ** Convenience static wrapper to insert a new doc item into the working queue.
  */
  public static void insertIntoQueue(long id, UserBean user)
  {
    CbtDocumentQueue dq = new CbtDocumentQueue();
    dq.createQueueItem(id,user);
  }
  
  /*
  ** public void deleteQueueItem(long id)
  **
  ** Deletes a queue item and all corresponding q_doc_data records.
  */
  public void deleteQueueItem(long id, UserBean user)
  {
    // remove actual queue item from completed/working queue
    QueueTools.removeQueue(id,MesQueues.Q_CBT_DOC_WORKING,user);
    QueueTools.removeQueue(id,MesQueues.Q_CBT_DOC_COMPLETE,user);
  }
  
  /*
  ** public static void removeFromQueue(long id)
  **
  ** Convenience static wrapper to remove a doc item from the working or completed
  ** queues.
  */
  public static void removeFromQueue(long id, UserBean user)
  {
    CbtDocumentQueue dq = new CbtDocumentQueue();
    dq.deleteQueueItem(id,user);
  }
}
