/*************************************************************************

  FILE: $URL: $

  Description:

  B2FSingleton

  Singleton that provides easy access to the shared B2F instance

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-02 16:35:14 -0800 (Fri, 02 Mar 2007) $
  Version            : $Revision: 13506 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tricipher;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.mes.config.MesDefaults;
import com.mes.support.HttpHelper;
import com.tricipher.B2F.B2FConfiguration;
import com.tricipher.B2F.B2FCryptoProvider;
import com.tricipher.B2F.B2FCryptoProviderImplJDK14;
import com.tricipher.B2F.B2FRandomNumberGenerator;
import com.tricipher.B2F.B2FRandomNumberGeneratorImpl;

public class B2FConfigInvoker implements ServletContextListener
{
  private static Log log = LogFactory.getLog(B2FConfigInvoker.class);
  
  /*
  ** public void contextInitialized(ServletContextEvent contextEvent)
  **
  ** This method is called at application initialization and configures
  ** the B2FConfiguration object 
  */
  public void contextInitialized(ServletContextEvent contextEvent)
  {
    log.debug("B2FConfigInvoker started with application startup.");
    
    // DISABLED to use MES-driven B2F rather than TriCipher.    
    if( HttpHelper.doesUseB2F(null) && false )
    {
      B2FConfiguration config = new B2FConfiguration();
    
      try
      {
        log.debug("Creating B2FConfiguration object from MESDefault parameters");
      
        B2FCryptoProvider crypto = new B2FCryptoProviderImplJDK14();
        B2FRandomNumberGenerator rng = new B2FRandomNumberGeneratorImpl();
      
        String[] appliances = { MesDefaults.getString(MesDefaults.DK_TRICIPHER_APPLIANCE_ADDR) };
      
        config.setAppliance(appliances);
        config.setManagerPort((short) (MesDefaults.getInt(MesDefaults.DK_TRICIPHER_APPLIANCE_PORT)));
        config.setUserPort((short) (MesDefaults.getInt(MesDefaults.DK_TRICIPHER_APPLIANCE_PORT)));
        config.setB2fManagerLogin(MesDefaults.getString(MesDefaults.DK_TRICIPHER_B2F_USER));
        config.setB2fManagerPassword(MesDefaults.getString(MesDefaults.DK_TRICIPHER_B2F_PASSWORD));
        config.setB2fFieldName(MesDefaults.getString(MesDefaults.DK_TRICIPHER_B2F_FIELD_NAME));
        config.setB2fUserFieldName(MesDefaults.getString(MesDefaults.DK_TRICIPHER_B2F_USER_FIELD_NAME));
        config.setCookiePrefix(MesDefaults.getString(MesDefaults.DK_TRICIPHER_B2F_COOKIE_PREFIX));
      
        config.setB2fFieldSigningKey(MesDefaults.getString(MesDefaults.DK_TRICIPHER_B2F_SIGNING_KEY));
        config.setB2fFieldSigningCert(MesDefaults.getString(MesDefaults.DK_TRICIPHER_B2F_SIGNING_CERT));
        config.setEncryption(crypto);
        config.setRng(rng);
        config.setCsrManagerUserId(MesDefaults.getString(MesDefaults.DK_TRICIPHER_B2F_CSR_USER));
        config.setCsrManagerPassword(MesDefaults.getString(MesDefaults.DK_TRICIPHER_B2F_CSR_PASSWORD));
        config.setMaintainB2fManagerSession(true);
      }
      catch(Exception e)
      {
        log.error("Unable to build configuration: " + e.toString());
      }
    
      if(!config.isValid())
      {
        log.error("missing or malformed default parameters in MES_DEFAULTS.");
      }
      else
      {
        // initialize singleton
        B2FSingleton.setB2fConfiguration(config);
        log.debug("finished configuring application");
      }
    }
  }
  
  public void contextDestroyed(ServletContextEvent arg0)
  {
    log.debug("Shutting down B2FConfigInvoker");
  }
  
}