/*************************************************************************

  FILE: $URL: $

  Description:

  B2FSingleton

  Singleton that provides easy access to the shared B2F instance

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-02 16:35:14 -0800 (Fri, 02 Mar 2007) $
  Version            : $Revision: 13506 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tricipher;

import org.apache.log4j.Logger;
import com.tricipher.B2F.B2F;
import com.tricipher.B2F.B2FConfiguration;
import com.tricipher.B2F.B2FException;
import com.tricipher.B2F.B2FInitializationException;

public class B2FSingleton
{
  // actual singleton instance
  private static B2FSingleton       _instance     = null;
  private static B2F                b2f_instance  = null;
  private static B2FConfiguration   config        = null;
  private static String             realm         = "";
  
  protected boolean initialized                   = false;
  
  static Logger log = Logger.getLogger(B2FSingleton.class);
  
  
  public B2FSingleton()
  {
    //realm = HttpHelper.getB2fRealm(null);
    log.debug("Realm = " + realm);
    initialized = true;
  }
  
  public static String getRealm()
  {
    return realm;
  }
  
  public static B2F getB2fInstance()
    throws B2FInitializationException
  {
    try
    {
      if(b2f_instance == null)
      {
        // create singleton
        _instance = new B2FSingleton();
        
        // create B2FConfig object based on defaults in MESDefaults
        log.debug("creating b2f_instance");
        b2f_instance = new B2F(config);
        log.debug("b2f_instance successfully created");
      }
    }
    catch(B2FException e)
    {
      log.error("Unable to get B2FInstance: " + e.toString());
      throw new B2FInitializationException("Unable to get B2FInstance: " + e.toString());
    }
    catch(Exception e)
    {
      log.error("Unable to get B2FInstance: " + e.toString());
      com.mes.support.SyncLog.LogEntry("B2FSingleton::getB2fInstance(): " + e.toString());
    }
    catch(Error er)
    {
      er.printStackTrace(System.out);
      log.error("Unable to get B2FInstance: " + er.toString());
    }
    
    if(_instance.initialized == false)
    {
      throw new B2FInitializationException("Unable to get b2f_instance");
    }
    else
    {
      return b2f_instance;
    }
  }
  
  public static void setB2fConfiguration(B2FConfiguration config)
  {
    B2FSingleton.config = config;
  }
  
  public static void destroy()
  {
    // kill singletons
    _instance = null;
    b2f_instance = null;
  }
}