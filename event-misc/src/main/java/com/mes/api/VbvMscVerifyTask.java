/*@lineinfo:filename=VbvMscVerifyTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/VbvMscVerifyTask.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import com.cardinalcommerce.client.CentinelRequest;
import com.cardinalcommerce.client.CentinelResponse;

public class VbvMscVerifyTask extends TridentApiTransactionTask
{
  public VbvMscVerifyTask( TridentApiTransaction tran )
  {
    super(tran);
  }
  
  public boolean doTask()
  {
    CentinelRequest     centinelRequest   = null;
    CentinelResponse    centinelResponse  = null;
    boolean             retVal            = false;
    
    try
    {
      connect();
      
      // build the Cardinal Centinel request to determine if 
      // this cardholder is eligible for Verified By Visa (vbv)
      // or MasterCard Secure Code (msc)
      //
      centinelRequest = new CentinelRequest();
      
      // required fields
	    centinelRequest.add("Version",          TridentApiConstants.CENTINEL_MESSAGE_VERSION);
	    centinelRequest.add("MsgType",          "cmpi_authenticate");
	    centinelRequest.add("ProcessorId",      TridentApiConstants.CENTINEL_PROCESSOR_ID);
	    centinelRequest.add("MerchantId",       String.valueOf(Transaction.getMerchantId()));
	    centinelRequest.add("TransactionPwd",	  Transaction.getProfileKey());
		  centinelRequest.add("TransactionId",    Transaction.getTranId3D());
	    centinelRequest.add("TransactionType",	"C");
		  centinelRequest.add("PAResPayload",     Transaction.getPayload3D());
      
      // post the request to Cardinal Commerce 
    	centinelResponse = centinelRequest.sendHTTP( Transaction.getCardinalCentinelUrl(), TridentApiConstants.CENTINEL_TIMEOUT_CONNECT, TridentApiConstants.CENTINEL_TIMEOUT_READ);
      
      // process the Cardinal Centinel response
      String errorCode = centinelResponse.getValue("ErrorNo");
      if ( errorCode.equals( TridentApiConstants.CENTINEL_ERROR_NONE ) )
      {
        String cardType = Transaction.getCardType();
        
        if ( cardType.equals("VS") )
        {
          Transaction.setVisaCavv( centinelResponse.getValue("Cavv") );
          Transaction.setVisaXid( centinelResponse.getValue("Xid") );
          Transaction.setMotoEcommIndicator( centinelResponse.getValue("EciFlag") );
        }
        else if ( cardType.equals("MC") )
        {
          String eciFlag = centinelResponse.getValue("EciFlag");
          
          Transaction.setUcafCollectionInd( eciFlag );
          Transaction.setUcafAuthData( centinelResponse.getValue("Cavv") );
          
          if ( eciFlag.equals("02") )
          {
            Transaction.setMotoEcommIndicator( TridentApiConstants.FV_MOTO_SECURE_ECOMMERCE );
          }
          else if ( eciFlag.equals("01") )
          {
            Transaction.setMotoEcommIndicator( TridentApiConstants.FV_MOTO_NON_AUTH_SECURE_ECOMMERCE );
          }
          else
          {
            Transaction.setMotoEcommIndicator( TridentApiConstants.FV_MOTO_ECOMMERCE );
          }
        }          
        retVal = true;
      }
      else  // return the centinel error code and description
      {
        setErrorCode( TridentApiConstants.ER_3D_VERIFY_FAILED );
        setErrorDesc( centinelResponse.getValue("ErrorDesc") + " (" + errorCode + ")" );
      }
    }
    catch( Exception e )
    {
      setErrorCode(TridentApiConstants.ER_3D_VERIFY_FAILED);
      logEntry("doTask()",e.toString());
    }
    finally
    {
      cleanUp(); 
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/