/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/api/ApiStipEntry.java $

  Description:
  
  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2010-10-27 14:55:23 -0700 (Wed, 27 Oct 2010) $
  Version            : $Revision: 18053 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.io.Serializable;

public class ApiStipEntry
  implements Serializable
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;

  private   long        MerchantId          = 0L;
  private   String      ProfileId           = null;
  private   String      RequestParams       = null;
  private   String      ServerName          = "Unknown";
  private   String      TridentTranId       = null;
  
  
  public ApiStipEntry( TridentApiTransaction apiTran, String requestParams, String serverName )
  {
    MerchantId      = apiTran.getMerchantId();
    ProfileId       = apiTran.getProfileId();
    TridentTranId   = apiTran.getTridentTranId();
    RequestParams   = requestParams;
    ServerName      = serverName;
  }
  
  public long       getMerchantId()         { return( MerchantId );     }
  public String     getProfileId()          { return( ProfileId );      }
  public String     getRequestParams()      { return( RequestParams );  }
  public String     getServerName()         { return( ServerName );     }
  public String     getTridentTranId()      { return( TridentTranId );  }
 
  public void showData( )
  {
    System.out.println("MerchantId        : " + MerchantId);
    System.out.println("ProfileId         : " + ProfileId);
    System.out.println("RequestParams     : " + RequestParams);
    System.out.println("ServerName        : " + ServerName);
    System.out.println("TridentTranId     : " + TridentTranId);
  }
}