/*@lineinfo:filename=OriginalTranLoadTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/api/OriginalTranLoadTask.sqlj $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2011-09-20 13:32:22 -0700 (Tue, 20 Sep 2011) $
  Version            : $Revision: 19270 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.sql.ResultSet;
import com.mes.config.MesDefaults;
import sqlj.runtime.ResultSetIterator;

public class OriginalTranLoadTask extends TridentApiTransactionTask
{
  protected double    OriginalAuthAmount  = 0.0; 
  protected String    OriginalAuthCode    = null;
  protected int       ReauthDays          = 30;
  
  public OriginalTranLoadTask( TridentApiTransaction tran )
  {
    super(tran);
    try { ReauthDays = MesDefaults.getInt(MesDefaults.DK_PG_REAUTH_DAYS); } catch (Exception e){}
  }
  
  public boolean doTask()
  {
    boolean retVal = false;
    
    if( TridentApiTranBase.TT_RE_AUTH.equals( Transaction.getTranType() ) )
    {
      retVal = loadOriginalAuth(); 
    }
    else
    {
      retVal = loadOriginalSale();
    }
    return( retVal );
  }
  
  public double getOriginalAuthAmount()
  {
    return( OriginalAuthAmount );
  }
  
  public String getOriginalAuthCode()
  {
    return( OriginalAuthCode );
  }
  
  private boolean loadOriginalAuth()
  {
    double              amountAvailable = 0.0;
    ResultSetIterator   it              = null;
    String              profileId       = null;
    ResultSet           resultSet       = null;
    boolean             retVal          = false;
    double              tranAmount      = 0.0;
    String              tranId          = null;
    String              testFlag        = Transaction.getTestFlag();
    
    try
    {
      connect();
      
      tranId    = Transaction.getOriginalTranId();
      profileId = Transaction.getProfileId();
      
      /*@lineinfo:generated-code*//*@lineinfo:86^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+ index(al tapi_access_log_tran_id) */
//                    al.merchant_number          as merchant_number,
//                    al.transaction_type         as tran_type,
//                    extract_api_request_param(al.request_params,'invoice_number',17)
//                                                as purchase_id,
//                    extract_api_request_param(al.request_params,'client_reference_number',96)
//                                                as client_ref_num,
//                    auth.authorized_amount      as auth_amount,
//                    auth.authorization_code     as auth_code,
//                    auth.avs_street             as avs_street,
//                    auth.avs_zip                as avs_zip,
//                    dukpt_decrypt_wrapper(auth.card_number_enc)
//                                                as card_number_full,
//                    auth.currency_code          as currency_code,
//                    auth.developer_id           as developer_id,
//                    (
//                      substr(auth.expiration_date_incoming,3,2) ||
//                      substr(auth.expiration_date_incoming,1,2)
//                    )                           as exp_date_mmyy,
//                    auth.moto_ecommerce_indicator 
//                                                as moto_ecomm_ind,
//                    auth.response_code          as response_code,
//                    auth.terminal_id            as terminal_id,
//                    nvl(:testFlag,'Y')          as test_flag
//            from    trident_api_access_log     al,
//                    tc33_trident               auth
//            where   al.trident_tran_id = :tranId 
//                    and al.profile_id = :profileId
//                    and al.request_time between (sysdate-:ReauthDays) and sysdate
//                    and al.transaction_type in ('P','D')
//                    and auth.merchant_number = al.merchant_number
//                    and auth.transaction_date = al.request_date
//                    and auth.trident_transaction_id = al.trident_tran_id
//                    and auth.response_code != '00'
//                    and auth.developer_id = 'TRIAPI'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    /*+ index(al tapi_access_log_tran_id) */\n                  al.merchant_number          as merchant_number,\n                  al.transaction_type         as tran_type,\n                  extract_api_request_param(al.request_params,'invoice_number',17)\n                                              as purchase_id,\n                  extract_api_request_param(al.request_params,'client_reference_number',96)\n                                              as client_ref_num,\n                  auth.authorized_amount      as auth_amount,\n                  auth.authorization_code     as auth_code,\n                  auth.avs_street             as avs_street,\n                  auth.avs_zip                as avs_zip,\n                  dukpt_decrypt_wrapper(auth.card_number_enc)\n                                              as card_number_full,\n                  auth.currency_code          as currency_code,\n                  auth.developer_id           as developer_id,\n                  (\n                    substr(auth.expiration_date_incoming,3,2) ||\n                    substr(auth.expiration_date_incoming,1,2)\n                  )                           as exp_date_mmyy,\n                  auth.moto_ecommerce_indicator \n                                              as moto_ecomm_ind,\n                  auth.response_code          as response_code,\n                  auth.terminal_id            as terminal_id,\n                  nvl( :1 ,'Y')          as test_flag\n          from    trident_api_access_log     al,\n                  tc33_trident               auth\n          where   al.trident_tran_id =  :2  \n                  and al.profile_id =  :3 \n                  and al.request_time between (sysdate- :4 ) and sysdate\n                  and al.transaction_type in ('P','D')\n                  and auth.merchant_number = al.merchant_number\n                  and auth.transaction_date = al.request_date\n                  and auth.trident_transaction_id = al.trident_tran_id\n                  and auth.response_code != '00'\n                  and auth.developer_id = 'TRIAPI'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.api.OriginalTranLoadTask",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,testFlag);
   __sJT_st.setString(2,tranId);
   __sJT_st.setString(3,profileId);
   __sJT_st.setInt(4,ReauthDays);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.api.OriginalTranLoadTask",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:123^7*/
      resultSet = it.getResultSet();

      if ( resultSet.next() )
      {
        tranAmount = resultSet.getDouble("auth_amount");
        setTransactionFields(resultSet);
      }
      else
      {
        setError(TridentApiConstants.ER_INVALID_TRAN_ID);
      }
      resultSet.close();
      it.close();
      
      // if there are no errors, validate the amount 
      if( getErrorCode().equals(TridentApiConstants.ER_NONE) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:141^9*/

//  ************************************************************
//  #sql [Ctx] { select  (:tranAmount - nvl( sum( auth_amount ),0 )) 
//            
//            from    trident_capture_api tapi
//            where   tapi.original_trident_tran_id = :tranId and
//                    tapi.transaction_type in ('P', 'D')
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ( :1  - nvl( sum( auth_amount ),0 )) \n           \n          from    trident_capture_api tapi\n          where   tapi.original_trident_tran_id =  :2  and\n                  tapi.transaction_type in ('P', 'D')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.api.OriginalTranLoadTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDouble(1,tranAmount);
   __sJT_st.setString(2,tranId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amountAvailable = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:148^9*/
        
        if( amountAvailable <= 0 )
        {
          setError(TridentApiConstants.ER_REAUTH_NOT_ELIGIBLE);
        }
        else if( amountAvailable < Transaction.getTranAmount() )
        {
          setError(TridentApiConstants.ER_INVALID_AMOUNT);
        }
        
        if( Transaction.getTranAmount() == 0.0 )
        {
          Transaction.setTranAmount(amountAvailable);
        }
      }
      retVal = getErrorCode().equals(TridentApiConstants.ER_NONE);
    }
    catch( Exception e )
    {
      setError(TridentApiConstants.ER_ORIGINAL_AUTH_LOAD_FAILED);
      logEntry("loadOriginalAuth(" + tranId + "," + profileId + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }
  
  private boolean loadOriginalSale()
  {
    ResultSetIterator   it                = null;
    String              profileId         = null;
    ResultSet           resultSet         = null;
    boolean             retVal            = false;
    String              tranId            = null;
    
    try
    {
      connect();
      
      tranId    = Transaction.getOriginalTranId();
      profileId = Transaction.getProfileId();
      
      /*@lineinfo:generated-code*//*@lineinfo:194^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tapi.auth_amount              as auth_amount,
//                  tapi.auth_code                as auth_code,
//                  tapi.transaction_amount       as tran_amount,
//                  tapi.transaction_type         as tran_type,
//                  dukpt_decrypt_wrapper(tapi.card_number_enc)
//                                                as card_number_full,
//                  tapi.exp_date                 as exp_date_mmyy,
//                  auth.avs_street               as avs_street,
//                  tapi.avs_zip                  as avs_zip,
//                  nvl(tapi.test_flag,'Y')       as test_flag,
//                  tapi.purchase_id              as purchase_id,
//                  tapi.moto_ecommerce_ind       as moto_ecomm_ind,
//                  tapi.client_reference_number  as client_ref_num,
//                  tapi.currency_code            as currency_code,
//                  tapi.transaction_type         as tran_type,
//                  decode(nvl(tapi.fx_rate_id,0),0,'N','Y') 
//                                                as fx_tran
//          from    trident_capture_api     tapi,
//                  tc33_trident            auth
//          where   tapi.trident_tran_id = :tranId 
//                  and tapi.terminal_id = :profileId
//                  and tapi.mesdb_timestamp between (sysdate-:ReauthDays) and sysdate
//                  and tapi.transaction_type = :TridentApiTransaction.TT_DEBIT 
//                  and nvl(tapi.test_flag,'Y') = :Transaction.getTestFlag()
//                  and auth.rec_id(+) = tapi.auth_rec_id
//                  and auth.transaction_date(+) between nvl(tapi.auth_date,tapi.transaction_date)-1 and
//                                                       nvl(tapi.auth_date,tapi.transaction_date)+1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_141 = Transaction.getTestFlag();
  try {
   String theSqlTS = "select  tapi.auth_amount              as auth_amount,\n                tapi.auth_code                as auth_code,\n                tapi.transaction_amount       as tran_amount,\n                tapi.transaction_type         as tran_type,\n                dukpt_decrypt_wrapper(tapi.card_number_enc)\n                                              as card_number_full,\n                tapi.exp_date                 as exp_date_mmyy,\n                auth.avs_street               as avs_street,\n                tapi.avs_zip                  as avs_zip,\n                nvl(tapi.test_flag,'Y')       as test_flag,\n                tapi.purchase_id              as purchase_id,\n                tapi.moto_ecommerce_ind       as moto_ecomm_ind,\n                tapi.client_reference_number  as client_ref_num,\n                tapi.currency_code            as currency_code,\n                tapi.transaction_type         as tran_type,\n                decode(nvl(tapi.fx_rate_id,0),0,'N','Y') \n                                              as fx_tran\n        from    trident_capture_api     tapi,\n                tc33_trident            auth\n        where   tapi.trident_tran_id =  :1  \n                and tapi.terminal_id =  :2 \n                and tapi.mesdb_timestamp between (sysdate- :3 ) and sysdate\n                and tapi.transaction_type =  :4  \n                and nvl(tapi.test_flag,'Y') =  :5 \n                and auth.rec_id(+) = tapi.auth_rec_id\n                and auth.transaction_date(+) between nvl(tapi.auth_date,tapi.transaction_date)-1 and\n                                                     nvl(tapi.auth_date,tapi.transaction_date)+1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.api.OriginalTranLoadTask",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,tranId);
   __sJT_st.setString(2,profileId);
   __sJT_st.setInt(3,ReauthDays);
   __sJT_st.setString(4,TridentApiTransaction.TT_DEBIT);
   __sJT_st.setString(5,__sJT_141);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.api.OriginalTranLoadTask",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:223^7*/
      resultSet = it.getResultSet();

      if ( resultSet.next() )
      {
        if ( "Y".equals(resultSet.getString("fx_tran")) )
        {
          setError(TridentApiConstants.ER_FX_MULTI_SETTLE_NOT_ALLOWED);
        }
        else
        {
          setTransactionFields(resultSet);
        }
      }
      else
      {
        setError(TridentApiConstants.ER_INVALID_TRAN_ID);
      }
      resultSet.close();
      it.close();
      
      retVal = getErrorCode().equals(TridentApiConstants.ER_NONE);
    }
    catch( Exception e )
    {
      setError(TridentApiConstants.ER_ORIGINAL_TRAN_LOAD_FAILED);
      logEntry("loadOriginalSale(" + tranId + "," + profileId + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }
  
  protected void setTransactionFields( ResultSet resultSet )
    throws java.sql.SQLException
  {
    String        value       = null;
    
    // store the original auth code in case a multi-capture
    // needs to force the transaction as offline
    OriginalAuthCode    = resultSet.getString("auth_code");
    OriginalAuthAmount  = resultSet.getDouble("auth_amount");
    
    // set the fields in the current transaction
    Transaction.setOriginalTranId     ( Transaction.getTridentTranId()        );
    Transaction.setAvsStreet          ( resultSet.getString("avs_street")     );
    Transaction.setAvsZip             ( resultSet.getString("avs_zip")        );
    Transaction.setExpDate            ( resultSet.getString("exp_date_mmyy")  );
    Transaction.setCardNumberFull     ( resultSet.getString("card_number_full") );
    Transaction.setTransactionType    ( resultSet.getString("tran_type")      );
    Transaction.setCurrencyCode       ( resultSet.getString("currency_code")  );
    Transaction.setTestFlag           ( resultSet.getString("test_flag")      );
    
    value = resultSet.getString("moto_ecomm_ind");
    if ( "56".indexOf(value) >= 0 ) 
    {
      value = "7";    // do not have the 3d secure data, convert to std ecomm
    }
    Transaction.setMotoEcommIndicator ( value );
    
    if( Transaction.isBlank(Transaction.getPurchaseId()) )
    {
      Transaction.setPurchaseId( resultSet.getString("purchase_id") );
    }
  
    if( Transaction.isBlank(Transaction.getClientReferenceNumber()) )
    {
      Transaction.setClientReferenceNumber( resultSet.getString("client_ref_num") );
    }
  }
}/*@lineinfo:generated-code*/