/*@lineinfo:filename=RateConvertTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/RateConvertTask.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

public class RateConvertTask extends TridentApiTransactionTask
{
  public RateConvertTask( TridentApiTransaction tran )
  {
    super(tran);
  }
  
  public boolean doTask()
  {
    String    currencyCode  = null;
    boolean   retVal        = false;
    
    try
    {
      connect();
      
      currencyCode = Transaction.getCurrencyCodeAlpha();
      
      if ( currencyCode == null || currencyCode.equals("") ||
           currencyCode.equals(TridentApiConstants.FV_CURRENCY_CODE_ALPHA_DEFAULT) )
      {
        setErrorCode(TridentApiConstants.ER_CURRENCY_NOT_ACCEPTED);
      }
      else
      {
        RateTable rateTable = new RateTable( Transaction.getMerchantId() );
        ForeignExchangeRate fxRate = rateTable.getRateByCurrencyCode(currencyCode);
        
        if ( fxRate == null || !fxRate.isValid() )
        {
          setErrorCode(TridentApiConstants.ER_INVALID_RATE_ID);
        }
        else if ( fxRate.isExpired() )
        {
          setErrorCode(TridentApiConstants.ER_RATE_TABLE_EXPIRED);
        }
        else
        {
          Transaction.setTranAmount( fxRate.convertAmount(Transaction.getFxAmount()) );
          Transaction.setFxRate( fxRate );
          retVal = true;
        }
      }        
    }
    catch( Exception e )
    {
      setErrorCode(TridentApiConstants.ER_FX_RATE_LOOKUP_FAILED);
      logEntry("doTask()",e.toString());
    }
    finally
    {
      cleanUp(); 
    }
    
    return( retVal );
  }
}/*@lineinfo:generated-code*/