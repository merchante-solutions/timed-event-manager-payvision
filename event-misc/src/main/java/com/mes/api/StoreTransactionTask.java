/*@lineinfo:filename=StoreTransactionTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/StoreTransactionTask.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import com.mes.support.SyncLog;
import oracle.sql.BLOB;

public class StoreTransactionTask extends TridentApiTransactionTask
{
  public StoreTransactionTask( TridentApiTransaction tran )
  {
    super(tran);
  }
  
  public boolean doTask()
  {
    BLOB                    blobData          = null;
    OutputStream            bOut              = null;
    String                  cardNumberFull    = null;
    String                  cardType          = null;
    int                     createBlob        = 0;
    double                  rate              = 0.0;
    long                    recId             = 0L;
    String                  reimbursementAttr = null;
    String[]                responseFields    = new String[8];
    boolean                 retVal            = false;
    String                  testFlag          = "N";
    String                  tranType          = null;
    byte[]                  xmlBytes          = null;
    String                  batchId           = null;
    String                  batchNumber       = null;
    
    try
    {
      connect();
      setAutoCommit(false);   // disable auto-commit
      testFlag = ApiDb.loadTestFlag(ProdFlagDefault);
      tranType = Transaction.getTranType();
      
      // set the response fields to null
      for( int i = 0; i < responseFields.length; ++i )
      {
        responseFields[i] = null;
      }
      
      // get a new rec id.  required to store blob
      /*@lineinfo:generated-code*//*@lineinfo:69^7*/

//  ************************************************************
//  #sql [Ctx] { select  trident_capture_api_sequence.nextval 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trident_capture_api_sequence.nextval  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.api.StoreTransactionTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:73^7*/
      
      if ( Transaction.VisaDResponse != null )
      {
        responseFields[0] = Transaction.VisaDResponse.getApprovalCode();
        responseFields[1] = Transaction.VisaDResponse.getResponseCode();
        responseFields[2] = Transaction.VisaDResponse.getRetrievalReferenceNumber();
        responseFields[3] = Transaction.VisaDResponse.getTransactionIdentifier();
        responseFields[4] = Transaction.VisaDResponse.getValidationCode();
        responseFields[5] = Transaction.VisaDResponse.getAuthorizationSourceCode();
        responseFields[6] = Transaction.VisaDResponse.getAVSResultCode();
        responseFields[7] = Transaction.VisaDResponse.getReturnedACI();
        createBlob        = 1;
      }
      else if ( tranType.equals( Transaction.TT_FORCE ) )
      {
        // allow forced items
        responseFields[0] = Transaction.getAuthCode();
        responseFields[5] = TridentApiConstants.AUTH_SRC_OFFLINE_MANUAL_ENTRY;
      }
      
      if ( Transaction.isBatchRequest() )
      {
        testFlag = Transaction.getTestFlag();
        responseFields[0] = Transaction.getAuthCode();
        responseFields[1] = Transaction.getAuthResponseCode();
        responseFields[2] = Transaction.getAuthRefNum();
        responseFields[3] = Transaction.getAuthTranId();
        responseFields[4] = Transaction.getAuthValCode();
        responseFields[5] = Transaction.getAuthSourceCode();
        responseFields[6] = Transaction.getAuthAvsResponse();
        responseFields[7] = Transaction.getReturnedAci();
      }
      else
      {
        Transaction.setAuthAmount(Transaction.getTranAmount());
      }

      ForeignExchangeRate fxRate = Transaction.getFxRate();
      if ( fxRate != null && fxRate.isValid() )
      {
        rate = fxRate.getRate();
      }
      
      // if this is a FX transaction then mark set the post flag to "P"ending
      String fxPostFlag = (Transaction.isFxTransaction() ? "P" : null);

      // override to debit values for pin debit transactions
      if ( Transaction.hasDebitData() )
      {
        cardType          = "DB";
        reimbursementAttr = "Z";
      }
      
      // if necessary, override the load filename so the 
      // tran will be skipped by the settlement processes 
      String  loadFilename      = null;
      
      // non-USD and USD via international vendors should be skipped
      if ( Transaction.useAdyen() )
      {
        loadFilename      = "processed-adyen";
      }
      else if ( Transaction.usePayVision() )
      {
        loadFilename      = "processed-payvision";
      }
      else if ( Transaction.holdForReview() )
      {
        String fraudResult = Transaction.getFraudResult().toLowerCase();
        loadFilename      = "hold-fraud-" + fraudResult;
      }
      
      // Retrieve the current batch ID/num from the profile. Pre-auths will be updated on the settle request. Adding batch num/id here would be confusing.
      if(!Transaction.getTranType().equals(TridentApiTranBase.TT_PRE_AUTH)) {
        String q = "select current_batch_id, current_batch_number from trident_profile_api where terminal_id = ?";
         PreparedStatement ps = getPreparedStatement(q);
         ps.setString(1, Transaction.getProfileId());
         ResultSet rs = ps.executeQuery();
         if(rs.next()) {
           batchId = rs.getString("current_batch_id");
           batchNumber = rs.getString("current_batch_number");
         }
         ps.close();
         rs.close();
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:160^7*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_capture_api       
//          (
//             rec_id,
//             terminal_id,                       
//             merchant_number,                   
//             dba_name,                          
//             dba_city,                          
//             dba_state,                         
//             dba_zip,                           
//             sic_code,                          
//             phone_number,
//             dm_contact_info,
//             card_number,
//             card_number_enc,
//             card_type,
//             card_present,
//             moto_ecommerce_ind,
//             transaction_date,                  
//             debit_credit_indicator,
//             currency_code,
//             debit_network_id,
//             transaction_amount,                
//             transaction_type,
//             reference_number,                  
//             client_reference_number,
//             purchase_id,                       
//             tax_amount,                        
//             pos_entry_mode,                    
//             pos_data_code,
//             auth_code,
//             auth_response_code,
//             auth_ref_num,                      
//             auth_tran_id,                      
//             auth_val_code,
//             auth_source_code,
//             auth_avs_response,
//             auth_amount,
//             auth_date,
//             auth_returned_aci,
//             reimburse_attribute,
//             trident_tran_id,
//             trident_tran_id_number,
//             external_tran_id,
//             original_trident_tran_id,
//             test_flag, 
//             emulation,
//             avs_zip,
//             exp_date,
//             server_name,
//             auth_response_xml,
//             recurring_payment_ind,
//             recurring_payment_number,
//             recurring_payment_count,
//             ucaf_collection_ind,
//             fx_rate_id,
//             fx_rate,
//             fx_amount_base,
//             fx_reference_number,
//             fx_post_status,
//             level_iii_data_present,
//             merchant_tax_id,
//             customer_tax_id,
//             summary_commodity_code,
//             discount_amount,
//             shipping_amount,
//             duty_amount,
//             ship_to_zip,
//             ship_from_zip,
//             dest_country_code,
//             vat_invoice_number,
//             order_date,
//             vat_amount,
//             vat_rate,
//             alternate_tax_amount,
//             alternate_tax_indicator,
//             requester_name,
//             cardholder_reference_number,
//             fraud_result,
//             name_of_place,
//             rate_daily,
//             statement_date_begin,
//             statement_date_end,
//             statement_city_begin,
//             statement_city_end,
//             statement_region_begin,
//             statement_region_end,
//             statement_country_begin,
//             statement_country_end,
//             load_filename,
//             batch_id,
//             batch_number,
//             ach_sequence,
//             batch_close_timestamp,
//             load_file_id
//          )
//          values
//          (
//            :recId,
//            :Transaction.getProfileId(),
//            :Transaction.getMerchantId(),
//            substr(:Transaction.getDbaName(),1,:TridentApiConstants.FLM_MERCHANT_NAME),
//            substr(:Transaction.getDbaCity(),1,:TridentApiConstants.FLM_MERCHANT_CITY),
//            substr(:Transaction.getDbaState(),1,:TridentApiConstants.FLM_MERCHANT_STATE),
//            substr(:Transaction.getDbaZip(),1,:TridentApiConstants.FLM_MERCHANT_ZIP),
//            :Transaction.getSicCode(),
//            :Transaction.getCustServicePhone(),
//            :Transaction.getDMContactInfo(),
//            :Transaction.getCardNumber(),
//            :Transaction.getCardNumberEnc(),
//            :cardType,
//            :Transaction.getCardPresentFlag(),
//            :Transaction.getMotoEcommIndicator(),
//            :Transaction.getTranDate(),
//            :Transaction.getDebitCreditInd(),
//            :Transaction.getCurrencyCode(),
//            :Transaction.getDebitNetworkId(),
//            :Transaction.getTranAmount(),
//            :tranType,
//            substr(:Transaction.getReferenceNumber(),1,:TridentApiConstants.FLM_REFERENCE_NUMBER),
//            :Transaction.getClientReferenceNumber(),
//            substr(:Transaction.getPurchaseId(),1,:TridentApiConstants.FLM_PURCHASE_ID),
//            :Transaction.getTaxAmount(),
//            :Transaction.getPosEntryMode(),
//            :Transaction.getPosDataCode(),
//            :responseFields[0],
//            :responseFields[1],
//            :responseFields[2],
//            :responseFields[3],
//            :responseFields[4],
//            :responseFields[5],
//            :responseFields[6],
//            :Transaction.getAuthAmount(),
//            :Transaction.getTranDate(),
//            :responseFields[7],
//            :reimbursementAttr,
//            :Transaction.getTridentTranId(),
//            :Transaction.getTridentTranIdNumber(),
//            :Transaction.getExternalTranId(),
//            :Transaction.getOriginalTranId(),
//            :testFlag,
//            :Transaction.getEmulationString(),
//            :Transaction.getAvsZip(),
//            :Transaction.getExpDate(),
//            :Transaction.getServerNameEncoded(),
//            decode(:createBlob,1,empty_blob(),null),
//            :Transaction.getRecurringPaymentInd(),
//            :Transaction.getRecurringPaymentNumber(),
//            :Transaction.getRecurringPaymentCount(),
//            :Transaction.getUcafCollectionInd(),
//            :Transaction.getFxRateId(),
//            :rate,
//            :Transaction.getFxAmount(),
//            :Transaction.getFxTranId(),
//            :fxPostFlag,
//            :Transaction.hasLevelIIIData() ? "Y" : "N",
//            :Transaction.getLevelIIIField(TridentApiConstants.FN_MERCHANT_TAX_ID),
//            :Transaction.getLevelIIIField(TridentApiConstants.FN_CUSTOMER_TAX_ID),
//            :Transaction.getLevelIIIField(TridentApiConstants.FN_SUMMARY_COMMODITY_CODE),
//            :Transaction.getLevelIIIField(TridentApiConstants.FN_DISCOUNT_AMOUNT),
//            :Transaction.getLevelIIIField(TridentApiConstants.FN_SHIPPING_AMOUNT),
//            :Transaction.getLevelIIIField(TridentApiConstants.FN_DUTY_AMOUNT),
//            :Transaction.getShipToZip(),
//            :Transaction.getLevelIIIField(TridentApiConstants.FN_SHIP_FROM_ZIP),
//            :Transaction.getShipToCountryCode(),
//            :Transaction.getLevelIIIField(TridentApiConstants.FN_VAT_INVOICE_NUMBER),
//            :Transaction.getLevelIIIFieldAsDate(TridentApiConstants.FN_ORDER_DATE),
//            :Transaction.getLevelIIIField(TridentApiConstants.FN_VAT_AMOUNT),
//            :Transaction.getLevelIIIField(TridentApiConstants.FN_VAT_RATE),
//            :Transaction.getLevelIIIField(TridentApiConstants.FN_ALT_TAX_AMOUNT),
//            :Transaction.getLevelIIIField(TridentApiConstants.FN_ALT_TAX_AMOUNT_IND),
//            :Transaction.hasLevelIIIData() ? Transaction.getLevelIIIField(TridentApiConstants.FN_REQUESTER_NAME) : Transaction.getTravelCheckInName(),
//            :Transaction.getCardholderRefNum(),
//            :Transaction.getFraudResult(),
//            :Transaction.getTravelCheckInLocation(),
//            :Transaction.getTravelCheckInAmount(),
//            :Transaction.getTravelDateBegin(),
//            :Transaction.getTravelDateEnd(),
//            :Transaction.getTravelCityBegin(),
//            :Transaction.getTravelCityEnd(),
//            :Transaction.getTravelRegionBegin(),
//            :Transaction.getTravelRegionEnd(),
//            :Transaction.getTravelCountryBegin(),
//            :Transaction.getTravelCountryEnd(),
//            :loadFilename,
//            :batchId,
//            :batchNumber,
//            0,
//            sysdate,
//            -1
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_190 = Transaction.getProfileId();
 long __sJT_191 = Transaction.getMerchantId();
 String __sJT_192 = Transaction.getDbaName();
 String __sJT_193 = Transaction.getDbaCity();
 String __sJT_194 = Transaction.getDbaState();
 String __sJT_195 = Transaction.getDbaZip();
 String __sJT_196 = Transaction.getSicCode();
 String __sJT_197 = Transaction.getCustServicePhone();
 String __sJT_198 = Transaction.getDMContactInfo();
 String __sJT_199 = Transaction.getCardNumber();
 String __sJT_200 = Transaction.getCardNumberEnc();
 String __sJT_201 = Transaction.getCardPresentFlag();
 String __sJT_202 = Transaction.getMotoEcommIndicator();
 java.sql.Date __sJT_203 = Transaction.getTranDate();
 String __sJT_204 = Transaction.getDebitCreditInd();
 String __sJT_205 = Transaction.getCurrencyCode();
 String __sJT_206 = Transaction.getDebitNetworkId();
 double __sJT_207 = Transaction.getTranAmount();
 String __sJT_208 = Transaction.getReferenceNumber();
 String __sJT_209 = Transaction.getClientReferenceNumber();
 String __sJT_210 = Transaction.getPurchaseId();
 double __sJT_211 = Transaction.getTaxAmount();
 String __sJT_212 = Transaction.getPosEntryMode();
 String __sJT_213 = Transaction.getPosDataCode();
 String __sJT_214 = responseFields[0];
 String __sJT_215 = responseFields[1];
 String __sJT_216 = responseFields[2];
 String __sJT_217 = responseFields[3];
 String __sJT_218 = responseFields[4];
 String __sJT_219 = responseFields[5];
 String __sJT_220 = responseFields[6];
 double __sJT_221 = Transaction.getAuthAmount();
 java.sql.Date __sJT_222 = Transaction.getTranDate();
 String __sJT_223 = responseFields[7];
 String __sJT_224 = Transaction.getTridentTranId();
 long __sJT_225 = Transaction.getTridentTranIdNumber();
 String __sJT_226 = Transaction.getExternalTranId();
 String __sJT_227 = Transaction.getOriginalTranId();
 String __sJT_228 = Transaction.getEmulationString();
 String __sJT_229 = Transaction.getAvsZip();
 String __sJT_230 = Transaction.getExpDate();
 String __sJT_231 = Transaction.getServerNameEncoded();
 String __sJT_232 = Transaction.getRecurringPaymentInd();
 int __sJT_233 = Transaction.getRecurringPaymentNumber();
 int __sJT_234 = Transaction.getRecurringPaymentCount();
 String __sJT_235 = Transaction.getUcafCollectionInd();
 int __sJT_236 = Transaction.getFxRateId();
 double __sJT_237 = Transaction.getFxAmount();
 String __sJT_238 = Transaction.getFxTranId();
 String __sJT_239 = Transaction.hasLevelIIIData() ? "Y" : "N";
 String __sJT_240 = Transaction.getLevelIIIField(TridentApiConstants.FN_MERCHANT_TAX_ID);
 String __sJT_241 = Transaction.getLevelIIIField(TridentApiConstants.FN_CUSTOMER_TAX_ID);
 String __sJT_242 = Transaction.getLevelIIIField(TridentApiConstants.FN_SUMMARY_COMMODITY_CODE);
 String __sJT_243 = Transaction.getLevelIIIField(TridentApiConstants.FN_DISCOUNT_AMOUNT);
 String __sJT_244 = Transaction.getLevelIIIField(TridentApiConstants.FN_SHIPPING_AMOUNT);
 String __sJT_245 = Transaction.getLevelIIIField(TridentApiConstants.FN_DUTY_AMOUNT);
 String __sJT_246 = Transaction.getShipToZip();
 String __sJT_247 = Transaction.getLevelIIIField(TridentApiConstants.FN_SHIP_FROM_ZIP);
 String __sJT_248 = Transaction.getShipToCountryCode();
 String __sJT_249 = Transaction.getLevelIIIField(TridentApiConstants.FN_VAT_INVOICE_NUMBER);
 java.sql.Date __sJT_250 = Transaction.getLevelIIIFieldAsDate(TridentApiConstants.FN_ORDER_DATE);
 String __sJT_251 = Transaction.getLevelIIIField(TridentApiConstants.FN_VAT_AMOUNT);
 String __sJT_252 = Transaction.getLevelIIIField(TridentApiConstants.FN_VAT_RATE);
 String __sJT_253 = Transaction.getLevelIIIField(TridentApiConstants.FN_ALT_TAX_AMOUNT);
 String __sJT_254 = Transaction.getLevelIIIField(TridentApiConstants.FN_ALT_TAX_AMOUNT_IND);
 String __sJT_255 = Transaction.hasLevelIIIData() ? Transaction.getLevelIIIField(TridentApiConstants.FN_REQUESTER_NAME) : Transaction.getTravelCheckInName();
 String __sJT_256 = Transaction.getCardholderRefNum();
 String __sJT_257 = Transaction.getFraudResult();
 String __sJT_258 = Transaction.getTravelCheckInLocation();
 double __sJT_259 = Transaction.getTravelCheckInAmount();
 java.sql.Date __sJT_260 = Transaction.getTravelDateBegin();
 java.sql.Date __sJT_261 = Transaction.getTravelDateEnd();
 String __sJT_262 = Transaction.getTravelCityBegin();
 String __sJT_263 = Transaction.getTravelCityEnd();
 String __sJT_264 = Transaction.getTravelRegionBegin();
 String __sJT_265 = Transaction.getTravelRegionEnd();
 String __sJT_266 = Transaction.getTravelCountryBegin();
 String __sJT_267 = Transaction.getTravelCountryEnd();
   String theSqlTS = "insert into trident_capture_api       \n        (\n           rec_id,\n           terminal_id,                       \n           merchant_number,                   \n           dba_name,                          \n           dba_city,                          \n           dba_state,                         \n           dba_zip,                           \n           sic_code,                          \n           phone_number,\n           dm_contact_info,\n           card_number,\n           card_number_enc,\n           card_type,\n           card_present,\n           moto_ecommerce_ind,\n           transaction_date,                  \n           debit_credit_indicator,\n           currency_code,\n           debit_network_id,\n           transaction_amount,                \n           transaction_type,\n           reference_number,                  \n           client_reference_number,\n           purchase_id,                       \n           tax_amount,                        \n           pos_entry_mode,                    \n           pos_data_code,\n           auth_code,\n           auth_response_code,\n           auth_ref_num,                      \n           auth_tran_id,                      \n           auth_val_code,\n           auth_source_code,\n           auth_avs_response,\n           auth_amount,\n           auth_date,\n           auth_returned_aci,\n           reimburse_attribute,\n           trident_tran_id,\n           trident_tran_id_number,\n           external_tran_id,\n           original_trident_tran_id,\n           test_flag, \n           emulation,\n           avs_zip,\n           exp_date,\n           server_name,\n           auth_response_xml,\n           recurring_payment_ind,\n           recurring_payment_number,\n           recurring_payment_count,\n           ucaf_collection_ind,\n           fx_rate_id,\n           fx_rate,\n           fx_amount_base,\n           fx_reference_number,\n           fx_post_status,\n           level_iii_data_present,\n           merchant_tax_id,\n           customer_tax_id,\n           summary_commodity_code,\n           discount_amount,\n           shipping_amount,\n           duty_amount,\n           ship_to_zip,\n           ship_from_zip,\n           dest_country_code,\n           vat_invoice_number,\n           order_date,\n           vat_amount,\n           vat_rate,\n           alternate_tax_amount,\n           alternate_tax_indicator,\n           requester_name,\n           cardholder_reference_number,\n           fraud_result,\n           name_of_place,\n           rate_daily,\n           statement_date_begin,\n           statement_date_end,\n           statement_city_begin,\n           statement_city_end,\n           statement_region_begin,\n           statement_region_end,\n           statement_country_begin,\n           statement_country_end,\n           load_filename,\n           batch_id,\n           batch_number,\n           ach_sequence,\n           batch_close_timestamp,\n           load_file_id\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n          substr( :4 ,1, :5 ),\n          substr( :6 ,1, :7 ),\n          substr( :8 ,1, :9 ),\n          substr( :10 ,1, :11 ),\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n           :16 ,\n           :17 ,\n           :18 ,\n           :19 ,\n           :20 ,\n           :21 ,\n           :22 ,\n           :23 ,\n           :24 ,\n           :25 ,\n          substr( :26 ,1, :27 ),\n           :28 ,\n          substr( :29 ,1, :30 ),\n           :31 ,\n           :32 ,\n           :33 ,\n           :34 ,\n           :35 ,\n           :36 ,\n           :37 ,\n           :38 ,\n           :39 ,\n           :40 ,\n           :41 ,\n           :42 ,\n           :43 ,\n           :44 ,\n           :45 ,\n           :46 ,\n           :47 ,\n           :48 ,\n           :49 ,\n           :50 ,\n           :51 ,\n           :52 ,\n           :53 ,\n          decode( :54 ,1,empty_blob(),null),\n           :55 ,\n           :56 ,\n           :57 ,\n           :58 ,\n           :59 ,\n           :60 ,\n           :61 ,\n           :62 ,\n           :63 ,\n           :64 ,\n           :65 ,\n           :66 ,\n           :67 ,\n           :68 ,\n           :69 ,\n           :70 ,\n           :71 ,\n           :72 ,\n           :73 ,\n           :74 ,\n           :75 ,\n           :76 ,\n           :77 ,\n           :78 ,\n           :79 ,\n           :80 ,\n           :81 ,\n           :82 ,\n           :83 ,\n           :84 ,\n           :85 ,\n           :86 ,\n           :87 ,\n           :88 ,\n           :89 ,\n           :90 ,\n           :91 ,\n           :92 ,\n           :93 ,\n           :94 ,\n           :95 ,\n          0,\n          sysdate,\n          -1\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.api.StoreTransactionTask",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setString(2,__sJT_190);
   __sJT_st.setLong(3,__sJT_191);
   __sJT_st.setString(4,__sJT_192);
   __sJT_st.setInt(5,TridentApiConstants.FLM_MERCHANT_NAME);
   __sJT_st.setString(6,__sJT_193);
   __sJT_st.setInt(7,TridentApiConstants.FLM_MERCHANT_CITY);
   __sJT_st.setString(8,__sJT_194);
   __sJT_st.setInt(9,TridentApiConstants.FLM_MERCHANT_STATE);
   __sJT_st.setString(10,__sJT_195);
   __sJT_st.setInt(11,TridentApiConstants.FLM_MERCHANT_ZIP);
   __sJT_st.setString(12,__sJT_196);
   __sJT_st.setString(13,__sJT_197);
   __sJT_st.setString(14,__sJT_198);
   __sJT_st.setString(15,__sJT_199);
   __sJT_st.setString(16,__sJT_200);
   __sJT_st.setString(17,cardType);
   __sJT_st.setString(18,__sJT_201);
   __sJT_st.setString(19,__sJT_202);
   __sJT_st.setDate(20,__sJT_203);
   __sJT_st.setString(21,__sJT_204);
   __sJT_st.setString(22,__sJT_205);
   __sJT_st.setString(23,__sJT_206);
   __sJT_st.setDouble(24,__sJT_207);
   __sJT_st.setString(25,tranType);
   __sJT_st.setString(26,__sJT_208);
   __sJT_st.setInt(27,TridentApiConstants.FLM_REFERENCE_NUMBER);
   __sJT_st.setString(28,__sJT_209);
   __sJT_st.setString(29,__sJT_210);
   __sJT_st.setInt(30,TridentApiConstants.FLM_PURCHASE_ID);
   __sJT_st.setDouble(31,__sJT_211);
   __sJT_st.setString(32,__sJT_212);
   __sJT_st.setString(33,__sJT_213);
   __sJT_st.setString(34,__sJT_214);
   __sJT_st.setString(35,__sJT_215);
   __sJT_st.setString(36,__sJT_216);
   __sJT_st.setString(37,__sJT_217);
   __sJT_st.setString(38,__sJT_218);
   __sJT_st.setString(39,__sJT_219);
   __sJT_st.setString(40,__sJT_220);
   __sJT_st.setDouble(41,__sJT_221);
   __sJT_st.setDate(42,__sJT_222);
   __sJT_st.setString(43,__sJT_223);
   __sJT_st.setString(44,reimbursementAttr);
   __sJT_st.setString(45,__sJT_224);
   __sJT_st.setLong(46,__sJT_225);
   __sJT_st.setString(47,__sJT_226);
   __sJT_st.setString(48,__sJT_227);
   __sJT_st.setString(49,testFlag);
   __sJT_st.setString(50,__sJT_228);
   __sJT_st.setString(51,__sJT_229);
   __sJT_st.setString(52,__sJT_230);
   __sJT_st.setString(53,__sJT_231);
   __sJT_st.setInt(54,createBlob);
   __sJT_st.setString(55,__sJT_232);
   __sJT_st.setInt(56,__sJT_233);
   __sJT_st.setInt(57,__sJT_234);
   __sJT_st.setString(58,__sJT_235);
   __sJT_st.setInt(59,__sJT_236);
   __sJT_st.setDouble(60,rate);
   __sJT_st.setDouble(61,__sJT_237);
   __sJT_st.setString(62,__sJT_238);
   __sJT_st.setString(63,fxPostFlag);
   __sJT_st.setString(64,__sJT_239);
   __sJT_st.setString(65,__sJT_240);
   __sJT_st.setString(66,__sJT_241);
   __sJT_st.setString(67,__sJT_242);
   __sJT_st.setString(68,__sJT_243);
   __sJT_st.setString(69,__sJT_244);
   __sJT_st.setString(70,__sJT_245);
   __sJT_st.setString(71,__sJT_246);
   __sJT_st.setString(72,__sJT_247);
   __sJT_st.setString(73,__sJT_248);
   __sJT_st.setString(74,__sJT_249);
   __sJT_st.setDate(75,__sJT_250);
   __sJT_st.setString(76,__sJT_251);
   __sJT_st.setString(77,__sJT_252);
   __sJT_st.setString(78,__sJT_253);
   __sJT_st.setString(79,__sJT_254);
   __sJT_st.setString(80,__sJT_255);
   __sJT_st.setString(81,__sJT_256);
   __sJT_st.setString(82,__sJT_257);
   __sJT_st.setString(83,__sJT_258);
   __sJT_st.setDouble(84,__sJT_259);
   __sJT_st.setDate(85,__sJT_260);
   __sJT_st.setDate(86,__sJT_261);
   __sJT_st.setString(87,__sJT_262);
   __sJT_st.setString(88,__sJT_263);
   __sJT_st.setString(89,__sJT_264);
   __sJT_st.setString(90,__sJT_265);
   __sJT_st.setString(91,__sJT_266);
   __sJT_st.setString(92,__sJT_267);
   __sJT_st.setString(93,loadFilename);
   __sJT_st.setString(94,batchId);
   __sJT_st.setString(95,batchNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:352^7*/

      // store the visa D response as a blob 
      if ( Transaction.VisaDResponse != null )
      {
        // store the xml response as a blob
        xmlBytes = Transaction.VisaDResponse.toXml().getBytes();
        /*@lineinfo:generated-code*//*@lineinfo:359^9*/

//  ************************************************************
//  #sql [Ctx] { select  auth_response_xml 
//            from    trident_capture_api tc
//            where   tc.rec_id = :recId
//            for update
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  auth_response_xml  \n          from    trident_capture_api tc\n          where   tc.rec_id =  :1 \n          for update";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.api.StoreTransactionTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   blobData = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:365^9*/
        bOut = blobData.setBinaryStream(1L);
        bOut.write( xmlBytes, 0, xmlBytes.length );
        bOut.flush();
        bOut.close();
      }
      retVal = (Ctx.getExecutionContext().getUpdateCount() != 0);
      
      List lineItemData = Transaction.getLineItemData();
      if ( lineItemData != null )
      {
        for( int i = 0; i < lineItemData.size(); ++i )
        {
          /*@lineinfo:generated-code*//*@lineinfo:378^11*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_api_line_item_detail
//              (
//                rec_id,
//                line_item_id,
//                card_type,
//                raw_line_item_data
//              )
//              values
//              (
//                :recId,
//                :i,
//                :Transaction.getCardType(),
//                :(String)lineItemData.get(i)
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_268 = Transaction.getCardType();
 String __sJT_269 = (String)lineItemData.get(i);
   String theSqlTS = "insert into trident_api_line_item_detail\n            (\n              rec_id,\n              line_item_id,\n              card_type,\n              raw_line_item_data\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.api.StoreTransactionTask",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setInt(2,i);
   __sJT_st.setString(3,__sJT_268);
   __sJT_st.setString(4,__sJT_269);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:394^11*/
        }
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:398^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:401^7*/
    }
    catch( Exception e )
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:405^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:405^34*/ }catch( Exception ee){} // rollback tran
      String tranId = "n/a";
      try{ tranId = Transaction.getTridentTranId(); } catch( Exception ee ){}
      
      SyncLog.logThrottled(this.getClass().getName()+":doTask", e);
      try{ /*@lineinfo:generated-code*//*@lineinfo:410^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:410^32*/ }catch( Exception ee){}   // commit log entry
      if ( getErrorCode() == TridentApiConstants.ER_NONE )
      {
        setErrorCode(TridentApiConstants.ER_INTERNAL_ERROR);
        setErrorDesc("Internal Error - Try Again");
      }
    }
    finally
    {
      setAutoCommit(true);    // restore auto commit
      cleanUp(); 
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/