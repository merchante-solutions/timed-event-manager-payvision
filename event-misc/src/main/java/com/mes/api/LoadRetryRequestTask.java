/*@lineinfo:filename=LoadRetryRequestTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/LoadRetryRequestTask.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

public class LoadRetryRequestTask extends TridentApiTransactionTask
{
  private String    OriginalResponse    = null;
  
  public LoadRetryRequestTask( TridentApiTransaction tran )
  {
    super(tran);
  }
  
  public boolean doTask()
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    boolean             retVal      = false;
    
    try
    {
      connect();
    
      OriginalResponse = null;
      /*@lineinfo:generated-code*//*@lineinfo:44^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ltrim(rtrim(response))    as response
//          from    trident_api_access_log  al
//          where   al.profile_id = :Transaction.getProfileId() 
//                  and al.retry_id = :Transaction.getRetryId() 
//                  and al.request_time > (sysdate-2)
//                  and al.transaction_type != :TridentApiTranBase.TT_RESPONSE_INQUIRY -- 'I'
//          order by rec_id desc                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_137 = Transaction.getProfileId();
 String __sJT_138 = Transaction.getRetryId();
  try {
   String theSqlTS = "select  ltrim(rtrim(response))    as response\n        from    trident_api_access_log  al\n        where   al.profile_id =  :1  \n                and al.retry_id =  :2  \n                and al.request_time > (sysdate-2)\n                and al.transaction_type !=  :3  -- 'I'\n        order by rec_id desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.api.LoadRetryRequestTask",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_137);
   __sJT_st.setString(2,__sJT_138);
   __sJT_st.setString(3,TridentApiTranBase.TT_RESPONSE_INQUIRY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.api.LoadRetryRequestTask",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:53^7*/
      resultSet = it.getResultSet();
      if ( resultSet.next() )
      {
        setResponse( resultSet.getString("response") );
      }
      resultSet.close();
      retVal = true;
    }
    catch( Exception e )
    {
      setError( TridentApiConstants.ER_RETRY_ID_LOOKUP_FAILED );
      logEntry("doTask()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ){}
      cleanUp(); 
      
      // be sure that the error code is set if the task failed
      if ( !retVal && (getErrorCode() == TridentApiConstants.ER_NONE) )
      {
        setError( TridentApiConstants.ER_RETRY_ID_LOOKUP_FAILED );
      }
    }
    return( retVal );
  }
  
  public String getResponse( )
  {
    return( OriginalResponse );
  }
  
  protected void setResponse( String originalResponse )
    throws java.sql.SQLException
  {
    int     endIndex      = 0;
    int     index         = 0;
    int     retryCount    = 0;
    
    if( originalResponse != null && originalResponse.length() > 0 )
    {
      StringBuffer buffer = new StringBuffer( originalResponse );
      
      /*@lineinfo:generated-code*//*@lineinfo:97^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1) 
//          from    trident_api_access_log al
//          where   al.profile_id = :Transaction.getProfileId() and
//                  al.retry_id = :Transaction.getRetryId() and
//                  al.request_time > (sysdate-2)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_139 = Transaction.getProfileId();
 String __sJT_140 = Transaction.getRetryId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)  \n        from    trident_api_access_log al\n        where   al.profile_id =  :1  and\n                al.retry_id =  :2  and\n                al.request_time > (sysdate-2)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.api.LoadRetryRequestTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_139);
   __sJT_st.setString(2,__sJT_140);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retryCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:104^7*/
      
      if ( (index = originalResponse.indexOf(TridentApiConstants.FN_RETRY_COUNT)) >= 0 )
      {
        index += TridentApiConstants.FN_RETRY_COUNT.length()+1;   // move to the first character
        endIndex = index;
      
        while( endIndex < buffer.length() && buffer.charAt(endIndex) != '&' )
        {
          ++endIndex;
        }
        buffer.replace(index,endIndex,String.valueOf(retryCount));
      }
      else 
      {
        buffer.append("&retry_count=");
        buffer.append( retryCount );
      }        
    
      // store the new value in the class member
      OriginalResponse = buffer.toString();
    }
  }
}/*@lineinfo:generated-code*/