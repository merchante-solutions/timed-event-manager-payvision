/*@lineinfo:filename=StipSyncThread*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/api/StipSyncThread.sqlj $

  Description:
  
  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2010-10-27 14:55:23 -0700 (Wed, 27 Oct 2010) $
  Version            : $Revision: 18053 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.util.Iterator;
import java.util.Map;
import com.mes.database.SleepyCatManager;

public class StipSyncThread extends TridentApiSyncThread
{
  private   Map             TridentStipTable      = null;

  public StipSyncThread( long sleepTime )
  {
    super(sleepTime);
    
    try
    {
      SleepyCatManager mgr = SleepyCatManager.getInstance(TridentApiConstants.getApiDatabasePath());
      TridentStipTable     = mgr.getStoredMap(TridentApiConstants.API_TABLE_STIP);
    }
    catch( Exception e )
    {
      System.out.println("StipSyncThread - " + e.toString());
      e.printStackTrace();
      logEntry("StipSyncThread()", e.toString());
    }
  }
  
  protected void execute( )
  {
    ApiStipEntry                data          = null;
    String                      tranId        = null;
  
    try
    {
      connect(true);
      
      // process transactions
      Iterator iterator = TridentStipTable.keySet().iterator();
      while ( iterator.hasNext() ) 
      {
        tranId  = (String)iterator.next();
        data    = (ApiStipEntry)TridentStipTable.get( tranId );
        
        /*@lineinfo:generated-code*//*@lineinfo:69^9*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_api_stip
//            (
//              merchant_number,
//              profile_id,
//              trident_tran_id,
//              request_params,
//              server_name,
//              status
//            )
//            values
//            (
//              :data.getMerchantId(),
//              :data.getProfileId(),
//              :data.getTridentTranId(),
//              :data.getRequestParams(),
//              :data.getServerName(),
//              -1
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_185 = data.getMerchantId();
 String __sJT_186 = data.getProfileId();
 String __sJT_187 = data.getTridentTranId();
 String __sJT_188 = data.getRequestParams();
 String __sJT_189 = data.getServerName();
   String theSqlTS = "insert into trident_api_stip\n          (\n            merchant_number,\n            profile_id,\n            trident_tran_id,\n            request_params,\n            server_name,\n            status\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n            -1\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.api.StipSyncThread",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_185);
   __sJT_st.setString(2,__sJT_186);
   __sJT_st.setString(3,__sJT_187);
   __sJT_st.setString(4,__sJT_188);
   __sJT_st.setString(5,__sJT_189);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:89^9*/
      
        if ( Ctx.getExecutionContext().getUpdateCount() > 0 )
        {
          TridentStipTable.remove( tranId );
        }
      }
    }
    catch( Exception e )
    {
      System.out.println("execute() - " + e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/