/*@lineinfo:filename=TransactionSyncThread*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/TransactionSyncThread.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.util.Iterator;
import java.util.Map;
import com.mes.api.ach.AchLogRequestTask;
import com.mes.api.bml.BmlBaseTran;
import com.mes.api.bml.BmlStoreTransactionTask;
import com.mes.database.SleepyCatManager;
import com.mes.support.SyncLog;

public class TransactionSyncThread extends TridentApiSyncThread
{
  private   Map             TridentCaptureTable   = null;

  public TransactionSyncThread( long sleepTime )
  {
    super(sleepTime);
    
    try
    {
      SleepyCatManager mgr = SleepyCatManager.getInstance(TridentApiConstants.getApiDatabasePath());
      TridentCaptureTable = mgr.getStoredMap(TridentApiConstants.API_TABLE_CAPTURE);
    }
    catch( Exception e )
    {
      System.out.println("TransactionSyncThread - " + e.toString());
      e.printStackTrace();
      logEntry("TransactionSyncThread()", e.toString());
    }
    
    // clean up any partial entries
    cleanOrphans();
  }
  
  protected void cleanOrphans( )
  {
    try
    {
      // check for orphaned authorization responses
      TridentApiTransaction   data      = null;
      Iterator                iterator  = TridentCaptureTable.keySet().iterator();
      String                  tranId    = null;
      while ( iterator.hasNext() ) 
      {
        tranId  = (String)iterator.next();
        data    = (TridentApiTransaction)TridentCaptureTable.get( tranId );
    
        if ( data != null )
        {
          switch( data.getTranState() )
          {
            case TridentApiTransaction.ST_NEW:
            case TridentApiTransaction.ST_AUTH_REQUESTED:
            case TridentApiTransaction.ST_RESP_RECEIVED:
              TridentCaptureTable.remove(tranId);
              break;
          }
        }          
      }   
    }
    catch( Exception e )
    {
      SyncLog.LogEntry(this.getClass().getName() + "::cleanOrphans()", e.toString());
    }
  }

  protected void execute( )
  {
    TridentApiTransaction       data          = null;
    TridentApiTransactionTask   task          = null;
    String                      tranId        = null;
    String                      tranType      = null;
  
    try
    {
      // process transactions
      Iterator iterator = TridentCaptureTable.keySet().iterator();
      while ( iterator.hasNext() ) 
      {
        try
        {
          tranId  = (String)iterator.next();
          data    = (TridentApiTransaction)TridentCaptureTable.get( tranId );
          task    = null;
      
          if ( data.getTranState() == data.ST_DELETE )
          {
            TridentCaptureTable.remove( tranId );
          }
          else if ( data.getTranState() == data.ST_COMPLETE )
          {
            tranType = data.getTranType();
        
            if ( tranType.equals(TridentApiTranBase.TT_SETTLE) )
            {
              task = new SettleTransactionTask(data);
            }
            else if ( tranType.equals(TridentApiTranBase.TT_BML_REQUEST) )
            {
              task = new BmlStoreTransactionTask((BmlBaseTran)data);
            }
            else if ( tranType.equals(TridentApiTranBase.TT_ACH_REQUEST) )
            {
              AchLogRequestTask.doTask(data);
            }
            else    // default
            {
              // store approved sales/pre-auths and credits
              if( data.isApproved() || 
                  tranType.equals(TridentApiTranBase.TT_CREDIT) )
              {
                task = new StoreTransactionTask(data);
              }
            }
          
            if ( task != null && task.doTask() )
            {
              TridentCaptureTable.remove( tranId );
            }
          } // end if ( data.getTranState() == data.ST_COMPLETE )
        }
        catch( Exception tran_e )
        {
          logEntry("execute(" + tranId + ")", tran_e.toString());
        }          
      }
    }
    catch( Exception e )
    {
      System.out.println("execute() - " + e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/