/*@lineinfo:filename=RateTable*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/RateTable.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2007,2008 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.io.ByteArrayOutputStream;
import java.sql.ResultSet;
import java.util.Vector;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.tools.CurrencyCodeConverter;
import masthead.util.XOMSerializer;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Serializer;
import sqlj.runtime.ResultSetIterator;

public class RateTable extends SQLJConnectionBase
{
  private long        MerchantId              = 0L;
  private Vector      Rates                   = new Vector();
  
  public RateTable( long merchantId )
  {
    MerchantId  = merchantId;
    load();
  }
  
  public ForeignExchangeRate getRateByCurrencyCode( String currencyCode )
  {
    String                currencyCodeAlpha = currencyCode;
    ForeignExchangeRate   rate              = null;
    ForeignExchangeRate   retVal            = null;
    
    try
    {
      int tempVal = Integer.parseInt(currencyCodeAlpha);
      CurrencyCodeConverter converter = CurrencyCodeConverter.getInstance();
      currencyCodeAlpha = converter.numericCodeToAlphaCode(currencyCode);
    }
    catch( NumberFormatException nfe )
    {
      // ignore, already in alpha
    }
    
    for( int i = 0; i < Rates.size(); ++i )
    {
      rate = (ForeignExchangeRate)Rates.elementAt(i);
      if ( rate.getConsumerCurrencyCode().equals(currencyCodeAlpha) && !rate.isExpired() )
      {
        retVal = rate;
        break;
      }
    }
    return( retVal );
  }
  
  public ForeignExchangeRate getRateById( int rateId )
  {
    ForeignExchangeRate   rate              = null;
    ForeignExchangeRate   retVal            = null;
    
    for( int i = 0; i < Rates.size(); ++i )
    {
      rate = (ForeignExchangeRate)Rates.elementAt(i);
      if ( rate.getRateId() == rateId )
      {
        retVal = rate;
        break;
      }
    }
    return( retVal );
  }
  
  public int getRateCount( )
  {
    return( Rates.size() );
  }
  
  public void load()
  {
    int                     hours       = 24;
    ResultSetIterator       it          = null;
    ResultSet               resultSet   = null;
    
    try
    {
      connect();
      
      Rates.removeAllElements();
      hours = MesDefaults.getInt(MesDefaults.DK_FX_RATE_EXPIRATION_EXTENSION);
      
      /*@lineinfo:generated-code*//*@lineinfo:111^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  fxt.merchant_number                 as merchant_number,
//                  fxt.merchant_id                     as merchant_id,
//                  fxt.client_id                       as client_id,
//                  fxt.rate_id                         as rate_id,
//                  fxt.merchant_currency_code          as merchant_currency_code,
//                  fxt.consumer_currency_code          as consumer_currency_code,
//                  fxt.consumer_currency_country       as consumer_currency_country,
//                  fxt.consumer_currency_desc          as consumer_currency_desc,
//                  (fxt.expiration_date + (:hours/24)) as expiration_date,
//                  fxt.rate                            as rate
//          from    fx_rate_tables    fxt
//          where   fxt.merchant_number = :MerchantId
//                  -- pad the expiration date
//                  and (fxt.expiration_date + (:hours/24)) >= sysdate
//          order by  fxt.consumer_currency_code, 
//                    fxt.expiration_date desc                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  fxt.merchant_number                 as merchant_number,\n                fxt.merchant_id                     as merchant_id,\n                fxt.client_id                       as client_id,\n                fxt.rate_id                         as rate_id,\n                fxt.merchant_currency_code          as merchant_currency_code,\n                fxt.consumer_currency_code          as consumer_currency_code,\n                fxt.consumer_currency_country       as consumer_currency_country,\n                fxt.consumer_currency_desc          as consumer_currency_desc,\n                (fxt.expiration_date + ( :1 /24)) as expiration_date,\n                fxt.rate                            as rate\n        from    fx_rate_tables    fxt\n        where   fxt.merchant_number =  :2 \n                -- pad the expiration date\n                and (fxt.expiration_date + ( :3 /24)) >= sysdate\n        order by  fxt.consumer_currency_code, \n                  fxt.expiration_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.api.RateTable",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,hours);
   __sJT_st.setLong(2,MerchantId);
   __sJT_st.setInt(3,hours);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.api.RateTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:129^7*/
      resultSet = it.getResultSet();

      while ( resultSet.next() )
      {
        // sort order of query insures that the most recent rate id
        // will be used when lookups are performed with the currency code
        Rates.addElement( new ForeignExchangeRate(resultSet) );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("load()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp(); 
    }
  }
  
  public void showData( )
  {
  }
  
  public String toXml( )
  {
    return( toXml(false) );
  }
  
  public String toXml( boolean includeHistoricRates )
  {
    String                currencyCode      = "";
    Element               field             = null;
    String                lastCurrencyCode  = "";
    String                retVal            = null;
    Element               root              = null;
    
    try
    {
      root = new Element("FXRateTable");
    
      for( int i = 0; i < Rates.size(); ++i )
      {
        ForeignExchangeRate rate = (ForeignExchangeRate)Rates.elementAt(i);
        currencyCode = rate.getConsumerCurrencyCode();
        if ( includeHistoricRates || !currencyCode.equals(lastCurrencyCode) )
        {
          root.appendChild( rate.getXmlRootElement() );
        }          
        lastCurrencyCode = currencyCode;
      }
    
      Document              doc     = new Document( root );
      ByteArrayOutputStream xmlOut  = new ByteArrayOutputStream();
    
      Serializer serializer = new XOMSerializer(xmlOut, "ISO-8859-1");
    
      serializer.setIndent(1);    // enable std indentation
      serializer.write(doc);
      retVal = xmlOut.toString();
    }
    catch( Exception e )
    {
      logEntry("toXml()",e.toString());
    }
    return( retVal );
  }
  
  public static void main( String[] args )
  {
    RateTable   rate = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      long merchantId = 0L;
      rate = new RateTable(merchantId);
      System.out.println(rate.toXml());
    }
    catch(Exception e )
    {
      System.out.println(e.toString());
    }
    
    
  }
}/*@lineinfo:generated-code*/