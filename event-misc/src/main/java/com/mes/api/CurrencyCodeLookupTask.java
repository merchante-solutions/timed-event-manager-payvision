/*@lineinfo:filename=CurrencyCodeLookupTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/CountryCodeToCurrencyCodeTask.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import com.mes.config.MesDefaults;
import com.mes.tools.CountryCodeConverter;
import com.tfxusa.secure.IPInfo;
import com.tfxusa.secure.TFXUSAServiceLocator;
import com.tfxusa.secure.TFXUSAServiceSoapStub;

public class CurrencyCodeLookupTask extends TridentApiTransactionTask
{
  public CurrencyCodeLookupTask( TridentApiTransaction tran )
  {
    super(tran);
  }
  
  public boolean doTask()
  {
    String    countryCode   = Transaction.getCountryCode();
    boolean   retVal        = false;
    
    try
    {
      if ( countryCode.equals( TridentApiConstants.FV_COUNTRY_CODE_DEFAULT ) )
      {
        // no country code provided, attempt to use the ip address
        String ipAddr = Transaction.getIpAddress();
        
        if ( ipAddr != null && !ipAddr.equals("") )
        {
          // set the currency based on the IP address
          TFXUSAServiceLocator   sl      =  new TFXUSAServiceLocator(); 
          TFXUSAServiceSoapStub  service = (TFXUSAServiceSoapStub)sl.getTFXUSAServiceSoap(); 
          IPInfo                 ipInfo  = service.TFX_GetIPInfo_1_00_Post( MesDefaults.getString(MesDefaults.DK_TOCCATA_USER_ID),
                                                                            MesDefaults.getString(MesDefaults.DK_TOCCATA_ACCESS_KEY),
                                                                            ipAddr);
          // set the currency and country codes to match the IP address
          Transaction.setCountryCode( ipInfo.getCountryISO() );
          Transaction.setCurrencyCode( ipInfo.getCurrencyISO() );
          retVal = true;
        }
      }
      else    // user specified a country code to use for currency lookup
      {
        CountryCodeConverter converter = CountryCodeConverter.getInstance();
        String currencyCode = converter.getCurrencyCode( countryCode );
      
        if ( currencyCode != null && !currencyCode.trim().equals("") )
        {
          Transaction.setCurrencyCode( currencyCode );
          retVal = true;
        }
        else    // failed to find a currency
        {
          setError( TridentApiConstants.ER_CURRENCY_LOOKUP_FAILED );
        }
      }        
    }
    catch( Exception e )
    {
      logEntry("doTask()",e.toString());
      setError( TridentApiConstants.ER_CURRENCY_LOOKUP_FAILED );
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/