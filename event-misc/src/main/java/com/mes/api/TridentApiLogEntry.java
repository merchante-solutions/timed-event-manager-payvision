/*@lineinfo:filename=TridentApiLogEntry*//*@lineinfo:user-code*//*@lineinfo:1^1*/
/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/TridentApiLogEntry.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

public class TridentApiLogEntry extends TridentApiTransactionTask
{
  private   String      RequestMethod       = "TPG";
  
  public TridentApiLogEntry( TridentApiTransaction tran )
  {
    super(tran);
  }

  public boolean doTask()
  {
    long       recId       = 0L;
    boolean    retVal      = true;
    String     retrievalReferenceNunmber  = null;

    try
    {
      connect();
      setAutoCommit(false);  
      
      /*@lineinfo:generated-code*//*@lineinfo:45^7*/

//  ************************************************************
//  #sql [Ctx] { select  trident_api_log_sequence.nextval 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trident_api_log_sequence.nextval  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.api.TridentApiLogEntry",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:49^7*/
      
      if ( Transaction.VisaDResponse != null )
      {
        retrievalReferenceNunmber = Transaction.VisaDResponse.getRetrievalReferenceNumber();
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:56^7*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_api_access_log       
//          (
//             profile_id,
//             merchant_number,                   
//             request_date,
//             request_params,
//             response,
//             server_name,
//             request_time,
//             rec_id,
//             response_full,
//             transaction_type,
//             transaction_duration,
//             switch_duration,
//             trident_tran_id,
//             retrieval_ref_num,
//             currency_code,
//             request_method
//          )                                     
//          values                                
//          ( 
//            :Transaction.getProfileId(),
//            :Transaction.getMerchantId(),
//            sysdate,
//            :Transaction.getRequestData(),                    
//            :Transaction.encodeResponse(),                    
//            :Transaction.getServerNameEncoded(),                    
//            :Transaction.getRequestTimestamp(),
//            :recId,
//            null,
//            :Transaction.getTranType(),
//            :Transaction.getTransactionDuration(),
//            :Transaction.getSwitchDuration(),
//            :Transaction.getTridentTranId(),          
//            :retrievalReferenceNunmber,
//            :Transaction.getCurrencyCode(),
//            'TPG'
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_270 = Transaction.getProfileId();
 long __sJT_271 = Transaction.getMerchantId();
 String __sJT_272 = Transaction.getRequestData();
 String __sJT_273 = Transaction.encodeResponse();
 String __sJT_274 = Transaction.getServerNameEncoded();
 java.sql.Timestamp __sJT_275 = Transaction.getRequestTimestamp();
 String __sJT_276 = Transaction.getTranType();
 long __sJT_277 = Transaction.getTransactionDuration();
 long __sJT_278 = Transaction.getSwitchDuration();
 String __sJT_279 = Transaction.getTridentTranId();
 String __sJT_280 = Transaction.getCurrencyCode();
   String theSqlTS = "insert into trident_api_access_log       \n        (\n           profile_id,\n           merchant_number,                   \n           request_date,\n           request_params,\n           response,\n           server_name,\n           request_time,\n           rec_id,\n           response_full,\n           transaction_type,\n           transaction_duration,\n           switch_duration,\n           trident_tran_id,\n           retrieval_ref_num,\n           currency_code,\n           request_method\n        )                                     \n        values                                \n        ( \n           :1 ,\n           :2 ,\n          sysdate,\n           :3 ,                    \n           :4 ,                    \n           :5 ,                    \n           :6 ,\n           :7 ,\n          null,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,          \n           :12 ,\n           :13 ,\n          'TPG'\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.api.TridentApiLogEntry",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_270);
   __sJT_st.setLong(2,__sJT_271);
   __sJT_st.setString(3,__sJT_272);
   __sJT_st.setString(4,__sJT_273);
   __sJT_st.setString(5,__sJT_274);
   __sJT_st.setTimestamp(6,__sJT_275);
   __sJT_st.setLong(7,recId);
   __sJT_st.setString(8,__sJT_276);
   __sJT_st.setLong(9,__sJT_277);
   __sJT_st.setLong(10,__sJT_278);
   __sJT_st.setString(11,__sJT_279);
   __sJT_st.setString(12,retrievalReferenceNunmber);
   __sJT_st.setString(13,__sJT_280);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:96^7*/

      /*@lineinfo:generated-code*//*@lineinfo:98^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:101^7*/
    }
    catch( Exception e )
    {
      retVal = false;
      try{ /*@lineinfo:generated-code*//*@lineinfo:106^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:106^34*/ }catch( Exception ee){} // rollback tran
      String tranId = "n/a";
      try{ tranId = Transaction.getTridentTranId(); } catch( Exception ee ){}
      logEntry("doTask(" + tranId + ")",e.toString());
      try{ /*@lineinfo:generated-code*//*@lineinfo:110^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:110^32*/ }catch( Exception ee){}   // commit log entry
      System.out.println("doTask(" + tranId + "): " + e.toString());
    }
    finally
    {
      setAutoCommit(true);    // restore auto commit
      cleanUp(); 
    }
    return( retVal );
  }

}/*@lineinfo:generated-code*/