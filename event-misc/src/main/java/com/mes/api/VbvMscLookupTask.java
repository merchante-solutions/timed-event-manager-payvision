/*@lineinfo:filename=VbvMscLookupTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/VbvMscLookupTask.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import com.cardinalcommerce.client.CentinelRequest;
import com.cardinalcommerce.client.CentinelResponse;
import com.mes.support.StringUtilities;

public class VbvMscLookupTask extends TridentApiTransactionTask
{
  public VbvMscLookupTask( TridentApiTransaction tran )
  {
    super(tran);
  }
  
  public boolean doTask()
  {
    CentinelRequest     centinelRequest   = null;
    CentinelResponse    centinelResponse  = null;
    int                 idx               = -1;
    boolean             retVal            = false;
    
    try
    {
      connect();
      
      // strip the decimal from the amount
      StringBuffer amount = new StringBuffer(Transaction.getTranAmountFormatted());
      while( (idx = amount.indexOf(".")) >= 0 )
      {
        amount.deleteCharAt(idx);
      }
      
      // build the Cardinal Centinel request to determine if 
      // this cardholder is eligible for Verified By Visa (vbv)
      // or MasterCard Secure Code (msc)
      //
      centinelRequest = new CentinelRequest();
      
      // required fields
	    centinelRequest.add("Version",          TridentApiConstants.CENTINEL_MESSAGE_VERSION);
	    centinelRequest.add("MsgType",          "cmpi_lookup");
	    centinelRequest.add("ProcessorId",      TridentApiConstants.CENTINEL_PROCESSOR_ID);
	    centinelRequest.add("MerchantId",       String.valueOf(Transaction.getMerchantId()));
	    centinelRequest.add("TransactionPwd",	  Transaction.getProfileKey());
	    centinelRequest.add("TransactionType",	"C");
	    centinelRequest.add("Amount",			      amount.toString());
	    centinelRequest.add("CurrencyCode",		  Transaction.getCurrencyCode());
	    centinelRequest.add("CardNumber",		    Transaction.getCardNumberFull());
	    centinelRequest.add("CardExpMonth",		  StringUtilities.rightJustify(String.valueOf(Transaction.getExpDateMonth()), 2,'0'));
	    centinelRequest.add("CardExpYear",		  StringUtilities.rightJustify(String.valueOf(Transaction.getExpDateYear()), 4, '0'));
	    centinelRequest.add("OrderNumber",		  Transaction.getPurchaseId());
      
      // optional fields
	    centinelRequest.add("OrderDescription", "");
	    centinelRequest.add("UserAgent",        "");
	    centinelRequest.add("BrowserHeader",    "");
	    centinelRequest.add("IPAddress",		    Transaction.getIpAddress());
	    centinelRequest.add("EMail",			      "");
     
      // post the request to Cardinal Commerce 
    	centinelResponse = centinelRequest.sendHTTP(Transaction.getCardinalCentinelUrl(), TridentApiConstants.CENTINEL_TIMEOUT_CONNECT, TridentApiConstants.CENTINEL_TIMEOUT_READ);
      
      // process the Cardinal Centinel response
      String errorCode = centinelResponse.getValue("ErrorNo");
      if ( errorCode.equals( TridentApiConstants.CENTINEL_ERROR_NONE ) )
      {
        Transaction.set3DResponseData( centinelResponse );
        retVal = true;
      }
      else  // return the centinel error code and description
      {
        setErrorCode( TridentApiConstants.ER_3D_ENROLL_CHECK_FAILED );
        setErrorDesc( centinelResponse.getValue("ErrorDesc") + " (" + errorCode + ")" );
      }
    }
    catch( Exception e )
    {
      setErrorCode(TridentApiConstants.ER_3D_ENROLL_CHECK_FAILED);
      logEntry("doTask()",e.toString());
    }
    finally
    {
      cleanUp(); 
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/