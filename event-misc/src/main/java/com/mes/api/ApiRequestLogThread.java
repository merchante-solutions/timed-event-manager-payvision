/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/ApiRequestLogThread.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.util.Iterator;
import java.util.Map;
import com.mes.database.SleepyCatManager;
import com.mes.support.PropertiesFile;

public class ApiRequestLogThread extends TridentApiSyncThread
{
  private   String          ServerName              = null;
  private   Map             TridentApiRequestTable  = null;

  public ApiRequestLogThread( long sleepTime )
    throws com.sleepycat.je.DatabaseException
  {
    super(sleepTime);

    try
    {
      SleepyCatManager mgr = SleepyCatManager.getInstance(TridentApiConstants.getApiDatabasePath());
      TridentApiRequestTable = mgr.getStoredMap(TridentApiConstants.API_TABLE_API_REQUEST);
    
      PropertiesFile propsFile = new PropertiesFile( TridentApiConstants.PropertiesFilename );
      ServerName = propsFile.getString(TridentApiConstants.PROP_MES_HOST_NAME,("tapi-" + String.valueOf(this)));
    }
    catch( Exception e )
    {
      String encodedName = TridentApiTranBase.encodeServerName(ServerName);
      System.out.println("ApiRequestLogThread constructor - " + e.toString());
      logEntry("ApiRequestLogThread()", e.toString());
    }
  }

  protected void execute( )
  {
    ApiDb               db        = null;
    String              key       = null;
    ApiRequestLogEntry  logEntry  = null;
  
    try
    {
      db = new ApiDb();
      db.connect();
    
      // process transactions
      Iterator iterator = TridentApiRequestTable.keySet().iterator();
      while ( iterator.hasNext() ) 
      {
        key       = (String)iterator.next();
        logEntry  = (ApiRequestLogEntry)TridentApiRequestTable.get(key);
        
        if ( logEntry != null && db._storeApiRequestLogEntry(logEntry) )
        {
          TridentApiRequestTable.remove( key );
        }
      }
    }
    catch( Exception e )
    {
      String encodedName = TridentApiTranBase.encodeServerName(ServerName);
      System.out.println("execute(" + encodedName + "," + logEntry + ") - " + e.toString());
      logEntry("execute(" + encodedName + "," + logEntry + ")", e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch( Exception ee ) {}
    }
  }
}
