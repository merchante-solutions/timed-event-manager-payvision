/*@lineinfo:filename=TridentApiSyncThread*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/TridentApiSyncThread.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.io.InputStream;
import java.sql.PreparedStatement;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.PropertiesFile;
import com.mes.support.SyncLog;

public abstract class TridentApiSyncThread extends SQLJConnectionBase
  implements Runnable
{
  public  static final long     BACKGROUND_THREAD_WAIT_DEFAULT  = 10000L;

  protected boolean   Running       = false;
  private   long      SleepTime     = BACKGROUND_THREAD_WAIT_DEFAULT;  
  protected Thread    SystemThread  = null;
  protected String    ThreadName    = null;
  
  public TridentApiSyncThread( long sleepTime )
  {
    // re-init this connection as a direct connection
    initialize( SQLJConnectionBase.getDirectConnectString() );
    
    SleepTime   = sleepTime;
    ThreadName  = this.getClass().getName();
  }
  
  protected abstract void execute();
  
  public Thread getSystemThread()
  {
    return(SystemThread);
  }
  
  protected void logExecute()
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:61^7*/

//  ************************************************************
//  #sql [Ctx] { update  trident_api_sync_threads
//          set     last_execute = sysdate
//          where   thread_id = :String.valueOf(this)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_281 = String.valueOf(this);
   String theSqlTS = "update  trident_api_sync_threads\n        set     last_execute = sysdate\n        where   thread_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.api.TridentApiSyncThread",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_281);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:66^7*/
      
      // commit in case it autoCommit is
      // disabled by the child class
      /*@lineinfo:generated-code*//*@lineinfo:70^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:73^7*/
    }
    catch( Exception e )
    {
      logEntry( "logExecute()",e.toString() );
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void logEntry(String method, String message)
  {
    try
    {
      PropertiesFile  propsFile   = new PropertiesFile();
      StringBuffer    subject     = new StringBuffer();
      
      try
      {
        propsFile.load("mes.properties");
      }
      catch(Exception e)
      {
        InputStream inputStream = Thread.currentThread().getContextClassLoader()
          .getResourceAsStream("resources/mes.properties");      
        
        propsFile.load(inputStream);
        
        inputStream.close();
      }
      subject.append("Trident API ");
      subject.append(this.getClass().getName());
      subject.append(" failed on host ");
      subject.append(propsFile.getString("com.mes.hostname",ThreadName));
      
      MailMessage.sendSystemErrorEmail(subject.toString(), method + "\n" + message);
    }
    catch(Exception e)
    {
    }
    super.logEntry(method, message);
  }
  
  public void run( )
  {
    System.out.println("Thread " + ThreadName + " starting");
    try
    { 
      Running = true;

      signOn();
      
      while( Running )
      {
        execute();
        logExecute();
        Thread.sleep( SleepTime );
      }
      
      signOff();
    }
    catch( Exception e )
    {
      logEntry("run(" + ThreadName + ")",e.toString());
      System.out.println(ThreadName + ": " + e.toString());
    }
    finally
    {
    }
    System.out.println("Thread " + ThreadName + " exiting");
  }
  
  public void setSleepTime( long sleepTimeMillis )
  {
    if ( sleepTimeMillis > 0 ) 
    {
      SleepTime = sleepTimeMillis;
    }
  }
  
  protected void signOff()
  {
    PreparedStatement   ps        = null;
    SQLJConnectionBase  sqljCon   = null;
    
    try
    {
      // signing off, use a direct connection in case 
      // the connection pool has already shut down.
      sqljCon = new SQLJConnectionBase(SQLJConnectionBase.getDirectConnectString());
      sqljCon.connect();
      
      ps = sqljCon.getPreparedStatement( "delete from trident_api_sync_threads where thread_id = ?" );
      ps.setString(1,String.valueOf(this));
      ps.executeUpdate();
      ps.close();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("signOff()",e.toString());
      System.out.println("signOff() failed: " + e.toString());
    }
    finally
    {
      try{ sqljCon.cleanUp(); } catch(Exception ee) {}
    }
  }
  protected void signOn()
  {
    PropertiesFile    propsFile     = null;
    String            hostName      = null;
    
    try
    {
      connect();
    
      propsFile = new PropertiesFile( TridentApiConstants.PropertiesFilename );
      hostName  = propsFile.getString(TridentApiConstants.PROP_MES_HOST_NAME,"unregistered");
    
      /*@lineinfo:generated-code*//*@lineinfo:194^7*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_api_sync_threads
//          ( host_name, thread_id )
//          values 
//          ( :hostName, :String.valueOf(this) )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_282 = String.valueOf(this);
   String theSqlTS = "insert into trident_api_sync_threads\n        ( host_name, thread_id )\n        values \n        (  :1 ,  :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.api.TridentApiSyncThread",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,hostName);
   __sJT_st.setString(2,__sJT_282);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:200^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:202^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:205^7*/
    }
    catch( Exception e )
    {
      logEntry("signOn()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void start( )
  {
    if ( SystemThread == null )
    {
      SystemThread = new Thread(this);
      SystemThread.start();
    }
  }
  
  public void stop( )
  {
    stop(false);
  }
  
  public void stop( boolean joinThread )
  {
    Running = false;
    if ( joinThread )
    {
      try
      {
        SystemThread.join();
      }
      catch( Exception e )
      {
      }
    }
  }
}/*@lineinfo:generated-code*/