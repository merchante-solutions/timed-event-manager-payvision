/*@lineinfo:filename=RefundTransactionTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/RefundTransactionTask.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Currency;
import org.apache.log4j.Logger;
import org.datacontract.schemas._2004._07.SubmitTransactionPacket;
import org.datacontract.schemas._2004._07.TransactionResponsePacket;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.CurrencyTypesIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.ThreeDLevel;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxCodes;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxInstructionTypesIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxOperationType;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxPaymentMethodsIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxPaymentTypeIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxProcessingTypeIDs;
import org.datacontract.schemas._2004._07.IncrediPay_Common_Settings_Enums.TxRequestTypesIDs;
import com.adyen.services.common.Amount;
import com.adyen.services.payment.ModificationRequest;
import com.adyen.services.payment.ModificationResult;
import com.adyen.services.payment.PaymentLocator;
import com.adyen.services.payment.PaymentPortType;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import com.mes.support.SyncLog;
import com.payvision.gateway.BasicOperationsLocator;
import com.payvision.gateway.BasicOperationsSoap;
import com.payvision.gateway.BasicOperationsSoapStub;
import com.payvision.gateway.TransactionResult;
import br.com.pagador.www.webservice.pagador.ArrayOfTransactionDataRequest;
import br.com.pagador.www.webservice.pagador.ErrorReportDataResponse;
import br.com.pagador.www.webservice.pagador.PagadorTransactionLocator;
import br.com.pagador.www.webservice.pagador.PagadorTransactionSoap;
import br.com.pagador.www.webservice.pagador.RefundCreditCardTransactionRequest;
import br.com.pagador.www.webservice.pagador.RefundCreditCardTransactionResponse;
import br.com.pagador.www.webservice.pagador.TransactionDataRequest;
import br.com.pagador.www.webservice.pagador.TransactionDataResponse;
import net.atpay.transactions.webservices.ATPayTxWS.ATPayTxWSLocator;
import net.atpay.transactions.webservices.ATPayTxWS.IATPayTxWS;

public class RefundTransactionTask extends TridentApiTransactionTask
{
  protected     String          RefundAction    = "";
  
  private static Logger log = Logger.getLogger(RefundTransactionTask.class);
  
  public RefundTransactionTask( TridentApiTransaction tran )
  {
    super(tran);
  }
  
  private boolean adjustAmount( double adjustedAmount )
  {
    boolean       retVal          = false;
    String        tid             = null;
    String        tranId          = null;
    
    try
    {
      connect();
    
      tranId      = Transaction.getTridentTranId();
      tid         = Transaction.getProfileId();
      
      /*@lineinfo:generated-code*//*@lineinfo:88^7*/

//  ************************************************************
//  #sql [Ctx] { update trident_capture_api
//          set   transaction_amount = :adjustedAmount,
//          refunded = 'Y'
//          where trident_tran_id = :tranId 
//                and terminal_id = :tid 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update trident_capture_api\n        set   transaction_amount =  :1 ,\n        refunded = 'Y'\n        where trident_tran_id =  :2  \n              and terminal_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.api.RefundTransactionTask",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,adjustedAmount);
   __sJT_st.setString(2,tranId);
   __sJT_st.setString(3,tid);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:95^7*/
      retVal = (Ctx.getExecutionContext().getUpdateCount() != 0);
    }
    catch( Exception e )
    {
      SyncLog.logThrottled(this.getClass().getName()+"::adjustAmount("+tranId+")", e);
    }
    finally
    {
      cleanUp(); 
    }
    return( retVal );
  }
  
  private boolean canTransactionBeRefunded()
  {
    int           recCount          = 0;
    boolean       retVal            = false;
    String        tranId            = null;
    
    try
    {
      connect();
      
      tranId = Transaction.getTridentTranId();
      
      /*@lineinfo:generated-code*//*@lineinfo:121^7*/

//  ************************************************************
//  #sql [Ctx] { select  count( trident_tran_id ) 
//          from    trident_capture_api             tapi,
//                  trident_capture_api_tran_type   tt
//          where   tapi.trident_tran_id = :tranId and
//                  tt.tran_type = tapi.transaction_type and
//                  nvl(tt.refund_eligible,'N') = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( trident_tran_id )  \n        from    trident_capture_api             tapi,\n                trident_capture_api_tran_type   tt\n        where   tapi.trident_tran_id =  :1  and\n                tt.tran_type = tapi.transaction_type and\n                nvl(tt.refund_eligible,'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.api.RefundTransactionTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tranId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:129^7*/
      retVal = (recCount != 0);
    }
    catch( Exception e )
    {
      SyncLog.logThrottled(this.getClass().getName()+"::canTransactionBeRefunded(" + tranId + ")", e);
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  public boolean doTask()
  {
    long                beginTS         = 0L;
    boolean             retVal          = false;
    String              tranId          = null;

    try
    {
      connect();
      
      tranId = Transaction.getTridentTranId();
      
      RefundAction = "";    // reset the refund action
      
      if( Transaction instanceof AuthorizeNetTransaction )
      {
        loadAuthNetData();  // resolve the GUID
      }
      // check the tran id for this transaction
      if ( !validTranId() )
      {
        setError(TridentApiConstants.ER_INVALID_TRAN_ID);
      }
      else if ( isPinDebitTransaction() )
      {
        setError(TridentApiConstants.ER_DEBIT_TRAN_TYPE_NOT_ALLOWED);
      }
      else
      {
        // load data from the original data (oracle)
        // (e.g. currency code, payvision tran id number, settled flag)
        loadTransactionData();
        
        double  originalAmount  = Transaction.getOriginalTranAmount() 
                                  + Transaction.getLinkedTranAmount();
        double  tranAmount      = Transaction.getTranAmount();
        if ( (tranAmount < 0) || (tranAmount > originalAmount) )
        {
          setError(TridentApiConstants.ER_INVALID_REFUND_AMOUNT);
        }
        else if ( !isTransactionRefundAvailable() )
        {
          setError(TridentApiConstants.ER_REFUNDED_ALREADY);
        }
        else if ( !canTransactionBeRefunded() )
        {
          setError(TridentApiConstants.ER_NOT_REFUND_ELIGIBLE);
        }
        else 
        {
          if ( Transaction.useIntlVendor() )
          {
            beginTS     = System.currentTimeMillis();
            boolean fxEnabled = Transaction.isFxEnabled();
            boolean fxValidationsEnabled = Transaction.areFxValidationsEnabled();
            
            if ( fxValidationsEnabled && fxEnabled && Transaction.isPartialRefund() && Transaction.getFxAmount() == 0.0 )
            {
              setError(TridentApiConstants.ER_FX_AMOUNT_REQUIRED);
            }
            else if ( fxValidationsEnabled && fxEnabled && !Transaction.isFxAmountValid() )
            {
              setError(TridentApiConstants.ER_RATE_CALC_INVALID);
            }
            else
            {
              if ( Transaction.useAdyen() )
              {
                // allow the service to be set to allow a mock 
                // service to be used during unit testing
                PaymentPortType service = (PaymentPortType)getSoapService();
                if ( service == null )
                {
                  // get a reference to the Adyen web service
                  PaymentLocator sl = new PaymentLocator(); 
                  sl.setPaymentHttpPortEndpointAddress(Transaction.getAdyenEndpoint());
                  service = sl.getPaymentHttpPort(); 
                }        
      
                // setup the Adyen HTTP authentication values
                ((org.apache.axis.client.Stub) service)._setProperty(org.apache.axis.client.Call.USERNAME_PROPERTY, Transaction.getAdyenCompanyId() );
                ((org.apache.axis.client.Stub) service)._setProperty(org.apache.axis.client.Call.PASSWORD_PROPERTY, Transaction.getAdyenPassword()  );
                
                ModificationRequest modRequest = new ModificationRequest();
                modRequest.setMerchantAccount( Transaction.getAdyenMerchantId() );
                modRequest.setOriginalReference( Transaction.getAdyenTranId() );
                modRequest.setModificationAmount( new Amount(Transaction.getCurrencyCodeAlpha(),Transaction.getTranAmountAsLong()) );
                ModificationResult result = service.refund(modRequest);
                
                if ( (retVal = "[refund-received]".equals(result.getResponse())) == true )
                {
                  Transaction.setExternalTranId(result.getPspReference());
                }
                else
                {
                  setError(TridentApiConstants.ER_ADYEN_REFUND_FAILED);
                  setErrorDesc(result.getResponse());
                }
              }
              else if ( Transaction.usePayVision() )
              {
                // get a reference to the PayVision web service
                BasicOperationsSoap service = (BasicOperationsSoap)getSoapService();
                if ( service == null )
                {
                  // use the production soap service
                  BasicOperationsLocator  sl =  new BasicOperationsLocator(); 
                  sl.setBasicOperationsSoapEndpointAddress( Transaction.getPayVisionEndpoint() + TridentApiConstants.PAYVISION_BASIC_PATH );
                  service = (BasicOperationsSoapStub)sl.getBasicOperationsSoap(); 
                }
              
                TransactionResult       result  = service.refund(Transaction.getPayVisionMemberId(),
                                                                 Transaction.getPayVisionMemberGuid(),
                                                                 Transaction.getPayVisionTranId(),
                                                                 Transaction.getPayVisionTranGuid(),
                                                                 new BigDecimal(Transaction.getTranAmount()),
                                                                 Transaction.getPayVisionCurrencyCode(),
                                                                 Transaction.getApiTranId());
                                                                 
                // save the result of the payvision transaction                                                        
                Transaction.setPayVisionResult(result);
                if ( (retVal = (result.getResult() == TridentApiConstants.PAYVISION_SUCCESS)) == true )
                {
                  // update the payvision tran id number and guid.
                  Transaction.setTridentTranIdNumber((long)result.getTransactionId());
                  Transaction.setExternalTranId(result.getTransactionGuid().replaceAll("-", ""));
                }
                else
                {
                  setError(TridentApiConstants.ER_PV_REFUND_FAILED);
                  setErrorDesc(Transaction.getPayVisionErrorDesc() +  " (" + Transaction.getPayVisionErrorCode() + ")");
                }
              }
              else if(Transaction.useBraspag()) {
                PagadorTransactionLocator sl = new PagadorTransactionLocator();
                sl.setPagadorTransactionSoapEndpointAddress(Transaction.getBraspagEndpoint());
                PagadorTransactionSoap service = sl.getPagadorTransactionSoap();
                int decDigits = Currency.getInstance(Transaction.getCurrencyCodeAlpha()).getDefaultFractionDigits();
                TransactionDataRequest tdr = new TransactionDataRequest(Transaction.addGuidDashes(Transaction.getExternalTranId()), MesMath.doubleToLong(Transaction.getTranAmount(), decDigits), 0L);

                TransactionDataRequest[] tdrArray = new TransactionDataRequest[1];
                tdrArray[0] = tdr;
                ArrayOfTransactionDataRequest tdrObj = new ArrayOfTransactionDataRequest(tdrArray);
                RefundCreditCardTransactionRequest request = new RefundCreditCardTransactionRequest(Transaction.getBraspagMerchantId(), tdrObj);
                request.setVersion("1.0"); // Required
                request.setRequestId(Transaction.addGuidDashes(Transaction.getTridentTranId()));

                // Process
                RefundCreditCardTransactionResponse result = service.refundCreditCardTransaction(request);
                if(!result.isSuccess()) {
                  ErrorReportDataResponse[] resultArray = result.getErrorReportDataCollection().getErrorReportDataResponse();
                  setErrorCode( TridentApiConstants.ER_BRASPAG_ERROR );
                  setErrorDesc(resultArray[0].getErrorMessage() + " ("+resultArray[0].getErrorCode()+")");
                }
                else {
                  TransactionDataResponse tdResponse = (TransactionDataResponse)result.getTransactionDataCollection().getTransactionDataResponse(0);
                  
                  log.debug("BP getBraspagTransactionId: "+tdResponse.getBraspagTransactionId());
                  log.debug("BP getReturnMessage: "+tdResponse.getReturnMessage());
                  log.debug("BP getReturnCode: "+tdResponse.getReturnCode());
                  log.debug("BP getStatus: "+tdResponse.getStatus());
                  
                  switch(tdResponse.getStatus().intValue()) {
                  case 0:   // Full cancel OK
                  case 3:   // Redecard only cancel OK
                    retVal = true;
                    setErrorCode(TridentApiConstants.ER_NONE);
                    setErrorDesc("Refund Request Accepted");
                    break;
                  case 1:   // Partial cancel OK
                    retVal = true;
                    setErrorCode(TridentApiConstants.ER_NONE);
                    setErrorDesc("Partial Refund Request Accepted");
                    break;
                  case 2:
                    setErrorCode(TridentApiConstants.ER_BRASPAG_ERROR);
                    setErrorDesc("Refund Request Failed ("+tdResponse.getReturnMessage()+")");
                    break;
                  }
                }
              }
              else if ( Transaction.useGts() )
              {
                // get a reference to the PayVision web service
                IATPayTxWS service = (IATPayTxWS)getSoapService();
                if ( service == null )
                {
                  // use the production soap service
                  ATPayTxWSLocator sl = new ATPayTxWSLocator(); 
                  sl.setBasicHttpBinding_IATPayTxWSEndpointAddress(Transaction.getGtsEndpoint());
                  service = sl.getBasicHttpBinding_IATPayTxWS(); 
                }                  
                SubmitTransactionPacket submitRequest = new SubmitTransactionPacket();
                submitRequest.setAccountID(Transaction.getGtsAccountId());
                submitRequest.setSubAccountID(Transaction.getGtsSubAccountId());
                submitRequest.setCustomerOriginatedTxID( Transaction.getTridentTranId() );      // trident tran id
                submitRequest.setOriginalTransactionID(Long.valueOf(Transaction.getExternalTranId()) );  // gts tran id from pre-auth
                submitRequest.setOperationType( TxOperationType.Credit );                       // dci
                submitRequest.setTransactionCode( TxCodes.Internet );
                submitRequest.setRequestType( TxRequestTypesIDs.Submit );                       // Submit for settle, void, and refund
                if ( Transaction.getTranAmount() < Transaction.getOriginalTranAmount() )
                {
                  submitRequest.setInstructionType( TxInstructionTypesIDs.PartialCredit );
                }
                else
                {
                  submitRequest.setInstructionType( TxInstructionTypesIDs.FullCredit );
                }                  
                submitRequest.setAmount( new BigDecimal(Transaction.getTranAmount()) );
                submitRequest.setScrubbingFlag(false);                           // fraud scan
                submitRequest.setThreeDSecureFlags( ThreeDLevel.NoThreeD );                     // three d secure
                submitRequest.setCurrencyType( CurrencyTypesIDs.fromValue(Transaction.getCurrencyCodeAlpha()) );
                submitRequest.setPaymentType( TxPaymentTypeIDs.Regular );
                submitRequest.setPaymentMethodType( TxPaymentMethodsIDs.CreditCard );           // do we need to use DebitCard?
                submitRequest.setProcessingType( TxProcessingTypeIDs.Processing );              // Gateway
        
                // defaults
                submitRequest.setSkinID(9);
                submitRequest.setAccessManagementFlag(false);
                submitRequest.setRecurringBillingFlag(false);
        
                TransactionResponsePacket submitResult = service.submitTransaction(submitRequest);
            
                if ( (retVal = (submitResult.getErrorID().intValue() == TridentApiConstants.GTS_SUCCESS)) == true )
                {
                  Transaction.setExternalTranId(String.valueOf(submitResult.getTransactionID()));
                }
                else
                {
                  setError(TridentApiConstants.ER_GTS_REFUND_FAILED);
                  setErrorDesc(submitResult.getErrorInfo() +  " (" + submitResult.getErrorID() + ")");
                }
              }
              else if ( Transaction.useItps() )
              {
            	  // ITPS Refund...
            	  setError(TridentApiConstants.ER_ITPS_REFUND_FAILED);
                  setErrorDesc("ITPS Refund Not Supported");
              }
              Transaction.setSwitchDuration(System.currentTimeMillis() - beginTS);

              if ( retVal )
              {
                issueRefund();    // update oracle
                RefundAction = "Refund";
              }
            }
          }
          else
          {
            if ( Transaction.isSettled() )
            {
              if ( (retVal = issueRefund()) == true )
              {
                RefundAction = "Credit";
                if ( Transaction.getTranAmount() > 0.0 )
                {
                  RefundAction = (RefundAction + " " + 
                                  MesMath.toCurrency(Transaction.getTranAmount()));
                }
              }
            }
            else    // not settled yet
            {
              // look for a partial reversal request
              double refundAmount  = Transaction.getTranAmount();
              if ( refundAmount > 0.0 && refundAmount < originalAmount )
              {
                // getDoubleString() will clean up any Java floating point 
                // inaccuracy (i.e. when value sb 28.00 and is 27.9999999998)
                double adjAmount = Double.parseDouble(NumberFormatter.getDoubleString((originalAmount - refundAmount),"0.00"));
                if ( (retVal = adjustAmount(adjAmount)) == true )
                {
                  RefundAction = ("Adjustment " + 
                                  MesMath.toCurrency(adjAmount));
                  try
                  {                                
                    loadAuthReversalData();
                    Transaction.setAuthAmount(originalAmount);  // set to original tran amount
                    Transaction.reverseAuthorization();
                  }
                  catch( Exception re )
                  {
                    SyncLog.logThrottled(this.getClass().getName()+"::doTask(reversal-" + tranId + ")", re);
                  }                  
                }
              }
              else if ( (retVal = voidTransaction()) == true )  // issue void
              {
                RefundAction = "Void";
              }
            }   
        
            if ( retVal != true && !hasError() ) 
            {
              // set the generic refund failed error code/message
              setError(TridentApiConstants.ER_REFUND_FAILED);
            }
          }          
        }
      }        
    }
    catch( Exception e )
    {
      retVal = false;
      SyncLog.logThrottled(this.getClass().getName()+"::doTask(reversal-" + tranId + ")", e);
      setError(TridentApiConstants.ER_REFUND_FAILED);
    }
    finally
    {
      cleanUp(); 
    }
    return( retVal );
  }
  
  public String getRefundAction()
  {
    return( RefundAction );
  }
  
  protected boolean isTransactionRefundAvailable( )
  {
    double        creditAvailableAmount = 0.0;
    double        originalAmount        = 0.0;
    boolean       retVal                = false;
    String        tranId                = null;
    int           voidCount             = 0;
    
    try
    {
      connect();
      
      originalAmount  = Transaction.getOriginalTranAmount()
                        + Transaction.getLinkedTranAmount();
      tranId          = Transaction.getTridentTranId();
      
      /*@lineinfo:generated-code*//*@lineinfo:479^7*/

//  ************************************************************
//  #sql [Ctx] { select  count( trident_tran_id ) 
//          from    trident_capture_api tapi
//          where   tapi.trident_tran_id = :tranId and
//                  tapi.transaction_type = :TridentApiTransaction.TT_VOID
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( trident_tran_id )  \n        from    trident_capture_api tapi\n        where   tapi.trident_tran_id =  :1  and\n                tapi.transaction_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.api.RefundTransactionTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tranId);
   __sJT_st.setString(2,TridentApiTransaction.TT_VOID);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   voidCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:485^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:487^7*/

//  ************************************************************
//  #sql [Ctx] { select  (:originalAmount - nvl( sum( transaction_amount ),0 )) 
//          
//          from    trident_capture_api tapi
//          where   tapi.original_trident_tran_id = :tranId and
//                  tapi.transaction_type = :TridentApiTransaction.TT_CREDIT
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ( :1  - nvl( sum( transaction_amount ),0 )) \n         \n        from    trident_capture_api tapi\n        where   tapi.original_trident_tran_id =  :2  and\n                tapi.transaction_type =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.api.RefundTransactionTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDouble(1,originalAmount);
   __sJT_st.setString(2,tranId);
   __sJT_st.setString(3,TridentApiTransaction.TT_CREDIT);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   creditAvailableAmount = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:494^7*/
      
      retVal = ( (Transaction.getTranAmount() <= creditAvailableAmount) && (voidCount == 0) );
    }
    catch( Exception e )
    {
      SyncLog.logThrottled(this.getClass().getName()+"::isTransactionRefundAvailable(" + tranId + ")", e);
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected boolean issueRefund()
  {
    double        fxAmount        = 0.0;
    long          recId           = 0L;
    boolean       retVal          = false;
    String        tid             = null;
    double        tranAmount      = 0.0;
    String        tranId          = null;
    String batchId = null;
    String batchNumber = null;
    
    try
    {
      connect();
      
      // Retrieve the current batch ID/num from the profile.
      {
        String q = "select current_batch_id, current_batch_number from trident_profile_api where terminal_id = ?";
         PreparedStatement ps = getPreparedStatement(q);
         ps.setString(1, Transaction.getProfileId());
         ResultSet rs = ps.executeQuery();
         if(rs.next()) {
           batchId = rs.getString("current_batch_id");
           batchNumber = rs.getString("current_batch_number");
         }
         ps.close();
         rs.close();
      }
      
      tranId      = Transaction.getTridentTranId();
      tid         = Transaction.getProfileId();
      tranAmount  = Transaction.getTranAmount();
      fxAmount    = Transaction.getFxAmount();
      
      // get a new rec id.  required to store blob
      /*@lineinfo:generated-code*//*@lineinfo:544^7*/

//  ************************************************************
//  #sql [Ctx] { select  trident_capture_api_sequence.nextval 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trident_capture_api_sequence.nextval  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.api.RefundTransactionTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:548^7*/
      
      // if necessary, override the load filename so the 
      // tran will be skipped by the settlement processes 
      String  loadFilename      = null;
      
      // non-USD and USD via international vendors should be skipped
      if ( Transaction.useAdyen() )
      {
        loadFilename      = "processed-adyen";
      }
      else if ( Transaction.usePayVision() )
      {
        loadFilename      = "processed-payvision";
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:564^7*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_capture_api
//          (
//             rec_id,
//             terminal_id,
//             merchant_number,
//             dba_name,
//             dba_city,
//             dba_state,
//             dba_zip,
//             sic_code,
//             country_code,
//             currency_code,
//             phone_number,
//             card_number,
//             card_number_enc,
//             transaction_date,
//             debit_credit_indicator,
//             transaction_amount,
//             transaction_type,
//             reference_number,
//             client_reference_number,
//             purchase_id,
//             pos_entry_mode,
//             card_present,
//             original_trident_tran_id,
//             trident_tran_id,
//             trident_tran_id_number,
//             external_tran_id,
//             emulation,
//             server_name,
//             test_flag,
//             fx_rate_id,
//             fx_rate,
//             fx_amount_base,
//             fx_reference_number,
//             fx_post_status,
//             fx_post_ts,
//             load_filename,
//             batch_id,
//             batch_number,
//             ach_sequence,
//             batch_close_timestamp,
//             load_file_id
//          )
//          select  :recId,
//                  tapi.terminal_id,
//                  tapi.merchant_number,
//                  tapi.dba_name,
//                  tapi.dba_city,
//                  tapi.dba_state,
//                  tapi.dba_zip,
//                  tapi.sic_code,
//                  tapi.country_code,
//                  tapi.currency_code,
//                  tapi.phone_number,
//                  tapi.card_number,
//                  tapi.card_number_enc,
//                  trunc(sysdate),
//                  'C',    -- credit
//                  least(tapi.transaction_amount,
//                        decode(:tranAmount,
//                               0,tapi.transaction_amount,
//                               :tranAmount)),
//                  :TridentApiTransaction.TT_CREDIT,
//                  :Transaction.getReferenceNumber(),
//                  nvl(:Transaction.getClientReferenceNumber(),tapi.client_reference_number),
//                  nvl(:Transaction.getPurchaseId(),tapi.purchase_id),
//                  '01',   -- always keyed
//                  'N',    -- CNP
//                  tapi.trident_tran_id,           -- save original id
//                  :Transaction.getApiTranId(),  -- use api generated tran id
//                  :Transaction.getTridentTranIdNumber(),
//                  :Transaction.getExternalTranId(),
//                  :Transaction.getEmulationString(),
//                  :Transaction.getServerNameEncoded(),
//                  tapi.test_flag,                 -- use same test flag setting
//                  tapi.fx_rate_id                               as fx_rate_id,
//                  tapi.fx_rate                                  as fx_rate,
//                  decode( :fxAmount, 
//                          0, decode( nvl(tapi.fx_rate,0), -- skip if there is no rate
//                                     0, 0,
//                                     decode( :tranAmount,
//                                             tapi.transaction_amount,tapi.fx_amount_base,
//                                             round((:tranAmount/tapi.fx_rate),2) )
//                                   ),
//                          :fxAmount )                           as fx_amount_base,
//                  tapi.fx_reference_number                      as fx_ref_num,
//                  decode(nvl(tapi.fx_rate_id,0),0,null,'P')     as fx_post_status,
//                  null                                          as fx_post_ts,
//                  :loadFilename                                 as load_filename,
//                  :batchId as batch_id,
//                  :batchNumber as batch_number,
//                  0 as ach_sequence,
//                  sysdate as batch_close_timestamp,
//                  -1 as load_file_id
//          from    trident_capture_api           tapi,
//                  trident_capture_api_tran_type tt
//          where   tapi.trident_tran_id = :tranId and
//                  tt.tran_type = tapi.transaction_type and
//                  nvl(tt.refund_eligible,'N') = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_142 = Transaction.getReferenceNumber();
 String __sJT_143 = Transaction.getClientReferenceNumber();
 String __sJT_144 = Transaction.getPurchaseId();
 String __sJT_145 = Transaction.getApiTranId();
 long __sJT_146 = Transaction.getTridentTranIdNumber();
 String __sJT_147 = Transaction.getExternalTranId();
 String __sJT_148 = Transaction.getEmulationString();
 String __sJT_149 = Transaction.getServerNameEncoded();
   String theSqlTS = "insert into trident_capture_api\n        (\n           rec_id,\n           terminal_id,\n           merchant_number,\n           dba_name,\n           dba_city,\n           dba_state,\n           dba_zip,\n           sic_code,\n           country_code,\n           currency_code,\n           phone_number,\n           card_number,\n           card_number_enc,\n           transaction_date,\n           debit_credit_indicator,\n           transaction_amount,\n           transaction_type,\n           reference_number,\n           client_reference_number,\n           purchase_id,\n           pos_entry_mode,\n           card_present,\n           original_trident_tran_id,\n           trident_tran_id,\n           trident_tran_id_number,\n           external_tran_id,\n           emulation,\n           server_name,\n           test_flag,\n           fx_rate_id,\n           fx_rate,\n           fx_amount_base,\n           fx_reference_number,\n           fx_post_status,\n           fx_post_ts,\n           load_filename,\n           batch_id,\n           batch_number,\n           ach_sequence,\n           batch_close_timestamp,\n           load_file_id\n        )\n        select   :1 ,\n                tapi.terminal_id,\n                tapi.merchant_number,\n                tapi.dba_name,\n                tapi.dba_city,\n                tapi.dba_state,\n                tapi.dba_zip,\n                tapi.sic_code,\n                tapi.country_code,\n                tapi.currency_code,\n                tapi.phone_number,\n                tapi.card_number,\n                tapi.card_number_enc,\n                trunc(sysdate),\n                'C',    -- credit\n                least(tapi.transaction_amount,\n                      decode( :2 ,\n                             0,tapi.transaction_amount,\n                              :3 )),\n                 :4 ,\n                 :5 ,\n                nvl( :6 ,tapi.client_reference_number),\n                nvl( :7 ,tapi.purchase_id),\n                '01',   -- always keyed\n                'N',    -- CNP\n                tapi.trident_tran_id,           -- save original id\n                 :8 ,  -- use api generated tran id\n                 :9 ,\n                 :10 ,\n                 :11 ,\n                 :12 ,\n                tapi.test_flag,                 -- use same test flag setting\n                tapi.fx_rate_id                               as fx_rate_id,\n                tapi.fx_rate                                  as fx_rate,\n                decode(  :13 , \n                        0, decode( nvl(tapi.fx_rate,0), -- skip if there is no rate\n                                   0, 0,\n                                   decode(  :14 ,\n                                           tapi.transaction_amount,tapi.fx_amount_base,\n                                           round(( :15 /tapi.fx_rate),2) )\n                                 ),\n                         :16  )                           as fx_amount_base,\n                tapi.fx_reference_number                      as fx_ref_num,\n                decode(nvl(tapi.fx_rate_id,0),0,null,'P')     as fx_post_status,\n                null                                          as fx_post_ts,\n                 :17                                  as load_filename,\n                 :18  as batch_id,\n                 :19  as batch_number,\n                0 as ach_sequence,\n                sysdate as batch_close_timestamp,\n                -1 as load_file_id\n        from    trident_capture_api           tapi,\n                trident_capture_api_tran_type tt\n        where   tapi.trident_tran_id =  :20  and\n                tt.tran_type = tapi.transaction_type and\n                nvl(tt.refund_eligible,'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.api.RefundTransactionTask",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setDouble(2,tranAmount);
   __sJT_st.setDouble(3,tranAmount);
   __sJT_st.setString(4,TridentApiTransaction.TT_CREDIT);
   __sJT_st.setString(5,__sJT_142);
   __sJT_st.setString(6,__sJT_143);
   __sJT_st.setString(7,__sJT_144);
   __sJT_st.setString(8,__sJT_145);
   __sJT_st.setLong(9,__sJT_146);
   __sJT_st.setString(10,__sJT_147);
   __sJT_st.setString(11,__sJT_148);
   __sJT_st.setString(12,__sJT_149);
   __sJT_st.setDouble(13,fxAmount);
   __sJT_st.setDouble(14,tranAmount);
   __sJT_st.setDouble(15,tranAmount);
   __sJT_st.setDouble(16,fxAmount);
   __sJT_st.setString(17,loadFilename);
   __sJT_st.setString(18,batchId);
   __sJT_st.setString(19,batchNumber);
   __sJT_st.setString(20,tranId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:666^7*/
      retVal = (Ctx.getExecutionContext().getUpdateCount() != 0);
      
      if ( retVal )
      {
        // return the tran id of the credit transaction
        Transaction.setTridentTranId( Transaction.getApiTranId() );
      }
    }
    catch( Exception e )
    {
      SyncLog.logThrottled(this.getClass().getName()+"::issueRefund(" + tranId + ")", e);
    }
    finally
    {
      cleanUp(); 
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/