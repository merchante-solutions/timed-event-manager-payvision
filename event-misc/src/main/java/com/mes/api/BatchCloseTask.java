/*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/api/BatchCloseTask.sqlj $

  Description:

  Last Modified By   : $Author: brice $
  Last Modified Date : $LastChangedDate: 2013-06-18 11:49:02 -0700 (Tue, 18 Jun 2013) $
  Version            : $Revision: 21199 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

 **************************************************************************/
package com.mes.api;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.support.MesMath;
import com.mes.support.SyncLog;

/**
 * This task will close the batch for the profile contained in the {@link TridentApiTransaction}.
 * @author brice
 *
 */
public class BatchCloseTask extends TridentApiTransactionTask {

  private static final long serialVersionUID = 1L;
  private String ProfileId = null;
  private boolean TestMode = false;
  private Logger log = Logger.getLogger(BatchCloseTask.class);
  private int batchNum;
  private long batchId;

  public BatchCloseTask(TridentApiTransaction tran) {
    super(tran);
  }

  public boolean doTask() {
    boolean retVal = false;
    ProfileId = Transaction.getProfileId();
    ApiDb apiDb = new ApiDb();
    retVal = apiDb._closeBatch(ProfileId);
    batchNum = apiDb.getAttemptedBatchCloseNumber();
    batchId = apiDb.getAttemptedBatchCloseId();
    return (retVal);
  }

  public String getBatchCloseInfo() {
    PreparedStatement ps = null;
    ResultSet rs = null;
    StringBuffer retVal = new StringBuffer("");
    String profileId = Transaction.getProfileId();

    try {
      connect();
      String q = "select " + 
          "  sum(" + 
          "    decode(" + 
          "      tapi.debit_credit_indicator, 'D', " + 
          "      1, 0" + 
          "    )" + 
          "  ) as sales_count, " + 
          "  sum(" + 
          "    decode(" + 
          "      tapi.debit_credit_indicator, 'D', " + 
          "      1, 0" + 
          "    ) * tapi.transaction_amount" + 
          "  ) as sales_amount, " + 
          "  sum(" + 
          "    decode(" + 
          "      tapi.debit_credit_indicator, 'C', " + 
          "      1, 0" + 
          "    )" + 
          "  ) as credits_count, " + 
          "  sum(" + 
          "    decode(" + 
          "      tapi.debit_credit_indicator, 'C', " + 
          "      1, 0" + 
          "    ) * tapi.transaction_amount" + 
          "  ) as credits_amount, " + 
          "  count(1) as total_count, " + 
          "  sum(" + 
          "    decode(" + 
          "      tapi.debit_credit_indicator, 'C', " + 
          "      - 1, 1" + 
          "    ) * tapi.transaction_amount" + 
          "  ) as net_amount " + 
          "from " + 
          "  trident_profile tp, " + 
          "  trident_capture_api tapi " + 
          "where " + 
          "  tp.terminal_id = ? " + 
          "  and tapi.merchant_number = tp.merchant_number " + 
          "  and tapi.batch_number = ? " +  // Batch number has to match the one we attempted to settle
          "  and tapi.batch_id = ? " +      // Batch ID has to match the one we attempted to settle
          "  and tapi.test_flag = 'N' " +   // No test transactions
          "  and tapi.terminal_id = tp.terminal_id" +
          "  and tapi.transaction_type in " + // Tran type can settle
          "    ( " + 
          "      select  tt.tran_type " + 
          "      from    trident_capture_api_tran_type tt " + 
          "      where   nvl(tt.settle,'N') = 'Y' " + 
          "    ) ";

      ps = getPreparedStatement(q);
      ps.setString(1, profileId);
      ps.setInt(2, batchNum);
      ps.setLong(3, batchId);
      rs = ps.executeQuery();
      rs.next();

      if (rs.getInt("total_count") > 0) {
        retVal.append("Batch ");
        retVal.append(batchNum);
        retVal.append(" Closed - Sales: ");
        retVal.append(rs.getInt("sales_count"));
        retVal.append(" for ");
        retVal.append(MesMath.toCurrency(rs.getDouble("sales_amount")));
        retVal.append(" Credits: ");
        retVal.append(rs.getInt("credits_count"));
        retVal.append(" for ");
        retVal.append(MesMath.toCurrency(rs.getDouble("credits_amount")));
        retVal.append(" Net: ");
        retVal.append(MesMath.toCurrency(rs.getDouble("net_amount")));
      }
      else {
        retVal.append("Empty Batch");
      }
    } catch (Exception e) {
      log.error(e);
      SyncLog.logThrottled(this.getClass().getName() + "::getBatchCloseInfo", e);
    } finally {
      cleanUp(ps, rs);
    }
    return (retVal.toString());
  }

  public void setTestMode(boolean testMode) {
    TestMode = testMode;
  }
}