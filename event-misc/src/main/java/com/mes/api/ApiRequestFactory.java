/*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/api/ApiRequestFactory.java $

  Description:
  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2011-10-10 17:49:55 -0700 (Mon, 10 Oct 2011) $
  Version            : $Revision: 19393 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

public class ApiRequestFactory
{
  public static TridentApiRequest createForceRequest( String[] credentials)
  {
    return( createRequest(credentials,TridentApiTranBase.TT_FORCE) );
  }

  public static TridentApiRequest createRefundRequest( String[] credentials, String tranId, double[] amounts )
  {
    TridentApiRequest   request   = createRequest(credentials,TridentApiTranBase.TT_REFUND);
    request.setArg(TridentApiConstants.FN_TRAN_ID,tranId);
    if ( amounts != null )
    {
      request.setArg(TridentApiConstants.FN_AMOUNT,amounts[0]);
      if ( amounts[1] != 0.0 )
      {
        request.setArg(TridentApiConstants.FN_FX_AMOUNT,amounts[1]);
      }
    }
    return( request );
  }

  public static TridentApiRequest createRequest( String[] credentials, String tranType )
  {
    TridentApiRequest   request   = new TridentApiRequest();
    
    request.setArg(TridentApiConstants.FN_TID,credentials[0]);
    request.setArg(TridentApiConstants.FN_TERM_PASS,credentials[1]);
    request.setArg(TridentApiConstants.FN_TRAN_TYPE,tranType);
    return( request );
  }
  
  public static TridentApiRequest createSettleRequest( String[] credentials, String tranId )
  {
    return( createSettleRequest(credentials,tranId,null) );
  }
  
  public static TridentApiRequest createSettleRequest( String[] credentials, String tranId, double[] amounts )
  {
    TridentApiRequest   request   = createRequest(credentials,TridentApiTranBase.TT_SETTLE);
    request.setArg(TridentApiConstants.FN_TRAN_ID,tranId);
    if ( amounts != null )
    {
      request.setArg(TridentApiConstants.FN_AMOUNT,amounts[0]);
      if ( amounts[1] != 0.0 )
      {
        request.setArg(TridentApiConstants.FN_FX_AMOUNT,amounts[1]);
      }
    }
    return( request );
  }
  
  public static TridentApiRequest createVoidRequest( String[] credentials, String tranId )
  {
    TridentApiRequest   request   = createRequest(credentials,TridentApiTranBase.TT_VOID);
    request.setArg(TridentApiConstants.FN_TRAN_ID,tranId);
    return( request );
  }
}
