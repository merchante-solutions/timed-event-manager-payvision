/*@lineinfo:filename=RateLookupTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/RateLookupTask.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

public class RateLookupTask extends TridentApiTransactionTask
{
  public RateLookupTask( TridentApiTransaction tran )
  {
    super(tran);
  }
  
  public boolean doTask()
  {
    String    currencyCode  = null;
    boolean   retVal        = false;
    
    try
    {
      connect();
      
      RateTable rateTable = new RateTable( Transaction.getMerchantId() );
      
      currencyCode = Transaction.getCurrencyCodeAlpha();
      
      if ( currencyCode == null || currencyCode.equals("") ||
           currencyCode.equals(TridentApiConstants.FV_CURRENCY_CODE_ALPHA_DEFAULT) )
      {
        Transaction.setFxRateTableXml(rateTable.toXml());
        retVal = true;
      }
      else
      {
        ForeignExchangeRate fxRate = rateTable.getRateByCurrencyCode(currencyCode);
        if ( fxRate != null )
        {
          Transaction.setFxRateXml(fxRate.toXml());
          retVal = true;
        }
        else
        {
          setErrorCode(TridentApiConstants.ER_INVALID_RATE_ID);
        }
      }        
    }
    catch( Exception e )
    {
      setErrorCode(TridentApiConstants.ER_FX_RATE_LOOKUP_FAILED);
      logEntry("doTask()",e.toString());
    }
    finally
    {
      cleanUp(); 
    }
    
    return( retVal );
  }
}/*@lineinfo:generated-code*/