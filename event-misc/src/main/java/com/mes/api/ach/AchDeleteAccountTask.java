package com.mes.api.ach;

import org.apache.log4j.Logger;
import com.mes.api.TridentApiTransaction;
import com.mes.api.TridentApiTransactionTask;

/**
 * Stores ACH account data in the ACH account data table in database.
 */
public class AchDeleteAccountTask extends TridentApiTransactionTask
{
  static Logger log = Logger.getLogger(AchDeleteAccountTask.class);

  private AchDeleteRequest request;

  public AchDeleteAccountTask(AchDeleteRequest request)
  {
    super(request);
    this.request = request;
  }

  public boolean doTask()
  {
    try
    {
      return AchDb.deleteAchAccountData(request);
    }
    catch (AchValidationException ve)
    {
      setError(ve.getErrorCode());
    }
    return false;
  }

  public static boolean doTask(AchDeleteRequest request)
  {
    return (new AchDeleteAccountTask(request)).doTask();
  }
  public static boolean doTask(TridentApiTransaction tran)
  {
    return doTask((AchDeleteRequest)tran);
  }
}



