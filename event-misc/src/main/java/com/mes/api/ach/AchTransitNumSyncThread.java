package com.mes.api.ach;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiSyncThread;
import com.mes.database.SleepyCatManager;
import com.sleepycat.bind.serial.SerialSerialKeyCreator;
import com.sleepycat.collections.StoredMap;
import com.sleepycat.collections.StoredSortedMap;

public class AchTransitNumSyncThread extends TridentApiSyncThread
{
  static Logger log = Logger.getLogger(AchTransitNumSyncThread.class);

  private StoredMap recMap = null;      // primary key map
  private StoredSortedMap idMap = null; // entries sorted by record id map

  public AchTransitNumSyncThread(long sleepTime)
  {
    super(sleepTime);

    try
    {
      SleepyCatManager mgr = 
        SleepyCatManager.getInstance(TridentApiConstants.getApiDatabasePath());

      recMap = mgr.getStoredMap(TridentApiConstants.API_TABLE_ACH_TRANSIT_NUMS);

      // create a stored map based on a secondary index for the entry table
      // that uses entry tran dates as the sorted key
      idMap = mgr.createSecondaryStoredMap(
        TridentApiConstants.API_TABLE_ACH_TRANSIT_NUMS,
        TridentApiConstants.API_SI_ACH_TN_REC_IDS,
        new SerialSerialKeyCreator(mgr.getClassCatalog(),String.class,
          TransitNumRecord.class,Integer.class)
        {
          public Object createSecondaryKey(Object primKeyInput, Object valueInput)
          {
            TransitNumRecord transitNum = (TransitNumRecord)valueInput;
            return transitNum.getRecId();
          }
        });

    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  private int tableSize;

  protected void execute()
  {
    try
    {
      AchDb db = new AchDb();

      // fetch most recent date
      Integer lastRecId = null;
      if (!idMap.isEmpty())
      {
        lastRecId = (Integer)idMap.lastKey();
      }
      else
      {
        lastRecId = 0;
      }

      // load up to 100 new transit nums at a time
      List newRecs = db.lookupNewTransitNumRecords(lastRecId,500);
      for (Iterator i = newRecs.iterator(); i.hasNext();)
      { 
        TransitNumRecord rec = (TransitNumRecord)i.next();
        recMap.put(rec.getTransitNum(),rec);
      }

      if (tableSize < recMap.size())
      {
        tableSize = recMap.size();
        log.debug("Transit num table increased to " + tableSize + " records");
      }
    }
    catch (Exception e)
    {
      log.error("Execute error: " + e);
      e.printStackTrace();
    }
  }
}
    
