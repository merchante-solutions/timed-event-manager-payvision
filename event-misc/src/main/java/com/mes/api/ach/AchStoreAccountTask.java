package com.mes.api.ach;

import org.apache.log4j.Logger;
import com.mes.api.TridentApiTransaction;
import com.mes.api.TridentApiTransactionTask;

/**
 * Stores ACH account data in the ACH account data table in database.
 */
public class AchStoreAccountTask extends TridentApiTransactionTask
{
  static Logger log = Logger.getLogger(AchStoreAccountTask.class);

  private AchStoreRequest request;

  public AchStoreAccountTask(AchStoreRequest request)
  {
    super(request);
    this.request = request;
  }

  public boolean doTask()
  {
    try
    {
      return AchDb.storeAchAccountData(request);
    }
    catch (AchValidationException ve)
    {
      setError(ve.getErrorCode());
    }
    return true;
  }

  public static boolean doTask(AchStoreRequest request)
  {
    return (new AchStoreAccountTask(request)).doTask();
  }
  public static boolean doTask(TridentApiTransaction tran)
  {
    return doTask((AchStoreRequest)tran);
  }
}
