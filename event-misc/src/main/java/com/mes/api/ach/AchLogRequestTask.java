package com.mes.api.ach;

import com.mes.api.TridentApiTransaction;
import com.mes.api.TridentApiTransactionTask;

/**
 * Used by TridentApiServletWorker to store ach request in the database.
 */
public class AchLogRequestTask extends TridentApiTransactionTask
{
  private AchRequest request;
  
  public AchLogRequestTask(AchRequest request)
  {
    super(request);
    this.request = request;
  }
  
  public boolean doTask()
  {
    return AchDb.storeAchRequest(request);
  }

  public static boolean doTask(AchRequest request)
  {
    return (new AchLogRequestTask(request)).doTask();
  }
  public static boolean doTask(TridentApiTransaction tran)
  {
    return doTask((AchRequest)tran);
  }
}