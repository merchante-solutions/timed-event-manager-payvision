package com.mes.api.bml.test;

import com.mes.tools.DropDownTable;

public class ProductionCodeTable extends DropDownTable
{
  public ProductionCodeTable()
  {
    addElement("","--");
    addElement("CNC","Cash & Carry");
    addElement("DIG","Digital Good");
    addElement("PHY","Physical Delivery");
    addElement("SVC","Service");
    addElement("TBD","To Be Determined");
  }
}
