package com.mes.api.bml;

import java.util.LinkedList;

public class BmlIdCache 
{
  public final static int cacheSize = 100;

  // singleton instance
  private static BmlIdCache cache = new BmlIdCache();

  /**
   * Thread safe internal interface
   */

  private BmlIdCache()
  {
  }

  private LinkedList idQueue = new LinkedList();

  private synchronized int getNext()
  {
    try
    {
      if (!idQueue.isEmpty())
      {
        return (Integer) idQueue.removeFirst();
      }
    }
    catch (Exception e)
    {
      System.out.println("Error fetching id: " + e);
    }

    return -1;
  }

  private synchronized void add(Integer id)
  {
    idQueue.add(id);
  }

  private synchronized int getCount()
  {
    return idQueue.size();
  }

  /**
   * Static public interface
   */

  public static int next()
  {
    return cache.getNext();
  }

  public static void add(int id)
  {
    cache.add(id);
  }

  public static boolean isFull()
  {
    return cache.getCount() >= cacheSize;
  }

}