package com.mes.api.bml.test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.tools.DropDownTable;

public class PromoCodeTable extends DropDownTable
{
  static Logger log = Logger.getLogger(PromoCodeTable.class);

  public PromoCodeTable(String emptySelector)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    if (emptySelector != null)
    {
      addElement("",emptySelector);
    }

    try
    {
      connect();

      ps = con.prepareStatement(
        " select              " +
        "  promo_code, label  " +
        " from                " +
        "  bml_promo_codes    " +
        " order by order_idx  ");

      rs = ps.executeQuery();

      while (rs.next())
      {
        addElement(rs);
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  public PromoCodeTable()
  {
    this(null);
  }
}
