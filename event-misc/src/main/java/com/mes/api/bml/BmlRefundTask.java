package com.mes.api.bml;

import org.apache.log4j.Logger;
import com.cardinalcommerce.client.CentinelRequest;
import com.cardinalcommerce.client.CentinelResponse;
import com.mes.api.TridentApiTransaction;

public class BmlRefundTask extends BmlBaseTask
{
  static Logger log = Logger.getLogger(BmlRefundTask.class);

  private BmlRefundTran tran;

  public BmlRefundTask(TridentApiTransaction apiTran)
  {
    super(apiTran);
    tran = (BmlRefundTran)apiTran;
  }

  public void setRequestData(CentinelRequest request)
  {
    request.add("OrderId",          tran.getBmlReqOrderId());
	  request.add("Amount",           stripDecimal(""+tran.getAmount()));
	  request.add("CurrencyCode",     "840");
    request.add("MerchantData",     ""+tran.getReconId());
  }

  public void setResponseData(CentinelResponse response)
  {
    tran.setResponseData(tran.FN_BML_TRAN_ID,     response.getValue("TransactionId"));
    tran.setResponseData(tran.FN_BML_RESP_ORDER_ID,response.getValue("OrderId"));
    tran.setResponseData(tran.FN_BML_STATUS_CODE, response.getValue("StatusCode"));
  }
}