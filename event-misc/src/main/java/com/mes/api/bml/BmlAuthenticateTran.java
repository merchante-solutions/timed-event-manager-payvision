package com.mes.api.bml;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.support.HttpHelper;

public class BmlAuthenticateTran extends BmlBaseTran
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;

  static Logger log = Logger.getLogger(BmlAuthenticateTran.class);

  // request data

  // tpg api request field names (received from merchant)
  public static final String  FN_BML_PA_RESP          = "pa_response";

  protected String            paResp                  = null;
  protected String            reqTranId               = null;

  // response data

  // tpg api response field names (sent to merchant)
  public static final String  FN_BML_PA_RESP_STATUS   = "application_status";
  public static final String  FN_BML_SIGNATURE_VERIFY = "signature_verify";
  public static final String  FN_BML_SHIP_FIRST_NAME  = "ship_first_name";
  public static final String  FN_BML_SHIP_LAST_NAME   = "ship_last_name";
  public static final String  FN_BML_SHIP_ADDR1       = "ship_addr1";
  public static final String  FN_BML_SHIP_ADDR2       = "ship_addr2";
  public static final String  FN_BML_SHIP_CITY        = "ship_city";
  public static final String  FN_BML_SHIP_STATE       = "ship_state";
  public static final String  FN_BML_SHIP_ZIP         = "ship_zip";

  public BmlAuthenticateTran(String tranId)
  {
    super(tranId);
  }

  public void setProperties(HttpServletRequest request)
  {
    super.setProperties(request);

    paResp        = HttpHelper.getString(request,FN_BML_PA_RESP,null);
  }

  public String getPaResp()
  {
    return paResp;
  }

  /**
   * Authenticate requests must refer to a lookup transaction in order to load
   * the lookup response order id.
   */
  public String getRefTranType()
  {
    return BmlOrderIdEntry.TT_LOOKUP;
  }

  /**
   * Data validation
   */
  public boolean isBmlValid()
  {
    if (isBlank(paResp))
    {
      setError(tac.ER_BML_PA_RESP_REQUIRED);
    }

    return !hasError();
  }

  /**
   * DB loading
   */

  public void setBmlRequestType()
  {
    bmlRequest = BR_AUTHENTICATE;
  }

  public void setTranData(ResultSet rs)
  {
  }
}



