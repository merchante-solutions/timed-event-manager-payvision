package com.mes.api.bml;

import org.apache.log4j.Logger;
import com.cardinalcommerce.client.CentinelRequest;
import com.cardinalcommerce.client.CentinelResponse;
import com.mes.api.TridentApiTransaction;

public class BmlAuthorizeTask extends BmlBaseTask
{
  static Logger log = Logger.getLogger(BmlAuthorizeTask.class);

  private BmlAuthorizeTran tran;

  public BmlAuthorizeTask(TridentApiTransaction apiTran)
  {
    super(apiTran);
    tran = (BmlAuthorizeTran)apiTran;
  }

  private void addBmlConsumerData(CentinelRequest request)
  {
    addRequestData(request,"BillingFirstName",    tran.getBillingFirstName());
    addRequestData(request,"BillingMiddleName",   tran.getBillingMiddleName());
    addRequestData(request,"BillingLastName",     tran.getBillingLastName());
    addRequestData(request,"BillingAddress1",     tran.getBillingAddress1());
    addRequestData(request,"BillingAddress2",     tran.getBillingAddress2());
    addRequestData(request,"BillingCity",         tran.getBillingCity());
    addRequestData(request,"BillingState",        tran.getBillingState());
    addRequestData(request,"BillingPostalCode",   tran.getBillingZip());
    addRequestData(request,"BillingCountryCode",  "US");
    addRequestData(request,"BillingPhone",        tran.getBillingPhone1());
    addRequestData(request,"BillingAltPhone",     tran.getBillingPhone2());
    addRequestData(request,"EMail",               tran.getBillingEmail());
    addRequestData(request,"ShippingFirstName",   tran.getShippingFirstName());
    addRequestData(request,"ShippingMiddleName",  tran.getShippingMiddleName());
    addRequestData(request,"ShippingLastName",    tran.getShippingLastName());
    addRequestData(request,"ShippingAddress1",    tran.getShippingAddress1());
    addRequestData(request,"ShippingAddress2",    tran.getShippingAddress2());
    addRequestData(request,"ShippingCity",        tran.getShippingCity());
    addRequestData(request,"ShippingState",       tran.getShippingState());
    addRequestData(request,"ShippingPostalCode",  tran.getShippingZip());
    addRequestData(request,"ShippingCountryCode", "US");
    addRequestData(request,"ShippingPhone",       tran.getShippingPhone1());
    addRequestData(request,"ShippingAltPhone",    tran.getShippingPhone2());
    addRequestData(request,"AltEMail",            tran.getShippingEmail());
  }

  private void addBmlCallCenterData(CentinelRequest request)
  {
    addRequestData(request,"HasCheckingAccount",  tran.getHasCheckingAccount());
    addRequestData(request,"HasSavingsAccount",   tran.getHasSavingsAccount());
    addRequestData(request,"ResidenceStatus",     tran.getResidenceStatus());
    addRequestData(request,"DateOfBirth",         tran.getDob());
    addRequestData(request,"SSN",                 tran.getSsn());
    addRequestData(request,"YearsAtEmployer",     tran.getYearsAtEmployer());
    addRequestData(request,"YearsAtResidence",    tran.getYearsAtResidence());
    log.debug(""+tran.getHouseholdIncome());
    if (!(""+tran.getHouseholdIncome()).equals("-1.00"))
    {
      log.debug("adding household income");
      addRequestData(request,"HouseholdIncome",     stripDecimal(""+tran.getHouseholdIncome()));
      addRequestData(request,"HouseholdIncomeCurrencyCode","840");
    }
  }

  public void setRequestData(CentinelRequest request)
  {
	  addRequestData(request,"Amount",              stripDecimal(""+tran.getAmount()));
	  addRequestData(request,"ShippingAmount",      stripDecimal(""+tran.getShipAmount()));
	  addRequestData(request,"CurrencyCode",        "840");
	  addRequestData(request,"OrderNumber",         tran.getOrderNum());
    addRequestData(request,"PromoCode",           tran.getPromoCode());  
    addRequestData(request,"CustomerRegistrationDate",
                                                  tran.getCustRegDate());
    addRequestData(request,"CustomerFlag",        tran.getCustFlag());
    addRequestData(request,"CategoryCode",        tran.getCatCode());
    addRequestData(request,"TransactionMode",     tran.getTranMode());
    addRequestData(request,"TermsAndConditions",  tran.getTerms());
    addRequestData(request,"IPAddress",           tran.getIpAddress());
    addRequestData(request,"OrderDesc",           tran.getOrderDesc());
    addRequestData(request,"ProductCode",         tran.getProdCode());

    if (tran.getBmlTaxAmount().doubleValue() > 0)
    {
      addRequestData(request,"TaxAmount",         stripDecimal(""+tran.getBmlTaxAmount()));
    }

    // auths can be for a new customer (use lookup order id)
    if (tran.getBmlReqOrderId() != null)
    {
      addRequestData(request,"OrderId",           tran.getBmlReqOrderId());
    }
    // or an existing customer (use bml acct number in CardNumber field)
    else if (tran.getBmlAcctNum() != null)
    {
      addRequestData(request,"CardNumber",        tran.getBmlAcctNum());
    }

    // add consumer billing and shipping data
    addBmlConsumerData(request);

    // add consumer data if present
    addBmlCallCenterData(request);
  }

  public void setResponseData(CentinelResponse response)
  {
    tran.setResponseData(tran.FN_BML_TRAN_ID,       response.getValue("TransactionId"));
    tran.setResponseData(tran.FN_BML_RESP_ORDER_ID, response.getValue("OrderId"));
    tran.setResponseData(tran.FN_BML_STATUS_CODE,   response.getValue("StatusCode"));
    tran.setResponseData(tran.FN_BML_RESP_ACCT_NUM, response.getValue("AccountNumber"));
    tran.setResponseData(tran.FN_BML_AUTH_CODE,     response.getValue("AuthorizationCode"));
  }
}