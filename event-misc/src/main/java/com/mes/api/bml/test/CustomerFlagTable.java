package com.mes.api.bml.test;

import com.mes.tools.DropDownTable;

public class CustomerFlagTable extends DropDownTable
{
  public CustomerFlagTable()
  {
    addElement("","--");
    addElement("E","Existing");
    addElement("N","New");
  }
}

