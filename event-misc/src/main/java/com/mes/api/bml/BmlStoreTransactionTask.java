package com.mes.api.bml;

import com.mes.api.TridentApiTransactionTask;

public class BmlStoreTransactionTask extends TridentApiTransactionTask
{
  private BmlBaseTran bmlTran;
  
  public BmlStoreTransactionTask(BmlBaseTran bmlTran)
  {
    super(bmlTran);
    this.bmlTran = bmlTran;
  }
  
  public boolean doTask()
  {
    return BmlDb.storeTransaction(bmlTran);
  }
}