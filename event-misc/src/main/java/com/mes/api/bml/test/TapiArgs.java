package com.mes.api.bml.test;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TapiArgs
{
  private List argList = new ArrayList();

  public void add(String name, String value)
  {
    argList.add(new String[] { name, value });
  }

  public String encode() 
  {
    try
    {
      StringBuffer buf = new StringBuffer();
      for (Iterator i = argList.iterator(); i.hasNext();)
      {
        if (buf.length() > 0)
        {
          buf.append("&");
        }
        String[] data = (String[])i.next();
        buf.append(URLEncoder.encode(data[0],"UTF-8") + "=");
        buf.append(URLEncoder.encode(data[1],"UTF-8"));
      }
      return buf.toString();
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
}