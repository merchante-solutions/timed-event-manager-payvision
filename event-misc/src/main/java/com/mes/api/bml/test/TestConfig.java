package com.mes.api.bml.test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.DateStringField;
import com.mes.mvc.tags.Viewable;

public class TestConfig extends SQLJConnectionBase implements Viewable
{
  static Logger log = Logger.getLogger(TestConfig.class);

  public static final String  DFLT_TPG_URL = null;
  public static final String  DFLT_CENTINEL_URL = null;

  public static final String  DFLT_TERMINAL_ID = null;

  public static final String  DFLT_DFLT_PROMO_CODE   = null;
  public static final String  DFLT_ORDER_NUM         = null;
  public static final String  DFLT_IP_ADDRESS        = null;
  public static final String  DFLT_CUST_FLAG         = null;
  public static final String  DFLT_CAT_CODE          = null;
  public static final String  DFLT_TRAN_MODE         = null;
  public static final String  DFLT_PROD_CODE         = null;
  public static final String  DFLT_TERMS             = null;
  public static final String  DFLT_TRAN_AMOUNT       = null;
  public static final String  DFLT_SHIP_AMOUNT       = null;
  public static final Date    DFLT_CUST_REG_DATE    = Calendar.getInstance().getTime();

  public static final String  DFLT_BILL_CONSUMER     = null;
  public static final String  DFLT_SHIP_CONSUMER     = null;

  private boolean bmlFlag;

  private String tpgUrl;
  private String centinelUrl;

  private String  merchNum;
  private String  merchName;
  private String  profileId;
  private String  profileKey;
  private List    promoCodes = new ArrayList();
  private String  defaultPromoCode;
  private String  orderNum;
  private String  ipAddress;

  private String  tranAmount;
  private String  shipAmount;

  private String  billId;
  private String  billName;
  private String  billAddress;
  private String  billCsz;
  private String  billPhone;

  private String  shipId;
  private String  shipName;
  private String  shipAddress;
  private String  shipCsz;
  private String  shipPhone;

  private Date    custRegDate;
  private String  custFlag;
  private String  catCode;
  private String  tranMode;
  private String  prodCode;
  private String  terms;

  public TestConfig()
  {
    setDefaults();
  }

  public void setDefaults()
  {
    try
    {
      setTestEnvironment(DFLT_TPG_URL,
                         DFLT_CENTINEL_URL);

      setProfileData    (DFLT_TERMINAL_ID);

      setRequestData    (DFLT_DFLT_PROMO_CODE,
                         DFLT_ORDER_NUM,
                         DFLT_IP_ADDRESS,
                         DFLT_CUST_FLAG,
                         DFLT_CAT_CODE,
                         DFLT_TRAN_MODE,
                         DFLT_PROD_CODE,
                         DFLT_TERMS,
                         DFLT_TRAN_AMOUNT,
                         DFLT_SHIP_AMOUNT,
                         DFLT_CUST_REG_DATE);

      setConsumerData   (DFLT_BILL_CONSUMER,
                         DFLT_SHIP_CONSUMER);
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  public void setBmlFlag(boolean bmlFlag)
  {
    this.bmlFlag = bmlFlag;
  }
  public boolean bmlEnabled()
  {
    return bmlFlag;
  }

  public boolean isLoaded()
  {
    return profileId != null;
  }

  /**
   * Test environment
   */

  public void setTpgUrl(String tpgUrl)
  {
    this.tpgUrl = tpgUrl;
  }
  public String getTpgUrl()
  {
    return tpgUrl;
  }

  public void setCentinelUrl(String centinelUrl)
  {
    this.centinelUrl = centinelUrl;
  }
  public String getCentinelUrl()
  {
    return centinelUrl;
  }

  /**
   * Profile data
   */

  // AKA terminal_id in trident_profile
  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }

  // AKA merchant_key in trident_profile
  public void setProfileKey(String profileKey)
  {
    this.profileKey = profileKey;
  }
  public String getProfileKey()
  {
    return profileKey;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return merchName;
  }

  public List getPromoCodes()
  {
    return promoCodes;
  }
  public void addPromoCode(String promoCode)
  {
    if (defaultPromoCode == null)
    {
      defaultPromoCode = promoCode;
    }
    promoCodes.add(promoCode);
  }
  public boolean hasPromoCode(String promoCode)
  {
    return promoCodes.contains(promoCode);
  }
  public void clearPromoCodes()
  {
    promoCodes.clear();
  }

  public String getDefaultPromoCode()
  {
    return defaultPromoCode;
  }
  public void setDefaultPromoCode(String defaultPromoCode)
  {
    if (hasPromoCode(defaultPromoCode))
    {
      this.defaultPromoCode = defaultPromoCode;
    }
  }

  /**
   * Request options
   */

  public void setTranAmount(String tranAmount)
  {
    this.tranAmount = tranAmount;
  }
  public String getTranAmount()
  {
    return tranAmount;
  }

  public void setShipAmount(String shipAmount)
  {
    this.shipAmount = shipAmount;
  }
  public String getShipAmount()
  {
    return shipAmount;
  }

  public void setOrderNum(String orderNum)
  {
    this.orderNum = orderNum;
  }
  public String getOrderNum()
  {
    return orderNum;
  }

  public void setIpAddress(String ipAddress)
  {
    this.ipAddress = ipAddress;
  }
  public String getIpAddress()
  {
    return ipAddress;
  }

  public void setCustRegDate(Date custRegDate)
  {
    this.custRegDate = custRegDate;
  }
  public Date getCustRegDate()
  {
    return custRegDate;
  }

  public void setCustFlag(String custFlag)
  {
    this.custFlag = custFlag;
  }
  public String getCustFlag()
  {
    return custFlag;
  }

  public void setCatCode(String catCode)
  {
    this.catCode = catCode;
  }
  public String getCatCode()
  {
    return catCode;
  }

  public void setTranMode(String tranMode)
  {
    this.tranMode = tranMode;
  }
  public String getTranMode()
  {
    return tranMode;
  }

  public void setProdCode(String prodCode)
  {
    this.prodCode = prodCode;
  }
  public String getProdCode()
  {
    return prodCode;
  }

  public void setTerms(String terms)
  {
    this.terms = terms;
  }
  public String getTerms()
  {
    return terms;
  }

  /**
   * Consumer data
   */

  public void setBillId(String billId)
  {
    this.billId = billId;
  }
  public String getBillId()
  {
    return billId;
  }

  public void setBillName(String billName)
  {
    this.billName = billName;
  }
  public String getBillName()
  {
    return billName;
  }

  public void setBillAddress(String billAddress)
  {
    this.billAddress = billAddress;
  }
  public String getBillAddress()
  {
    return billAddress;
  }

  public void setBillCsz(String billCsz)
  {
    this.billCsz = billCsz;
  }
  public String getBillCsz()
  {
    return billCsz;
  }

  public void setBillPhone(String billPhone)
  {
    this.billPhone = billPhone;
  }
  public String getBillPhone()
  {
    return billPhone;
  }

  public void setShipId(String shipId)
  {
    this.shipId = shipId;
  }
  public String getShipId()
  {
    return shipId;
  }

  public void setShipName(String shipName)
  {
    this.shipName = shipName;
  }
  public String getShipName()
  {
    return shipName;
  }

  public void setShipAddress(String shipAddress)
  {
    this.shipAddress = shipAddress;
  }
  public String getShipAddress()
  {
    return shipAddress;
  }

  public void setShipCsz(String shipCsz)
  {
    this.shipCsz = shipCsz;
  }
  public String getShipCsz()
  {
    return shipCsz;
  }

  public void setShipPhone(String shipPhone)
  {
    this.shipPhone = shipPhone;
  }
  public String getShipPhone()
  {
    return shipPhone;
  }

  /**
   * Viewable implementation
   */

  public List getRows()
  {
    return new ArrayList();
  }

  public boolean isReversed()
  {
    return false;
  }

  public int getLeadCount()
  {
    return 0;
  }

  public int getTrailCount()
  {
    return 0;
  }

  public boolean isExpanded()
  {
    return true;
  }

  public boolean exceedsLimits()
  {
    return getRows().size() > getLeadCount() + getTrailCount();
  }

  /**
   * TestConfigBean getters for test config bean
   */

  private void getTestEnvironment(TestConfigBean bean)
  {
    bean.setData(bean.FN_TPG_URL,         getTpgUrl());
    bean.setData(bean.FN_CENTINEL_URL,    getCentinelUrl());
  }

  private void getProfileData(TestConfigBean bean)
  {
    bean.setData(bean.FN_TEST_TID,        getProfileId());
  }

  private void getRequestData(TestConfigBean bean)
  {
    bean.setData(bean.FN_DFLT_PROMO_CODE, getDefaultPromoCode());
    bean.setData(bean.FN_ORDER_NUM,       getOrderNum());
    bean.setData(bean.FN_IP_ADDRESS,      getIpAddress());
    bean.setData(bean.FN_CUST_FLAG,       getCustFlag());
    bean.setData(bean.FN_CAT_CODE,        getCatCode());
    bean.setData(bean.FN_TRAN_MODE,       getTranMode());
    bean.setData(bean.FN_PROD_CODE,       getProdCode());
    bean.setData(bean.FN_TERMS,           getTerms());
    bean.setData(bean.FN_TRAN_AMOUNT,     getTranAmount());
    bean.setData(bean.FN_SHIP_AMOUNT,     getShipAmount());
    ((DateStringField)bean.getField(bean.FN_CUST_REG_DATE))
                              .setUtilDate(getCustRegDate());
  }

  private void getConsumerData(TestConfigBean bean)
  {
    bean.setData(bean.FN_BILL_ID,         getBillId());
    bean.setData(bean.FN_SHIP_ID,         getShipId());
  }

  public void getBeanData(TestConfigBean bean)
  {
    getProfileData(bean);
    getTestEnvironment(bean);
    getRequestData(bean);
    getConsumerData(bean);
  }

  /**
   * TestConfigBean setters for test config bean and defaults
   */

  private void setTestEnvironment(String tpgUrl, String centinelUrl)
  {
    setTpgUrl(tpgUrl);
    setCentinelUrl(centinelUrl);
  }
  private void setTestEnvironment(TestConfigBean bean)
  {
    setTestEnvironment(bean.getData(bean.FN_TPG_URL),
                       bean.getData(bean.FN_CENTINEL_URL));
  }

  private void setProfileData(String terminalId) throws Exception
  {
    if (terminalId.equals(getProfileId()))
    {
      return;
    }

    ResultSet rs = null;
    PreparedStatement ps = null;
    try
    {
      connect();

      ps = con.prepareStatement(
        " select                                  " +
        "  tp.merchant_number,                    " +
        "  tp.merchant_name,                      " +
        "  tp.merchant_key,                       " +
        "  nvl(tap.bml_accept,'N') bml_accept,    " +
        "  tap.bml_promo_code_1,                  " +
        "  tap.bml_promo_code_2,                  " +
        "  tap.bml_promo_code_3,                  " +
        "  tap.bml_promo_code_4,                  " +
        "  tap.bml_promo_code_5                   " +
        " from                                    " +
        "  trident_profile_api tap,               " +
        "  trident_profile tp                     " +
        " where                                   " +
        "  tap.terminal_id = '" + terminalId + "' " +
        "  and tap.terminal_id = tp.terminal_id   ");

      rs = ps.executeQuery();
      if (rs.next())
      {
        setProfileId(terminalId);
        setProfileKey(rs.getString("merchant_key"));
        setMerchNum(rs.getString("merchant_number"));
        setMerchName(rs.getString("merchant_name"));
        setBmlFlag(rs.getString("bml_accept").toUpperCase().equals("Y"));
        clearPromoCodes();
        for (int i = 1; i <= 5; ++i)
        {
          String pc = rs.getString("bml_promo_code_" + i);
          if (pc != null)
          {
            addPromoCode(pc);
          }
        }
      }
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }
  private void setProfileData(TestConfigBean bean) throws Exception
  {
    setProfileData(bean.getData(bean.FN_TEST_TID));
  }

  private void setRequestData(String dfltPromoCode,
                              String orderNum,
                              String ipAddress,
                              String custFlag,
                              String catCode,
                              String tranMode,
                              String prodCode,
                              String terms,
                              String tranAmount,
                              String shipAmount,
                              java.util.Date custRegDate)
  {
    setDefaultPromoCode (dfltPromoCode);
    setOrderNum         (orderNum);
    setIpAddress        (ipAddress);
    setCustFlag         (custFlag);
    setCatCode          (catCode);
    setTranMode         (tranMode);
    setProdCode         (prodCode);
    setTerms            (terms);
    setTranAmount       (tranAmount);
    setShipAmount       (shipAmount);
    setCustRegDate      (custRegDate);
  }
  private void setRequestData(TestConfigBean bean)
  {
    setRequestData(bean.getData(bean.FN_DFLT_PROMO_CODE),
                   bean.getData(bean.FN_ORDER_NUM),
                   bean.getData(bean.FN_IP_ADDRESS),
                   bean.getData(bean.FN_CUST_FLAG),
                   bean.getData(bean.FN_CAT_CODE),
                   bean.getData(bean.FN_TRAN_MODE),
                   bean.getData(bean.FN_PROD_CODE),
                   bean.getData(bean.FN_TERMS),
                   bean.getData(bean.FN_TRAN_AMOUNT),
                   bean.getData(bean.FN_SHIP_AMOUNT),
                   ((DateStringField)bean
                    .getField(bean.FN_CUST_REG_DATE)).getUtilDate());
  }

  private void setConsumerData(String billId, String shipId)
    throws Exception
  {
    if (billId.equals(getBillId()) && shipId.equals(getShipId()))
    {
      return;
    }

    ResultSet rs = null;
    PreparedStatement ps = null;
    try
    {
      connect();

      ps = con.prepareStatement(
        " select                                " +
        "  c1.name            bill_name,        " +
        "  c1.address1        bill_address,     " +
        "  c1.csz             bill_csz,         " +
        "  c1.phone1          bill_phone,       " +
        "  c2.name            ship_name,        " +
        "  c2.address1        ship_address,     " +
        "  c2.csz             ship_csz,         " +
        "  c2.phone1          ship_phone        " +
        " from                                  " +
        "  bml_test_consumers c1,               " +
        "  bml_test_consumers c2                " +
        " where                                 " +
        "  c1.id = '" + billId + "'             " +
        "  and c2.id = '" + shipId + "'         ");

      rs = ps.executeQuery();
      if (rs.next())
      {
        setBillId      (billId);
        setBillName    (rs.getString("bill_name"   ));
        setBillAddress (rs.getString("bill_address"));
        setBillCsz     (rs.getString("bill_csz"    ));
        setBillPhone   (rs.getString("bill_phone"  ));
        setShipId      (shipId);
        setShipName    (rs.getString("ship_name"   ));
        setShipAddress (rs.getString("ship_address"));
        setShipCsz     (rs.getString("ship_csz"    ));
        setShipPhone   (rs.getString("ship_phone"  ));
      }
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }
  private void setConsumerData(TestConfigBean bean) throws Exception
  {
    setConsumerData(bean.getData(bean.FN_BILL_ID),
                    bean.getData(bean.FN_SHIP_ID));
  }

  public void setBeanData(TestConfigBean bean) throws Exception
  {
    setTestEnvironment(bean);
    setProfileData(bean);
    setRequestData(bean);
    setConsumerData(bean);
  }
}