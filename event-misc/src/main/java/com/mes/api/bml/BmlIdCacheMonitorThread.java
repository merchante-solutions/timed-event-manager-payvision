package com.mes.api.bml;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiSyncThread;

public class BmlIdCacheMonitorThread extends TridentApiSyncThread
{
  static Logger log = Logger.getLogger(BmlIdCacheMonitorThread.class);

  public BmlIdCacheMonitorThread(long sleepTime)
  {
    super(sleepTime);
  }

  protected void execute()
  {
    if (BmlIdCache.isFull())
    {
      return;
    }

    PreparedStatement ps = null;
    ResultSet         rs = null;

    try
    {
      connect();

      ps = con.prepareStatement(
        " select bml_id_sequence.nextval id from dual ");
      while (!BmlIdCache.isFull())
      {
        rs = ps.executeQuery();
        if (rs.next())
        {
          int id = rs.getInt("id");
          BmlIdCache.add(id);
        }
      }
    }
    catch (Exception e)
    {
      log.error("Execute error: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { cleanUp(ps,rs); } catch (Exception e) { }
    }
  }
}
    
