package com.mes.api.bml;

import org.apache.log4j.Logger;
import com.cardinalcommerce.client.CentinelRequest;
import com.cardinalcommerce.client.CentinelResponse;
import com.mes.api.TridentApiTransaction;

public class BmlAuthenticateTask extends BmlBaseTask
{
  static Logger log = Logger.getLogger(BmlAuthenticateTask.class);

  private BmlAuthenticateTran tran;

  public BmlAuthenticateTask(TridentApiTransaction apiTran)
  {
    super(apiTran);
    tran = (BmlAuthenticateTran)apiTran;
  }

  /**
   * Request data
   */
  public void setRequestData(CentinelRequest request)
  {
    request.add("OrderId",          tran.getBmlReqOrderId());
    request.add("PAResPayload",     tran.getPaResp());
  }

  /**
   * Response data
   */
  public void setResponseData(CentinelResponse response)
  {
    tran.setResponseData(tran.FN_BML_PA_RESP_STATUS,
                                                response.getValue("PAResStatus"));
    tran.setResponseData(tran.FN_BML_SIGNATURE_VERIFY,
                                                response.getValue("SignatureVerification"));

    // shipping fields might be present in response
    tran.setResponseData(tran.FN_BML_SHIP_FIRST_NAME,
                                                response.getValue("ShippingFirstName"));
    tran.setResponseData(tran.FN_BML_SHIP_LAST_NAME,
                                                response.getValue("ShippingLastName"));
    tran.setResponseData(tran.FN_BML_SHIP_ADDR1,response.getValue("ShippingAddress1"));
    tran.setResponseData(tran.FN_BML_SHIP_ADDR2,response.getValue("ShippingAddress2"));
    tran.setResponseData(tran.FN_BML_SHIP_CITY, response.getValue("ShippingCity"));
    tran.setResponseData(tran.FN_BML_SHIP_STATE,response.getValue("ShippingState"));
    tran.setResponseData(tran.FN_BML_SHIP_ZIP,  response.getValue("ShippingPostalCode"));
      
  }
}