package com.mes.api.bml.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import org.apache.log4j.Logger;

public class TapiRequest
{
  static Logger log = Logger.getLogger(TapiRequest.class);

  private TapiArgs args;
  private URL postUrl;

  public TapiRequest(String urlStr)
  {
    setPostUrl(urlStr);
    resetArgs();
  }

  public void setPostUrl(String urlStr)
  {
    try
    {
      postUrl = new URL(urlStr);
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      throw new RuntimeException("Error: " + e);
    }
  }

  public void resetArgs()
  {
    args = new TapiArgs();
  }

  public void addArg(String name, String value)
  {
    if (args == null) resetArgs();
    if (value == null || value.equals("")) return;
    args.add(name,value);
  }

  public String getEncodedArgs()
  {
    if (args == null) resetArgs();
    return args.encode();
  }

  public TapiResponse post() throws Exception
  {
    URLConnection con = postUrl.openConnection();
    con.setDoInput(true);
    con.setDoOutput(true);
    con.setUseCaches(false);
    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    OutputStream out = con.getOutputStream();
    String encodedArgs = getEncodedArgs();
    out.write(encodedArgs.getBytes());
    out.flush();
    out.close();

    log.debug("Sending request to TPG: " + postUrl);
    log.debug("Encoded args: " + encodedArgs);

    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
    String recvData = null;
    StringBuffer dataBuf = new StringBuffer();
    while (null != (recvData = in.readLine()))
    {
      dataBuf.append(recvData);
    }
    in.close();

    log.debug("Received TPG response: " + dataBuf);

    return new TapiResponse(""+dataBuf);
  }

  public String toString()
  {
    return "TapiRequest [ " + getEncodedArgs() + " ]";
  }
}