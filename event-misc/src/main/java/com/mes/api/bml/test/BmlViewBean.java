package com.mes.api.bml.test;

import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.Field;
import com.mes.mvc.mes.MesAction;
import com.mes.mvc.mes.MesViewBean;
import com.mes.tools.DropDownTable;

public class BmlViewBean extends MesViewBean
{
  static Logger log = Logger.getLogger(BmlViewBean.class);

  protected TestConfig tc;

  public BmlViewBean(MesAction action)
  {
    super(action);
  }

  public class ProfilePromoCodeTable extends DropDownTable
  {
    public ProfilePromoCodeTable()
    {
      for (Iterator i = tc.getPromoCodes().iterator(); i.hasNext();)
      {
        String promoCode = ""+i.next();
        addElement(promoCode,promoCode);
      }
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      // retrieve test config object from request
      tc = (TestConfig)request.getAttribute("tc");
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
    }
  }

  protected void setDefault(String name, String data)
  {
    Field field = getField(name);
    if (field.isBlank())
    {
      field.setData(data);
    }
  }
}