package com.mes.api.bml.test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.mes.MesAction;
import com.mes.tools.DropDownTable;

public class TestConfigBean extends BmlViewBean
{
  static Logger log = Logger.getLogger(TestConfigBean.class);

  public static final String FN_TPG_URL           = "tpgUrl";
  public static final String FN_CENTINEL_URL      = "centinelUrl";
  public static final String FN_TEST_TID          = "testTid";
  public static final String FN_DFLT_PROMO_CODE   = "dfltPromoCode";
  public static final String FN_BILL_ID           = "billId";
  public static final String FN_SHIP_ID           = "shipId";
  public static final String FN_TRAN_AMOUNT       = "tranAmount";
  public static final String FN_SHIP_AMOUNT       = "shipAmount";
  public static final String FN_ORDER_NUM         = "orderNum";
  public static final String FN_IP_ADDRESS        = "ipAddress";
  public static final String FN_CUST_REG_DATE     = "custRegDate";
  public static final String FN_CUST_FLAG         = "custFlag";
  public static final String FN_CAT_CODE          = "catCode";
  public static final String FN_TRAN_MODE         = "tranMode";
  public static final String FN_PROD_CODE         = "prodCode";
  public static final String FN_TERMS             = "terms";

  public static final String FN_BACK_FLAG         = "backFlag";

  public static final String FN_SUBMIT_BTN        = "submitBtn";

  public TestConfigBean(MesAction action)
  {
    super(action);
  }

  public class ConsumerTable extends DropDownTable
  {
    public ConsumerTable(String consumerType) throws Exception
    {
      PreparedStatement ps = null;
      ResultSet rs = null;

      try
      {
        connect();

        ps = con.prepareStatement(
          " select id, id                       " +
          " from bml_test_consumers             " +
          " where type = '" + consumerType + "' ");
        rs = ps.executeQuery();

        while (rs.next())
        {
          addElement(rs);
        }
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { ps.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }

  private boolean backFlag;

  public boolean autoBack()
  {
    return backFlag;
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new HiddenField(FN_BACK_FLAG));

      fields.add(new DropDownField(FN_TPG_URL,new ConfigOptionTable("tpg_url",user.getLoginName()),false));
      fields.add(new DropDownField(FN_CENTINEL_URL,new ConfigOptionTable("centinel_url",user.getLoginName()),false));
      fields.add(new DropDownField(FN_TEST_TID,new ConfigOptionTable("test_tid",user.getLoginName()),false));
      fields.add(new DropDownField(FN_DFLT_PROMO_CODE,new PromoCodeTable("--"),false));
      fields.add(new DropDownField(FN_BILL_ID,new ConsumerTable("BILL"),true));
      fields.add(new DropDownField(FN_SHIP_ID,new ConsumerTable("SHIP"),true));

      fields.add(new Field(FN_ORDER_NUM,"Order #",32,32,true));
      fields.add(new Field(FN_IP_ADDRESS,"IP Address",15,15,true));

      fields.add(new CurrencyField(FN_TRAN_AMOUNT,"Transaction Amount",10,10,false));
      fields.add(new CurrencyField(FN_SHIP_AMOUNT,"Shipping Amount",10,10,false));

      fields.add(new DateStringField(FN_CUST_REG_DATE,"Customer Registration Date",false,false));
      fields.add(new DropDownField(FN_CUST_FLAG,"Customer Flag",new CustomerFlagTable(),false));
      fields.add(new DropDownField(FN_CAT_CODE,"Category Code",new CategoryCodeTable(),false));
      fields.add(new DropDownField(FN_TRAN_MODE,"Transaction Mode",new TransactionModeTable(),false));
      fields.add(new DropDownField(FN_PROD_CODE,"Production Code",new ProductionCodeTable(),false));
      fields.add(new DropDownField(FN_TERMS,"Terms & Conditions",new TermsTable(),false));

      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  protected boolean autoLoad()
  {
    try
    {
      tc.getBeanData(this);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      tc.setBeanData(this);
      backFlag = !getField(FN_BACK_FLAG).isBlank();
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}