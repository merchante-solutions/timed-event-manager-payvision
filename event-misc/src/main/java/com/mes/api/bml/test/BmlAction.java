package com.mes.api.bml.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiRequest;
import com.mes.api.TridentApiResponse;
import com.mes.api.TridentApiTranBase;
import com.mes.api.bml.BmlAuthenticateTran;
import com.mes.api.bml.BmlAuthorizeTran;
import com.mes.api.bml.BmlBaseTran;
import com.mes.api.bml.BmlCaptureTran;
import com.mes.api.bml.BmlLookupTran;
import com.mes.api.bml.BmlReauthorizeTran;
import com.mes.api.bml.BmlRefundTran;
import com.mes.forms.DateStringField;
import com.mes.mvc.ParameterEncoder;
import com.mes.mvc.mes.MesAction;

public class BmlAction extends MesAction
{
  static Logger log = Logger.getLogger(BmlAction.class);

  private static SimpleDateFormat defaultSdf = new SimpleDateFormat(
    "'<nobr>'EEE M/d/yy'</nobr> <nobr>'h:mma z'</nobr>'");

  private static String formatHtmlDate(Date date, SimpleDateFormat sdf)
  {
    if (date == null)
    {
      return "--";
    }
    return sdf.format(date);
  }
  public static String formatHtmlDate(Date date)
  {
    return formatHtmlDate(date,defaultSdf);
  }
  public static String formatHtmlDate(Date date, String formatStr)
  {
    return formatHtmlDate(date,new SimpleDateFormat(formatStr));
  }

  private TestConfig setTestConfig()
  {
    TestConfig tc = (TestConfig)getSessionAttr("bmlTestConfig",null);
    if (tc == null)
    {
      tc = new TestConfig();
      setSessionAttr("bmlTestConfig",tc);
    }
    request.setAttribute("tc",tc);
    return tc;
  }

  private void addTridentApiResponseToRequest(TridentApiResponse taResp)
  {
    request.setAttribute("respData",taResp);
  }

  public void doBmlTestConfig()
  {
    setTestConfig();
    TestConfigBean bean = new TestConfigBean(this);
    request.setAttribute("viewBean",bean);
    if (bean.isAutoSubmitOk())
    {
      addFeedback("Test configuration updated");
      if (bean.autoBack())
      {
        doLinkRedirect(getBackLink());
        return;
      }
    }
    doView("bmlTestConfig");
  }

  public class TransactionSender
  {
    TridentApiRequest taReq;
    boolean errorFlag;
    boolean commErrorFlag;
    String errorCode;
    String responseText;

    public TransactionSender(TridentApiRequest taReq)
    {
      this.taReq = taReq;
    }

    public TridentApiResponse send()
    {
      // get the tpg response
      TridentApiResponse taResp = null;
      try
      {
        taResp = taReq.post();
        errorCode = taResp.getParameter(TridentApiConstants.FN_ERROR_CODE);
        responseText = taResp.getParameter(TridentApiConstants.FN_AUTH_RESP_TEXT);
        if (errorCode == null || 
            !errorCode.equals(TridentApiConstants.ER_NONE))
        {
          errorFlag = true;
        }

      }
      catch (Exception e)
      {
        commErrorFlag = true;
        errorFlag = true;
        responseText = "Transaction failed: " + e;
        log.error("TransactionSender error: " + e);
        e.printStackTrace();
      }
      return taResp;
    }

    public boolean hasError()
    {
      return errorFlag;
    }

    public boolean hasCommError()
    {
      return commErrorFlag;
    }

    public String getErrorCode()
    {
      return errorCode;
    }

    public String getResponseText()
    {
      return responseText;
    }

    public String getErrorMsg()
    {
      return (errorCode != null ? errorCode + " - " : "") + responseText;
    }
  }

  public static final String SN_BML_LOOKUP_DATA = "bml-lookup-data";

  public class LookupTranData
  {
    public String tpgUrl;

    public String tranId;
    public String profileId;
    public String profileKey;
    public String orderNum;
    public String orderDesc;
    public String tranAmount;
    public String shipAmount;
    public String taxAmount;
    public String ipAddress;
    //public String bmlTranId;
    //public String bmlOrderId;
    public String overrideUrl;
    public String promoCode;
    public String billName;
    public String billMiddleName;
    public String billAddress1;
    public String billAddress2;
    public String billCsz;
    public String billPhone1;
    public String billPhone2;
    public String billEmail;
    public String shipName;
    public String shipMiddleName;
    public String shipAddress1;
    public String shipAddress2;
    public String shipCsz;
    public String shipPhone1;
    public String shipPhone2;
    public String shipEmail;
  }

  private boolean verifyTestConfigLoaded()
  {
    if (!setTestConfig().isLoaded())
    {
      redirectWithFeedback(getBackLink(),
        "Use Test Setup option from BillMeLater menu to configure first");
      return false;
    }
    return true;
  }

  public void doBmlLookup()
  {
    if (!verifyTestConfigLoaded()) return;

    LookupBean bean = new LookupBean(this);
    if (bean.isAutoSubmitOk())
    {
      // send a bml lookup request to tpg
      TridentApiRequest taReq = new TridentApiRequest(bean.getData(bean.FN_TPG_URL));
      taReq.addArg(BmlBaseTran.FN_CENTINEL_URL_OVERRIDE,        bean.getData(bean.FN_CENTINEL_URL));
      taReq.addArg(TridentApiConstants.FN_TRAN_TYPE,            TridentApiTranBase.TT_BML_REQUEST);
      taReq.addArg(TridentApiConstants.FN_TID,                  bean.getData(bean.FN_PROFILE_ID));
      taReq.addArg(TridentApiConstants.FN_TERM_PASS,            bean.getData(bean.FN_PROFILE_KEY));
      taReq.addArg(BmlLookupTran.FN_BML_REQUEST,                BmlBaseTran.BR_LOOKUP);
      taReq.addArg(BmlLookupTran.FN_BML_PROMO_CODE,             bean.getData(bean.FN_PROMO_CODE));
      taReq.addArg(BmlLookupTran.FN_BML_ORDER_NUM,              bean.getData(bean.FN_ORDER_NUM));
      taReq.addArg(BmlLookupTran.FN_BML_ORDER_DESC,             bean.getData(bean.FN_ORDER_DESC));
      taReq.addArg(BmlLookupTran.FN_BML_AMOUNT,                 bean.getData(bean.FN_TRAN_AMOUNT));
      taReq.addArg(BmlLookupTran.FN_BML_SHIP_AMOUNT,            bean.getData(bean.FN_SHIP_AMOUNT));
      taReq.addArg(BmlLookupTran.FN_BML_TAX_AMOUNT,             bean.getData(bean.FN_TAX_AMOUNT));
      taReq.addArg(BmlLookupTran.FN_BML_IP_ADDRESS,             bean.getData(bean.FN_IP_ADDRESS));
      taReq.addArg(BmlLookupTran.FN_BML_BILL_FIRST_NAME,        bean.getData(bean.FN_BILL_FIRST_NAME));
      taReq.addArg(BmlLookupTran.FN_BML_BILL_MIDDLE_NAME,       bean.getData(bean.FN_BILL_MIDDLE_NAME));
      taReq.addArg(BmlLookupTran.FN_BML_BILL_LAST_NAME,         bean.getData(bean.FN_BILL_LAST_NAME));
      taReq.addArg(BmlLookupTran.FN_BML_BILL_ADDR1,             bean.getData(bean.FN_BILL_ADDRESS1));
      taReq.addArg(BmlLookupTran.FN_BML_BILL_ADDR2,             bean.getData(bean.FN_BILL_ADDRESS2));
      taReq.addArg(BmlLookupTran.FN_BML_BILL_CITY,              bean.getData(bean.FN_BILL_CSZ_CITY));
      taReq.addArg(BmlLookupTran.FN_BML_BILL_STATE,             bean.getData(bean.FN_BILL_CSZ_STATE));
      taReq.addArg(BmlLookupTran.FN_BML_BILL_ZIP,               bean.getData(bean.FN_BILL_CSZ_ZIP));
      taReq.addArg(BmlLookupTran.FN_BML_BILL_PHONE1,            bean.getData(bean.FN_BILL_PHONE1));
      taReq.addArg(BmlLookupTran.FN_BML_BILL_PHONE2,            bean.getData(bean.FN_BILL_PHONE2));
      taReq.addArg(BmlLookupTran.FN_BML_BILL_EMAIL,             bean.getData(bean.FN_BILL_EMAIL));
      taReq.addArg(BmlLookupTran.FN_BML_SHIP_FIRST_NAME,        bean.getData(bean.FN_SHIP_FIRST_NAME));
      taReq.addArg(BmlLookupTran.FN_BML_SHIP_MIDDLE_NAME,       bean.getData(bean.FN_SHIP_MIDDLE_NAME));
      taReq.addArg(BmlLookupTran.FN_BML_SHIP_LAST_NAME,         bean.getData(bean.FN_SHIP_LAST_NAME));
      taReq.addArg(BmlLookupTran.FN_BML_SHIP_ADDR1,             bean.getData(bean.FN_SHIP_ADDRESS1));
      taReq.addArg(BmlLookupTran.FN_BML_SHIP_ADDR2,             bean.getData(bean.FN_SHIP_ADDRESS2));
      taReq.addArg(BmlLookupTran.FN_BML_SHIP_CITY,              bean.getData(bean.FN_SHIP_CSZ_CITY));
      taReq.addArg(BmlLookupTran.FN_BML_SHIP_STATE,             bean.getData(bean.FN_SHIP_CSZ_STATE));
      taReq.addArg(BmlLookupTran.FN_BML_SHIP_ZIP,               bean.getData(bean.FN_SHIP_CSZ_ZIP));
      taReq.addArg(BmlLookupTran.FN_BML_SHIP_PHONE1,            bean.getData(bean.FN_SHIP_PHONE1));
      taReq.addArg(BmlLookupTran.FN_BML_SHIP_PHONE2,            bean.getData(bean.FN_SHIP_PHONE2));
      taReq.addArg(BmlLookupTran.FN_BML_SHIP_EMAIL,             bean.getData(bean.FN_SHIP_EMAIL));

      // send the request, notice errors
      TransactionSender sender = new TransactionSender(taReq);
      TridentApiResponse taResp = sender.send();
      addTridentApiResponseToRequest(taResp);
      if (sender.hasError())
      {
        addFeedback(sender.getErrorMsg());
        doView("bmlLookup");
        return;
      }

      // handle negative enrollment status responses
      String enrolled = taResp.getParameter(BmlLookupTran.FN_BML_ENROLLED);
      if (!enrolled.equals("Y"))
      {
        String errorMsg = "BillMeLater processing unavailable (enrolled = 'N')";
        log.error(errorMsg);
        addFeedback(errorMsg);
        doView("bmlLookup");
        return;
      }

      // TODO: store data for use with subsequent authenticate request 
      LookupTranData ltd = new LookupTranData();
      ltd.tpgUrl          = bean.getData(bean.FN_TPG_URL);
      ltd.tranId          = taResp.getParameter(TridentApiConstants.FN_TRAN_ID);
      ltd.profileId       = bean.getData(bean.FN_PROFILE_ID);
      ltd.profileKey      = bean.getData(bean.FN_PROFILE_KEY);
      ltd.orderNum        = bean.getData(bean.FN_ORDER_NUM);
      ltd.orderDesc       = bean.getData(bean.FN_ORDER_DESC);
      ltd.tranAmount      = bean.getData(bean.FN_TRAN_AMOUNT);
      ltd.shipAmount      = bean.getData(bean.FN_SHIP_AMOUNT);
      ltd.taxAmount       = bean.getData(bean.FN_TAX_AMOUNT);
      ltd.ipAddress       = bean.getData(bean.FN_IP_ADDRESS);
      //ltd.bmlTranId       = taResp.getParameter(BmlBaseTran.FN_BML_TRAN_ID);
      //ltd.bmlOrderId      = taResp.getParameter(BmlBaseTran.FN_BML_RESP_ORDER_ID);
      ltd.overrideUrl     = bean.getData(bean.FN_CENTINEL_URL);
      ltd.promoCode       = bean.getData(bean.FN_PROMO_CODE);
      ltd.billName        = bean.getData(bean.FN_BILL_NAME);
      ltd.billMiddleName  = bean.getData(bean.FN_BILL_MIDDLE_NAME);
      ltd.billAddress1    = bean.getData(bean.FN_BILL_ADDRESS1);
      ltd.billAddress2    = bean.getData(bean.FN_BILL_ADDRESS2);
      ltd.billCsz         = bean.getData(bean.FN_BILL_CSZ);
      ltd.billPhone1      = bean.getData(bean.FN_BILL_PHONE1);
      ltd.billPhone2      = bean.getData(bean.FN_BILL_PHONE2);
      ltd.billEmail       = bean.getData(bean.FN_BILL_EMAIL);
      ltd.shipName        = bean.getData(bean.FN_SHIP_NAME);
      ltd.shipMiddleName  = bean.getData(bean.FN_SHIP_MIDDLE_NAME);
      ltd.shipAddress1    = bean.getData(bean.FN_SHIP_ADDRESS1);
      ltd.shipAddress2    = bean.getData(bean.FN_SHIP_ADDRESS2);
      ltd.shipCsz         = bean.getData(bean.FN_SHIP_CSZ);
      ltd.shipPhone1      = bean.getData(bean.FN_SHIP_PHONE1);
      ltd.shipPhone2      = bean.getData(bean.FN_SHIP_PHONE2);
      ltd.shipEmail       = bean.getData(bean.FN_SHIP_EMAIL);
      setSessionAttr(SN_BML_LOOKUP_DATA,ltd);

      // construct acs url from lookup response data, redirect the consumer
      String requestUrl = ""+request.getRequestURL();
      String termUrl = requestUrl.substring(0,requestUrl.lastIndexOf("/"))
        + "/acsReturn";
      String acsUrl = taResp.getParameter("acs_url") + "&TermUrl=" + termUrl
        + "&MD=" + ltd.tranId;
      setRequestAttr("acsUrl",acsUrl);
      doView("bmlAcsFrame");
    }
    else
    {
      doView("bmlLookup");
    }
  }

  public void doAcsReturn()
  {
    setTestConfig();
    // manually set acs return back links in case action 
    // ends in a view needing a back link
    setBackLink("acsReturn",handler.generateLink("bmlTestMenu",
      null,"Back to Bill Me Later Testing"));


    // retrieve session stored lookup transaction data
    LookupTranData ltd = (LookupTranData)getSessionAttr(SN_BML_LOOKUP_DATA);

    // send a bml authenticate request to tpg
    TridentApiRequest taReq = new TridentApiRequest(ltd.tpgUrl);
    taReq.addArg(TridentApiConstants.FN_TRAN_TYPE,  TridentApiTranBase.TT_BML_REQUEST);
    taReq.addArg(TridentApiConstants.FN_TID,        ltd.profileId);
    taReq.addArg(TridentApiConstants.FN_TERM_PASS,  ltd.profileKey);
    taReq.addArg(BmlBaseTran.FN_BML_REQUEST,        BmlBaseTran.BR_AUTHENTICATE);
    //taReq.addArg(BmlBaseTran.FN_BML_REQ_ORDER_ID,   ltd.bmlOrderId);
    taReq.addArg(BmlBaseTran.FN_REF_TRAN_ID,        ltd.tranId);
    taReq.addArg(BmlAuthenticateTran.FN_BML_PA_RESP,
                                                    request.getParameter("PaRes"));
    taReq.addArg(BmlBaseTran.FN_CENTINEL_URL_OVERRIDE,
                                                    ltd.overrideUrl);

    // send the request
    TransactionSender sender = new TransactionSender(taReq);
    TridentApiResponse taResp = sender.send();
    addTridentApiResponseToRequest(taResp);
    if (sender.hasError())
    {
      addFeedback(sender.getErrorMsg());
      doView("bmlAuthenticateError");
      return;
    }

    ParameterEncoder parms = new ParameterEncoder();
    parms.add(AuthorizeBean.FN_PROFILE_ID,       ltd.profileId);
    parms.add(AuthorizeBean.FN_PROFILE_KEY,      ltd.profileKey);
    parms.add(AuthorizeBean.FN_ORDER_NUM,        ltd.orderNum);
    parms.add(AuthorizeBean.FN_ORDER_DESC,       ltd.orderDesc);
    parms.add(AuthorizeBean.FN_TRAN_AMOUNT,      ltd.tranAmount);
    parms.add(AuthorizeBean.FN_SHIP_AMOUNT,      ltd.shipAmount);
    parms.add(AuthorizeBean.FN_TAX_AMOUNT,       ltd.taxAmount);
    parms.add(AuthorizeBean.FN_IP_ADDRESS,       ltd.ipAddress);
    //parms.add(AuthorizeBean.FN_LOOKUP_TRAN_ID,   ltd.bmlTranId);
    //parms.add(AuthorizeBean.FN_BML_ORDER_ID,     ltd.bmlOrderId);
    parms.add(AuthorizeBean.FN_REF_TRAN_ID,      ltd.tranId);
    parms.add(AuthorizeBean.FN_CENTINEL_URL,     ltd.overrideUrl);
    parms.add(AuthorizeBean.FN_PROMO_CODE,       ltd.promoCode);
    parms.add(AuthorizeBean.FN_BILL_NAME,        ltd.billName);
    parms.add(AuthorizeBean.FN_BILL_MIDDLE_NAME, ltd.billMiddleName);
    parms.add(AuthorizeBean.FN_BILL_ADDRESS1,    ltd.billAddress1);
    parms.add(AuthorizeBean.FN_BILL_ADDRESS2,    ltd.billAddress2);
    parms.add(AuthorizeBean.FN_BILL_CSZ,         ltd.billCsz);
    parms.add(AuthorizeBean.FN_BILL_PHONE1,      ltd.billPhone1);
    parms.add(AuthorizeBean.FN_BILL_PHONE2,      ltd.billPhone2);
    parms.add(AuthorizeBean.FN_BILL_EMAIL,       ltd.billEmail);
    parms.add(AuthorizeBean.FN_SHIP_NAME,        ltd.shipName);
    parms.add(AuthorizeBean.FN_SHIP_MIDDLE_NAME, ltd.shipMiddleName);
    parms.add(AuthorizeBean.FN_SHIP_ADDRESS1,    ltd.shipAddress1);
    parms.add(AuthorizeBean.FN_SHIP_ADDRESS2,    ltd.shipAddress2);
    parms.add(AuthorizeBean.FN_SHIP_CSZ,         ltd.shipCsz);
    parms.add(AuthorizeBean.FN_SHIP_PHONE1,      ltd.shipPhone1);
    parms.add(AuthorizeBean.FN_SHIP_PHONE2,      ltd.shipPhone2);
    parms.add(AuthorizeBean.FN_SHIP_EMAIL,       ltd.shipEmail);

    doActionRedirect("bmlAuthorize",parms.encode());
  }

  public void doBmlAuthorize()
  {
    if (!verifyTestConfigLoaded()) return;

    AuthorizeBean bean = new AuthorizeBean(this);
    boolean isSale = bean.getData(bean.FN_SALE_FLAG).equals("y");
    if (bean.isAutoSubmitOk())
    {
      // send a bml authorize request to tpg
      TridentApiRequest taReq = new TridentApiRequest(bean.getData(bean.FN_TPG_URL));
      taReq.addArg(TridentApiConstants.FN_TRAN_TYPE,        TridentApiTranBase.TT_BML_REQUEST);
      taReq.addArg(TridentApiConstants.FN_TID,              bean.getData(bean.FN_PROFILE_ID));
      taReq.addArg(TridentApiConstants.FN_TERM_PASS,        bean.getData(bean.FN_PROFILE_KEY));
      taReq.addArg(BmlAuthorizeTran.FN_BML_REQUEST,         (isSale ? BmlBaseTran.BR_SALE : BmlBaseTran.BR_AUTHORIZE));
      taReq.addArg(BmlAuthorizeTran.FN_BML_ORDER_NUM,       bean.getData(bean.FN_ORDER_NUM));
      taReq.addArg(BmlAuthorizeTran.FN_BML_ORDER_DESC,      bean.getData(bean.FN_ORDER_DESC));
      taReq.addArg(BmlAuthorizeTran.FN_BML_PROMO_CODE,      bean.getData(bean.FN_PROMO_CODE));
      taReq.addArg(BmlAuthorizeTran.FN_BML_AMOUNT,          bean.getData(bean.FN_TRAN_AMOUNT));
      taReq.addArg(BmlAuthorizeTran.FN_BML_SHIP_AMOUNT,     bean.getData(bean.FN_SHIP_AMOUNT));
      taReq.addArg(BmlAuthorizeTran.FN_BML_TAX_AMOUNT,      bean.getData(bean.FN_TAX_AMOUNT));
      taReq.addArg(BmlAuthorizeTran.FN_BML_IP_ADDRESS,      bean.getData(bean.FN_IP_ADDRESS));
      taReq.addArg(BmlAuthorizeTran.FN_BML_CUST_FLAG,       bean.getData(bean.FN_CUST_FLAG));
      taReq.addArg(BmlAuthorizeTran.FN_BML_PROD_CODE,       bean.getData(bean.FN_PROD_CODE));
      taReq.addArg(BmlAuthorizeTran.FN_BML_CAT_CODE,        bean.getData(bean.FN_CAT_CODE));
      taReq.addArg(BmlAuthorizeTran.FN_BML_TRAN_MODE,       bean.getData(bean.FN_TRAN_MODE));
      taReq.addArg(BmlAuthorizeTran.FN_BML_TERMS,           bean.getData(bean.FN_TERMS));
      taReq.addArg(BmlAuthorizeTran.FN_BML_BILL_FIRST_NAME, bean.getData(bean.FN_BILL_FIRST_NAME));
      taReq.addArg(BmlAuthorizeTran.FN_BML_BILL_MIDDLE_NAME,bean.getData(bean.FN_BILL_MIDDLE_NAME));
      taReq.addArg(BmlAuthorizeTran.FN_BML_BILL_LAST_NAME,  bean.getData(bean.FN_BILL_LAST_NAME));
      taReq.addArg(BmlAuthorizeTran.FN_BML_BILL_ADDR1,      bean.getData(bean.FN_BILL_ADDRESS1));
      taReq.addArg(BmlAuthorizeTran.FN_BML_BILL_ADDR2,      bean.getData(bean.FN_BILL_ADDRESS2));
      taReq.addArg(BmlAuthorizeTran.FN_BML_BILL_CITY,       bean.getData(bean.FN_BILL_CSZ_CITY));
      taReq.addArg(BmlAuthorizeTran.FN_BML_BILL_STATE,      bean.getData(bean.FN_BILL_CSZ_STATE));
      taReq.addArg(BmlAuthorizeTran.FN_BML_BILL_ZIP,        bean.getData(bean.FN_BILL_CSZ_ZIP));
      taReq.addArg(BmlAuthorizeTran.FN_BML_BILL_PHONE1,     bean.getData(bean.FN_BILL_PHONE1));
      taReq.addArg(BmlAuthorizeTran.FN_BML_BILL_PHONE2,     bean.getData(bean.FN_BILL_PHONE2));
      taReq.addArg(BmlAuthorizeTran.FN_BML_BILL_EMAIL,      bean.getData(bean.FN_BILL_EMAIL));
      taReq.addArg(BmlAuthorizeTran.FN_BML_SHIP_FIRST_NAME, bean.getData(bean.FN_SHIP_FIRST_NAME));
      taReq.addArg(BmlAuthorizeTran.FN_BML_SHIP_MIDDLE_NAME,bean.getData(bean.FN_SHIP_MIDDLE_NAME));
      taReq.addArg(BmlAuthorizeTran.FN_BML_SHIP_LAST_NAME,  bean.getData(bean.FN_SHIP_LAST_NAME));
      taReq.addArg(BmlAuthorizeTran.FN_BML_SHIP_ADDR1,      bean.getData(bean.FN_SHIP_ADDRESS1));
      taReq.addArg(BmlAuthorizeTran.FN_BML_SHIP_ADDR2,      bean.getData(bean.FN_SHIP_ADDRESS2));
      taReq.addArg(BmlAuthorizeTran.FN_BML_SHIP_CITY,       bean.getData(bean.FN_SHIP_CSZ_CITY));
      taReq.addArg(BmlAuthorizeTran.FN_BML_SHIP_STATE,      bean.getData(bean.FN_SHIP_CSZ_STATE));
      taReq.addArg(BmlAuthorizeTran.FN_BML_SHIP_ZIP,        bean.getData(bean.FN_SHIP_CSZ_ZIP));
      taReq.addArg(BmlAuthorizeTran.FN_BML_SHIP_PHONE1,     bean.getData(bean.FN_SHIP_PHONE1));
      taReq.addArg(BmlAuthorizeTran.FN_BML_SHIP_PHONE2,     bean.getData(bean.FN_SHIP_PHONE2));
      taReq.addArg(BmlAuthorizeTran.FN_BML_SHIP_EMAIL,      bean.getData(bean.FN_SHIP_EMAIL));

      taReq.addArg(BmlAuthorizeTran.FN_BML_HAS_CHK_ACCT,    bean.getData(bean.FN_HAS_CHK_ACCT));
      taReq.addArg(BmlAuthorizeTran.FN_BML_HAS_SAV_ACCT,    bean.getData(bean.FN_HAS_SAV_ACCT));
      taReq.addArg(BmlAuthorizeTran.FN_BML_RES_STATUS,      bean.getData(bean.FN_RES_STATUS));
      taReq.addArg(BmlAuthorizeTran.FN_BML_SSN,             bean.getData(bean.FN_SSN));
      if (!bean.getField(bean.FN_HH_INCOME).isBlank())
      {
        taReq.addArg(BmlAuthorizeTran.FN_BML_HH_INCOME,     bean.getData(bean.FN_HH_INCOME));
      }
      taReq.addArg(BmlAuthorizeTran.FN_BML_EMP_YEARS,       bean.getData(bean.FN_EMP_YEARS));
      taReq.addArg(BmlAuthorizeTran.FN_BML_RES_YEARS,       bean.getData(bean.FN_RES_YEARS));

      taReq.addArg(BmlBaseTran.FN_CENTINEL_URL_OVERRIDE,    bean.getData(bean.FN_CENTINEL_URL));

      // format the customer registration and dob dates
      String custRegDate 
        = formatHtmlDate(
            ((DateStringField)bean
              .getField(bean.FN_CUST_REG_DATE)).getUtilDate(),
            "yyyyMMdd");
      taReq.addArg(BmlAuthorizeTran.FN_BML_CUST_REG_DATE,   custRegDate);
      String dob = bean.getData(bean.FN_DOB);
      if (dob != null && !dob.equals(""))
      {
        dob = formatHtmlDate(
                ((DateStringField)bean
                  .getField(bean.FN_DOB)).getUtilDate(),
                "yyyyMMdd");
      }
      taReq.addArg(BmlAuthorizeTran.FN_BML_DOB,             dob);
          

      // pass the reference tran id or bml acct num as appropriate
      //String orderId = bean.getData(bean.FN_BML_ORDER_ID);
      String refTranId = bean.getData(bean.FN_REF_TRAN_ID);
      if (refTranId != null && !refTranId.equals(""))
      {
        // if following lookup request, lookup tran id
        taReq.addArg(BmlAuthorizeTran.FN_REF_TRAN_ID,refTranId);
      }
      else
      {
        // returning customers provide a BML acct #
        taReq.addArg(BmlAuthorizeTran.FN_BML_REQ_ACCT_NUM,bean.getData(bean.FN_BML_ACCT_NUM));
      }

      // send the request, notice errors
      TransactionSender sender = new TransactionSender(taReq);
      TridentApiResponse taResp = sender.send();
      addTridentApiResponseToRequest(taResp);
      if (sender.hasError())
      {
        addFeedback(sender.getErrorMsg());
        doView("bmlAuthorize");
        return;
      }

      // handle negative enrollment status responses
      String statusCode = taResp.getParameter(BmlAuthorizeTran.FN_BML_STATUS_CODE);
      if (statusCode.equals("N") || statusCode.equals("E"))
      {
        String errorMsg = "BillMeLater authorization error (status code = '" 
          + statusCode + "')";
        log.error(errorMsg);
        addFeedback(errorMsg);
        doView("bmlAuthorize");
        return;
      }

      addFeedback("Response received");
      doView("bmlAuthorize");
    }
    else
    {
      doView("bmlAuthorize");
    }
  }

  public void doBmlCapture()
  {
    if (!verifyTestConfigLoaded()) return;

    CaptureBean bean = new CaptureBean(this);
    boolean isAutoCap = bean.getData(bean.FN_AUTO_CAP_FLAG).equals("y");
    if (bean.isAutoSubmitOk())
    {
      // send a bml capture request to tpg
      TridentApiRequest taReq = new TridentApiRequest(bean.getData(bean.FN_TPG_URL));
      taReq.addArg(TridentApiConstants.FN_TRAN_TYPE,  TridentApiTranBase.TT_BML_REQUEST);
      taReq.addArg(TridentApiConstants.FN_TID,        bean.getData(bean.FN_PROFILE_ID));
      taReq.addArg(TridentApiConstants.FN_TERM_PASS,  bean.getData(bean.FN_PROFILE_KEY));
      taReq.addArg(BmlCaptureTran.FN_BML_REQUEST,     BmlBaseTran.BR_CAPTURE);
      taReq.addArg(BmlCaptureTran.FN_BML_AMOUNT,      bean.getData(bean.FN_TRAN_AMOUNT));
      taReq.addArg(BmlCaptureTran.FN_REF_TRAN_ID,     bean.getData(bean.FN_REF_TRAN_ID));
      taReq.addArg(BmlBaseTran.FN_CENTINEL_URL_OVERRIDE,
                                                      bean.getData(bean.FN_CENTINEL_URL));
      if (isAutoCap)
      {
        taReq.addArg(BmlCaptureTran.FN_BML_AUTO_CAP,  "Y");
      }

      // send the request, notice errors
      TransactionSender sender = new TransactionSender(taReq);
      TridentApiResponse taResp = sender.send();
      addTridentApiResponseToRequest(taResp);
      if (sender.hasError())
      {
        addFeedback(sender.getErrorMsg());
      }
      else
      {
        // handle response status code
        String statusCode = taResp.getParameter(BmlCaptureTran.FN_BML_STATUS_CODE);
        if (statusCode.equals("E"))
        {
          addFeedback("BillMeLater capture error");
        }
        else if (statusCode.equals("P"))
        {
          addFeedback("BillMeLater capture pending");
        }
        else
        {
          addFeedback("BillMeLater capture success");
        }
      }
    }
    doView("bmlCapture");
  }

  private AuthLookupBean getAuthLookupBean()
  {
    AuthLookupBean bean 
      = (AuthLookupBean)getSessionAttr("authLookupBean",null);
    if (bean == null)
    {
      bean = new AuthLookupBean(this);
      setSessionAttr("authLookupBean",bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doBmlAuthLookup()
  {
    AuthLookupBean bean = getAuthLookupBean();
    request.setAttribute("viewBean",bean);
    doView("bmlAuthLookup");
  }

  private CapLookupBean getCapLookupBean()
  {
    CapLookupBean bean 
      = (CapLookupBean)getSessionAttr("capLookupBean",null);
    if (bean == null)
    {
      bean = new CapLookupBean(this);
      setSessionAttr("capLookupBean",bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doBmlCapLookup()
  {
    CapLookupBean bean = getCapLookupBean();
    request.setAttribute("viewBean",bean);
    doView("bmlCapLookup");
  }

  public void doBmlReauthorize()
  {
    if (!verifyTestConfigLoaded()) return;

    ReauthorizeBean bean = new ReauthorizeBean(this);
    if (bean.isAutoSubmitOk())
    {
      // send a bml reauthorize request to tpg
      TridentApiRequest taReq = new TridentApiRequest(bean.getData(bean.FN_TPG_URL));
      taReq.addArg(TridentApiConstants.FN_TRAN_TYPE,      TridentApiTranBase.TT_BML_REQUEST);
      taReq.addArg(TridentApiConstants.FN_TID,            bean.getData(bean.FN_PROFILE_ID));
      taReq.addArg(TridentApiConstants.FN_TERM_PASS,      bean.getData(bean.FN_PROFILE_KEY));
      taReq.addArg(BmlReauthorizeTran.FN_BML_REQUEST,     BmlBaseTran.BR_REAUTHORIZE);
      taReq.addArg(BmlReauthorizeTran.FN_BML_AMOUNT,      bean.getData(bean.FN_TRAN_AMOUNT));
      //taReq.addArg(BmlReauthorizeTran.FN_BML_REQ_ORDER_ID,bean.getData(bean.FN_BML_ORDER_ID));
      taReq.addArg(BmlReauthorizeTran.FN_REF_TRAN_ID,     bean.getData(bean.FN_REF_TRAN_ID));
      taReq.addArg(BmlReauthorizeTran.FN_REF_TRAN_ID,     bean.getData(bean.FN_REF_TRAN_ID));
      taReq.addArg(BmlBaseTran.FN_CENTINEL_URL_OVERRIDE,  bean.getData(bean.FN_CENTINEL_URL));

      // send the request, notice errors
      TransactionSender sender = new TransactionSender(taReq);
      TridentApiResponse taResp = sender.send();
      addTridentApiResponseToRequest(taResp);
      if (sender.hasError())
      {
        addFeedback(sender.getErrorMsg());
      }
      else
      {
        // handle response status code
        String statusCode = taResp.getParameter(BmlReauthorizeTran.FN_BML_STATUS_CODE);
        if (statusCode.equals("N"))
        {
          addFeedback("BillMeLater reauthorize declined");
        }
        else if (statusCode.equals("E"))
        {
          addFeedback("BillMeLater reauthorize error");
        }
        else
        {
          addFeedback("BillMeLater reauthorize success");
        }
      }
    }
    doView("bmlReauthorize");
  }

  public void doBmlRefund()
  {
    if (!verifyTestConfigLoaded()) return;

    RefundBean bean = new RefundBean(this);
    if (bean.isAutoSubmitOk())
    {
      // send a bml refund request to tpg
      TridentApiRequest taReq = new TridentApiRequest(bean.getData(bean.FN_TPG_URL));
      taReq.addArg(TridentApiConstants.FN_TRAN_TYPE,  TridentApiTranBase.TT_BML_REQUEST);
      taReq.addArg(TridentApiConstants.FN_TID,        bean.getData(bean.FN_PROFILE_ID));
      taReq.addArg(TridentApiConstants.FN_TERM_PASS,  bean.getData(bean.FN_PROFILE_KEY));
      taReq.addArg(BmlRefundTran.FN_BML_REQUEST,      BmlBaseTran.BR_REFUND);
      taReq.addArg(BmlRefundTran.FN_BML_AMOUNT,       bean.getData(bean.FN_TRAN_AMOUNT));
      //taReq.addArg(BmlCaptureTran.FN_BML_REQ_ORDER_ID,bean.getData(bean.FN_BML_ORDER_ID));
      taReq.addArg(BmlRefundTran.FN_REF_TRAN_ID,      bean.getData(bean.FN_REF_TRAN_ID));
      taReq.addArg(BmlBaseTran.FN_CENTINEL_URL_OVERRIDE,
                                                      bean.getData(bean.FN_CENTINEL_URL));

      // send the request, notice errors
      TransactionSender sender = new TransactionSender(taReq);
      TridentApiResponse taResp = sender.send();
      addTridentApiResponseToRequest(taResp);
      if (sender.hasError())
      {
        addFeedback(sender.getErrorMsg());
      }
      else
      {
        // handle response status code
        String statusCode = taResp.getParameter(BmlRefundTran.FN_BML_STATUS_CODE);
        if (statusCode.equals("P"))
        {
          addFeedback("BillMeLater refund pending");
        }
        else if (statusCode.equals("E"))
        {
          addFeedback("BillMeLater refund error");
        }
        else
        {
          addFeedback("BillMeLater refund success");
        }
      }
    }
    doView("bmlRefund");
  }
}
