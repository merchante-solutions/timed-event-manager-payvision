package com.mes.api.bml.test;

import com.mes.tools.DropDownTable;

public class CategoryCodeTable extends DropDownTable
{
  public CategoryCodeTable()
  {
    addElement("","--");
    addElement("1030","Electronics, Computer");
    addElement("1230","Sporting Goods");
    addElement("1290","Travel, Air");
    addElement("4011","TEST: 4011");
  }
}

