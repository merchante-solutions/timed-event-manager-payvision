package com.mes.api.bml.test;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.CityStateZipField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.NameField;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.mvc.mes.MesAction;
import com.mes.tools.DropDownTable;

public class AuthorizeBean extends BmlViewBean
{
  static Logger log = Logger.getLogger(AuthorizeBean.class);

  public static final String FN_PROFILE_ID        = "profileId";
  public static final String FN_PROFILE_KEY       = "profileKey";
  public static final String FN_TPG_URL           = "tpgUrl";
  public static final String FN_CENTINEL_URL      = "centinelUrl";
  public static final String FN_PROMO_CODE        = "promoCode";
  public static final String FN_ORDER_NUM         = "orderNum";
  public static final String FN_ORDER_DESC        = "orderDesc";
  public static final String FN_TRAN_AMOUNT       = "tranAmount";
  public static final String FN_SHIP_AMOUNT       = "shipAmount";
  public static final String FN_TAX_AMOUNT        = "taxAmount";
  public static final String FN_IP_ADDRESS        = "ipAddress";
  public static final String FN_BML_ACCT_NUM      = "bmlAcctNum";
  //public static final String FN_LOOKUP_TRAN_ID    = "lookupTranId";
  //public static final String FN_BML_ORDER_ID      = "bmlOrderId";
  public static final String FN_REF_TRAN_ID       = "refTranId";
  public static final String FN_CUST_REG_DATE     = "custRegDate";
  public static final String FN_CUST_FLAG         = "custFlag";
  public static final String FN_TRAN_MODE         = "tranMode";
  public static final String FN_PROD_CODE         = "prodCode";
  public static final String FN_TERMS             = "terms";
  public static final String FN_CAT_CODE          = "catCode";

  public static final String FN_BILL_NAME         = "billName";
  public static final String FN_BILL_FIRST_NAME   = FN_BILL_NAME + "First";
  public static final String FN_BILL_LAST_NAME    = FN_BILL_NAME + "Last";
  public static final String FN_BILL_MIDDLE_NAME  = "billNameMiddle";
  public static final String FN_BILL_ADDRESS1     = "billAddress1";
  public static final String FN_BILL_ADDRESS2     = "billAddress2";
  public static final String FN_BILL_CSZ          = "billCsz";
  public static final String FN_BILL_CSZ_CITY     = FN_BILL_CSZ + "City";
  public static final String FN_BILL_CSZ_STATE    = FN_BILL_CSZ + "State";
  public static final String FN_BILL_CSZ_ZIP      = FN_BILL_CSZ + "Zip";
  public static final String FN_BILL_PHONE1       = "billPhone1";
  public static final String FN_BILL_PHONE2       = "billPhone2";
  public static final String FN_BILL_EMAIL        = "billEmail";

  public static final String FN_SHIP_NAME         = "shipName";
  public static final String FN_SHIP_FIRST_NAME   = FN_SHIP_NAME + "First";
  public static final String FN_SHIP_LAST_NAME    = FN_SHIP_NAME + "Last";
  public static final String FN_SHIP_MIDDLE_NAME  = "shipNameMiddle";
  public static final String FN_SHIP_ADDRESS1     = "shipAddress1";
  public static final String FN_SHIP_ADDRESS2     = "shipAddress2";
  public static final String FN_SHIP_CSZ          = "shipCsz";
  public static final String FN_SHIP_CSZ_CITY     = FN_SHIP_CSZ + "City";
  public static final String FN_SHIP_CSZ_STATE    = FN_SHIP_CSZ + "State";
  public static final String FN_SHIP_CSZ_ZIP      = FN_SHIP_CSZ + "Zip";
  public static final String FN_SHIP_PHONE1       = "shipPhone1";
  public static final String FN_SHIP_PHONE2       = "shipPhone2";
  public static final String FN_SHIP_EMAIL        = "shipEmail";

  public static final String FN_HAS_CHK_ACCT      = "hasCheckingAcct";
  public static final String FN_HAS_SAV_ACCT      = "hasSavingsAcct";
  public static final String FN_RES_STATUS        = "residenceStatus";
  public static final String FN_DOB               = "dob";
  public static final String FN_SSN               = "ssn";
  public static final String FN_HH_INCOME         = "householdIncome";
  public static final String FN_EMP_YEARS         = "yearsAtEmployer";
  public static final String FN_RES_YEARS         = "yearsAtResidence";

  public static final String FN_SALE_FLAG         = "saleFlag";

  public static final String FN_SUBMIT_BTN        = "submitBtn";


  public static class YesNoDdField extends DropDownField
  {
    public static class YesNoTable extends DropDownTable
    {
      public YesNoTable()
      {
        addElement("","--");
        addElement("Y","Yes");
        addElement("N","No");
      }
    }

    public YesNoDdField(String name,boolean nullAllowed)
    {
      super(name,new YesNoTable(),nullAllowed);
    }
  }

  public static class ResidenceStatusTable extends DropDownTable
  {
    public ResidenceStatusTable()
    {
      addElement("","--");
      addElement("R","Rents");
      addElement("O","Owns");
      addElement("X","Other");
    }
  }

  public AuthorizeBean(MesAction action)
  {
    super(action);
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new DropDownField(FN_TPG_URL,new ConfigOptionTable("tpg_url",user.getLoginName()),false));
      fields.add(new DropDownField(FN_CENTINEL_URL,new ConfigOptionTable("centinel_url",user.getLoginName()),false));
      fields.add(new Field(FN_PROFILE_ID,"Profile ID",20,20,false));
      fields.add(new Field(FN_PROFILE_KEY,"Profile Key",32,32,false));
      fields.add(new DropDownField(FN_PROMO_CODE,new ProfilePromoCodeTable(),false));
      //fields.add(new Field(FN_BML_ORDER_ID,"Order ID",20,20,true));
      //fields.add(new Field(FN_LOOKUP_TRAN_ID,"Lookup Tran ID",20,20,true));
      fields.add(new Field(FN_REF_TRAN_ID,"Tran ID",32,32,true));

      fields.add(new CurrencyField(FN_TRAN_AMOUNT,"Transaction Amount",10,10,false));
      fields.add(new CurrencyField(FN_SHIP_AMOUNT,"Shipping Amount",10,10,false));
      fields.add(new CurrencyField(FN_TAX_AMOUNT,"Tax Amount",10,10,true));
      fields.add(new Field(FN_BML_ACCT_NUM,"BML Account #",19,19,true));
      fields.add(new Field(FN_ORDER_NUM,"Order #",32,32,true));
      fields.add(new Field(FN_ORDER_DESC,"Order Desc.",125,32,true));
      fields.add(new Field(FN_IP_ADDRESS,"IP Address",15,15,true));

      fields.add(new DateStringField(FN_CUST_REG_DATE,"Customer Registration Date",false,false));
      fields.add(new DropDownField(FN_CUST_FLAG,"Customer Flag",new CustomerFlagTable(),false));
      fields.add(new DropDownField(FN_CAT_CODE,"Category Code",new CategoryCodeTable(),false));
      fields.add(new DropDownField(FN_TRAN_MODE,"Transaction Mode",new TransactionModeTable(),false));
      fields.add(new DropDownField(FN_PROD_CODE,"Production Code",new ProductionCodeTable(),false));
      fields.add(new DropDownField(FN_TERMS,"Terms & Conditions",new TermsTable(),false));

      fields.add(new NameField(FN_BILL_NAME,"Billing Name",32,false,true));
      fields.add(new Field(FN_BILL_MIDDLE_NAME,"Billing Middle Name",32,32,true));
      fields.add(new Field(FN_BILL_ADDRESS1,"Billing Address",40,32,false));
      fields.add(new Field(FN_BILL_ADDRESS2,"Billing Address 2",40,32,true));
      fields.add(new CityStateZipField(FN_BILL_CSZ,"Billing Csz",32,false));
      fields.add(new PhoneField(FN_BILL_PHONE1,"Billing Phone",false));
      fields.add(new PhoneField(FN_BILL_PHONE2,"Billing Phone 2",true));
      fields.add(new EmailField(FN_BILL_EMAIL,"Shipping Email",64,32,true));

      fields.add(new NameField(FN_SHIP_NAME,"Shipping Name",32,false,true));
      fields.add(new Field(FN_SHIP_MIDDLE_NAME,"Shipping Middle Name",32,32,true));
      fields.add(new Field(FN_SHIP_ADDRESS1,"Shipping Address",40,32,false));
      fields.add(new Field(FN_SHIP_ADDRESS2,"Shipping Address 2",40,32,true));
      fields.add(new CityStateZipField(FN_SHIP_CSZ,"Shipping Csz",32,false));
      fields.add(new PhoneField(FN_SHIP_PHONE1,"Shipping Phone",false));
      fields.add(new PhoneField(FN_SHIP_PHONE2,"Shipping Phone 2",true));
      fields.add(new EmailField(FN_SHIP_EMAIL,"Shipping Email",64,32,true));

      fields.add(new YesNoDdField(FN_HAS_CHK_ACCT,true));
      fields.add(new YesNoDdField(FN_HAS_SAV_ACCT,true));
      fields.add(new DropDownField(FN_RES_STATUS,new ResidenceStatusTable(),true));
      fields.add(new DateStringField(FN_DOB,"Date of Birth",true,false));
      fields.add(new Field(FN_SSN,"Social Security #",9,9,true));
      fields.add(new CurrencyField(FN_HH_INCOME,"Household Income",10,10,true));
      fields.add(new NumberField(FN_EMP_YEARS,"Years at Employer",2,2,true,0));
      fields.add(new NumberField(FN_RES_YEARS,"Years at Residence",2,2,true,0));

      fields.add(new CheckField(FN_SALE_FLAG,"Sale Transaction","y",true));

      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  protected boolean autoLoad()
  {
    try
    {
      setDefault(FN_TPG_URL,      tc.getTpgUrl());
      setDefault(FN_CENTINEL_URL, tc.getCentinelUrl());
      setDefault(FN_PROFILE_ID,   tc.getProfileId());
      setDefault(FN_PROFILE_KEY,  tc.getProfileKey());
      if (tc.getPromoCodes().contains(tc.getDefaultPromoCode()))
      {
        setDefault(FN_PROMO_CODE,  tc.getDefaultPromoCode());
      }
      setDefault(FN_TRAN_AMOUNT,  tc.getTranAmount());
      setDefault(FN_SHIP_AMOUNT,  tc.getShipAmount());
      setDefault(FN_BILL_NAME,    tc.getBillName());
      setDefault(FN_BILL_ADDRESS1,tc.getBillAddress());
      setDefault(FN_BILL_CSZ,     tc.getBillCsz());
      setDefault(FN_BILL_PHONE1,  tc.getBillPhone());
      setDefault(FN_SHIP_NAME,    tc.getShipName());
      setDefault(FN_SHIP_ADDRESS1,tc.getShipAddress());
      setDefault(FN_SHIP_CSZ,     tc.getShipCsz());
      setDefault(FN_SHIP_PHONE1,  tc.getShipPhone());
      setDefault(FN_IP_ADDRESS,   tc.getIpAddress());
      setDefault(FN_ORDER_NUM,    tc.getOrderNum());
      
      DateStringField custRegDate = (DateStringField)getField(FN_CUST_REG_DATE);
      if (custRegDate.isBlank())
      {
        custRegDate.setUtilDate(tc.getCustRegDate());
      }
      setDefault(FN_CUST_FLAG,    tc.getCustFlag());
      setDefault(FN_CAT_CODE,     tc.getCatCode());
      setDefault(FN_TRAN_MODE,    tc.getTranMode());
      setDefault(FN_PROD_CODE,    tc.getProdCode());
      setDefault(FN_TERMS,        tc.getTerms());
      return true;
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }

    return false;
  }
}