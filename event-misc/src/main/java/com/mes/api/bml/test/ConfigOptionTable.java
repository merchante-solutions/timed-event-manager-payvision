package com.mes.api.bml.test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.tools.DropDownTable;

public class ConfigOptionTable extends DropDownTable
{
  static Logger log = Logger.getLogger(ConfigOptionTable.class);

  public ConfigOptionTable(String configTag, String userName, String emptySelector)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    if (emptySelector != null)
    {
      addElement("",emptySelector);
    }

    try
    {
      connect();

      ps = con.prepareStatement(
        " select                            " +
        "  config_value, config_name        " +
        " from                              " +
        "  bml_test_config_options          " +
        " where                             " +
        "  config_tag = '" + configTag + "' " +
        "  and ( user_name is null          " +
        "        or user_name = '" + userName + "' ) " +
        " order by user_name, order_idx     ");

      rs = ps.executeQuery();

      while (rs.next())
      {
        addElement(rs);
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      throw new RuntimeException("Error creating ConfigOptionTable: " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  public ConfigOptionTable(String configTag, String userName)
  {
    this(configTag,userName,null);
  }
}
      



