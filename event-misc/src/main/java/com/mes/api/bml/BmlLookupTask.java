package com.mes.api.bml;

import org.apache.log4j.Logger;
import com.cardinalcommerce.client.CentinelRequest;
import com.cardinalcommerce.client.CentinelResponse;
import com.mes.api.TridentApiTransaction;

public class BmlLookupTask extends BmlBaseTask
{
  static Logger log = Logger.getLogger(BmlLookupTask.class);

  private BmlLookupTran tran;

  public BmlLookupTask(TridentApiTransaction apiTran)
  {
    super(apiTran);
    tran = (BmlLookupTran)apiTran;
  }

  private void addBmlConsumerData(CentinelRequest request)
  {
    addRequestData(request,"BillingFirstName",    tran.getBillingFirstName());
    addRequestData(request,"BillingMiddleName",   tran.getBillingMiddleName());
    addRequestData(request,"BillingLastName",     tran.getBillingLastName());
    addRequestData(request,"BillingAddress1",     tran.getBillingAddress1());
    addRequestData(request,"BillingAddress2",     tran.getBillingAddress2());
    addRequestData(request,"BillingCity",         tran.getBillingCity());
    addRequestData(request,"BillingState",        tran.getBillingState());
    addRequestData(request,"BillingPostalCode",   tran.getBillingZip());
    addRequestData(request,"BillingCountryCode",  "US");
    addRequestData(request,"BillingPhone",        tran.getBillingPhone1());
    addRequestData(request,"BillingAltPhone",     tran.getBillingPhone2());
    addRequestData(request,"EMail",               tran.getBillingEmail());
    addRequestData(request,"ShippingFirstName",   tran.getShippingFirstName());
    addRequestData(request,"ShippingMiddleName",  tran.getShippingMiddleName());
    addRequestData(request,"ShippingLastName",    tran.getShippingLastName());
    addRequestData(request,"ShippingAddress1",    tran.getShippingAddress1());
    addRequestData(request,"ShippingAddress2",    tran.getShippingAddress2());
    addRequestData(request,"ShippingCity",        tran.getShippingCity());
    addRequestData(request,"ShippingState",       tran.getShippingState());
    addRequestData(request,"ShippingPostalCode",  tran.getShippingZip());
    addRequestData(request,"ShippingCountryCode", "US");
    addRequestData(request,"ShippingPhone",       tran.getShippingPhone1());
    addRequestData(request,"ShippingAltPhone",    tran.getShippingPhone2());
    addRequestData(request,"AltEMail",            tran.getShippingEmail());
  }

  public void setRequestData(CentinelRequest request)
  {
    addRequestData(request,"Amount",              stripDecimal(""+tran.getAmount()));
    addRequestData(request,"ShippingAmount",      stripDecimal(""+tran.getShipAmount()));
    addRequestData(request,"CurrencyCode",        "840");
    addRequestData(request,"OrderNumber",         tran.getOrderNum());
    addRequestData(request,"OrderDesc",           tran.getOrderDesc());
    addRequestData(request,"PromoCode",           tran.getPromoCode());
    addRequestData(request,"IPAddress",           tran.getIpAddress());
    if (tran.getTaxAmount() > 0)
    {
      addRequestData(request,"TaxAmount",           stripDecimal(""+tran.getTaxAmount()));
    }
    addBmlConsumerData(request);
  }

  public void setResponseData(CentinelResponse response)
  {
    tran.setResponseData(tran.FN_BML_TRAN_ID,     response.getValue("TransactionId"));
    tran.setResponseData(tran.FN_BML_RESP_ORDER_ID,response.getValue("OrderId"));
    tran.setResponseData(tran.FN_BML_ENROLLED,    response.getValue("Enrolled"));
    tran.setResponseData(tran.FN_BML_ACS_URL,     response.getValue("ACSUrl"));
    tran.setResponseData(tran.FN_BML_PA_REQ,      response.getValue("Payload"));
  }
}