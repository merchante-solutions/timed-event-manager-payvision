package com.mes.api.bml.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.api.bml.BmlDb;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.mvc.mes.MesAction;

public class CapLookupBean extends BmlViewBean
{
  static Logger log = Logger.getLogger(CapLookupBean.class);

  public static final String FN_FROM_DATE     = "fromDate";
  public static final String FN_TO_DATE       = "toDate";
  public static final String FN_SUBMIT_BTN    = "submitBtn";

  private List caps;

  public CapLookupBean(MesAction action)
  {
    super(action);
  }
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new DateStringField(FN_FROM_DATE,"From Date",false,false));
      fields.add(new DateStringField(FN_TO_DATE,"To Date",true,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

  public List getRows()
  {
    if (caps == null || (isAutoSubmit() && !isAutoValid()))
    {
      caps = new ArrayList();
    }
    return caps;
  }

  protected boolean autoSubmit()
  {
    setData(FN_SUBMIT_BTN,"");
    try
    {
      Date fromDate = ((DateStringField)getField(FN_FROM_DATE)).getUtilDate();
      Date toDate = ((DateStringField)getField(FN_TO_DATE)).getUtilDate();
      caps = BmlDb.lookupCaps(fromDate,toDate);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      Calendar cal = Calendar.getInstance();
      DateStringField fromField = (DateStringField)fields.getField(FN_FROM_DATE);
      if (fromField.isBlank())
      {
        DateStringField toField = (DateStringField)fields.getField(FN_TO_DATE);
        toField.setUtilDate(cal.getTime());
        cal.add(cal.DATE,-7);
        fromField.setUtilDate(cal.getTime());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}