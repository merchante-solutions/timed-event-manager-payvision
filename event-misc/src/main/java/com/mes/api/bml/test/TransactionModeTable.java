package com.mes.api.bml.test;

import com.mes.tools.DropDownTable;

public class TransactionModeTable extends DropDownTable
{
  public TransactionModeTable()
  {
    addElement("","--");
    addElement("M","MOTO");
    addElement("R","Retail");
    addElement("S","E-Commerce");
  }
}

