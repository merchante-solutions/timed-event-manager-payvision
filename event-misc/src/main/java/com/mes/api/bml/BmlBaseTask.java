package com.mes.api.bml;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import com.cardinalcommerce.client.CentinelRequest;
import com.cardinalcommerce.client.CentinelResponse;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiTransaction;
import com.mes.api.TridentApiTransactionTask;

public abstract class BmlBaseTask extends TridentApiTransactionTask 
{
  static Logger log = Logger.getLogger(BmlBaseTask.class);

  // centinel message types
  public static final String CENTINEL_MT_LOOKUP       = "cmpi_lookup";
  public static final String CENTINEL_MT_AUTHENTICATE = "cmpi_authenticate";
  public static final String CENTINEL_MT_AUTHORIZE    = "cmpi_authorize";
  public static final String CENTINEL_MT_CAPTURE      = "cmpi_capture";
  public static final String CENTINEL_MT_REAUTHORIZE  = "cmpi_reauthorize";
  public static final String CENTINEL_MT_REFUND       = "cmpi_refund";
  public static final String CENTINEL_MT_UNKNOWN      = "unknown";

  protected TridentApiConstants   tac     = new TridentApiConstants();
  protected BmlBaseTran           bmlTran;

  protected String                centMsgType;
  protected String                centTranType;
  protected long                  responseMs;

  public BmlBaseTask(TridentApiTransaction apiTran)
  {
    super(apiTran);
    bmlTran = (BmlBaseTran)apiTran;
    centMsgType = getCentinelMessageType(bmlTran.getBmlRequest());
  }

  /**
   * General Centinel BML request support
   */

  protected String getCentinelMessageType(String bmlRequest)
  {
    if (bmlRequest.equals(BmlBaseTran.BR_LOOKUP))
    {
      return CENTINEL_MT_LOOKUP;
    }
    else if (bmlRequest.equals(BmlBaseTran.BR_AUTHENTICATE))
    {
      return CENTINEL_MT_AUTHENTICATE;
    }
    else if (bmlRequest.equals(BmlBaseTran.BR_AUTHORIZE) ||
             bmlRequest.equals(BmlBaseTran.BR_SALE))
    {
      return CENTINEL_MT_AUTHORIZE;
    }
    else if (bmlRequest.equals(BmlBaseTran.BR_CAPTURE))
    {
      return CENTINEL_MT_CAPTURE;
    }
    else if (bmlRequest.equals(BmlBaseTran.BR_REAUTHORIZE))
    {
      return CENTINEL_MT_REAUTHORIZE;
    }
    else if (bmlRequest.equals(BmlBaseTran.BR_REFUND))
    {
      return CENTINEL_MT_REFUND;
    }
    return CENTINEL_MT_UNKNOWN;
  }

  protected CentinelRequest getCentinelRequest()
  {
    CentinelRequest request = new CentinelRequest();
	  request.add("MsgType",          centMsgType);
	  request.add("TransactionType",	"B");
	  request.add("Version",          tac.CENTINEL_MESSAGE_VERSION);
	  request.add("ProcessorId",      tac.CENTINEL_PROCESSOR_ID);
	  request.add("MerchantId",       String.valueOf(bmlTran.getMerchantId()));
	  request.add("TransactionPwd",	  bmlTran.getProfileKey());
    return request;
  }

  /**
   * Cardinal wants amounts in the BML requests to have implied decimals,
   * so strip them out with these.
   */

  protected String stripDecimal(String amountStr)
  {
    int idx = -1;
    BigDecimal dec = new BigDecimal(amountStr);
    StringBuffer amountBuf = new StringBuffer(""+dec.setScale(2));
    while ((idx = amountBuf.indexOf(".")) >= 0)
    {
      amountBuf.deleteCharAt(idx);
    }
    return amountBuf.toString();
  }

  /**
   * Map various Cardinal and BML error codes to TPG codes.
   */
  private boolean setErrorResponseData(CentinelResponse response)
  {
    String tpgCode      = tac.ER_NONE;
    String tpgDesc      = null;
    String cardErrNum   = response.getValue("ErrorNo");
    String cardErrDesc  = response.getValue("ErrorDesc");
    String bmlReasCode  = response.getValue("ReasonCode");
    String bmlReasDesc  = response.getValue("ReasonDesc");
    boolean hasError    = false;

    // determine what error code to use
    if (bmlReasCode != null && !bmlReasCode.equals("")
        && !bmlReasCode.equals("000")) // 
    {
      // TODO: map bml reason codes to tpg error codes
      tpgCode = tac.ER_BML_PROCESSOR_ERROR;
      if (bmlReasDesc != null && !bmlReasDesc.equals(""))
      {
        tpgDesc = bmlReasDesc + " (RC " + bmlReasCode + ")";
      }
      else if (cardErrDesc != null && !cardErrDesc.equals(""))
      {
        tpgDesc = cardErrDesc + " (RC " + bmlReasCode + ")";
      }
      hasError = true;
    }
    else if (cardErrNum != null && 
             !cardErrNum.equals("") && 
             !cardErrNum.equals("0"))
    {
      // TODO: map cardinal error nums to tpg error codes
      tpgCode = tac.ER_BML_CARDINAL_ERROR;
      if (cardErrDesc != null && !cardErrDesc.equals(""))
      {
        tpgDesc = cardErrDesc + " (EN " + cardErrNum + ")";
      }
      hasError = true;
    }

    log.debug("cardErrNum  = '" + cardErrNum + "'");
    log.debug("cardErrDesc = '" + cardErrDesc + "'");
    log.debug("bmlReasCode = '" + bmlReasCode + "'");
    log.debug("bmlReasDesc = '" + bmlReasDesc + "'");
    log.debug("tpgCode     = '" + tpgCode + "'");
    log.debug("tpgDesc     = '" + tpgDesc + "'");

    setError(tpgCode);
    if (tpgDesc != null)
    {
      setErrorDesc(tpgDesc);
    }
    return hasError;
  }

  protected void addRequestData(CentinelRequest request, String name, String data)
  {
    if (data != null)
    {
      request.add(name,data);
    }
  }

  public abstract void setRequestData(CentinelRequest request);

  public abstract void setResponseData(CentinelResponse response);

  /**
   * Send BillMeLater request via Cardinal Centinel Thin Client.
   */
  public boolean doTask()
  {
    boolean isSuccess = true;

    try
    {
      // get request object
      CentinelRequest request = getCentinelRequest();

      // load with task specific data
      setRequestData(request);

      // post the request to Cardinal Commerce
      Date startTs = Calendar.getInstance().getTime();
      CentinelResponse response
        = request.sendHTTP(bmlTran.getCardinalCentinelUrl(),
                           tac.CENTINEL_TIMEOUT_CONNECT,
                           tac.CENTINEL_TIMEOUT_READ);
      Date endTs = Calendar.getInstance().getTime();
      long responseMs = endTs.getTime() - startTs.getTime();
      log.debug("Cardinal response time: " + responseMs + " ms");

      // make sure response data is clear
      bmlTran.clearResponseData();

      // set error data if needed
      if (setErrorResponseData(response))
      {
        isSuccess = false;
      }

      // set task specific response data
      setResponseData(response);
    }
    catch( Exception e )
    {
      setError(tac.ER_BML_REQUEST_FAILED);
      log.error("Error: " + e);
      e.printStackTrace();
      logEntry("doTask()",e.toString());
      isSuccess = false;
    }

    return isSuccess;
  }
}
