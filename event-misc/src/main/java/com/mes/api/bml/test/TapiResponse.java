package com.mes.api.bml.test;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.mvc.tags.Viewable;

public class TapiResponse implements Viewable
{
  static Logger log = Logger.getLogger(TapiResponse.class);

  private Map parmMap = new HashMap();

  public TapiResponse(String rawResponse) throws Exception
  {
    String[] parms = rawResponse.split("&");
    for (int i = 0; i < parms.length; ++i)
    {
      String[] nameVal = parms[i].split("=");
      String name = URLDecoder.decode(nameVal[0],"UTF-8");
      String value = null;
      if (nameVal.length > 1)
      {
        value = URLDecoder.decode(nameVal[1],"UTF-8");
      }
      else
      {
        value = "";
      }
      if (parmMap.containsKey(name))
      {
        Object origValue = parmMap.get(name);
        List valList = null;
        if (origValue instanceof String)
        {
          valList = new ArrayList();
          valList.add(origValue);
          parmMap.put(name,valList);
        }
        else
        {
          valList = (ArrayList)origValue;
        }
        valList.add(value);
      }
      else
      {
        parmMap.put(name,value);
      }
    }
  }

  public String getParameter(String name)
  {
    Object value = parmMap.get(name);
    if (value == null)
    {
      return null;
    }
    if (value instanceof String)
    {
      return (String)value;
    }
    return (String)((List)value).get(0);
  }

  public boolean isMultiParameter(String name)
  {
    Object value = parmMap.get(name);
    return value != null && value instanceof List;
  }

  public List getMultiParameter(String name)
  {
    return (List)parmMap.get(name);
  }

  public String getParameter(String name, int index)
  {
    return (String)(getMultiParameter(name).get(index));
  }

  public List getParameterList()
  {
    return new ArrayList(parmMap.keySet());
  }

  /**
   * Viewable implementation
   */

  public class Row
  {
    String name;
    String value;

    public Row(String name, String value)
    {
      this.name = name;
      this.value = value;
    }

    public String getName()
    {
      return name;
    }
    public String getValue()
    {
      return value;
    }
  }

  private List rows = new ArrayList();

  public List getRows()
  {
    rows.clear();
    for (Iterator i = parmMap.keySet().iterator(); i.hasNext();)
    {
      String name = ""+i.next();
      String value = ""+parmMap.get(name);
      rows.add(new Row(name,value));
    }
    return rows;
  }

  public boolean isReversed()
  {
    return false;
  }

  public int getLeadCount()
  {
    return 0;
  }

  public int getTrailCount()
  {
    return 0;
  }

  public boolean isExpanded()
  {
    return true;
  }

  public boolean exceedsLimits()
  {
    return getRows().size() > getLeadCount() + getTrailCount();
  }

}

