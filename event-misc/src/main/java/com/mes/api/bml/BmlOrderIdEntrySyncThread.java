package com.mes.api.bml;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiSyncThread;
import com.mes.database.SleepyCatManager;
import com.sleepycat.bind.serial.SerialSerialKeyCreator;
import com.sleepycat.collections.StoredMap;
import com.sleepycat.collections.StoredSortedMap;

public class BmlOrderIdEntrySyncThread extends TridentApiSyncThread
{
  static Logger log = Logger.getLogger(BmlOrderIdEntrySyncThread.class);

  private StoredMap entryMap = null;      // primary key map
  private StoredSortedMap dateMap = null; // entries sorted by date map

  public BmlOrderIdEntrySyncThread(long sleepTime)
  {
    super(sleepTime);

    try
    {
      SleepyCatManager mgr = 
        SleepyCatManager.getInstance(TridentApiConstants.getApiDatabasePath());

      entryMap = mgr.getStoredMap(TridentApiConstants.API_TABLE_BML_ENTRIES);

      // create a stored map based on a secondary index for the entry table
      // that uses entry tran dates as the sorted key
      dateMap = mgr.createSecondaryStoredMap(
        TridentApiConstants.API_TABLE_BML_ENTRIES,
        TridentApiConstants.API_SI_BML_DATE_ENTRIES,
        new SerialSerialKeyCreator(mgr.getClassCatalog(),String.class,
          BmlOrderIdEntry.class,Date.class)
        {
          public Object createSecondaryKey(Object primKeyInput, Object valueInput)
          {
            BmlOrderIdEntry entry = (BmlOrderIdEntry)valueInput;
            return entry.getTranDate();
          }
        });

    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  protected void execute()
  {
    try
    {
      BmlDb db = new BmlDb();

      // remove entries older than 30 days 
      Calendar cal = Calendar.getInstance();
      cal.add(cal.DATE,-30);
      SortedMap oldMap = dateMap.headMap(cal.getTime());
      oldMap.clear();

      // if no entries loaded load first entry in last 30 days
      BmlOrderIdEntry entry = null;
      if (entryMap.isEmpty())
      {
        entry = db.lookupFirstOrderEntry(30);
        entryMap.put(entry.getTranId(),entry);
      }
      // else load most recent entry
      else
      {
        Date last = (Date)dateMap.lastKey();
        entry = (BmlOrderIdEntry)dateMap.get(last);
      }

      // load up to 100 new order entries at a time
      List newEntries = db.lookupNextEntries(entry.getRecId(),100);
      for (Iterator i = newEntries.iterator(); i.hasNext();)
      {
        entry = (BmlOrderIdEntry)i.next();
        entryMap.put(entry.getTranId(),entry);
      }
    }
    catch (Exception e)
    {
      log.error("Execute error: " + e);
      e.printStackTrace();
    }
  }
}
    
