package com.mes.api.bml;

import org.apache.log4j.Logger;
import com.cardinalcommerce.client.CentinelRequest;
import com.cardinalcommerce.client.CentinelResponse;
import com.mes.api.TridentApiTransaction;

public class BmlReauthorizeTask extends BmlBaseTask
{
  static Logger log = Logger.getLogger(BmlReauthorizeTask.class);

  private BmlReauthorizeTran tran;

  public BmlReauthorizeTask(TridentApiTransaction apiTran)
  {
    super(apiTran);
    tran = (BmlReauthorizeTran)apiTran;
  }

  public void setRequestData(CentinelRequest request)
  {
    addRequestData(request,"OrderId",       tran.getBmlReqOrderId());
	  addRequestData(request,"Amount",        stripDecimal(""+tran.getAmount()));
	  addRequestData(request,"CurrencyCode",  "840");
  }

  public void setResponseData(CentinelResponse response)
  {
    tran.setResponseData(tran.FN_BML_TRAN_ID,       response.getValue("TransactionId"));
    tran.setResponseData(tran.FN_BML_RESP_ORDER_ID, response.getValue("OrderId"));
    tran.setResponseData(tran.FN_BML_STATUS_CODE,   response.getValue("StatusCode"));
    tran.setResponseData(tran.FN_BML_ACCOUNT_NUM,   response.getValue("AccountNumber"));
  }
}