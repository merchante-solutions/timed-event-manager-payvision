package com.mes.api.bml.test;

import com.mes.tools.DropDownTable;

public class TermsTable extends DropDownTable
{
  public TermsTable()
  {
    addElement("","--");
    addElement("32103","E-Commerce Transactions");
    addElement("12103","Call Center Transactions");
  }
}
