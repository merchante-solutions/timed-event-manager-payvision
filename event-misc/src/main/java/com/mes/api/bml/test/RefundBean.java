package com.mes.api.bml.test;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.mvc.mes.MesAction;

public class RefundBean extends BmlViewBean
{
  static Logger log = Logger.getLogger(RefundBean.class);

  public static final String FN_PROFILE_ID        = "profileId";
  public static final String FN_PROFILE_KEY       = "profileKey";
  public static final String FN_ORDER_NUM         = "orderNum";
  //public static final String FN_BML_ORDER_ID      = "bmlOrderId";
  public static final String FN_REF_TRAN_ID       = "refTranId";
  public static final String FN_TRAN_AMOUNT       = "tranAmount";

  public static final String FN_TPG_URL           = "tpgUrl";
  public static final String FN_CENTINEL_URL      = "centinelUrl";

  public static final String FN_SUBMIT_BTN        = "submitBtn";

  public RefundBean(MesAction action)
  {
    super(action);
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new DropDownField(FN_TPG_URL,new ConfigOptionTable("tpg_url",user.getLoginName()),false));
      fields.add(new DropDownField(FN_CENTINEL_URL,new ConfigOptionTable("centinel_url",user.getLoginName()),false));
      fields.add(new Field(FN_PROFILE_ID,"Profile ID",20,20,false));
      fields.add(new Field(FN_PROFILE_KEY,"Profile Key",32,32,false));
      //fields.add(new Field(FN_BML_ORDER_ID,"Order ID",20,20,false));
      fields.add(new Field(FN_REF_TRAN_ID,"Tran ID",32,32,true));
      fields.add(new CurrencyField(FN_TRAN_AMOUNT,"Transaction Amount",10,10,false));

      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  protected boolean autoLoad()
  {
    try
    {
      setDefault(FN_TPG_URL,      tc.getTpgUrl());
      setDefault(FN_CENTINEL_URL, tc.getCentinelUrl());
      setDefault(FN_PROFILE_ID,   tc.getProfileId());
      setDefault(FN_PROFILE_KEY,  tc.getProfileKey());
      return true;
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }

    return false;
  }
}