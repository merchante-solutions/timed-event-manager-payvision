/*@lineinfo:filename=CardDataStoreTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/CardDataStoreTask.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.sql.ResultSet;
import com.mes.support.MesEncryption;
import com.mes.support.TridentTools;
import sqlj.runtime.ResultSetIterator;

public class CardDataStoreTask extends TridentApiTransactionTask
{
  public CardDataStoreTask( TridentApiTransaction tran )
  {
    super(tran);
  }
  
  public boolean doTask()
  {
    String                  cardId            = null;
    boolean                 retVal            = false;
    
    try
    {
      connect();
      setAutoCommit(false);   // disable auto-commit
      
      cardId = getCardId();
                          
      if ( cardId != null )
      {
        // card exists, return the existing transaction id
        Transaction.setTridentTranId( cardId );
        retVal = true;
      }
      else  // card does not exist yet, verify it then store it
      {
        // perform a mod-10 check to validate the card
        retVal = TridentTools.mod10Check(Transaction.getCardNumberFull());

        // card verification failed, set error code with a description
        // indicating it was a verfication failure
        if ( !retVal )
        {
          setErrorCode(TridentApiConstants.ER_CARD_DATA_STORE_FAILURE);
          setErrorDesc("Failed to store card data, card verification failed");
        }
        // auth request succeeds, do the card data storage
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:67^11*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_api_card_data
//              (
//                 terminal_id,                       
//                 merchant_number,
//                 card_id,
//                 card_number,
//                 exp_date,
//                 cardholder_name
//              )                                     
//              values                                
//              ( 
//                :Transaction.getProfileId(),
//                :Transaction.getMerchantId(),
//                :Transaction.getTridentTranId(),
//                :Transaction.getCardNumberFull(),
//                :Transaction.getExpDate(),
//                :Transaction.getCardholderName()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_131 = Transaction.getProfileId();
 long __sJT_132 = Transaction.getMerchantId();
 String __sJT_133 = Transaction.getTridentTranId();
 String __sJT_134 = Transaction.getCardNumberFull();
 String __sJT_135 = Transaction.getExpDate();
 String __sJT_136 = Transaction.getCardholderName();
   String theSqlTS = "insert into trident_api_card_data\n            (\n               terminal_id,                       \n               merchant_number,\n               card_id,\n               card_number,\n               exp_date,\n               cardholder_name\n            )                                     \n            values                                \n            ( \n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.api.CardDataStoreTask",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_131);
   __sJT_st.setLong(2,__sJT_132);
   __sJT_st.setString(3,__sJT_133);
   __sJT_st.setString(4,__sJT_134);
   __sJT_st.setString(5,__sJT_135);
   __sJT_st.setString(6,__sJT_136);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:87^11*/
          retVal = (Ctx.getExecutionContext().getUpdateCount() != 0);
        
          /*@lineinfo:generated-code*//*@lineinfo:90^11*/

//  ************************************************************
//  #sql [Ctx] { commit
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:93^11*/
        }
      }        
      
      // see if the card store db work failed
      if ( !retVal && !hasError() )
      {
        // set the task error to failed if a specific error was not set
        setErrorCode(TridentApiConstants.ER_CARD_DATA_STORE_FAILURE);
      }
    }
    catch( Exception e )
    {
      setErrorCode(TridentApiConstants.ER_CARD_DATA_STORE_FAILURE);
      try{ /*@lineinfo:generated-code*//*@lineinfo:107^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:107^34*/ }catch( Exception ee){} // rollback tran
      logEntry("doTask()",e.toString());
      try{ /*@lineinfo:generated-code*//*@lineinfo:109^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:109^32*/ }catch( Exception ee){}   // commit log entry
      System.out.println("doTask(): " + e.toString());
    }
    finally
    {
      setAutoCommit(true);    // restore auto commit
      cleanUp(); 
    }
    
    return( retVal );
  }
  
  protected String getCardId()
  {
    String              cardId          = null;
    String              cardNumber      = Transaction.getCardNumber();
    String              cardNumberFull  = Transaction.getCardNumberFull();
    ResultSetIterator   it              = null;
    long                merchantId      = Transaction.getMerchantId();
    ResultSet           resultSet       = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:132^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (cd idx_trid_api_card_merch_num) */
//                  card_id,
//                  card_number_enc
//          from    trident_api_card_data cd
//          where   merchant_number = :merchantId and
//                  card_number = :cardNumber
//          order by rec_id                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (cd idx_trid_api_card_merch_num) */\n                card_id,\n                card_number_enc\n        from    trident_api_card_data cd\n        where   merchant_number =  :1  and\n                card_number =  :2 \n        order by rec_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.api.CardDataStoreTask",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setString(2,cardNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.api.CardDataStoreTask",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:141^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        String decryptedValue =  new String(MesEncryption.getClient().decrypt(resultSet.getString("card_number_enc")));
        if ( cardNumberFull.equals(decryptedValue) )
        {
          cardId = resultSet.getString("card_id");
          break;
        }
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("getCardId(" + merchantId + "," + cardNumber + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
    return( cardId );
  }
}/*@lineinfo:generated-code*/