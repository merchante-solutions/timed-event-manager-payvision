/*@lineinfo:filename=CardDataDeleteTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/CardDataStoreTask.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

public class CardDataDeleteTask extends TridentApiTransactionTask
{
  public CardDataDeleteTask( TridentApiTransaction tran )
  {
    super(tran);
  }
  
  public boolean doTask()
  {
    int       recCount    = 0;
    boolean   retVal      = false;
    
    try
    {
      connect();
      setAutoCommit(false);   // disable auto-commit
      
      /*@lineinfo:generated-code*//*@lineinfo:41^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1) 
//          from    trident_api_card_data
//          where   merchant_number = :Transaction.getMerchantId() and
//                  card_id = :Transaction.getCardId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_125 = Transaction.getMerchantId();
 String __sJT_126 = Transaction.getCardId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)  \n        from    trident_api_card_data\n        where   merchant_number =  :1  and\n                card_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.api.CardDataDeleteTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_125);
   __sJT_st.setString(2,__sJT_126);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:47^7*/
      
      if ( recCount == 0 )
      {
        // card data does not exists
        setErrorCode(TridentApiConstants.ER_CARD_DATA_DOES_NOT_EXIST);
      }
      else  // card does not exist yet, store it
      {                          
        /*@lineinfo:generated-code*//*@lineinfo:56^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    trident_api_card_data
//            where   merchant_number = :Transaction.getMerchantId() and
//                    card_id = :Transaction.getCardId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_127 = Transaction.getMerchantId();
 String __sJT_128 = Transaction.getCardId();
  try {
   String theSqlTS = "delete\n          from    trident_api_card_data\n          where   merchant_number =  :1  and\n                  card_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.api.CardDataDeleteTask",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_127);
   __sJT_st.setString(2,__sJT_128);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:62^9*/
        retVal = (Ctx.getExecutionContext().getUpdateCount() != 0);
        
        /*@lineinfo:generated-code*//*@lineinfo:65^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:68^9*/
      }        
      
      if ( !retVal && !hasError() )
      {
        // set the task error to failed if a specific error was not set
        setErrorCode(TridentApiConstants.ER_CARD_DATA_DELETE_FAILURE);
      }
    }
    catch( Exception e )
    {
      setErrorCode(TridentApiConstants.ER_CARD_DATA_DELETE_FAILURE);
      try{ /*@lineinfo:generated-code*//*@lineinfo:80^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:80^34*/ }catch( Exception ee){} // rollback tran
      logEntry("doTask()",e.toString());
      try{ /*@lineinfo:generated-code*//*@lineinfo:82^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:82^32*/ }catch( Exception ee){}   // commit log entry
      System.out.println("doTask(): " + e.toString());
    }
    finally
    {
      setAutoCommit(true);    // restore auto commit
      cleanUp(); 
    }
    
    return( retVal );
  }
}/*@lineinfo:generated-code*/