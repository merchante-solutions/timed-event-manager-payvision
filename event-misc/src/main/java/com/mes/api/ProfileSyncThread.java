/*@lineinfo:filename=ProfileSyncThread*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/ProfileSyncThread.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.database.SleepyCatManager;
import sqlj.runtime.ResultSetIterator;

public class ProfileSyncThread extends TridentApiSyncThread
{
  static Logger log = Logger.getLogger(ProfileSyncThread.class);

  protected Timestamp     LastProfileDate       = null;
  protected Map           TridentProfileTable   = null;
  
  public ProfileSyncThread( long sleepTime )
  {
    super(sleepTime);
    
    try
    {
      SleepyCatManager mgr = SleepyCatManager.getInstance(TridentApiConstants.getApiDatabasePath());
      TridentProfileTable = mgr.getStoredMap(TridentApiConstants.API_TABLE_PROFILE);
    }
    catch( Exception e )
    {
      System.out.println("ProfileSyncThread - " + e.toString());
      e.printStackTrace();
      logEntry("ProfileSyncThread()", e.toString());
    }
  }
  
  protected void execute( )
  {
    ResultSetIterator       it            = null;
    Timestamp               lastDate      = null;
    ResultSet               resultSet     = null;
    String                  tid           = null;
  
    try
    {
      // call connect each time to insure a good connection
      connect(true);    
      
      /*@lineinfo:generated-code*//*@lineinfo:67^7*/

//  ************************************************************
//  #sql [Ctx] { -- less one second to prevent mid-second misses
//          select  (sysdate - 0.000016) 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "-- less one second to prevent mid-second misses\n        select  (sysdate - 0.000016)  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.api.ProfileSyncThread",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   lastDate = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:72^7*/

      ApiDb.updateProfileMap(LastProfileDate,TridentProfileTable);
      LastProfileDate = lastDate;
    
      // profile query has been moved to com.mes.api.ApiDb
      /*
      #sql [Ctx] it =
      {
         select  tp.terminal_id                   as tid,             
                 tp.merchant_number               as merchant_number, 
                 tp.bin_number                    as bin_number,
                 tp.merchant_name                 as dba_name,        
                 tp.addr_city                     as dba_city,        
                 tp.addr_line_1                   as dba_addr_1,      
                 tp.addr_state                    as dba_state,       
                 tp.addr_zip                      as dba_zip,         
                 tp.phone_number                  as phone_number,    
                 nvl(tp.profile_status,'A')       as profile_status,  
                 tp.sic_code                      as sic_code,        
                 tp.time_zone                     as time_zone,
                 tp.merchant_key                  as profile_key,
                 nvl(tp.vmc_accept,'N')           as vmc_accept,
                 nvl(tp.amex_accept,'N')          as amex_accept,
                 nvl(tp.disc_accept,'N')          as disc_accept,
                 nvl(tp.diners_accept,'N')        as dinr_accept,
                 nvl(tp.jcb_accept,'N')           as jcb_accept,
                 nvl(tp.api_credits_allowed,'N')  as api_credits_allowed,
                 nvl(tp.api_enabled,'N')          as api_enabled,
                 nvl(tpa.amex_cid_response_enabled,'N') as amex_cid_enabled,
                 tpa.avs_results_to_decline       as bad_avs_results,
                 tpa.cvv2_results_to_decline      as bad_cvv2_results,
                 decode( nvl(tpa.fx_client_id,0),
                         0,'N','Y')               as fx_enabled,
                 nvl(tpa.fx_client_id,0)          as fx_client_id,
                 nvl(tpa.fx_product_type,0)       as fx_product_type,
                 tpa.payvision_member_id          as member_id,
                 tpa.payvision_member_guid        as member_guid,
                 'Y'                              as load_bml_info,
                 nvl(tpa.bml_accept,'N')          as bml_accept,
                 tpa.bml_merch_id_1               as bml_merch_id_1,
                 tpa.bml_promo_code_1             as bml_promo_code_1,
                 tpa.bml_merch_id_2               as bml_merch_id_2,
                 tpa.bml_promo_code_2             as bml_promo_code_2,
                 tpa.bml_merch_id_3               as bml_merch_id_3,
                 tpa.bml_promo_code_3             as bml_promo_code_3,
                 tpa.bml_merch_id_4               as bml_merch_id_4,
                 tpa.bml_promo_code_4             as bml_promo_code_4,
                 tpa.bml_merch_id_5               as bml_merch_id_5,
                 tpa.bml_promo_code_5             as bml_promo_code_5
         from    trident_profile             tp,
                 trident_profile_api         tpa 
         where   tp.last_modified_date >= nvl(:LastProfileDate,'01-JAN-2000') and
                 tpa.terminal_id(+) = tp.terminal_id
      };
      resultSet = it.getResultSet();
    
      while( resultSet.next() && Running )
      {
        tid = resultSet.getString("tid");

        if ( resultSet.getString("api_enabled").equals("Y") &&
             resultSet.getString("profile_status").equals("A") &&
             resultSet.getString("profile_key") != null )
        {
          // insert or update
          tp = new TridentApiProfile(resultSet);
          TridentProfileTable.put( tid, tp );
        }
        else
        {
          TridentProfileTable.remove( tid );
        }
      }
      resultSet.close();
      it.close();
      */
    }
    catch( Exception e )
    {
      System.out.println("execute() - " + e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/