/*@lineinfo:filename=PreAuthSyncThread*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/PreAuthSyncThread.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Map;
import com.mes.database.SleepyCatManager;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class PreAuthSyncThread extends TridentApiSyncThread
{
  protected Map           AuthNetPreAuthTable   = null;
  protected Timestamp     LastPreAuthDate       = null;
  protected boolean       PreLoadRequest        = false;
  protected Map           TridentApiParamsTable = null;
  protected Map           TridentPreAuthTable   = null;
  
  public PreAuthSyncThread( long sleepTime )
  {
    super(sleepTime);
    
    try
    {
      SleepyCatManager mgr = SleepyCatManager.getInstance(TridentApiConstants.getApiDatabasePath());
      TridentPreAuthTable = mgr.getStoredMap(TridentApiConstants.API_TABLE_PREAUTH);
      AuthNetPreAuthTable = mgr.getStoredMap(TridentApiConstants.API_TABLE_AN_PREAUTH);
      TridentApiParamsTable = mgr.getStoredMap(TridentApiConstants.API_TABLE_PARAMS);
      LastPreAuthDate = (Timestamp)TridentApiParamsTable.get("LastPreAuthDate");
    }
    catch( Exception e )
    {
      System.out.println("PreAuthSyncThread - " + e.toString());
      e.printStackTrace();
      logEntry("PreAuthSyncThread()", e.toString());
    }
  }
  
  protected void execute( )
  {
    int                     emulation     = 0;
    ResultSetIterator       it            = null;
    Timestamp               lastDate      = null;
    Map                     map           = null;
    int                     preLoad       = (isPreLoadRequest() ? 1 : 0);
    int                     recCount      = 0;
    ResultSet               resultSet     = null;
    String                  tranId        = null;
    String                  tranType      = null;
  
    try
    {
      // call connect each time to insure a good connection
      connect(true);    
      
      /*@lineinfo:generated-code*//*@lineinfo:77^7*/

//  ************************************************************
//  #sql [Ctx] { -- less one second to prevent mid-second misses
//          select  (sysdate - 0.000016) 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "-- less one second to prevent mid-second misses\n        select  (sysdate - 0.000016)  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.api.PreAuthSyncThread",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   lastDate = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:82^7*/
      
      if ( LastPreAuthDate == null )
      {
        /*@lineinfo:generated-code*//*@lineinfo:86^9*/

//  ************************************************************
//  #sql [Ctx] { select  (sysdate-30)
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  (sysdate-30)\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.api.PreAuthSyncThread",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   LastPreAuthDate = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:91^9*/
      }
      
      String dateTimeString = DateTimeFormatter.getFormattedDate(LastPreAuthDate,"MM/dd/yyyy HH:mm:ss");
    
      /*@lineinfo:generated-code*//*@lineinfo:96^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (tapi idx_tapi_last_modified_date) */
//                  tapi.terminal_id                as terminal_id,
//                  tapi.trident_tran_id            as trident_tran_id,
//                  tapi.trident_tran_id_number     as trident_tran_id_num,
//                  tapi.external_tran_id           as external_tran_id,
//                  tapi.dba_name                   as dba_name,
//                  tapi.dba_city                   as dba_city,
//                  tapi.dba_state                  as dba_state,
//                  tapi.dba_zip                    as dba_zip,
//                  tapi.debit_credit_indicator     as debit_credit_ind,
//                  tapi.sic_code                   as sic_code,
//                  tapi.phone_number               as phone_number,
//                  tapi.card_number                as card_number,
//                  tapi.transaction_date           as tran_date,
//                  tapi.transaction_amount         as tran_amount,
//                  tapi.auth_amount                as auth_amount,
//                  tapi.auth_returned_aci          as returned_aci,
//                  tapi.auth_code                  as auth_code,
//                  tapi.reference_number           as reference_number,
//                  tapi.purchase_id                as purchase_id,
//                  tapi.transaction_type           as tran_type,
//                  tapi.debit_network_id           as debit_network_id,
//                  tapi.merchant_number            as merchant_number,
//                  tapi.pos_entry_mode             as pos_entry_mode,
//                  tapi.currency_code              as currency_code,
//                  tapi.avs_zip                    as avs_zip,
//                  decode( nvl(tapi.emulation,'TPG'),
//                          'AUTHNET',1,  /* EMU_AUTHORIZE_NET */
//                          0 )  /* EMU_TPG */      as emulation,
//                  (
//                    tapi.terminal_id || '-' ||
//                    to_char(tapi.trident_tran_id_number)
//                  )                               as auth_net_tran_id,
//                  tapi.fx_rate_id                 as fx_rate_id,
//                  tapi.fx_amount_base             as fx_amount,
//                  tapi.fx_rate                    as fx_rate,
//                  tapi.fx_reference_number        as fx_ref_num,
//                  nvl(tapi.test_flag,'Y')         as test_flag
//          from    trident_capture_api     tapi
//          where   tapi.last_modified_date between to_date(:dateTimeString,'mm/dd/yyyy hh24:mi:ss') and sysdate 
//                  and not tapi.trident_tran_id is null
//                  and ( :preLoad = 0 or tapi.transaction_type = 'P' )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (tapi idx_tapi_last_modified_date) */\n                tapi.terminal_id                as terminal_id,\n                tapi.trident_tran_id            as trident_tran_id,\n                tapi.trident_tran_id_number     as trident_tran_id_num,\n                tapi.external_tran_id           as external_tran_id,\n                tapi.dba_name                   as dba_name,\n                tapi.dba_city                   as dba_city,\n                tapi.dba_state                  as dba_state,\n                tapi.dba_zip                    as dba_zip,\n                tapi.debit_credit_indicator     as debit_credit_ind,\n                tapi.sic_code                   as sic_code,\n                tapi.phone_number               as phone_number,\n                tapi.card_number                as card_number,\n                tapi.transaction_date           as tran_date,\n                tapi.transaction_amount         as tran_amount,\n                tapi.auth_amount                as auth_amount,\n                tapi.auth_returned_aci          as returned_aci,\n                tapi.auth_code                  as auth_code,\n                tapi.reference_number           as reference_number,\n                tapi.purchase_id                as purchase_id,\n                tapi.transaction_type           as tran_type,\n                tapi.debit_network_id           as debit_network_id,\n                tapi.merchant_number            as merchant_number,\n                tapi.pos_entry_mode             as pos_entry_mode,\n                tapi.currency_code              as currency_code,\n                tapi.avs_zip                    as avs_zip,\n                decode( nvl(tapi.emulation,'TPG'),\n                        'AUTHNET',1,  /* EMU_AUTHORIZE_NET */\n                        0 )  /* EMU_TPG */      as emulation,\n                (\n                  tapi.terminal_id || '-' ||\n                  to_char(tapi.trident_tran_id_number)\n                )                               as auth_net_tran_id,\n                tapi.fx_rate_id                 as fx_rate_id,\n                tapi.fx_amount_base             as fx_amount,\n                tapi.fx_rate                    as fx_rate,\n                tapi.fx_reference_number        as fx_ref_num,\n                nvl(tapi.test_flag,'Y')         as test_flag\n        from    trident_capture_api     tapi\n        where   tapi.last_modified_date between to_date( :1 ,'mm/dd/yyyy hh24:mi:ss') and sysdate \n                and not tapi.trident_tran_id is null\n                and (  :2  = 0 or tapi.transaction_type = 'P' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.api.PreAuthSyncThread",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,dateTimeString);
   __sJT_st.setInt(2,preLoad);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.api.PreAuthSyncThread",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:140^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() && Running )
      {
        emulation = resultSet.getInt("emulation");
        tranType  = resultSet.getString("tran_type");
        
        switch( emulation )
        {
          case TridentApiConstants.EMU_AUTHORIZE_NET:
            map     = AuthNetPreAuthTable;
            tranId  = resultSet.getString("auth_net_tran_id");
            break;
          
          default:
            map     = TridentPreAuthTable;
            tranId  = resultSet.getString("trident_tran_id");
            break;
        }
        
        if ( TridentApiTranBase.TT_PRE_AUTH.equals(tranType) )
        {
          map.put(tranId, new TridentApiTransaction(resultSet));
        }
        else 
        {
          map.remove(tranId);
        }
        
        if ( 1 == preLoad )
        {
          System.out.print(((recCount == 0) ? "\n" : "") + "scanned: " + ++recCount + "          \r");
        }
      }
      resultSet.close();
      LastPreAuthDate = lastDate;
      
      TridentApiParamsTable.put("LastPreAuthDate",LastPreAuthDate);
    }
    catch( Exception e )
    {
      System.out.println("execute() - " + e.toString());
      e.printStackTrace();
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {} 
      cleanUp();
    }
  }
  
  public boolean isPreLoadRequest()
  {
    return( PreLoadRequest );
  }
  
  public void setPreLoadRequest( boolean preLoad )
  {
    PreLoadRequest = preLoad;
  }
  
  public void setLastPreAuthDate( Timestamp lastLoadDate )
  {
    LastPreAuthDate = lastLoadDate;
  }
}/*@lineinfo:generated-code*/