/*@lineinfo:filename=SettleTransactionTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/api/SettleTransactionTask.sqlj $

  Description:

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import com.mes.database.SleepyCatManager;
import com.mes.support.SyncLog;
import sqlj.runtime.ResultSetIterator;

public class SettleTransactionTask extends TridentApiTransactionTask
{
  public SettleTransactionTask( TridentApiTransaction tran )
  {
    super(tran);
  }
  
  public boolean doTask()
  {
    boolean             retVal      = false;
    String              tid         = null;
    String              tranId      = null;
    String batchId = null;
    String batchNumber = null;
    
    try
    {
      connect();
      setAutoCommit(false);   // disable auto-commit
      
      {
        // Retrieve the current batch ID/num from the profile.
        if(!Transaction.getTranType().equals(TridentApiTranBase.TT_PRE_AUTH)) {
          String q = "select current_batch_id, current_batch_number from trident_profile_api where terminal_id = ?";
           PreparedStatement ps = getPreparedStatement(q);
           ps.setString(1, Transaction.getProfileId());
           ResultSet rs = ps.executeQuery();
           if(rs.next()) {
             batchId = rs.getString("current_batch_id");
             batchNumber = rs.getString("current_batch_number");
           }
           ps.close();
           rs.close();
        }
      }
      
      if( Transaction instanceof AuthorizeNetTransaction )
      {
        loadAuthNetData();  // resolve the GUID
      }
      
      tid    = Transaction.getProfileId();
      tranId = Transaction.getTridentTranId();
      
      if ( isTransactionSettled(tranId) )
      {
        if ( Transaction.isMultiCaptureAllowed() == true )
        {
          retVal = processMultiCapRequest();
        }
        else
        {
          retVal = true;      // already settled, remove from list
        }
      }
      else
      {
        // if this is a FX transaction then mark set the post flag to "P"ending
        String fxPostFlag = (Transaction.isFxTransaction() ? "P" : null);
        double tranAmount = Transaction.getTranAmount();
        double fxAmount   = Transaction.getFxAmount();
        
        /*@lineinfo:generated-code*//*@lineinfo:93^9*/

//  ************************************************************
//  #sql [Ctx] { update trident_capture_api 
//            set transaction_type = :TridentApiTranBase.TT_DEBIT,
//                transaction_amount = :tranAmount,
//                transaction_date = trunc(sysdate),
//                reference_number = nvl( :Transaction.getReferenceNumber(), reference_number ),
//                client_reference_number = nvl( :Transaction.getClientReferenceNumber(), client_reference_number ),
//                purchase_id = nvl( :Transaction.getPurchaseId(), purchase_id ),
//                tax_amount = decode( :Transaction.getTaxAmount(),
//                                     0,tax_amount,
//                                     :Transaction.getTaxAmount() ),
//                trident_tran_id_number = :Transaction.getTridentTranIdNumber(),
//                external_tran_id = :Transaction.getExternalTranId(),
//                fx_amount_base = 
//                  decode( :fxAmount, 
//                          0, decode( nvl(fx_rate,0),  -- skip if there is no rate
//                                     0, 0,
//                                     decode( :tranAmount,
//                                             transaction_amount,fx_amount_base,
//                                             round((:tranAmount/fx_rate),2) )
//                                   ),
//                          :fxAmount ),
//                fx_post_status = :fxPostFlag,
//                fx_post_ts = null,
//                level_iii_data_present  = :Transaction.hasLevelIIIData() ? "Y" : "N",
//                merchant_tax_id         = :Transaction.getLevelIIIField(TridentApiConstants.FN_MERCHANT_TAX_ID),
//                customer_tax_id         = :Transaction.getLevelIIIField(TridentApiConstants.FN_CUSTOMER_TAX_ID),
//                summary_commodity_code  = :Transaction.getLevelIIIField(TridentApiConstants.FN_SUMMARY_COMMODITY_CODE),
//                discount_amount         = :Transaction.getLevelIIIField(TridentApiConstants.FN_DISCOUNT_AMOUNT),
//                shipping_amount         = :Transaction.getLevelIIIField(TridentApiConstants.FN_SHIPPING_AMOUNT),
//                duty_amount             = :Transaction.getLevelIIIField(TridentApiConstants.FN_DUTY_AMOUNT),
//                ship_to_zip             = :Transaction.getShipToZip(),
//                ship_from_zip           = :Transaction.getLevelIIIField(TridentApiConstants.FN_SHIP_FROM_ZIP),
//                dest_country_code       = :Transaction.getShipToCountryCode(),
//                vat_invoice_number      = :Transaction.getLevelIIIField(TridentApiConstants.FN_VAT_INVOICE_NUMBER),
//                order_date              = :Transaction.getLevelIIIFieldAsDate(TridentApiConstants.FN_ORDER_DATE),
//                vat_amount              = :Transaction.getLevelIIIField(TridentApiConstants.FN_VAT_AMOUNT),
//                vat_rate                = :Transaction.getLevelIIIField(TridentApiConstants.FN_VAT_RATE),
//                alternate_tax_amount    = :Transaction.getLevelIIIField(TridentApiConstants.FN_ALT_TAX_AMOUNT),
//                alternate_tax_indicator = :Transaction.getLevelIIIField(TridentApiConstants.FN_ALT_TAX_AMOUNT_IND),
//                load_filename           = decode(instr(load_filename,'hold-fraud'),0,load_filename,null),
//                batch_id                = :batchId,
//                batch_number            = :batchNumber
//            where trident_tran_id = :tranId 
//                  and terminal_id = :tid 
//                  and transaction_type = :TridentApiTranBase.TT_PRE_AUTH
//                  and mesdb_timestamp >= sysdate-60
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_150 = Transaction.getReferenceNumber();
 String __sJT_151 = Transaction.getClientReferenceNumber();
 String __sJT_152 = Transaction.getPurchaseId();
 double __sJT_153 = Transaction.getTaxAmount();
 double __sJT_154 = Transaction.getTaxAmount();
 long __sJT_155 = Transaction.getTridentTranIdNumber();
 String __sJT_156 = Transaction.getExternalTranId();
 String __sJT_157 = Transaction.hasLevelIIIData() ? "Y" : "N";
 String __sJT_158 = Transaction.getLevelIIIField(TridentApiConstants.FN_MERCHANT_TAX_ID);
 String __sJT_159 = Transaction.getLevelIIIField(TridentApiConstants.FN_CUSTOMER_TAX_ID);
 String __sJT_160 = Transaction.getLevelIIIField(TridentApiConstants.FN_SUMMARY_COMMODITY_CODE);
 String __sJT_161 = Transaction.getLevelIIIField(TridentApiConstants.FN_DISCOUNT_AMOUNT);
 String __sJT_162 = Transaction.getLevelIIIField(TridentApiConstants.FN_SHIPPING_AMOUNT);
 String __sJT_163 = Transaction.getLevelIIIField(TridentApiConstants.FN_DUTY_AMOUNT);
 String __sJT_164 = Transaction.getShipToZip();
 String __sJT_165 = Transaction.getLevelIIIField(TridentApiConstants.FN_SHIP_FROM_ZIP);
 String __sJT_166 = Transaction.getShipToCountryCode();
 String __sJT_167 = Transaction.getLevelIIIField(TridentApiConstants.FN_VAT_INVOICE_NUMBER);
 java.sql.Date __sJT_168 = Transaction.getLevelIIIFieldAsDate(TridentApiConstants.FN_ORDER_DATE);
 String __sJT_169 = Transaction.getLevelIIIField(TridentApiConstants.FN_VAT_AMOUNT);
 String __sJT_170 = Transaction.getLevelIIIField(TridentApiConstants.FN_VAT_RATE);
 String __sJT_171 = Transaction.getLevelIIIField(TridentApiConstants.FN_ALT_TAX_AMOUNT);
 String __sJT_172 = Transaction.getLevelIIIField(TridentApiConstants.FN_ALT_TAX_AMOUNT_IND);
   String theSqlTS = "update trident_capture_api \n          set transaction_type =  :1 ,\n              transaction_amount =  :2 ,\n              transaction_date = trunc(sysdate),\n              reference_number = nvl(  :3 , reference_number ),\n              client_reference_number = nvl(  :4 , client_reference_number ),\n              purchase_id = nvl(  :5 , purchase_id ),\n              tax_amount = decode(  :6 ,\n                                   0,tax_amount,\n                                    :7  ),\n              trident_tran_id_number =  :8 ,\n              external_tran_id =  :9 ,\n              fx_amount_base = \n                decode(  :10 , \n                        0, decode( nvl(fx_rate,0),  -- skip if there is no rate\n                                   0, 0,\n                                   decode(  :11 ,\n                                           transaction_amount,fx_amount_base,\n                                           round(( :12 /fx_rate),2) )\n                                 ),\n                         :13  ),\n              fx_post_status =  :14 ,\n              fx_post_ts = null,\n              level_iii_data_present  =  :15 ,\n              merchant_tax_id         =  :16 ,\n              customer_tax_id         =  :17 ,\n              summary_commodity_code  =  :18 ,\n              discount_amount         =  :19 ,\n              shipping_amount         =  :20 ,\n              duty_amount             =  :21 ,\n              ship_to_zip             =  :22 ,\n              ship_from_zip           =  :23 ,\n              dest_country_code       =  :24 ,\n              vat_invoice_number      =  :25 ,\n              order_date              =  :26 ,\n              vat_amount              =  :27 ,\n              vat_rate                =  :28 ,\n              alternate_tax_amount    =  :29 ,\n              alternate_tax_indicator =  :30 ,\n              load_filename           = decode(instr(load_filename,'hold-fraud'),0,load_filename,null),\n              batch_id                =  :31 ,\n              batch_number            =  :32 \n          where trident_tran_id =  :33  \n                and terminal_id =  :34  \n                and transaction_type =  :35 \n                and mesdb_timestamp >= sysdate-60";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.api.SettleTransactionTask",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,TridentApiTranBase.TT_DEBIT);
   __sJT_st.setDouble(2,tranAmount);
   __sJT_st.setString(3,__sJT_150);
   __sJT_st.setString(4,__sJT_151);
   __sJT_st.setString(5,__sJT_152);
   __sJT_st.setDouble(6,__sJT_153);
   __sJT_st.setDouble(7,__sJT_154);
   __sJT_st.setLong(8,__sJT_155);
   __sJT_st.setString(9,__sJT_156);
   __sJT_st.setDouble(10,fxAmount);
   __sJT_st.setDouble(11,tranAmount);
   __sJT_st.setDouble(12,tranAmount);
   __sJT_st.setDouble(13,fxAmount);
   __sJT_st.setString(14,fxPostFlag);
   __sJT_st.setString(15,__sJT_157);
   __sJT_st.setString(16,__sJT_158);
   __sJT_st.setString(17,__sJT_159);
   __sJT_st.setString(18,__sJT_160);
   __sJT_st.setString(19,__sJT_161);
   __sJT_st.setString(20,__sJT_162);
   __sJT_st.setString(21,__sJT_163);
   __sJT_st.setString(22,__sJT_164);
   __sJT_st.setString(23,__sJT_165);
   __sJT_st.setString(24,__sJT_166);
   __sJT_st.setString(25,__sJT_167);
   __sJT_st.setDate(26,__sJT_168);
   __sJT_st.setString(27,__sJT_169);
   __sJT_st.setString(28,__sJT_170);
   __sJT_st.setString(29,__sJT_171);
   __sJT_st.setString(30,__sJT_172);
   __sJT_st.setString(31,batchId);
   __sJT_st.setString(32,batchNumber);
   __sJT_st.setString(33,tranId);
   __sJT_st.setString(34,tid);
   __sJT_st.setString(35,TridentApiTranBase.TT_PRE_AUTH);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:141^9*/
        
        // the only way for a fraud deny transaction to get here is to have
        // requested an authorization from the issuer during the settle request
        // see com.mes.servlets.TridentApiServletWorker
        if ( TridentApiConstants.EDNA_RESP_DENY.equals(Transaction.getFraudResult()) )
        {
          if ( Transaction.VisaDResponse != null )
          {
            // store the authorization data
            /*@lineinfo:generated-code*//*@lineinfo:151^13*/

//  ************************************************************
//  #sql [Ctx] { update  trident_capture_api
//                set   auth_code               = :Transaction.VisaDResponse.getApprovalCode(),
//                      auth_response_code      = :Transaction.VisaDResponse.getResponseCode(),
//                      auth_ref_num            = :Transaction.VisaDResponse.getRetrievalReferenceNumber(),
//                      auth_tran_id            = :Transaction.VisaDResponse.getTransactionIdentifier(),
//                      auth_val_code           = :Transaction.VisaDResponse.getValidationCode(),
//                      auth_source_code        = :Transaction.VisaDResponse.getAuthorizationSourceCode(),
//                      auth_avs_response       = :Transaction.VisaDResponse.getAVSResultCode(),
//                      auth_returned_aci       = :Transaction.VisaDResponse.getReturnedACI(),
//                      original_trident_tran_id= :Transaction.VisaDResponse.getUuid(),
//                      auth_amount             = :Transaction.getAuthAmount(),
//                      auth_date               = :Transaction.getTranDate()
//                where trident_tran_id = :tranId 
//                      and terminal_id = :tid 
//                      and mesdb_timestamp >= sysdate-60
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_173 = Transaction.VisaDResponse.getApprovalCode();
 String __sJT_174 = Transaction.VisaDResponse.getResponseCode();
 String __sJT_175 = Transaction.VisaDResponse.getRetrievalReferenceNumber();
 String __sJT_176 = Transaction.VisaDResponse.getTransactionIdentifier();
 String __sJT_177 = Transaction.VisaDResponse.getValidationCode();
 String __sJT_178 = Transaction.VisaDResponse.getAuthorizationSourceCode();
 String __sJT_179 = Transaction.VisaDResponse.getAVSResultCode();
 String __sJT_180 = Transaction.VisaDResponse.getReturnedACI();
 String __sJT_181 = Transaction.VisaDResponse.getUuid();
 double __sJT_182 = Transaction.getAuthAmount();
 java.sql.Date __sJT_183 = Transaction.getTranDate();
   String theSqlTS = "update  trident_capture_api\n              set   auth_code               =  :1 ,\n                    auth_response_code      =  :2 ,\n                    auth_ref_num            =  :3 ,\n                    auth_tran_id            =  :4 ,\n                    auth_val_code           =  :5 ,\n                    auth_source_code        =  :6 ,\n                    auth_avs_response       =  :7 ,\n                    auth_returned_aci       =  :8 ,\n                    original_trident_tran_id=  :9 ,\n                    auth_amount             =  :10 ,\n                    auth_date               =  :11 \n              where trident_tran_id =  :12  \n                    and terminal_id =  :13  \n                    and mesdb_timestamp >= sysdate-60";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.api.SettleTransactionTask",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_173);
   __sJT_st.setString(2,__sJT_174);
   __sJT_st.setString(3,__sJT_175);
   __sJT_st.setString(4,__sJT_176);
   __sJT_st.setString(5,__sJT_177);
   __sJT_st.setString(6,__sJT_178);
   __sJT_st.setString(7,__sJT_179);
   __sJT_st.setString(8,__sJT_180);
   __sJT_st.setString(9,__sJT_181);
   __sJT_st.setDouble(10,__sJT_182);
   __sJT_st.setDate(11,__sJT_183);
   __sJT_st.setString(12,tranId);
   __sJT_st.setString(13,tid);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:168^13*/
          }
          
        }
      
        // only remove if the update worked
        retVal = (Ctx.getExecutionContext().getUpdateCount() != 0);
        
        List lineItemData = Transaction.getLineItemData();
        if ( retVal && lineItemData != null )
        {
          String    cardType    = null;
          long      recId       = 0L;
        
          /*@lineinfo:generated-code*//*@lineinfo:182^11*/

//  ************************************************************
//  #sql [Ctx] { select  rec_id, card_type
//              
//              from    trident_capture_api tapi
//              where   tapi.trident_tran_id = :tranId and
//                      tapi.terminal_id = :tid
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  rec_id, card_type\n             \n            from    trident_capture_api tapi\n            where   tapi.trident_tran_id =  :1  and\n                    tapi.terminal_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.api.SettleTransactionTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tranId);
   __sJT_st.setString(2,tid);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cardType = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:189^11*/
        
          if ( !cardType.equals( Transaction.getLevelIIICardType() ) )
          {
            try{ /*@lineinfo:generated-code*//*@lineinfo:193^18*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:193^40*/ }catch( Exception ee){} // rollback tran
            setError( TridentApiConstants.ER_LEVEL_III_CARD_TYPE_MISMATCH );
            retVal = false;
          }
        
          for( int i = 0; i < lineItemData.size() && retVal; ++i )
          {
            /*@lineinfo:generated-code*//*@lineinfo:200^13*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_api_line_item_detail
//                (
//                  rec_id,
//                  line_item_id,
//                  card_type,
//                  raw_line_item_data
//                )
//                values
//                (
//                  :recId,
//                  :i,
//                  :cardType,
//                  :(String)lineItemData.get(i)
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_184 = (String)lineItemData.get(i);
   String theSqlTS = "insert into trident_api_line_item_detail\n              (\n                rec_id,\n                line_item_id,\n                card_type,\n                raw_line_item_data\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.api.SettleTransactionTask",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setInt(2,i);
   __sJT_st.setString(3,cardType);
   __sJT_st.setString(4,__sJT_184);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:216^13*/
          }
        }
        
        if ( retVal )
        {
          ResultSetIterator   it          = null;
          ResultSet           resultSet   = null;
      
          try
          {
            // issue partial reversal if necessary
            /*@lineinfo:generated-code*//*@lineinfo:228^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  (auth_amount - transaction_amount)  as reversal_amount
//                from    trident_capture_api   tapi
//                where   tapi.trident_tran_id = :tranId and
//                        tapi.terminal_id = :tid and
//                        tapi.transaction_type = :TridentApiTranBase.TT_DEBIT and
//                        tapi.transaction_amount < tapi.auth_amount
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  (auth_amount - transaction_amount)  as reversal_amount\n              from    trident_capture_api   tapi\n              where   tapi.trident_tran_id =  :1  and\n                      tapi.terminal_id =  :2  and\n                      tapi.transaction_type =  :3  and\n                      tapi.transaction_amount < tapi.auth_amount";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.api.SettleTransactionTask",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,tranId);
   __sJT_st.setString(2,tid);
   __sJT_st.setString(3,TridentApiTranBase.TT_DEBIT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.api.SettleTransactionTask",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:236^13*/
            resultSet = it.getResultSet();
        
            if ( resultSet.next() && !Transaction.usePayVision() )
            {
              loadAuthReversalData();
              Transaction.setTranAmount( resultSet.getDouble("reversal_amount") );
              Transaction.reverseAuthorization(); // issue partial reversal request
            }
            resultSet.close();
          }
          catch( Exception re )
          {
            SyncLog.logThrottled(this.getClass().getName()+"::doTask(" + tranId + ")", re);
          }
          finally
          {
            try{ it.close(); } catch( Exception ee ) {}
          }
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:257^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:260^9*/
      }   // end if ( isTransactionSettled(..) )
    }
    catch( Exception e )
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:265^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:265^34*/ }catch( Exception ee){} // rollback tran
      SyncLog.logThrottled(this.getClass().getName()+"::doTask(" + tranId + ")", e);
      try{ /*@lineinfo:generated-code*//*@lineinfo:267^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:267^32*/ }catch( Exception ee){}   // commit log entry
      
      if ( getErrorCode() == TridentApiConstants.ER_NONE )
      {
        setErrorCode(TridentApiConstants.ER_INTERNAL_ERROR);
        setErrorDesc("Internal Error - Try Again");
      }
    }
    finally
    {
      setAutoCommit(true);    // restore auto commit
      cleanUp(); 
    }
    return( retVal );
  }
  
  protected boolean isTransactionSettled( String tranId )
  {
    ResultSetIterator   it            = null;
    boolean             retVal        = false;
    ResultSet           resultSet     = null;
      
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:293^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tapi.card_number_enc  as card_number_enc,
//                  tapi.auth_code        as auth_code,
//                  tapi.exp_date         as exp_date,
//                  tapi.currency_code    as currency_code,
//                  case
//                    when nvl(tapi.load_filename,'blank') like 'hold-fraud%' then upper(substr(tapi.load_filename,12))
//                    else null
//                  end                   as fraud_result,
//                  case 
//                    when nvl(tt.settle,'N') = 'Y' or tapi.transaction_type = 'V' then 1
//                    else 0
//                  end                   as settled
//          from    trident_capture_api             tapi,
//                  trident_capture_api_tran_type   tt
//          where   tapi.trident_tran_id = :tranId
//                  and tt.tran_type(+) = tapi.transaction_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tapi.card_number_enc  as card_number_enc,\n                tapi.auth_code        as auth_code,\n                tapi.exp_date         as exp_date,\n                tapi.currency_code    as currency_code,\n                case\n                  when nvl(tapi.load_filename,'blank') like 'hold-fraud%' then upper(substr(tapi.load_filename,12))\n                  else null\n                end                   as fraud_result,\n                case \n                  when nvl(tt.settle,'N') = 'Y' or tapi.transaction_type = 'V' then 1\n                  else 0\n                end                   as settled\n        from    trident_capture_api             tapi,\n                trident_capture_api_tran_type   tt\n        where   tapi.trident_tran_id =  :1 \n                and tt.tran_type(+) = tapi.transaction_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.api.SettleTransactionTask",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,tranId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.api.SettleTransactionTask",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:311^7*/
      resultSet = it.getResultSet();
      resultSet.next();
      
      Transaction.setCardNumberEnc( resultSet.getString("card_number_enc")  );
      Transaction.setExpDate      ( resultSet.getString("exp_date")         );
      Transaction.setAuthCode     ( resultSet.getString("auth_code")        );
      Transaction.setCurrencyCode ( resultSet.getString("currency_code")    );
      Transaction.setFraudResult  ( resultSet.getString("fraud_result")     );
      retVal = (resultSet.getInt("settled") == 1);
      
      resultSet.close();
    }
    catch( Exception e )
    {
      SyncLog.logThrottled(this.getClass().getName()+"::isTransactionSettled(" + tranId + ")", e);
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ){}
      cleanUp(); 
    }
    return( retVal );
  }
  
  protected boolean processMultiCapRequest()
  {
    boolean       retVal      = false;
    
    try
    {
      // set the default test flag status before reloading original
      // this prevents production from blending with test transactions
      Transaction.setTestFlag( ApiDb.loadTestFlag(ProdFlagDefault) );
    
      OriginalTranLoadTask loadTask = new OriginalTranLoadTask(Transaction);
      if ( loadTask.doTask() )
      {
        if ( Transaction.requestAuthorization() == false )
        { 
          // transaction did not authorize, run as an offline force
          Transaction.setTransactionType( TridentApiTransaction.TT_FORCE );
          Transaction.setAuthCode( loadTask.getOriginalAuthCode() );
        }
        
        // attempt to store to Oracle immediately
        StoreTransactionTask task = new StoreTransactionTask(Transaction);
        if ( !task.doTask() )
        {
          // failed to store to Oracle, have background
          // thread continue to attempt until Oracle returns
          SleepyCatManager mgr = SleepyCatManager.getInstance(TridentApiConstants.getApiDatabasePath());
          Map tbl = mgr.getStoredMap(TridentApiConstants.API_TABLE_CAPTURE);
          tbl.put(Transaction.getTridentTranId(),Transaction);
        }
        retVal = true;    // multicap processed
      }
    }
    catch( Exception e )
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:371^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:371^34*/ }catch( Exception ee){} // rollback tran
      SyncLog.logThrottled(this.getClass().getName()+"::processMultiCapRequest", e);
      try{ /*@lineinfo:generated-code*//*@lineinfo:373^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:373^32*/ }catch( Exception ee){}   // commit log entry
      
      if ( getErrorCode() == TridentApiConstants.ER_NONE )
      {
        setErrorCode(TridentApiConstants.ER_INTERNAL_ERROR);
        setErrorDesc("Internal Error - Try Again");
      }
    }
    finally
    {
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/