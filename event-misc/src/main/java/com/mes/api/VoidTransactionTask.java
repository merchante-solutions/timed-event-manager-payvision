/*@lineinfo:filename=VoidTransactionTask*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/api/VoidTransactionTask.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $LastModifiedDate$
  Version            : $Revision: 15771 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import com.payvision.gateway.BasicOperationsLocator;
import com.payvision.gateway.BasicOperationsSoapStub;
import com.payvision.gateway.TransactionResult;

public class VoidTransactionTask extends TridentApiTransactionTask
{
  public VoidTransactionTask( TridentApiTransaction tran )
  {
    super(tran);
  }
  
  public boolean doTask()
  {
    String    cardType    = null;
    boolean   retVal      = false;
    String    tranId      = null;
    long      beginTS     = System.currentTimeMillis();

    try
    {
      connect();
      
      if( Transaction instanceof AuthorizeNetTransaction )
      {
        loadAuthNetData();
      }
      
      cardType = Transaction.getCardType();
      tranId   = Transaction.getTridentTranId();
      
      // check the tran id for this transaction
      if ( !validTranId() )
      {
        setError(TridentApiConstants.ER_INVALID_TRAN_ID);
      }
      else if ( isTransactionVoid() )
      {
        setError(TridentApiConstants.ER_VOID_ALREADY);
      }
      else if ( isPinDebitTransaction() )
      {
        setError(TridentApiConstants.ER_DEBIT_TRAN_TYPE_NOT_ALLOWED);
      }
      else 
      {
        // load data from the original data (oracle)
        // (e.g. currency code, payvision tran id number)
        loadTransactionData();
        
        if ( Transaction.usePayVision() )
        {
          // get a reference to the PayVision web service
          BasicOperationsLocator  sl      =  new BasicOperationsLocator(); 
          sl.setBasicOperationsSoapEndpointAddress( Transaction.getPayVisionEndpoint() + TridentApiConstants.PAYVISION_BASIC_PATH );
          BasicOperationsSoapStub service = (BasicOperationsSoapStub)sl.getBasicOperationsSoap(); 
          System.out.println("payvision void - " + Transaction.getTridentTranId());
          TransactionResult       result  = service._void(Transaction.getPayVisionMemberId(),
                                                          Transaction.getPayVisionMemberGuid(),
                                                          Transaction.getPayVisionTranId(),
                                                          Transaction.getPayVisionTranGuid(),
                                                          Transaction.getApiTranId());
          // save the result of the payvision transaction                                                        
          Transaction.setPayVisionResult(result);          
          Transaction.setSwitchDuration(System.currentTimeMillis() - beginTS);
                                                          
          if ( result.getResult() == TridentApiConstants.PAYVISION_SUCCESS )
          {
            retVal = true;
            voidTransaction();    // mark as void in Oracle
          }
          else
          {
            setError(TridentApiConstants.ER_PV_VOID_FAILED);
            setErrorDesc(Transaction.getPayVisionErrorDesc() +  " (" + Transaction.getPayVisionErrorCode() + ")");
          }  
        }
        else  // just do the void in Oracle
        {
          retVal = voidTransaction();
        }
        
        // if the void failed and the error code has no already
        // been set, assign the generic void failed error code
        if ( retVal != true && getErrorCode().equals(TridentApiConstants.ER_NONE) ) 
        {
          setError(TridentApiConstants.ER_VOID_TRAN_SETTLED);
        }
      }
    }
    catch( Exception e )
    {
      retVal = false;
      setError(TridentApiConstants.ER_VOID_FAILED);
    }
    finally
    {
      cleanUp(); 
    }
    return( retVal );
  }
  
  protected boolean isTransactionVoid( )
  {
    int           recCount    = 0;
    boolean       retVal      = false;
    String        tranId      = null;
    
    try
    {
      connect();
      
      tranId = Transaction.getTridentTranId();
      
      /*@lineinfo:generated-code*//*@lineinfo:137^7*/

//  ************************************************************
//  #sql [Ctx] { select  count( trident_tran_id ) 
//          from    trident_capture_api tapi
//          where   tapi.trident_tran_id = :tranId and
//                  tapi.transaction_type = :TridentApiTransaction.TT_VOID
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( trident_tran_id )  \n        from    trident_capture_api tapi\n        where   tapi.trident_tran_id =  :1  and\n                tapi.transaction_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.api.VoidTransactionTask",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tranId);
   __sJT_st.setString(2,TridentApiTransaction.TT_VOID);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:143^7*/
      retVal = (recCount != 0);
    }
    catch( Exception e )
    {
      logEntry("isTransactionVoid(" + tranId + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/