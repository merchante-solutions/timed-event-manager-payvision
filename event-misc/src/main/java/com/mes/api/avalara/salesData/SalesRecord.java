package com.mes.api.avalara.salesData;

import java.sql.Date;

public class SalesRecord { 
  
  
  int processCode;
  String docCode;
  String docType;
  Date docDate;
  String customerCode;
  String lineNo;
  double amount;
  String merchantName;
  String destRegion;
  String destPostalCode;
  //String destCounty;
  String destCity;
  String origRegion;
  String origPostalCode;
  String itemDescription;
  
  public int getProcessCode() {
    return processCode;
  }
  public void setProcessCode(int processCode) {
    this.processCode = processCode;
  }
  public String getDocCode() {
    return docCode;
  }
  public void setDocCode(String docCode) {
    this.docCode = docCode;
  }
  public String getDocType() {
    return docType;
  }
  public void setDocType(String docType) {
    this.docType = docType;
  }
  public Date getDocDate() {
    return docDate;
  }
  public void setDocDate(Date docDate) {
    this.docDate = docDate;
  }
  public String getCustomerCode() {
    return customerCode;
  }
  public void setCustomerCode(String customerCode) {
    this.customerCode = customerCode;
  }
  public String getLineNo() {
    return lineNo;
  }
  public void setLineNo(String lineNo) {
    this.lineNo = lineNo;
  }
  public double getAmount() {
    return amount;
  }
  public void setAmount(double amount) {
    this.amount = amount;
  }
  public String getDestRegion() {
    return destRegion;
  }
  public void setDestRegion(String destRegion) {
    this.destRegion = destRegion;
  }
  public String getDestPostalCode() {
    return destPostalCode;
  }
  public void setDestPostalCode(String destPostalCode) {
    this.destPostalCode = destPostalCode;
  }
  public String getOrigRegion() {
    return origRegion;
  }
  public void setOrigRegion(String origRegion) {
    this.origRegion = origRegion;
  }
  public String getOrigPostalCode() {
    return origPostalCode;
  }
  public void setOrigPostalCode(String origPostalCode) {
    this.origPostalCode = origPostalCode;
  }
//  public String getDestCounty() {
//    return destCounty;
//  }
//  public void setDestCounty(String destCounty) {
//    this.destCounty = destCounty;
//  }
  public String getDestCity() {
    return destCity;
  }
  public void setDestCity(String destCity) {
    this.destCity = destCity;
  }
  
  public String getMerchantName() {
    return merchantName;
  }
  public void setMerchantName(String merchantName) {
    this.merchantName = merchantName;
  }
  public String getItemDescription() {
    return itemDescription;
  }
  public void setItemDescription(String itemDescription) {
    this.itemDescription = itemDescription;
  }
  
  
}
