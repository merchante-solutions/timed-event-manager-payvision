/*@lineinfo:filename=SalesDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.api.avalara.salesData;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.mes.api.avalara.commonResponse.CommonResponse;
import com.mes.api.avalara.getTax.GetTaxProcess;
import com.mes.api.avalara.getTax.GetTaxRequest;
import com.mes.api.avalara.getTax.GetTaxRequest.DetailLevel;
import com.mes.api.avalara.getTax.GetTaxRequest.DocType;
import com.mes.api.avalara.getTax.GetTaxRequest.Line;
import com.mes.api.avalara.getTax.GetTaxResult;
import com.mes.api.avalara.validateAddress.Address;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.CSVFileMemory;
import sqlj.runtime.ScrollableResultSetIterator;

public class SalesDataBean extends SQLJConnectionBase {

  static Logger log = Logger.getLogger(SalesDataBean.class);
  ArrayList<SalesRecord> salesRows = new ArrayList<SalesRecord>();
  ArrayList<GetTaxRequest> taxRequestCollection = new ArrayList<GetTaxRequest>();
  static String userName = null;
  static String password = null;
  static String baseURL = null;
  static String companyCode = null;
  static String toEmailAddress = null;
  static String fromEmailAddress = null;
  static String ccEmailAddress = null;

  CSVFileMemory csvRentalEquipmentFile = null;
  CSVFileMemory csvMesOwnedEquipmentFile = null;

  int counter = 0;
  int lineNo = 0;
  long nodeId = 9999999999L;
  StringBuffer taxResultMessage = new StringBuffer();

  public void loadData(Date beginDate, Date endDate) {
    try {      
      log.info("Loading Sales Data in Avalara for the period " + beginDate
          + " to " + endDate);
      loadAvalaraProperties();      
      connect();
      // message for status email
      taxResultMessage.append("Uploaded sales data for the period " + beginDate
          + " to " + endDate);
      taxResultMessage.append(System.lineSeparator());
      taxResultMessage.append(System.lineSeparator());
      getSalesDataForEquipmentRental(beginDate, endDate, nodeId);
      getSalesDataForOwnedEquipment(beginDate, endDate, nodeId);      
      translateToGetTaxRequestObjects();
      log.info("Redy for uploading " + taxRequestCollection.size() + " records to Avalara");            
      loadDataInAvalara();      
      log.info("Done!");      
      //printData(); // Uncomment for testing. Comment out loadDataInAvalara()
    } catch (Exception e) {
      logEntry("loadData()", e.toString());
    } finally {
      cleanUp();
    }

  }

  private void createSalesRecords(ResultSet resultSet) throws SQLException {
   
    while (resultSet.next()) {
      SalesRecord sr = new SalesRecord();
      sr.setAmount(resultSet.getDouble("charge_amount"));
      sr.setCustomerCode(resultSet.getString("merchant_number"));
      sr.setMerchantName(resultSet.getString("dba_name"));
      sr.setDestPostalCode(resultSet.getString("zip"));
      sr.setDestRegion(resultSet.getString("state"));
      sr.setDestCity(resultSet.getString("city"));
      sr.setDocCode(getDocCode(resultSet.getDate("order_date")));
      sr.setDocDate(resultSet.getDate("order_date"));
      sr.setItemDescription(resultSet.getString("statement_msg"));
      sr.setDocType("1"); // Always 1 for "Sales Invoice"
      sr.setLineNo(String.valueOf(++lineNo));
      sr.setProcessCode(3); // 3 -Sales Invoice as per Avalara api ref.
      salesRows.add(sr);     
    }    
    logInfo("Sales record collection size = " + salesRows.size());
  }

  private boolean validate(String input, int validLength) {
    boolean validString = true;
    if (StringUtils.isNotBlank(input)) {
      if (input.length() > validLength) {
        validString = false;
      }
    }
    return validString;
  }

  // private Date getLastYearDate(Date date) {
  // Calendar cal = Calendar.getInstance();
  // cal.setTime(date);
  // cal.set(Calendar.YEAR, 2011);
  // return new java.sql.Date(cal.getTime().getTime());
  // }

  private void getSalesDataForOwnedEquipment(Date beginDate, Date endDate, long nodeId) {
    ScrollableResultSetIterator             it                = null;  
    ResultSet                     resultSet         = null;  
   
    try{
   
      /*@lineinfo:generated-code*//*@lineinfo:127^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  so.merch_num                as merchant_number,
//            mf.dba_name                         as dba_name,
//            trunc(so.order_date)                as order_date,
//            app.appsrctype_code                 as app_type,
//            appt.app_description                as app_desc,
//            so.ship_address_state               as state, 
//            tx.county                           as county,
//            so.ship_address_city                as city, 
//            so.ship_address_zip                 as zip,
//            si.description                      as statement_msg,
//            soi.quantity                        as item_count,
//            soi.cost                            as item_amount,
//            (
//              round((soi.cost*soi.quantity), 2) * 
//              decode(so.markup_multiplier, 0, 0, 1)
//            )                                   as charge_amount 
//    from    organization            o,
//            group_merchant          gm,
//            supply_order            so, 
//            supply_order_item       soi,
//            supply_item             si,
//            merchant                mr,
//            application             app,
//            app_type                appt,
//            tmg_client_deploy_rules dr,
//            tmg_inventories         inv,
//            mif                     mf, 
//            salestax_flat           tx
//    where   o.org_group = :nodeId
//            and gm.org_num = o.org_num
//            and so.merch_num = gm.merchant_number         
//            and so.order_date between :beginDate and :endDate
//            and not so.process_sequence is null                     
//            and soi.supply_order_id = so.supply_order_id
//            and si.supply_item_id = soi.supply_item_id
//            and mr.merch_number = so.merch_num
//            and app.app_seq_num = mr.app_seq_num 
//            and appt.app_type_code = app.app_type
//            and dr.app_type = app.app_type
//            and inv.inv_id = dr.deploy_to_inv_id
//            and inv.inv_name = 'MES'
//            and mf.merchant_number = so.merch_num
//            and tx.id = sales_tax_id(substr(so.ship_address_zip,1,5), so.ship_address_city)
//    order by order_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  so.merch_num                as merchant_number,\n          mf.dba_name                         as dba_name,\n          trunc(so.order_date)                as order_date,\n          app.appsrctype_code                 as app_type,\n          appt.app_description                as app_desc,\n          so.ship_address_state               as state, \n          tx.county                           as county,\n          so.ship_address_city                as city, \n          so.ship_address_zip                 as zip,\n          si.description                      as statement_msg,\n          soi.quantity                        as item_count,\n          soi.cost                            as item_amount,\n          (\n            round((soi.cost*soi.quantity), 2) * \n            decode(so.markup_multiplier, 0, 0, 1)\n          )                                   as charge_amount \n  from    organization            o,\n          group_merchant          gm,\n          supply_order            so, \n          supply_order_item       soi,\n          supply_item             si,\n          merchant                mr,\n          application             app,\n          app_type                appt,\n          tmg_client_deploy_rules dr,\n          tmg_inventories         inv,\n          mif                     mf, \n          salestax_flat           tx\n  where   o.org_group =  :1 \n          and gm.org_num = o.org_num\n          and so.merch_num = gm.merchant_number         \n          and so.order_date between  :2  and  :3 \n          and not so.process_sequence is null                     \n          and soi.supply_order_id = so.supply_order_id\n          and si.supply_item_id = soi.supply_item_id\n          and mr.merch_number = so.merch_num\n          and app.app_seq_num = mr.app_seq_num \n          and appt.app_type_code = app.app_type\n          and dr.app_type = app.app_type\n          and inv.inv_id = dr.deploy_to_inv_id\n          and inv.inv_name = 'MES'\n          and mf.merchant_number = so.merch_num\n          and tx.id = sales_tax_id(substr(so.ship_address_zip,1,5), so.ship_address_city)\n  order by order_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.api.avalara.salesData.SalesDataBean",theSqlTS,1004,1007);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ScrollableResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.api.avalara.salesData.SalesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:173^9*/
    resultSet = it.getResultSet();
    
    if(resultSet != null){          
      createSalesRecords(resultSet);
      resultSet.beforeFirst(); 
      csvMesOwnedEquipmentFile = new CSVFileMemory(resultSet, true); // copy to csv file
      resultSet.close();  
    }
    
    }catch(Exception e){
      logEntry("Exception in getSalesDataForOwnedEquipment() : ",e.toString());
    }finally{
      try{
        logInfo("Closing SQL connection.");        
        it.close();       
      }catch(Exception e){
        logEntry("Exception in finally block of getSalesDataForOwnedEquipment() : ", e.toString());
      }
    }
  }
  
  
  private void loadAvalaraProperties() throws Exception{
      //loading avalara properties from mes_defaults table
      userName = MesDefaults.getString(MesDefaults.AVATAX_USERNAME);
      password = MesDefaults.getString(MesDefaults.AVATAX_PASSWORD);
      baseURL = MesDefaults.getString(MesDefaults.AVATAX_BASEURL);
      companyCode = MesDefaults.getString(MesDefaults.AVATAX_COMPANY_CODE);
      fromEmailAddress = MesDefaults.getString(MesDefaults.AVATAX_FROM_EMAIL_ADDRESS);
      toEmailAddress = MesDefaults.getString(MesDefaults.AVATAX_TO_EMAIL_ADDRESS);
      ccEmailAddress = MesDefaults.getString(MesDefaults.AVATAX_CC_EMAIL_ADDRESS);
      log.info("Avalara Properties baseURL = " + baseURL);    
      log.info("Avalara Properties companyCode : " + companyCode);
      log.info("Avalara Properties fromEmailAddress : " + fromEmailAddress);
      log.info("Avalara Properties toEmailAddress : " +toEmailAddress);
      log.info("Avalara Properties ccEmailAddress : " + ccEmailAddress);
  }



  private void loadDataInAvalara() {	  
    try {    	
      for (GetTaxRequest req : taxRequestCollection) {
    	
        GetTaxResult res = GetTaxProcess.GetTax(baseURL, userName, password,
            req);        
        prepareResultNotification(res);    	
      }

      taxResultMessage.append(System.lineSeparator());
      taxResultMessage.append("Total Record count = "
          + taxRequestCollection.size());
      // send email
      log.info("Sending notification email");
      sendNotificationEmail(true, taxResultMessage.toString());
    } catch (Exception e) {
      logEntry("loadDataInAvalara()", e.toString());
      logError("Exception in loadDataInAvalara() method = " + e.getMessage());
      sendNotificationEmail(false, "Upload failed : " + e.getMessage());
      e.printStackTrace();
    }
  }

  private void prepareResultNotification(GetTaxResult res) {
        if (res != null && res.Messages != null && res.Messages.length > 0) {        
          for (int i = 0; i < res.Messages.length; i++) {
        	  log.info("Summary = " + res.Messages[i].Summary);       
        	  if (res.ResultCode.equals(CommonResponse.SeverityLevel.Error)) {
        		  //if error then we send it in email notification
		            taxResultMessage.append("Summary = " + res.Messages[i].Summary);
		            taxResultMessage.append(System.lineSeparator());
        	  }
      }
    }
  }

  private void printData() {
    for (Object o : taxRequestCollection) {
      GetTaxRequest sr = (GetTaxRequest) o;
      System.out.println("Commit = " +sr.Commit + "  Cust Code = " + sr.CustomerCode + "  doc code = " + sr.DocCode
          + " Amount : " + sr.Lines[0].Amount + "  LineNo = "
          + sr.Lines[0].LineNo);
    }

  }

  private void getSalesDataForEquipmentRental(Date beginDate, Date endDate, long nodeId) {
    ScrollableResultSetIterator             it                = null;  
    ResultSet                     resultSet         = null;    
    
    try{
    /*@lineinfo:generated-code*//*@lineinfo:265^5*/

//  ************************************************************
//  #sql [Ctx] it = { select gn.hh_merchant_number as merchant_number,
//          mf.dba_name as dba_name,
//          gn.hh_active_date as order_date,
//          app.appsrctype_code as app_type,
//          appt.app_description as app_desc,
//          mf.dmstate as state,
//          stx.county as county,
//          mf.dmcity as city,
//          mf.dmzip as zip,
//          st.st_statement_desc as statement_msg,
//          st.st_number_of_items_on_stmt as item_count,
//          st.st_amount_of_item_on_stmt as item_amount,
//          st.st_fee_amount as charge_amount
//     from t_hierarchy th,
//          mif mf,
//          monthly_extract_gn gn,
//          monthly_extract_cg cg,
//          monthly_extract_st st,
//          merchant mr,
//          application app,
//          app_type appt,
//          tmg_client_deploy_rules dr,
//          tmg_inventories inv,
//          salestax_flat stx
//    where     th.ancestor = :nodeId
//          and th.descendent = mf.association_node
//          and mf.merchant_number = gn.hh_merchant_number        
//          and gn.hh_active_date between :beginDate and :endDate       
//          and cg.hh_load_sec = gn.hh_load_sec
//          and cg.cg_charge_record_type = 'POS'
//          and not cg.cg_message_for_stmt like '%TAX%'
//          and not cg.cg_message_for_stmt like '%FEE FOR LOANER%'
//          and not cg.cg_message_for_stmt like '%LOANER FEE%'
//          and not cg.cg_message_for_stmt = 'POS PARTNER MAINTENANCE FEE'
//          and not cg.cg_message_for_stmt like '%MONTHLY SERVICE FEE%'
//          and not cg.cg_message_for_stmt like '%TERMINAL INSURANCE%'
//          and not cg.cg_message_for_stmt like
//                     '%MONTHLY FEE-VERISGN PAYFLOW LINK%'
//          and st.hh_load_sec = cg.hh_load_sec
//          and st.st_statement_desc = cg.cg_message_for_stmt
//          and mr.merch_number = gn.hh_merchant_number
//          and app.app_seq_num = mr.app_seq_num
//          and appt.app_type_code = app.app_type
//          and dr.app_type = app.app_type
//          and inv.inv_id = dr.deploy_to_inv_id
//          and inv.inv_name = 'MES'
//          and stx.id = sales_tax_id (substr (mf.dmzip, 1, 5), mf.dmcity)
//  order by gn.hh_active_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select gn.hh_merchant_number as merchant_number,\n        mf.dba_name as dba_name,\n        gn.hh_active_date as order_date,\n        app.appsrctype_code as app_type,\n        appt.app_description as app_desc,\n        mf.dmstate as state,\n        stx.county as county,\n        mf.dmcity as city,\n        mf.dmzip as zip,\n        st.st_statement_desc as statement_msg,\n        st.st_number_of_items_on_stmt as item_count,\n        st.st_amount_of_item_on_stmt as item_amount,\n        st.st_fee_amount as charge_amount\n   from t_hierarchy th,\n        mif mf,\n        monthly_extract_gn gn,\n        monthly_extract_cg cg,\n        monthly_extract_st st,\n        merchant mr,\n        application app,\n        app_type appt,\n        tmg_client_deploy_rules dr,\n        tmg_inventories inv,\n        salestax_flat stx\n  where     th.ancestor =  :1 \n        and th.descendent = mf.association_node\n        and mf.merchant_number = gn.hh_merchant_number        \n        and gn.hh_active_date between  :2  and  :3        \n        and cg.hh_load_sec = gn.hh_load_sec\n        and cg.cg_charge_record_type = 'POS'\n        and not cg.cg_message_for_stmt like '%TAX%'\n        and not cg.cg_message_for_stmt like '%FEE FOR LOANER%'\n        and not cg.cg_message_for_stmt like '%LOANER FEE%'\n        and not cg.cg_message_for_stmt = 'POS PARTNER MAINTENANCE FEE'\n        and not cg.cg_message_for_stmt like '%MONTHLY SERVICE FEE%'\n        and not cg.cg_message_for_stmt like '%TERMINAL INSURANCE%'\n        and not cg.cg_message_for_stmt like\n                   '%MONTHLY FEE-VERISGN PAYFLOW LINK%'\n        and st.hh_load_sec = cg.hh_load_sec\n        and st.st_statement_desc = cg.cg_message_for_stmt\n        and mr.merch_number = gn.hh_merchant_number\n        and app.app_seq_num = mr.app_seq_num\n        and appt.app_type_code = app.app_type\n        and dr.app_type = app.app_type\n        and inv.inv_id = dr.deploy_to_inv_id\n        and inv.inv_name = 'MES'\n        and stx.id = sales_tax_id (substr (mf.dmzip, 1, 5), mf.dmcity)\norder by gn.hh_active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.api.avalara.salesData.SalesDataBean",theSqlTS,1004,1007);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ScrollableResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.api.avalara.salesData.SalesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:315^7*/    
     
    resultSet = it.getResultSet();
 
    if(resultSet != null ){ 
      createSalesRecords(resultSet);
      //rewind the resultset cursor 
      resultSet.beforeFirst();
      csvRentalEquipmentFile = createCSVFile(resultSet, true); // copy to CSV file
      resultSet.close();  
    }
        
    }catch(Exception e){
      logEntry("Exception in getSalesDataForEquipment() : ",e.toString());
    }finally{
      try{
        logInfo("Closing SQL connection.");        
        it.close();        
      }catch(Exception e){
        logEntry("Exception in finally block of getSalesDataForEquipment() : ", e.toString());
      }
    }
  }
  
  public CSVFileMemory createCSVFile(ResultSet anotherSet, boolean withHeader){
    CSVFileMemory file = new CSVFileMemory(anotherSet, withHeader);
    return file;
  }

  private String getDocCode(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    SimpleDateFormat dtformat = new SimpleDateFormat("yyyyMMdd");
    String docDate = dtformat.format(date);
    StringBuilder docCode = new StringBuilder();
    docCode.append(docDate).append(String.format("%05d", ++counter));
    return docCode.toString();
  }

  // translates SalesRecord to GetTaxRequest object
  /***
   * Length limits provided by Avalara consultant through email. They don't have
   * this documented on API site. DocCode: 50 CustomerCode: 50 ItemCode: 50
   * 
   * Address Line1: 50 Line2: 50 Line3: 50 City: 50 Region: 3 PostalCode: 11
   * Country: 50, but should use ISO 3166-1 alpha-2
   * 
   */
  public void translateToGetTaxRequestObjects() {
    if (salesRows.size() > 0) {
      for (SalesRecord sr : salesRows) {
        GetTaxRequest getTaxRequest = new GetTaxRequest();

        getTaxRequest.CompanyCode = companyCode; // R: Company Code from the
                                                 // accounts Admin Console
        String docCode = sr.getDocCode();
        if (!validate(docCode, 50)) {
          docCode = docCode.substring(0, 50);
        }
        getTaxRequest.DocCode = docCode; // R: Invoice or document tracking
                                         // number - Must be unique
        getTaxRequest.DocType = DocType.SalesInvoice; // R: Typically
                                                      // SalesOrder,SalesInvoice,
                                                      // ReturnInvoice
        getTaxRequest.DocDate = sr.getDocDate(); // R: sets reporting date and
                                                 // default tax date

        String customerCode = sr.getCustomerCode();
        if (!validate(customerCode, 50)) {
          customerCode = customerCode.substring(0, 50);
        }
        getTaxRequest.CustomerCode = customerCode; // R: String - Customer
                                                   // Tracking number or
                                                   // Exemption Customer Code
        getTaxRequest.DetailLevel = DetailLevel.Tax; // R: Chose Summary,
                                                     // Document, Line or Tax -
                                                     // varying levels of
                                                     // results detail
        //temp changed to false to verify how april upload works. change it back to true once confirmed.
        getTaxRequest.Commit = true; // O: Default is "false" - Set to "true"  
                                      // to commit the Document
                            

        getTaxRequest.Addresses = getAddresses(sr);
        getTaxRequest.Lines = getLineItem(sr); // Sets array of lines
        taxRequestCollection.add(getTaxRequest);
      }
    }
  }

  private Line[] getLineItem(SalesRecord sr) {

    // Add invoice lines
    Line line1 = new Line(); // New instance of a line
    line1.LineNo = sr.getLineNo();
    String itemCode = sr.getItemDescription(); // R: string - line Number of
                                               // invoice - must be unique.
    if (!validate(sr.getItemDescription(), 50)) {
      itemCode = itemCode.substring(0, 50);
    }

    line1.ItemCode = itemCode; // R: string - SKU or short name of Item
    line1.Qty = new BigDecimal(1); // R: decimal - The number of items -- Qty of
                                   // product sold. Does not function as a
                                   // mulitplier for Amount
                                   // see
                                   // http://docs.oracle.com/javase/6/docs/api/java/math/BigDecimal.html
                                   // regarding
                                   // the use of BigDecimal. BigDecimal class
                                   // provides operations for arithmetic, scale
                                   // manipulation, rounding, comparison,
                                   // hashing, and format conversion
    line1.Amount = new BigDecimal(sr.getAmount()); // R: decimal - the "NET"
                                                   // amount -- Amount should be
                                                   // the 'extended' or 'net'
                                                   // amount

    line1.Description = sr.getItemDescription();
    line1.OriginCode = "Origin"; // R: Value representing the Origin Address
    line1.DestinationCode = "Dest"; // R: Value representing the Destination
                                    // Address

    Line[] lines = { line1 };
    return lines;
  }

  private Address[] getAddresses(SalesRecord sr) {
    // address
    Address origin = getOriginalAddress();
    Address destination = new Address();
    destination.AddressCode = "Dest";
    String line1 = sr.getMerchantName();
    if (!validate(line1, 50)) {
      line1 = line1.substring(0, 50);
    }
    destination.Line1 = line1;

    String city = sr.getDestCity();
    if (!validate(city, 50)) {
      city = city.substring(0, 50);
    }
    destination.City = city;

    String region = sr.getDestRegion();
    if (!validate(region, 3)) {
      region = region.substring(0, 3);
    }
    destination.Region = region;

    String postalCode = sr.getDestPostalCode();
    if (!validate(postalCode, 11)) {
      postalCode = postalCode.substring(0, 11);
    }
    destination.PostalCode = postalCode;
    destination.Country = "USA";
    Address[] addresses = { origin, destination };
    return addresses;
  }

  Address getOriginalAddress() {
    Address origin = new Address();
    origin.AddressCode = "Origin";
    origin.Line1 = "MES";
    origin.City = "Spokane";
    origin.Region = "WA";
    origin.PostalCode = "99213-3305";
    origin.Country = "USA";
    return origin;
  }

  private void sendNotificationEmail(boolean success, String message) {
	log.info("Sending email notification from " + fromEmailAddress + " to " + toEmailAddress);
    StringBuffer body = new StringBuffer("");
    StringBuffer subject = new StringBuffer("");

    try {
      if (success) {
        subject.append("Avalara Upload Success");
      } else {
        subject.append("Avalara Upload Error");
      }

      body.append(message);

      MailMessage msg = new MailMessage();
      msg.setFrom(fromEmailAddress);
      msg.addTo(toEmailAddress);
      msg.addCC(ccEmailAddress);

      if (csvRentalEquipmentFile != null) {
        msg.addFile("RentalEquipmentRecords.csv", csvRentalEquipmentFile
            .toString().getBytes());
      }
      if (csvMesOwnedEquipmentFile != null) {
        msg.addFile("MesOwnedEquimentRecords.csv", csvMesOwnedEquipmentFile
            .toString().getBytes());
      }
      msg.setSubject(subject.toString());
      msg.setText(body.toString());

      msg.send();
    } catch (Exception e) {    	
      logEntry("sendNotificationEmail(): ", e.getMessage());

    }
  }

  public static void main(String args[]) {

      System.out.println("IN main method");
//    System.out.println("In main method()");
//    // SalesDataBean sb = new SalesDataBean();
//    Calendar cal = Calendar.getInstance();
//
//    // temporary
//    //cal.set(2014, 02, 01);
//    cal.add(Calendar.MONTH, -1);
//    cal.set(Calendar.DAY_OF_MONTH, 1);
//    
//    Date activeDate = new java.sql.Date(cal.getTime().getTime());
//    cal.setTime(activeDate);
//    System.out.println("activeDate = " + cal.getTime());
//
//    cal.set(Calendar.YEAR, 2014);
//    cal.add(Calendar.MONTH, 1);
//    cal.set(Calendar.DAY_OF_MONTH, 1);
//    cal.add(Calendar.DAY_OF_MONTH, -1);
//    Date monthEndDate = new java.sql.Date(cal.getTime().getTime());
//    // end temporary
//    System.out.println("monthEndDate = " + cal.getTime());

    SalesDataBean sb = new SalesDataBean();
   // sb.loadData(activeDate, monthEndDate);
   // sb.loadAvalaraProperties();
    sb.sendNotificationEmail(true, "This is test message");

  }

}/*@lineinfo:generated-code*/