/*************************************************************************

  FILE: $Archive: $

  Description:  
    Connection support for MS SQL Server database


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000-2004,2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Vector;

public class SimpleConnection
{
  protected int             ConnectCount            = 0;
  protected String          ConnectionString        = null;
  protected Connection      Con                     = null;
  protected String          DriverName              = null;
  protected boolean         Initialized             = false;
  protected Vector          Statements              = new Vector();
  
  public SimpleConnection( String driverName, String connectString )
  {
    ConnectionString  = connectString;
    DriverName        = driverName;
    initialize();
  }
  
  public boolean connect( )
  {
    boolean       retVal      = false;
    
    try
    {
      ++ConnectCount;
    
      if ( isStale() )
      {
        Con = DriverManager.getConnection( ConnectionString );
      }        
    }      
    catch( Exception e )
    {
      System.out.println("connection() - " + e.toString());
    }
    
    return( retVal );
  }
  
  public void close( )
  {
    try
    { 
      if( --ConnectCount <= 0 )
      {
        // clean up any left over statements
        for( int i = 0; i < Statements.size(); ++i )
        {
          try
          {
            ((PreparedStatement)Statements.elementAt(i)).close();
          }
          catch(Exception ee)
          {
          }
        }
        Statements.removeAllElements();
     
        try
        {
          // close the database connection
          Con.close();
        }
        catch(Exception closeException)
        {
        }
        
        ConnectCount = 0;
      }
    } 
    catch(Exception e)
    {
      System.out.println("close() - " + e.toString());
    }
  }
  
  public PreparedStatement getPreparedStatement( String sql )
  {
    PreparedStatement     retVal    = null;
    
    try
    {
      if ( !isStale() )
      {
        retVal = Con.prepareStatement(sql);
        Statements.add(retVal);
      }
    }
    catch(Exception e)
    {
      System.out.println("getPreparedStatement() - " + e.toString());
    }
    
    return( retVal );
  }
  
  protected void initialize( )
  {
    try 
    { 
      Class.forName(DriverName).newInstance();
    } 
    catch (Exception e) 
    { 
      System.out.println("initialize() - " + e.toString());
    }
    Initialized = true;
  }
  
  public boolean isStale( )
  {
    boolean     retVal      = true;
    
    try
    {
      if ( Con != null )
      {
        retVal = Con.isClosed();
      }        
    }
    catch( Exception e )
    {
      System.out.println("isStale() - " + e.toString());
    }
    return( retVal );
  }
  
  public static void main( String[] args )
  {
    SimpleConnection  con = null;
    ResultSet         rs    = null;
    PreparedStatement s     = null;
    ResultSetMetaData md    = null;
    StringBuffer      line  = null;
    
    try
    {
      con = new SimpleConnection(args[0],args[1]);
      con.connect();
      
      
      //@ apmaster.addr_sort_1
      s = con.getPreparedStatement(args[2]);
      System.out.println("selecting");
      s.executeQuery();
      rs = s.getResultSet();
      
      md = rs.getMetaData();
      
      while( rs.next() )
      {
        if ( line == null )
        {
          line = new StringBuffer();
          for (int col = 1; col <= md.getColumnCount(); col++) 
          {
            if ( col > 1 )
            {
              line.append(",");
            }
            line.append("\"");
            line.append( md.getColumnName( col ) );
            line.append("\"");
          }
          System.out.println(line.toString());
        }
        
        line.setLength(0);
        for (int col = 1; col <= md.getColumnCount(); col++) 
        {
          if ( col > 1 )
          {
            line.append(",");
          }
          line.append("\"");
          line.append( rs.getString(md.getColumnName(col)) );
          line.append("\"");
        }
        System.out.println(line.toString());
      }
      rs.close();
      s.close();
    }
    catch(Exception e)
    {
      System.out.println(e.toString());
    }
    finally
    {
      con.close();
    }
  }
}