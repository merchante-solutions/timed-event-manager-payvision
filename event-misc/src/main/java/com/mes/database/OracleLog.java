/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/database/OracleLog.java $

  Description:  
    Utility class for writing log entries to a log file


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/19/02 5:22p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

 **************************************************************************/
package com.mes.database;

import java.sql.CallableStatement;
import java.sql.Connection;

public class OracleLog {
	// constants for error logging
	private final static int SZ_SOURCE = 80;
	private final static int SZ_MESSAGE = 1024;

	// constants for connection logging
	private final static int SZ_CALLER = 80;
	private final static int SZ_CON_MESSAGE = 40;

	// constants for access logging
	private final static int SZ_ID = 30;
	private final static int SZ_LOCATION = 30;
	private final static int SZ_RESOURCE = 1024;
	private final static int SZ_RESULT = 30;

	/*
	 * * METHOD LogEntry** Writes an entry to the log file
	 */
	public static void LogEntry(int Error, String Source, String Message) {
		Connection con = null;
		OracleConnectionPool.PoolConnection poolCon = null;
		CallableStatement st = null;

		try {
			poolCon = OracleConnectionPool.getInstance().getFreeConnection(null);
			con = poolCon.getConnection();
			con.setAutoCommit(true);

			if (con != null) {
				st = con.prepareCall("{ call log_entry(?, ?, ?) }");
				st.setInt(1, Error);
				st.setString(2, (Source.length() > SZ_SOURCE ? Source.substring(0, SZ_SOURCE) : Source));
				st.setString(3, (Message.length() > SZ_MESSAGE ? Message.substring(0, SZ_MESSAGE) : Message));
				st.execute();
			}
		}
		catch (Exception e) {
			// what to do here? exception logging an exception...
			System.out.println("LogEntry: " + e.toString());
		}
		finally {
			try {
				st.close();
			}
			catch (Exception e) {}
			poolCon.release(OracleConnectionPool.getInstance());
		}
	}

	public static void LogEntry(String Source, String Message) {
		LogEntry(0, Source, Message);
	}

	public static void LogEntry(String Message) {
		LogEntry(0, "No Source", Message);
	}

	public static void LogEntry(int Error, String Message) {
		LogEntry(Error, "No Source", Message);
	}
}
