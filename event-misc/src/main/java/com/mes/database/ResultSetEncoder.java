/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/database/ResultSetEncoder.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/27/01 1:17p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.database;

import java.io.PrintStream;
// math imports
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
// SQL imports
import java.sql.Types;
import java.text.NumberFormat;
// Std Imports
import java.text.SimpleDateFormat;
// servlet imports
import javax.servlet.ServletOutputStream;

//C+            ResultSetEncoder
public class    ResultSetEncoder
{
  String                    LastErrorMessage  = "";
  ResultSetMetaData         MetaData          = null;
  ServletOutputStream       ServletOut        = null;
  PrintStream               StdOut            = null;
  
  public ResultSetEncoder( ServletOutputStream out )
  {
    ServletOut = out;
  }
  
  public ResultSetEncoder( PrintStream out )
  {
    StdOut = out;
  }
  
  private boolean isServlet( )
  {
    return( ( ServletOut != null ) );
  }
  
  protected String getColumnDataAsString( ResultSet resultSet, int columnNumber, int dataFormat )
    throws SQLException
  {
    BigDecimal            bigDecimalValue;
    SimpleDateFormat      dateFormat    = new SimpleDateFormat("MM/dd/yyyy");
    int                   decimalCount;
    StringBuffer          valueString   = new StringBuffer();
    NumberFormat          numberFormat  = NumberFormat.getInstance();
    
    try
    {
      switch ( dataFormat )
      {
          // SQL Type             // Java Type
        case Types.CHAR:          // String - fixed length string
        case Types.VARCHAR:       // String - variable length string
        case Types.LONGVARCHAR:   // String - variable length string - large (BLOB data)
          valueString.append( "\"" );
          valueString.append( resultSet.getString( columnNumber ) );
          valueString.append( "\"" );
          break;
    
        case Types.NUMERIC:       // java.math.BigDecimal - numeric data of variable precision
        case Types.DECIMAL:       // java.math.BigDecimal - numeric data of variable precision and scale
          bigDecimalValue = resultSet.getBigDecimal( columnNumber );
        
          if ( bigDecimalValue != null )
          {
            decimalCount = MetaData.getScale( columnNumber );
            if ( ( decimalCount == 0 ) && ( MetaData.getColumnName( columnNumber ).indexOf("AMOUNT") >= 0 ) )
            {
              //
              // SUM( <amount_field> ) will not have the correct
              // scale in the meta data.  If this appears to be an
              // amount field, then make sure we encode using two
              // decimal places.
              //
              decimalCount = 2;
            }          
            numberFormat.setMaximumFractionDigits( decimalCount );
            numberFormat.setMinimumFractionDigits( decimalCount );
            numberFormat.setGroupingUsed( false );
            valueString.append( numberFormat.format( bigDecimalValue ) );
          }          
          break;
    
        case Types.BIT:           // boolean
          valueString.append( resultSet.getBoolean( columnNumber ) );
          break;
    
        case Types.TINYINT:       // byte   (  8-bit signed )
          valueString.append( resultSet.getByte( columnNumber ) );
          break;
    
        case Types.SMALLINT:      // short  ( 16-bit signed )
          valueString.append( resultSet.getShort( columnNumber ) );
          break;
    
        case Types.INTEGER:       // int    ( 32-bit signed )
          valueString.append( resultSet.getInt( columnNumber ) );
          break;
    
        case Types.BIGINT:        // long   ( 32-bit signed )
          valueString.append( resultSet.getLong( columnNumber ) );
          break;
    
        case Types.REAL:          // float  ( 32-bit floating point )
          valueString.append( resultSet.getFloat( columnNumber ) );
          break;
    
        case Types.FLOAT:         // double ( 64-bit floating point )
        case Types.DOUBLE:        // ""
          valueString.append( resultSet.getDouble( columnNumber ) );
          break;
    
        case Types.DATE:          // java.sql.Date - date data
          valueString.append( dateFormat.format( resultSet.getDate( columnNumber ) ) );
          break;
    
        case Types.TIME:          // java.sql.Time - time data
          valueString.append( resultSet.getTime( columnNumber ).toString() );
          break;
    
        case Types.TIMESTAMP:     // java.sql.Timestamp - date and time data
          // treat time stamp fields like dates
          valueString.append( dateFormat.format( resultSet.getDate( columnNumber ) ) );
          break;
    
  //@            case Types.BINARY:        // byte[] - fixed length binary
  //@            case Types.VARBINARY:     // byte[] - variable length binary
  //@            case Types.LONGVARBINARY: // byte[] - variable length binary
        default:
          break;
      }
    }
    catch (NullPointerException e)
    {
      valueString.setLength(0);
    }
    return( valueString.toString() );
  }
  
  public String getLastErrorMessage( )
  {
    return( LastErrorMessage );
  }
  
  public void encodeCSV( ResultSet resultSet )
  {
    StringBuffer          line          = new StringBuffer("");
    String                valueString   = null;
    
    try
    {
      MetaData = resultSet.getMetaData();
      
      if ( resultSet.next() )
      {
        for (int col = 1; col <= MetaData.getColumnCount(); col++) 
        {
          if ( line.toString().equals("") )
          {
            line.append( "\"" );
            line.append( MetaData.getColumnName( col ) );
            line.append( "\"" );
          }
          else
          {
            line.append( ",\"" );
            line.append( MetaData.getColumnName( col ) );
            line.append( "\"" );
          }
        }
      
        writeLine( line.toString() );
    
        do 
        {
          line.delete( 0, line.length() );
          line.append("");
        
          for (int col = 1; col <= MetaData.getColumnCount(); col++) 
          {
          
            valueString = getColumnDataAsString( resultSet, col, MetaData.getColumnType( col ) );
            if ( line.toString().equals("") )
            {
              line.append( valueString );
            }
            else
            {
              line.append( "," );
              line.append( valueString );
            }
          }
        
          // output the line
          writeLine( line.toString() ) ;
        
        } while( resultSet.next() );
      }
    }
    catch( java.sql.SQLException e )
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::encodeCSV()" + e.toString());
      LastErrorMessage = e.toString();
    }
  }
  
  protected void writeLine( String line )
  {
    try 
    {
      if ( isServlet() )
      {
        ServletOut.print( line );
        ServletOut.print("\n" );
      }
      else if ( StdOut != null )
      {
        StdOut.println( line );
      }
    }
    catch( java.io.IOException e )
    {
    }      
  }
  
  static void main( String[] args )
  {
  }
}
//C-            ResultSetEncoder