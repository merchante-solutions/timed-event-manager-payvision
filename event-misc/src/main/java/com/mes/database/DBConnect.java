/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/database/DBConnect.java $

  Description:  
    Utility class for getting a database connection from an established 
    connection pool.


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.database;

import java.io.Serializable;
import java.sql.Connection;

public class DBConnect 
  implements Serializable
{
  private String        caller                = "";
  private String        method                = "";
  private Object        Owner                 = null;
  private long          connectionSequence    = 0;
  
  // instance class fields
  String                                defaultConnectString  = "";
  Connection                            DbConnection          = null;
  OracleConnectionPool.PoolConnection   PoolCon               = null;
  String                                sErrorMessage         = "";
  
  /*
  ** CONSTRUCTOR DBConnect
  **
  */
  public DBConnect(Object caller, String method) 
  {
    Owner = caller;
    if(caller == null)
    {
      this.caller = "null";
    }
    else
    {
      this.caller = caller.getClass().getName();
    }
    
    if(method == null)
    {
      this.method = "null";
    }
    else
    {
      this.method = method;
    }
  }

  /*
  ** CONSTRUCTOR DBConnect
  **
  ** Second version without tracking data
  */
//  public DBConnect()
//  {
//    this.caller = "unknown";
//    this.method = "unknown";
//  }
  
  /*
  ** METHOD getErrorMessage
  **
  ** Returns the current error message
  */
  public String getErrorMessage() 
  {
    return( sErrorMessage );
  }
  
  /*
  ** METHOD getConnection
  **
  ** Returns a connection from the connection pool.
  **
  ** PARAMS:
  **
  **    connectString : Formatted connection string.  
  **
  **    direct;<jdbc url>;<driver class name>;..[name=value] 
  **              OR
  **    pool;<pool name>
  **
  **    <connection type>   := pool OR direct
  **    <driver class name> := i.e. oracle.jdbc.driver.OracleDriver
  **    [name=value]        := database properties (up to 7)
  */
  public java.sql.Connection getConnection(String connectString) 
  {
    try
    {
      if ( isConnectionStale() )
      {
        // return to closed connection to
        // the pool.  the pool will handle
        // reopening the connection when the
        // next thread requests this connection.
        releaseConnection();      
        
        // get a new connection from the connection pool
        PoolCon       = OracleConnectionPool.getInstance().getFreeConnection(((Owner == null) ? this : Owner));
        DbConnection  = PoolCon.getConnection();
      }        
    }
    catch(Exception e)
    {
    }
    return(DbConnection);
  }
  
  public Connection getConnection()
  {
    Connection      con   = null;
    try
    {
      con = getConnection(defaultConnectString);
    }
    catch(Exception e)
    {
      sErrorMessage = e.toString();
    }
    
    return(con);
  }
  
  public boolean isConnectionStale( )
  {
    boolean       retVal = true;
    
    try
    {
      retVal = DbConnection.isClosed();
    }
    catch( NullPointerException e )
    {
    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  /*
  ** METHOD releaseConnection
  **
  ** If this object has a connection, returns the connection.
  */
  public void releaseConnection()
  {
    try
    {
      PoolCon.release(OracleConnectionPool.getInstance());
      PoolCon = null;
    }
    catch( NullPointerException e )
    {
    }
  }
}
