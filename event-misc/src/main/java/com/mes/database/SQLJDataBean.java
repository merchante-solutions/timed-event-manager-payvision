/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/database/SQLJDataBean.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:37p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;

public class SQLJDataBean extends SQLJConnectionBase
{
  private boolean               submit            = false;
  private Vector                errors            = new Vector();
  
  public SQLJDataBean()
  {
  }
  
  /*
  ** FUNCTION getErrors
  */
  public Vector getErrors()
  {
    return this.errors;
  }
  
  /*
  ** FUNCTION addError
  */
  protected void addError(String error)
  {
    try
    {
      errors.add(error);
    }
    catch(Exception e)
    {
      logEntry("addError()", e.toString());
    }
  }
  
  /*
  ** FUNCTION hasErrors
  */
  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }
  
  /*
  ** FUNCTION recordExists
  **
  ** tests to see if at least one record exists in the specified table
  ** with the specified primary key
  */
  public boolean recordExists(String  tableName,
                              String  primaryKey,
                              long    primaryKeyVal)
  {
    PreparedStatement     ps              = null;
    ResultSet             rs              = null;
    StringBuffer          qs              = new StringBuffer("");
    boolean               wasConnected    = true;
    
    boolean result = false;
    
    try
    {
      if(isConnectionStale())
      {
        // connect to the database
        connect();
        wasConnected = false;
      }
      
      qs.append("select count(");
      qs.append(primaryKey);
      qs.append(") as count ");
      qs.append("from ");
      qs.append(tableName);
      qs.append(" where ");
      qs.append(primaryKey);
      qs.append(" = ");
      qs.append(primaryKeyVal);
      
//      System.out.println(qs.toString());
      ps = Ctx.getConnection().prepareStatement(qs.toString());
      
      rs = ps.executeQuery();
      
      int count = 0;
      if(rs.next())
      {
        count = rs.getInt("count");
      }
      
      result = (count > 0);
    }
    catch(Exception e)
    {
      addError(e.toString());
      logEntry("recordExists()", e.toString());
    }
    finally
    {
      if(! wasConnected)
      {
        cleanUp();
      }
    }
    
    return result;
  }
  
  /***************************************************************************
  * Overridable methods
  ***************************************************************************/
  /*
  ** FUNCTION getData
  */
  public void getData(long appSeqNum)
  {
  }
  
  /*
  ** FUNCTION submitData
  */
  public void submitData(HttpServletRequest req, long appSeqNum)
  {
  }
  
  /*
  ** FUNCTION validate
  */
  public boolean validate(HttpServletRequest req)
  {
    return(true);
  }
  
  /*
  ** FUNCTION updateData
  */
  public void updateData(HttpServletRequest req)
  {
  }
  
  /*
  ** FUNCTION isRecalculated
  */
  public boolean isRecalculated()
  {
    return false;
  }

  /*
  ** FUNCTION isCancelled
  */
  public boolean isCancelled()
  {
    return false;
  }
  
  /*
  ** FUNCTION setDefaults
  */
  public void setDefaults(String appSeqNum)
  {
    try
    {
      setDefaults(Long.parseLong(appSeqNum));
    }
    catch(Exception e)
    {
    }
  }
  public void setDefaults(long appSeqNum)
  {
  }
  
  /*
  ** FUNCTION validate
  */
  public boolean validate()
  {
    return(true);
  }
  
  /*
  ** FUNCTION validate
  */
  public boolean validate(long primaryKey)
  {
    return(true);
  }
  
  /*
  ** FUNCTION displayData
  */
  public String displayData()
  {
    return "displayData() undefined";
  }
  
  /*
  ** FUNCTION canContinue
  */
  public boolean canContinue()
  {
    return true;
  }
  
  /***************************************************************************
  * Helper functions
  ***************************************************************************/
  public static boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }
  
  public boolean isNumber(String test)
  {
    boolean pass = false;
    
    try
    {
      long i = Long.parseLong(test);
      pass = true;
    }
    catch(Exception e)
    {
    }
    
    return pass;
  }
  
  public boolean isEmail(String test)
  {
    boolean pass = false;
    
    int firstAt = test.indexOf('@');
    if (!isBlank(test) && firstAt > 0 && 
        test.indexOf('@',firstAt + 1) == -1)
    {
      pass = true;
    }
    
    return pass;
  }
  
  public long getDigits(String raw)
  {
    long          result      = 0L;
    StringBuffer  digits      = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }
      
      result = parseLong(digits.toString());
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDigits: " + e.toString());
    }
    
    return result;
  }
  
  public long getDigitsNoDecimal(String raw)
  {
    long          result        = 0L;
    StringBuffer  digits        = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
        else if(raw.charAt(i) == '.')
        {
          break;
        }
      }
      
      result = parseLong(digits.toString());
    }
    catch(Exception e)
    {
      logEntry("getDigitsNoDecimal()", e.toString());
    }
    
    return result;
  }
  
  public double getDigitsWithDecimal(String raw)
  {
    double        result        = 0.0;
    StringBuffer  digits        = new StringBuffer("");
    
    try
    {
      for(int i=0; i < raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)) || raw.charAt(i) == '.')
        {
          digits.append(raw.charAt(i));
        }
      }
      
      result = parseDouble(digits.toString());
    }
    catch(Exception e)
    {
      logEntry("getDigitsWithDecimal()", e.toString());
    }
    
    return result;
  }
  
  public int countDigits(String raw)
  {
    int result = 0;
    
    for(int i=0; i<raw.length(); ++i)
    {
      if(Character.isDigit(raw.charAt(i)))
      {
        ++result;
      }
    }
    
    return result;
  }
  
  /*
  ** METHOD isValidPhone
  */
  public boolean isValidPhone(String phone)
  {
    // check for valid number of digits
    if (countDigits(phone) != 10)
    {
      return false;
    }
    
    // scan for invalid chars
    for (int i = 0; i < phone.length(); ++i)
    {
      if (!Character.isDigit(phone.charAt(i)) &&
          phone.charAt(i) != '(' &&
          phone.charAt(i) != ')' &&
          phone.charAt(i) != ' ' &&
          phone.charAt(i) != '-')
      {
        return false;
      }
    }
      
    // make sure translated value is valid
    if (getDigits(phone) < 1000000000L)
    {
      return false;
    }
    
    return true;
  }
  
  /*
  ** METHOD is isValidTaxId
  */
  public boolean isValidTaxId(String taxId)
  {
    // check for valid number of digits
    if (countDigits(taxId) != 9)
    {
      return false;
    }
    
    // scan for invalid chars
    for (int i = 0; i < taxId.length(); ++i)
    {
      if (!Character.isDigit(taxId.charAt(i)) &&
          taxId.charAt(i) != ' ' &&
          taxId.charAt(i) != '-')
      {
        return false;
      }
    }
      
    return true;
  }
  
  /*
  ** METHOD parseLong
  */
  public long parseLong(String val)
  {
    long rv;
    try
    {
      rv = Long.parseLong(val);
    }
    catch (Exception e)
    {
      rv = 0;
    }
    return rv;
  }

  /*
  ** METHOD parseDouble
  */
  public double parseDouble(String val)
  {
    double rv;
    try
    {
      rv = Double.parseDouble(val);
    }
    catch (Exception e)
    {
      rv = 0.0;
    }
    return rv;
  }
  
  /*
  ** METHOD parseInt
  */
  public int parseInt(String val)
  {
    int rv;
    try
    {
      rv = Integer.parseInt(val);
    }
    catch (Exception e)
    {
      rv = 0;
    }
    return rv;
  }
  
  public boolean isSubmitted()
  {
    return this.submit;
  }
  
  /***************************************************************************
  * Getters and Setters
  ***************************************************************************/
  public void setSubmit(String submit)
  {
    this.submit = true;
  }
  public boolean getSubmit()
  {
    return this.submit;
  }
}

