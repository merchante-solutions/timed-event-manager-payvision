/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/database/Requester.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/21/02 9:45a $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.database;

import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

public class Requester
  implements Comparable
{
  private static HashMap  requestors          = null;
  private static int      globalRequestCount  = 0;
                          
  public        String    name                = "";
  public        int       requestCount        = 0;
  
  
  public Requester()
  {
  }
  
  public Requester(String name)
  {
    this.name = name;
  }
  
  public int compareTo(Object o)
  {
    Requester r       = (Requester)o;
    int       retVal  = 0;
    
    // we want to sort in reverse order (largest first)
    if(r.requestCount == requestCount)
    {
      retVal = r.name.compareTo(name);
    }
    else
    {
      retVal = r.requestCount - requestCount;
    }
    
    return retVal;
  }
  
  public static synchronized void updateRequester(Object req)
  {
    String  reqName   = "";
    try
    {
      ++globalRequestCount;
      
      // establish the requestor name
      if(req == null)
      {
        reqName = "Null Requester";
      }
      else
      {
        reqName = req.getClass().getName();
      }
      
      // make sure the HashMap is instantiated
      if(requestors == null)
      {
        requestors = new HashMap();
      }
      
      // get this requestor object from the map
      Requester r = (Requester)requestors.get(reqName);
      
      if(r == null)
      {
        r = new Requester(reqName);
        requestors.put(reqName, r);
      }
      
      // increment the count for r
      ++r.requestCount;
    }
    catch(Exception e)
    {
      System.out.println("Exception in Requester: " + e.toString());
      // duh
    }
  }
  
  public static Iterator getRequesters()
  {
    Iterator result = null;
    if(requestors != null)
    {
      TreeSet     sortedSet   = new TreeSet(requestors.values());
      result = sortedSet.iterator();
    }
    
    return result;
  }
  
  public static synchronized int getGlobalRequestCount()
  {
    return globalRequestCount;
  }
}
