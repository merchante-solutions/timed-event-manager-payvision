/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/database/SleepyCatDatabase.java $

  Description:  

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.database;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.collections.StoredMap;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

public class SleepyCatDatabase 
{
  // Needed for efficient object serialization
  private StoredClassCatalog    ClassCatalog      = null;
  private Database              ClassCatalogDb    = null;
  private DatabaseConfig        DbConfig          = new DatabaseConfig();
  private Environment           DbEnv             = null;
  private String                DbHome            = null;
  private Vector                OpenTables        = new Vector();
  private HashMap               StoredMapList     = new HashMap();
  
  private static HashMap        Databases         = new HashMap();
  
  private SleepyCatDatabase( String dbPath )
    throws DatabaseException
  {
    DbHome = dbPath;
    
    // an env is a little database on disk
    File envHome = new File(DbHome);
    envHome.mkdirs();

    setupEnv( envHome );
  }

  // don't call this - more code is needed to 'close' all the stored-maps
  public synchronized void close() 
    throws DatabaseException 
  {
    // close all the open table handles
    while( OpenTables.size() > 0 )
    {
      try
      {
        Database db = (Database)OpenTables.elementAt(0);
        OpenTables.removeElementAt(0);
        db.close();
      }
      catch(Exception e)
      {
        System.out.println(e.toString());
      }
    }
    // close the class catalog
    try{ ClassCatalogDb.close(); } catch(Exception e){}
    
    // empty the stored map list
    StoredMapList.clear();
    
    // close the environment, remove from vector
    DbEnv.close();
    Databases.remove(DbHome);
  }

  protected Database createDatabase( String name ) 
    throws DatabaseException 
  {
    Database db = DbEnv.openDatabase(null, name, DbConfig);
    OpenTables.add(db);   // store to close later
    return( db );
  }
  
  protected StoredClassCatalog getClassCatalog() 
  { 
    return( ClassCatalog ); 
  }
  
  public static synchronized SleepyCatDatabase getInstance(String dbPath)
    throws DatabaseException
  {
    SleepyCatDatabase   retVal    = null;
    
    if ( (retVal = (SleepyCatDatabase)Databases.get(dbPath)) == null )
    {
      retVal = new SleepyCatDatabase(dbPath);
      Databases.put(dbPath,retVal);
    }
    return( retVal );
  }
  
  public synchronized Map getStoredMap(String tableName) 
    throws DatabaseException 
  {
    StoredMap       map     = null;
    
    if ( (map = (StoredMap)StoredMapList.get(tableName)) == null )
    {
      // create binding objects
      EntryBinding keyBinding  = new SerialBinding(getClassCatalog(), Object.class);
      EntryBinding dataBinding = new SerialBinding(getClassCatalog(), Object.class);

      // create database
      Database db = createDatabase(tableName);
      map = new StoredMap(db, keyBinding, dataBinding, true);
      StoredMapList.put(tableName,map);
    }
    return( map );
  }

  protected void setupEnv( File envHome )  
    throws DatabaseException 
  {
    EnvironmentConfig myEnvConfig = new EnvironmentConfig();

    myEnvConfig.setReadOnly(false);
    myEnvConfig.setAllowCreate(true);
    myEnvConfig.setTransactional(true);

    DbConfig.setReadOnly(false);
    DbConfig.setAllowCreate(true);
    DbConfig.setTransactional(true);
    DbConfig.setSortedDuplicates(false);    // replace duplicates

    // Open the environment
    DbEnv = new Environment(envHome, myEnvConfig);

    // Open the class catalog db. This is used to optimize class serialization.
    ClassCatalogDb = DbEnv.openDatabase(null, "ClassCatalogDB", DbConfig);

    // Create our class catalog
    ClassCatalog = new StoredClassCatalog(ClassCatalogDb);
  }
  
  public static void main(String[] args) throws Exception 
  {
    SleepyCatDatabase db = new SleepyCatDatabase(args[0]);
    
    // each stored map is like a two column table in the database
    Map table = db.getStoredMap(args[1]);
    
    System.out.println("table: " + table);
  }
}