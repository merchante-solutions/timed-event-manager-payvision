/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/database/SQLJConnection.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/14/01 3:42p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.database;

public class SQLJConnection extends SQLJConnectionBase
{
  public SQLJConnection( )
  {
    connect();
  }
  
  public SQLJConnection( boolean autoCommit)
  {
    super( autoCommit );
    connect();
  }
  
  public SQLJConnection( String connectString ) 
  {
    super( connectString );
    connect();
  }
  
  public SQLJConnection( String connectionString, boolean autoCommit)
  {
    super( connectionString, autoCommit );
    connect();
  }
}
