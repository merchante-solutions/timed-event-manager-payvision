package com.mes.match;

// log4j classes.
import org.apache.log4j.Logger;

/**
 * MATCHRecord_IM
 */
class MATCHRecord_IM extends MATCHRecord_Composite
{
  // create class log category
  static Logger log = Logger.getLogger(MATCHRecord_IM.class);

  // constants
  // (none)

  // data members
  MATCHRecord_Composite composite;
  
  // construction
  protected MATCHRecord_IM(String name,int sequenceNum)
  {
    super(name,MATCHRecord.MATCHRECTYPE_COMPOSITEINQUIRY,sequenceNum);
    
    general_record=null;
    principal_record=null;
  }
  
  public String getGeneralRecordString()
  {
    log.debug("MATCHRecord_IM.getGeneralRecordString()="+record.length());
    
    if(record.length() != ((MATCHRECORD_SIZE*2)+(2*DELIM_RECORD.length())))
      return "";
    
    return record.substring(0,MATCHRECORD_SIZE);
  }
  
  public String getPrincipalRecordString()
  {
    if(record.length() != ((MATCHRECORD_SIZE*2)+(2*DELIM_RECORD.length())))
      return "";
    
    return record.substring(MATCHRECORD_SIZE+DELIM_RECORD.length(),record.length()-DELIM_RECORD.length());
  }
  
  public boolean isRequestRecord()
    throws Exception
  {
    return false;
  }

  public boolean IsResponseOriginatedRecord()
    throws Exception
  {
    return true;
  }

  public boolean HasRelatedResponseOriginatedRecords()
    throws Exception
  {
    return false;
  }
    
  public int getRequestType()
  {
    return this.MATCHRECTYPE_COMPOSITEINQUIRY;
  }

  /**
   * setRecordDefinition()
   * 
   * Attempts to set the 'definition' data member if not alredy via
   * the class (static) functions.
   */
  protected void setRecordDefinition()
    throws Exception
  {
    if(general_record==null || principal_record==null)
      throw new Exception("MATCHRecord_IM.setRecordDefinition() - Either the general or principal record is null.");

    if(general_record.definition==null) {
      general_record.definition = 
        "TRANSACTION_CODE:3" + 
        "|ACTION_INDICATOR:1" + 
        "|ACN:7" + 
        "|REC_SEQ_NUM:6" + 
        "|RECORD_TYPE:1" + 
        "|PROCESSED_BY:8" + 
        "|SOURCE:1" +
        "|MERCH_BUSINESS_NAME:30" + 
        "|MERCH_BUSINESS_DBA:30" + 
        "|BUS_ADDRESS1:30" + 
        "|BUS_ADDRESS2:30" + 
        "|BUS_CITY:15" + 
        "|BUS_STATE:2" +
        "|BUS_ZIP:10" + 
        "|BUS_COUNTRY:3" + 
        "|BUS_PHONE:10" + 
        "|MERCH_STATETAX_ID:9" + 
        "|MERCH_FEDERAL_TAX_ID:9" + 
        "|MERCHANT_CATEGORY_CODE:4" + 
        "|MERCH_DATE_OPENED:8" + 
        "|MERCH_DATE_CLOSED:8" + 
        "|MERCH_NUMBER:15" + 
        "|MERCH_BUSINESS_NAME_MATCH:1" + 
        "|MERCH_BUSINESS_DBA_MATCH:1" + 
        "|BUSOWNER_ADDRESS_MATCH:1" + 
        "|BUSINESS_PHONE_MATCH:1" + 
        "|CAT_INDICATOR:1" + 
        "|MERCH_STATETAX_ID_MATCH:1" + 
        "|MERCH_FEDERAL_TAX_ID_MATCH:1" + 
        "|FILLER:1" + 
        "|REQUEST_REASON_CODE:2" + 
        "|MASTERCARD_REF_NUMBER:20" + 
        "|VISA_BIN:6" + 
        "|ORIG_MASTERCARD_REF_NUMBER:20" + 
        "|APP_SEQ_NUM:16" + 
        "|FUTURE_EXPANSION:700";
    }
    
    if(principal_record.definition==null) {
      principal_record.definition = 
           "TRANSACTION_CODE:3"
          +"|ACTION_INDICATOR:1"
          +"|ACN:7"
          +"|REC_SEQ_NUM:6"
          +"|RECORD_TYPE:1"
          +"|PROCESSED_BY:8"
          +"|SOURCE:1"
          +"|BUSOWNER_LAST_NAME_1:25"
          +"|BUSOWNER_FIRST_NAME_1:15"
          +"|BUSOWNER_NAME_INITIAL_1:1"
          +"|MAILADR_LINE1_1:30"
          +"|MAILADR_LINE2_1:30"
          +"|MAILADR_CITY_1:15"
          +"|MAILADR_STATE_1:2"
          +"|MAILADR_ZIP_1:10"
          +"|MAILADR_COUNTRYCODE_1:3"
          +"|MAILADR_PHONE_1:10"
          +"|BUSOWNER_SSN_1:9"
          +"|BUSOWNER_DRVLICNUM_1:25"
          +"|BUSOWNER_DRVLIC_STATE_1:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_1:3"
          +"|BUSOWNER_LAST_NAME_2:25"
          +"|BUSOWNER_FIRST_NAME_2:15"
          +"|BUSOWNER_NAME_INITIAL_2:1"
          +"|MAILADR_LINE1_2:30"
          +"|MAILADR_LINE2_2:30"
          +"|MAILADR_CITY_2:15"
          +"|MAILADR_STATE_2:2"
          +"|MAILADR_ZIP_2:10"
          +"|MAILADR_COUNTRYCODE_2:3"
          +"|MAILADR_PHONE_2:10"
          +"|BUSOWNER_SSN_2:9"
          +"|BUSOWNER_DRVLICNUM_2:25"
          +"|BUSOWNER_DRVLIC_STATE_2:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_2:3"
          +"|BUSOWNER_LAST_NAME_3:25"
          +"|BUSOWNER_FIRST_NAME_3:15"
          +"|BUSOWNER_NAME_INITIAL_3:1"
          +"|MAILADR_LINE1_3:30"
          +"|MAILADR_LINE2_3:30"
          +"|MAILADR_CITY_3:15"
          +"|MAILADR_STATE_3:2"
          +"|MAILADR_ZIP_3:10"
          +"|MAILADR_COUNTRYCODE_3:3"
          +"|MAILADR_PHONE_3:10"
          +"|BUSOWNER_SSN_3:9"
          +"|BUSOWNER_DRVLICNUM_3:25"
          +"|BUSOWNER_DRVLIC_STATE_3:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_3:3"
          +"|BUSOWNER_LAST_NAME_4:25"
          +"|BUSOWNER_FIRST_NAME_4:15"
          +"|BUSOWNER_NAME_INITIAL_4:1"
          +"|MAILADR_LINE1_4:30"
          +"|MAILADR_LINE2_4:30"
          +"|MAILADR_CITY_4:15"
          +"|MAILADR_STATE_4:2"
          +"|MAILADR_ZIP_4:10"
          +"|MAILADR_COUNTRYCODE_4:3"
          +"|MAILADR_PHONE_4:10"
          +"|BUSOWNER_SSN_4:9"
          +"|BUSOWNER_DRVLICNUM_4:25"
          +"|BUSOWNER_DRVLIC_STATE_4:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_4:3"
          +"|BUSOWNER_LAST_NAME_5:25"
          +"|BUSOWNER_FIRST_NAME_5:15"
          +"|BUSOWNER_NAME_INITIAL_5:1"
          +"|MAILADR_LINE1_5:30"
          +"|MAILADR_LINE2_5:30"
          +"|MAILADR_CITY_5:15"
          +"|MAILADR_STATE_5:2"
          +"|MAILADR_ZIP_5:10"
          +"|MAILADR_COUNTRYCODE_5:3"
          +"|MAILADR_PHONE_5:10"
          +"|BUSOWNER_SSN_5:9"
          +"|BUSOWNER_DRVLICNUM_5:25"
          +"|BUSOWNER_DRVLIC_STATE_5:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_5:3"

          +"|BUSOWNER_LAST_NAME_MATCH_1:1"
          +"|MAILADR_LINE1_MATCH_1:1"
          +"|MAILADR_PHONE_MATCH_1:1"
          +"|BUSOWNER_SSN_MATCH_1:1"
          +"|BUSOWNER_DRVLICNUM_MATCH_1:1"
          
          +"|BUSOWNER_LAST_NAME_MATCH_2:1"
          +"|MAILADR_LINE1_MATCH_2:1"
          +"|MAILADR_PHONE_MATCH_2:1"
          +"|BUSOWNER_SSN_MATCH_2:1"
          +"|BUSOWNER_DRVLICNUM_MATCH_2:1"

          +"|BUSOWNER_LAST_NAME_MATCH_3:1"
          +"|MAILADR_LINE1_MATCH_3:1"
          +"|MAILADR_PHONE_MATCH_3:1"
          +"|BUSOWNER_SSN_MATCH_3:1"
          +"|BUSOWNER_DRVLICNUM_MATCH_3:1"

          +"|BUSOWNER_LAST_NAME_MATCH_4:1"
          +"|MAILADR_LINE1_MATCH_4:1"
          +"|MAILADR_PHONE_MATCH_4:1"
          +"|BUSOWNER_SSN_MATCH_4:1"
          +"|BUSOWNER_DRVLICNUM_MATCH_4:1"

          +"|BUSOWNER_LAST_NAME_MATCH_5:1"
          +"|MAILADR_LINE1_MATCH_5:1"
          +"|MAILADR_PHONE_MATCH_5:1"
          +"|BUSOWNER_SSN_MATCH_5:1"
          +"|BUSOWNER_DRVLICNUM_MATCH_5:1"

          +"|FUTURE_EXPANSION:60";
    }
  }
  
  /**
   * generateRecord()
   * 
   * Sub-class override.
   */
  public boolean generateRecord()
  {
    try {
      
      if(record.length()>0) // presume already generated
        return true;
      
      // ensure both general and principal constituent records are set
      if(general_record==null && principal_record==null) {
        log.error("Unable to generate IM record: Both general and principal constituent records not currently specifed.");
        return false;
      }
      
      StringBuffer recordBuf = new StringBuffer(0);
      MATCHRecord mr = null;
    
      // general record
      if(general_record!=null) {
        if(!general_record.generateRecord())
          log.error("An error occurred attempting to generate composite general record string (MATCH record: '"+general_record.getName()+"')+");
        else
          recordBuf.append(general_record.getRecord());
        
        // IMPT: replace the action identifier char to indicate a response originated record!
        recordBuf.setCharAt(3,'M');
        
      } else
        log.warn("General record NOT generated.");
    
      // principal record
      if(principal_record!=null) {
        if(!principal_record.generateRecord())
          log.error("An error occurred attempting to generate IM principal record string (MATCH record: '"+principal_record.getName()+"')+");
        else
          recordBuf.append(principal_record.getRecord());
        
        // IMPT: replace the action identifier char to indicate a response originated record!
        recordBuf.setCharAt(MATCHRECORD_SIZE+DELIM_RECORD.length()+3,'M');
      
      } else
        log.warn("Principal record NOT generated.");
      
      // set 'record' data member
      record=recordBuf.toString();
      
      log.debug("************");
      log.debug("general_record.length()="+general_record.getRecord().length());
      log.debug("principal_record.length()="+principal_record.getRecord().length());
      log.debug("record.length()="+record.length());
      log.debug("************");
      
      return true;
    }
    catch(Exception e) {
      log.error("generateRecord(IM) - Exception: '"+e.getMessage()+"'.");
      return false;
    }
    
  }
  
  public String getMRID()
    throws Exception
  {
    throw new Exception("MATCHRecord_IM EXCEPTION: Attempt to call unsupported method: getMRID().");
  }
  
  public String getResponseAction()
    throws Exception
  {
    throw new Exception("MATCHRecord_IM EXCEPTION: Attempt to call unsupported method: getResponseAction().");
  }
  
  public String getErrorCodes()
    throws Exception
  {
    throw new Exception("MATCHRecord_IM EXCEPTION: Attempt to call unsupported method: getErrorCodes().");
  }
  
  public String getMCRefNum()
    throws Exception
  {
    throw new Exception("MATCHRecord_IM EXCEPTION: Attempt to call unsupported method: getMCRefNum().");
  }
  
  public String getVisaBin()
    throws Exception
  {
    throw new Exception("MATCHRecord_IM EXCEPTION: Attempt to call unsupported method: getVisaBin().");
  }

  public String getOrigMCRefNum()
    throws Exception
  {
    throw new Exception("MATCHRecord_IM EXCEPTION: Attempt to call unsupported method: getOrigMCRefNum().");
  }

} // class MATCHRecord_IM
