package com.mes.match;

import java.util.Enumeration;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;

/**
 * IMPManager - Internal Match Process Manager
 * 
 * Manages the goings on of the Internal Match Process
 */
final class IMPManager extends Object
{
  // create class log category
  static Category log = Category.getInstance(IMPManager.class.getName());

  // Singleton
  public static IMPManager getInstance()
  {
    if(_instance==null) {
      try {
        _instance = new IMPManager();
      }
      catch(Exception e) {
        _instance = null;
        log.error("IMPManager - Unable to instantiate due to exception: "+e.getMessage()+".");
      }
    }
    return _instance;
  }
  private static IMPManager _instance = null;
  ///
  
  
  // constants
  
  /**
   * Blacklist target fields.  I.e. a match has to occur on one of these fields for
   *  an app to be deemed blackisted.
   */
  public static final String[]      blacklistFields = 
  {
     "MERCH_BUSINESS_NAME"
    ,"MERCH_BUSINESS_DBA"
    ,"MERCH_STATETAX_ID"
    ,"MERCH_FEDERAL_TAX_ID"
    ,"MERCH_WEB_URL"
    ,"MAIL_ADDRESS1"
    ,"BUSINESS_PHONE"
    ,"DDA"
    ,"BUSOWNER_LAST_NAME"
    ,"BUSOWNER_SSN"
    ,"BUSOWNER_DRVLICNUM"
    ,"MAILADR_LINE1"
    ,"MAILADR_PHONE"
  };
  
  /**
   * General fields that aren't actively searched against.
   */
  public static final String[]      gnrlSkipFlds = 
  {
     "MAIL_ADDRESS2"
    ,"MAIL_CITY"
    ,"MAIL_STATE"
    ,"MAIL_ZIP"
    ,"MAIL_COUNTRY"
  };
  
  /**
   * Principal fields that aren't actively searched against.
   */
  public static final String[]      prinSkipFlds = 
  {
     "MAILADR_LINE2"
    ,"MAILADR_CITY"
    ,"MAILADR_STATE"
    ,"MAILADR_ZIP"
    ,"MAILADR_COUNTRYCODE"
    ,"BUSOWNER_FIRST_NAME"
    ,"BUSOWNER_NAME_INITIAL"
  };
  
  /**
   * Subset of the skipped General fields that are subject to a field match.
   */
  public static final String[]      gnrlSkipMatchFields = 
  {
     "MAIL_CITY"
    ,"MAIL_ZIP"
  };
  
  /**
   * Subset of the skipped Principal fields that are subject to a field match.
   */
  public static final String[]      prinSkipMatchFields = 
  {
     "BUSOWNER_FIRST_NAME"
    ,"MAILADR_CITY"
    ,"MAILADR_ZIP"
  };
  
  
  // data members
  protected DBQueueOps_MATCHDataQueueItem dbom = null;


  // construction
  private IMPManager()
  {
    dbom = new DBQueueOps_MATCHDataQueueItem();
  }
  
  public void doInternalMatchCheck(PersistQueue_MATCHDataQueueItem_Input requests)
  {
    MATCHDataQueueItem mdqi;
    for(Enumeration e = requests.getQueueElmntKeys();e.hasMoreElements();) {
      mdqi=(MATCHDataQueueItem)requests.getElement((String)e.nextElement());
      try {
        //if(mdqi.getIMPResponse().length()==0) // only run if not already run
          doInternalMatchCheck(mdqi);
      }
      catch(Exception ex) {
        log.error("doInternalMatchCheck(requests) EXCEPTION: "+ex.toString());
      }
    }
  }
  
  
  /**
   * doInternalMatchCheck()
   * 
   * Performs an internal match check for the given request (MATCHDataQueueItem).
   * Adds the IM responses to this given request.
   */
  public void doInternalMatchCheck(MATCHDataQueueItem mdqi)
  {
    if(mdqi==null || mdqi.getNumGenerals()<1) {
      return;
    }
    
    log.info("Performing IM check for MATCH request '"+mdqi.getAppSeqNum()+"'...");
    
    mdqi.clearIMResponseItems();
    mdqi.setIMPResponse(MATCHDataQueueItem.MATCHRESPONSEACTION_NOMATCH);
    mdqi.setIMPStatus(MATCHDataQueueItem.IMPSTATUS_OK);
    
    for(int j=0; j<2; j++) {  // blacklist/imp search
    
      boolean blacklist = (j==0);
      
      log.debug("doInternalMatchCheck(appSeqNum: "+mdqi.getAppSeqNum()+") "+(blacklist? "BLACKLIST CHECK...":"IMP Search..."));
      
      for(int k=0; k<2; k++) {  // general/principal
      
        boolean   gnrlOrPrin    = (k==0);
        
        String[]  impAry        = gnrlOrPrin? MATCHRecord.BASEFLDNMES_GENERAL:MATCHRecord.BASEFLDNMES_PRINCIPAL;
        String[]  skipAry       = gnrlOrPrin? gnrlSkipFlds:prinSkipFlds;
        String[]  skipMatchAry  = gnrlOrPrin? gnrlSkipMatchFields:prinSkipMatchFields;

        log.debug("doInternalMatchCheck() "+(gnrlOrPrin? "GENERAL...":"PRINCIPALS..."));
        
        for(int c=0; c<impAry.length; c++) {  // field arrays
          
          try {
          
            String fldNme   = impAry[c];
            String fldVal   = null;
            String[] Ids    = null;
            String id       = null;
      
            // skip these fields
            boolean skipFld       = false;
            for(int g=0; g<skipAry.length; g++) {
              if(fldNme.compareToIgnoreCase(skipAry[g]) == 0) {
                skipFld=true;
                break;
              }
            }
            if(skipFld) {
              log.debug("Skipping field '"+fldNme+"'.  Continuing...");
              continue;
            }
            
            if(gnrlOrPrin) {
              Ids = new String[1];
              Ids[0] = mdqi.getAppSeqNum();
            } else {
              Ids = mdqi.getPrincipalIDs();
            }
            
            for(int t=0; t<Ids.length; t++)  {  // id array
              fldVal = gnrlOrPrin? mdqi.getGeneralDataItem(fldNme) : mdqi.getPrincipalDataItem(Ids[t],fldNme);
              
              log.debug("doInternalMatchCheck(mdqi.appSeqNum="+mdqi.getAppSeqNum()+") - fldNme: "+fldNme+", fldVal: "+fldVal);
              
              // skip empty field values
              if(fldVal.length()<1) {
                continue;
              }
              
              fldVal = fldVal.trim().toLowerCase();
              
              // url --> domain name
              if(fldNme.compareToIgnoreCase("MERCH_WEB_URL") == 0) {
                if(fldVal.startsWith("http://"))
                  fldVal = fldVal.substring(7);
                if(fldVal.startsWith("www."))
                  fldVal = fldVal.substring(4);
              }
      
              Vector v=dbom.impFind(mdqi.getAppSeqNum(),fldNme,fldVal,gnrlOrPrin,blacklist);
              
              log.debug("doInternalMatchCheck() - impFind().size()="+v.size());

              if(v.size()>0) {
                
                if(blacklist) {
                  for(int jj=0; jj<blacklistFields.length; jj++) {
                    if(blacklistFields[jj].compareToIgnoreCase(fldNme) == 0) {
                      mdqi.setIMPResponse(MATCHDataQueueItem.MATCHRESPONSEACTION_BLACKLISTED);
                      log.info("App Seq Num: "+mdqi.getAppSeqNum()+" found BLACKLISTED!");
                      break;
                    }
                  }
                }
                
                for(int i=0;i<v.size();i++) { // im responses
                  id=(String)v.elementAt(i);
                  IMResponse imr = loadIMPMerchant(id,mdqi,blacklist);
                  
                  // determine if match is exact or phonetic
                  String matchType;
                  String matchedField;
                  String mtchIndicatorFld = fldNme + "_MATCH";
                  
                  // general fields
                  if(gnrlOrPrin) {
                    matchedField = imr.getMATCHDataItem().getGeneralDataItem(fldNme);
                    if(matchedField.trim().toLowerCase().equals(fldVal.trim().toLowerCase())) {
                      matchType = "E";
                    } else {
                      matchType = "P";
                    }
                    mdqi.addIMResponseGeneralItem(id,mtchIndicatorFld,matchType);
                    log.debug("mdqi.addIMResponseGeneralItem(id: "+id+",mtchIndicatorFld: "+mtchIndicatorFld+","+matchType+")");
                    
                    // identify any "exact" (no phonetic) matches on "skip match" fields for each im response
                    for(int g=0; g<skipMatchAry.length; g++) {
                      
                      try {
                        String skipFldVal = imr.getMATCHDataItem().getGeneralDataItem(skipMatchAry[g]);
                        if(skipFldVal.length()>0 && mdqi.getGeneralDataItem(skipMatchAry[g]).trim().compareToIgnoreCase(skipFldVal) == 0) {
                          mtchIndicatorFld = skipMatchAry[g] + "_MATCH";
                          mdqi.addIMResponseGeneralItem(id,mtchIndicatorFld,"E");
                          log.debug("SKIPPED GENERAL FIELD MATCH!: mdqi.addIMResponseGeneralItem(id: "+id+",mtchIndicatorFld: "+mtchIndicatorFld+","+matchType+")");
                        }
                      }
                      catch(Exception e) {
                        log.error("SKIPPED GENERAL FIELD MATCH check EXCEPTION: "+e.toString());
                      }
                      
                    }
                    ///
        
                  // principal fields
                  } else {
                    
                    String pIds[] = imr.getMATCHDataItem().getPrincipalIDs();
                    for(int b=0; b<pIds.length; b++) {
                      matchedField = imr.getMATCHDataItem().getPrincipalDataItem(pIds[b],fldNme);
                      if(matchedField.trim().toLowerCase().equals(fldVal.trim().toLowerCase())) {
                        matchType = "E";
                      } else {
                        matchType = "P";
                      }
                      mdqi.addIMResponsePrincipalItem(id,pIds[b],mtchIndicatorFld,matchType);
                      log.debug("mdqi.addIMResponsePrincipalItem(gnrl id: "+id+", prin id: "+pIds[b]+", mtchIndicatorFld: "+mtchIndicatorFld+", "+matchType+")");

                      // identify any "exact" (no phonetic) matches on "skip match" fields for each im response
                      for(int g=0; g<skipMatchAry.length; g++) {

                        try {
                          String skipFldVal = imr.getMATCHDataItem().getPrincipalDataItem(pIds[b],skipMatchAry[g]);
                          if(skipFldVal.length()<1)
                            continue;
                          
                          String[] prntPids = mdqi.getPrincipalIDs();
                          for(int h=0; h<prntPids.length; h++) {
                        
                            if(mdqi.getPrincipalDataItem(id,prntPids[h]).trim().compareToIgnoreCase(skipFldVal) == 0) {
                              mtchIndicatorFld = skipMatchAry[g] + "_MATCH";
                              mdqi.addIMResponsePrincipalItem(id,prntPids[h],mtchIndicatorFld,"E");
                              log.debug("SKIPPED PRINCIPAL FIELD MATCH!: mdqi.addIMResponsePrincipalItem(id: "+id+",prntPids["+h+"]:"+prntPids[h]+",mtchIndicatorFld: "+mtchIndicatorFld+",E");
                            }

                          }
                          
                        }
                        catch(Exception e) {
                          log.error("SKIPPED PRINCIPAL FIELD MATCH check EXCEPTION: "+e.toString());
                        }
                    
                      }
                      ///

                    }
                  }
                }
              }
            }
      
          }
          catch(Exception e) {
            log.error("doInternalMatchCheck() EXCEPTION: "+e.toString());
          }
    
        }
      
      }  
    
    }
    
    
    if(!mdqi.isImpBlacklisted()) {
    
      Vector imResponses = mdqi.getIMResponseItems();
      IMResponse imResponse;
      boolean bOK;
    
      log.info("doInternalMatch() [PRE-INTERCEDE] imResponses.size()="+imResponses.size());

      // intercede - require principals to have at least two matching fields
      log.debug("Enforcing at least one principal have at least two matching fields for a valid internal match response (imResponses.size()="+imResponses.size()+").");
      for(int i=0;i<imResponses.size();i++) {
        try {
          imResponse = (IMResponse)imResponses.elementAt(i);
        }
        catch(Exception e) {
          log.warn("bailing from loop - all elements have been dynamically removed!");
          break;
        }
      
        //log.debug("On imResponse '"+imResponse.getMATCHDataItem().getGeneralID()+"'...");
        bOK=false;  // reset for this im response
        String[] pIds = imResponse.getMATCHDataItem().getPrincipalIDs(); 
        int[] numPMatches = new int[pIds.length];
        for(int j=0;j<pIds.length;j++) {
          //log.debug("im - 2 principal fields check: On principal num: "+pIds[j]);
          numPMatches[j]=0; // init
          for(Enumeration e=imResponse.getMATCHDataItem().getPrincipalKeys(StringUtilities.stringToInt(pIds[j]));e!=null && e.hasMoreElements();) {
            String pkey=(String)e.nextElement();
            //log.debug("pkey="+pkey);
            if(pkey.endsWith("_MATCH"))
              numPMatches[j]++;
          }
        }
        for(int j=0;j<numPMatches.length;j++) {
          log.debug("numPMatches["+j+"]="+numPMatches[j]);
          if(numPMatches[j]>1) {
            bOK=true;
            break;
          }
        }
        if(!bOK) {
          mdqi.clearIMResponseItem(imResponse);
          i--;
          log.warn("IM Response (General ID: '"+imResponse.getMATCHDataItem().getGeneralID()+"') cleared: Less than two principal field matches exist for this response.");
        }
      }
      log.debug("Enforcing at least two principals (imResponses.size()="+imResponses.size()+") - DONE");
    

      // exclude those IM responses whose assoc. app type has been excluded from being included in im result sets.
      log.debug("Excluding IM responses whose associated app type is excluded from the IM process (imResponses.size()="+imResponses.size()+")...");
      int appType;
      final int[] appBlacklist = dbom.getIMResultExclusionsByAppType();
      for(int i=0;i<imResponses.size();i++) {
        try {
          imResponse = ((IMResponse)imResponses.elementAt(i));
        }
        catch(Exception e) {
          log.debug("bailing from app type exclusion loop - all elements have been dynamically removed!");
          break;
        }
        appType=StringUtilities.stringToInt(imResponse.getMATCHDataItem().getGeneralDataItem("MERCH_APPLICATION_TYPE"),-1);
        for(int j=0;j<appBlacklist.length;j++) {
          if(appType==appBlacklist[j]) {
            mdqi.clearIMResponseItem(imResponse);
            i--;
            log.warn("IM Response (General ID: '"+imResponse.getMATCHDataItem().getGeneralID()+"') cleared: Associated application type excluded from the Internal Match process.");
            break;
          }
        }
      }
      
    } else {
      log.warn("Skipped IM filtering since app found blacklisted.");
    }
    
    // set the IM response and status
    if(mdqi.getNumIMResponseItems()>0 && mdqi.getIMPStatus()!=MATCHDataQueueItem.IMPSTATUS_RESULTSETTOOLARGE && mdqi.getIMPResponse()!=MATCHDataQueueItem.MATCHRESPONSEACTION_BLACKLISTED) {
      mdqi.setIMPResponse(MATCHDataQueueItem.MATCHRESPONSEACTION_POSSIBLEMATCH);
    }
    
    
    // convert all loaded IM responses to MATCH record format compliant strings in order to persist the IM data
    log.debug("doInternalMatchCheck() - Building IM Record strings...");
    IMResponse imr;
    MATCHRecord_IM mrim;
    Vector imItems = mdqi.getIMResponseItems();
    for(int i=0;i<imItems.size();i++) {
      imr=(IMResponse)imItems.elementAt(i);
      try {
        mrim=MATCHRecordBuilder.buildIMRecord(imr.getMATCHDataItem());
        imr.setGeneralRecord(mrim.getGeneralRecordString());
        imr.setPrincipalRecord(mrim.getPrincipalRecordString());
      }
      catch(Exception e) {
        log.error("Unable to build IM response record for IM Response ID '"+imr.getID()+"' (Persistence will fail.): '"+e.getMessage()+"'.");
      }
    }
    
    // impose queue ops based on IM match status
    log.debug("doInternalMatchCheck() - Impose queue ops based on IM match status...");
    if(mdqi.getIMPResponse().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_POSSIBLEMATCH)
    || mdqi.getIMPResponse().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_RETROPOSSIBLEMATCH)
    || mdqi.getIMPResponse().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_BLACKLISTED))
    {    
      mdqi.setQueueAction(MATCHDataQueueItem.QUEUEACTION_ADDTOINTERNALMATCHCREDIT);
    }
    else 
      mdqi.setQueueAction(MATCHDataQueueItem.QUEUEACTION_IMRELEASE);
    
    log.info("IM Check for MATCH Request '"+mdqi.getID()+"' complete.  Num IM Matches: "+mdqi.getNumIMResponseItems()+".");
  }
  
  
  /**
   * loadIMPMerchant()
   * 
   * Loads the merchant specified by the general id into the IM response collection.
   */
  private IMResponse loadIMPMerchant(String id,MATCHDataQueueItem mdqi,boolean fromImpBlacklist)
  {
    //log.debug("loadIMPMerchant(id:'"+id+"',mdqi.id:'"+mdqi.getID()+"') - START");
    
    // check if general ID exists
    
    IMResponse imr  = null;
    
    try {
      Vector v        = mdqi.getIMResponseItems();
      boolean bFound  = false;
    
      for(int i=0;i<v.size();i++) {
        imr=(IMResponse)v.elementAt(i);
        if(imr.getMATCHDataItem().getGeneralDataItem("ID_GENERAL").equals(id)) {
          bFound=true;
          break;
        }
      }
    
      //log.debug("loadIMPMerchant() - bFound="+bFound);
      if(!bFound) {
        // load merchant data into the IMResponse (snapshot in time of the merchant data at time of IM check).
        imr = new IMResponse(new MATCHDataItem());
        mdqi.setIMResponse(imr);
        imr.getMATCHDataItem().setGeneralDataItem("ID_GENERAL",id);
        if(fromImpBlacklist) {
          dbom.loadIMResponseMerchantBlacklist(id,imr);
          imr.setImpFromBlacklist(true);
        } else {
          dbom.loadIMResponseMerchant(id,imr);
        }
        log.debug("IM Response Merchant (App Seq Num: '"+id+"') loaded.");
      }
    }
    catch(Exception e) {
      log.error("loadIMPMerchant() EXCEPTION: "+e.toString());
    }
    
    return imr; // for convenience
  }

} // class IMPManager
