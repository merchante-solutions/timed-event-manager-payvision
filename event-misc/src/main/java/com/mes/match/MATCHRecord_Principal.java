package com.mes.match;

// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;

/**
 * MATCHRecord_Principal
 * 
 * Extends MATCHRecord encapsulating request header specific record data and logic
 */
class MATCHRecord_Principal extends MATCHRecord
{
  // create class log category
  static Category log = Category.getInstance(MATCHRecord_Principal.class.getName());

  // constants
  // (none)

  // data members
  // (none)
  
  // construction
  protected MATCHRecord_Principal(String name,int record_type,int sequenceNum)
  {
    super(name,record_type,sequenceNum);
  }
  
  /**
   * getFieldValue()
   * 
   * Sub-class override.
   */
  protected String getFieldValue(String fldNme)
    throws Exception
  {
    String value=null;
    
    switch(record_type) {
      case MATCHRECTYPE_ADDPRINCIPAL:
        // fall through to MATCHRECTYPE_INQUIRYPRINCIPAL case
      case MATCHRECTYPE_INQUIRYPRINCIPAL:
        if(fldNme.equals("TRANSACTION_CODE"))
          value="651";
        else if(fldNme.equals("ACTION_INDICATOR")) {
          if(record_type==MATCHRECTYPE_ADDPRINCIPAL)
            value="A";
          else if(record_type==MATCHRECTYPE_INQUIRYPRINCIPAL)
            value="I";
        }
        else if(fldNme.equals("ACN"))
          value=StringUtilities.rightJustify(MATCHRECCNST_ACN,7,'0');
        else if(fldNme.equals("REC_SEQ_NUM"))
          value=StringUtilities.rightJustify(Integer.toString(sequenceNum),6,'0');
        else if(fldNme.equals("RECORD_TYPE"))
          value="2";
        else if(fldNme.equals("FUTURE_EXPANSION"))
          value="";
        else {
          if(mdi==null)
            throw new Exception("Unable to retreive field value from general request record: No data container specified.");
          // direct native field (1-to-1 in this case)
          if(fldNme.endsWith("_1") || fldNme.endsWith("_2") || fldNme.endsWith("_3") || fldNme.endsWith("_4") || fldNme.endsWith("_5") || fldNme.endsWith("_6")) {
            final String actFldNme = fldNme.substring(0,fldNme.length()-2);
            final String principalNum = fldNme.substring(fldNme.length()-1);
              // NOTE: this value (principalNum) corres. exactly to MATCHDataItem.principalID
            //log.debug("actFldNme='"+actFldNme+"', principalNum='"+principalNum+"'.");
            value=mdi.getPrincipalDataItem(principalNum,actFldNme);
          }
        }
        break;
    }
    
    if(value==null)
      throw new Exception("Get request principal field value failed: field ('"+fldNme+"') un-mapped.");
    
    //log.debug("Field: '"+fldNme+"' mapped to value: '"+value+"' (MATCHRecord.name='"+name+"',MATCHRecord.record_type='"+record_type+"'");
    return value;
  }
  
  /**
   * setFieldValue()
   * 
   * No eligable fields exist.
   */
  protected void setFieldValue(String fldNme, String fldVal)
    throws Exception
  {
    throw new Exception("Erroneous method call: MATCHRecord_Principal.setFieldValue()");
  }

} // class MATCHRecord_Principal
