/*@lineinfo:filename=DBQueueOps_MATCHDataQueueItem*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.match;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.database.SQLJConnectionDirect;
import sqlj.runtime.ResultSetIterator;

/**
 * DBQueueOps_MATCHDataQueueItem
 * -----------------------------
 * - Performs all MATCHDataQueueItem db related ops
 * - All data subject to MATCH submission is represented in table 'MATCH_REQUESTS'
 * 
 * 
 * MATCHDataQueueItem db mapping
 * -----------------------------
 * MATCH_REQUESTS <1--M> MERCHANT <1--M> BUSINESSOWNER
 *                          ""    <1--1> ADDRESS (ADDRESSTYPE_CODE=1 - mailing address)
 *                          ""    <1--1> ADDRESS (ADDRESSTYPE_CODE=2 - business address)
 *                          ""    <1--1> ADDRESS (ADDRESSTYPE_CODE=4,5,6 - principal business owner 1-3)
 *                          ""    <1--1> MERCHCONTACT
 *      ""        <1--M> MATCH_REQUESTS_RESPONSEDATA
 * 
 *  MATCH_REQUESTS  - MATCH request queue (main table - holds all MATCH requests and corres. response data)
 *  MERCHANT        - corres. to General MATCH data
 *  BUSINESSOWNER   - corres. to Principal MATCH data
 *  MERCHCONTACT    - Alternate Principal data.  Used when no BUSINESSOWNER-based principal data found.
 *  MATCH_REQUESTS_RESPONSEDATA - contains any MATCH db data sent back in 
 *                                response records assoc. w/ the original request
 * 
 */
final class DBQueueOps_MATCHDataQueueItem extends SQLJConnectionDirect
{
  // create class log category
  static Category log = Category.getInstance(DBQueueOps_MATCHDataQueueItem.class.getName());
  
  // constants
  
  // queue data divisions
  public static final int     DBQ_TYPE_INPUT            = 1;
  public static final int     DBQ_TYPE_OUTPUT           = 2;
  public static final int     DBQ_TYPE_MERCHANT         = 3;
    // this corres. to queue containing all requests for a given merchant only
  
  // match data divisions
  public static final int     DBQ_MATCHDATA_REQUEST     = 1;
  public static final int     DBQ_MATCHDATA_GENERAL     = 2;
  public static final int     DBQ_MATCHDATA_PRINCIPAL   = 3;
  public static final int     DBQ_MATCHDATA_MDRECS      = 4;
  public static final int     DBQ_MATCHDATA_ALL         = 5;
  
  // key types 
  public static final int     DBQ_KEY_UNDEFINED         = 0;
  public static final int     DBQ_KEY_MERCNUMBER        = 1;  // MERCHANT.MERCH_NUMBER
  public static final int     DBQ_KEY_APP_SEQ_NUM       = 2;  // MERCHANT.APP_SEQ_NUM
  public static final int     DBQ_KEY_MERCCNTRLNUM      = 3;  // MERCHANT.MERC_CNTRL_NUMBER
  public static final int     DBQ_KEY_PENDINGBYMERCNAME = 4;  // deferred
    
  
  // data members
  // (NONE)
  

  // construction
  public DBQueueOps_MATCHDataQueueItem()
  {
    // - disable auto commit
    super(false);
  }
  
  
  public int[] getIMResultExclusionsByAppType()
  {
    int[] rval = null;
    
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try {
              
      connect();

      int cnt = 0;

      /*@lineinfo:generated-code*//*@lineinfo:100^7*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//          
//          from   app_type
//          where  EXCLUDE_FROM_INT_MATCH_RESULTS = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n         \n        from   app_type\n        where  EXCLUDE_FROM_INT_MATCH_RESULTS = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:106^7*/

      rval = new int[cnt];
      
      /*@lineinfo:generated-code*//*@lineinfo:110^7*/

//  ************************************************************
//  #sql [Ctx] it = { select app_type_code
//          from   app_type
//          where  EXCLUDE_FROM_INT_MATCH_RESULTS = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select app_type_code\n        from   app_type\n        where  EXCLUDE_FROM_INT_MATCH_RESULTS = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:115^7*/
      rs = it.getResultSet();

      while(rs.next()) {
        rval[--cnt] = rs.getInt(1);
      }
              
    }
    catch(Exception e) {
      log.error("getIMResultExclusionsByAppType() EXCEPTION: "+e.getMessage());
    }
    finally {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return rval;
  }
  
  /**
  * removeOldByProcessEndDate()
  * 
  * Removes MATCH_REQUEST records and related records those requests
  * found to have process end date earlier or equal to the process end date param
  */
  public void removeOldByProcessEndDate(Date processEndDate)
  {
    try {
              
      connect();

      java.sql.Timestamp sqlDate_procEndDate = new java.sql.Timestamp(processEndDate.getTime());

      // IMPT: 'MATCH_REQUESTS_RESPONSEDATA' records are cascade deleted

      /*@lineinfo:generated-code*//*@lineinfo:151^7*/

//  ************************************************************
//  #sql [Ctx] { DELETE  
//                  MATCH_REQUESTS
//          WHERE   
//                  PROCESS_END_DATE <= :sqlDate_procEndDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "DELETE  \n                MATCH_REQUESTS\n        WHERE   \n                PROCESS_END_DATE <=  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,sqlDate_procEndDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:157^7*/
              
    }
    catch(Exception e) {
      log.error("removeOld() EXCEPTION: '"+e.getMessage()+"'.");
      return;
    }
    finally {
      cleanUp();
    }
  }
  
  public Vector impFind(String appSeqNum, String fldNme, String fldVal, boolean isGnrlOrPrinFld, boolean blacklist)
  {
    log.debug("impFind(appSeqNum: "+appSeqNum+", fldNme: "+fldNme+", fldVal: "+fldVal+", isGnrlOrPrinFld: "+isGnrlOrPrinFld+", blacklist: "+blacklist+") - START");

    Vector v = new Vector(0,1);
    
    try {
      connect();

      StringBuffer sql = new StringBuffer();
      PreparedStatement ps = null;

      // blacklist
      if(blacklist) {
        sql.append("select    mib_seq_num ");
        sql.append("from      match_imp_blacklist ");
        sql.append("where     (UPPER(TRIM(BOTH ' ' from ");
        sql.append(fldNme);
        sql.append(")) like '%' || UPPER(TRIM(BOTH ' ' from ?)) || '%') or ('%' || UPPER(TRIM(BOTH ' ' from ");
        sql.append(fldNme);
        sql.append(")) || '%' like UPPER(TRIM(BOTH ' ' from ?))) ");
        
        log.debug("impFind() sql="+sql.toString());
        
        ps = getPreparedStatement(sql.toString());
        ps.setString(1,fldVal);
        ps.setString(2,fldVal);
      
      // existing merchants
      } else {
        
        // map field name: (match record --> mesdb)
        if(fldNme.compareToIgnoreCase("MERCH_BUSINESS_NAME")==0) {
          fldNme = "MERCH_LEGAL_NAME";
        } else if(fldNme.compareToIgnoreCase("MERCH_BUSINESS_DBA")==0) {
          fldNme = "MERCH_BUSINESS_NAME";
        } else if(fldNme.compareToIgnoreCase("MAIL_ADDRESS1")==0 || fldNme.compareToIgnoreCase("MAILADR_LINE1")==0) {
          fldNme = "ADDRESS_LINE1";
        } else if(fldNme.compareToIgnoreCase("BUSINESS_PHONE")==0) {
          fldNme = "ADDRESS_PHONE";
        } else if(fldNme.compareToIgnoreCase("DDA")==0) {
          fldNme = "MERCHBANK_ACCT_NUM";
        }
        
        sql.append("select * from (");
        
        sql.append("select    m.app_seq_num     ");
        sql.append("from      merchant        m ");
        sql.append("          ,address        a ");
        sql.append("          ,merchbank      b ");
        
        if(!isGnrlOrPrinFld)
          sql.append("        ,businessowner  bo ");
        
        sql.append("where     m.app_seq_num = a.app_seq_num(+) and a.addresstype_code(+) = ? ");
        
        if(!isGnrlOrPrinFld)
          sql.append("        and m.app_seq_num = bo.app_seq_num(+) ");
          
        sql.append("          and m.app_seq_num = b.app_seq_num(+) ");
        sql.append("          and m.app_seq_num <> ? ");

        sql.append("          and (instr("+fldNme+",?)>0 or (upper(trim(both ' ' from "+fldNme+")) like '%' || upper(trim(both ' ' from ?)) || '%') or ('%' || upper(trim(both ' ' from "+fldNme+")) || '%' like upper(trim(both ' ' from ?)))) ");
        
        sql.append("          order by m.app_seq_num desc");
        
        sql.append(") where rownum < 50 ");
        log.debug("impFind() sql="+sql.toString());
        
        ps = getPreparedStatement(sql.toString());

        int c = 0;

        // addresstype code
        if(fldNme.startsWith("MAIL_ADDRESS")) // (general)
          ps.setInt(++c,1);
        else if(fldNme.startsWith("MAILADR_LINE"))  // (principal)
          ps.setInt(++c,2);
        else
          ps.setInt(++c,-1);

        ps.setString(++c,appSeqNum);
        
        ps.setString(++c,fldVal);
        
        ps.setString(++c,fldVal);
        ps.setString(++c,fldVal);
      }

      ResultSet rs = ps.executeQuery();

      while(rs.next())
        v.addElement(rs.getString(1));
      
      rs.close();
      ps.close();

    }
    catch(Exception e) {
      log.error("impFind() EXCEPTION: "+e.getMessage());
    }
    finally {
      cleanUp();
    }

    log.debug("impFind(appSeqNum: "+appSeqNum+", fldNme: "+fldNme+", fldVal: "+fldVal+", isGnrlOrPrinFld: "+isGnrlOrPrinFld+", blacklist: "+blacklist+") - "+v.size()+" match(es) found.");
    return v;
  }
  
  /**
   * getPendingRequestIDByMerchBusinessName()
   *
   * Reteives the MATCH request ID associated with Pending requests having associated merch business name as specified
   * For use in attempting to pigeon hole a response to originating request 
   *  as MATCH FileExpress system stupidly does not provide place to round-trip the request ID in some cases.
   * 
   * @param       merch_business_name   Corres. to db field: MERCHANT.MERCH_BUSINESS_NAME.
   * 
   * @exception   Throws exception upon non-determinate cases. (0 matches or more than one match) or otherwise.
   * 
   * @return      String - Matching MATCH Request ID
   */
  public String getPendingRequestIDByMerchBusinessName(String merch_business_name)
    throws Exception
  {
    try {
      
      connect();
    
      ResultSetIterator rsItr = null;
      
      /*@lineinfo:generated-code*//*@lineinfo:300^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                  MATCH_REQUESTS.MATCH_SEQ_NUM
//          FROM    
//                  MERCHANT,MATCH_REQUESTS
//          WHERE   
//                  MERCHANT.APP_SEQ_NUM=MATCH_REQUESTS.APP_SEQ_NUM
//          	      AND MATCH_REQUESTS.REQUEST_STATUS=:MATCHDataQueueItem.MATCHREQUESTSTATUS_PENDING
//          	      AND (UPPER(MERCHANT.MERCH_BUSINESS_NAME)=UPPER(:merch_business_name) OR UPPER(MERCHANT.MERCH_LEGAL_NAME)=UPPER(:merch_business_name))
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                MATCH_REQUESTS.MATCH_SEQ_NUM\n        FROM    \n                MERCHANT,MATCH_REQUESTS\n        WHERE   \n                MERCHANT.APP_SEQ_NUM=MATCH_REQUESTS.APP_SEQ_NUM\n        \t      AND MATCH_REQUESTS.REQUEST_STATUS= :1 \n        \t      AND (UPPER(MERCHANT.MERCH_BUSINESS_NAME)=UPPER( :2 ) OR UPPER(MERCHANT.MERCH_LEGAL_NAME)=UPPER( :3 ))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,MATCHDataQueueItem.MATCHREQUESTSTATUS_PENDING);
   __sJT_st.setString(2,merch_business_name);
   __sJT_st.setString(3,merch_business_name);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:310^7*/
      ResultSet rs = rsItr.getResultSet();
      
      int numRecs = 0;
      StringBuffer sb = new StringBuffer(64);
      while(rs.next()) {
        if(++numRecs>1)
          break;
        sb.append(rs.getString(1));
      }
      
      rs.close();
      rsItr.close();
      
      if(numRecs==0)
        throw new Exception("No matching Pending MATCH Requests found for Merchant Business Name '"+merch_business_name+"'");
      if(numRecs>1)
        throw new Exception("More than one Pending MATCH Request found for Merchant Business Name '"+merch_business_name+"'");
      
      return sb.toString();
    
    }
    catch(Exception e) {
      log.error(e.getMessage());
      return "";
    }
    finally {
      cleanUp();
    }
  }
   
  
  /**
   * getRequestIDsByKey()
   *
   * Reteives all MATCH request IDs associated with the merchant of the given merchant number.
   * 
   * @param       key         DBQ_KEY_{...}
   * @param       val         Value of the key
   * 
   * @return      String array of Match request IDs
   */
  public String[] getRequestIDsByKey(int key, String val)
    throws Exception
  {
    try {
      
      connect();
    
      ResultSetIterator rsItr = null;
      
      switch(key) {
        case DBQ_KEY_APP_SEQ_NUM:
          /*@lineinfo:generated-code*//*@lineinfo:363^11*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                      MATCH_REQUESTS.MATCH_SEQ_NUM
//              FROM    
//                      MERCHANT,MATCH_REQUESTS
//              WHERE   
//                      MERCHANT.APP_SEQ_NUM=MATCH_REQUESTS.APP_SEQ_NUM
//              	      AND MERCHANT.APP_SEQ_NUM=:val
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                    MATCH_REQUESTS.MATCH_SEQ_NUM\n            FROM    \n                    MERCHANT,MATCH_REQUESTS\n            WHERE   \n                    MERCHANT.APP_SEQ_NUM=MATCH_REQUESTS.APP_SEQ_NUM\n            \t      AND MERCHANT.APP_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,val);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:372^11*/
          break;
        case DBQ_KEY_MERCNUMBER:
          /*@lineinfo:generated-code*//*@lineinfo:375^11*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                      MATCH_REQUESTS.MATCH_SEQ_NUM
//              FROM    
//                      MERCHANT,MATCH_REQUESTS
//              WHERE   
//                      MERCHANT.APP_SEQ_NUM=MATCH_REQUESTS.APP_SEQ_NUM
//              	      AND MERCHANT.MERCH_NUMBER=:val
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                    MATCH_REQUESTS.MATCH_SEQ_NUM\n            FROM    \n                    MERCHANT,MATCH_REQUESTS\n            WHERE   \n                    MERCHANT.APP_SEQ_NUM=MATCH_REQUESTS.APP_SEQ_NUM\n            \t      AND MERCHANT.MERCH_NUMBER= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,val);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:384^11*/
          break;
        case DBQ_KEY_MERCCNTRLNUM:
          /*@lineinfo:generated-code*//*@lineinfo:387^11*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                      MATCH_REQUESTS.MATCH_SEQ_NUM
//              FROM    
//                      MERCHANT,MATCH_REQUESTS
//              WHERE   
//                      MERCHANT.APP_SEQ_NUM=MATCH_REQUESTS.APP_SEQ_NUM
//              	      AND MERCHANT.MERC_CNTRL_NUMBER=:val
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                    MATCH_REQUESTS.MATCH_SEQ_NUM\n            FROM    \n                    MERCHANT,MATCH_REQUESTS\n            WHERE   \n                    MERCHANT.APP_SEQ_NUM=MATCH_REQUESTS.APP_SEQ_NUM\n            \t      AND MERCHANT.MERC_CNTRL_NUMBER= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,val);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:396^11*/
          break;
        default:
          throw new Exception("Unhandled key '"+key+"' attempting to retreive MATCH request IDs by key.  Aborted.");
      }
      
      ResultSet rs = rsItr.getResultSet();
      
      int numRecs = 0;
      StringBuffer sb = new StringBuffer(1024);
      while(rs.next()) {
        numRecs++;
        sb.append(',');
        sb.append(rs.getString(1));
      }
      if(numRecs>0)
        sb.deleteCharAt(0);
      
      rs.close();
      
      rsItr.close();
      
      String[] arrIDs = new String[numRecs];
      StringTokenizer st = new StringTokenizer(sb.toString(),",");
      for(int i=0;i<numRecs;i++)
        arrIDs[i]=st.nextToken();
        
      log.debug(arrIDs.length+" MATCH requests found to exist for key: '"+key+"' of value: '"+val+"'.");
      
      return arrIDs;
    
    }
    catch(Exception e) {
      log.error(e.getMessage());
      return new String[] { "" };
    }
    finally {
      cleanUp();
    }
  }
  
  
  /**
   * getMatchRequestIDsByQueueType()
   
   * Reteives all PK IDs by queue type
   * Used to load queues of a particular type
   * 
   * @return      String array of Match request IDs
   */
  private String[] getMatchRequestIDsByQueueType(int dbqType)
    throws Exception
  {
    try {
      
      connect();
    
      ResultSetIterator rsItr = null;
      
      switch(dbqType) {
        case DBQ_TYPE_INPUT:
          /*@lineinfo:generated-code*//*@lineinfo:457^11*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT          
//                              MATCH_SEQ_NUM
//              FROM            
//                              MATCH_REQUESTS
//              WHERE           
//                              (REQUEST_STATUS=:MATCHDataQueueItem.MATCHREQUESTSTATUS_NEW OR REQUEST_STATUS=:MATCHDataQueueItem.MATCHREQUESTSTATUS_RESUBMIT)
//                              --match_seq_num = 43992
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT          \n                            MATCH_SEQ_NUM\n            FROM            \n                            MATCH_REQUESTS\n            WHERE           \n                            (REQUEST_STATUS= :1  OR REQUEST_STATUS= :2 )\n                            --match_seq_num = 43992";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,MATCHDataQueueItem.MATCHREQUESTSTATUS_NEW);
   __sJT_st.setString(2,MATCHDataQueueItem.MATCHREQUESTSTATUS_RESUBMIT);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:466^11*/
          break;
        case DBQ_TYPE_OUTPUT:
          /*@lineinfo:generated-code*//*@lineinfo:469^11*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT          
//                              MATCH_SEQ_NUM
//              FROM            
//                              MATCH_REQUESTS
//              WHERE           
//                              (REQUEST_STATUS=:MATCHDataQueueItem.MATCHREQUESTSTATUS_COMPLETE)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT          \n                            MATCH_SEQ_NUM\n            FROM            \n                            MATCH_REQUESTS\n            WHERE           \n                            (REQUEST_STATUS= :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,MATCHDataQueueItem.MATCHREQUESTSTATUS_COMPLETE);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:477^11*/
          break;
        default:
          throw new Exception("DBQueueOps_MATCHDataQueueItem.getMatchRequestIDsByQueueType() exception: Unhandled queue type: '"+dbqType+"'.");
      }
      ResultSet rs = rsItr.getResultSet();
      
      int numRecs = 0;
      StringBuffer sb = new StringBuffer(1024);
      while(rs.next()) {
        numRecs++;
        sb.append(',');
        sb.append(rs.getString(1));
      }
      if(numRecs>0)
        sb.deleteCharAt(0);
      
      rs.close();
      
      rsItr.close();
      
      String[] arrIDs = new String[numRecs];
      StringTokenizer st = new StringTokenizer(sb.toString(),",");
      for(int i=0;i<numRecs;i++)
        arrIDs[i]=st.nextToken();
        
      log.debug(arrIDs.length+" MATCH requests found in db MATCH requests queue.");
      
      return arrIDs;
      
    }
    catch(Exception e) {
      log.error(e.getMessage());
      return new String[] { "" };
    }
    finally {
      cleanUp();
    }
    
  }
  
  
  /**
   * save()
   *  - SINGLE-RECORD save f()
   *  - saves given MATCH data item to db
   *  - presumes that corres. rec. already exists and therefore only updates
   *  - non-existant rec. (via ReqID) ==> error 
   *  - does NOT update data kept in MATCHDataQueueItem.MATCHDataQueueItemData rather only 
   *     'primary' MATCH data item data and assoc. mms ERRORs
   */
  public boolean save(MATCHDataQueueItem mdqi,int dbqType)
  {
    return save(mdqi,dbqType,false);
  }
  
  private boolean save(MATCHDataQueueItem mdqi,int dbqType,boolean bBatch)
  {
    try {
    
      connect();
      
      final long matchReqID = mdqi.getID().length()>0? Long.parseLong(mdqi.getID()):-1;
      long appSeqNum = mdqi.getGeneralDataItem("ID_GENERAL").length()>0? Long.parseLong(mdqi.getGeneralDataItem("ID_GENERAL")):-1;
      
      if(matchReqID<=0) {
        log.error("Unable to save MATCH data queue item: No App Seq Num specified.");
        return false;
      }
      
      log.info("Saving MATCH data item: ID: '"+matchReqID+"', Response: '"+mdqi.getResponseAction()+"', IM Response: '"+mdqi.getIMPResponse()+"'...");
      
      ResultSetIterator rsItr = null;
      
      // convert the java.util.Date to a java.sql.Timestamp
      java.sql.Timestamp sqlDate_start = new java.sql.Timestamp(mdqi.getProcessStartDate().getTime());
      java.sql.Timestamp sqlDate_end = new java.sql.Timestamp(mdqi.getProcessEndDate().getTime());

      // save main match request data
      switch(dbqType) 
      {
        case DBQ_TYPE_INPUT:
          /*@lineinfo:generated-code*//*@lineinfo:559^11*/

//  ************************************************************
//  #sql [Ctx] { UPDATE  
//                      MATCH_REQUESTS
//              SET     
//                      REQUEST_STATUS = :mdqi.getRequestStatus()
//                      ,PROCESS_START_DATE = :sqlDate_start
//                      ,REQUEST_COMMENTS = :mdqi.getRequestComments()
//                      ,IMP_RESPONSE = :mdqi.getIMPResponse()
//                      ,IMP_STATUS = :mdqi.getIMPStatus()
//              WHERE   
//                      MATCH_SEQ_NUM = :matchReqID
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1821 = mdqi.getRequestStatus();
 String __sJT_1822 = mdqi.getRequestComments();
 String __sJT_1823 = mdqi.getIMPResponse();
 String __sJT_1824 = mdqi.getIMPStatus();
   String theSqlTS = "UPDATE  \n                    MATCH_REQUESTS\n            SET     \n                    REQUEST_STATUS =  :1 \n                    ,PROCESS_START_DATE =  :2 \n                    ,REQUEST_COMMENTS =  :3 \n                    ,IMP_RESPONSE =  :4 \n                    ,IMP_STATUS =  :5 \n            WHERE   \n                    MATCH_SEQ_NUM =  :6";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1821);
   __sJT_st.setTimestamp(2,sqlDate_start);
   __sJT_st.setString(3,__sJT_1822);
   __sJT_st.setString(4,__sJT_1823);
   __sJT_st.setString(5,__sJT_1824);
   __sJT_st.setLong(6,matchReqID);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:571^11*/
          break;
          
        case DBQ_TYPE_OUTPUT:
          // ensure request status is not already marked completed
          String reqStat;
          /*@lineinfo:generated-code*//*@lineinfo:577^11*/

//  ************************************************************
//  #sql [Ctx] { select request_status
//              
//              from   match_requests
//              where  match_seq_num = :matchReqID
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select request_status\n             \n            from   match_requests\n            where  match_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,matchReqID);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   reqStat = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:583^11*/

          if( reqStat.equals(MATCHDataQueueItem.MATCHREQUESTSTATUS_COMPLETE) || 
              reqStat.equals(MATCHDataQueueItem.MATCHREQUESTSTATUS_RESPPARSEERROR) ) 
          {
            log.warn("Attempt to RE-save Match request '"+matchReqID+"'.  Aborting.");
            return true;  // no penalty
          }
          
          /*@lineinfo:generated-code*//*@lineinfo:592^11*/

//  ************************************************************
//  #sql [Ctx] { update  match_requests
//              set     request_status          = :mdqi.getRequestStatus(),
//                      process_end_date        = sysdate,
//                      response_action         = :mdqi.getResponseAction(),
//                      error_codes             = :mdqi.getErrorCodes(),
//                      mastercard_ref_number   = :mdqi.getMCRefNum(),
//                      visa_bin                = :mdqi.getVisaBin(),
//                      orig_mastercard_refnum  = :mdqi.getOrigMCRefNum()
//              where   match_seq_num = :matchReqID and
//                      request_status != 'C' -- not completed
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1825 = mdqi.getRequestStatus();
 String __sJT_1826 = mdqi.getResponseAction();
 String __sJT_1827 = mdqi.getErrorCodes();
 String __sJT_1828 = mdqi.getMCRefNum();
 String __sJT_1829 = mdqi.getVisaBin();
 String __sJT_1830 = mdqi.getOrigMCRefNum();
   String theSqlTS = "update  match_requests\n            set     request_status          =  :1 ,\n                    process_end_date        = sysdate,\n                    response_action         =  :2 ,\n                    error_codes             =  :3 ,\n                    mastercard_ref_number   =  :4 ,\n                    visa_bin                =  :5 ,\n                    orig_mastercard_refnum  =  :6 \n            where   match_seq_num =  :7  and\n                    request_status != 'C' -- not completed";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1825);
   __sJT_st.setString(2,__sJT_1826);
   __sJT_st.setString(3,__sJT_1827);
   __sJT_st.setString(4,__sJT_1828);
   __sJT_st.setString(5,__sJT_1829);
   __sJT_st.setString(6,__sJT_1830);
   __sJT_st.setLong(7,matchReqID);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:604^11*/
          break;
        
        case DBQ_TYPE_MERCHANT:
          // re-submit request?
          if( mdqi.isMarkedForResubmit() ) 
          {
            final Date now = new Date();
            java.sql.Timestamp sqlDate_now = new java.sql.Timestamp(now.getTime());
            java.sql.Timestamp sqlDate_nil = new java.sql.Timestamp(0);
          
            /*@lineinfo:generated-code*//*@lineinfo:615^13*/

//  ************************************************************
//  #sql [Ctx] { update  match_requests
//                set     request_status = :MATCHDataQueueItem.MATCHREQUESTSTATUS_RESUBMIT
//                        ,process_start_date     = :sqlDate_now
//                        ,process_end_date       = :sqlDate_nil
//                        ,response_action        = null
//                        ,imp_response           = null
//                        ,imp_status             = null
//                        ,error_codes            = null
//                        ,request_comments       = 'Resubmit.'
//                        ,mastercard_ref_number  = null
//                        ,visa_bin               = null
//                        ,orig_mastercard_refnum = null
//                where   match_seq_num = :matchReqID
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  match_requests\n              set     request_status =  :1 \n                      ,process_start_date     =  :2 \n                      ,process_end_date       =  :3 \n                      ,response_action        = null\n                      ,imp_response           = null\n                      ,imp_status             = null\n                      ,error_codes            = null\n                      ,request_comments       = 'Resubmit.'\n                      ,mastercard_ref_number  = null\n                      ,visa_bin               = null\n                      ,orig_mastercard_refnum = null\n              where   match_seq_num =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,MATCHDataQueueItem.MATCHREQUESTSTATUS_RESUBMIT);
   __sJT_st.setTimestamp(2,sqlDate_now);
   __sJT_st.setTimestamp(3,sqlDate_nil);
   __sJT_st.setLong(4,matchReqID);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:630^13*/
            
            /*@lineinfo:generated-code*//*@lineinfo:632^13*/

//  ************************************************************
//  #sql [Ctx] { delete
//                from    match_requests_responsedata
//                where   match_seq_num = :matchReqID            
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n              from    match_requests_responsedata\n              where   match_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,matchReqID);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:637^13*/
            
            // remove from all match queues!
            /*@lineinfo:generated-code*//*@lineinfo:640^13*/

//  ************************************************************
//  #sql [Ctx] { delete
//                from    app_queue
//                where   app_seq_num = :appSeqNum
//                        and app_queue_type = :com.mes.ops.QueueConstants.QUEUE_MATCH
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n              from    app_queue\n              where   app_seq_num =  :1 \n                      and app_queue_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,com.mes.ops.QueueConstants.QUEUE_MATCH);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:646^13*/
            
            // flagged for re-submission at this point
            // sync object with just persisted data
            mdqi.setRequestStatus(MATCHDataQueueItem.MATCHREQUESTSTATUS_RESUBMIT);
            mdqi.setProcessStartDate(now);
            mdqi.setProcessEndDate(new Date(0));
            mdqi.setResponseAction("");
            mdqi.setIMPResponse("");
            mdqi.setIMPStatus("");
            mdqi.setErrorCodes("");
            mdqi.setRequestComments("Resubmit.");
            mdqi.setMCRefNum("");
            mdqi.setVisaBin("");
            mdqi.setOrigMCRefNum("");
            mdqi.clearResponseItems();
            
            mdqi.clearResubmit();
          } 
          else  // not marked for re-submission
          {
            // update the response action
            /*@lineinfo:generated-code*//*@lineinfo:668^13*/

//  ************************************************************
//  #sql [Ctx] { update  match_requests
//                set     response_action = :mdqi.getResponseAction(),
//                        imp_response    = :mdqi.getIMPResponse()
//                where   match_seq_num = :matchReqID
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1831 = mdqi.getResponseAction();
 String __sJT_1832 = mdqi.getIMPResponse();
   String theSqlTS = "update  match_requests\n              set     response_action =  :1 ,\n                      imp_response    =  :2 \n              where   match_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1831);
   __sJT_st.setString(2,__sJT_1832);
   __sJT_st.setLong(3,matchReqID);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:674^13*/
          }
          break;
          
        default:
          throw new Exception("Unhandled MATCH data db queue type for update: "+dbqType+".  Update failed.");
      }
      
      // save related data (if any)
      if(true) 
      {
        log.debug("Saving "+mdqi.getNumResponseItems()+" Response MATCH records (ID: '"+matchReqID+"')...");
        
        Vector v = mdqi.getResponseItems();
        MATCHResponse mresp=null;
        long seqnum;
        StringBuffer sb = new StringBuffer(10*v.size());  // ensure big enough so buffer doesn't need to grow later
        
        for(int i=0;i<v.size();i++) 
        {
          mresp=(MATCHResponse)v.elementAt(i);
          if(mresp.isDirty()) 
          {
            if(mresp.getID().length()>0) 
            {
              log.debug("UPDATING MATCH Response record '"+mresp.getID()+"'.");
              // record presumed to exist
              /*@lineinfo:generated-code*//*@lineinfo:701^15*/

//  ************************************************************
//  #sql [Ctx] { UPDATE
//                          MATCH_REQUESTS_RESPONSEDATA
//                  SET
//                          GENERAL_RECORD=:mresp.getGeneralRecord()
//                          ,PRINCIPAL_RECORD=:mresp.getPrincipalRecord()
//                          ,ISACKNOWLEDGED=:mresp.isAcknowledged()
//                          ,IS_IMR=0
//                          ,IS_FROM_IMPBLACKLIST=0
//                  WHERE
//                          MRD_SEQ_NUM=:mresp.getID()
//                  
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1833 = mresp.getGeneralRecord();
 String __sJT_1834 = mresp.getPrincipalRecord();
 boolean __sJT_1835 = mresp.isAcknowledged();
 String __sJT_1836 = mresp.getID();
  try {
   String theSqlTS = "UPDATE\n                        MATCH_REQUESTS_RESPONSEDATA\n                SET\n                        GENERAL_RECORD= :1 \n                        ,PRINCIPAL_RECORD= :2 \n                        ,ISACKNOWLEDGED= :3 \n                        ,IS_IMR=0\n                        ,IS_FROM_IMPBLACKLIST=0\n                WHERE\n                        MRD_SEQ_NUM= :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1833);
   __sJT_st.setString(2,__sJT_1834);
   __sJT_st.setBoolean(3,__sJT_1835);
   __sJT_st.setString(4,__sJT_1836);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:714^15*/
              mresp.clean();
          
            } else {
              // record presumed to NOT exist
              // get unique acri pkid
              /*@lineinfo:generated-code*//*@lineinfo:720^15*/

//  ************************************************************
//  #sql [Ctx] { SELECT  MATCH_RESPONSE_DATA_SEQUENCE.nextval
//                  
//                  FROM    dual
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  MATCH_RESPONSE_DATA_SEQUENCE.nextval\n                 \n                FROM    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   seqnum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:725^15*/
              log.debug("INSERTING MATCH Response record '"+seqnum+"' binding to parent MATCH Request '"+mdqi.getID()+"'.");
              // add record
              /*@lineinfo:generated-code*//*@lineinfo:728^15*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO
//                          MATCH_REQUESTS_RESPONSEDATA
//                          
//                          (
//                            MRD_SEQ_NUM
//                            ,MATCH_SEQ_NUM
//                            ,GENERAL_RECORD
//                            ,PRINCIPAL_RECORD
//                            ,ISACKNOWLEDGED
//                            ,IS_IMR
//                            ,IS_FROM_IMPBLACKLIST
//                          )
//                  VALUES
//                          (
//                            :seqnum
//                            ,:matchReqID
//                            ,:mresp.getGeneralRecord()
//                            ,:mresp.getPrincipalRecord()
//                            ,:mresp.isAcknowledged()
//                            ,0
//                            ,0
//                          )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1837 = mresp.getGeneralRecord();
 String __sJT_1838 = mresp.getPrincipalRecord();
 boolean __sJT_1839 = mresp.isAcknowledged();
   String theSqlTS = "INSERT INTO\n                        MATCH_REQUESTS_RESPONSEDATA\n                        \n                        (\n                          MRD_SEQ_NUM\n                          ,MATCH_SEQ_NUM\n                          ,GENERAL_RECORD\n                          ,PRINCIPAL_RECORD\n                          ,ISACKNOWLEDGED\n                          ,IS_IMR\n                          ,IS_FROM_IMPBLACKLIST\n                        )\n                VALUES\n                        (\n                           :1 \n                          , :2 \n                          , :3 \n                          , :4 \n                          , :5 \n                          ,0\n                          ,0\n                        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,seqnum);
   __sJT_st.setLong(2,matchReqID);
   __sJT_st.setString(3,__sJT_1837);
   __sJT_st.setString(4,__sJT_1838);
   __sJT_st.setBoolean(5,__sJT_1839);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:752^15*/
              mresp.setID(Long.toString(seqnum));
              mresp.clean();
            }
          }
          
          // append id to IN clause for later use
          sb.append(',');
          sb.append(mresp.getID());
          
        }

        // IMPT: delete those recs not contained in items collection but in db
        // (dynamic query so have to use JDBC)
        log.debug("Cleansing related child response records for MATCH Request ID '"+mdqi.getID()+"'...");
        if(sb.length()>1) {
          
          final String inclause = sb.substring(1);  // remove preceeding ','
          log.debug("About to cleanse any orphaned MATCH Responses items not in: '"+inclause+"' for MATCH Request ID '"+mdqi.getID()+"'.");
          final String dynmcSQL = 
              "DELETE"
            +" FROM"
                    +" MATCH_REQUESTS_RESPONSEDATA"
            +" WHERE "
                    +" MATCH_SEQ_NUM="+matchReqID
                    +" AND IS_IMR=0"
                    +" AND NOT MRD_SEQ_NUM IN ("+inclause+")";
            
          PreparedStatement ps = getPreparedStatement(dynmcSQL);
          ps.executeQuery();
          ps.close();
        }
        
        // IMP Responses
        
        log.debug("Saving "+mdqi.getNumIMResponseItems()+" Response MATCH data (ID: '"+matchReqID+"')...");

        v = mdqi.getIMResponseItems();
        sb.delete(0,sb.length()); // reset
        
        for(int i=0;i<v.size();i++) {
          mresp=(IMResponse)v.elementAt(i);
          if(mresp.isDirty()) {
            log.debug("IMResponse '"+mresp.getID()+"' is dirty");
            int isFromImBlacklist = ((IMResponse)mresp).isFromImpBlacklist()? 1:0;
            if(mresp.getID().length()>0) {
              log.debug("UPDATING IMP Response record '"+mresp.getID()+"'.");
              // record presumed to exist
              /*@lineinfo:generated-code*//*@lineinfo:800^15*/

//  ************************************************************
//  #sql [Ctx] { UPDATE
//                          MATCH_REQUESTS_RESPONSEDATA
//                  SET
//                          GENERAL_RECORD=:mresp.getGeneralRecord()
//                          ,PRINCIPAL_RECORD=:mresp.getPrincipalRecord()
//                          ,ISACKNOWLEDGED=:mresp.isAcknowledged()
//                          ,IS_IMR=1
//                          ,IS_FROM_IMPBLACKLIST=:isFromImBlacklist
//                  WHERE
//                          MRD_SEQ_NUM=:mresp.getID()
//                  
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1840 = mresp.getGeneralRecord();
 String __sJT_1841 = mresp.getPrincipalRecord();
 boolean __sJT_1842 = mresp.isAcknowledged();
 String __sJT_1843 = mresp.getID();
  try {
   String theSqlTS = "UPDATE\n                        MATCH_REQUESTS_RESPONSEDATA\n                SET\n                        GENERAL_RECORD= :1 \n                        ,PRINCIPAL_RECORD= :2 \n                        ,ISACKNOWLEDGED= :3 \n                        ,IS_IMR=1\n                        ,IS_FROM_IMPBLACKLIST= :4 \n                WHERE\n                        MRD_SEQ_NUM= :5";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1840);
   __sJT_st.setString(2,__sJT_1841);
   __sJT_st.setBoolean(3,__sJT_1842);
   __sJT_st.setInt(4,isFromImBlacklist);
   __sJT_st.setString(5,__sJT_1843);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:813^15*/
              mresp.clean();
          
            } else {
              // record presumed to NOT exist
              // get unique acri pkid
              /*@lineinfo:generated-code*//*@lineinfo:819^15*/

//  ************************************************************
//  #sql [Ctx] { SELECT  MATCH_RESPONSE_DATA_SEQUENCE.nextval
//                  
//                  FROM    dual
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  MATCH_RESPONSE_DATA_SEQUENCE.nextval\n                 \n                FROM    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   seqnum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:824^15*/
              log.debug("INSERTING IMP Response record '"+seqnum+"' binding to parent MATCH Request '"+mdqi.getID()+"'.");
              // add record
              /*@lineinfo:generated-code*//*@lineinfo:827^15*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO
//                          MATCH_REQUESTS_RESPONSEDATA
//                          
//                          (
//                            MRD_SEQ_NUM
//                            ,MATCH_SEQ_NUM
//                            ,GENERAL_RECORD
//                            ,PRINCIPAL_RECORD
//                            ,ISACKNOWLEDGED
//                            ,IS_IMR
//                            ,IS_FROM_IMPBLACKLIST
//                          )
//                  VALUES
//                          (
//                            :seqnum
//                            ,:matchReqID
//                            ,:mresp.getGeneralRecord()
//                            ,:mresp.getPrincipalRecord()
//                            ,:mresp.isAcknowledged()
//                            ,1
//                            ,:((IMResponse)mresp).isFromImpBlacklist()
//                          )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1844 = mresp.getGeneralRecord();
 String __sJT_1845 = mresp.getPrincipalRecord();
 boolean __sJT_1846 = mresp.isAcknowledged();
 boolean __sJT_1847 = ((IMResponse)mresp).isFromImpBlacklist();
   String theSqlTS = "INSERT INTO\n                        MATCH_REQUESTS_RESPONSEDATA\n                        \n                        (\n                          MRD_SEQ_NUM\n                          ,MATCH_SEQ_NUM\n                          ,GENERAL_RECORD\n                          ,PRINCIPAL_RECORD\n                          ,ISACKNOWLEDGED\n                          ,IS_IMR\n                          ,IS_FROM_IMPBLACKLIST\n                        )\n                VALUES\n                        (\n                           :1 \n                          , :2 \n                          , :3 \n                          , :4 \n                          , :5 \n                          ,1\n                          , :6 \n                        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,seqnum);
   __sJT_st.setLong(2,matchReqID);
   __sJT_st.setString(3,__sJT_1844);
   __sJT_st.setString(4,__sJT_1845);
   __sJT_st.setBoolean(5,__sJT_1846);
   __sJT_st.setBoolean(6,__sJT_1847);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:851^15*/
              mresp.setID(Long.toString(seqnum));
              mresp.clean();
            }
          }
          
          // append id to IN clause for later use
          sb.append(',');
          sb.append(mresp.getID());
          
        }

        // IMPT: delete those recs not contained in items collection but in db
        // (dynamic query so have to use JDBC)
        log.debug("Cleansing related child response records for MATCH Request ID '"+mdqi.getID()+"'...");
        if(sb.length()>1) {
          
          final String inclause = sb.substring(1);  // remove preceeding ','
          log.debug("About to cleanse any orphaned IMP Responses items not in: '"+inclause+"' for MATCH Request ID '"+mdqi.getID()+"'.");
          final String dynmcSQL = 
              "DELETE"
            +" FROM"
                    +" MATCH_REQUESTS_RESPONSEDATA"
            +" WHERE "
                    +" MATCH_SEQ_NUM="+matchReqID
                    +" AND IS_IMR=1"
                    +" AND NOT MRD_SEQ_NUM IN ("+inclause+")";
            
          PreparedStatement ps = getPreparedStatement(dynmcSQL);
          ps.executeQuery();
          ps.close();
        }

      }
      
      if(dbqType == DBQ_TYPE_MERCHANT) {
        
        // upate responses

        Vector v = mdqi.getResponseItems();
        MATCHResponse mresp=null;

        for(int i=0;i<v.size();i++) {
          mresp=(MATCHResponse)v.elementAt(i);
          if(mresp.getID().length()>0 && mresp.isDirty()) {
            log.debug("UPDATING MATCH Response record '"+mresp.getID()+"' (Merchant).");
            // record presumed to exist
            /*@lineinfo:generated-code*//*@lineinfo:898^13*/

//  ************************************************************
//  #sql [Ctx] { UPDATE
//                        MATCH_REQUESTS_RESPONSEDATA
//                SET
//                        ISACKNOWLEDGED=:mresp.isAcknowledged()
//                WHERE
//                        MRD_SEQ_NUM=:mresp.getID()
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 boolean __sJT_1848 = mresp.isAcknowledged();
 String __sJT_1849 = mresp.getID();
  try {
   String theSqlTS = "UPDATE\n                      MATCH_REQUESTS_RESPONSEDATA\n              SET\n                      ISACKNOWLEDGED= :1 \n              WHERE\n                      MRD_SEQ_NUM= :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setBoolean(1,__sJT_1848);
   __sJT_st.setString(2,__sJT_1849);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:906^13*/
            mresp.clean();
          } else
            log.debug("Skipping MATCH Response record '"+mresp.getID()+"' (Merchant) because no ID or not dirty.");
        }
      
        v = mdqi.getIMResponseItems();
        
        for(int i=0;i<v.size();i++) {
          mresp=(IMResponse)v.elementAt(i);
          if(mresp.getID().length()>0 && mresp.isDirty()) {
            log.debug("UPDATING IMP Response record '"+mresp.getID()+"' (Merchant).");
            // record presumed to exist
            /*@lineinfo:generated-code*//*@lineinfo:919^13*/

//  ************************************************************
//  #sql [Ctx] { UPDATE
//                        MATCH_REQUESTS_RESPONSEDATA
//                SET
//                        ISACKNOWLEDGED=:mresp.isAcknowledged()
//                WHERE
//                        MRD_SEQ_NUM=:mresp.getID()
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 boolean __sJT_1850 = mresp.isAcknowledged();
 String __sJT_1851 = mresp.getID();
  try {
   String theSqlTS = "UPDATE\n                      MATCH_REQUESTS_RESPONSEDATA\n              SET\n                      ISACKNOWLEDGED= :1 \n              WHERE\n                      MRD_SEQ_NUM= :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setBoolean(1,__sJT_1850);
   __sJT_st.setString(2,__sJT_1851);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:927^13*/
            mresp.clean();
          } else
            log.debug("Skipping IMP Response record '"+mresp.getID()+"' (Merchant) because no ID or not dirty.");
        }

      }
      
      // ** queue logic **
      if(mdqi.getQueueAction()!=MATCHDataQueueItem.QUEUEACTION_UNDEFINED) {
        
        if(appSeqNum<0) {
            /*@lineinfo:generated-code*//*@lineinfo:939^13*/

//  ************************************************************
//  #sql [Ctx] { SELECT  APP_SEQ_NUM
//                
//                FROM
//                        MATCH_REQUESTS
//                WHERE
//                        MATCH_SEQ_NUM=:matchReqID
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  APP_SEQ_NUM\n               \n              FROM\n                      MATCH_REQUESTS\n              WHERE\n                      MATCH_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,matchReqID);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:947^13*/
        }
        
        log.debug("performing APP QUEUE OP (appSeqNum: '"+appSeqNum+"', MATCH Req ID: '"+matchReqID+"')...");
        if(doAppQueueOp(mdqi.getQueueAction(),matchReqID,appSeqNum))
          mdqi.setQueueAction(MATCHDataQueueItem.QUEUEACTION_UNDEFINED);
      }
          
      // commit if NOT in batch
      if(!bBatch)
        commit();
      
      return true;
    }
    catch(Exception e) {
      log.error(e.getMessage());
      //log.debug("Exception occurred attempting to save MATCH data item: ID: '"+mdqi.getID()+"': \n\n"+mdqi.toString()+"\n\n");
      log.debug("Exception occurred attempting to save MATCH data item: ID: '"+mdqi.getID());
      return false;
    }
    finally {
      cleanUp();
    }

  }
  
  public boolean isAppDeclined(long appSeqNum)
  {
    try {
        
      connect();
      
      int count=0;
      
      /*@lineinfo:generated-code*//*@lineinfo:981^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(*)
//          
//          from    app_queue
//          where   app_seq_num = :appSeqNum
//                  AND app_queue_type = :com.mes.ops.QueueConstants.QUEUE_CREDIT
//                  AND app_queue_stage = :com.mes.ops.QueueConstants.Q_CREDIT_DECLINE
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)\n         \n        from    app_queue\n        where   app_seq_num =  :1 \n                AND app_queue_type =  :2 \n                AND app_queue_stage =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,com.mes.ops.QueueConstants.QUEUE_CREDIT);
   __sJT_st.setInt(3,com.mes.ops.QueueConstants.Q_CREDIT_DECLINE);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:989^7*/

      return (count>0);
    }
    catch(Exception e) {
      log.error("isAppDeclined() EXCEPTION: (app seq num '"+appSeqNum+"'): '"+e.getMessage()+"'.");
      return false;
    }
    finally {
      cleanUp();
    }
  }
  
  public boolean doAppQueueOp(int queueAction,long matchReqID,long appSeqNum)
  {
    try {
        
      connect();
      
      java.sql.Timestamp sqlDate_now = new java.sql.Timestamp(new Date().getTime());
      int count=0;
      
      // only add to [internal] match credit or match error queues if NOT already declined
      if((queueAction==MATCHDataQueueItem.QUEUEACTION_ADDTOMATCHCREDIT || queueAction==MATCHDataQueueItem.QUEUEACTION_ADDTOMATCHERROR || queueAction==MATCHDataQueueItem.QUEUEACTION_ADDTOINTERNALMATCHCREDIT)
          && isAppDeclined(appSeqNum))
      {
        log.warn("Attempt to perform App Queue Op '"+queueAction+"' halted because app '"+appSeqNum+"' found Declined.");
        return true;  // already declined so no-op
      }
      
      if(queueAction==MATCHDataQueueItem.QUEUEACTION_ADDTOMATCHCREDIT) {
        
        // new queue methodology (not fully implemented)
        /*
        QueueTools.insertQueue(appSeqNum,com.mes.constants.MesQueues.Q_MATCHCREDIT);
        log.info("Added Application '"+appSeqNum+"' (MATCH Request '"+matchReqID+"') to Credit-MATCH holding queue.");
        */
        
        com.mes.ops.AddQueueBean aqb = new com.mes.ops.AddQueueBean();
        
        // old queue methodology
        aqb.connect();
        if(!aqb.insertQueue(
          appSeqNum
          ,com.mes.ops.QueueConstants.QUEUE_MATCH
          ,com.mes.ops.QueueConstants.Q_MATCH_PEND
          ,com.mes.ops.QueueConstants.Q_STATUS_NONE
          ,"SYSTEM"
          ,-1
          ,"SYSTEM"))
        {
          log.error("Attempt to add app '"+appSeqNum+"' to MATCH Pending queue failed!");
          return false;
        }
        aqb.cleanUp();
      
      } else if(queueAction==MATCHDataQueueItem.QUEUEACTION_ADDTOINTERNALMATCHCREDIT) {
        
        com.mes.ops.AddQueueBean aqb = new com.mes.ops.AddQueueBean();
        
        // old queue methodology
        aqb.connect();
        if(!aqb.insertQueue(
          appSeqNum
          ,com.mes.ops.QueueConstants.QUEUE_INTERNALMATCH
          ,com.mes.ops.QueueConstants.Q_INTERNALMATCH_PEND
          ,com.mes.ops.QueueConstants.Q_STATUS_NONE
          ,"SYSTEM"
          ,-1
          ,"SYSTEM"))
        {
          log.error("Attempt to add app '"+appSeqNum+"' to Internal Match queue failed!");
          return false;
        }
        aqb.cleanUp();
      
      } else if(queueAction==MATCHDataQueueItem.QUEUEACTION_ADDTOMATCHERROR) {
        
        final com.mes.ops.AddQueueBean aqb = new com.mes.ops.AddQueueBean();
        
        // old queue methodology
        aqb.connect();
        if(!aqb.insertQueue(
          appSeqNum
          ,com.mes.ops.QueueConstants.QUEUE_MATCH
          ,com.mes.ops.QueueConstants.Q_MATCH_ERROR
          ,com.mes.ops.QueueConstants.Q_STATUS_NONE
          ,"SYSTEM"
          ,-1
          ,"SYSTEM"))
        {
          log.error("Attempt to add app '"+appSeqNum+"' to MATCH Error queue failed!");
          aqb.cleanUp();
          return false;
        }
        aqb.cleanUp();
      
      } else if(queueAction==MATCHDataQueueItem.QUEUEACTION_DECLINE) {
        
        // old queue methodology
        /*@lineinfo:generated-code*//*@lineinfo:1089^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(*)
//            
//            from    app_queue
//            where   app_seq_num = :appSeqNum
//                    AND app_queue_type = :com.mes.ops.QueueConstants.QUEUE_CREDIT
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)\n           \n          from    app_queue\n          where   app_seq_num =  :1 \n                  AND app_queue_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,com.mes.ops.QueueConstants.QUEUE_CREDIT);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1096^9*/
        
        if(count>0) {
          // app still in credit so simply remove from pending match queue
          /*@lineinfo:generated-code*//*@lineinfo:1100^11*/

//  ************************************************************
//  #sql [Ctx] { DELETE
//              FROM    app_queue
//              where   app_seq_num = :appSeqNum
//                      AND (
//                               (app_queue_type = :com.mes.ops.QueueConstants.QUEUE_MATCH AND app_queue_stage = :com.mes.ops.QueueConstants.Q_MATCH_PEND)
//                            OR (app_queue_type = :com.mes.ops.QueueConstants.QUEUE_INTERNALMATCH AND app_queue_stage = :com.mes.ops.QueueConstants.Q_INTERNALMATCH_PEND)
//                          )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "DELETE\n            FROM    app_queue\n            where   app_seq_num =  :1 \n                    AND (\n                             (app_queue_type =  :2  AND app_queue_stage =  :3 )\n                          OR (app_queue_type =  :4  AND app_queue_stage =  :5 )\n                        )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,com.mes.ops.QueueConstants.QUEUE_MATCH);
   __sJT_st.setInt(3,com.mes.ops.QueueConstants.Q_MATCH_PEND);
   __sJT_st.setInt(4,com.mes.ops.QueueConstants.QUEUE_INTERNALMATCH);
   __sJT_st.setInt(5,com.mes.ops.QueueConstants.Q_INTERNALMATCH_PEND);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1109^11*/
        } else {
          // app not in credit
          /*@lineinfo:generated-code*//*@lineinfo:1112^11*/

//  ************************************************************
//  #sql [Ctx] { UPDATE
//                      APP_QUEUE
//              SET
//                      APP_QUEUE_TYPE=:com.mes.ops.QueueConstants.QUEUE_CREDIT
//                      ,APP_QUEUE_STAGE=:com.mes.ops.QueueConstants.Q_CREDIT_DECLINE
//                      ,APP_STATUS=:com.mes.ops.QueueConstants.Q_STATUS_DECLINED
//                      ,APP_STATUS_REASON='Exact match deemed from MATCH Inquiry data.'
//                      ,APP_USER=-1
//                      ,APP_SOURCE=''
//                      ,APP_LAST_DATE=:sqlDate_now
//                      ,APP_LAST_USER='SYSTEM'
//                      ,APP_QUEUE_LOCK=''
//              WHERE
//                      app_seq_num = :appSeqNum
//                      AND app_queue_type = :com.mes.ops.QueueConstants.QUEUE_MATCH
//                      AND app_queue_stage = :com.mes.ops.QueueConstants.Q_MATCH_PEND
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "UPDATE\n                    APP_QUEUE\n            SET\n                    APP_QUEUE_TYPE= :1 \n                    ,APP_QUEUE_STAGE= :2 \n                    ,APP_STATUS= :3 \n                    ,APP_STATUS_REASON='Exact match deemed from MATCH Inquiry data.'\n                    ,APP_USER=-1\n                    ,APP_SOURCE=''\n                    ,APP_LAST_DATE= :4 \n                    ,APP_LAST_USER='SYSTEM'\n                    ,APP_QUEUE_LOCK=''\n            WHERE\n                    app_seq_num =  :5 \n                    AND app_queue_type =  :6 \n                    AND app_queue_stage =  :7";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,com.mes.ops.QueueConstants.QUEUE_CREDIT);
   __sJT_st.setInt(2,com.mes.ops.QueueConstants.Q_CREDIT_DECLINE);
   __sJT_st.setInt(3,com.mes.ops.QueueConstants.Q_STATUS_DECLINED);
   __sJT_st.setTimestamp(4,sqlDate_now);
   __sJT_st.setLong(5,appSeqNum);
   __sJT_st.setInt(6,com.mes.ops.QueueConstants.QUEUE_MATCH);
   __sJT_st.setInt(7,com.mes.ops.QueueConstants.Q_MATCH_PEND);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1130^11*/
        }
        
      } else if(queueAction==MATCHDataQueueItem.QUEUEACTION_RELEASE) {  // (relinquish)
        
        // old queue methodology
        /*@lineinfo:generated-code*//*@lineinfo:1136^9*/

//  ************************************************************
//  #sql [Ctx] { DELETE
//            FROM    app_queue
//            WHERE   app_seq_num = :appSeqNum
//                    AND app_queue_type = :com.mes.ops.QueueConstants.QUEUE_MATCH
//                    AND (app_queue_stage = :com.mes.ops.QueueConstants.Q_MATCH_PEND
//                      OR app_queue_stage = :com.mes.ops.QueueConstants.Q_MATCH_ERROR)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "DELETE\n          FROM    app_queue\n          WHERE   app_seq_num =  :1 \n                  AND app_queue_type =  :2 \n                  AND (app_queue_stage =  :3 \n                    OR app_queue_stage =  :4 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,com.mes.ops.QueueConstants.QUEUE_MATCH);
   __sJT_st.setInt(3,com.mes.ops.QueueConstants.Q_MATCH_PEND);
   __sJT_st.setInt(4,com.mes.ops.QueueConstants.Q_MATCH_ERROR);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1144^9*/

      } else if(queueAction==MATCHDataQueueItem.QUEUEACTION_IMRELEASE) {
        
        // old queue methodology
        /*@lineinfo:generated-code*//*@lineinfo:1149^9*/

//  ************************************************************
//  #sql [Ctx] { DELETE
//            FROM    app_queue
//            WHERE   app_seq_num = :appSeqNum
//                    AND app_queue_type = :com.mes.ops.QueueConstants.QUEUE_INTERNALMATCH
//                    AND (app_queue_stage = :com.mes.ops.QueueConstants.Q_INTERNALMATCH_PEND)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "DELETE\n          FROM    app_queue\n          WHERE   app_seq_num =  :1 \n                  AND app_queue_type =  :2 \n                  AND (app_queue_stage =  :3 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,com.mes.ops.QueueConstants.QUEUE_INTERNALMATCH);
   __sJT_st.setInt(3,com.mes.ops.QueueConstants.Q_INTERNALMATCH_PEND);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1156^9*/
      
      } else
        throw new Exception("Unsupported queue action '"+queueAction+"'.");
        
      log.debug("doAppQueueOp() - (appSeqNum '"+appSeqNum+"', matchReqID '"+matchReqID+"') SUCCESSFUL");
      return true;
      
    }
    catch(Exception e) {
      log.error("Exception occurred attempting to do app queue op for app seq num '"+appSeqNum+"': '"+e.getMessage()+"'.");
      return false;
    }
    finally {
      cleanUp();
    }
  }
  
  /**
   * loadIMResponseMerchant()
   *
   * Loads merchant data into an IMResponse object 
   * in the same way as it is loaded into a MATCH request.
   */
  public boolean loadIMResponseMerchantBlacklist(String appSeqNum,IMResponse imr)
  {
    return loadIMResponseMerchant(appSeqNum, imr, true);
  }
  public boolean loadIMResponseMerchant(String appSeqNum,IMResponse imr)
  {
    return loadIMResponseMerchant(appSeqNum, imr, false);
  }
  private boolean loadIMResponseMerchant(String id,IMResponse imr,boolean bFromBlacklist)
  {
    boolean rval = true;

    try {
        
      connect();
      
      ResultSet         rs        = null;
      ResultSetIterator it        = null;
      int               numPrecs  = 0;

      final int matchdtaType = DBQ_MATCHDATA_ALL; // currently, retreive all merchant data for an IM response

      // general data (1 record only)
      if(matchdtaType==DBQ_MATCHDATA_GENERAL || matchdtaType==DBQ_MATCHDATA_ALL) {
      
        if(bFromBlacklist) {
          /*@lineinfo:generated-code*//*@lineinfo:1206^11*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//            	      m.MERCH_BUSINESS_NAME
//            	      ,m.MERCH_BUSINESS_DBA
//            	      ,m.MERCH_STATETAX_ID
//            	      ,m.MERCH_FEDERAL_TAX_ID
//            	      ,m.MERCH_WEB_URL
//            	      ,m.MAIL_ADDRESS1
//            	      ,m.MAIL_ADDRESS2
//            	      ,m.MAIL_CITY
//            	      ,m.MAIL_STATE
//            	      ,m.MAIL_ZIP
//            	      ,m.MAIL_COUNTRY
//            	      ,m.BUSINESS_PHONE
//            	      ,m.DDA
//  
//              FROM
//            	      MATCH_IMP_BLACKLIST m
//                
//              WHERE
//                    m.mib_seq_num = :id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n          \t      m.MERCH_BUSINESS_NAME\n          \t      ,m.MERCH_BUSINESS_DBA\n          \t      ,m.MERCH_STATETAX_ID\n          \t      ,m.MERCH_FEDERAL_TAX_ID\n          \t      ,m.MERCH_WEB_URL\n          \t      ,m.MAIL_ADDRESS1\n          \t      ,m.MAIL_ADDRESS2\n          \t      ,m.MAIL_CITY\n          \t      ,m.MAIL_STATE\n          \t      ,m.MAIL_ZIP\n          \t      ,m.MAIL_COUNTRY\n          \t      ,m.BUSINESS_PHONE\n          \t      ,m.DDA\n\n            FROM\n          \t      MATCH_IMP_BLACKLIST m\n              \n            WHERE\n                  m.mib_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"31com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1228^11*/
        } else {
          /*@lineinfo:generated-code*//*@lineinfo:1230^11*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//            	       MERCHANT.MERCH_NUMBER
//            	      ,MERCHANT.APP_SEQ_NUM ID_GENERAL
//            	      ,MERCHANT.MERCH_APPLICATION_TYPE
//            	      ,MERCHANT.MERCH_LEGAL_NAME MERCH_BUSINESS_NAME
//            	      ,MERCHANT.MERCH_BUSINESS_NAME MERCH_BUSINESS_DBA
//            	      ,MERCHANT.MERCH_CAT_CODE
//            	      ,MERCHANT.MERCH_DATE_OPENED
//            	      ,MERCHANT.MERCH_STATETAX_ID
//            	      ,MERCHANT.MERCH_FEDERAL_TAX_ID
//            	      ,MERCHANT.MERCH_WEB_URL
//            	      ,ADRBUSINESS.ADDRESS_LINE1 MAIL_ADDRESS1
//            	      ,ADRBUSINESS.ADDRESS_LINE2 MAIL_ADDRESS2
//            	      ,ADRBUSINESS.ADDRESS_CITY MAIL_CITY
//            	      ,ADRBUSINESS.COUNTRYSTATE_CODE MAIL_STATE
//            	      ,ADRBUSINESS.ADDRESS_ZIP MAIL_ZIP
//            	      ,ADRBUSINESS.COUNTRY_CODE MAIL_COUNTRY
//            	      ,ADRBUSINESS.ADDRESS_PHONE BUSINESS_PHONE
//            	      ,MB.MERCHBANK_ACCT_NUM DDA
//  
//              FROM
//            	      MERCHANT    MERCHANT
//            	      ,ADDRESS    ADRBUSINESS
//                    ,MERCHBANK  MB
//                
//              WHERE
//            		    (MERCHANT.APP_SEQ_NUM=ADRBUSINESS.APP_SEQ_NUM(+) AND ADRBUSINESS.ADDRESSTYPE_CODE(+) = 1)
//                    AND MERCHANT.APP_SEQ_NUM=MB.APP_SEQ_NUM(+)
//                    AND MERCHANT.APP_SEQ_NUM=:id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n          \t       MERCHANT.MERCH_NUMBER\n          \t      ,MERCHANT.APP_SEQ_NUM ID_GENERAL\n          \t      ,MERCHANT.MERCH_APPLICATION_TYPE\n          \t      ,MERCHANT.MERCH_LEGAL_NAME MERCH_BUSINESS_NAME\n          \t      ,MERCHANT.MERCH_BUSINESS_NAME MERCH_BUSINESS_DBA\n          \t      ,MERCHANT.MERCH_CAT_CODE\n          \t      ,MERCHANT.MERCH_DATE_OPENED\n          \t      ,MERCHANT.MERCH_STATETAX_ID\n          \t      ,MERCHANT.MERCH_FEDERAL_TAX_ID\n          \t      ,MERCHANT.MERCH_WEB_URL\n          \t      ,ADRBUSINESS.ADDRESS_LINE1 MAIL_ADDRESS1\n          \t      ,ADRBUSINESS.ADDRESS_LINE2 MAIL_ADDRESS2\n          \t      ,ADRBUSINESS.ADDRESS_CITY MAIL_CITY\n          \t      ,ADRBUSINESS.COUNTRYSTATE_CODE MAIL_STATE\n          \t      ,ADRBUSINESS.ADDRESS_ZIP MAIL_ZIP\n          \t      ,ADRBUSINESS.COUNTRY_CODE MAIL_COUNTRY\n          \t      ,ADRBUSINESS.ADDRESS_PHONE BUSINESS_PHONE\n          \t      ,MB.MERCHBANK_ACCT_NUM DDA\n\n            FROM\n          \t      MERCHANT    MERCHANT\n          \t      ,ADDRESS    ADRBUSINESS\n                  ,MERCHBANK  MB\n              \n            WHERE\n          \t\t    (MERCHANT.APP_SEQ_NUM=ADRBUSINESS.APP_SEQ_NUM(+) AND ADRBUSINESS.ADDRESSTYPE_CODE(+) = 1)\n                  AND MERCHANT.APP_SEQ_NUM=MB.APP_SEQ_NUM(+)\n                  AND MERCHANT.APP_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"32com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1261^11*/
        }
        rs = it.getResultSet();
        if(!rs.next()) {
          rval=false;
          log.error("IM Response load General data failed: No general record found for App Seq Num '"+id+"'.");
        } else if(!dbRecToObject(DBQ_MATCHDATA_GENERAL,rs,imr)) {
          rval=false;
          log.error("IM Response load General data failed: Error converting rs-->obj for App Seq Num '"+id+"'.");
        }
      
        rs.close();
        it.close();
      }
    
      // principal data (0-n possible records)
      if(matchdtaType==DBQ_MATCHDATA_PRINCIPAL || matchdtaType==DBQ_MATCHDATA_ALL) {
        if(bFromBlacklist) {
          /*@lineinfo:generated-code*//*@lineinfo:1279^11*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT 
//            	      1 ID_PRINCIPAL
//            	     ,m.BUSOWNER_LAST_NAME
//            	     ,m.BUSOWNER_FIRST_NAME
//            	     ,m.BUSOWNER_NAME_INITIAL
//            	     ,m.BUSOWNER_SSN
//            	     ,m.BUSOWNER_DRVLICNUM
//            	     ,m.MAILADR_LINE1
//            	     ,m.MAILADR_LINE2
//            	     ,m.MAILADR_CITY
//            	     ,m.MAILADR_STATE
//            	     ,m.MAILADR_ZIP
//            	     ,m.MAILADR_COUNTRYCODE
//            	     ,m.MAILADR_PHONE
//  
//              FROM
//            	      MATCH_IMP_BLACKLIST m
//                
//              WHERE
//                    m.mib_seq_num = :id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT \n          \t      1 ID_PRINCIPAL\n          \t     ,m.BUSOWNER_LAST_NAME\n          \t     ,m.BUSOWNER_FIRST_NAME\n          \t     ,m.BUSOWNER_NAME_INITIAL\n          \t     ,m.BUSOWNER_SSN\n          \t     ,m.BUSOWNER_DRVLICNUM\n          \t     ,m.MAILADR_LINE1\n          \t     ,m.MAILADR_LINE2\n          \t     ,m.MAILADR_CITY\n          \t     ,m.MAILADR_STATE\n          \t     ,m.MAILADR_ZIP\n          \t     ,m.MAILADR_COUNTRYCODE\n          \t     ,m.MAILADR_PHONE\n\n            FROM\n          \t      MATCH_IMP_BLACKLIST m\n              \n            WHERE\n                  m.mib_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"33com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1301^11*/
        } else {
          /*@lineinfo:generated-code*//*@lineinfo:1303^11*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT 
//            	      BUSINESSOWNER.BUSOWNER_NUM ID_PRINCIPAL
//            	     ,BUSINESSOWNER.BUSOWNER_LAST_NAME
//            	     ,BUSINESSOWNER.BUSOWNER_FIRST_NAME
//            	     ,BUSINESSOWNER.BUSOWNER_NAME_INITIAL
//            	     ,BUSINESSOWNER.BUSOWNER_SSN
//            	     ,BUSINESSOWNER.BUSOWNER_DRVLICNUM
//            	     ,ADDRESS.ADDRESS_LINE1 MAILADR_LINE1
//            	     ,ADDRESS.ADDRESS_LINE2 MAILADR_LINE2
//            	     ,ADDRESS.ADDRESS_CITY MAILADR_CITY
//            	     ,ADDRESS.COUNTRYSTATE_CODE MAILADR_STATE
//            	     ,ADDRESS.ADDRESS_ZIP MAILADR_ZIP
//            	     ,ADDRESS.COUNTRY_CODE MAILADR_COUNTRYCODE
//            	     ,ADDRESS.ADDRESS_PHONE MAILADR_PHONE
//  
//              FROM
//            	      BUSINESSOWNER,ADDRESS
//              WHERE
//            	 	    (BUSINESSOWNER.APP_SEQ_NUM=ADDRESS.APP_SEQ_NUM(+) AND ADDRESS.ADDRESSTYPE_CODE(+)=2)
//                    AND BUSINESSOWNER.APP_SEQ_NUM=:id
//                    AND LENGTH(BUSOWNER_LAST_NAME)>0
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT \n          \t      BUSINESSOWNER.BUSOWNER_NUM ID_PRINCIPAL\n          \t     ,BUSINESSOWNER.BUSOWNER_LAST_NAME\n          \t     ,BUSINESSOWNER.BUSOWNER_FIRST_NAME\n          \t     ,BUSINESSOWNER.BUSOWNER_NAME_INITIAL\n          \t     ,BUSINESSOWNER.BUSOWNER_SSN\n          \t     ,BUSINESSOWNER.BUSOWNER_DRVLICNUM\n          \t     ,ADDRESS.ADDRESS_LINE1 MAILADR_LINE1\n          \t     ,ADDRESS.ADDRESS_LINE2 MAILADR_LINE2\n          \t     ,ADDRESS.ADDRESS_CITY MAILADR_CITY\n          \t     ,ADDRESS.COUNTRYSTATE_CODE MAILADR_STATE\n          \t     ,ADDRESS.ADDRESS_ZIP MAILADR_ZIP\n          \t     ,ADDRESS.COUNTRY_CODE MAILADR_COUNTRYCODE\n          \t     ,ADDRESS.ADDRESS_PHONE MAILADR_PHONE\n\n            FROM\n          \t      BUSINESSOWNER,ADDRESS\n            WHERE\n          \t \t    (BUSINESSOWNER.APP_SEQ_NUM=ADDRESS.APP_SEQ_NUM(+) AND ADDRESS.ADDRESSTYPE_CODE(+)=2)\n                  AND BUSINESSOWNER.APP_SEQ_NUM= :1 \n                  AND LENGTH(BUSOWNER_LAST_NAME)>0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"34com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1326^11*/
        }
        rs = it.getResultSet();
      
        // principal data (0..n)
        while(rs.next()) {
          if(dbRecToObject(DBQ_MATCHDATA_PRINCIPAL,rs,imr))
            numPrecs++;
          else
            log.warn("IM Response load Principal record failed:  (App Seq Num: '"+id+"').  Principal record skipped.");
        }

        rs.close();
        it.close();
      }
      if(numPrecs==0)
        log.warn("NO IM Response PRINCIPAL DATA LOADED for App Seq Num '"+id+"'.");
    
      log.debug("-->loadIMResponseMerchant() - numPrecs="+numPrecs);
    }
    catch(Exception e) {
      log.error("loadIMResponseMerchant() EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
    finally {
      cleanUp();
    }

    return rval;
  }
  
  
  /**
   * load() - SINGLE
   *
   * @param   MRID    Match Request ID
   */
  public boolean load(String MRID,int matchdtaType,MATCHDataQueueItem mdqi)
  {
    try {
        
      connect();
      
      if(MRID==null || MRID.length()==0) {
        log.error("Unable to load MATCH data queue item: No ID specified.");
        return false;
      }
    
      log.debug("MRID="+MRID);

      ResultSet         rs = null;
      ResultSetIterator it = null;
      boolean bLoad=true;
      int numPrecs = 0, numMDrecs = 0;
      
      // match request data (allow for 0-n for each APP_SEQ_NUM (merchant))
      /*@lineinfo:generated-code*//*@lineinfo:1382^7*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT 
//                  :MRID ID_MATCHREQUEST
//            	   	,MATCH_REQUESTS.APP_SEQ_NUM ID_GENERAL
//                  ,MATCH_REQUESTS.PROCESS_START_DATE
//                  ,MATCH_REQUESTS.PROCESS_END_DATE
//                  ,MATCH_REQUESTS.REQUEST_STATUS
//                  ,MATCH_REQUESTS.REQUEST_ACTION
//                  ,MATCH_REQUESTS.RESPONSE_ACTION
//                  ,MATCH_REQUESTS.IMP_RESPONSE
//                  ,MATCH_REQUESTS.IMP_STATUS
//                  ,MATCH_REQUESTS.REQUEST_REASON_CODE
//                  ,MATCH_REQUESTS.REQUEST_COMMENTS
//                  ,MATCH_REQUESTS.ERROR_CODES
//                  ,MATCH_REQUESTS.MASTERCARD_REF_NUMBER
//                  ,MATCH_REQUESTS.VISA_BIN
//                  ,MATCH_REQUESTS.ORIG_MASTERCARD_REFNUM
//          FROM    
//                  MATCH_REQUESTS
//          WHERE   
//                  MATCH_REQUESTS.MATCH_SEQ_NUM=:MRID
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT \n                 :1  ID_MATCHREQUEST\n          \t   \t,MATCH_REQUESTS.APP_SEQ_NUM ID_GENERAL\n                ,MATCH_REQUESTS.PROCESS_START_DATE\n                ,MATCH_REQUESTS.PROCESS_END_DATE\n                ,MATCH_REQUESTS.REQUEST_STATUS\n                ,MATCH_REQUESTS.REQUEST_ACTION\n                ,MATCH_REQUESTS.RESPONSE_ACTION\n                ,MATCH_REQUESTS.IMP_RESPONSE\n                ,MATCH_REQUESTS.IMP_STATUS\n                ,MATCH_REQUESTS.REQUEST_REASON_CODE\n                ,MATCH_REQUESTS.REQUEST_COMMENTS\n                ,MATCH_REQUESTS.ERROR_CODES\n                ,MATCH_REQUESTS.MASTERCARD_REF_NUMBER\n                ,MATCH_REQUESTS.VISA_BIN\n                ,MATCH_REQUESTS.ORIG_MASTERCARD_REFNUM\n        FROM    \n                MATCH_REQUESTS\n        WHERE   \n                MATCH_REQUESTS.MATCH_SEQ_NUM= :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"35com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,MRID);
   __sJT_st.setString(2,MRID);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"35com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1404^7*/
      rs = it.getResultSet();
          
      if(!(bLoad=rs.next()))
        throw new Exception("Unable to load MATCH request.  Request record '"+MRID+"' not found.");
      else if(!(bLoad=dbRecToObject(DBQ_MATCHDATA_REQUEST,rs,mdqi)))
        throw new Exception("Load MATCH Request '"+MRID+"' failed.");
        
      rs.close();
      it.close();
      
      // general data (1 record only)
      if(matchdtaType==DBQ_MATCHDATA_GENERAL || matchdtaType==DBQ_MATCHDATA_ALL) {
        /*@lineinfo:generated-code*//*@lineinfo:1417^9*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//            	     MERCHANT.MERCH_NUMBER
//            	    ,MERCHANT.MERCH_APPLICATION_TYPE
//            	    ,MERCHANT.MERCH_LEGAL_NAME MERCH_BUSINESS_NAME
//            	    ,MERCHANT.MERCH_BUSINESS_NAME MERCH_BUSINESS_DBA
//            	    ,MERCHANT.MERCH_CAT_CODE
//            	    ,MERCHANT.MERCH_DATE_OPENED
//            	    ,MERCHANT.MERCH_STATETAX_ID
//            	    ,MERCHANT.MERCH_FEDERAL_TAX_ID
//            	    ,MERCHANT.MERCH_WEB_URL
//            	    ,ADRBUSINESS.ADDRESS_LINE1 MAIL_ADDRESS1
//            	    ,ADRBUSINESS.ADDRESS_LINE2 MAIL_ADDRESS2
//            	    ,ADRBUSINESS.ADDRESS_CITY MAIL_CITY
//            	    ,ADRBUSINESS.COUNTRYSTATE_CODE MAIL_STATE
//            	    ,ADRBUSINESS.ADDRESS_ZIP MAIL_ZIP
//            	    ,ADRBUSINESS.COUNTRY_CODE MAIL_COUNTRY
//            	    ,ADRBUSINESS.ADDRESS_PHONE BUSINESS_PHONE
//            	    ,MB.MERCHBANK_ACCT_NUM DDA
//  
//            FROM
//            	    MERCHANT    MERCHANT
//            	    ,ADDRESS    ADRBUSINESS
//                  ,MERCHBANK  MB
//                  
//            WHERE
//            		  (MERCHANT.APP_SEQ_NUM=ADRBUSINESS.APP_SEQ_NUM(+) AND ADRBUSINESS.ADDRESSTYPE_CODE(+) = 1)
//                  AND MERCHANT.APP_SEQ_NUM=MB.APP_SEQ_NUM(+)
//                  AND (MERCHANT.APP_SEQ_NUM=:mdqi.getGeneralDataItem("ID_GENERAL"))
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1852 = mdqi.getGeneralDataItem("ID_GENERAL");
  try {
   String theSqlTS = "SELECT\n          \t     MERCHANT.MERCH_NUMBER\n          \t    ,MERCHANT.MERCH_APPLICATION_TYPE\n          \t    ,MERCHANT.MERCH_LEGAL_NAME MERCH_BUSINESS_NAME\n          \t    ,MERCHANT.MERCH_BUSINESS_NAME MERCH_BUSINESS_DBA\n          \t    ,MERCHANT.MERCH_CAT_CODE\n          \t    ,MERCHANT.MERCH_DATE_OPENED\n          \t    ,MERCHANT.MERCH_STATETAX_ID\n          \t    ,MERCHANT.MERCH_FEDERAL_TAX_ID\n          \t    ,MERCHANT.MERCH_WEB_URL\n          \t    ,ADRBUSINESS.ADDRESS_LINE1 MAIL_ADDRESS1\n          \t    ,ADRBUSINESS.ADDRESS_LINE2 MAIL_ADDRESS2\n          \t    ,ADRBUSINESS.ADDRESS_CITY MAIL_CITY\n          \t    ,ADRBUSINESS.COUNTRYSTATE_CODE MAIL_STATE\n          \t    ,ADRBUSINESS.ADDRESS_ZIP MAIL_ZIP\n          \t    ,ADRBUSINESS.COUNTRY_CODE MAIL_COUNTRY\n          \t    ,ADRBUSINESS.ADDRESS_PHONE BUSINESS_PHONE\n          \t    ,MB.MERCHBANK_ACCT_NUM DDA\n\n          FROM\n          \t    MERCHANT    MERCHANT\n          \t    ,ADDRESS    ADRBUSINESS\n                ,MERCHBANK  MB\n                \n          WHERE\n          \t\t  (MERCHANT.APP_SEQ_NUM=ADRBUSINESS.APP_SEQ_NUM(+) AND ADRBUSINESS.ADDRESSTYPE_CODE(+) = 1)\n                AND MERCHANT.APP_SEQ_NUM=MB.APP_SEQ_NUM(+)\n                AND (MERCHANT.APP_SEQ_NUM= :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1852);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"36com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1447^9*/
        rs = it.getResultSet();
        if(!rs.next()) {
          log.error("MATCH Request load General data failed: No general record found for MATCH request '"+mdqi.getID()+"'.");
          mdqi.setRequestStatus(MATCHDataQueueItem.MATCHREQUESTSTATUS_LOADERROR);
          mdqi.setRequestComments(mdqi.getRequestComments()+"  MATCH Request load error: No General data found (usu. means no MERCHANT record exists).");
          // NOTE: request will still be loaded in order to reflect the change to the response action field.
        } else if(!(bLoad=dbRecToObject(DBQ_MATCHDATA_GENERAL,rs,mdqi))) {
          log.error("MATCH Request load General data failed: Error converting rs-->obj for MATCH request '"+mdqi.getID()+"'.");
          mdqi.setRequestStatus(MATCHDataQueueItem.MATCHREQUESTSTATUS_LOADERROR);
          mdqi.setRequestComments(mdqi.getRequestComments()+"MATCH Request load error: No General data found: Error converting rs-->obj.");
        }
        
        log.debug("mdqi.getGeneralDataItem(MERCH_WEB_URL) = '"+mdqi.getGeneralDataItem("MERCH_WEB_URL")+"'");
        
        rs.close();
        it.close();
      }
      
      // principal data (0-n possible records)
      if(matchdtaType==DBQ_MATCHDATA_PRINCIPAL || matchdtaType==DBQ_MATCHDATA_ALL) {
        /*@lineinfo:generated-code*//*@lineinfo:1468^9*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT 
//            	    BUSINESSOWNER.BUSOWNER_NUM ID_PRINCIPAL
//            	   ,BUSINESSOWNER.BUSOWNER_LAST_NAME
//            	   ,BUSINESSOWNER.BUSOWNER_FIRST_NAME
//            	   ,BUSINESSOWNER.BUSOWNER_NAME_INITIAL
//            	   ,BUSINESSOWNER.BUSOWNER_SSN
//            	   ,BUSINESSOWNER.BUSOWNER_DRVLICNUM
//            	   ,ADDRESS.ADDRESS_LINE1 MAILADR_LINE1
//            	   ,ADDRESS.ADDRESS_LINE2 MAILADR_LINE2
//            	   ,ADDRESS.ADDRESS_CITY MAILADR_CITY
//            	   ,ADDRESS.COUNTRYSTATE_CODE MAILADR_STATE
//            	   ,ADDRESS.ADDRESS_ZIP MAILADR_ZIP
//            	   ,ADDRESS.COUNTRY_CODE MAILADR_COUNTRYCODE
//            	   ,ADDRESS.ADDRESS_PHONE MAILADR_PHONE
//  
//            FROM
//            	    BUSINESSOWNER,ADDRESS
//            WHERE
//            	 	  (BUSINESSOWNER.APP_SEQ_NUM=ADDRESS.APP_SEQ_NUM(+) AND ADDRESS.ADDRESSTYPE_CODE(+)=2)
//                  AND BUSINESSOWNER.APP_SEQ_NUM=:mdqi.getGeneralDataItem("ID_GENERAL")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1853 = mdqi.getGeneralDataItem("ID_GENERAL");
  try {
   String theSqlTS = "SELECT \n          \t    BUSINESSOWNER.BUSOWNER_NUM ID_PRINCIPAL\n          \t   ,BUSINESSOWNER.BUSOWNER_LAST_NAME\n          \t   ,BUSINESSOWNER.BUSOWNER_FIRST_NAME\n          \t   ,BUSINESSOWNER.BUSOWNER_NAME_INITIAL\n          \t   ,BUSINESSOWNER.BUSOWNER_SSN\n          \t   ,BUSINESSOWNER.BUSOWNER_DRVLICNUM\n          \t   ,ADDRESS.ADDRESS_LINE1 MAILADR_LINE1\n          \t   ,ADDRESS.ADDRESS_LINE2 MAILADR_LINE2\n          \t   ,ADDRESS.ADDRESS_CITY MAILADR_CITY\n          \t   ,ADDRESS.COUNTRYSTATE_CODE MAILADR_STATE\n          \t   ,ADDRESS.ADDRESS_ZIP MAILADR_ZIP\n          \t   ,ADDRESS.COUNTRY_CODE MAILADR_COUNTRYCODE\n          \t   ,ADDRESS.ADDRESS_PHONE MAILADR_PHONE\n\n          FROM\n          \t    BUSINESSOWNER,ADDRESS\n          WHERE\n          \t \t  (BUSINESSOWNER.APP_SEQ_NUM=ADDRESS.APP_SEQ_NUM(+) AND ADDRESS.ADDRESSTYPE_CODE(+)=2)\n                AND BUSINESSOWNER.APP_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1853);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"37com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1490^9*/
        rs = it.getResultSet();
        
        // principal data (0..n)
        while(rs.next()) {
          if(dbRecToObject(DBQ_MATCHDATA_PRINCIPAL,rs,mdqi))
            numPrecs++;
          else
            log.warn("MATCH Request load Principal record failed:  (Request ID: '"+mdqi.getID()+"').  Principal record skipped.");
        }
        
        // substitute merchant contact in for principal data if principal data non-existant
        if(numPrecs==0 && (matchdtaType==DBQ_MATCHDATA_GENERAL || matchdtaType==DBQ_MATCHDATA_ALL)) {
          
          rs.close();
          it.close();
          
          /*@lineinfo:generated-code*//*@lineinfo:1507^11*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT 
//              	    1 ID_PRINCIPAL
//              	   ,MERCHCONTACT.MERCHCONT_PRIM_FIRST_NAME BUSOWNER_FIRST_NAME
//              	   ,MERCHCONTACT.MERCHCONT_PRIM_LAST_NAME BUSOWNER_LAST_NAME
//              	   ,MERCHCONTACT.MERCHCONT_PRIM_NAME_INITIAL BUSOWNER_NAME_INITIAL
//              	   ,MERCHCONTACT.MERCHCONT_PRIM_PHONE MAILADR_PHONE
//              FROM
//              	    MERCHCONTACT
//              WHERE
//              	 	  MERCHCONTACT.APP_SEQ_NUM=:mdqi.getGeneralDataItem("ID_GENERAL")
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1854 = mdqi.getGeneralDataItem("ID_GENERAL");
  try {
   String theSqlTS = "SELECT \n            \t    1 ID_PRINCIPAL\n            \t   ,MERCHCONTACT.MERCHCONT_PRIM_FIRST_NAME BUSOWNER_FIRST_NAME\n            \t   ,MERCHCONTACT.MERCHCONT_PRIM_LAST_NAME BUSOWNER_LAST_NAME\n            \t   ,MERCHCONTACT.MERCHCONT_PRIM_NAME_INITIAL BUSOWNER_NAME_INITIAL\n            \t   ,MERCHCONTACT.MERCHCONT_PRIM_PHONE MAILADR_PHONE\n            FROM\n            \t    MERCHCONTACT\n            WHERE\n            \t \t  MERCHCONTACT.APP_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"38com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1854);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"38com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1519^11*/
          rs = it.getResultSet();
          if(!rs.next())
            log.error("No Merchant contact info exists for primary principal substitution for MATCH Request ID '"+mdqi.getID()+"'.");
          else if(dbRecToObject(DBQ_MATCHDATA_PRINCIPAL,rs,mdqi)) {
            numPrecs++;
            log.warn("Merchant Contact data substituted for Primary principal data for MATCH Request ID '"+mdqi.getID()+"'.");
          } else
            log.error("MATCH Request load Merchant Contact (substitute) Principal data failed for MATCH Request ID '"+mdqi.getID()+"'.");
        }
        
        rs.close();
        it.close();
      }
      if(numPrecs==0)
        log.warn("NO PRINCIPAL DATA LOADED for MATCH Request ID '"+mdqi.getID()+"'.");
      
      if(matchdtaType==DBQ_MATCHDATA_MDRECS || matchdtaType==DBQ_MATCHDATA_ALL) {
        /*@lineinfo:generated-code*//*@lineinfo:1537^9*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT 
//            	    MRD_SEQ_NUM,ISACKNOWLEDGED,GENERAL_RECORD,PRINCIPAL_RECORD,IS_IMR,IS_FROM_IMPBLACKLIST
//            FROM
//            	    MATCH_REQUESTS_RESPONSEDATA
//            WHERE
//            	 	  MATCH_SEQ_NUM=:mdqi.getID()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1855 = mdqi.getID();
  try {
   String theSqlTS = "SELECT \n          \t    MRD_SEQ_NUM,ISACKNOWLEDGED,GENERAL_RECORD,PRINCIPAL_RECORD,IS_IMR,IS_FROM_IMPBLACKLIST\n          FROM\n          \t    MATCH_REQUESTS_RESPONSEDATA\n          WHERE\n          \t \t  MATCH_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.match.DBQueueOps_MATCHDataQueueItem",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1855);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"39com.mes.match.DBQueueOps_MATCHDataQueueItem",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1545^9*/
        rs = it.getResultSet();
        
        // match data records (0..n)
        while(rs.next()) {
          if(dbRecToObject(DBQ_MATCHDATA_MDRECS,rs,mdqi))
            numMDrecs++;
          else
            log.error("MATCH Request load MATCH Data Record failed:  (Request ID: '"+mdqi.getID()+"').  MATCH data record skipped.");
        }
        
        rs.close();
        rs.close();
      }
      if(numMDrecs>0)
        log.info(numMDrecs+" MATCH RESPONSE records loaded for MATCH Request ID '"+mdqi.getID()+"'.");

          
      // TODO: For Add requests, Date Closed field must be created in db and currently isn't.
      
      return bLoad;
    }
    catch(Exception e) {
      log.error("load(SINGLE) Exception: '"+e.getMessage()+"'.");
      mdqi.clear();
      return false;
    }
    finally {
      cleanUp();
    }
  }

  /**
   * load() - MULTI-Merchant - by key
   * 
   * NOTE: Always overwrites the queueItems - clearing it before any additions
   * 
   * @param   val             key value
   * @param   key             Enum: DBQ_KEY_{...}
   * @param   matchdtaType    Enum: DBQ_MATCHDATA_{...}
   * @param   queueItems      Container to hold loaded objects
   * 
   * @return  Returns number of loaded MATCH data items.
   */
  public int load(String val, int key, int matchdtaType, Hashtable queueItems)
  {
    int numLoaded = 0;
    
    try {
    
      queueItems.clear();
      
      connect();
      
      String[] arrMRID = null;
      MATCHDataQueueItem mdqi=null;
      
      // get array of matching match request ids according to mercspec
      if(key==DBQ_KEY_PENDINGBYMERCNAME) {
        final String mrid = getPendingRequestIDByMerchBusinessName(val);
        if(mrid.length()==0)
          throw new Exception("No pending MATCH Request found for merchant business name '"+val+"'.");
        arrMRID = new String[] { mrid };
      } else
        arrMRID = getRequestIDsByKey(key,val);
      
      for(int i=0; i<arrMRID.length; i++) {
        mdqi=new MATCHDataQueueItem();
        if(!load(arrMRID[i],matchdtaType,mdqi))
          throw new Exception("Failed to load Merchant MATCH data queue item (ID: '"+arrMRID[i]+"').");
        queueItems.put(arrMRID[i],mdqi);
        numLoaded++;
      } // arrMRID[]
          
      return numLoaded;
        
    }
    catch(Exception e) {
      log.error("load(MULTI-merchant) Exception: '"+e.getMessage()+"'.");
      queueItems.clear();
      return 0;
    }
    finally {
      cleanUp();
    }
    
  }  // load(MULTI-merchant)
  
  /**
   * load()
   *  - MULTI-RECORD load f() - MATCHDataQueueItem implementation
   *  - returns number of loaded MATCH data items
   */
  public int load(int dbqType,int matchdtaType,Hashtable queueItems,boolean bOverwrite)
  {
    int numLoaded = 0;
    
    try {
    
      connect();
      
      // get array of matching match request ids
      final String[] arrMRID = getMatchRequestIDsByQueueType(dbqType);
      MATCHDataQueueItem mdqi;
      boolean bLoad;
      
      for(int i=0; i<arrMRID.length; i++) {
        if(bLoad = bOverwrite? true : !queueItems.containsKey(arrMRID[i])) {
          mdqi=new MATCHDataQueueItem();
          if(!load(arrMRID[i],matchdtaType,mdqi))
            throw new Exception("Failed to load MATCH data queue item (ID: '"+arrMRID[i]+"').");
          queueItems.put(arrMRID[i],mdqi);
          numLoaded++;
        }
      } // arrMRID[]
          
      return numLoaded;
        
    }
    catch(Exception e) {
      log.error("load(MULTI) Exception: '"+e.getMessage()+"'.");
      queueItems.clear();
      return 0;
    }
    finally {
      cleanUp();
    }
    
  }  // load(MULTI)
  
  
  /**
   * save()
   *  - MULTI-RECORD save f()
   *  - saves MATCH data items to db contained in hashtable arg
   *  - returns number of saved MATCH data items
   *  - calls SINGLE-RECORD save f() for each MATCH data item in MATCH data item hashtable arg
   */
  public boolean save(Hashtable queueItems,int dbqType)
  {
    try {
      
      connect();

      String id;
      MATCHDataQueueItem mdqi = null;
    
      for(Enumeration e=queueItems.keys(); e.hasMoreElements(); ) {
        id=(String)e.nextElement();
        mdqi=(MATCHDataQueueItem)queueItems.get(id);
        if(!save(mdqi,dbqType,true))
          log.error("Error saving MATCH data queue item (ID: '"+mdqi.getID()+"').  Continuing.");
      }
      
      // commit the db trans
      commit();

      return true;
    }
    catch(Exception e) {
      log.error("save(MULTI) Exception: '"+e.getMessage()+"'.");
      return false;
    }
    finally {
      cleanUp();
    }
  }  // save()

  
  protected boolean dbRecToObject(int matchdtaType, ResultSet rs, MATCHDataQueueItem mdqi)
  {
    try {
    
      String sDta;
      
      // pre-validation
      if(matchdtaType==DBQ_MATCHDATA_PRINCIPAL) {
        // ensure first name and last name exist or don't load
        sDta=rs.getString("BUSOWNER_LAST_NAME");
        if(sDta==null || sDta.length()<1) {
          log.warn("Principal record skipped - BUSOWNER_LAST_NAME field empty.");
          return false;
        }
        sDta=rs.getString("BUSOWNER_FIRST_NAME");
        if(sDta==null || sDta.length()<1) {
          log.warn("Principal record skipped - BUSOWNER_FIRST_NAME field empty.");
          return false;
        }
      }
      
      ResultSetMetaData rsMtaDta = rs.getMetaData();
      
      String mrd_seq_num=null,general_record=null,principal_record=null;
      boolean is_acknowledged=false,is_imr=false;
      MATCHResponse mresp;
      
      for(int i=1; i<=rsMtaDta.getColumnCount(); i++) {
      
        sDta = rs.getString(i);
        if(null == sDta)
          sDta = "";
          
        //log.debug(rsMtaDta.getColumnName(i)+" = "+sDta);
        
        if(matchdtaType==DBQ_MATCHDATA_REQUEST) {
          if(rsMtaDta.getColumnName(i).equals("ID_MATCHREQUEST")) {
            log.debug(">>>mdqi.id set to "+sDta);
            mdqi.setID(sDta);
          } else if(rsMtaDta.getColumnName(i).equals("ID_GENERAL"))
            mdqi.setGeneralDataItem(rsMtaDta.getColumnName(i),sDta);
          else if(rsMtaDta.getColumnName(i).equals("PROCESS_START_DATE")) {
            if(rs.getDate(i) != null)
              mdqi.setProcessStartDate(rs.getTimestamp(i));
          } else if(rsMtaDta.getColumnName(i).equals("PROCESS_END_DATE")) {
            if(rs.getDate(i) != null)
              mdqi.setProcessEndDate(rs.getTimestamp(i));
          } else if(rsMtaDta.getColumnName(i).equals("REQUEST_STATUS"))
            mdqi.setRequestStatus(sDta);
          else if(rsMtaDta.getColumnName(i).equals("REQUEST_ACTION"))
            mdqi.setRequestAction(sDta);
          else if(rsMtaDta.getColumnName(i).equals("RESPONSE_ACTION"))
            mdqi.setResponseAction(sDta);
          else if(rsMtaDta.getColumnName(i).equals("IMP_RESPONSE"))
            mdqi.setIMPResponse(sDta);
          else if(rsMtaDta.getColumnName(i).equals("IMP_STATUS"))
            mdqi.setIMPStatus(sDta);
          else if(rsMtaDta.getColumnName(i).equals("REQUEST_REASON_CODE"))
            mdqi.setRequestReasonCode(sDta);
          else if(rsMtaDta.getColumnName(i).equals("REQUEST_COMMENTS"))
            mdqi.setRequestComments(sDta);
          else if(rsMtaDta.getColumnName(i).equals("ERROR_CODES"))
            mdqi.setErrorCodes(sDta);
          else if(rsMtaDta.getColumnName(i).equals("MASTERCARD_REF_NUMBER"))
            mdqi.setMCRefNum(sDta);
          else if(rsMtaDta.getColumnName(i).equals("VISA_BIN"))
            mdqi.setVisaBin(sDta);
          else if(rsMtaDta.getColumnName(i).equals("ORIG_MASTERCARD_REFNUM"))
            mdqi.setOrigMCRefNum(sDta);
        
        } else if(matchdtaType==DBQ_MATCHDATA_GENERAL) {
          if(rsMtaDta.getColumnName(i).equals("MAIL_COUNTRY")) {
            /*
            if(sDta.length()==0 || (new String("US,U.S.,U.S.A.")).indexOf(sDta.toUpperCase())>=0) {
              sDta="USA";
              log.warn("Set empty General Mail Country field to 'USA' as default for MATCH Request '"+mdqi.getID()+"'.");
            }
            */
            // Blindly set country to USA
            log.warn("Blindly overwritting General Mail Country field to 'USA' for MATCH Request '"+mdqi.getID()+"'.");
            sDta="USA";
          }
          mdqi.setGeneralDataItem(rsMtaDta.getColumnName(i),sDta);
        
        } else if(matchdtaType==DBQ_MATCHDATA_PRINCIPAL) {
          if(rsMtaDta.getColumnName(i).equals("MAILADR_COUNTRYCODE") && sDta.length()>0) {
            sDta="USA";
            log.warn("Blindly overwriting Principal Mail Address Country field to 'USA' for MATCH Request '"+mdqi.getID()+"'.");
          } else if(rsMtaDta.getColumnName(i).equals("MAILADR_PHONE")) {
            if(sDta.length()!=10)
              sDta="";
          } else if(rsMtaDta.getColumnName(i).equals("BUSOWNER_SSN")) {
            if(sDta.length()!=9)
              sDta="";
          }
          mdqi.setPrincipalDataItem(rs.getString("ID_PRINCIPAL"),rsMtaDta.getColumnName(i),sDta);
        
        } else if(matchdtaType==DBQ_MATCHDATA_MDRECS) {
          if(rsMtaDta.getColumnName(i).equals("MRD_SEQ_NUM"))
            mrd_seq_num=sDta;
          else if(rsMtaDta.getColumnName(i).equals("ISACKNOWLEDGED"))
            is_acknowledged=rs.getBoolean(i);
          else if(rsMtaDta.getColumnName(i).equals("GENERAL_RECORD"))
            general_record=sDta;
          else if(rsMtaDta.getColumnName(i).equals("PRINCIPAL_RECORD"))
            principal_record=sDta;
          else if(rsMtaDta.getColumnName(i).equals("IS_IMR")) {
            is_imr=rs.getBoolean(i);
            
            if(is_imr) {
              // Internal Match Response
              mresp = new IMResponse(mrd_seq_num,is_acknowledged,general_record,principal_record,null);
              ((IMResponse)mresp).setImpFromBlacklist(rs.getBoolean("IS_FROM_IMPBLACKLIST"));
              mdqi.setIMResponse((IMResponse)mresp);
              log.debug("IMP Response '"+mresp.getID()+"' added to MATCH Request '"+mdqi.getID()+"'.");
            } else {
              // regular MATCH Response
              mresp = new MATCHResponse(mrd_seq_num,is_acknowledged,general_record,principal_record,null);
              mdqi.setResponseItem(mresp);
              log.debug("MATCH Response '"+mresp.getID()+"' added to MATCH Request '"+mdqi.getID()+"'.");
            }
            mresp.clean();
          }
        } else
          throw new Exception("Unhandled MATCH data type: '"+matchdtaType+"'.");
      
      }

    }
    catch(Exception e) {
      log.error("dbRecToObject() - Exception: "+e.getMessage());
      return false;
    }
    
    return true;
  }
  
  protected boolean dbRecToObject(int matchdtaType, ResultSet rs, IMResponse imr)
  {
    try {
    
      String sDta;
      
      ResultSetMetaData rsMtaDta = rs.getMetaData();
      
      String mrd_seq_num=null,general_record=null,principal_record=null;
      boolean is_acknowledged=false,is_imr=false;
      
      for(int i=1; i<=rsMtaDta.getColumnCount(); i++) {
      
        sDta = rs.getString(i);
        if(null == sDta)
          sDta = "";
          
        //log.debug(rsMtaDta.getColumnName(i)+" = "+sDta);
        
        if(matchdtaType==DBQ_MATCHDATA_GENERAL) {
          imr.getMATCHDataItem().setGeneralDataItem(rsMtaDta.getColumnName(i),sDta);
        
        } else if(matchdtaType==DBQ_MATCHDATA_PRINCIPAL) {
          imr.getMATCHDataItem().setPrincipalDataItem(rs.getString("ID_PRINCIPAL"),rsMtaDta.getColumnName(i),sDta);
        
        } else
          throw new Exception("Unhandled MATCH data type (IMResponse): '"+matchdtaType+"'.");
      
      }

    }
    catch(Exception e) {
      log.error("dbRecToObject(IMResponse) - EXCEPTION: '"+e.getMessage()+"'");
      return false;
    }
    
    return true;
  }

}/*@lineinfo:generated-code*/