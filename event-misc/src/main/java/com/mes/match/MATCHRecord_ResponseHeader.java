package com.mes.match;

// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;

/**
 * MATCHRecord_ResponseHeader
 * -------------------
 * - extends MATCHRecord encapsulating header spec. record logic
 */
class MATCHRecord_ResponseHeader extends MATCHRecord
{
  // create class log category
  static Category log = Category.getInstance(MATCHRecord_ResponseHeader.class.getName());

  // constants
  // (none)

  // data members
  protected String          rundate;      // YYYYMMDD
  protected String          runtime;      // HHMMSS
  protected String          reportID;
  
  // construction
  protected MATCHRecord_ResponseHeader(String name,int sequenceNum)
  {
    super(name,MATCHRecord.MATCHRECTYPE_RESPONSEHEADER,sequenceNum);
    
    rundate="";
    runtime="";
    reportID="";
  }
  
  /**
   * getFieldValue()
   * 
   * Sub-class override.
   */
  protected String getFieldValue(String fldNme)
    throws Exception
  {
    String value=null;
    
    // presume this object is of record_type: 'MATCHRecord_ResponseHeader'
    if(fldNme.equals("RECORD_TYPE"))
      value="RHD";
    else if(fldNme.equals("TRANSACTION_CODE"))
      value="653";
    else if(fldNme.equals("ACN"))
      value=StringUtilities.rightJustify(MATCHRECCNST_ACN,7,'0');
    else if(fldNme.equals("RUNDATE"))
      value=rundate;
    else if(fldNme.equals("RUNTIME"))
      value=runtime;
    else if(fldNme.equals("REPORTID"))
      value=reportID;
    else
      throw new Exception("Get response header field value failed: field ('"+fldNme+"') un-mapped.");
    
    //log.debug("HEADER - Field: '"+fldNme+"' mapped to value: '"+value+"'");
    return value;
  }
  
  /**
   * setFieldValue()
   * 
   * Sub-class override.
   */
  protected void setFieldValue(String fldNme, String fldVal)
  {
    //log.debug("setFieldValue(RESPONSE HEADER) - START");
    
    if(fldNme==null || fldNme.length()<1)
      return;
    if(fldVal==null)
      fldVal="";
    
    if(fldNme.equals("RUNDATE"))
      rundate=fldVal;
    else if(fldNme.equals("RUNTIME"))
      runtime=fldVal;
    else if(fldNme.equals("REPORTID"))
      reportID=fldVal;
  }
  
} // class MATCHRecord_ResponseHeader
