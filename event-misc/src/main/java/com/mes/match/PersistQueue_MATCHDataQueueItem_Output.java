package com.mes.match;

// log4j classes.
import org.apache.log4j.Category;

/**
 * PersistQueue_MATCHDataQueueItem_Output
 * ---------------------------------
*/
class PersistQueue_MATCHDataQueueItem_Output extends PersistQueue_MATCHDataQueueItem
{
  // create class log category
  static Category log = Category.getInstance(PersistQueue_MATCHDataQueueItem_Output.class.getName());

  // construction
  public PersistQueue_MATCHDataQueueItem_Output()
    throws Exception
  {
    super(false);
  }
  
  public void load()
    throws Exception
  {
    super.load(DBQueueOps_MATCHDataQueueItem.DBQ_TYPE_OUTPUT,false);
  }
  
  public void save()
    throws Exception
  {
    super.save(DBQueueOps_MATCHDataQueueItem.DBQ_TYPE_OUTPUT);
  }

  public void updatePersist()
    throws Exception
  {
    super.updatePersist(DBQueueOps_MATCHDataQueueItem.DBQ_TYPE_OUTPUT);
  }

  public void updatePersist(String mdqiID)
    throws Exception
  {
    super.updatePersist(mdqiID,DBQueueOps_MATCHDataQueueItem.DBQ_TYPE_OUTPUT);
  }

} // class PersistQueue_MATCHDataQueueItem_Output
