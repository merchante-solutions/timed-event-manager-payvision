package com.mes.match;

// log4j classes.
import org.apache.log4j.Category;

/**
 * MATCHRecord_ResponsePrincipal
 * -------------------
 * - extends MATCHRecord encapsulating header spec. record logic
 */
class MATCHRecord_ResponsePrincipal extends MATCHRecord
{
  // create class log category
  static Category log = Category.getInstance(MATCHRecord_ResponsePrincipal.class.getName());

  // constants
  // (none)

  // data members
  // (none)
  
  // construction
  protected MATCHRecord_ResponsePrincipal(String name,int sequenceNum)
  {
    super(name,MATCHRecord.MATCHRECTYPE_RESPONSEPRINCIPAL,sequenceNum);
  }
  
  /**
   * getFieldValue()
   * 
   * Sub-class override.
   */
  protected String getFieldValue(String fldNme)
    throws Exception
  {
    if(mdi==null)
      return "";
    
    String val;
           
    if(fldNme.equals("TRANSACTION_CODE"))
      val="651";
    else if(fldNme.equals("ACTION_INDICATOR"))
      val="P";
    else if(fldNme.equals("RECORD_TYPE"))
      val="2";
    else if(mdi!=null) {
      // parse away ending _{number} (de-flat file)
      if(fldNme.endsWith("_1"))
        val=mdi.getPrincipalDataItem("1",fldNme.substring(0,fldNme.length()-2));
      else if(fldNme.endsWith("_2"))
        val=mdi.getPrincipalDataItem("2",fldNme.substring(0,fldNme.length()-2));
      else if(fldNme.endsWith("_3"))
        val=mdi.getPrincipalDataItem("3",fldNme.substring(0,fldNme.length()-2));
      else if(fldNme.endsWith("_4"))
        val=mdi.getPrincipalDataItem("4",fldNme.substring(0,fldNme.length()-2));
      else if(fldNme.endsWith("_5"))
        val=mdi.getPrincipalDataItem("5",fldNme.substring(0,fldNme.length()-2));
      else
        val="";
    } else
      val="";
    
    return val;
  }
  
  /**
   * setFieldValue()
   * 
   * Sub-class override.
   * NOTE: many fields in the general response are currently ignored including the {...}_MATCH ones
   */
  protected void setFieldValue(String fldNme, String fldVal)
    throws Exception
  {
    if(!IsResponseOriginatedRecord())
      return; // no-op
    
    if(fldNme==null || fldNme.length()<1)
      throw new Exception("Unable to set principal response field value: Null or empty field name specified.");
    if(fldVal==null)
      fldVal="";
    
    // ensure data container exists
    if(mdi==null)
      mdi = new MATCHDataItem();
    
    // parse away ending _{number} (de-flat file)
    if(fldNme.endsWith("_1"))
      mdi.setPrincipalDataItem("1",fldNme.substring(0,fldNme.length()-2),fldVal);
    else if(fldNme.endsWith("_2"))
      mdi.setPrincipalDataItem("2",fldNme.substring(0,fldNme.length()-2),fldVal);
    else if(fldNme.endsWith("_3"))
      mdi.setPrincipalDataItem("3",fldNme.substring(0,fldNme.length()-2),fldVal);
    else if(fldNme.endsWith("_4"))
      mdi.setPrincipalDataItem("4",fldNme.substring(0,fldNme.length()-2),fldVal);
    else if(fldNme.endsWith("_5"))
      mdi.setPrincipalDataItem("5",fldNme.substring(0,fldNme.length()-2),fldVal);
  }
  
} // class MATCHRecord_ResponsePrincipal
