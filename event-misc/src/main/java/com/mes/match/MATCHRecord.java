package com.mes.match;

import java.util.StringTokenizer;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;

/**
 * MATCHRecord
 * -----------
 * - Represents MATCH data in string record form per MATCH system specs.
 * - Houses logic that transforms native data to target data (string record) and vice-versa.
 * - Sub-classes represent MATCH record types per the MATCH specs.
 * 
 * Request/Response Action - LEGEND
 * --------------------------------
 * 
 *     REQUEST_ACTION RESPONSE_ACTION DESCRIPTION                             
 *     -------------- --------------- ----------------------------------------
 *     A              A               Successful Merchant Add                 
 *     A              E               Error(s) found in the add record        
 *     I              N               No match to inquiry                     
 *     I              E               Errors found in the inquiry record      
 *     I              P               Possible match                          
 *     I              R               Retroactive possible match              
 *     I              M               New possible merchant match             
 *     I              H               Previously reported possible merchant ma
 *     I              Q               New possible inquiry match              
 *     I              Y               Previously reported possible inquiry mat
 */
abstract class MATCHRecord extends Object
{
  // create class log category
  static Category log = Category.getInstance(MATCHRecord.class.getName());

  
  // constants
  // MATCH Record types
  public static final int       MATCHRECTYPE_UNDEFINED            = 0;
  
  public static final int       MATCHRECTYPE_ADDHEADER            = 1;
  public static final int       MATCHRECTYPE_ADDGENERAL           = 2;
  public static final int       MATCHRECTYPE_ADDPRINCIPAL         = 3;
  public static final int       MATCHRECTYPE_ADDTRAILER           = 4;
  public static final int       MATCHRECTYPE_INQUIRYHEADER        = 5;
  public static final int       MATCHRECTYPE_INQUIRYGENERAL       = 6;
  public static final int       MATCHRECTYPE_INQUIRYPRINCIPAL     = 7;
  public static final int       MATCHRECTYPE_INQUIRYTRAILER       = 8;
    // request related
  public static final int       MATCHRECTYPE_RESPONSEHEADER       = 9;
  public static final int       MATCHRECTYPE_RESPONSEGENERAL      = 10;
  public static final int       MATCHRECTYPE_RESPONSEPRINCIPAL    = 11;
  public static final int       MATCHRECTYPE_RESPONSETRAILER      = 12;
    // response related
  public static final int       MATCHRECTYPE_COMPOSITEADD         = 13;
  public static final int       MATCHRECTYPE_COMPOSITEINQUIRY     = 14;
    // request or response related
  
  public static final int       MATCHREQUESTTYPE_UNDEFINED        = 0;
  public static final int       MATCHREQUESTTYPE_ADD              = 1;
  public static final int       MATCHREQUESTTYPE_INQUIRY          = 2;
  
  public static final String    MATCHRECCNST_ACN                  = "0005185";
    // member Acquirer Customer Number (MES')

  public static final int       NUMBER_REQUIRED_PHONETIC_MATCHES  = 2;  // or 3
    // number of required phonetic matches for general inquiries
    // value must be either 2 or 3

  public static final int       MATCHRECORD_SIZE                  = 1012; // (chars)
  
  public static final String    DELIM_FIELD                       = "";
    // field delimeter
  public static final String    DELIM_RECORD                      = System.getProperty("line.separator");
    // record delimeter
  
  public static final int       MIN_RECORD_LENGTH                 = 18;
    // reason: 18th position (1-based counting) is the Action Identifier field
  
  /**
   * Base General fields
   */
  public final static String[] BASEFLDNMES_GENERAL = 
  {
     "MERCH_BUSINESS_NAME"
    ,"MERCH_BUSINESS_DBA"
    ,"MERCH_STATETAX_ID"
    ,"MERCH_FEDERAL_TAX_ID"
    ,"MERCH_WEB_URL"
    ,"MAIL_ADDRESS1"
    ,"MAIL_ADDRESS2"
    ,"MAIL_CITY"
    ,"MAIL_STATE"
    ,"MAIL_ZIP"
    ,"MAIL_COUNTRY"
    ,"BUSINESS_PHONE"
    ,"DDA"
  };

  /**
   * Base Principal fields
   */
  public final static String[] BASEFLDNMES_PRINCIPAL = 
  {
     "BUSOWNER_LAST_NAME"
    ,"BUSOWNER_FIRST_NAME"
    ,"BUSOWNER_NAME_INITIAL"
    ,"BUSOWNER_SSN"
    ,"BUSOWNER_DRVLICNUM"
    ,"MAILADR_LINE1"
    ,"MAILADR_LINE2"
    ,"MAILADR_CITY"
    ,"MAILADR_STATE"
    ,"MAILADR_ZIP"
    ,"MAILADR_COUNTRYCODE"
    ,"MAILADR_PHONE"
  };
  
  
  // data members
  protected String          name;             // verbose record name based on the record type
  protected int             record_type;      // one of the MATCHRECTYPE_{...} constants
  protected int             request_type;     // one of the MATCHREQUESTTYPE_{...} constants
  protected String          definition;       // per getRequest[Response]RecordDefinition() class functions
  protected String          record;           // string containing MATCH data based on the associated record definition
  protected boolean         isRecordValid;    // is 'record' data member valid? (true ONLY when non-empty and parsed successfully)
  protected int             sequenceNum;      // incremental integer to distinquish among records used for batching
  protected MATCHDataItem   mdi;              // MATCHRecord data container (Native data)
  
  
  // construction
  protected MATCHRecord(String name,int record_type,int sequenceNum)
  {
    this.name = name;
    this.record_type = record_type;
    this.definition = null;
    record = "";
    isRecordValid=false;
    this.sequenceNum=sequenceNum;
    this.mdi=null;
    
    // attempt to auto-set the request type
    if(record_type>=MATCHRECTYPE_ADDHEADER && record_type<=MATCHRECTYPE_ADDTRAILER)
      request_type=MATCHREQUESTTYPE_ADD;
    else if(record_type>=MATCHRECTYPE_INQUIRYHEADER && record_type<=MATCHRECTYPE_INQUIRYTRAILER)
      request_type=MATCHREQUESTTYPE_INQUIRY;
    else if(record_type==MATCHRECTYPE_COMPOSITEADD)
      request_type=MATCHREQUESTTYPE_ADD;
    else if(record_type==MATCHRECTYPE_COMPOSITEINQUIRY)
      request_type=MATCHREQUESTTYPE_INQUIRY;
    else
      request_type=MATCHREQUESTTYPE_UNDEFINED;
    
    //log.debug("--> MATCHRecord() request_type='"+request_type+"'");
  }
  
  // accessors
  public String getName() { return name; }
  public int getRecordType() { return record_type; }
  public String getRecord() { return record; }
  public boolean IsRecordValid() { return isRecordValid; }
  public int getSequenceNum() { return sequenceNum; }
  public int getRecSize() { return record.length(); }
  public MATCHDataItem getDataContainer()
  {
    if(mdi==null)
      mdi = new MATCHDataItem();
    
    return mdi;
  }
  
  /**
   * isRequestRecord
   * 
   * Answers whether this instance is a request (as opposed to response) record.
   */
  public boolean isRequestRecord()
    throws Exception
  {
    if(record_type==MATCHRECTYPE_UNDEFINED || record_type==MATCHRECTYPE_COMPOSITEADD || record_type==MATCHRECTYPE_COMPOSITEINQUIRY)
      throw new Exception("Unable to deterine if this record is of request or response type.  Record type is undefined or of Composite type.");
    
    return (record_type>=MATCHRECTYPE_ADDHEADER && record_type<=MATCHRECTYPE_INQUIRYTRAILER);
  }
  /**
   * isResponseRecord
   * 
   * Converse of isRequsetRecord() function.
   */
  public boolean isResponseRecord()
    throws Exception
  {
    return !isRequestRecord();
  }
  
  /**
   * IsResponseOriginatedRecord()
   * 
   * Object version of class function of same name.
   * 
   * @exception         Upon indeterminate response record param or otherwise
   */
  public boolean IsResponseOriginatedRecord()
    throws Exception
  {
    if(record_type!=MATCHRECTYPE_RESPONSEGENERAL && record_type!=MATCHRECTYPE_RESPONSEPRINCIPAL)
      throw new Exception("IsResponseOriginatedRecord() EXCEPTION: MATCH record must be of response general or response principal type.");
    
    final char ai = record.charAt(3);         // action identifier field
    
    return IsResponseOriginatedRecord(ai);
  }
  
  
  /**
   * HasRelatedResponseOriginatedRecords()
   * 
   * Object version of class function of same name.
   * 
   * @exception         Upon indeterminate response record param or otherwise
   */
  public boolean HasRelatedResponseOriginatedRecords()
    throws Exception
  {
    if(record_type!=MATCHRECTYPE_RESPONSEGENERAL)
      throw new Exception("HasRelatedResponseOriginatedRecords() EXCEPTION: MATCH record must be of response general type.");
    
    final char ai = record.charAt(3);         // action identifier field
    
    return HasRelatedResponseOriginatedRecords(ai);
  }
  
  /**
   * getRequestType()
   * 
   * Determines the originating type of request accociated with this record 
   * expressed in terms of MATCHREQUESTTYPE_{...} enumeration
   */
  public int getRequestType()
  {
    if(request_type==MATCHREQUESTTYPE_UNDEFINED) {
      try {
        if(isResponseRecord() && record!=null)
          request_type=getRequestTypeFromResponseRecord(record);
      }
      catch(Exception e) {
        log.error("getRequestType() EXCEPTION: '"+e.getMessage()+"'.");
      }
    }
    
    return request_type;
  }
  
  // mutators
  protected void setRecord(String record)
    throws Exception
  {
    if(!isResponseRecord())
      throw new Exception("Can only set record strings on Response type MATCH records.");
    if(record==null)
      throw new Exception("Attempt to set null MATCH record string for record '"+name+"'.");
    this.record=record;
  }
  protected void setDataContainer(MATCHDataItem mdi)
    throws Exception
  {
    if(mdi==null)
      throw new Exception("Attempt to set null MATCH data container for record '"+name+"'.");
    this.mdi=mdi;
  }
  protected void setRequestType(int request_type)
    throws Exception
  {
    if(this.request_type!=MATCHREQUESTTYPE_UNDEFINED)
      throw new Exception("Unable to set MATCH record request type: Request type already set.");
    
    this.request_type=request_type;
  }
  
  /**
   * generateRecord()
   * 
   * Sets the 'record' data member.
   */
  public boolean generateRecord()
  {
    return ObjToRec();
  }
  
  /**
   * parseRecord()
   * 
   * Parses the record string data member loading parsed data into 'mdi' data member
   * guarding if already successfully parsed in which case simply returns true
   */
  public boolean parseRecord()
  {
    return isRecordValid? true:(isRecordValid=RecToObj());
  }

  
  /** 
   * fldtransform_toTarget()
   * -----------------------
   *  - transforms format from native to target
   * 
   *  - target format specs:
   *    --------------------
   *     - left justified fixed width space padded fields
   *     - empty field ==> space padded to field width
   */
  private String fldtransform_toTarget(String fldVal,int fldWidth)
  {
    try {
      
      //log.debug("fldtransform_toTarget(): fldVal='"+fldVal+"', fldWidth='"+fldWidth+"'.");

      if(fldVal.length() < fldWidth) {
        return StringUtilities.leftJustify(fldVal,fldWidth,' ');
      } else if(fldVal.length() > fldWidth)
        return fldVal.substring(0,fldWidth);
      else
        return fldVal;
    }
    catch(Exception e) {
      log.error("fldtransform_toTarget() - Exception: '"+e.getMessage()+"'.  Aborted.");
      return "ERROR";
    }
  }
  
  /**
   * fldtransform_toNative()
   * -----------------------
   *  - trim beg/end whitespace
   *  - " ...   " --> "..."
   */
  private String fldtransform_toNative(String ftpBatchFld)
  {
    return ftpBatchFld.trim();
  }

  /**
   * ObjToRec()
   * 
   * Converts data held in a native Object to a record string
   * 
   * @conditional Data member 'mdi' may be NULL.
   * 
   */
  protected boolean ObjToRec()
  {
    try {
      
      //log.debug("------->ObjToRec(name='"+name+"',record_type='"+record_type+"') - START"+(mdi!=null? " (for MATCHDataItem: "+mdi.toString():""));

      setRecordDefinition();
      
      StringTokenizer fldTkns = new StringTokenizer(definition,"|:",false);
      String token,fldNme,fldValue;
      int fldWidth = 0,fldTWidth = 0,insPnt = 0;
      StringBuffer recordBuf = new StringBuffer(MATCHRECORD_SIZE);
      
      while(fldTkns.hasMoreTokens()) {
        
        fldNme=fldTkns.nextToken();
        fldTWidth=Integer.parseInt(fldTkns.nextToken());
        fldValue = fldtransform_toTarget(getFieldValue(fldNme),fldTWidth);
        
        //log.debug("ObjToRec() INSERTING FLD IN REC: fldNme (width)="+StringUtilities.leftJustify(fldNme,20,' ')+" ("+StringUtilities.leftJustify(Integer.toString(fldTWidth),3,' ')+"), fldValue='"+StringUtilities.leftJustify(fldValue,20,' ')+"', insPnt='"+StringUtilities.leftJustify(Integer.toString(insPnt),3,' ')+"'");
        
        recordBuf.insert(insPnt,DELIM_FIELD+fldValue);
        insPnt += (DELIM_FIELD.length() + fldValue.length());
        
      }
      
      // remove preceeding fld dlm and add trailing record dlm
      recordBuf.delete(0,DELIM_FIELD.length());
      recordBuf.append(DELIM_RECORD);
      
      // set the record strayght
      record=recordBuf.toString();
      
      //log.debug("ObjToRec() record='"+record+"'");
      
      return true;
    }
    catch(Exception e) {
      log.error("ObjToRec - Exception: '"+e.getMessage()+"'.");
      return false;
    }
    
  }
  
  /**
   * getFieldValue()
   * 
   * - retreives the native field data value given the target field name
   * - this is required as there is not a direct (1-to-1) relationship 
   *    for all native and target fields 
   *    (native is hierarchical and target is flat w/ non-native constants)
   * 
   * @param     fldNme Native object data container
   * 
   * @exception Throws exception when encounters an un-mappable field name or otherwise
   * 
   * @return    Field value either of class specific origin or from the member data contianer
   */
  protected abstract String getFieldValue(String fldNme) throws Exception;
  
  /**
   * setFieldValue
   * 
   * Performs the reverse of getFieldValue putting appropriate MATCH data into the member 'mdi'
   * Overwrites value if already existant
   * Only posts actual MATCHDataItem ignoring MATCH record specifiec fields
   */
  protected abstract void setFieldValue(String fldNme, String fldVal) throws Exception;

  /**
   * RecToObj()
   * 
   * Loads record into MATCH data container.
   * 
   */
  protected boolean RecToObj()
  {
    try {
      
      log.debug("MATCHRecord.RecToObj - START (Rec. Type='"+record_type+"'");
      
      // ensure record string exists
      if(record==null || record.length()<1) {
        log.error("Attempt to process null or empty MATCH record string (this: "+toString()+").");
        return false;
      }
      
      setRecordDefinition();
      //log.debug("RecToObj() definition="+definition);
      
      StringTokenizer fldTkns = new StringTokenizer(definition,"|:",false);
      String fldNme,fldVal,fldLen,recsubstr;
      int offset=0,len=0,clen=0;

      while(fldTkns.hasMoreTokens()) {
            
        fldNme = fldTkns.nextToken();
        fldLen = fldTkns.nextToken();
        len=Integer.parseInt(fldLen);
        if(offset>record.length() || (offset+len)>record.length()) {
          //log.warn("Bailing early from parsing MATCH record string. Offset or offset+len exceeds record length.");
          break;
        }
        recsubstr = record.substring(offset,offset+len);
        fldVal = fldtransform_toNative(recsubstr);
        
        //log.debug("MATCHRecord.RecToObj(): Setting field value: fldNme='"+fldNme+"',fldVal='"+fldVal+"'...");
        try {
          setFieldValue(fldNme,fldVal);
        }
        catch(Exception e) {
          log.error("MATCHRecord.RecToObj(): Set field value exception occurred for field '"+fldNme+"' (value: '"+fldVal+"').  Field skipped. (Exception: '"+e.getMessage()+"')");
        }
        
        if((offset += len)>=record.length() && fldTkns.hasMoreTokens()) {
          log.warn("Bailing early from parsing MATCH record string. (offset='"+offset+"', record length='"+record.length()+"').");
          break;
        }

      }
      
      //log.debug("RecToObj() returning true");
      return true;
      
    }
    catch(Exception e) {
      log.error(e.getMessage());
    }
    
    return false;
  }
  
  /**
   * setRecordDefinition()
   * 
   * Attempts to set the 'definition' data member if not alredy via
   * the class (static) functions.
   */
  protected void setRecordDefinition()
    throws Exception
  {
    if(definition!=null)
      return;
    
    // ensure record_type is valid
    if(record_type==MATCHRECTYPE_UNDEFINED)
        throw new Exception("Unable to determine MATCHRecord object record definition string: Undefined object record type.");

    definition = (isRequestRecord()? getRequestRecordDefinition(record_type):getResponseRecordDefinition(record,request_type));
  }
  
  protected String extractRecordValue(String fldNme)
    throws Exception
  {
    //log.debug("extractRecordValue("+fldNme+") - START (MATCHRecord: '"+toString()+"')");
    
    if(record==null)
      throw new Exception("Unable to extract record field value - null record.");
    
    setRecordDefinition();
    
    StringTokenizer fldTkns = new StringTokenizer(definition,"|:",false);
    String defNme,fldVal;
    int defLen=0,offset=0;
    boolean bFound=false;
    
    while(fldTkns.hasMoreTokens()) {
      defNme=fldTkns.nextToken();
      defLen=Integer.parseInt(fldTkns.nextToken());
      if(defNme.equals(fldNme)) {
        bFound=true;
        break;
      } else
        offset += defLen;
    }
    
    if(!bFound)
      throw new Exception("Unable to extract record value: Field name ('"+fldNme+"') not found in definition.");
    if(record.length()<=offset)
      throw new Exception("Unable to extract field '"+fldNme+"' from record string: Calculated offset is greater than or equal to the record length.");
    
    //log.debug("offset='"+offset+"',defLen='"+defLen+"',record='"+record+"'");
    
    fldVal = ((offset+defLen) > record.length())?
                record.substring(offset)
                :record.substring(offset,offset+defLen);
    
    //log.debug("extractRecordValue("+fldNme+")='"+fldVal+"' - END");
    
    return fldVal;
    
  }
  
  /**
   * toString()
   */
  public String toString()
  {
    return "name: '"+name+"', record_type: '"+record_type+"', request_type='"+request_type+"', sequence: '"+sequenceNum+"'";
  }


  // class functions
  
  
  /*
  public static boolean isBaseGeneralField(String fldNme)
  {
    return isBaseField(true,fldNme);
  }
  public static boolean isBasePrincipalField(String fldNme)
  {
    return isBaseField(false,fldNme);
  }
  private static boolean isBaseField(boolean gnrlOrPrin, String fldNme)
  {
    try {
      String[] ary = gnrlOrPrin? BASEFLDNMES_PRINCIPAL:BASEFLDNMES_PRINCIPAL;
    
      for(int i=0; i < ary.length; i++) {
        if(ary.compareToIgnoreCase(fldNme)==0)
          return true;
      }
    }  
    catch(Exception e) {
      log.error("isBaseField() EXCEPTION: "+e.getMsg());
    }
    
    return false;
  }
  */
  
  /**
   * getRequestRecordDefinition()
   * 
   * Retreives the MATCH record request definition string.
   * 
   * @param   record_type   MATCHRECTYPE_{...}
   * 
   * @return                string definition: list of field names and their associated lengths
   */
  protected static String getRequestRecordDefinition(int record_type)
    throws Exception
  {
    String definition=null;
    
    switch(record_type) {
      
      case MATCHRecord.MATCHRECTYPE_ADDHEADER:
        definition = 
           "RECORD_TYPE:3"
          +"|TRANSACTION_CODE:3"
          +"|ACN:7"
          +"|TRANSMISSIONID:17"
          +"|FILLER:982";
        break;
      case MATCHRecord.MATCHRECTYPE_ADDGENERAL:
        definition = 
           "TRANSACTION_CODE:3"
          +"|ACTION_INDICATOR:1"
          +"|ACN:7"
          +"|REC_SEQ_NUM:6"
          +"|RECORD_TYPE:1"
          +"|MERCHANT_CATEGORY_CODE:4"
          +"|MERCH_DATE_OPENED:8"
          +"|MERCH_DATE_CLOSED:8"
          +"|MERCH_NUMBER:15"
          +"|MERCH_BUSINESS_NAME:30"
          +"|MERCH_BUSINESS_DBA:30"
          +"|MAIL_ADDRESS1:30"
          +"|MAIL_ADDRESS2:30"
          +"|MAIL_CITY:15"
          +"|MAIL_STATE:2"
          +"|MAIL_ZIP:10"
          +"|MAIL_COUNTRY:3"
          +"|BUSINESS_PHONE:10"
          +"|CAT_INDICATOR:1"
          +"|MERCH_STATETAX_ID:9"
          +"|MERCH_FEDERAL_TAX_ID:9"
          +"|FILLER:1"
          +"|REQUEST_REASON_CODE:2"
          +"|REQUEST_COMMENTS:200"
          +"|FUTURE_EXPANSION:577";
        break;
      case MATCHRecord.MATCHRECTYPE_ADDPRINCIPAL:
        definition = 
           "TRANSACTION_CODE:3"
          +"|ACTION_INDICATOR:1"
          +"|ACN:7"
          +"|REC_SEQ_NUM:6"
          +"|RECORD_TYPE:1"
          +"|BUSOWNER_LAST_NAME_1:25"
          +"|BUSOWNER_FIRST_NAME_1:15"
          +"|BUSOWNER_NAME_INITIAL_1:1"
          +"|MAILADR_LINE1_1:30"
          +"|MAILADR_LINE2_1:30"
          +"|MAILADR_CITY_1:15"
          +"|MAILADR_STATE_1:2"
          +"|MAILADR_ZIP_1:10"
          +"|MAILADR_COUNTRYCODE_1:3"
          +"|MAILADR_PHONE_1:10"
          +"|BUSOWNER_SSN_1:9"
          +"|BUSOWNER_DRVLICNUM_1:25"
          +"|BUSOWNER_DRVLIC_STATE_1:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_1:3"
          +"|BUSOWNER_LAST_NAME_2:25"
          +"|BUSOWNER_FIRST_NAME_2:15"
          +"|BUSOWNER_NAME_INITIAL_2:1"
          +"|MAILADR_LINE1_2:30"
          +"|MAILADR_LINE2_2:30"
          +"|MAILADR_CITY_2:15"
          +"|MAILADR_STATE_2:2"
          +"|MAILADR_ZIP_2:10"
          +"|MAILADR_COUNTRYCODE_2:3"
          +"|MAILADR_PHONE_2:10"
          +"|BUSOWNER_SSN_2:9"
          +"|BUSOWNER_DRVLICNUM_2:25"
          +"|BUSOWNER_DRVLIC_STATE_2:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_2:3"
          +"|BUSOWNER_LAST_NAME_3:25"
          +"|BUSOWNER_FIRST_NAME_3:15"
          +"|BUSOWNER_NAME_INITIAL_3:1"
          +"|MAILADR_LINE1_3:30"
          +"|MAILADR_LINE2_3:30"
          +"|MAILADR_CITY_3:15"
          +"|MAILADR_STATE_3:2"
          +"|MAILADR_ZIP_3:10"
          +"|MAILADR_COUNTRYCODE_3:3"
          +"|MAILADR_PHONE_3:10"
          +"|BUSOWNER_SSN_3:9"
          +"|BUSOWNER_DRVLICNUM_3:25"
          +"|BUSOWNER_DRVLIC_STATE_3:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_3:3"
          +"|BUSOWNER_LAST_NAME_4:25"
          +"|BUSOWNER_FIRST_NAME_4:15"
          +"|BUSOWNER_NAME_INITIAL_4:1"
          +"|MAILADR_LINE1_4:30"
          +"|MAILADR_LINE2_4:30"
          +"|MAILADR_CITY_4:15"
          +"|MAILADR_STATE_4:2"
          +"|MAILADR_ZIP_4:10"
          +"|MAILADR_COUNTRYCODE_4:3"
          +"|MAILADR_PHONE_4:10"
          +"|BUSOWNER_SSN_4:9"
          +"|BUSOWNER_DRVLICNUM_4:25"
          +"|BUSOWNER_DRVLIC_STATE_4:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_4:3"
          +"|BUSOWNER_LAST_NAME_5:25"
          +"|BUSOWNER_FIRST_NAME_5:15"
          +"|BUSOWNER_NAME_INITIAL_5:1"
          +"|MAILADR_LINE1_5:30"
          +"|MAILADR_LINE2_5:30"
          +"|MAILADR_CITY_5:15"
          +"|MAILADR_STATE_5:2"
          +"|MAILADR_ZIP_5:10"
          +"|MAILADR_COUNTRYCODE_5:3"
          +"|MAILADR_PHONE_5:10"
          +"|BUSOWNER_SSN_5:9"
          +"|BUSOWNER_DRVLICNUM_5:25"
          +"|BUSOWNER_DRVLIC_STATE_5:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_5:3"
          +"|FUTURE_EXPANSION:94";
        break;
      case MATCHRecord.MATCHRECTYPE_ADDTRAILER:
        definition = 
           "RECORD_TYPE:3"
          +"|TRANSACTION_CODE:3"
          +"|ACN:7"
          +"|TRANSMISSIONID:17"
          +"|TOTAL_A_RECORDS:10"
          +"|TOTAL_I_RECORDS:10"
          +"|TOTAL_RECORDS:10"
          +"|FILLER:952";
        break;
      case MATCHRecord.MATCHRECTYPE_INQUIRYHEADER:
        definition = 
           "RECORD_TYPE:3"
          +"|TRANSACTION_CODE:3"
          +"|ACN:7"
          +"|TRANSMISSIONID:17"
          +"|FILLER:982";
        break;
      case MATCHRecord.MATCHRECTYPE_INQUIRYGENERAL:
        definition = 
           "TRANSACTION_CODE:3"
          +"|ACTION_INDICATOR:1"
          +"|ACN:7"
          +"|REC_SEQ_NUM:6"
          +"|RECORD_TYPE:1"
          +"|NUMBER_OF_REQUIRED_PHONETIC_MATCHES:1"
          +"|MERCH_BUSINESS_NAME:30"
          +"|MERCH_BUSINESS_DBA:30"
          +"|MAIL_ADDRESS1:30"
          +"|MAIL_ADDRESS2:30"
          +"|MAIL_CITY:15"
          +"|MAIL_STATE:2"
          +"|MAIL_ZIP:10"
          +"|MAIL_COUNTRY:3"
          +"|BUSINESS_PHONE:10"
          +"|MERCH_STATETAX_ID:9"
          +"|MERCH_FEDERAL_TAX_ID:9"
          +"|TRANSACTION_REF_NUMBER:5"
          +"|REGION_SEARCH_1:3"
          +"|REGION_SEARCH_2:3"
          +"|REGION_SEARCH_3:3"
          +"|REGION_SEARCH_4:3"
          +"|REGION_SEARCH_5:3"
          +"|REGION_SEARCH_6:3"
          +"|REGION_COUNTRY_1:3"
          +"|REGION_COUNTRY_2:3"
          +"|REGION_COUNTRY_3:3"
          +"|REGION_COUNTRY_4:3"
          +"|REGION_COUNTRY_5:3"
          +"|REGION_COUNTRY_6:3"
          +"|REGION_COUNTRY_7:3"
          +"|REGION_COUNTRY_8:3"
          +"|REGION_COUNTRY_9:3"
          +"|REGION_COUNTRY_10:3"
          +"|FUTURE_EXPANSION:762";
        break;
      case MATCHRecord.MATCHRECTYPE_INQUIRYPRINCIPAL:
        definition = 
           "TRANSACTION_CODE:3"
          +"|ACTION_INDICATOR:1"
          +"|ACN:7"
          +"|REC_SEQ_NUM:6"
          +"|RECORD_TYPE:1"
          +"|BUSOWNER_LAST_NAME_1:25"
          +"|BUSOWNER_FIRST_NAME_1:15"
          +"|BUSOWNER_NAME_INITIAL_1:1"
          +"|MAILADR_LINE1_1:30"
          +"|MAILADR_LINE2_1:30"
          +"|MAILADR_CITY_1:15"
          +"|MAILADR_STATE_1:2"
          +"|MAILADR_ZIP_1:10"
          +"|MAILADR_COUNTRYCODE_1:3"
          +"|MAILADR_PHONE_1:10"
          +"|BUSOWNER_SSN_1:9"
          +"|BUSOWNER_DRVLICNUM_1:25"
          +"|BUSOWNER_DRVLIC_STATE_1:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_1:3"
          +"|BUSOWNER_LAST_NAME_2:25"
          +"|BUSOWNER_FIRST_NAME_2:15"
          +"|BUSOWNER_NAME_INITIAL_2:1"
          +"|MAILADR_LINE1_2:30"
          +"|MAILADR_LINE2_2:30"
          +"|MAILADR_CITY_2:15"
          +"|MAILADR_STATE_2:2"
          +"|MAILADR_ZIP_2:10"
          +"|MAILADR_COUNTRYCODE_2:3"
          +"|MAILADR_PHONE_2:10"
          +"|BUSOWNER_SSN_2:9"
          +"|BUSOWNER_DRVLICNUM_2:25"
          +"|BUSOWNER_DRVLIC_STATE_2:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_2:3"
          +"|BUSOWNER_LAST_NAME_3:25"
          +"|BUSOWNER_FIRST_NAME_3:15"
          +"|BUSOWNER_NAME_INITIAL_3:1"
          +"|MAILADR_LINE1_3:30"
          +"|MAILADR_LINE2_3:30"
          +"|MAILADR_CITY_3:15"
          +"|MAILADR_STATE_3:2"
          +"|MAILADR_ZIP_3:10"
          +"|MAILADR_COUNTRYCODE_3:3"
          +"|MAILADR_PHONE_3:10"
          +"|BUSOWNER_SSN_3:9"
          +"|BUSOWNER_DRVLICNUM_3:25"
          +"|BUSOWNER_DRVLIC_STATE_3:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_3:3"
          +"|BUSOWNER_LAST_NAME_4:25"
          +"|BUSOWNER_FIRST_NAME_4:15"
          +"|BUSOWNER_NAME_INITIAL_4:1"
          +"|MAILADR_LINE1_4:30"
          +"|MAILADR_LINE2_4:30"
          +"|MAILADR_CITY_4:15"
          +"|MAILADR_STATE_4:2"
          +"|MAILADR_ZIP_4:10"
          +"|MAILADR_COUNTRYCODE_4:3"
          +"|MAILADR_PHONE_4:10"
          +"|BUSOWNER_SSN_4:9"
          +"|BUSOWNER_DRVLICNUM_4:25"
          +"|BUSOWNER_DRVLIC_STATE_4:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_4:3"
          +"|BUSOWNER_LAST_NAME_5:25"
          +"|BUSOWNER_FIRST_NAME_5:15"
          +"|BUSOWNER_NAME_INITIAL_5:1"
          +"|MAILADR_LINE1_5:30"
          +"|MAILADR_LINE2_5:30"
          +"|MAILADR_CITY_5:15"
          +"|MAILADR_STATE_5:2"
          +"|MAILADR_ZIP_5:10"
          +"|MAILADR_COUNTRYCODE_5:3"
          +"|MAILADR_PHONE_5:10"
          +"|BUSOWNER_SSN_5:9"
          +"|BUSOWNER_DRVLICNUM_5:25"
          +"|BUSOWNER_DRVLIC_STATE_5:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_5:3"
          +"|FUTURE_EXPANSION:94";
        break;
      case MATCHRecord.MATCHRECTYPE_INQUIRYTRAILER:
        definition = 
           "RECORD_TYPE:3"
          +"|TRANSACTION_CODE:3"
          +"|ACN:7"
          +"|TRANSMISSIONID:17"
          +"|TOTAL_A_RECORDS:10"
          +"|TOTAL_I_RECORDS:10"
          +"|TOTAL_RECORDS:10"
          +"|FILLER:952";
        break;
    }
    
    if(definition==null)
      throw new Exception("Unable to determine MATCH record definition (record type: '"+record_type+"'.");
    
    return definition;
      
  }
  
  /**
   * getResponseRecordDefinition()
   * 
   * Retreives the MATCH response record definition string.
   * IMPT: Reponse record definitions are a function of the actual record string itself
   * 
   * @param   record        Record string.
   * @param   request_type  MATCHREQUESTTYPE_{...}.
   * 
   * @return                string definition: list of field names and their associated lengths
   */
  protected static String getResponseRecordDefinition(String record,int request_type)
    throws Exception
  {
    // ensure valid record string
    if(record==null || record.length()<MIN_RECORD_LENGTH)
      throw new Exception("Unable to determine MATCHRecord definition string: null response record specified.");
    
    String definition=null;
    final int record_type = getRecordTypeFromRecord(record,false);
    
    switch(record_type) {
      
      case MATCHRECTYPE_RESPONSEHEADER:
        // header is the same for ALL response types
        definition = 
           "RECORD_TYPE:3"
          +"|TRANSACTION_CODE:3"
          +"|ACN:7"
          +"|RUNDATE:8"
          +"|RUNTIME:6"
          +"|REPORTID:11"
          +"|FILLER:974";
          // RUNDATE format: YYYYMMDD
          // RUNTIME format: HHMMSS
        break;
      case MATCHRECTYPE_RESPONSEGENERAL:
        definition = 
           "TRANSACTION_CODE:3"
          +"|ACTION_INDICATOR:1"
          +"|ACN:7"
          +"|REC_SEQ_NUM:6"
          +"|RECORD_TYPE:1"
          +"|PROCESSED_BY:8"
          +"|SOURCE:1"
          +"|MERCH_BUSINESS_NAME:30"
          +"|MERCH_BUSINESS_DBA:30"
          +"|MAIL_ADDRESS1:30"
          +"|MAIL_ADDRESS2:30"
          +"|MAIL_CITY:15"
          +"|MAIL_STATE:2"
          +"|MAIL_ZIP:10"
          +"|MAIL_COUNTRY:3"
          +"|BUSINESS_PHONE:10"
          +"|MERCH_STATETAX_ID:9"
          +"|MERCH_FEDERAL_TAX_ID:9";
        switch(record.charAt(3)) {  // action identifier field
          case 'A':
            definition +=
               "|MERCHANT_CATEGORY_CODE:4"
              +"|MERCH_DATE_OPENED:8"
              +"|MERCH_DATE_CLOSED:8"
              +"|MERCH_NUMBER:15"
              +"|CAT_INDICATOR:1"
              +"|FILLER:1"
              +"|REQUEST_REASON_CODE:2"
              +"|REQUEST_COMMENTS:200"
              +"|MASTERCARD_REF_NUMBER:20"
              +"|FUTURE_EXPANSION:548";
            break;
          case 'E':
            // the ambiguous case (could be Add Error OR could be Inquiry Error)
            switch(request_type) {
              case MATCHREQUESTTYPE_ADD:
                definition +=
                   "|MERCHANT_CATEGORY_CODE:4"
                  +"|MERCH_DATE_OPENED:8"
                  +"|MERCH_DATE_CLOSED:8"
                  +"|MERCH_NUMBER:15"
                  +"|CAT_INDICATOR:1"
                  +"|FILLER:1"
                  +"|REQUEST_REASON_CODE:2"
                  +"|REQUEST_COMMENTS:200"
                  +"|ERROR_CODES:30"
                  +"|FUTURE_EXPANSION:538";
                break;
              case MATCHREQUESTTYPE_INQUIRY:
                definition +=
                   "|NUMBER_OF_REQUIRED_PHONETIC_MATCHES:1"
                  +"|TRANSACTION_REF_NUMBER:5"
                  +"|REGION_SEARCH_1:3"
                  +"|REGION_SEARCH_2:3"
                  +"|REGION_SEARCH_3:3"
                  +"|REGION_SEARCH_4:3"
                  +"|REGION_SEARCH_5:3"
                  +"|REGION_SEARCH_6:3"
                  +"|REGION_COUNTRY_1:3"
                  +"|REGION_COUNTRY_2:3"
                  +"|REGION_COUNTRY_3:3"
                  +"|REGION_COUNTRY_4:3"
                  +"|REGION_COUNTRY_5:3"
                  +"|REGION_COUNTRY_6:3"
                  +"|REGION_COUNTRY_7:3"
                  +"|REGION_COUNTRY_8:3"
                  +"|REGION_COUNTRY_9:3"
                  +"|REGION_COUNTRY_10:3"
                  +"|ERROR:30"
                  +"|FUTURE_EXPANSION:722";
                break;
            }
            break;
          case 'N':
            definition +=
               "|TRANSACTION_REF_NUMBER:5"
              +"|REGION_SEARCH_1:3"
              +"|REGION_SEARCH_2:3"
              +"|REGION_SEARCH_3:3"
              +"|REGION_SEARCH_4:3"
              +"|REGION_SEARCH_5:3"
              +"|REGION_SEARCH_6:3"
              +"|REGION_COUNTRY_1:3"
              +"|REGION_COUNTRY_2:3"
              +"|REGION_COUNTRY_3:3"
              +"|REGION_COUNTRY_4:3"
              +"|REGION_COUNTRY_5:3"
              +"|REGION_COUNTRY_6:3"
              +"|REGION_COUNTRY_7:3"
              +"|REGION_COUNTRY_8:3"
              +"|REGION_COUNTRY_9:3"
              +"|REGION_COUNTRY_10:3"
              +"|MASTERCARD_REF_NUMBER:20"
              +"|FUTURE_EXPANSION:734";
            break;
          case 'P': // fall through to 'R'
          case 'R':
            definition +=
               "|TRANSACTION_REF_NUMBER:5"
              +"|REGION_SEARCH_1:3"
              +"|REGION_SEARCH_2:3"
              +"|REGION_SEARCH_3:3"
              +"|REGION_SEARCH_4:3"
              +"|REGION_SEARCH_5:3"
              +"|REGION_SEARCH_6:3"
              +"|REGION_COUNTRY_1:3"
              +"|REGION_COUNTRY_2:3"
              +"|REGION_COUNTRY_3:3"
              +"|REGION_COUNTRY_4:3"
              +"|REGION_COUNTRY_5:3"
              +"|REGION_COUNTRY_6:3"
              +"|REGION_COUNTRY_7:3"
              +"|REGION_COUNTRY_8:3"
              +"|REGION_COUNTRY_9:3"
              +"|REGION_COUNTRY_10:3"
              +"|MASTERCARD_REF_NUMBER:20"
              +"|BUSINESS_NAME_MATCH:1"
              +"|BUSINESS_DBA_MATCH:1"
              +"|BUSINESS_ADDRESS_MATCH:1"
              +"|BUSINESS_TELEPHONE_MATCH:1"
              +"|STATETAX_ID_MATCH:1"
              +"|FEDERAL_TAX_ID_MATCH:1"
              +"|FUTURE_EXPANSION:728";
            break;
          case 'M': // fall through to 'H'
          case 'H':
            definition +=
               "|MERCHANT_CATEGORY_CODE:4"
              +"|MERCH_DATE_OPENED:8"
              +"|MERCH_DATE_CLOSED:8"
              +"|MERCH_NUMBER:15"
              +"|BUSINESS_NAME_MATCH:1"
              +"|BUSINESS_DBA_MATCH:1"
              +"|BUSINESS_ADDRESS_MATCH:1"
              +"|BUSINESS_TELEPHONE_MATCH:1"
              +"|CAT_INDICATOR:1"
              +"|STATETAX_ID_MATCH:1"
              +"|FEDERAL_TAX_ID_MATCH:1"
              +"|FILLER:1"
              +"|REQUEST_REASON_CODE:2"
              +"|MASTERCARD_REF_NUMBER:20"
              +"|VISA_BIN:6"
              +"|ORIG_MASTERCARD_REF_NUMBER:20"
              +"|FUTURE_EXPANSION:716";
            break;
          case 'Q': // fall through to 'Y'
          case 'Y':
            definition +=
               "|BUSINESS_NAME_MATCH:1"
              +"|BUSINESS_DBA_MATCH:1"
              +"|BUSINESS_ADDRESS_MATCH:1"
              +"|BUSINESS_TELEPHONE_MATCH:1"
              +"|STATETAX_ID_MATCH:1"
              +"|FEDERAL_TAX_ID_MATCH:1"
              +"|TRANSACTION_REF_NUMBER:5"
              +"|REGION_SEARCH_1:3"
              +"|REGION_SEARCH_2:3"
              +"|REGION_SEARCH_3:3"
              +"|REGION_SEARCH_4:3"
              +"|REGION_SEARCH_5:3"
              +"|REGION_SEARCH_6:3"
              +"|REGION_COUNTRY_1:3"
              +"|REGION_COUNTRY_2:3"
              +"|REGION_COUNTRY_3:3"
              +"|REGION_COUNTRY_4:3"
              +"|REGION_COUNTRY_5:3"
              +"|REGION_COUNTRY_6:3"
              +"|REGION_COUNTRY_7:3"
              +"|REGION_COUNTRY_8:3"
              +"|REGION_COUNTRY_9:3"
              +"|REGION_COUNTRY_10:3"
              +"|ORIG_MASTERCARD_REF_NUMBER:20"
              +"|FUTURE_EXPANSION:748";
            break;
        }
        break;
      case MATCHRECTYPE_RESPONSEPRINCIPAL:
        definition = 
           "TRANSACTION_CODE:3"
          +"|ACTION_INDICATOR:1"
          +"|ACN:7"
          +"|REC_SEQ_NUM:6"
          +"|RECORD_TYPE:1"
          +"|PROCESSED_BY:8"
          +"|SOURCE:1"
          +"|BUSOWNER_LAST_NAME_1:25"
          +"|BUSOWNER_FIRST_NAME_1:15"
          +"|BUSOWNER_NAME_INITIAL_1:1"
          +"|MAILADR_LINE1_1:30"
          +"|MAILADR_LINE2_1:30"
          +"|MAILADR_CITY_1:15"
          +"|MAILADR_STATE_1:2"
          +"|MAILADR_ZIP_1:10"
          +"|MAILADR_COUNTRYCODE_1:3"
          +"|MAILADR_PHONE_1:10"
          +"|BUSOWNER_SSN_1:9"
          +"|BUSOWNER_DRVLICNUM_1:25"
          +"|BUSOWNER_DRVLIC_STATE_1:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_1:3"
          +"|BUSOWNER_LAST_NAME_2:25"
          +"|BUSOWNER_FIRST_NAME_2:15"
          +"|BUSOWNER_NAME_INITIAL_2:1"
          +"|MAILADR_LINE1_2:30"
          +"|MAILADR_LINE2_2:30"
          +"|MAILADR_CITY_2:15"
          +"|MAILADR_STATE_2:2"
          +"|MAILADR_ZIP_2:10"
          +"|MAILADR_COUNTRYCODE_2:3"
          +"|MAILADR_PHONE_2:10"
          +"|BUSOWNER_SSN_2:9"
          +"|BUSOWNER_DRVLICNUM_2:25"
          +"|BUSOWNER_DRVLIC_STATE_2:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_2:3"
          +"|BUSOWNER_LAST_NAME_3:25"
          +"|BUSOWNER_FIRST_NAME_3:15"
          +"|BUSOWNER_NAME_INITIAL_3:1"
          +"|MAILADR_LINE1_3:30"
          +"|MAILADR_LINE2_3:30"
          +"|MAILADR_CITY_3:15"
          +"|MAILADR_STATE_3:2"
          +"|MAILADR_ZIP_3:10"
          +"|MAILADR_COUNTRYCODE_3:3"
          +"|MAILADR_PHONE_3:10"
          +"|BUSOWNER_SSN_3:9"
          +"|BUSOWNER_DRVLICNUM_3:25"
          +"|BUSOWNER_DRVLIC_STATE_3:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_3:3"
          +"|BUSOWNER_LAST_NAME_4:25"
          +"|BUSOWNER_FIRST_NAME_4:15"
          +"|BUSOWNER_NAME_INITIAL_4:1"
          +"|MAILADR_LINE1_4:30"
          +"|MAILADR_LINE2_4:30"
          +"|MAILADR_CITY_4:15"
          +"|MAILADR_STATE_4:2"
          +"|MAILADR_ZIP_4:10"
          +"|MAILADR_COUNTRYCODE_4:3"
          +"|MAILADR_PHONE_4:10"
          +"|BUSOWNER_SSN_4:9"
          +"|BUSOWNER_DRVLICNUM_4:25"
          +"|BUSOWNER_DRVLIC_STATE_4:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_4:3"
          +"|BUSOWNER_LAST_NAME_5:25"
          +"|BUSOWNER_FIRST_NAME_5:15"
          +"|BUSOWNER_NAME_INITIAL_5:1"
          +"|MAILADR_LINE1_5:30"
          +"|MAILADR_LINE2_5:30"
          +"|MAILADR_CITY_5:15"
          +"|MAILADR_STATE_5:2"
          +"|MAILADR_ZIP_5:10"
          +"|MAILADR_COUNTRYCODE_5:3"
          +"|MAILADR_PHONE_5:10"
          +"|BUSOWNER_SSN_5:9"
          +"|BUSOWNER_DRVLICNUM_5:25"
          +"|BUSOWNER_DRVLIC_STATE_5:2"
          +"|BUSOWNER_DRVLIC_COUNTRY_5:3";
        switch(record.charAt(3)) {  // action identifier field
          case 'A':
            definition +=
              "|FUTURE_EXPANSION:548";
            break;
          case 'E':
            // the ambiguous case (could be Add Error OR could be Inquiry Error)
            switch(request_type) {
              case MATCHREQUESTTYPE_ADD:
                definition +=
                  "|FUTURE_EXPANSION:548";
                break;
              case MATCHREQUESTTYPE_INQUIRY:
                definition +=
                  "|FUTURE_EXPANSION:85";
                break;
            }
            break;
          case 'N':
            definition +=
              "|FUTURE_EXPANSION:85";
            break;
          case 'P': // fall through to 'R'
          case 'R':
            definition +=
               "|BUSOWNER_LAST_NAME_MATCH_1:1"
              +"|BUSOWNER_ADDRESS_MATCH_1:1"
              +"|BUSOWNER_TELEPHONE_MATCH_1:1"
              +"|BUSOWNER_SSN_MATCH_1:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_1:1"
              +"|BUSOWNER_LAST_NAME_MATCH_2:1"
              +"|BUSOWNER_ADDRESS_MATCH_2:1"
              +"|BUSOWNER_TELEPHONE_MATCH_2:1"
              +"|BUSOWNER_SSN_MATCH_2:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_2:1"
              +"|BUSOWNER_LAST_NAME_MATCH_3:1"
              +"|BUSOWNER_ADDRESS_MATCH_3:1"
              +"|BUSOWNER_TELEPHONE_MATCH_3:1"
              +"|BUSOWNER_SSN_MATCH_3:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_3:1"
              +"|BUSOWNER_LAST_NAME_MATCH_4:1"
              +"|BUSOWNER_ADDRESS_MATCH_4:1"
              +"|BUSOWNER_TELEPHONE_MATCH_4:1"
              +"|BUSOWNER_SSN_MATCH_4:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_4:1"
              +"|BUSOWNER_LAST_NAME_MATCH_5:1"
              +"|BUSOWNER_ADDRESS_MATCH_5:1"
              +"|BUSOWNER_TELEPHONE_MATCH_5:1"
              +"|BUSOWNER_SSN_MATCH_5:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_5:1"
              +"|FUTURE_EXPANSION:60";
            break;
          case 'M': // fall through to 'H'
          case 'H':
            definition +=
               "|BUSOWNER_LAST_NAME_MATCH_1:1"
              +"|BUSOWNER_ADDRESS_MATCH_1:1"
              +"|BUSOWNER_TELEPHONE_MATCH_1:1"
              +"|BUSOWNER_SSN_MATCH_1:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_1:1"
              +"|BUSOWNER_LAST_NAME_MATCH_2:1"
              +"|BUSOWNER_ADDRESS_MATCH_2:1"
              +"|BUSOWNER_TELEPHONE_MATCH_2:1"
              +"|BUSOWNER_SSN_MATCH_2:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_2:1"
              +"|BUSOWNER_LAST_NAME_MATCH_3:1"
              +"|BUSOWNER_ADDRESS_MATCH_3:1"
              +"|BUSOWNER_TELEPHONE_MATCH_3:1"
              +"|BUSOWNER_SSN_MATCH_3:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_3:1"
              +"|BUSOWNER_LAST_NAME_MATCH_4:1"
              +"|BUSOWNER_ADDRESS_MATCH_4:1"
              +"|BUSOWNER_TELEPHONE_MATCH_4:1"
              +"|BUSOWNER_SSN_MATCH_4:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_4:1"
              +"|BUSOWNER_LAST_NAME_MATCH_5:1"
              +"|BUSOWNER_ADDRESS_MATCH_5:1"
              +"|BUSOWNER_TELEPHONE_MATCH_5:1"
              +"|BUSOWNER_SSN_MATCH_5:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_5:1"
              +"|FUTURE_EXPANSION:60";
            break;
          case 'Q': // fall through to 'Y'
          case 'Y':
            definition +=
               "|BUSOWNER_LAST_NAME_MATCH_1:1"
              +"|BUSOWNER_ADDRESS_MATCH_1:1"
              +"|BUSOWNER_TELEPHONE_MATCH_1:1"
              +"|BUSOWNER_SSN_MATCH_1:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_1:1"
              +"|BUSOWNER_LAST_NAME_MATCH_2:1"
              +"|BUSOWNER_ADDRESS_MATCH_2:1"
              +"|BUSOWNER_TELEPHONE_MATCH_2:1"
              +"|BUSOWNER_SSN_MATCH_2:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_2:1"
              +"|BUSOWNER_LAST_NAME_MATCH_3:1"
              +"|BUSOWNER_ADDRESS_MATCH_3:1"
              +"|BUSOWNER_TELEPHONE_MATCH_3:1"
              +"|BUSOWNER_SSN_MATCH_3:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_3:1"
              +"|BUSOWNER_LAST_NAME_MATCH_4:1"
              +"|BUSOWNER_ADDRESS_MATCH_4:1"
              +"|BUSOWNER_TELEPHONE_MATCH_4:1"
              +"|BUSOWNER_SSN_MATCH_4:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_4:1"
              +"|BUSOWNER_LAST_NAME_MATCH_5:1"
              +"|BUSOWNER_ADDRESS_MATCH_5:1"
              +"|BUSOWNER_TELEPHONE_MATCH_5:1"
              +"|BUSOWNER_SSN_MATCH_5:1"
              +"|BUSOWNER_DRVLICNUM_MATCH_5:1"
              +"|FUTURE_EXPANSION:60";
            break;
        }
        break;
      case MATCHRECTYPE_RESPONSETRAILER:
        // determine response trailer type by the number of trailing space chars
        // assumption: NO white space chars exist at start of record string
        final String twss = record.trim();
        //log.debug("twss="+twss);
        switch(twss.length()) {
          case (MATCHRECORD_SIZE-952): // Number 1 type
            definition =
               "TRAILER_LITERAL:3"
              +"|ACN:7"
              +"|TOTAL_RECORDS:10"
              +"|TOTAL_N_RECORDS:10"
              +"|TOTAL_P_RECORDS:10"
              +"|TOTAL_M_RECORDS:10"
              +"|TOTAL_Q_RECORDS:10"
              +"|FILLER:952";
            break;
          case (MATCHRECORD_SIZE-942): // Number 2 type
            definition =
               "TRAILER_LITERAL:3"
              +"|ACN:7"
              +"|TOTAL_RECORDS:10"
              +"|TOTAL_R_RECORDS:10"
              +"|TOTAL_M_RECORDS:10"
              +"|TOTAL_H_RECORDS:10"
              +"|TOTAL_Q_RECORDS:10"
              +"|TOTAL_Y_RECORDS:10"
              +"|FILLER:942";
            break;
          case (MATCHRECORD_SIZE-992): // Number 3 type
            definition =
               "TRAILER_LITERAL:3"
              +"|ACN:7"
              +"|TOTAL_RECORDS:10"
              +"|FILLER:992";
            break;
        }
        break;
    }
    
    if(definition==null)
      throw new Exception("Unable to determine MATCH record definition (record: '"+record+"').");
    
    //log.debug("Response record definition: '"+definition+"'");
    return definition;
  }

  /**
   * IsResponseOriginatedRecord()
   * 
   * Determines whether the record was part of the originating request
   * OR is a match related response record (not a record sent in corres. request).
   * as a function of the record's action_indicator field.
   * 
   * Match related response records are
   * ALWAYS related to a request originated response record via:
   *   {request originated response record}.MASTERCARD_REF_NUMBER 
   *    <1--M>
   *   {response originated response record}.ORIG_MASTERCARD_REF_NUMBER
   * 
   */
  public static boolean IsResponseOriginatedRecord(char action_indicator)
  {
    return (action_indicator=='M' || action_indicator=='H' || action_indicator=='Q' || action_indicator=='Y');
  }
  
  /**
   * HasRelatedResponseOriginatedRecords()
   * 
   * Is a request originated response record with related 
   * response originated records?
   * 
   * In other words, if a request comes back as possible match ('P') or retroactive possible match ('R'),
   * this means there are associated records of either 'M','H','Q' or 'Y'
   * 
   */
  public static boolean HasRelatedResponseOriginatedRecords(char action_indicator)
  {
    return (action_indicator=='P' || action_indicator=='R');
  }

  /**
   * getRecordTypeFromRecord()
   * 
   * Determines the record type from a record string.
   * 
   * @param  record              record string
   * @param  bRequestOrResponse  true: record string is a request record, false: record string is a response record
   * 
   * @return MATCHRECTYPE_{...}
   */
  protected static int getRecordTypeFromRecord(String record,boolean bRequestOrResponse)
  {
    final String htrectype = record.substring(0,3);
    
    if(htrectype.equals("ADH"))
      return MATCHRECTYPE_ADDHEADER;
    else if(htrectype.equals("ADT"))
      return MATCHRECTYPE_ADDTRAILER;
    else if(htrectype.equals("IDH"))
      return MATCHRECTYPE_INQUIRYHEADER;
    else if(htrectype.equals("IDT"))
      return MATCHRECTYPE_INQUIRYTRAILER;
    else if(htrectype.equals("RHD"))
      return MATCHRECTYPE_RESPONSEHEADER;
    else if(htrectype.equals("TRL"))
      return MATCHRECTYPE_RESPONSETRAILER;
    
    final char ai = record.charAt(3);         // action identifier field
    final char drectype = record.charAt(17);  // record type field 
    
    //log.debug("getRecordTypeFromRecord() - ai="+ai);
    //log.debug("getRecordTypeFromRecord() - drectype="+drectype);
    
    if(bRequestOrResponse) {
      // request
      switch(drectype) {
        case '1': // general
          switch(ai) {
            case 'A':
              return MATCHRECTYPE_ADDGENERAL;
            case 'I':
              return MATCHRECTYPE_INQUIRYGENERAL;
          }
          break;
        case '2': // principal
          switch(ai) {
            case 'A':
              return MATCHRECTYPE_ADDPRINCIPAL;
            case 'I':
              return MATCHRECTYPE_INQUIRYPRINCIPAL;
          }
          break;
      }
    } else {
      // response record
      switch(drectype) {
        case '1': // general
          return MATCHRECTYPE_RESPONSEGENERAL;
        case '2': // principal
          return MATCHRECTYPE_RESPONSEPRINCIPAL;
      }
    }
    
    log.error("Indeterminate record type for record: '"+record+"'.");
    return MATCHRECTYPE_UNDEFINED;
  }
  
  
  /**
   * getRequestTypeFromResponseRecord()
   * 
   * Determines the originating request type from a response record string.
   * Assumes that 'record' param is a response record.
   * 
   * @param  record  Record string.
   * 
   * @return MATCHREQUESTTYPE_{...}
   */
  protected static int getRequestTypeFromResponseRecord(String record)
    throws Exception
  {
    if(record==null || record.length()<MIN_RECORD_LENGTH)
      throw new Exception("Unable to determine Request type from Response record: Invalid record string '"+record+"' specified.");
    
    final String htrectype = record.substring(0,3);
    
    // if record is a header or trailer, request type is undefined
    if(htrectype.equals("RHD") || htrectype.equals("TRL")) {
      log.warn("Get Request type failed: Request type undefined for response header and trailer records (record: '"+record+"').");
      return MATCHREQUESTTYPE_UNDEFINED;
    }
    
    final char responseActnIdfr = record.charAt(3);
    int request_type = MATCHREQUESTTYPE_UNDEFINED;
    
    switch(responseActnIdfr) {
      case 'E':
        // this is the only ambiguous case so to dis-ambiguate:
        //   from the MATCH record specs doc
        //   we know in the case of 'E' for Response Action Identifier field,
        //   that the ending FUTURE_EXPANSION field is:
        //    538 ' ' chars for ADD     General Response records
        //    722 ' ' chars for INQUIRY General Response records
        // ensure record of type General
        if(record.charAt(17)!='1')
          throw new Exception("Unable to determine request type from specified record string: '"+record+"'.  Record is of indeterminate type (non-General type).");
        request_type=(record.length()>289? MATCHREQUESTTYPE_ADD:MATCHREQUESTTYPE_INQUIRY);
        break;
      case 'A':
        request_type=MATCHREQUESTTYPE_ADD;
        break;
      case 'N':
      case 'P':
      case 'R':
      case 'M':
      case 'H':
      case 'Q':
      case 'Y':
        request_type=MATCHREQUESTTYPE_INQUIRY;
        break;
      case 'D': // merchant info Deleted from MATCH db (per written request)
        request_type=MATCHREQUESTTYPE_INQUIRY;
        break;
      case 'C': // merchant info Corrected in MATCH db (per written request)
        request_type=MATCHREQUESTTYPE_INQUIRY;
        break;
      default:
        throw new Exception("Unable to determine request type from specified record string ('"+record+"'):  Unhandled response action identifier encountered '"+responseActnIdfr+"'.");
    }
    
    log.debug("getRequestTypeFromResponseRecord() request_type='"+request_type+"'.");
    
    return request_type;
  }
  
} // class MATCHRecord
