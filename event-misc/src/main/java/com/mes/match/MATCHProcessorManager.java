package com.mes.match;

import java.util.Enumeration;
// log4j classes.
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.PropertiesFile;

/**
 * MATCHProcessorManager
 * -------------------
 *  - Manages the goings on of MATCH requests
 */
final class MATCHProcessorManager extends Object implements Runnable
{
  // create class log category
  static Logger log = Logger.getLogger(MATCHProcessorManager.class);

  // Singleton
  public static MATCHProcessorManager getInstance()
  {
    if(_instance==null) {
      try {
        _instance = new MATCHProcessorManager();
      }
      catch(Exception e) {
        _instance = null;
        log.error("MATCHProcessorManager - Unable to instantiate due to exception: "+e.getMessage()+".");
      }
    }
    return _instance;
  }
  private static MATCHProcessorManager _instance = null;
  ///
  
  // constants
  public static final long      PROCESS_FREQUENCY_MILIS_DEFAULT     = (1000*60*60*1);  // default to every hour
  public static final boolean   TEST_OR_LIVE_MODE_DEFAULT           = true;            // default to test mode
  
  
  // data members
  private long                                    PROCESS_FREQUENCY_MILIS;
  private boolean                                 isStarted;
  private Thread                                  clockThread;
  private boolean                                 bResourceLock;
  private PersistQueue_MATCHDataQueueItem_Input   requests;
  private PersistQueue_MATCHDataQueueItem_Output  responses;
  private boolean                                 bTestOrLive;


  // construction
  private MATCHProcessorManager()
    throws Exception
  {
    // load certain data members from properties file
    PropertiesFile props = new PropertiesFile("MATCHProcessorManager.properties");
    PROCESS_FREQUENCY_MILIS=props.getLong("PROCESS_FREQUENCY_MILIS",PROCESS_FREQUENCY_MILIS_DEFAULT);
    log.info("MATCH Process frequency set at "+(PROCESS_FREQUENCY_MILIS/(1000*60))+" minutes.");
    bTestOrLive=props.getBoolean("TEST_OR_LIVE_MODE",TEST_OR_LIVE_MODE_DEFAULT);
    log.info("MATCH Processor in "+(bTestOrLive? "TEST":"**LIVE**")+" mode.");

    isStarted=false;
    clockThread=null;
    bResourceLock=false;

    requests = new PersistQueue_MATCHDataQueueItem_Input();
    responses = new PersistQueue_MATCHDataQueueItem_Output();

    // propagate test/live toggle to MFE proxy
    MATCHFileExpressProxy.setTestMode(bTestOrLive);
  }
  
  // static startup/shutdown routines
  public static final boolean startup()
  {
    return (getInstance()==null)?
      false : _instance.Startup();
  }
  
  public static final boolean isStarted()
  {
    return (_instance==null)?
      false : _instance.isStarted;
  }
  
  public static final void shutdown()
  {
    if(_instance!=null)
      _instance.Shutdown();
  }
  

  /**
   * run()
   */
  public void run()
  {
    Thread myThread = Thread.currentThread();
    
    if(!isStarted && (clockThread == myThread)) {
      try {
        
        // Enforce Direct db connections
        SQLJConnectionBase.setConnectString(SQLJConnectionBase.getDirectConnectString());
        
        isStarted=true;
        log.info("MATCHProcessorManager is now started.");
      }
      catch(Exception e) {
        log.error(e.getMessage());
        isStarted=false;
      }
    }
    
    while (clockThread == myThread) {
      
      // process
      //log.warn("WARNING: go() method disabled.");
      go();
      
      // go back to your room
      try {
        Thread.sleep(PROCESS_FREQUENCY_MILIS);
      }
      catch(Exception e) {
        log.error("MATCHProcessorManager - Thread.sleep exception occurred: "+e.getMessage());
      }

    }
    
    if(isStarted && clockThread==null) {
      isStarted=false;
      log.info("MATCHProcessorManager is now shutdown.");
    }
    
  }

  
  private final boolean Startup()
  {
    if(isStarted)
      return true;
    
    try {
      
      // spawn the clock thread
      log.info("Starting ProcMngrClock thread...");
      this.clockThread = new Thread(this, "ProcMngrClock");
      this.clockThread.start();
      
      // wait max 60 seconds for thread to start
      int milli = 60*1000;
      while(--milli!=0 && !isStarted)
        Thread.currentThread().sleep(1);
      
    }
    catch(Exception e) {
      log.error(e.getMessage());
    }
    
    return isStarted;
  }
  
  private synchronized final void Shutdown()
  {
    while(bResourceLock) {
      try {
        wait();
      }
      catch (InterruptedException e) {
      }
    }
    bResourceLock=true;
    
    try {
    
      if(!isStarted)
        return;
      
      log.info("MATCHProcessorManager beginning shutdown..");

      clockThread = null;

      // wait for clock thread to stop
      while(isStarted)
        Thread.currentThread().sleep(1);
    
    }
    catch(Exception e) {
      log.error(e.getMessage());
    }
    finally {
      bResourceLock=false;
      notifyAll();
    }
  }
  
  /**
   * go()
   */
  private synchronized final void go()
  {
    while(bResourceLock) {
      try {
        wait();
      }
      catch (InterruptedException e) {
      }
    }
    bResourceLock=true;
    
    try {

      log.info(""); // blank line
      log.info("*** MATCHProcessorManager.go() - START *************");
        
      final IMPManager impMngr = IMPManager.getInstance();
      log.info("IMP Manager retreived.");
      
      log.info("Beginning MFE proxy session...");
      final MATCHFileExpressProxy mfeProxy = MATCHFileExpressProxy.getInstance();
      mfeProxy.beginSession();
      
      log.info("Beginning Persistence session...");
      PersistQueue_MATCHDataQueueItem.beginSession();
        
      // ** Process requests **
      //log.warn("IMPT: Request processing turned OFF.");
      try {
        log.info("Loading new MATCH requests...");
        requests.load();
        if(requests.getNumElements()>0) {
          log.info("Loaded "+requests.getNumElements()+" new MATCH requests.");
          
          // MATCH request generation
          //log.warn("MATCH request generation - TURNED OFF");
          // create the batches
          log.info("Generating MATCH request file for current requests...");
          try {
            MATCHFile.generateRequestFile(requests,bTestOrLive);
          }
          catch(Exception e) {
            log.error("An error occurred attempting to generate request file: "+e.getMessage()+".");
          }

          // IMP check
          try {
            log.info("Performing Internal Match check for current requests...");
            impMngr.doInternalMatchCheck(requests);
          }
          catch(Exception e) {
            log.error("An error occurred while performing Internal Mactch check: '"+e.getMessage()+"'.");
          }
          
          log.info("Persisting match just processed Match requests...");
          requests.save();
          requests.unload();
          
        } else
          log.info("No new MATCH requests were found.");
      }
      catch(Exception e) {
        log.error("Process MATCH requests exception: '"+e.getMessage()+"'.");
      }
      
      MATCHDataQueueItem mdqi=null;
      
      // ** Process responses **
      //log.warn("IMPT: Response processing turned OFF.");
      try {
        log.info("Checking for MATCH responses...");
        MATCHFile.parseResponseFiles(responses,bTestOrLive);
        if(responses.getNumElements()>0) {
          
          //log.warn("IMPT: App Queue Logic Ops turned OFF.");
          // *** App Queue Logic Ops ***
          // ***************************
          //  1.  No Match          ==>   No action.
          //  2.  Possible Match    ==>   Add to 'MATCH-Credit' Queue.
          //  3.  ERROR             ==>   Add to 'MATCH-Error' Queue.
          for(Enumeration e=responses.getQueueElmntKeys();e.hasMoreElements();) {
            mdqi=(MATCHDataQueueItem)responses.getElement((String)e.nextElement());
            
            if(mdqi.getResponseAction().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_POSSIBLEMATCH) || mdqi.getResponseAction().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_RETROPOSSIBLEMATCH)) {
              mdqi.setQueueAction(MATCHDataQueueItem.QUEUEACTION_ADDTOMATCHCREDIT);
            } else if(mdqi.getResponseAction().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_INQUIRYERROR)) {
              mdqi.setQueueAction(MATCHDataQueueItem.QUEUEACTION_ADDTOMATCHERROR);
            } else if(mdqi.getResponseAction().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_NOMATCH)) {
              mdqi.setQueueAction(MATCHDataQueueItem.QUEUEACTION_RELEASE);
            }
          }
          
          //log.warn("NOT PERSISTING Response parsing status!");
          responses.save();
          responses.unload();
          
          log.info("Parsing MATCH responses COMPLETE");
        } else
          log.info("No MATCH responses found.");
      }
      catch(Exception e) {
        log.error("Process MATCH responses exception: '"+e.getMessage()+"'.");
      }
      
      log.info("Ending Persistence session...");
      PersistQueue_MATCHDataQueueItem.endSession();
      
      log.info("Ending MFE proxy session...");
      mfeProxy.endSession();
        
      log.info("*** MATCHProcessorManager.go() - END *************");
      log.info(""); // blank line
    }
    catch(Exception e) {
      log.error("go() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      bResourceLock=false;
      notifyAll();
    }
  
  }

  public synchronized String toString()
  {
    return
       System.getProperty("line.separator")+"==========================================="
      +System.getProperty("line.separator")+"Number of requests:  "+requests.getNumElements()
      +System.getProperty("line.separator")+"Number of responses: "+responses.getNumElements()
      +System.getProperty("line.separator")+"==========================================="+System.getProperty("line.separator")
      ;
  }

} // class MATCHProcessorManager
