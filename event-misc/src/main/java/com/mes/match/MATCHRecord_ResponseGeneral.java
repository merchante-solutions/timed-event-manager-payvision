package com.mes.match;

import java.util.Enumeration;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;

/**
 * MATCHRecord_ResponseGeneral
 * -------------------
 * - extends MATCHRecord encapsulating header spec. record logic
 */
class MATCHRecord_ResponseGeneral extends MATCHRecord
{
  // create class log category
  static Category log = Category.getInstance(MATCHRecord_ResponseGeneral.class.getName());

  // constants
  // (none)

  // data members
  protected String          mrid;             // MATCH Request ID - used when parsing response records
  
  // construction
  protected MATCHRecord_ResponseGeneral(String name,int sequenceNum)
  {
    super(name,MATCHRecord.MATCHRECTYPE_RESPONSEGENERAL,sequenceNum);
    
    mrid="";
  }
  
  public String getMRID() { return mrid; }
  
  public void setMRID(String mrid)
  {
    if(mrid!=null)
      this.mrid=mrid;
  }
  
  /**
   * getFieldValue()
   * 
   * Sub-class override.
   */
  protected String getFieldValue(String fldNme)
    throws Exception
  {
    String value;
    
    if(fldNme.equals("TRANSACTION_CODE"))
      value="651";
    else if(fldNme.equals("ACTION_INDICATOR"))
      value="P";
    else if(fldNme.equals("RECORD_TYPE"))
      value="1";
    else if(mdi!=null)
      value=mdi.getGeneralDataItem(fldNme);
    else
      value="";
    
    return value;
  }
  
  /**
   * setFieldValue()
   * 
   * Sub-class override.
   * NOTE: many fields in the general response are currently ignored including the {...}_MATCH ones
   */
  protected void setFieldValue(String fldNme, String fldVal)
    throws Exception
  {
    if(fldNme==null || fldNme.length()<1)
      throw new Exception("Unable to set general response field value: Null or empty field name specified.");
    if(fldVal==null)
      fldVal="";
    
    // ensure data container exists
    if(mdi==null)
      mdi = new MATCHDataItem();
    
    if(fldNme.equals("REC_SEQ_NUM"))
      sequenceNum=Integer.parseInt(fldVal);
    
    if(IsResponseOriginatedRecord())
      mdi.setGeneralDataItem(fldNme,fldVal);
    else {
      if(fldNme.equals("ACTION_INDICATOR"))
        mdi.setGeneralDataItem(fldNme,fldVal);
      else if(fldNme.equals("MASTERCARD_REF_NUMBER"))
        mdi.setGeneralDataItem(fldNme,fldVal);
      else if(fldNme.equals("ERROR_CODES"))
        mdi.setGeneralDataItem(fldNme,fldVal);
      else if(fldNme.equals("ERROR"))
        mdi.setGeneralDataItem(fldNme,fldVal);
      // NOTE: ERROR_CODES & ERROR field name map to the same native field
      else if(fldNme.equals("VISA_BIN"))
        mdi.setGeneralDataItem(fldNme,fldVal);
      else if(fldNme.equals("ORIG_MASTERCARD_REF_NUMBER"))
        mdi.setGeneralDataItem(fldNme,fldVal);
      else if(fldNme.equals("MERCH_BUSINESS_NAME")) // used for post determination of Request ID if missing
        mdi.setGeneralDataItem(fldNme,fldVal);
    }
    
    //log.debug("setFieldValue(RESPONSE GENERAL) - mrid='"+mrid+"'.");

    // attempt to retreive embedded mrid from response record string
    // ASSUMPTION: Non-empty REQUEST_COMMENTS and non-empty TRANSACTION_REF_NUMBER field contains the assoc. request ID.
    if ( !IsResponseOriginatedRecord() && mrid.length() == 0 && fldVal.length() > 0 ) 
    {
      switch(getRequestType()) 
      {
        case MATCHREQUESTTYPE_ADD:
          if(fldNme.equals("REQUEST_COMMENTS")) 
          {
            fldVal=Integer.toString(StringUtilities.stringToInt(fldVal));
            if(!fldVal.equals("0")) 
            {
              mrid=fldVal;
              log.info("Found Request ID '"+fldVal+"' in General Response record '"+name+"' REQUEST_COMMENTS field.");
            } 
            else
            {
              log.error("EMBEDDED REQUEST ID MISSING in response record '"+name+"' REQUEST_COMMENTS field.");
            }              
          }
          break;
          
        case MATCHREQUESTTYPE_INQUIRY:
          final char ai = record.charAt(3);         // action identifier field
          //log.debug("setFieldValue(RESPONSE GENERAL) - MAPPING ai = '"+ai+"'");
          switch(ai) 
          {
            case 'E':
            case 'N':
            case 'P':
            case 'R':
            case 'Q':
            case 'Y':
              if( fldNme.equals("TRANSACTION_REF_NUMBER") ) 
              {
                fldVal = Integer.toString(StringUtilities.stringToInt(fldVal));
                if( !fldVal.equals("0") ) 
                {
                  mrid = fldVal;
                  log.info("Found Request ID '"+fldVal+"' in General Response record '"+name+"' TRANSACTION_REF_NUMBER field.");
                } 
                else 
                {
                  log.error("EMBEDDED REQUEST ID MISSING in response record '"+name+"' TRANSACTION_REF_NUMBER field.");
                }
              }
              break;
              
            case 'M':
            case 'H':
              // Rely on Pigeon hole logic in RecToObj() 
              /*
              if(fldNme.equals("MERCH_NUMBER")) {
                // get all existing MATCH requests for the given merchant
                // interrogating each of them to see if a 1-to-1 association can be made with this response and a found request
                // A 1-to-1 occurs when there is ONE and ONLY ONE existing non-Complete request of inquiry type of given merchant number.
                log.debug("Attempting to pigeon hole a merchant to a MATCH inquiry type request...");
                try {
                  PersistQueue_MATCHDataQueueItem_Merchant pqmdqi_m;
                  if(null==(pqmdqi_m = new PersistQueue_MATCHDataQueueItem_Merchant(fldVal,PersistQueue_MATCHDataQueueItem_Merchant.MATCHDATA_REQUEST)))
                    throw new Exception("Unable to instantiate MATCH persist queue of merchant type.");
                  pqmdqi_m.load();
                  if(pqmdqi_m.getNumElements()<1)
                    throw new Exception("Unable to find any MATCH requests for merchant number '"+fldVal+"'.");
                  MATCHDataQueueItem mmdqi=null,tgt=null;
                  for(Enumeration e=pqmdqi_m.getQueueElmntKeys();e.hasMoreElements();) {
                    mmdqi=(MATCHDataQueueItem)pqmdqi_m.getElement((String)e.nextElement());
                    if(mmdqi.getRequestAction().equals(MATCHDataQueueItem.MATCHREQUESTACTION_INQUIRY) && !mmdqi.isComplete()) {
                      if(tgt!=null) {
                        // more than one inquiry request - indeterminate
                        tgt=null;
                        break;
                      }
                      tgt=mmdqi;
                    }
                  }
                  if(tgt!=null) {
                    // determinate target (match) found
                    mrid=tgt.getID();
                    log.info("Pigeon holed MATCH response '"+toString()+"' to originating MATCH request of ID: '"+mrid+"'.");
                  }
                }
                catch(Exception e) {
                  log.error("Exception occurred attempting to pigeon hole merchant to MATCH request: '"+e.getMessage()+"'.");
                }
                if(mrid.length()<1)
                  log.info("Failed to pigeon hole MATCH response ("+toString()+") to originating request.");
              }
              */
              break;
          }
      }
    }

    //log.debug("setFieldValue(RESPONSE GENERAL) - END");
  }
  
  /**
   * RecToObj()
   * 
   * Sub-class over-ride.
   */
  protected boolean RecToObj()
  {
    if(!super.RecToObj())
      return false;
    
    try {
      
      boolean isValid=true;
      
      if(IsResponseOriginatedRecord()) {
        log.debug("Found MATCH Response originating record: '"+record+"'.");
        /*
        // ensure original master card ref number exists
        if((mdi==null || mdi.getGeneralDataItem("ORIG_MASTERCARD_REF_NUMBER").length()==0)) {
          isValid=false;
          log.error("No originating MasterCard reference number detected in MATCH originating response record.");
        }
        */
      } else {
        // attempt to pigeon hole response to originating request via the General Response record's MERCH_BUSINESS_NAME field.
        if(mrid.length()==0 && mdi.getGeneralDataItem("MERCH_BUSINESS_NAME").length()>0) {
          log.debug("Attempting to determine the MATCH Request ID from the Merchant Business Name specified in the Response record...");
          try {
            PersistQueue_MATCHDataQueueItem_Merchant pqmdqi_m;
            if(null==(pqmdqi_m = new PersistQueue_MATCHDataQueueItem_Merchant(mdi.getGeneralDataItem("MERCH_BUSINESS_NAME"))))
              throw new Exception("Unable to instantiate Merchant MATCH persist queue based on Merchant business name.");
            pqmdqi_m.load();
            if(pqmdqi_m.getNumElements()<1)
              throw new Exception("Unable to find any applicable MATCH requests for merchant business name '"+mdi.getGeneralDataItem("MERCH_BUSINESS_NAME")+"'.");
            MATCHDataQueueItem mmdqi=null,tgt=null;
            for(Enumeration e=pqmdqi_m.getQueueElmntKeys();e.hasMoreElements();) {
              mmdqi=(MATCHDataQueueItem)pqmdqi_m.getElement((String)e.nextElement());
              if(tgt!=null) {
                // more than one inquiry request - indeterminate
                tgt=null;
                break;
              }
              tgt=mmdqi;
            }
            if(tgt!=null) {
              // determinate target (match) found
              mrid=tgt.getID();
              log.info("Pigeon holed MATCH Request ID '"+mrid+"' from Merchant Business Name '"+mdi.getGeneralDataItem("MERCH_BUSINESS_NAME")+"'.");
            }
          }
          catch(Exception e) {
            log.error("Exception occurred attempting to determine MATCH Request ID from Merchant Business Name: '"+e.getMessage()+"'.");
          }
          if(mrid.length()<1)
            log.info("Failed to determine MATCH Request ID from Merchant Business Name '"+mdi.getGeneralDataItem("MERCH_BUSINESS_NAME")+"'.");
        }
        /*
        // ensure master card ref number exists ONLY when matching records possible to exist
        // (i.e. response action is not equal to 'N' (no match))
        if(HasRelatedResponseOriginatedRecords() && (mdi==null || mdi.getGeneralDataItem("MASTERCARD_REF_NUMBER").length()==0)) {
          isValid=false;
          log.error("No MasterCard reference number detected in request originating response record.");
        }
        */
      }
      
      return isValid;
    }
    catch(Exception e) {
      log.error(e.getMessage());
      return false;
    }
  }
  
} // class MATCHRecord_ResponseGeneral
