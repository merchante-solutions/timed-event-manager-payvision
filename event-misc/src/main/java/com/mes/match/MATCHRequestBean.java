package com.mes.match;

import java.io.Serializable;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Logger;
import com.mes.ops.QueueBean;
/**
 * MATCHRequestBean
 * 
 * Contains and manages MATCH Request data for a given single account identified by appSeqNum.
 * 
 * Dependencies
 * ------------
 * - com.mes.database package
 * - com.mes.support package
 * - com.mes.mms.autoprocess package
 * 
 * Setup
 * -----
 * (none)
 * 
 */
public final class MATCHRequestBean extends Object implements Serializable
{
  // create class log category
  static Logger log = Logger.getLogger(MATCHRequestBean.class);
  
  // constants
  // application MATCH status (i.e. the status of the app relative to the assoc. MATCH Inquiry)
  public static final int                 APPMATCHSTATUS_UNDEFINED              = 0;
  public static final int                 APPMATCHSTATUS_REQNOTISSUED           = 1;   // MATCH request not issued for the app
  public static final int                 APPMATCHSTATUS_WAITINGFORRESPONSE     = 2;   // Waiting for MATCH response
  public static final int                 APPMATCHSTATUS_REQUESTERROR           = 3;   // unable to process on MATCH end due to request format related error
  public static final int                 APPMATCHSTATUS_UNCHECKED              = 4;   // MATCH response rec'd and possible match but not checked yet (at this stage, the app is either relinquished or declined)
  public static final int                 APPMATCHSTATUS_RELINQUISHED           = 5;   // App relinquished from MATCH (meaning no exact match was deemed found)
  public static final int                 APPMATCHSTATUS_DECLINED               = 6;   // App declined independent of the MATCH processes
  public static final int                 APPMATCHSTATUS_MATCHDEEMED            = 7;   // Exact match deemed
  
  // application IM (Internal Match) status
  public static final int                 APPIMSTATUS_UNDEFINED                 = 0;
  public static final int                 APPIMSTATUS_REQNOTISSUED              = 1;   // IM request not issued for the app
  public static final int                 APPIMSTATUS_UNCHECKED                 = 2;   // IM response rec'd and possible IM but not checked yet (at this stage, the app is either relinquished or declined)
  public static final int                 APPIMSTATUS_RELINQUISHED              = 3;   // App relinquished from IM (meaning no exact IM was deemed found)
  public static final int                 APPIMSTATUS_DECLINED                  = 4;   // App declined independent of the IM processes
  public static final int                 APPIMSTATUS_IMDEEMED                  = 5;   // Exact IM deemed
  public static final int                 APPIMSTATUS_RESULTSETTOOLARGE         = 6;
  
  // data members
  private long              appSeqNum;
  private PersistQueue_MATCHDataQueueItem_Merchant matchRequests;
  private boolean           is_loaded;
  private MATCHResponse     crntMatchResponse;       // current child MATCH Response of single parent contained in member matchRequests
  private IMResponse        crntIMResponse;          // current child IM Response of single parent contained in member matchRequests
  private Vector            errors;                  // ref. to Vector of Strings used to store error descriptors for use by outside world
  
  private QueueBean         qb;                      // com.mes.ops.QueueBean (for queueAppEnumerator)
  private queueAppSeqNumPrevNext qnav;
  
  // class functions
  public static String getAppMATCHStatusDescriptor(int appMatchStatus)
  {
    switch(appMatchStatus) {
      
      default:
      case APPMATCHSTATUS_UNDEFINED:
        return "Undefined";
      
      case APPMATCHSTATUS_REQNOTISSUED:
        return "MATCH Request not issued";
      case APPMATCHSTATUS_WAITINGFORRESPONSE:
        return "Waiting for MATCH response";
      case APPMATCHSTATUS_REQUESTERROR:
        return "Request Error";
      case APPMATCHSTATUS_UNCHECKED:
        return "Unchecked";
      case APPMATCHSTATUS_RELINQUISHED:
        return "Relinquished";
      case APPMATCHSTATUS_DECLINED:
        return "DECLINED";
      case APPMATCHSTATUS_MATCHDEEMED:
        return "EXACT MATCH";
    }
  }
  
  public static String getAppIMStatusDescriptor(int appIMStatus)
  {
    switch(appIMStatus) {
      
      default:
      case APPIMSTATUS_UNDEFINED:
        return "Undefined";
      
      case APPIMSTATUS_REQNOTISSUED:
        return "IM Request not issued";
      case APPIMSTATUS_UNCHECKED:
        return "Unchecked";
      case APPIMSTATUS_RELINQUISHED:
        return "Relinquished";
      case APPIMSTATUS_DECLINED:
        return "DECLINED";
      case APPIMSTATUS_IMDEEMED:
        return "EXACT Internal Match";
      case APPIMSTATUS_RESULTSETTOOLARGE:
        return "Result Set too Large";
    }
  }
  
  // object functions
  
  // construction
  public MATCHRequestBean()
  {
    appSeqNum=-1L;
    matchRequests=null;
    is_loaded=false;
    crntMatchResponse=null;
    crntIMResponse=null;
    errors=null;
    qb=null;
    qnav=null;
  }
  
  // *** queue navigation related functions ***
  public long queueAppPrev()
  {
    if(qnav==null)
      qnav = new queueAppSeqNumPrevNext();
    
    return qnav.getPrevASN();
  }
  
  public long queueAppNext()
  {
    if(qnav==null)
      qnav = new queueAppSeqNumPrevNext();
    
    return qnav.getNextASN();
  }
  
  public void killQueueApp()
  {
    if(qb!=null)
      qb.cleanUp();
    
    qnav=null;
  }
  
  private class queueAppSeqNumPrevNext
  {
    private long        prev_asn;
    private long        next_asn;
    
    public long getPrevASN() { return prev_asn; }
    public long getNextASN() { return next_asn; }
    
    public queueAppSeqNumPrevNext()
    {
      prev_asn=next_asn=-1L;
      
      if(qb==null || !qb.getQueueFiltered("app_queue_credit"))
      {
        return;
      }
      
      java.sql.ResultSet rs = null;
      
      try 
      {
        rs = qb.getQueueResultSet();
      
        if(rs==null)
        {
          return;
        }
      
        boolean bFound=false;
      
        while(!bFound && rs.next()) 
        {
          if(rs.getLong("app_seq_num")==appSeqNum) 
          {
            // found app!
            if(rs.next())
            {
              next_asn=rs.getLong("app_seq_num");
            }
            bFound=true;
          }
          if(!bFound)
          {
            prev_asn=rs.getLong("app_seq_num");
          }
        }
        
        rs.close();
        
        if(!bFound)
        {
          prev_asn=next_asn=-1L;
        }
      }
      catch(Exception e) 
      {
        log.error("queueAppSeqNumPrevNext() EXCEPTION: '"+e.getMessage()+"'.");
      }
      finally 
      {
        try { rs.close(); } catch(Exception e) {}
      }
    }
  } // class queueAppEnumerator
  
  
  public String getGeneralRespFldHTML(String fldNme)
  {
    final String attrib = getMatchAttrib(false,"",fldNme,MATCHRecord.MATCHRECTYPE_RESPONSEGENERAL);
    return getRespFldHTML(attrib,"",fldNme,getCurrentMATCHResponse().getMATCHDataItem().getGeneralDataItem(fldNme),MATCHRecord.MATCHRECTYPE_RESPONSEGENERAL);
  }
  
  public String getPrincipalRespFldHTML(String pID,String fldNme)
  {
    final String attrib = getMatchAttrib(false,pID,fldNme,MATCHRecord.MATCHRECTYPE_RESPONSEPRINCIPAL);
    return getRespFldHTML(attrib,pID,fldNme,getCurrentMATCHResponse().getMATCHDataItem().getPrincipalDataItem(pID,fldNme),MATCHRecord.MATCHRECTYPE_RESPONSEPRINCIPAL);
  }

  public String getIMGeneralRespFld(String fldNme)
  {
    return getCurrentIMResponse().getMATCHDataItem().getGeneralDataItem(fldNme);
  }

  public String getIMGeneralRespFldHTML(String fldNme)
  {
    final String attrib = getMatchAttrib(true,"",fldNme,MATCHRecord.MATCHRECTYPE_RESPONSEGENERAL);
    return getRespFldHTML(attrib,"",fldNme,getCurrentIMResponse().getMATCHDataItem().getGeneralDataItem(fldNme),MATCHRecord.MATCHRECTYPE_RESPONSEGENERAL);
  }
  
  public String getIMPrincipalRespFldHTML(String pID,String fldNme)
  {
    final String attrib = getMatchAttrib(true,pID,fldNme,MATCHRecord.MATCHRECTYPE_RESPONSEPRINCIPAL);
    return getRespFldHTML(attrib,pID,fldNme,getCurrentIMResponse().getMATCHDataItem().getPrincipalDataItem(pID,fldNme),MATCHRecord.MATCHRECTYPE_RESPONSEPRINCIPAL);
  }

  private String getRespFldHTML(String attrib,String pID,String fldNme,String fldVal,int record_type)
  {
    //log.debug("getRespFldHTML(fldNme='"+fldNme+"',fldVal='"+fldVal+"') - START");

    if(fldVal.length()==0)
      return "";

    StringBuffer sb = new StringBuffer(256);
    
    String clr = this.getMatchAttribColor(attrib);
      
    // no match
    if(attrib==null || attrib.equals("") || attrib.equals("N")) {
      sb.append("<SPAN STYLE=\"color:");
      sb.append(clr);
      sb.append("\">");
      sb.append(fldVal);
      sb.append("</SPAN>");
    
    // phonetic match
    } else if(attrib.equals("P")) {
      sb.append("<SPAN STYLE=\"color:");
      sb.append(clr);
      sb.append("\"><b>");
      sb.append(fldVal);
      sb.append("</b></SPAN>");
    
    // exact match
    } else if(attrib.equals("E")) {
      sb.append("<SPAN STYLE=\"color:");
      sb.append(clr);
      sb.append("\"><b>");
      sb.append(fldVal);
      sb.append("</b></SPAN>");
    
    // unknown
    } else
      sb.append(fldVal);
    
    return sb.toString();
  }
  
  public String getMatchAttribColor(String attrib)
  {
    if(attrib==null || attrib.length()==0 || attrib.equals("N"))  // no match
       return "Black";
    else if(attrib.equals("P")) // phonetic
      return "Orange";
    else if(attrib.equals("E")) // exact
      return "Red";

    return "Undefined";
  }
   
  private String getMatchAttrib(boolean bInternal,String pID,String fldNme,int record_type)
  {
    //log.debug("getMatchAttrib(bInternal='"+bInternal+"',fldNme='"+fldNme+"',record_type='"+record_type+"' - START");
    
    try {
      
      if (fldNme.substring(0, fldNme.length() - 1).equals("BUSOWNER_ADDRESS") )
      {
        fldNme = "BUSOWNER_ADDRESS";
      }
      
      String matchfldNme = fldNme + "_MATCH";
      String attrib = "";

      // Response - General
      if(record_type==MATCHRecord.MATCHRECTYPE_RESPONSEGENERAL) {
      
        attrib = bInternal? 
                 crntIMResponse.getMATCHDataItem().getGeneralDataItem(matchfldNme):
                 crntMatchResponse.getMATCHDataItem().getGeneralDataItem(matchfldNme);
                 
      // Response - Principal
      } else if(record_type==MATCHRecord.MATCHRECTYPE_RESPONSEPRINCIPAL) {
      
        attrib = bInternal? 
                 crntIMResponse.getMATCHDataItem().getPrincipalDataItem(pID,matchfldNme):
                 crntMatchResponse.getMATCHDataItem().getPrincipalDataItem(pID,matchfldNme);
      }

      return attrib;
    }
    catch(Exception e) {
      return "";
    }    
  }
  
  // accessors
  
  public boolean isImpBlacklisted()
  {
    try {
      MATCHDataQueueItem mdqi = ((MATCHDataQueueItem)matchRequests.getElement((String)matchRequests.getQueueElmntKeys().nextElement()));
      return mdqi.isImpBlacklisted();
    }
    catch(Exception e) {
      return false;
    }
  }
  
  /**
   * getAppIMStatus()
   * 
   * Determines the application's Internal Match status
   */
  public int getAppIMStatus()
  {
    load(); // i.e. ensure loaded

    if(!is_loaded) {
      if(appSeqNum==-1L)
        return APPIMSTATUS_UNDEFINED;
      else
        return APPIMSTATUS_REQNOTISSUED;
    }
    
    // app declined?
    if(matchRequests.isAppDeclined(appSeqNum))
      return APPIMSTATUS_DECLINED;
    
    final MATCHDataQueueItem mreq = getMATCHRequest();
    
    // result set too large?
    if(mreq.getIMPStatus().equals(MATCHDataQueueItem.IMPSTATUS_RESULTSETTOOLARGE))
      return APPIMSTATUS_RESULTSETTOOLARGE;
      
    // no IM match
    if(mreq.getIMPResponse().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_NOMATCH))
      return APPIMSTATUS_RELINQUISHED;
      
    // possible match (i.e. hasn't been checked yet)
    else if(mreq.getIMPResponse().length()<1 
      || mreq.getIMPResponse().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_POSSIBLEMATCH) 
      || mreq.getIMPResponse().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_RETROPOSSIBLEMATCH)
      || mreq.getIMPResponse().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_BLACKLISTED))
      return APPIMSTATUS_UNCHECKED;

    // relinquished
    else if(mreq.getIMPResponse().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_RELINQUISHED))
      return APPIMSTATUS_RELINQUISHED;
      
    // exact match deemed
    else if(mreq.getIMPResponse().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_MATCHDEEMED))
      return APPIMSTATUS_IMDEEMED;
    
    // Undefined
    else
      return APPIMSTATUS_UNDEFINED;
  }
  
  /**
   * getAppMATCHStatus()
   * 
   * Determines the application's status given the MATCH Inquiry/response info
   */
  public int getAppMATCHStatus()
  {
    load(); // i.e. ensure loaded
    
    if(!is_loaded) {
      if(appSeqNum==-1L)
        return APPMATCHSTATUS_UNDEFINED;
      else
        return APPMATCHSTATUS_REQNOTISSUED;
    }
    
    final MATCHDataQueueItem mreq = getMATCHRequest();
    
    // undefined (error)
    if(mreq.getRequestStatus().equals(MATCHDataQueueItem.MATCHREQUESTSTATUS_LOADERROR)
       || mreq.getRequestStatus().equals(MATCHDataQueueItem.MATCHREQUESTSTATUS_RESPPARSEERROR))
    {
      log.warn("Attempt to get App MATCH status for app seq num '"+appSeqNum+"' w/ assoc. MATCH Inquiry having status of '"+mreq.getRequestStatusDescriptor()+"'.");
      return APPMATCHSTATUS_UNDEFINED;
    
    // waiting
    } else if(mreq.getRequestStatus().equals(MATCHDataQueueItem.MATCHREQUESTSTATUS_NEW)
              || mreq.getRequestStatus().equals(MATCHDataQueueItem.MATCHREQUESTSTATUS_PENDING)
              || mreq.getRequestStatus().equals(MATCHDataQueueItem.MATCHREQUESTSTATUS_RESUBMIT))
      return APPMATCHSTATUS_WAITINGFORRESPONSE;
    
    // request complete
    else if(mreq.getRequestStatus().equals(MATCHDataQueueItem.MATCHREQUESTSTATUS_COMPLETE)) {
      
      // app declined?
      if(matchRequests.isAppDeclined(appSeqNum))
        return APPMATCHSTATUS_DECLINED;
      
      // inquiry error
      else if(mreq.getResponseAction().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_INQUIRYERROR))
        return APPMATCHSTATUS_REQUESTERROR;
      
      // no match (i.e. auto-relinquished)
      else if(mreq.getResponseAction().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_NOMATCH))
        return APPMATCHSTATUS_RELINQUISHED;
      
      // possible match (i.e. hasn't been checked yet)
      else if(mreq.getResponseAction().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_POSSIBLEMATCH) || mreq.getResponseAction().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_RETROPOSSIBLEMATCH))
        return APPMATCHSTATUS_UNCHECKED;

      // relinquished
      else if(mreq.getResponseAction().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_RELINQUISHED))
        return APPMATCHSTATUS_RELINQUISHED;
      
      // exact match deemed
      else if(mreq.getResponseAction().equals(MATCHDataQueueItem.MATCHRESPONSEACTION_MATCHDEEMED))
        return APPMATCHSTATUS_MATCHDEEMED;
    }
    
    // Indeterminate App MATCH status!
    log.error("Indeterminate App MATCH status for app seq num '"+appSeqNum+"'!");
    return APPMATCHSTATUS_UNDEFINED;
  }
  
  public int getNumMATCHResponses()
  {
    if(!is_loaded)
      return -1;
    
    final MATCHDataQueueItem matchRequest = ((MATCHDataQueueItem)matchRequests.getElement((String)matchRequests.getQueueElmntKeys().nextElement()));
    
    return matchRequest.getNumResponseItems();
  }
  
  public int getNumIMResponses()
  {
    if(!is_loaded)
      return -1;
    
    final MATCHDataQueueItem matchRequest = ((MATCHDataQueueItem)matchRequests.getElement((String)matchRequests.getQueueElmntKeys().nextElement()));
    
    return matchRequest.getNumIMResponseItems();
  }
  
  public boolean isLoaded()
  {
    return is_loaded;
  }
  
  public long getAppSeqNum()
  {
    return appSeqNum;
  }
  
  /**
   * isCrntResponseAtEnd()
   * 
   * Determines whether current match response data member is either first or last one
   * of the child responses.
   * 
   * @param bFirstOrLast    true: first, false: last
   */
  public boolean isCrntResponseAtEnd(boolean bFirstOrLast)
  {
    // ensure loaded
    if(!is_loaded)
      return false;
    
    // ensure current response gotten
    if(crntMatchResponse==null) {
      try {
        getCurrentMATCHResponse();
      }
      catch(Exception e) {
        postError("isCrntResponseAtEnd("+bFirstOrLast+") EXCEPTION: '"+e.getMessage()+"'.");
        return false;
      }
    }
    
    // do actual check
    
    final MATCHDataQueueItem matchRequest = ((MATCHDataQueueItem)matchRequests.getElement((String)matchRequests.getQueueElmntKeys().nextElement()));
    
    if(matchRequest.getNumResponseItems()<1)
      return false;
    
    final Vector matchResponses = matchRequest.getResponseItems();
    
    return bFirstOrLast? 
            crntMatchResponse==matchResponses.firstElement():
            crntMatchResponse==matchResponses.lastElement();
  }
  
  public boolean isCrntIMResponseAtEnd(boolean bFirstOrLast)
  {
    // ensure loaded
    if(!is_loaded)
      return false;
    
    // ensure current response gotten
    if(crntIMResponse==null) {
      try {
        getCurrentIMResponse();
      }
      catch(Exception e) {
        postError("isCrntIMResponseAtEnd("+bFirstOrLast+") EXCEPTION: '"+e.getMessage()+"'.");
        return false;
      }
    }
    
    // do actual check
    
    final MATCHDataQueueItem matchRequest = ((MATCHDataQueueItem)matchRequests.getElement((String)matchRequests.getQueueElmntKeys().nextElement()));
    
    if(matchRequest.getNumIMResponseItems()<1)
      return false;
    
    final Vector matchResponses = matchRequest.getIMResponseItems();
    
    return bFirstOrLast? 
            crntIMResponse==matchResponses.firstElement():
            crntIMResponse==matchResponses.lastElement();
  }
  
  public MATCHDataQueueItem getMATCHRequest()
  {
    if(!is_loaded)
      return null;

    // get first and only element
    return ((MATCHDataQueueItem)matchRequests.getElement((String)matchRequests.getQueueElmntKeys().nextElement()));
  }
  
  public MATCHResponse getCurrentMATCHResponse()
  {
    try {
      if(!is_loaded)
        throw new Exception("Get Current MATCH Response FAILED: Currently no MATCH Request loaded.");
    
      final MATCHDataQueueItem mreq = getMATCHRequest();
      
      // default get first response record in collection if no current response record set
      if(crntMatchResponse==null) {
        if(mreq.getNumResponseItems()<1)
          throw new Exception("Get Current MATCH Response FAILED: No MATCH Responses exist for loaded MATCH Request '"+mreq.getID()+"'.");
        crntMatchResponse=(MATCHResponse)getMATCHRequest().getResponseItems().elementAt(0);

      }
    
      // IMPT: parse the raw general/principal response records if not already
      if(!crntMatchResponse.isParsed()) {
        log.debug("Parsing current MATCH Response record '"+crntMatchResponse.getID()+"'...");
        final MATCHRecord_Composite mrc = MATCHRecordBuilder.buildDetailComposite(false,crntMatchResponse.getGeneralRecord(),crntMatchResponse.getPrincipalRecord());
        if(!mrc.IsRecordValid())
          throw new Exception("Unable to set default current MATCH Response: MATCH Response record parsing error.");
        else
          crntMatchResponse.setMATCHDataItem(mrc.getDataContainer());
      }
    }
    catch(Exception e) {
      postError(e.getMessage());
      return null;
    }

    return crntMatchResponse;
  }
  
  public IMResponse getCurrentIMResponse()
  {
    try {
      if(!is_loaded)
        throw new Exception("Get Current IM Response FAILED: Currently no MATCH Request loaded.");
    
      final MATCHDataQueueItem mreq = getMATCHRequest();
    
      // default get first response record in collection if no current response record set
      if(crntIMResponse==null) {
        if(mreq.getNumIMResponseItems()<1)
          throw new Exception("Get Current IM Response FAILED: No MATCH Responses exist for loaded MATCH Request '"+mreq.getID()+"'.");
        crntIMResponse=(IMResponse)getMATCHRequest().getIMResponseItems().elementAt(0);
      }
    
      // IMPT: parse the raw general/principal response records if not already
      if(!crntIMResponse.isParsed()) {
        log.debug("Parsing current IM Response record '"+crntIMResponse.getID()+"'...");
        final MATCHRecord_IM imr = MATCHRecordBuilder.buildIMRecord(mreq.getID(),crntIMResponse.getGeneralRecord(),crntIMResponse.getPrincipalRecord());
        if(!imr.IsRecordValid())
          throw new Exception("Unable to set default current IM Response: IM Response record parsing error.");
        else
          crntIMResponse.setMATCHDataItem(imr.getDataContainer());
      }
    }
    catch(Exception e) {
      postError(e.getMessage());
      return null;
    }
    
    return crntIMResponse;
  }

  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    String responses,currentresp,currentimresp;
    
    if(is_loaded) {
      MATCHDataQueueItem mr=null;
      try {
        mr=getMATCHRequest();
      }
      catch(Exception e) {
        mr=null;
      }
      if(mr!=null)
        responses = nl+"MATCH Request: "+mr.toString()+".";
      else
        responses="";
      currentresp   = nl+"Crnt. Resp ID: '"+(crntMatchResponse==null? "<null>":(String)crntMatchResponse.getID())+"'.";
      currentimresp = nl+"Crnt. IM Resp ID: '"+(crntIMResponse==null? "<null>":(String)crntIMResponse.getID())+"'.";
    } else {
      responses="";
      currentresp="";
      currentimresp="";
    }
    
    return
             nl+"*** MATCHRequestBean ***"
            +nl+"appSeqNum="+appSeqNum
            +nl+"is_loaded="+is_loaded
            +responses
            +currentresp
            +currentimresp
            +nl+"************************";
  }
  
  // mutators
  
  /**
   * relinquishRltdApplications()
   * 
   * NO exact match deemed found in the MACTH system.
   * Relinquishes control of the assoc. applications from the MATCH enforced queue.
   * Takes it out of the MATCH enforced queue (usu. Credit).
   */
  public boolean relinquishRltdApplications()
  {
    try {
      
      final MATCHDataQueueItem mr = getMATCHRequest();
      
      mr.setResponseAction(MATCHDataQueueItem.MATCHRESPONSEACTION_RELINQUISHED);
      
      // app queue logic
      mr.setQueueAction(MATCHDataQueueItem.QUEUEACTION_RELEASE);
      
      save();
      
      return true;
    }
    catch(Exception e) {
      postError("relinquishRltdApplications() EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
  }
  
  /**
   * deemExactMatchRltdApplications()
   * 
   * EXACT match deemed found in the MACTH system.
   * IMPT: DECLINES the assoc app(s)
   */
  public boolean deemExactMatchRltdApplications()
  {
    try {
      final MATCHDataQueueItem mr = getMATCHRequest();
      
      mr.setResponseAction(MATCHDataQueueItem.MATCHRESPONSEACTION_MATCHDEEMED);

      // app queue logic
      mr.setQueueAction(MATCHDataQueueItem.QUEUEACTION_DECLINE);
      
      save();
      
      return true;
    }
    catch(Exception e) {
      postError("declineRltdApplications() EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
  }
  
  /**
   * imRelinquishRltdApplications()
   * 
   * Internal Match
   */
  public boolean imRelinquishRltdApplications()
  {
    try {
      
      final MATCHDataQueueItem mr = getMATCHRequest();
      
      mr.setIMPResponse(MATCHDataQueueItem.MATCHRESPONSEACTION_RELINQUISHED);
      
      // app queue logic
      mr.setQueueAction(MATCHDataQueueItem.QUEUEACTION_IMRELEASE);
      
      save();
      
      return true;
    }
    catch(Exception e) {
      postError("imRelinquishRltdApplications() EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
  }
  
  /**
   * deemExactInternalMatch()
   * 
   * EXACT match deemed found internally.
   * IMPT: DECLINES the assoc app(s)
   */
  public boolean deemExactInternalMatch()
  {
    try {
      final MATCHDataQueueItem mr = getMATCHRequest();
      
      mr.setIMPResponse(MATCHDataQueueItem.MATCHRESPONSEACTION_MATCHDEEMED);

      // app queue logic
      mr.setQueueAction(MATCHDataQueueItem.QUEUEACTION_DECLINE);
      
      save();
      
      return true;
    }
    catch(Exception e) {
      postError("deemExactInternalMatch() EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
  }
  
  /**
   * resubmitRltdApplications()
   * 
   * Updates the MATCH request record such that it will get re-submitted the next go around of MATCHProcessorBean
   */
  public boolean resubmitRltdApplications()
  {
    try {
      final MATCHDataQueueItem mr = getMATCHRequest();
      
      mr.markToResubmit();
      save();
      
      return true;
    }
    catch(Exception e) {
      postError("resubmitRltdApplications() EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
  }
  
  public void setErrors(Vector errors)
  {
    if(errors!=null)
      this.errors=errors;
  }
  
  private final void postError(String es)
  {
    if(es==null || es.length()==0)
      return;
    if(errors!=null)
      errors.addElement(es);
    log.error(es);
  }
  
  public void setQueueBean(QueueBean qb)
  {
    if(qb!=null)
      this.qb=qb;
  }
  
  public QueueBean getQueueBean()
  {
    return qb;
  }
  
  public boolean nextResponse()
  {
    return IncrementCurrentMATCHResponse(true);
  }
  
  public boolean prevResponse()
  {
    return IncrementCurrentMATCHResponse(false);
  }
  
  public boolean nextIMResponse()
  {
    return IncrementCurrentIMResponse(true);
  }
  
  public boolean prevIMResponse()
  {
    return IncrementCurrentIMResponse(false);
  }
  
  public void save()
  {
    if(matchRequests==null)
      return;
    try {
      matchRequests.save();
    }
    catch(Exception e) {
      postError("Save EXCEPTION occurred while saving MATCH Request bean.");
    }
  }
  
  private boolean IncrementCurrentMATCHResponse(boolean bIncrOrDecr)
  {
    if(!is_loaded)
      return false;
    
    // get the expected one and only element (MATCHRequest)
    final MATCHDataQueueItem matchRequest = ((MATCHDataQueueItem)matchRequests.getElement((String)matchRequests.getQueueElmntKeys().nextElement()));
    final Vector matchResponses = matchRequest.getResponseItems();
    MATCHResponse mresp=null;
    
    // default get first response record in collection if no current response record set
    if(crntMatchResponse==null) {
      if(matchRequest.getNumResponseItems()<1)
        // no child responses
        return false;
      crntMatchResponse=(MATCHResponse)matchRequest.getResponseItems().elementAt(0);
    }
    
    // get to the current match response
    for(int i=0;i<matchResponses.size();i++) {
      mresp=(MATCHResponse)matchResponses.elementAt(i);
      if(mresp.getID().equals(crntMatchResponse.getID())) {
        if(bIncrOrDecr) { // increment
          if(i+1>=matchResponses.size())
            return false;
          mresp=(MATCHResponse)matchResponses.elementAt(i+1);
        } else {          // decrement
          if(i<1)
            return false;
          mresp=(MATCHResponse)matchResponses.elementAt(i-1);
        }
        break;
      } else
        mresp=null;
    }

    if(mresp!=null)
      crntMatchResponse=mresp;
    
    return (mresp!=null);
  }
  
  private boolean IncrementCurrentIMResponse(boolean bIncrOrDecr)
  {
    if(!is_loaded)
      return false;
    
    // get the expected one and only element (MATCHRequest)
    final MATCHDataQueueItem matchRequest = ((MATCHDataQueueItem)matchRequests.getElement((String)matchRequests.getQueueElmntKeys().nextElement()));
    final Vector imResponses = matchRequest.getIMResponseItems();
    IMResponse imr=null;
    
    // default get first response record in collection if no current response record set
    if(crntIMResponse==null) {
      if(matchRequest.getNumIMResponseItems()<1)
        // no child responses
        return false;
      crntIMResponse=(IMResponse)matchRequest.getIMResponseItems().elementAt(0);
    }
    
    // get to the current match response
    for(int i=0;i<imResponses.size();i++) {
      imr=(IMResponse)imResponses.elementAt(i);
      if(imr.getID().equals(crntIMResponse.getID())) {
        if(bIncrOrDecr) { // increment
          if(i+1>=imResponses.size())
            return false;
          imr=(IMResponse)imResponses.elementAt(i+1);
        } else {          // decrement
          if(i<1)
            return false;
          imr=(IMResponse)imResponses.elementAt(i-1);
        }
        break;
      } else
        imr=null;
    }

    if(imr!=null)
      crntIMResponse=imr;
    
    return (imr!=null);
  }
  
  public void setAppSeqNum(long appSeqNum)
  {
    if(this.appSeqNum==appSeqNum)
      return;
    
    clear();
    
    this.appSeqNum=appSeqNum;
  }
  
  private void clear()
  {
    if(matchRequests!=null) {
      matchRequests.unload();
      matchRequests.endSession();
    }
    
    appSeqNum=-1L;
    matchRequests=null;
    is_loaded=false;
    crntMatchResponse=null;
    crntIMResponse=null;
  }
  
  public void load()
  {
    if(is_loaded)
      return;
    
    if(appSeqNum==-1L) {
      postError("Load MATCHRequestBean failed: No App Seq Num specified.");
      return;
    }
    
    try {
      
      if(matchRequests==null)
        matchRequests = new PersistQueue_MATCHDataQueueItem_Merchant(appSeqNum,PersistQueue_MATCHDataQueueItem_Merchant.MATCHDATA_ALL);
      
      matchRequests.beginSession();
    
      matchRequests.load();
      
      is_loaded=(matchRequests.getNumElements()==1);
    }
    catch(Exception e) {
      postError("MATCHRequestBean: EXCEPTION occurred attempting to load: '"+e.getMessage()+"'.");
      clear();
      return;
    }

  }
  
  public void unload()
  {
    clear();
  }
    
} // MATCHRequestBean
