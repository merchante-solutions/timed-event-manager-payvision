package com.mes.match;

// log4j classes.
import org.apache.log4j.Category;

/**
 * PersistQueue_MATCHDataItem_Input
 * --------------------------------
*/
class PersistQueue_MATCHDataQueueItem_Input extends PersistQueue_MATCHDataQueueItem
{
  // create class log category
  static Category log = Category.getInstance(PersistQueue_MATCHDataQueueItem_Input.class.getName());

  // construction
  public PersistQueue_MATCHDataQueueItem_Input()
    throws Exception
  {
    super(true);
  }
  
  public void load()
    throws Exception
  {
    super.load(DBQueueOps_MATCHDataQueueItem.DBQ_TYPE_INPUT,false);
  }
  
  public void save()
    throws Exception
  {
    super.save(DBQueueOps_MATCHDataQueueItem.DBQ_TYPE_INPUT);
  }

  public void updatePersist()
    throws Exception
  {
    super.updatePersist(DBQueueOps_MATCHDataQueueItem.DBQ_TYPE_INPUT);
  }

  public void updatePersist(String mdqiID)
    throws Exception
  {
    super.updatePersist(mdqiID,DBQueueOps_MATCHDataQueueItem.DBQ_TYPE_INPUT);
  }

} // class PersistQueue_MATCHDataQueueItem_Input
