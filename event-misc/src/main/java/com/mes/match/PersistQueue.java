/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/match/PersistQueue.java $

  Description:  


  Last Modified By   : $Author: Jkirton $
  Last Modified Date : $Date: 12/11/03 11:22a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.match;

import java.util.Enumeration;
import java.util.Hashtable;
// log4j classes.
import org.apache.log4j.Category;

public abstract class PersistQueue extends Object
{
  // create class log category
  static Category log = Category.getInstance(PersistQueue.class.getName());

  // constants
   protected static final int NUM_MAX_ELEMENTS          = 1000;
  
   // persist method enumeration
   protected static final int PERSIST_METHOD_UNDEFINED  = 0;
   protected static final int PERSIST_METHOD_DB         = 1;
   protected static final int PERSIST_METHOD_FILE       = 2;

  
  // data members
   protected Hashtable objElements;
    // key: String
    // val: {object reference}
   protected int iPersistMethod;
    // identifies a particular persistance media
    // permanant attribute for lifetime of object
  protected boolean bInputOrOutput;
    // true  --> input  queue
    // false  --> output queue
  
  
  // construction
  public PersistQueue(int iPersistMethod,boolean bInputOrOutput)
  {
    this.iPersistMethod=iPersistMethod;
    this.bInputOrOutput=bInputOrOutput;
    objElements = new Hashtable();
  }
  
  /**
   * startup()
   *  - initial loading of queue data
   */
  public void startup()
    throws Exception
  {
    // load input queues from persistence media
    if(bInputOrOutput)
      load();
  }
  
  /**
   * shutdown()
   *  - unloading and saving (if necessary) of 
   */
  public void shutdown()
  {
    try {
      unload();
    }
    catch(Exception e) {
      log.error(e.getMessage());
    }
  }

  /**
   * load()
   * ------
   *  - loads elements into queue from persistence media
   */
  protected abstract void load()
    throws Exception;

  /**
   * unload()
   * ------
   *  - unloads all elements in queue
   *  - does NOT alter persistence media
   */
  public void unload()
  {
    objElements.clear();
  }
  
  /**
   * save()
   * ------
   *  - saves elements in queue to persistence media
   */
  public abstract void save()
    throws Exception;
  
  /**
   * updatePersist()
   * ------
   *  - saves ALL elements in queue to persistence media
   */
  public abstract void updatePersist()
    throws Exception;

  /**
   * updatePersist()
   * ------
   *  - saves element in queue spec. by arg to persistence media
   */
  public abstract void updatePersist(String elmKey)
    throws Exception;

  /**
   * getElement()
   * --------
   *  - returns reference to element in queue id'd by key 
   *     and null if element not in queue
   */
  public Object getElement(String elmKey)
  {
    return objElements.get(elmKey);
  }
  
  /**
   * addElement()
   * --------
   *  - adds element to queue replacing element if already present in queue
   *  - only adds element when max num allowed elements has not been reached
   */
  public boolean addElement(String elmKey,Object objElement)
  {
    try {
      if(objElements.size()>=NUM_MAX_ELEMENTS) {
        log.error("Unable to add element to queue: Maximum limit reached.");
        return false;
      }
      objElements.put(elmKey,objElement);
    }
    catch(Exception e) {
         log.error(e.getMessage());
      return false;
    }
    return true;
  }
  
  /**
   * unloadElement()
   * --------
   *  - unloads specified element from queue
   *  - does NOT remove from persistence media
   */
  public void unloadElement(String elmKey)
  {
    if(objElements.containsKey(elmKey))
      objElements.remove(elmKey);
  }
  
  public abstract void loadElement(String elmKey)
    throws Exception;
  
  public Enumeration getQueueElmntKeys()
  {
    return objElements.keys();
  }
  
  
  public boolean IsElementInQueue(String elmKey)
  {
    return objElements.containsKey(elmKey);
  }

  public int getNumElements()
  {
    return objElements.size();
  }
  
  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    String s = nl+"**Persist Queue (iPersistMethod="+iPersistMethod+",bInputOrOutput="+bInputOrOutput+",Num Elements="+objElements.size()+")";

    if(!objElements.isEmpty()) {
      s += nl+"->Elements:";
      String key;
      for (Enumeration e = objElements.keys(); e.hasMoreElements(); ) {
        key=(String)e.nextElement();
        s+=nl+key+" = "+objElements.get(key).toString();
      }
    } else
      s += "EMPTY";
    
    return s;
  }
  
}
