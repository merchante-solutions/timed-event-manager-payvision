package com.mes.match;

import java.util.Calendar;
import java.util.Date;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;

/**
 * MATCHRecord_Header
 * 
 * Extends MATCHRecord encapsulating request header specific record data and logic
 */
class MATCHRecord_Header extends MATCHRecord
{
  // create class log category
  static Category log = Category.getInstance(MATCHRecord_Header.class.getName());

  // constants
  // (none)

  // data members
  protected String transmissionID;
  
  // construction
  protected MATCHRecord_Header(String name,int record_type,int sequenceNum)
  {
    super(name,record_type,sequenceNum);
    
    // transmissionID
    // yyyymmdd
    Calendar clndr = Calendar.getInstance();
    clndr.setTime(new Date());
    final String datestamp = 
      Integer.toString(clndr.get(Calendar.YEAR))
      +StringUtilities.rightJustify(Integer.toString(clndr.get(Calendar.MONTH)+1), 2, '0')
      +StringUtilities.rightJustify(Integer.toString(clndr.get(Calendar.DAY_OF_MONTH)), 2, '0');
    transmissionID=MATCHRECCNST_ACN+datestamp+StringUtilities.rightJustify(Integer.toString(sequenceNum),2,'0');
  }
  
  /**
   * getFieldValue()
   * 
   * Sub-class override.
   */
  protected String getFieldValue(String fldNme)
    throws Exception
  {
    String value=null;
    
    if(fldNme.equals("RECORD_TYPE")) {
      if(record_type==MATCHRECTYPE_INQUIRYHEADER)
        value="IDH";
      else if(record_type==MATCHRECTYPE_ADDHEADER)
        value="ADH";
    }
    else if(fldNme.equals("TRANSACTION_CODE"))
      value="651";
    else if(fldNme.equals("ACN"))
      value=StringUtilities.rightJustify(MATCHRECCNST_ACN,7,'0');
    else if(fldNme.equals("TRANSMISSIONID"))
      value=transmissionID;
    else if(fldNme.equals("FILLER"))
      value="";
    else
      throw new Exception("Get header field value failed: field ('"+fldNme+"') un-mapped.");
    
    //log.debug("HEADER - Field: '"+fldNme+"' mapped to value: '"+value+"'");
    return value;
  }
  
  /**
   * setFieldValue()
   * 
   * No eligable fields exist.
   */
  protected void setFieldValue(String fldNme, String fldVal)
    throws Exception
  {
    throw new Exception("Erroneous method call: MATCHRecord_Header.getFieldValue()");
  }

} // class MATCHRecord_Header
