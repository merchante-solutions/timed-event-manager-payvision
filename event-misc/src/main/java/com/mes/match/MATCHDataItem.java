package com.mes.match;

import java.util.Enumeration;
import java.util.Hashtable;
// log4j classes.
import org.apache.log4j.Category;


/**
 * MATCHDataItem
 * -------------
 * Encapsulates 'general' and 'principal' data that can represent 
 *  'add', 'inquiry' or 'response' data per the MasterCard MATCH system
 * 
 *  dataItems hashtable keys LEGEND:
 *  --------------------------------
 *  key: "general"    val: {MATCHDataUnit}    single general data unit
 *  key: "principal"  val: {Hashtable}        hashtable of 0-n principal data unit(s)
 *  
 *    principal hashtable keys LEGEND:
 *    --------------------------------
 *    key: {principalID}   val: {MATCHDataUnit}  single principal data unit
 */
public class MATCHDataItem extends MATCHDataUnit
{
  // create class log category
  static Category log = Category.getInstance(MATCHDataItem.class.getName());
    
  // constants
  // (none)
  
  // data members
  // (none)
  
  // construction
  public MATCHDataItem()
  {
    super();
    
    dataItems.put("general",new MATCHDataUnit());
    dataItems.put("principal",new Hashtable(2));
  }
  
  public Enumeration getGeneralKeys()
  {
    MATCHDataUnit mdu=(MATCHDataUnit)dataItems.get("general");
    
    return (mdu==null)? null:(Enumeration)mdu.dataItems.keys();
  }

  public Enumeration getPrincipalKeys(int principalNum)
  {
    MATCHDataUnit mdu=(MATCHDataUnit)((Hashtable)dataItems.get("principal")).get(Integer.toString(principalNum));
    
    return (mdu==null)? null:mdu.dataItems.keys();
  }
  
  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    // general string
    String general = ">>General data: "+nl+((MATCHDataUnit)dataItems.get("general")).toString();
    
    // principal string
    String principal="";
    MATCHDataUnit mdu=null;
    if(getNumPrincipals()>0) {
      String principalNum;
      for(Enumeration e=((Hashtable)dataItems.get("principal")).keys();e.hasMoreElements();) {
        principalNum=(String)e.nextElement();
        mdu=(MATCHDataUnit)((Hashtable)dataItems.get("principal")).get(principalNum);
        principal += nl+"  --Principal #"+principalNum+": "+nl+mdu.toString();
      }
    } else
      principal="  *No Principal data.*";
      
    return general+principal;
  }
  
  public void clear()
  {
    try {
      // general
      ((MATCHDataUnit)dataItems.get("general")).clear();
      // principal
      for(Enumeration e=((Hashtable)dataItems.get("principal")).elements();e.hasMoreElements();)
        ((MATCHDataUnit)e.nextElement()).clear();
      ((Hashtable)dataItems.get("principal")).clear();
    }
    catch(Exception e) {
      log.error("MATCHDataItem.clear() Exception: '"+e.getMessage()+"'.");
    }
  }
  
  /**
   * setGeneralDataTo()
   * 
   * Re-points reference to general data to that specifed in the param
   * 
   * @param mdi MATCHDataItem object whose general data is gets pointed to by this object
   */
  public void setGeneralDataTo(MATCHDataItem mdi)
    throws Exception
  {
    if(mdi==null)
      throw new Exception("Unable to transfer General data: Null MATCH data item param specified.");
    
    // remove current ref
    dataItems.remove("general");
    
    // add ref
    dataItems.put("general",mdi.dataItems.get("general"));
  }
  
  /**
   * setPrincipalDataTo()
   * 
   * Re-points reference to principal data to that specifed in the param
   * 
   * @param mdi MATCHDataItem object whose principal data is gets pointed to by this object
   */
  public void setPrincipalDataTo(MATCHDataItem mdi)
    throws Exception
  {
    if(mdi==null)
      throw new Exception("Unable to set Principal data: Null MATCH data item param specified.");
    
    // remove current ref
    dataItems.remove("principal");
    
    // add ref
    dataItems.put("principal",mdi.dataItems.get("principal"));
  }
  
  /**
   * setGeneralDataItem()
   * -------------------
   * 
   * @param   nme   general data item name
   * @param   val   general data item value
   */
  public void setGeneralDataItem(String nme,String val)
  {
    ((MATCHDataUnit)dataItems.get("general")).setUnit(nme,val);
  }
  
  /**
   * setPrincipalDataItem()
   * ----------------------
   * REQUIRED: 'general.MATCHDataUnit.generalID' is required to set ANY principal data
   * 
   * @param   principalID   identifies the principal to affect
   * @param   nme           principal item name
   * @param   val           principal item value
   */
  public void setPrincipalDataItem(String principalID,String nme,String val)
    throws Exception
  {
    // ensure generalID is already specified
    if(((MATCHDataUnit)dataItems.get("general")).unitExists("generalID"))
      throw new Exception("Unable to set principal data item because the general data item 'generalID' is not yet specified.");
    
    // ensure principal exists
    if(!((Hashtable)dataItems.get("principal")).containsKey(principalID))
      ((Hashtable)dataItems.get("principal")).put(principalID,new MATCHDataUnit());
    
    ((MATCHDataUnit)((Hashtable)dataItems.get("principal")).get(principalID)).setUnit(nme,val);
  }
  
  public String getGeneralDataItem(String nme)
  {
    return ((MATCHDataUnit)dataItems.get("general")).getUnit(nme);
  }
  
  public String getPrincipalDataItem(String principalID,String nme)
  {
    return (!((Hashtable)dataItems.get("principal")).containsKey(principalID))?
            "":((MATCHDataUnit)((Hashtable)dataItems.get("principal")).get(principalID)).getUnit(nme);
  }
  
  public String getGeneralID()
  {
    return ((MATCHDataUnit)dataItems.get("general")).getUnit("ID_GENERAL");
  }
  
  public String[] getPrincipalIDs()
  {
    final int numP = getNumPrincipals();
    
    if(numP<1)
      return new String[] { "" };
    
    String arrPriIDs[] = new String[numP];
    int i=0;
    
    for(Enumeration e=((Hashtable)dataItems.get("principal")).keys();e.hasMoreElements();)
      arrPriIDs[i++] = (String)e.nextElement();
    
    return arrPriIDs;
  }
  
  public int getNumPrincipals()
  {
    return ((Hashtable)dataItems.get("principal")).size();
  }
  
  public int getNumGeneralItems()
  {
    return ((MATCHDataUnit)dataItems.get("general")).getNumUnits();
  }
  
  public int getNumPrincipalItems(String principalID)
  {
    return 
      ((Hashtable)dataItems.get("principal")).containsKey(principalID)?
        ((MATCHDataUnit)((Hashtable)dataItems.get("principal")).get(principalID)).getNumUnits():0;
  }
  
} // class MATCHDataItem

