package com.mes.match;

// log4j classes.
import org.apache.log4j.Category;

/**
 * MATCHRecordBuilder 
 * 
 * Builds the specified type of MATCHRecord object based on the build function called.
 * 
 */
final class MATCHRecordBuilder
{
  // create class log category
  static Category log = Category.getInstance(MATCHRecordBuilder.class.getName());
  
  // construction
  private MATCHRecordBuilder() { }
  
  
  // ****** REQUEST records *********
  
  /**
   * buildRequestAddHeader()
   * 
   * NOTE: header sequence number corres. to the MATCH batch sequence number
   */
  public static MATCHRecord_Header buildRequestAddHeader(int sequenceNum)
    throws Exception
  {
    return new MATCHRecord_Header("Request Add Header",MATCHRecord.MATCHRECTYPE_ADDHEADER,sequenceNum);
  }
  
  /**
   * buildRequestInquiryHeader()
   * 
   * NOTE: header sequence number corres. to the MATCH batch sequence number
   */
  public static MATCHRecord_Header buildRequestInquiryHeader(int sequenceNum)
    throws Exception
  {
    return new MATCHRecord_Header("Request Inquiry Header",MATCHRecord.MATCHRECTYPE_INQUIRYHEADER,sequenceNum);
  }
  
  /**
   * buildDetailConstituent_InquiryGeneral()
   * 
   * @param mrid    MATCH Request ID
   */
  public static MATCHRecord buildDetailConstituent_InquiryGeneral(int sequenceNum,String mrid,MATCHDataItem mdi)
    throws Exception
  {
    if(mdi==null)
      throw new Exception("Unable to construct MATCH record object: null MATCH data container specified.");
    
    MATCHRecord instance = new MATCHRecord_General("Request General Inquiry constituent",MATCHRecord.MATCHRECTYPE_INQUIRYGENERAL,sequenceNum);
    
    instance.setDataContainer(mdi);
    ((MATCHRecord_General)instance).setMRID(mrid);
    
    return instance;
  }
           
  /**
   * buildDetailConstituent_InquiryPrincipal()
   * 
   * NOTE: mdi --> record conversion FLAT FILES principal "objects" to a single principal record
   */
  public static MATCHRecord buildDetailConstituent_InquiryPrincipal(int sequenceNum,MATCHDataItem mdi)
    throws Exception
  {
    if(mdi==null)
      throw new Exception("Unable to construct MATCH record object: null MATCH data container specified.");
    
    MATCHRecord instance = new MATCHRecord_Principal("Request Principal Inquiry constituent",MATCHRecord.MATCHRECTYPE_INQUIRYPRINCIPAL,sequenceNum);
    
    instance.setDataContainer(mdi);
    
    return instance;
  }
           
  /**
   * buildDetailConstituent_AddGeneral()
   * 
   * @param mrid    MATCH Request ID
   */
  public static MATCHRecord buildDetailConstituent_AddGeneral(int sequenceNum,String mrid,MATCHDataItem mdi)
    throws Exception
  {
    if(mdi==null)
      throw new Exception("Unable to construct MATCH record object: null MATCH data container specified.");
    
    MATCHRecord instance = new MATCHRecord_General("Request General Add constituent",MATCHRecord.MATCHRECTYPE_ADDGENERAL,sequenceNum);
    instance.setDataContainer(mdi);
    ((MATCHRecord_General)instance).setMRID(mrid);
    
    return instance;
  }
           
  /**
   * buildDetailConstituent_AddPrincipal()
   */
  public static MATCHRecord buildDetailConstituent_AddPrincipal(int sequenceNum,MATCHDataItem mdi)
    throws Exception
  {
    if(mdi==null)
      throw new Exception("Unable to construct MATCH record object: null MATCH data container specified.");
    
    MATCHRecord instance = new MATCHRecord_Principal("Request Add Inquiry Constituent",MATCHRecord.MATCHRECTYPE_ADDPRINCIPAL,sequenceNum);
    instance.setDataContainer(mdi);
    
    return instance;
  }
  
  /**
   * buildRequestTrailer()
   * 
   * Builds either a MATCH add or inquiry record trailer.
   * 
   * @param   header          Associated header record.
   * @param   numTypeRecs     number of composite detail records of add or inquiry type
   */
  public static MATCHRecord_Trailer buildRequestTrailer(MATCHRecord_Header header,int numTypeRecs)
    throws Exception
  {
    MATCHRecord_Trailer instance=null;
    
    //log.debug("buildRequestTrailer() - START");
    
    // set sub-class specific info
    if(header.getRecordType()==MATCHRecord.MATCHRECTYPE_ADDHEADER) {
      instance = new MATCHRecord_Trailer("Request Add Trailer",MATCHRecord.MATCHRECTYPE_ADDTRAILER,header.getSequenceNum());
      //log.debug("Integer.toString(numTypeRecs)="+Integer.toString(numTypeRecs));
      instance.setFieldValue("TOTAL_A_RECORDS",Integer.toString(numTypeRecs));
      instance.setFieldValue("TOTAL_I_RECORDS",Integer.toString(0));
    } else if(header.getRecordType()==MATCHRecord.MATCHRECTYPE_INQUIRYHEADER) {
      instance = new MATCHRecord_Trailer("Request Add Trailer",MATCHRecord.MATCHRECTYPE_INQUIRYTRAILER,header.getSequenceNum());
      instance.setFieldValue("TOTAL_A_RECORDS",Integer.toString(0));
      instance.setFieldValue("TOTAL_I_RECORDS",Integer.toString(numTypeRecs));
    } else
      throw new Exception("Unable to construct MATCH record trailer - Unsupported header record type: "+header.getRecordType()+" specified.");
    
    instance.transmissionID=header.transmissionID;
    
    return instance;
  }

  
  // ****** RESPONSE records *********
  
  /**
   * buildResponseRecord
   * 
   * Constructs a MATCHRecord object from the record string.
   * 
   * @param record String representing the record.  The record type is determined from this argument.
   */
  public static MATCHRecord buildResponseRecord(String record)
    throws Exception
  {
    final int record_type = MATCHRecord.getRecordTypeFromRecord(record,false);
    log.debug("buildResponseRecord() record_type="+record_type);
    
    if(record_type==MATCHRecord.MATCHRECTYPE_UNDEFINED)
      throw new Exception("Unable to construct MATCH response record: Unable to determine the record type from the record string: '"+record+"'");
    
    MATCHRecord instance = null;
    
    switch(record_type) {
      case MATCHRecord.MATCHRECTYPE_RESPONSEHEADER:
        instance = new MATCHRecord_ResponseHeader("Response Header",0);
        break;
      case MATCHRecord.MATCHRECTYPE_RESPONSEGENERAL:
        instance = new MATCHRecord_ResponseGeneral("Response General",0);
        break;
      case MATCHRecord.MATCHRECTYPE_RESPONSEPRINCIPAL:
        instance = new MATCHRecord_ResponsePrincipal("Response Principal",0);
        break;
      case MATCHRecord.MATCHRECTYPE_RESPONSETRAILER:
        instance = new MATCHRecord_ResponseTrailer("Response Trailer",0);
        break;
    }
    
    if(instance==null)
      throw new Exception("Unable to construct MATCH response record from record string: '"+record+"'.");
    
    instance.setRecord(record);
    
    //log.debug("Build response record: '"+record.toString()+"'");
    
    return instance;
  }

  
  // ****** REQUEST and RESPONSE records *********
  
  /**
   * buildDetailCompositeAdd()
   * 
   * Builds composite add record ( 1 general record + 1-5 principal record(s) )
   * NOTE: composite detail records do not have a defined sequence number so it is set to zero (base-class)
   * 
   * @param sourceID Used to bind the record to a source.  For requests, the sourceID is the MATCH Request ID
   */
  public static MATCHRecord_Composite buildDetailCompositeAdd(String sourceID)
    throws Exception
  {
    return new MATCHRecord_Composite(("Composite Add - "+sourceID),MATCHRecord.MATCHRECTYPE_COMPOSITEADD,0);
  }
  
  /**
   * buildDetailCompositeInquiry()
   * 
   * Builds composite inqiury record ( 1 general record + 1-5 principal record(s) )
   * NOTE: composite detail records do not have a defined sequence number so it is set to zero (base-class)
   * 
   * @param sourceID Used to bind the record to a source.  For requests, the sourceID is the MATCH Request ID
   */
  public static MATCHRecord_Composite buildDetailCompositeInquiry(String sourceID)
    throws Exception
  {
    return new MATCHRecord_Composite(("Composite Inquiry - "+sourceID),MATCHRecord.MATCHRECTYPE_COMPOSITEINQUIRY,0);
  }
  
  /**
   * buildDetailComposite()
   * 
   * record string --> object, 
   * end to end loading of record string to complete assembled composite record object.
   * 
   * @param   bRequestOrResponse  true:  record string params represent request records, false: record string params represent response records
   * @param   general_record      general record string
   * @param   principal_record    principal record string
   */
  public static MATCHRecord_Composite buildDetailComposite(boolean bRequestOrResponse, String general_record, String principal_record)
    throws Exception
  {
    if(general_record==null || general_record.length()==0)
      throw new Exception("Unable to construct composite MATCH record object from record string: No general record string specified.");
    
    int request_type = MATCHRecord.MATCHREQUESTTYPE_UNDEFINED;
    int record_type = MATCHRecord.MATCHRECTYPE_UNDEFINED;
    String name=null;
    MATCHRecord_Composite cmprec=null;
    
    if(bRequestOrResponse)
      // request
      throw new Exception("Unable to construct composite MATCH record object from record string: Only Response type records can be constructed from record strings.");
    else {
      // response
      if((request_type=MATCHRecord.getRequestTypeFromResponseRecord(general_record))==MATCHRecord.MATCHREQUESTTYPE_UNDEFINED)
        throw new Exception("Unable to construct composite MATCH record object from record string: Unable to determine the request type from the response record: '"+general_record+"'.");
      if(request_type==MATCHRecord.MATCHREQUESTTYPE_ADD) {
        record_type = MATCHRecord.MATCHRECTYPE_COMPOSITEADD;
        name = "Composite Add";
      } else if(request_type==MATCHRecord.MATCHREQUESTTYPE_INQUIRY) {
        record_type = MATCHRecord.MATCHRECTYPE_COMPOSITEINQUIRY;
        name = "Composite Inquiry";
      } else
        throw new Exception("Unable to construct composite MATCH record object from record string: Unhandled request type '"+request_type+"'.");
        
      MATCHRecord general = (general_record!=null && general_record.length()>0)?
                                MATCHRecordBuilder.buildResponseRecord(general_record)
                                :null;
      MATCHRecord principal = (principal_record!=null && principal_record.length()>0)?
                                MATCHRecordBuilder.buildResponseRecord(principal_record)
                                :null;
      
      if(general==null)
        throw new Exception("Unable to construct composite MATCH record object from record string: Unable to create general record object from record string: '"+general_record+"'.");
      
      cmprec = new MATCHRecord_Composite(name,record_type,1);
      
      cmprec.setGeneralRecord(general);
      cmprec.setPrincipalRecord(principal);
    }
    
    if(cmprec==null)
      throw new Exception("Unable to construct composite MATCH record object from record string: Unable to instantiate composite record object.");
    
    if(!cmprec.parseRecord())
      throw new Exception("Unable to construct composite MATCH record object from record string: Error parsing record string.");
    
    // at this point, the data container held by composite record is fully loaded with contents held in record string
    
    //log.debug("cmprec.getDataContainer().getNumGeneralItems()="+cmprec.getDataContainer().getNumGeneralItems());
    
    return cmprec;
  }
  
  /**
   * buildIMRecord()
   * 
   * Used to construct MATCHRecord_IM object to load from persist media.
   * Load version.
   */
  public static MATCHRecord_IM buildIMRecord(String mrid,String general_record,String principal_record)
    throws Exception
  {
    MATCHRecord general = (general_record!=null && general_record.length()>0)?
                              MATCHRecordBuilder.buildResponseRecord(general_record)
                              :null;
    MATCHRecord principal = (principal_record!=null && principal_record.length()>0)?
                              MATCHRecordBuilder.buildResponseRecord(principal_record)
                              :null;
      
    if(general==null)
      throw new Exception("Unable to construct IM record object from record string: Unable to create general record object from record string: '"+general_record+"'.");
    
    ((MATCHRecord_ResponseGeneral)general).setMRID(mrid);
      
    MATCHRecord_IM imrec = new MATCHRecord_IM("IM Record",1);
      
    imrec.setGeneralRecord(general);
    imrec.setPrincipalRecord(principal);
    
    imrec.setRecordDefinition();
    
    if(!imrec.parseRecord())
      throw new Exception("Unable to construct IM record object from record string: Error parsing record string.");

    return imrec;
  }
  
  /**
   * buildIMRecord()
   * 
   * Used to construct MATCHRecord_IM from data object in order to persist the data.
   * Save version.
   */
  public static MATCHRecord_IM buildIMRecord(MATCHDataItem mdi)
    throws Exception
  {
    MATCHRecord general   = new MATCHRecord_ResponseGeneral("IM Response General",1);
    MATCHRecord principal = new MATCHRecord_ResponsePrincipal("IM Response Principal",1);
      
    general.setDataContainer(mdi);
    principal.setDataContainer(mdi);
    
    MATCHRecord_IM imrec = new MATCHRecord_IM("IM Record",1);
    
    imrec.setGeneralRecord(general);
    imrec.setPrincipalRecord(principal);

    imrec.setRecordDefinition();
    
    if(!imrec.generateRecord())
      throw new Exception("Unable to construct IM record object from data container: Error generating record string.");

    return imrec;
  }

} // class MATCHRecordBuilder
