package com.mes.match;

import org.apache.log4j.Category;
// log4j classes.
import org.apache.log4j.PropertyConfigurator;

/**
 * MATCHProcessorBean
 * 
 * Outside world sole contact point for running 'com.mes.match' bean.
 * 
 * Dependencies
 * ------------
 * - jakarta-log4j-1.1.3 (log4j.jar)
 * - com.mes.database package
 * - com.mes.support package
 * 
 * Setup
 * -----
 * - Working dir structure:
 *   -{working directory}
 * 
 */
public final class MATCHProcessorBean
{
  // create class log category
  static Category log = Category.getInstance(MATCHProcessorBean.class.getName());
  
  // constants
  public static final String              LOGCONFIG_FILENAME       = "MATCHProcessorBean_Log4j.cfg";

  
  // class functions
  
  public static void main(String[] args)
  {
    try {
      
      // get run duration time (allowed running time: 1 minute - 24 hours)
      if(args.length != 1) {
        usage("One and only one argument can be specified.");
        return;
      }
      final long milli_duration = Long.parseLong(args[0]);
      if(milli_duration<60000 || milli_duration>86400000)
        throw new Exception("The allowed milli-second running time range is 60000 (1 minute) - 86400000 (24 hours).");
    
      // run
      MATCHProcessorBean apb = new MATCHProcessorBean();
      apb.run(milli_duration);

    }
    catch(Exception e) {
      log.error("MATCHProcessorBean run Excepton: '"+e.getMessage()+"'");
    }
  
  }  // END main

  private static void usage(String errMsg)
  {
    System.err.println(errMsg);
    System.err.println(
       "\nUsage: MATCHProcessorBean -Duration [Milli-second value]\n"
    );
    System.exit(1);
  }
  
  private static void InitLogger()
  {
    PropertyConfigurator.configure(LOGCONFIG_FILENAME);
  }
  
  
  // object functions

  // construction
  public MATCHProcessorBean()
  {
    InitLogger();
  }


  /**
   * run()
   * -----
   * - Runs the MMS MATCHProcessorBean for a specified amount of time
   * - This function serves as a container to fully control running
   *    (starting and stopping) this bean 
   *    whereas the startup() and shutdown() functions serve to run this bean 
   *    for an indefinte amount of time
   */
  public final void run(long duration_millis)
  {
    log.info("*******************************");
    log.info("*** MATCH Processor STARTUP ***");
    log.info("*******************************");
    log.info("Starting MATCHProcessorBean (duration: "+(duration_millis/(1000*60))+" minutes)...");
    if(startup()) {
      log.info("MATCHProcessorBean started.");
      try {
        Thread.currentThread().sleep(duration_millis);
      }
      catch(InterruptedException ie) {
      }
      log.info("Shutting down MATCHProcessorBean...");
      if(shutdown())
        log.info("MATCHProcessorBean shutdown.");
      else
        log.error("Error occurred attempting to shutdown MATCHProcessorBean.");
    }
    log.info("********************************");
    log.info("*** SHUTDOWN MATCH Processor ***");
    log.info("********************************");
  }

  /**
   * startup()
   * ---------
   * - Starts the MATCHProcessorBean
   */
  public final boolean startup() 
  {
    return MATCHProcessorManager.startup();
  }
  
  /**
   * shutdown()
   * ---------
   * - Stops the MATCHProcessorBean
   */
  public final boolean shutdown()
  {
    MATCHProcessorManager.shutdown();
    return !MATCHProcessorManager.isStarted();
  }
  
  public final boolean isStarted()
  {
    return MATCHProcessorManager.isStarted();
  }

} // class MATCHProcessorBean
