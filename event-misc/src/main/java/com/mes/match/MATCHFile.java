package com.mes.match;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;

/**
 * MATCHFile
 * ---------
 *  - Represents a MATCH file
 *  - creates a MATCH file on disk based on member data
 *  - data member 'filename' is auto-created based on data held in the MATCHBatch type data members
 * 
 *  MATCHFile.filename FORMAT: "{MATCH bulk type}.{sequence num (000 - 999)}"
 */
class MATCHFile extends Object
{
  // create class log category
  static Category log = Category.getInstance(MATCHFile.class.getName());

  
  // constants
  public static final int         MATCHFILETYPE_START                 = 0;
  public static final int         MATCHFILETYPE_UNDEFINED             = MATCHFILETYPE_START;
  public static final int         MATCHFILETYPE_REQUEST               = 1;  // aka 'R' files
  public static final int         MATCHFILETYPE_RESPONSE              = 2;  // aka 'T' files
  public static final int         MATCHFILETYPE_END                   = MATCHFILETYPE_RESPONSE;
  
  
  // data members
  protected MATCHBatch            batch_add;                // 0..1 (non-null for request file type only)
  protected MATCHBatch            batch_inquiry;            // 0..1 (non-null for request file type only)
  protected Vector                batch_response;           // Vector containing 0..N response batches (non-null for response file type only)
  protected String                workingDir;               // directory the member 'filename' would reside in (relative or absolute naming supported)
  protected int                   filetype;                 // corres. to MATCHFILETYPE_{...}
  protected String                filename;                 // local name (no path)
  protected String                match_bulk_type;          // corres. to MATCHFileExpressProxy.BULKTYPE_{...}
  protected int                   file_seq_num;             // 1-999 (max)
  
  
  // class functions
  
  /**
   * parseResponseFiles
   * 
   * Process contents of all encountered MATCH response files filling the response MATCH data queue.
   * 
   * @param responses     FILLED IN: Container to hold the responses that get loaded from file.
   * @param bTestOrLive   true: test, false: live.
   * 
   * @exception Throws exception upon error or otherwise.
   */
  public static void parseResponseFiles(PersistQueue_MATCHDataQueueItem_Output responses,boolean bTestOrLive)
    throws Exception
  {
    try {
      
      if(responses==null)
        throw new Exception("Unable to parse response files - Null responses argument specified.");
      
      // get MATCH File Express specific params necessary to generate request file
      final MATCHFileExpressProxy mfeProxy = MATCHFileExpressProxy.getInstance();
      if(mfeProxy==null)
        throw new Exception("Parse response files failed: Unable to obtain MATCHFileExpress object instance.");
      
      final String workingDir = mfeProxy.getMFEDownloadDir();
      File dirspec = new File(workingDir);
      File[] fresponses = dirspec.listFiles();
      String fname=null,fcontents=null,batchspec=null;
      MATCHFile mf=null;
      long fbytelen=0;
      int numbytesread=0,respFileNum=-1,posStrt=0,posEnd=0,offset=0;
      char cbuf[] = null;
      FileReader fr = null;
      MATCHFile[] arr_respfiles = new MATCHFile[(int)fresponses.length];
      final int numStartResponses = responses.getNumElements();
      final String match_bulk_type=getMATCHFileBulkType(MATCHFILETYPE_RESPONSE,bTestOrLive);
      MATCHBatch response_batch=null;
      int batchnum=0;

      // construct array of MATCHFiles representing all discovered response files
      for(int i=0;i<fresponses.length; i++) {
        if(!fresponses[i].isFile()) // skip sub-directories
          continue;
        fname = fresponses[i].getName();
        log.info("Loading MATCH response file '"+fname+"'...");
        // read in the file all at once
        fbytelen = fresponses[i].length();
        cbuf = new char[(int)fbytelen];
        fr = new FileReader(fresponses[i]);
        numbytesread = fr.read(cbuf,0,(int)fbytelen);
        fr.close();
        log.debug("  fbytelen='"+fbytelen+"',numbytesread='"+numbytesread+"'");
        fcontents = String.valueOf(cbuf);
        
        mf = new MATCHFile(workingDir,fname,bTestOrLive);
        
        // load the response batches found in current file
        batchnum=offset=0; // reset
        while((posStrt=fcontents.indexOf("RHD",offset))>=0) {
          if((posEnd=fcontents.indexOf("RHD",posStrt+3))==-1)
            posEnd=fcontents.length();
          batchspec = fcontents.substring(posStrt,posEnd);
          log.debug("batchspec.length()="+batchspec.length());
          response_batch = MATCHBatch.generateResponseBatch(batchspec,++batchnum);
          mf.addBatch(response_batch);
          log.info("Response batch '"+response_batch.getName()+"' loaded from file '"+fname+"'.");
          offset = posEnd;
        }
        
        arr_respfiles[++respFileNum] = mf;
        log.info("Response file '"+mf.getFilename()+"' successfully loaded."+System.getProperty("line.separator")+System.getProperty("line.separator"));
      }
      
      MATCHRecord_Composite mdcr;
      MATCHDataQueueItem mdqi;
      boolean bArchiveResponseFile=false;
      String requestID=null;
      MATCHBatch respbatch=null;
      final Date now = new Date();
      
      // parse the found response files
      //log.warn("IMPT: Response file PARSING turned OFF.");
      for(int i=0;i<=respFileNum;i++) {
        log.info("Parsing MATCH response file '"+arr_respfiles[i].getFilename()+"'...");
        bArchiveResponseFile=true; // reset
        for(int j=0;j<arr_respfiles[i].batch_response.size();j++) {
          respbatch=(MATCHBatch)arr_respfiles[i].batch_response.elementAt(j);
          log.debug("Parsing response batch '"+respbatch.toString()+"'...");
          if(!respbatch.parseBatch(responses)) {
            bArchiveResponseFile=false;
            log.error("Error parsing response batch '"+respbatch.getName()+"'.  Continuing.");
          }
        }
        // archive response file?
        //log.warn("--> ARCHIVING MATCH RESPONSE FILES TURNED OFF!");
        if(bArchiveResponseFile)
          mfeProxy.archiveResponseFile(arr_respfiles[i].getFilename());
        else
          log.error("Response file '"+arr_respfiles[i].getFilename()+"' NOT archived due to one or more constituent batches having at least one parsing error.");
      }
      log.info("Parsed "+(responses.getNumElements()-numStartResponses)+" MATCH responses.");
    }
    catch(Exception e) {
      throw new Exception("Parse MATCH Response file exception: '"+e.getMessage()+"'.");
    }

  }
  
  /**
   * generateRequestFile()
   * 
   * Generates a MATCH request from a request data container
   * This is the converse of parseResponseFile()
   * 
   * @param requests The request data container
   * @param bTestOrLive true: test, false: live
   * 
   * @exception Throws exception upon error or otherwise
   */
  public static void generateRequestFile(PersistQueue_MATCHDataQueueItem requests, boolean bTestOrLive)
    throws Exception
  {
    try {
      
      log.info("Generating MATCH request file...");
      
      // ensure data container has valid data
      log.debug("Generate req. file - Validating MATCH request data...");
      if(requests.getNumElements()<1)
        throw new Exception("Generate MATCH request file failed: No MATCH requests specified in MATCH request data container.");
      
      // get MATCH File Express specific params necessary to generate request file
      log.debug("Getting MATCHFileExpressProxy...");
      final MATCHFileExpressProxy mfeProxy = MATCHFileExpressProxy.getInstance();
      if(mfeProxy==null)
        throw new Exception("Generate MATCH request file failed: Unable to obtain MATCHFileExpress object instance.");

      final int filetype = MATCHFILETYPE_REQUEST;
      final String match_bulk_type=getMATCHFileBulkType(filetype,bTestOrLive);
      final int file_seq_num = mfeProxy.getNextUploadFileSeqNum(match_bulk_type);
      
      // instantiate MATCHFile
      log.debug("About to instantiate request MATCHFile(filetype='"+filetype+"', file_seq_num='"+file_seq_num+"', bTestOrLive='"+bTestOrLive+"')...");
      MATCHFile mf = new MATCHFile(match_bulk_type,file_seq_num,bTestOrLive);
      
      // add request batches to MATCH file based on data held in data container
      log.debug("Creating 'add' batch...");
      MATCHBatch batch_add = MATCHBatch.generateRequestBatch(requests,MATCHBatch.MATCHBATCHTYPE_ADD,1);
      log.info("Adding 'add' batch ("+batch_add.getName()+") to MATCH file ("+mf.getFilename()+")...");
      mf.addBatch(batch_add);
      log.debug("Creating 'inquiry' batch...");
      MATCHBatch batch_inquiry = MATCHBatch.generateRequestBatch(requests,MATCHBatch.MATCHBATCHTYPE_INQUIRY,1);
      log.info("Adding 'inquiry' batch ("+batch_inquiry.getName()+") to MATCH file ("+mf.getFilename()+")...");
      mf.addBatch(batch_inquiry);
      
      // write MATCH request file to disk:
      //  - ensure MFE NOT currently transferring files
      //    otherwise disk write op may DISCONNECT MFE Unattended app!
      if(MATCHFileExpressProxy.getInstance().waitForNoTransferCondition()) {
        mf.writeFile();
        log.info("MATCH request file '"+mf.getFilename()+"' just written to disk.");
      } else
        throw new Exception("Unable to write MATCH request file '"+mf.getFilename()+"' to disk: Failed at establishing MATCH FileExpress - No Transfer condition.");
      
      MATCHDataQueueItem mdqi=null;
      final Date now = new Date();
      
      // update request status and date started
      for(Enumeration e=requests.getQueueElmntKeys();e.hasMoreElements();) {
        mdqi=(MATCHDataQueueItem)requests.getElement((String)e.nextElement());
        if(!mdqi.isValidStatus())
          continue;
        // update request status
        mdqi.setRequestStatus(MATCHDataQueueItem.MATCHREQUESTSTATUS_PENDING);
        // set request process start date
        mdqi.setProcessStartDate(now);
        log.info("***  MATCH Request '"+mdqi.getID()+"' initiated.  ***");
      }
    
    }
    catch(Exception e) {
      throw new Exception("Generate MATCH Request file exception: '"+e.getMessage()+"'.");
    }
  }
  
  /**
   * getMATCHFileBulkType()
   * 
   * Determines the MATCH file bulk type given the MATCH file type and if test or live?
   */
  public static String getMATCHFileBulkType(int filetype,boolean bTestOrLive)
    throws Exception
  {
    String match_bulk_type=MATCHFileExpressProxy.BULKTYPE_UNDEFINED;
    
    if(bTestOrLive) {
      // test file
      if(filetype==MATCHFILETYPE_REQUEST)
        match_bulk_type=MATCHFileExpressProxy.BULKTYPE_MATCHREQUEST_TEST;
      else
        match_bulk_type=MATCHFileExpressProxy.BULKTYPE_MATCHRESPONSE_TEST;
    } else {
      // live file
      if(filetype==MATCHFILETYPE_REQUEST)
        match_bulk_type=MATCHFileExpressProxy.BULKTYPE_MATCHREQUEST;
      else
        match_bulk_type=MATCHFileExpressProxy.BULKTYPE_MATCHRESPONSE;
    }
    
    if(match_bulk_type.equals(MATCHFileExpressProxy.BULKTYPE_UNDEFINED))
      throw new Exception("Unable to determine MATCH file Bulk type given filetype='"+filetype+"',bTestOrLive="+bTestOrLive);
    
    return match_bulk_type;
  }
  
  
  // Object functions
  
  /**
   * MATCHFile() Constructor - Intended for request type files only
   * 
   * @param match_bulk_type   MATCHFileExpressProxy.BULKTYPE_{...}
   * @param file_seq_num      file sequence number
   * @param bTestOrLive       test or live mode?
   */
  private MATCHFile(String match_bulk_type, int file_seq_num, boolean bTestOrLive)
    throws Exception
  {
    // verify seq num
    if(file_seq_num<0 || file_seq_num>999)
      throw new Exception("Unable to construct MATCH Request file - Invalid file sequence number specified ("+file_seq_num+").  The Valid range is: 1 - 999.");
    this.file_seq_num=file_seq_num;
    
    // verify bulk type
    if(!MATCHFileExpressProxy.isValidBulkFileType(match_bulk_type))
      throw new Exception("Unable to construct MATCH Request file - Invalid MATCH bulk type specified: '"+match_bulk_type+"'.");
    this.match_bulk_type=match_bulk_type;
    
    // set filename
    filename=match_bulk_type+"."+StringUtilities.rightJustify(Integer.toString(file_seq_num),3,'0');
    
    // auto-set 'workingDir' and ensure 'workingDir' exists
    workingDir = MATCHFileExpressProxy.getInstance().getMFEUploadDir()+File.separator+match_bulk_type;
    File f = new File(workingDir);
    if(!f.exists() && !f.mkdir())
      throw new Exception("Unable to construct MATCH Request file instance: Specified working directory '"+workingDir+"' wasn't created.");
    this.workingDir=workingDir;
    
    this.filetype=MATCHFILETYPE_REQUEST;
    
    batch_add=null;
    batch_inquiry=null;
    batch_response=null;
  }
  
  /**
   * MATCHFile() Constructor - Intended for response type files only
   * 
   * @param workingdir        dir path of filename
   * @param filename          Local filename of response file on disk.
   *                          ASSUMPTION: File exists on disk at: {workingDir}/{filename}
   * @param bTestOrLive       test or live mode?
   */
  private MATCHFile(String workingDir, String filename, boolean bTestOrLive)
    throws Exception
  {
    // validate filename
    if(filename.length()<11 || filename.charAt(filename.length()-4) != '.')
      throw new Exception("Unable to construct MATCH Response file object: Invalid filename '"+filename+"'.");
    
    // auto-set file_seq_num
    if((file_seq_num=StringUtilities.stringToInt(filename.substring(filename.length()-3),-1)) == -1)
      throw new Exception("Unable to construct MATCH Response file object: Invalid file extension on target file '"+filename+"'.");
    
    this.filetype=MATCHFILETYPE_RESPONSE;
    
    this.filename=filename;
    this.workingDir=workingDir;
    this.match_bulk_type = filename.substring(1,filename.length()-6);
    
    batch_add=null;
    batch_inquiry=null;
    batch_response=null;
  }
    
  // accessors
  public String getWorkingDir() { return workingDir; }
  public int getFileType() { return filetype; }
  public String getFilename() { return filename; }
  public String getMATCHBulkType() { return match_bulk_type; }
  public int getFileSeqNum() { return file_seq_num; }
  
  public boolean isFileOnDisk()
  {
    final String filespec = workingDir+File.separator+filename;
    File f = new File(filespec);
    return (f.exists());
  }
  
  protected boolean removeFileFromDisk()
  {
    log.warn("Removing MATCH file: '"+filename+"' from disk...");
    final String filespec = workingDir+File.separator+filename;
    File f = new File(filespec);
    return f.delete();
  }
  
  // mutators
  
  /**
   * addBatch()
   * 
   * Adds a MATCH batch to file.
   * 
   * @param batch The batch to add to this file.
   * @exception Throws exception upon unhandled file type, presence of file on disk or otherwise.
   */
  public void addBatch(MATCHBatch batch)
    throws Exception
  {
    // ensure file type (this) is of proper type
    if(filetype==MATCHFILETYPE_UNDEFINED)
      throw new Exception("Add batch to file failed - MATCH file type is undefined.");
    
    // only add when file NOT on disk
    if(filetype==MATCHFILETYPE_REQUEST && isFileOnDisk())
      throw new Exception("Add batch to file failed - This MATCH file '"+filename+"' currently resides on disk.");
    
    // set batch
    if(batch.getType()==MATCHBatch.MATCHBATCHTYPE_ADD) {
      if(batch_add!=null)
        log.warn("Replacing MATCH file add batch: '"+batch_add.getName()+"' with batch: '"+batch.getName()+"'.");
      batch_add = batch;
    } else if(batch.getType()==MATCHBatch.MATCHBATCHTYPE_INQUIRY) {
      if(batch_inquiry!=null)
        log.warn("Replacing MATCH file inquiry batch: '"+batch_add.getName()+"' with batch: '"+batch.getName()+"'.");
      batch_inquiry = batch;
    } else if(batch.getType()==MATCHBatch.MATCHBATCHTYPE_RESPONSE) {
      if(batch_response==null)
        batch_response=new Vector(2,1);
      batch_response.addElement(batch);
    } else
      throw new Exception("Add batch ('"+batch.getName()+"') to file ('"+filename+"') failed: Unhandled batch type.");
  }
  
  /**
   * writeFile Writes this file to disk.
   * 
   * @param requests Collection of MATCH data items to post to file.
   * @exception Throws exception when no valid batches are present in file.
   */
  public void writeFile()
    throws Exception
  {
    // file has at least one batch?
    if(batch_add==null && batch_inquiry==null)
      throw new Exception("Unable to write file to disk - No file batches currently exist.");
      
    // remove if already exists
    if(isFileOnDisk())
      removeFileFromDisk();
    
    final String filespec = workingDir+File.separator+filename;
    FileWriter fw = new FileWriter(filespec);
    
    // add 'add' batch to file
    if(batch_add!=null && batch_add.getNumDetailRecords()>0) {
      log.debug("Writing 'add' batch to file '"+filename+"'...");
      fw.write(batch_add.getBatchString());
    }
      
    // add 'inquiry' batch to file
    if(batch_inquiry!=null && batch_inquiry.getNumDetailRecords()>0) {
      log.debug("Writing 'inquiry' batch to file '"+filename+"'...");
      fw.write(batch_inquiry.getBatchString());
    }
    
    fw.close();
      
    log.debug("Finished writing MATCH file '"+filename+"'.");
  }
  
} // class MATCHFile
