package com.mes.match;

import java.io.File;
import java.util.Date;
import java.util.StringTokenizer;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.PropertiesFile;
import com.mes.support.StringUtilities;


/**
 * MATCHFileExpressProxy
 * 
 *  - Encapsulates the rules set forth by MasterCard's MATCH File Express software
 *     in particular:
 *      - directory structure for sending/receiving files to from MasterCard
 *        via File Express: dictates where to put request files to be uploaded
 *          and from where to get downloaded files.
 *      - file sequence numbering
 * 
 *  - SINGLETON object model
 */
class MATCHFileExpressProxy extends Object
{
  // create class log category
  static Category log = Category.getInstance(MATCHFileExpressProxy.class.getName());


  // Singleton
  public static MATCHFileExpressProxy getInstance()
    throws Exception
  {
    if(_instance==null)
      _instance = new MATCHFileExpressProxy();
    
    return _instance;
  }
  private static MATCHFileExpressProxy _instance = null;
  ///

  
  // constants
  private static final String         MFE_DIRNAME_ROOT_TEST_DEFAULT   = File.separator+"MFE_TEST"+File.separator+"BP_TEST";
  private static final String         MFE_DIRNAME_ROOT_PROD_DEFAULT   = File.separator+"MCONLINE"+File.separator+"MFE";
  // mfe dir struct related
  public static final String          MFE_DIRNAME_ARCHIVE             = "ARCHIVE";
  public static final String          MFE_DIRNAME_DOWNLOAD            = "DOWNLOAD";
  public static final String          MFE_DIRNAME_DOWNLOAD_ARCHIVE    = MFE_DIRNAME_DOWNLOAD+File.separator+"DWNLDARCHIVE";
  public static final String          MFE_DIRNAME_REPORTS             = "REPORTS";
  public static final String          MFE_DIRNAME_UPLOAD              = "UPLOAD";
  // mastercard [bulk] file types (partial)
  public static final String          BULKTYPE_UNDEFINED              = "";
  public static final String          BULKTYPE_MATCHREQUEST_TEST      = "R655";
  public static final String          BULKTYPE_MATCHRESPONSE_TEST     = "T657";
  public static final String          BULKTYPE_MATCHREQUEST           = "R651";
  public static final String          BULKTYPE_MATCHRESPONSE          = "T653";
  
  private static boolean              bTestMode                       = false;
    // true: test mode, false: "Production" mode

  private static long                 WAITTIME_MILLIS_MAX             = (1000*60*45); // 45 minutes
    // maximum allowed time to wait for establishing a No Transfer condition
  
  
  // data members
  private String mfeDir;      // MasterCard File Express root directory (relative or absolute supported)
  private Date   nwcStrtTme;  // No wait condition start time - used by waitForNoTransferCondition() only
  
  
  // class functions
  
  public static boolean isInTestMode()
  {
    return bTestMode;
  }
  
  public static void setTestMode(boolean bTestMode)
    throws Exception
  {
    if(_instance!=null)
      throw new Exception("Unable to set MATCHFileExpressProxy.bTestMode: Singleton object already instantiated.");
    
    MATCHFileExpressProxy.bTestMode=bTestMode;
    log.info("MATCHFileExpressProxy set to "+(MATCHFileExpressProxy.bTestMode? "TEST":"PRODUCTION")+" mode.");
  }
  
  /**
   * isValidBulkFileType()
   * 
   * Determines if specified bulk file type is valid (supported)
   */
  public static boolean isValidBulkFileType(String bulkfiletype)
  {
    return
      (bulkfiletype==null)?
       false:
       (   bulkfiletype.equals(BULKTYPE_MATCHREQUEST_TEST)
        || bulkfiletype.equals(BULKTYPE_MATCHRESPONSE_TEST)
        || bulkfiletype.equals(BULKTYPE_MATCHREQUEST)
        || bulkfiletype.equals(BULKTYPE_MATCHRESPONSE)
       );
  }
  
  
  // object functions
  
  /**
   * MATCHFileExpressProxy()
   * 
   * Constructor
   */
  private MATCHFileExpressProxy()
    throws Exception
  {
    nwcStrtTme=null;
    
    PropertiesFile props = new PropertiesFile("MATCHFileExpressProxy.properties");
    
    // test or production mode?
    log.info("MasterCard File Express Proxy in  '"+(bTestMode? "TEST":"PRODUCTION")+"' mode.");

    // set root (mfe) directory
    mfeDir = bTestMode?
              props.getString("MFE_DIRNAME_ROOT_TEST",MFE_DIRNAME_ROOT_TEST_DEFAULT)
              :props.getString("MFE_DIRNAME_ROOT_PROD",MFE_DIRNAME_ROOT_PROD_DEFAULT);
    log.info("Established MasterCard File Express root directory as: '"+mfeDir+"'.");
    
    // assert the directory structure based on the specified root directory
    
    File f=null;
    String subdir=null;
    
    // ensure the mfe dir exists
    f = new File(mfeDir);
    if(!f.exists())
      throw new Exception("Unable to instantiate MATCHFileExpressProxy - The specified MasterCard File Express root directory: '"+mfeDir+"' does not exist.");
    
    StringTokenizer st;
    String dir="";
    File fpath;
    
    // ensure all needed sub-dirs exist
    log.info("MATCHFileExpress() construction - Verifying sub-dirs exist...");
    for(int i=1; i<=6; i++) {
      switch(i) {
        case 1:   // archive
          subdir=MFE_DIRNAME_ARCHIVE;
          break;
        case 2:   // download
          subdir=MFE_DIRNAME_DOWNLOAD;
          break;
        case 3:   // download archive
          subdir=MFE_DIRNAME_DOWNLOAD_ARCHIVE;
          break;
        case 4:   // reports
          subdir=MFE_DIRNAME_REPORTS;
          break;
        case 5:   // upload
          subdir=MFE_DIRNAME_UPLOAD;
          break;
      }
      if(subdir.length()==0)
        continue;
      log.debug("MATCHFileExpress() construction - Verifying mfe sub-dir ("+subdir+")...");
      if(!(f = new File(mfeDir+File.separator+subdir)).exists()) {
        log.info("'"+subdir+"' sub-dir not found.  Creating...");
        st = new StringTokenizer(subdir,File.separator);
        dir="";
        while(st.hasMoreElements()) {
          dir+=File.separator+st.nextToken();
          log.debug("Verifying sub-dir '"+mfeDir+dir+"' exists...");
          if(!(fpath = new File(mfeDir+dir)).exists()) {
            log.debug("Creating non-existant sub-dir '"+(mfeDir+dir)+"'...");
            if(!fpath.mkdir())
              throw new Exception("MATCHFileExpress construction failed: Unable to create sub-directory: '"+(mfeDir+dir)+"'.");
          }
        }
      }
    }
    
  }
  
  // accessors
  public String getMFERootDir() { return mfeDir; }
  public String getMFEArchiveDir() { return mfeDir+File.separator+MFE_DIRNAME_ARCHIVE; }
  public String getMFEDownloadDir() { return mfeDir+File.separator+MFE_DIRNAME_DOWNLOAD; }
  public String getMFEDownloadArchiveDir() { return mfeDir+File.separator+MFE_DIRNAME_DOWNLOAD_ARCHIVE; }
  public String getMFEReportsDir() { return mfeDir+File.separator+MFE_DIRNAME_REPORTS; }
  public String getMFEUploadDir() { return mfeDir+File.separator+MFE_DIRNAME_UPLOAD; }
  
  /**
   * beginSession()
   * 
   * Performs any necessary tasks before file requests are posted 
   */
  public void beginSession()
  {
    // no startup tasks currently necessary
  }
  
  /**
   * endSession()
   * 
   * Performs any necessary tasks after response files (if any) are retreived
   * 
   */
  public void endSession()
  {
    // no shutdown tasks currently necessary
  }
  
  /**
   * getNextUploadFileSeqNum()
   * 
   * @param bulkfiletype The type of file per mfe.pdf specs doc.
   * 
   * @conditional No file locking scheme so this function may not return the correct number
   */
  public int getNextUploadFileSeqNum(String bulkfiletype)
    throws Exception
  {
    // validate bulk file type
    if(!isValidBulkFileType(bulkfiletype))
      throw new Exception("Get next MFE upload file sequence number failed: Invalid bulk file type specified: '"+bulkfiletype+"'");
    
    final String upDirname=mfeDir+File.separator+MFE_DIRNAME_UPLOAD+File.separator+bulkfiletype;
    File upDir = new File(upDirname);
    
    // ensure the bulk file type sub-dir exists
    if(!upDir.exists() && !upDir.mkdir())
      throw new Exception("Get next MFE upload file sequence number failed: Unable to create bulk file type sub-dir '"+bulkfiletype+"'.");

    // find the file on disk in the upload sub-dir with the greatest seq num
    String[] uploadFilenames = upDir.list();
    String sseqNum=null;
    int csn=0,gcsn=0; // current sequence number,greatest current sequence number
    for(int i=0; i<uploadFilenames.length; i++) {
      if(uploadFilenames[i].length()>5 && uploadFilenames[i].charAt(uploadFilenames[i].length()-4)=='.') {
        sseqNum=uploadFilenames[i].substring(uploadFilenames[i].length()-3,uploadFilenames[i].length());
        csn=Integer.parseInt(sseqNum);
        if(csn>gcsn)
          gcsn=csn;
      }
    }
    
    // return the greatest current sequence number + 1
    return gcsn+1;
  }
  
  /**
   * waitForNoTransferCondition()
   * 
   * Recursive function - Waits up to WAITTIME_MILLIS_MAX milli-seconds 
   *  for a "No Transfer" condition.
   * No Transfer condition is when:
   *  - "TRANSFERINPRG" file does NOT exist in any file type sub-directory under the UPLOAD sub-dir
   * 
   * NOTE: This function is NOT fool-proof.  
   * It may fail as there is no file-locking mechanism 
   * nor any communication between the process calling this function and the MasterCard File Express [Unattended] program.
   */
  public boolean waitForNoTransferCondition()
  {
    if(nwcStrtTme==null)
      nwcStrtTme = new Date();
    
    final Date now = new Date();
    
    if(now.getTime()-nwcStrtTme.getTime() > WAITTIME_MILLIS_MAX) {
      nwcStrtTme=null;
      return false;
    }
    
    File uploadDir = new File(mfeDir+File.separator+MFE_DIRNAME_UPLOAD);
    File[] upldFileTypSubDirs = uploadDir.listFiles();
    File[] requestFiles=null;
    
    for(int i=0;i<upldFileTypSubDirs.length;i++) {
      if(upldFileTypSubDirs[i].isDirectory()) {
        requestFiles = upldFileTypSubDirs[i].listFiles();
        for(int j=0;j<requestFiles.length;j++) {
          if(requestFiles[j].isFile() && requestFiles[j].getName().equals("MFEINPRG")) {
            try {
              Thread.currentThread().sleep(1000*2); // wait for 2 seconds
            }
            catch(InterruptedException e) {
            }
            waitForNoTransferCondition();
          }
        }
      }
    }
    
    nwcStrtTme=null;
    return true;
  }
  
  public void archiveResponseFile(String filename)
  {
    File f = new File(mfeDir+File.separator+MFE_DIRNAME_DOWNLOAD+File.separator+filename);
    if(!f.exists() || !f.isFile()) {
      log.error("Unable to archive file MATCH Resonse file '"+filename+"'.  File not found in download directory.");
      return;
    }
		File archivefDir = new File(mfeDir+File.separator+MFE_DIRNAME_DOWNLOAD_ARCHIVE);
    if(!archivefDir.isDirectory()) {
      log.error("Unable to archive file MATCH Resonse file '"+filename+"'.  Download archive directory not found.");
      return;
    }
    log.debug("About to archive file f.getName() '"+f.getAbsolutePath()+"' to archivefDir: '"+archivefDir.getAbsolutePath()+"'.");
    final String archiveFilename = StringUtilities.dateStamp(new Date())+"_"+f.getName();
    try {
      if(!f.renameTo(new File(archivefDir, archiveFilename)))
		  	log.error("MATCH Response file '"+f.getName()+"' NOT MOVED to download archive dir '"+MFE_DIRNAME_DOWNLOAD_ARCHIVE+"'.");
    }
    catch(Exception e) {
      log.error("archiveResponseFile() EXCEPTION: '"+e.getMessage()+"'.");
    }
  }
  
  
  public String toString()
  {
    return "mfe directory: '"+mfeDir+"'.";
  }
  
} // class MATCHFileExpressProxy
