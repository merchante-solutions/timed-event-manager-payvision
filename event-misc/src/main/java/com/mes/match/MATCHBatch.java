package com.mes.match;

import java.util.Date;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;

/**
 * MATCHBatch
 * ----------
 *  - Represents a MATCH batch
 *  - Houses the logic relating to batches and batching
 * 
 *  MATCHBatch.name FORMAT:
 *  -----------------------
 *  "mesmb_"{batch type}"_"{batch sequence num}
 * 
 */
class MATCHBatch extends Object
{
  // create class log category
  static Category log = Category.getInstance(MATCHBatch.class.getName());

  
  // constants
  public  static final int    MATCHBATCHTYPE_UNDEFINED  = 0;
  
  public  static final int    MATCHBATCHTYPE_ADD        = 1;
  public  static final int    MATCHBATCHTYPE_INQUIRY    = 2;
    // request type batches
  public  static final int    MATCHBATCHTYPE_RESPONSE   = 3;
    // response type batch
  
  public  static final int    MATCHBATCH_MAXINQUIRYRECS = 1000;
    // number of allowed composite inquiry records per batch
  
  
  // data members
  protected String        name;
  protected int           type;
  protected MATCHRecord   header;
  protected Vector        detail;   // vector of MATCHRecord_Composite objects
  protected MATCHRecord   trailer;
  
  
  // construction
  private MATCHBatch(String name,int type,MATCHRecord header,Vector detail,MATCHRecord trailer)
  {
    this.name=name;
    this.type=type;
    this.header=header;
    this.detail=detail;
    this.trailer=trailer;
    
    // ensure detail vector is non-null
    if(detail==null)
      detail = new Vector(1,1);
  }
  
  // static builder functions
  
  /**
   * generateResponseBatch()
   * 
   * Parses the specified batch string.
   * ASSERTION: expect to encounter only one header and one trailer record string.
   * ASSERTION: All general record strings are expected to have a complementing principal record string.
   * 
   * @param batchspec The batch string to be parsed.
   * @param batchSeqNum Batch sequence number.
   * 
   * @return MATCHBatch instance.
   */
  public static final MATCHBatch generateResponseBatch(String batchspec,int batchSeqNum)
    throws Exception
  {
    
    // construct batch name
    log.debug("MATCHBatch.generateResponseBatch() - Constructing batch name...");
    final String name = "mesmb_"+MATCHBATCHTYPE_RESPONSE+"_"+batchSeqNum;
    
    log.info("Generating MATCH Response batch '"+name+"'...");
    
    StringTokenizer st = new StringTokenizer(batchspec,MATCHRecord.DELIM_RECORD,false);
    final int numrecs = st.countTokens();
    
    if(numrecs<3)
      throw new Exception("Generate Response Batch error: Only "+numrecs+" records found in specified batch string.  Minimum 3 records required to construct valid response batch.");
    
    final int numdtlrecs = (numrecs-2)/2;   // header + (general+principal)*{numdtlrecs} + trailer = totalnumrecs
    int dtlcntr=0;  // detail composite record counter
    log.debug("generateResponseBatch() numdtlrecs="+numdtlrecs);
    String record = null;
    MATCHRecord mr=null,header=null,trailer=null,constituentdtl=null,compositedtl=null;
    Vector detail = new Vector(numdtlrecs,1); // (exclude header and trailer records)
    boolean bOK=true;
    
    while(st.hasMoreTokens() && bOK) {
      record=st.nextToken();
      //log.debug("record='"+record+"'");
      if(record.length()<MATCHRecord.MIN_RECORD_LENGTH) {
        log.warn("Record string '"+record+"' skipped - Length less than valid minimum.");
        continue;
      }
      mr = MATCHRecordBuilder.buildResponseRecord(record);
      switch(mr.getRecordType()) {
        case MATCHRecord.MATCHRECTYPE_RESPONSEHEADER:
          log.debug("Found Response header record.");
          if(header!=null)
            throw new Exception("Generate Response Batch fatal parsing error - Encountered second header record.  Aborted.");
          header = mr;
          break;
        case MATCHRecord.MATCHRECTYPE_RESPONSEGENERAL:
          log.debug("Found Response general detail record.");
          // build composite detail record (abstraction - container only)
          if(compositedtl!=null)
            throw new Exception("Generate Response Batch fatal parsing error - Encountered general detail record without complementing previous general record with a principal record.  Aborted.");
          // NOTE: Currently not using MATCHRecord_Composite.sourceID but should use it to reference the "source of the response" somehow to track "issues".
          switch(mr.getRequestType()) {
            case MATCHRecord.MATCHREQUESTTYPE_ADD:
              compositedtl = MATCHRecordBuilder.buildDetailCompositeAdd("Response");
              break;
            case MATCHRecord.MATCHREQUESTTYPE_INQUIRY:
              compositedtl = MATCHRecordBuilder.buildDetailCompositeInquiry("Response");
              break;
            default:
              throw new Exception("Generate Response Batch fatal error - Unhandled response record request type.");
          }
          // add general record to composite record
          ((MATCHRecord_Composite)compositedtl).setGeneralRecord(mr);
          break;
        case MATCHRecord.MATCHRECTYPE_RESPONSEPRINCIPAL:
          log.debug("Found Response principal detail record.");
          if(compositedtl==null)
            throw new Exception("Generate Response Batch fatal parsing error - Encountered principal record with no previous complimenting general record.  Aborted.");
          // set request type for this principal record since (importantly) principal response record strings have indeterminate request type!
          mr.setRequestType(compositedtl.getRequestType());
          // add principal record to composite record
          ((MATCHRecord_Composite)compositedtl).setPrincipalRecord(mr);
          // add complete composite detail record to batch
          dtlcntr++;
          log.debug("Adding composite detail record '"+dtlcntr+"' to batch detail vector...");
          detail.addElement(compositedtl);
          // IMPT: null-out compositedtl reference
          compositedtl=null;
          break;
        case MATCHRecord.MATCHRECTYPE_RESPONSETRAILER:
          log.debug("Found Response trailer record.");
          if(trailer!=null)
            throw new Exception("Generate Response Batch fatal parsing error - Encountered second trailer record.  Aborted.");
          trailer = mr;
          bOK=false;  // done
          break;
      }
    }
    
    // validate batch components
    if(header==null)
      throw new Exception("Generate Response Batch fatal parsing error No header record encountered.");
    if(trailer==null)
      throw new Exception("Generate Response Batch fatal parsing error No trailer record encountered.");
    if(detail.size()<1)
      throw new Exception("Generate Response Batch fatal parsing error - No detail records encountered.");
    
    return new MATCHBatch(name,MATCHBATCHTYPE_RESPONSE,header,detail,trailer);
  }
  
  /**
   * generateRequestBatch()
   * 
   * Internal class builder.
   * Builds MATCHBatch instances as a function of a set of MATCH requests and the sepcified batch type.
   * 
   * @param   pqmdqi      Group of MATCH requests the generated batch will be based on.
   * @param   type        MATCH batch type: MATCHBatch.MATCHBATCHTYPE_{...}.
   * 
   * @return  MATCHBatch instance.
   */
  public static final MATCHBatch generateRequestBatch(PersistQueue_MATCHDataQueueItem pqmdqi,int type,int batchSeqNum)
    throws Exception
  {
    
    // construct batch name
    final String name = "mesmb_"+type+"_"+batchSeqNum;
    
    log.info("Generating Request batch '"+name+"'...");
    
    int numDetailRecs = 0;
    String matchReqAction = MATCHDataQueueItem.MATCHREQUESTACTION_UNDEFINED;
    int matchDtlRecType = MATCHRecord.MATCHRECTYPE_UNDEFINED;
    
    // validate batch type
    // get number of to be detail records to be according to the desired batch type
    switch(type) {
      case MATCHBATCHTYPE_ADD:
        matchReqAction=MATCHDataQueueItem.MATCHREQUESTACTION_ADD;
        matchDtlRecType=MATCHRecord.MATCHRECTYPE_COMPOSITEADD;
        break;
      case MATCHBATCHTYPE_INQUIRY:
        matchReqAction=MATCHDataQueueItem.MATCHREQUESTACTION_INQUIRY;
        matchDtlRecType=MATCHRecord.MATCHRECTYPE_COMPOSITEINQUIRY;
        break;
      default:
        throw new Exception("Unable to instantiate MATCH batch: Unsupported batch type specified: '"+type+"'.");
    }
    for(Enumeration e=pqmdqi.getQueueElmntKeys();e.hasMoreElements();) {
      if(((MATCHDataQueueItem)pqmdqi.getElement((String)e.nextElement())).getRequestAction().equals(matchReqAction))
        numDetailRecs++;
    }
    log.debug("MATCHBatch.generateRequestBatch() numDetailRecs="+numDetailRecs);
    
    // enforce the number of detail records limit
    if(type==MATCHBATCHTYPE_INQUIRY && numDetailRecs>MATCHBATCH_MAXINQUIRYRECS)
      throw new Exception("Unable to instantiate a MATCHBatch - the number of generated batch detail records: "+pqmdqi.getNumElements()+" would exceed the allowed limit of: "+MATCHBATCH_MAXINQUIRYRECS+".");
    
    Vector detail = new Vector(numDetailRecs,1);
    MATCHDataQueueItem mdqi;
    int recDtlSeqNum_Constituent = 0;
    MATCHRecord_Composite detailCompositeRec = null;
    MATCHRecord detailConstituent = null;
    int dtlConType = MATCHRecord.MATCHRECTYPE_UNDEFINED;

    // create the batch detail records
    log.debug("MATCHBatch.generateRequestBatch() - Constructing batch detail records...");
    for(Enumeration e=pqmdqi.getQueueElmntKeys();e.hasMoreElements();) {
      if((mdqi=(MATCHDataQueueItem)pqmdqi.getElement((String)e.nextElement())).getRequestAction().equals(matchReqAction)) {
        // found an element of proper request action type
        
        // ensure valid status
        if(!mdqi.isValidStatus()) {
          log.warn("Encountered invalid MATCH Request status ('"+mdqi.getID()+"').  Request Skipped.");
          continue;
        }
        
        log.info("Creating MATCH detail record from MATCH queue item (ID: '"+mdqi.getID()+"',num General recs: "+mdqi.getNumGenerals()+",num Principal recs: "+mdqi.getNumPrincipals()+")...");
        
        // instantiate composite detail record
        switch(matchDtlRecType) {
          case MATCHRecord.MATCHRECTYPE_COMPOSITEADD:
            detailCompositeRec=MATCHRecordBuilder.buildDetailCompositeAdd(mdqi.getID());
            break;
          case MATCHRecord.MATCHRECTYPE_COMPOSITEINQUIRY:
            detailCompositeRec=MATCHRecordBuilder.buildDetailCompositeInquiry(mdqi.getID());
            break;
          default:
            detailCompositeRec=null;
        }
        if(detailCompositeRec==null)
          throw new Exception("Unable to instantiate MATCH batch: Unable to instantiate composite detail record.");

        // add general detail record(s)
        if(mdqi.getNumGenerals()>0) {
          // determine constituent record type of composite record
          if(type==MATCHBATCHTYPE_ADD)
            detailConstituent=MATCHRecordBuilder.buildDetailConstituent_AddGeneral(++recDtlSeqNum_Constituent,mdqi.getID(),mdqi.getRequestData());
          else if(type==MATCHBATCHTYPE_INQUIRY)
            detailConstituent=MATCHRecordBuilder.buildDetailConstituent_InquiryGeneral(++recDtlSeqNum_Constituent,mdqi.getID(),mdqi.getRequestData());
          // add constituent record to composite record
          detailCompositeRec.setGeneralRecord(detailConstituent);
          log.debug("General data added to MATCH detail record. (ID: "+mdqi.getID()+")");
        }
        
        // add principal detail record(s)
        if(mdqi.getNumPrincipals()>0) {
          // determine constituent record type of composite record
          if(type==MATCHBATCHTYPE_ADD)
            detailConstituent=MATCHRecordBuilder.buildDetailConstituent_AddPrincipal(++recDtlSeqNum_Constituent,mdqi.getRequestData());
          else if(type==MATCHBATCHTYPE_INQUIRY)
            // NOTE: mdqi --> record conversion FLAT FILES principal "objects" to a single principal record
            detailConstituent=MATCHRecordBuilder.buildDetailConstituent_InquiryPrincipal(++recDtlSeqNum_Constituent,mdqi.getRequestData());
          // add constituent record to composite record
          detailCompositeRec.setPrincipalRecord(detailConstituent);
          log.debug("Principal data added to MATCH detail record. (ID: "+mdqi.getID()+")");
        }
      
        // generate detail record string
        log.debug("Generating batch detail record string (ID: "+mdqi.getID()+")...");
        if(!detailCompositeRec.generateRecord())
          throw new Exception("Generate batch detail record of type '"+matchDtlRecType+"' failed for MATCH data queue item: '"+mdqi.getName()+"' (ID: "+mdqi.getID()+").");
        
        // add composite record to batch
        log.debug("Adding composite detail record '"+detailCompositeRec.getName()+"' to batch...");
        detail.addElement(detailCompositeRec);
        
      }
    }
    
    MATCHRecord_Header header=null;
    MATCHRecord_Trailer trailer=null;
    
    // create the header and trailer batch records
    log.debug("MATCHBatch.generateRequestBatch() - Constructing header and trailer batch records...");
    switch(type) {
      case MATCHBATCHTYPE_ADD:
        header = MATCHRecordBuilder.buildRequestAddHeader(batchSeqNum);
        break;
      case MATCHBATCHTYPE_INQUIRY:
        header = MATCHRecordBuilder.buildRequestInquiryHeader(batchSeqNum);
        break;
      case MATCHBATCHTYPE_UNDEFINED:
      default:
        throw new Exception("Unable to instantiate a MATCHBatch - Unhandled MATCH batch type specified: '"+type+"'.");
    }
    trailer = MATCHRecordBuilder.buildRequestTrailer(header,detail.size());
    // generate record strings for header and trailer
    log.debug("generating header and trailer record strings...");
    if(!header.generateRecord())
      throw new Exception("Generate header record failed for batch '"+name+"'.");
    else if(!trailer.generateRecord())
      throw new Exception("Generate trailer record failed for batch '"+name+"'.");
    
    final MATCHBatch mb = new MATCHBatch(name,type,header,detail,trailer);
    
    if(mb==null)
      throw new Exception("Unable to instantiate a MATCHBatch '"+name+"' - Construction failed.");
      
    log.info("MATCH Batch - '"+mb.getName()+"' successfully created.");
    
    return mb;
  }
  
  
  // accessors
  public String getName() { return name; }
  public int getType() { return type; }
  public MATCHRecord getHeader() { return header; }
  public MATCHRecord getTrailer() { return trailer; }
  public Enumeration getCmpsteDtlRecsEnumeration()
  {
    return detail==null? null:detail.elements();
  }
  
  public int getNumDetailRecords()
  {
    return detail.size();
  }
  
  /**
   * getBatchString()
   * 
   * @return    string representing the batch 
   *            which is composed of a header record, 1-n composite detail records
   *            and a trailer record
   */
  public String getBatchString()
  {
    // ensure batch type is request related
    if(!(type==MATCHBATCHTYPE_ADD || type==MATCHBATCHTYPE_INQUIRY)) {
      log.error("Batch strings are only retreived for request related batch types.  Get batch string failed.");
      return "";
    }
    
    int bufSize=0;

    for(Enumeration e=detail.elements();e.hasMoreElements();)
      bufSize += ((MATCHRecord)e.nextElement()).getRecSize();

    StringBuffer dtlBuf = new StringBuffer(bufSize);

    for(Enumeration e=detail.elements();e.hasMoreElements();)
      dtlBuf.append(((MATCHRecord)e.nextElement()).getRecord());
    
    final String batch_string = 
                               header.getRecord()
                               +dtlBuf.toString()
                               +trailer.getRecord();
    
    log.debug("batch_string='"+batch_string+"'");
    
    return batch_string;
  }
  
  /**
   * parseBatch()
   * 
   * Parses the contents held in this batch loading 
   * transferring loaded and parsed contents into the persist queue param
   * 
   * @param   pqmdqi  Persist queue that receives the parsed contents of the response batch
   * 
   */
  public boolean parseBatch(PersistQueue_MATCHDataQueueItem pqmdqi)
  {
    // ensure batch type is response related
    if(!(type==MATCHBATCHTYPE_RESPONSE)) {
      log.error("Parse batch ("+toString()+") failed - Only response related batches may be parsed.");
      return false;
    }
    
    if(pqmdqi==null) {
      log.error("Unable to parse batch: Null Persist queue parameter specified.");
      return false;
    }
    
    if(header==null || detail==null || trailer==null) {
      log.error("Parse batch ("+name+") failed - Header, Detail or Trailer is NULL.");
      return false;
    }
    
    // parse header
    log.debug("parsing batch header...");
    if(!header.parseRecord()) {
      log.error("Parse batch ("+name+") failed - Parse header record failed.");
      return false;
    }
    
    String mrid=null;
    MATCHRecord_Composite cdr=null;
    MATCHDataQueueItem mdqi=null;
    Vector rqor = new Vector(1,1);  // request originated response [parent] queue items temp container
    final Date now = new Date();
    
    // parse detail recs
    // assemble all request originated recs in vector initially
    log.debug("parsing batch detail...");
    for(int i=0;i<detail.size();i++) 
    {
      cdr=(MATCHRecord_Composite)detail.elementAt(i);
      log.debug(">> Parsing composite detail rec '"+cdr.getName()+"'...");
      if(!cdr.parseRecord()) 
      {
        log.warn("Parse batch ('"+name+"') composite detail record ('"+cdr.getName()+"') failed.");
      } 
      else 
      {
        try 
        {
          if( !cdr.IsResponseOriginatedRecord() ) 
          {
            mdqi = new MATCHDataQueueItem();
            rqor.addElement(mdqi);
            
            if( (mrid=cdr.getMRID()).length() < 1 ) 
            {
              mdqi.setRequestStatus(MATCHDataQueueItem.MATCHREQUESTSTATUS_RESPPARSEERROR);
              log.error("<><> UNBOUND request originated response record encountered - Unable to determine the originating MATCH Request ID for Response record '"+cdr.getRecord()+"'.  Record ignored. <><>");
            } 
            else 
            {
              mdqi.setRequestData(cdr.getDataContainer());  // bind to original request data
              mdqi.setID(mrid);
              mdqi.setErrorCodes(cdr.getErrorCodes());
              mdqi.setMCRefNum(cdr.getMCRefNum());
              mdqi.setVisaBin(cdr.getVisaBin());
              mdqi.setOrigMCRefNum(cdr.getOrigMCRefNum());
              mdqi.setResponseAction(cdr.getResponseAction());
              mdqi.setRequestStatus(cdr.IsRecordValid()? 
                                    MATCHDataQueueItem.MATCHREQUESTSTATUS_COMPLETE
                                    :MATCHDataQueueItem.MATCHREQUESTSTATUS_RESPPARSEERROR);
              if(mdqi.getRequestStatus()==MATCHDataQueueItem.MATCHREQUESTSTATUS_COMPLETE)
              {
                mdqi.setProcessEndDate(now);
              }
              log.info("** Request originated response record MRID:'"+mrid+"' parsed successfully. **");
            }
          }
        }
        catch(Exception e) {
          log.error("Exception occurred attempting to migrate record to object: '"+cdr.getRecord()+"'.  Continuing.");
        }
      }
    }
    log.debug("rqor.size()="+rqor.size());

    // parse trailer
    log.debug("parsing batch trailer...");
    if(!trailer.parseRecord())
      log.warn("Parse batch ("+name+") parse error - Parse trailer record failed.");
    
    String mcrn=null,origmcrn=null;
    MATCHResponse mresp=null;
    int numFoundRecs;
    
    
    // add all response originated responses as children under already added request originated responses
    // binding parent to child via: MC ref num <1--M(0..n)> Orig MC ref num
    log.debug("Binding response originated records (0..N) to their request originated counter-parts (1)...");
    try {
      for(int i=0;i<rqor.size();i++) {
        
        // get mc ref num of rec
        mdqi=(MATCHDataQueueItem)rqor.elementAt(i);
        mcrn = mdqi.getMCRefNum();
        
        numFoundRecs=0; // reset
        
        // bind response originated recs expected to exist in batch to current request originated response record
        if(MATCHRecord.HasRelatedResponseOriginatedRecords(mdqi.getResponseAction().charAt(0))) {
          log.debug("Searching for MATCH originated response records for '"+mdqi.getID()+"' (mcrn='"+mcrn+"').");
          for(int j=0;j<detail.size();j++) {
            cdr=(MATCHRecord_Composite)detail.elementAt(j);
            log.debug("checking cdr '"+cdr.getName()+"' for binding to request orig. response record '"+mdqi.getID()+"'.");
            if(cdr.IsResponseOriginatedRecord()) {
              origmcrn=cdr.getOrigMCRefNum();
              log.debug("origmcrn="+origmcrn);
              if(mcrn.equals(origmcrn)) {
                // found match!
                log.debug("found mc ref num/orig mc ref num match: '"+mcrn+"'.");
                // add response item (object)
                mresp = new MATCHResponse("",false,cdr.getGeneralRecord().getRecord(),cdr.getPrincipalRecord().getRecord(),cdr.getDataContainer());
                mdqi.setResponseItem(mresp);
                // mark rec as found
                numFoundRecs++;
              }
            }
          }
          if(numFoundRecs==0)
            log.error("No child response originated records were found to bind with parent request originated response record: MRID: '"+mdqi.getID()+"'.");
          else
            log.info("Found "+numFoundRecs+" response originated record(s) bound with request originated record MRID: '"+mdqi.getID()+"'.");
        }
        
        // add request originated response record to responses persist queue param
        log.debug("Adding request response '"+mdqi.getID()+"' to responses persist queue...");
        log.debug("**********");
        log.debug(mdqi.toString());
        log.debug("**********");
        pqmdqi.addElement(mdqi);
        
      }
    }
    catch(Exception e) {
      log.error("Error attempting to bind response originated records to their request originated response record counter-parts: '"+e.getMessage()+"'.");
      return false;
    }
    
    log.info("Batch parsing complete for batch '"+name+"'.");
    
    return true;
  }
  
  public String toString()
  {
    return "name='"+name+"', type='"+type+"'.";
  }

} // class MATCHBatch
