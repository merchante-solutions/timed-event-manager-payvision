package com.mes.match;

// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;

public class MATCHResponse extends Object
{
  // create class log category
  static Category log = Category.getInstance(MATCHResponse.class.getName());
  
  // data members
  public String         id;                 // {MATCH_REQUESTS_RESPONSEDATA}.{MRD_SEQ_NUM}
  public boolean        is_acknowledged;
  public String         general_record;     // un-parsed general record string
  public String         principal_record;   // un-parsed principal record string
  public MATCHDataItem  mdi;                // holds parsed data held in both general_record and principal_record data members
  private boolean       is_dirty;           // internal flag indicating if state was altered
                                            // FUTURE NOTE: 'is_dirty' should go in separate DAO assoc. class ideally
  
  // class functions
  // (none)
  
  // object functions
    
  // construction
  MATCHResponse()
  {
    id="";
    is_acknowledged=false;
    general_record="";
    principal_record="";
    mdi=null;
    is_dirty=true;
  }
  MATCHResponse(String id,boolean is_acknowledged,String general_record,String principal_record,MATCHDataItem mdi)
  {
    this.id=id;
    this.is_acknowledged=is_acknowledged;
    this.general_record=general_record;
    this.principal_record=principal_record;
    this.mdi=mdi;
    is_dirty=true;
  }
  
  // accessors
  public String getID() { return id; }
  public boolean isAcknowledged() { return is_acknowledged; }
  public String getGeneralRecord() { return general_record; }
  public String getPrincipalRecord() { return principal_record; }
  public MATCHDataItem getMATCHDataItem() { return mdi; }
  
  /**
   * isParsed()
   * 
   * Data members 'general_record' and 'principal_record' were parsed and data put
   * in MATCHDataItem data member?
   */
  public boolean isParsed()
  {
    return (mdi!=null && mdi.getNumGeneralItems()>0);
  }
  
  public boolean isDirty()
  {
    return is_dirty;
  }
  
  // mutators
  
  public void setID(String v)
  {
    if(v==null)
      return;
    id=v;
    is_dirty=true;
  }
  
  public void dirty()
  {
    is_dirty=true;
  }
  
  public void clean()
  {
    is_dirty=false;
  }
  
  public void acknowledge()
  {
    is_acknowledged=true;
    is_dirty=true;
  }
  
  public void setMATCHDataItem(MATCHDataItem mdi)
  {
    if(mdi==null)
      return;
    this.mdi=mdi;
    is_dirty=true;
  }
  
  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    final String mdis = mdi==null? "":(nl+mdi.toString());
    
    return
             nl+"* MATCHResponse *"
            +nl+"id="+id
            +nl+"is_acknowledged="+is_acknowledged
            +nl+"gen rec str="+((general_record!=null)? StringUtilities.leftJustify(general_record,20,' '):"-")
            +nl+"pri rec str="+((principal_record!=null)? StringUtilities.leftJustify(principal_record,20,' '):"-")
            +mdis
            +nl+"*****************"+nl;
  }
    
} // class MATCHResponse
