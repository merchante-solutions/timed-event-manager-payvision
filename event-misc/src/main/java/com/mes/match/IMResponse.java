package com.mes.match;

// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;

public class IMResponse extends MATCHResponse
{
  // create class log category
  static Category log = Category.getInstance(IMResponse.class.getName());
  
  // data members
  private boolean fromImpBlacklist = false;
  
  // class functions
  // (none)
  
  // object functions
    
  // construction
  IMResponse()
  {
    super();
  }
  IMResponse(MATCHDataItem mdi)
  {
    super();
    
    if(mdi!=null)
      this.mdi = mdi;
  }
  IMResponse(String id,boolean is_acknowledged,String general_record,String principal_record,MATCHDataItem mdi)
  {
    super(id,is_acknowledged,general_record,principal_record,mdi);
  }
  
  public boolean isFromImpBlacklist()
  {
    return fromImpBlacklist;
  }
  
  public void setImpFromBlacklist(boolean v)
  {
    fromImpBlacklist=v;
  }
  
  void setGeneralRecord(String v)
  {
    if(v!=null)
      this.general_record=v;
  }
  
  void setPrincipalRecord(String v)
  {
    if(v!=null)
      this.principal_record=v;
  }
  
  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    final String mdis = mdi==null? "":(nl+mdi.toString());
    
    return
             nl+"* IMResponse *"
            +nl+"id="+id
            +nl+"is_acknowledged="+is_acknowledged
            +nl+"gen rec str="+((general_record!=null)? StringUtilities.leftJustify(general_record,1012,' '):"-")
            +nl+"pri rec str="+((principal_record!=null)? StringUtilities.leftJustify(principal_record,1012,' '):"-")
            +mdis
            +nl+"*****************"+nl;
  }

} // class IMResponse
