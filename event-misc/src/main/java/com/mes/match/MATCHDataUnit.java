package com.mes.match;

import java.util.Enumeration;
import java.util.Hashtable;
// log4j classes.
import org.apache.log4j.Category;

class MATCHDataUnit extends Object
{
  // create class log category
  static Category log = Category.getInstance(MATCHDataUnit.class.getName());
  
  // constants

  // data members
  protected Hashtable dataItems;
        
  // construction
  public MATCHDataUnit()
  {
    dataItems = new Hashtable();
  }
        
  public void clear()
  {
    dataItems.clear();
  }
        
  public boolean unitExists(String nme)
  {
    return dataItems.containsKey(nme);
  }
  
  public String getUnit(String nme)
  {
    return
      (nme==null || nme.length()<1)?
        "":(dataItems.containsKey(nme))?
           (String)dataItems.get(nme):"";
  }
        
  public void setUnit(String nme,String val)
  {
    try {
            
      if(nme==null || val==null || nme.length()<1 || val.length()<1)
        return;
            
      dataItems.put(nme,val);
    }
    catch(Exception e) {
      log.error("MATCHDataUnit.setUnit() Exception: '"+e.getMessage()+"'.");
    }
  }
  
  public int getNumUnits()
  {
    return dataItems.size();
  }
        
  public String toString()
  {
    String nme,val;
    StringBuffer sb = new StringBuffer();
    sb.ensureCapacity(512);  // low-end estimate
    Enumeration e=dataItems.keys();
    while(e.hasMoreElements()) {
      nme=(String)e.nextElement();
      val=(String)dataItems.get(nme);
      sb.append(nme);
      sb.append(" = ");
      sb.append(val);
      sb.append(System.getProperty("line.separator"));
    }
    return sb.toString();
  }
        
} // class MATCHDataUnit
