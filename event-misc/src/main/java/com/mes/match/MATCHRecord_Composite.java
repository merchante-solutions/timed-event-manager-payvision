package com.mes.match;

// log4j classes.
import org.apache.log4j.Category;

/**
 * MATCHRecord_Composite
 * 
 * Represents composite MATCH record (add, inquiry or response record)
 * containing a general record and 0-5 principal records.
 * 
 */
class MATCHRecord_Composite extends MATCHRecord
{
  // create class log category
  static Category log = Category.getInstance(MATCHRecord_Composite.class.getName());

  // constants
  // (none)
  
  // data members
  protected MATCHRecord general_record;
  protected MATCHRecord principal_record;     // flat file fields for 5 principals
  
  // construction
  protected MATCHRecord_Composite(String name,int record_type,int sequenceNum)
  {
    super(name,record_type,sequenceNum);
    
    general_record=null;
    principal_record=null;
  }
  
  // accessors
  public MATCHRecord getGeneralRecord() { return general_record; }
  public MATCHRecord getPrincipalRecord() { return principal_record; }
  
  // mutators
  public void setGeneralRecord(MATCHRecord record)
    throws Exception
  {
    if(record==null)
      throw new Exception("Attempt to set general record to null.");
    
    general_record=record;
  }
  
  public void setPrincipalRecord(MATCHRecord record)
    throws Exception
  {
    if(record==null)
      throw new Exception("Attempt to set principal record to null.");
    
    principal_record=record;
  }
  
  protected String getFieldValue(String fldNme)
    throws Exception
  {
    throw new Exception("Unsupported method called: MATCHRecord_Composite.getFieldValue()");
  }
  protected void setFieldValue(String fldNme,String fldVal)
    throws Exception
  {
    throw new Exception("Unsupported method called: MATCHRecord_Composite.setFieldValue()");
  }
  
  /**
   * isRequestRecord
   * 
   * Sub-class override.
   */
  public boolean isRequestRecord()
    throws Exception
  {
    if(general_record==null)
      throw new Exception("Unable to determine if composite record is a request record:  Null general record.");
    
    return general_record.isRequestRecord();
  }

  /**
   * IsResponseOriginatedRecord()
   * 
   * Sub-class override.
   * NOTE: Ignores principal record and relies on presence of general record.
   */
  public boolean IsResponseOriginatedRecord()
    throws Exception
  {
    if(general_record==null)
      throw new Exception("Unable to determine if response originated record from composite: No general record specified in this composite record.");
    
    return general_record.IsResponseOriginatedRecord();
  }

  /**
   * HasRelatedResponseOriginatedRecords()
   * 
   * Sub-class override.
   * NOTE: Ignores principal record and relies on presence of general record.
   */
  public boolean HasRelatedResponseOriginatedRecords()
    throws Exception
  {
    if(general_record==null)
      throw new Exception("Unable to determine if related response originated records exist: No general record specified in this composite record.");
    
    return general_record.HasRelatedResponseOriginatedRecords();
  }
  
    
  /**
   * getRequestType()
   * 
   * Sub-class override.
   */
  public int getRequestType()
  {
    return (general_record==null)? 
              MATCHREQUESTTYPE_UNDEFINED:general_record.getRequestType();
  }

  /**
   * generateRecord()
   * 
   * Sub-class override.
   */
  public boolean generateRecord()
  {
    try {
      
      if(record.length()>0) // presume already generated
        return true;
      
      // ensure both general and principal constituent records are set
      if(general_record==null && principal_record==null) {
        log.error("Unable to generate composite record: Both general and principal constituent records not currently specifed.");
        return false;
      }
      
      StringBuffer recordBuf = new StringBuffer(0);
      MATCHRecord mr = null;
    
      // general record
      if(general_record!=null) {
        if(!general_record.generateRecord())
          log.error("An error occurred attempting to generate composite general record string (MATCH record: '"+general_record.getName()+"')+");
        else
          recordBuf.append(general_record.getRecord());
      } else
        log.warn("General record NOT generated.");
    
      // principal record
      if(principal_record!=null) {
        if(!principal_record.generateRecord())
          log.error("An error occurred attempting to generate composite principal record string (MATCH record: '"+principal_record.getName()+"')+");
        else
          recordBuf.append(principal_record.getRecord());
      } else
        log.warn("Principal record NOT generated.");
      
      // set 'record' data member
      record=recordBuf.toString();
      
      return true;
    }
    catch(Exception e) {
      log.error("generateRecord(COMPOSITE) - Exception: '"+e.getMessage()+"'.");
      return false;
    }
    
  }

  /**
   * parseRecord()
   * 
   * Sub-class override.
   */
  public boolean parseRecord()
  {
    try {
      
      if(isRecordValid)
        // assume already parsed successfully
        return true;
      
      // ensure data container exists
      if(mdi==null)
        mdi = new MATCHDataItem();

      // ensure general and principal constituent records are both not null
      if(general_record==null && principal_record==null) {
        log.error("Unable to parse composite record: No general nor principal constituent records exist.");
        return false;
      }
    
      boolean bParseOK=true;
      
      // parse constituents
      if(general_record!=null) {
        if(bParseOK=general_record.parseRecord())
          mdi.setGeneralDataTo(general_record.getDataContainer());
        else {
          log.error("Error attempting to parse general constituent record of composite record.");
          bParseOK=false;
        }
      }
      if(principal_record!=null) {
        if(bParseOK=principal_record.parseRecord())
          mdi.setPrincipalDataTo(principal_record.getDataContainer());
        else {
          log.error("Error attempting to parse principal constituent record of composite record.");
          bParseOK=false;
        }
      }
      
      // set this.isRecordValid
      return (isRecordValid=bParseOK);
    }
    catch(Exception e) {
      log.error("Exception parsing composite response record: '"+e.getMessage()+"'.");
      return false;
    }
    
  }
  
  public String getMRID()
    throws Exception
  {
    if(!isRecordValid)
    {
      throw new Exception("Unable to retreive MATCH Request ID - Invalid record.");
    }
          
    if( IsResponseOriginatedRecord() || general_record == null )
    {
      throw new Exception("Unable to retreive MATCH Request ID from composite record object.");
    }      
    
    return ((MATCHRecord_ResponseGeneral)general_record).getMRID();
  }
  
  public String getResponseAction()
    throws Exception
  {
    if(!isRecordValid)
      throw new Exception("Unable to retreive Response Action - Invalid record.");
    
    return
      (general_record!=null)?
        general_record.extractRecordValue("ACTION_INDICATOR")
        :(principal_record!=null)?
          principal_record.extractRecordValue("ACTION_INDICATOR")
          :"";
  }
  
  public String getErrorCodes()
    throws Exception
  {
    final String response_action = getResponseAction();
    
    return
      (general_record!=null)? 
        (response_action.equals("E"))? 
          general_record.extractRecordValue((general_record.getRequestType()==MATCHREQUESTTYPE_ADD)? "ERROR_CODES":"ERROR")
          :""
        :"";
  }
  
  public String getMCRefNum()
    throws Exception
  {
    final String response_action = getResponseAction();
    
    return
      (general_record!=null)? 
        (response_action.equals("A") || response_action.equals("N") || response_action.equals("P") || response_action.equals("R") || response_action.equals("M") || response_action.equals("H"))?
          general_record.extractRecordValue("MASTERCARD_REF_NUMBER")
          :""
        :"";
  }
  
  public String getVisaBin()
    throws Exception
  {
    final String response_action = getResponseAction();
    
    return
      (general_record!=null)? 
        (response_action.equals("M") || response_action.equals("H"))?
          general_record.extractRecordValue("VISA_BIN")
          :""
        :"";
  }

  public String getOrigMCRefNum()
    throws Exception
  {
    final String response_action = getResponseAction();
    
    return
      (general_record!=null)? 
        (response_action.equals("M") || response_action.equals("H") || response_action.equals("Q") || response_action.equals("Y"))?
          general_record.extractRecordValue("ORIG_MASTERCARD_REF_NUMBER")
          :""
        :"";
  }

} // class MATCHRecord_Composite
