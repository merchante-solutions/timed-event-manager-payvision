package com.mes.match;

// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;

/**
 * MATCHRecord_ResponseTrailer
 * -------------------
 * - extends MATCHRecord containing additional trailer specifiec data
 */
class MATCHRecord_ResponseTrailer extends MATCHRecord
{
  // create class log category
  static Category log = Category.getInstance(MATCHRecord_ResponseTrailer.class.getName());

  // constants
  public static int               RESPONSETRAILERTYPE_UNDEFINED   = 0;
  public static int               RESPONSETRAILERTYPE_NPM         = 1;
    // New Possible Match (Number 1) - (p,m,q,n)
  public static int               RESPONSETRAILERTYPE_RMP         = 2;
    // Retroactive Possible Match (Number 2) - (r,m,h,q,y)
  public static int               RESPONSETRAILERTYPE_OTHER       = 3;
    // Other (Number 3) - (a,e,c,d)

  
  // data members
  protected int             rttype;           // RESPONSETRAILERTYPE_{...}
  protected int             totalrecs_p;
  protected int             totalrecs_m;
  protected int             totalrecs_q;
  protected int             totalrecs_n;
  protected int             totalrecs_r;
  protected int             totalrecs_h;
  protected int             totalrecs_y;
  protected int             totalrecs_a;
  protected int             totalrecs_e;
  protected int             totalrecs_c;
  protected int             totalrecs_d;
  
  
  // construction
  protected MATCHRecord_ResponseTrailer(String name,int rttype)
  {
    super(name,MATCHRECTYPE_RESPONSETRAILER,0);
    
    this.rttype=rttype;
    
    this.totalrecs_p=0;
    this.totalrecs_m=0;
    this.totalrecs_q=0;
    this.totalrecs_n=0;
    this.totalrecs_r=0;
    this.totalrecs_h=0;
    this.totalrecs_y=0;
    this.totalrecs_a=0;
    this.totalrecs_e=0;
    this.totalrecs_c=0;
    this.totalrecs_d=0;
  }
  
  public int getTotalNumRecords()
  {
    return
       totalrecs_p
      +totalrecs_m
      +totalrecs_q
      +totalrecs_n
      +totalrecs_r
      +totalrecs_h
      +totalrecs_y
      +totalrecs_a
      +totalrecs_e
      +totalrecs_c
      +totalrecs_d;
  }
  
  /**
   * getFieldValue()
   * 
   * Sub-class override.
   */
  protected String getFieldValue(String fldNme)
    throws Exception
  {
    String value=null;
    
    if(fldNme.equals("TRAILER_LITERAL"))
      value="TRL";
    else if(fldNme.equals("ACN"))
      value=StringUtilities.rightJustify(MATCHRECCNST_ACN,7,'0');
    else if(fldNme.equals("TOTAL_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(getTotalNumRecords()),10,'0');
    else if(fldNme.equals("TOTAL_P_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(totalrecs_p),10,'0');
    else if(fldNme.equals("TOTAL_M_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(totalrecs_m),10,'0');
    else if(fldNme.equals("TOTAL_Q_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(totalrecs_q),10,'0');
    else if(fldNme.equals("TOTAL_N_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(totalrecs_n),10,'0');
    else if(fldNme.equals("TOTAL_R_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(totalrecs_r),10,'0');
    else if(fldNme.equals("TOTAL_H_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(totalrecs_h),10,'0');
    else if(fldNme.equals("TOTAL_Y_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(totalrecs_y),10,'0');
    else if(fldNme.equals("TOTAL_A_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(totalrecs_a),10,'0');
    else if(fldNme.equals("TOTAL_E_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(totalrecs_e),10,'0');
    else if(fldNme.equals("TOTAL_C_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(totalrecs_c),10,'0');
    else if(fldNme.equals("TOTAL_D_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(totalrecs_d),10,'0');
    else if(fldNme.equals("FILLER"))
      value="";
    else
      throw new Exception("Get trailer field value failed: field ('"+fldNme+"') un-mapped.");
    
    //log.debug("TRAILER - Field: '"+fldNme+"' mapped to value: '"+value+"'");
    return value;
  }

  /**
   * setFieldValue()
   * 
   * Sub-class override.
   */
  protected void setFieldValue(String fldNme, String fldVal)
    throws Exception
  {
    if(fldNme==null || fldNme.length()<1)
      throw new Exception("Unable to set response trailer field value: Null or empty field name specified.");
    if(fldVal==null)
      fldVal="";
    
    if(fldNme.equals("TOTAL_P_RECORDS"))
      totalrecs_p=StringUtilities.stringToInt(fldVal,0);
    else if(fldNme.equals("TOTAL_M_RECORDS"))
      totalrecs_m=StringUtilities.stringToInt(fldVal,0);
    else if(fldNme.equals("TOTAL_Q_RECORDS"))
      totalrecs_q=StringUtilities.stringToInt(fldVal,0);
    else if(fldNme.equals("TOTAL_N_RECORDS"))
      totalrecs_n=StringUtilities.stringToInt(fldVal,0);
    else if(fldNme.equals("TOTAL_R_RECORDS"))
      totalrecs_r=StringUtilities.stringToInt(fldVal,0);
    else if(fldNme.equals("TOTAL_H_RECORDS"))
      totalrecs_h=StringUtilities.stringToInt(fldVal,0);
    else if(fldNme.equals("TOTAL_Y_RECORDS"))
      totalrecs_y=StringUtilities.stringToInt(fldVal,0);
    else if(fldNme.equals("TOTAL_A_RECORDS"))
      totalrecs_a=StringUtilities.stringToInt(fldVal,0);
    else if(fldNme.equals("TOTAL_E_RECORDS"))
      totalrecs_e=StringUtilities.stringToInt(fldVal,0);
    else if(fldNme.equals("TOTAL_C_RECORDS"))
      totalrecs_c=StringUtilities.stringToInt(fldVal,0);
    else if(fldNme.equals("TOTAL_D_RECORDS"))
      totalrecs_d=StringUtilities.stringToInt(fldVal,0);
  
  }
  
  public String toString()
  {
    return
        "totalrecs_p="+totalrecs_p
       +" ,totalrecs_m="+totalrecs_m
       +" ,totalrecs_q="+totalrecs_q
       +" ,totalrecs_n="+totalrecs_n
       +" ,totalrecs_r="+totalrecs_r
       +" ,totalrecs_h="+totalrecs_h
       +" ,totalrecs_y="+totalrecs_y
       +" ,totalrecs_a="+totalrecs_a
       +" ,totalrecs_e="+totalrecs_e
       +" ,totalrecs_c="+totalrecs_c
       +" ,totalrecs_d="+totalrecs_d;
  }

} // class MATCHRecord_ResponseTrailer
