package com.mes.match;

import java.util.Date;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;

/**
 * MATCHDataQueueItem
 * 
 * aka 'MATCHRequest'
 * 
 * Encapsulates MATCHDataItem and associated request data
 * This class serves as a generic container for both MATCH Requests and Responses
 * 
 */
public final class MATCHDataQueueItem extends Object
{
  // create class log category
  static Category log = Category.getInstance(MATCHDataQueueItem.class.getName());

  // constants
  
  // request status
  // NOTE: "T" value in database indicates test record
  public static final String MATCHREQUESTSTATUS_NEW                       = "N";  // new
  public static final String MATCHREQUESTSTATUS_TEST                      = "T";  // used for testing only
  public static final String MATCHREQUESTSTATUS_RESUBMIT                  = "S";  // re-Submit (functions like new request status)
  public static final String MATCHREQUESTSTATUS_PENDING                   = "P";  // in process
  public static final String MATCHREQUESTSTATUS_LOADERROR                 = "L";  // load error
  public static final String MATCHREQUESTSTATUS_RESPPARSEERROR            = "R";  // error parsing response
  public static final String MATCHREQUESTSTATUS_COMPLETE                  = "C";  // complete
  
  // request types
  public static final String MATCHREQUESTACTION_UNDEFINED                 = "";
  public static final String MATCHREQUESTACTION_ADD                       = "A";
  public static final String MATCHREQUESTACTION_INQUIRY                   = "I";
  
  // *** responses ***
    // response pending (request sent)
  public static final String MATCHRESPONSEACTION_ADDSUCCESS               = "A";
  public static final String MATCHRESPONSEACTION_ADDERROR                 = "E";
  public static final String MATCHRESPONSEACTION_INQUIRYERROR             = "E";
  public static final String MATCHRESPONSEACTION_NOMATCH                  = "N";
  public static final String MATCHRESPONSEACTION_POSSIBLEMATCH            = "P";
  public static final String MATCHRESPONSEACTION_RETROPOSSIBLEMATCH       = "R";
  public static final String MATCHRESPONSEACTION_MERCHANTDELETED          = "D";
  public static final String MATCHRESPONSEACTION_MERCHANTCORRECTION       = "C";
  
  public static final String MATCHRESPONSEACTION_RELINQUISHED             = "L";
  public static final String MATCHRESPONSEACTION_MATCHDEEMED              = "M";
  public static final String MATCHRESPONSEACTION_BLACKLISTED              = "B";
    // in-house spec. actions

  // IMP Status
  public static final String  IMPSTATUS_UNDEFINED                         = "";
  public static final String  IMPSTATUS_OK                                = "O";
  public static final String  IMPSTATUS_RESULTSETTOOLARGE                 = "L";
  
  // queue action (related to app queue routing logic)
  public static final int    QUEUEACTION_UNDEFINED                        = 0;
  public static final int    QUEUEACTION_DECLINE                          = 1;
  public static final int    QUEUEACTION_RELEASE                          = 2;  // (MATCH Relinquish)
  public static final int    QUEUEACTION_ADDTOMATCHCREDIT                 = 3;
  public static final int    QUEUEACTION_ADDTOMATCHERROR                  = 4;
  public static final int    QUEUEACTION_ADDTOINTERNALMATCHCREDIT         = 5;
  public static final int    QUEUEACTION_IMRELEASE                        = 6;  // (IM Relinquish)
  
  // data members
  protected String          id;                   // "{MATCH_REQUESTS}.{MATCH_SEQ_NUM}"
  protected String          name;
  protected Date            processStartDate;
  protected Date            processEndDate;
  protected String          request_status;
  protected String          request_action;
  protected String          request_reason_code;
  protected String          request_comments;
  protected String          response_action;
  protected String          imp_response;
  protected String          imp_status;
  protected String          error_codes;
  protected String          mc_refnum;
  protected String          visa_bin;
  protected String          orig_mc_refnum;
  protected MATCHDataItem   dataRequest;        // request originating data
    // lazy initialization
  protected Vector          dataResponse;       // response originating data (usu. matching data that came back from MATCH db)
    // lazy initialization
    // vector of 0..N MATCHResponse objects
  protected Vector          imResponse;         // Internal Match responses
    // lazy initialization
    // vector of 0..N IMResponse objects
  protected int             queueAction;        // QUEUEACTION_{...}
  protected boolean         bResubmit;           // resubmit MATCH request?
  
  // construction
  MATCHDataQueueItem()
  {
    processStartDate=null;
    processEndDate=null;
    
    dataRequest=null;
    dataResponse=null;
    imResponse=null;
    
    clear();
  }
  
  // accessors
  public String getID() { return id; }
  public String getName() { return name; }
  public Date getProcessStartDate() { return processStartDate; }
  public Date getProcessEndDate() { return processEndDate; }
  public String getRequestStatus() { return request_status; }
  public String getRequestAction() { return request_action; }
  public String getRequestReasonCode() { return request_reason_code; }
  public String getRequestComments() { return request_comments; }
  public String getResponseAction() { return response_action; }
  public String getIMPResponse() { return imp_response; }
  public String getIMPStatus() { return imp_status; }
  public String getErrorCodes() { return error_codes; }
  public String getMCRefNum() { return mc_refnum; }
  public String getVisaBin() { return visa_bin; }
  public String getOrigMCRefNum() { return orig_mc_refnum; }
  public int getQueueAction() { return queueAction; }
  public boolean isMarkedForResubmit() { return bResubmit; }
  
  public boolean isImpBlacklisted()
  {
    return imp_response.equals(MATCHRESPONSEACTION_BLACKLISTED);
  }
  
  public String getAppSeqNum()
  {
    return (dataRequest==null)? "":dataRequest.getGeneralID();
  }
  
  public MATCHDataItem getRequestData() {return dataRequest; }
  
  public String getGeneralDataItem(String nme)
  {
    return (dataRequest==null)? "":dataRequest.getGeneralDataItem(nme);
  }
  public String getPrincipalDataItem(String principalID,String nme)
  {
    return (dataRequest==null)? "":dataRequest.getPrincipalDataItem(principalID,nme);
  }
  public String[] getPrincipalIDs()
  {
    return dataRequest.getPrincipalIDs();
  }
  public int getNumGenerals()
  {
    return dataRequest==null? 0:1;
  }
  public int getNumPrincipals()
  {
    return dataRequest==null? 0:dataRequest.getNumPrincipals();
  }
  public int getNumGeneralItems()
  {
    return dataRequest==null? 0:dataRequest.getNumGeneralItems();
  }
  public int getNumPrincipalItems(String principalID)
  {
    return dataRequest==null? 0:dataRequest.getNumPrincipalItems(principalID);
  }

  public Vector getResponseItems()
  {
    if(dataResponse==null)
      dataResponse = new Vector(0,1);
    
    return dataResponse;
  }
  public int getNumResponseItems()
  {
    return dataResponse==null? 0:dataResponse.size();
  }

  public Vector getIMResponseItems()
  {
    if(imResponse==null)
      imResponse = new Vector(0,1);
    
    return imResponse;
  }
  public int getNumIMResponseItems()
  {
    return imResponse==null? 0:imResponse.size();
  }
  
  public boolean isValidStatus()
  {
    return !(request_status.equals(MATCHREQUESTSTATUS_LOADERROR) || request_status.equals(MATCHREQUESTSTATUS_RESPPARSEERROR));
  }
  
  public boolean isComplete()
  {
    return request_status.equals(MATCHREQUESTSTATUS_COMPLETE);
  }
  
  public String getRequestStatusDescriptor()
  {
    if(request_status.equals(MATCHREQUESTSTATUS_NEW))
      return "New";
    else if(request_status.equals(MATCHREQUESTSTATUS_RESUBMIT))
      return "Re-Submit";
    else if(request_status.equals(MATCHREQUESTSTATUS_PENDING))
      return "Pending";
    else if(request_status.equals(MATCHREQUESTSTATUS_LOADERROR))
      return "Load Error";
    else if(request_status.equals(MATCHREQUESTSTATUS_RESPPARSEERROR))
      return "Response Parse Error";
    else if(request_status.equals(MATCHREQUESTSTATUS_COMPLETE))
      return "Complete";
    else
      return "Undefined";
  }
  
  // mutators
  public void setID(String v) { if(v!=null) id=v; }
  public void setName(String v) { if(v!=null) name=v; }
  public void setProcessStartDate(Date v) { if(v!=null) processStartDate=v; }
  public void setProcessEndDate(Date v) { if(v!=null) processEndDate=v; }
  public void setRequestStatus(String v) { if(v!=null) request_status=v; }
  public void setRequestAction(String v) { if(v!=null) request_action=v; }
  public void setRequestReasonCode(String v) { if(v!=null) request_reason_code=v; }
  public void setRequestComments(String v) { if(v!=null) request_comments=v; }
  public void setResponseAction(String v) { if(v!=null) response_action=v; }
  public void setIMPResponse(String v) { if(v!=null) imp_response=v; }
  public void setIMPStatus(String v) { if(v!=null) imp_status=v; }
  public void setErrorCodes(String v) { if(v!=null) error_codes=v; }
  public void setMCRefNum(String v) { if(v!=null) mc_refnum=v; }
  public void setVisaBin(String v) { if(v!=null) visa_bin=v; }
  public void setOrigMCRefNum(String v) { if(v!=null) orig_mc_refnum=v; }
  
  public void setRequestData(MATCHDataItem v)
  {
    if(v!=null)
      dataRequest=v;
  }

  public void setGeneralDataItem(String nme,String val)
  {
    if(dataRequest==null)
      dataRequest = new MATCHDataItem();
    dataRequest.setGeneralDataItem(nme,val);
  }
  public void setPrincipalDataItem(String principalID,String nme,String val)
    throws Exception
  {
    if(dataRequest==null)
      dataRequest = new MATCHDataItem();
    dataRequest.setPrincipalDataItem(principalID,nme,val);
  }
  
  public void setResponseItem(MATCHResponse mri)
  {
    if(mri==null)
      return;
    
    if(dataResponse==null)
      dataResponse = new Vector(0,1);
    
    dataResponse.addElement(mri);
  }
  
  public void clearResponseItems()
  {
    if(dataResponse!=null)
      dataResponse.removeAllElements();
  }
  
  public void setIMResponse(IMResponse imr)
  {
    if(imr==null)
      return;
    
    if(imResponse==null)
      imResponse = new Vector(0,1);
    
    imResponse.addElement(imr);
  }
  
  /**
   * addIMResponseGeneralItem()
   * 
   * Adds an IMResponse general name value pair using the general ID (appSeqNum) passed in 
   *  to determine whether or not the assoc. IMResponse object exists.
   * 
   * @NOTE: The IMResponse.id is not created until it is persisted.
   */
  public void addIMResponseGeneralItem(String gnrlID,String nme,String val)
  {
    IMResponse imr=null;
    boolean imExists=false;
    
    // determine if IM response already exists by general id
    for(int i=0;i<imResponse.size();i++) {
      imr=(IMResponse)imResponse.elementAt(i);
      if(imr.getMATCHDataItem().getGeneralDataItem("ID_GENERAL").equals(gnrlID)) {
        imExists=true;
        break;
      }
    }
    
    if(!imExists) {
      imr = new IMResponse();
      imr.getMATCHDataItem().setGeneralDataItem("ID_GENERAL",gnrlID);
      imResponse.addElement(imr);
    }
    
    imr.getMATCHDataItem().setGeneralDataItem(nme,val);
  }
  
  /**
   * addIMResponsePrincipalItem()
   * 
   * Adds an IMResponse principal name value pair using the general ID (appSeqNum) passed in 
   *  to determine whether or not the assoc. IMResponse object exists.
   * 
   * @NOTE: The IMResponse.id is not created until it is persisted.
   */
  public void addIMResponsePrincipalItem(String gnrlID,String principalID,String nme,String val)
  {
    IMResponse imr=null;
    
    for(int i=0;i<imResponse.size();i++) {
      imr=(IMResponse)imResponse.elementAt(i);
      if(imr.getMATCHDataItem().getGeneralDataItem("ID_GENERAL").equals(gnrlID)) {
        // found IM response
        try {
          imr.getMATCHDataItem().setPrincipalDataItem(principalID,nme,val);
        }
        catch(Exception e) {
          log.error("addIMResponsePrincipalItem() EXCEPTION: '"+e.getMessage()+"'.");
        }
        return;
      }
    }
    
  }

  public MATCHDataItem getIMPMATCHDataItem(String gnrlID)
  {
    IMResponse imr=null;
    MATCHDataItem mdi=null;
    
    for(int i=0;i<imResponse.size();i++) {
      imr=(IMResponse)imResponse.elementAt(i);
      if(imr.getMATCHDataItem().getGeneralDataItem("ID_GENERAL").equals(gnrlID)) {
        mdi=imr.getMATCHDataItem();
        break;
      }
    }
    
    return mdi;
  }
  
  public void clearIMResponseItems()
  {
    if(imResponse!=null)
      imResponse.removeAllElements();
  }
  
  public void clearResponseItem(MATCHResponse mr)
  {
    if(dataResponse!=null)
      dataResponse.removeElement(mr);
  }
  
  public void clearIMResponseItem(IMResponse imr)
  {
    if(imResponse!=null)
      while(imResponse.removeElement(imr));
  }
  
  public void setQueueAction(int queueAction)
  {
    this.queueAction=queueAction;
  }
  
  public void markToResubmit()
  {
    bResubmit=true;
  }
  
  public void clearResubmit()
  {
    bResubmit=false;
  }

  public void clear()
  {
    id="";
    name="";
    
    if(processStartDate!=null)
      processStartDate.setTime(0);
    else
      processStartDate = new Date(0);
    
    if(processEndDate!=null)
      processEndDate.setTime(0);
    else
      processEndDate = new Date(0);
    
    request_status="";
    request_action="";
    request_reason_code="";
    request_comments="";
    
    response_action="";
    
    imp_response="";
    imp_status="";
    
    queueAction=QUEUEACTION_UNDEFINED;

    if(dataRequest!=null)
      dataRequest.clear();
    
    if(dataResponse!=null)
      dataResponse.removeAllElements();
    
    if(imResponse!=null)
      imResponse.removeAllElements();

    bResubmit=false;
  }
  
  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    // queue component
    String qi =  
                    " id="+id
                +nl+" name="+name
                +nl+" processStartDate="+processStartDate
                +nl+" processEndDate="+processEndDate
                +nl+" request_status="+request_status
                +nl+" request_action="+request_action
                +nl+" request_reason_code="+request_reason_code
                +nl+" request_comments="+request_comments
                +nl+" response_action="+response_action
                +nl+" imp_response="+imp_response
                +nl+" error_codes="+error_codes
                +nl+" mc_refnum="+mc_refnum
                +nl+" visa_bin="+visa_bin
                +nl+" orig_mc_refnum="+orig_mc_refnum
                +nl+" bResubmit="+bResubmit;
    
    // request data
    String req = nl+((dataRequest!=null)? dataRequest.toString():"NO Request MATCH Data.");
    
    // response objects
    String rsp = nl+((dataResponse!=null)? dataResponse.toString():"NO Response MATCH Data.");

    // im response objects
    String imrsp = nl+((imResponse!=null)? imResponse.toString():"NO Response MATCH Data.");

    return "MATCHDataQueueItem(id: "+id+"): "
           +nl+qi
           +nl+req
           +nl+rsp
           +nl+imrsp;
  }

} // class MatchDataQueueItem
