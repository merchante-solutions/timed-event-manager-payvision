package com.mes.match;

// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;

/**
 * MATCHRecord_General
 * 
 * Extends MATCHRecord encapsulating request header specific record data and logic
 */
class MATCHRecord_General extends MATCHRecord
{
  // create class log category
  static Category log = Category.getInstance(MATCHRecord_General.class.getName());

  // constants
  // (none)

  // data members
  protected String          mrid;             // MATCH Request ID - used to embed in request record in order to map back from corres. response record
  
  // construction
  protected MATCHRecord_General(String name,int record_type,int sequenceNum)
  {
    super(name,record_type,sequenceNum);
    
    mrid="";
  }
  
  // accessors
  
  public String getMRID() { return mrid; }
  
  /**
   * getFieldValue()
   * 
   * Sub-class override.
   */
  protected String getFieldValue(String fldNme)
    throws Exception
  {
    String value=null;
    
    switch(record_type) {
      case MATCHRECTYPE_ADDGENERAL:
        if(fldNme.equals("TRANSACTION_CODE"))
          value="651";
        else if(fldNme.equals("ACTION_INDICATOR"))
          value="A";
        else if(fldNme.equals("ACN"))
          value=StringUtilities.rightJustify(MATCHRECCNST_ACN,7,'0');
        else if(fldNme.equals("REC_SEQ_NUM"))
          value=StringUtilities.rightJustify(Integer.toString(sequenceNum),6,'0');
        else if(fldNme.equals("RECORD_TYPE"))
          value="1";
        else if(fldNme.equals("FUTURE_EXPANSION"))
          value="";
        else if(fldNme.equals("REQUEST_COMMENTS") && mrid.length()>0) {
          // HACK (CRITICAL): Embed the request ID into the Comments field
          //                  in order to identify the original response from the response record
          value=mrid;
          log.info("REQUEST_COMMENTS field of General Request record '"+name+"' set to Request ID '"+mrid+"'.");
        } else if(mdi!=null)
          // direct native field (1-to-1 in this case)
          value=mdi.getGeneralDataItem(fldNme);
        break;
      case MATCHRECTYPE_INQUIRYGENERAL:
        if(fldNme.equals("TRANSACTION_CODE"))
          value="651";
        else if(fldNme.equals("ACTION_INDICATOR"))
          value="I";
        else if(fldNme.equals("ACN"))
          value=StringUtilities.rightJustify(MATCHRECCNST_ACN,7,'0');
        else if(fldNme.equals("REC_SEQ_NUM"))
          value=StringUtilities.rightJustify(Integer.toString(sequenceNum),6,'0');
        else if(fldNme.equals("RECORD_TYPE"))
          value="1";
        else if(fldNme.equals("NUMBER_OF_REQUIRED_PHONETIC_MATCHES"))
          value=Integer.toString(NUMBER_REQUIRED_PHONETIC_MATCHES);
        else if(fldNme.equals("FUTURE_EXPANSION"))
          value="";
        else if(fldNme.equals("TRANSACTION_REF_NUMBER") && mrid.length()>0) {
          // HACK (CRITICAL): Embed the request ID in the 'TRANSACTION_REF_NUMBER' field
          //                  in order to identify the original response from the response record
          value=StringUtilities.rightJustify(mrid,5,'0');
          log.info("TRANSACTION_REF_NUMBER field of General Request record '"+name+"' set to Request ID '"+mrid+"'.");
        } else if(mdi!=null)
          // direct native field (1-to-1 in this case)
          value=mdi.getGeneralDataItem(fldNme);
        break;
    }
    
    if(value==null)
      throw new Exception("Get request general field value failed: field ('"+fldNme+"') un-mapped.");
    
    //log.debug("Field: '"+fldNme+"' mapped to value: '"+value+"' (MATCHRecord.name='"+name+"',MATCHRecord.record_type='"+record_type+"'");
    return value;
  }
  
  // mutators
  
  public void setMRID(String v)
  {
    if(v!=null)
      mrid=v;
  }
  
  /**
   * setFieldValue()
   * 
   * No eligable fields exist.
   */
  protected void setFieldValue(String fldNme,String fldVal)
    throws Exception
  {
    throw new Exception("Erroneous method call: MATCHRecord_General.setFieldValue()");
  }

} // class MATCHRecord_General
