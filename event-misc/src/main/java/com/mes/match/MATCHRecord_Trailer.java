package com.mes.match;

// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;

/**
 * MATCHRecord_Trailer
 * -------------------
 * - extends MATCHRecord containing additional trailer specifiec data
 */
class MATCHRecord_Trailer extends MATCHRecord
{
  // create class log category
  static Category log = Category.getInstance(MATCHRecord_Trailer.class.getName());

  // constants
  // (none)

  // data members
  protected String                transmissionID;       // (same as what that in corres. header record)
  protected int                   totalrecs_a;          // total number of add records
  protected int                   totalrecs_i;          // total number of inquiry records
  
  // construction
  protected MATCHRecord_Trailer(String name,int record_type,int sequenceNum)
  {
    super(name,record_type,sequenceNum);
    
    this.transmissionID="";
  }
  
  /**
   * getFieldValue()
   * 
   * Sub-class override.
   */
  protected String getFieldValue(String fldNme)
    throws Exception
  {
    String value=null;
    
    // presume this object is of record_type: 'MATCHRecord_Trailer'
    if(fldNme.equals("RECORD_TYPE")) {
      if(record_type==MATCHRECTYPE_INQUIRYTRAILER)
        value="IDT";
      else if(record_type==MATCHRECTYPE_ADDTRAILER)
        value="ADT";
    }
    else if(fldNme.equals("TRANSACTION_CODE"))
      value="651";
    else if(fldNme.equals("ACN"))
      value=StringUtilities.rightJustify(MATCHRECCNST_ACN,7,'0');
    else if(fldNme.equals("TRANSMISSIONID"))
      value=transmissionID;
    else if(fldNme.equals("TOTAL_A_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(totalrecs_a),10,'0');
    else if(fldNme.equals("TOTAL_I_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(totalrecs_i),10,'0');
    else if(fldNme.equals("TOTAL_RECORDS"))
      value=StringUtilities.rightJustify(Integer.toString(totalrecs_a+totalrecs_i),10,'0');
    else if(fldNme.equals("FILLER"))
      value="";
    else
      throw new Exception("Get trailer field value failed: field ('"+fldNme+"') un-mapped.");
    
    //log.debug("TRAILER - Field: '"+fldNme+"' mapped to value: '"+value+"'");
    return value;
  }

  /**
   * setFieldValue()
   * 
   * Sub-class override.
   */
  protected void setFieldValue(String fldNme, String fldVal)
    throws Exception
  {
    if(fldNme==null || fldNme.length()<1)
      return;
    if(fldVal==null)
      fldVal="";
    
    //log.debug("MATCHRecord_Trailer.setFieldValue() fldNme="+fldNme+",fldVal="+fldVal);
    
    if(fldNme.equals("TOTAL_A_RECORDS"))
      totalrecs_a=Integer.parseInt(fldVal);
    else if(fldNme.equals("TOTAL_I_RECORDS"))
      totalrecs_i=Integer.parseInt(fldVal);
    else
      throw new Exception("Set field value failed: Unsupported field name: '"+fldNme+"'.");
  }

} // class MATCHRecord_Trailer
