package com.mes.match;

// log4j classes.
import org.apache.log4j.Category;

/**
 * PersistQueue_MATCHDataItem_Input
 * 
 * Persist queue containing any and all MATCH requests for a given merchant
 * OR Persist queue containing PENDING MATCH requests of a 
 * merchant having a spec. business name
*/
class PersistQueue_MATCHDataQueueItem_Merchant extends PersistQueue_MATCHDataQueueItem
{
  // create class log category
  static Category log = Category.getInstance(PersistQueue_MATCHDataQueueItem_Merchant.class.getName());
  
  // constants
  public static final int     MATCHDATA_UNDEFINED   = 0;
  public static final int     MATCHDATA_START       = 1;
  public static final int     MATCHDATA_REQUEST     = MATCHDATA_START;
  public static final int     MATCHDATA_GENERAL     = 2;
  public static final int     MATCHDATA_PRINCIPAL   = 3;
  public static final int     MATCHDATA_ALL         = 4;
  public static final int     MATCHDATA_END         = MATCHDATA_ALL;
  
  // data members
  protected String    merch_number;
  protected String    merch_business_name;
  protected int       matchDataLevel;
    // MATCHDATA_{...}: indicates the type(s) of MATCH data held in each member match request
  protected long      app_seq_num;
  
  // construction
  /**
   * PersistQueue_MATCHDataQueueItem_Merchant()
   * 
   * app seq num based constructor.
   */
  public PersistQueue_MATCHDataQueueItem_Merchant(long app_seq_num, int matchDataLevel)
    throws Exception
  {
    super(true);
    
    this.merch_number=null;
    this.merch_business_name=null;
    if(matchDataLevel<MATCHDATA_START || matchDataLevel>MATCHDATA_END)
      throw new Exception("Unable to construct PersistQueue_MATCHDataQueueItem_Merchant object: Invalid MATCH data level '"+matchDataLevel+"' specified.");
    this.matchDataLevel=matchDataLevel;
    this.app_seq_num=app_seq_num;
  }
  /**
   * PersistQueue_MATCHDataQueueItem_Merchant()
   * 
   * merchant number based constructor
   */
  public PersistQueue_MATCHDataQueueItem_Merchant(String merch_number, int matchDataLevel)
    throws Exception
  {
    super(true);
    
    this.merch_number=merch_number;
    this.merch_business_name=null;
    if(matchDataLevel<MATCHDATA_START || matchDataLevel>MATCHDATA_END)
      throw new Exception("Unable to construct PersistQueue_MATCHDataQueueItem_Merchant object: Invalid MATCH data level '"+matchDataLevel+"' specified.");
    this.matchDataLevel=matchDataLevel;
    this.app_seq_num=-1L;
  }
  /**
   * PersistQueue_MATCHDataQueueItem_Merchant()
   * 
   * merchant business name based constructor.
   * Represents PENDING match requests for given merchant spec. by given merchant business name.
   */
  public PersistQueue_MATCHDataQueueItem_Merchant(String merch_business_name)
    throws Exception
  {
    super(true);
    
    this.merch_number=null;
    this.merch_business_name=merch_business_name;
    this.matchDataLevel=MATCHDATA_REQUEST;
    this.app_seq_num=-1L;
  }
  
  // accessors
  
  public boolean isAppDeclined(long appSeqNum)
  {
    return dbom.isAppDeclined(appSeqNum);
  }

  public String getMerchNumber()
  {
    return merch_number==null? "":merch_number;
  }
  public String getMerchBusinessName()
  {
    return merch_business_name==null? "":merch_business_name;
  }
  public long getAppSeqNum()
  {
    return app_seq_num;
  }
  public int getMATCHDataLevel()
  {
    return matchDataLevel;
  }
  
  public void load()
    throws Exception
  {
    int matchdtaType=0;
    switch(matchDataLevel) {
      case MATCHDATA_REQUEST:
        matchdtaType=DBQueueOps_MATCHDataQueueItem.DBQ_MATCHDATA_REQUEST;
        break;
      case MATCHDATA_GENERAL:
        matchdtaType=DBQueueOps_MATCHDataQueueItem.DBQ_MATCHDATA_GENERAL;
        break;
      case MATCHDATA_PRINCIPAL:
        matchdtaType=DBQueueOps_MATCHDataQueueItem.DBQ_MATCHDATA_PRINCIPAL;
        break;
      case MATCHDATA_ALL:
        matchdtaType=DBQueueOps_MATCHDataQueueItem.DBQ_MATCHDATA_ALL;
        break;
    }
    
    // load based on the state of this object (which constructor was called)
    try {
      int numLoadedElements=0;
      if(app_seq_num!=-1L)
        numLoadedElements = dbom.load(Long.toString(app_seq_num,-1),DBQueueOps_MATCHDataQueueItem.DBQ_KEY_APP_SEQ_NUM,matchdtaType,objElements);
      else if(merch_number!=null)
        numLoadedElements = dbom.load(merch_number,DBQueueOps_MATCHDataQueueItem.DBQ_KEY_MERCNUMBER,matchdtaType,objElements);
      else
        numLoadedElements = dbom.load(merch_business_name,DBQueueOps_MATCHDataQueueItem.DBQ_KEY_PENDINGBYMERCNAME,matchdtaType,objElements);
      log.info("Loaded "+numLoadedElements+" elements into Merchant MATCH data item persist queue.");
    }
    catch(Exception e) {
      throw new Exception("MATCH data item merchant perist queue load exception: '"+e.getMessage()+"'.");
    }
  
  }

  public void save()
    throws Exception
  {
    super.save(DBQueueOps_MATCHDataQueueItem.DBQ_TYPE_MERCHANT);
  }

  public void updatePersist()
    throws Exception
  {
    super.save(DBQueueOps_MATCHDataQueueItem.DBQ_TYPE_MERCHANT);
  }

  public void updatePersist(String mdqiID)
    throws Exception
  {
    super.updatePersist(mdqiID,DBQueueOps_MATCHDataQueueItem.DBQ_TYPE_MERCHANT);
  }

} // class PersistQueue_MATCHDataQueueItem_Merchant
