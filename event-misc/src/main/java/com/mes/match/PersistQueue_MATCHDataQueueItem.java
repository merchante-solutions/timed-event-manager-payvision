package com.mes.match;

import java.util.Date;
// log4j classes.
import org.apache.log4j.Category;

abstract class PersistQueue_MATCHDataQueueItem extends PersistQueue
{
  // create class log category
  static Category log = Category.getInstance(PersistQueue_MATCHDataQueueItem.class.getName());
  
  public static final int               PERSIST_TIME_DAYS             = 30;
    // Persist MATCH Requests no longer than 'PERSIST_TIME_DAYS' days.

  // data members
  protected DBQueueOps_MATCHDataQueueItem dbom;
    // handles all db interaction

  // construction
  public PersistQueue_MATCHDataQueueItem(boolean bInputOrOutput)
    throws Exception
  {
    super(PERSIST_METHOD_DB,bInputOrOutput);
    
    dbom = new DBQueueOps_MATCHDataQueueItem();
  }
  
  
  public boolean addElement(Object objMdqi)
  {
    // ensure objMdqi is MATCHDataItem
    if(!objMdqi.getClass().getName().equals("com.mes.match.MATCHDataQueueItem")) {
      log.error("Add element failed: Object must be an MATCHDataQueueItem.");
      return false;
    }
    final MATCHDataQueueItem mdqi = (MATCHDataQueueItem)objMdqi;
    if(mdqi.getID().length()<1) {
      log.error("Add element failed: MATCHDataQueueItem element has no valid id which is required to key off of.");
      return false;
    }
    return super.addElement(mdqi.getID(),mdqi);
  }
  
  protected void load(int dbqType,boolean bOverwrite)
    throws Exception
  {
    try {
      
      // load all MATCH data items subject to file build process initiation
      final int numLoadedElements = dbom.load(dbqType,DBQueueOps_MATCHDataQueueItem.DBQ_MATCHDATA_ALL,objElements,bOverwrite);
      
      log.debug("Loaded "+numLoadedElements+" elements into MATCH data item persist queue.");
    }
    catch(Exception e) {
      throw new Exception("MATCH data item perist queue load exception: '"+e.getMessage()+"'.");
    }

  }
  
  public void loadElement(String mdqiID)
    throws Exception
  {
    MATCHDataQueueItem mdqi = objElements.containsKey(mdqiID)? 
                    (MATCHDataQueueItem)objElements.get(mdqiID):new MATCHDataQueueItem();
    
    if(!dbom.load(mdqiID,DBQueueOps_MATCHDataQueueItem.DBQ_MATCHDATA_ALL,mdqi))
      return;

    if(!objElements.containsKey(mdqiID))
      objElements.put(mdqiID,mdqi);
  }

  public void save(int dbqType)
    throws Exception
  {
    if(!dbom.save(objElements,dbqType))
      throw new Exception("Error attempting to save MATCH data items in persist queue.");
  }
  
  public void updatePersist(int dbqType)
    throws Exception
  {
    if(!dbom.save(objElements,dbqType))
      throw new Exception("Error attempting to persist MATCH data items in persist queue.");
  }

  public void updatePersist(String mdqiID,int dbqType)
    throws Exception
  {
    if(!objElements.containsKey((String)mdqiID))
      throw new Exception("Error attempting to persist MATCH data item of Req ID: '"+mdqiID+"'.  No such MATCH data item exists in this queue.");
    MATCHDataQueueItem mdqi = (MATCHDataQueueItem)objElements.get((String)mdqiID);
    if(!dbom.save(mdqi,dbqType))
      throw new Exception("Error attempting to persist MATCH data item of Req ID: '"+mdqiID+"'.");
  }

  public static void beginSession()
  {
    //DBQueueOps_MATCHDataQueueItem.getInstance().dbTransStart();
  }
  
  public static void endSession()
  {
    //removeOld();
    
    //DBQueueOps_MATCHDataQueueItem.getInstance().dbTransEnd();
  }
  
  /**
   * removeOld()
   * 
   * Removes "old" items from persistence media
   */
  private static final void removeOld()
  {
    // TODO: FIX  - flawed date logic - renders future date for fuck's sake
    // define "old" process end date
    final Date old = new Date((new Date()).getTime() - (1000*60*60*24*PERSIST_TIME_DAYS));
    
    log.info("REMOVING MATCH Requests older than: '"+old.toString()+"'...");
    (new DBQueueOps_MATCHDataQueueItem()).removeOldByProcessEndDate(old);
  }
  
} // class PersistQueue_MATCHDataQueueItem
