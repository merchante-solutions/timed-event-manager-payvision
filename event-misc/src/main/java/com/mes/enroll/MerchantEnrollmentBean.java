/*@lineinfo:filename=MerchantEnrollmentBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/enroll/MerchantEnrollmentBean.sqlj $

  Description:
  
  NewPasswordBean
  
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/10/03 11:02a $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.enroll;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.ButtonField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.PasswordField;
import com.mes.forms.Validation;
import com.mes.net.MailMessage;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class MerchantEnrollmentBean extends FieldBean
{
  public final static int     ENROLL_STATE_CONFIRM      = 1;
  public final static int     ENROLL_STATE_PASSWORD     = 2;
  public final static int     ENROLL_STATE_COMPLETE     = 3;
  public final static int     ENROLL_STATE_ERROR        = 99;
  
  private   int     enrollState     = ENROLL_STATE_CONFIRM;
  
  private   boolean fatalError      = false;
  
  public MerchantEnrollmentBean(int enrollState)
  {
    super();
    
    // set up fields based on the current enrollState
    this.enrollState = enrollState;
  }
  
  public int getEnrollState()
  {
    return this.enrollState;
  }
  
  public int getNextState()
  {
    int retVal = ENROLL_STATE_CONFIRM;
    
    switch(enrollState)
    {
      case ENROLL_STATE_CONFIRM:
        retVal = ENROLL_STATE_PASSWORD;
        break;
        
      case ENROLL_STATE_PASSWORD:
        retVal = ENROLL_STATE_COMPLETE;
        break;
        
      case ENROLL_STATE_COMPLETE:
        retVal = ENROLL_STATE_COMPLETE;
        break;
      
      default:
        break;
    }
    
    if(isFatalError())
    {
      retVal = ENROLL_STATE_ERROR;
    }
    
    return retVal;
  }
  
  public String getNextStateString()
  {
    return Integer.toString(getNextState());
  }
  
  public void addFields()
  {
    switch(enrollState)
    {
      case ENROLL_STATE_CONFIRM:
        setConfirmFields();
        break;
        
      case ENROLL_STATE_PASSWORD:
        setPasswordFields();
        break;
        
      case ENROLL_STATE_COMPLETE:
        setCompleteFields();
        break;
        
      case ENROLL_STATE_ERROR:
        // do nothing, no fields in this state
        break;
        
      default:
        // set enrollment state to ENROLL_STATE_CONFIRM
        this.enrollState = ENROLL_STATE_CONFIRM;
        setConfirmFields();
        break;
    }
    
    // add the hidden field for confirmation id
    fields.add(new HiddenField("confNum"));
    
    // add the submit button for all enroll states
    fields.add(new ButtonField("Submit"));
    
    fields.setGroupHtmlExtra("class=\"formFields\"");
    
    fields.setShowErrorText(true);
  }
  
  /*
  ** METHOD setConfirmFields
  **
  ** Sets up fields for the first page of enrollment:
  **
  ** Email Address
  ** Email Address Confirmation
  ** Merchant Number
  ** Confirmation Code
  */
  private void setConfirmFields()
  {
    // set up form fields
    EmailField  fEmail            = new EmailField("email", 75, 20, false);
    EmailField  fEmailConfirm     = new EmailField("emailConf", 75, 20, false);
    NumberField fMerchantNumber   = new NumberField("merchantNumber", 16, 16, false, 0);
    Field       fConfirmationCode = new Field("confirmation", 10, 10, false);
    
    // set up validations
    fEmailConfirm.addValidation( 
      new FieldEqualsFieldValidation(fEmail, "Email Address") );
      
    fConfirmationCode.addValidation(
      new ConfirmationCodeValidation(fMerchantNumber, Ctx) );
      
    // add fields to field thingy
    fields.add(fEmail);
    fields.add(fEmailConfirm);
    fields.add(fMerchantNumber);
    fields.add(fConfirmationCode);
  }
  
  private void setPasswordFields()
  {
    Field         fLoginName      = new Field("loginName", 75, 20, false);
    NumberField   fMerchantNumber = new NumberField("merchantNumber", 16, 16, false, 0);
    NumberField   fConfNum        = new NumberField("confNum", 16, 16, false, 0);
    PasswordField fPassword       = new PasswordField("password", 32, 20, false);
    PasswordField fConfirm        = new PasswordField("confirm", 32, 20, false);
    
    fLoginName.addValidation(
      new DesiredLoginNameValidation(Ctx));
      
    fPassword.addValidation(
      new MesPasswordValidation());
      
    fConfirm.addValidation(
      new FieldEqualsFieldValidation(fPassword, "Password"));
      
    fields.add(fLoginName);
    fields.add(fMerchantNumber);
    fields.add(fPassword);
    fields.add(fConfirm);
  }
  
  private void setCompleteFields()
  {
  }
  
  private void submitConfirm()
  {
    try
    {
      String  merchantNumber = getData("merchantNumber");
      String  confirmationCode = getData("confirmation");
      String  emailAddress = getData("email");
      String  confirmationId = getData("confNum");
      
      // add the start date if necessary
      /*@lineinfo:generated-code*//*@lineinfo:205^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_enrollment
//          set     start_date = sysdate,
//                  num_attempts = 0
//          where   merchant_number = :merchantNumber and
//                  confirmation_code = :confirmationCode and
//                  start_date is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_enrollment\n        set     start_date = sysdate,\n                num_attempts = 0\n        where   merchant_number =  :1  and\n                confirmation_code =  :2  and\n                start_date is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.enroll.MerchantEnrollmentBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   __sJT_st.setString(2,confirmationCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:213^7*/
      
      // add the last attempt date, email address, and conf id
      /*@lineinfo:generated-code*//*@lineinfo:216^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_enrollment
//          set     last_attempt_date = sysdate,
//                  num_attempts = num_attempts + 1,
//                  email_address = :emailAddress,
//                  confirmation_id = :confirmationId
//          where   merchant_number = :merchantNumber and
//                  confirmation_code = :confirmationCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_enrollment\n        set     last_attempt_date = sysdate,\n                num_attempts = num_attempts + 1,\n                email_address =  :1 ,\n                confirmation_id =  :2 \n        where   merchant_number =  :3  and\n                confirmation_code =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.enroll.MerchantEnrollmentBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,emailAddress);
   __sJT_st.setString(2,confirmationId);
   __sJT_st.setString(3,merchantNumber);
   __sJT_st.setString(4,confirmationCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:225^7*/
    }
    catch(Exception e)
    {
      logEntry("submitConfirm()", e.toString());
    }
  }
  
  private void submitPassword()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      String  merchantNumber  = getData("merchantNumber");
      String  confirmationId  = getData("confNum");
      String  loginName       = getData("loginName");
      String  password        = getData("password");
      
      // add the login name, mark as complete
      /*@lineinfo:generated-code*//*@lineinfo:246^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_enrollment
//          set     login_name      = :loginName,
//                  password        = :password,
//                  complete_date   = sysdate
//          where   merchant_number = :merchantNumber and
//                  confirmation_id = :confirmationId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_enrollment\n        set     login_name      =  :1 ,\n                password        =  :2 ,\n                complete_date   = sysdate\n        where   merchant_number =  :3  and\n                confirmation_id =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.enroll.MerchantEnrollmentBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setString(2,password);
   __sJT_st.setString(3,merchantNumber);
   __sJT_st.setString(4,confirmationId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:254^7*/
      
      // now we need to create the user with the password they have chosen
      
      // get email address from merchant_enrollment table
      String  emailAddress      = "unknown@unknown.com";
      
      /*@lineinfo:generated-code*//*@lineinfo:261^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  email_address
//          from    merchant_enrollment
//          where   merchant_number = :merchantNumber and
//                  confirmation_id = :confirmationId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  email_address\n        from    merchant_enrollment\n        where   merchant_number =  :1  and\n                confirmation_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.enroll.MerchantEnrollmentBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   __sJT_st.setString(2,confirmationId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.enroll.MerchantEnrollmentBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:267^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        emailAddress = rs.getString("email_address");
      }
      
      rs.close();
      it.close();
      
      // get merchant data from mif
      int     bankNumber      = 3941;
      String  merchantName    = "Unknown Merchant";
      
      /*@lineinfo:generated-code*//*@lineinfo:283^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dba_name,
//                  bank_number
//          from    mif
//          where   merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dba_name,\n                bank_number\n        from    mif\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.enroll.MerchantEnrollmentBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.enroll.MerchantEnrollmentBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:289^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        bankNumber    = rs.getInt("bank_number");
        merchantName  = rs.getString("dba_name");
      }
      
      rs.close();
      it.close();
      
      // get user type using bank number
      int   userType    = mesConstants.USER_MERCHANT;
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:307^9*/

//  ************************************************************
//  #sql [Ctx] { select  user_type
//            
//            from    merchant_user_types
//            where   bank_number = :bankNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  user_type\n           \n          from    merchant_user_types\n          where   bank_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.enroll.MerchantEnrollmentBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   userType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:313^9*/
      }
      catch(SQLException se)
      {
        userType = mesConstants.USER_MERCHANT;
        logEntry("submitPassword() - getting user type", se.toString());
      }
      
      // now create the user
      /*@lineinfo:generated-code*//*@lineinfo:322^7*/

//  ************************************************************
//  #sql [Ctx] { call create_update_user(
//            :userType, 
//            :loginName, 
//            :password, 
//            :merchantName, 
//            :emailAddress,
//            :merchantNumber,
//            '')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN create_update_user(\n           :1 , \n           :2 , \n           :3 , \n           :4 , \n           :5 ,\n           :6 ,\n          '')\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.enroll.MerchantEnrollmentBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,userType);
   __sJT_st.setString(2,loginName);
   __sJT_st.setString(3,password);
   __sJT_st.setString(4,merchantName);
   __sJT_st.setString(5,emailAddress);
   __sJT_st.setString(6,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:332^7*/
      
      // finally, send an email to the user
      MailMessage   msg   = new MailMessage();
      StringBuffer  body  = new StringBuffer("");
      
      
      body.append("Welcome and thank you for choosing BB&T Merchant Connection.  ");
      body.append("Your user name is ");
      body.append(loginName);
      body.append(" and your password is what you chose during the registration process.  ");
      body.append("You may log onto Merchant Connection anytime by going to http://www.bbandt.com/merchantservices/merchantconnection.");
      
      msg.setAddresses(MesEmails.MSG_ADDRS_BBT_ENROLLMENT);
      msg.addTo(emailAddress);
      msg.setSubject("Registration Confirmation");
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("submitPassword()", e.toString());
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  private void submitComplete()
  {
  }
  
  public void submitData()
  {
    // submit state-dependent data to database
    switch(enrollState)
    {
      case ENROLL_STATE_CONFIRM:
        submitConfirm();
        break;
        
      case ENROLL_STATE_PASSWORD:
        submitPassword();
        break;
        
      case ENROLL_STATE_COMPLETE:
        submitComplete();
        break;
      
      default:
        break;
    }
  }
  
  private void loadPasswordData()
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;
    
    try
    {
      // only do this if the login name wasn't set by submission
      if(getData("loginName").equals(""))
      {
        String  userName  = "";
        String  mNumber   = getData("merchantNumber");
        String  confId    = getData("confNum");
      
        // load the email address into the desired login name field
        /*@lineinfo:generated-code*//*@lineinfo:403^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  email_address
//            from    merchant_enrollment
//            where   merchant_number = :mNumber and
//                    confirmation_id = :confId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  email_address\n          from    merchant_enrollment\n          where   merchant_number =  :1  and\n                  confirmation_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.enroll.MerchantEnrollmentBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,mNumber);
   __sJT_st.setString(2,confId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.enroll.MerchantEnrollmentBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:409^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          setData("loginName", rs.getString("email_address"));
        }
        else
        {
          // confirmation number was incorrect -- this is an error condition
          fatalError = true;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadPasswordData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  public void loadData()
  {
    // perform state-specific data loading
    switch(enrollState)
    {
      case ENROLL_STATE_CONFIRM:
        // set the confnum field to the current time millis
        setData("confNum", String.valueOf(System.currentTimeMillis()));
        break;
        
      case ENROLL_STATE_PASSWORD:
        loadPasswordData();
        break;
        
      case ENROLL_STATE_COMPLETE:
        break;
        
      default:
        break;
    }
  }
  
  public class MesPasswordValidation extends SQLJConnectionBase
    implements Validation
  {
    String  errorText = "";
    
    public MesPasswordValidation()
    {
    }
    
    // passwords must be at least 6 characters.  May add more validation later
    public boolean validate(String password)
    {              
      boolean validated = true;
      
      try
      {
        if(password == null || password.length() < 6)
        {
          validated = false;
          errorText = "Password length must be at least 6 characters";
        }
      }
      catch(Exception e)
      {
        logEntry("validate(" + password + ")", e.toString());
      }
      
      return validated;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }
  
  public class DesiredLoginNameValidation extends SQLJConnectionBase
    implements Validation
  {
    String errorText    = "";
    
    public DesiredLoginNameValidation(DefaultContext Ctx)
    {
      this.Ctx = Ctx;
    }
    
    public boolean validate(String desiredLoginName)
    {
      boolean validated = true;
      
      try
      {
        // see if this name is a) long enough and b) not already used
        if(desiredLoginName.length() < 6)
        {
          errorText = "Must be at least 6 characters long";
          validated = false;
        }
        
        if(validated)
        {
          // now check to see if the user name is already in use
          int userCount = 0;
          
          /*@lineinfo:generated-code*//*@lineinfo:521^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(login_name)
//              
//              from    users
//              where   login_name = :desiredLoginName
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(login_name)\n             \n            from    users\n            where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.enroll.MerchantEnrollmentBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,desiredLoginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   userCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:527^11*/
          
          if(userCount > 0)
          {
            errorText = "Already in use, please try a different user id";
            validated = false;
          }
        }
      }
      catch(Exception e)
      {
        logEntry("validate(" + desiredLoginName + ")", e.toString());
      }
      
      return validated;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }
  
  public class ConfirmationCodeValidation extends SQLJConnectionBase
    implements Validation
  {
    private Field     merchantField     = null;
    private String    errorText         = "";
    
    public ConfirmationCodeValidation(Field merchantField, DefaultContext Ctx)
    {
      this.merchantField = merchantField;
      this.Ctx = Ctx;
    }
    
    public boolean validate(String confirmationCode)
    {
      boolean validated = false;
      
      try
      {
        // check to make sure that the confirmation code matches the merchant
        // number in the MERCHANT_ENROLLMENT table
        int   confCount = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:572^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//            
//            from    merchant_enrollment
//            where   merchant_number = :merchantField.getData() and
//                    confirmation_code = :confirmationCode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1885 = merchantField.getData();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n           \n          from    merchant_enrollment\n          where   merchant_number =  :1  and\n                  confirmation_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.enroll.MerchantEnrollmentBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_1885);
   __sJT_st.setString(2,confirmationCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   confCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:579^9*/
        
        validated = (confCount > 0);
        
        if(validated)
        {
          // additional check to see if the merchant has already been set up
          int completeCount = 0;
          
          /*@lineinfo:generated-code*//*@lineinfo:588^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//              
//              from    merchant_enrollment
//              where   merchant_number = :merchantField.getData() and
//                      confirmation_code = :confirmationCode and
//                      complete_date is null
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1886 = merchantField.getData();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n             \n            from    merchant_enrollment\n            where   merchant_number =  :1  and\n                    confirmation_code =  :2  and\n                    complete_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.enroll.MerchantEnrollmentBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_1886);
   __sJT_st.setString(2,confirmationCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   completeCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:596^11*/
          
          validated = (completeCount > 0);
          
          if(!validated)
          {
            errorText = "This merchant number has already been enrolled";
          }
        }
        else
        {
          errorText = "Invalid merchant number or confirmation code";
        }
      }
      catch(Exception e)
      {
        logEntry("validate(" + confirmationCode + ", " + merchantField.getData() + ")", e.toString());
      }
      
      return validated;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }
  
  public boolean isFatalError()
  {
    return this.fatalError;
  }
}/*@lineinfo:generated-code*/