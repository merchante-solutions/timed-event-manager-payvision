/*@lineinfo:filename=BBTMerchantEmailBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/enroll/BBTMerchantEmailBean.sqlj $

  Description:

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/24/03 11:30a $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.enroll;

import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.EmailField;
import com.mes.forms.FieldBean;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import sqlj.runtime.ref.DefaultContext;

public class BBTMerchantEmailBean extends FieldBean
{
  public boolean    success     = false;
  
  public void setProperties(HttpServletRequest request, UserBean user)
  {
    try
    {
      this.user = user;
      
      // register bean with MesSystem
      register(user.getLoginName(), request);
      
      // add form fields to vector
      addFields();
      
      if(!HttpHelper.getString(request, "Submit", "").equals(""))
      {      
        setFields(request);
      }
      
      if(getData("Submit").equals("Submit"))
      {
        if(isValid())
        {
          submitData();
          success = true;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
  }
  
  private void submitData()
  {
    int     addrCount   = 0;
    try
    {
      // see if data already exists for this merchant
      /*@lineinfo:generated-code*//*@lineinfo:78^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//          
//          from    merchant_enrollment_bbt
//          where   merchant_number = :getData("merchantNumber")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1856 = getData("merchantNumber");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n         \n        from    merchant_enrollment_bbt\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.enroll.BBTMerchantEmailBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_1856);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   addrCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^7*/
      
      if(addrCount > 0)
      {
        // update existing data -- also make sure that a new email will be sent
        /*@lineinfo:generated-code*//*@lineinfo:89^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_enrollment_bbt
//            set     email_address = :getData("email"),
//                    date_created = sysdate,
//                    date_email_set = null
//            where   merchant_number = :getData("merchantNumber")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1857 = getData("email");
 String __sJT_1858 = getData("merchantNumber");
   String theSqlTS = "update  merchant_enrollment_bbt\n          set     email_address =  :1 ,\n                  date_created = sysdate,\n                  date_email_set = null\n          where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.enroll.BBTMerchantEmailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1857);
   __sJT_st.setString(2,__sJT_1858);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:96^9*/
      }
      else
      {
        // add new data to the merchant_enrollment_bbt table
        /*@lineinfo:generated-code*//*@lineinfo:101^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant_enrollment_bbt
//            (
//              merchant_number,
//              email_address,
//              date_created
//            )
//            values
//            (
//              :getData("merchantNumber"),
//              :getData("email"),
//              sysdate
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1859 = getData("merchantNumber");
 String __sJT_1860 = getData("email");
   String theSqlTS = "insert into merchant_enrollment_bbt\n          (\n            merchant_number,\n            email_address,\n            date_created\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n            sysdate\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.enroll.BBTMerchantEmailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1859);
   __sJT_st.setString(2,__sJT_1860);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:115^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("submitData()", e.toString());
    }
  }
  
  private void addFields()
  {
    // set up form fields
    NumberField     fMerchantNumber     = new NumberField("merchantNumber", 16, 16, false, 0);
    EmailField      fEmail              = new EmailField("email", 75, 25, false);
    EmailField      fEmailConfirm       = new EmailField("emailConf", 75, 25, false);
    
    // add validations
    fEmailConfirm.addValidation(new FieldEqualsFieldValidation(fEmail, "Email Address"));
    
    //fMerchantNumber.addValidation(new MerchantEmailExistsValidation(Ctx));
    
    // add fields to vector
    fields.add(fMerchantNumber);
    fields.add(fEmail);
    fields.add(fEmailConfirm);
    fields.add(new ButtonField("Submit"));
    
    fields.setGroupHtmlExtra("class=\"formFields\"");
  }
  
  public class MerchantEmailExistsValidation
    implements Validation
  {
    String            errorText   = "";
    DefaultContext    Ctx         = null;
    
    public MerchantEmailExistsValidation(DefaultContext Ctx)
    {
      this.Ctx = Ctx;
    }
    
    public boolean validate(String merchantNumber)
    {
      boolean   validated     = true;
      try
      {
        // see if this merchant number has already been added
        int   addrCount   = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:164^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//            
//            from    merchant_enrollment_bbt
//            where   merchant_number = :merchantNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n           \n          from    merchant_enrollment_bbt\n          where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.enroll.BBTMerchantEmailBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   addrCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:170^9*/
        
        if(addrCount > 0)
        {
          errorText   = "This merchant has already been assigned an email address";
          validated = false;
        }
      }
      catch(Exception e)
      {
        logEntry("validate(" + merchantNumber + ")", e.toString());
      }
      
      return validated;
    }
    
    public String getErrorText()
    {
      return this.errorText;
    }
  }
}/*@lineinfo:generated-code*/