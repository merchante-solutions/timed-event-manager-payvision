/*@lineinfo:filename=ClientUserAdmin*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/enroll/ClientUserAdmin.sqlj $

  Description:

  NewPasswordBean

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2008-11-12 09:52:51 -0800 (Wed, 12 Nov 2008) $
  Version            : $Revision: 15520 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.enroll;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.constants.MesEmails;
import com.mes.constants.MesHierarchy;
import com.mes.constants.MesUsers;
import com.mes.crypt.MD5;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.CityStateZipField;
import com.mes.forms.Condition;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.HtmlTable;
import com.mes.forms.NumberField;
import com.mes.forms.PasswordField;
import com.mes.forms.PhoneField;
import com.mes.forms.Validation;
import com.mes.net.MailMessage;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import com.mes.user.TCUserManager;
import com.mes.user.UserBean;
import com.mes.user.UserType;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class ClientUserAdmin extends FieldBean
{
  public static final int   ATTR_USER_TYPE          = 1;
  public static final int   ATTR_USER_GROUP         = 2;
  
  public static final long  COMDATA_HIERARCHY_NODE  = 101000000L;
  
  protected DefaultContext  myCtx         = null;
  
  public    String          loginName     = "";
  public    boolean         editing       = false;
  public    boolean         success       = false;
  public    boolean         isComdata     = false;
  public    HtmlTable       userDetails   = null;
  
  static Logger log = Logger.getLogger(ClientUserAdmin.class);
  
  public boolean connect()
  {
    boolean result = super.connect();
    
    myCtx = Ctx;

    return result;
  }

  public void cleanUp()
  {
    super.cleanUp();
  }

  public void setProperties(HttpServletRequest request, UserBean _user)
  {
    try
    {
      user = _user;

      // get the requested login name from the request if present
      loginName = HttpHelper.getString(request, "loginName");
      
      if( ! loginName.equals("") || HttpHelper.getBoolean(request, "editing", false))
      {
        // login name was passed in which means edit mode
        editing = true;
      }

      // register bean with MesSystem
      register(user.getLoginName(), request);

      // determine if this is a comdata user which means special user admin      
      isComdata = (COMDATA_HIERARCHY_NODE == user.getHierarchyNode());

      // add form fields to vector
      addFields();
      
      // set hierarchyNode field if passed in request
      setData("hierarchyNode", HttpHelper.getString(request, "hierarchynode"));

      // load data from request into fields (only if form has been submitted)
      if(!HttpHelper.getString(request, "Submit", "").equals(""))
      {      
        setFields(request);
        
        // set user type to current user type if not found in existing list
        UserTypeDropDownField utField = (UserTypeDropDownField)getField("userType");
        
        if( ! utField.containsType(Integer.parseInt(getData("userType"))) )
        {
          utField.setUserType(Integer.parseInt(getData("userType")));
        }
        
        if(isComdata)
        {
          setData("hierarchyNode", Long.toString(COMDATA_HIERARCHY_NODE));
        }
      }
      else
      {
        // if editing then get data from database
        if(editing)
        {
          loadData();
        }
      }
      
      if(isComdata)
      {
        userDetails = ComdataUserDetails.getUserDetails(fields.getData("user"), Ctx);
      }
      
      if(!editing || canMaintainUser())
      {
        // check for submission
        if(getData("Submit").equals("Submit"))
        {
          if(isValid())
          {
            submitData();
            loadData();
            success = true;
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
  }

  private void loadData()
  {
    ResultSetIterator   it            = null;
    ResultSet           rs            = null;
    int                 loadUserType  = 0;
    long                userId        = 0L;
    
    try
    {
      // get login name, name, hierarchy node, email address and user type
      /*@lineinfo:generated-code*//*@lineinfo:167^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  u.name                    name,
//                  u.hierarchy_node          hierarchy_node,
//                  u.email                   email,
//                  u.type_id                 type_id,
//                  u.user_id                 user_id,
//                  u.address1                address1,
//                  u.address2                address2,
//                  ( u.city || ',' || ' ' ||
//                    u.state || '  ' ||
//                    u.zip )                 user_csz,
//                  u.email                   email,
//                  u.phone                   phone,
//                  u.fax                     fax,
//                  u.mobile                  mobile,
//                  u.pager                   pager,
//                  u.pager_pin               pager_pin,
//                  lower(u.enabled)          enabled
//          from    users   u
//          where   login_name = :loginName
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  u.name                    name,\n                u.hierarchy_node          hierarchy_node,\n                u.email                   email,\n                u.type_id                 type_id,\n                u.user_id                 user_id,\n                u.address1                address1,\n                u.address2                address2,\n                ( u.city || ',' || ' ' ||\n                  u.state || '  ' ||\n                  u.zip )                 user_csz,\n                u.email                   email,\n                u.phone                   phone,\n                u.fax                     fax,\n                u.mobile                  mobile,\n                u.pager                   pager,\n                u.pager_pin               pager_pin,\n                lower(u.enabled)          enabled\n        from    users   u\n        where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.enroll.ClientUserAdmin",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.enroll.ClientUserAdmin",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:188^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        // overload necessary fields
        setData("user", loginName);
        setData("name", rs.getString("name"));
        setData("hierarchyNode", rs.getString("hierarchy_node"));
        setData("email", rs.getString("email"));
        userId = rs.getLong("user_id");
        loadUserType = rs.getInt("type_id");
        setData("enabled", rs.getString("enabled"));
        
        // set the extended fields
        setData( "address1", rs.getString("address1") );
        setData( "address2", rs.getString("address2") );
        setData( "userCsz", rs.getString("user_csz") );
        setData( "phone", rs.getString("phone") );
        setData( "fax", rs.getString("fax") );
        setData( "mobile", rs.getString("mobile") );
        setData( "pager", rs.getString("pager") );
        setData( "pagerPin", rs.getString("pager_pin") );
      }
      
      // set b2f enabled flag based on TCUserManager status
      if( TCUserManager.shouldUseB2f( loginName ) == true )
      {
        setData("b2fEnabled", "y");
      }
      else
      {
        setData("b2fEnabled", "n");
      }
      
      // set user type in drop down
      UserTypeDropDownField utField = (UserTypeDropDownField)getField("userType");
      
      if( !utField.containsType(loadUserType) )
      {
        utField.setUserType(loadUserType);
      }
      setData("userType", Integer.toString(loadUserType));
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  public void submitData()
  {
    MD5   md5     = new MD5();
    int   typeId  = getInt("userType",-1);
    
    try
    {
      md5.Update(getData("userPassword"));
      String passwordEnc = md5.asHex();
      int updateB2f = (user.hasRight(MesUsers.RIGHT_MANAGE_B2F) ? 1 : 0);
      
      if(editing)
      {
        // if emtpy password than don't touch the password column
        if(getData("userPassword").equals(""))
        {
          /*@lineinfo:generated-code*//*@lineinfo:263^11*/

//  ************************************************************
//  #sql [Ctx] { update  users
//              set     name            = :getData("name"),
//                      hierarchy_node  = :getData("hierarchyNode"),
//                      email           = :getData("email"),
//                      type_id         = decode(:typeId,-1,type_id,:typeId),
//                      enabled         = :getData("enabled").toUpperCase(),
//                      use_b2f         = decode( :updateB2f,
//                                                1, :getData("b2fEnabled").toUpperCase(),
//                                                use_b2f )
//              where   login_name      = :loginName
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1861 = getData("name");
 String __sJT_1862 = getData("hierarchyNode");
 String __sJT_1863 = getData("email");
 String __sJT_1864 = getData("enabled").toUpperCase();
 String __sJT_1865 = getData("b2fEnabled").toUpperCase();
   String theSqlTS = "update  users\n            set     name            =  :1 ,\n                    hierarchy_node  =  :2 ,\n                    email           =  :3 ,\n                    type_id         = decode( :4 ,-1,type_id, :5 ),\n                    enabled         =  :6 ,\n                    use_b2f         = decode(  :7 ,\n                                              1,  :8 ,\n                                              use_b2f )\n            where   login_name      =  :9";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.enroll.ClientUserAdmin",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1861);
   __sJT_st.setString(2,__sJT_1862);
   __sJT_st.setString(3,__sJT_1863);
   __sJT_st.setInt(4,typeId);
   __sJT_st.setInt(5,typeId);
   __sJT_st.setString(6,__sJT_1864);
   __sJT_st.setInt(7,updateB2f);
   __sJT_st.setString(8,__sJT_1865);
   __sJT_st.setString(9,loginName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:275^11*/
        }
        else
        {
          // include new encrypted password
          /*@lineinfo:generated-code*//*@lineinfo:280^11*/

//  ************************************************************
//  #sql [Ctx] { update  users
//              set     name            = :getData("name"),
//                      password_enc    = :passwordEnc,
//                      hierarchy_node  = :getData("hierarchyNode"),
//                      email           = :getData("email"),
//                      type_id         = decode(:typeId,-1,type_id,:typeId),
//                      enabled         = :getData("enabled").toUpperCase(),
//                      use_b2f         = decode( :updateB2f,
//                                                1, :getData("b2fEnabled").toUpperCase(),
//                                                use_b2f )
//              where   login_name      = :loginName
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1866 = getData("name");
 String __sJT_1867 = getData("hierarchyNode");
 String __sJT_1868 = getData("email");
 String __sJT_1869 = getData("enabled").toUpperCase();
 String __sJT_1870 = getData("b2fEnabled").toUpperCase();
   String theSqlTS = "update  users\n            set     name            =  :1 ,\n                    password_enc    =  :2 ,\n                    hierarchy_node  =  :3 ,\n                    email           =  :4 ,\n                    type_id         = decode( :5 ,-1,type_id, :6 ),\n                    enabled         =  :7 ,\n                    use_b2f         = decode(  :8 ,\n                                              1,  :9 ,\n                                              use_b2f )\n            where   login_name      =  :10";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.enroll.ClientUserAdmin",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1866);
   __sJT_st.setString(2,passwordEnc);
   __sJT_st.setString(3,__sJT_1867);
   __sJT_st.setString(4,__sJT_1868);
   __sJT_st.setInt(5,typeId);
   __sJT_st.setInt(6,typeId);
   __sJT_st.setString(7,__sJT_1869);
   __sJT_st.setInt(8,updateB2f);
   __sJT_st.setString(9,__sJT_1870);
   __sJT_st.setString(10,loginName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:293^11*/
          
          // reset user on TriCipher appliance
          TCUserManager.resetUser(loginName, getData("userPassword"));
        }
      }
      else
      {
        // insert new user
        // make sure loginName gets set
        loginName = fields.getData("user");
        
        long  userId;
        
        /*@lineinfo:generated-code*//*@lineinfo:307^9*/

//  ************************************************************
//  #sql [Ctx] { select  users_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  users_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.enroll.ClientUserAdmin",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   userId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:312^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:314^9*/

//  ************************************************************
//  #sql [Ctx] { insert into users
//            (
//              user_id,
//              login_name,
//              name,
//              password_enc,
//              hierarchy_node,
//              email,
//              type_id,
//              enabled,
//              use_b2f,
//              create_user
//            )
//            values
//            (
//              :userId,
//              :getData("user"),
//              :getData("name"),
//              :passwordEnc,
//              :getData("hierarchyNode"),
//              :getData("email"),
//              :getData("userType"),
//              :getData("enabled").toUpperCase(),
//              decode( :updateB2f,
//                      1, :getData("b2fEnabled").toUpperCase(),
//                      null ),
//              :user.getLoginName()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1871 = getData("user");
 String __sJT_1872 = getData("name");
 String __sJT_1873 = getData("hierarchyNode");
 String __sJT_1874 = getData("email");
 String __sJT_1875 = getData("userType");
 String __sJT_1876 = getData("enabled").toUpperCase();
 String __sJT_1877 = getData("b2fEnabled").toUpperCase();
 String __sJT_1878 = user.getLoginName();
   String theSqlTS = "insert into users\n          (\n            user_id,\n            login_name,\n            name,\n            password_enc,\n            hierarchy_node,\n            email,\n            type_id,\n            enabled,\n            use_b2f,\n            create_user\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n            decode(  :9 ,\n                    1,  :10 ,\n                    null ),\n             :11 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.enroll.ClientUserAdmin",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userId);
   __sJT_st.setString(2,__sJT_1871);
   __sJT_st.setString(3,__sJT_1872);
   __sJT_st.setString(4,passwordEnc);
   __sJT_st.setString(5,__sJT_1873);
   __sJT_st.setString(6,__sJT_1874);
   __sJT_st.setString(7,__sJT_1875);
   __sJT_st.setString(8,__sJT_1876);
   __sJT_st.setInt(9,updateB2f);
   __sJT_st.setString(10,__sJT_1877);
   __sJT_st.setString(11,__sJT_1878);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:344^9*/
      }
      
      commit();
      
      // send email if necessary
      if(getData("sendEmail").equals("y"))
      {
        MailMessage   msg   = new MailMessage();
        StringBuffer  body  = new StringBuffer("");
        
        body.append("Welcome and thank you for choosing BB&T Merchant Connection. ");
        body.append("You may log onto Merchant Connection anytime by going to ");
        body.append("http://www.bbandt.com/merchantservices/merchantconnection.html.\n");
        body.append("Your user ID is: ");
        body.append(getData("user"));
        body.append(".\n");
        body.append("Your password is your nine digit Tax ID/Social Security Number.  ");
        body.append("It is strongly recommended that you change your password once you have signed onto Merchant Connection via the Change Profile option.\n\n");
        body.append("Please record your password in a secure location.  ");
        body.append("Log-on information is case sensitive.\n\n");
        body.append("Should you need any assistance, please call our Merchant Services Call Center at 1-877-MRCHBBT (672-4228).  ");
        body.append("Our office hours are Monday - Friday, 8:30 a.m. - 9:00 p.m. EST.");
        
        msg.setAddresses(MesEmails.MSG_ADDRS_CLIENT_USER_EMAIL);
        msg.addTo(getData("email"));
        msg.setSubject("Welcome to BB&T Merchant Connection");
        msg.setText(body.toString());
        msg.send();
      }
      
      // now reset password field so that it doesn't get loaded back into the form
      setData("userPassword", "");
      setData("passwordConfirm", "");
    }
    catch(Exception e)
    {
      logEntry("submitData()", e.toString());
    }
  }

  private void addFields()
  {
    // set up form fields
    //UserGroups            fUserGroups       = new UserGroups("userGroups", user.getOrgBankId());
    Field                 fLoginName        = new Field("user", 75, 20, false);
    Field                 fHiddenLoginName  = new HiddenField("user");
    Field                 fUserName         = new Field("name", 100, 30, false);
    Field                 fHierarchyNode    = null;
    
    if(isComdata)
    {
      fHierarchyNode    = new HiddenField("hierarchyNode", Long.toString(COMDATA_HIERARCHY_NODE));
    }
    else
    {
      fHierarchyNode    = new NumberField("hierarchyNode", 16, 16, false, 0);
      fHierarchyNode.addValidation(new HierarchyNodeAllowedValidation(user.getHierarchyNode()));
    }
    
    EmailField            fEmail            = new EmailField("email", 100, 30, true);
    UserTypeDropDownField fUserType         = new UserTypeDropDownField("userType", user.getHierarchyNode(), user.getUserType());
    CheckboxField         fEnabled          = new CheckboxField("enabled", "Enabled", true);
    CheckboxField         fB2fEnabled       = new CheckboxField("b2fEnabled", "B2F Enabled", true);
    PasswordField         fPassword         = null;
    PasswordField         fConfirmPassword  = null;
    CheckboxField         fSendEmail        = null;
    
    if(editing)
    {
      // password not required if editing existing user
      fPassword         = new PasswordField("userPassword", 32, 20, true);
      fConfirmPassword  = new PasswordField("passwordConfirm", 32, 20, true);
    }
    else
    {
      // password required if creating a new user
      fPassword         = new PasswordField("userPassword", 32, 20, false);
      fConfirmPassword  = new PasswordField("passwordConfirm", 32, 20, false);
      fSendEmail        = new CheckboxField("sendEmail", "Send Email to New User", false);
      
      // add validation that will require the email text box to have data
      // if the sendEmail checkbox is checked
      Condition   cSendEmail = new FieldValueCondition(fSendEmail, "y");
      fEmail.addValidation(new ConditionalRequiredValidation(cSendEmail));
    }
    
    // add validations
    fConfirmPassword.addValidation(new FieldEqualsFieldValidation(fPassword, "Password"));
    fPassword.addValidation(new MesPasswordValidation(fPassword.getNullAllowed()));
    fUserType.addValidation(new ValidUserTypeValidation(user.getOrgBankId()));
    
    if(!editing)
    {
      fLoginName.addValidation(new DuplicateUserValidation());
      fLoginName.addValidation(new LoginNameValidation());
    }

    // add fields to vector
    if(editing)
    {
      fields.add(fHiddenLoginName);
    }
    else
    {
      fields.add(fLoginName);
      fields.add(fSendEmail);
    }
    

    fields.add(fUserName);
    fields.add(fPassword);
    fields.add(fConfirmPassword);
    fields.add(fHierarchyNode);
    fields.add(fEmail);
    fields.add(fUserType);
    fields.add(fEnabled);
    fields.add(fB2fEnabled);
    
    // set up extended form fields - no validate, read only
    fields.add(new Field("address1", "Addr Line 1: ", 40, 40, true));
    fields.add(new Field("address2", "Addr Line 2:", 40, 40, true));
    fields.add(new CityStateZipField("userCsz", "City, State, Zip", 30, true));
    fields.add(new PhoneField("phone", "Phone", true));
    fields.add(new PhoneField("fax", "Fax", true));
    fields.add(new PhoneField("mobile", "Mobile", true));
    fields.add(new PhoneField("pager", "Pager", true));
    fields.add(new Field("pagerPin", "Pager PIN", 10, 10, true));
    
    fields.add(new ButtonField("Submit"));

    fields.setGroupHtmlExtra("class=\"formFields\"");
    fields.setHtmlExtra("class=\"formFields\"");
  }
  
  public boolean canMaintainUser()
  {
    boolean   result    = false;
    
    // see if the requested user's hierarchy node is accessible to the maintainer
    result = hierarchyNodeMaintainable(user.getHierarchyNode(), getData("hierarchyNode"));
    
    return result;
  }
  
  public String getUserTypeDesc( int userType )
  {
    String      retVal    = "";
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:496^7*/

//  ************************************************************
//  #sql [Ctx] { select  ut.name 
//          from    user_types    ut
//          where   ut.type_id = :userType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ut.name  \n        from    user_types    ut\n        where   ut.type_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.enroll.ClientUserAdmin",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,userType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:501^7*/
    }
    catch( Exception e )
    {
      logEntry("getUserTypeDesc(" + userType + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected boolean hierarchyNodeMaintainable(long userNode, String desiredNode)
  {
    boolean   result = false;
    
    try
    {
      if(desiredNode.equals(Long.toString(userNode)))
      {
        // a user can always maintain other users at his node
        result = true;
      }
      else
      {
        // look for relationships in group_merchant or t_hierarchy
        int   relationCount   = 0;
        long  userOrg         = 0;
      
        // get org id for user
        /*@lineinfo:generated-code*//*@lineinfo:532^9*/

//  ************************************************************
//  #sql [Ctx] { select  org_num
//            
//            from    organization
//            where   org_group = :userNode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  org_num\n           \n          from    organization\n          where   org_group =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.enroll.ClientUserAdmin",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,userNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   userOrg = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:538^9*/
        
        // look in group_merchant
        /*@lineinfo:generated-code*//*@lineinfo:541^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(org_num)
//            
//            from    group_merchant
//            where   org_num = :userOrg and
//                    merchant_number = :desiredNode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(org_num)\n           \n          from    group_merchant\n          where   org_num =  :1  and\n                  merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.enroll.ClientUserAdmin",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,userOrg);
   __sJT_st.setString(2,desiredNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   relationCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:548^9*/
        
        if(relationCount > 0)
        {
          // desired node is a merchant that user has access to
          result = true;
        }
        else
        {
          // look in t_hierarchy
          /*@lineinfo:generated-code*//*@lineinfo:558^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(ancestor)
//              
//              from    t_hierarchy
//              where   hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                      ancestor = :userNode and
//                      descendent = :desiredNode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ancestor)\n             \n            from    t_hierarchy\n            where   hier_type =  :1  and\n                    ancestor =  :2  and\n                    descendent =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.enroll.ClientUserAdmin",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(2,userNode);
   __sJT_st.setString(3,desiredNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   relationCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:566^11*/
          
          if(relationCount > 0)
          {
            result = true;
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("validate(" + userNode + ", " + desiredNode + ")", e.toString());
    }
    
    return result;
  }
  
  public boolean supportsExtendedData( )
  {
    boolean   retVal    = false;
    long      nodeId    = user.getHierarchyNode();
    
    if ( nodeId == COMDATA_HIERARCHY_NODE )
    {
      retVal = true;
    }      
    return( retVal );
  }

  public class UserTypeDropDownField extends DropDownField
  {
    public UserTypeDropDownField(String fname, long hierarchyNode, UserType userType)
    {
      super(fname, null, false);
      dropDown = new UserTypeTable(hierarchyNode, userType);
    }
    
    public boolean containsType ( int userType ) 
    {
      String val = this.dropDown.getFirstValue();
      
      while( val != null )
      {
        if ( Integer.parseInt(val) == userType )
        {
          break;
        }
        val = this.dropDown.getNextValue();
      }
      return( val != null );
    }
    
    public void setUserType( int userType )
    {
      // remove all elements except for --Select-- and current user type
      // this prevents accidentally changing a user type for accounts that
      // really shouldn't be changed
      this.dropDown.removeAllElements();
      
      this.dropDown.addElement("-1", "-- Select --");
      this.dropDown.addElement(String.valueOf(userType),getUserTypeDesc(userType));
    }
    
    public class UserTypeTable extends DropDownTable
    {
      public UserTypeTable(long hierarchyNode, UserType userType)
      {
        getData(hierarchyNode, userType);
      }

      private void getData(long hierarchyNode, UserType userType)
      {
        ResultSetIterator     it      = null;
        ResultSet             rs      = null;

        try
        {
          addElement("-1", "-- Select --");
          
          // see if user has restrictions
          int recCount = 0;
          /*@lineinfo:generated-code*//*@lineinfo:647^11*/

//  ************************************************************
//  #sql [myCtx] { select  count(user_type)
//              
//              from    user_client_attr_restrict
//              where   user_type = :userType.getUserTypeId()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = myCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1879 = userType.getUserTypeId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(user_type)\n             \n            from    user_client_attr_restrict\n            where   user_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.enroll.ClientUserAdmin",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1879);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:653^11*/
          
          if(recCount > 0)
          {
            // only show types that are allowed
            /*@lineinfo:generated-code*//*@lineinfo:658^13*/

//  ************************************************************
//  #sql [myCtx] it = { select  distinct uca.attr_value,
//                        ut.name
//                from    user_client_attr          uca,
//                        user_types                ut,
//                        user_client_attr_restrict ucar
//                where   uca.hierarchy_node = :hierarchyNode and
//                        uca.attr_type   = :ATTR_USER_TYPE and
//                        uca.attr_value  = ut.type_id and
//                        ut.type_id = ucar.allowed_type and
//                        ucar.user_type = :userType.getUserTypeId()
//                order by attr_value asc
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = myCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1880 = userType.getUserTypeId();
  try {
   String theSqlTS = "select  distinct uca.attr_value,\n                      ut.name\n              from    user_client_attr          uca,\n                      user_types                ut,\n                      user_client_attr_restrict ucar\n              where   uca.hierarchy_node =  :1  and\n                      uca.attr_type   =  :2  and\n                      uca.attr_value  = ut.type_id and\n                      ut.type_id = ucar.allowed_type and\n                      ucar.user_type =  :3 \n              order by attr_value asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.enroll.ClientUserAdmin",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   __sJT_st.setInt(2,ATTR_USER_TYPE);
   __sJT_st.setLong(3,__sJT_1880);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.enroll.ClientUserAdmin",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:671^13*/
          }
          else
          {
            /*@lineinfo:generated-code*//*@lineinfo:675^13*/

//  ************************************************************
//  #sql [myCtx] it = { select  distinct uca.attr_value,
//                        ut.name
//                from    user_client_attr    uca,
//                        user_types          ut
//                where   uca.hierarchy_node = :hierarchyNode and
//                        uca.attr_type   = :ATTR_USER_TYPE and
//                        uca.attr_value  = ut.type_id
//                order by attr_value asc
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = myCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct uca.attr_value,\n                      ut.name\n              from    user_client_attr    uca,\n                      user_types          ut\n              where   uca.hierarchy_node =  :1  and\n                      uca.attr_type   =  :2  and\n                      uca.attr_value  = ut.type_id\n              order by attr_value asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.enroll.ClientUserAdmin",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   __sJT_st.setInt(2,ATTR_USER_TYPE);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.enroll.ClientUserAdmin",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:685^13*/
          }
          
          rs = it.getResultSet();

          while(rs.next())
          {
            addElement(rs);
          }

          rs.close();
          it.close();
        }
        catch(Exception e)
        {
          logEntry("getData(" + hierarchyNode + ")", e.toString());
        }
        finally
        {
          try { rs.close(); } catch(Exception e) {}
          try { it.close(); } catch(Exception e) {}
        }
      }
    }
  }

  public class HierarchyNodeAllowedValidation
    implements Validation
  {
    private long      userNode      = 0L;
    
    public HierarchyNodeAllowedValidation(long userNode)
    {
      super();
      this.userNode = userNode;
    }
    
    public boolean validate(String hierarchyNode)
    {
      boolean   validated = false;
      
      validated = hierarchyNodeMaintainable(userNode, hierarchyNode);
      
      return validated;
    }
    
    public String getErrorText()
    {
      return "Invalid hierarchy node or merchant number";
    }
  }
  
  public class MesPasswordValidation
    implements Validation
  {
    String  errorText = "";
    boolean nullAllowed = false;
    
    public MesPasswordValidation(boolean nullAllowed)
    {
      super();
      this.nullAllowed = nullAllowed;
    }
    
    // passwords must be at least 6 characters.  May add more validation later
    public boolean validate(String password)
    {              
      boolean validated = true;
      
      try
      {
        // if field is optional and the password is blank then don't bother
        if(nullAllowed && (password == null || password.equals("")))
        {
          validated = true;
        }
        else
        {
          if(password == null || password.length() < 6)
          {
            validated = false;
            errorText = "Password length must be at least 6 characters";
          }
        }
      }
      catch(Exception e)
      {
        logEntry("validate(" + password + ")", e.toString());
      }
      
      return validated;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }
  
  public class DuplicateUserValidation
    implements Validation
  {
    public DuplicateUserValidation()
    {
    }
    
    public boolean validate(String loginName)
    {
      boolean validated = true;
      
      try
      {
        int userCount = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:799^9*/

//  ************************************************************
//  #sql [myCtx] { select  count(login_name)
//            
//            from    users
//            where   lower(login_name) = lower(:loginName)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = myCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(login_name)\n           \n          from    users\n          where   lower(login_name) = lower( :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.enroll.ClientUserAdmin",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   userCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:805^9*/
        
        // only valid if there are no existing users in the database
        validated = (userCount == 0);
      }
      catch(Exception e)
      {
        logEntry("validate(" + loginName + ")", e.toString());
      }
      
      return validated;
    }
    
    public String getErrorText()
    {
      return "This User ID already exists.  Please try a different User ID.";
    }
  }
  
  public class ValidUserTypeValidation
    implements Validation
  {
    public  int   bankNumber    = 0;
    
    public ValidUserTypeValidation(int bankNumber)
    {
      super();
      
      this.bankNumber = bankNumber;
    }
    
    public boolean validate(String userType)
    {
      boolean validated = true;
      
      try
      {
        // as long as they haven't selected the -- Select -- item (value = -1)
        // then the user type is valid because the generation of the drop-down
        // table ensures valid user types
        if( userType.equals("-1") )
        {
          validated = false;
        }
      }
      catch(Exception e)
      {
        logEntry("validate(" + userType + ")", e.toString());
      }
      
      return validated;
    }
    
    public String getErrorText()
    {
      return "Invalid user type";
    }
  }
  
  
  public class LoginNameValidation
    implements Validation
  {
    private String    errorText   = "";
    
    public LoginNameValidation()
    {
    }
    
    public boolean validate(String loginName)
    {
      boolean validated = true;
      
      try
      {
        if(loginName.indexOf(' ') != -1)
        {
          // login name must not contain spaces
          errorText = "User ID cannot contain spaces";
          validated = false;
        }
        
        // could do other invalid character checks here
      }
      catch(Exception e)
      {
        logEntry("validate(" + loginName + ")", e.toString());
      }
      
      return validated;
    }
    
    public String getErrorText()
    {
      return this.errorText;
    }
  }
}/*@lineinfo:generated-code*/