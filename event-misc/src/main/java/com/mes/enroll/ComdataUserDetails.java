/*@lineinfo:filename=ComdataUserDetails*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/enroll/ClientUserAdmin.sqlj $

  Description:

  NewPasswordBean

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 8/04/03 10:46a $
  Version            : $Revision: 11 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.enroll;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.HtmlTable;
import com.mes.forms.HtmlTableCell;
import com.mes.forms.HtmlTableRow;
import com.mes.forms.RadioButtonField;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class ComdataUserDetails extends FieldBean
{
  public    int             userId    = 0;
  public    String          loginName = "";
  public    String          userName  = "";
  public    String          enabled   = "";
  public    String          message   = "";
  public    boolean         error     = false;
  public    boolean         isMessage = true;

  protected static final String[][] codeTypeList = 
  {
    { "Location",   "LOC"   },
    { "Chain",      "CHAIN" },
    { "Corporate",  "CORP"  }
  };
  
  public ComdataUserDetails()
  {
  }
  
  public ComdataUserDetails(DefaultContext Ctx)
  {
    super(Ctx);
  }
  
  public void setProperties(HttpServletRequest request, UserBean _user)
  {
    try
    {
      connect();
      
      user = _user;
      
      register(user.getLoginName(), request);
      
      addFields();
      
      // get user data from request
      loadUserData(request);
      
      setFields(request);
      if(HttpHelper.getString(request, "Delete", "").equals("Delete"))
      {
        deleteCode();
        setMessage("Code Deleted", false);
      }
      else if(HttpHelper.getString(request, "Submit", "").equals("Submit"))
      {
        if(isValid())
        {
          submitCode();
          setMessage("Code Added", false);
        }
        else
        {
          StringBuffer msg = new StringBuffer("");
          msg.append("There were errors fulfilling your request.  Hover the mouse over the ");
          msg.append("<img src=\"/images/fixfield.gif\" width=\"21\" border=\"0\" align=\"absmiddle\" title=\"Hover on the errors below\" height=\"9\"> for more details.");
          setMessage(msg.toString(), true);
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void setMessage(String _message, boolean _error)
  {
    try
    {
      isMessage = true;
      
      message = _message;
      error = _error;
    }
    catch(Exception e)
    {
      logEntry("setMessage()", e.toString());
    }
  }
  
  private void loadUserData(HttpServletRequest request)
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      loginName = HttpHelper.getString(request, "loginName", "");
      
      /*@lineinfo:generated-code*//*@lineinfo:134^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  user_id,
//                  name,
//                  enabled
//          from    users
//          where   login_name = :loginName
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  user_id,\n                name,\n                enabled\n        from    users\n        where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.enroll.ComdataUserDetails",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.enroll.ComdataUserDetails",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:141^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        userId = rs.getInt("user_id");
        setData("userId", rs.getString("user_id"));
        userName = rs.getString("name");
        enabled = rs.getString("enabled");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      System.out.println("loadUserData(): " + e.toString());
      logEntry("loadUserData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void submitCode()
  {
    try
    {
      // add code
      /*@lineinfo:generated-code*//*@lineinfo:173^7*/

//  ************************************************************
//  #sql [Ctx] { insert into user_comdata
//          (
//            user_id,
//            code_type,
//            code
//          )
//          values
//          (
//            :fields.getData("userId"),
//            :fields.getData("codeType"),
//            :fields.getData("code")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1881 = fields.getData("userId");
 String __sJT_1882 = fields.getData("codeType");
 String __sJT_1883 = fields.getData("code");
   String theSqlTS = "insert into user_comdata\n        (\n          user_id,\n          code_type,\n          code\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.enroll.ComdataUserDetails",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1881);
   __sJT_st.setString(2,__sJT_1882);
   __sJT_st.setString(3,__sJT_1883);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:187^7*/
    }
    catch(Exception e)
    {
      System.out.println("submitCode(): " + e.toString());
      logEntry("submitCode()", e.toString());
    }
  }
  
  private void deleteCode()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:200^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    user_comdata
//          where   id = :fields.getData("delId")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1884 = fields.getData("delId");
  try {
   String theSqlTS = "delete\n        from    user_comdata\n        where   id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.enroll.ComdataUserDetails",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1884);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:205^7*/
    }
    catch(Exception e)
    {
      System.out.println("deleteCode(): " + e.toString());
      logEntry("deleteCode()", e.toString());
    }
  }
  
  private HtmlTable _getUserDetails(String loginName)
  {
    return getUserDetailsFull(loginName, false);
  }
  
  public HtmlTable getUserDetailsFull(String loginName, boolean extra)
  {
    HtmlTable           userDetails = null;
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    try
    {
      connect();
      
      userDetails = new HtmlTable();
      userDetails.setProperty("border", "0");
      userDetails.setProperty("align", "center");
      userDetails.setProperty("width", "300");
      userDetails.setProperty("cellspacing", "1");
      userDetails.setProperty("cellpadding", "4");
      
      HtmlTableRow titleRow = userDetails.add(new HtmlTableRow());
      HtmlTableCell titleCell = titleRow.add(new HtmlTableCell());
      titleCell.setProperty("colspan", "2");
      titleCell.setProperty("class", "tableColumnHead");
      titleCell.add("Existing Loc/Chain/Corp Codes");
      
      HtmlTableRow tr = userDetails.add(new HtmlTableRow());
      HtmlTableCell tc1 = tr.add(new HtmlTableCell());
      tc1.setProperty("class", "tableDataStrong");
      tc1.setProperty("align", "center");
      tc1.add("Type");
      HtmlTableCell tc2 = tr.add(new HtmlTableCell());
      tc2.setProperty("class", "tableDataStrong");
      tc2.setProperty("align", "center");
      tc2.add("Code");
      
      if(extra)
      {
        titleCell.setProperty("colspan", "3");
        HtmlTableCell tc3 = tr.add(new HtmlTableCell());
        tc3.setProperty("class", "tableDataStrong");
        tc3.setProperty("align", "center");
      }
      
      // load user code assignments
      /*@lineinfo:generated-code*//*@lineinfo:260^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  uc.code_type    code_type,
//                  uc.code         code,
//                  uc.id           id
//          from    user_comdata    uc,
//                  users           u
//          where   u.login_name = :loginName and
//                  u.user_id = uc.user_id
//          order by code_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  uc.code_type    code_type,\n                uc.code         code,\n                uc.id           id\n        from    user_comdata    uc,\n                users           u\n        where   u.login_name =  :1  and\n                u.user_id = uc.user_id\n        order by code_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.enroll.ComdataUserDetails",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.enroll.ComdataUserDetails",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:270^7*/
      
      rs = it.getResultSet();
      
      int codesFound = 0;
      while(rs.next())
      {
        ++codesFound;
        // add to html table 
        HtmlTableRow row = userDetails.add(new HtmlTableRow());
        
        // add cells
        HtmlTableCell c1 = row.add(new HtmlTableCell());
        HtmlTableCell c2 = row.add(new HtmlTableCell());
        
        c1.setProperty("class", "tableData");
        c1.setProperty("align", "center");
        c2.setProperty("class", "tableData");
        c2.setProperty("align", "center");
        c1.add(rs.getString("code_type"));
        c2.add(rs.getString("code"));
        
        if(extra)
        {
          HtmlTableCell c3 = row.add(new HtmlTableCell());
          c3.setProperty("class", "tableData");
          c3.setProperty("align", "center");
          StringBuffer delButton = new StringBuffer("");
          delButton.append("<form method=\"post\"> ");
          delButton.append("  <input type=\"hidden\" name=\"delId\" value=\"");
          delButton.append(rs.getInt("id"));
          delButton.append("\"> ");
          delButton.append("  <input type=\"submit\" name=\"Delete\" value=\"Delete\" class=\"tableData\"> ");
          delButton.append("</form>");
          c3.add(delButton.toString());
        }
      }
      
      rs.close();
      it.close();
      
      // footer row
      HtmlTableRow  fr = userDetails.add(new HtmlTableRow());
      HtmlTableCell fc = fr.add(new HtmlTableCell());
      fc.setProperty("class", "tableDataStrong");
      fc.setProperty("colspan", "2");
      fc.setProperty("align", "center");
      fc.add(Integer.toString(codesFound) + " codes found");
      if(extra)
      {
        fc.setProperty("colspan", "3");
      }
    }
    catch(Exception e)
    {
      System.out.println("getUserDetailsFull(" + loginName + ") " + e.toString());
      logEntry("getUserDetailsFull(" + loginName + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return userDetails;
  }
  
  public static HtmlTable getUserDetails(String loginName, DefaultContext Ctx)
  {
    HtmlTable result = null;
    try
    {
      ComdataUserDetails cud = new ComdataUserDetails(Ctx);
      
      result = cud._getUserDetails(loginName);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.enroll.ComdataUserDetais.getUserDetails()", e.toString());
    }
    return result;
  }
  
  private void addFields()
  {
    try
    {
      // fields for universal work
      fields.add(new HiddenField("userId"));
      
      // fields needed only for submitting
      fields.add(new Field("code", "Location/Chain/Corp Code", 5, 5, false));
      fields.add(new RadioButtonField("codeType", codeTypeList, -1, false, "Required"));
      fields.add(new ButtonField("Submit"));
      
      // fields needed only for deleting
      fields.add(new HiddenField("delId"));
      fields.setGroupHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("addFields()", e.toString());
    }
  }
}/*@lineinfo:generated-code*/