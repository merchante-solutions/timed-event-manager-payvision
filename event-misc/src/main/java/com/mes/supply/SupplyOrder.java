/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/supply/SupplyOrder.sqlj $

  Description:

    SupplyOrder

    Represents a single order - one per instance of bean

  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 8/04/04 3:18p $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.supply;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
// log4j
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;


public class SupplyOrder
{

  // create class log category
  static Logger log = Logger.getLogger(SupplyOrder.class);

  protected ArrayList supplyItems;
  public long id;
  public Date orderDate;
  public boolean isRush;
  public double itemCostSum;
  public double itemShipSum;
  public String processSeq;
  public String loadFileName;
  public double baseShipRate;

  private MerchantSupplyConfig merchant;

  public int status;
  public boolean isTotalled = false;

  //leave room for other states?
  public static final int OUTSTANDING = 1;
  public static final int COMPLETED = 2;
  public static final int PAID = 3;
  public static final int CANCELLED = 9;

  //as of R.oReilly 12/17/2004
  public double SHIP_COST_BASE;
  public double SHIP_COST_RUSH_BASE;

  {
    try
    {
       SHIP_COST_BASE = MesDefaults.getDouble(MesDefaults.DK_ACR_SHIP_COST_BASE);
       SHIP_COST_RUSH_BASE  = MesDefaults.getDouble(MesDefaults.DK_ACR_SHIP_COST_RUSH_BASE);
     }
     catch(Exception e)
     {
       SHIP_COST_BASE = 4.00d;
       SHIP_COST_RUSH_BASE = 8.00d;
     }
  }

  public SupplyOrder()
  {
    initOrder("",false);
  }

  public SupplyOrder(String merchNum)
  {
    initOrder(merchNum, false);
  }

  public SupplyOrder(String merchNum, boolean isRush)
  {

    initOrder(merchNum, isRush);
  }

  protected void initOrder(String merchNum, boolean isRush)
  {
    if(null==merchNum || merchNum.equals("") || merchNum.length()==0)
    {
      merchant = new MerchantSupplyConfig();
    }
    else
    {
      merchant = new MerchantSupplyConfig(merchNum);
      //log.debug(merchant.toString());
    }

    supplyItems = new ArrayList();
    itemCostSum = 0.00;
    baseShipRate = isRush ? SHIP_COST_RUSH_BASE : SHIP_COST_BASE;
    //itemShipSum= baseShipRate;
    itemShipSum= 0.00;
    status = OUTSTANDING;
    processSeq="";
    loadFileName="";
    this.isRush = isRush;
    Calendar cal = Calendar.getInstance();
    orderDate = cal.getTime();
  }

  public void reset()
  {
    log.debug("Starting reset()...");
    if( supplyItems != null && supplyItems.size()>0 )
    {
      supplyItems.clear();
    }
    itemCostSum   = 0.00;
    itemShipSum   = 0.00;
    status        = OUTSTANDING;
    processSeq    = "";
    loadFileName  = "";
    clearOrderAddress();
    this.isRush   = false;
    log.debug("reset() DONE");
  }

  public void add(SupplyDescriptor sd, int quantity){
    supplyItems.add(new SupplyOrderItem(sd, merchant.getMarkUp(), quantity, isRush));
  }

  public void add(SupplyOrderItem soi){
    supplyItems.add(soi);
  }

  public Iterator getItemIterator(){
    return supplyItems.iterator();
  }

  public boolean hasItems(){
    return supplyItems.size()>0;
  }

  public double calculateTax()
  {
    if(!isTotalled){
      calculateSumCosts();
    }
    return MesMath.round(itemCostSum*merchant.getTaxRate(),2);
  }

  public double getTotalShipHandleCost(){
    if(!isTotalled){
      calculateSumCosts();
    }
    return itemShipSum + merchant.getHandleFee();
  }

  public double getSubTotalCost(){
    if(!isTotalled){
      calculateSumCosts();
    }
    return itemCostSum;
  }

  public double getTotalCost(){
    if(!isTotalled){
      calculateSumCosts();
    }
    return calculateTax() + getTotalShipHandleCost() + itemCostSum;
  }

  public double getShipCost(){
    if(!isTotalled){
      calculateSumCosts();
    }
    return itemShipSum;
  }


  /**
   * calculateSumCosts
   * cycles through the entire itemsList and adds all item costs
   * as of 3/8/05 only uses the sum of the two most expensive
   * shipCosts for items and only uses a one-time charge per item
   */
  public void calculateSumCosts()
  {
    //loop through list and add item costs, ship costs
    if(supplyItems.size()==0 || getMarkUp()==0)
    {
      itemCostSum=0.0;
      itemShipSum=0.0;
    }
    else
    {
      SupplyOrderItem soi;
      for(int i=0;i<supplyItems.size();i++)
      {
        soi = (SupplyOrderItem)supplyItems.get(i);
        itemCostSum += soi.getTotalPrice();
        //itemShipSum += soi.getTotalShipPrice();
      }//end for

      itemShipSum = calculateShippingCost(supplyItems);
    }
    isTotalled=true;
  }

  /**
   * calculateShippingCost
   * only charges once even if quantity is greater than 1
   * if item list is 2 or great, uses the sum of the most
   * expensive items as shipCost
   */
  private double calculateShippingCost(ArrayList items)
  {

    double shipCost  = 0.0d;
    SupplyOrderItem soi1, soi2;
    boolean calculated = false;

    switch (items.size())
    {
      case 1:
        //just get the only item ship cost,
        //ignore quantity
        soi1 = (SupplyOrderItem)items.get(0);
        shipCost = soi1.shipCost;
        calculated = true;
        break;
      case 2:
        //leave it alone
        break;
      default:
        //sort the SupplyOrderItems (by shipCost desc)
        try
        {
          Collections.sort(items);
        }
        catch (Exception e)
        {}
        break;
    }

    if(!calculated)
    {
      soi1 = (SupplyOrderItem)items.get(0);
      soi2 = (SupplyOrderItem)items.get(1);
      shipCost = soi1.shipCost + soi2.shipCost;
      calculated = true;
    }

    return shipCost;

  }

  public String renderAddressHtml(){

    StringBuffer sb = new StringBuffer(256);
    sb.append("<table border=0 cellpadding=0 cellspacing=0><tr><td class=\"formFields\"><b>Shipping Address:</b></td></tr>");
    sb.append("<tr><td class=\"formFields\">").append(getShipName()).append("</td></tr>");
    sb.append("<tr><td class=\"formFields\">").append(getShipLine1()).append("</td></tr>");
    if(getShipLine2().length()>0){
      sb.append("<tr><td class=\"formFields\">").append(getShipLine2()).append("</td></tr>");
    }
    sb.append("<tr><td class=\"formFields\">").append(getShipCity()).append(", ");
    sb.append(getShipState()).append(" ").append(getShipZip()).append("</td></tr>");
    sb.append("</td></tr></table>");
    return sb.toString();
  }

  public String renderItemListHtml(){
    StringBuffer sb = new StringBuffer();
    sb.append("<table><tr><td class=\"tableDataPlain\">");
    if(supplyItems==null||supplyItems.size()==0){
      sb.append("details unavailable");
    }else{
      SupplyOrderItem soi;
      //sb.append("<ul>");
      Iterator it = getItemIterator();
      while(it.hasNext()){
        soi = (SupplyOrderItem)it.next();
        sb.append("<li>");
        sb.append(soi.description);
        sb.append(" (").append(soi.quantity).append("): ").append(NumberFormatter.getDoubleString(soi.getTotalPrice(),NumberFormatter.WEB_CURRENCY_FORMAT));
        sb.append("</li>");
      }
      //sb.append("</ul>");
    }
    sb.append("</td></tr></table>");
    return sb.toString();
  }


//convenience mutators
  public void setTaxRate()
  {
    merchant.setTaxRate();
  }

  public void setTaxRate(double amount)
  {
    merchant.setTaxRate(amount);
  }

  public void setMerchantNum (String string){
    merchant.setMerchantNum(string);
  }

  public void setMarkUp (double amount){
    merchant.setMarkUp(amount);
  }

  public void setHandleFee (double amount){
    merchant.setHandleFee(amount);
  }

  public void setShipName (String string){
    merchant.setShipName(string);
  }

  public void setShipLine1 (String string){
    merchant.setShipLine1(string);
  }

  public void setShipLine2 (String string){
    merchant.setShipLine2(string);
  }

  public void setShipCity (String string){
    merchant.setShipCity(string);
  }

  public void setShipState (String string){
    merchant.setShipState(string);
  }

  public void setShipZip (String string){
    merchant.setShipZip(string);
  }

  public void clearOrderAddress()
  {
    merchant.clearAddress();
  }

  public void setRush(boolean isRush)
  {
    this.isRush = isRush;
    if(isRush)
    {
      baseShipRate = SHIP_COST_RUSH_BASE;
    }
    else
    {
      baseShipRate = SHIP_COST_BASE;
    }
  }
      //convenience accessors
  public String getMerchantNum ()
  {
    return merchant.getMerchantNum();
  }

  public boolean isClub()
  {
    return merchant.isClub();
  }

  //this might need to be moved to MerchantSupplyConfig,
  //as it may be merchant-dependent at some point
  public double getBaseShipRate()
  {
    return baseShipRate;
  }

  public double getTaxRate (){
    return merchant.getTaxRate();
  }

  public double getMarkUp (){
    return merchant.getMarkUp();
  }

  public double getHandleFee (){
    return merchant.getHandleFee();
  }

  public String getShipName (){
    return merchant.getShipName();
  }

  public String getShipLine1 (){
    return merchant.getShipLine1();
  }

  public String getShipLine2 (){
    return merchant.getShipLine2();
  }

  public String getShipCity (){
    return merchant.getShipCity();
  }

  public String getShipState (){
    return merchant.getShipState();
  }

  public String getShipZip (){
    return merchant.getShipZip();
  }

}//class SupplyOrder
