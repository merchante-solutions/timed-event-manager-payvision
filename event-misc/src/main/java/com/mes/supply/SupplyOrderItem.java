/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/supply/SupplyOrderItem.java $

  Description:

    SupplyOrderItem

    Represents a single order item within a supply order

  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 6/10/04 12:25p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.supply;
// log4j
import org.apache.log4j.Category;
import com.mes.config.MesDefaults;
import com.mes.support.MesMath;

public class SupplyOrderItem implements Comparable
{

  // create class log category
  static Category log = Category.getInstance(SupplyOrderItem.class.getName());

  public String       id; //same as seqNum for Supply Descriptor
  public int          quantity;
  protected double    price;
  public double       shipCost;
  public String       description;
  public String       groupName;

  protected double SHIP_COST_BASE;
  protected double SHIP_COST_RUSH_BASE;

 {
    try
    {
       SHIP_COST_BASE = MesDefaults.getDouble(MesDefaults.DK_ACR_SHIP_COST_BASE);
       SHIP_COST_RUSH_BASE  = MesDefaults.getDouble(MesDefaults.DK_ACR_SHIP_COST_RUSH_BASE);
     }
     catch(Exception e)
     {
       SHIP_COST_BASE = 4.00d;
       SHIP_COST_RUSH_BASE = 8.00d;
     }
  }

  public SupplyOrderItem(){
    id = "";
    quantity=0;
    price=0.00;
    shipCost=0.00;
    description="";
    groupName="";
  }

  public SupplyOrderItem(SupplyDescriptor sd, double markUp, int quantity, boolean isRush){

    id = sd.seqNum;
    this.quantity = quantity;
    price = MesMath.round(sd.getDisplayPrice(markUp),2);
    shipCost = buildShipCost(sd,isRush);
    description = buildDescription(sd);
    groupName = sd.groupTitle;

  }

  public void setPrice(double price){
    this.price=price;
  }

  public double getPrice(){
    return price;
  }

  public double getTotalPrice(){
    return price*quantity;
  }

  public double getTotalShipPrice(){
    return shipCost*quantity;
  }

  private String buildDescription(SupplyDescriptor sd){
    StringBuffer sb = new StringBuffer(32);
    sb.append(sd.partNum).append(": ");
    sb.append(sd.title).append(" (");
    sb.append(sd.unit).append("-");
    sb.append(sd.equivalent).append(")");
    return sb.toString();
  }

  /**
   * DEPR - - - 03/08/05
   * ship cost is determined by taking static amount as
   * base, and subtracting it from the DB amount...
   * this base cost is added back in during order calculation
   */
  private double buildShipCost(SupplyDescriptor sd, boolean isRush)
  {
    if(isRush)
    {
      //return sd.shipCostRush - SHIP_COST_RUSH_BASE;
      return sd.shipCostRush;
    }
    else
    {
      //return sd.shipCostStandard - SHIP_COST_BASE;
      return sd.shipCostStandard;
    }
  }

  /**
   * compareTo - added for 03/08/05 release
   * changing our current shipping rates to reflect two most expensive items
   * from SupplyOrderItem list (in SupplyOrder) thus need to rank items in desc
   * order of shipCost
   */
  public int compareTo(Object o)
  {
    SupplyOrderItem item       = (SupplyOrderItem)o;
    int       retVal  = 0;

    // we want to sort in reverse order (most expensive shipping first)
    if(this.shipCost < item.shipCost)
    {
      retVal = 1;
    }
    else if (this.shipCost == item.shipCost)
    {
      retVal = 0;
    }
    else
    {
      retVal = -1;
    }

    return retVal;
  }


}


