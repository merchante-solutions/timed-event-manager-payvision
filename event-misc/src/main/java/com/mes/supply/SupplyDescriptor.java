/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/supply/SupplyDescriptor.java $

  Description:

    SupplyDescriptor

    Represents descriptive attributes associated with a single supply item.

  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 5/04/04 2:51p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.supply;

// log4j
import org.apache.log4j.Logger;

public class SupplyDescriptor
{

  // create class log category
  static Logger log = Logger.getLogger(SupplyDescriptor.class);

  // data members
  public String    seqNum;
  public String    topGrpTitle;
  public String    groupTitle;
  public String    partNum;
  public String    title;
  public String    unit;
  public String    equivalent;
  protected double cost;
  public double    shipCostRush;
  public double    shipCostStandard;

  // construction
  public SupplyDescriptor()
  {
    seqNum=null;
    topGrpTitle=null;
    groupTitle=null;
    partNum=null;
    title=null;
    unit=null;
    equivalent=null;
    cost = 0.00;
    shipCostRush = 0.00;
    shipCostStandard=0.00;
  }

  public void setCost(double cost){
    this.cost=cost;
  }

  public double getDisplayPrice(double markUp){
    return cost*markUp;
  }
} // class SupplyDescriptors