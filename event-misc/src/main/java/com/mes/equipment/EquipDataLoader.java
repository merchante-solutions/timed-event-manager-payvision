package com.mes.equipment;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Category;
import com.mes.database.SQLJConnectionBase;

public class EquipDataLoader extends SQLJConnectionBase
{
  static Category log = Category.getInstance(EquipDataLoader.class.getName());

  public static final int TERMINAL_ONLY = 1;
  private String terminalQuery =  "        and ( pf.feat_id in (913, 915, 917))  ";


  private static EquipDataLoader loader = new EquipDataLoader();

  private EquipDataLoader()
  {
  }

  private List createSelectorList(String merchNum, Class selectorClass,
    String valuePrefix, int extra, int otherCount)

  {
    PreparedStatement ps    = null;
    ResultSet         rs    = null;
    List              items = null;

    try
    {
      connect();
      /*
      String qs
        = "select  ei.ei_serial_number             serial_num,        "
        + "        nvl(e.equip_descriptor,'--')    description,       "
        + "        nvl(es.equip_status_desc,'--')  disposition,       "
        + "        nvl(ei.ei_part_number,'--')     part_num,          "
        + "        nvl(et.equiptype_description,'--')                 "
        + "                                        equip_type         "
        + "                                                           "
        + "from    equip_inventory ei,                                "
        + "        equipment       e,                                 "
        + "        equiptype       et,                                "
        + "        equip_status    es                                 "
        + "                                                           "
        + "where   ei.ei_merchant_number     = ?                      "
        + "        and ei.ei_lrb             = es.equip_status_id(+)  "
        + "        and ei.ei_part_number     = e.equip_model(+)       "
        + "        and e.equiptype_code      = et.equiptype_code      "
        + "";
         */
        String qs
          = "select                                               "
          + "   pt.description          description,              "
          + "   ps.disp_code            disposition,              "
          + "   pt.model_code           part_num,                 "
          + "   ps.serial_num           serial_num,               "
          + "   ''                      v_number,                 "
//          + "   msi.vnum                v_number,                 "
          + "   pf.feat_name            equip_type                "
          + " from                                                "
          + "   tmg_part_states ps,                               "
          + "   tmg_deployment d,                                 "
          + "   tmg_part_types pt,                                "
          + "   tmg_part_feature_maps pfm,                        "
          + "   tmg_part_features pf                             "
//          + "   mms_stage_info msi                                "
          + " where                                               "
          + "   ps.deploy_id = d.deploy_id                        "
          + "   and d.merch_num = ?                               "
          + "  and ps.pt_id = pt.pt_id                            "
          + "  and pt.pt_id = pfm.pt_id                           "
          + "  and pfm.feat_id = pf.feat_id                       "
          + "  and pf.ft_code = 'CATEGORY'                        "
//          + "  and d.merch_num = msi.merch_number(+)              "
          + "  and d.end_ts is null                               ";
      switch(extra)
      {
        case TERMINAL_ONLY:
          qs += terminalQuery;
          break;

        default:
          //do nothing
      }

      qs += "  order by equip_type,description                    ";

      log.debug(qs);

      ps = con.prepareStatement(qs);
      ps.setString(1,merchNum);

      rs = ps.executeQuery();
      items = new ArrayList();
      int idx = 0;
      while (rs.next())
      {
        EquipDataItem item = new EquipDataItem();

        item.setDescription (rs.getString("description"));
        item.setDisposition (rs.getString("disposition"));
        item.setPartNum     (rs.getString("part_num"));
        item.setSerialNum   (rs.getString("serial_num"));
        item.setEquipType   (rs.getString("equip_type"));

        EquipSelectorItem esi = (EquipSelectorItem)selectorClass.newInstance();
        esi.setEquipDataItem(item,valuePrefix + "_" + idx);
        items.add(esi);
        ++idx;
      }

      // add other selectors
      for (int i = 0; i < otherCount; ++i)
      {
        EquipSelectorItem esi = (EquipSelectorItem)selectorClass.newInstance();
        esi.setType(EquipSelectorItem.T_OTHER);
        esi.setValue(valuePrefix + "_" + idx);
        items.add(esi);
        ++idx;
      }
    }
    catch (Exception e)
    {
      log.error("createSelectorList(): " + e);
      e.printStackTrace();
      throw new RuntimeException("EquipDataLoader.createSelectorList() error: "
        + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }

    return items;
  }

  public static List getSelectorList(String merchNum, Class selectorClass,
    String valuePrefix, int otherCount)
  {
    return loader.createSelectorList(merchNum,selectorClass,valuePrefix, 0, otherCount);
  }

  public static List getSelectorList(String merchNum, Class selectorClass,
    String valuePrefix, int extra, int otherCount)
  {
    return loader.createSelectorList(merchNum,selectorClass,valuePrefix, extra, otherCount);
  }
}