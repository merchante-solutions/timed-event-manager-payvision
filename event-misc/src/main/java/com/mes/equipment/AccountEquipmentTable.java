package com.mes.equipment;

import java.util.Iterator;
import java.util.List;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.tools.Key;
import com.mes.tools.TableView;
import com.mes.tools.TableViewHelper;


public final class AccountEquipmentTable
  implements TableView
{
  // create class log category
  static Category log = Category.getInstance(AccountEquipmentTable.class.getName());

  // constants
  private static final String[][] columnData = 
    {
       {"Model",                  "getEquipModel"}
      ,{"Desc",                   "getDescriptor"}
      ,{"TSYS Desc",              "getTsysEquipDesc"}
      ,{"Type",                   "getEquipTypeName"}
      ,{"Mfr",                    "getEquipMfrName"}
      ,{"Disposition",            "getDisposition"}
    };
  
  // data members
  protected List              list                = null;
  protected TableViewHelper   helper              = null;
  protected String            detailPageBaseUrl   = null;
  protected String            queryStringSuffix   = null;
  
  // construction
  public AccountEquipmentTable(List list, String detailPageBaseUrl, String queryStringSuffix)
  {
    this.list = list;
    this.helper = new TableViewHelper(EquipmentDataBase.class, this, columnData);
    this.detailPageBaseUrl = detailPageBaseUrl;
    this.queryStringSuffix = queryStringSuffix;
  }
  
  public List           getRows()
  {
    return list;
  }
  
  public void           setRows(List list)
  {
    this.list = list;
  }
  
  public Key            getRowKey(int rowOrdinal)
  {
    EquipmentDataBase edb = (EquipmentDataBase)helper.getRowObject(rowOrdinal);
    return (Key)edb.getKey();
  }

  public Object         getRowObject(int rowOrdinal)
  {
    return helper.getRowObject(rowOrdinal);
  }

  public int            getNumRows()
  {
    return list==null? 0:list.size();
  }
  
  public int            getNumCols()
  {
    return columnData.length;
  }
  
  public String         getLinkToDetailColumnName()
  {
    return "Model";
  }
  
  public String         getDetailPageBaseUrl()
  {
    return detailPageBaseUrl;
  }
    
  public String         getQueryStringSuffix()
  {
    return queryStringSuffix;
  }
  
  public void           setQueryStringSuffix(String v)
  {
    queryStringSuffix=v;
  }
  
  public String getDetailPageQueryString(int rowOrdinal)
  {
    return helper.getDetailPageQueryString(rowOrdinal);
  }

  public Iterator       getRowIterator()
  {
    return list==null? null:list.iterator();
  }
  
  public String         getColumnName(int colOrdinal)
  {
    return helper.getColumnName(colOrdinal);
  }
  
  public String[]       getColumnNames()
  {
    return helper.getColumnNames();
  }
  
  public String         getCell(int rowOrdinal, int colOrdinal)
  {
    return helper.getCell(rowOrdinal, colOrdinal);
  }
  
  public String         getHtmlCell(int rowOrdinal, int colOrdinal)
  {
    return helper.getHtmlCell(rowOrdinal, colOrdinal);
  }

}
