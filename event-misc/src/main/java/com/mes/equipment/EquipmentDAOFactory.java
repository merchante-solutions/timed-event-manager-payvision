package com.mes.equipment;

public final class EquipmentDAOFactory
{
  // Singleton
  public static EquipmentDAOFactory getInstance()
  {
    if(_instance==null)
      _instance = new EquipmentDAOFactory();
    
    return _instance;
  }
  private static EquipmentDAOFactory _instance = null;
  ///
  
  // constants
  public static final int       EQUIPMENTDAO_UNDEFINED = 0;
  public static final int       EQUIPMENTDAO_MESDB     = 1;
  
  // data members
  private int       daoType     = EQUIPMENTDAO_MESDB;
  
  // construction
  private EquipmentDAOFactory()
  {
  }
  
  public void setDaoType(int daoType)
  {
    this.daoType = daoType;
  }
  
  // creational methods
  
  public EquipmentDAO    getEquipmentDAO()
  {
    switch(daoType) {
      case EQUIPMENTDAO_MESDB:
        return new mesdbEquipmentDAO();
      default:
        return null;
    }
  }
  

} // class DAOFactory
