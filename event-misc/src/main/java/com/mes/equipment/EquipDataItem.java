package com.mes.equipment;

public class EquipDataItem
{
  private String description;
  private String disposition;
  private String partNum;
  private String serialNum;
  private String vNumber;
  private String equipType;
  
  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }
  
  public void setDisposition(String disposition)
  {
    this.disposition = disposition;
  }
  public String getDisposition()
  {
    return disposition;
  }
  
  public void setPartNum(String partNum)
  {
    this.partNum = partNum;
  }
  public String getPartNum()
  {
    return partNum;
  }
  
  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }
  
  public void setVNumber(String vNumber)
  {
    this.vNumber = vNumber;
  }
  public String getVNumber()
  {
    return vNumber;
  }
  
  public void setEquipType(String equipType)
  {
    this.equipType = equipType;
  }
  public String getEquipType()
  {
    return equipType;
  }
}