package com.mes.equipment;

import java.util.Date;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.constants.mesConstants;
import com.mes.tools.AccountKey;
import com.mes.tools.EquipmentKey;


/**
 * EquipmentDataBuilder
 * 
 * Constructs concrete sub-classes of EquipmentDataBase.
 * Loads data into these concrete sub-classes from database.
 */
public final class EquipmentDataBuilder
{
  // create class log category
  static Category log = Category.getInstance(EquipmentDataBuilder.class.getName());

  // Singleton
  public static EquipmentDataBuilder getInstance()
  {
    if(_instance==null)
    {
      _instance = new EquipmentDataBuilder();
    }
    
    return _instance;
  }
  public static void killInstance()
  {
    _instance = null;
  }
  private static EquipmentDataBuilder _instance = null;
  ///
  
  // constants
  // (NONE)
  
  public class UnknownEquipmentTypeException extends Exception
  {
    public UnknownEquipmentTypeException(int type)
    {
      super("Unknown Equipment type: '"+type+"' exception occurred.");
    }
  } // UnknownEquipmentTypeException
  
  public static boolean isEquipment(int type)
  {
    return (type != mesConstants.EQUIPTYPE_TERMAPP && type != mesConstants.EQUIPTYPE_TIDREQUEST);
  }
  
  /*****  BUILD ROUTINES ******/
  
  public EquipmentDataBase build(EquipmentKey ekey)
    throws UnknownEquipmentTypeException
  {
    return ekey == null ? null : build(ekey.getEquipType());
  }
  
  public EquipmentDataBase build(int type)
    throws UnknownEquipmentTypeException
  {
    EquipmentDataBase o = null;
    
    switch(type) 
    {
      case mesConstants.EQUIPTYPE_MERCHEQUIP:
        o = new EquipmentData_MerchEquip();
        break;
      case mesConstants.EQUIPTYPE_EQUIPINV:
        o = new EquipmentData_EquipInv();
        break;
      case mesConstants.EQUIPTYPE_TERMAPP:
        o = new TermAppProfileSummary();
        break;
      case mesConstants.EQUIPTYPE_TIDREQUEST:
        o = new TidRequest();
        break;
      default:
        throw new EquipmentDataBuilder.UnknownEquipmentTypeException(type);
    }
    
    return o;
  }
  
  public EquipmentHistory buildEquipmentHistory(EquipmentData_EquipInv ei)
  {
    EquipmentHistory eh = null;
    
    try 
    {
      eh = new EquipmentHistory();
      eh.setEquipmentKey(ei.getKey());
      eh.setOwner(ei.getOwner());
      eh.setEquipStatusCode(ei.getStatusCode());
      eh.setEquipStatusName(ei.getStatusName());
      eh.setOwner(ei.getOwner());
      eh.setDate(new Date());
    }
    catch(Exception e) 
    {
      log.error("buildEquipmentHistory() EXCEPTION: "+e.toString());
    }
    
    return eh;
  }
  
  
  /*****  LOAD ROUTINES ******/

  /**
   * loadEquipment()
   * 
   * Loads all col associated with the specified account (merchant number).
   */
  public Vector loadEquipment(AccountKey key)
  {
    return loadEquipment(key,true);
  }
  public Vector loadEquipment(AccountKey key, boolean loadHistory)
  {
    Vector col = new Vector(0,1);
    EquipmentDataBase ed = null;
    
    for(int i = mesConstants.EQUIPTYPE__START;i<=mesConstants.EQUIPTYPE__END;i++) 
    {
      if(!isEquipment(i))
      {
        continue;
      }
      
      // build
      try 
      {
        ed = build(i);
      }
      catch(Exception e) 
      {
        com.mes.support.SyncLog.LogEntry(EquipmentDataBuilder.class.getName() + "loadEquipment()",e.toString());
        continue;
      }
      
      // load
      Vector eq = ed.load(key);
      
      // load equipment history?
      if(loadHistory && eq.size()>0) 
      {
        for(int j=0; j<eq.size(); j++) 
        {
          ed = (EquipmentDataBase)eq.elementAt(j);
          //log.debug("loadEquipment(AccountKey) - loading equipment history for '"+ed.getKey().toString()+"'...");
          Vector v = EquipmentHistory.load(ed.getKey());
          //log.debug("loadEquipment(AccountKey) - num equip. history entries ="+(v!=null? v.size():0));
          if(v!=null && v.size()>0)
            ((EquipmentData_EquipInv)ed).setEquipmentHistory(v);
        }
      }
      
      // ammend to return vector
      try 
      {
        if(eq.size()>0)
        {
          col.addAll(eq);
        }
      }
      catch(Exception e) 
      {
      }
    }
    return col;
  }
  
  /**
   * loadTerminalProfiles()
   * 
   * Loads all terminal profiles with the specified account (merchant number).
   */
  public Vector loadTerminalProfiles(AccountKey key)
  {
    Vector col = new Vector(0,1);
    
    try 
    {
      // build
      EquipmentDataBase ed = build(mesConstants.EQUIPTYPE_TERMAPP);
      
      // load
      Vector eq = ed.load(key);
      
      // ammend to return vector
      if(eq.size()>0)
      {
        col.addAll(eq);
      }
    }
    catch(Exception e) 
    {
      com.mes.support.SyncLog.LogEntry(EquipmentDataBuilder.class.getName() + "loadTerminalProfiles()",e.toString());
    }
      
    return col;
  }
  
  /**
   * loadTidRequests()
   * 
   * Loads all tid requests with the specified account.
   */
  public Vector loadTidRequests(AccountKey key)
  {
    Vector col = new Vector(0,1);
    
    try 
    {
      // build
      EquipmentDataBase ed = build(mesConstants.EQUIPTYPE_TIDREQUEST);
      // load
      Vector eq = ed.load(key);
      // ammend to return vector
      if(eq.size()>0)
      {
        col.addAll(eq);
      }
    }
    catch(Exception e) 
    {
      com.mes.support.SyncLog.LogEntry(EquipmentDataBuilder.class.getName() + "loadTidRequests()",e.toString());
    }
    
    return col;
  }
  
  public EquipmentDataBase load(EquipmentKey key)
    throws UnknownEquipmentTypeException, Exception
  {
    return load(key,false);
  }
  public EquipmentDataBase load(EquipmentKey key, boolean bLoadHistory)
    throws UnknownEquipmentTypeException, Exception
  {
    EquipmentDataBase ed = this.build(key.getEquipType());
    
    if(!ed.load(key))
    {
      throw new Exception("Unable to load Equipment Data.");
    }
    
    // load equipment history?
    if(bLoadHistory && isEquipment(ed.getType())) 
    {
      Vector v = EquipmentHistory.load(ed.getKey());
      if(v!=null && v.size()>0)
      {
        ((EquipmentData_EquipInv)ed).setEquipmentHistory(v);
      }
    }
      
    return ed;
  }
  
} // EquipmentDataBuilder
