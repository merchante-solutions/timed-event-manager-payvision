package com.mes.equipment;

import java.util.Iterator;
import java.util.List;
import com.mes.tools.Key;
import com.mes.tools.TableView;
import com.mes.tools.TableViewHelper;

public final class AccountTerminalProfilesTable
  implements TableView
{
  // constants
  private static final String[][] columnData = 
    {
       {"VNum",                   "getVnum"}
      ,{"TID",                    "getTid"}
      ,{"Term App",               "getTermAppDescriptor"}
      ,{"Equip Model",            "getEquipModel"}
      ,{"Trmnl Num",              "getTerminalNumber"}
      ,{"Store Num",              "getStoreNumber"}
      ,{"Lctn Num",               "getLocationNumber"}
    };
  
  // data members
  protected List                  list                = null;
  protected TableViewHelper       helper              = null;
  protected String                detailPageBaseUrl   = null;
  protected String                queryStringSuffix   = null;
  
  // construction
  public AccountTerminalProfilesTable(List list, String detailPageBaseUrl, String queryStringSuffix)
  {
    this.list = list;
    this.helper = new TableViewHelper(TermAppProfileSummary.class, this, columnData);
    this.detailPageBaseUrl = detailPageBaseUrl;
    this.queryStringSuffix = queryStringSuffix;
  }
  
  public List           getRows()
  {
    return list;
  }
  
  public void           setRows(List list)
  {
    this.list = list;
  }
  
  public int            getNumCols()
  {
    return columnData.length;
  }

  public Key           getRowKey(int rowOrdinal)
  {
    TermAppProfileSummary taprs = (TermAppProfileSummary)helper.getRowObject(rowOrdinal);
    return taprs.getKey();
  }

  public Object         getRowObject(int rowOrdinal)
  {
    return helper.getRowObject(rowOrdinal);
  }

  public int            getNumRows()
  {
    return list==null? 0:list.size();
  }
  
  public String         getLinkToDetailColumnName()
  {
    return "VNum";
  }
  
  public String         getDetailPageBaseUrl()
  {
    return detailPageBaseUrl;
  }

  public String         getQueryStringSuffix()
  {
    return queryStringSuffix;
  }
  
  public void           setQueryStringSuffix(String v)
  {
    queryStringSuffix=v;
  }
  
  public String getDetailPageQueryString(int rowOrdinal)
  {
    return helper.getDetailPageQueryString(rowOrdinal);
  }

  public Iterator       getRowIterator()
  {
    return list==null? null:list.iterator();
  }
  
  public String         getColumnName(int colOrdinal)
  {
    return helper.getColumnName(colOrdinal);
  }
  
  public String[]       getColumnNames()
  {
    return helper.getColumnNames();
  }
  
  public String         getCell(int rowOrdinal, int colOrdinal)
  {
    return helper.getCell(rowOrdinal, colOrdinal);
  }
  
  public String         getHtmlCell(int rowOrdinal, int colOrdinal)
  {
    return helper.getHtmlCell(rowOrdinal, colOrdinal);
  }

}
