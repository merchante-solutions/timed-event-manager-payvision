/*@lineinfo:filename=mesdbEquipmentDAO*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.equipment;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.tools.EquipmentKey;
import sqlj.runtime.ResultSetIterator;


public final class mesdbEquipmentDAO extends SQLJConnectionBase
  implements EquipmentDAO
{
  // log4j class log category
  static Category log = Category.getInstance(EquipmentDataBuilder.class.getName());

  // constants
  // (none)

  // methods
  
  
  // construction
  public mesdbEquipmentDAO()
  {
    super(false);
  }
  
  /**
   * createEquipmentData
   * 
   * @param   equipType   corres. to mesConstants.EQUIPTYPE{...}
   */
  public EquipmentDataBase            createEquipmentData(int equipType)
  {
    EquipmentDataBase ed = null;
    
    try {
      ed = EquipmentDataBuilder.getInstance().build(equipType);
    }
    catch(Exception e) {
      log.error("createEquipmentData() EXCEPTION: "+e.toString());
    }

    return ed;
  }

  public EquipmentDataBase            findEquipmentData(EquipmentKey key)
  {
    EquipmentDataBase ed = null;
    
    try {
      ed = EquipmentDataBuilder.getInstance().load(key);
    }
    catch(Exception e) {
      log.error("findEquipmentData() EXCEPTION: "+e.toString());
    }

    return ed;
  }

  public boolean                  persistEquipmentData(EquipmentDataBase ed)
  {
    log.debug("persistEquipmentData(EquipmentDataBase) - START");
    
    boolean wasStale = false;
    boolean rval = false;

    try {

      EquipmentKey key = ed.getKey();
      log.debug("persistEquipmentData(EquipmentDataBase) - key==null? "+(key==null? "Yes":"No"));
      if(!key.isSet()) {
        log.error("persistEquipmentData(EquipmentDataBase) - ERROR: Equipment key not set.");
        return false;
      }

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      switch(key.getEquipType()) {
        
        // MeS Inventory
        case mesConstants.EQUIPTYPE_EQUIPINV:
          
          EquipmentData_EquipInv ei = (EquipmentData_EquipInv)ed;
          Timestamp rd = ei.getReceivedDate()==null? null:new Timestamp(ei.getReceivedDate().getTime());
          Timestamp dd = ei.getDeployedDate()==null? null:new Timestamp(ei.getDeployedDate().getTime());
          
          if(key.exists()) {
            // UPDATE
            /*@lineinfo:generated-code*//*@lineinfo:100^13*/

//  ************************************************************
//  #sql [Ctx] { update      equip_inventory
//              
//                set          EI_UNIT_COST          = :ei.getUnitCost()
//                            ,EI_CLASS              = :ei.getClassCode()
//                            ,EI_STATUS             = :ei.getStatusCode()
//                            ,EI_LRB                = :ei.getEquipLendTypeCode()
//                            ,EI_RECEIVED_DATE      = :rd
//                            ,EI_DEPLOYED_DATE      = :dd
//                            ,EI_INVOICE_NUMBER     = :ei.getInvoiceNumber()
//                            ,EI_TRANSACTION_ID     = :ei.getTransId()
//                            ,EI_MERCHANT_NUMBER    = :ei.getMerchantNumber()
//                            ,EI_ORIGINAL_CLASS     = :ei.getOriginalClassCode()
//                            ,EI_SHIPPING_COST      = :ei.getShippingCost()
//                            ,EI_LRB_PRICE          = :ei.getLrbPrice()
//                            ,EI_OWNER              = :ei.getOwner()
//                          
//                where       ei_part_number = :ei.getPartNumber()
//                            and ei_serial_number = :ei.getSerialNumber()
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_421 = ei.getUnitCost();
 int __sJT_422 = ei.getClassCode();
 int __sJT_423 = ei.getStatusCode();
 int __sJT_424 = ei.getEquipLendTypeCode();
 String __sJT_425 = ei.getInvoiceNumber();
 String __sJT_426 = ei.getTransId();
 String __sJT_427 = ei.getMerchantNumber();
 int __sJT_428 = ei.getOriginalClassCode();
 double __sJT_429 = ei.getShippingCost();
 double __sJT_430 = ei.getLrbPrice();
 int __sJT_431 = ei.getOwner();
 String __sJT_432 = ei.getPartNumber();
 String __sJT_433 = ei.getSerialNumber();
   String theSqlTS = "update      equip_inventory\n            \n              set          EI_UNIT_COST          =  :1 \n                          ,EI_CLASS              =  :2 \n                          ,EI_STATUS             =  :3 \n                          ,EI_LRB                =  :4 \n                          ,EI_RECEIVED_DATE      =  :5 \n                          ,EI_DEPLOYED_DATE      =  :6 \n                          ,EI_INVOICE_NUMBER     =  :7 \n                          ,EI_TRANSACTION_ID     =  :8 \n                          ,EI_MERCHANT_NUMBER    =  :9 \n                          ,EI_ORIGINAL_CLASS     =  :10 \n                          ,EI_SHIPPING_COST      =  :11 \n                          ,EI_LRB_PRICE          =  :12 \n                          ,EI_OWNER              =  :13 \n                        \n              where       ei_part_number =  :14 \n                          and ei_serial_number =  :15";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.equipment.mesdbEquipmentDAO",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_421);
   __sJT_st.setInt(2,__sJT_422);
   __sJT_st.setInt(3,__sJT_423);
   __sJT_st.setInt(4,__sJT_424);
   __sJT_st.setTimestamp(5,rd);
   __sJT_st.setTimestamp(6,dd);
   __sJT_st.setString(7,__sJT_425);
   __sJT_st.setString(8,__sJT_426);
   __sJT_st.setString(9,__sJT_427);
   __sJT_st.setInt(10,__sJT_428);
   __sJT_st.setDouble(11,__sJT_429);
   __sJT_st.setDouble(12,__sJT_430);
   __sJT_st.setInt(13,__sJT_431);
   __sJT_st.setString(14,__sJT_432);
   __sJT_st.setString(15,__sJT_433);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:120^13*/
          } else {
            /*@lineinfo:generated-code*//*@lineinfo:122^13*/

//  ************************************************************
//  #sql [Ctx] { insert into  equip_inventory
//                (
//                   EI_PART_NUMBER
//                  ,EI_SERIAL_NUMBER
//                  ,EI_UNIT_COST
//                  ,EI_CLASS
//                  ,EI_STATUS
//                  ,EI_LRB
//                  ,EI_RECEIVED_DATE
//                  ,EI_DEPLOYED_DATE
//                  ,EI_INVOICE_NUMBER
//                  ,EI_TRANSACTION_ID
//                  ,EI_MERCHANT_NUMBER
//                  ,EI_ORIGINAL_CLASS
//                  ,EI_SHIPPING_COST
//                  ,EI_LRB_PRICE
//                  ,EI_OWNER
//                )
//                values
//                (
//                  :ei.getPartNumber()
//                 ,:ei.getSerialNumber()
//                 ,:ei.getUnitCost()
//                 ,:ei.getClassCode()
//                 ,:ei.getStatusCode()
//                 ,:ei.getEquipLendTypeCode()
//                 ,:rd
//                 ,:dd
//                 ,:ei.getInvoiceNumber()
//                 ,:ei.getTransId()
//                 ,:ei.getMerchantNumber()
//                 ,:ei.getOriginalClassCode()
//                 ,:ei.getShippingCost()
//                 ,:ei.getLrbPrice()
//                 ,:ei.getOwner()
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_434 = ei.getPartNumber();
 String __sJT_435 = ei.getSerialNumber();
 double __sJT_436 = ei.getUnitCost();
 int __sJT_437 = ei.getClassCode();
 int __sJT_438 = ei.getStatusCode();
 int __sJT_439 = ei.getEquipLendTypeCode();
 String __sJT_440 = ei.getInvoiceNumber();
 String __sJT_441 = ei.getTransId();
 String __sJT_442 = ei.getMerchantNumber();
 int __sJT_443 = ei.getOriginalClassCode();
 double __sJT_444 = ei.getShippingCost();
 double __sJT_445 = ei.getLrbPrice();
 int __sJT_446 = ei.getOwner();
   String theSqlTS = "insert into  equip_inventory\n              (\n                 EI_PART_NUMBER\n                ,EI_SERIAL_NUMBER\n                ,EI_UNIT_COST\n                ,EI_CLASS\n                ,EI_STATUS\n                ,EI_LRB\n                ,EI_RECEIVED_DATE\n                ,EI_DEPLOYED_DATE\n                ,EI_INVOICE_NUMBER\n                ,EI_TRANSACTION_ID\n                ,EI_MERCHANT_NUMBER\n                ,EI_ORIGINAL_CLASS\n                ,EI_SHIPPING_COST\n                ,EI_LRB_PRICE\n                ,EI_OWNER\n              )\n              values\n              (\n                 :1 \n               , :2 \n               , :3 \n               , :4 \n               , :5 \n               , :6 \n               , :7 \n               , :8 \n               , :9 \n               , :10 \n               , :11 \n               , :12 \n               , :13 \n               , :14 \n               , :15 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.equipment.mesdbEquipmentDAO",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_434);
   __sJT_st.setString(2,__sJT_435);
   __sJT_st.setDouble(3,__sJT_436);
   __sJT_st.setInt(4,__sJT_437);
   __sJT_st.setInt(5,__sJT_438);
   __sJT_st.setInt(6,__sJT_439);
   __sJT_st.setTimestamp(7,rd);
   __sJT_st.setTimestamp(8,dd);
   __sJT_st.setString(9,__sJT_440);
   __sJT_st.setString(10,__sJT_441);
   __sJT_st.setString(11,__sJT_442);
   __sJT_st.setInt(12,__sJT_443);
   __sJT_st.setDouble(13,__sJT_444);
   __sJT_st.setDouble(14,__sJT_445);
   __sJT_st.setInt(15,__sJT_446);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:160^13*/
          }
          
          // handle mesdb.equipment related fields
          int cnt = 0;
          /*@lineinfo:generated-code*//*@lineinfo:165^11*/

//  ************************************************************
//  #sql [Ctx] { select    count(*)
//              
//              from      equipment e
//              where     e.equip_model = :ei.getEquipModel()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_447 = ei.getEquipModel();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    count(*)\n             \n            from      equipment e\n            where     e.equip_model =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.equipment.mesdbEquipmentDAO",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_447);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:171^11*/
          
          // insert equipment record
          if(cnt < 1) {
            insertEquipmentRecord(ei);
          }
          
          // NOTE: do not update Equipment records (only add)

          break;
        
        default:
          log.error("persistEquipmentData(EquipmentDataBase) - Unsupported equipment type for persist op: "+key.getEquipType());
          return false;
      }
      
      rval = true;
    }
    catch(Exception e) {
      log.error("persistEquipmentData(EquipmentDataBase) EXCEPTION: "+e.toString());
    }
    finally {
      if(rval)  
        commit();
      if(wasStale)
        cleanUp();
    }

    return rval;
  }

  public boolean insertEquipmentRecord(EquipmentDataBase ed)
  {
    boolean rval      = false;
    boolean wasStale  = false;

    try {
      
      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:214^7*/

//  ************************************************************
//  #sql [Ctx] { insert into  equipment
//          (
//             EQUIPMFGR_MFR_CODE
//            ,EQUIPTYPE_CODE
//            ,EQUIP_MODEL
//            ,EQUIP_DESCRIPTOR
//            ,EQUIP_PRICE
//            ,EQUIP_RENT
//            ,EQUIP_USAGE_IND
//            ,TSYS_EQUIP_DESC
//          )
//          values
//          (
//            :ed.getEquipMfrCode()
//           ,:ed.getEquipTypeCode()
//           ,:ed.getEquipModel()
//           ,:ed.getDescriptor()
//           ,:ed.getPrice()
//           ,:ed.getRent()
//           ,:ed.getUsageInd()
//           ,:ed.getTsysEquipDesc()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_448 = ed.getEquipMfrCode();
 int __sJT_449 = ed.getEquipTypeCode();
 String __sJT_450 = ed.getEquipModel();
 String __sJT_451 = ed.getDescriptor();
 double __sJT_452 = ed.getPrice();
 double __sJT_453 = ed.getRent();
 String __sJT_454 = ed.getUsageInd();
 String __sJT_455 = ed.getTsysEquipDesc();
   String theSqlTS = "insert into  equipment\n        (\n           EQUIPMFGR_MFR_CODE\n          ,EQUIPTYPE_CODE\n          ,EQUIP_MODEL\n          ,EQUIP_DESCRIPTOR\n          ,EQUIP_PRICE\n          ,EQUIP_RENT\n          ,EQUIP_USAGE_IND\n          ,TSYS_EQUIP_DESC\n        )\n        values\n        (\n           :1 \n         , :2 \n         , :3 \n         , :4 \n         , :5 \n         , :6 \n         , :7 \n         , :8 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.equipment.mesdbEquipmentDAO",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_448);
   __sJT_st.setInt(2,__sJT_449);
   __sJT_st.setString(3,__sJT_450);
   __sJT_st.setString(4,__sJT_451);
   __sJT_st.setDouble(5,__sJT_452);
   __sJT_st.setDouble(6,__sJT_453);
   __sJT_st.setString(7,__sJT_454);
   __sJT_st.setString(8,__sJT_455);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:238^7*/
      
      rval = true;
    }
    catch(Exception e) {
      log.error("insertEquipmentRecord() EXCEPTION: "+e.toString());
    }
    finally {
      if(rval)  
        commit();
      if(wasStale)
        cleanUp();
    }

    return rval;
  }

  public void                     deleteEquipmentData(EquipmentDataBase ed)
  {
    log.error("deleteEquipmentData() EXCEPTION: CURRENTLY NOT IMPLEMENTED");
  }

  public boolean addEquipmentHistoryRecord(EquipmentHistory eh)
  {
    boolean rval = false;
    try {
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:265^7*/

//  ************************************************************
//  #sql [Ctx] { insert into  equip_history
//          (
//             EH_SERIAL_NUMBER 
//            ,EH_DATE 
//            ,EH_USER 
//            ,EH_SOURCE_CODE 
//            ,EH_NOTE
//            ,EH_ACTION_PERFORMED 
//            ,EQUIP_STATUS 
//            ,OWNER 
//            ,PART_NUMBER
//          )
//          values
//          (
//             :eh.getSerialNum()
//            ,sysdate
//            ,:eh.getUserId()
//            ,:eh.getSourceCode()
//            ,:eh.getNote()
//            ,:eh.getActionPerformed()
//            ,:eh.getEquipStatusCode()
//            ,:eh.getOwner()
//            ,:eh.getPartNum()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_456 = eh.getSerialNum();
 long __sJT_457 = eh.getUserId();
 int __sJT_458 = eh.getSourceCode();
 String __sJT_459 = eh.getNote();
 String __sJT_460 = eh.getActionPerformed();
 int __sJT_461 = eh.getEquipStatusCode();
 int __sJT_462 = eh.getOwner();
 String __sJT_463 = eh.getPartNum();
   String theSqlTS = "insert into  equip_history\n        (\n           EH_SERIAL_NUMBER \n          ,EH_DATE \n          ,EH_USER \n          ,EH_SOURCE_CODE \n          ,EH_NOTE\n          ,EH_ACTION_PERFORMED \n          ,EQUIP_STATUS \n          ,OWNER \n          ,PART_NUMBER\n        )\n        values\n        (\n            :1 \n          ,sysdate\n          , :2 \n          , :3 \n          , :4 \n          , :5 \n          , :6 \n          , :7 \n          , :8 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.equipment.mesdbEquipmentDAO",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_456);
   __sJT_st.setLong(2,__sJT_457);
   __sJT_st.setInt(3,__sJT_458);
   __sJT_st.setString(4,__sJT_459);
   __sJT_st.setString(5,__sJT_460);
   __sJT_st.setInt(6,__sJT_461);
   __sJT_st.setInt(7,__sJT_462);
   __sJT_st.setString(8,__sJT_463);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:291^7*/
      rval = true;
    }
    catch(Exception e) {
      log.error("addEquipmentHistoryRecord() EXCEPTION: "+e.toString());
    }
    finally {
      cleanUp();
    }
    return rval;
  }

  public ArrayList getSimpleEquipmentList()
  {
    
    ResultSetIterator it        = null;
    ResultSet         rs        = null;
    StringBuffer      qs        = new StringBuffer();
    boolean           wasStale  = false;
    ArrayList list              = new ArrayList();

    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
     /*@lineinfo:generated-code*//*@lineinfo:320^6*/

//  ************************************************************
//  #sql [Ctx] it = { select  eq.equip_model,
//                  eq.equip_descriptor
//          from    equipment       eq 
//          order by eq.equip_model asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  eq.equip_model,\n                eq.equip_descriptor\n        from    equipment       eq \n        order by eq.equip_model asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.equipment.mesdbEquipmentDAO",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.equipment.mesdbEquipmentDAO",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:326^7*/
      
      rs = it.getResultSet();

      while(rs.next())
      {
        list.add(rs.getString("equip_model") + " - " + rs.getString("equip_descriptor"));
      }
      rs.close();
      it.close();
    }  
    catch(Exception e)
    {
      //nothing for now - return empty list
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
      if(wasStale)
      {
        cleanUp();
      }
    }
    
    return list;
  } 
  
}/*@lineinfo:generated-code*/