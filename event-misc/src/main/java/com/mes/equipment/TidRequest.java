/*@lineinfo:filename=TidRequest*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.equipment;

import java.sql.ResultSet;
import java.util.Date;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.tools.AccountKey;
import com.mes.tools.EquipmentKey;
import sqlj.runtime.ResultSetIterator;


/**
 * TidRequest
 * 
 * Read-only value object containing summary-based data for a given TID request.
 */
public class TidRequest extends EquipmentDataBase
{
  // data members
  protected long                    requestId               = 0L;
  protected long                    appSeqNum               = 0L;
  protected String                  vnum                    = "";
  protected String                  tid                     = "";
  protected String                  processMethod           = "";
  protected String                  processStatus           = "";
  protected String                  processResponse         = "";
  protected Date                    processStartDate        = new Date(0L);
  protected Date                    processEndDate          = new Date(0L);
  protected String                  mmsTermApp              = "";
  protected Date                    profileCompletedDate    = new Date(0L);
  protected int                     profGenCode             = mesConstants.PROFGEN_UNDEFINED;
  protected String                  profGenName             = "";
  
  
  // class methods
  // (NONE)
  
  
  // object methods
  
  // construction
  public TidRequest()
  {
  }
  
  public void clear()
  {
    super.clear();

    requestId               = 0L;
    appSeqNum               = 0L;
    vnum                    = "";
    tid                     = "";
    processMethod           = "";
    processStatus           = "";
    processResponse         = "";
    processStartDate.setTime(0L);
    processEndDate.setTime(0L);
    mmsTermApp              = "";
    profileCompletedDate.setTime(0L);
    profGenCode             = mesConstants.PROFGEN_UNDEFINED;
    profGenName             = "";
  }
  
  
  // ******* accessors ***********

  public long getRequestId()
  {
    return requestId;
  }
  
  public long getAppSeqNum()
  {
    return appSeqNum;
  }
  
  public String getVnum()
  {
    return vnum;
  }
  
  public String getTid()
  {
    return tid;
  }
  
  public String getProcessMethod()
  {
    return processMethod;
  }
  
  public String getProcessStatus()
  {
    return processStatus;
  }
  
  public String getProcessResponse()
  {
    return processResponse;
  }
  
  public Date getProcessStartDate()
  {
    return processStartDate;
  }
  
  public Date getProcessEndDate()
  {
    return processEndDate;
  }
  
  public String getMmsTermApp()
  {
    return mmsTermApp;
  }
  
  public Date getProfileCompletionDate()
  {
    return profileCompletedDate;
  }
  
  public int getProfGenCode()
  {
    return profGenCode;
  }
  
  public String getProfGenName()
  {
    return profGenName;
  }
  

  public int getType()
  {
    return mesConstants.EQUIPTYPE_TIDREQUEST;
  }

  public String getTypeDesc()
  {
    return "TID Request";
  }

  // ********* mutators **********
  // (NONE)


  // *********** load routine *******
  
  public Vector load(AccountKey accountKey)
  {
    Vector rval = new Vector(0,1);

    long appSeqNum;
    try {
      appSeqNum = accountKey.getAppSeqNum();
    }
    catch(Exception e) {
      return rval;
    }
    
    TidRequest o = null;
    
    // find all associated terminal profile requests
    ResultSetIterator           it          = null;
    ResultSet                   rs          = null;
      
    try {
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:171^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    msi.request_id
//                   ,msi.app_seq_num 
//                   ,msi.vnum 
//                   ,mv.tid
//                   ,mv.PROFILE_COMPLETED_DATE 
//                   ,nvl(mv.PROFILE_GENERATOR_CODE,:mesConstants.DEFAULT_PROFGEN) PROFILE_GENERATOR_CODE
//                   ,msi.process_method
//                   ,msi.process_status
//                   ,msi.process_response
//                   ,msi.process_start_date
//                   ,msi.process_end_date
//                   ,nvl(msi.term_application_profgen,'STAGE') mms_app_code
//                   ,epg.name profile_generator_name
//          
//          from      mms_stage_info             msi
//                   ,equip_profile_generators   epg
//                   ,merch_vnumber              mv
//  
//          where     msi.app_seq_num(+) = :appSeqNum
//                    and epg.code(+) = nvl(msi.PROFILE_GENERATOR_CODE,:mesConstants.DEFAULT_PROFGEN)
//                    and msi.vnum = mv.vnumber(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    msi.request_id\n                 ,msi.app_seq_num \n                 ,msi.vnum \n                 ,mv.tid\n                 ,mv.PROFILE_COMPLETED_DATE \n                 ,nvl(mv.PROFILE_GENERATOR_CODE, :1 ) PROFILE_GENERATOR_CODE\n                 ,msi.process_method\n                 ,msi.process_status\n                 ,msi.process_response\n                 ,msi.process_start_date\n                 ,msi.process_end_date\n                 ,nvl(msi.term_application_profgen,'STAGE') mms_app_code\n                 ,epg.name profile_generator_name\n        \n        from      mms_stage_info             msi\n                 ,equip_profile_generators   epg\n                 ,merch_vnumber              mv\n\n        where     msi.app_seq_num(+) =  :2 \n                  and epg.code(+) = nvl(msi.PROFILE_GENERATOR_CODE, :3 )\n                  and msi.vnum = mv.vnumber(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.equipment.TidRequest",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.DEFAULT_PROFGEN);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,mesConstants.DEFAULT_PROFGEN);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.equipment.TidRequest",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:194^7*/
      rs=it.getResultSet();
      
      while(rs.next()) {
        
        o = new TidRequest();
        
        o.requestId               = rs.getLong("request_id");
        o.appSeqNum               = rs.getLong("app_seq_num");
        o.vnum                    = rs.getString("vnum");
        o.tid                     = (rs.getString("tid")==null? "":rs.getString("tid"));
        o.processMethod           = rs.getString("process_method");
        o.processStatus           = rs.getString("process_status");
        o.processResponse         = rs.getString("process_response");
        o.processStartDate.setTime(rs.getTimestamp("process_start_date")==null? 0L:rs.getTimestamp("process_start_date").getTime());
        o.processEndDate.setTime(rs.getTimestamp("process_end_date")==null? 0L:rs.getTimestamp("process_end_date").getTime());
        o.mmsTermApp              = rs.getString("mms_app_code");
        o.profileCompletedDate.setTime(rs.getTimestamp("PROFILE_COMPLETED_DATE")==null? 0L:rs.getTimestamp("PROFILE_COMPLETED_DATE").getTime());
        o.profGenCode             = rs.getInt("profile_generator_code");
        o.profGenName             = rs.getString("profile_generator_name");

        o.descriptor = o.vnum;
        
        o.setEquipmentKey(new EquipmentKey(o.vnum));
        
        rval.addElement(o);
      }
    }
    catch(Exception e) {
      logEntry("load(AccountKey)", e.toString());
    }
    finally {
      try { rs.close(); } catch( Exception e ) { }
      try { it.close(); } catch( Exception e ) { }
      cleanUp();
    }
    
    return rval;
  }

  /**
   * load(EquipmentKey)
   */
  public boolean load(EquipmentKey key)
  {
    if(key==null || key.getEquipType()!=mesConstants.EQUIPTYPE_TIDREQUEST || !key.isSet())
      return false;

    ResultSetIterator           it          = null;
    ResultSet                   rs          = null;
      
    try {

      long requestId = key.getTidRequestKey().getRequestId();

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:251^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    msi.request_id
//                   ,msi.vnum
//                   ,mv.tid
//                   ,msi.process_method
//                   ,msi.process_status
//                   ,msi.process_response
//                   ,msi.process_start_date
//                   ,msi.process_end_date
//                   ,nvl(msi.term_application_profgen,'STAGE') mms_app_code
//                   ,epg.name profile_generator_name
//                   ,mv.PROFILE_COMPLETED_DATE 
//                   ,nvl(mv.PROFILE_GENERATOR_CODE,:mesConstants.DEFAULT_PROFGEN) PROFILE_GENERATOR_CODE
//          
//          from       mms_stage_info             msi
//                    ,equip_profile_generators   epg
//                    ,merch_vnumber              mv
//  
//          where     msi.request_id = :requestId
//                    and epg.code(+) = nvl(msi.PROFILE_GENERATOR_CODE,:mesConstants.DEFAULT_PROFGEN)
//                    and msi.vnum = mv.vnumber(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    msi.request_id\n                 ,msi.vnum\n                 ,mv.tid\n                 ,msi.process_method\n                 ,msi.process_status\n                 ,msi.process_response\n                 ,msi.process_start_date\n                 ,msi.process_end_date\n                 ,nvl(msi.term_application_profgen,'STAGE') mms_app_code\n                 ,epg.name profile_generator_name\n                 ,mv.PROFILE_COMPLETED_DATE \n                 ,nvl(mv.PROFILE_GENERATOR_CODE, :1 ) PROFILE_GENERATOR_CODE\n        \n        from       mms_stage_info             msi\n                  ,equip_profile_generators   epg\n                  ,merch_vnumber              mv\n\n        where     msi.request_id =  :2 \n                  and epg.code(+) = nvl(msi.PROFILE_GENERATOR_CODE, :3 )\n                  and msi.vnum = mv.vnumber(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.equipment.TidRequest",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.DEFAULT_PROFGEN);
   __sJT_st.setLong(2,requestId);
   __sJT_st.setInt(3,mesConstants.DEFAULT_PROFGEN);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.equipment.TidRequest",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:273^7*/
      rs=it.getResultSet();
      
      if(rs.next()) {
        
        this.key                = key;

        requestId               = rs.getLong("request_id");
        appSeqNum               = rs.getLong("app_seq_num");
        vnum                    = rs.getString("vnum");
        tid                     = (rs.getString("tid")==null? "":rs.getString("tid"));
        processMethod           = rs.getString("process_method");
        processStatus           = rs.getString("process_status");
        processResponse         = rs.getString("process_response");
        processStartDate.setTime(rs.getTimestamp("process_start_date")==null? 0L:rs.getTimestamp("process_start_date").getTime());
        processEndDate.setTime(rs.getTimestamp("process_end_date")==null? 0L:rs.getTimestamp("process_end_date").getTime());
        mmsTermApp              = rs.getString("mms_app_code");
        profileCompletedDate.setTime(rs.getTimestamp("PROFILE_COMPLETED_DATE")==null? 0L:rs.getTimestamp("PROFILE_COMPLETED_DATE").getTime());
        profGenCode             = rs.getInt("profile_generator_code");
        profGenName             = rs.getString("profile_generator_name");
        
        descriptor              = Long.toString(requestId);
      
      } else
        return false;
    }
    catch(Exception e) {
      logEntry("load(EquipmentKey)", e.toString());
      return false;
    }
    finally {
      try { rs.close(); } catch( Exception e ) { }
      try { it.close(); } catch( Exception e ) { }
      cleanUp();
    }
    
    return true;
  }

}/*@lineinfo:generated-code*/