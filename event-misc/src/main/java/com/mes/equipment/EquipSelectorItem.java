package com.mes.equipment;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.forms.Condition;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HtmlDiv;
import com.mes.forms.HtmlRenderable;
import com.mes.forms.HtmlText;
import com.mes.forms.SelectorItem;
import com.mes.forms.Validation;

public abstract class EquipSelectorItem extends SelectorItem
{
  static Logger log = Logger.getLogger(EquipSelectorItem.class);

  protected FieldGroup fields;
  protected EquipDataItem dataItem;
  private Map suffixMap = new HashMap();

  public EquipSelectorItem()
  {
  }

  public void setValue(String value)
  {
    super.setValue(value);
    fields = new FieldGroup(value + "_fg");
  }

  public void setEquipDataItem(EquipDataItem dataItem, String value)
  {
    this.dataItem = dataItem;
    setValue(value);
  }

  public FieldGroup getFields()
  {
    return fields;
  }

  private Field findField(String suffix)
  {
    if (fields != null)
    {
      for (Iterator i = fields.iterator(); i.hasNext();)
      {
        Field field = (Field)i.next();
        if (field.getName().toLowerCase().endsWith(suffix))
        {
          return field;
        }
      }
    }
    return null;
  }

  private String getFieldData(String suffix)
  {
    Field field = (Field)suffixMap.get(suffix);
    if (field == null && !suffixMap.containsKey(suffix))
    {
      field = findField(suffix);
      suffixMap.put(suffix,field);
    }
    return field != null ? field.getData() : null;
  }

  public String getSerialNum()
  {
    return dataItem == null ? 
      getFieldData("sernum") : dataItem.getSerialNum();
  }

  public String getDescription()
  {
    return dataItem == null ? 
      getFieldData("description") : dataItem.getDescription();
  }

  public String getDisposition()
  {
    return dataItem == null ? "unknown" : dataItem.getDisposition();
  }

  public String getPartNum()
  {
    return dataItem == null ? "unknown" : dataItem.getPartNum(); 
  }

  public String getEquipType()
  {
    return dataItem == null ? 
      getFieldData("equiptype") : dataItem.getEquipType();
  }

  protected final static HtmlDiv createDiv(HtmlRenderable hr, int width)
  {
    HtmlDiv tag = new HtmlDiv();
    tag.setProperty("style","width: " + width + "px; float: left");
    tag.add(hr);
    return tag;
  }

  protected final static HtmlDiv createDiv(String text, int width)
  {
    return createDiv(new HtmlText(text),width);
  }

  /********************
  Shared validation
  *********************/
  public class FieldValueCondition implements Condition
  {
    private Field field;
    private String equalsData;

    public FieldValueCondition(Field field, String equalsData)
    {
      log.debug("creating local FieldValueCondition");
      this.field = field;
      this.equalsData = equalsData;
    }

    public boolean isMet()
    {
      return field.getData().equals(equalsData);
    }
  }

  public class ConditionalRequiredValidation implements Validation
  {
    private Condition condition;
    private String errorText = null;

    public ConditionalRequiredValidation(Condition condition)
    {
      this.condition = condition;
    }
    public ConditionalRequiredValidation(Condition condition, String errorText)
    {
      this.condition = condition;
      this.errorText = errorText;
    }

    public boolean validate(String fieldData)
    {
      if (condition != null && condition.isMet()
          && (fieldData == null || fieldData.length() == 0))
      {
        return false;
      }

      return true;
    }

    public String getErrorText()
    {
      if (errorText == null)
      {
        return "Field required";
      }

      return errorText;
    }
  }

  public class AtLeastOneValidation implements Validation
  {
    protected String          ErrorText       = null;
    protected Vector          FieldList       = new Vector();

    public AtLeastOneValidation(String errorText)
    {
      ErrorText = errorText;
    }
    public AtLeastOneValidation(String errorText, Vector fields)
    {
      ErrorText  = errorText;
      FieldList  = fields;
    }
    public AtLeastOneValidation(String errorText, FieldGroup fg)
    {
      ErrorText  = errorText;

      Iterator i = fg.iterator();
      while(i.hasNext())
        FieldList.add((Field)i.next());
    }

    public void addField(Field field)
    {
      FieldList.add(field);
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      // require at least one

      for (int i = 0; i < FieldList.size(); ++i ) {
        if(!((Field)FieldList.elementAt(i)).isBlank())
          return true;
      }

      return false;
    }

    public String getErrorText()
    {
      return(ErrorText);
    }
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("\nserial number = ").append(getSerialNum()).append("\n");
    sb.append("description = ").append(getDescription()).append("\n");
    sb.append("disposition =").append(getDisposition()).append("\n");
    sb.append("part num = ").append(getPartNum()).append("\n");
    sb.append("equip type = ").append(getEquipType()).append("\n");
    return sb.toString();
  }

}
