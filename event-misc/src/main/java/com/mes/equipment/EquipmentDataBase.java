package com.mes.equipment;

import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import com.mes.tools.AccountKey;
import com.mes.tools.EquipmentKey;


public abstract class EquipmentDataBase extends SQLJConnectionBase
  implements Cloneable
{
  // constants
  // (NONE)
  
  
  // data members
  protected EquipmentKey  key                       = null;
  protected String        descriptor                = "";
  protected String        disposition               = "";
  protected String        equipModel                = "";
  protected int           equipTypeCode             = 0;
  protected String        equipTypeName             = "";
  protected int           equipMfrCode              = 0;
  protected String        equipMfrName              = "";
  protected String        tsysEquipDesc             = "";
  protected double        price                     = 0d;
  protected double        rent                      = 0d;
  protected String        usageInd                  = "";
  
  
  // class methods
  // (NONE)
  

  // object methods
  
  // construction
  protected EquipmentDataBase()
  {
  }
  
  public EquipmentDataBase getCopy()
  {
    return (EquipmentDataBase)this.clone();
  }
  
  protected Object clone()
  {
    try {
      return super.clone();
    }
    catch(Exception e) {
      return null;
    }
  }
  
  public void clear()
  {
    key                       = null;
    descriptor                = "";
    disposition               = "";
    equipModel                = "";
    equipTypeCode             = 0;
    equipTypeName             = "";
    equipMfrCode              = 0;
    equipMfrName              = "";
    tsysEquipDesc             = "";
    price                     = 0d;
    rent                      = 0d;
    usageInd                  = "";
    //associate                 = null;
  }
  
  // accessors
  public EquipmentKey     getKey()                      { return key; }
  public String           getDescriptor()               { return descriptor; }
  public String           getDisposition()              { return disposition; }
  public String           getEquipModel()               { return equipModel; }
  public int              getEquipTypeCode()            { return equipTypeCode; }
  public String           getEquipTypeName()            { return equipTypeName; }
  public int              getEquipMfrCode()             { return equipMfrCode; }
  public String           getEquipMfrName()             { return equipMfrName; }
  public String           getTsysEquipDesc()            { return tsysEquipDesc; }
  public double           getPrice()                    { return price; }
  public double           getRent()                     { return rent; }
  public String           getUsageInd()                 { return usageInd; }
  
  //public EquipmentDataBase   getAssociate()             { return associate; }

  // mutators
  public void setEquipmentKey(EquipmentKey v)
  {
    key = v;
  }
  
  public void setDescriptor(String v)
  {
    descriptor = v;
  }
  public void setDisposition(String v)
  {
    disposition = v;
  }
  public void setEquipModel(String v)
  {
    equipModel = v;
  }
  public void setEquipTypeCode(int v)
  {
    equipTypeCode = v;
  }
  public void setEquipTypeName(String v)
  {
    equipTypeName = v;
  }
  public void setEquipMfrCode(int v)
  {
    equipMfrCode = v;
  }
  public void setEquipMfrName(String v)
  {
    equipMfrName = v;
  }
  public void setTsysEquipDesc(String v)
  {
    tsysEquipDesc = v;
  }
  public void setPrice(double v)
  {
    price = v;
  }
  public void setRent(double v)
  {
    rent = v;
  }
  public void setUsageInd(String v)
  {
    usageInd = v;
  }
  
  public boolean equals(Object obj)
  {
    if(!(obj instanceof EquipmentDataBase))
      return false;
    
    if(key==null)
      return false;
    
    return key.equals(((EquipmentDataBase)obj).getKey());
  }
  
  /**
   * getType()
   * 
   * mesConstants.EQUIPTYPE_{...}
   */
  public abstract int     getType();
  
  /**
   * getTypeDesc()
   * 
   * Descriptive name of the equip type corres. to the concrete sub-class.
   */
  public abstract String  getTypeDesc();
  
  /**
   * load()
   * 
   * Loads all equipment of 'this' type 
   *  associated with account speicified by the account key parameter.
   */
  public abstract Vector  load(AccountKey key);
  
  /**
   * load()
   * 
   * Loads the equipment item of 'this' type associated with the specified equipment key.
   * Returns whether successful or not.
   */
  public abstract boolean load(EquipmentKey key);

} // EquipmentDataBase
