package com.mes.equipment;

import java.util.ArrayList;
import com.mes.tools.EquipmentKey;

/**
 * EquipmentDAO interface
 */
public interface EquipmentDAO
{
  // constants
  // (none)

  // methods

  public EquipmentDataBase        createEquipmentData(int equipType);
  public EquipmentDataBase        findEquipmentData(EquipmentKey key);
  public boolean                  persistEquipmentData(EquipmentDataBase ed);
  public void                     deleteEquipmentData(EquipmentDataBase ed);

  public boolean                  addEquipmentHistoryRecord(EquipmentHistory eh);

  public ArrayList getSimpleEquipmentList();
} // interface EquipmentDataDAO
