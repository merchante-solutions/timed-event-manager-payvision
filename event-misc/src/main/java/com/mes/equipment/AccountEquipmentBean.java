package com.mes.equipment;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.constants.mesConstants;
import com.mes.support.HttpHelper;
import com.mes.tools.AccountKey;
import com.mes.tools.EquipmentKey;
import com.mes.tools.Key;
import com.mes.tools.KeyBuilder;
import com.mes.tools.TableView;


/**
 * AccountEquipmentBean
 * 
 * Represents all associated equipment for a given account (merchant).
 */
public final class AccountEquipmentBean
{
  // create class log category
  static Category log = Category.getInstance(AccountEquipmentBean.class.getName());

  
  // constants
  public static final String       BASEURL_EQUIPMENTDETAIL       = "/jsp/maintenance/equipinfodtl.jsp";
  public static final String       BASEURL_TERMAPPDETAIL         = "/jsp/maintenance/termappdtl.jsp";
  public static final String       BASEURL_TIDREQUESTDETAIL      = "/jsp/credit/mms_screen.jsp";
  public static final String       BASEURL_EQUIPPROFILE          = "/srv/eqprof?method=vep";
  
  
  // data members
  protected   boolean                       accountLoaded             = false;
  protected   boolean                       detailItemLoaded          = false;
  protected   AccountKey                    key                       = null;
  protected   AccountEquipmentTable         equipmentTable            = null;
  protected   AccountTerminalProfilesTable  profilesTable             = null;
  protected   AccountTidRequestsTable       requestsTable             = null;
  protected   EquipmentDataBase             detailItem                = null;
  protected   String                        detailItemAssociateLink   = null;
  protected   String                        accountDescriptor         = "";
  protected   String                        queryStringSuffix         = "";
  protected   Hashtable                     bindings                  = new Hashtable();
    // FORMAT: { {key: equipment ref},{element: termapp ref} }
  protected   boolean                       showEquipHistories        = false;
    // those equipment items associated with term apps (and vice versa)
  
  
  // class methods

  public static String getTermAppProfileDetailLink(TermAppProfileSummary taps)
  {
    StringBuffer sb = null;
    
    if(taps != null)
    {
      sb = new StringBuffer();
    
      sb.append("<a href=\"");
      sb.append(BASEURL_EQUIPPROFILE);
      sb.append("&id=");
      sb.append(taps.getRequestId());
      sb.append("\">Profile Detail</a>");
    }
    
    return sb == null ? null : sb.toString();
  }
  
  
  
  // object methods
  
  // construction
  public AccountEquipmentBean()
  {
  }
  public AccountEquipmentBean(AccountKey key)
  {
    this.key = key;
  }
  public AccountEquipmentBean(String s,boolean isMerchNumOrControlNum)
  {
    key = new AccountKey(s,isMerchNumOrControlNum);
  }
  public AccountEquipmentBean(long appSeqNum)
  {
    key = new AccountKey(appSeqNum);
  }
  
  protected void clear()
  {
    accountLoaded             = false;
    detailItemLoaded          = false;
    key                       = null;
    equipmentTable            = null;
    profilesTable             = null;
    requestsTable             = null;
    detailItem                = null;
    detailItemAssociateLink   = null;
    accountDescriptor         = "";
    queryStringSuffix         = "";
    bindings.clear();
    showEquipHistories       = false;
  }
  
  public String getVnumberLink(String vnum)
  {
    long appSeqNum;
    StringBuffer sb = new StringBuffer();
    
    try 
    {
      appSeqNum = key.getAppSeqNum();
      
      sb.append("<a href=\"");
      sb.append("/jsp/credit/vnumber_assign.jsp?primaryKey=");
      sb.append(appSeqNum);
      sb.append("\">");
      sb.append(vnum);
      sb.append("</a>");
    }
    catch(Exception e) 
    {
      sb.append(vnum);
    }
    
    return sb.toString();
  }
  
  public String getMmsRequestLink(long requestId)
  {
    StringBuffer sb = new StringBuffer();
    
    sb.append("<a href=\"");
    sb.append("/jsp/credit/mms_screen.jsp?requestId=");
    sb.append(requestId);
    sb.append("\">");
    sb.append(requestId);
    sb.append("</a>");
    
    return sb.toString();
  }

  public AccountKey getAccountKey()
  {
    return key;
  }
  
  public void setAccountKey(AccountKey key)
  {
    if(this.key == null || !this.key.equals(key))
    {
      this.key=key;
      this.accountLoaded=false;
    }
  }
  
  public String getMerchantNumber()
  {
    String result = null;
    try 
    {
      result = key.getMerchNum();
    }
    catch(Exception e) 
    {
    }
    return result;
  }
  
  public long getAppSeqNum()
  {
    long result = 0L;
    try 
    {
      result = key.getAppSeqNum();
    }
    catch(Exception e) 
    {
    }
    return result;
  }

  public String getMercCntrlNum()
  {
    String result = "";
    try 
    {
      result = key.getMercCntrlNumber();
    }
    catch(Exception e) 
    {
    }
    return result;
  }

  public String getAccountDescriptor()
  {
    return accountDescriptor;
  }
  
  public void setAccountDescriptor(String v)
  {
    this.accountDescriptor = v;
  }
  
  private EquipmentDataBase getAssociatedEquipment(EquipmentDataBase eq)
  {
    EquipmentDataBase edb = null;
    
    try 
    {
      for(Enumeration enm=bindings.keys();enm.hasMoreElements();) 
      {
        EquipmentDataBase e = (EquipmentDataBase)enm.nextElement();
        TermAppProfileSummary t = (TermAppProfileSummary)bindings.get(e);
        if(t.equals(eq)) 
        {
          edb = e;
          break;
        }
        else if(e.equals(eq)) 
        {
          edb = t;
          break;
        }
      }
    }
    catch(Exception e) 
    {
      log.error("getAssociatedEquipment - EXCEPTION: "+e.toString());
    }
    
    return edb;
  }
  
  public boolean getShowEquipHistories()
  {
    return showEquipHistories;
  }
  
  public void setShowEquipHistories(boolean v)
  {
    showEquipHistories=v;
  }
  
  /****** LOAD ROUTINES *********/
  
  public boolean isAccountLoaded()
  {
    return accountLoaded;
  }
  
  public boolean isDetailItemLoaded()
  {
    return detailItemLoaded;
  }
  
  public void load(HttpServletRequest request)
  {
    try 
    {
      key=KeyBuilder.getInstance().buildAccountKey(request);
      load(key,HttpHelper.getString(request,"ad",""));
    }
    catch(Exception e) 
    {
      log.error("load(request) - EXCEPTION: '"+e.toString()+"'.");
    }
  }
  public void load(String merchNum)
  {
    try 
    {
      if(!accountLoaded || merchNum  == null || !key.getMerchNum().equals(merchNum))
      {
        load(new AccountKey(merchNum,true),null);
      }
    }
    catch(Exception e) 
    {
      log.error("load("+merchNum+"): " + e.toString());
    }
  }
  
  public void load(long appSeqNum)
  {
    try 
    {
      if(!accountLoaded || key.getAppSeqNum() != appSeqNum)
      {
        load(new AccountKey(appSeqNum),null);
      }
    }
    catch(Exception e) 
    {
      log.error("load("+appSeqNum+"): " + e.toString());
    }
  }
  public void load(AccountKey key, String accountDescriptor) throws UnsupportedEncodingException
  {
    if(key != null)
    {
      // account descriptor
      if(accountDescriptor != null)
      {
        this.accountDescriptor = accountDescriptor;
      }
    
      Vector equipment = EquipmentDataBuilder.getInstance().loadEquipment(key,showEquipHistories);
      Vector vnums     = EquipmentDataBuilder.getInstance().loadTerminalProfiles(key);
      Vector requests  = EquipmentDataBuilder.getInstance().loadTidRequests(key);

      // construct standard query string suffix for equip info detail page
      StringBuffer sb = new StringBuffer();
      if(this.accountDescriptor!=null && this.accountDescriptor.length()>1) 
      {
        sb.append("ad=");
        sb.append(URLEncoder.encode(this.accountDescriptor, StandardCharsets.UTF_8.toString()));
      }
      if(key!=null) 
      {
        if(sb.length()>0)
        {
          sb.append('&');
        }
        sb.append(key.toQueryString());
      }
      this.queryStringSuffix = sb.toString();

      equipmentTable = new AccountEquipmentTable((List)equipment,BASEURL_EQUIPMENTDETAIL,queryStringSuffix);
      profilesTable  = new AccountTerminalProfilesTable((List)vnums,BASEURL_TERMAPPDETAIL,queryStringSuffix);
      requestsTable  = new AccountTidRequestsTable((List)requests,BASEURL_TIDREQUESTDETAIL,null);
    
      // load bindings - associating equipment to term vnums via the equip model
      bindings.clear();
      for(Iterator itre = this.equipmentTable.getRowIterator(); itre.hasNext();) 
      {
        EquipmentDataBase ed = (EquipmentDataBase)itre.next();
        for(Iterator itrp = this.profilesTable.getRowIterator(); itrp.hasNext();) 
        {
          TermAppProfileSummary tp = (TermAppProfileSummary)itrp.next();
          if(ed.getEquipModel().length()>0 && ed.getEquipModel().equals(tp.getEquipModel()))
          {
            bindings.put(ed,tp);
          }
        }
      }
    
      this.key=key;
    
      accountLoaded = true;
    }
  }

  public EquipmentDataBase loadDetailItem(HttpServletRequest request)
  {
    EquipmentDataBase result = null;
    
    load(request);  // in order to have valid bindings
    
    EquipmentKey eKey;
    
    try 
    {
      eKey=KeyBuilder.getInstance().buildEquipmentKey(request);
      result = loadDetailItem(eKey, true);
    }
    catch(Exception e) 
    {
      log.error("loadDetailItem(request) - EXCEPTION: "+e.toString());
    }
    
    return result;
  }

  public EquipmentDataBase loadDetailItem(EquipmentKey eKey, boolean loadHistory)
  {
    if(eKey != null)
    {
      try 
      {
        if(accountLoaded) 
        {
          boolean bFound = false;
          for(Iterator itr=this.equipmentTable.getRowIterator(); itr.hasNext();) 
          {
            EquipmentDataBase ed = (EquipmentDataBase)itr.next();
            if(ed.getKey().equals(eKey)) 
            {
              detailItem=ed;
              bFound=true;
              break;
            }
          }
          if(!bFound) 
          {
            for(Iterator itr=this.profilesTable.getRowIterator(); itr.hasNext();) 
            {
              EquipmentDataBase ed = (EquipmentDataBase)itr.next();
              if(ed.getKey().equals(eKey)) 
              {
                detailItem=ed;
                bFound=true;
                break;
              }
            }
          }
          if(!bFound) 
          {
            for(Iterator itr=this.requestsTable.getRowIterator(); itr.hasNext();) 
            {
              EquipmentDataBase ed = (EquipmentDataBase)itr.next();
              if(ed.getKey().equals(eKey)) 
              {
                detailItem=ed;
                bFound=true;
                break;
              }
            }
          }
          if(!bFound)
          {
            throw new Exception("Equipment of key '"+eKey.toString()+"' is not assocaited with Account: '"+key.toString()+"'.");
          }
        
          // find the associate (if exists)
          EquipmentDataBase associate = getAssociatedEquipment(detailItem);
    
          // construct the detail item associate link
          if(associate==null)
          {
            detailItemAssociateLink = null;
          }
          else 
          {
            StringBuffer sb = new StringBuffer();
            sb.append("<a href=\"");
    
            switch(associate.getType()) 
            {
              // equipment
              case mesConstants.EQUIPTYPE_MERCHEQUIP:
              case mesConstants.EQUIPTYPE_EQUIPINV:
                sb.append(BASEURL_EQUIPMENTDETAIL);
                break;
          
              // term application
              case mesConstants.EQUIPTYPE_TERMAPP:
                sb.append(BASEURL_TERMAPPDETAIL);
                break;
          
              // tid request
              case mesConstants.EQUIPTYPE_TIDREQUEST:
                sb.append(BASEURL_TIDREQUESTDETAIL);
                break;
            }

            String qs = associate.getKey().toQueryString();
            sb.append('?');
            sb.append(qs);
        
            if(this.queryStringSuffix!=null && this.queryStringSuffix.length()>0) 
            {
              sb.append('&');
              sb.append(this.queryStringSuffix);
            }
    
            sb.append("\">");
            sb.append(detailItem.getEquipModel());
            sb.append("</a>");
    
            detailItemAssociateLink = sb.toString();
          }
    
        } 
        else 
        {
          detailItem = EquipmentDataBuilder.getInstance().load(eKey,loadHistory);
        }
      }
      catch(Exception e)
      {
        log.error("loadDetailItem() EXCEPTION: '"+e.toString()+"'.");
        detailItem = null;
      }
    
      this.detailItemLoaded = (detailItem!=null);
    }
    else
    {
      detailItem = null;
    }
    
    return detailItem;
  }
  
  public void unload()
  {
    clear();
  }
  
  //////////////////////
  
  public EquipmentDataBase getDetailItem()
  {
    return detailItem;
  }
  
  public String getDetailItemAssociateLink()
  {
    return this.detailItemAssociateLink;
  }
  
  public boolean hasEquipment()
    throws NotLoadedException
  {
    return (getNumEquipmentItems()>0);
  }
  
  public int getNumEquipmentItems()
    throws NotLoadedException
  {
    if(!accountLoaded)
    {
      throw new NotLoadedException("Get number of equipment items failed: Account equipment not loaded.");
    }
    
    return equipmentTable.getNumRows();
  }
  
  public TableView getEquipmentTable()
  {
    return (TableView)equipmentTable;
  }
  
  public EquipmentDataBase getEquipmentItem(Key key)
    throws NotLoadedException
  {
    if(!accountLoaded)
    {
      throw new NotLoadedException("Get equipment item failed: Account equipment not loaded.");
    }
    
    for(Iterator itr = equipmentTable.getRowIterator(); itr.hasNext();) 
    {
      EquipmentDataBase eq = (EquipmentDataBase)itr.next();
      if(eq.getKey()==key)
      {
        return eq;
      }
    }
    
    return null;
  }
  
  public TermAppProfileSummary getTerminalProfileItem(Key key)
    throws NotLoadedException
  {
    if(!accountLoaded)
    {
      throw new NotLoadedException("Get terminal profile failed: Account equipment not loaded.");
    }
    
    for(Iterator itr = this.profilesTable.getRowIterator(); itr.hasNext();) 
    {
      TermAppProfileSummary tp = (TermAppProfileSummary)itr.next();
      if(tp.getKey()==key)
      {
        return tp;
      }
    }
    
    return null;
  }

  public TidRequest getTidRequest(Key key)
    throws NotLoadedException
  {
    if(!accountLoaded)
    {
      throw new NotLoadedException("Get TID request failed: Account equipment not loaded.");
    }
    
    for(Iterator itr = this.requestsTable.getRowIterator(); itr.hasNext();) 
    {
      TidRequest tr = (TidRequest)itr.next();
      if(tr.getKey()==key)
      {
        return tr;
      }
    }
    
    return null;
  }

  public boolean hasTerminalProfiles()
    throws NotLoadedException
  {
    return (getNumTerminalProfileItems()>0);
  }
  
  public boolean hasTidRequests()
    throws NotLoadedException
  {
    return (getNumTidRequests()>0);
  }
  
  public int getNumTidRequests()
    throws NotLoadedException
  {
    if(!accountLoaded)
    {
      throw new NotLoadedException("Get number of TID requests failed: Account equipment not loaded.");
    }
    
    return requestsTable.getNumRows();
  }
  
  public int getNumTerminalProfileItems()
    throws NotLoadedException
  {
    if(!accountLoaded)
    {
      throw new NotLoadedException("Get number of terminal profile items failed: Account equipment not loaded.");
    }
    
    return profilesTable.getNumRows();
  }
  
  public TableView getTerminalProfilesTable()
  {
    return (TableView)profilesTable;
  }
  
  public TableView getTidRequestsTable()
  {
    return (TableView)requestsTable;
  }
  
  public class NotLoadedException extends Exception
  {
    public NotLoadedException()
    {
      super("AccountEquipmentBean not loaded.");
    }
    public NotLoadedException(String s)
    {
      super(s);
    }
  } // NotLoadedException
  
  public TableView getEquipmentHistoryTable(EquipmentDataBase equipItem)
  {
    if(!(equipItem instanceof EquipmentData_EquipInv))
    {
      return null;
    }
    
    EquipmentData_EquipInv eii = (EquipmentData_EquipInv)equipItem;
    
    TableView tv = new EquipmentHistoryTable(eii.getEquipmentHistory());
    
    return tv;
  }
  
} // AccountEquipmentBean
