package com.mes.equipment;

import java.util.Iterator;
import java.util.List;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.tools.Key;
import com.mes.tools.TableView;
import com.mes.tools.TableViewHelper;


public final class AccountTidRequestsTable
  implements TableView
{
  // create class log category
  static Category log = Category.getInstance(AccountTidRequestsTable.class.getName());
  
  // constants
  private static final String[][] columnData = 
    {
       {"Request Id",             "getRequestId"}
      ,{"VNum",                   "getVnum"}
      ,{"TID",                    "getTid"}
      ,{"Profile Generator",      "getProfGenName"}
      ,{"Prof Cmpltd Date",       "getProfileCompletionDate"}
      ,{"Req Method",             "getProcessMethod"}
      ,{"Req Status",             "getProcessStatus"}
      ,{"Req Response",           "getProcessResponse"}
    };
  
  // data members
  protected List                  list                = null;
  protected TableViewHelper       helper              = null;
  protected String                detailPageBaseUrl   = null;
  protected String                queryStringSuffix   = null;
  
  // construction
  public AccountTidRequestsTable(List list, String detailPageBaseUrl, String queryStringSuffix)
  {
    this.list = list;
    this.helper = new TableViewHelper(TidRequest.class, this, columnData);
    this.detailPageBaseUrl = detailPageBaseUrl;
    this.queryStringSuffix = queryStringSuffix;
  }
  
  public List           getRows()
  {
    return list;
  }
  
  public void           setRows(List list)
  {
    this.list = list;
  }
  
  public int            getNumCols()
  {
    return columnData.length;
  }

  public Key           getRowKey(int rowOrdinal)
  {
    TidRequest taprs = (TidRequest)helper.getRowObject(rowOrdinal);
    return taprs.getKey();
  }

  public Object         getRowObject(int rowOrdinal)
  {
    return helper.getRowObject(rowOrdinal);
  }

  public int            getNumRows()
  {
    return list==null? 0:list.size();
  }
  
  public String         getLinkToDetailColumnName()
  {
    return "Request Id";
  }
  
  public String         getDetailPageBaseUrl()
  {
    return detailPageBaseUrl;
  }

  public String         getQueryStringSuffix()
  {
    return queryStringSuffix;
  }
  
  public void           setQueryStringSuffix(String v)
  {
    queryStringSuffix=v;
  }
  
  public String getDetailPageQueryString(int rowOrdinal)
  {
    StringBuffer sb = new StringBuffer();
    
    try {
    
      TidRequest o = (TidRequest)getRowObject(rowOrdinal);
    
      sb.append("requestId=");
      sb.append(o.getRequestId());
    
    }
    catch(Exception e) {
      log.error("getDetailPageQueryString() EXCEPTION: "+e.toString());
    }
    
    return sb.toString();
  }

  public Iterator       getRowIterator()
  {
    return list==null? null:list.iterator();
  }
  
  public String         getColumnName(int colOrdinal)
  {
    return helper.getColumnName(colOrdinal);
  }
  
  public String[]       getColumnNames()
  {
    return helper.getColumnNames();
  }
  
  public String         getCell(int rowOrdinal, int colOrdinal)
  {
    return helper.getCell(rowOrdinal, colOrdinal);
  }
  
  public String         getHtmlCell(int rowOrdinal, int colOrdinal)
  {
    return helper.getHtmlCell(rowOrdinal, colOrdinal);
  }

}
