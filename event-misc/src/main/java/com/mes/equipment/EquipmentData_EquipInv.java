/*@lineinfo:filename=EquipmentData_EquipInv*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.equipment;

import java.sql.ResultSet;
import java.util.Date;
import java.util.List;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.constants.mesConstants;
import com.mes.tools.AccountKey;
import com.mes.tools.EquipmentKey;
import sqlj.runtime.ResultSetIterator;


/**
 * EquipmentData_EquipInv
 * 
 * MeS owned equipment.
 */
public class EquipmentData_EquipInv extends EquipmentDataBase
{
  // create class log category
  static Category log = Category.getInstance(EquipmentData_EquipInv.class.getName());

  // constants
  // (NONE)
  
  // data members
  protected String      part_number                = "";
  protected String      serial_number              = "";
  protected double      unit_cost                  = 0d;
  protected int         class_code                 = 0;
  protected int         status_code                = 0;
  protected int         equiplendtype_code         = 0;
  protected Date        received_date              = new Date(0L);
  protected Date        deployed_date              = new Date(0L);
  protected String      invoice_number             = "";
  protected String      transaction_id             = "";
  protected String      merchant_number            = "";
  protected String      ei_dba_name                = "";  // the dba name of the merchant_number data member
  protected int         original_class_code        = 0;
  protected double      shipping_cost              = 0d;
  protected double      lrb_price                  = 0d;
  protected int         owner                      = 0;  // i.e. app type
  protected String      owner_name                 = "";
  protected String      status_name                = "";  
  protected String      class_name                 = "";
  protected String      equiplendtype_name         = "";
  protected String      original_class_name        = "";

  protected Vector      equipmentHistory           = new Vector(0,1);
  
  // construction
  public EquipmentData_EquipInv()
  {
    super();
  }
  
  public void clear()
  {
    super.clear();

    part_number                = "";
    serial_number              = "";
    unit_cost                  = 0d;
    class_code                 = 0;
    status_code                = 0;
    equiplendtype_code         = 0;
    received_date.setTime(0L);
    deployed_date.setTime(0L);
    invoice_number             = "";
    transaction_id             = "";
    merchant_number            = "";
    ei_dba_name                = "";
    original_class_code        = 0;
    shipping_cost              = 0d;
    lrb_price                  = 0d;
    owner                      = 0;
    owner_name                 = "";
    status_name                = "";  
    class_name                 = "";
    equiplendtype_name         = "";
    original_class_name        = "";

    if(equipmentHistory!=null)
      equipmentHistory.removeAllElements();
  }
    
  // accessors
  public String   getPartNumber()             { return part_number; }
  public String   getSerialNumber()           { return serial_number; }
  public double   getUnitCost()               { return unit_cost; }
  public int      getClassCode()              { return class_code; }
  public String   getClassName()              { return class_name; }
  public int      getStatusCode()             { return status_code; }
  public String   getStatusName()             { return status_name; }
  public int      getEquipLendTypeCode()      { return equiplendtype_code; }
  public String   getEquipLendTypeName()      { return equiplendtype_name; }
  public Date     getReceivedDate()           { return received_date; }
  public Date     getDeployedDate()           { return deployed_date; }
  public String   getInvoiceNumber()          { return invoice_number; }
  public String   getTransId()                { return transaction_id; }
  public String   getMerchantNumber()         { return merchant_number; }
  public String   getEiDbaName()              { return ei_dba_name; }
  public int      getOriginalClassCode()      { return original_class_code; }
  public String   getOriginalClassName()      { return original_class_name; }
  public double   getShippingCost()           { return shipping_cost; }
  public double   getLrbPrice()               { return lrb_price; }
  public int      getOwner()                  { return owner; }
  public String   getOwnerName()              { return owner_name; }

  // mutators
  public void setEquipmentKey(EquipmentKey v)
  { 
    if(v==null || v.getEquipType()!=mesConstants.EQUIPTYPE_EQUIPINV)
      return;
    
    super.setEquipmentKey(v);

    this.part_number    = v.getPartNum();
    this.serial_number  = v.getSerialNum();

    equipModel = part_number;
  }

  public void    setUnitCost(double v)
  {
    unit_cost = v;
  }
  public void     setClassCode(int v)
  {
    class_code = v;
  }
  public void     setClassName(String v)
  {
    class_name = v;
  }
  public void     setStatusCode(int v)
  {
    status_code = v;
  }
  public void     setStatusName(String v)
  {
    status_name = v;
  }
  public void     setEquipLendTypeCode(int v)
  {
    equiplendtype_code = v;
  }
  public void     setEquipLendTypeName(String v)
  {
    equiplendtype_name = v;
  }
  public void     setReceivedDate(Date v)
  {
    received_date = v;
  }
  public void     setDeployedDate(Date v)
  {
    deployed_date = v;
  }
  public void     setInvoiceNumber(String v)
  {
    invoice_number = v;
  }
  public void     setTransId(String v)
  {
    transaction_id = v;
  }
  public void     setMerchantNumber(String v)
  {
    merchant_number = v;
  }
  public void     setEiDbaName(String v)
  {
    ei_dba_name = v;
  }
  public void     setOriginalClassCode(int v)
  {
    original_class_code = v;
  }
  public void     setOriginalClassName(String v)
  {
    original_class_name = v;
  }
  public void     setShippingCost(double v)
  {
    shipping_cost = v;
  }
  public void     setLrbPrice(double v)
  {
    lrb_price = v;
  }
  public void     setOwner(int v)
  {
    owner = v;
  }
  public void     setOwnerName(String  v)
  {
    owner_name = v;
  }
  
  public boolean    hasEquipmentHistory()
  {
    return equipmentHistory==null? false:!equipmentHistory.isEmpty();
  }
  public List       getEquipmentHistory()
  {
    return (List)equipmentHistory;
  }

  public int getType()
  {
    return mesConstants.EQUIPTYPE_EQUIPINV;
  }

  public String getTypeDesc()
  {
    return "MeS Inventory Equipment";
  }

  public void setEquipmentHistory(Vector v)
  {
    log.debug("setEquipmentHistory() v.size()="+(v==null? 0:v.size()));
    if(v!=null)
      this.equipmentHistory=v;
  }

  public Vector load(AccountKey key)
  {
    Vector                      eq = new Vector(0,1);
    ResultSetIterator           it          = null;
    ResultSet                   rs          = null;
      
    try {

      String merchNum = key.getMerchNum();
      
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:243^7*/

//  ************************************************************
//  #sql [Ctx] it = { select     e.EQUIPMFGR_MFR_CODE            mfr_code
//                    ,e.equiptype_code                equiptype_code
//                    ,e.equip_model                  equip_model
//                    ,e.EQUIP_DESCRIPTOR              descriptor
//                    ,e.EQUIP_PRICE                  price
//                    ,e.EQUIP_RENT                    rent
//                    ,e.EQUIP_USAGE_IND              usage_ind
//                    ,e.TSYS_EQUIP_DESC               tsys_equip_desc
//                    ,em.EQUIPMFGR_MFR_NAME          mfr_name
//                    ,et.EQUIPTYPE_DESCRIPTION        equiptype_name      
//                    ,elt.EQUIPLENDTYPE_DESCRIPTION  disposition
//                    ,ei.EI_PART_NUMBER              part_number
//                    ,ei.EI_SERIAL_NUMBER            serial_number
//                    ,ei.EI_UNIT_COST                unit_cost
//                    ,ei.EI_CLASS                    class_code
//                    ,ei.EI_STATUS                    status_code
//                    ,ei.EI_LRB                       equiplendtype_code
//                    ,ei.EI_RECEIVED_DATE            received_date
//                    ,ei.EI_DEPLOYED_DATE            deployed_date
//                    ,ei.EI_INVOICE_NUMBER            invoice_number
//                    ,ei.EI_TRANSACTION_ID            transaction_id
//                    ,ei.EI_MERCHANT_NUMBER          merchant_number
//                    ,m.merch_business_name          ei_dba_name
//                    ,ei.EI_ORIGINAL_CLASS            original_class_code
//                    ,ei.EI_SHIPPING_COST            shipping_cost
//                    ,ei.EI_LRB_PRICE                lrb_price
//                    ,ei.EI_OWNER                    owner
//                    ,at.app_description             owner_name
//                    ,es.EQUIP_STATUS_DESC            status_name
//                    ,ec.EC_CLASS_NAME                class_name
//                    ,elt.EQUIPLENDTYPE_DESCRIPTION  equiplendtype_name
//                    ,ec2.ec_class_name              original_class_name
//          from       equipment                      e
//                    ,equipmfgr                      em
//                    ,equiptype                      et
//                    ,equip_inventory                ei
//                    ,equiplendtype                  elt
//                    ,equip_status                    es
//                    ,equip_class                    ec
//                    ,equip_class                    ec2
//                    ,app_type                       at
//                    ,merchant                       m
//          where     e.EQUIPMFGR_MFR_CODE = em.EQUIPMFGR_MFR_CODE
//                    and e.equiptype_code = et.equiptype_code
//                    and ei.ei_part_number = e.equip_model(+)
//                    and ei.ei_merchant_number = :merchNum
//                    and ei.ei_lrb = elt.EQUIPLENDTYPE_CODE
//                    and es.EQUIP_STATUS_ID = ei.EI_STATUS
//                    and  ec.EC_CLASS_ID = ei.EI_CLASS
//                    and ec2.ec_class_id = ei.ei_original_class
//                    and at.app_type_code(+) = ei.ei_owner
//                    and ei.ei_merchant_number = m.merch_number(+)
//          order by  equip_model
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select     e.EQUIPMFGR_MFR_CODE            mfr_code\n                  ,e.equiptype_code                equiptype_code\n                  ,e.equip_model                  equip_model\n                  ,e.EQUIP_DESCRIPTOR              descriptor\n                  ,e.EQUIP_PRICE                  price\n                  ,e.EQUIP_RENT                    rent\n                  ,e.EQUIP_USAGE_IND              usage_ind\n                  ,e.TSYS_EQUIP_DESC               tsys_equip_desc\n                  ,em.EQUIPMFGR_MFR_NAME          mfr_name\n                  ,et.EQUIPTYPE_DESCRIPTION        equiptype_name      \n                  ,elt.EQUIPLENDTYPE_DESCRIPTION  disposition\n                  ,ei.EI_PART_NUMBER              part_number\n                  ,ei.EI_SERIAL_NUMBER            serial_number\n                  ,ei.EI_UNIT_COST                unit_cost\n                  ,ei.EI_CLASS                    class_code\n                  ,ei.EI_STATUS                    status_code\n                  ,ei.EI_LRB                       equiplendtype_code\n                  ,ei.EI_RECEIVED_DATE            received_date\n                  ,ei.EI_DEPLOYED_DATE            deployed_date\n                  ,ei.EI_INVOICE_NUMBER            invoice_number\n                  ,ei.EI_TRANSACTION_ID            transaction_id\n                  ,ei.EI_MERCHANT_NUMBER          merchant_number\n                  ,m.merch_business_name          ei_dba_name\n                  ,ei.EI_ORIGINAL_CLASS            original_class_code\n                  ,ei.EI_SHIPPING_COST            shipping_cost\n                  ,ei.EI_LRB_PRICE                lrb_price\n                  ,ei.EI_OWNER                    owner\n                  ,at.app_description             owner_name\n                  ,es.EQUIP_STATUS_DESC            status_name\n                  ,ec.EC_CLASS_NAME                class_name\n                  ,elt.EQUIPLENDTYPE_DESCRIPTION  equiplendtype_name\n                  ,ec2.ec_class_name              original_class_name\n        from       equipment                      e\n                  ,equipmfgr                      em\n                  ,equiptype                      et\n                  ,equip_inventory                ei\n                  ,equiplendtype                  elt\n                  ,equip_status                    es\n                  ,equip_class                    ec\n                  ,equip_class                    ec2\n                  ,app_type                       at\n                  ,merchant                       m\n        where     e.EQUIPMFGR_MFR_CODE = em.EQUIPMFGR_MFR_CODE\n                  and e.equiptype_code = et.equiptype_code\n                  and ei.ei_part_number = e.equip_model(+)\n                  and ei.ei_merchant_number =  :1 \n                  and ei.ei_lrb = elt.EQUIPLENDTYPE_CODE\n                  and es.EQUIP_STATUS_ID = ei.EI_STATUS\n                  and  ec.EC_CLASS_ID = ei.EI_CLASS\n                  and ec2.ec_class_id = ei.ei_original_class\n                  and at.app_type_code(+) = ei.ei_owner\n                  and ei.ei_merchant_number = m.merch_number(+)\n        order by  equip_model";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.equipment.EquipmentData_EquipInv",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.equipment.EquipmentData_EquipInv",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:298^7*/
      rs = it.getResultSet();

      while(rs.next()) {
        
        EquipmentData_EquipInv aed = new EquipmentData_EquipInv();

        aed.load(rs);
          
        aed.setEquipmentKey(new EquipmentKey(aed.part_number,aed.serial_number));
        
        eq.add(aed);
      }
        
      rs.close();
      it.close();
    }
    catch(Exception e) {
      logEntry("load(AccountKey)", e.toString());
    }
    finally {
      try{ rs.close(); } catch( Exception e ) { }
      try{ it.close(); } catch( Exception e ) { }
      cleanUp();
    }
      
    return eq;
  }

  public boolean load(EquipmentKey key)
  {
  
    if(key==null || key.getEquipType()!=mesConstants.EQUIPTYPE_EQUIPINV || !key.isSet())
      return false;

    ResultSetIterator           it          = null;
    ResultSet                   rs          = null;
      
    try {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:340^7*/

//  ************************************************************
//  #sql [Ctx] it = { select     e.EQUIPMFGR_MFR_CODE            mfr_code
//                    ,e.equiptype_code                equiptype_code
//                    ,e.equip_model                  equip_model
//                    ,e.EQUIP_DESCRIPTOR              descriptor
//                    ,e.EQUIP_PRICE                  price
//                    ,e.EQUIP_RENT                    rent
//                    ,e.EQUIP_USAGE_IND              usage_ind
//                    ,e.TSYS_EQUIP_DESC               tsys_equip_desc
//                    ,em.EQUIPMFGR_MFR_NAME          mfr_name
//                    ,et.EQUIPTYPE_DESCRIPTION        equiptype_name      
//                    ,elt.EQUIPLENDTYPE_DESCRIPTION  disposition
//  
//                    ,ei.EI_PART_NUMBER              part_number
//                    ,ei.EI_SERIAL_NUMBER            serial_number
//                    ,ei.EI_UNIT_COST                unit_cost
//                    ,ei.EI_CLASS                    class_code
//                    ,ei.EI_STATUS                    status_code
//                    ,ei.EI_LRB                       equiplendtype_code
//                    ,ei.EI_RECEIVED_DATE            received_date
//                    ,ei.EI_DEPLOYED_DATE            deployed_date
//                    ,ei.EI_INVOICE_NUMBER            invoice_number
//                    ,ei.EI_TRANSACTION_ID            transaction_id
//                    ,ei.EI_MERCHANT_NUMBER          merchant_number
//                    ,m.merch_business_name          ei_dba_name
//                    ,ei.EI_ORIGINAL_CLASS            original_class_code
//                    ,ei.EI_SHIPPING_COST            shipping_cost
//                    ,ei.EI_LRB_PRICE                lrb_price
//                    ,ei.EI_OWNER                    owner
//                    ,es.EQUIP_STATUS_DESC            status_name
//                    ,ec.EC_CLASS_NAME                class_name
//                    ,elt.EQUIPLENDTYPE_DESCRIPTION  equiplendtype_name
//                    ,ec2.ec_class_name              original_class_name
//                    ,at.app_description             owner_name
//  
//          from       equipment                      e
//                    ,equipmfgr                      em
//                    ,equiptype                      et
//                    ,equip_inventory                ei
//                    ,equiplendtype                  elt
//                    ,equip_status                    es
//                    ,equip_class                    ec
//                    ,equip_class                    ec2
//                    ,merchant                       m
//                    ,app_type                       at
//  
//          where     e.EQUIPMFGR_MFR_CODE = em.EQUIPMFGR_MFR_CODE
//                    and e.equiptype_code = et.equiptype_code
//                    and ei.ei_part_number = e.equip_model(+)
//                    and ei.ei_part_number = :key.getEquipInvKey().getPartNum()
//                    and ei.ei_serial_number = :key.getEquipInvKey().getSerialNum()
//                    and ei.ei_lrb = elt.EQUIPLENDTYPE_CODE
//                    and es.EQUIP_STATUS_ID = ei.EI_STATUS
//                    and  ec.EC_CLASS_ID = ei.EI_CLASS
//                    and ec2.ec_class_id(+) = ei.ei_original_class
//                    and at.app_type_code(+) = ei.ei_owner
//                    and ei.ei_merchant_number = m.merch_number(+)
//  
//          order by  equip_model
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_419 = key.getEquipInvKey().getPartNum();
 String __sJT_420 = key.getEquipInvKey().getSerialNum();
  try {
   String theSqlTS = "select     e.EQUIPMFGR_MFR_CODE            mfr_code\n                  ,e.equiptype_code                equiptype_code\n                  ,e.equip_model                  equip_model\n                  ,e.EQUIP_DESCRIPTOR              descriptor\n                  ,e.EQUIP_PRICE                  price\n                  ,e.EQUIP_RENT                    rent\n                  ,e.EQUIP_USAGE_IND              usage_ind\n                  ,e.TSYS_EQUIP_DESC               tsys_equip_desc\n                  ,em.EQUIPMFGR_MFR_NAME          mfr_name\n                  ,et.EQUIPTYPE_DESCRIPTION        equiptype_name      \n                  ,elt.EQUIPLENDTYPE_DESCRIPTION  disposition\n\n                  ,ei.EI_PART_NUMBER              part_number\n                  ,ei.EI_SERIAL_NUMBER            serial_number\n                  ,ei.EI_UNIT_COST                unit_cost\n                  ,ei.EI_CLASS                    class_code\n                  ,ei.EI_STATUS                    status_code\n                  ,ei.EI_LRB                       equiplendtype_code\n                  ,ei.EI_RECEIVED_DATE            received_date\n                  ,ei.EI_DEPLOYED_DATE            deployed_date\n                  ,ei.EI_INVOICE_NUMBER            invoice_number\n                  ,ei.EI_TRANSACTION_ID            transaction_id\n                  ,ei.EI_MERCHANT_NUMBER          merchant_number\n                  ,m.merch_business_name          ei_dba_name\n                  ,ei.EI_ORIGINAL_CLASS            original_class_code\n                  ,ei.EI_SHIPPING_COST            shipping_cost\n                  ,ei.EI_LRB_PRICE                lrb_price\n                  ,ei.EI_OWNER                    owner\n                  ,es.EQUIP_STATUS_DESC            status_name\n                  ,ec.EC_CLASS_NAME                class_name\n                  ,elt.EQUIPLENDTYPE_DESCRIPTION  equiplendtype_name\n                  ,ec2.ec_class_name              original_class_name\n                  ,at.app_description             owner_name\n\n        from       equipment                      e\n                  ,equipmfgr                      em\n                  ,equiptype                      et\n                  ,equip_inventory                ei\n                  ,equiplendtype                  elt\n                  ,equip_status                    es\n                  ,equip_class                    ec\n                  ,equip_class                    ec2\n                  ,merchant                       m\n                  ,app_type                       at\n\n        where     e.EQUIPMFGR_MFR_CODE = em.EQUIPMFGR_MFR_CODE\n                  and e.equiptype_code = et.equiptype_code\n                  and ei.ei_part_number = e.equip_model(+)\n                  and ei.ei_part_number =  :1 \n                  and ei.ei_serial_number =  :2 \n                  and ei.ei_lrb = elt.EQUIPLENDTYPE_CODE\n                  and es.EQUIP_STATUS_ID = ei.EI_STATUS\n                  and  ec.EC_CLASS_ID = ei.EI_CLASS\n                  and ec2.ec_class_id(+) = ei.ei_original_class\n                  and at.app_type_code(+) = ei.ei_owner\n                  and ei.ei_merchant_number = m.merch_number(+)\n\n        order by  equip_model";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.equipment.EquipmentData_EquipInv",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_419);
   __sJT_st.setString(2,__sJT_420);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.equipment.EquipmentData_EquipInv",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:400^7*/
      rs = it.getResultSet();

      if(rs.next()) {
        
        this.key = key;

        this.load(rs);
        
      } else
        return false;
        
    }
    catch(Exception e) {
      logEntry("load(EquipmentKey)", e.toString());
      return false;
    }
    finally {
      try{ rs.close(); } catch( Exception e ) { }
      try{ it.close(); } catch( Exception e ) { }
      cleanUp();
    }
      
    return true;
  }

  public boolean load(ResultSet rs)
  {
    boolean rval = false;
    
    try {
      
      disposition                         = rs.getString("disposition");
      equipMfrCode                        = rs.getInt("mfr_code");
      equipTypeCode                       = rs.getInt("equiptype_code");
      equipModel                          = rs.getString("equip_model");
      descriptor                          = rs.getString("descriptor");
      price                               = rs.getDouble("price");
      rent                                = rs.getDouble("rent");
      usageInd                            = rs.getString("usage_ind");
      tsysEquipDesc                       = rs.getString("tsys_equip_desc");
      equipMfrName                        = rs.getString("mfr_name");
      equipTypeName                       = rs.getString("equiptype_name");
      
      part_number                         = rs.getString("part_number");
      serial_number                       = rs.getString("serial_number");
      unit_cost                           = rs.getDouble("unit_cost");
      class_code                          = rs.getInt("class_code");
      status_code                         = rs.getInt("status_code");
      equiplendtype_code                  = rs.getInt("equiplendtype_code");
      received_date.setTime(rs.getString("received_date")==null? 0L:rs.getTimestamp("received_date").getTime());
      deployed_date.setTime(rs.getString("deployed_date")==null? 0L:rs.getTimestamp("deployed_date").getTime());
      invoice_number                      = rs.getString("invoice_number");
      transaction_id                      = rs.getString("transaction_id");
      merchant_number                     = rs.getString("merchant_number");
      ei_dba_name                         = rs.getString("ei_dba_name");
      original_class_code                 = rs.getInt("original_class_code");
      shipping_cost                       = rs.getDouble("shipping_cost");
      lrb_price                           = rs.getDouble("lrb_price");
      owner                               = rs.getInt("owner");
      owner_name                          = rs.getString("owner_name");
      status_name                         = rs.getString("status_name");
      class_name                          = rs.getString("class_name");
      equiplendtype_name                  = rs.getString("equiplendtype_name");
      original_class_name                 = rs.getString("original_class_name");

      rval = true;
    }
    catch(Exception e) {
      log.error("load(ResultSet) EXCEPTION: "+e.toString());
    }

    return rval;
  }

}/*@lineinfo:generated-code*/