package com.mes.equipment;

import java.util.Iterator;
import java.util.List;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.tools.Key;
import com.mes.tools.TableView;
import com.mes.tools.TableViewHelper;


public final class EquipmentHistoryTable
  implements TableView
{
  // create class log category
  static Category log = Category.getInstance(EquipmentHistoryTable.class.getName());

  // constants
  private static final String[][] columnData = 
    {
       {"Date",            "getDate"}
      ,{"User",            "getUser"}
      ,{"Action",          "getActionPerformed"}
      ,{"Equip Status",    "getEquipStatusName"}
      ,{"Note",            "getNote"}
    };
  
  private static final String   USERDETAIL_BASEURL  = "/jsp/credit/contact_info.jsp";
  private static final String   DATE_FORMAT         = "MM/dd/yyyy";
  
  // data members
  protected List              list                = null;
  protected TableViewHelper   helper              = null;
  protected String            detailPageBaseUrl   = null;
  protected String            queryStringSuffix   = null;
  
  // construction
  public EquipmentHistoryTable(List list)
  {
    this(list,null,null);
  }
  public EquipmentHistoryTable(List list, String detailPageBaseUrl, String queryStringSuffix)
  {
    this.list = list;
    this.helper = new TableViewHelper(EquipmentHistory.class, this, columnData);
    helper.setDateFormat(DATE_FORMAT);
    this.detailPageBaseUrl = detailPageBaseUrl;
    this.queryStringSuffix = queryStringSuffix;
  }
  
  public List           getRows()
  {
    return list;
  }
  
  public void           setRows(List list)
  {
    this.list = list;
  }
  
  public Key            getRowKey(int rowOrdinal)
  {
    EquipmentHistory edb = (EquipmentHistory)helper.getRowObject(rowOrdinal);
    return (Key)edb.getKey();
  }

  public Object         getRowObject(int rowOrdinal)
  {
    return helper.getRowObject(rowOrdinal);
  }

  public int            getNumRows()
  {
    return list==null? 0:list.size();
  }
  
  public int            getNumCols()
  {
    return columnData.length;
  }
  
  public String         getLinkToDetailColumnName()
  {
    return "";
  }
  
  public String         getDetailPageBaseUrl()
  {
    return detailPageBaseUrl;
  }
    
  public String         getQueryStringSuffix()
  {
    return queryStringSuffix;
  }
  
  public void           setQueryStringSuffix(String v)
  {
    queryStringSuffix=v;
  }
  
  public String getDetailPageQueryString(int rowOrdinal)
  {
    return helper.getDetailPageQueryString(rowOrdinal);
  }

  public Iterator       getRowIterator()
  {
    return list==null? null:list.iterator();
  }
  
  public String         getColumnName(int colOrdinal)
  {
    return helper.getColumnName(colOrdinal);
  }
  
  public String[]       getColumnNames()
  {
    return helper.getColumnNames();
  }
  
  public String         getCell(int rowOrdinal, int colOrdinal)
  {
    return helper.getCell(rowOrdinal, colOrdinal);
  }
  
  public String         getHtmlCell(int rowOrdinal, int colOrdinal)
  {
    try {
      if(columnData[colOrdinal-1][0].equals("User")) {
        String data = getCell(rowOrdinal,colOrdinal);
        StringBuffer sb = new StringBuffer();
        sb.append("<a href=\"");
        sb.append(USERDETAIL_BASEURL);
        sb.append("?userName=");
        sb.append(data);
        sb.append("\">");
        sb.append(data);
        sb.append("</a>");
        return sb.toString();
      }
    }
    catch(Exception e) {
      log.error("getHtmlCell() EXCEPTION: '"+e.toString()+"'");
    }
    
    return helper.getHtmlCell(rowOrdinal, colOrdinal);
  }

}
