/*@lineinfo:filename=EquipmentHistory*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.equipment;

import java.sql.ResultSet;
import java.util.Date;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.tools.EquipmentKey;
import sqlj.runtime.ResultSetIterator;


public class EquipmentHistory extends SQLJConnectionBase
{
  // create class log category
  static Category log = Category.getInstance(EquipmentHistory.class.getName());

  // constants
  // (NONE)
  
  
  // data members
  protected EquipmentKey  key                       = null;
  protected Date          date                      = new Date(0L);
  protected long          userId                    = 0L; // i.e. mesdb.userIds.user_id
  protected String        user                      = ""; // i.e. login_name
  protected int           sourceCode                = 0;
  protected String        note                      = "";
  protected String        actionPerformed           = "";
  protected int           equipStatusCode           = 0;
  protected String        equipStatusName           = "";
  protected int           owner                     = 0;
  
  
  // class methods
  public static Vector load(EquipmentKey key)
  {
    EquipmentHistory eh = new EquipmentHistory();
    return eh._load(key);
  }

  
  // object methods
  
  // construction
  public EquipmentHistory()
  {
  }
  
  public void clear()
  {
    key                       = null;
    date.setTime(0L);
    userId                    = 0L;
    user                      = "";
    sourceCode                = 0;
    note                      = "";
    actionPerformed           = "";
    equipStatusCode           = 0;
    equipStatusName           = "";
    owner                     = 0;
  }
  
  // accessors
  public EquipmentKey     getKey()                      { return key; }
  public Date             getDate()                     { return date; }
  public long             getUserId()                   { return userId; }
  public String           getUser()                     { return user; }
  public int              getSourceCode()               { return sourceCode; }
  public String           getNote()                     { return note; }
  public String           getActionPerformed()          { return actionPerformed; }
  public int              getEquipStatusCode()          { return equipStatusCode; }
  public String           getEquipStatusName()          { return equipStatusName; }
  public int              getOwner()                    { return owner; }

  public String           getPartNum()
  {
    return key.getPartNum();
  }
  
  public String           getSerialNum()
  {
    return key.getSerialNum();
  }
  
  // mutators
  public void setEquipmentKey(EquipmentKey v)
  {
    if(v==null || v.equals(this.key))
      return;

    this.key = v;
  }
  
  public void setDate(Date v)
  {
    this.date = v;
  }
  
  public void setUser(String v)
  {
    this.user = v;
  }
  
  public void setUserId(long v)
  {
    this.userId = v;
  }
  
  public void setSourceCode(int v)
  {
    this.sourceCode = v;
  }
  
  public void setNote(String v)
  {
    this.note = v;
  }
  
  public void setActionPerformed(String v)
  {
    this.actionPerformed = v;
  }
  
  public void setEquipStatusCode(int v)
  {
    this.equipStatusCode = v;
  }
  
  public void setEquipStatusName(String v)
  {
    this.equipStatusName = v;
  }

  public void setOwner(int v)
  {
    this.owner = v;
  }

  ///
  
  public boolean equals(Object obj)
  {
    if(!(obj instanceof EquipmentHistory))
      return false;
    
    if(key==null)
      return false;
    
    return key.equals(((EquipmentHistory)obj).getKey());
  }

  private Vector _load(EquipmentKey key)
  {
    if(key==null || key.getEquipType()!=mesConstants.EQUIPTYPE_EQUIPINV || !key.isSet())
      return null;

    Vector rval = new Vector(0,1);

    ResultSetIterator           it          = null;
    ResultSet                   rs          = null;
      
    try {

      String partNum    = key.getEquipInvKey().getPartNum();
      String serialNum  = key.getEquipInvKey().getSerialNum();

      //log.debug("load() partNum="+partNum+", serialNum="+serialNum);
        
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:175^7*/

//  ************************************************************
//  #sql [Ctx] it = { select		 eh.*
//                     ,es.equip_status_desc
//                     ,u.login_name
//          from       equip_history eh
//                     ,equip_status es
//                     ,users        u
//  
//          where     eh.part_number = :partNum
//                    and eh.eh_serial_number = :serialNum
//                    and eh.equip_status = es.equip_status_id(+)
//                    and eh.eh_user=u.user_id(+)
//          
//          order by  eh.eh_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\t\t eh.*\n                   ,es.equip_status_desc\n                   ,u.login_name\n        from       equip_history eh\n                   ,equip_status es\n                   ,users        u\n\n        where     eh.part_number =  :1 \n                  and eh.eh_serial_number =  :2 \n                  and eh.equip_status = es.equip_status_id(+)\n                  and eh.eh_user=u.user_id(+)\n        \n        order by  eh.eh_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.equipment.EquipmentHistory",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,partNum);
   __sJT_st.setString(2,serialNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.equipment.EquipmentHistory",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:190^7*/
      rs = it.getResultSet();

      while(rs.next()) {
        
        EquipmentHistory eh = new EquipmentHistory();
          
        eh.key                       = key;
        eh.date                      = (java.util.Date)rs.getTimestamp("eh_date");
        eh.userId                    = rs.getLong("eh_user");
        eh.user                      = rs.getString("login_name");
        eh.sourceCode                = rs.getInt("eh_source_code");
        eh.note                      = rs.getString("eh_note");
        eh.actionPerformed           = rs.getString("eh_action_performed");
        eh.equipStatusCode           = rs.getInt("equip_status");
        eh.equipStatusName           = rs.getString("equip_status_desc");
        eh.owner                     = rs.getInt("owner");

        rval.add(eh);
      }

    }
    catch(Exception e) {
      log.error("load() EXCEPTION: '"+e.toString()+"'.");
    }
    finally {
      try{ rs.close(); } catch( Exception e ) { }
      try{ it.close(); } catch( Exception e ) { }
      cleanUp();
    }
      
    //log.debug("load() rval.size()="+rval.size());
    return rval;
  }
  

}/*@lineinfo:generated-code*/