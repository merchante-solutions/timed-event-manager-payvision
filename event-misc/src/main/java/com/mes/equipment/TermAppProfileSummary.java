/*@lineinfo:filename=TermAppProfileSummary*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.equipment;

import java.sql.ResultSet;
import java.util.Date;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.tools.AccountKey;
import com.mes.tools.EquipmentKey;
import sqlj.runtime.ResultSetIterator;


/**
 * TermAppProfileSummary
 * 
 * Read-only value object containing summary-based data for a given TID.
 */
public class TermAppProfileSummary extends EquipmentDataBase
{
  public static final int           HOST_VITAL              = 0;
  public static final int           HOST_TRIDENT            = 1;
  
  // data members
  protected String                  vnum                    = "";
  protected String                  tid                     = "";
  protected String                  termApp                 = "";
  protected String                  terminalNumber          = "";
  protected String                  storeNumber             = "";
  protected String                  locationNumber          = "";
  protected String                  timeZone                = "";

  protected long                    requestId               = 0L;
  protected String                  processMethod           = "";
  protected String                  processStatus           = "";
  protected String                  processResponse         = "";
  protected Date                    processStartDate        = new Date(0L);
  protected Date                    processEndDate          = new Date(0L);
  protected String                  mmsTermApp              = "";
  protected Date                    profileCompletedDate    = new Date(0L);
  protected int                     profGenCode             = mesConstants.PROFGEN_UNDEFINED;
  protected String                  profGenName             = "";
  protected int                     frontEnd                = HOST_VITAL;
  protected String                  frontEndDescription     = "";
  
  protected String[] frontEndDescriptions =
  {
    "Vital",
    "Trident"
  };
  
  
  // class methods
  // (NONE)
  
  
  // object methods
  
  // construction
  public TermAppProfileSummary()
  {
  }
  
  public void clear()
  {
    super.clear();

    vnum                    = "";
    tid                     = "";
    termApp                 = "";
    terminalNumber          = "";
    storeNumber             = "";
    locationNumber          = "";
    timeZone                = "";
    requestId               = 0L;
    processMethod           = "";
    processStatus           = "";
    processResponse         = "";
    processStartDate.setTime(0L);
    processEndDate.setTime(0L);
    mmsTermApp              = "";
    profileCompletedDate.setTime(0L);
    profGenCode             = mesConstants.PROFGEN_UNDEFINED;
    profGenName             = "";
  }
  
  
  // ******* accessors ***********

  public boolean hasRequest()
  {
    return (requestId > 0L);
  }
  
  public String getVnum()
  {
    return vnum;
  }
  
  public String getTid()
  {
    return tid;
  }
  
  public String getTermApp()
  {
    return termApp;
  }
  
  public String getTermAppDescriptor()
  {
    StringBuffer sb = new StringBuffer();

    if(profGenCode == mesConstants.PROFGEN_MMS) 
    {
      sb.append(termApp);
    } 
    else 
    {
      sb.append("MMS: '");
      sb.append(mmsTermApp);
      sb.append("' - ");
      sb.append(profGenName);
      sb.append(": '");
      sb.append(termApp);
      sb.append("'");
    }

    return sb.toString();
  }

  public String getTerminalNumber()
  {
    return terminalNumber;
  }
  
  public String getStoreNumber()
  {
    return storeNumber;
  }
  
  public String getLocationNumber()
  {
    return locationNumber;
  }
  
  public String getTimeZone()
  {
    return timeZone;
  }
  
  public long getRequestId()
  {
    return requestId;
  }
  
  public String getProcessMethod()
  {
    return processMethod;
  }
  
  public String getProcessStatus()
  {
    return processStatus;
  }
  
  public String getProcessResponse()
  {
    return processResponse;
  }
  
  public Date getProcessStartDate()
  {
    return processStartDate;
  }
  
  public Date getProcessEndDate()
  {
    return processEndDate;
  }
  
  public String getMmsTermApp()
  {
    return mmsTermApp;
  }
  
  public Date getProfileCompletionDate()
  {
    return profileCompletedDate;
  }
  
  public int getProfGenCode()
  {
    return profGenCode;
  }
  
  public String getProfGenName()
  {
    return profGenName;
  }
  
  
  public int getType()
  {
    return mesConstants.EQUIPTYPE_TERMAPP;
  }

  public String getTypeDesc()
  {
    return "Terminal Application Profile Summary";
  }

  // ********* mutators **********
  // (NONE)


  // *********** load routine *******
  
  /**
   * load(AccountKey)
   *
   * Loads all found vnum/tids for a given account.
   * If an Mms Request is found associated with a given vnum/tid, that information
   *  is loaded as well.  But only on the basis of the vnumber linking to an Mms Request.
   */
  public Vector load(AccountKey accountKey)
  {
    Vector                      rval        = new Vector(0,1);
    ResultSetIterator           it          = null;
    ResultSet                   rs          = null;

    try
    {
      connect();
      
      long appSeqNum = accountKey.getAppSeqNum();
    
      TermAppProfileSummary o = null;
    
      // find all associated terminal profile requests
      /*@lineinfo:generated-code*//*@lineinfo:240^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mv.vnumber                          vnumber,
//                  mv.tid                              tid,
//                  mv.profile_completed_date           profile_completed_date,
//                  nvl(mv.profile_generator_code,
//                    :mesConstants.DEFAULT_PROFGEN)  profile_generator_code,
//                  mv.app_code                         app_code,
//                  mv.equip_model                      equip_model,
//                  mv.terminal_number                  terminal_number,
//                  mv.store_number                     store_number,
//                  mv.location_number                  location_number,
//                  mv.request_id                       request_id,
//                  msi.process_method                  process_method,
//                  msi.process_status                  process_status,
//                  msi.process_response                process_response,
//                  msi.process_start_date              process_start_date,
//                  msi.process_end_date                process_end_date,
//                  nvl(msi.term_application,'STAGE')   mms_app_code,
//                  epg.name                            profile_generator_name,
//                  tz.description                      time_zone,
//                  nvl(mv.front_end, 0)                front_end
//          from    merch_vnumber                       mv,
//                  mms_stage_info                      msi,
//                  equip_profile_generators            epg,
//                  time_zones                          tz
//          where   mv.app_seq_num = :appSeqNum
//                  and msi.vnum(+) = mv.vnumber
//                  and epg.code(+) = nvl(mv.profile_generator_code,:mesConstants.DEFAULT_PROFGEN)
//                  and tz.code(+) = mv.time_zone
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mv.vnumber                          vnumber,\n                mv.tid                              tid,\n                mv.profile_completed_date           profile_completed_date,\n                nvl(mv.profile_generator_code,\n                   :1 )  profile_generator_code,\n                mv.app_code                         app_code,\n                mv.equip_model                      equip_model,\n                mv.terminal_number                  terminal_number,\n                mv.store_number                     store_number,\n                mv.location_number                  location_number,\n                mv.request_id                       request_id,\n                msi.process_method                  process_method,\n                msi.process_status                  process_status,\n                msi.process_response                process_response,\n                msi.process_start_date              process_start_date,\n                msi.process_end_date                process_end_date,\n                nvl(msi.term_application,'STAGE')   mms_app_code,\n                epg.name                            profile_generator_name,\n                tz.description                      time_zone,\n                nvl(mv.front_end, 0)                front_end\n        from    merch_vnumber                       mv,\n                mms_stage_info                      msi,\n                equip_profile_generators            epg,\n                time_zones                          tz\n        where   mv.app_seq_num =  :2 \n                and msi.vnum(+) = mv.vnumber\n                and epg.code(+) = nvl(mv.profile_generator_code, :3 )\n                and tz.code(+) = mv.time_zone";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.equipment.TermAppProfileSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.DEFAULT_PROFGEN);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,mesConstants.DEFAULT_PROFGEN);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.equipment.TermAppProfileSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:270^7*/

      rs=it.getResultSet();

      while(rs.next())
      {
        o = new TermAppProfileSummary();

        o.vnum                    = rs.getString("vnumber");
        o.tid                     = rs.getString("tid");
        o.termApp                 = rs.getString("app_code");
        o.equipModel              = rs.getString("equip_model");
        o.terminalNumber          = rs.getString("terminal_number");
        o.storeNumber             = rs.getString("store_number");
        o.locationNumber          = rs.getString("location_number");
        o.timeZone                = rs.getString("time_zone");

        o.requestId               = rs.getString("request_id")==null? 0L:rs.getLong("request_id");
        o.processMethod           = rs.getString("process_method");
        o.processStatus           = rs.getString("process_status");
        o.processResponse         = rs.getString("process_response");
        o.processStartDate.setTime(rs.getTimestamp("process_start_date")==null? 0L:rs.getTimestamp("process_start_date").getTime());
        o.processEndDate.setTime(rs.getTimestamp("process_end_date")==null? 0L:rs.getTimestamp("process_end_date").getTime());
        o.mmsTermApp              = rs.getString("mms_app_code");
        o.profileCompletedDate.setTime(rs.getTimestamp("PROFILE_COMPLETED_DATE")==null? 0L:rs.getTimestamp("PROFILE_COMPLETED_DATE").getTime());
        o.profGenCode             = rs.getInt("profile_generator_code");
        o.profGenName             = rs.getString("profile_generator_name");
        o.frontEnd                = rs.getInt("front_end");
        o.frontEndDescription     = frontEndDescriptions[frontEnd];

        o.descriptor = o.vnum;

        o.setEquipmentKey(new EquipmentKey(o.vnum));

        rval.addElement(o);
      }
    }
    catch(Exception e)
    {
      logEntry("load(AccountKey)", e.toString());
    }
    finally
    {
      try { rs.close(); } catch( Exception e ) { }
      try { it.close(); } catch( Exception e ) { }
      cleanUp();
    }

    return rval;
  }

  /**
   * load(EquipmentKey)
   *
   * Loads single term app profile summary having specified vnumber.
   */
  public boolean load(EquipmentKey key)
  {
    boolean                     result      = false;
    ResultSetIterator           it          = null;
    ResultSet                   rs          = null;
    
    try 
    {
      connect();
      
      if(key != null && key.getEquipType()==mesConstants.EQUIPTYPE_TERMAPP && key.isSet())
      {
        /*@lineinfo:generated-code*//*@lineinfo:338^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mv.vnumber                          vnumber,
//                    mv.tid                              tid,
//                    mv.profile_completed_date           profile_completed_date,
//                    nvl(mv.profile_generator_code,
//                      :mesConstants.DEFAULT_PROFGEN)  profile_generator_code,
//                    mv.app_code                         app_code,
//                    mv.equip_model                      equip_model,
//                    mv.terminal_number                  terminal_number,
//                    mv.store_number                     store_number,
//                    mv.location_number                  location_number,
//                    mv.request_id                       request_id,
//                    msi.process_method                  process_method,
//                    msi.process_status                  process_status,
//                    msi.process_response                process_cxresponse,
//                    msi.process_start_date              process_start_date,
//                    msi.process_end_date                process_end_date,
//                    nvl(msi.term_application,'STAGE')   mms_app_code,
//                    epg.name                            profile_generator_name,
//                    tz.description                      time_zone,
//                    mv.front_end                        front_end
//            from    merch_vnumber                       mv,
//                    mms_stage_info                      msi,
//                    equip_profile_generators            epg,
//                    time_zones                          tz
//            where   mv.vnumber = :key.getTermAppProfileSummaryKey().vnum
//                    and msi.app_seq_num(+) = mv.app_seq_num
//                    and msi.vnum is not null
//                    and epg.code(+) = nvl(mv.PROFILE_GENERATOR_CODE,:mesConstants.DEFAULT_PROFGEN)
//                    and tz.code(+) = mv.time_zone
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mv.vnumber                          vnumber,\n                  mv.tid                              tid,\n                  mv.profile_completed_date           profile_completed_date,\n                  nvl(mv.profile_generator_code,\n                     :1 )  profile_generator_code,\n                  mv.app_code                         app_code,\n                  mv.equip_model                      equip_model,\n                  mv.terminal_number                  terminal_number,\n                  mv.store_number                     store_number,\n                  mv.location_number                  location_number,\n                  mv.request_id                       request_id,\n                  msi.process_method                  process_method,\n                  msi.process_status                  process_status,\n                  msi.process_response                process_cxresponse,\n                  msi.process_start_date              process_start_date,\n                  msi.process_end_date                process_end_date,\n                  nvl(msi.term_application,'STAGE')   mms_app_code,\n                  epg.name                            profile_generator_name,\n                  tz.description                      time_zone,\n                  mv.front_end                        front_end\n          from    merch_vnumber                       mv,\n                  mms_stage_info                      msi,\n                  equip_profile_generators            epg,\n                  time_zones                          tz\n          where   mv.vnumber =  :2 \n                  and msi.app_seq_num(+) = mv.app_seq_num\n                  and msi.vnum is not null\n                  and epg.code(+) = nvl(mv.PROFILE_GENERATOR_CODE, :3 )\n                  and tz.code(+) = mv.time_zone";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.equipment.TermAppProfileSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.DEFAULT_PROFGEN);
   __sJT_st.setString(2,key.getTermAppProfileSummaryKey().vnum);
   __sJT_st.setInt(3,mesConstants.DEFAULT_PROFGEN);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.equipment.TermAppProfileSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:369^9*/
        
        rs=it.getResultSet();

        if(rs.next()) 
        {
          this.key                = key;

          vnum                    = rs.getString("vnumber");
          tid                     = rs.getString("tid");
          termApp                 = rs.getString("app_code");
          equipModel              = rs.getString("equip_model");
          terminalNumber          = rs.getString("terminal_number");
          storeNumber             = rs.getString("store_number");
          locationNumber          = rs.getString("location_number");
          timeZone                = rs.getString("time_zone");

          requestId               = rs.getString("request_id")==null? 0L:rs.getLong("request_id");
          processMethod           = rs.getString("process_method");
          processStatus           = rs.getString("process_status");
          processResponse         = rs.getString("process_response");
          processStartDate.setTime(rs.getTimestamp("process_start_date")==null? 0L:rs.getTimestamp("process_start_date").getTime());
          processEndDate.setTime(rs.getTimestamp("process_end_date")==null? 0L:rs.getTimestamp("process_end_date").getTime());
          mmsTermApp              = rs.getString("mms_app_code");
          profileCompletedDate.setTime(rs.getTimestamp("PROFILE_COMPLETED_DATE")==null? 0L:rs.getTimestamp("PROFILE_COMPLETED_DATE").getTime());
          profGenCode             = rs.getInt("profile_generator_code");
          profGenName             = rs.getString("profile_generator_name");
          frontEnd                = rs.getInt("front_end");
          frontEndDescription     = frontEndDescriptions[frontEnd];

          descriptor              = vnum;

          result = true;
        }
      }
    }
    catch(Exception e) 
    {
      logEntry("load(EquipmentKey)", e.toString());
      result = false;
    }
    finally 
    {
      try { rs.close(); } catch( Exception e ) { }
      try { it.close(); } catch( Exception e ) { }
      cleanUp();
    }

    return result;
  }

}/*@lineinfo:generated-code*/