package com.mes.equipment.profile;

import java.util.Enumeration;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;


public final class AutoProfileProcessor
{
  // create class log category
  static Category log = Category.getInstance(AutoProfileProcessor.class.getName());

  // Singleton
  public static AutoProfileProcessor getInstance()
  {
    if(_instance==null)
      _instance = new AutoProfileProcessor();
    
    return _instance;
  }
  private static AutoProfileProcessor _instance = null;
  ///
  
  // constants
  // (NONE)
  
  // data members
  private   Vector          processors          = null; // list of AutoProfileRecordBatchProcess implementing objects
  
  // construction
  private AutoProfileProcessor()
  {
    initialize();
  }
  
  private void initialize()
  {
    // generate the processors list
    processors = new Vector(1,1);
    // Unfinished code in TermMasterBatchProcess
    //processors.add(new TermMasterBatchProcess());
  }
  
  public void process()
  {
    for(Enumeration e=processors.elements(); e.hasMoreElements();) {
      AutoProfileRecordBatchProcess apProcess = (AutoProfileRecordBatchProcess)e.nextElement();
      String processName = apProcess.getName();
      
      // post
      log.info("Generating outgoing "+processName+" batches...");
      try {
        apProcess.postBatches();
      }
      catch(Exception ex) {
        log.error("Post Batches EXCEPTION ["+processName+"]: "+ex.toString());
      }
      
      // pend
      log.info("Parsing incoming "+processName+" batches responses...");
      try {
        apProcess.pendBatches();
      }
      catch(Exception ex) {
        log.error("Parse Batches EXCEPTION ["+processName+"]: "+ex.toString());
      }
      
    }
  }
  
} // AutoProfileProcessor
