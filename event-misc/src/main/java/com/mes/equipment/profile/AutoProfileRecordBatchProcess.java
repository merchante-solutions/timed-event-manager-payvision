package com.mes.equipment.profile;


/**
 * AutoProfileRecordBatchProcess interface
 **/
public interface AutoProfileRecordBatchProcess
{
  public String             getName();
  public void               postBatches()      throws AutoProfileRecordBatchException;
  public void               pendBatches()      throws AutoProfileRecordBatchException;
  
  public class AutoProfileRecordBatchException extends Exception
  {
    public AutoProfileRecordBatchException()
    {
      super("An auto-profile record batch exception occurred.");
    }
    public AutoProfileRecordBatchException(String msg)
    {
      super(msg);
    }
  } // AutoProfileRecordBatchException
  
  public class NoAutoProfilesException extends AutoProfileRecordBatchException
  {
    public NoAutoProfilesException()
    {
      super("No auto-profiles found (exist) for the specified operation.");
    }
    public NoAutoProfilesException(String msg)
    {
      super(msg);
    }
  } // NoAutoProfilesException
  
} // AutoProfileRecordBatchProcess