/*@lineinfo:filename=EquipmentData_MerchEquip*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.equipment;

import java.sql.ResultSet;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.constants.mesConstants;
import com.mes.tools.AccountKey;
import com.mes.tools.EquipmentKey;
import sqlj.runtime.ResultSetIterator;


/**
 * EquipmentData_MerchEquip
 * 
 * MeS owned equipment item associated with a given merchant.
 */
public class EquipmentData_MerchEquip extends EquipmentDataBase
{
  // create class log category
  static Category log = Category.getInstance(EquipmentData_MerchEquip.class.getName());

  // constants
  // (NONE)
  
  // data members
  protected long        appSeqNum               = -1L;
  protected int         equiplendtype_code      = 0;
  protected String      equiplendtype_name      = "";
  protected int         equip_quantity          = 0;
  protected int         prod_option_id          = 0;
  protected String      prod_option_name        = "";
  protected String      impplate_size           = "";
  protected double      amount                  = 0d;
  protected int         quantity_deployed       = 0;
  protected int         quantity_refurbished    = 0;
  protected double      credit_amount           = 0d;
  protected double      charge_amount           = 0d;
  protected int         payment_plan_months     = 0;

  // construction
  public EquipmentData_MerchEquip()
  {
    super();
  }
  
  public void clear()
  {
    super.clear();
    
    appSeqNum               = -1L;
    equiplendtype_code      = 0;
    equiplendtype_name      = "";
    equip_quantity          = 0;
    prod_option_id          = 0;
    impplate_size           = "";
    amount                  = 0d;
    quantity_deployed       = 0;
    quantity_refurbished    = 0;
    credit_amount           = 0d;
    charge_amount           = 0d;
    payment_plan_months     = 0;
  }

  // accessors
  public long   getAppSeqNum()                    { return appSeqNum; }
  public int    getEquipLendTypeCode()            { return equiplendtype_code; }
  public String getEquipLendTypeName()            { return equiplendtype_name; }
  public int    getEquipQuantity()                { return equip_quantity; }
  public int    getProdOptionId()                 { return prod_option_id; }
  public String getProdOptionName()               { return prod_option_name; }
  public String getImpplateSize()                 { return impplate_size; }
  public double getAmount()                       { return amount; }
  public int    getQuantityDeployed()             { return quantity_deployed; }
  public int    getQuantityRefurbished()          { return quantity_refurbished; }
  public double getCreditAmount()                 { return credit_amount; }
  public double getChargeAmount()                 { return charge_amount; }
  public int    getPaymentPlanMonths()            { return payment_plan_months; }

  public int getType()
  {
    return mesConstants.EQUIPTYPE_MERCHEQUIP;
  }

  public String getTypeDesc()
  {
    return "Merchant Owned Equipment";
  }

  public Vector load(AccountKey key)
  {
    Vector eq = new Vector(0,1);
    
    long appSeqNum;
    try {
      appSeqNum = key.getAppSeqNum();
    }
    catch(Exception e) {
      return eq;
    }

    ResultSetIterator           it          = null;
    ResultSet                   rs          = null;

    try {
        
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:111^7*/

//  ************************************************************
//  #sql [Ctx] it = { select     e.EQUIPMFGR_MFR_CODE                  mfr_code
//                    ,e.equiptype_code                      equiptype_code
//                    ,e.equip_model                         equip_model
//                    ,e.EQUIP_DESCRIPTOR                    descriptor
//                    ,e.EQUIP_PRICE                        price
//                    ,e.EQUIP_RENT                          rent
//                    ,e.EQUIP_USAGE_IND                    usage_ind
//                    ,e.TSYS_EQUIP_DESC                     tsys_equip_desc
//                    ,em.EQUIPMFGR_MFR_NAME                mfr_name
//                    ,et.EQUIPTYPE_DESCRIPTION              equiptype_name
//                    ,elt.EQUIPLENDTYPE_DESCRIPTION        disposition
//                    ,me.EQUIPLENDTYPE_CODE                equiplendtype_code
//                    ,me.MERCHEQUIP_EQUIP_QUANTITY          equip_quantity
//                    ,me.PROD_OPTION_ID                    prod_option_id
//                    ,po.PROD_OPTION_DES                   prod_option_name
//                    ,me.MERCHEQUIP_IMPPLATE_SIZE          impplate_size
//                    ,me.MERCHEQUIP_AMOUNT                  amount
//                    ,me.QUANTITY_DEPLOYED                  quantity_deployed
//                    ,me.QUANTITY_REFURBISHED              quantity_refurbished
//                    ,me.MERCHEQUIP_CREDIT_AMOUNT          credit_amount
//                    ,me.MERCHEQUIP_CHARGE_AMOUNT          charge_amount
//                    ,me.PAYMENT_PLAN_MONTHS                payment_plan_months
//                    ,elt.EQUIPLENDTYPE_DESCRIPTION        equiplendtype_name
//                    ,me.app_seq_num                       app_seq_num
//          from       equipment                            e
//                    ,equipmfgr                            em
//                    ,equiptype                            et
//                    ,merchequipment                        me
//                    ,equiplendtype                        elt
//                    ,prodoption                           po
//          where      e.EQUIPMFGR_MFR_CODE = em.EQUIPMFGR_MFR_CODE
//                    and e.equiptype_code = et.equiptype_code
//                    and me.equip_model = e.equip_model(+)
//                    and me.APP_SEQ_NUM = :appSeqNum
//                    and me.EQUIPLENDTYPE_CODE = elt.EQUIPLENDTYPE_CODE
//                    and po.PROD_OPTION_ID(+) = me.prod_option_id
//          order by  equip_model
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select     e.EQUIPMFGR_MFR_CODE                  mfr_code\n                  ,e.equiptype_code                      equiptype_code\n                  ,e.equip_model                         equip_model\n                  ,e.EQUIP_DESCRIPTOR                    descriptor\n                  ,e.EQUIP_PRICE                        price\n                  ,e.EQUIP_RENT                          rent\n                  ,e.EQUIP_USAGE_IND                    usage_ind\n                  ,e.TSYS_EQUIP_DESC                     tsys_equip_desc\n                  ,em.EQUIPMFGR_MFR_NAME                mfr_name\n                  ,et.EQUIPTYPE_DESCRIPTION              equiptype_name\n                  ,elt.EQUIPLENDTYPE_DESCRIPTION        disposition\n                  ,me.EQUIPLENDTYPE_CODE                equiplendtype_code\n                  ,me.MERCHEQUIP_EQUIP_QUANTITY          equip_quantity\n                  ,me.PROD_OPTION_ID                    prod_option_id\n                  ,po.PROD_OPTION_DES                   prod_option_name\n                  ,me.MERCHEQUIP_IMPPLATE_SIZE          impplate_size\n                  ,me.MERCHEQUIP_AMOUNT                  amount\n                  ,me.QUANTITY_DEPLOYED                  quantity_deployed\n                  ,me.QUANTITY_REFURBISHED              quantity_refurbished\n                  ,me.MERCHEQUIP_CREDIT_AMOUNT          credit_amount\n                  ,me.MERCHEQUIP_CHARGE_AMOUNT          charge_amount\n                  ,me.PAYMENT_PLAN_MONTHS                payment_plan_months\n                  ,elt.EQUIPLENDTYPE_DESCRIPTION        equiplendtype_name\n                  ,me.app_seq_num                       app_seq_num\n        from       equipment                            e\n                  ,equipmfgr                            em\n                  ,equiptype                            et\n                  ,merchequipment                        me\n                  ,equiplendtype                        elt\n                  ,prodoption                           po\n        where      e.EQUIPMFGR_MFR_CODE = em.EQUIPMFGR_MFR_CODE\n                  and e.equiptype_code = et.equiptype_code\n                  and me.equip_model = e.equip_model(+)\n                  and me.APP_SEQ_NUM =  :1 \n                  and me.EQUIPLENDTYPE_CODE = elt.EQUIPLENDTYPE_CODE\n                  and po.PROD_OPTION_ID(+) = me.prod_option_id\n        order by  equip_model";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.equipment.EquipmentData_MerchEquip",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.equipment.EquipmentData_MerchEquip",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:150^7*/
      rs = it.getResultSet();

      while(rs.next()) {
        EquipmentData_MerchEquip aed = new EquipmentData_MerchEquip();
          
        aed.disposition                         = rs.getString("disposition");
        aed.equipMfrCode                        = rs.getInt("mfr_code");
        aed.equipTypeCode                       = rs.getInt("equiptype_code");
        aed.equipModel                          = rs.getString("equip_model");
        aed.descriptor                          = rs.getString("descriptor");
        aed.price                               = rs.getDouble("price");
        aed.rent                                = rs.getDouble("rent");
        aed.usageInd                            = rs.getString("usage_ind");
        aed.tsysEquipDesc                       = rs.getString("tsys_equip_desc");
        aed.equipMfrName                        = rs.getString("mfr_name");
        aed.equipTypeName                       = rs.getString("equiptype_name");
        
        aed.equiplendtype_code                  = rs.getInt("equiplendtype_code");
        aed.equip_quantity                      = rs.getInt("equip_quantity");
        aed.prod_option_id                      = rs.getString("prod_option_id")==null? 0:rs.getInt("prod_option_id");
        aed.prod_option_name                    = rs.getString("prod_option_name")==null? "":rs.getString("prod_option_name");
        aed.impplate_size                       = rs.getString("impplate_size");
        aed.amount                              = rs.getDouble("amount");
        aed.quantity_deployed                   = rs.getInt("quantity_deployed");
        aed.quantity_refurbished                = rs.getInt("quantity_refurbished");
        aed.credit_amount                       = rs.getDouble("credit_amount");
        aed.charge_amount                       = rs.getDouble("charge_amount");
        aed.payment_plan_months                 = rs.getInt("payment_plan_months");
        aed.equiplendtype_name                  = rs.getString("equiplendtype_name");
        aed.appSeqNum                           = rs.getLong("app_seq_num");
          
        aed.setEquipmentKey(new EquipmentKey(aed.appSeqNum,aed.equipModel,aed.equiplendtype_code));
        
        eq.add(aed);
      }
        
      rs.close();
      it.close();
    }
    catch(Exception e) {
      logEntry("load(AccountKey)", e.toString());
    }
    finally {
      try{ rs.close(); } catch( Exception e ) { }
      try{ it.close(); } catch( Exception e ) { }
      cleanUp();
    }
      
    return eq;
  }

  public boolean load(EquipmentKey key)
  {
    if(key==null || key.getEquipType()!=mesConstants.EQUIPTYPE_MERCHEQUIP || !key.isSet())
      return false;

    ResultSetIterator           it          = null;
    ResultSet                   rs          = null;
      
    try {
        
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:214^7*/

//  ************************************************************
//  #sql [Ctx] it = { select     e.EQUIPMFGR_MFR_CODE                  mfr_code
//                    ,e.equiptype_code                      equiptype_code
//                    ,e.equip_model                         equip_model
//                    ,e.EQUIP_DESCRIPTOR                    descriptor
//                    ,e.EQUIP_PRICE                        price
//                    ,e.EQUIP_RENT                          rent
//                    ,e.EQUIP_USAGE_IND                    usage_ind
//                    ,e.TSYS_EQUIP_DESC                     tsys_equip_desc
//                    ,em.EQUIPMFGR_MFR_NAME                mfr_name
//                    ,et.EQUIPTYPE_DESCRIPTION              equiptype_name
//                    ,elt.EQUIPLENDTYPE_DESCRIPTION        disposition
//                    ,me.EQUIPLENDTYPE_CODE                equiplendtype_code
//                    ,me.MERCHEQUIP_EQUIP_QUANTITY          equip_quantity
//                    ,me.PROD_OPTION_ID                    prod_option_id
//                    ,po.PROD_OPTION_DES                    prod_option_name
//                    ,me.MERCHEQUIP_IMPPLATE_SIZE          impplate_size
//                    ,me.MERCHEQUIP_AMOUNT                  amount
//                    ,me.QUANTITY_DEPLOYED                  quantity_deployed
//                    ,me.QUANTITY_REFURBISHED              quantity_refurbished
//                    ,me.MERCHEQUIP_CREDIT_AMOUNT          credit_amount
//                    ,me.MERCHEQUIP_CHARGE_AMOUNT          charge_amount
//                    ,me.PAYMENT_PLAN_MONTHS                payment_plan_months
//                    ,elt.EQUIPLENDTYPE_DESCRIPTION        equiplendtype_name
//                    ,me.app_seq_num                       app_seq_num
//          from       equipment                            e
//                    ,equipmfgr                            em
//                    ,equiptype                            et
//                    ,merchequipment                        me
//                    ,equiplendtype                        elt
//                    ,prodoption                           po
//          where      e.EQUIPMFGR_MFR_CODE = em.EQUIPMFGR_MFR_CODE
//                    and e.equiptype_code = et.equiptype_code
//                    and me.equip_model = e.equip_model(+)
//                    and me.APP_SEQ_NUM = :key.getMerchEquipKey().appSeqNum
//                    and me.EQUIP_MODEL = :key.getMerchEquipKey().equipModel
//                    and me.EQUIPLENDTYPE_CODE = :key.getMerchEquipKey().lendTypeCode
//                    and me.EQUIPLENDTYPE_CODE = elt.EQUIPLENDTYPE_CODE
//                    and po.PROD_OPTION_ID(+) = me.prod_option_id
//          order by  equip_model
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select     e.EQUIPMFGR_MFR_CODE                  mfr_code\n                  ,e.equiptype_code                      equiptype_code\n                  ,e.equip_model                         equip_model\n                  ,e.EQUIP_DESCRIPTOR                    descriptor\n                  ,e.EQUIP_PRICE                        price\n                  ,e.EQUIP_RENT                          rent\n                  ,e.EQUIP_USAGE_IND                    usage_ind\n                  ,e.TSYS_EQUIP_DESC                     tsys_equip_desc\n                  ,em.EQUIPMFGR_MFR_NAME                mfr_name\n                  ,et.EQUIPTYPE_DESCRIPTION              equiptype_name\n                  ,elt.EQUIPLENDTYPE_DESCRIPTION        disposition\n                  ,me.EQUIPLENDTYPE_CODE                equiplendtype_code\n                  ,me.MERCHEQUIP_EQUIP_QUANTITY          equip_quantity\n                  ,me.PROD_OPTION_ID                    prod_option_id\n                  ,po.PROD_OPTION_DES                    prod_option_name\n                  ,me.MERCHEQUIP_IMPPLATE_SIZE          impplate_size\n                  ,me.MERCHEQUIP_AMOUNT                  amount\n                  ,me.QUANTITY_DEPLOYED                  quantity_deployed\n                  ,me.QUANTITY_REFURBISHED              quantity_refurbished\n                  ,me.MERCHEQUIP_CREDIT_AMOUNT          credit_amount\n                  ,me.MERCHEQUIP_CHARGE_AMOUNT          charge_amount\n                  ,me.PAYMENT_PLAN_MONTHS                payment_plan_months\n                  ,elt.EQUIPLENDTYPE_DESCRIPTION        equiplendtype_name\n                  ,me.app_seq_num                       app_seq_num\n        from       equipment                            e\n                  ,equipmfgr                            em\n                  ,equiptype                            et\n                  ,merchequipment                        me\n                  ,equiplendtype                        elt\n                  ,prodoption                           po\n        where      e.EQUIPMFGR_MFR_CODE = em.EQUIPMFGR_MFR_CODE\n                  and e.equiptype_code = et.equiptype_code\n                  and me.equip_model = e.equip_model(+)\n                  and me.APP_SEQ_NUM =  :1 \n                  and me.EQUIP_MODEL =  :2 \n                  and me.EQUIPLENDTYPE_CODE =  :3 \n                  and me.EQUIPLENDTYPE_CODE = elt.EQUIPLENDTYPE_CODE\n                  and po.PROD_OPTION_ID(+) = me.prod_option_id\n        order by  equip_model";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.equipment.EquipmentData_MerchEquip",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,key.getMerchEquipKey().appSeqNum);
   __sJT_st.setString(2,key.getMerchEquipKey().equipModel);
   __sJT_st.setInt(3,key.getMerchEquipKey().lendTypeCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.equipment.EquipmentData_MerchEquip",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:255^7*/
      rs = it.getResultSet();

      if(rs.next()) {
        
        this.key                            = key;
        
        disposition                         = rs.getString("disposition");
        equipMfrCode                        = rs.getInt("mfr_code");
        equipTypeCode                       = rs.getInt("equiptype_code");
        equipModel                          = rs.getString("equip_model");
        descriptor                          = rs.getString("descriptor");
        price                               = rs.getDouble("price");
        rent                                = rs.getDouble("rent");
        usageInd                            = rs.getString("usage_ind");
        tsysEquipDesc                       = rs.getString("tsys_equip_desc");
        equipMfrName                        = rs.getString("mfr_name");
        equipTypeName                       = rs.getString("equiptype_name");
        
        equiplendtype_code                  = rs.getInt("equiplendtype_code");
        equip_quantity                      = rs.getInt("equip_quantity");
        prod_option_id                      = rs.getString("prod_option_id")==null? 0:rs.getInt("prod_option_id");
        prod_option_name                    = rs.getString("prod_option_name")==null? "":rs.getString("prod_option_name");
        impplate_size                       = rs.getString("impplate_size");
        amount                              = rs.getDouble("amount");
        quantity_deployed                   = rs.getInt("quantity_deployed");
        quantity_refurbished                = rs.getInt("quantity_refurbished");
        credit_amount                       = rs.getDouble("credit_amount");
        charge_amount                       = rs.getDouble("charge_amount");
        payment_plan_months                 = rs.getInt("payment_plan_months");
        equiplendtype_name                  = rs.getString("equiplendtype_name");
        appSeqNum                           = rs.getLong("app_seq_num");

      } else
        return false;
        
    }
    catch(Exception e) {
      logEntry("load(EquipmentKey)", e.toString());
      return false;
    }
    finally {
      try{ rs.close(); } catch( Exception e ) { }
      try{ it.close(); } catch( Exception e ) { }
      cleanUp();
    }
      
    return true;
  }

}/*@lineinfo:generated-code*/