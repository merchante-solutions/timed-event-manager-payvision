package com.mes.creditmatrix;

import java.util.Vector;

/**
 * CreditMatrixScoreBean
 * 
 * Interface for retreiving all credit matrix (app underwriting) scores for a given application.
 */
public final class CreditMatrixScoreBean
{
  private long            app_seq_num;
  private Vector          scores; // all creditmatrix scores assoc. w/ a given app
  
  public CreditMatrixScoreBean(long app_seq_num)
  {
    load(app_seq_num);
  }
  
  private void load(long app_seq_num)
  {
    if(this.app_seq_num==app_seq_num)
      return;
    
    if(scores==null)
      scores=new Vector(0,1);
    
    scores=getDAOFactory().getCreditMatrixScoreDAO().getCreditMatrixScoresByApplication(app_seq_num);
    this.app_seq_num=app_seq_num;
  }
  
  private DAOFactory getDAOFactory()
  {
    // hook onto the mes db for this bean
    return DAOFactoryBuilder.getDAOFactory(DAOFactory.DAO_MESDB);
  }
  
  public Vector getCreditMatrixScores()
  {
    return scores;
  }

} // CreditMatrixScoreBean
