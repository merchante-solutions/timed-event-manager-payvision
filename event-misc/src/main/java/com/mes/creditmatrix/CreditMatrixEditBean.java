/*@lineinfo:filename=CreditMatrixEditBean*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.creditmatrix;

import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
// log4j
import org.apache.log4j.Category;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.forms.TextareaField;
import com.mes.support.HttpTextMessageManager;
import com.mes.support.StringUtilities;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

/**
 * CreditMatrixEditBean class.
 * 
 * Used to edit credit matrices and their constituent elements for respective app types.
 */
public final class CreditMatrixEditBean extends FieldBean
{
  // create class log category
  static Category log = Category.getInstance(CreditMatrixEditBean.class.getName());
  
  // constants
  public static final int         VIEWMODE_UNDEFINED                        = 0;
  public static final int         VIEWMODE_START                            = 1;
  public static final int         VIEWMODE_APPTYPES                         = VIEWMODE_START;
  public static final int         VIEWMODE_SCORE                            = 2;
  public static final int         VIEWMODE_CREDITMATRICESBYAPPTYPE          = 3;
  public static final int         VIEWMODE_CREDITMATRIX_ADD                 = 4;
  public static final int         VIEWMODE_CREDITMATRIX_EDIT                = 5;
  public static final int         VIEWMODE_CREDITMATRIX_COPY                = 6;
  public static final int         VIEWMODE_CREDITMATRIXELEMENT_ADD          = 7;
  public static final int         VIEWMODE_CREDITMATRIXELEMENT_ADDSUB       = 8;
  public static final int         VIEWMODE_CREDITMATRIXELEMENT_EDIT         = 9;
  public static final int         VIEWMODE_END                              = VIEWMODE_CREDITMATRIXELEMENT_EDIT;

  // data members
  private int                   view_mode;
  private int                   app_type;
  private String                app_type_desc;
  private long                  cm_id;
  private long                  cme_id;
  private long                  app_seq_num;
  
  private CreditMatrix          cm;
  private CreditMatrixElement   cme;
  private CreditMatrixScore     cms;
  
  private HttpServletRequest    request;
  private HttpTextMessageManager  tmm;

  // class methods
  
  
  // object methods
  
  // construction
  public CreditMatrixEditBean()
  {
    super();

    fields.setShowName("");   // so "baseGroup:" prefix doesn't show up in error message text.
    
    this.request=null;
    
    view_mode=VIEWMODE_START;
    app_type=-1;
    app_type_desc="";
    cm_id=-1L;
    cme_id=-1L;
    app_seq_num=-1L;

    cm=null;
    cme=null;
    cms=null;
    
    tmm = new HttpTextMessageManager();
    
    createFields();
  }

  public int getViewMode() { return view_mode; }
  public long getCreditMatrixID() { return cm_id; }
  public long getCreditMatrixElementID() { return cme_id; }
  public long getAppSeqNum() { return app_seq_num; }
  public CreditMatrix getCreditMatrix() { return cm; }
  public CreditMatrixElement getCreditMatrixElement() { return cme; }
  public CreditMatrixScore getCreditMatrixScore() { return cms; }
  public String getCreditMatrixScoreDetailHtmlText()
  {
    if(cms==null)
      return "";

    String s = cms.getScoreDetailText();

    return StringUtilities.replace(s,"\n","<br>");
  }
  public int getCreditMatrixScoreNumRedFlags()
  {
    return cms==null? 0:cms.getNumRedFlags();
  }
  
  public HttpTextMessageManager getTextMessageManager()
  {
    return tmm;
  }
  
  private void createFields()
  {
    FieldGroup fg;
    Field fld;
    
    // CreditMatrix
    fg = new FieldGroup("CreditMatrix");
    fld = new Field("Matrix Name",50,20,false);
    fg.add(fld);
    fld = new DropDownField("Application Type",new AppTypeDropDownTable(),false);
    fg.add(fld);
    fld = new NumberField("Auto-approve Threshold",6,2,false,1,0,9999);
    fg.add(fld);
    fld = new NumberField("Auto-decline Threshold",6,2,false,1,0,9999);
    fg.add(fld);
    fld = new Field("app_seq_num",10,10,true);
    fg.add(fld);
    fields.add(fg);
        
    // CreditMatrixElement
    fg = new FieldGroup("CreditMatrixElement");
    fld = new DropDownField("Type",new CMETypeDropDown(),false);
    fg.add(fld);
    /*
    fld = new Field("Name",50,20,false);
    fg.add(fld);
    fld = new Field("Label",50,20,false);
    fg.add(fld);
    */
    fld = new NumberField("Weight",6,2,true,4,0,9999);
    fg.add(fld);
    fld = new NumberField("Offset",6,2,true,4,0,9999);
    fg.add(fld);
    fld = new TextareaField("Match String",2000,20,35,true);
    fg.add(fld);
    fld = new DropDownField("Mib_Accessor",new AppFieldNamesDropDownTable(),true);
    fg.add(fld);
    fld = new TextareaField("Numeric Range",2000,20,35,true);
    fg.add(fld);
    fld = new NumberField("Red Flag Threshold",4,4,true,0,0,999);
    fg.add(fld);
    fld = new CheckboxField("Is Bounded","Is Numeric Range bounded?",false);
    fg.add(fld);

    fields.add(fg);
  }

  private DAOFactory getDAOFactory()
  {
    // hook onto the mes db for this bean
    return DAOFactoryBuilder.getDAOFactory(DAOFactory.DAO_MESDB);
  }
  
  private final class AppFieldNamesDropDownTable extends DropDownTable
  {
    public AppFieldNamesDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      addElement("","");

      String[] appFlds = ApplicationProxy.getInstance().getAppFieldNames();
      for(int i=0;i<appFlds.length;i++)
        addElement(appFlds[i],appFlds[i]);

    }
  }
  
  private final class CMETypeDropDown extends DropDownTable
  {
    public CMETypeDropDown()
    {
      getData();
    }

    private void getData()
    {
      addElement("","");

      for(int i=CreditMatrixElement.CMETYPE_START;i<=CreditMatrixElement.CMETYPE_END;i++)
        addElement(Integer.toString(i),CreditMatrixElement.getTypeDescriptor(i));
    }
    
  } // CMETypeDropDown class
  
  public final class AppTypeDropDownTable extends DropDownTable
  {
    public AppTypeDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      ResultSetIterator   it      = null;
      ResultSet           rs      = null;

      try
      {
        addElement("","");
        
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:220^9*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT    
//                      APP_TYPE_CODE
//                      ,APP_DESCRIPTION
//            FROM      
//                      APP_TYPE
//            ORDER BY  
//                      APP_DESCRIPTION ASC
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT    \n                    APP_TYPE_CODE\n                    ,APP_DESCRIPTION\n          FROM      \n                    APP_TYPE\n          ORDER BY  \n                    APP_DESCRIPTION ASC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.creditmatrix.CreditMatrixEditBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.creditmatrix.CreditMatrixEditBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:229^9*/

        rs = it.getResultSet();
        
        String val,desc;

        while (rs.next()) {
          val = rs.getString("APP_TYPE_CODE");
          desc = StringUtilities.rightJustify(val,4,' ') + " - " + rs.getString("APP_DESCRIPTION");
          addElement(val,desc);
        }

        it.close();
      }
      catch (Exception e) {
        log.error("AppTypeDropDownTable.getData() EXCEPTION: '"+e.getMessage()+"'.");
      }
      finally {
        cleanUp();
      }

    }
  
  } // AppTypeDropDownTable class
  
  public String[][] getApplicationTypes()
  {
    return getDAOFactory().getCreditMatrixDAO().getApplicationTypes();
  }
  
  public Vector getCreditMatricesByAppType()
  {
    return getDAOFactory().getCreditMatrixDAO().getCreditMatricesByAppType(app_type);
  }
  
  private void addCreditMatrix()
  {
    if(cm==null)
      cm = getDAOFactory().getCreditMatrixDAO().createCreditMatrix();
    else
      cm.clear();
    cm.setID(-1L);  // reset
    
    Field fld;
    FieldGroup fg;

    fg = (FieldGroup)fields.getField("CreditMatrix");
    
    fld=fg.getField("Application Type");
    cm.setAppType(Integer.parseInt(fld.getData()));
    fld=fg.getField("Matrix Name");
    cm.setName(fld.getData());
    fld=fg.getField("Auto-approve Threshold");
    cm.setAutoApproveThreshold(StringUtilities.stringToLong(fld.getData(),0L));
    fld=fg.getField("Auto-decline Threshold");
    cm.setAutoDeclineThreshold(StringUtilities.stringToLong(fld.getData(),0L));

    
    cm_id = getDAOFactory().getCreditMatrixDAO().insertCreditMatrix(cm);

    if(cm_id>0)
      tmm.msg("Matrix '"+cm.getName()+"' (ID: "+cm_id+") added.");
    else
      tmm.err("Unable to add the Matrix.");
  }
  
  private void updateCreditMatrix()
  {
    if(cm==null)
      cm = getDAOFactory().getCreditMatrixDAO().createCreditMatrix();
    
    Field fld;
    FieldGroup fg;

    fg = (FieldGroup)fields.getField("CreditMatrix");
    
    cm.setID(cm_id);
    
    fld=fg.getField("Matrix Name");
    cm.setName(fld.getData());
    fld=fg.getField("Application Type");
    cm.setAppType(Integer.parseInt(fld.getData()));
    fld=fg.getField("Auto-approve Threshold");
    cm.setAutoApproveThreshold(Long.parseLong(fld.getData()));
    fld=fg.getField("Auto-decline Threshold");
    cm.setAutoDeclineThreshold(Long.parseLong(fld.getData()));
    
    if(getDAOFactory().getCreditMatrixDAO().updateCreditMatrix(cm))
      tmm.msg("Matrix '"+cm.getName()+"' updated.");
    else
      tmm.err("Unable to update the Matrix.");
  }
  
  private void deleteCreditMatrix()
  {
    if(cm==null)
      cm = getDAOFactory().getCreditMatrixDAO().createCreditMatrix();

    cm.setID(cm_id);
    
    if(getDAOFactory().getCreditMatrixDAO().deleteCreditMatrix(cm))
      tmm.msg("Matrix '"+cm.getID()+"' deleted.");
    else
      tmm.err("Unable to delete the Matrix.");

    // reset
    cm_id=-1L;
    cme_id=-1L;
    if(cme!=null)
      cme.clear();
    if(cm!=null)
      cm.clear();
  }

  private void copyCreditMatrix()
  {
    if(cm_id==-1L)
      return;
    if(cm==null)
      cm = getDAOFactory().getCreditMatrixDAO().findCreditMatrix(cm_id);

    Field fld;
    FieldGroup fg;

    fg = (FieldGroup)fields.getField("CreditMatrix");
    fld=fg.getField("Application Type");

    long newid = getDAOFactory().getCreditMatrixDAO().getUniqueID();

    CreditMatrix cmcopy = cm.copy();

    cmcopy.setAppType(StringUtilities.stringToInt(fld.getData(),0));
    cmcopy.setID(newid);
    cmcopy.setName("Credit Matrix "+newid);  // enforce unique name

    log.debug("cmcopy.toString(): \n"+cmcopy.toString());

    if(getDAOFactory().getCreditMatrixDAO().persistCreditMatrix(cmcopy))
      tmm.msg("Matrix '"+cm.getID()+"' copied to selected Application Type.");
    else
      tmm.err("Unable to copy the Matrix.");

  }
  
  private void preAddCreditMatrixElement()
  {
    Field fld;
    FieldGroup fg;

    fg = (FieldGroup)fields.getField("CreditMatrixElement");
    fld=fg.getField("Type");
    
    // prep cme
    int cme_type=StringUtilities.stringToInt(fld.getData(),0);
    
    if(cme==null)
      cme = getDAOFactory().getCreditMatrixElementDAO().createCreditMatrixElement(cme_type);
    else {
      if(cme_type==cme.getType())
        cme.clear();
      else
        cme=getDAOFactory().getCreditMatrixElementDAO().createCreditMatrixElement(cme_type);
    }

    if(cme==null) {
      tmm.err("Unable to create Matrix Element.");
      return;
    }
    
    view_mode=VIEWMODE_CREDITMATRIXELEMENT_ADDSUB;
    
  }
  
  private void addCreditMatrixElement()
  {
    // NOTE: cme is presumed to be non-null
    cme.setID(-1L);
    cme.setCreditMatrixID(cm_id);
    
    Field fld;
    FieldGroup fg;

    fg = (FieldGroup)fields.getField("CreditMatrixElement");
    
    // App Weight
    if(cme.getType()==CreditMatrixElement.CMETYPE_APPWEIGHT) {
      fld=fg.getField("Mib_Accessor");
      ((CreditMatrixElement_App)cme).setMibAccessor("get"+fld.getData());
      fld=fg.getField("Weight");
      ((CreditMatrixElement_AppWeight)cme).setWeight(Float.valueOf(fld.getData()).floatValue());
      cme.setName(((CreditMatrixElement_App)cme).getMibAccessor());
      cme.setLabel(cme.getName());
    
    // App Offset
    } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPOFFSET) {
      fld=fg.getField("Mib_Accessor");
      ((CreditMatrixElement_App)cme).setMibAccessor(fld.getData());
      fld=fg.getField("Offset");
      ((CreditMatrixElement_AppOffset)cme).setOffset(Float.valueOf(fld.getData()).floatValue());
      cme.setName(((CreditMatrixElement_App)cme).getMibAccessor());
      cme.setLabel(cme.getName());
    
    // App String Match
    } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPSTRINGMATCH) {
      fld=fg.getField("Mib_Accessor");
      ((CreditMatrixElement_App)cme).setMibAccessor(fld.getData());
      fld=fg.getField("Match String");
      ((CreditMatrixElement_AppStringMatch)cme).setMatchstring(fld.getData());
      cme.setName(((CreditMatrixElement_App)cme).getMibAccessor());
      cme.setLabel(cme.getName());
    
    // App Num Range
    } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPNUMRANGE) {
      fld=fg.getField("Mib_Accessor");
      ((CreditMatrixElement_App)cme).setMibAccessor(fld.getData());
      fld=fg.getField("Numeric Range");
      ((CreditMatrixElement_AppNumRange)cme).setNumRange(fld.getData());
      fld=fg.getField("Is Bounded");
      ((CreditMatrixElement_AppNumRange)cme).setIsRangeBound(((CheckboxField)fld).isChecked());
      cme.setName(((CreditMatrixElement_App)cme).getMibAccessor());
      cme.setLabel(cme.getName());

    // Credit Score
    } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPCREDITSCORE) {
      fld=fg.getField("Numeric Range");
      ((CreditMatrixElement_CreditScore)cme).setNumRange(fld.getData());
      fld=fg.getField("Red Flag Threshold");
      ((CreditMatrixElement_CreditScore)cme).setRedFlagThreshold(StringUtilities.stringToInt(fld.getData()));
      fld=fg.getField("Is Bounded");
      ((CreditMatrixElement_CreditScore)cme).setIsRangeBound(((CheckboxField)fld).isChecked());

    }
    
    cme_id = getDAOFactory().getCreditMatrixElementDAO().insertCreditMatrixElement(cme);

    if(cme_id<1) {
      tmm.err("Unable to add the Matrix Element.  Make sure the Application Field Name is not already in use for this Matrix.");
      return;
    }
    tmm.msg("Matrix Element '"+cme.getName()+"' (ID: "+cme_id+") added.");

  }

  private void updateCreditMatrixElement()
  {
    Field fld;
    FieldGroup fg;

    fg = (FieldGroup)fields.getField("CreditMatrixElement");
    
    cme.setID(cme_id);
    cme.setCreditMatrixID(cm_id);
    
    // App Weight
    if(cme.getType()==CreditMatrixElement.CMETYPE_APPWEIGHT) {
      fld=fg.getField("Mib_Accessor");
      ((CreditMatrixElement_App)cme).setMibAccessor(fld.getData());
      fld=fg.getField("Weight");
      ((CreditMatrixElement_AppWeight)cme).setWeight(Float.valueOf(fld.getData()).floatValue());
      cme.setName(((CreditMatrixElement_App)cme).getMibAccessor());
      cme.setLabel(cme.getName());
    
    // App Offset
    } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPOFFSET) {
      fld=fg.getField("Mib_Accessor");
      ((CreditMatrixElement_App)cme).setMibAccessor(fld.getData());
      fld=fg.getField("Offset");
      ((CreditMatrixElement_AppOffset)cme).setOffset(Float.valueOf(fld.getData()).floatValue());
      cme.setName(((CreditMatrixElement_App)cme).getMibAccessor());
      cme.setLabel(cme.getName());
    
    // App String Match
    } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPSTRINGMATCH) {
      fld=fg.getField("Mib_Accessor");
      ((CreditMatrixElement_App)cme).setMibAccessor(fld.getData());
      fld=fg.getField("Match String");
      ((CreditMatrixElement_AppStringMatch)cme).setMatchstring(fld.getData());
      cme.setName(((CreditMatrixElement_App)cme).getMibAccessor());
      cme.setLabel(cme.getName());
    
    // App Num Range
    } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPNUMRANGE) {
      fld=fg.getField("Mib_Accessor");
      ((CreditMatrixElement_App)cme).setMibAccessor(fld.getData());
      fld=fg.getField("Numeric Range");
      ((CreditMatrixElement_AppNumRange)cme).setNumRange(fld.getData());
      fld=fg.getField("Is Bounded");
      ((CreditMatrixElement_AppNumRange)cme).setIsRangeBound(((CheckboxField)fld).isChecked());
      cme.setName(((CreditMatrixElement_App)cme).getMibAccessor());
      cme.setLabel(cme.getName());
    
    // Credit Score
    } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPCREDITSCORE) {
      fld=fg.getField("Numeric Range");
      ((CreditMatrixElement_CreditScore)cme).setNumRange(fld.getData());
      fld=fg.getField("Red Flag Threshold");
      ((CreditMatrixElement_CreditScore)cme).setRedFlagThreshold(StringUtilities.stringToInt(fld.getData()));
      fld=fg.getField("Is Bounded");
      ((CreditMatrixElement_CreditScore)cme).setIsRangeBound(((CheckboxField)fld).isChecked());

    }
    
    if(getDAOFactory().getCreditMatrixElementDAO().updateCreditMatrixElement(cme))
      tmm.msg("Matrix Element '"+cme.getName()+"' updated.");
    else
      tmm.err("Unable to update the Matrix Element.  Make sure the element Name is unique.");
  }

  private void deleteCreditMatrixElement()
  {
    if(getDAOFactory().getCreditMatrixElementDAO().deleteCreditMatrixElement(cme_id))
      tmm.msg("Matrix Element '"+cme.getID()+"' deleted.");
    else {
      tmm.err("Unable to delete the Matrix Element.");
      return;
    }

    cme_id=-1L;
    if(cme!=null)
      cme.clear();
  }

  private void calculateCreditMatrixScore()
  {
    CreditMatrixScoreCalculator cmsc = new CreditMatrixScoreCalculator();

    log.debug("calculateCreditMatrixScore() app_seq_num="+app_seq_num);
    try {
      cms = cmsc.calculateScore(app_seq_num);
    }
    catch(Exception e) {
      tmm.wrn(e.getMessage());
      if(cms!=null)
        cms.clear();
    }
  }

  public final boolean hasViewLinks()
  {
    return (view_mode>=VIEWMODE_START && view_mode<=VIEWMODE_END);
  }

  public final String getViewLinksHtmlTable(String cssClassName)
  {
    StringBuffer sb = new StringBuffer(1024);
    boolean bAssSpace=false;
    
    sb.append("<table border=0 cellpadding=0 cellspacing=0>");
    sb.append("<tr>");

    // VIEWMODE_APPTYPES
    if(view_mode!=VIEWMODE_APPTYPES) {
      if(bAssSpace)
        sb.append("<td width=8>&nbsp;|&nbsp;</td>");
      sb.append("<td class=\"smallText\"><a href=\"/jsp/creditmatrix/cmedit.jsp?view_mode="+VIEWMODE_APPTYPES+"\">Application Types</a></td>");
      bAssSpace=true;
    }

    // VIEWMODE_SCORE
    if(view_mode!=VIEWMODE_SCORE) {
      if(bAssSpace)
        sb.append("<td width=8>&nbsp;|&nbsp;</td>");
      sb.append("<td class=\"smallText\"><a href=\"/jsp/creditmatrix/cmedit.jsp?view_mode="+VIEWMODE_SCORE+"\">Perform Application Scoring</a></td>");
      bAssSpace=true;
    }

    /*
    // VIEWMODE_CREDITMATRICESBYAPPTYPE
    if(view_mode!=VIEWMODE_CREDITMATRICESBYAPPTYPE && app_type>0) {
      if(bAssSpace)
        sb.append("<td width=8>&nbsp;|&nbsp;</td>");
      sb.append("<td class=\"smallText\"><a href=\"/jsp/creditmatrix/cmedit.jsp?view_mode="+VIEWMODE_CREDITMATRICESBYAPPTYPE+"\">Credit Matrices</a></td>");
      bAssSpace=true;
    }
    */

    // VIEWMODE_CREDITMATRIX_ADD
    if(view_mode!=VIEWMODE_CREDITMATRIX_ADD) {
      if(bAssSpace)
        sb.append("<td width=8>&nbsp;|&nbsp;</td>");
      sb.append("<td class=\"smallText\"><a href=\"/jsp/creditmatrix/cmedit.jsp?view_mode="+VIEWMODE_CREDITMATRIX_ADD+"\">Add Matrix</a></td>");
      bAssSpace=true;
    }

    // VIEWMODE_CREDITMATRIX_EDIT
    if(cm_id>0 && view_mode!=VIEWMODE_CREDITMATRIX_EDIT) {
      if(bAssSpace)
        sb.append("<td width=8>&nbsp;|&nbsp;</td>");
      sb.append("<td class=\"smallText\"><a href=\"/jsp/creditmatrix/cmedit.jsp?view_mode="+VIEWMODE_CREDITMATRIX_EDIT+"\">Edit current Matrix</a></td>");
      bAssSpace=true;
    }
    
    // VIEWMODE_CREDITMATRIX_COPY
    if(cm_id>0 && view_mode!=VIEWMODE_CREDITMATRIX_COPY) {
      if(bAssSpace)
        sb.append("<td width=8>&nbsp;|&nbsp;</td>");
      sb.append("<td class=\"smallText\"><a href=\"/jsp/creditmatrix/cmedit.jsp?view_mode="+VIEWMODE_CREDITMATRIX_COPY+"\">Copy Matrix</a></td>");
      bAssSpace=true;
    }

    // VIEWMODE_CREDITMATRIXELEMENT_ADD
    if(cm_id>0 && view_mode!=VIEWMODE_CREDITMATRIXELEMENT_ADD) {
      if(bAssSpace)
        sb.append("<td width=8>&nbsp;|&nbsp;</td>");
      sb.append("<td class=\"smallText\"><a href=\"/jsp/creditmatrix/cmedit.jsp?view_mode="+VIEWMODE_CREDITMATRIXELEMENT_ADD+"\">Add Matrix Element</a></td>");
      bAssSpace=true;
    }

    // VIEWMODE_CREDITMATRIXELEMENT_EDIT
    if(cme_id>0 && view_mode!=VIEWMODE_CREDITMATRIXELEMENT_EDIT) {
      if(bAssSpace)
        sb.append("<td width=8>&nbsp;|&nbsp;</td>");
      sb.append("<td class=\"smallText\"><a href=\"/jsp/creditmatrix/cmedit.jsp?view_mode="+VIEWMODE_CREDITMATRIXELEMENT_EDIT+"\">Edit current Matrix Element</a></td>");
      bAssSpace=true;
    }
    
    sb.append("</tr>");
    sb.append("</table>");
    
    return sb.toString();
  }

  public boolean isValid()
  {
    switch(view_mode) {
      case VIEWMODE_CREDITMATRIX_ADD:
      case VIEWMODE_CREDITMATRIX_EDIT:
        return ((FieldGroup)fields.getField("CreditMatrix")).isValid();
      case VIEWMODE_CREDITMATRIXELEMENT_ADDSUB:
      case VIEWMODE_CREDITMATRIXELEMENT_EDIT:
        return ((FieldGroup)fields.getField("CreditMatrixElement")).isValid();
      default:
        return true;
    }
  }
  
  public final void doRequest(HttpServletRequest request)
  {
    if(request==null)
      return;

    this.request=request;

    // get request data
    if(request.getParameter("app_type")!=null)
      app_type=StringUtilities.stringToInt(request.getParameter("app_type"),0);
    if(request.getParameter("app_type_desc")!=null)
      app_type_desc=request.getParameter("app_type_desc");
    if(request.getParameter("cm_id")!=null)
      cm_id=StringUtilities.stringToLong(request.getParameter("cm_id"),0);
    if(request.getParameter("cme_id")!=null)
      cme_id=StringUtilities.stringToLong(request.getParameter("cme_id"),0);
    if(request.getParameter("app_seq_num")!=null) {
      app_seq_num=StringUtilities.stringToLong(request.getParameter("app_seq_num"),0);
      log.debug("doRequest() app_seq_num="+app_seq_num);
    }
    
    // perform op if one specified
    log.debug("request.getParameter(method)="+request.getParameter("method"));
    String method = request.getParameter("method");
    if(method!=null && method.length()>0) {

      // pull data
      setFields(request);
      
      if(method.equals("Continue")) {
        // this is step 1 in a 2 step process of adding a matrix element
        preAddCreditMatrixElement();
      } else {
        if(!isValid()) {
          prepTextMessages();
          return;
        }
      }
    
      if(method.equals("Add Matrix")) {
        addCreditMatrix();
      } else if(method.equals("Update Matrix")) {
        updateCreditMatrix();
      } else if(method.equals("Delete Matrix")) {
        deleteCreditMatrix();
        view_mode=VIEWMODE_APPTYPES;
      } else if(method.equals("Copy Matrix")) {
        copyCreditMatrix();
      } else if(method.equals("Add Matrix Element")) {
        addCreditMatrixElement();
      } else if(method.equals("Update Matrix Element")) {
        updateCreditMatrixElement();
      } else if(method.equals("Delete Matrix Element")) {
        deleteCreditMatrixElement();
        view_mode=VIEWMODE_CREDITMATRIX_EDIT;
        // NOTE: assume cm_id exists
      } else if(method.equals("Calculate Score")) {
        calculateCreditMatrixScore();
      }
    }

    // set view mode
    log.debug("request.getParameter(view_mode)="+request.getParameter("view_mode"));
    if(request.getParameter("view_mode")!=null)
      view_mode=StringUtilities.stringToInt(request.getParameter("view_mode"),VIEWMODE_START);

    // push data
    switch(view_mode) {
      case VIEWMODE_CREDITMATRIX_EDIT:
      {
        Field fld;
        FieldGroup fg = (FieldGroup)fields.getField("CreditMatrix");

        if((cm = getDAOFactory().getCreditMatrixDAO().findCreditMatrix(cm_id))==null)
          break;

        fld=fg.getField("Application Type");
        fld.setData(Integer.toString(cm.getAppType()));
        fld=fg.getField("Matrix Name");
        fld.setData(cm.getName());
        fld=fg.getField("Auto-approve Threshold");
        fld.setData(Long.toString(cm.getAutoApproveThreshold()));
        fld=fg.getField("Auto-decline Threshold");
        fld.setData(Long.toString(cm.getAutoDeclineThreshold()));

        break;
      }
      
      case VIEWMODE_CREDITMATRIXELEMENT_EDIT:
      {
        Field fld;
        FieldGroup fg = (FieldGroup)fields.getField("CreditMatrixElement");
        
        log.debug("VIEWMODE_CREDITMATRIXELEMENT_EDIT cme_id="+cme_id);
        if((cme = getDAOFactory().getCreditMatrixElementDAO().findCreditMatrixElement(cme_id))==null) {
          log.debug(cme==null? "cme IS NULL":"cme is NOT null");
          break;
        }

        /*
        fld=fg.getField("Name");
        fld.setData(cme.getName());
        fld=fg.getField("Label");
        fld.setData(cme.getLabel());
        */
        
        // NOTE: The cme type is not alterable

        fld=fg.getField("Type");
        fld.setData(Integer.toString(cme.getType()));

        if(cme.getType()==CreditMatrixElement.CMETYPE_APPWEIGHT) {
          fld=fg.getField("Mib_Accessor");
          fld.setData(((CreditMatrixElement_App)cme).getMibAccessor());
          fld=fg.getField("Weight");
          fld.setData(Float.toString(((CreditMatrixElement_AppWeight)cme).getWeight()));
        } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPOFFSET) {
          fld=fg.getField("Mib_Accessor");
          fld.setData(((CreditMatrixElement_App)cme).getMibAccessor());
          fld=fg.getField("Offset");
          fld.setData(Float.toString(((CreditMatrixElement_AppOffset)cme).getOffset()));
        } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPSTRINGMATCH) {
          fld=fg.getField("Mib_Accessor");
          fld.setData(((CreditMatrixElement_App)cme).getMibAccessor());
          fld=fg.getField("Match String");
          fld.setData(((CreditMatrixElement_AppStringMatch)cme).getMatchstring());
        } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPNUMRANGE) {
          fld=fg.getField("Mib_Accessor");
          fld.setData(((CreditMatrixElement_App)cme).getMibAccessor());
          fld=fg.getField("Numeric Range");
          fld.setData(((CreditMatrixElement_AppNumRange)cme).getNumRange());
          fld=fg.getField("Is Bounded");
          fld.setData(((CreditMatrixElement_AppNumRange)cme).isNumRangeBounded()? "y":"n");
        } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPCREDITSCORE) {
          fld=fg.getField("Numeric Range");
          fld.setData(((CreditMatrixElement_CreditScore)cme).getNumRange());
          fld=fg.getField("Red Flag Threshold");
          fld.setData(Integer.toString(((CreditMatrixElement_CreditScore)cme).getRedFlagThreshold()));
          fld=fg.getField("Is Bounded");
          fld.setData(((CreditMatrixElement_CreditScore)cme).isNumRangeBounded()? "y":"n");
        }

        break;
      }
      
    }
  
  }

  public String getHeadingName()
  {
    StringBuffer sb=new StringBuffer(256);
    
    switch(view_mode) {
      case VIEWMODE_APPTYPES:
        sb.append("Application Types");
        break;
      case VIEWMODE_SCORE:
        sb.append("Application Scoring");
        break;
      case VIEWMODE_CREDITMATRICESBYAPPTYPE:
        sb.append("Credit Matrices for Application Type '");
        sb.append(app_type_desc);
        sb.append("'");
        break;
      case VIEWMODE_CREDITMATRIX_ADD:
        sb.append("Add Matrix");
        break;
      case VIEWMODE_CREDITMATRIX_EDIT:
        sb.append("Edit Matrix");
        break;
      case VIEWMODE_CREDITMATRIX_COPY:
        sb.append("Copy Matrix");
        break;
      case VIEWMODE_CREDITMATRIXELEMENT_ADD:
      case VIEWMODE_CREDITMATRIXELEMENT_ADDSUB:
        sb.append("Add Matrix Element");
        sb.append(" to Matrix '");
        sb.append(cm.getName());
        sb.append("'");
        break;
      case VIEWMODE_CREDITMATRIXELEMENT_EDIT:
        sb.append("Edit Matrix Element ");
        sb.append(" for Matrix '");
        sb.append(cm.getName());
        sb.append("'");
        break;
    }

    return sb.toString();
  }

  /**
   * prepTextMessages()
   * 
   * Transfers FieldBean based messages (usu. error messages) to the TextMessageManager.
   */
  private final void prepTextMessages()
  {
    Vector fldErrors = null;
    
    // pump out field level errors by current field group
    switch(view_mode) {
      case VIEWMODE_CREDITMATRIX_ADD:
      case VIEWMODE_CREDITMATRIX_EDIT:
        fldErrors = ((FieldGroup)fields.getField("CreditMatrix")).getErrors();
        break;
      case VIEWMODE_CREDITMATRIXELEMENT_ADDSUB:
      case VIEWMODE_CREDITMATRIXELEMENT_EDIT:
        fldErrors = ((FieldGroup)fields.getField("CreditMatrixElement")).getErrors();
        break;
    }
    log.debug("prepTextMessages() Num field level errors="+(fldErrors==null? 0:fldErrors.size()));
    if(fldErrors!=null && fldErrors.size()>0) {
      for(Enumeration e=fldErrors.elements();e.hasMoreElements();)
        tmm.err((String)e.nextElement());
    }

    // pump out bean level errors (manual errors)
    log.debug("prepTextMessages("+this.getClass().getName()+") Num bean errors="+errors.size());
    if(errors.size()>0) {
      for(Enumeration e=errors.elements();e.hasMoreElements();)
        tmm.err((String)e.nextElement());
    }
    
  }
  
}/*@lineinfo:generated-code*/