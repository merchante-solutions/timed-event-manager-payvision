/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tags/TemplateUseTag.java $

  Description:
  
  TemplateUseTagTag
  
  Expands a template around the file where the tag occured.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.tags;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

public class TemplateUseTag extends TagSupport
{
  private TemplateStack tStack;
  
  private boolean inSelf;
  
  private String template;
  private String title;
  private String name;
  
  public String getTemplate() { return template; }
  public String getTitle()    { return title; }
  public String getName()     { return name; }
  
  public void setTemplate(String s) { template = s; }
  public void setTitle(String s)    { title = s; }
  public void setName(String s)     { name = s; }


  private String curPath;
  
  private String getCurPath()
  {
    if (curPath == null)
    {
      curPath = (String)pageContext
        .getAttribute("template_cur_path",PageContext.REQUEST_SCOPE);
        
      if (curPath == null)
      {
        curPath = ((HttpServletRequest)pageContext
          .getRequest()).getServletPath();
      }
    }
      
    return curPath;
  }
      
  public int doStartTag() throws JspException
  {
    getCurPath();
    
    // look for existing template stack, create it if necessary
    tStack = (TemplateStack)pageContext
      .getAttribute("template_stack",PageContext.REQUEST_SCOPE);
    if (tStack == null)
    {
      tStack = new TemplateStack();
      pageContext
        .setAttribute("template_stack",tStack,PageContext.REQUEST_SCOPE);
    }
    
    // generate a stack frame that corresponds with this temlate
    TemplateStackFrame tFrame = new TemplateStackFrame(curPath + "." + name);
    
    // check to see if a stack frame for 
    // this template is on the stack already
    if (tStack.search(tFrame) != -1)
    {
      // template included original file,
      // so turn on the in self flag and
      // skip body
      inSelf = true;
      return SKIP_BODY;
    }
    else
    {
      // first time encountering this template in 
      // this page, so turn off in self flag and
      // continue
      inSelf = false;
    }
    
    // place the parent path and the title into the frame
    tFrame.put("parent_page",new TemplatePageParameter(curPath,"false"));
    tFrame.put("parent_title",new TemplatePageParameter(title,"true"));
    
    // push the new stack frame onto the stack
    tStack.push(tFrame);

    // evaluate the body
    return EVAL_BODY_INCLUDE;
  }
    
  public int doEndTag() throws JspException
  {
    // if already processed self, then just 
    // continue evaluating the page
    if (inSelf)
    {
      return EVAL_PAGE;
    }

    // expand the template since not in self
    try
    {
      pageContext
        .setAttribute("template_cur_path",template,PageContext.REQUEST_SCOPE);
      pageContext.include(template);
      pageContext
        .setAttribute("template_cur_path",curPath,PageContext.REQUEST_SCOPE);
    }
    catch (Exception e)
    {
      throw new JspException(e.getMessage());
    }
    
    // pop the stack frame off the stack
    tStack.pop();

    
    // skip the rest of the page (loop avoided here)
    return SKIP_PAGE;
  }
  
  public void release()
  {
    template = null;
  }
}
