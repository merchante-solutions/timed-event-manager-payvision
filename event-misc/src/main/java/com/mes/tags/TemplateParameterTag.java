/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tags/TemplateParameterTag.java $

  Description:
  
  TemplateParameterTag
  
  Places a string into the request as an attribute.  Useful for passing
  information from a content pages into templates. 
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

public class TemplateParameterTag extends TagSupport
{
  private String name;
  private String value;

  // setters
  public void setName(String s)     { name  = s; }
  public void setvalue(String s)    { value = s; }

  public int doStartTag() throws JspException
  {
    // place the name value into the request as an attribute
    pageContext.setAttribute(name,value,PageContext.REQUEST_SCOPE);

    // not interested in tag body, skip it
    return SKIP_BODY; 
  }

  // tag handlers should always implement release() because
  // handlers can be reused by the JSP container
  public void release()
  {
    name = value = null;
  }
}
