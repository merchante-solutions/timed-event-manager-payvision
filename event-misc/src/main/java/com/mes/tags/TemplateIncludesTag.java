package com.mes.tags;

import java.util.Iterator;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import org.apache.log4j.Logger;

public class TemplateIncludesTag extends TemplateTag
{
  static Logger log = Logger.getLogger(TemplateIncludesTag.class);

  private String curPath;
  private String name;

  public void setName(String name)
  {
    this.name = name;
  }
  public String getName()
  {
    return name;
  }
  
  /*
  ** private String getCurPath() throws JspException
  **
  ** The current path is retrieved from the request.
  **
  ** Throws JspException if path is not found.
  */
  private String getCurPath() throws JspException
  {
    if (curPath == null)
    {
      curPath = ((String)pageContext
        .getAttribute("template_cur_path",PageContext.REQUEST_SCOPE));
        
      if (curPath == null)
      {
        throw new JspException("TemplateIncludesTag.getCurPath(): path not found");
      }
    }
      
    return curPath;
  }
      
  /*
  ** public TemplateStackFrame getFrame() throws JspException
  **
  ** Locates the template stack frame in the request.
  **
  ** Throws JspException if the frame isn't found.
  */
  public TemplateStackFrame getFrame() throws JspException
  {
    TemplateStack tStack = (TemplateStack)pageContext
      .getAttribute("template_stack",PageContext.REQUEST_SCOPE);
    if (tStack == null)
    {
      throw new JspException(
        "TemplateIncludesTag.getFrame(): no template stack in request");
    }
    
    TemplateStackFrame tFrame = (TemplateStackFrame)tStack.peek();
    if (tFrame == null)
    {
      throw new JspException(
        "TemplateIncludesTag.getFrame(): no frame on stack");
    }
    
    return tFrame;
  }

  private void includeCss(String cssName)
  {
    try
    {
      StringBuffer html = new StringBuffer();
      html.append("    <link rel=\"stylesheet\" type=\"text/css\" ");
      html.append("href=\"" + cssName + "\" />");
      html.append("\n");
      pageContext.getOut().print(html.toString());
    }
    catch (Exception e)
    {
      log.error("Error including css file " + cssName + ": " + e);
    }
  }

  private void includeJavaScript(String jsName)
  {
    try
    {
      StringBuffer html = new StringBuffer();
      html.append("    <script language=\"JavaScript\" type=\"text/javascript\" ");
      html.append("src=\"" + jsName + "\"></script>");
      html.append("\n");
      pageContext.getOut().print(html.toString());
    }
    catch (Exception e)
    {
      log.error("Error including javascript file " + jsName + ": " + e);
    }
  }
  
  /*
  ** public int doStartTag() throws JspException
  **
  ** Locates the template stack frame and attempts to retrieve the
  ** names page parameter from it.  If found it either outputs it
  ** directly or includes it as a page, based on the isDirect flag
  ** of the parm.
  **
  ** Throws JspException if stack frame can't be located.
  */
  public int doStartTag() throws JspException
  {
    try
    {
      getCurPath();

      List includes = (List)pageContext.getAttribute(name,PageContext.REQUEST_SCOPE);
      if (includes == null)
      {
        String warning = "Include list with name '" + name + "' not found in request";
        log.warn(warning);
        pageContext.getOut().print("    <!-- " + warning + " -->\n");
      }
      else
      {
        // generate tags to include styles, scripts, etc. in html header
        for (Iterator i = includes.iterator(); i.hasNext();)
        {
          String include = (String)i.next();
          if (include.endsWith(".css"))
          {
            includeCss(include);
          }
          else if (include.endsWith(".js"))
          {
            includeJavaScript(include);
          }
          else
          {
            String warning = "Unsupported include type: '" + include + "'";
            log.warn(warning);
            pageContext.getOut().print("    <!-- " + warning + " -->\n");
          }
        }
      }
    }
    catch (Exception e)
    {
      throw new JspException("Error in doStartTag(): " + e);
    }

    return SKIP_BODY; 
  }
  
  // tag handlers should always implement release() because
  // handlers can be reused by the JSP container
  public void release()
  {
  }
}