/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tags/SecurityRequireTag.java $

  Description:
  
  SecurityRequireTag
  
  Determines if a user entity in the session scope holds the rights
  specified by nested set tags.
  
  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.tags;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import com.mes.support.HttpHelper;
import com.mes.support.RequestedPageBean;
import com.mes.user.ClientLoginCookie;
import com.mes.user.UserBean;

public class SecurityRequireTag extends SecurityContainerTag
{
  public int doEndTag() throws JspException
  {
    HttpServletResponse response =
      (HttpServletResponse)pageContext.getResponse();
    HttpServletRequest request =
      (HttpServletRequest)pageContext.getRequest();
      
    // get the user object from the session
    UserBean user = (UserBean)pageContext
      .getAttribute("UserLogin",pageContext.SESSION_SCOPE);
      
    // if user object not yet present, redirect to login page
    if (user == null)
    {
      try
      {
        // look for the requested page object in the session
        RequestedPageBean requestedPage = (RequestedPageBean)pageContext
          .getAttribute("RequestedPage",pageContext.SESSION_SCOPE);
        
        // create the requested page object if it isn't 
        // present in the session
        if (requestedPage == null)
        {
          requestedPage = new RequestedPageBean();
          pageContext.getSession()
            .setAttribute("RequestedPage",requestedPage);
        }
        
        // store the requested url in the requeste page url
        // so that the login page can redirect here if login
        // is required and is successful
        requestedPage.setRequestedPage(request);
        
        // try to determine the users login page from a client cookie object
        ClientLoginCookie clientCookie = ClientLoginCookie
          .getLoginCookie(request);
        StringBuffer loginPage = new StringBuffer("");
        if(clientCookie != null)
        {
          // client cookie found, use cookie login page
          loginPage.append(clientCookie.getLoginPage());
        }
        else
        {
          // no login cookie, use default login page
          loginPage.append("/jsp/sLogin.jsp");
          
          //loginPage.append(
          //   MesDefaults.getString(MesDefaults.DK_RAP_LOGIN_PAGE));
        }
        
        // append requested parameters to allow auto-login
        if (requestedPage.getQueryString() != null 
            && !requestedPage.getQueryString().equals(""))
        {
          loginPage.append("?");
          loginPage.append(requestedPage.getQueryString());
        }
  
        // redirect the client to the login page
        response.sendRedirect(loginPage.toString());

        return SKIP_PAGE;
      }
      catch (Exception e)
      {
        throw new JspException(e.getMessage());
      }
    }

    // check user permissions
    if (!getPermissions().hasPermissions(user,request))
    {
      try
      {
        // forward user to insufficient rights page
        response.sendRedirect(response.encodeRedirectURL(
          "/jsp/secure/InsufficientAccess.jsp"));

        //response.sendRedirect(
        //  MesDefaults.getString(MesDefaults.DK_RAP_INSUFFICIENT_RIGHTS_PAGE));
        
        return SKIP_PAGE;
      }
      catch (Exception e)
      {
        throw new JspException(e.getMessage());
      }
    }
    
    // make sure the user is using SSL in production
    if( request.getServerPort() != 443 && 
        (HttpHelper.isProdServer(request) || HttpHelper.doesUseSSL(request)))
    {
      try
      {
        // send them to ssl
        StringBuffer  sslPage = new StringBuffer(HttpHelper.getServerURL(request));
  
        sslPage.append(request.getServletPath());
  
        if(request.getQueryString() != null && !request.getQueryString().equals(""))
        {
          sslPage.append("?");
          sslPage.append(request.getQueryString());
        }
        response.sendRedirect(sslPage.toString());
      
        return SKIP_PAGE;
      }
      catch (Exception e)
      {
        throw new JspException(e.getMessage());
      }
    }
    
    return EVAL_PAGE;
  }
}
