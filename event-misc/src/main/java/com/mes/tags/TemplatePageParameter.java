package com.mes.tags;

public class TemplatePageParameter
{
  private String content;
  private boolean direct;

  public TemplatePageParameter(String content, String directStr)
  {
    this.content = content;
    setDirect(directStr);
  }
  
  public TemplatePageParameter(String content, boolean direct)
  {
    this.content = content;
    this.direct = direct;
  }
  
  public void setContent(String content)
  {
    this.content = content;
  }
  
  public void setDirect(String directStr)
  {
    direct = Boolean.valueOf(directStr).booleanValue();
  }
  public void setDirect(boolean direct)
  {
    this.direct = direct;
  }

  public String getContent() { return content;}
  public boolean isDirect() { return direct; }
}
