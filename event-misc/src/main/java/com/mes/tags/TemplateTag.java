/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tags/TemplateTag.java $

  Description:
  
  TemplateTag
  
  Base class for template classes.  Defines a getAncestor routine that
  allows template objects to find their containing ancestors.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class TemplateTag extends TagSupport
{
  // convenience method for finding ancestor names with
  // a specific class name
  protected TagSupport getAncestor(String className) throws JspException
  {
    // can't name variable "class"
    Class klass = null;
    try
    {
      klass = Class.forName(className);
    }
    catch(ClassNotFoundException ex)
    {
      throw new JspException(ex.getMessage());
    }
    
    return (TagSupport)findAncestorWithClass(this, klass);
  }
}
