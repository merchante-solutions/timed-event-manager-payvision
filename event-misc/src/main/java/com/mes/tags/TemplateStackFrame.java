package com.mes.tags;

import java.util.Hashtable;

public class TemplateStackFrame extends Hashtable
{
  private String id;
  
  public TemplateStackFrame(String id)
  {
    this.id = id;
  }
  
  public String getId() { return id; }
  
  public boolean equals(Object o)
  {
    if (o instanceof TemplateStackFrame)
    {
      return ((TemplateStackFrame)o).getId().equals(getId());
    }
    return false;
  }
}