/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tags/TemplateExpandTag.java $

  Description:
  
  TemplateExpandTag
  
  Expands a layout file, will be invoked from a template file.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

public class TemplateExpandTag extends TagSupport
{
  private TemplateStackFrame tFrame;
  
  private String layout;
  
  public void setLayout(String s) { layout = s; }
  
  private String curPath;
  
  /*
  ** private String getCurPath() throws JspException
  **
  ** The current path is retrieved from the request.
  **
  ** Throws JspException if path is not found.
  */
  private String getCurPath() throws JspException
  {
    if (curPath == null)
    {
      curPath = ((String)pageContext
        .getAttribute("template_cur_path",PageContext.REQUEST_SCOPE));
        
      if (curPath == null)
      {
        throw new JspException(
          "TemplateExpandTag.getCurPath(): path not found");
      }
    }
      
    return curPath;
  }
      
  /*
  ** public TemplateStackFrame getFrame() throws JspException
  **
  ** Locates the template stack frame in the request.
  **
  ** Throws JspException if the frame isn't found.
  */
  public TemplateStackFrame getFrame() throws JspException
  {
    if (tFrame == null)
    {
      TemplateStack tStack = (TemplateStack)pageContext
        .getAttribute("template_stack",PageContext.REQUEST_SCOPE);
      if (tStack == null)
      {
        throw new JspException(
          "TemplateExpandTag.getFrame(): no template stack in request");
      }
      
      tFrame = (TemplateStackFrame)tStack.peek();
      if (tFrame == null)
      {
        throw new JspException(
          "TemplateExpandTag.getFrame(): no frame on stack");
      }
    }
    
    return tFrame;
  }
  
  /*
  ** public int doStartTag() throws JspException
  **
  ** Locates the template stack frame, returns eval body.
  **
  ** Throws JspException if stack frame cannot be located.
  */
  public int doStartTag() throws JspException
  {
    // get the current path
    getCurPath();
    
    // get the stack frame
    getFrame();
    
    // evaluate nested tags in the body
    return EVAL_BODY_INCLUDE;  
  }

  /*
  ** public int doEndTag() throws JspException
  **
  ** Attempts to include a layout file and then has rest of page processed.
  */
  public int doEndTag() throws JspException
  {
    try
    {
      // include template
      pageContext.setAttribute("template_cur_path",
        layout,PageContext.REQUEST_SCOPE);
      pageContext.include(layout); 
      pageContext.setAttribute("template_cur_path",
        curPath,PageContext.REQUEST_SCOPE);
    }
    // IOException or ServletException
    catch(Exception e) 
    { 
      // recast exception
      throw new JspException(e.getMessage()); 
    }

    // evaluate the rest of the page after the tag
    return EVAL_PAGE; 
  }
  
  public void release()
  {
    layout = null;
    tFrame = null;
  }
}
