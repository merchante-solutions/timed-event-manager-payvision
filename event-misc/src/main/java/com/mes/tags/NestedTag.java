package com.mes.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.TagSupport;

public class NestedTag extends BodyTagSupport
{
  // convenience method for finding ancestor names with
  // a specific class name
  protected TagSupport getAncestor(String className) throws JspException
  {
    // can't name variable "class"
    Class klass = null;
    try
    {
      klass = Class.forName(className);
    }
    catch(ClassNotFoundException e)
    {
      throw new JspException(e.getMessage());
    }
    
    return (TagSupport)findAncestorWithClass(this, klass);
  }
}
