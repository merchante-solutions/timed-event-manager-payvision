/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tags/SecurityRightTag.java $

  Description:
  
  SecurityRightTag
  
  Adds a right to the requirements set.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.tags;

import javax.servlet.jsp.JspException;
import com.mes.user.Permissions;

public class SecurityRightTag extends NestedTag
{
  /*
  ** public int doStartTag() throws JspException
  **
  ** Looks for the presence of a name parameter containing a rap right
  ** label.  If found, the right is added to the rap rights container
  ** and the body is skipped.  If not, the body is evaluated so that
  ** an mes right id can be looked for.
  **
  ** RETURNS: SKIP_BODY if a rap right is found, EVAL_BODY_BUFFERED if not.
  */
  public int doStartTag() throws JspException
  {
    return EVAL_BODY_BUFFERED;
  }
  
  /*
  ** public int doAfterBody() throws JspException
  **
  ** If this is called then no rap right was found.  The body of the tag
  ** is assumed to contain an mes right id.  The id is added to the 
  ** mes permissions container in the parent container tag.
  **
  ** RETURNS: SKIP_BODY.
  */
  public int doAfterBody() throws JspException
  {
    String body = getBodyContent().getString();

    // throw exception if no body found
    if (body == null || body.length() == 0)
    {    
      throw new JspException("SecurityRightTag.doStartTag(): " +
                             "no right in body");
    }
    
    // get the requirements container from the enclosing require tag
    SecurityContainerTag parent = 
      (SecurityContainerTag)getAncestor("com.mes.tags.SecurityContainerTag");
    if (parent == null)
    {
      throw new JspException("SecurityRightTag.doStartTag(): " +
                             "no SecurityContainerTag parent");
    }
    
    // try to add the body as a mes right id
    try
    {
      int rightId = Integer.parseInt(body);
      Permissions permissions = parent.getPermissions();
      permissions.allowRight(rightId);
    }
    catch (Exception e)
    {
      throw new JspException("Unable to convert body of tag to right id: "
        + e.toString());
    }
    
    return SKIP_BODY;
  }
}
