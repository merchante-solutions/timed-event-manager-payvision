/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tags/TemplateSetTag.java $

  Description:
  
  TemplateSetTag
  
  Places a template component into the template hash table at the top of 
  the template stack.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.tags;

import javax.servlet.jsp.JspException;

public class TemplateSetTag extends NestedTag
{
  private String name;
  private String content;
  private String direct     =   "false";

  // setter methods for Put tag attributes
  public void setName(String s)     { name    = s; }
  public void setContent(String s)  { content = s; }
  public void setDirect(String s)   { direct  = s; }

  public int doStartTag() throws JspException
  {
    // get the stack from the enclosing expand tag
    TemplateExpandTag parent = 
      (TemplateExpandTag)getAncestor("com.mes.tags.TemplateExpandTag");
    if (parent == null)
    {
      throw new JspException("TemplateSetTag.doStartTag(): " +
                             "no TemplateExpandTag parent");
    }
    TemplateStackFrame tFrame = parent.getFrame();

    // put a new TemplatePageParameter in the hashtable
    tFrame.put(name, new TemplatePageParameter(content, direct));

    // not interested in tag body, skip it
    return SKIP_BODY; 
  }

  // tag handlers should always implement release() because
  // handlers can be reused by the JSP container
  public void release()
  {
    name = content = direct = null;
  }
}
