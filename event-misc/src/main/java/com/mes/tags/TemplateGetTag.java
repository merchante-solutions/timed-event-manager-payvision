/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tags/TemplateGetTag.java $

  Description:
  
  TemplateGetTag
  
  Retrieves template data, either directly outputting a string to output
  or including a file.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

public class TemplateGetTag extends TemplateTag
{
  private String name;

  public void setName(String s) { name = s; }
  
  private String curPath;
  
  /*
  ** private String getCurPath() throws JspException
  **
  ** The current path is retrieved from the request.
  **
  ** Throws JspException if path is not found.
  */
  private String getCurPath() throws JspException
  {
    if (curPath == null)
    {
      curPath = ((String)pageContext
        .getAttribute("template_cur_path",PageContext.REQUEST_SCOPE));
        
      if (curPath == null)
      {
        throw new JspException("TemplateGetTag.getCurPath(): path not found");
      }
    }
      
    return curPath;
  }
      
  /*
  ** public TemplateStackFrame getFrame() throws JspException
  **
  ** Locates the template stack frame in the request.
  **
  ** Throws JspException if the frame isn't found.
  */
  public TemplateStackFrame getFrame() throws JspException
  {
    TemplateStack tStack = (TemplateStack)pageContext
      .getAttribute("template_stack",PageContext.REQUEST_SCOPE);
    if (tStack == null)
    {
      throw new JspException(
        "TemplateGetTag.getFrame(): no template stack in request");
    }
    
    TemplateStackFrame tFrame = (TemplateStackFrame)tStack.peek();
    if (tFrame == null)
    {
      throw new JspException(
        "TemplateGetTag.getFrame(): no frame on stack");
    }
    
    return tFrame;
  }
  
  /*
  ** public int doStartTag() throws JspException
  **
  ** Locates the template stack frame and attempts to retrieve the
  ** names page parameter from it.  If found it either outputs it
  ** directly or includes it as a page, based on the isDirect flag
  ** of the parm.
  **
  ** Throws JspException if stack frame can't be located.
  */
  public int doStartTag() throws JspException
  {
    getCurPath();
    
    // get the template stack frame
    TemplateStackFrame tFrame = getFrame();
    
    // get page parameter from the frame hash
    TemplatePageParameter pageParm 
      = (TemplatePageParameter)tFrame.get(name);
    if (pageParm != null)
    {
      String content = pageParm.getContent();

      if (pageParm.isDirect())
      {
        // print content if direct attribute is true
        try
        {
          pageContext.getOut().print(content);
        }
        catch(java.io.IOException ex)
        {
          throw new JspException(ex.toString());
        }
      }
      else
      {
        // include content if direct attribute is false
        try
        {
          pageContext.setAttribute("template_cur_path",
            content,PageContext.REQUEST_SCOPE);
          pageContext.include(content);
          pageContext.setAttribute("template_cur_path",
            curPath,PageContext.REQUEST_SCOPE);
        }
        catch(Exception ex)
        {
          System.out.println("Error in get tag: " + ex);
          ex.printStackTrace();
          throw new JspException(ex.toString());
        }
      }
    }

    // not interested in tag body, if present
    return SKIP_BODY; 
  }
  
  // tag handlers should always implement release() because
  // handlers can be reused by the JSP container
  public void release()
  {
    name = null;
  }
}