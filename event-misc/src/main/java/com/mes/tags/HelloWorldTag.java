package com.mes.tags;

import java.io.IOException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class HelloWorldTag extends TagSupport
{
  public int doStartTag()
  {
    try
    {
      JspWriter out = pageContext.getOut();
      out.print("Hello World!");
    }
    catch(IOException ioe)
    {
      System.out.println("Error in HelloWorldTag::doStartUp(): " + ioe);
    }
    
    return(SKIP_BODY);
  }
}
