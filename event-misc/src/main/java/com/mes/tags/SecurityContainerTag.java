/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tags/SecurityContainerTag.java $

  Description:
  
  SecurityContainerTag
  
  Base class for tags that need to define a set of security entity
  objects.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import com.mes.user.Permissions;

public class SecurityContainerTag extends TagSupport
{
  protected Permissions permissions;
  
  public Permissions getPermissions() throws JspException
  {
    // create permissions if not already there
    if (permissions == null)
    {
      permissions = new Permissions();
    }
    return permissions;
  }
  
  public int doStartTag() throws JspException
  {
    return EVAL_BODY_INCLUDE;
  }
  
  public void release()
  {
    permissions = null;
  }
}
