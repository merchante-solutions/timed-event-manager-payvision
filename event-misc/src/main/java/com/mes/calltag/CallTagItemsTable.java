package com.mes.calltag;

import java.util.Iterator;
import java.util.List;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.constants.mesConstants;
import com.mes.equipment.EquipmentDataBase;
import com.mes.tools.EquipmentKey;
import com.mes.tools.Key;
import com.mes.tools.TableView;
import com.mes.tools.TableViewHelper;


public final class CallTagItemsTable
  implements TableView
{
  // create class log category
  static Category log = Category.getInstance(CallTagItemsTable.class.getName());

  public static final String SERVLET_URL = "/srv/calltag";
  
  // constants
  private static final String[][] columnData = 
    {
       {"Equipment",              null}
      ,{"Serial Number",          "getSerialNum"}
      ,{"Status",                 "getStatusDesc"}
      ,{"Action",                 null}
    };
  
  // data members
  protected List              rows                = null;
  protected TableViewHelper   helper              = null;
  protected String            detailPageBaseUrl   = null;
  protected String            queryStringSuffix   = null;
  
  // construction
  public CallTagItemsTable(List rows, String detailPageBaseUrl, String queryStringSuffix)
  {
    this.rows = rows;
    this.helper = new TableViewHelper(CallTagItem.class, this, columnData);
    this.detailPageBaseUrl = detailPageBaseUrl;
    this.queryStringSuffix = queryStringSuffix;
  }
  
  public List           getRows()
  {
    return rows;
  }
  
  public void           setRows(List rows)
  {
    this.rows = rows;
  }
  
  public Key            getRowKey(int rowOrdinal)
  {
    CallTagItem obj = (CallTagItem)getRowObject(rowOrdinal);
    return (Key)obj.getEquipmentKey();
  }

  public Object         getRowObject(int rowOrdinal)
  {
    return helper.getRowObject(rowOrdinal);
  }

  public int            getNumRows()
  {
    return rows.size();
  }
  
  public int            getNumCols()
  {
    return columnData.length;
  }
  
  public String         getLinkToDetailColumnName()
  {
    return "Equipment";
  }
  
  public String         getDetailPageBaseUrl()
  {
    return detailPageBaseUrl;
  }
    
  public String         getQueryStringSuffix()
  {
    return queryStringSuffix;
  }
  
  public void           setQueryStringSuffix(String v)
  {
    queryStringSuffix=v;
  }
  
  public String getDetailPageQueryString(int rowOrdinal)
  {
    CallTagBase ctb = (CallTagBase)getRowObject(rowOrdinal);
    
    StringBuffer sb = new StringBuffer();
    sb.append("method=vecti&id=");
    sb.append(ctb.getId());
    
    return sb.toString();
  }

  public Iterator       getRowIterator()
  {
    return rows.iterator();
  }
  
  public String         getColumnName(int colOrdinal)
  {
    return helper.getColumnName(colOrdinal);
  }
  
  public String[]       getColumnNames()
  {
    return helper.getColumnNames();
  }
  
  public String         getCell(int rowOrdinal, int colOrdinal)
  {
    StringBuffer cellHtml = new StringBuffer();
    CallTagItem tagItem = (CallTagItem)getRowObject(rowOrdinal);
    EquipmentKey key = tagItem.getEquipmentKey();
      
    if (getColumnName(colOrdinal).equals("Equipment"))
    {
      boolean inInventory = false;
      try
      {
        inInventory=key.exists();
      }
      catch(Exception e) { }
      
      if (inInventory)
      {
        EquipmentDataBase equipData = tagItem.getEquipmentData();
        if (equipData != null)
        {
          cellHtml.append(equipData.getDescriptor());
        }
        else
        {
          cellHtml.append(key.toString());
        }

        switch(key.getEquipType())
        {
          case mesConstants.EQUIPTYPE_EQUIPINV:
            cellHtml.append(" - MeS Inventory");
            break;
            
          case mesConstants.EQUIPTYPE_MERCHEQUIP:
            cellHtml.append(" - Merchant Owned");
            break;
        }
      }
      else
      {
        cellHtml.append(key.toString());
      }
    }
    else if (getColumnName(colOrdinal).equals("Action"))
    {
      cellHtml.append("<a href=\"");
      cellHtml.append(SERVLET_URL);
      cellHtml.append("?method=delTagItem&itemId=");
      cellHtml.append(tagItem.getId());
      cellHtml.append("\">delete</a>");
    }
    else
    {
      cellHtml.append(helper.getCell(rowOrdinal, colOrdinal));
    }
    
    return cellHtml.toString();
  }
  
  public String         getHtmlCell(int rowOrdinal, int colOrdinal)
  {
    return helper.getHtmlCell(rowOrdinal, colOrdinal);
  }

}
