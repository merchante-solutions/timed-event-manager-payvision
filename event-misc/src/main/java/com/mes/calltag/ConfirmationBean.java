package com.mes.calltag;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.FieldBean;

/**
 * This is a helper bean for the call tag module that is used in conjunction
 * with a jsp that asks the user to confirm some requested action (like 
 * deleting something).
 */
public class ConfirmationBean extends FieldBean
{
  static Logger log = Logger.getLogger(ConfirmationBean.class);

  private String confirmMsg;
  private String queryStr;
  
  public ConfirmationBean(String confirmMsg, String queryStr)
  {
    this.confirmMsg = confirmMsg;
    this.queryStr = queryStr;
  }

  public void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    fields.add(new ButtonField("confirmBtn","Yes"));
    fields.add(new ButtonField("cancelBtn","Cancel"));
    
    fields.setHtmlExtra("class=\"formText\"");
  }
  
  public String getConfirmMsg()
  {
    return confirmMsg;
  }
  
  public String getQueryStr()
  {
    return queryStr;
  }
}