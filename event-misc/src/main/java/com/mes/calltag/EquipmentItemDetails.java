package com.mes.calltag;

import org.apache.log4j.Logger;
import com.mes.tools.EquipmentKey;

public class EquipmentItemDetails
{
  static Logger log = Logger.getLogger(EquipmentItemDetails.class);
  
  // static null constant references
  public static CallTagConstants cmds   = null;
  public static CallTagConstants paths  = null;
  
  private CallTagItem tagItem;
  private EquipmentKey key;
  
  public EquipmentItemDetails(CallTagItem tagItem)
  {
    this.tagItem = tagItem;
    key = tagItem.getEquipmentKey();
  }
  
  public String renderDescriptor()
  {
    // determine best descriptor - equip model if available, or serial num
    String descriptor = key.getPartNum();
    if (descriptor == null || descriptor.length() == 0)
    {
      descriptor = "Unknown Model";
    }
    
    StringBuffer html = new StringBuffer();
    html.append("<a href=\"");
    html.append(paths.PATH_SERVLET_URL);
    html.append("?method=" + cmds.CMD_EDIT_TAG_ITEM);
    html.append("&itemId=" + tagItem.getId());
    html.append("\">");
    html.append(tagItem.getStatusDesc());
    html.append(" - ");
    html.append(descriptor);
    html.append("</a>");
    return html.toString();
  }
  
  public String renderSerialNum()
  {
    String serialNum = key.getSerialNum();
    if (serialNum == null || serialNum.length() == 0)
    {
      return "--";
    }
    return serialNum;
  }
  
  public String renderInventoryType()
  {
    StringBuffer html = new StringBuffer();
    
    // if key is somehow not set then there is nothing to be
    // done or shown about the inventory status
    if (!key.isSet())
    {
      html.append("--");
    }
    // key is set, determine if it exists in inventory or not
    else 
    {
      html.append("<a href=\"");
      html.append(paths.PATH_SERVLET_URL);
      html.append("?method=" + cmds.CMD_EDIT_INV_STATUS);
      html.append("&itemId=" + tagItem.getId());
      html.append("\">");
      
      boolean keyExists = false;
      try
      {
        keyExists = key.exists();
      }
      catch (Exception e) { }
      
      
      // for now show a generic "in inventory" indicator
      // there is not currently a more specific but abbreviated
      // inventory code available for this narrow spot
      if (keyExists)
      {
        html.append("MES");
      }
      // if key doesn't exists in inventory then show an "add" link
      else
      {
        html.append("add");
      }
      
      html.append("</a>");
    }
    return html.toString();
  }
  
  public String renderReceivedStatus()
  { 
    if (tagItem.getStatusCode() == tagItem.STATUS_INCOMING)
    {
      return tagItem.getReceived();
    }
    return "&nbsp;";
  }
  
  public String renderActions()
  {
    StringBuffer html = new StringBuffer();
    html.append("<a href=\"");
    html.append(paths.PATH_SERVLET_URL);
    html.append("?method=" + cmds.CMD_DEL_TAG_ITEM);
    html.append("&itemId=" + tagItem.getId());
    html.append("\">delete</a>");
    return html.toString();
  }
}