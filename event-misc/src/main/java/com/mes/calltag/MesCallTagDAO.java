package com.mes.calltag;

import java.util.Iterator;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.maintenance.AccountLookup;
import com.mes.maintenance.AccountLookup.MerchantData;
import com.mes.tools.AccountKey;
import com.mes.user.UserBean;


/**
 * MesCallTagDAO class.
 * 
 * CallTagDAO implementation for the MES db.
 */
class MesCallTagDAO implements CallTagDAO
{
  // log4j class log category
  static Category log = Category.getInstance(MesCallTagDAO.class.getName());
  
  // constants
  
  // data members
  protected CallTagMESDB mesdb;
  
  // construction
  public MesCallTagDAO()
  {
    mesdb = new CallTagMESDB();
  }
  
  // class methods
  // (none)
  
  // object methods

  
  // call tags
  
  public CallTag createCallTag()
  {
    CallTag ct = new CallTag();
    
    ct.setId(mesdb.getNextAvailID());
    
    return ct;
  }

  public void deleteCallTag(CallTag ct)
  {
    try {
      mesdb.deleteCallTag(ct.getId());
    }
    catch(Exception e) {
      log.error("deleteCallTag EXCEPTION: "+e.toString());
    }
  }
  
  public CallTag findCallTag(long pkid)
  {
    try {
      return mesdb.loadCallTag(pkid);
    }
    catch(Exception e) {
      log.error("findCallTag() EXCEPTION: '"+e.getMessage()+"'.");
      return null;
    }
  }
  
  public boolean persistCallTag(CallTag ct)
  {
    try {
      return mesdb.persist(ct,true);
    }
    catch(Exception e) {
      log.error("persistCallTag() EXCEPTION: "+e.toString());
      return false;
    }
  }
  
  
  // call tags items
  
  public CallTagItem createCallTagItem()
  {
    try {
      CallTagItem cti = new CallTagItem();
      cti.setId(mesdb.getNextAvailID());
      return cti;
    }
    catch(Exception e) {
      log.error("createCallTagItem() EXCEPTION: "+e.toString());
      return null;
    }
  }

  public void deleteCallTagItem(CallTagItem cti)
  {
    try {
      mesdb.deleteCallTagItem(cti.getId());
    }
    catch(Exception e) {
      log.error("deleteCallTagItem() EXCEPTION: "+e.toString());
    }
  }
  
  public CallTagItem findCallTagItem(long pkid)
  {
    try {
      return mesdb.loadCallTagItem(pkid);
    }
    catch(Exception e) {
      log.error("findCallTagItem() EXCEPTION: "+e.toString());
      return null;
    }
  }
  
  public boolean persistCallTagItem(CallTagItem cti)
  {
    try {
      return mesdb.persist(cti,true);
    }
    catch(Exception e) {
      log.error("persistCallTagItem() EXCEPTION: "+e.toString());
      return false;
    }
  }

  
  // call tags notifications
  
  public CallTagNotification createCallTagNotification()
  {
    try {
      CallTagNotification ctn = new CallTagNotification();
      ctn.setId(mesdb.getNextAvailID());
      return ctn;
    }
    catch(Exception e) {
      log.error("createCallTagNotification() EXCEPTION: "+e.toString());
      return null;
    }
  }

  public void deleteCallTagNotification(CallTagNotification ctn)
  {
    try {
      mesdb.deleteCallTagNotification(ctn.getId());
    }
    catch(Exception e) {
      log.error("deleteCallTagNotification() EXCEPTION: "+e.toString());
    }
  }
  
  public CallTagNotification findCallTagNotification(long pkid)
  {
    try {
      return mesdb.loadCallTagNotification(pkid);
    }
    catch(Exception e) {
      log.error("findCallTagNotification() EXCEPTION: "+e.toString());
      return null;
    }
  }
  
  public boolean persistCallTagNotification(CallTagNotification ctn)
  {
    try {
      return mesdb.persist(ctn,true);
    }
    catch(Exception e) {
      log.error("persistCallTagNotification() EXCEPTION: "+e.toString());
      return false;
    }
  }

  public void loadAccountData(String merchNumber, UserBean loginUser)
  {
    if(merchNumber == null || loginUser == null)
      return;

  }
  

  // Merchant Info
  public MerchantInfo getMerchantInfo(AccountKey key, UserBean loginUser)
  {
    MerchantInfo mi = null;
    
    try {
      
      mi = new MerchantInfo();
      
      mi.setAccountKey(key);
      
      // AccountLookup.MerchantData
      AccountLookup lookupBean = new AccountLookup();
      lookupBean.setLookupValue(key.getMerchNum());
      lookupBean.getData(loginUser);
      Iterator it = lookupBean.getSortedResults();
      MerchantData md = (it==null? null:(MerchantData)(it.next()));
      if(md != null) {
        mi.setDbaName(md.getDbaName());
        mi.setAssocNumber(md.getAssocNumber());
        mi.setBankNumber(md.getBankNumber());
      }
      
      // dao
      mesdb.fillMerchantInfo(mi);
    }
    catch(Exception e) {
      log.error("getMerchantInfo() EXCEPTION: "+e.toString());
    }
    
    return mi;
  }
  
  
  // helpers
  
  public String[][]         getEquipTypes()
  {
    return mesdb.getEquipTypes();
  }
  
  public String[][]         getEquipManufacturers()
  {
    return mesdb.getEquipManufacturers();
  }
  
  public String[][]         getAppTypes()
  {
    return mesdb.getAppTypes();
  }
  
  public String[][]         getEquipClasses()
  {
    return mesdb.getEquipClasses();
  }
  
  public String[][]         getParentTypes()
  {
    return mesdb.getParentTypes();
  }

  /*
  public String[][]         getCallTagStatuses()
  {
    return mesdb.getCallTagStatuses();
  }
  */
  
  public String[][]         getCallTagItemStatuses()
  {
    return mesdb.getCallTagItemStatuses();
  }

  public String[][]               getEquipLendTypes()
  {
    return mesdb.getEquipLendTypes();
  }
  
  public String[][]               getEquipModels()
  {
    return mesdb.getEquipModels();
  }
  
  /*
  public String[][] getMesInvEquipPartNums()
  {
    return mesdb.getMesInvEquipPartNums();
  }
  */
  
  public String[][] getEquipInvStatuses()
  {
    return mesdb.getEquipInvStatuses();
  }

  public long getCallTagIdFromAcrId(long acrid)
  {
    return mesdb.getCallTagIdFromAcrId(acrid);
  }
  
  public int getAppType(String merchNum)
  {
    return mesdb.getAppType(merchNum);
  }

} // class MesCallTagDAO
