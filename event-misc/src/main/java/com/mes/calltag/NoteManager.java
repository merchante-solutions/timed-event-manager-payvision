package com.mes.calltag;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.user.UserBean;

/**
 * Manages note creation and retrieval.
 */
public class NoteManager extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(NoteManager.class);
  
  public void addNote(Note note)
  {
    log.debug("adding " + note);
    
    if (note.getCalltagId() == -1L)
    {
      throw new RuntimeException("Note is missing calltagid");
    }
    
    if (note.getAuthor() == null)
    {
      throw new RuntimeException("Note is missing author");
    }
    
    if (note.getText() == null)
    {
      throw new RuntimeException("No note text given");
    }
    
    PreparedStatement ps = null;
    
    try
    {
      connect();
      
      // determine sql version of create date (may be null)
      Timestamp createTs = null;
      Date createDate = note.getCreateDate();
      if (createDate != null)
      {
        createTs = new Timestamp(createDate.getTime());
      }
      
      String idStr = null;
      if (note.getId() != -1L)
      {
        idStr = String.valueOf(note.getId());
      }
      
      String qs
        = "insert into equip_calltag_notes            "
        + "  ( id, ctid, create_date, author, note )  "
        + "values                                     "
        + "  ( ?, ?, ?, ?, ? )                        ";
        
      ps = con.prepareStatement(qs);
      ps.setString    (1,idStr);
      ps.setLong      (2,note.getCalltagId());
      ps.setTimestamp (3,createTs);
      ps.setString    (4,note.getAuthor());
      ps.setString    (5,note.getText());
      
      ps.execute();
      
      log.debug("note logged");
    }
    catch (Exception e)
    {
      log.error("addNote(): " + e);
      e.printStackTrace();
      throw new RuntimeException(e.toString());
    }
    finally
    {
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  public void addNote(long ctid, String author, String text)
  {
    Note note = new Note();
    note.setCalltagId(ctid);
    note.setAuthor(author);
    note.setText(text);
    addNote(note);
  }
  
  public void addNote(long ctid, UserBean author, String text)
  {
    addNote(ctid,author.getLoginName(),text);
  }
  
  public List getNotes(long ctid)
  {
    ArrayList notes = new ArrayList();
      
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    try
    {
      connect();
      
      String qs
        = "select id,                 "
        + "       ctid,               "
        + "       create_date,        "
        + "       author,             "
        + "       note                "
        + "from   equip_calltag_notes "
        + "where  ctid = ?            "
        + "order by create_date desc  ";
        
      ps = con.prepareStatement(qs);
      ps.setLong(1,ctid);
      
      rs = ps.executeQuery();
      while (rs.next())
      {
        Note note = new Note();
        
        note.setId        (rs.getLong("id"));
        note.setCalltagId (rs.getLong("ctid"));
        note.setCreateDate(rs.getTimestamp("create_date"));
        note.setAuthor    (rs.getString("author"));
        note.setText      (rs.getString("note"));
        
        notes.add(note);
      }
    }
    catch (Exception e)
    {
      log.error("getNotes(" + ctid + "): " + e);
      throw new RuntimeException(e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
    
    return notes;
  }
}
