package com.mes.calltag;

// log4j classes.
import org.apache.log4j.Category;

public final class CallTagBuilder
{
  // create class log category
  static Category log = Category.getInstance(CallTagBuilder.class.getName());
  
  // Singleton
  public static CallTagBuilder getInstance()
  {
    if(_instance==null)
      _instance = new CallTagBuilder();
    
    return _instance;
  }
  private static CallTagBuilder _instance = null;
  ///
  

  // constants
  
  // call tag type
  public static final int   TYPE_UNDEFINED                            = 0;
  public static final int   TYPE_CALLTAG                              = 1;
  public static final int   TYPE_CALLTAGITEM                          = 2;
  public static final int   TYPE_CALLTAGNOTIFICATION                  = 3;
  
  
  // data members
  // (NONE)

  // class methods
  
  // object methods
  
  // construction
  private CallTagBuilder()
  {
  }
  
  public class UnknownCallTagTypeException extends Exception
  {
    public UnknownCallTagTypeException(int type)
    {
      super("Unknown or unhandled Call Tag type: '"+type+"' specified.  Unable to build object.");
    }
  } // class CallTagBuildException
  
  public CallTag buildCallTag()
    throws UnknownCallTagTypeException
  {
    return (CallTag)buildCallTag(TYPE_CALLTAG);
  }
  
  public CallTagItem buildCallTagItem()
    throws UnknownCallTagTypeException
  {
    return (CallTagItem)buildCallTag(TYPE_CALLTAGITEM);
  }

  public CallTagNotification buildCallTagNotification()
    throws UnknownCallTagTypeException
  {
    return (CallTagNotification)buildCallTag(TYPE_CALLTAGNOTIFICATION);
  }

  private CallTagBase buildCallTag(int type)
    throws UnknownCallTagTypeException
  {
    log.debug("buildCallTag(type: "+type+") - START");
    
    CallTagBase ct = null;
    
    switch(type) {
      case TYPE_CALLTAG:
        ct = CallTagDAOFactory.getInstance().getCallTagDAO().createCallTag();
        break;
      case TYPE_CALLTAGITEM:
        ct = CallTagDAOFactory.getInstance().getCallTagDAO().createCallTagItem();
        break;
      case TYPE_CALLTAGNOTIFICATION:
        ct = CallTagDAOFactory.getInstance().getCallTagDAO().createCallTagNotification();
        break;
      default:
        throw new UnknownCallTagTypeException(type);
    }
    
    log.debug("buildCallTag(type: "+type+") - built.  (END)");
    return ct;
  }

} // class CallTagBuilder
