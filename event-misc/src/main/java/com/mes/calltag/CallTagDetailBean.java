package com.mes.calltag;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Category;
import com.mes.calltag.CallTagDAO.MerchantInfo;
import com.mes.constants.mesConstants;
import com.mes.equipment.EquipmentDAO;
import com.mes.equipment.EquipmentDAOFactory;
import com.mes.equipment.EquipmentDataBase;
import com.mes.equipment.EquipmentDataBuilder;
import com.mes.equipment.EquipmentData_EquipInv;
import com.mes.forms.ButtonField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.DynamicLabelField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldError;
import com.mes.forms.FieldGroup;
import com.mes.forms.FieldValidator;
import com.mes.forms.RadioButtonField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.support.HttpHelper;
import com.mes.support.TextMessageManager;
import com.mes.tools.AccountKey;
import com.mes.tools.DBGrunt;
import com.mes.tools.DropDownTable;
import com.mes.tools.EquipmentKey;
import com.mes.tools.Key;
import com.mes.tools.TableView;
import com.mes.user.UserBean;

/**
 * CallTagDetailBean
 *
 * Manages editing of Call Tags.
 */
public class CallTagDetailBean extends FieldBean
{
  // create class log category
  static Category log = Category.getInstance(CallTagDetailBean.class.getName());

  // constants
  public static final   String        SERVLET_URL            = "/srv/calltag";

  public static final   int           DISPMODE_UNDEFINED     = 0;
  public static final   int           DISPMODE_CT_ADD        = 1;
  public static final   int           DISPMODE_CT_EDIT       = 2;
  public static final   int           DISPMODE_CTI_ADD       = 3;
  public static final   int           DISPMODE_CTI_EDIT      = 4;
  public static final   int           DISPMODE_CTN_ADD       = 5;
  public static final   int           DISPMODE_CTN_EDIT      = 6;

  // equipment display mode/ops
  public static final   int           EQUIPMODE_NONE         = 0;
  public static final   int           EQUIPMODE_UNASSIGN     = 1;
  public static final   int           EQUIPMODE_ASSIGN       = 2;
  public static final   int           EQUIPMODE_ADDTOMESINV  = 3;


  // data members
  protected       int                       displayMode               = DISPMODE_CT_ADD;
  protected       CallTag                   ct                        = null;
  protected       CallTagItem               crntCti                   = null;
  protected       CallTagNotification       crntCtn                   = null;
  protected       MerchantInfo              merchantInfo              = null;
    // for displaying related account (merchant) details
  protected       CallTagItemsTable         itemsTable                = null;
  protected       CallTagNotificationsTable notificationsTable        = null;
  protected       TextMessageManager        tmm                       = null;
  protected       int                       equipDispMode             = EQUIPMODE_NONE;
  protected       int                       equipOp                   = EQUIPMODE_NONE;

  private         String[][]                ctiStatus                 = null;
  private         String[][]                eiStatus                  = null;
  private         String[][]                eiLendTypes               = null;
  private         String[][]                eiAppTypes                = null;
  private         String[][]                eiEquipClasses            = null;
  private         String[][]                eiEquipManufacturers      = null;
  private         String[][]                eiEquipTypes              = null;

  private ArrayList equipmentList;

  // class methods
  // (NONE)


  // object methods

  // construction
  public CallTagDetailBean()
  {
    super();

    initialize();
  }

  protected void initialize()
  {
    autoSubmitName = "method";

    ctiStatus             = this.getCallTagDAO().getCallTagItemStatuses();
    eiStatus              = this.getCallTagDAO().getEquipInvStatuses();
    eiLendTypes           = this.getCallTagDAO().getEquipLendTypes();
    eiAppTypes            = this.getCallTagDAO().getAppTypes();
    eiEquipClasses        = this.getCallTagDAO().getEquipClasses();
    eiEquipManufacturers  = this.getCallTagDAO().getEquipManufacturers();
    eiEquipTypes          = this.getCallTagDAO().getEquipTypes();

    /*
    equipTypes = new String[2][2];
    equipTypes[0][1] = Integer.toString(mesConstants.EQUIPTYPE_MERCHEQUIP);
    equipTypes[0][0] = "Merchant Owned";
    equipTypes[1][1] = Integer.toString(mesConstants.EQUIPTYPE_EQUIPINV);
    equipTypes[1][0] = "MeS Inventory";
    */
    equipmentList = this.getEquipmentDAO().getSimpleEquipmentList();

    createFields(null);

    itemsTable          = new CallTagItemsTable(null,SERVLET_URL,"");
    notificationsTable  = new CallTagNotificationsTable(null,SERVLET_URL,"");
  }

  public TextMessageManager getTextMessageManager()
  {
    return tmm;
  }

  public void setTextMessageManager(TextMessageManager tmm)
  {
    this.tmm=tmm;
  }

  private final String getDesc(int code, String[][] array)
  {
    for(int i=0; i<array.length; i++) {
      if(code == Integer.parseInt(array[i][1]))
        return array[i][0];
    }
    return "";
  }

  public void loadAccountData(AccountKey key, UserBean loginUser)
  {
    this.merchantInfo = getCallTagDAO().getMerchantInfo(key,loginUser);
  }

  public int getPersistCallTagDisplayMode()
  {
    return isCurrentCallTagPersisted()? DISPMODE_CT_EDIT:DISPMODE_CT_ADD;
  }

  public boolean isCurrentCallTagPersisted()
  {
    return ct==null? false:(ct.getLastModified()!=null && ct.getLastModified().getTime()!=0L);
  }

  protected CallTagDAO getCallTagDAO()
  {
    return CallTagDAOFactory.getInstance().getCallTagDAO();
  }

  protected EquipmentDAO getEquipmentDAO()
  {
    return EquipmentDAOFactory.getInstance().getEquipmentDAO();
  }

  public boolean isEditable()
  {
    return true;
  }

  public boolean isSourceEditable()
  {
    return !isCurrentCallTagPersisted();
  }

  public boolean hasItems()
  {
    return (ct==null? false:ct.getNumItems()>0);
  }

  public boolean hasNotifications()
  {
    return (ct==null? false:ct.getNumNotifications()>0);
  }

  public CallTagItem getCurrentCallTagItem()
  {
    return crntCti;
  }

  public CallTagNotification getCurrentCallTagNotification()
  {
    return crntCtn;
  }

  public void setCurrentItemToNew()
  {
    crntCti = this.getCallTagDAO().createCallTagItem();
  }

  public void setCurrentNotificationToNew()
  {
    crntCtn = this.getCallTagDAO().createCallTagNotification();
  }

  public CallTagItem setCurrentItem(long id)
  {
    log.debug("setting current item to " + id);
    for(Enumeration e=ct.getItemsEnumerator();e.hasMoreElements();) {
      CallTagItem cti = (CallTagItem)e.nextElement();

      if(cti.getId()==id) {
        log.debug("found current item with key " + cti.equipKey);
        this.crntCti=cti;
        break;
      }
    }

    return this.crntCti;  // for convenience
  }

  public CallTagNotification setCurrentNotification(long id)
  {
    for(Enumeration e=ct.getNotificationsEnumerator();e.hasMoreElements();) {
      CallTagNotification ctn = (CallTagNotification)e.nextElement();

      if(ctn.getId()==id) {
        this.crntCtn=ctn;
        break;
      }
    }

    return this.crntCtn;  // for convenience
  }

  public Enumeration getNotificationsEnumerator()
  {
    return ct.getNotificationsEnumerator();
  }

  public String getCurrentItemEquipType()
  {
    try {
      switch(crntCti.getEquipmentKey().getEquipType()) {
        case mesConstants.EQUIPTYPE_EQUIPINV:
          return "MeS Inventory";
        case mesConstants.EQUIPTYPE_MERCHEQUIP:
          return "Merchant Owned";
        default:
          return "-UNKNOWN TYPE-";
      }
    }
    catch(Exception e) {
      return "-NOT SET-";
    }
  }

  public void setDisplayMode(int v)
  {
    displayMode=v;
  }

  public int getDisplayMode()
  {
    return displayMode;
  }

  public MerchantInfo getMerchantInfo()
  {
    return merchantInfo;
  }

  public CallTag getCallTag()
  {
    return ct;
  }

  public boolean isSourceSet()
  {
    try {
      return ct.getParentKey().isSet();
    }
    catch(Exception e) {
      return false;
    }
  }

  public boolean isMesInventoryEquipment()
  {
    try {
      EquipmentKey ekey = crntCti.getEquipmentKey();
      return (
        ekey.getEquipType() == mesConstants.EQUIPTYPE_EQUIPINV
        && ekey.exists()
      );
    }
    catch(Exception e) {
      return false;
    }
  }

  public String getCallTagSourceColor()
  {
    try {
      switch(ct.getParentType()) {

        case mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP:
          return "#00FF00"; // lime
        case mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL:
          return "#008000"; // green

        case mesConstants.ACTION_CODE_SWAP:
          return "#D2B48C"; // tan
        case mesConstants.ACTION_CODE_DEPLOYMENT_SWAP:
          return "#8B4513"; // saddle brown

        case mesConstants.ACTION_CODE_PINPAD_EXCHANGE:
          return "#00CED1"; // dark turquiose
        case mesConstants.ACTION_CODE_DEPLOYMENT_PINPAD_EXCHANGE:
          return "#1E90FF"; // dodger blue

        case mesConstants.ACTION_CODE_REPAIR:
          return "#B22222"; // fire brick

        case mesConstants.ACTION_CODE_CALLTAG:
          return "#C0C0C0"; // silver
      }
    }
    catch(Exception e) {
    }

    return "";
  }

  public void setCallTag(CallTag ct)
  {
    if(ct==null || ct.equals(this.ct))
      return;

    this.ct = ct;

    crntCti = null;
    crntCtn = null;

    //equipDispMode = EQUIPMODE_NONE;

    if(itemsTable!=null)
      itemsTable.setRows(ct.getItems());
    if(notificationsTable!=null)
      notificationsTable.setRows(ct.getNotifications());
  }

  public String getDisplayModeDescriptor()
  {
    switch(displayMode) {
      case DISPMODE_CT_ADD:
        return "Add Call Tag";
      case DISPMODE_CT_EDIT:
        return "Call Tag Detail";
      case DISPMODE_CTI_ADD:
        return "Add Call Tag Item";
      case DISPMODE_CTI_EDIT:
        return "Call Tag Item Detail";
      case DISPMODE_CTN_ADD:
        return "Add Call Tag Notification";
      case DISPMODE_CTN_EDIT:
        return "Call Tag Notification Detail";
    }

    return "Unknown display mode";
  }

  public String getSubmitButtonText()
  {
    switch(displayMode) {
      case DISPMODE_CT_ADD:
        return "Add Call Tag";
      case DISPMODE_CT_EDIT:
        return "Update Call Tag";
      case DISPMODE_CTI_ADD:
        return "Add Call Tag Item";
      case DISPMODE_CTI_EDIT:
        return "Update Call Tag Item";
      case DISPMODE_CTN_ADD:
        return "Add Call Tag Notification";
      case DISPMODE_CTN_EDIT:
        return "Update Call Tag Notification";
    }

    return "Unknown display mode";
  }

  protected void createFields(HttpServletRequest request)
  {
    fields.setShowName("");   // so "baseGroup:" prefix doesn't show up in error message text.

    Field f;
    FieldGroup fg;
    String btnName;

    final String[][] arrYesNo =
    {
      { "Yes"  , "y"  },
      { "No"   , "n"   }
    };


    // submit buttons

    f = new ButtonField("add");
    ((ButtonField)f).setButtonName(autoSubmitName);
    fields.add(f);

    f = new ButtonField("update");
    ((ButtonField)f).setButtonName(autoSubmitName);
    fields.add(f);

    f = new ButtonField("delete");
    ((ButtonField)f).setButtonName(autoSubmitName);
    fields.add(f);

    // Call Tag

    fg = new FieldGroup("Call Tag");
    fields.add(fg);

    f = new DropDownField("actionCode","Source Type",new ParentTypeDropDownTable(),true);
    fg.add(f);
    fg.addAlias("actionCode","parent_type");

    f = new Field("id","Source Ref Num",15,15,true);
    fg.add(f);
    fg.addAlias("id","parentkey_refnum");

    f = new Field("partNum","Equip Part Num",30,30,true);
    fg.add(f);
    fg.addAlias("partNum","parentkey_partnum");

    f = new Field("serialNum","Equip Serial Num",25,25,true);
    fg.add(f);
    fg.addAlias("serialNum","parentkey_serialnum");

    f = new DateField("billDate","Bill Date (mm/dd/yyyy)",true);
    ((DateField)f).setDateFormat("MM/dd/yyyy");
    fg.add(f);

    f = new CurrencyField("billAmount","Bill Amount",8,8,true,0,99999);
    fg.add(f);

    f = new RadioButtonField("billCharged","Bill Charged?",arrYesNo,1,true,"Please specify whether or not the bill was charged.");
    ((RadioButtonField)f).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
    fg.add(f);



    f = new TextareaField("newNote","Note",300,2,70,true);
    fg.add(f);
    
    f = new ButtonField("submitNote","Add Note Button","Add Note",autoSubmitName);
    fg.add(f);
    


    f = new TextareaField("notes","Notes",2000,6,50,true);
    fg.add(f);

    f = new ButtonField("Set Source");
    ((ButtonField)f).setButtonName(autoSubmitName);
    ((ButtonField)f).setButtonText("Set Source");
    fg.add(f);

    // Call Tag validations
    fg.addValidation(new CallTagValidation());

    // Call Tag Item

    fg = new FieldGroup("Call Tag Item");
    fields.add(fg);

    f = new RadioButtonField("equip_status","Status",ctiStatus,-1,false,"Please specify a Call Tag Item Status.");
    ((RadioButtonField)f).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
    fg.add(f);

    f = new Field("equip_serialnum","Equipment Serial Number",25,25,true);
    fg.add(f);

    f = new ButtonField("Set Equipment");
    ((ButtonField)f).setButtonName(autoSubmitName);
    ((ButtonField)f).setButtonText("Set Equipment");
    fg.add(f);

    f = new DropDownField("equip_model","Equipment Model",new EquipDescTable(),false);//required
    fg.add(f);

    /* //OLD
    f = new Field("equip_model","Equipment Model",100,30,false);
    fg.add(f);
    */

    // Call Tag Item validations
    fg.addValidation(new CallTagItemValidation());

    // equip inventory related (for [un-]binding account to the item's equipment

    fg = new FieldGroup("Equipment Inventory");
    fields.add(fg);

    f = new Field("equip_TsysDesc","TSYS Descriptor",100,50,true);
    fg.add(f);

    f = new Field("equip_Descriptor","Descriptor",100,50,true);
    fg.add(f);

    f = new DropDownField("equip_TypeCode","Type",new EquipTypesTable(),false);//required
    fg.add(f);

    f = new DropDownField("equip_MfrCode","Manufacturer",new EquipManufacturersTable(),false);//required
    fg.add(f);

    f = new DropDownField("equip_Owner","Equipment Owner",new AppTypesTable(),false);//required
    fg.add(f);

    f = new DropDownField("equip_Status","Equipment Inventory Status",new EquipInvStatusDropDownTable(),false);//required
    fg.add(f);

    f = new DropDownField("equip_LendType","Equipment Lend Type",new EquipLendTypeDropDownTable(),true);
    fg.add(f);

    f = new CurrencyField("equip_LendTypePrice","Equipment Inventory Lend Type Price",6,6,true);
    fg.add(f);

    f = new DateField("equip_DeployedDate","Equipment Inventory Deployed Date",true);
    ((DateField)f).setDateFormat("MM/dd/yyyy");
    fg.add(f);

    f = new DropDownField("equip_Class","Equipment Inventory Class",new EquipClassesTable(),false);//required
    fg.add(f);

    f = new DropDownField("equip_OrigClass","Equipment Inventory Original Class",new EquipClassesTable(),true);
    fg.add(f);

    f = new DynamicLabelField("equip_assignedTo","Assigned To (Merchant Number)");
    fg.add(f);

    f = new Field("equip_InvoiceNumber","Invoice Number",25,25,true);
    fg.add(f);

    f = new Field("equip_TransId","Transaction Id",15,15,true);
    fg.add(f);

    f = new CurrencyField("equip_UnitCost","Unit Cost",6,6,false);//required
    fg.add(f);

    f = new CurrencyField("equip_ShippingCost","Shipping Cost",6,6,true);
    fg.add(f);

    f = new ButtonField("equip_assignment");
    ((ButtonField)f).setButtonName(autoSubmitName);
    fg.add(f);

    // Equipment Inventory validations
    fg.addValidation(new EquipmentOpValidation());

    // Call Tag Notification

    fg = new FieldGroup("Call Tag Notification");
    fields.add(fg);

    f = new Field("pkg_trk_num","Package Tracking Number",30,30,false);
    fg.add(f);

    Date now = DBGrunt.getCurrentDate();
    f = new DateField("issue_date","Issue Date",now,false);
    ((DateField)f).setDateFormat("MM/dd/yyyy");
    fg.add(f);

    f = new DropDownField("letter_name","Letter Name", new _letterDDField(), false);
    fg.add(f);

    // Call Tag Notificaion validations
    fg.addValidation(new CallTagNotificationValidation());

    super.createFields(request);
  }

  private class _letterDDField extends DropDownTable
  {
    String[][] letters = mesConstants.CT_LETTERS;

    public _letterDDField()
    {
      for(int i = 0; i<letters.length; i++)
      {
        //use only the string component...
        //the [0] space will be used to generate
        //the appropriate PDF letter
        addElement(letters[i][1],letters[i][1]);
      }
    }
  }

  private final class CallTagValidation implements Validation
  {
    private String errorText = "";

    public boolean validate(String fieldData)
    {
      if(ct==null) {
        errorText = "Null Call Tag reference.";
        return false;
      }

      Key key;

      // ensure source is valid
      key = ct.getParentKey();
      if(key==null || !key.isSet()) {
        errorText = "A Source must be specified.";
        return false;
      }
      try {
        if(!key.exists()) {
          errorText = "Warning: Specified Call Tag Source does not exist.";
        }
      }
      catch(Exception e) { }

      // ensure billing account is set and exists
      key = ct.getBillMerchant();
      if(key==null) {
        errorText = "Null Billing Merchant key reference.";
        return false;
      }
      if(!key.isSet()) {
        errorText = "No Billing Merchant specified.";
        return false;
      }
      try {
        if(!key.exists()) {
          errorText = "Specified Billing Merchant does not exist.";
          return false;
        }
      }
      catch(Exception e) {
        errorText = e.getMessage();
        return false;
      }

      // ensure at least one item exists
      if(ct.getNumItems()<1) {
        errorText = "At least one Call Tag Item must be specified.";
        return false;
      }

      return true;
    }

    public String getErrorText()
    {
      return errorText;
    }

  }

  private final class CallTagItemValidation implements Validation
  {
    private String errorText = "";

    public boolean validate(String fieldData)
    {
      if(crntCti==null) {
        errorText = "Null Call Tag Item reference.";
        return false;
      }

      // ensure equipment is good for outgoing equipment
      if(crntCti.getStatusCode() == CallTagItem.STATUS_OUTGOING) {
        Key key = crntCti.getEquipmentKey();
        if(key==null) {
          errorText = "Null Call Tag Item Equipment key reference.";
          return false;
        }
        if(!key.isSet()) {
          errorText = "No Call Tag Item Equipment specified.";
          return false;
        }
        try {
          if(!key.exists()) {
            errorText = "Specified Call Tag Item equipment does not exist.";
            return false;
          }
        }
        catch(Exception e) {
          errorText = e.getMessage();
          return false;
        }
      }

      return true;
    }

    public String getErrorText()
    {
      return errorText;
    }

  }

  private final class CallTagNotificationValidation implements Validation
  {
    private String errorText = "";

    public boolean validate(String fieldData)
    {
      // currently no inter-field validation exists for notifications
      return true;
    }

    public String getErrorText()
    {
      return errorText;
    }

  }

  private final class EquipmentOpValidation implements Validation
  {
    private String  errorText     = "";

    public boolean validate(String fieldData)
    {
      errorText = "";

      boolean rval = true;

      try {

        EquipmentData_EquipInv ei = null;
        try {
          ei = (EquipmentData_EquipInv)crntCti.getEquipmentData();
        }
        catch(Exception e) {
          throw new Exception("Unable to obtain reference to inventory equipment item.");
        }

        switch(equipOp) {

          case EQUIPMODE_ADDTOMESINV:

            Key key = crntCti.getEquipmentKey();
            if(key==null || !key.isSet()) {
              errorText = "Both a Serial Number and Equipment Model must be specified.";
            } else {
              try {
                if(key.exists()) {
                  errorText = "Unable to add specified equipment data: The specified Serial Number and Equipment Model ("+key.toString()+") are already specified in MeS inventory.";
                }
              }
              catch(Exception e) {
                errorText = e.getMessage();
              }
            }

            // TODO: finish validation

            rval = (errorText.length() < 1);

            break;

          case EQUIPMODE_ASSIGN:

            // ensure equipment is accounted for in inventory
            if(!ei.getKey().exists()) {
              throw new Exception("Unable to assign equipment '"+ei.getDescriptor()+"' to account '"+merchantInfo.getDbaName()+"': This equipment does not exist in MeS inventory.");
            }

            // ensure no ownership conflicts
            if(merchantInfo.getAppTypeCode() != ei.getOwner()) {
              throw new Exception("Unable to assign equipment '"+ei.getDescriptor()+"' to account '"+merchantInfo.getDbaName()+"': This equipment is owned by '"+ei.getOwnerName()+"' and merchant# '"+merchantInfo.getAccountKey().getMerchNum()+"' is of type "+merchantInfo.getAppTypeDesc());
            }

            // ensure equipment ei merchant number is that of the billing merchant's
            if(ei.getMerchantNumber()!=null && !ei.getMerchantNumber().equals(merchantInfo.getMerchantNumber())) {
              throw new Exception("Unable to assign equipment '"+ei.getDescriptor()+"' to account '"+merchantInfo.getDbaName()+"': This equipment is currently assigned to: "+ei.getEiDbaName());
            }

            // ensure equipment ei deployed date is specified
            if(ei.getDeployedDate()==null || ei.getDeployedDate().getTime()==0L) {
              throw new Exception("Unable to assign equipment '"+ei.getDescriptor()+"' to account '"+merchantInfo.getDbaName()+"': No Deployed Date specified.");
            }

            // TODO: validate inventory status
            // TODO: validate disposition

            // ensure inventory deployed date is specified
            if(ei.getDeployedDate() == null || ei.getDeployedDate().getTime() == 0L) {
              throw new Exception("Unable to assign equipment '"+ei.getDescriptor()+"' to account '"+merchantInfo.getDbaName()+"': An Inventory Deployed Date must be specified.");
            }

            break;

          case EQUIPMODE_UNASSIGN:

            // ensure no merchant number specified
            if(ei.getMerchantNumber()!=null && ei.getMerchantNumber().length()>0) {
              throw new Exception("Unable to unassign equipment '"+ei.getDescriptor()+"' from account '"+merchantInfo.getDbaName()+"': This equipment is currently assigned to merchant number: "+ei.getEiDbaName());
            }

            // TODO: validate inventory status
            // TODO: validate disposition

            break;

          default:
            errorText = "Unhandled equipment op specified: "+equipDispMode+".  No validation performed.";
            break;
        }

      }
      catch(Exception e) {
        rval = false;
        errorText = e.getMessage();
      }

      return rval;
    }

    public String getErrorText()
    {
      return errorText;
    }

  }

  public boolean isValid()
  {
    FieldGroup fg = null;

    switch(displayMode) {
      case DISPMODE_CT_ADD:
      case DISPMODE_CT_EDIT:
        fg = (FieldGroup)fields.getField("Call Tag");
        break;
      case DISPMODE_CTI_ADD:
      case DISPMODE_CTI_EDIT:
        if(equipOp > EQUIPMODE_NONE)
          fg = (FieldGroup)fields.getField("Equipment Inventory");
        else
          fg = (FieldGroup)fields.getField("Call Tag Item");
        break;
      case DISPMODE_CTN_ADD:
      case DISPMODE_CTN_EDIT:
        fg = (FieldGroup)fields.getField("Call Tag Notification");
        break;
      default:
        return false;
    }

    // init errors vector
    clearErrors();

    // use field validator to validate fields, collect any field errors
    FieldValidator validator = new FieldValidator();
    if (!validator.validateFields(fg))
    {
      getErrors().addAll(validator.getFieldErrors());
      for (Iterator i = getErrors().iterator(); i.hasNext(); )
      {
        FieldError fe = (FieldError)i.next();
      }
    }

    // do any non-specific validation of fields
    validateFields();

    // return whether or not errors were detected
    return !hasErrors();
  }

  // "pull" field bean --> calltag objects
  protected void postHandleRequest(HttpServletRequest request)
  {
    // pull the data from fields to the native calltag object schema
    // field --> object

    FieldGroup fg;
    Field f;

    String submitBtnValue;
    try {
      submitBtnValue = HttpHelper.getString(request,autoSubmitName,"");
    }
    catch(Exception e) {
      submitBtnValue = "";
    }
    
    log.debug("submitBtnValue = '" + submitBtnValue + "'");

    // set equip op
    if(submitBtnValue.indexOf("Assign Equipment") >= 0)
      equipOp = EQUIPMODE_ASSIGN;
    else if(submitBtnValue.indexOf("Unassign Equipment") >= 0)
      equipOp = EQUIPMODE_UNASSIGN;
    else if(submitBtnValue.indexOf("Add to MeS Inventory") >= 0)
      equipOp = EQUIPMODE_ADDTOMESINV;
    else
      equipOp = EQUIPMODE_NONE;

    switch(displayMode) {

      case DISPMODE_CT_ADD:
      case DISPMODE_CT_EDIT:

        fg = (FieldGroup)fields.getField("Call Tag");

        if(!submitBtnValue.equals("Set Source")) {

          if((f = fg.getField("billDate"))!=null)
            ct.setBillDate(((DateField)f).getUtilDate());

          if((f = fg.getField("billAmount"))!=null)
            ct.setBillAmount(f.asDouble());

          if((f = fg.getField("billCharged"))!=null)
            ct.setBillCharged(f.getData().equals("y"));

          if((f = fg.getField("billCharged"))!=null)
            ct.setBillCharged(f.getData().equals("y"));

          if((f = fg.getField("notes"))!=null)
            ct.setNotes(f.getData());
        }
        
        break;

      case DISPMODE_CTI_ADD:
      case DISPMODE_CTI_EDIT:

        // call tag item
        if(submitBtnValue.equals("Update Call Tag Item") || submitBtnValue.equals("Add Call Tag Item")) {

          fg = (FieldGroup)fields.getField("Call Tag Item");

          if((f = fg.getField("equip_serialnum"))!=null)
            crntCti.setSerialNum(f.getData());

          if((f = fg.getField("equip_model"))!=null)
            crntCti.setEquipModel(f.getData());

          if((f = fg.getField("equip_status"))!=null) {
            crntCti.setStatusCode(f.asInteger());
            // set the desc too
            crntCti.setStatusDesc(getDesc(crntCti.getStatusCode(), ctiStatus));
          }

        }

        // equip inventory specific ops
        else if(equipOp > EQUIPMODE_NONE) {

          try {

            EquipmentData_EquipInv ei = null;
            if(equipOp == EQUIPMODE_ASSIGN || equipOp == EQUIPMODE_UNASSIGN)
            {
              log.debug("using existing equip data in call tag item");

              ei = (EquipmentData_EquipInv)crntCti.getEquipmentData();
            }
            else if(equipOp == EQUIPMODE_ADDTOMESINV) {
              ei = (EquipmentData_EquipInv)EquipmentDataBuilder.getInstance().build(mesConstants.EQUIPTYPE_EQUIPINV);

              log.debug("creating new equip data for call tag item");

              fg = (FieldGroup)fields.getField("Call Tag Item");

              String pn = null, sn = null;

              if((f = fg.getField("equip_serialnum"))!=null)
                sn = f.getData();

              if((f = fg.getField("equip_model"))!=null)
                pn = f.getData();

              ei.setEquipmentKey(new EquipmentKey(pn,sn));
            }

            fg = (FieldGroup)fields.getField("Equipment Inventory");

            if((f = fg.getField("equip_Descriptor"))!=null)
              ei.setDescriptor(f.getData());

            if((f = fg.getField("equip_TypeCode"))!=null) {
              ei.setEquipTypeCode(f.asInteger());
              // set the desc too
              ei.setEquipTypeName(getDesc(ei.getEquipTypeCode(), eiEquipTypes));
            }

            if((f = fg.getField("equip_MfrCode"))!=null) {
              ei.setEquipMfrCode(f.asInteger());
              // set the desc too
              ei.setEquipMfrName(getDesc(ei.getEquipMfrCode(), eiEquipTypes));
            }

            if((f = fg.getField("equip_TsysDesc"))!=null)
              ei.setTsysEquipDesc(f.getData());

            if((f = fg.getField("equip_InvoiceNumber"))!=null)
              ei.setInvoiceNumber(f.getData());

            if((f = fg.getField("equip_ShippingCost"))!=null)
              ei.setShippingCost(f.asDouble());

            if((f = fg.getField("equip_Owner"))!=null) {
              ei.setOwner(f.asInt());
              // set the desc too
              ei.setOwnerName(getDesc(ei.getOwner(), eiAppTypes));
            }

            if((f = fg.getField("equip_Class"))!=null) {
              ei.setClassCode(f.asInt());
              // set the desc too
              ei.setClassName(getDesc(ei.getClassCode(), eiEquipClasses));
            }

            if((f = fg.getField("equip_OrigClass"))!=null) {
              ei.setOriginalClassCode(f.asInt());
              // set the desc too
              ei.setOriginalClassName(getDesc(ei.getOriginalClassCode(), eiEquipClasses));
            }

            if((f = fg.getField("equip_TransId"))!=null)
              ei.setTransId(f.getData());

            if((f = fg.getField("equip_Status"))!=null) {
              ei.setStatusCode(f.asInteger());
              // set the desc too
              ei.setStatusName(getDesc(ei.getStatusCode(), eiStatus));
            }

            if((f = fg.getField("equip_LendType"))!=null) {
              ei.setEquipLendTypeCode(f.asInteger());
              // set the desc too
              ei.setEquipLendTypeName(getDesc(ei.getEquipLendTypeCode(), eiLendTypes));
            }

            if((f = fg.getField("equip_LendTypePrice"))!=null)
              ei.setLrbPrice(f.asDouble());

            if((f = fg.getField("equip_DeployedDate"))!=null)
              ei.setDeployedDate(((DateField)f).getUtilDate());

            crntCti.setEquipmentData(ei);

            log.debug("set equip data in current call tag:" + ei.getKey());
          }
          catch(Exception e) {
            log.error("postHandleRequest(request) - [Un]Assign Equipment EXCEPTION: "+e.toString());
          }

        }

        break;

      case DISPMODE_CTN_ADD:
      case DISPMODE_CTN_EDIT:

        fg = (FieldGroup)fields.getField("Call Tag Notification");

        if((f = fg.getField("pkg_trk_num"))!=null)
          crntCtn.setPkgTrkNum(f.getData());

        if((f = fg.getField("issue_date"))!=null)
          crntCtn.setIssueDate(((DateField)f).getUtilDate());

        if((f = fg.getField("letter_name"))!=null)
          crntCtn.setLetterName(f.getData());

        break;
    }

  }

  // "push" calltag objects --> field bean

  public void setFields() // i.e. doPush()
  {
    FieldGroup fg;
    Field fld;

    switch(displayMode) {

      case DISPMODE_CT_ADD:
      case DISPMODE_CT_EDIT:

        if(ct==null)
          return;

        fg = (FieldGroup)fields.getField("Call Tag");

        if((fld = fg.getField("parent_type"))!=null)
          fld.setData(Integer.toString(ct.getParentType()));
        if((fld = fg.getField("parentkey_refnum"))!=null)
          fld.setData(ct.getParentRefNum()>0? Long.toString(ct.getParentRefNum()):"");
        if((fld = fg.getField("parentkey_partnum"))!=null)
          fld.setData(ct.getParentPartNum());
        if((fld = fg.getField("parentkey_serialnum"))!=null)
          fld.setData(ct.getParentSerialNum());

        if((fld = fg.getField("billDate"))!=null && ct.getBillDate()!=null) {
          ((DateField)fld).setUtilDate(ct.getBillDate());
        } if((fld = fg.getField("billAmount"))!=null)
          fld.setData(Double.toString(ct.getBillAmount()));
        if((fld = fg.getField("billCharged"))!=null)
          fld.setData(ct.isBillCharged()? "y":"n");

        if((fld = fg.getField("notes"))!=null)
          fld.setData(ct.getNotes());

        break;

      case DISPMODE_CTI_ADD:
      case DISPMODE_CTI_EDIT:

        if(crntCti==null)
          return;

        fg = (FieldGroup)fields.getField("Call Tag Item");

        if((fld = fg.getField("equip_status"))!=null)
          fld.setData(Integer.toString(crntCti.getStatusCode()));

        if((fld = fg.getField("equip_serialnum"))!=null)
          fld.setData(crntCti.getSerialNum());

        if((fld = fg.getField("equip_model"))!=null)
          fld.setData(crntCti.getEquipModel());

        // equipment specific fields
        fg = (FieldGroup)fields.getField("Equipment Inventory");

        EquipmentData_EquipInv ei = (EquipmentData_EquipInv)crntCti.getEquipmentData();
        if(ei != null) {

          if((fld = fg.getField("equip_TsysDesc"))!=null)
            fld.setData(ei.getTsysEquipDesc());

          if((fld = fg.getField("equip_Descriptor"))!=null)
            fld.setData(ei.getDescriptor());

          if((fld = fg.getField("equip_TypeCode"))!=null)
            fld.setData(Integer.toString(ei.getEquipTypeCode()));

          if((fld = fg.getField("equip_MfrCode"))!=null)
            fld.setData(Integer.toString(ei.getEquipMfrCode()));

          if((fld = fg.getField("equip_Owner"))!=null)
            fld.setData(Integer.toString(ei.getOwner()));

          if((fld = fg.getField("equip_Status"))!=null)
            fld.setData(Integer.toString(ei.getStatusCode()));

          if((fld = fg.getField("equip_LendType"))!=null)
            fld.setData(Integer.toString(ei.getEquipLendTypeCode()));

          if((fld = fg.getField("equip_LendTypePrice"))!=null)
            fld.setData(Double.toString(ei.getLrbPrice()));

          if((fld = fg.getField("equip_DeployedDate"))!=null) {
            if(ei.getDeployedDate() == null)
              fld.setData("");
            else
              ((DateField)fld).setUtilDate(ei.getDeployedDate());
          }

          if((fld = fg.getField("equip_Class"))!=null)
            fld.setData(Integer.toString(ei.getClassCode()));

          if((fld = fg.getField("equip_OrigClass"))!=null)
            fld.setData(Integer.toString(ei.getOriginalClassCode()));

          if((fld = fg.getField("equip_assignedTo"))!=null)
            fld.setData(ei.getMerchantNumber());

          if((fld = fg.getField("equip_InvoiceNumber"))!=null)
            fld.setData(ei.getInvoiceNumber());

          if((fld = fg.getField("equip_TransId"))!=null)
            fld.setData(ei.getTransId());

          if((fld = fg.getField("equip_UnitCost"))!=null)
            fld.setData(Double.toString(ei.getUnitCost()));

        }

        // determine equip op if not already
        if(equipDispMode == EQUIPMODE_NONE)
          setEquipDisplayMode();

        break;

      case DISPMODE_CTN_ADD:
      case DISPMODE_CTN_EDIT:

        if(crntCtn==null)
          return;

        fg = (FieldGroup)fields.getField("Call Tag Notification");

        if((fld = getField("pkg_trk_num"))!=null)
          fld.setData(crntCtn.getPkgTrkNum());

        if((fld = getField("issue_date"))!=null && crntCtn.getIssueDate()!=null)
          ((DateField)fld).setUtilDate(crntCtn.getIssueDate());

        if((fld = getField("letter_name"))!=null)
          fld.setData(crntCtn.getLetterName());

        break;
    }

    // push fields message to the text message manager
    prepTextMessages();
  }

  public int getEquipDisplayMode()
  {
    return equipDispMode;
  }

  public void setEquipDisplayMode(int v)
  {
    equipDispMode = v;

    // alter the editability of the equip inventory fields
    FieldGroup fg = (FieldGroup)fields.getField("Equipment Inventory");
    fg.makeReadOnly();
    fields.getField("equip_assignment").unmakeReadOnly();
    switch(equipDispMode) {

      case EQUIPMODE_ASSIGN:
        fields.getField("equip_Owner").unmakeReadOnly();
        fields.getField("equip_Status").unmakeReadOnly();
        fields.getField("equip_LendType").unmakeReadOnly();
        fields.getField("equip_LendTypePrice").unmakeReadOnly();
        fields.getField("equip_UnitCost").unmakeReadOnly();
        fields.getField("equip_DeployedDate").unmakeReadOnly();
        break;

      case EQUIPMODE_UNASSIGN:
        break;


      case EQUIPMODE_ADDTOMESINV:
        fields.getField("equip_Descriptor").unmakeReadOnly();
        fields.getField("equip_TsysDesc").unmakeReadOnly();
        fields.getField("equip_MfrCode").unmakeReadOnly();
        fields.getField("equip_TypeCode").unmakeReadOnly();
        fields.getField("equip_UnitCost").unmakeReadOnly();
        fields.getField("equip_LendTypePrice").unmakeReadOnly();
        fields.getField("equip_Owner").unmakeReadOnly();
        fields.getField("equip_Status").unmakeReadOnly();
        fields.getField("equip_LendType").unmakeReadOnly();
        fields.getField("equip_DeployedDate").unmakeReadOnly();
        fields.getField("equip_Class").unmakeReadOnly();
        fields.getField("equip_OrigClass").unmakeReadOnly();
        fields.getField("equip_InvoiceNumber").unmakeReadOnly();
        fields.getField("equip_TransId").unmakeReadOnly();
        fields.getField("equip_ShippingCost").unmakeReadOnly();
        break;
    }
  }

  public void setEquipDisplayMode()
  {
    try {

      if(!crntCti.getEquipmentKey().isSet() || !crntCti.getEquipmentKey().exists())
        setEquipDisplayMode(EQUIPMODE_ADDTOMESINV);
      else {
        EquipmentData_EquipInv ei = (EquipmentData_EquipInv)crntCti.getEquipmentData();
        String eimn = ei.getMerchantNumber();
        if(eimn != null && eimn.length() > 0)
          setEquipDisplayMode(EQUIPMODE_UNASSIGN);
        else
          setEquipDisplayMode(EQUIPMODE_ASSIGN);
      }

    }
    catch(Exception e) {
      setEquipDisplayMode(EQUIPMODE_NONE);
    }
  }

  /**
   * HTML render related fields
   */

  public String renderHtml(String fname)
  {
    if(fname==null || fname.length()<1)
      return "";

    FieldGroup fg;

    if(fname.equals("ctid")) {
      return Long.toString(ct.getId());
    }
    else if(fname.equals("date_created")) {
      return this.getDateCreated();
    }
    else if(fname.equals("last_modified")) {
      return this.getLastModified();
    }
    else if(fname.equals("calltag_source")) {
      return ct==null? "":((ct.getParentKey()==null || !ct.getParentKey().isSet())? "":ct.getParentKey().getActionDesc());
    }
    else if(fname.equals("calltag_billingaccount")) {
      return merchantInfo==null? "":merchantInfo.getDbaName();
    }
    else if(fname.equals("parent_type") && !isEditable()) {
      return ct.getParentTypeDesc();
    }

    else if(fname.equals("equip_assignment")) {

      String eab = "";
      fg = (FieldGroup)fields.getField("Equipment Inventory");
      ButtonField bf = (ButtonField)fg.getField(fname);

      switch(equipDispMode) {
        case EQUIPMODE_UNASSIGN:
            bf.setButtonText("Unassign Equipment");
            eab = bf.renderHtml();
          break;
        case EQUIPMODE_ASSIGN:
            bf.setButtonText("Assign Equipment to Call Tag Billing Merchant");
            eab = bf.renderHtml();
          break;
        case EQUIPMODE_ADDTOMESINV:
            bf.setButtonText("Add to MeS Inventory");
            eab = bf.renderHtml();
          break;
        default:
          eab = "EXCEPTION";
          break;
      }

      return eab;
    }

    // default
    switch(displayMode) {
      case DISPMODE_CT_ADD:
      case DISPMODE_CT_EDIT:
        fg = (FieldGroup)fields.getField("Call Tag");
        break;
      case DISPMODE_CTI_ADD:
      case DISPMODE_CTI_EDIT:
        if(((FieldGroup)fields.getField("Call Tag Item")).getField(fname)==null)
          fg = (FieldGroup)fields.getField("Equipment Inventory");
        else
          fg = (FieldGroup)fields.getField("Call Tag Item");
        break;
      case DISPMODE_CTN_ADD:
      case DISPMODE_CTN_EDIT:
        fg = (FieldGroup)fields.getField("Call Tag Notification");
        break;
      default:
        return "";
    }

    return fg.renderHtml(fname);
  }

  public String renderSubmitButtons()
  {
    StringBuffer sb = new StringBuffer();

    ButtonField btn;

    switch(displayMode) {

      case DISPMODE_CT_ADD:
        btn = (ButtonField)getField("add");
        btn.setButtonText("Add Call Tag");
        sb.append(btn.renderHtml());
        break;

      /*
      case DISPMODE_CT_EDIT:
        btn = (ButtonField)getField("update");
        btn.setButtonText("Update Call Tag");
        sb.append(btn.renderHtml());
        // NOTE: no delete op for Call Tags
        break;
      */

      case DISPMODE_CTI_ADD:
        btn = (ButtonField)getField("add");
        btn.setButtonText("Add Call Tag Item");
        sb.append(btn.renderHtml());
        break;

      case DISPMODE_CTI_EDIT:
        btn = (ButtonField)getField("update");
        btn.setButtonText("Update Call Tag Item");
        sb.append(btn.renderHtml());
        sb.append("&nbsp;&nbsp;&nbsp;");
        btn = (ButtonField)getField("delete");
        btn.setButtonText("Delete Call Tag Item");
        sb.append(btn.renderHtml());
        break;

      case DISPMODE_CTN_ADD:
        btn = (ButtonField)getField("add");
        btn.setButtonText("Add Call Tag Notification");
        sb.append(btn.renderHtml());
        break;

      case DISPMODE_CTN_EDIT:
        btn = (ButtonField)getField("update");
        btn.setButtonText("Update Call Tag Notification");
        sb.append(btn.renderHtml());
        sb.append("&nbsp;&nbsp;&nbsp;");
        btn = (ButtonField)getField("delete");
        btn.setButtonText("Delete Call Tag Notification");
        sb.append(btn.renderHtml());
        break;

    }

    return sb.toString();
  }

  public String getHeadingName()
  {
    return getDisplayModeDescriptor();
  }

  public long getCallTagId()
  {
    return ct==null? -1L:ct.getId();
  }

  public String getDateCreated()
  {
    Date d = ct.getDateCreated();

    if(d.getTime()==0)
      return "-";

    SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy h:mm a");
    return sdf.format(d);
  }

  public String getLastModified()
  {
    Date d = ct.getLastModified();

    if(d.getTime()==0)
      return "-";

    SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy h:mm a");
    return sdf.format(d);
  }

  public String renderCallTagIdHtmlLink()
  {
    if(ct==null)
      return "";

    StringBuffer sb = new StringBuffer();

    // render as link only when display mode of item or notificaiton type
    if(displayMode == DISPMODE_CT_ADD || displayMode == DISPMODE_CT_EDIT) {

      sb.append(ct.getId());

    } else {

      sb.append("<a href=\"");
      sb.append(SERVLET_URL);
      sb.append('?');
      sb.append(autoSubmitName);
      sb.append('=');
      sb.append("vctd");
      sb.append("\">");
      sb.append(ct.getId());
      sb.append("</a>");

    }

    return sb.toString();
  }

  protected String renderAccountDetailHtmlLink(String mn, String linkText)
  {
    StringBuffer sb = new StringBuffer();

    sb.append("<a href=\"");
    sb.append(SERVLET_URL);
    sb.append('?');
    sb.append(autoSubmitName);
    sb.append('=');
    sb.append("vm");
    sb.append("&mn=");
    sb.append(mn);
    sb.append("\">");
    sb.append(linkText);
    sb.append("</a>");

    return sb.toString();
  }

  public String renderBillingAccountDetailHtmlLink(String linkText)
  {
    return renderAccountDetailHtmlLink(Long.toString(ct.getMerchNumber()), linkText);
  }

  public String renderParentHtmlLink()
  {
    try {
      return renderParentHtmlLink(ct.getParentKey().toString());
    }
    catch(Exception e) {
      return "";
    }
  }
  public String renderParentHtmlLink(String linkText)
  {
    StringBuffer sb = new StringBuffer();

    try {
      Key key = ct.getParentKey();
      if(key.exists()) {
        sb.append("<a href=\"");
        sb.append(SERVLET_URL);
        sb.append('?');
        sb.append(autoSubmitName);
        sb.append('=');
        sb.append("vctp");
        sb.append("&parent_type=");
        sb.append(ct.getParentType());
        sb.append('&');
        sb.append(key.toQueryString());
        sb.append("\">");
        sb.append(linkText);
        sb.append("</a>");
      } else {
        sb.append(key.toString());
        sb.append(" - INVALID");
      }
    }
    catch(Exception e) {
      sb.append("-NOT SET-");
    }

    return sb.toString();
  }

  public String renderAddItemLink()
  {
    StringBuffer sb = new StringBuffer();

    /*
    sb.append("<a href=\"");
    sb.append(SERVLET_URL);
    sb.append('?');
    sb.append(autoSubmitName);
    sb.append('=');
    sb.append("vacti");
    sb.append("\">Add Item</a>");
    */

    sb.append("<a href=\"" + SERVLET_URL +
      "?method=addInItem\">Add Incoming</a>");

    sb.append("&nbsp;|&nbsp;");

    sb.append("<a href=\"" + SERVLET_URL +
      "?method=addOutItem\">Add Outgoing</a>");

    return sb.toString();
  }

  public String renderAddNotificationLink()
  {
    StringBuffer sb = new StringBuffer();

    sb.append("<a href=\"");
    sb.append(SERVLET_URL);
    sb.append('?');
    sb.append(autoSubmitName);
    sb.append('=');
    sb.append("vactn");
    sb.append("\">Add Notification</a>");

    return sb.toString();
  }

  public String renderCrntItemEquipDetailsHtmlLink(boolean showKey)
  {
    if(crntCti==null)
      return "";

    StringBuffer sb = new StringBuffer();

    try {
      EquipmentDataBase ed = crntCti.getEquipmentData();

      if(ed.getKey().exists()) {

        sb.append("<a href=\"");
        sb.append(SERVLET_URL);
        sb.append('?');
        sb.append(autoSubmitName);
        sb.append('=');
        sb.append("ved");
        sb.append('&');
        sb.append(ed.getKey().toQueryString());
        sb.append("\">");
        sb.append(ed.getDescriptor());
        if(showKey) {
          sb.append("<div>(");
          sb.append(ed.getKey().toString());
          sb.append(")</div>");
        }
        sb.append("</a>");

      } else {

        sb.append(ed.getKey().toString());
        sb.append(" - NON MeS Inventory Equipment");

      }
    }
    catch(Exception e) {
      //sb.append("-NOT SET-");
      log.error("renderCrntItemEquipDetailsHtmlLink() EXCEPTION: "+e.toString());
    }

    return sb.toString();
  }

  public String renderSourceLookupHtmlTable()
  {
    if(ct==null)
      return "";

    StringBuffer sb = new StringBuffer();

    sb.append("<table cellpadding=0 cellspacing=2 border=0 width=100%>");

      // source lookup

      sb.append("<tr>");
        sb.append("<td class=smallText>Type</td>");
        sb.append("<td width=5>&nbsp;</td>");
        sb.append("<td class=formFields>");

        sb.append(renderHtml("parent_type"));

        sb.append("<br>&nbsp;");
        sb.append("</td>");
      sb.append("</tr>");

      sb.append("<tr>");
        sb.append("<td class=smallText>Source Ref Num</td>");
        sb.append("<td width=5>&nbsp;</td>");
        sb.append("<td class=formFields>");
        sb.append(renderHtml("parentkey_refnum"));
        sb.append("</td>");
      sb.append("</tr>");

      sb.append("<tr>");
        sb.append("<td colspan=3 class=smallBold>- or -</td>");
      sb.append("</tr>");

      /*
      sb.append("<tr>");
        sb.append("<td class=smallText>Equip Part Num</td>");
        sb.append("<td width=5>&nbsp;</td>");
        sb.append("<td class=formFields>");
        sb.append(renderHtml("parentkey_partnum"));
        sb.append("</td>");
      sb.append("</tr>");
      */

      sb.append("<tr>");
        sb.append("<td class=smallText>Equip Serial Num</td>");
        sb.append("<td width=5>&nbsp;</td>");
        sb.append("<td class=formFields>");
        sb.append(renderHtml("parentkey_serialnum"));
        sb.append("</td>");
      sb.append("</tr>");

      sb.append("<tr>");
        sb.append("<td align=left colspan=3 class=smallBold>");
        sb.append(renderHtml("Set Source"));
        sb.append("</td>");
      sb.append("</tr>");

    sb.append("</table>");

    return sb.toString();
  }

  public String renderBillingAccountHtmlTable()
  {
    if(ct==null)
      return "";

    if(merchantInfo==null)
      return "";


    StringBuffer sb = new StringBuffer();
    String rowHgt = "25";

    sb.append("<table cellpadding=0 cellspacing=2 border=0 width=100%>");

      /*
      sb.append("<tr height=");
      sb.append(rowHgt);
      sb.append(">");
        sb.append("<td class=smallText>Bill Date</td>");
        sb.append("<td width=5>&nbsp;</td>");
        sb.append("<td class=smallText>");
        sb.append(renderHtml("billDate"));
        sb.append("</td");
      sb.append("</tr>");

      sb.append("<tr height=");
      sb.append(rowHgt);
      sb.append(">");
        sb.append("<td class=smallText>Bill Amount</td>");
        sb.append("<td width=5>&nbsp;</td>");
        sb.append("<td class=smallText>");
        sb.append(renderHtml("billAmount"));
        sb.append("</td");
      sb.append("</tr>");

      sb.append("<tr height=");
      sb.append(rowHgt);
      sb.append(">");
        sb.append("<td class=smallText>Bill Charged</td>");
        sb.append("<td width=5>&nbsp;</td>");
        sb.append("<td class=smallText>");
        sb.append(renderHtml("billCharged"));
        sb.append("</td");
      sb.append("</tr>");
      */

      /*
      sb.append("<tr height=");
      sb.append(rowHgt);
      sb.append(">");
        sb.append("<td colspan=3><hr size=0></td>");
      sb.append("</tr>");
      */

      sb.append("<tr height=");
      sb.append(rowHgt);
      sb.append(">");
        sb.append("<td class=smallText>Dba Name</td>");
        sb.append("<td width=5>&nbsp;</td>");
        sb.append("<td class=smallGrey>");
        sb.append(merchantInfo.getDbaName());
        sb.append("</td");
      sb.append("</tr>");

      sb.append("<tr height=");
      sb.append(rowHgt);
      sb.append(">");
        sb.append("<td class=smallText>Merchant Number</td>");
        sb.append("<td width=5>&nbsp;</td>");
        sb.append("<td class=smallText>");
        sb.append(renderBillingAccountDetailHtmlLink(merchantInfo.getMerchantNumber()));
        sb.append("</td");
      sb.append("</tr>");

      /*
      sb.append("<tr height=");
      sb.append(rowHgt);
      sb.append(">");
        sb.append("<td class=smallText>Bank Number</td>");
        sb.append("<td width=5>&nbsp;</td>");
        sb.append("<td class=smallGrey>");
        sb.append(merchantInfo.getBankNumber());
        sb.append("</td");
      sb.append("</tr>");

      sb.append("<tr height=");
      sb.append(rowHgt);
      sb.append(">");
        sb.append("<td class=smallText>Association Number</td>");
        sb.append("<td width=5>&nbsp;</td>");
        sb.append("<td class=smallGrey>");
        sb.append(merchantInfo.getAssocNumber());
        sb.append("</td");
      sb.append("</tr>");

      sb.append("<tr height=");
      sb.append(rowHgt);
      sb.append(">");
        sb.append("<td class=smallText>App Type</td>");
        sb.append("<td width=5>&nbsp;</td>");
        sb.append("<td class=smallGrey>");
        sb.append(merchantInfo.getAppTypeDesc());
        sb.append("</td");
      sb.append("</tr>");
      */

      sb.append("<tr height=");
      sb.append(rowHgt);
      sb.append(">");
        sb.append("<td class=smallText>Sales Channel</td>");
        sb.append("<td width=5>&nbsp;</td>");
        sb.append("<td class=smallGrey>");
        sb.append(merchantInfo.getMerchantTypeDesc());
        sb.append("</td");
      sb.append("</tr>");

    sb.append("</table>");

    return sb.toString();
  }

  public String renderItemsHtmlTable()
  {
    StringBuffer sb = new StringBuffer();

    sb.append("<div>");
    sb.append("<table width=100% border=\"0\" cellspacing=\"0\" cellpadding=\"2\">");
      sb.append("<tr>");
        sb.append("<td class=smallBold align=left>Item(s)</td>");
        sb.append("<td align=right>");
        sb.append(isEditable()? renderAddItemLink():"&nbsp;");
        sb.append("</td>");
      sb.append("</tr>");
    sb.append("</table>");
    sb.append("</div>");

    sb.append("<div>");
    if(hasItems())
      sb.append(renderTableView(itemsTable));
    else
      sb.append("<span class=smallText>Currently, no items on record.</span>");
    sb.append("</div>");

    return sb.toString();
  }

  public String renderNotificationsHtmlTable()
  {
    StringBuffer sb = new StringBuffer();

    sb.append("<div>");
    sb.append("<table width=100% border=\"0\" cellspacing=\"0\" cellpadding=\"2\">");
      sb.append("<tr>");
        sb.append("<td align=left class=smallBold>Notificaction(s)</td>");
        sb.append("<td align=right>");
          sb.append(isEditable()? renderAddNotificationLink():"&nbsp;");
        sb.append("</td>");
      sb.append("</tr>");
    sb.append("</table>");
    sb.append("</div>");

    sb.append("<div>");
    if(hasNotifications())
      sb.append(renderTableView(notificationsTable));
    else
      sb.append("<span class=smallText>Currently, no notifications on record.</span>");
    sb.append("</div>");

    return sb.toString();
  }

  private String renderTableView(TableView tv)
  {
    String[]      colNames  = null;
    boolean       tgl       = false;
    String        rowColor  = "";
    StringBuffer  sb        = new StringBuffer();

    sb.append("<table width=100% border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"tableFill\">");
      sb.append("<tr>");
        sb.append("<td>");
          sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">");

    // column headings
    sb.append("<tr>");
    colNames = tv.getColumnNames();
    for(int i=0; i<colNames.length; i++) {
      sb.append("<td align=left class=tableColumnHead>");
      sb.append(colNames[i]);
      sb.append("</td>");
    }
    sb.append("</tr>");

    // column data
    for(int j=0;j<tv.getNumRows();j++) {
      rowColor = ((tgl=(!tgl)) ? "tableRowHighlightOff" : "tableRowHighlightOn");
      sb.append("<tr class="+rowColor+">");
      for(int i=0; i<colNames.length; i++) {
        sb.append("<td align=center class=tableDataPlain>");
        sb.append(tv.getHtmlCell(j+1,i+1));
        sb.append("</td>");
      }
      sb.append("</tr>");
    }

          sb.append("</table>");
        sb.append("</td>");
      sb.append("</tr>");
    sb.append("</table>");

    return sb.toString();
  }

  //////////////////

  private class EquipTypesTable extends DropDownTable
  {
    EquipTypesTable ()
    {
      super();

      addElement("","");

      String[][] arr = getCallTagDAO().getEquipTypes();

      if(arr == null)
        return;

      for(int i=0;i<arr.length;i++) {
        addElement(arr[i][1],arr[i][0]);
      }
    }
  }

  private class EquipManufacturersTable extends DropDownTable
  {
    EquipManufacturersTable ()
    {
      super();

      addElement("","");

      String[][] arr = getCallTagDAO().getEquipManufacturers();

      if(arr == null)
        return;

      for(int i=0;i<arr.length;i++) {
        addElement(arr[i][1],arr[i][0]);
      }
    }
  }

  private class AppTypesTable extends DropDownTable
  {
    AppTypesTable()
    {
      super();

      addElement("","");

      String[][] arr = getCallTagDAO().getAppTypes();

      if(arr == null)
        return;

      for(int i=0;i<arr.length;i++) {
        addElement(arr[i][1],arr[i][0]);
      }
    }
  }

  private class EquipClassesTable extends DropDownTable
  {
    EquipClassesTable()
    {
      super();

      addElement("","");

      String[][] arr = getCallTagDAO().getEquipClasses();

      if(arr == null)
        return;

      for(int i=0;i<arr.length;i++) {
        addElement(arr[i][1],arr[i][0]);
      }
    }
  }

  /*
  private class CallTagStatusDropDownTable extends DropDownTable
  {
    CallTagStatusDropDownTable()
    {
      super();

      addElement("","");

      String[][] arr = getCallTagDAO().getCallTagStatuses();

      if(arr == null)
        return;

      for(int i=0;i<arr.length;i++) {
        addElement(arr[i][1],arr[i][0]);
      }
    }
  }
  */

  /*
  private class CallTagItemStatusDropDownTable extends DropDownTable
  {
    CallTagItemStatusDropDownTable()
    {
      super();

      addElement("","");

      if(ctiStatus==null || ctiStatus.length<1)
        return;

      for(int i=0;i<ctiStatus.length;i++) {
        addElement(ctiStatus[i][1],ctiStatus[i][0]);
      }
    }
  }
  */

  private class ParentTypeDropDownTable extends DropDownTable
  {
    ParentTypeDropDownTable()
    {
      super();

      addElement("","");

      String[][] arr = getCallTagDAO().getParentTypes();

      if(arr == null)
        return;

      for(int i=0;i<arr.length;i++) {
        addElement(arr[i][1],arr[i][0]);
      }
    }
  }

  private class EquipLendTypeDropDownTable extends DropDownTable
  {
    EquipLendTypeDropDownTable()
    {
      super();

      addElement("","");

      if(eiLendTypes == null)
        return;

      for(int i=0;i<eiLendTypes.length;i++) {
        addElement(eiLendTypes[i][1],eiLendTypes[i][0]);
      }
    }
  }

  private class EquipDescTable extends DropDownTable
  {
    EquipDescTable()
    {
      super();
      addElement("","select one");
      if(equipmentList == null || equipmentList.size()==0){
        return;
      }

      for(int i=0;i<equipmentList.size();i++){
        String s = (String)equipmentList.get(i);
        String sSub = (s.indexOf("-") != -1)?s.substring(0,s.indexOf("-")).trim():s;
        addElement(sSub,s);
      }
    }
  }

  /*
  private class EquipModelDropDownTable extends DropDownTable
  {
    EquipModelDropDownTable()
    {
      super();

      addElement("","");

      String[][] arr = getCallTagDAO().getEquipModels();

      if(arr == null)
        return;

      for(int i=0;i<arr.length;i++) {
        addElement(arr[i][1],arr[i][0]);
      }
    }
  }
  */

  /*
  private class EquipPartNumsDropDownTable extends DropDownTable
  {
    EquipPartNumsDropDownTable()
    {
      super();

      addElement("","");

      String[][] arr = getCallTagDAO().getEquipModels();

      if(arr == null)
        return;

      for(int i=0;i<arr.length;i++) {
        addElement(arr[i][1],arr[i][0]);
      }
    }
  }
  */

  private class EquipInvStatusDropDownTable extends DropDownTable
  {
    EquipInvStatusDropDownTable()
    {
      super();

      addElement("","");

      if(eiStatus== null)
        return;

      for(int i=0;i<eiStatus.length;i++) {
        addElement(eiStatus[i][1],eiStatus[i][0]);
      }
    }
  }


  /**
   * prepTextMessages()
   *
   * Transfers FieldBean based messages (usu. error messages) to the TextMessageManager.
   */
  private final void prepTextMessages()
  {
    for (Iterator i = fields.iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      if (f instanceof FieldGroup)
      {
        prepTextMessages((FieldGroup)f);
      }

    }

    super.clearErrors();
  }

  private final void prepTextMessages(FieldGroup fg)
  {
    // pump out the transformed field errors
    Vector fldErrors = fg.getErrors();
    if(fldErrors != null && fldErrors.size()>0) {
      for(Enumeration e=fldErrors.elements();e.hasMoreElements();)
        tmm.err(e.nextElement().toString());
    }
  }

  public List getEquipmentItemDetails()
  {
    ArrayList details = new ArrayList();
    for (Iterator i = ct.getItems().iterator(); i.hasNext();)
    {
      details.add(new EquipmentItemDetails((CallTagItem)i.next()));
    }
    return details;
  }
} // class CallTagDetailBean
