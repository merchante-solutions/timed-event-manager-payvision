package com.mes.calltag;

// log4j classes.
import org.apache.log4j.Category;
import com.mes.constants.mesConstants;
import com.mes.equipment.EquipmentDataBase;
import com.mes.equipment.EquipmentDataBuilder;
import com.mes.tools.EquipmentKey;


/**
 * CallTagItem class
 * 
 * One or more notifications can be associated with a single call tag.
 */
public final class CallTagItem extends CallTagBase
{
  // create class log category
  static Category log = Category.getInstance(CallTagItem.class.getName());
  
  // constants
  
  // call tag item status - corres. to mesdb.EQUIP_CALLTAG_ITEM_STATUS
  public static final int     STATUS_UNDEFINED      = 0;
  public static final int     STATUS_OUTGOING       = 1;
  public static final int     STATUS_INCOMING       = 2;
  //public static final int     STATUS_OUTSTANDING    = 2;

  
  // data members
  protected long              ctid                   = 0L;
  protected EquipmentKey      equipKey               = null;
  protected int               status_code            = STATUS_UNDEFINED;
  protected String            status_desc            = "";
  protected String            serialNum              = "";
  protected String            equipModel             = "";  // aka part number
  protected String            received               = "";
  
  protected EquipmentDataBase ed                     = null;
  
  
  // class methods
  
  public static String getStatusDesc(int statusCode)
  {
    switch(statusCode) {
      case STATUS_INCOMING:
        return "Incoming";
      case STATUS_OUTGOING:
        return "Outgoing";
      default:
        return "Undefined Status";
    }
  }

  
  // object methods
  
  // construction
  public CallTagItem()
  {
  }
  
  public void clear()
  {
    ctid        = 0L;
    equipKey    = null;
    status_code = STATUS_UNDEFINED;
    status_desc = "";
    ed          = null;
    
    super.clear();
  }
    
  // accessors
  public long         getCallTagId()      { return ctid; }
  
  public EquipmentKey getEquipmentKey()   
  {
    if(equipKey == null)
      setEquipmentKey();

    return equipKey;
  }
  
  public int          getStatusCode()     { return status_code; }
  public String       getStatusDesc()     { return status_desc; }
  public String       getSerialNum()      { return serialNum; }
  public String       getEquipModel()     { return equipModel; }
  
  public EquipmentDataBase getEquipmentData()
  {
    if (ed == null)
    {
      try
      {
        ed = EquipmentDataBuilder.getInstance().load(getEquipmentKey());
      }
      catch(Exception e)
      {
        log.error("getEquipmentData() EXCEPTION: " + e);
      }
    }
    return ed;
  }
  
  public void setEquipmentData(EquipmentDataBase equipData)
  {
    try
    {
      ed         = equipData;
      equipKey   = equipData.getKey();
      serialNum  = equipKey.getSerialNum();
      equipModel = equipKey.getPartNum();
    }
    catch(Exception e)
    {
      log.error("setEquipmentData() EXCEPTION: " + e);
    }
  }
  
  public int getEquipType()
  {
    try {
      return getEquipmentKey().getEquipType();
    }
    catch(Exception e) {
      return mesConstants.EQUIPTYPE_UNDEFINED;
    }
  }
  
  public String getEquipmentDisposition()
  {
    return getEquipmentData()==null? "" : ed.getDisposition();
  }
    
  public String getPartNum()
  {
    return getEquipModel();
  }
  
  public long getAppSeqNum()
  {
    try {
      return getEquipmentKey().getMerchEquipKey().appSeqNum;
    }
    catch(Exception e) {
      return 0L;
    }
  }

  public int getLendTypeCode()
  {
    try {
      return getEquipmentKey().getMerchEquipKey().lendTypeCode;
    }
    catch(Exception e) {
      return -1;
    }
  }

  // mutators
  
  public void setCallTagId(long v)
  {
    if(v==ctid)
      return;
    
    this.ctid=v;
    dirty();
  }
  
  public void setEquipmentKey()
  {
    equipKey = new EquipmentKey(equipModel,serialNum);
  }
  
  public void setEquipmentKey(EquipmentKey key)
  {
    if(key==null || key.equals(this.equipKey))
       return;
    
    ed = null;
    this.equipKey   = key;
    this.serialNum  = key.getSerialNum();
    this.equipModel = key.getPartNum();
    
    dirty();
  }
  
  public void setSerialNum(String v)
  {
    if(v == null || v.equals(this.serialNum))
      return;
      
    this.serialNum = v;
    
    this.equipKey = null;
    this.ed = null;
    
    dirty();
  }
  
  public void setEquipModel(String v)
  {
    if(v == null || v.equals(this.equipModel))
      return;
      
    this.equipModel = v;
    
    this.equipKey = null;
    this.ed = null;
    
    dirty();
  }
  
  public void setStatusCode(int v)
  {
    if(v==status_code)
      return;
    status_code=v;
    // hack: for some reason there is internal status desc generation
    // as well as db maintenance, so when adding new items we need to
    // manually set status desc (later when loaded from db it will be
    // loaded from the status table)
    setStatusDesc(getStatusDesc(v));
    dirty();
  }

  public void setStatusDesc(String v)
  {
    if(v==null || v.equals(status_desc))
      return;
    status_desc=v;
    dirty();
  }
  
  public void setReceived(boolean isReceived)
  {
    received = isReceived ? "Y" : "N";
  }
  
  public void setReceived(String received)
  {
    this.received = received != null && received.equals("Y") ? "Y" : "N";
  }
  
  public String getReceived()
  {
    return received;
  }
  
} // class CallTagItem
