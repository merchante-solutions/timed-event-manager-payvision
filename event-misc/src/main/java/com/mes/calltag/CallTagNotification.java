package com.mes.calltag;

import java.text.DateFormat;
import java.util.Comparator;
import java.util.Date;
// log4j classes.
import org.apache.log4j.Category;

/**
 * CallTagNotification class
 *
 * One or more notifications can be associated with a single call tag.
 */
public class CallTagNotification extends CallTagBase
{
  // create class log category
  static Category log = Category.getInstance(CallTagNotification.class.getName());

  // constants
  static final String DHL_LINK = "http://track.dhl-usa.com/TrackByNbr.asp?ShipmentNumber=";

  // static null constant references
  public static CallTagConstants cmds   = null;
  public static CallTagConstants paths  = null;

  // data members
  protected long         ctid                = 0L;
  protected String       pkgTrkNum           = "";
  protected Date         issueDate           = null;
  protected String       letterName          = "";   // name (template) of notification (cover) letter used

  private class Comparator_IssueDate
    implements Comparator
  {
    public int compare(Object o1, Object o2)
    {
      // compare by issue date
      return ((CallTagNotification)o1).issueDate.compareTo(((CallTagNotification)o2).issueDate);
    }
  } // class Comparator_IssueDate


  // construction
  public CallTagNotification()
  {
  }

  public void clear()
  {
    pkgTrkNum = "";
    if(issueDate != null)
      issueDate.setTime(0L);
    letterName = "";

    super.clear();
  }


  // accessors
  public long     getCallTagId()      { return ctid; }
  public String   getPkgTrkNum()      { return pkgTrkNum; }
  public Date     getIssueDate()      { return issueDate; }
  public String   getLetterName()     { return letterName; }
  public String   getDHLLink()        { return DHL_LINK+pkgTrkNum; }

  public Comparator_IssueDate getIssueDateComparator()
  {
    return new Comparator_IssueDate();
  }


  // mutators

  public void setCallTagId(long v)
  {
    if(v==this.ctid)
      return;

    this.ctid=v;
    dirty();
  }

  public void setPkgTrkNum(String v)
  {
    if(v==null || v.equals(pkgTrkNum))
      return;
    pkgTrkNum=v;
    is_dirty=true;
  }

  public void setIssueDate(Date v)
  {
    if(v==null || v.equals(issueDate))
      return;
    issueDate=v;
    is_dirty=true;
  }

  public void setLetterName(String v)
  {
    if(v==null || v.equals(letterName))
      return;
    letterName=v;
    is_dirty=true;
  }

  public String getIssueDateString()
  {
    DateFormat df = DateFormat.getDateInstance();
    return issueDate==null?"":df.format(issueDate);
  }

  public String getLetterLink()
  {
    StringBuffer html = new StringBuffer();
    html.append("<a href=\"");
    html.append(paths.PATH_PDF_SERVLET_URL);
    html.append("?pdfType=ctn");
    html.append("&ctid=" + ctid);
    html.append("&ltr=" + letterName);
    html.append("&dhl=" + pkgTrkNum);
    html.append("\" target='_notifyLtr'>");
    html.append(letterName);
    html.append("</a>");
    return html.toString();
  }

  public String renderActions()
  {
      StringBuffer html = new StringBuffer();
      html.append("<a href=\"");
      html.append(paths.PATH_SERVLET_URL);
      html.append("?method=" + cmds.CMD_DEL_NOTIFICATION);
      html.append("&ctnid=" + getId());
      html.append("\">delete</a>");
      html.append(" | ");
      html.append("<a href=\"");
      html.append(paths.PATH_SERVLET_URL);
      html.append("?method=" + cmds.CMD_EDIT_NOTIFICATION);
      html.append("&id=" + getId());
      html.append("\">edit</a>");
      return html.toString();
  }

} // class CallTagNotification
