/*@lineinfo:filename=CallTagDetailQueueBean*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.calltag;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.constants.MesQueues;
import com.mes.queues.CallTagQueue;
import com.mes.queues.QueueBase;
import com.mes.queues.QueueData;
import com.mes.queues.QueueTools;
import com.mes.support.DateTimeFormatter;
import com.mes.support.TextMessageManager;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;


/**
 * CallTagDetailQueueBean
 */
public final class CallTagDetailQueueBean extends CallTagDetailBean
{
  // create class log category
  static Category log = Category.getInstance(CallTagDetailQueueBean.class.getName());
  
  // constants
  public static final   int           QUEUE_ITEM_TYPE                = MesQueues.Q_ITEM_TYPE_CALLTAG;

  
  // data members
  protected   CallTagQueue      queueBean         = null;
  protected   int               curRow            = -1;
  protected   TreeMap           commands          = null;     
    // holds string nme/vals representing commands "available".
    // key:    "{Description}"
    // value:  "{URL-based command string (in QueryString form) recognizable by ACRController}"
  protected   Hashtable         commandCols       = null;  
  // associates each commmand to a column number
  // key:     "{Description}"
  // value:    {col num}
  
  
  public class ActionHistory
  {
    // data members
    private Vector rows = new Vector();

    // construction
    public ActionHistory()
    {
      if(ct!=null)
        load(ct.getId());
    }

    public class Row
    {
      private String logDate;
      private String eventDescription;
      private String userLogin;
      
      public Row(ResultSet rs)
      {
        loadData(rs);
      }
      
      private void loadData(ResultSet rs)
      {
        try
        {
          logDate             = DateTimeFormatter
                                  .getFormattedDate(rs.getTimestamp("log_date"),
                                    "MM/dd/yy hh:mm:ss a");
          userLogin           = rs.getString("user_login");
          eventDescription    = rs.getString("event_description");
          
          int oldQueue        = rs.getInt   ("old_type");
          int newQueue        = rs.getInt   ("new_type");
          int intAction       = rs.getInt   ("action");
        }
        catch (Exception e)
        {
          log.error("ActionHistory.Row.loadData() EXCEPTION: '"+e.getMessage()+"'.");
        }
      }
      
      public String getLogDate()
      {
        return logDate;
      }
      public String getEventDescription()
      {
        return eventDescription;
      }
      public String getUserLogin()
      {
        return userLogin;
      }
    }
  
    private void load(long qId)
    {
      ResultSetIterator it  = null;
      ResultSet         rs  = null;
      
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:118^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    l.log_seq_num,
//                      l.log_date                  log_date,
//                      l.action                    action,
//                      l.old_type                  old_type,
//                      l.new_type                  new_type,
//                      l.description               event_description,
//                      l.user_name                 user_login,
//                      nvl(l.description,'')   	  description
//            from      q_log             l
//            where     l.id = :qId
//                      and l.user_name not like 'q_data%'
//                      and (l.new_type >= :MesQueues.Q_CALLTAG__START and l.new_type <= :MesQueues.Q_CALLTAG__END)
//                      
//            order by  log_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    l.log_seq_num,\n                    l.log_date                  log_date,\n                    l.action                    action,\n                    l.old_type                  old_type,\n                    l.new_type                  new_type,\n                    l.description               event_description,\n                    l.user_name                 user_login,\n                    nvl(l.description,'')   \t  description\n          from      q_log             l\n          where     l.id =  :1 \n                    and l.user_name not like 'q_data%'\n                    and (l.new_type >=  :2  and l.new_type <=  :3 )\n                    \n          order by  log_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.calltag.CallTagDetailQueueBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,qId);
   __sJT_st.setInt(2,MesQueues.Q_CALLTAG__START);
   __sJT_st.setInt(3,MesQueues.Q_CALLTAG__END);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.calltag.CallTagDetailQueueBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:134^9*/
        
        rs = it.getResultSet();
        while (rs.next())
        {
          rows.add(new Row(rs));
        }
      }
      catch (Exception e)
      {
        log.error("load() EXCEPTION: "+e.getMessage());
      }
      finally
      {
        try { it.close(); } catch (Exception e) {}
        try { rs.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  
    public Iterator iterator()
    {
      return rows.iterator();
    }
  
  } // ActionHistory

  public ActionHistory getQueueActionHistory()
  {
    return new ActionHistory();
  }

  
  // class methods

  public static String getQueueDescriptor(int q)
  {
    switch(q) {
      case MesQueues.Q_CALLTAG_INCOMPLETE:
        return "Incomplete (New)";
      case MesQueues.Q_CALLTAG_COMPLETE:
        return "Complete";
      case MesQueues.Q_CALLTAG_CANCELLED:
        return "Cancelled (Aborted)";
      case MesQueues.Q_CALLTAG_TERMINATED:
        return "Terminated (Equipment not Received)";
    }
    
    return "UNKNOWN CALLTAG QUEUE";
  }
  
  public static String getQueueOpDescriptor(int srcQ,int dstQ)
  {
    return getQueueOpDescriptor(srcQ,dstQ,false);
  }
  public static String getQueueOpDescriptor(int srcQ,int dstQ,boolean showFrom)
  {
    if(srcQ==MesQueues.Q_NONE)
      return "Initiate Call Tag";
    
    StringBuffer sb = new StringBuffer(64);
    
    if(dstQ==MesQueues.Q_CALLTAG_CANCELLED) {
      sb.append("Cancel Call Tag");
    } else if(dstQ==MesQueues.Q_CALLTAG_COMPLETE) {
      sb.append("Complete Call Tag");
    } else {
      if(showFrom) {
        sb.append("Move from ");
        sb.append(getQueueDescriptor(srcQ));
        sb.append(" to ");
      } else
        sb.append("Move to ");
      sb.append(getQueueDescriptor(dstQ));
    }
    
    return sb.toString();
  }
  
  
  // object methods
  
  // construction
  public CallTagDetailQueueBean()
  {
    super();
  }
  
  public CallTagQueue getQueueBean()
  {
    return queueBean;
  }
  
  public boolean isQueueBeanSet()
  {
    return (queueBean!=null);
  }
  
  public void setQueueBean(QueueBase queueBean)
  {
    if(queueBean instanceof CallTagQueue)
      this.queueBean=(CallTagQueue)queueBean; // NOTE: allow to set null ref
    
    if(queueBean.getUser()==null && this.user!=null)
      queueBean.setUser(this.user);
  }

  public void unloadQueueItem()
  {
    queueBean = null;
    curRow=-1;
    clearCommands();
  }
  
  public void loadQueueItem() throws UnsupportedEncodingException {
    log.debug("loadQueueItem() - START");
    
    if(queueBean==null || ct==null)
      return;
    
    long id = ct.getId();
    
    queueBean.loadItem(id);
    
    // re-set current row to match the current queue item
    curRow=-1;
    for(int i=0;i<queueBean.getRows().size();i++) {
      if(id==((QueueData)queueBean.getRows().elementAt(i)).getId()) {
        curRow=i;
        break;
      }
    }
    
    log.debug("loadQueueItem() - generating commands...");
    this.generateQueueCommands();
    
    log.debug("Queue item loaded.");
  }
  
  public boolean isItemInQueue()
  {
    return (queueBean!=null && queueBean.getQueueItem() != null);
  }

  public String renderHtml(String fname)
  {
    if(isQueueBeanSet()) {
      
      if(fname.equals("calltag_source"))
        return queueBean.getQueueItem().getDescription();
    
    }

    return super.renderHtml(fname);
  }
  
  public void setCallTag(CallTag ct)
  {
    super.setCallTag(ct);
    unloadQueueItem();
  }
    
  public static int[][] getAllowedQueueOps()
  {
    return new int[][] 
    {
       {MesQueues.Q_NONE,MesQueues.Q_CALLTAG_INCOMPLETE}

      ,{MesQueues.Q_CALLTAG_INCOMPLETE,MesQueues.Q_CALLTAG_COMPLETE}
      ,{MesQueues.Q_CALLTAG_INCOMPLETE,MesQueues.Q_CALLTAG_CANCELLED}
      ,{MesQueues.Q_CALLTAG_INCOMPLETE,MesQueues.Q_CALLTAG_TERMINATED}
    };      
  }

  public static boolean queueInitiate(CallTag ct, String opNotes, TextMessageManager tmm)
  {
    return false;
  }
  
  /**
   * queueInitiate()
   * 
   * Initiates a Call Tag to the queue system placing the CallTag in one or more initial queues.
   * This initiates the queue process for a given Call Tag.
   */
  public boolean queueInitiate()
  {
    return queueInitiate(null,false);
  }
  public boolean queueInitiate(String opNotes, boolean isRush)
  {
    return queueInitiate(ct.getId(), opNotes, isRush, this.user, this.tmm);
  }
  public static boolean queueInitiate(long ctid, String opNotes, boolean isRush, UserBean user, TextMessageManager tmm)
  {
    int numInitiated=0;
    
    try {
    
      final int[][] availQOps = getAvailableQueueOps(new int[] {MesQueues.Q_NONE});
      
      if(availQOps==null || availQOps.length<1)
        return true;  // no available ops and no penalty since no exception thrown
      
      for(int i=0;i<availQOps.length;i++) {
        if(!doQueueOp(ctid,availQOps[i][0],availQOps[i][1],opNotes,isRush,user,tmm)) {
          log.error("Attempt to do queue op (src:'"+availQOps[i][0]+", dst:"+availQOps[i][1]+"' failed for CallTag '"+ctid+"'.");
        } else
          numInitiated++;
      }

    }
    catch(Exception e) {
      log.error("Exception occurred while initiating Call Tag '"+ctid+"' to the queues: '"+e.getMessage()+"'.");
    }
    
    return (numInitiated>0);  // return true when at least one ok since unable to do all or nothing trans from here
  }
  
  protected int[][] getAvailableQueueOps()
  {
    return getAvailableQueueOps(QueueTools.getResidentQueues(ct.getId(),MesQueues.Q_ITEM_TYPE_CALLTAG));
  }
  
  protected static int[][] getAvailableQueueOps(int[] srcQueues)
  {
    int cnt = 0;
    int[][] queueOps = getAllowedQueueOps();
    
    for(int i=0;i<srcQueues.length;i++) {
      for(int j=0;j<queueOps.length;j++) {
        if(queueOps[j][0]==srcQueues[i])
          cnt++;
      }
    }
    
    if(cnt==0)
      return null;
    
    int[][] aqo = new int[cnt][2];
    cnt=0;
    
    for(int i=0;i<srcQueues.length;i++) {
      for(int j=0;j<queueOps.length;j++) {
        if(queueOps[j][0]==srcQueues[i]) {
          aqo[cnt][0]=queueOps[j][0];
          aqo[cnt][1]=queueOps[j][1];
          cnt++;
        }
      }
    }
    
    return aqo;
  }
  
  public boolean doQueueOp(int srcQ, int dstQ, String opNotes,boolean isRush)
  {
    return doQueueOp(ct.getId(),srcQ,dstQ,opNotes,isRush,this.user, this.tmm);
  }
  
  public static boolean doQueueOp(long ctid, int srcQ, int dstQ, String opNotes,boolean isRush, UserBean user, TextMessageManager tmm)
  {
    log.debug("doQueueOp() - START");
    
    boolean bOp = true;
    StringBuffer sb = new StringBuffer(); // for status msg
    
    sb.append(getQueueOpDescriptor(srcQ,dstQ,true));
    
    // do actual queue operation
    try {
      if(srcQ==MesQueues.Q_NONE) {
        QueueTools.insertQueue(ctid,dstQ,user,isRush);
      } else if(dstQ!=MesQueues.Q_NONE) { // validate destination which must be an existant queue
        QueueTools.moveQueueItem(ctid,srcQ,dstQ,user,opNotes,isRush);
      } else
        bOp=false;
    }
    catch(Exception e) {
      log.error("Do Queue Op EXCEPTION: '"+e.getMessage()+"'.");
      bOp=false;
    }
    
    sb.append(bOp? " successful":" FAILED");
    sb.append(" for Call Tag '");
    sb.append(ctid);
    sb.append("'.");
    
    if(tmm!=null)
      tmm.msg(sb.toString());
    
    return bOp;
  }

  // ******** HTML related functions ********
  
  protected final void generateQueueCommands() throws UnsupportedEncodingException {
    // IMPT: clear out existing commands
    clearCommands();
    
    int[][] aqo = getAvailableQueueOps();
    
    if(aqo==null || aqo.length == 0)
      return;
    
    String qo = null, baseDesc, desc;
    StringBuffer sb = new StringBuffer(64);
    int type;
    
    // add commands according to their heretofor imposed "category": move, complete, cancel
    
    for(int i=0;i<aqo.length;i++) {
        
      baseDesc=getQueueOpDescriptor(aqo[i][0],aqo[i][1]);
        
      if(baseDesc.indexOf("Move to ")>=0)
        type=1;
      else if(baseDesc.indexOf("Complete")>=0)
        type=2;
      else if(baseDesc.indexOf("Cancel")>=0)
        type=3;
      else {
        log.warn("Encountered unknown queue operation: '"+baseDesc+"'.  Skipping.");
        continue;
      }

      qo=URLEncoder.encode(String.valueOf(aqo[i][0])+":"+String.valueOf(aqo[i][1]), StandardCharsets.UTF_8.toString());
      //log.debug("generateQueueOpLinks() qo='"+qo+"': "+baseDesc);

      if(type==3) {
        log.debug("found a cancel queue op...");
        String[] cr = getCancellationReasons(aqo[i][0]);
        log.debug("cr.length="+cr.length);
        if(cr.length>0) {
          for(int j=0;j<cr.length;j++) {
            log.debug("cr["+j+"]="+cr[j]);
            sb.delete(0,sb.length()); // reset
            sb.append(baseDesc);
            sb.append(" - ");
            sb.append(cr[j]);
            desc=sb.toString();
            addCommand(SERVLET_URL+"?method=dqo&qo="+qo+"&on="+URLEncoder.encode(cr[j], StandardCharsets.UTF_8.toString()), desc, type);
          }
        // default: single non-descript cancel link
        } else
          addCommand(SERVLET_URL+"?method=dqo&qo="+qo,baseDesc,type);
      // TODO: provide same mechanism for moving into completed queue
      } else
        addCommand(SERVLET_URL+"?method=dqo&qo="+qo,baseDesc,type);
    }
  }
  
  protected String[] getCancellationReasons(int crntQ)
  {
    // universal cancellation reasons
    return new String[]
    {
       "Duplicate Call Tag"
      ,"Incomplete Information"
      ,"Call Tag Cancelled"
    };
  }

  public String getQueueAffiliate()
  {
    return queueBean.getQueueItem().getAffiliate();
  }

  public String getQueueSource()
  {
    return queueBean.getQueueItem().getSource();
  }
    
  public String getQueueLastUser()
  {
    return queueBean.getQueueItem().getLastUser(true);
  }
    
  public String getQueueLockedBy()
  {
    return queueBean.getQueueItem().getLockedBy();
  }
    
  public String getQueueDateCreated()
  {
    return queueBean.getQueueItem().getDateCreated();
  }

  public String getQueueLastChanged()
  {
    return queueBean.getQueueItem().getLastChangedDate();
  }
    
  public boolean getQueueIsRush()
  {
    return queueBean.getQueueItem().getIsRush();
  }
    
  public int getResidentQueue()
  {
    try {
      int q[] = QueueTools.getResidentQueues(ct.getId(),MesQueues.Q_ITEM_TYPE_CALLTAG);
      return q[0];
    }
    catch(Exception e) {
      return MesQueues.Q_NONE;
    }
  }

  public String getResidentQueueDesc(int type)
  {
    return CallTagDetailQueueBean.getQueueDescriptor(type);
  }
    
  public String getResidentQueueDesc()
  {
    return CallTagDetailQueueBean.getQueueDescriptor(getResidentQueue());
  }
    
  public final boolean hasCommands()
  {
    return (commands!=null && commands.size()>0);
  }
  
  public final void addCommand(String command,String desc,int colNum)
  {
    if(command==null || desc==null || command.length()<1 || desc.length()<1 || colNum<1 || colNum>3) {
      log.error("addCommand() - Unable to add command: Invalid parameters.");
      return;
    }
    
    if(commands==null) { // lazy init
      commands = new TreeMap();
      commandCols = new Hashtable();
    }
    
    // remove any previously existing command
    if(commands.containsKey(desc)) {
      commands.remove(desc);
      commandCols.remove(desc);
    }
    
    commands.put(desc,command);
    commandCols.put(desc, colNum);
  }
  
  public final void clearCommand(String desc)
  {
    if(desc==null || desc==null || !commands.containsKey(desc))
      return;
    
    commands.remove(desc);
    commandCols.remove(desc);
  }
  
  public final void clearCommands()
  {
    if(commands!=null)
      commands.clear();
    if(commandCols!=null)
      commandCols.clear();
  }
  
  public boolean hasNextRow()
  {
    return (queueBean.getRows().size()>0 && curRow>-1 && curRow<(queueBean.getRows().size()-1));
  }
    
  public boolean hasPrevRow()
  {
    return (queueBean.getRows().size()>0 && curRow>0);
  }

  public long getCrntRowID()
  {
    return ((curRow>-1 && queueBean.getRows().size()>0)? ((QueueData)(queueBean.getRows()).elementAt(curRow)).getId():-1L);
  }

  public void incrCrntRow()
  {
    curRow++;
  }

  public void decrCrntRow()
  {
    curRow--;
  }

  public boolean hasQueueIterationLinks()
  {
    return (hasNextRow() || hasPrevRow());
  }
    
  public String getQueueLastWorkedByLink() throws UnsupportedEncodingException
  {
    return getUserProfileLink(queueBean.getUserLoginName());
  }

  public final String getUserProfileLink(String loginName) throws UnsupportedEncodingException
  {
    StringBuffer sb = new StringBuffer(128);
    
    sb.append("<a href=\"");
    sb.append(SERVLET_URL);
    sb.append("?method=vsrc");
    sb.append("&ln=");
    sb.append(URLEncoder.encode(loginName, StandardCharsets.UTF_8.toString()));
    sb.append("\">");
    sb.append(loginName);
    sb.append("</a>");
    
    return sb.toString();
  }
  
  public String getCommandLinkHtmlTable(String cssClassName)
  {
    // Modus Operandi: Commands shall fall within one of three columns.
    //                 Each column is sorted lexiconographically.
    
    if(commands==null || commands.size()<1)
      return "";
    
    if(cssClassName==null)
      cssClassName="";
    
    StringBuffer sb = new StringBuffer(512*commands.size());
    Set set = commands.keySet();
    String key,val;
    int numCols=0;
    int colNum=0;
    
    // find the highest column number as this dictates the number of columns
    Collection c = commandCols.values();
    Iterator i=c.iterator();
    int n;
    while(i.hasNext()) {
      n=((Integer)i.next()).intValue();
      if(numCols<n)
        numCols=n;
    }
    if(numCols<1)
      return "";
    int spcrColWidthPcnt = (int)((1/(6*(double)numCols-1))*100);
    int colWidthPcnt = 5*spcrColWidthPcnt;
    
    sb.append("<table border=0 cellpadding=0 cellspacing=0>");
    sb.append("<tr>");
    
    for(int j=1;j<=numCols;j++) {
      
      sb.append("<td valign=top width=");
      sb.append(colWidthPcnt);
      sb.append("%>");
      sb.append("<table border=0 cellpadding=0 cellspacing=2>\n");
      
      for(i=set.iterator();i.hasNext();) {
        key=(String)i.next();
        val=(String)commands.get(key);
        colNum=((Integer)commandCols.get(key)).intValue();
        
        if(colNum!=j)
          continue;
        
        sb.append("<tr><td class=\"");
        sb.append(cssClassName);
        sb.append("\"><a class=\"");
        sb.append(cssClassName);
        sb.append("\" href=\"");
        sb.append(val);
        sb.append("\">");
        sb.append(key);
        sb.append("</a></td></tr>\n");
        
      }
      
      sb.append("</table>");
      sb.append("</td>\n");
      
      // spacer col
      if(j<numCols) {
        sb.append("<td width=");
        sb.append(spcrColWidthPcnt);
        sb.append("%>&nbsp;</td>\n");
      }
    }
    
    sb.append("</tr>");
    sb.append("</table>\n");
    
    return sb.toString();
  }

  public String getQueueIterationLinksHtmlTable(String cssClassName)
  {
    StringBuffer sb = new StringBuffer(1024);
      
    sb.append("<table border=0 cellpadding=0 cellspacing=0>");
    sb.append("<tr>");
      
    // prev link
    if(hasPrevRow()) {
      decrCrntRow();
        
      sb.append("<td");
      if(cssClassName!=null && cssClassName.length()>0) {
        sb.append(" class=\"");
        sb.append(cssClassName);
        sb.append("\"");
      }
      sb.append("><a");
      if(cssClassName!=null && cssClassName.length()>0) {
        sb.append(" class=\"");
        sb.append(cssClassName);
        sb.append("\" ");
      }
      sb.append("href=\"/srv/QueueRouter?type=");
      sb.append(queueBean.getType());
      sb.append("&itemType=");
      sb.append(QUEUE_ITEM_TYPE);
      sb.append("&id=");
      sb.append(getCrntRowID());
      sb.append("\">Previous Queue Item</a></td>");
        
      incrCrntRow();
    }
      
    // spacer
    if(hasNextRow() && hasPrevRow()) {
      sb.append("<td width=5>&nbsp;|&nbsp;</td>");
    }
      
    // next
    if(hasNextRow()) {
      incrCrntRow();
        
      sb.append("<td");
      if(cssClassName!=null && cssClassName.length()>0) {
        sb.append(" class=\"");
        sb.append(cssClassName);
        sb.append("\"");
      }
      sb.append("><a");
      if(cssClassName!=null && cssClassName.length()>0) {
        sb.append(" class=\"");
        sb.append(cssClassName);
        sb.append("\" ");
      }
      sb.append("href=\"/srv/QueueRouter?type=");
      sb.append(queueBean.getType());
      sb.append("&itemType=");
      sb.append(QUEUE_ITEM_TYPE);
      sb.append("&id=");
      sb.append(getCrntRowID());
      sb.append("\">Next Queue Item</a></td>");
    
      decrCrntRow();
    }
      
    sb.append("</tr>");
    sb.append("</table>");
      
    return sb.toString();
  }

  public String getQueueBackLink(String cssClassName)
  {
    StringBuffer sb = new StringBuffer(512);

    int type = getResidentQueue();
    
    sb.append("<a");
    if(cssClassName!=null && cssClassName.length()>0) {
      sb.append(" class=\"");
      sb.append(cssClassName);
      sb.append("\" ");
    }
    sb.append("href=\"/jsp/queues/queue.jsp?type=");
    sb.append(type);
    sb.append("\">");
    sb.append(getResidentQueueDesc(type));
    sb.append(" - Queue Listing</a>");

    return sb.toString();
  }
  
  public boolean isEditable()
  {
    // require resident queue a non-end point queue
    
    if(!isItemInQueue())
      return true;
    
    int rq = getResidentQueue();
    
    return (rq != MesQueues.Q_CALLTAG_CANCELLED && rq != MesQueues.Q_CALLTAG_COMPLETE);
  }
  

}/*@lineinfo:generated-code*/