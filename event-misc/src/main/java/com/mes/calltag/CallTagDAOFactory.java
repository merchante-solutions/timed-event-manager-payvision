package com.mes.calltag;

public final class CallTagDAOFactory
{
  // Singleton
  public static CallTagDAOFactory getInstance()
  {
    if(_instance==null)
      _instance = new CallTagDAOFactory();
    
    return _instance;
  }
  private static CallTagDAOFactory _instance = null;
  ///
  
  // constants
  public static final int       CALLTAGDAO_UNDEFINED = 0;
  public static final int       CALLTAGDAO_MESDB     = 1;
  
  // data members
  private int       daoType     = CALLTAGDAO_MESDB;
  
  // construction
  private CallTagDAOFactory()
  {
  }
  
  public void setDaoType(int daoType)
  {
    this.daoType = daoType;
  }
  
  // creational methods
  
  public CallTagDAO    getCallTagDAO()
  {
    switch(daoType) {
      case CALLTAGDAO_MESDB:
        return new MesCallTagDAO();
      default:
        return null;
    }
  }
  

} // class DAOFactory
