package com.mes.calltag;

import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.constants.mesConstants;
import com.mes.tools.AccountKey;
import com.mes.tools.EquipmentActionKey;

public class CallTag extends CallTagBase
{
  // create class log category
  static Category log = Category.getInstance(CallTag.class.getName());

  // constants

  // data members
  protected AccountKey            billMerchant            = null;
  protected Date                  billDate                = null;
  protected double                billAmount              = 0d;
  protected boolean               billCharged             = false;
  protected Vector                items                   = null;
  protected Vector                notifications           = null;
  protected EquipmentActionKey    parentKey               = null;
  protected Object                parent                  = null;
  protected String                notes                   = "";


  /**
   * CallTagComparator class
   */
  private class CallTagComparator
    implements Comparator
  {
    public int compare(Object o1, Object o2)
    {
      // compare by date created
      return ((CallTag)o1).dateCreated.compareTo(((CallTag)o2).dateCreated);
    }
  } // class CallTagComparator


  // class methods


  // object methods

  // construction
  public CallTag()
  {
    super();

    items = new Vector(0,1);
    notifications = new Vector(0,1);

    notes = "";
  }
  
  public void clear()
  {
    billMerchant = null;
    
    if(billDate!=null)
      billDate.setTime(0L);
    
    billAmount = 0d;
    billCharged = false;

    if(items != null)
      items.removeAllElements();

    if(notifications != null)
      notifications.removeAllElements();

    parent = null;
    parentKey = null;
    
    notes = "";

    super.clear();
  }

  public boolean equals(Object o)
  {
    if(o instanceof CallTag)
      return (id == ((CallTag)o).id);

    return false;
  }


  // accessors
  public AccountKey           getBillMerchant()         { return billMerchant; }
  public Date                 getBillDate()             { return billDate; }
  public double               getBillAmount()           { return billAmount; }
  public boolean              isBillCharged()           { return billCharged; }
  public String               getNotes()                { return notes; }

  public EquipmentActionKey   getParentKey()            { return parentKey; }
  public int                  getParentType()
  {
    return parentKey==null?
           mesConstants.ACTION_CODE_UNDEFINED:parentKey.getActionCode();
  }
  public String               getParentTypeDesc()
  {
    return parentKey==null? "":parentKey.getActionDesc();
  }

  public long                 getParentRefNum()
  {
    return parentKey==null? 0L:parentKey.getId();
  }

  public String               getParentPartNum()
  {
    return parentKey==null? "":parentKey.getPartNum();
  }

  public String               getParentSerialNum()
  {
    return parentKey==null? "":parentKey.getSerialNum();
  }

  public Object               getParent()
  {
    return parent;
  }

  public long         getMerchNumber()
  {
    try {
      return Long.parseLong(billMerchant.getMerchNum());
    }
    catch(Exception e) {
      return 0L;
    }
  }

  public int          getNumItems()
  {
    return items.size();
  }
  public int          getNumNotifications()
  {
    return notifications.size();
  }
  public Enumeration  getItemsEnumerator()
  {
    return items.elements();
  }
  public Enumeration  getNotificationsEnumerator()
  {
    return notifications.elements();
  }

  public Vector  getItems()
  {
    return items;
  }
  public Vector  getNotifications()
  {
    return notifications;
  }


  // mutators
  public void setBillMerchant(AccountKey v)
  {
    if(v==null || v.equals(billMerchant))
      return;

    billMerchant=v;
    dirty();
  }

  public void setMerchNumber(String merchNumber)
  {
    try {
      if(merchNumber==null || merchNumber.equals(billMerchant.getMerchNum()))
        return;
    }
    catch(Exception e) {
    }

    if(billMerchant == null)
      billMerchant = new AccountKey(merchNumber,true);
    else
      billMerchant.setMerchNum(merchNumber);
    dirty();
  }

  public void setBillDate(Date v)
  {
    if(v==null || v.equals(billDate))
      return;

    billDate=v;
    dirty();
  }

  public void setBillAmount(double v)
  {
    if(v==billAmount)
      return;
    billAmount=v;
    dirty();
  }

  public void setBillCharged(boolean v)
  {
    if(v==billCharged)
      return;
    billCharged=v;
    dirty();
  }

  public void setNotes(String v)
  {
    if(v==null || v.length()<1)
      return;

    notes=v;
    dirty();
  }

  public void clearParent()
  {
    parentKey = null;
    parent    = null;
  }

  public void setParentKey(EquipmentActionKey v)
  {
    if(v==null || v.equals(this.parentKey))
      return;

    parentKey = v;
    dirty();
  }

  public void setParent(Object v)
  {
    this.parent=v;
  }

  public void addItem(CallTagItem v)
  {
    if(v==null)
      return;

    if(items.contains(v))
      items.removeElement(v);

    items.addElement(v);
    v.setCallTagId(this.id);
    v.dirty();
  }
  
  /**
   * Looks for a tag item with the given id, returns it if found, else null.
   */
  public CallTagItem getItem(long itemId)
  {
    for (Iterator i = items.iterator(); i.hasNext();)
    {
      CallTagItem item = (CallTagItem)i.next();
      if (item.getId() == itemId)
      {
        return item;
      }
    }
    return null;
  }

  public void removeItem(CallTagItem v)
  {
    if(v==null || !items.contains(v))
      return;

    items.removeElement(v);

    v.setCallTagId(0L);
  }

  public void addNotification(CallTagNotification v)
  {
    if(v==null)
      return;

    if(notifications.contains(v))
      notifications.removeElement(v);

    notifications.addElement(v);
    v.setCallTagId(this.id);
    v.dirty();
  }

  public void removeNotification(CallTagNotification v)
  {
    if(v==null || !notifications.contains(v))
      return;

    notifications.removeElement(v);

    v.setCallTagId(0L);
  }

} // class CallTag
