package com.mes.calltag;

import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.support.DateTimeFormatter;

/**
 * Manages creation and retrieval for viewing.
 */
public class Note
{
  static Logger log = Logger.getLogger(Note.class);
  
  public Note()
  {
  }
  
  private static String formatStr = "MM/dd/yy hh:mm:ss a";
  
  private long    id          = -1L;
  private long    calltagId   = -1L;
  private Date    createDate  = null;
  private String  author      = null;
  private String  text        = null;
  
  public void setId(long id)                  { this.id   = id; }
  public void setCalltagId(long calltagId)    { this.calltagId = calltagId; }
  public void setCreateDate(Date createDate)  { this.createDate = createDate; }
  public void setAuthor(String author)        { this.author = author; }
  public void setText(String text)            { this.text = text; }
  
  public long   getId()         { return id; }
  public long   getCalltagId()  { return calltagId; }
  public Date   getCreateDate() { return createDate; }
  public String getAuthor()     { return author; }
  public String getText()       { return text; }
  
  
  public String renderCreateDate()
  {
    if (createDate != null)
    {
      return DateTimeFormatter.getFormattedDate(createDate,formatStr);
    }
    return "no date";
  }
  
  public String toString()
  {
    return "Note " + id + " for calltag " + calltagId + " created " 
      + createDate + " by " + author + ": " + text;
  }
}
