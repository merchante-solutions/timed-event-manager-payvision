package com.mes.calltag;

import com.mes.tools.AccountKey;
import com.mes.user.UserBean;


/**
 * CallTagDAO interface
 * 
 * Defines data access routines for Call Tag related classes
 */
public interface CallTagDAO
{
  // constants
  // (none)
  
  // methods
  
  // call tag
  public CallTag                  createCallTag();
  public void                     deleteCallTag(CallTag ct);
  public CallTag                  findCallTag(long pkid);
  public boolean                  persistCallTag(CallTag ct);
  
  // call tag item
  public CallTagItem              createCallTagItem();
  public void                     deleteCallTagItem(CallTagItem cti);
  public CallTagItem              findCallTagItem(long pkid);
  public boolean                  persistCallTagItem(CallTagItem cti);
  
  // call tag notification
  public CallTagNotification      createCallTagNotification();
  public void                     deleteCallTagNotification(CallTagNotification ctn);
  public CallTagNotification      findCallTagNotification(long pkid);
  public boolean                  persistCallTagNotification(CallTagNotification ctn);
  
  
  /**
   * MerchantInfo
   * 
   * Read-only merchant info value object.
   */
  public final class MerchantInfo
  {
    // data members
    private   AccountKey  key                 = null;
    private   String      dbaName             = "";
    private   String      assocNumber         = "";
    private   String      bankNumber          = "";
    private   int         appTypeCode         = 0;
    private   int         merchantTypeCode    = 0;      // i.e. sales channel
    private   String      appTypeDesc         = null;
    private   String      merchantTypeDesc    = null;
    
    // accessors
    public  AccountKey  getAccountKey()        { return key; }
    public  String      getDbaName()           { return dbaName; }
    public  String      getAssocNumber()       { return assocNumber; }
    public  String      getBankNumber()        { return bankNumber; }
    public  int         getAppTypeCode()       { return appTypeCode; }
    public  int         getMerchantTypeCode()  { return merchantTypeCode; }
    public  String      getAppTypeDesc()       { return appTypeDesc; }
    public  String      getMerchantTypeDesc()  { return merchantTypeDesc; }
    
    public String getMerchantNumber()
    { 
      try {
        return key.getMerchNum();
      }
      catch(Exception e) {
        return "";
      }
    }
    public String getControlNumber()
    { 
      try {
        return key.getMercCntrlNumber();
      }
      catch(Exception e) {
        return "";
      }
    }
    public long getAppSeqNum()
    { 
      try {
        return key.getAppSeqNum();
      }
      catch(Exception e) {
        return 0L;
      }
    }
    
    // mutators
    public void setAccountKey(AccountKey v)
    {
      this.key = v;
    }
    public void setDbaName(String v)
    {
      dbaName = v;
    }
    public void setAssocNumber(String v)
    {
      assocNumber = v;
    }
    public void setBankNumber(String v)
    { 
      bankNumber = v;
    }
    public void setAppTypeCode(int appTypeCode)
    {
      this.appTypeCode = appTypeCode;
    }
    public void setAppTypeDesc(String v)
    {
      this.appTypeDesc = v;
    }
    public void setMerchantTypeCode(int v)
    {
      this.merchantTypeCode = v;
    }
    public void setMerchantTypeDesc(String v)
    {
      this.merchantTypeDesc = v;
    }
  
  } // MerchantInfo class
  
  
  public MerchantInfo             getMerchantInfo(AccountKey key, UserBean user);
  
  
  // helpers
  
  // NOTE: String[][]: {name}{value} order
  
  public String[][]               getEquipTypes();
  public String[][]               getEquipManufacturers();
  public String[][]               getAppTypes();
  public String[][]               getEquipClasses();
  public String[][]               getParentTypes();
  //public String[][]               getCallTagStatuses();
  public String[][]               getCallTagItemStatuses();
  public String[][]               getEquipLendTypes();
  public String[][]               getEquipModels();
  //public String[][]               getMesInvEquipPartNums();
  public String[][]               getEquipInvStatuses();
  
  public long                     getCallTagIdFromAcrId(long acrid);
  public int                      getAppType(String merchNum);
  
} // interface CallTagDAO
