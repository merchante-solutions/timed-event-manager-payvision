package com.mes.calltag;

import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.Validation;
import com.mes.tools.EquipmentKey;

/**
 * This is a helper bean for the call tag module that gathers user input
 * (in this case just a serial number) in order to create a new call tag
 * item to be deployed ("outgoing").  These are not really call tag items
 *  per se...
 */
public class AddOutgoingBean extends FieldBean
{
  static Logger log = Logger.getLogger(AddOutgoingBean.class);

  public static final String FN_SERIAL_NUM  = "serialNum";

  public static final String FN_METHOD      = "method";

  public static final String FN_SUBMIT      = "submitBtn";
  public static final String FN_CANCEL      = "cancelBtn";
  
  private CallTag callTag;  

  public AddOutgoingBean(CallTag callTag)
  {
    this.callTag = callTag;
  }

  /**
   * Checks for duplicate serial num in this call tag.
   */
  public class DuplicateSerialNumValidation implements Validation
  {
    public boolean validate(String serialNum)
    {
      // look for the serial num already present in call tag items
      for (Iterator i = callTag.getItems().iterator(); i.hasNext();)
      {
        CallTagItem item = (CallTagItem)i.next();
        if (item.getSerialNum().equals(serialNum))
        {
          log.debug("duplicate s/n found for " + serialNum);
          return false;
        }
      }
      return true;
    }
    
    public String getErrorText()
    {
      return "Call tag item with serial number already exists";
    }
  }
  
  /**
   * Checks to make sure serial num exists in inventory.
   */
  public class ValidSerialNumValidation implements Validation
  {
    public boolean validate(String serialNum)
    {
      // if serial num is not present don't invalidate it here because of that
      if (serialNum == null || serialNum.equals(""))
      {
        // let the field required validation deal with this...
        return true;
      }

      // HACK: use an equipment key to determine if s/n is valid
      //       key will return true from isSet() if part num/equip model
      //       has not been set (key is instantiated with model set to null)
      //       but it can load it with the s/n
      EquipmentKey key = new EquipmentKey(null,serialNum);
      return key.isSet();
    }
    
    public String getErrorText()
    {
      return "Not found in inventory";
    }
  }
  
  public void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    fields.add(new HiddenField(FN_METHOD));
    
    Field serialNum = new Field(FN_SERIAL_NUM,"Serial Number",25,25,false);
    fields.add(serialNum);
    serialNum.addValidation(new DuplicateSerialNumValidation());
    serialNum.addValidation(new ValidSerialNumValidation());
    
    fields.add(new ButtonField(FN_SUBMIT,"Submit"));
    fields.add(new ButtonField(FN_CANCEL,"Cancel"));
 
    fields.setHtmlExtra("class=\"formText\"");
    fields.setFixImage("/images/arrow1_left.gif",10,10);
  }
  
  /**
   * Generate an equipment key suitable for creating a new call tag item.
   */
  public EquipmentKey generateKey()
  {
    return new EquipmentKey(null,fields.getData(FN_SERIAL_NUM));
  }
}