package com.mes.calltag;

import java.util.Iterator;
import java.util.List;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.equipment.EquipmentDataBase;
import com.mes.tools.Key;
import com.mes.tools.TableView;
import com.mes.tools.TableViewHelper;


public final class CallTagNotificationsTable
  implements TableView
{
  // create class log category
  static Category log = Category.getInstance(CallTagNotificationsTable.class.getName());

  // constants
  private static final String[][] columnData = 
    {
       {"Id",                       "getId"}
      ,{"Package Tracking Number",  "getPkgTrkNum"}
      ,{"Issue Date",               "getIssueDate"}
      ,{"Letter Name",              "getLetterName"}
    };
  
  // data members
  protected List              rows                = null;
  protected TableViewHelper   helper              = null;
  protected String            detailPageBaseUrl   = null;
  protected String            queryStringSuffix   = null;
  
  // construction
  public CallTagNotificationsTable(List rows, String detailPageBaseUrl, String queryStringSuffix)
  {
    this.rows = rows;
    this.helper = new TableViewHelper(CallTagNotification.class, this, columnData);
    this.helper.setDateFormat("MM/dd/yyyy");
    this.detailPageBaseUrl = detailPageBaseUrl;
    this.queryStringSuffix = queryStringSuffix;
  }
  
  public List           getRows()
  {
    return rows;
  }
  
  public void           setRows(List rows)
  {
    this.rows = rows;
  }
  
  public Key            getRowKey(int rowOrdinal)
  {
    EquipmentDataBase edb = (EquipmentDataBase)helper.getRowObject(rowOrdinal);
    return (Key)edb.getKey();
  }

  public Object         getRowObject(int rowOrdinal)
  {
    return helper.getRowObject(rowOrdinal);
  }

  public int            getNumRows()
  {
    return rows.size();
  }
  
  public int            getNumCols()
  {
    return columnData.length;
  }
  
  public String         getLinkToDetailColumnName()
  {
    return "Package Tracking Number";
  }
  
  public String         getDetailPageBaseUrl()
  {
    return detailPageBaseUrl;
  }
    
  public String         getQueryStringSuffix()
  {
    return queryStringSuffix;
  }
  
  public void           setQueryStringSuffix(String v)
  {
    queryStringSuffix=v;
  }
  
  public String getDetailPageQueryString(int rowOrdinal)
  {
    CallTagBase ctb = (CallTagBase)getRowObject(rowOrdinal);
    
    StringBuffer sb = new StringBuffer();
    sb.append("method=vectn&id=");
    sb.append(ctb.getId());
    
    return sb.toString();
  }

  public Iterator       getRowIterator()
  {
    return rows.iterator();
  }
  
  public String         getColumnName(int colOrdinal)
  {
    return helper.getColumnName(colOrdinal);
  }
  
  public String[]       getColumnNames()
  {
    return helper.getColumnNames();
  }
  
  public String         getCell(int rowOrdinal, int colOrdinal)
  {
    return helper.getCell(rowOrdinal, colOrdinal);
  }
  
  public String         getHtmlCell(int rowOrdinal, int colOrdinal)
  {
    return helper.getHtmlCell(rowOrdinal, colOrdinal);
  }

}
