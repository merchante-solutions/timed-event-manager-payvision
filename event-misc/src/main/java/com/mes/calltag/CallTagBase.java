package com.mes.calltag;

import java.util.Comparator;
import java.util.Date;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.database.ValueObjectBase;

public abstract class CallTagBase extends ValueObjectBase
{
  // create class log category
  static Category log = Category.getInstance(CallTagBase.class.getName());

  
  // constants
  // (NONE)  
  
  
  // data members
  protected long                  id                      = 0L;
  protected Date                  dateCreated             = null;
  protected Date                  lastModified            = null;
  protected String                lastStatusUserid        = "";
  //protected String                notes                   = null;
  
  
  /**
   * DateCreatedComparator class
   */
  private class DateCreatedComparator
    implements Comparator
  {
    public int compare(Object o1, Object o2)
    {
      // compare by date created
      return ((CallTagBase)o1).dateCreated.compareTo(((CallTagBase)o2).dateCreated);
    }
  } // class DateCreatedComparator

  
  // class methods
  // (NONE)
  
  
  // object methods
  
  // construction
  public CallTagBase()
  {
    super();
  }
  
  public void clear()
  {
    id = -1L;
    if(dateCreated!=null)
      dateCreated.setTime(0L);
    if(lastModified!=null)
      lastModified.setTime(0L);
    lastStatusUserid = "";
    //notes = "";
    
    clean();
  }
  
  public boolean equals(Object o)
  {
    if(o instanceof CallTagBase)
      return (id == ((CallTagBase)o).id);

    return false;
  }

  
  // accessors
  public long                 getId()                   { return id; }
  public Date                 getDateCreated()          { return dateCreated; }
  public Date                 getLastModified()         { return lastModified; }
  public String               getLastStatusUserid()     { return lastStatusUserid; }
  //public String               getNotes()                { return notes; }
  
  
  // mutators
  public void setId(long v)
  {
    if(v==id)
      return;
    id=v;
    is_dirty=true;
  }
  
  public void setDateCreated(Date v)
  {
    if(v==null || v.equals(dateCreated))
      return;
    dateCreated=v;
    is_dirty=true;
  }

  public void setLastModified(Date v)
  {
    if(v==null || v.equals(lastModified))
      return;
    lastModified=v;
    is_dirty=true;
  }

  public void setLastStatusUserid(String v)
  {
    if(v==null || v.equals(lastStatusUserid))
      return;
    lastStatusUserid=v;
    is_dirty=true;
  }
  
  /*
  public void setNotes(String v)
  {
    if(v==null || v.equals(notes))
      return;
    
    notes=v;
  }
  */
  
} // class CallTagBase
