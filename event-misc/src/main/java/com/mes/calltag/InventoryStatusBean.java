package com.mes.calltag;

import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.equipment.EquipmentDataBuilder;
import com.mes.equipment.EquipmentData_EquipInv;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;
import com.mes.tools.EquipmentKey;

/**
 * Call tag helper bean.  Allows call tag item's inventory status to be
 * modified.
 */
public class InventoryStatusBean extends FieldBean
{
  static Logger log = Logger.getLogger(InventoryStatusBean.class);
  
  public static final String FN_OWNER       = "inventoryOwner";
  public static final String FN_STATUS      = "equipStatus";
  public static final String FN_CONDITION   = "equipCondition";
  public static final String FN_COST        = "unitCost";
  public static final String FN_ASSIGNED    = "assignToMerch";
  public static final String FN_MERCH_NUM   = "merchNum";
  
  public static final String FN_METHOD      = "method";
  public static final String FN_ITEM_ID     = "itemId";

  public static final String FN_SUBMIT      = "submitBtn";
  public static final String FN_CANCEL      = "cancelBtn";
  
  private CallTagItem tagItem;  
  private CallTagDAO  calltagDb;
  private StatusTable statusTable;

  public InventoryStatusBean(CallTagItem tagItem)
  {
    this.tagItem = tagItem;
    calltagDb = CallTagDAOFactory.getInstance().getCallTagDAO();
  }
  
  public class ArrayLoaderTable extends DropDownTable
  {
    public void loadArray(String[][] tableData)
    {
      if (tableData == null)
      {
        log.error("null table data in " + this.getClass().getName());
        return;
      }
      
      addElement("","-- select --");
      for (int i = 0; i < tableData.length; i++)
      {
        addElement(tableData[i][1],tableData[i][0]);
      }
    }
  }

  public class OwnerTable extends ArrayLoaderTable
  {
    public OwnerTable()
    {
      loadArray(calltagDb.getAppTypes());
    }
  }
  
  public class StatusTable extends ArrayLoaderTable
  {
    private int[][] lendTypes;
    
    public StatusTable()
    {
      String[][] tableData = calltagDb.getEquipInvStatuses();
      loadArray(tableData);
      lendTypes = new int[tableData.length][2];
      for (int i = 0; i < tableData.length; i++)
      {
        try
        {
          lendTypes[i][0] = Integer.parseInt(tableData[i][1]);
          lendTypes[i][1] = Integer.parseInt(tableData[i][2]);
        }
        catch (Exception e)
        {
          log.error("Unable to parse tableData element " + i
            + " ( " + tableData[i][1] + ", " + tableData[i][2] + " )");
        }
      }
    }
    
    public int getAssociatedLendType(int statusId)
    {
      for (int i = 0; i < lendTypes.length; ++i)
      {
        if (statusId == lendTypes[i][0])
        {
          return lendTypes[i][1];
        }
      }
      log.error("Status id " + statusId + " not found in lend types array");
      return -1;
    }
  }

  public class ConditionTable extends ArrayLoaderTable
  {
    public ConditionTable()
    {
      loadArray(calltagDb.getEquipClasses());
    }
  }
  
  public class MerchNumValidation implements Validation
  {
    private CheckField assigned;
    
    public MerchNumValidation(CheckField assigned)
    {
      this.assigned = assigned;
    }
    
    public String getErrorText()
    {
      return "Field required";
    }
    
    public boolean validate(String fieldData)
    {
      if (assigned.isChecked())
      {
        return !(fieldData == null || fieldData.length() == 0);
      }
      return true;
    }
  }
  
  /**
   * Create fields for inventory status screen.
   */
  public void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    fields.add(new DropDownField(FN_OWNER,"Inventory Owner",
      new OwnerTable(),false));
    fields.add(new DropDownField(FN_CONDITION,"Equipment Condition",
      new ConditionTable(),false));
    fields.add(new CurrencyField(FN_COST,"Unit Cost",6,6,false));

    statusTable = new StatusTable();
    fields.add(new DropDownField(FN_STATUS,"Equipment Status",
      statusTable,false));
    
    CheckField assigned 
      = new CheckField(FN_ASSIGNED,"Assign to Merchant Number","y",false);
    fields.add(assigned);
    Field merchNum = new Field(FN_MERCH_NUM,"Merchant Number",25,15,true);
    fields.add(merchNum);
    merchNum.addValidation(new MerchNumValidation(assigned));
    
    // convenience storage so that controller command and item id
    // can be maintained via fields and jsp submission...
    fields.add(new HiddenField(FN_METHOD));
    fields.add(new HiddenField(FN_ITEM_ID));

    fields.add(new ButtonField(FN_SUBMIT,"Submit"));
    fields.add(new ButtonField(FN_CANCEL,"Cancel"));
    
    fields.setHtmlExtra("class=\"formText\"");
    fields.setFixImage("/images/arrow1_left.gif",10,10);
  }
  
  /**
   * Load data from call tag equipment data into fields.
   */
  public boolean autoLoad()
  {
    //setData(FN_MODEL,tagItem.getEquipModel());
    
    EquipmentData_EquipInv equipData
      = (EquipmentData_EquipInv)tagItem.getEquipmentData();
    if (equipData != null)
    {
      setData(FN_OWNER,     Integer.toString(equipData.getOwner()));
      setData(FN_STATUS,    Integer.toString(equipData.getStatusCode()));
      setData(FN_CONDITION, Integer.toString(equipData.getClassCode()));
      setData(FN_COST,      Double.toString(equipData.getUnitCost()));
      
      String merchNum = equipData.getMerchantNumber();
      if (merchNum != null && merchNum.length() > 0)
      {
        setData(FN_MERCH_NUM,merchNum);
        setData(FN_ASSIGNED,"y");
      }
      else
      {
        setData(FN_ASSIGNED,"n");
      }
    }
    
    return true;
  }
  
  /**
   * Determines if an equipment status code is a deployed status.
   */
  private boolean isDeployedStatus(int statusCode)
  {
    return  statusCode == 1  || statusCode == 2  || statusCode == 5  || 
            statusCode == 7  || statusCode == 20 || statusCode == 26 || 
            statusCode == 27 || statusCode == 28;
  }


  /**
   * Pull data from fields into call tag item equipment data storage.
   */
  public boolean autoSubmit()
  {
    // set equip model directly in tag item
    //tagItem.setEquipModel(getData(FN_MODEL));
      
    // look for existing equip data
    EquipmentData_EquipInv equipData
      = (EquipmentData_EquipInv)tagItem.getEquipmentData();
      
    log.debug("equipData does " + (equipData == null ? "NOT" : "") + " exist");
    
    // create new equip data in tag item if none found
    if (equipData == null)
    {
      try
      {
        // use the tag item's equipment key to set a new equip data in item
        EquipmentKey equipKey = tagItem.getEquipmentKey();
        log.debug("retrieved equip key from calltag item: " + equipKey);
        
        EquipmentDataBuilder builder = EquipmentDataBuilder.getInstance();
        equipData = (EquipmentData_EquipInv)builder.build(equipKey);
        equipData.setEquipmentKey(equipKey);
        log.debug("built new equip data with key: " + equipData.getKey());
          
        tagItem.setEquipmentData(equipData);
        log.debug("new equip data created in calltag item");
      }
      catch (Exception e)
      {
        log.error("Unable to generate new equip data: " + e);
        e.printStackTrace();
        return false;
      }
    }
    
    // store the equipment data
    equipData.setOwner(getField(FN_OWNER).asInteger());
    equipData.setClassCode(getField(FN_CONDITION).asInteger());
    equipData.setUnitCost(getField(FN_COST).asDouble());
    
    // look at the old vs. new status code
    // if the status code is changing there are a
    // few fields that may need to be adjusted
    int oldStatusCode = equipData.getStatusCode();
    int newStatusCode = getField(FN_STATUS).asInteger();
    if (oldStatusCode != newStatusCode)
    {
      // store the new status code
      equipData.setStatusCode(newStatusCode);
      
      // determine lend type that is associated with the status code
      int lendType = statusTable.getAssociatedLendType(newStatusCode);
      equipData.setEquipLendTypeCode(lendType);
      log.debug("associated lend type for new status " + newStatusCode 
        + " is " + lendType);
        
      // if will not be deploying null out the deployed date
      if (!isDeployedStatus(newStatusCode))
      {
        equipData.setDeployedDate(null);
      }
      // if will be deploying and was not previously 
      // deployed set to current date
      else if (!isDeployedStatus(oldStatusCode))
      {
        equipData.setDeployedDate(Calendar.getInstance().getTime());
      }
    }    

    // see if a merch num is assigned to the equipment
    CheckField assigned = (CheckField)getField(FN_ASSIGNED);
    equipData.setMerchantNumber(
      (assigned.isChecked() ? getData(FN_MERCH_NUM) : ""));

    log.debug("equip data has been saved in calltag item");

    return true;
  }

  /**
   * Expose serial number of calltag item.
   */
  public String getSerialNum()
  {
    return tagItem.getSerialNum();
  }
}