package com.mes.calltag;

public class CallTagConstants
{
  /**
   * Command string constants defining commands supported by the call tag
   * controller com.mes.utils.CallTagServlet, referenced by call tag package
   * objects and jsp's.
   */
  public static final String CMD_ADD_TAG_ITEM           = "addTagItem";
  public static final String CMD_EDIT_TAG_ITEM          = "editTagItem";
  public static final String CMD_DEL_TAG_ITEM           = "delTagItem";
  public static final String CMD_EDIT_INV_STATUS        = "editInvStatus";
  public static final String CMD_ADD_NOTE               = "addNote";
  
  public static final String CMD_DEL_NOTIFICATION       = "dctn";
  public static final String CMD_EDIT_NOTIFICATION      = "vectn";
  
  /**
   * Servlet and jsp paths used by call tags.
   */
  public static final String PATH_SERVLET_URL           = "/srv/calltag";
  
  public static final String PATH_CONFIRMATION          = "/jsp/calltag/confirm_action.jsp";
  public static final String PATH_ADD_TAG_ITEM          = "/jsp/calltag/edit_item.jsp";
  public static final String PATH_EDIT_TAG_ITEM         = "/jsp/calltag/edit_item.jsp";
  public static final String PATH_INVENTORY_STATUS      = "/jsp/calltag/edit_inventory_status.jsp";


  public static final String PATH_PDF_SERVLET_URL       = "/srv/PdfStatementServlet";
}