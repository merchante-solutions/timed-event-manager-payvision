package com.mes.calltag;

import com.mes.forms.FieldBean;
import com.mes.support.TextMessageManager;
import com.mes.user.UserBean;


public final class CallTagSession
{
  private CallTagDetailQueueBean ctBean = null;
  private FieldBean helperBean = null;
  private CallTagItem tagItem = null;
  
  public CallTagSession()
  {
    ctBean = new CallTagDetailQueueBean();
  }
  
  public CallTagDetailQueueBean getCallTagDetailQueueBean()
  {
    return ctBean;
  }
  
  public void setTextMessageManager(TextMessageManager tmm)
  {
    ctBean.setTextMessageManager(tmm);
  }
  
  public void setUser(UserBean loginUser)
  {
    ctBean.setUser(loginUser);
  }
  
  public FieldBean setHelperBean(FieldBean helperBean)
  {
    this.helperBean = helperBean;
    return helperBean;
  }
  
  public FieldBean getHelperBean()
  {
    return helperBean;
  }
  
  public CallTagItem setTagItem(CallTagItem tagItem)
  {
    this.tagItem = tagItem;
    return tagItem;
  }
  
  public CallTagItem getTagItem()
  {
    return tagItem;
  }
}
