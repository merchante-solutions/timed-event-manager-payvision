package com.mes.calltag;

import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.equipment.EquipmentDAO;
import com.mes.equipment.EquipmentDAOFactory;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;
import com.mes.tools.EquipmentKey;

/**
 * This is a helper bean for the call tag module that gathers user input
 * in order to replace an unknown incoming item with a known incoming item.
 */
public class EditIncomingBean extends FieldBean
{
  static Logger log = Logger.getLogger(EditIncomingBean.class);

  public static final String FN_SERIAL_NUM  = "serialNum";
  public static final String FN_MODEL       = "equipModel";

  public static final String FN_METHOD      = "method";
  public static final String FN_ITEM_ID     = "itemId";

  public static final String FN_SUBMIT      = "submitBtn";
  public static final String FN_CANCEL      = "cancelBtn";
  
  private CallTag calltag;  
  private CallTagItem tagItem;

  public EditIncomingBean(CallTag calltag, CallTagItem tagItem)
  {
    this.calltag = calltag;
    this.tagItem = tagItem;
  }

  /**
   * A drop down table containing equip model options for use in the
   * equip model drop down field
   */
  public class EquipModelTable extends DropDownTable
  {
    public EquipModelTable()
    {
      addElement("","-- load from serial number --");
      EquipmentDAO equipDao 
        = EquipmentDAOFactory.getInstance().getEquipmentDAO();
      List models = equipDao.getSimpleEquipmentList();
      
      for (Iterator i = models.iterator(); i.hasNext();)
      {
        String desc = (String)i.next();
        String code = desc;
        int idx = code.indexOf("-");
        if (idx > 0)
        {
          code = code.substring(0,idx).trim();
        }
        addElement(code,desc);
      }
    }
  }
    
  /**
   * Checks for duplicate serial num in this call tag.
   */
  public class SerialNumValidation implements Validation
  {
    public boolean validate(String serialNum)
    {
      // look for the serial num already present in call tag items
      for (Iterator i = calltag.getItems().iterator(); i.hasNext();)
      {
        CallTagItem item = (CallTagItem)i.next();
        // look for matching serial num (ignoring the current
        // tag item's serial num)
        if (item.getSerialNum().equals(serialNum) && 
            !tagItem.getSerialNum().equals(serialNum))
        {
          log.debug("duplicate s/n found for " + serialNum);
          return false;
        }
      }
      return true;
    }
    
    public String getErrorText()
    {
      return "Call tag item with serial number already exists";
    }
  }
  
  /**
   * Determines if equip model is valid.
   */
  public class EquipModelValidation implements Validation
  {
    private Field serialNumField;
    private String errorText;
    
    public EquipModelValidation(Field serialNumField)
    {
      this.serialNumField = serialNumField;
    }
    
    public boolean validate(String equipModel)
    {
      // only do validation logic if serial num is present
      if (!serialNumField.isBlank())
      {
        String serialNum = serialNumField.getData();
        
        // HACK: use an equipment key to determine if 
        //       serial number/equip model combo is valid
        //
        //       VALID:   unknown s/n + any model
        //                known s/n + no model given
        //                known s/n + matching model
        //
        //       INVALID: unknown s/n + no model given
        //                known s/n + non-matching model
        //
        EquipmentKey key = new EquipmentKey(null,serialNum);
        
        // determine if s/n is known
        boolean isKnown = key.isSet();
        
        // determine if model is provided
        boolean modelGiven = equipModel != null && equipModel.length() > 0;
        
        // determine if model given does not match inventory model
        boolean nonMatching = modelGiven && isKnown && 
                              !key.getPartNum().equals(equipModel);
                              
        // invalid case 1
        if (!isKnown && !modelGiven)
        {
          errorText = "Serial number is unknown, equipment model must be"
            + " specified";
          return false;
        }

        // invalid case 2        
        if (isKnown && nonMatching)
        {
          errorText = "Mismatch for given serial number, select"
            + " '-- load from serial number --' or change serial number";
          return false;
        }
      }
      return true;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }
    
  public void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    fields.add(new HiddenField(FN_METHOD));
    fields.add(new HiddenField(FN_ITEM_ID));
    
    Field serialNum = new Field(FN_SERIAL_NUM,"Serial Number",25,25,false);
    fields.add(serialNum);
    serialNum.addValidation(new SerialNumValidation());
    
    Field equipModel = new DropDownField(FN_MODEL,"Equipment Model",
      new EquipModelTable(),true);
    fields.add(equipModel);
    equipModel.addValidation(new EquipModelValidation(serialNum));
    
    fields.add(new ButtonField(FN_SUBMIT,"Submit"));
    fields.add(new ButtonField(FN_CANCEL,"Cancel"));
    
    fields.setHtmlExtra("class=\"formText\"");
    fields.setFixImage("/images/arrow1_left.gif",10,10);
  }
  
  /**
   * Load data from calltag item data into fields.
   */
  public boolean autoLoad()
  {
    String equipModel = tagItem.getEquipModel();
    if (equipModel != null)
    {
      setData(FN_MODEL,equipModel);
    }
    
    String serialNum = tagItem.getSerialNum();
    if (serialNum != null)
    {
      setData(FN_SERIAL_NUM,serialNum);
    }
    
    return true;
  }
  
  /**
   * Generate an equipment key suitable for creating a new call tag item.
   */
  public EquipmentKey generateKey()
  {
    return new EquipmentKey(getData(FN_MODEL),getData(FN_SERIAL_NUM));
  }
}