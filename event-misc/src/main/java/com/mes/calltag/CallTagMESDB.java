/*@lineinfo:filename=CallTagMESDB*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.calltag;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.calltag.CallTagDAO.MerchantInfo;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.tools.EquipmentActionKey;
import com.mes.tools.EquipmentKey;
import sqlj.runtime.ResultSetIterator;


class CallTagMESDB extends SQLJConnectionBase
{
  // create class log category
  static Category log = Category.getInstance(CallTagMESDB.class.getName());
  

  // constants
  // (NONE)
  

  // construction
  public CallTagMESDB()
  {
    super(false);
  }
  
  // class methods
  // (none)
  
  // object methods

  public void fillMerchantInfo(MerchantInfo mi)
  {
    boolean wasStale = false;
    
    try {
    
      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      int atc = 0,mtc = 0;
      String atd = "",mtd = "";
      
      /*@lineinfo:generated-code*//*@lineinfo:58^7*/

//  ************************************************************
//  #sql [Ctx] { select    a.app_type
//                    ,a.appsrctype_code
//  
//          
//          
//          from      merchant      m
//                    ,application  a
//          
//          where     m.merch_number = :mi.getMerchantNumber()
//                    and m.app_seq_num=a.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1887 = mi.getMerchantNumber();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    a.app_type\n                  ,a.appsrctype_code\n\n         \n        \n        from      merchant      m\n                  ,application  a\n        \n        where     m.merch_number =  :1 \n                  and m.app_seq_num=a.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_1887);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   atc = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   atd = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:71^7*/

      mi.setAppTypeCode(atc);
      mi.setAppTypeDesc(atd);
      
      /*@lineinfo:generated-code*//*@lineinfo:76^7*/

//  ************************************************************
//  #sql [Ctx] { select    merchant_type	      merchant_type_code
//  		              ,merchant_type_desc   merchant_type_desc
//  
//          
//          
//          from      merchant_types
//          
//          where     merchant_number = :mi.getMerchantNumber()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1888 = mi.getMerchantNumber();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    merchant_type\t      merchant_type_code\n\t\t              ,merchant_type_desc   merchant_type_desc\n\n         \n        \n        from      merchant_types\n        \n        where     merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_1888);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mtc = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mtd = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:87^7*/

      mi.setMerchantTypeCode(mtc);
      mi.setMerchantTypeDesc(mtd);

    }
    catch(Exception e) {
      log.error("getMerchantInfo() EXCEPTION: "+e.toString());
    }
    finally {
      if(wasStale)
        cleanUp();
    }

  }
  
  public long getNextAvailID()
  {
    long id = -1L;
    boolean wasStale = false;
    
    try {
    
      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:115^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT  EQUIPCALLTAG_SEQUENCE.nextval
//          
//          FROM    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  EQUIPCALLTAG_SEQUENCE.nextval\n         \n        FROM    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   id = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:120^7*/

    }
    catch(Exception e) {
      log.error("getNextAvailID() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      if(wasStale)
        cleanUp();
    }
    
    return id;
  }

  // helpers

  public String[][]       getEquipTypes()
  {
    log.debug("getEquipTypes() - START");
    
    boolean wasStale = false;
    String[][] rval = null;

    ResultSet rs=null;
    ResultSetIterator it=null;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      int count = 0;

      /*@lineinfo:generated-code*//*@lineinfo:155^7*/

//  ************************************************************
//  #sql [Ctx] { select    count(*)
//          
//          from		  equiptype
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    count(*)\n         \n        from\t\t  equiptype";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:160^7*/

      if(count == 0)
        rval=null;
      else {
      
        rval = new String[count][2];

        /*@lineinfo:generated-code*//*@lineinfo:168^9*/

//  ************************************************************
//  #sql [Ctx] it = { select     EQUIPTYPE_CODE
//                      ,EQUIPTYPE_DESCRIPTION
//            from		  equiptype
//            order by  EQUIPTYPE_DESCRIPTION
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select     EQUIPTYPE_CODE\n                    ,EQUIPTYPE_DESCRIPTION\n          from\t\t  equiptype\n          order by  EQUIPTYPE_DESCRIPTION";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.calltag.CallTagMESDB",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.calltag.CallTagMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:174^9*/
      
        rs = it.getResultSet();
        int i=0;

        while(rs.next()) {
          rval[i][0] = rs.getString(2);
          rval[i][1] = rs.getString(1);
          i++;
        }

        rs.close();
        it.close();
      }

    }
    catch(Exception e) {
      log.error("getEquipManufacturers() EXCEPTION: "+e.toString());
    }
    finally {
      if(wasStale)
        cleanUp();
    }

    return rval;
  }
  
  public String[][]       getEquipManufacturers()
  {
    log.debug("getEquipManufacturers() - START");
    
    boolean wasStale = false;
    String[][] rval = null;

    ResultSet rs=null;
    ResultSetIterator it=null;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      int count = 0;

      /*@lineinfo:generated-code*//*@lineinfo:220^7*/

//  ************************************************************
//  #sql [Ctx] { select    count(*)
//          
//          from		  equipmfgr
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    count(*)\n         \n        from\t\t  equipmfgr";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:225^7*/

      if(count == 0)
        rval=null;
      else {
      
        rval = new String[count][2];

        /*@lineinfo:generated-code*//*@lineinfo:233^9*/

//  ************************************************************
//  #sql [Ctx] it = { select     EQUIPMFGR_MFR_CODE
//                      ,EQUIPMFGR_MFR_NAME
//            from		  equipmfgr
//            order by  EQUIPMFGR_MFR_NAME
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select     EQUIPMFGR_MFR_CODE\n                    ,EQUIPMFGR_MFR_NAME\n          from\t\t  equipmfgr\n          order by  EQUIPMFGR_MFR_NAME";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.calltag.CallTagMESDB",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.calltag.CallTagMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:239^9*/
      
        rs = it.getResultSet();
        int i=0;

        while(rs.next()) {
          rval[i][0] = rs.getString(2);
          rval[i][1] = rs.getString(1);
          i++;
        }

        rs.close();
        it.close();
      }

    }
    catch(Exception e) {
      log.error("getEquipManufacturers() EXCEPTION: "+e.toString());
    }
    finally {
      if(wasStale)
        cleanUp();
    }

    return rval;
  }
  
  public String[][]       getAppTypes()
  {
    log.debug("getAppTypes() - START");
    
    boolean wasStale = false;
    String[][] rval = null;

    ResultSet rs=null;
    ResultSetIterator it=null;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      int count = 0;

      /*@lineinfo:generated-code*//*@lineinfo:285^7*/

//  ************************************************************
//  #sql [Ctx] { select    count(*)
//          
//          from		  app_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    count(*)\n         \n        from\t\t  app_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:290^7*/

      if(count == 0)
        rval=null;
      else {
      
        rval = new String[count][2];

        /*@lineinfo:generated-code*//*@lineinfo:298^9*/

//  ************************************************************
//  #sql [Ctx] it = { select     app_type_code
//                      ,app_description
//            from		  app_type
//            order by  app_description
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select     app_type_code\n                    ,app_description\n          from\t\t  app_type\n          order by  app_description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.calltag.CallTagMESDB",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.calltag.CallTagMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:304^9*/
      
        rs = it.getResultSet();
        int i=0;

        while(rs.next()) {
          rval[i][0] = rs.getString(2);
          rval[i][1] = rs.getString(1);
          i++;
        }

        rs.close();
        it.close();
      }

    }
    catch(Exception e) {
      log.error("getAppTypes() EXCEPTION: "+e.toString());
    }
    finally {
      if(wasStale)
        cleanUp();
    }

    return rval;
  }
  
  public String[][]       getEquipClasses()
  {
    log.debug("getEquipClasses() - START");
    
    boolean wasStale = false;
    String[][] rval = null;

    ResultSet rs=null;
    ResultSetIterator it=null;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      int count = 0;

      /*@lineinfo:generated-code*//*@lineinfo:350^7*/

//  ************************************************************
//  #sql [Ctx] { select    count(*)
//          
//          from		  equip_class
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    count(*)\n         \n        from\t\t  equip_class";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:355^7*/

      if(count == 0)
        rval=null;
      else {
      
        rval = new String[count][2];

        /*@lineinfo:generated-code*//*@lineinfo:363^9*/

//  ************************************************************
//  #sql [Ctx] it = { select     ec_class_id
//                      ,ec_class_name
//            from		  equip_class
//            order by  ec_class_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select     ec_class_id\n                    ,ec_class_name\n          from\t\t  equip_class\n          order by  ec_class_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.calltag.CallTagMESDB",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.calltag.CallTagMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:369^9*/
      
        rs = it.getResultSet();
        int i=0;

        while(rs.next()) {
          rval[i][0] = rs.getString(2);
          rval[i][1] = rs.getString(1);
          i++;
        }

        rs.close();
        it.close();
      }

    }
    catch(Exception e) {
      log.error("getEquipClasses() EXCEPTION: "+e.toString());
    }
    finally {
      if(wasStale)
        cleanUp();
    }

    return rval;
  }
  
  /*
  public String[][]       getMesInvEquipPartNums()
  {
    log.debug("getMesInvEquipPartNums() - START");
    
    boolean wasStale = false;
    String[][] rval = null;

    ResultSet rs=null;
    ResultSetIterator it=null;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      int count = 0;

      #sql [Ctx]
      {
        select    count(distinct ei_part_number)
        into      :count
        from		  equip_inventory
      };

      if(count == 0)
        rval=null;
      else {
      
        rval = new String[count][2];

        #sql [Ctx] it = 
        {
          select    distinct 
                    ei_part_number
                    ,ei_part_number
          from		  equip_inventory
          order by  ei_part_number
        };
      
        rs = it.getResultSet();
        int i=0;

        while(rs.next()) {
          rval[i][0] = rs.getString(2);
          rval[i][1] = rs.getString(1);
          i++;
        }

        rs.close();
        it.close();
      }

    }
    catch(Exception e) {
      log.error("getMesInvEquipPartNums() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      if(wasStale)
        cleanUp();
    }

    return rval;
  }
  */

  public String[][]       getEquipLendTypes()
  {
    log.debug("getEquipLendTypes() - START");
    
    boolean wasStale = false;
    String[][] rval = null;

    ResultSet rs=null;
    ResultSetIterator it=null;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      int count = 0;

      /*@lineinfo:generated-code*//*@lineinfo:483^7*/

//  ************************************************************
//  #sql [Ctx] { select 		count(*)
//          
//          from		  EQUIPLENDTYPE
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select \t\tcount(*)\n         \n        from\t\t  EQUIPLENDTYPE";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:488^7*/

      if(count == 0)
        rval=null;
      else {
      
        rval = new String[count][3];

        /*@lineinfo:generated-code*//*@lineinfo:496^9*/

//  ************************************************************
//  #sql [Ctx] it = { select 		EQUIPLENDTYPE_CODE
//                      ,EQUIPLENDTYPE_DESCRIPTION
//            from		  EQUIPLENDTYPE
//            order by  EQUIPLENDTYPE_DESCRIPTION
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select \t\tEQUIPLENDTYPE_CODE\n                    ,EQUIPLENDTYPE_DESCRIPTION\n          from\t\t  EQUIPLENDTYPE\n          order by  EQUIPLENDTYPE_DESCRIPTION";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.calltag.CallTagMESDB",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.calltag.CallTagMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:502^9*/
      
        rs = it.getResultSet();
        int i=0;

        while(rs.next()) {
          rval[i][0] = rs.getString(2);
          rval[i][1] = rs.getString(1);
          i++;
        }

        rs.close();
        it.close();
      }

    }
    catch(Exception e) {
      log.error("getEquipLendTypes() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      if(wasStale)
        cleanUp();
    }

    return rval;
  }

  public String[][]       getParentTypes()
  {
    log.debug("getParentTypes() - START");
    
    boolean wasStale = false;
    String[][] rval = null;

    ResultSet rs=null;
    ResultSetIterator it=null;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      int count = 0;

      /*@lineinfo:generated-code*//*@lineinfo:548^7*/

//  ************************************************************
//  #sql [Ctx] { select 		count(*)
//          
//          from		  EQUIP_ACTION_TYPE
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select \t\tcount(*)\n         \n        from\t\t  EQUIP_ACTION_TYPE";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:553^7*/

      if(count == 0)
        rval=null;
      else {
      
        rval = new String[count][2];

        /*@lineinfo:generated-code*//*@lineinfo:561^9*/

//  ************************************************************
//  #sql [Ctx] it = { select 		code,description
//            from		  EQUIP_ACTION_TYPE
//            order by  sort_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select \t\tcode,description\n          from\t\t  EQUIP_ACTION_TYPE\n          order by  sort_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.calltag.CallTagMESDB",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.calltag.CallTagMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:566^9*/
      
        rs = it.getResultSet();
        int i=0;

        while(rs.next()) {
          rval[i][0] = rs.getString(2);
          rval[i][1] = rs.getString(1);
          i++;
        }

        rs.close();
        it.close();
      }

    }
    catch(Exception e) {
      log.error("getParentTypes() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      if(wasStale)
        cleanUp();
    }

    return rval;
  }
  
  public String[][]       getCallTagItemStatuses()
  {
    boolean wasStale = false;
    String[][] rval = null;

    ResultSet rs=null;
    ResultSetIterator it=null;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      int count = 0;

      /*@lineinfo:generated-code*//*@lineinfo:610^7*/

//  ************************************************************
//  #sql [Ctx] { select 		count(*)
//          
//          from		  EQUIP_CALLTAG_ITEM_STATUS
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select \t\tcount(*)\n         \n        from\t\t  EQUIP_CALLTAG_ITEM_STATUS";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:615^7*/

      if(count == 0)
        rval=null;
      else {
      
        rval = new String[count][2];

        /*@lineinfo:generated-code*//*@lineinfo:623^9*/

//  ************************************************************
//  #sql [Ctx] it = { select 		code,description
//            from		  EQUIP_CALLTAG_ITEM_STATUS
//            order by  code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select \t\tcode,description\n          from\t\t  EQUIP_CALLTAG_ITEM_STATUS\n          order by  code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.calltag.CallTagMESDB",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.calltag.CallTagMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:628^9*/
      
        rs = it.getResultSet();
        int i=0;

        while(rs.next()) {
          rval[i][0] = rs.getString(2);
          rval[i][1] = rs.getString(1);
          i++;
        }

        rs.close();
        it.close();
      }

    }
    catch(Exception e) {
      log.error("getCallTagItemStatuses() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      if(wasStale)
        cleanUp();
    }

    return rval;
  }

  public String[][]       getEquipModels()
  {
    boolean           doDisconnect = false;
    String[][]        models = null;
    
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      if (isConnectionStale())
      {
        connect();
        doDisconnect = true;
      }

      /*@lineinfo:generated-code*//*@lineinfo:671^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_model   model,
//                  equip_model || ' - ' || equip_descriptor
//                                descriptor
//          from    equipment
//          where   equiptype_code not in ( 7, 8, 9 )
//          order by model
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_model   model,\n                equip_model || ' - ' || equip_descriptor\n                              descriptor\n        from    equipment\n        where   equiptype_code not in ( 7, 8, 9 )\n        order by model";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.calltag.CallTagMESDB",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.calltag.CallTagMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:679^7*/
      rs = it.getResultSet();

      ArrayList modelList = new ArrayList();
      while (rs.next())
      {
        String[] modelItem = new String[2];
        modelItem[0] = rs.getString("descriptor");
        modelItem[1] = rs.getString("model");
        modelList.add(modelItem);
      }
      
      models = new String[modelList.size()][];
      int idx = 0;
      for(Iterator i = modelList.iterator(); i.hasNext();)
      {
        models[idx++] = (String[])i.next();
      }
    }
    catch(Exception e)
    {
      log.error("Error in loading equip models: " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { it.close(); } catch (Exception e) { }
      
      if (doDisconnect)
      {
        cleanUp();
      }
    }

    return models;
  }

  public String[][] getEquipInvStatuses()
  {
    boolean wasStale = false;
    String[][] rval = null;

    ResultSet rs=null;
    ResultSetIterator it=null;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      int count = 0;

      /*@lineinfo:generated-code*//*@lineinfo:733^7*/

//  ************************************************************
//  #sql [Ctx] { select 		count(*)
//          
//          from		  equip_status
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select \t\tcount(*)\n         \n        from\t\t  equip_status";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:738^7*/

      if(count == 0)
        rval=null;
      else {
      
        rval = new String[count][3];

        /*@lineinfo:generated-code*//*@lineinfo:746^9*/

//  ************************************************************
//  #sql [Ctx] it = { select 		equip_status_id
//                      ,equip_status_desc
//                      ,associated_lend_type
//            from		  equip_status
//            order by  equip_status_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select \t\tequip_status_id\n                    ,equip_status_desc\n                    ,associated_lend_type\n          from\t\t  equip_status\n          order by  equip_status_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.calltag.CallTagMESDB",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.calltag.CallTagMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:753^9*/
      
        rs = it.getResultSet();
        int i=0;

        while(rs.next()) {
          rval[i][0] = rs.getString(2);
          rval[i][1] = rs.getString(1);
          rval[i][2] = rs.getString(3);
          i++;
        }

        rs.close();
        it.close();
      }

    }
    catch(Exception e) {
      log.error("getEquipInvStatuses() EXCEPTION: "+e.toString());
    }
    finally {
      if(wasStale)
        cleanUp();
    }

    return rval;
  }

  public long getCallTagIdFromAcrId(long acrid)
  {
    boolean wasStale = false;
    long id = -1L;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      /*@lineinfo:generated-code*//*@lineinfo:793^7*/

//  ************************************************************
//  #sql [Ctx] { select  ct_seq_num
//          
//          from    equip_calltag
//          where   parentkey_refnum = :acrid
//                  and parent_type = :mesConstants.ACTION_CODE_ACR
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ct_seq_num\n         \n        from    equip_calltag\n        where   parentkey_refnum =  :1 \n                and parent_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,acrid);
   __sJT_st.setInt(2,mesConstants.ACTION_CODE_ACR);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   id = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:800^7*/

    }
    catch(Exception e) {
      log.error("getCallTagIdFromAcrId(acrid:"+acrid+") EXCEPTION: "+e.toString());
    }
    finally {
      if(wasStale)
        cleanUp();
    }

    return id;
  }

  public int getAppType(String merchNum)
  {
    int appType = 0;  // (mes default)

    try {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:821^7*/

//  ************************************************************
//  #sql [Ctx] { select merchant_type
//          
//          from   merchant_types
//          where  merchant_number = :merchNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select merchant_type\n         \n        from   merchant_types\n        where  merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:827^7*/

    }
    catch(Exception e) {
      log.error("getAppType() EXCEPTION: "+e.toString());
    }
    finally {
      cleanUp();
    }

    return appType;
  }

  // INSERT
  public long insert(CallTag ct,boolean bCommit)
  {
    log.debug("insertCallTag() - START");
    
    boolean wasStale = false;
    long id = -1L;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      id = ct.getId();

      if(id<0)
        id=getNextAvailID();
    
      Date now = new Date();
      java.sql.Timestamp sqld = new java.sql.Timestamp(now.getTime());
      java.sql.Timestamp sqlbd = new java.sql.Timestamp(ct.getBillDate()==null? 0L:ct.getBillDate().getTime());

      // insert call tag record
      
      /*@lineinfo:generated-code*//*@lineinfo:866^7*/

//  ************************************************************
//  #sql [Ctx] { insert into EQUIP_CALLTAG
//          (  
//            CT_SEQ_NUM
//            ,date_created
//            ,last_modified
//            ,last_status_userid
//            ,parentkey_refnum
//            ,parentkey_partnum
//            ,parentkey_serialnum
//            ,parent_type
//            ,bill_date
//            ,bill_amount
//            ,bill_charged
//            ,merch_number
//            ,notes
//          ) 
//          values 
//          (
//            :id
//            ,:sqld
//            ,:sqld
//            ,:ct.getLastStatusUserid()
//            ,:ct.getParentRefNum()
//            ,:ct.getParentPartNum()
//            ,:ct.getParentSerialNum()
//            ,:ct.getParentType()
//            ,:sqlbd
//            ,:ct.getBillAmount()
//            ,:ct.isBillCharged()
//            ,:ct.getMerchNumber()
//            ,:ct.getNotes()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1889 = ct.getLastStatusUserid();
 long __sJT_1890 = ct.getParentRefNum();
 String __sJT_1891 = ct.getParentPartNum();
 String __sJT_1892 = ct.getParentSerialNum();
 int __sJT_1893 = ct.getParentType();
 double __sJT_1894 = ct.getBillAmount();
 boolean __sJT_1895 = ct.isBillCharged();
 long __sJT_1896 = ct.getMerchNumber();
 String __sJT_1897 = ct.getNotes();
   String theSqlTS = "insert into EQUIP_CALLTAG\n        (  \n          CT_SEQ_NUM\n          ,date_created\n          ,last_modified\n          ,last_status_userid\n          ,parentkey_refnum\n          ,parentkey_partnum\n          ,parentkey_serialnum\n          ,parent_type\n          ,bill_date\n          ,bill_amount\n          ,bill_charged\n          ,merch_number\n          ,notes\n        ) \n        values \n        (\n           :1 \n          , :2 \n          , :3 \n          , :4 \n          , :5 \n          , :6 \n          , :7 \n          , :8 \n          , :9 \n          , :10 \n          , :11 \n          , :12 \n          , :13 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setTimestamp(2,sqld);
   __sJT_st.setTimestamp(3,sqld);
   __sJT_st.setString(4,__sJT_1889);
   __sJT_st.setLong(5,__sJT_1890);
   __sJT_st.setString(6,__sJT_1891);
   __sJT_st.setString(7,__sJT_1892);
   __sJT_st.setInt(8,__sJT_1893);
   __sJT_st.setTimestamp(9,sqlbd);
   __sJT_st.setDouble(10,__sJT_1894);
   __sJT_st.setBoolean(11,__sJT_1895);
   __sJT_st.setLong(12,__sJT_1896);
   __sJT_st.setString(13,__sJT_1897);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:900^7*/

      // insert the child items
      CallTagItem cti;
      for(Enumeration e=ct.getItemsEnumerator();e.hasMoreElements();) {
        cti = (CallTagItem)e.nextElement();

        insert(cti,false);
      }

      // insert the child notifications
      CallTagNotification ctn;
      for(Enumeration e=ct.getNotificationsEnumerator();e.hasMoreElements();) {
        ctn = (CallTagNotification)e.nextElement();
        
        insert(ctn,false);
      }

      ct.setDateCreated(now);
      ct.setLastModified(now);
      ct.setId(id);
      
      if(bCommit)
        commit();

      ct.clean();
    }
    catch(Exception e) {
      ct.setId(0L);
      log.error("insert(CallTag) EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      if(wasStale)
        cleanUp();
    }
    
    return ct.getId();
  }
  
  public long insert(CallTagItem cti,boolean bCommit)
  {
    log.debug("insert(CallTagItem) - START");
    
    boolean wasStale = false;
    long id = -1L;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      id = cti.getId();

      if(id<0)
        id=getNextAvailID();
    
      Date now = new Date();
      java.sql.Date sqld = new java.sql.Date(now.getTime());

      // insert call tag item record
      EquipmentKey ek = cti.getEquipmentKey();
      
      /*@lineinfo:generated-code*//*@lineinfo:964^7*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_calltag_item
//          (  
//            cti_seq_num
//            ,ctid
//            ,ei_part_number
//            ,ei_serial_number
//            ,app_seq_num
//            ,equiplendtype_code
//            ,status_code
//            ,received
//          ) 
//          values 
//          (
//            :id
//            ,:cti.getCallTagId()
//            ,:ek.getPartNum()
//            ,:ek.getSerialNum()
//            ,:ek.getAppSeqNum()
//            ,:ek.getLendTypeCode()
//            ,:cti.getStatusCode()
//            ,:cti.getReceived()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1898 = cti.getCallTagId();
 String __sJT_1899 = ek.getPartNum();
 String __sJT_1900 = ek.getSerialNum();
 long __sJT_1901 = ek.getAppSeqNum();
 int __sJT_1902 = ek.getLendTypeCode();
 int __sJT_1903 = cti.getStatusCode();
 String __sJT_1904 = cti.getReceived();
   String theSqlTS = "insert into equip_calltag_item\n        (  \n          cti_seq_num\n          ,ctid\n          ,ei_part_number\n          ,ei_serial_number\n          ,app_seq_num\n          ,equiplendtype_code\n          ,status_code\n          ,received\n        ) \n        values \n        (\n           :1 \n          , :2 \n          , :3 \n          , :4 \n          , :5 \n          , :6 \n          , :7 \n          , :8 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setLong(2,__sJT_1898);
   __sJT_st.setString(3,__sJT_1899);
   __sJT_st.setString(4,__sJT_1900);
   __sJT_st.setLong(5,__sJT_1901);
   __sJT_st.setInt(6,__sJT_1902);
   __sJT_st.setInt(7,__sJT_1903);
   __sJT_st.setString(8,__sJT_1904);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:988^7*/

      cti.setId(id);
      
      if(bCommit)
        commit();
  
      cti.clean();
    }
    catch(Exception e) {
      cti.setId(0L);
      log.error("insert(CallTagItem) EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      if(wasStale)
        cleanUp();
    }
    
    return cti.getId();
  }
  
  public long insert(CallTagNotification ctn,boolean bCommit)
  {
    boolean wasStale = false;
    long id = -1L;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      id = ctn.getId();

      if(id<0)
        id=getNextAvailID();
    
      // insert call tag notification record

      java.sql.Timestamp isdt = new java.sql.Timestamp(ctn.getIssueDate()==null? 0L:ctn.getIssueDate().getTime());
      
      /*@lineinfo:generated-code*//*@lineinfo:1030^7*/

//  ************************************************************
//  #sql [Ctx] { insert into EQUIP_CALLTAG_NOTIFICATION
//          (  
//            ctn_seq_num
//            ,ctid
//            ,pkg_trk_num
//            ,issue_date
//            ,letter_name
//          ) 
//          values 
//          (
//            :id
//            ,:ctn.getCallTagId()
//            ,:ctn.getPkgTrkNum()
//            ,:isdt
//            ,:ctn.getLetterName()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1905 = ctn.getCallTagId();
 String __sJT_1906 = ctn.getPkgTrkNum();
 String __sJT_1907 = ctn.getLetterName();
   String theSqlTS = "insert into EQUIP_CALLTAG_NOTIFICATION\n        (  \n          ctn_seq_num\n          ,ctid\n          ,pkg_trk_num\n          ,issue_date\n          ,letter_name\n        ) \n        values \n        (\n           :1 \n          , :2 \n          , :3 \n          , :4 \n          , :5 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setLong(2,__sJT_1905);
   __sJT_st.setString(3,__sJT_1906);
   __sJT_st.setTimestamp(4,isdt);
   __sJT_st.setString(5,__sJT_1907);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1048^7*/
      
      if(bCommit)
        commit();
  
      ctn.clean();
    }
    catch(Exception e) {
      ctn.setId(0L);
      log.error("insert(CallTagNotification) EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      if(wasStale)
        cleanUp();
    }
    
    return ctn.getId();
  }

  // DELETE
  public void deleteCallTag(long pkid)
  {
    boolean wasStale = false;
    
    try {
    
      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:1079^7*/

//  ************************************************************
//  #sql [Ctx] { delete  equip_calltag
//          where   ct_seq_num = :pkid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  equip_calltag\n        where   ct_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1083^7*/

      // commit the db trans
      commit();
    }
    catch(Exception e) {
      log.error("deleteCallTag() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      if(wasStale)
        cleanUp();
    }
  }

  public void deleteCallTagItem(long pkid)
  {
    boolean wasStale = false;
    
    try {
    
      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:1108^7*/

//  ************************************************************
//  #sql [Ctx] { delete  equip_calltag_item
//          where   cti_seq_num = :pkid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  equip_calltag_item\n        where   cti_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1112^7*/

      // commit the db trans
      commit();
    }
    catch(Exception e) {
      log.error("deleteCallTagItem() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      if(wasStale)
        cleanUp();
    }
  }
  
  public void deleteCallTagNotification(long pkid)
  {
    boolean wasStale = false;
    
    try {
    
      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:1137^7*/

//  ************************************************************
//  #sql [Ctx] { delete  equip_calltag_notification
//          where   ctn_seq_num = :pkid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  equip_calltag_notification\n        where   ctn_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1141^7*/

      // commit the db trans
      commit();
    }
    catch(Exception e) {
      log.error("deleteCallTagNotification() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      if(wasStale)
        cleanUp();
    }
  }

  // SELECT
  public CallTag loadCallTag(long pkid)
  {
    log.debug("loadCallTag() - START");
    
    boolean wasStale = false;
    ResultSet rs=null;
    ResultSetIterator it=null;

    CallTag ct = null;
    
    try {
    
      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      /*@lineinfo:generated-code*//*@lineinfo:1173^7*/

//  ************************************************************
//  #sql [Ctx] it = { select 		ct.CT_SEQ_NUM id
//  					        ,ct.*
//  							    ,eat.description action_desc
//  							    ,eat2.description parent_type_desc
//          from		  equip_calltag               ct
//                    ,EQUIP_ACTION_TYPE          eat
//                    ,EQUIP_ACTION_TYPE          eat2
//          where		  ct.ACTION_CODE=eat.CODE(+)
//                    and ct.parent_type=eat2.CODE(+)
//  			            and ct.CT_SEQ_NUM = :pkid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select \t\tct.CT_SEQ_NUM id\n\t\t\t\t\t        ,ct.*\n\t\t\t\t\t\t\t    ,eat.description action_desc\n\t\t\t\t\t\t\t    ,eat2.description parent_type_desc\n        from\t\t  equip_calltag               ct\n                  ,EQUIP_ACTION_TYPE          eat\n                  ,EQUIP_ACTION_TYPE          eat2\n        where\t\t  ct.ACTION_CODE=eat.CODE(+)\n                  and ct.parent_type=eat2.CODE(+)\n\t\t\t            and ct.CT_SEQ_NUM =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"28com.mes.calltag.CallTagMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1185^7*/
      
      rs = it.getResultSet();

      if(!rs.next())
        throw new Exception("Call Tag record of id: '"+pkid+"' does not exist.  Load failed.");

      ct = new CallTag();

      // fill call tag spec data
      ct.setId(rs.getLong("id"));
      ct.setDateCreated((java.util.Date)rs.getTimestamp("date_created"));
      ct.setLastModified((java.util.Date)rs.getTimestamp("last_modified"));
      ct.setLastStatusUserid(rs.getString("last_status_userid"));
      
      int parentType = rs.getInt("parent_type");
        
      EquipmentActionKey key = new EquipmentActionKey(parentType,rs.getLong("PARENTKEY_REFNUM"));
      if(!key.isSet())
        key = new EquipmentActionKey(parentType,rs.getString("PARENTKEY_PARTNUM"),rs.getString("PARENTKEY_SERIALNUM"));
      ct.setParentKey(key);

      ct.setBillDate((java.util.Date)rs.getTimestamp("bill_date"));
      ct.setBillAmount(rs.getDouble("bill_amount"));
      ct.setBillCharged(rs.getInt("bill_charged")==1);
      
      ct.setNotes(rs.getString("notes"));
      
      ct.setMerchNumber(rs.getString("merch_number"));

      rs.close();
      it.close();

      ct.clean();

      // load the items

      /*@lineinfo:generated-code*//*@lineinfo:1222^7*/

//  ************************************************************
//  #sql [Ctx] it = { select 		cti.cti_seq_num id
//          from		  EQUIP_CALLTAG_ITEM cti
//          where     cti.ctid = :ct.getId()
//          order by  id desc
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1908 = ct.getId();
  try {
   String theSqlTS = "select \t\tcti.cti_seq_num id\n        from\t\t  EQUIP_CALLTAG_ITEM cti\n        where     cti.ctid =  :1 \n        order by  id desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1908);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"29com.mes.calltag.CallTagMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1228^4*/

      rs = it.getResultSet();

      CallTagItem cti;
      
      while(rs.next()) {
        cti = loadCallTagItem(rs.getLong(1));
        ct.addItem(cti);
      }

      rs.close();
      it.close();

      // load the notifications

      /*@lineinfo:generated-code*//*@lineinfo:1244^7*/

//  ************************************************************
//  #sql [Ctx] it = { select 		ctn.ctn_seq_num id
//          from		  EQUIP_CALLTAG_NOTIFICATION ctn
//          where     ctn.ctid = :ct.getId()
//          order by  id desc
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1909 = ct.getId();
  try {
   String theSqlTS = "select \t\tctn.ctn_seq_num id\n        from\t\t  EQUIP_CALLTAG_NOTIFICATION ctn\n        where     ctn.ctid =  :1 \n        order by  id desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1909);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"30com.mes.calltag.CallTagMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1250^4*/

      rs = it.getResultSet();

      CallTagNotification ctn;
      
      while(rs.next()) {
        ctn = loadCallTagNotification(rs.getLong(1));
        ct.addNotification(ctn);
      }

      rs.close();
      it.close();
    }
    catch(Exception e) {
      log.error("loadCallTag() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      try{ it.close(); } catch(Exception e) {}
      if(wasStale)
        cleanUp();
    }
     
    return ct; 
  }
  
  public CallTagItem loadCallTagItem(long pkid)
  {
    log.debug("loadCallTagItem() - START");
    
    boolean wasStale = false;
    ResultSet rs=null;
    ResultSetIterator it=null;

    CallTagItem cti = null;
    
    try {
    
      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      /*@lineinfo:generated-code*//*@lineinfo:1293^7*/

//  ************************************************************
//  #sql [Ctx] it = { select 		cti.CTI_SEQ_NUM id
//  					        ,cti.*
//  							    ,ets.DESCRIPTION status_desc
//          from		  equip_calltag_item cti
//  				          ,EQUIP_CALLTAG_ITEM_STATUS ets
//          where		  cti.status_code=ets.CODE(+)
//  			            and cti.CTI_SEQ_NUM = :pkid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select \t\tcti.CTI_SEQ_NUM id\n\t\t\t\t\t        ,cti.*\n\t\t\t\t\t\t\t    ,ets.DESCRIPTION status_desc\n        from\t\t  equip_calltag_item cti\n\t\t\t\t          ,EQUIP_CALLTAG_ITEM_STATUS ets\n        where\t\t  cti.status_code=ets.CODE(+)\n\t\t\t            and cti.CTI_SEQ_NUM =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"31com.mes.calltag.CallTagMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1302^7*/
      
      rs = it.getResultSet();

      if(!rs.next())
        throw new Exception("Call Tag Item record of id: '"+pkid+"' does not exist.  Load failed.");

      cti = new CallTagItem();

      // fill call tag spec data
      cti.setId(rs.getLong("id"));
      cti.setCallTagId(rs.getLong("ctid"));
      cti.setStatusCode(rs.getInt("status_code"));
      cti.setStatusDesc(rs.getString("status_desc"));
      cti.setReceived(rs.getString("received"));
      
      // generate equip key based on the info available
      String partNum    = rs.getString("ei_part_number");
      String serialNum  = rs.getString("ei_serial_number");
      
      log.debug("call tag item has part num: " + partNum);
      log.debug("call tag item has serial num: " + serialNum);
      
      // first try for key based on equip model (part num) and serial num 
      // (EquipInvKey)
      EquipmentKey ek = new EquipmentKey(partNum,serialNum);
      if(!ek.isSet())
      {
        log.debug("unable to set key with part and serial nums...");
        
        // second try based on serial num only (EquipInvKey)
        if (serialNum != null && serialNum.length() > 0)
        {
          ek = new EquipmentKey(null,serialNum);
          log.debug("key is set with serial num only");
        }
        // finally base it on appseqnum, part num and lend type (MerchEquipKey)
        else
        {
          ek = new EquipmentKey(rs.getLong("app_seq_num"),partNum,
            rs.getInt("equiplendtype_code"));
          log.debug("key is set with app_seq_num, part num and lendtype code");
        }
      }
      log.debug("key.toString() = '" + ek.toString() + "'");
      cti.setEquipmentKey(ek);

      rs.close();
      it.close();

      cti.clean();
    }
    catch(Exception e) {
      log.error("loadCallTagItem() EXCEPTION: '" + e + "'.");
    }
    finally {
      try{ it.close(); } catch(Exception e) {}
      if(wasStale)
        cleanUp();
    }
     
    return cti; 
  }

  public CallTagNotification loadCallTagNotification(long pkid)
  {
    log.debug("loadCallTagNotification() - START");
    
    boolean wasStale = false;
    ResultSet rs=null;
    ResultSetIterator it=null;

    CallTagNotification ctn = null;
    
    try {
    
      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      /*@lineinfo:generated-code*//*@lineinfo:1383^7*/

//  ************************************************************
//  #sql [Ctx] it = { select 		ctn.CTN_SEQ_NUM id
//  					        ,ctn.*
//          from		  equip_calltag_notification ctn
//          where		  ctn.CTN_SEQ_NUM = :pkid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select \t\tctn.CTN_SEQ_NUM id\n\t\t\t\t\t        ,ctn.*\n        from\t\t  equip_calltag_notification ctn\n        where\t\t  ctn.CTN_SEQ_NUM =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"32com.mes.calltag.CallTagMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1389^7*/
      
      rs = it.getResultSet();

      if(!rs.next())
        throw new Exception("Call Tag Notification record of id: '"+pkid+"' does not exist.  Load failed.");

      ctn = new CallTagNotification();

      // fill call tag spec data
      ctn.setId(rs.getLong("id"));
      
      ctn.setCallTagId(rs.getLong("ctid"));

      ctn.setPkgTrkNum(rs.getString("pkg_trk_num"));
      ctn.setIssueDate(rs.getDate("issue_date"));
      ctn.setLetterName(rs.getString("letter_name"));

      rs.close();
      it.close();

      ctn.clean();
    }
    catch(Exception e) {
      log.error("loadCallTagNotification() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      try{ it.close(); } catch(Exception e) {}
      if(wasStale)
        cleanUp();
    }
     
    return ctn; 
  }

  // UPDATE
  public boolean persist(CallTag ct,boolean bCommit)
  {
    log.debug("persist(CallTag) - START");
    
    boolean wasStale = false;
    boolean rval = false;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      long id = ct.getId();
      
      if(id < 1) {
        id=insert(ct,bCommit);
        return (id > 0L);
      }
      
      int count;

      /*@lineinfo:generated-code*//*@lineinfo:1448^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT  COUNT(*)
//          
//          FROM    equip_calltag
//          WHERE   ct_seq_num=:id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  COUNT(*)\n         \n        FROM    equip_calltag\n        WHERE   ct_seq_num= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1454^7*/

      if(count < 1) {
        id = insert(ct,bCommit);
        return (id > 0);
      }
    
      if(ct.isDirty()) {

        Date now = new Date();

        java.sql.Timestamp dlm = new java.sql.Timestamp(now.getTime());
      
        StringBuffer sb = new StringBuffer();

        // persist items
        
        CallTagItem cti;

        for(Enumeration e=ct.getItemsEnumerator();e.hasMoreElements();) {
          cti = (CallTagItem)e.nextElement();
          
          persist(cti,false);
          
          sb.append(",");
          sb.append(cti.getId());
        }

        // delete removed items
        if(sb.length()>1) {
          final String inclause = sb.substring(1);  // remove preceeding ','
          final String dynmcSQL = 
              "DELETE EQUIP_CALLTAG_ITEM"
            +" WHERE  ctid="+ct.getId()
            +" AND cti_seq_num not in ("+inclause+")";
            
          PreparedStatement ps = getPreparedStatement(dynmcSQL);
          ps.executeQuery();
          ps.close();
        } else {
          /*@lineinfo:generated-code*//*@lineinfo:1494^11*/

//  ************************************************************
//  #sql [Ctx] { delete  EQUIP_CALLTAG_ITEM
//              where   ctid=:ct.getId()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1910 = ct.getId();
   String theSqlTS = "delete  EQUIP_CALLTAG_ITEM\n            where   ctid= :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"34com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1910);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1498^11*/
        }

        // persist notifications

        sb.delete(0,sb.length()); // reset

        CallTagNotification ctn;

        for(Enumeration e=ct.getNotificationsEnumerator();e.hasMoreElements();) {
          ctn = (CallTagNotification)e.nextElement();
          
          persist(ctn,false);
          
          sb.append(",");
          sb.append(ctn.getId());
        }

        // delete removed child recs
        if(sb.length()>1) {
          final String inclause = sb.substring(1);  // remove preceeding ','
          final String dynmcSQL = 
              "DELETE EQUIP_CALLTAG_NOTIFICATION"
            +" WHERE  ctid="+ct.getId()
            +" AND ctn_seq_num not in ("+inclause+")";
            
          PreparedStatement ps = getPreparedStatement(dynmcSQL);
          ps.executeQuery();
          ps.close();
        } else {
          /*@lineinfo:generated-code*//*@lineinfo:1528^11*/

//  ************************************************************
//  #sql [Ctx] { delete  EQUIP_CALLTAG_NOTIFICATION
//              where   ctid=:ct.getId()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1911 = ct.getId();
   String theSqlTS = "delete  EQUIP_CALLTAG_NOTIFICATION\n            where   ctid= :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"35com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1911);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1532^11*/
        }

        // persist parent
      
        java.sql.Timestamp sqld_bd = ct.getBillDate()==null? null:new java.sql.Timestamp(ct.getBillDate().getTime());

        /*@lineinfo:generated-code*//*@lineinfo:1539^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_calltag
//            set     last_modified = :dlm
//                    ,last_status_userid = :ct.getLastStatusUserid()
//                    ,PARENTKEY_REFNUM = :ct.getParentRefNum()
//                    ,PARENTKEY_PARTNUM = :ct.getParentPartNum()
//                    ,PARENTKEY_SERIALNUM = :ct.getParentSerialNum()
//                    ,bill_date = :sqld_bd
//                    ,bill_amount = :ct.getBillAmount()
//                    ,bill_charged = :ct.isBillCharged()
//                    ,merch_number = :ct.getMerchNumber()
//                    ,notes = :ct.getNotes()
//            where   ct_seq_num = :id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1912 = ct.getLastStatusUserid();
 long __sJT_1913 = ct.getParentRefNum();
 String __sJT_1914 = ct.getParentPartNum();
 String __sJT_1915 = ct.getParentSerialNum();
 double __sJT_1916 = ct.getBillAmount();
 boolean __sJT_1917 = ct.isBillCharged();
 long __sJT_1918 = ct.getMerchNumber();
 String __sJT_1919 = ct.getNotes();
   String theSqlTS = "update  equip_calltag\n          set     last_modified =  :1 \n                  ,last_status_userid =  :2 \n                  ,PARENTKEY_REFNUM =  :3 \n                  ,PARENTKEY_PARTNUM =  :4 \n                  ,PARENTKEY_SERIALNUM =  :5 \n                  ,bill_date =  :6 \n                  ,bill_amount =  :7 \n                  ,bill_charged =  :8 \n                  ,merch_number =  :9 \n                  ,notes =  :10 \n          where   ct_seq_num =  :11";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"36com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,dlm);
   __sJT_st.setString(2,__sJT_1912);
   __sJT_st.setLong(3,__sJT_1913);
   __sJT_st.setString(4,__sJT_1914);
   __sJT_st.setString(5,__sJT_1915);
   __sJT_st.setTimestamp(6,sqld_bd);
   __sJT_st.setDouble(7,__sJT_1916);
   __sJT_st.setBoolean(8,__sJT_1917);
   __sJT_st.setLong(9,__sJT_1918);
   __sJT_st.setString(10,__sJT_1919);
   __sJT_st.setLong(11,id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1553^9*/
      
        ct.clean();
      
        if(bCommit)
          commit();

        rval = true;
      } else
        rval = true;

    }
    catch(Exception e) {
      log.error("persist(CallTag) EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      if(wasStale)
        cleanUp();
    }

    return rval;
  }

  public boolean persist(CallTagItem cti,boolean bCommit)
  {
    boolean wasStale = false;
    boolean rval = false;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      long id = cti.getId();
      
      if(id < 1) {
        id=insert(cti,bCommit);
        return (id > 0);
      }
      
      int count;

      /*@lineinfo:generated-code*//*@lineinfo:1597^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT  COUNT(*)
//          
//          FROM    EQUIP_CALLTAG_ITEM
//          WHERE   cti_seq_num=:id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  COUNT(*)\n         \n        FROM    EQUIP_CALLTAG_ITEM\n        WHERE   cti_seq_num= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1603^7*/

      if(count < 1) {
        id = insert(cti,bCommit);
        return (id > 0);
      }

      if(cti.isDirty()) {

        EquipmentKey ek = cti.getEquipmentKey();

        /*@lineinfo:generated-code*//*@lineinfo:1614^9*/

//  ************************************************************
//  #sql [Ctx] { update  EQUIP_CALLTAG_ITEM
//            set     ctid = :cti.getCallTagId()
//                    ,ei_part_number = :ek.getPartNum()
//                    ,ei_serial_number = :ek.getSerialNum()
//                    ,app_seq_num = :ek.getAppSeqNum()
//                    ,equiplendtype_code = :ek.getLendTypeCode()
//                    ,status_code = :cti.getStatusCode()
//                    ,received = :cti.getReceived()
//            where   cti_seq_num = :id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1920 = cti.getCallTagId();
 String __sJT_1921 = ek.getPartNum();
 String __sJT_1922 = ek.getSerialNum();
 long __sJT_1923 = ek.getAppSeqNum();
 int __sJT_1924 = ek.getLendTypeCode();
 int __sJT_1925 = cti.getStatusCode();
 String __sJT_1926 = cti.getReceived();
   String theSqlTS = "update  EQUIP_CALLTAG_ITEM\n          set     ctid =  :1 \n                  ,ei_part_number =  :2 \n                  ,ei_serial_number =  :3 \n                  ,app_seq_num =  :4 \n                  ,equiplendtype_code =  :5 \n                  ,status_code =  :6 \n                  ,received =  :7 \n          where   cti_seq_num =  :8";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"38com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1920);
   __sJT_st.setString(2,__sJT_1921);
   __sJT_st.setString(3,__sJT_1922);
   __sJT_st.setLong(4,__sJT_1923);
   __sJT_st.setInt(5,__sJT_1924);
   __sJT_st.setInt(6,__sJT_1925);
   __sJT_st.setString(7,__sJT_1926);
   __sJT_st.setLong(8,id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1625^9*/
      
        cti.clean();
      
        if(bCommit)
          commit();

        rval = true;
      } else
        rval = true;

    }
    catch(Exception e) {
      log.error("persist(CallTagItem) EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      if(wasStale)
        cleanUp();
    }

    return rval;
  }

  public boolean persist(CallTagNotification ctn,boolean bCommit)
  {
    log.debug("persist(CallTagNotification) - START");
    
    boolean wasStale = false;
    boolean rval = false;

    try {

      if(isConnectionStale()) {
        connect();
        wasStale = true;
      }

      long id = ctn.getId();
      
      if(id < 1) {
        id=insert(ctn,bCommit);
        return (id > 0);
      }
      
      int count;

      /*@lineinfo:generated-code*//*@lineinfo:1671^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT  COUNT(*)
//          
//          FROM    EQUIP_CALLTAG_NOTIFICATION
//          WHERE   ctn_seq_num=:id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  COUNT(*)\n         \n        FROM    EQUIP_CALLTAG_NOTIFICATION\n        WHERE   ctn_seq_num= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.calltag.CallTagMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1677^7*/

      if(count < 1) {
        id = insert(ctn,bCommit);
        return (id > 0);
      }

      if(ctn.isDirty()) {

        java.sql.Timestamp isdt = ctn.getIssueDate()==null? null:new java.sql.Timestamp(ctn.getIssueDate().getTime());
        
        /*@lineinfo:generated-code*//*@lineinfo:1688^9*/

//  ************************************************************
//  #sql [Ctx] { update  EQUIP_CALLTAG_NOTIFICATION
//            set     ctid = :ctn.getCallTagId()
//                    ,pkg_trk_num = :ctn.getPkgTrkNum()
//                    ,issue_date = :isdt
//                    ,letter_name = :ctn.getLetterName()
//            where   ctn_seq_num = :id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1927 = ctn.getCallTagId();
 String __sJT_1928 = ctn.getPkgTrkNum();
 String __sJT_1929 = ctn.getLetterName();
   String theSqlTS = "update  EQUIP_CALLTAG_NOTIFICATION\n          set     ctid =  :1 \n                  ,pkg_trk_num =  :2 \n                  ,issue_date =  :3 \n                  ,letter_name =  :4 \n          where   ctn_seq_num =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"40com.mes.calltag.CallTagMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1927);
   __sJT_st.setString(2,__sJT_1928);
   __sJT_st.setTimestamp(3,isdt);
   __sJT_st.setString(4,__sJT_1929);
   __sJT_st.setLong(5,id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1696^9*/
      
        ctn.clean();
      
        if(bCommit)
          commit();

        rval = true;
      } else
        rval = true;

    }
    catch(Exception e) {
      log.error("persist(CallTagNotification) EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      if(wasStale)
        cleanUp();
    }

    return rval;
  }

}/*@lineinfo:generated-code*/