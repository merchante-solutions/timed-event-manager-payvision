package com.mes.user;

import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import org.apache.log4j.Logger;

public class PermissionsMap
{
  static Logger log = Logger.getLogger(PermissionsMap.class);
  
  private String functionStr;
  private HashMap functions;
  
  public PermissionsMap(String functionStr)
  {
    setFunctionStr(functionStr);
  }
  
  /**
   * Given a string and a delimiter string an array is generated containing
   * all substrings separated by the delimiter.
   */
  private String[] split(String str, String delim)
  {
    StringTokenizer tok = new StringTokenizer(str,delim);
    String[] strs = new String[tok.countTokens()];
    for (int i = 0; i < strs.length; ++i)
    {
      strs[i] = tok.nextToken().trim();
    }
    return strs;
  }
  
  /**
   * Takes a function string, stores a ref to it and parses it into the
   * function map.  Currently the parsing only supports rights in the 
   * string.
   *
   * Format to use for right strings:
   *
   *   <FUNC1> = <ID1>, <ID2>; <FUNC2> = <ID1>, <ID2>, <ID3>; . . . etc.
   */
  public void setFunctionStr(String functionStr)
  {
    this.functionStr = functionStr;
    functions = new HashMap();
    
    if (functionStr == null) return;
    
    // split into function chunks
    String[] funcStrs = split(functionStr,";");
    for (int fIdx = 0; fIdx < funcStrs.length; ++fIdx)
    {
      // split function chunk into function name/values pair
      String[] funcPair = split(funcStrs[fIdx],"=");
      
      // make sure a valid split occurred
      if (funcPair.length == 2)
      {
        // split values into individual right num strings
        String[] rightNums = split(funcPair[1],",");
      
        // build a permissions object with allowed right nums
        Permissions allowed = new Permissions();
        for (int rIdx = 0; rIdx < rightNums.length; ++rIdx)
        {
          try
          {
            allowed.allowRight(Long.parseLong(rightNums[rIdx]));
          }
          catch (Exception e)
          {
            log.error("Error parsing right id " + rightNums[rIdx] + ": " + e);
          }
        }
      
        // put the function's permissions into the functions map
        functions.put(funcPair[0],allowed);
      }
      else
      {
        log.error("Error parsing function string '" + funcStrs[fIdx] + "'");
      }
    }
  }
  
  public String getFunctionStr()
  {
    return functionStr;
  }
  
  public Permissions getPermissions(String funcName)
  {
    return (Permissions)functions.get(funcName);
  }
  
  /**
   * Determines if user has permission for the given function name.
   *
   * NOTE: If function name is not mapped then permission is assumed to be
   *       granted -- thus setting the function string to null effectively
   *       grants permission to all users for all functions.
   */
  public boolean hasPermissions(UserBean user, String funcName)
  {
    Permissions permissions = getPermissions(funcName);
    if (permissions != null)
    {
      return permissions.hasPermissions(user);
    }
    return true;
  }
  
  public String toString()
  {
    StringBuffer buf = new StringBuffer("Function Map: { ");
    buf.append("Function Str = '" + functionStr + "', ");
    int cnt = 0;
    for (Iterator i = functions.keySet().iterator(); i.hasNext(); ++cnt)
    {
      String funcName = (String)i.next();
      buf.append((cnt > 0 ? ", " : "") + funcName + " -> " 
        + functions.get(funcName));
    }
    buf.append(" }");
    return buf.toString();
  }
}