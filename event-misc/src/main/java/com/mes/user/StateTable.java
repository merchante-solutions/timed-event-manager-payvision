/*@lineinfo:filename=StateTable*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/StateTable.sqlj $

  Description:  
  
    StateTable object.
    
    Loads list of 2 digit state codes from countrystate table.  
    StateTable provides iterator methods to allow iteration over
    the records.


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 11/05/01 4:17p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.user;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class StateTable extends SQLJConnectionBase
{
  private Vector  states            = new Vector();
  private int     currentElement    = 0;

  /*
  ** CONSTRUCTOR
  **
  ** Loads the contents of countrystate.
  */
  public StateTable()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:57^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  countrystate_code
//          from    countrystate
//          order by countrystate_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  countrystate_code\n        from    countrystate\n        order by countrystate_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.StateTable",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.StateTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:62^7*/
      
      rs = it.getResultSet();
      while(rs.next())
      {
        states.add(rs.getString(1));
      }
      
      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("StateTable() constructor", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD public String getFirst()
  **
  ** Sets currentElement to 0.
  **
  ** RETURNS: first element in vector if it exists, else null.
  */
  public String getFirst()
  {
    currentElement = 0;
    return getCurrent();
  }
  
  /*
  ** METHOD public String getCurrent()
  **
  ** RETURNS: element at currentElement, null if it does not exist.
  */
  public String getCurrent()
  {
    String foundElement = null;
    if (currentElement < states.size())
    {
      foundElement = (String)states.elementAt(currentElement);
    }
    return foundElement;
  }
  
  /*
  ** METHOD public String getNext()
  **
  ** Attempts to get the next available right record from the set.
  **
  ** RETURNS: the next record if found, else null.
  */
  public String getNext()
  {
    ++currentElement;
    return getCurrent();
  }
}/*@lineinfo:generated-code*/