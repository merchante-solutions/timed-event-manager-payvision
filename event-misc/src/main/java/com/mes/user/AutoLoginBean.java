/*@lineinfo:filename=AutoLoginBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: $

  Description:  
  
  AutoLogiBean
  
  Class to encapsulate user manipulation within the TriCipher TACS appliance

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-07 18:09:23 -0800 (Wed, 07 Mar 2007) $
  Version            : $Revision: 13522 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.user;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.HttpHelper;

public class AutoLoginBean extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(AutoLoginBean.class);
  
  private String  loginName   = "";
  private String  password    = "";
  private String  incomingIp  = "";
  
  public AutoLoginBean()
  {
  }
  
  public AutoLoginBean(HttpServletRequest request)
  {
    try
    {
      loginName = HttpHelper.getString(request, "login");
      password  = HttpHelper.getString(request, "password");
      incomingIp  = request.getHeader("WL-Proxy-Client-IP");
    }
    catch(Exception e)
    {
      logEntry("AutoLoginBean(request, response)", e.toString());
    }
  }
  
  public boolean canLogin()
  {
    boolean result = false;
    
    try
    {
      connect();
      
      // check for allowed ip address and correct password
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:73^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(alai.ip_address)
//          
//          from    users u,
//                  t_hierarchy th,
//                  auto_login_allowed_ip alai
//          where   u.login_name = :loginName and
//                  u.hierarchy_node = th.descendent and
//                  th.ancestor = alai.hierarchy_node and
//                  alai.ip_address = :incomingIp and
//                  u.password_enc = encrypt_password(:password)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(alai.ip_address)\n         \n        from    users u,\n                t_hierarchy th,\n                auto_login_allowed_ip alai\n        where   u.login_name =  :1  and\n                u.hierarchy_node = th.descendent and\n                th.ancestor = alai.hierarchy_node and\n                alai.ip_address =  :2  and\n                u.password_enc = encrypt_password( :3 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.AutoLoginBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setString(2,incomingIp);
   __sJT_st.setString(3,password);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:85^7*/
      
      if( recCount > 0 )
      {
        result = true;
      }
      
      // support VPN auto-login
      if( incomingIp.substring(0, 7).equals(MesDefaults.getString(MesDefaults.AUTO_LOGIN_IP_PREFIX)) )
      {
        result = true;
      }
    }
    catch(Exception e)
    {
      logEntry("canLogin(" + loginName + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( result );
  }
}/*@lineinfo:generated-code*/