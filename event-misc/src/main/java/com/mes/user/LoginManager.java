/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/UserBean.sqlj $

  Description:

  UserBean

  Allows validation of user id's and passwords and gives access to various
  user attributes such as user type, user group membership and user rights.

  Replaces UserValidateBean.  Contains legacy support for that older class.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/19/04 10:38a $
  Version            : $Revision: 63 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.user;

import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import javax.servlet.http.HttpSession;
import com.mes.support.DateTimeFormatter;
import com.mes.support.SyncLog;

public class LoginManager
{
  private static LoginManager         singleton = null;
  private static Hashtable            logins    = null;
  
  /*
  ** INSTANCE METHODS 
  */
  public LoginManager()
  {
    logins = new Hashtable();
  }
  
  private synchronized boolean getSortedVector(Vector outList)
  {
    boolean result = false;
    try
    {
      TreeSet   sortedRows  = new TreeSet(new LoginDataComparator());
      
      sortedRows.addAll(logins.values());
      
      // now get out the results into a vector
      Iterator it = sortedRows.iterator();
      
      while(it.hasNext())
      {
        outList.add(new ViewLoginData((LoginData)(it.next())));
      }
      
      result = true;
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::getSortedVector()", e.toString());
    }
    return result;
  }
  
  private synchronized void putLogin(UserBean user, HttpSession session, String address)
  {
    try
    {
      if(user != null && session != null)
      {
        LoginData ld = new LoginData(user, session, address);
        
        logins.put(ld.sessionId, ld);
      }
    }
    catch(Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName() + "::put()", e.toString());
    }
  }
  
  private synchronized void remove(String sessionId)
  {
    if(logins != null)
    {
      logins.remove(sessionId);
    }
  }
  
  /*
  ** STATIC METHODS
  */
  public static void addLogin(UserBean user, HttpSession session, String address)
  {
    try
    {
      if(singleton == null)
      {
        singleton = new LoginManager();
      }
      
      singleton.putLogin(user, session, address);
    }
    catch(Exception e)
    {
      SyncLog.LogEntry("com.mes.user.LoginManager::addLogin(" + user.getLoginName() + ")", e.toString());
    }
  }
  
  public static void removeLogin(String sessionId)
  {
    try
    {
      if(singleton != null)
      {
        singleton.remove(sessionId);
      }
    }
    catch(Exception e)
    {
      SyncLog.LogEntry("com.mes.user.LoginManager::removeLogin(" + sessionId + ")", e.toString());
    }
  }

  public static boolean getSortedLogins(Vector outList)
  {
    boolean result = false;
    
    if(singleton != null && outList != null)
    {
      synchronized(singleton)
      {
        result = singleton.getSortedVector(outList);
      }
    }
    
    return result;
  }
  
  public class ViewLoginData
  {
    public String dateCreated   = "";
    public String lastAccessed  = "";
    public String lastResource  = "";
    public String idleTime      = "";
    public String userName      = "";
    public String loginName     = "";
    public String userLink      = "";
    public String hierarchyNode = "";
    public String nodeLink      = "";
    public String address       = "";
    
    public ViewLoginData(LoginData ld)
    {
      if(ld != null)
      {
        dateCreated   = ld.getDateCreatedString();
        lastAccessed  = ld.getLastAccessedString();
        lastResource  = ld.getLastResource();
        idleTime      = ld.getIdleTimeString();
        userName      = ld.getUser().getUserName();
        hierarchyNode = Long.toString(ld.getUser().getHierarchyNode());
        loginName     = ld.getUser().getLoginName();
        if(ld.getUser().getMerchId() != -1)
        {
          userLink    = "/jsp/maintenance/view_account.jsp?merchant=" + hierarchyNode + "&action=1";
          nodeLink    = userLink;
        }
        else
        {
          userLink    = "/jsp/credit/contact_info.jsp?userName=" + loginName;
          nodeLink    = "/jsp/maintenance/view_node.jsp?node=" + hierarchyNode;
        }
        address       = ld.getAddress();
      }
    }
  }

  public class LoginData
  {
    private Date      dateCreated       = null;
    private String    sessionId         = "";
    private UserBean  user              = null;
    private String    address           = "";

    public LoginData()
    {
    }

    public LoginData(UserBean user, HttpSession session, String address)
    {
      this.dateCreated = new Date(session.getCreationTime());
      this.sessionId = session.getId().substring(0, session.getId().indexOf('!'));
      this.user = user;
      this.address = address;
    }

    public synchronized String getSessionId()
    {
      return this.sessionId;
    }

    public synchronized UserBean getUser()
    {
      return this.user;
    }

    public synchronized String getAddress()
    {
      return this.address;
    }

    public synchronized String getDateCreatedString()
    {
      String result = "";

      try
      {
        result = DateTimeFormatter.getFormattedDate(dateCreated, DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT);
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getDateCreated()", e.toString());
      }

      return result;
    }

    public synchronized Date getDateCreated()
    {
      return dateCreated;
    }

    public synchronized Date getLastAccessed()
    {
      return user.getLastAccessed();
    }

    public synchronized String getLastAccessedString()
    {
      return user.getLastAccessedString();
    }

    public synchronized String getLastResource()
    {
      return user.getLastResource();
    }

    public synchronized String getIdleTimeString()
    {
      StringBuffer  result      = new StringBuffer("");
      long          idleTime    = 0L;
      long          hours       = 0L;
      long          minutes     = 0L;
      long          seconds     = 0L;

      try
      {
        idleTime = System.currentTimeMillis() - user.getLastAccessed().getTime();

        result.append( DateTimeFormatter.getFormattedTimestamp(idleTime) );
      }
      catch(Exception e)
      {
        result.append(e.toString());
      }

      return result.toString();
    }
  }

  public class LoginDataComparator implements Comparator
  {
    public LoginDataComparator()
    {
    }

    private String getCompareString(LoginData ld)
    {
      String result = "";

      try
      {
        result = DateTimeFormatter.getFormattedDate(ld.getLastAccessed(), "yyyyMMddHHmmss");
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getCompareString()", e.toString());
      }

      return result;
    }

    private int doCompare(LoginData o1, LoginData o2)
    {
      int       result    = 0;

      String compareString1 = getCompareString(o1);
      String compareString2 = getCompareString(o2);

      result = compareString1.compareTo(compareString2);

      return result;
    }

    private boolean doEquals(LoginData o1, LoginData o2)
    {
      boolean result = false;

      result = o1.sessionId.equals(o2.sessionId);

      return result;
    }

    public int compare(Object o1, Object o2)
    {
      int result = 0;

      try
      {
        result = doCompare((LoginData)o1, (LoginData)o2);
      }
      catch(Exception e)
      {
      }

      return result;
    }

    public boolean equals(Object o1, Object o2)
    {
      boolean result = false;

      try
      {
        result = doEquals((LoginData)o1, (LoginData)o2);
      }
      catch(Exception e)
      {
      }

      return result;
    }
  }
}