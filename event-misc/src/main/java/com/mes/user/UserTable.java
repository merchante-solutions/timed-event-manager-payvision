/*@lineinfo:filename=UserTable*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/UserTable.sqlj $

  Description:  
  
    UserTable.
    
    Loads user records in users into an ordered set.  Records may then be 
    retrieved by userId.  Also the set may be iterated over.
    
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/06/03 3:48p $
  Version            : $Revision: 13 $

  Change History:
     See VSS database

  Copyuser (C) 2000,2001 by Merchant e-Solutions Inc.
  All users reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Vector;
import com.mes.constants.MesUsers;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;


public class UserTable extends SQLJConnectionBase
{
  public class UserRecordComparator implements Comparator
  {
    public int compare(Object o1, Object o2)
    {
      if (o1.getClass() != UserRecord.class || o2.getClass() != UserRecord.class)
      {
        throw new ClassCastException(
          "Attempted to compare class type other than UserRecord");
      }
    
      long ur1 = ((UserRecord)o1).getUserId();
      long ur2 = ((UserRecord)o2).getUserId();
      int result = 0;
    
      if (ur1 < ur2)
      {
        result = -1;
      }
      else if (ur1 > ur2)
      {
        result = 1;
      }
    
      return result;
    }
  
    public boolean equals(Object o)
    {
      boolean result = false;
    
      if (o.getClass() == this.getClass())
      {
        result = true;
      }
      return result;
    }
  }

  // filter setup
  public final static int   UF_NONE             = 0;
  public final static int   UF_GROUP            = 1;
  public final static int   UF_TYPE             = 2;
  public final static int   UF_NAME             = 3;
  
  private int               filterType          = UF_GROUP;
  private long              groupFilter         = 0L;
  private long              typeFilter          = MesUsers.USER_MES;
  private String            nameFilter          = "";
  private String            submitVal           = "";
  
  // set storage and iteration
  private Vector            users               = new Vector();
  private int               currentIndex        = 0;

  /*
  ** CONSTRUCTOR public UserTable()
  */
  public UserTable()
  {
    init();
  }  

  /*
  ** METHOD public void init()
  **
  ** Resets filtering back to default values.
  */
  public void init()
  {
    filterType = UF_NAME;
    //groupFilter = MesUsers.GROUP_MES;
  }
  
  /*
  ** METHOD protected boolean getData()
  **
  ** Builds a query based on the filter type and executes it.  The results
  ** are loaded into the set in the form of UserRecord objects.
  **
  ** RETURNS: true if successful.
  */
  public boolean getData()
  {
    boolean             getOk             = false;
    ResultSetIterator   it                = null;
    ResultSet           rs                = null;
    String              filterDescription = "";
    
    try
    {
      connect();
    
      switch (filterType)
      {
        case UF_NONE:
        default:
          filterDescription = "no filtering";
          
          /*@lineinfo:generated-code*//*@lineinfo:143^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//              from    users
//              order by login_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n            from    users\n            order by login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.UserTable",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.UserTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:148^11*/
          break;
        
        case UF_GROUP:
          filterDescription = "group filter by " + Long.toString(groupFilter);
          /*@lineinfo:generated-code*//*@lineinfo:153^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  u.*
//              from    users u,
//                      user_to_group u2g
//              where   (u.user_id = u2g.user_id or u.type_id = u2g.user_id) and
//                      u2g.group_id = :groupFilter
//              order by login_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  u.*\n            from    users u,\n                    user_to_group u2g\n            where   (u.user_id = u2g.user_id or u.type_id = u2g.user_id) and\n                    u2g.group_id =  :1 \n            order by login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.user.UserTable",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,groupFilter);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.user.UserTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:161^11*/
          break;
          
        case UF_TYPE:
          filterDescription = "type filter by " + Long.toString(typeFilter);
          /*@lineinfo:generated-code*//*@lineinfo:166^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//              from    users
//              where   type_id = :typeFilter
//              order by login_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n            from    users\n            where   type_id =  :1 \n            order by login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.user.UserTable",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,typeFilter);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.user.UserTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:172^11*/
          break;
          
        case UF_NAME:
          filterDescription = "name filter by " + nameFilter;
          /*@lineinfo:generated-code*//*@lineinfo:177^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//              from    users
//              where   login_name = :nameFilter
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n            from    users\n            where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.user.UserTable",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,nameFilter);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.user.UserTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:182^11*/
          break;
      }
      
      rs = it.getResultSet();
      
      while (rs.next())
      {
        add(new UserRecord(rs));
      }
      
      rs.close();
      it.close();
      
      if (users.size() != 0)
      {
        getOk = true;
      }
    }
    catch (Exception e)
    {
      logEntry("get(" + filterDescription + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
          
    return getOk;
  }

  /*
  ** METHOD public UserRecord get(long userId)
  **
  ** Looks for the UserRecord corresponding with the given userId
  ** in the user record set.
  **
  ** RETURNS: UserRecord if found, else null.
  */
  public UserRecord get(long userId)
  {
    UserRecord ur = null;
    boolean foundIt = false;
    for (int i = 0; i < users.size() && !foundIt; ++i)
    {
      ur = (UserRecord)users.elementAt(i);
      if (ur.getUserId() == userId)
      {
        foundIt = true;
      }
    }
    
    if (foundIt)
    {
      return ur;
    }
    
    return null;
  }
  
  /*
  ** METHOD public void clear()
  **
  ** Clears the record set.
  */
  public void clear()
  {
    users.clear();
  }

  /*
  ** MERHOD public boolean add(UserRecord ur)
  **
  ** Adds the user record to the record set.
  **
  ** RETURNS: true if successfully added, else false (record already in set)
  */
  public boolean add(UserRecord ur)
  {
    return users.add(ur);
  }
  
  /*
  ** METHOD public boolean contains(UserRecord ur)
  **
  ** RETURNS: true if user record is in the record set, else false.
  */
  public boolean contains(UserRecord ur)
  {
    return users.contains(ur);
  }
  public boolean contains(long userId)
  {
    return contains(new UserRecord(userId, Ctx));
  }
  
  /*
  ** METHOD public boolean remove(UserRecord ur)
  **
  ** Removes the user record from the record set.
  **
  ** RETURNS: true if record was found and removed, else false.
  */
  public boolean remove(UserRecord ur)
  {
    return users.remove(ur);
  }
  public boolean remove(long userId)
  {
    return remove(new UserRecord(userId, Ctx));
  }
  
  /*
  ** METHOD public UserRecord getFirst()
  **
  ** RETURNS: the first record in the set, or null if empty set.
  */
  public UserRecord getFirst()
  {
    currentIndex = 0;
    return getCurrent();
  }
  
  /*
  ** METHOD public UserRecord getCurrent()
  **
  ** RETURNS: currentRecord (which may be null).
  */
  public UserRecord getCurrent()
  {
    return (users.size() > currentIndex ? (UserRecord)users.elementAt(currentIndex) : null);
  }
  
  /*
  ** METHOD public UserRecord getNext()
  **
  ** Attempts to get the next available user record from the set.
  **
  ** RETURNS: the next record if found, else null.
  */
  public UserRecord getNext()
  {
    ++currentIndex;
    return getCurrent();
  }
  
  /*
  ** ACCESSORS
  */
  public int getFilterType()
  {
    return filterType;
  }
  public void setFilterType(String filterType)
  {
    try
    {
      setFilterType(Integer.parseInt(filterType));
    }
    catch(Exception e)
    {
      logEntry("setFilterType(" + filterType + ")", e.toString());
    }
  }
  public void setFilterType(int newFilterType)
  {
    filterType = newFilterType;
  }
  
  public long getGroupFilter()
  {
    return groupFilter;
  }
  public void setGroupFilter(String groupFilter)
  {
    try
    {
      setGroupFilter(Long.parseLong(groupFilter));
    }
    catch(Exception e)
    {
      logEntry("setGroupFilter(" + groupFilter + ")", e.toString());
    }
  }
  public void setGroupFilter(long newGroupFilter)
  {
    groupFilter = newGroupFilter;
  }
  
  public long getTypeFilter()
  {
    return typeFilter;
  }
  public void setTypeFilter(String typeFilter)
  {
    try
    {
      setTypeFilter(Long.parseLong(typeFilter));
    }
    catch(Exception e)
    {
      logEntry("setTypeFilter(" + typeFilter + ")", e.toString());
    }
  }
  public void setTypeFilter(long newTypeFilter)
  {
    typeFilter = newTypeFilter;
  }
  
  public String getNameFilter()
  {
    return nameFilter;
  }
  public void setNameFilter(String newNameFilter)
  {
    nameFilter = newNameFilter;
  }
  
  public String getSubmitVal()
  {
    return submitVal;
  }
  public void setSubmitVal(String newVal)
  {
    submitVal = newVal;
  }
}/*@lineinfo:generated-code*/