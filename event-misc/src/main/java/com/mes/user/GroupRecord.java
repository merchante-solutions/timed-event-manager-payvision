/*@lineinfo:filename=GroupRecord*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/GroupRecord.sqlj $

  Description:  
  
    GroupRecord.
    
    Contains a user_groups record.  Allows insertion, modification and
    deletion of a record.
    
  Last Modified By   : $
  Last Modified Date : $
  Version            : $

  Change History:
     See VSS database

  Copygroup (C) 2000,2001 by Merchant e-Solutions Inc.
  All groups reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.sql.ResultSet;
import java.util.Iterator;
import sqlj.runtime.ResultSetIterator;

public class GroupRecord extends DBBean
{
  // record fields
  private Group               group       = null;
  private String              name        = "";
  private String              description = "";
  private RightSet            rightSet    = new RightSet();
  
  /*
  ** CONSTRUCTOR GroupRecord()
  */
  public GroupRecord()
  {
  }

  /*
  ** CONSTRUCTOR GroupRecord(ResultSet rs)
  **
  ** Initializes a group record with the contents of a result set.
  */
  public GroupRecord(ResultSet rs)
  {
    loadResultSet(rs);
  }
  
  /*
  ** METHOD public void loadResultSet(ResultSet rs)
  **
  ** Loads the contents of a ResultSet into internal field storage.
  */
  public void loadResultSet(ResultSet rs)
  {
    try
    {
      group       = new Group(rs.getLong("group_id"));
      name        = processString(rs.getString("name"));
      description = processString(rs.getString("description"));
    }
    catch (Exception e)
    {
      logEntry("loadResultSet()", e.toString());
    }
  }
  
  /*
  ** METHOD protected boolean get()
  **
  ** Loads a group record from the user_groups table.
  **
  ** RETURNS: true if user record successfully loaded, else false.
  */
  protected boolean get()
  {
    boolean             getOk   = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             found   = false;
    
    try
    {
      // look up user record in users table by login name
      /*@lineinfo:generated-code*//*@lineinfo:98^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    user_groups
//          where   group_id = :group.getGroupId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_84 = group.getGroupId();
  try {
   String theSqlTS = "select  *\n        from    user_groups\n        where   group_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.GroupRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_84);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.GroupRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:103^7*/
      
      rs = it.getResultSet();
      
      if (rs.next())
      {
        // load the record into internal storage
        loadResultSet(rs);
        
        found = true;
      }
      
      it.close();
       
      if(found)
      { 
        // look up right associations
        /*@lineinfo:generated-code*//*@lineinfo:120^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  right_id
//            from    user_group_to_right
//            where   group_id = :group.getGroupId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_85 = group.getGroupId();
  try {
   String theSqlTS = "select  right_id\n          from    user_group_to_right\n          where   group_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.user.GroupRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_85);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.user.GroupRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^9*/
        
        rightSet.clear();
        rs = it.getResultSet();
        
        while (rs.next())
        {
          rightSet.add(new Right(rs.getLong("right_id")));
        }
        
        it.close();
          
        getOk = true;
      }
    }
    catch (Exception e)
    {
      logEntry("get()", e.toString());
    }
    
    return getOk;
  }
  
  /*
  ** METHOD public boolean getData(long getId)
  **
  ** Loads a group record with the given id from the user_groups table.
  **
  ** RETURNS: true if user record successfully loaded, else false.
  */
  public boolean getData(long getId)
  {
    setGroupId(getId);
    return getData();
  }
  
  /*
  ** METHOD protected boolean submit()
  **
  ** Inserts or updates a group record.
  **
  ** RETURNS: true if submit successful, else false.
  */
  protected boolean submit()
  {
    boolean   submitOk  = false;
    
    try
    {
      int count = 0;
      /*@lineinfo:generated-code*//*@lineinfo:175^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(group_id) 
//          from    user_groups
//          where   group_id = :group.getGroupId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_86 = group.getGroupId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(group_id)  \n        from    user_groups\n        where   group_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.user.GroupRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_86);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:180^7*/
      
      if(count > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:184^9*/

//  ************************************************************
//  #sql [Ctx] { update  user_groups
//            set     name        = :name,
//                    description = :description
//            where   group_id    = :group.getGroupId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_87 = group.getGroupId();
   String theSqlTS = "update  user_groups\n          set     name        =  :1 ,\n                  description =  :2 \n          where   group_id    =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.user.GroupRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,name);
   __sJT_st.setString(2,description);
   __sJT_st.setLong(3,__sJT_87);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:190^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:194^9*/

//  ************************************************************
//  #sql [Ctx] { insert into user_groups
//            (
//              name,
//              description,
//              group_id
//            )
//            values 
//            (
//              :name,
//              :description,
//              :group.getGroupId()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_88 = group.getGroupId();
   String theSqlTS = "insert into user_groups\n          (\n            name,\n            description,\n            group_id\n          )\n          values \n          (\n             :1 ,\n             :2 ,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.user.GroupRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,name);
   __sJT_st.setString(2,description);
   __sJT_st.setLong(3,__sJT_88);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:208^9*/
      }
      
      // clear old right associations
      /*@lineinfo:generated-code*//*@lineinfo:212^7*/

//  ************************************************************
//  #sql [Ctx] { delete from user_group_to_right
//          where       group_id = :group.getGroupId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_89 = group.getGroupId();
   String theSqlTS = "delete from user_group_to_right\n        where       group_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.user.GroupRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_89);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:216^7*/
        
      // insert new right associations
      Iterator  rIter = rightSet.getRightIterator();
      Right right = null;
      
      while(rIter.hasNext())
      {
        right = (Right)rIter.next();
        
        /*@lineinfo:generated-code*//*@lineinfo:226^9*/

//  ************************************************************
//  #sql [Ctx] { insert into user_group_to_right
//            (
//              group_id,
//              right_id
//            )
//            values
//            (
//              :group.getGroupId(),
//              :right.getRightId()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_90 = group.getGroupId();
 long __sJT_91 = right.getRightId();
   String theSqlTS = "insert into user_group_to_right\n          (\n            group_id,\n            right_id\n          )\n          values\n          (\n             :1 ,\n             :2 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.user.GroupRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_90);
   __sJT_st.setLong(2,__sJT_91);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:238^9*/
      }
      
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submit()", e.toString());
    }
    
    return submitOk;
  }
  
  /*
  ** METHOD protected boolean validate()
  **
  ** Validates group data.
  **
  ** RETURNS: true if data is valid, else false.
  */
  protected boolean validate()
  {
    return true;
  }
  
  /*
  ** METHOD public boolean delete()
  **
  ** Deletes the current record from the database.
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean delete()
  {
    boolean deleteOk = false;
    
    try
    {
      // delete group to right records
      /*@lineinfo:generated-code*//*@lineinfo:277^7*/

//  ************************************************************
//  #sql [Ctx] { delete from user_group_to_right
//          where       group_id = :group.getGroupId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_92 = group.getGroupId();
   String theSqlTS = "delete from user_group_to_right\n        where       group_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.user.GroupRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_92);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:281^7*/
      
      // delete user to group records
      /*@lineinfo:generated-code*//*@lineinfo:284^7*/

//  ************************************************************
//  #sql [Ctx] { delete from user_to_group
//          where       group_id = :group.getGroupId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_93 = group.getGroupId();
   String theSqlTS = "delete from user_to_group\n        where       group_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.user.GroupRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_93);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:288^7*/
      
      // delete group record
      /*@lineinfo:generated-code*//*@lineinfo:291^7*/

//  ************************************************************
//  #sql [Ctx] { delete from user_groups
//          where       group_id = :group.getGroupId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_94 = group.getGroupId();
   String theSqlTS = "delete from user_groups\n        where       group_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.user.GroupRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_94);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:295^7*/
      
      deleteOk = true;
    }
    catch (Exception e)
    {
      logEntry("delete()", e.toString());
    }

    return deleteOk;
  }
  
  /*
  ** ACCESSORS
  */
  public Group getGroup()
  {
    return group;
  }
  public void setGroup(Group newGroup)
  {
    group = newGroup;
  }
  
  public long getGroupId()
  {
    if (group != null)
    {
      return group.getGroupId();
    }
    return 0L;
  }
  public void setGroupId(String newGroupId)
  {
    try
    {
      setGroupId(Long.parseLong(newGroupId));
    }
    catch(Exception e)
    {
      logEntry("setNewGroupid(" + newGroupId + ")", e.toString());
    }
  }
  public void setGroupId(long newGroupId)
  {
    group = new Group(newGroupId);
  }
  
  public String getName()
  {
    return name;
  }
  public void setName(String newName)
  {
    name = newName;
  }
  
  public void setDescription(String newDescription)
  {
    description = newDescription;
  }
  public String getDescription()
  {
    return description;
  }
  
  public void setRightCheck(String[] rightIds)
  {
    for (int i = 0; i < java.lang.reflect.Array.getLength(rightIds); ++i)
    {
      rightSet.add(new Right(Long.parseLong(rightIds[i])));
    }
  }
  
  public RightSet getRightSet()
  {
    return rightSet;
  }
}/*@lineinfo:generated-code*/