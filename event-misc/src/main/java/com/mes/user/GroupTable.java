/*@lineinfo:filename=GroupTable*//*@lineinfo:user-code*//*@lineinfo:1^1*/ /*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/GroupTable.sqlj $

  Description:  
  
    GroupTable.
    
    Loads all right records in user_rights into an ordered set upon 
    instantiation.  Records may then be retrieved by right value or
    index.  Also the set may be iterated over.
    
    
  Last Modified By   : $
  Last Modified Date : $
  Version            : $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class GroupTable extends SQLJConnectionBase
{
  public class GroupRecordComparator implements Comparator
  {
    public int compare(Object o1, Object o2)
    {
      GroupRecord groupRec = new GroupRecord();
      if (o1.getClass() != groupRec.getClass() || o2.getClass() != groupRec.getClass())
      {
        throw new ClassCastException(
          "Attempted to compare class type other than GroupRecord");
      }
    
      GroupRecord r1 = (GroupRecord)o1;
      GroupRecord r2 = (GroupRecord)o2;
      int result = r1.getName().compareTo(r2.getName());
      if (result == 0)
      {
        if (r1.getGroupId() < r2.getGroupId())
        {
          result = -1;
        }
        else if (r1.getGroupId() > r2.getGroupId())
        {
          result = 1;
        }
      }
      
      return result;
    }
  
    public boolean equals(Object o)
    {
      boolean result = false;
    
      if (o.getClass() == this.getClass())
      {
        result = true;
      }
      return result;
    }
  }

  protected Set             groupSet        = new TreeSet(new GroupRecordComparator());
  protected Iterator        iter            = groupSet.iterator();
  protected GroupRecord     currentRecord   = null;
  
  /*
  ** CONSTRUCTOR
  **
  ** Loads the contents of user_groups.
  */
  public GroupTable()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    try
    {
      connect();
      
      // load the contents of user_groups
      /*@lineinfo:generated-code*//*@lineinfo:105^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    user_groups
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    user_groups";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.GroupTable",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.GroupTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:109^7*/
      
      rs = it.getResultSet();
      
      while (rs.next())
      {
        add(new GroupRecord(rs));
      }

      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("GroupTable constructor: ", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD public GroupRecord get(long groupId)
  **
  ** Looks for the GroupRecord corresponding with the given group id
  ** in the group record set.
  **
  ** RETURNS: GroupRecord if found, else null.
  */
  public GroupRecord get(long groupId)
  {
    GroupRecord groupRec = null;
    boolean foundIt = false;
    for (Iterator i = groupSet.iterator(); i.hasNext() && !foundIt;)
    {
      groupRec = (GroupRecord)(i.next());
      if (groupRec.getGroupId() == groupId)
      {
        foundIt = true;
      }
    }
    
    if (foundIt)
    {
      return groupRec;
    }
    
    return null;
  }
  
  /*
  ** METHOD public void clear()
  **
  ** Clears the record set.
  */
  public void clear()
  {
    groupSet.clear();
  }

  /*
  ** MERHOD public boolean add(GroupRecord groupRec)
  **
  ** Adds the group record to the record set.
  **
  ** RETURNS: true if successfully added, else false (record already in set)
  */
  public boolean add(GroupRecord groupRec)
  {
    return groupSet.add(groupRec);
  }
  
  /*
  ** METHOD public boolean contains(GroupRecord groupRec)
  **
  ** RETURNS: true if group record is in the record set, else false.
  */
  public boolean contains(GroupRecord groupRec)
  {
    return groupSet.contains(groupRec);
  }
  
  /*
  ** METHOD public boolean remove(GroupRecord groupRec)
  **
  ** Removes the group record from the record set.
  **
  ** RETURNS: true if record was found and removed, else false.
  */
  public boolean remove(GroupRecord groupRec)
  {
    return groupSet.remove(groupRec);
  }
  
  /*
  ** METHOD public GroupRecord getFirst()
  **
  ** Initializes internal iterator to the first item in the ordered set then
  ** sets the currentRecord equal to the first record.
  **
  ** RETURNS: the first record in the set, or null if empty set.
  */
  public GroupRecord getFirst()
  {
    iter = groupSet.iterator();
    currentRecord = null;
    
    if (iter.hasNext())
    {
      currentRecord = (GroupRecord)iter.next();
    }
    
    return currentRecord;
  }
  
  /*
  ** METHOD public GroupRecord getCurrent()
  **
  ** RETURNS: currentRecord (which may be null).
  */
  public GroupRecord getCurrent()
  {
    return currentRecord;
  }
  
  /*
  ** METHOD public GroupRecord getNext()
  **
  ** Attempts to get the next available group record from the set.
  **
  ** RETURNS: the next record if found, else null.
  */
  public GroupRecord getNext()
  {
    currentRecord = null;
    if (iter != null && iter.hasNext())
    {
      currentRecord = (GroupRecord)iter.next();
    }
    return currentRecord;
  }
}/*@lineinfo:generated-code*/