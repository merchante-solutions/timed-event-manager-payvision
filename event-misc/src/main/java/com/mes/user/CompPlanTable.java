/*@lineinfo:filename=CompPlanTable*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/CompPlanTable.java $

  Description:  
  
    CompPlanTable object.
    
    Loads compensation plan list from rep_plan table.


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/30/01 8:49p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.user;

import java.sql.ResultSet;
import com.mes.support.SyncLog;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class CompPlanTable extends DropDownTable
{
  /*
  ** CONSTRUCTOR public CompPlanTable()
  */
  public CompPlanTable()
  {
    getData();
  }
  
  /*
  ** METHOD protected boolean getData()
  **
  ** Loads the compensation plan list.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean getData()
  {
    boolean             getOk   = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:67^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rep_plan_id,
//                  rep_plan_desc
//          from    rep_plan
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rep_plan_id,\n                rep_plan_desc\n        from    rep_plan";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.CompPlanTable",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.CompPlanTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:72^7*/
      
      rs = it.getResultSet();
      
      while (rs.next())
      {
        addElement(rs);
        getOk = true;
      }
      
      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName(), 
        "getData: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return getOk;
  }
}/*@lineinfo:generated-code*/