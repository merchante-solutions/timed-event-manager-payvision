/*@lineinfo:filename=UserSessionLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/UserSessionLoader.sqlj $

  Description:  

  UserBean
  
  Allows validation of user id's and passwords and gives access to various 
  user attributes such as user type, user group membership and user rights.
  
  Replaces UserValidateBean.  Contains legacy support for that older class.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/23/03 5:19p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.user;

import javax.servlet.http.HttpServletRequest;
import com.mes.database.SQLJConnectionBase;


public class UserSessionLoader extends SQLJConnectionBase
{
  String              requestedSession    = null;
  
  public UserSessionLoader(String sessionId)
  {
    // establish requested Session which is everything before the first '!' in the string
    if(sessionId != null)
    {
      requestedSession = sessionId.substring(0, sessionId.indexOf('!'));
    }
  }
  
  private String getSessionUserId()
  {
    String result = null;
    
    try
    {
      connect();
      
      int sessionCount    = 0;
      
      // see if session exists with this session id
      /*@lineinfo:generated-code*//*@lineinfo:65^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(session_id)
//          
//          from    user_sessions
//          where   session_id = :requestedSession
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(session_id)\n         \n        from    user_sessions\n        where   session_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.UserSessionLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,requestedSession);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sessionCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:71^7*/
      
      if(sessionCount > 0)
      {
        // get login name
        /*@lineinfo:generated-code*//*@lineinfo:76^9*/

//  ************************************************************
//  #sql [Ctx] { select  login_name
//            
//            from    user_sessions
//            where   session_id = :requestedSession
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  login_name\n           \n          from    user_sessions\n          where   session_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.user.UserSessionLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,requestedSession);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:82^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("getSessionUserId()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  private void persist(String loginName)
  {
    try
    {
      connect();
      
      int sessionCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:105^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(session_id)
//          
//          from    user_sessions
//          where   session_id = :requestedSession
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(session_id)\n         \n        from    user_sessions\n        where   session_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.user.UserSessionLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,requestedSession);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sessionCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:111^7*/
      
      if(sessionCount > 0)
      {
        // update this session id with current login name
        /*@lineinfo:generated-code*//*@lineinfo:116^9*/

//  ************************************************************
//  #sql [Ctx] { update  user_sessions
//            set     login_name = :loginName,
//                    login_date = sysdate
//            where   session_id = :requestedSession
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  user_sessions\n          set     login_name =  :1 ,\n                  login_date = sysdate\n          where   session_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.user.UserSessionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setString(2,requestedSession);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:122^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:126^9*/

//  ************************************************************
//  #sql [Ctx] { insert into user_sessions
//            (
//              session_id,
//              login_name,
//              login_date
//            )
//            values
//            (
//              :requestedSession,
//              :loginName,
//              sysdate
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into user_sessions\n          (\n            session_id,\n            login_name,\n            login_date\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n            sysdate\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.user.UserSessionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,requestedSession);
   __sJT_st.setString(2,loginName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:140^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("persist(" + loginName + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public static String getSessionLoginName(HttpServletRequest request)
  {
    String result = null;
    try
    {
      UserSessionLoader loader = new UserSessionLoader(request.getRequestedSessionId());
      
      result = loader.getSessionUserId();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.user.UserSessionLoader.getSessionLoginName()", e.toString());
    }
    
    return result;
  }
  
  public static void persistSessionLoginName(HttpServletRequest request, String loginName)
  {
    try
    {
      UserSessionLoader loader = new UserSessionLoader(request.getRequestedSessionId());
      
      loader.persist(loginName);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.user.UserSessionLoader.persistSessionLoginName()", e.toString());
    }
  }
}/*@lineinfo:generated-code*/