/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/Permissions.java $

  Description:  
  
    Permissions
    
    Defines a set of permissions for a resource.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/23/04 5:16p $
  Version            : $Revision: 12 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;

public class Permissions
{
  private Vector required  = new Vector();
  private Vector allowed   = new Vector();

  /*
  ** METHOD private boolean hasRequired(UserBean user)
  **
  ** RETURNS: true if all required rights, groups and user types are
  **          contained within the user record.
  */
  private boolean hasRequired(UserBean user)
  {
    Object item;
    boolean hasReqs = true;
    int curIndex = 0;
    while (curIndex < required.size() && hasReqs)
    {
      item = required.elementAt(curIndex++);
      if (item.getClass() == UserType.class)
      {
        hasReqs = user.getUserType().equals((UserType)item);
      }
      else if (item.getClass() == Group.class)
      {
        hasReqs = user.getGroups().contains((Group)item);
      }
      else if (item.getClass() == Right.class)
      {
        hasReqs = user.getRights().contains((Right)item);
      }
    }
    
    return hasReqs;
  }
  
  /*
  ** METHOD private boolean isAllowed(UserBean user)
  **
  ** RETURNS: true if any allowd right, group or user type is contained
  **          within the user bean.
  */
  private boolean isAllowed(UserBean user)
  {
    Object item;
    boolean isAllowed = (allowed.size() == 0);
    int curIndex = 0;
    while (curIndex < allowed.size() && !isAllowed)
    {
      item = allowed.elementAt(curIndex++);;
      if (item.getClass() == Right.class)
      {
        isAllowed = user.getRights().contains((Right)item);
      }
      else if (item.getClass() == Group.class)
      {
        isAllowed = user.getGroups().contains((Group)item);
      }
      else if (item.getClass() == UserType.class)
      {
        isAllowed = user.getUserType().equals((UserType)item);
      }
    }
    return isAllowed;
  }
  
  /*
  ** METHOD public boolean hasPermission(UserBean user)
  **
  ** RETURNS: true if all requirements and any allowed are met.
  */
  public boolean hasPermissions(UserBean user)
  {
    boolean result = (hasRequired(user) && isAllowed(user));
    
    return result;
  }
  
  /*
  ** METHOD hasPermissions
  **
  ** This version accepts a request object and uses it to determine the
  ** requested resource for logging purposes
  */
  public boolean hasPermissions(UserBean user, HttpServletRequest request)
  {
    boolean retVal      = false;
    String  result      = "Insufficient Access";
    
    if(hasPermissions(user))
    {
      user.logAccessDetails(request);
      retVal = true;
      result = "Success";
    }
    
    return retVal;
  }
    
  /*
  ** METHODS Requirement Adders
  **
  ** The following routines provide an interface for adding Rights and 
  ** RightsSets, Groups and GroupSets, and UserTypes to the set of
  ** required permissions.
  */
  private void addRequirement(Object addReq)
  {
    required.add(addReq);
  }
  public void require(Right right)
  {
    addRequirement(right);
  }
  public void require(Group group)
  {
    addRequirement(group);
  }
  public void require(UserType userType)
  {
    addRequirement(userType);
  }
  public void requireRight(long rightId)
  {
    addRequirement(new Right(rightId));
  }
  public void requireGroup(long groupId)
  {
    addRequirement(new Group(groupId));
  }
  public void requireUserType(long userTypeId)
  {
    addRequirement(new UserType(userTypeId));
  }
  private void requireSet(Set set)
  {
    Iterator iter = set.iterator();
    while (iter.hasNext())
    {
      addRequirement(iter.next());
    }
  }
  public void require(RightSet rights)
  {
    requireSet(rights.rightSet);
  }
  public void require(GroupSet groups)
  {
    requireSet(groups.groupSet);
  }
  
  /*
  ** METHODS Allowable Adders
  **
  ** The following routines provide an interface for adding Rights and 
  ** RightsSets, Groups and GroupSets, and UserTypes to the set of
  ** allowed permissions.
  */
  private void addAllowable(Object addReq)
  {
    allowed.add(addReq);
  }
  public void allow(Right right)
  {
    addAllowable(right);
  }
  public void allow(Group group)
  {
    addAllowable(group);
  }
  public void allow(UserType userType)
  {
    addAllowable(userType);
  }
  public void allowRight(long rightId)
  {
    addAllowable(new Right(rightId));
  }
  public void allowGroup(long groupId)
  {
    addAllowable(new Group(groupId));
  }
  public void allowUserType(long userTypeId)
  {
    addAllowable(new UserType(userTypeId));
  }
  private void allowSet(Set set)
  {
    Iterator iter = set.iterator();
    while (iter.hasNext())
    {
      addAllowable(iter.next());
    }
  }
  public void allow(RightSet rights)
  {
    allowSet(rights.rightSet);
  }
  public void allow(GroupSet groups)
  {
    allowSet(groups.groupSet);
  }
  
  /*
  ** METHOD public void clear()
  **
  ** Clears all permissions.
  */
  public void clear()
  {
    required.clear();
    allowed.clear();
  }
  
  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("Permissions: required = { ");
    int cnt = 0;
    for (Iterator i = required.iterator(); i.hasNext(); ++cnt)
    {
      buf.append((cnt > 0 ? ", " : "") + i.next());
    }
    buf.append(" }, allowed = { ");
    cnt = 0;
    for (Iterator i = allowed.iterator(); i.hasNext(); ++cnt)
    {
      buf.append((cnt > 0 ? ", " : "") + i.next());
    }
    buf.append(" }");
    return buf.toString();
  }
}
