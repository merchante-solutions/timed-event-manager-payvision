/*@lineinfo:filename=ManagerTable*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/ManagerTable.sqlj $

  Description:  
  
    ManagerTable object.
    
    Loads sales rep manager list from sales_rep table.


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 11/05/01 5:01p $
  Version            : $Revision: 10 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.user;

import java.sql.ResultSet;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class ManagerTable extends DropDownTable
{
  /*
  ** CONSTRUCTOR public ManagerTable()
  */
  public ManagerTable(long userId, int hierarchyType)
  {
    getData(userId, hierarchyType);
  }
  
  /*
  ** METHOD protected boolean getData()
  **
  ** Loads the sales reps that belong to the group given.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean getData(long userId, int hierarchyType)
  {
    boolean               getOk   = false;
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:68^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    hn.name,
//                    h1.descendent,
//                    r.rep_id
//          from      t_hierarchy_names hn,
//                    t_hierarchy       h1,
//                    t_hierarchy       h2,
//                    sales_rep         r
//          where     h1.hier_type = :hierarchyType and
//                    h1.entity_type <> 3 and
//                    h1.descendent not in
//                    (
//                      select  descendent
//                      from    t_hierarchy
//                      where   ancestor = :userId
//                    ) and
//                    h1.relation = 1 and
//                    h1.descendent = r.user_id(+) and
//                    h1.descendent = hn.hier_id and
//                    h1.descendent = h2.descendent and
//                    h2.ancestor = 123
//          order by  h2.relation, hn.name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    hn.name,\n                  h1.descendent,\n                  r.rep_id\n        from      t_hierarchy_names hn,\n                  t_hierarchy       h1,\n                  t_hierarchy       h2,\n                  sales_rep         r\n        where     h1.hier_type =  :1  and\n                  h1.entity_type <> 3 and\n                  h1.descendent not in\n                  (\n                    select  descendent\n                    from    t_hierarchy\n                    where   ancestor =  :2 \n                  ) and\n                  h1.relation = 1 and\n                  h1.descendent = r.user_id(+) and\n                  h1.descendent = hn.hier_id and\n                  h1.descendent = h2.descendent and\n                  h2.ancestor = 123\n        order by  h2.relation, hn.name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.ManagerTable",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,hierarchyType);
   __sJT_st.setLong(2,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.ManagerTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:91^7*/
      
      rs = it.getResultSet();
      
      while (rs.next())
      {
        if (rs.getString("rep_id") != null)
        {
          addElement(rs.getString("rep_id"),rs.getString("name"));
        }
        else
        {
          addElement(rs.getString("descendent"),rs.getString("name"));
        }
        getOk = true;
      }
      
      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return getOk;
  }
}/*@lineinfo:generated-code*/