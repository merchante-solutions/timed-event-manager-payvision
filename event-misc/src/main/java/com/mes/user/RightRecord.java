/*@lineinfo:filename=RightRecord*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/RightRecord.sqlj $

  Description:  
  
    RightRecord.
    
    Contains a user_rights record.  Allows insertion, modification and
    deletion of a record.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:39p $
  Version            : $Revision: 9 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

public class RightRecord extends DBBean
{
  // record fields
  private Right               right       = null;
  private String              name        = "";
  private String              description = "";
  
  /*
  ** CONSTRUCTOR RightRecord()
  */
  public RightRecord()
  {
  }
  
  /*
  ** CONSTRUCTOR RightRecord(ResultSet rs)
  **
  ** Initializes right record with contents of result set.
  */
  public RightRecord(ResultSet rs)
  {
    loadResultSet(rs);
  }

  /*
  ** METHOD public void loadResultSet(ResultSet rs)
  **
  ** Loads the contents of a ResultSet into internal field storage.
  */
  public void loadResultSet(ResultSet rs)
  {
    try
    {
      right       = new Right(rs.getLong("right"));
      name        = processString(rs.getString("name"));
      description = processString(rs.getString("description"));
    }
    catch (Exception e)
    {
      logEntry("loadResultSet()", e.toString());
    }
  }
  
  /*
  ** METHOD protected boolean get()
  **
  ** Loads a right record from the user_rights table.
  **
  ** RETURNS: true if user record successfully loaded, else false.
  */
  protected boolean get()
  {
    boolean             getOk   = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      // look up user record in users table by login name
      /*@lineinfo:generated-code*//*@lineinfo:95^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    user_rights
//          where   right = :right.getRightId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_95 = right.getRightId();
  try {
   String theSqlTS = "select  *\n        from    user_rights\n        where   right =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.RightRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_95);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.RightRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:100^7*/
      
      rs = it.getResultSet();
      
      if (rs.next())
      {
        // load the record into internal storage
        loadResultSet(rs);
        getOk = true;
      }
      
      it.close();
    }
    catch (Exception e)
    {
      logEntry("get()", e.toString());
    }
    
    return getOk;
  }
  
  /*
  ** METHOD public boolean getData(Right newRight)
  **
  ** Loads the right record corresponding with the newRight.
  **
  ** RETURNS: true if user record successfully loaded, else false.
  */
  public boolean getData(Right newRight)
  {
    setRight(newRight);
    return getData();
  }
  public boolean getData(int getId)
  {
    return getData(new Right(getId));
  }
  
  /*
  ** METHOD protected boolean submit()
  **
  ** Inserts or updates a right record.
  **
  ** RETURNS: true if submit successful, else false.
  */
  protected boolean submit()
  {
    boolean             submitOk  = false;
    
    try
    {
      int count = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:153^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(right) 
//          from    user_rights
//          where   right = :right.getRightId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_96 = right.getRightId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(right)  \n        from    user_rights\n        where   right =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.user.RightRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_96);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:158^7*/
      
      if(count > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:162^9*/

//  ************************************************************
//  #sql [Ctx] { update  user_rights
//            set     name = :name,
//                    description = :description
//            where   right = :right.getRightId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_97 = right.getRightId();
   String theSqlTS = "update  user_rights\n          set     name =  :1 ,\n                  description =  :2 \n          where   right =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.user.RightRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,name);
   __sJT_st.setString(2,description);
   __sJT_st.setLong(3,__sJT_97);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:168^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:172^9*/

//  ************************************************************
//  #sql [Ctx] { insert into user_rights
//            (
//              name,
//              description,
//              right
//            )
//            values
//            (
//              :name,
//              :description,
//              :right.getRightId()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_98 = right.getRightId();
   String theSqlTS = "insert into user_rights\n          (\n            name,\n            description,\n            right\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.user.RightRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,name);
   __sJT_st.setString(2,description);
   __sJT_st.setLong(3,__sJT_98);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:186^9*/
      }
      
      submitOk = true;
    }
    catch(Exception e)
    {
      logEntry("submit()", e.toString());
    }
    
    return submitOk;
  }
  
  /*
  ** METHOD protected boolean validate()
  **
  ** Validates right data.
  **
  ** RETURNS: true if data is valid, else false.
  */
  protected boolean validate()
  {
    return true;
  }
  
  /*
  ** METHOD public boolean delete()
  **
  ** Deletes the current record from the database.
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean delete()
  {
    boolean deleteOk = false;
    
    try
    {
      // delete group to right records
      /*@lineinfo:generated-code*//*@lineinfo:225^7*/

//  ************************************************************
//  #sql [Ctx] { delete from user_group_to_right
//          where       right_id = :right.getRightId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_99 = right.getRightId();
   String theSqlTS = "delete from user_group_to_right\n        where       right_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.user.RightRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_99);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:229^7*/
      
      // delete right record
      /*@lineinfo:generated-code*//*@lineinfo:232^7*/

//  ************************************************************
//  #sql [Ctx] { delete from user_rights
//          where       right_id = :right.getRightId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_100 = right.getRightId();
   String theSqlTS = "delete from user_rights\n        where       right_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.user.RightRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_100);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:236^7*/
      
      deleteOk = true;
    }
    catch (Exception e)
    {
      logEntry("delete()", e.toString());
    }

    return deleteOk;
  }
  
  /*
  ** ACCESSORS
  */
  public Right getRight()
  {
    return right;
  }
  public void setRight(Right newRight)
  {
    right = newRight;
  }
  
  public long getRightId()
  {
    if (right != null)
    {
      return right.getRightId();
    }
    return 0L;
  }
  public void setRightId(String newRightId)
  {
    try
    {
      setRightId(Long.parseLong(newRightId));
    }
    catch(Exception e)
    {
      logEntry("setRightId(" + newRightId + ")", e.toString());
    }
  }
  public void setRightId(long newRightId)
  {
    right = new Right(newRightId);
  }
  
  public String getName()
  {
    return name;
  }
  public void setName(String newName)
  {
    name = newName;
  }
  
  public void setDescription(String newDescription)
  {
    description = newDescription;
  }
  public String getDescription()
  {
    return description;
  }
}/*@lineinfo:generated-code*/