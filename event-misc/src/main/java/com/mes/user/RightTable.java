/*@lineinfo:filename=RightTable*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/RightTable.sqlj $

  Description:  
  
    RightTable.
    
    Loads all right records in user_rights into an ordered set upon 
    instantiation.  Records may then be retrieved by right value or
    index.  Also the set may be iterated over.
    
    
  Last Modified By   : $
  Last Modified Date : $
  Version            : $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class RightTable extends SQLJConnectionBase
{
  public class RightRecordComparator implements Comparator
  {
    public int compare(Object o1, Object o2)
    {
      if (o1.getClass() != RightRecord.class || o2.getClass() != RightRecord.class)
      {
        throw new ClassCastException(
          "Attempted to compare class type other than RightRecord");
      }
    
      RightRecord r1 = (RightRecord)o1;
      RightRecord r2 = (RightRecord)o2;
      int result = r1.getName().compareTo(r2.getName());
      if (result == 0)
      {
        if (r1.getRightId() < r2.getRightId())
        {
          result = -1;
        }
        else if (r1.getRightId() > r2.getRightId())
        {
          result = 1;
        }
      }
      
      return result;
    }
  
    public boolean equals(Object o)
    {
      boolean result = false;
    
      if (o.getClass() == this.getClass())
      {
        result = true;
      }
      return result;
    }
  }

  protected Set             rightSet        = new TreeSet(new RightRecordComparator());
  protected Iterator        iter            = rightSet.iterator();
  protected RightRecord     currentRecord   = null;
  
  /*
  ** CONSTRUCTOR
  **
  ** Loads the contents of user_rights.
  */
  public RightTable()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    try
    {
      connect();
      
      // load the contents of user_rights
      /*@lineinfo:generated-code*//*@lineinfo:104^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    user_rights
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    user_rights";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.RightTable",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.RightTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:108^7*/
      
      rs = it.getResultSet();
      
      while (rs.next())
      {
        add(new RightRecord(rs));
      }
      
      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("RightTable constructor: ", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD public void clear()
  **
  ** Clears the record set.
  */
  public void clear()
  {
    rightSet.clear();
  }

  /*
  ** MERHOD public boolean add(RightRecord rr)
  **
  ** Adds the right record to the record set.
  **
  ** RETURNS: true if successfully added, else false (record already in set)
  */
  public boolean add(RightRecord rr)
  {
    return rightSet.add(rr);
  }
  
  /*
  ** METHOD public boolean contains(RightRecord rr)
  **
  ** RETURNS: true if right record is in the record set, else false.
  */
  public boolean contains(RightRecord rr)
  {
    return rightSet.contains(rr);
  }
  
  /*
  ** METHOD public boolean remove(RightRecord rr)
  **
  ** Removes the right record from the record set.
  **
  ** RETURNS: true if record was found and removed, else false.
  */
  public boolean remove(RightRecord rr)
  {
    return rightSet.remove(rr);
  }
  
  /*
  ** METHOD public RightRecord getFirst()
  **
  ** Initializes internal iterator to the first item in the ordered set then
  ** sets the currentRecord equal to the first record.
  **
  ** RETURNS: the first record in the set, or null if empty set.
  */
  public RightRecord getFirst()
  {
    iter = rightSet.iterator();
    currentRecord = null;
    
    if (iter.hasNext())
    {
      currentRecord = (RightRecord)iter.next();
    }
    
    return currentRecord;
  }
  
  /*
  ** METHOD public RightRecord getCurrent()
  **
  ** RETURNS: currentRecord (which may be null).
  */
  public RightRecord getCurrent()
  {
    return currentRecord;
  }
  
  /*
  ** METHOD public RightRecord getNext()
  **
  ** Attempts to get the next available right record from the set.
  **
  ** RETURNS: the next record if found, else null.
  */
  public RightRecord getNext()
  {
    currentRecord = null;
    if (iter != null && iter.hasNext())
    {
      currentRecord = (RightRecord)iter.next();
    }
    return currentRecord;
  }
}/*@lineinfo:generated-code*/