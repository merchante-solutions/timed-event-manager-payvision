/*@lineinfo:filename=UserTypeTable*//*@lineinfo:user-code*//*@lineinfo:1^1*/ /*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/UserTypeTable.sqlj $

  Description:  
  
    UserTypeTable.
    
    Loads all right records in user_rights into an ordered set upon 
    instantiation.  Records may then be retrieved by right value or
    index.  Also the set may be iterated over.
    
    
  Last Modified By   : $
  Last Modified Date : $
  Version            : $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class UserTypeTable extends SQLJConnectionBase
{
  public class UserTypeRecordComparator implements Comparator
  {
    public int compare(Object o1, Object o2)
    {
      UserTypeRecord typeRec = new UserTypeRecord();
      if (o1.getClass() != typeRec.getClass() || o2.getClass() != typeRec.getClass())
      {
        throw new ClassCastException(
          "Attempted to compare class type other than UserTypeRecord");
      }
    
      UserTypeRecord r1 = (UserTypeRecord)o1;
      UserTypeRecord r2 = (UserTypeRecord)o2;
      int result;
      if ((result = r1.getName().compareTo(r2.getName())) == 0)
      {
        if (r1.getUserTypeId() < r2.getUserTypeId())
        {
          result = -1;
        }
        else if (r1.getUserTypeId() > r2.getUserTypeId())
        {
          result = 1;
        }
      }
            
      return result;
    }
  
    public boolean equals(Object o)
    {
      boolean result = false;
    
      if (o.getClass() == this.getClass())
      {
        result = true;
      }
      return result;
    }
  }
  
  protected Set             typeSet         = new TreeSet(new UserTypeRecordComparator());
  protected Iterator        iter            = typeSet.iterator();
  protected UserTypeRecord  currentRecord   = null;
  
  /*
  ** CONSTRUCTOR
  **
  ** Loads the contents of user_types.
  */
  public UserTypeTable()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:103^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    user_types
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    user_types";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.UserTypeTable",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.UserTypeTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:107^7*/
      
      rs = it.getResultSet();
      while (rs.next())
      {
        add(new UserTypeRecord(rs));
      }
      
      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("UserTypeTable constructor", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD public UserTypeRecord get(long typeId)
  **
  ** Looks for the UserTypeRecord corresponding with the given group id
  ** in the group record set.
  **
  ** RETURNS: UserTypeRecord if found, else null.
  */
  public UserTypeRecord get(long typeId)
  {
    UserTypeRecord typeRec = null;
    boolean foundIt = false;
    for (Iterator i = typeSet.iterator(); i.hasNext() && !foundIt;)
    {
      typeRec = (UserTypeRecord)(i.next());
      if (typeRec.getUserTypeId() == typeId)
      {
        foundIt = true;
      }
    }
    
    if (foundIt)
    {
      return typeRec;
    }
    
    return null;
  }
  
  /*
  ** METHOD public void clear()
  **
  ** Clears the record set.
  */
  public void clear()
  {
    typeSet.clear();
  }

  /*
  ** MERHOD public boolean add(UserTypeRecord typeRec)
  **
  ** Adds the type record to the record set.
  **
  ** RETURNS: true if successfully added, else false (record already in set)
  */
  public boolean add(UserTypeRecord typeRec)
  {
    return typeSet.add(typeRec);
  }
  
  /*
  ** METHOD public boolean contains(UserTypeRecord typeRec)
  **
  ** RETURNS: true if type record is in the record set, else false.
  */
  public boolean contains(UserTypeRecord typeRec)
  {
    return typeSet.contains(typeRec);
  }
  
  /*
  ** METHOD public boolean remove(UserTypeRecord typeRec)
  **
  ** Removes the type record from the record set.
  **
  ** RETURNS: true if record was found and removed, else false.
  */
  public boolean remove(UserTypeRecord typeRec)
  {
    return typeSet.remove(typeRec);
  }
  
  /*
  ** METHOD public UserTypeRecord getFirst()
  **
  ** Initializes internal iterator to the first item in the ordered set then
  ** sets the currentRecord equal to the first record.
  **
  ** RETURNS: the first record in the set, or null if empty set.
  */
  public UserTypeRecord getFirst()
  {
    iter = typeSet.iterator();
    currentRecord = null;
    
    if (iter.hasNext())
    {
      currentRecord = (UserTypeRecord)iter.next();
    }
    
    return currentRecord;
  }
  
  /*
  ** METHOD public UserTypeRecord getCurrent()
  **
  ** RETURNS: currentRecord (which may be null).
  */
  public UserTypeRecord getCurrent()
  {
    return currentRecord;
  }
  
  /*
  ** METHOD public UserTypeRecord getNext()
  **
  ** Attempts to get the next available type record from the set.
  **
  ** RETURNS: the next record if found, else null.
  */
  public UserTypeRecord getNext()
  {
    currentRecord = null;
    if (iter != null && iter.hasNext())
    {
      currentRecord = (UserTypeRecord)iter.next();
    }
    return currentRecord;
  }
}/*@lineinfo:generated-code*/