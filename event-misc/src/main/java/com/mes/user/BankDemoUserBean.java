/*@lineinfo:filename=BankDemoUserBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/user/BankDemoUserBean.java $

  Description:  

  UserBean
  
  Allows validation of user id's and passwords and gives access to various 
  user attributes such as user type, user group membership and user rights.
  
  Replaces UserValidateBean.  Contains legacy support for that older class.

  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 8/29/01 9:03a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.user;

import java.io.Serializable;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.crypt.MD5;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class BankDemoUserBean extends SQLJConnectionBase
  implements Serializable
{
  // user data
  private String      password;
  private String      loginName;
  private boolean     isValid;
  
  /*
  ** CONSTRUCTOR public UserBean()
  */
  public BankDemoUserBean()
  {
    // initialize internals
    clear();
  }
  
  /*
  ** METHOD public void clear()
  **
  ** Clears out internal data.
  */
  public synchronized void clear()
  {
    loginName = "";
    password = "";
    isValid = false;
  }

  public synchronized boolean isValid()
  {
    return isValid;
  }
   
  public synchronized void invalidate( )
  {
    clear();
  }
  
  private synchronized boolean isValidated(HttpServletRequest request)
  {
    return isValid;
  }
  public boolean validated(HttpServletRequest request)
  {
    return(isValidated(request));
  }
  
  public synchronized boolean validate(String userId, String password)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:102^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    bank_demo_password
//          where   user_id = :userId.trim() and
//                  password = :password.trim()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_80 = userId.trim();
 String __sJT_81 = password.trim();
  try {
   String theSqlTS = "select  *\n        from    bank_demo_password\n        where   user_id =  :1  and\n                password =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.user.BankDemoUserBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_80);
   __sJT_st.setString(2,__sJT_81);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.user.BankDemoUserBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:108^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        this.isValid   = true;
        this.loginName = userId;
        this.password  = password;
      }
      else
      {
        this.isValid   = false;
        return validateMesUser(userId,password);
      }
      
      rs.close();     
      it.close();
    }
    catch(Exception e)
    {
      logEntry("validate(" + userId + ", " + password + ")", e.toString());
      this.isValid = false;
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return isValid;
  }
  
  private boolean validateMesUser(String userId, String password)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    MD5               md5 = new MD5();
    
    try
    {
      connect();
      
      // get hash of password
      md5.Init();
      md5.Update(password.trim());
      String passHash = md5.asHex();
     
      /*@lineinfo:generated-code*//*@lineinfo:157^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    users
//          where   login_name = :userId.trim() and
//                  password_enc = :passHash.trim()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_82 = userId.trim();
 String __sJT_83 = passHash.trim();
  try {
   String theSqlTS = "select  *\n        from    users\n        where   login_name =  :1  and\n                password_enc =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.user.BankDemoUserBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_82);
   __sJT_st.setString(2,__sJT_83);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.user.BankDemoUserBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:163^7*/
      
      rs = it.getResultSet();

      if(rs.next())
      {
        this.isValid   = true;
        this.loginName = userId;
        this.password  = password;
      }
      else
      {
        this.isValid   = false;
      }
      
      rs.close();     
      it.close();
    }
    catch(Exception e)
    {
      logEntry("validateMesUser(" + userId + ", " + password + ")", e.toString());
      this.isValid = false;
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return this.isValid;
  }
 }/*@lineinfo:generated-code*/