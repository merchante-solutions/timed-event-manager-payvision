/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/SynXMLTestBean.java $

  Description:  
  
    SynXMLTestBean

    syn_xml_test.jsp bean.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 3/13/03 10:40a $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import javax.servlet.http.HttpServletRequest;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.HttpHelper;

public class SynXMLTestBean extends SQLJConnectionBase
{
  private   String    targetUrl     = null;
  private   int       messageType   = 1;
  private   long      appSeqNum     = 0L;
  
  public void setProperties(HttpServletRequest request)
  {
    setAppSeqNum(HttpHelper.getString(request,"appSeqNum"));
    setMessageType(HttpHelper.getString(request,"messageType"));
    setTargetUrl(HttpHelper.getString(request,"targetUrl"));
  }
  
  /*
  ** ACCESSORS
  */
  public String getTargetUrl()
  {
    return targetUrl;
  }
  public void setTargetUrl(String newTargetUrl)
  {
    targetUrl = newTargetUrl;
  }
  
  public long getAppSeqNum()
  {
    return appSeqNum;
  }
  public void setAppSeqNum(String newAppSeqNum)
  {
    try
    {
      appSeqNum = Long.parseLong(newAppSeqNum);
    }
    catch (Exception e)
    {
      appSeqNum = 0;
    }
  }
  
  public int getMessageType()
  {
    return messageType;
  }
  public void setMessageType(String newMessageType)
  {
    try
    {
      messageType = Integer.parseInt(newMessageType);
    }
    catch (Exception e)
    {
      messageType = 0;
    }
  }
}