/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/XMLTestBean.java $

  Description:  
  
    XMLTestBean

    xml_test.jsp bean.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 4/03/03 11:00a $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.lang.reflect.Array;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.HttpHelper;

public class XMLTestBean extends SQLJConnectionBase
{
  private   String    targetUrl     = null;
  private   long      appSeqNum     = 0L;
  private   int       msgType       = 1;
  private   int       statusType    = 1;
  private   boolean   amex          = false;
  private   boolean   discover      = false;
  private   boolean   diners        = false;
  private   boolean   jcb           = false;
  private   boolean   check         = false;

  public void setProperties(HttpServletRequest request)
  {
    setAppSeqNum(HttpHelper.getString(request,"appSeqNum"));
    setMsgType(HttpHelper.getString(request,"msgType"));
    setStatusType(HttpHelper.getString(request,"statusType"));
    setCards(request.getParameterValues("cards"));
    setTargetUrl(HttpHelper.getString(request,"targetUrl"));
  }
  
  /*
  ** ACCESSORS
  */
  public String getTargetUrl()
  {
    return targetUrl;
  }
  public void setTargetUrl(String newTargetUrl)
  {
    targetUrl = newTargetUrl;
  }
  
  public long getAppSeqNum()
  {
    return appSeqNum;
  }
  public void setAppSeqNum(String newAppSeqNum)
  {
    try
    {
      appSeqNum = Long.parseLong(newAppSeqNum);
    }
    catch (Exception e)
    {
      appSeqNum = 0;
    }
  }
  
  public int getMsgType()
  {
    return msgType;
  }
  public void setMsgType(String newMsgType)
  {
    try
    {
      msgType = Integer.parseInt(newMsgType);
    }
    catch (Exception e)
    {
      msgType = 0;
    }
  }
  
  public int getStatusType()
  {
    return statusType;
  }
  public void setStatusType(String newStatusType)
  {
    try
    {
      statusType = Integer.parseInt(newStatusType);
    }
    catch (Exception e)
    {
      statusType = 0;
    }
  }
  
  public boolean getCard(int whichCard)
  {
    boolean cardOn = false;
    
    switch (whichCard)
    {
      case mesConstants.APP_CT_AMEX:
        cardOn = amex;
        break;
        
      case mesConstants.APP_CT_DISCOVER:
        cardOn = discover;
        break;
        
      case mesConstants.APP_CT_DINERS_CLUB:
        cardOn = diners;
        break;
        
      case mesConstants.APP_CT_JCB:
        cardOn = jcb;
        break;
        
      case mesConstants.APP_CT_CHECK_AUTH:
        cardOn = check;
        break;
    }
    
    return cardOn;
  }
  public void setCards(String[] cards)
  {
    amex = false;
    discover = false;
    diners = false;
    jcb = false;
    check = false;
    if (cards != null)
    {
      for (int i = 0; i < Array.getLength(cards); ++i)
      {
        try
        {
          switch (Integer.parseInt(cards[i]))
          {
            case mesConstants.APP_CT_AMEX:
              amex = true;
              break;
        
            case mesConstants.APP_CT_DISCOVER:
              discover = true;
              break;
        
            case mesConstants.APP_CT_DINERS_CLUB:
              diners = true;
              break;
        
            case mesConstants.APP_CT_JCB:
              jcb = true;
              break;
        
            case mesConstants.APP_CT_CHECK_AUTH:
              check = true;
              break;
          }
        }
        catch (Exception e) { }
      }
    }
  }
}