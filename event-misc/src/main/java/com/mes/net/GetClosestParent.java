/*@lineinfo:filename=GetClosestParent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/GetClosestParent.sqlj $

  Description:

    AppLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/23/04 4:57p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.net;

import java.sql.ResultSet;
import com.mes.constants.MesHierarchy;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class GetClosestParent extends SQLJConnectionBase
{
  public GetClosestParent(DefaultContext Ctx)
  {
    if(Ctx != null)
    {
      this.Ctx = Ctx;
    }
  }
  
  private long getClosestNode(String tableName, long userNode)
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    long result = 0;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:57^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  th.ancestor
//          from    :tableName  tb,
//                  t_hierarchy   th
//          where   th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                  th.descendent = :userNode and
//                  th.ancestor = tb.hierarchy_node
//          order by th.relation
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  th.ancestor\n        from     ");
   __sjT_sb.append(tableName);
   __sjT_sb.append("   tb,\n                t_hierarchy   th\n        where   th.hier_type =  ?  and\n                th.descendent =  ?  and\n                th.ancestor = tb.hierarchy_node\n        order by th.relation");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "0com.mes.net.GetClosestParent:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(2,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:66^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        result = rs.getLong("ancestor");
      }
    }
    catch(Exception e)
    {
      logEntry("getClosestNode()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      
      cleanUp();
    }
    
    return result;
  }
  
  public static long get(String tableName, long userNode, DefaultContext Ctx)
  {
    long result = 0;
    try
    {
      GetClosestParent worker = new GetClosestParent(Ctx);
      
      result = worker.getClosestNode(tableName, userNode);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("GetClosestParent.get(" + tableName + ", " + userNode + ")", e.toString());
    }
    
    return result;
  }
}/*@lineinfo:generated-code*/