/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/XmlQueueTestBean.sqlj $

  Description:  
  
    XmlQueueTestBean

    xml_test.jsp bean.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 9/24/01 12:16p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import com.mes.database.SQLJConnectionBase;

public class XmlQueueTestBean extends SQLJConnectionBase
{
  private   long      appSeqNum     = 0L;
  private   int       statusType    = 0;
  private   int       cardType      = 0;

  /*
  ** ACCESSORS
  */
  public long getAppSeqNum()
  {
    return appSeqNum;
  }
  public void setAppSeqNum(String newAppSeqNum)
  {
    try
    {
      appSeqNum = Long.parseLong(newAppSeqNum);
    }
    catch (Exception e)
    {
      appSeqNum = 0;
    }
  }
  
  public int getStatusType()
  {
    return statusType;
  }
  public void setStatusType(String newStatusType)
  {
    try
    {
      statusType = Integer.parseInt(newStatusType);
    }
    catch (Exception e)
    {
      statusType = 0;
    }
  }

  public int getCardType()
  {
    return cardType;
  }
  public void setCardType(String newCardType)
  {
    try
    {
      cardType = Integer.parseInt(newCardType);
    }
    catch (Exception e)
    {
      cardType = 0;
    }
  }
}
