/*@lineinfo:filename=VSXMLSender*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VSXMLSender.sqlj $

  Description:  
  
    XMLSender

    Sends XML messages, receives responses.  Communications are logged.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 4/26/02 4:21p $
  Version            : $Revision: 10 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.io.PrintWriter;
import java.net.Socket;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import com.mes.database.SQLJConnectionBase;

public class VSXMLSender extends SQLJConnectionBase
{
  String jobDescriptor = null;
  
  public VSXMLSender(String newJobDescriptor)
  {
    jobDescriptor = newJobDescriptor;
  }
    
  /*
  ** LOGGING
  */
  private void log(VSMessage message, String details)
  {
    String messageType = "";
    if (message != null)
    {
      messageType = message.getMessageType();
    }
    if (details == null)
    {
      details = "No details";
    }
    
    try
    {
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:68^7*/

//  ************************************************************
//  #sql [Ctx] { insert into xml_log
//          (
//            log_time,
//            job_name,
//            msg_type,
//            details
//          )
//          values
//          (
//            sysdate,
//            :jobDescriptor,
//            :messageType,
//            :details
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into xml_log\n        (\n          log_time,\n          job_name,\n          msg_type,\n          details\n        )\n        values\n        (\n          sysdate,\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.net.VSXMLSender",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,jobDescriptor);
   __sJT_st.setString(2,messageType);
   __sJT_st.setString(3,details);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^7*/
      cleanUp();
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".log, " + e.toString());
      logEntry("log",e.toString());
    }
  }
  private void logConnectError(String details)
  {
    log(null,details);
  }
  private void logRequestSent(VSRequest requestMsg)
  {
    log(requestMsg,"Request sent: " + requestMsg.getDetails());
  }
  private void logResponseReceived(VSResponse responseMsg)
  {
    if (responseMsg.getResult() == 1)
    {
      log(responseMsg,"Response result success");
    }
    else
    {
      log(responseMsg,responseMsg.getDetails());
    }
  }
  
  /*
  ** public VSResponse send(VSRequest requestMsg)
  **
  ** Opens a connection with the redirector and sends the target url of
  ** the request followed by the XML data contained in the request.  The
  ** XML response generated is placed in a respone message to be returned.
  **
  ** RETURNS: response message.
  */
  public VSResponse send(VSRequest requestMsg)
  {
    VSResponse responseMsg = null;
    
    try
    {
      // connect to redirector
      String host = requestMsg.getRedirectorHost();
      int port = requestMsg.getRedirectorPort();
      Socket rSocket = new Socket(host,port);
      PrintWriter rpw = new PrintWriter(rSocket.getOutputStream(),true);
    
      // send the target url and content type
      rpw.println(requestMsg.getTargetUrl());
      rpw.println(requestMsg.getContentType());
      
      // send the xml message data
      XMLOutputter fmt = new XMLOutputter();
      fmt.output(requestMsg.getDocument(),rpw);
      logRequestSent(requestMsg);
    
      // send end_post_data
      rpw.println("end_post_data");
    
      // receive response
      SAXBuilder builder = new SAXBuilder();
      responseMsg = new VSResponse(builder.build(rSocket.getInputStream()));
      logResponseReceived(responseMsg);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".send, " + e.toString());
      logEntry("send",e.toString());
    }
        
    return responseMsg;
  }
  
  /*
  ** public static VSResponse sendMessage(VSRequest requestMsg)
  **
  ** Instantiates an instance of VSXMLSender, sends the request message.
  **
  ** RETURNS: the response message that was returned.
  */  
  public static VSResponse sendMessage(VSRequest requestMsg, String jobName)
  {
    VSXMLSender sender = new VSXMLSender(jobName);
    return sender.send(requestMsg);
  }
}/*@lineinfo:generated-code*/