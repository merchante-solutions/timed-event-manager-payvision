/*@lineinfo:filename=VirtualAppISOProcessor*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: com/mes/virtualapp/VirtualAppISOProcessor.sqlj

  Description:

    VirtualAppISOProcessor

    Validates and processes an ISO XML Virtual app from an outside vendor

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.virtualapp;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import com.mes.app.vs.Application;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldError;
import com.mes.net.MailMessage;
import com.mes.support.HttpHelper;
import oracle.sql.BLOB;
import sqlj.runtime.ResultSetIterator;

public class VirtualAppISOProcessor extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(VirtualAppISOProcessor.class);

  /**
   *  Quick and dirty response document generation
   */
  public static final String RESP_CODE_APPROVE  = "1";
  public static final String RESP_CODE_PEND     = "2";
  public static final String RESP_CODE_ERROR    = "3";

  public static final String RESP_TEXT_APPROVE  = "APPROVE";
  public static final String RESP_TEXT_PEND     = "PEND";
  public static final String RESP_TEXT_ERROR    = "ERROR";

  public class ResponseDocument extends Document
  {
    public ResponseDocument(Document requestDoc, String code, String text, 
      String subcode, String subtext)
    {
      String vendorId = "unknown";
      if (requestDoc != null)
      {
        vendorId = requestDoc.getRootElement().getChildText("VendorId");
      }
      Element root = new Element("XMLMESApplicationResponse");
      root.addContent(new Element("VendorId").setText(vendorId));
      root.addContent(new Element("SubmissionResponseCode").setText(code));
      root.addContent(new Element("SubmissionResponseText").setText(text));
      root.addContent(new Element("SubmissionResponseSubCode").setText(subcode));
      root.addContent(new Element("SubmissionResponseSubText").setText(subtext));
      addContent(root);
    }
    public ResponseDocument(String code, String text, String subcode, 
      String subtext)
    {
      this(null,code,text,subcode,subtext);
    }
  }

  private Document generateErrorResponseDoc(Document requestDoc, String errorText)
  {
    return new ResponseDocument(requestDoc,RESP_CODE_ERROR,RESP_TEXT_ERROR,
      "",errorText);
  }

  private Document generateErrorResponseDoc(String errorText)
  {
    return new ResponseDocument(RESP_CODE_ERROR,RESP_TEXT_ERROR,"",errorText);
  }

  private Document generateApprovalResponseDoc(Document requestDoc)
  {
    Document response = null;
    double monthlySales = 0.0;
    double avgTicket    = 0.0;
    
    try
    {
      StringBuffer tempBuf = new StringBuffer("");
      
      String temp = requestDoc.getRootElement().getChild("TransactionInformation").getChildText("MonthlyVMCSales");
      tempBuf.setLength(0);
      for(int i=0; i<temp.length(); ++i)
      {
        if(Character.isDigit(temp.charAt(i)) || temp.charAt(i) == '.')
        {
          tempBuf.append(temp.charAt(i));
        }
      }
      monthlySales  = Double.parseDouble(tempBuf.toString());
      
      temp = requestDoc.getRootElement().getChild("TransactionInformation").getChildText("AverageTicket");
      tempBuf.setLength(0);
      for(int i=0; i<temp.length(); ++i)
      {
        if(Character.isDigit(temp.charAt(i)) || temp.charAt(i) == '.')
        {
          tempBuf.append(temp.charAt(i));
        }
      }
      avgTicket     = Double.parseDouble(temp.toString());
      
      if(monthlySales <= 10000.00)
      {
        response = new ResponseDocument(requestDoc,RESP_CODE_APPROVE,RESP_TEXT_APPROVE,"","");
      }
      else if(avgTicket == 666.66)
      {
        // generate error response for testing purposes only
        response = generateErrorResponseDoc(requestDoc,"Avg Ticket = 666.66");
      }
      else
      {
        // generate pend response since monthly sales is too high
        response = new ResponseDocument(requestDoc,RESP_CODE_PEND,RESP_TEXT_PEND,"1000","Monthly Volume Exceeds $10,000");
      }
    }
    catch(Exception e)
    {
      logEntry("generateApprovalResponseDoc()", e.toString());
      response = new ResponseDocument(requestDoc,RESP_CODE_APPROVE,RESP_TEXT_APPROVE,"","");
    }
    return response;
  }

  /**
   *  Parses an xml doc from a string, returns the doc.
   */
  private Document convertDataToDoc(String data)
  {
    Document doc = null;
    try
    {
      // parse request data xml message
      //log.debug("generating xml doc from data...");
      SAXBuilder builder = new SAXBuilder();
      doc = builder.build(new ByteArrayInputStream(data.getBytes()));
    }
    catch (Exception e)
    {
      log.error("Error converting data to xml doc: " + e);
      logEntry("convertDataToDoc()",e.toString());
      throw new RuntimeException("Error converting data to xml doc: " + e);
    }
    return doc;
  }

  /**
   *  Takes an xml doc, uses a jdom outputter to convert it to a string,
   *  returns the string.
   */
  private String convertDocToData(Document doc)
  {
    String data = null;
    try
    {
      // convert the response to a string (with raw outputter)
      //log.debug("generating data from xml doc...");
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      XMLOutputter outputter = new XMLOutputter();
      outputter.output(doc,baos);
      data = baos.toString();
    }
    catch(Exception e)
    {
      log.error("Error converting xml doc to data: " + e);
      logEntry("convertDocToData()",e.toString());
      throw new RuntimeException("Error converting xml doc to data: " + e);
    }
    return data;
  }

  /**
   *  Generates a request xml doc from a string.
   */
  private Document generateRequestDoc(String requestData) throws Exception
  {
    try
    {
      return convertDataToDoc(requestData);
    }
    catch (Exception e)
    {
      log.error("Error converting requestData to requestDoc: " + e);
      logEntry("generateRequestDoc()",e.toString());
      throw new RuntimeException("Error converting requestData to requestDoc: " + e);
    }
  }

  /**
   *  Stores the given xml document in vapp_xml_post_iso as the posted data.
   *  Generates a new sequence number for the posting, returns the new number.
   */
  private long logRequestDoc(Document requestDoc) throws Exception
  {
    long vaSeqNum = -1L;
    boolean origCommit = getAutoCommit();
    String vendorId = null;
    String partnerId = null;

    try
    {
      setAutoCommit(false);
      connect();

      // pull the vendor and partner id out
      vendorId = requestDoc.getRootElement().getChildText("VendorId");
      partnerId = requestDoc.getRootElement().getChildText("PartnerId");
      //log.debug("vendorId = " + vendorId);
      //log.debug("partnerId = " + partnerId);

      // fetch a new seq num
      /*@lineinfo:generated-code*//*@lineinfo:245^7*/

//  ************************************************************
//  #sql [Ctx] { select  vapp_iso_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  vapp_iso_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.virtualapp.VirtualAppISOProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vaSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:250^7*/
      //log.debug("vaSeqNum = " + vaSeqNum);
      
      // create a new record for the posting
      /*@lineinfo:generated-code*//*@lineinfo:254^7*/

//  ************************************************************
//  #sql [Ctx] { insert into vapp_xml_post_iso
//          ( vapp_seq_num,
//            post_date,
//            vendor_id,
//            partner_id,
//            xml_data )
//          values
//          ( :vaSeqNum,
//            sysdate,
//            :vendorId,
//            :partnerId,
//            empty_blob() )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into vapp_xml_post_iso\n        ( vapp_seq_num,\n          post_date,\n          vendor_id,\n          partner_id,\n          xml_data )\n        values\n        (  :1 ,\n          sysdate,\n           :2 ,\n           :3 ,\n          empty_blob() )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.virtualapp.VirtualAppISOProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,vaSeqNum);
   __sJT_st.setString(2,vendorId);
   __sJT_st.setString(3,partnerId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:268^7*/
      
      // load the xml document data into the empty blob
      BLOB blobRef = null;
      /*@lineinfo:generated-code*//*@lineinfo:272^7*/

//  ************************************************************
//  #sql [Ctx] { select  xml_data
//          
//          from    vapp_xml_post_iso
//          where   vapp_seq_num = :vaSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  xml_data\n         \n        from    vapp_xml_post_iso\n        where   vapp_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.virtualapp.VirtualAppISOProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,vaSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   blobRef = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:278^7*/
      
      // write xml data into the blob (compressed)
      //log.debug("writing compressed xml app doc into db blob...");
      XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
      OutputStream out = new DeflaterOutputStream(blobRef.getBinaryOutputStream());
      outputter.output(requestDoc,out);
      out.flush();
      out.close();
      //log.debug("blob written.");

      commit();
    }
    finally
    {
      cleanUp();
      setAutoCommit(origCommit);
    }

    return vaSeqNum;
  }
  
  private boolean isDuplicate(String vendorId)
  {
    boolean isDup = false;
    
    try
    {
      connect();
      
      int appCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:310^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(vapp_seq_num)
//          
//          from    vapp_xml_post_iso
//          where   vendor_id = :vendorId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(vapp_seq_num)\n         \n        from    vapp_xml_post_iso\n        where   vendor_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.virtualapp.VirtualAppISOProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,vendorId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:316^7*/
      
      // it's a duplicate if there are more than one app in the system already
      isDup = (appCount > 1);
    }
    catch(Exception e)
    {
      logEntry("isDuplicate(" + vendorId + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return isDup;
  }

  private void validateRequestDoc(Document requestDoc)
    throws NullPointerException, DuplicateAppException, Exception
  {
    try
    {
      // generate an error if the id's are missing
      String vendorId = requestDoc.getRootElement().getChildText("VendorId");
      String partnerId = requestDoc.getRootElement().getChildText("PartnerId");
      if (vendorId == null || vendorId.equals("") || 
          partnerId == null || partnerId.equals(""))
      {
        throw new NullPointerException("Missing VendorId or PartnerId");
      }
    
      // check for duplicate app
      if(HttpHelper.isProdServer(null) && MesDefaults.getInt(MesDefaults.DK_VPS_ISO_CHECK_DUPLICATES) == 1 && isDuplicate(vendorId))
      {
        throw new DuplicateAppException("Duplicate Application");
      }
    }
    catch(Exception e)
    {
      throw e;
    }
  }

  /**
   * Logs the response xml doc in the database with the original request.
   */
  private void logResponseDoc(Document responseDoc, long vaSeqNum)
    throws Exception
  {
    boolean origCommit = getAutoCommit();
    try
    {
      setAutoCommit(false);
      connect();

      // create empty blob
      /*@lineinfo:generated-code*//*@lineinfo:372^7*/

//  ************************************************************
//  #sql [Ctx] { update  vapp_xml_post_iso
//          set     xml_response = empty_blob()
//          where   vapp_seq_num = :vaSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  vapp_xml_post_iso\n        set     xml_response = empty_blob()\n        where   vapp_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.virtualapp.VirtualAppISOProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,vaSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:377^7*/

      // get reference to the storage blob
      BLOB blobRef;
      /*@lineinfo:generated-code*//*@lineinfo:381^7*/

//  ************************************************************
//  #sql [Ctx] { select  xml_response
//          
//          from    vapp_xml_post_iso
//          where   vapp_seq_num = :vaSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  xml_response\n         \n        from    vapp_xml_post_iso\n        where   vapp_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.virtualapp.VirtualAppISOProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,vaSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   blobRef = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:387^7*/
    
      // write xml data into blob (compressed)
      DeflaterOutputStream daos = 
        new DeflaterOutputStream(blobRef.getBinaryOutputStream());
      XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
      outputter.output(responseDoc,daos);
      daos.flush();
      daos.close();
    
      commit();
    }
    finally
    {
      cleanUp();
      setAutoCommit(origCommit);
    }
  }

  /**
   *  Update a vapp_xml_post_iso row with the response text and elapsed
   *  processing time.
   */
  private void logResponseInfo(long vaSeqNum, Document responseDoc, long tsStart)
  {
    try
    {
      connect();

      //log.debug("logging response info for " + vaSeqNum + "...");

      // update response text and elapsed time
      long tsDone = System.currentTimeMillis();
      String responseText 
        = responseDoc.getRootElement().getChildText("SubmissionResponseText");
      //log.debug("response text = " + responseText);
    
      /*@lineinfo:generated-code*//*@lineinfo:424^7*/

//  ************************************************************
//  #sql [Ctx] { update  vapp_xml_post_iso
//          set     response_message  = :responseText,
//                  elapsed_millis    = :tsDone - tsStart
//          where   vapp_seq_num      = :vaSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3588 = tsDone - tsStart;
   String theSqlTS = "update  vapp_xml_post_iso\n        set     response_message  =  :1 ,\n                elapsed_millis    =  :2 \n        where   vapp_seq_num      =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.virtualapp.VirtualAppISOProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,responseText);
   __sJT_st.setLong(2,__sJT_3588);
   __sJT_st.setLong(3,vaSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:430^7*/
    }
    catch (Exception e)
    {
      log.error("Error logging response info: " + e);
      logEntry("logResponseInfo()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void logAppSeqNum(long vaSeqNum, long appSeqNum)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:449^7*/

//  ************************************************************
//  #sql [Ctx] { update  vapp_xml_post_iso
//          set     app_seq_num = :appSeqNum
//          where   vapp_seq_num = :vaSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  vapp_xml_post_iso\n        set     app_seq_num =  :1 \n        where   vapp_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.virtualapp.VirtualAppISOProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,vaSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:454^7*/

    }
    catch (Exception e)
    {
      log.error("Error logging app_seq_num " + appSeqNum + " with vapp_seq_num " 
        + vaSeqNum);
      logEntry("logAppSeqNum()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private String submitErrorText = null;

  private boolean submitAppForRequestDoc(Document requestDoc, long vaSeqNum)
  {
    boolean submitOk = false;
    try
    {
      // convert xml doc data to ola db data using app bean
      //log.debug("setting xml data in app bean...");
      Application appBean = new Application();
      appBean.setFields(
        requestDoc,FieldBean.MAP_TYPE_VERISIGN_ISO,null,vaSeqNum,null);

      // check for errors in the data
      if (!appBean.isValid())
      {
        // generate an error response with the first error found by the app bean
        //log.debug("invalid field data, generating error response...");
        FieldError error = (FieldError)(appBean.getErrors().get(0));
        submitErrorText = "Invalid field '" + error.getFieldLabel()
          + "' - " + error.getErrorText();
      }
      // store the ola app data and send approval response
      else
      {
        //log.debug("field data valid, submitting and generating" + " approval response...");
        appBean.doExternalSubmit();
        long appSeqNum = appBean.getField("appSeqNum").asLong();
        logAppSeqNum(vaSeqNum,appSeqNum);
        submitOk = true;
      }
    }
    catch (Exception e)
    {
      submitErrorText = "Error during submit: " + e;
      logEntry("submitAppForRequestDoc()",e.toString());
    }
    return submitOk;
  }

  /**
   *  Attempt to take data from an xml request doc and turn it into ola data.  If
   *  ola data is valid it is saved to db.  A response xml doc is created that
   *  indicates if the ola submission was successful or not.  Returns the generated
   *  response doc after logging it with the original request.
   */
  private Document processRequestDoc(Document requestDoc, long vaSeqNum,
    long tsStart) throws Exception
  {
    Document responseDoc = null;
    try
    {
      if (!submitAppForRequestDoc(requestDoc,vaSeqNum))
      {
        responseDoc = generateErrorResponseDoc(requestDoc,submitErrorText);
      }
      else
      {
        responseDoc = generateApprovalResponseDoc(requestDoc);
      }
    }
    catch (Exception e)
    {
      log.error("Error processing request doc: " + e);
      responseDoc = generateErrorResponseDoc(requestDoc,e.toString());
      logEntry("processRequestDoc()",e.toString());
    }
    return responseDoc;
  }

  /**
   *  Stores the response doc then converts it to a string, returns the string.
   */
  private String generateResponseData(Document responseDoc)
  {
    try
    {
      return convertDocToData(responseDoc);
    }
    catch (Exception e)
    {
      log.error("Error converting responseDoc to responseData: " + e);
      logEntry("generateResponseData()",e.toString());
      throw new RuntimeException("Unable to generate response data: " + e);
    }
  }

  /**
   * Converts request data string to an xml document, generate online app data
   * using the verisign iso application bean.  Generates a response and returns
   * it as a string.  The request and response data are logged in the
   * vapp_xml_post_iso table.
   */
  public String processRequestData(String requestData, long tsStart)
  {
    Document responseDoc = null;
    String responseData = null;
    long vaSeqNum = -1L;

    try
    {
      // generate request doc from request data string
      Document requestDoc = generateRequestDoc(requestData);

      // store the posted data and get a seq num
      vaSeqNum = logRequestDoc(requestDoc);

      // do some validation
      validateRequestDoc(requestDoc);

      // process request data into ola land, generate a response
      responseDoc = processRequestDoc(requestDoc,vaSeqNum,tsStart);

      // generate response data string from response xml doc
      responseData = generateResponseData(responseDoc);
      
      // email the response data
      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_VS_ISO_APP_NOTIFY);
      msg.setSubject("Virtual App Submission Response (vaSeqNum=" + vaSeqNum + ")");
      msg.setText(responseData);
      msg.send();
    }
    catch (DuplicateAppException de)
    {
      responseDoc = generateErrorResponseDoc(de.toString());
      responseData = generateResponseData(responseDoc);
    }
    catch (Exception e)
    {
      responseDoc = generateErrorResponseDoc(e.toString());
      responseData = generateResponseData(responseDoc);
      log.error("Unable to process request: " + e);
      e.printStackTrace();
      logEntry("processRequestData()",e.toString());
    }
    finally
    {
      // always log response doc if possible
      if (vaSeqNum != -1L && responseDoc != null)
      {
        try
        {
          logResponseDoc(responseDoc,vaSeqNum);
        }
        catch (Exception e)
        {
          log.error("Error logging response doc: " + e);
          logEntry("processRequestData()",e.toString());
        }
      }
    }

    // if process generated a seq num then log response info
    if (vaSeqNum != -1L)
    {
      logResponseInfo(vaSeqNum,responseDoc,tsStart);
    }

    return responseData;
  }

  /**
   *  Fetches a request or response with the given seq num.  Data type 1 returns
   *  the request, 2 returns the response.
   */
  private String getData(long vaSeqNum, int dataType)
  {
    String data = null;
    ResultSetIterator it = null;
    ResultSet rs = null;
    try
    {
      connect();

      switch (dataType)
      {
        case 1:
          /*@lineinfo:generated-code*//*@lineinfo:647^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  xml_data
//              from    vapp_xml_post_iso
//              where   vapp_seq_num = :vaSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  xml_data\n            from    vapp_xml_post_iso\n            where   vapp_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.virtualapp.VirtualAppISOProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,vaSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.virtualapp.VirtualAppISOProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:652^11*/
          break;

        case 2:
          /*@lineinfo:generated-code*//*@lineinfo:656^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  xml_response
//              from    vapp_xml_post_iso
//              where   vapp_seq_num = :vaSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  xml_response\n            from    vapp_xml_post_iso\n            where   vapp_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.virtualapp.VirtualAppISOProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,vaSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.virtualapp.VirtualAppISOProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:661^11*/
          break;

        default:
          throw new RuntimeException("getData() invalid data type " + dataType);
      }

      rs = it.getResultSet();
      if (rs.next())
      {
        InputStream in = rs.getBlob(1).getBinaryStream();
        InflaterInputStream iis = new InflaterInputStream(in);
        BufferedReader reader = new BufferedReader(new InputStreamReader(iis));
        StringBuffer dataBuf = new StringBuffer();
        for (String line = null; (line = reader.readLine()) != null;)
        {
          dataBuf.append(line);
        }
        in.close();
        data = dataBuf.toString();
      }
    }
    catch (Exception e)
    {
      log.error("Error getting data (" + dataType + "): " + e);
      throw new RuntimeException("Error getting data (" + dataType + "): " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { it.close(); } catch (Exception e) { }
      cleanUp();
    }
    return data;
  }

  public String getRequestData(long vaSeqNum)
  {
    return getData(vaSeqNum,1);
  }

  public String getResponseData(long vaSeqNum)
  {
    return getData(vaSeqNum,2);
  }
  
  private long getVappSeqNum(long appSeqNum)
  {
    long vaSeqNum = 0L;
    
    try
    {
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:714^7*/

//  ************************************************************
//  #sql [Ctx] { select  vapp_seq_num
//          
//          from    vapp_xml_post_iso
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  vapp_seq_num\n         \n        from    vapp_xml_post_iso\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.virtualapp.VirtualAppISOProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vaSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:720^7*/
    }
    catch(Exception e)
    {
      logEntry("getVappSeqNum(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return vaSeqNum;
  }
  
  public Document getRequestDocAppSeqNum(long appSeqNum)
  {
    return(getRequestDoc(getVappSeqNum(appSeqNum))); 
  }
  
  public Document getResponseDocAppSeqNum(long appSeqNum)
  {
    return(getResponseDoc(getVappSeqNum(appSeqNum)));
  }
  
  public Document getRequestDoc(long vaSeqNum)
  {
    return convertDataToDoc(getRequestData(vaSeqNum));
  }

  public Document getResponseDoc(long vaSeqNum)
  {
    return convertDataToDoc(getResponseData(vaSeqNum));
  }

  /**
   *  Reloads the xml document with the va seq num and attempts to submit an app
   *  for it.
   */
  public void reprocessRequest(long vaSeqNum)
  {
    // retrieve request doc
    //log.debug("fetching requestDoc...");
    Document requestDoc = getRequestDoc(vaSeqNum);

    // convert xml doc data to ola db data using app bean
    //log.debug("submitting app for requestDoc...");
    if (!submitAppForRequestDoc(requestDoc,vaSeqNum))
    {
      log.error("Error reprocessing request: " + submitErrorText);
      throw new RuntimeException("Error reprocessing request: " + submitErrorText);
    }
    else
    {
      //log.debug("App submitted.");
    }
  }


}/*@lineinfo:generated-code*/