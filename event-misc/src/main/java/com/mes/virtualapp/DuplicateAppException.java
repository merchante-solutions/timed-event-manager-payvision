package com.mes.virtualapp;

public class DuplicateAppException extends Exception
{
  public DuplicateAppException(String s)
  {
    super(s);
  }
}
