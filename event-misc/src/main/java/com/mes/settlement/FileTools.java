/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/FileTools.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-12-12 15:59:43 -0800 (Thu, 12 Dec 2013) $
  Version            : $Revision: 21951 $

  Change History:
     See SVN database

  Copyright (C) 2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import com.mes.support.DateTimeFormatter;
import com.mes.support.SyncLog;
import com.mes.tools.FileUtils;
import masthead.mastercard.clearingiso.IpmOfflineFile;
import masthead.mastercard.clearingiso.IpmOfflineMessage;
import masthead.mastercard.clearingiso.MicroFocusNetExpressFile;

public class FileTools
{
  protected static final HashMap  MasterCardMsgTypeToPrefix =
    new HashMap()
    {
      {
        //@put("","mc_dngrd");
        put("1740-*"    ,"mc_fee");
        put("1442-*"    ,"mc_chgbk");
        put("1644-603"  ,"mc_retr");
        put("1644-685"  ,"mc_recon");
        put("1644-688"  ,"mc_recon");
        put("1644-693"  ,"mc_msg");
      }
    };
  protected static final String DefaultMasterCardPrefix   = "mc_cycle";

  protected static final HashMap  VisaTranCodeToPrefix =
    new HashMap()
    {
      {
        put("04","vs_dngrd");
        put("10","vs_fee");
        put("15","vs_chgbk");   // sales cb
        put("16","vs_chgbk");   // credit voucher cb
        put("17","vs_chgbk");   // cash advance cb
        put("20","vs_fee");
        put("35","vs_chgbk");   // sales cb reversal
        put("36","vs_chgbk");   // credit voucher cb reversal
        put("37","vs_chgbk");   // cash advance cb reversal
        put("46","vs_recon");
        put("50","vs_msg");
        put("52","vs_retr");
        put("56","vs_dcc");     // dynamic currency conversion rates
      }
    };
  protected static final String DefaultVisaPrefix   = "vs_undiff";
  
  private static String generateItfFilename( String inFilename, String outFilename )
  {
    return( generateOutputFilename( inFilename, outFilename, "itf" ) );
  }
  
  private static String generateMCReportFilename( String inFilename, String outFilename )
  {
    return( generateOutputFilename( inFilename, outFilename, "txt" ) );
  }
  
  private static String generateOutputFilename( String inFilename, String outFilename, String outExt )
  {
    if ( outFilename == null || "null".equals(outFilename) )
    {
      String fname = inFilename;
      if ( fname.indexOf(".") >= 0 )
      {
        fname = fname.substring(0,fname.lastIndexOf("."));
      }
      outFilename = fname + "." + outExt;
    }
    
    return( outFilename );      
  }
  
  public static void fixVisaLineSeparators( String inFilename, String outFilename )
  {
    BufferedReader    in                = null;
    String            line              = null;
    int               lineCnt           = 0;
    int               lineLen           = 170;
    BufferedWriter    out               = null;
    String            output            = null;
  
    try
    {
      in        = new BufferedReader( new FileReader(inFilename) );
      out       = new BufferedWriter( new FileWriter(generateItfFilename(inFilename,outFilename),false) );
      
      //
      // java will correctly read lines terminated with \n or \r\n
      // the Visa Edit package will only access the PC newline (\r\n)
      //
      while( (line = in.readLine()) != null )
      {
        ++lineCnt;
        out.write(line);
        out.newLine();    // add system OS specific new line marker
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.VisaITFTools::fixVisaLineSeparators(" + inFilename + ")",e.toString());
      System.out.println(e.toString());
    }      
    finally
    {
      try{ out.close(); } catch(Exception ee){}
      try{ in.close(); } catch(Exception ee){}
    }
  }
  
  public static void extractVisaReportText( String inFilename, String outFilename )
  {
    BufferedReader    in                = null;
    String            line              = null;
    int               lineCnt           = 0;
    int               lineLen           = 170;
    BufferedWriter    out               = null;
    String            output            = null;
  
    try
    {
      in        = new BufferedReader( new FileReader(inFilename) );
      out       = new BufferedWriter( new FileWriter(generateItfFilename(inFilename,outFilename),false) );
      
      //
      // java will correctly read lines terminated with \n or \r\n
      // the Visa Edit package will only access the PC newline (\r\n)
      //
      while( (line = in.readLine()) != null )
      {
        ++lineCnt;
        if ( line.startsWith("47") )
        {
          out.write(line.substring(19,149));
          out.newLine();    // add system OS specific new line marker
        }          
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.FileTools::extractVisaReportText(" + inFilename + ")",e.toString());
      System.out.println(e.toString());
    }      
    finally
    {
      try{ out.close(); } catch(Exception ee){}
      try{ in.close(); } catch(Exception ee){}
    }
  }
  
  public static void formatMCReportText( String inFilename, String outFilename )
  {
    BufferedReader    in                = null;
    String            line              = null;
    int               lineCnt           = 0;
    int               lineLen           = 134;
    BufferedWriter    out               = null;
    String            output            = null;
  
    try
    {
      in        = new BufferedReader( new FileReader(inFilename) );
      out       = new BufferedWriter( new FileWriter(generateMCReportFilename(inFilename,outFilename),false) );
      
      //
      // java will correctly read lines terminated with \n or \r\n
      // the Visa Edit package will only access the PC newline (\r\n)
      //
      while( (line = in.readLine()) != null )
      {
        while( line.length() >= lineLen )
        {
          out.write(line.substring(0,(lineLen-1)));
          out.newLine();    // add system OS specific new line marker
          line = line.substring(lineLen-1);
        }
        
        if ( line.length() > 0 )
        {
          out.write(line);
          out.newLine();  
        }
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.FileTools::formatMCReportText(" + inFilename + ")",e.toString());
      System.out.println(e.toString());
    }      
    finally
    {
      try{ out.close(); } catch(Exception ee){}
      try{ in.close(); } catch(Exception ee){}
    }
  }
  
  public static boolean isCtfTrailerRecord( String tranCode )
  {
    return( "91".equals(tranCode) || 
            "92".equals(tranCode) );
  }
  
  public static boolean isIpmExtensionRecord( String msgType, String functionCode )
  {
    String  extensionFunctionCodes = "696";
    
    return( "1644".equals(msgType) &&
            extensionFunctionCodes.indexOf(functionCode) >= 0 );
  }
  
  public static boolean isIpmHeaderRecord( String msgType, String functionCode )
  {
    String  headerFunctionCodes = "697";
    return( "1644".equals(msgType) &&
            headerFunctionCodes.indexOf(functionCode) >= 0 );
  }
  
  public static boolean isIpmTrailerRecord( String msgType, String functionCode )
  {
    String  trailerFunctionCodes = "695";
    return( "1644".equals(msgType) &&
            trailerFunctionCodes.indexOf(functionCode) >= 0 );
  }
  
  public static void mcIpmPreProc( String ipmFilename, String fileDateSuffix )
  {
    int                 bankNumber        = 3941;
    Date                fileDate          = null;
    IpmOfflineMessage   fileHeader        = null;
    String              filePrefix        = null;
    String              functionCode      = null;
    IpmOfflineFile      ipmFile           = null;
    IpmOfflineMessage   ipmMsg            = null;
    String              lastMsgKey        = "";
    List                msgs              = null;
    String              msgKey            = null;
    String              msgType           = null;
    FileOutputStream    out               = null;
    HashMap             rawDataMap        = new HashMap();
    String              reasonCode        = null;
  
    try
    {
      if ( fileDateSuffix != null && fileDateSuffix.length() >= 14 )
      {
        // expecting "_MMddyy_HHmmss to be the file suffix
        fileDate = DateTimeFormatter.parseDate( fileDateSuffix.substring(1,7) +
                                                fileDateSuffix.substring(8,14),
                                                "MMddyyHHmmss" );
      }
      
      if ( fileDate == null )
      {
        fileDate = Calendar.getInstance().getTime(); 
      }        
      System.out.println("Pre-processing IPM file '" + ipmFilename + "'");

      //generate byte data
      byte[] data = FileUtils.getBytesFromFile(ipmFilename);

      //due to variance in files coming in,
      //there may be a parse exception
      try
      {
        // try accessing the data from static method 
        msgs = MicroFocusNetExpressFile.parseAsList(data);
      }
      catch(Exception e)
      {
        // not in MicroFocusNetExpress format
        ipmFile = IpmOfflineFile.parseMsgBlock(data);
        msgs    = ipmFile.getMessages();
      }
      if( msgs != null )
      {
        Iterator it = msgs.iterator();
        while( it.hasNext() )
        {
          ipmMsg          = (IpmOfflineMessage)it.next();
          msgType         = ipmMsg.getDataElement(0);
          functionCode    = ipmMsg.getDataElement(24);
          reasonCode      = ipmMsg.getDataElement(25);
          msgKey          = (msgType + "-" + functionCode);
          
          if ( isIpmHeaderRecord(msgType,functionCode) ) 
          { 
            fileHeader = ipmMsg;          // use same header in all files
            continue;
          }
          else if ( isIpmTrailerRecord(msgType,functionCode) ) 
          {
            continue;                     // ignore trailers
          }
          else if ( isIpmExtensionRecord(msgType,functionCode) )
          {
            ipmFile.addMessage(ipmMsg);   // add to the current file type
            continue;
          }
        
          if ( !lastMsgKey.equals(msgKey) ) 
          {
            filePrefix = (String)MasterCardMsgTypeToPrefix.get(msgKey);
            if ( filePrefix == null )
            {
              // try the wildcard key
              filePrefix = (String)MasterCardMsgTypeToPrefix.get(msgType+"-*");
            }
            
            if ( filePrefix == null ) 
            {
              filePrefix = DefaultMasterCardPrefix;
            }
            
            // look for specific record first
            ipmFile = (IpmOfflineFile) rawDataMap.get(filePrefix);
            if ( ipmFile == null ) 
            { 
              ipmFile = new IpmOfflineFile();
              ipmFile.setFileHeader(fileHeader);
              rawDataMap.put(filePrefix,ipmFile); 
            }
          }
          ipmFile.addMessage(ipmMsg);
          lastMsgKey = msgKey;
        }
      }
      
      for (Iterator it = rawDataMap.keySet().iterator(); it.hasNext(); )
      {
        filePrefix = (String)it.next();
        ipmFile = (IpmOfflineFile)rawDataMap.get(filePrefix);
        ipmFile.calculateTrailerTotals();
        ipmFile.setMessageNumbers();
        ipmFile.getFileTrailer().setPds(105, fileHeader.getPds(105));
        
        String outFilename = filePrefix + bankNumber + 
                              "_" + DateTimeFormatter.getFormattedDate(fileDate,"MMddyy") + 
                              "_" + DateTimeFormatter.getFormattedDate(fileDate,"HHmmss") +
                              ".ipm";
        System.out.println("Outputting MES data file '" + outFilename + "'");
        
        out = new FileOutputStream( outFilename, false );
        out.write( new MicroFocusNetExpressFile(ipmFile).toBytes() );	
        out.close();
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.FileTools::mcIpmPreProc(" + ipmFilename + ")",e.toString());
      System.out.println(e.toString());
    }      
    finally
    {
      try{ out.close(); } catch(Exception ee){}
    }
  }
    
    public static void dump( String ipmFilename )
  {
    int                 bankNumber        = 3941;
    Date                fileDate          = null;
    IpmOfflineMessage   fileHeader        = null;
    String              filePrefix        = null;
    String              functionCode      = null;
    IpmOfflineFile      ipmFile           = null;
    IpmOfflineMessage   ipmMsg            = null;
    String              lastMsgKey        = "";
    List                msgs              = null;
    String              msgKey            = null;
    String              msgType           = null;
    FileOutputStream    out               = null;
    HashMap             rawDataMap        = new HashMap();
    String              reasonCode        = null;
  
    try
    {
      byte[] data = FileUtils.getBytesFromFile(ipmFilename);
      try
      {
        msgs = MicroFocusNetExpressFile.parseAsList(data);
      }
      catch(Exception e)
      {
        ipmFile = IpmOfflineFile.parseMsgBlock(data);
        msgs    = ipmFile.getMessages();
      }
      if( msgs != null )
      {
        Iterator it = msgs.iterator();
        while( it.hasNext() )
        {
          ipmMsg          = (IpmOfflineMessage)it.next();
          System.out.println(ipmMsg.toXml());
        }
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.FileTools::mcIpmPreProc(" + ipmFilename + ")",e.toString());
      System.out.println(e.toString());
    }      
    finally
    {
      try{ out.close(); } catch(Exception ee){}
    }
  }
  
  public static void visaCtfPreProc( String ctfFilename, String fileDateSuffix )
  {
    int               bankNumber        = 3941;
    BufferedWriter    defaultOut        = null;
    Date              fileDate          = null;
    String            filePrefix        = null;
    BufferedReader    in                = null;
    String            lastTc            = "";
    String            line              = null;
    int               lineCnt           = 0;
    BufferedWriter    out               = null;
    HashMap           rawDataMap        = new HashMap();
    Vector            records           = null;
    String            tc                = null;
  
    try
    {
      if ( fileDateSuffix != null && fileDateSuffix.length() >= 14 )
      {
        // expecting "_MMddyy_HHmmss to be the file suffix
        fileDate = DateTimeFormatter.parseDate( fileDateSuffix.substring(1,7) +
                                                fileDateSuffix.substring(8,14),
                                                "MMddyyHHmmss" );
      }
      
      if ( fileDate == null )
      {
        fileDate = Calendar.getInstance().getTime(); 
      }        
    
      System.out.println("Pre-processing CTF file '" + ctfFilename + "'");
      in = new BufferedReader( new FileReader(ctfFilename) );
      
      while( (line = in.readLine()) != null )
      {
        ++lineCnt;
        if ( line.length() < 2 ) continue;        // skip short lines, necessary?
        
        tc = line.substring(0,2);
        
        if ( isCtfTrailerRecord(tc) ) continue;  // ignore trailers
        
        if ( !lastTc.equals(tc) ) 
        {
          records = (Vector) rawDataMap.get(tc);
          if ( records == null ) 
          { 
            records = new Vector(); 
            rawDataMap.put(tc,records); 
          }
        }
        records.add(line);
        lastTc = tc;
      }
      in.close();
      
      for (Iterator it = rawDataMap.keySet().iterator(); it.hasNext(); )
      {
        tc = (String)it.next();
        records = (Vector) rawDataMap.get(tc);
        filePrefix = (String)VisaTranCodeToPrefix.get(tc);
        if ( filePrefix == null ) 
        {
          if ( defaultOut == null ) 
          {
            String defaultFilename = DefaultVisaPrefix + bankNumber + 
                              "_" + DateTimeFormatter.getFormattedDate(fileDate,"MMddyy") + 
                              "_" + DateTimeFormatter.getFormattedDate(fileDate,"HHmmss") +
                              ".dat";
            defaultOut = new BufferedWriter( new FileWriter( defaultFilename ) );
          }
          out = defaultOut;
        }
        else
        {
          String outFilename = filePrefix + bankNumber + 
                                "_" + DateTimeFormatter.getFormattedDate(fileDate,"MMddyy") + 
                                "_" + DateTimeFormatter.getFormattedDate(fileDate,"HHmmss") +
                                ".dat";
          System.out.println("Outputting MES data file '" + outFilename + "'");
          out = new BufferedWriter( new FileWriter( outFilename, true ) );  // append to existing file
        }
        
        for ( int i = 0; i < records.size(); ++i )
        {
          out.write( (String)records.elementAt(i) );
          out.newLine();
        }
        out.close();
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.FileTools::visaCtfPreProc(" + ctfFilename + ")",e.toString());
      System.out.println(e.toString());
    }      
    finally
    {
      try{ out.close(); } catch(Exception ee){}
      try{ in.close(); } catch(Exception ee){}
    }
  }

  public static void removeItfHashValues( String inFilename, String outFilename )
  {
    BufferedReader    in                = null;
    String            line              = null;
    int               lineCnt           = 0;
    int               lineLen           = 170;
    BufferedWriter    out               = null;
    String            output            = null;
  
    try
    {
      in        = new BufferedReader( new FileReader(inFilename) );
      out       = new BufferedWriter( new FileWriter(generateItfFilename(inFilename,outFilename),false) );
      
      while( (line = in.readLine()) != null )
      {
        ++lineCnt;
      
        while ( line.length() < lineLen )
        {
          String line2 = in.readLine();
          line = line + " " + line2;
        }
        output = line.substring(0,2) + "  " + line.substring(4);
        
        out.write(output);
        out.newLine();
      }
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.VisaITFTools::removeItfHashValues(" + inFilename + ")",e.toString());
      System.out.println(e.toString());
    }      
    finally
    {
      try{ out.close(); } catch(Exception ee){}
      try{ in.close(); } catch(Exception ee){}
    }
  }
  
  public static void removeVisaPageBreaks( String filename )
  {
    String    line    = null;
    
    try
    {
      // replace 0x0D 0x0C combos with 0x0D 0x0A
      String  inFilename = filename + ".input";
      new File(filename).renameTo( new File(inFilename) );
      
      BufferedReader in  = new BufferedReader( new FileReader(inFilename) );
      BufferedWriter out = new BufferedWriter( new FileWriter(filename) );
      while( (line = in.readLine()) != null ) 
      {
        out.write(line.replace((char)0x0c,(char)0x0a));
        out.newLine();
      }
      out.close();
      in.close();
      new File(inFilename).delete();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.FileTools::removeVisaPageBreaks(" + filename + ")",e.toString());
      System.out.println(e.toString());
    }      
  }
  
  public static void main( String[] args ) 
  {
    try
    {
      if ( args.length == 0 )
      {
        System.out.println("Function required");
      }
      else if ( "removeItfHashValues".equals(args[0]) )
      {
        String outFilename = args.length > 2 ? args[2] : "null";
        FileTools.removeItfHashValues(args[1],outFilename);
      }
      else if ( "fixVisaLineSep".equals(args[0]) )
      {
        String outFilename = args.length > 2 ? args[2] : "null";
        FileTools.fixVisaLineSeparators(args[1],outFilename);
      }
      else if ( "removeVisaPageBreaks".equals(args[0]) )
      {
        FileTools.removeVisaPageBreaks(args[1]);
      }
      else if ( "extractVisaReportText".equals(args[0]) )
      {
        String outFilename = args.length > 2 ? args[2] : "null";
        FileTools.extractVisaReportText(args[1],outFilename);
      }
      else if ( "formatMCReportText".equals(args[0]) )
      {
        String outFilename = args.length > 2 ? args[2] : "null";
        FileTools.formatMCReportText(args[1],outFilename);
      }
      else if ( "mcIpmPreProc".equals(args[0]) )
      {
        String fileSuffix = args.length > 2 ? args[2] : null;
        FileTools.mcIpmPreProc(args[1],fileSuffix);
      }
      else if ( "visaCtfPreProc".equals(args[0]) )
      {
        String fileSuffix = args.length > 2 ? args[2] : null;
        FileTools.visaCtfPreProc(args[1],fileSuffix);
      } else if ("dump".equals(args[0])) {
          FileTools.dump(args[1]);
      }
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
  }
}