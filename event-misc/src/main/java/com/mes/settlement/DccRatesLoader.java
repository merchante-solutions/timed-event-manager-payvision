package com.mes.settlement;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.constants.MesFlatFiles;
import com.mes.database.MesQueryHandlerList;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.support.DateTimeFormatter;

public abstract class DccRatesLoader<T, K> extends SQLJConnectionBase {

	public enum RecordType {
		MC_T057_DETAIL(MesFlatFiles.DEF_TYPE_MC_T057_DETAIL),
		VISA_TC56_TCR0(MesFlatFiles.DEF_TYPE_VISA_TC56_TCR0),
		VISA_TC56_TCR1(MesFlatFiles.DEF_TYPE_VISA_TC56_TCR1);

		private Integer type;

		private RecordType(Integer type) {
			this.type = type;
		}

		public Integer getType() {
			return this.type;
		}
	}

	public abstract void processRateFile(String loadFilename, BufferedReader rateFileReader) throws Exception;

	public abstract List<Calendar> getEffectiveDates(T t) throws Exception;

	protected abstract void cleanData(K data);

	public MesQueryHandlerList getMeSQueryHandler() {
		return new MesQueryHandlerList();
	}

	protected void initializeLoadFileIndex(String loadFilename) {
		Logger log = Logger.getLogger(getClass());
		if ( log.isDebugEnabled()) {
			log.debug("[initializeLoadFileIndex()] - loadFilename="+loadFilename);
		}
		try {
			String updateSql = "BEGIN load_file_index_init( :1 ) \n; END;";
			String sqlKey = "2com.mes.settlement.DccRatesLoader";
			getMeSQueryHandler().executePreparedUpdate(sqlKey, updateSql, loadFilename);
		}
		catch (SQLException e) {
			log.error("[initializeLoadFileIndex()] - SQL Exception attemting to init file index for " + loadFilename);
		}
	}

	public void loadFXRates(String inputFilename) throws Exception {
		Logger log = Logger.getLogger(getClass());
		if ( log.isDebugEnabled()) {
			log.debug("[loadFXRates()] - loadFilename="+inputFilename);
		}
		try (BufferedReader reader = new BufferedReader(new FileReader(inputFilename))) {
			String loadFilename = trimFilename(inputFilename);

			initializeLoadFileIndex(loadFilename);

			log.info("[loadFXRates()] - processing DCC File - " + loadFilename);
			processRateFile(loadFilename, reader);
		}
		catch (Exception e) {
			logEntry("loadFXRates()", e.toString());
		}
	}

	// private method to enable junit testing
	public FlatFileRecord getFlatFileRecord(RecordType recordType) {
		return new FlatFileRecord(recordType.getType());
	}

	protected String trimFilename(String inputFilename) {
		String loadFilename = inputFilename.replace('\\', '/');
		if (loadFilename.indexOf('/') >= 0) {
			loadFilename = loadFilename.substring(loadFilename.lastIndexOf('/') + 1);
		}
		return loadFilename;
	}

	public static void main(String[] args) {
		DccRatesLoader loader = null;
		Logger log = Logger.getLogger(DccRatesLoader.class);
		try {
			Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
			String className = "";
			if ("loadMasterCardRates".equals(args[0])) {
				className = "com.mes.settlement.fx.MasterCardFxRateLoader";
			}
			else if ("loadVisaRates".equals(args[0])) {
				className = "com.mes.settlement.fx.VisaFxRateLoader";
			}
			else {
				System.out.println("Unrecognized command: " + args[0]);
			}

			loader = (DccRatesLoader) Class.forName(className).newInstance();
			loader.connect(true);
			loader.loadFXRates(args[1]);

			Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
			long elapsed = (endTime.getTime() - beginTime.getTime());

			System.out.println("Begin Time: " + String.valueOf(beginTime));
			System.out.println("End Time  : " + String.valueOf(endTime));
			System.out.println("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));
		}
		catch (Exception e) {
			log.error("[main()] - Unexpected exception attempting to " + args[0] + " rate file " + args[1], e);
		}
		finally {
			try {
				loader.cleanUp();
			}
			catch (Exception ee) {
			}
			try {
				OracleConnectionPool.getInstance().cleanUp();
			}
			catch (Exception e) {
			}
		}
		Runtime.getRuntime().exit(0);
	}
}