/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/IpmFileDumper.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2010-07-07 17:39:41 -0700 (Wed, 07 Jul 2010) $
  Version            : $Revision: 17510 $

  Change History:
     See SVN database

  Copyright (C) 2000-2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import com.mes.tools.FileUtils;
import masthead.mastercard.clearingiso.IpmOfflineFile;
import masthead.mastercard.clearingiso.IpmOfflineMessage;
import masthead.mastercard.clearingiso.MicroFocusNetExpressFile;

public class IpmFileDumper 
{
  public static void main( String[] args )
  {
    byte[]              data        = null;
    IpmOfflineFile      ipmFile     = null;
    HashMap             map         = null;
    IpmOfflineMessage   msg         = null;
    List                msgs        = null;
    boolean             msgIdsOnly  = (args.length > 1 && "msgIdsOnly".equals(args[1]));
    
    try
    {
      //generate byte data
      data = FileUtils.getBytesFromFile(args[0]);

      //due to variance in files coming in,
      //there may be a parse exception
      try
      {
        //try accessing the data from static method - REPR
        msgs = MicroFocusNetExpressFile.parseAsList(data);
      }
      catch(Exception e)
      {
        //try this method if the above one fails
        //e.printStackTrace();
        ipmFile = IpmOfflineFile.parseMsgBlock(data);
        msgs    = ipmFile.getMessages();
      }

      //if msgs found, run process
      if(msgs != null)
      {
        Iterator ipmMsgIter = msgs.iterator();

        while(ipmMsgIter.hasNext())
        {
          msg = (IpmOfflineMessage)ipmMsgIter.next();
          if ( msgIdsOnly )
          {
            if ( map == null ) { map = new HashMap(); }
            String msgId = msg.getMTI();
            map.put(msgId,msgId);
          }
          else
          {
            System.out.println(msg.toXml());
          }            
        }
        
        if ( msgIdsOnly && map != null )
        {
          for( Iterator it = map.keySet().iterator(); it.hasNext(); )
          {
            System.out.println( (String)it.next() );
          }
        }          
      }
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }      
    finally
    {
    }
  }
}