package com.mes.settlement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.mes.config.ConfigurationManager;
import com.mes.startup.EventBase;

public class McBulkFileAckProcessor extends EventBase {

	/**
	 * Default serial ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger log
	 */
	private static Logger log = Logger.getLogger(McBulkFileAckProcessor.class);
	
	/**
	 * String variable for T067
	 */
	private static String STR_T067 = "T067";
	
	/**
	 * String variable for T068
	 */
	private static String STR_T068 = "T068";
	
	/**
	 * String variable for mc_t067_ack
	 */
	private static String T067_FILE_PREFIX = "mc_t067_ack";
	
	/**
	 * String variable for mc_t068_ack
	 */
	private static String T068_FILE_PREFIX = "mc_t068_ack";
	
	/**
	 * String variable for .dat
	 */
	private static String FILE_EXT = ".dat";

	/**
	 * list of decimal value of the ASCII character to be ignored while reading the file.
	 * 0 - ^@ Null (NUL)
	 * 1 - ^A Start of heading (SOH)
	 * 3 - ^C End of text (ETX)
	 * 27 - ^[ Escape (ESC)
	 */
	List<Integer> ignoreCharList = new ArrayList<Integer>(Arrays.asList(0, 1, 3, 27));

	/**
	 * ConfigurationManager configManager
	 */
	private static ConfigurationManager configManager = null;

	/**
	 * This method will process the IPM MPE Daily Update file (T067) and create acknowledgement file mc_t067_ack*.dat
	 * @param inputFileName
	 * @param outputFileName
	 * @throws Exception
	 */
	public void loadT067File(String inputFileName, String outputFileName) throws Exception {
		log.info("processing IPM MPE Daily Update file (T067) name: " + inputFileName);

		StringBuilder strContent = new StringBuilder();
		try (FileInputStream fileInputStream = new FileInputStream(inputFileName);
				InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
				FileOutputStream out = new FileOutputStream(outputFileName)) {

			int singleCharInt;
			boolean startReading = false;
			boolean endOfText = false;
			while ((singleCharInt = inputStreamReader.read()) != -1 && !endOfText) {
				if (singleCharInt == 27) {
					startReading = true;
					continue;
				}
				if (singleCharInt == 3) {
					endOfText = true;
				}

				if (startReading && !ignoreCharList.contains(singleCharInt)) {
					char singleChar = (char) singleCharInt;
					strContent.append(singleChar);
				}
			}
			String finalOutput = StringUtils.rightPad(strContent.toString(), 125);
			if(!StringUtils.isEmpty(finalOutput)) {
				out.write(finalOutput.getBytes());
				log.info("Output ack file genereated: " + outputFileName);
			} else {
				log.error("Ouput ack file content is empty");
			}
		}
	}

	/**
	 * This method will process the IPM MPE Full replacement file (T068) and create acknowledgement file mc_t068_ack*.dat
	 * @param inputFileName
	 * @param outputFileName
	 * @throws Exception
	 */
	public void loadT068File(String inputFileName, String outputFileName) throws Exception {
		log.info("processing IPM MPE Full replacement file (T068) name: " + inputFileName);

		StringBuilder strContent = new StringBuilder();
		try (FileInputStream fileInputStream = new FileInputStream(inputFileName);
				InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
				FileOutputStream out = new FileOutputStream(outputFileName)) {

			int singleCharInt;
			boolean endOfText = false;
			while ((singleCharInt = inputStreamReader.read()) != -1 && !endOfText) {
				if (singleCharInt == 3) {
					endOfText = true;
				}

				if (!ignoreCharList.contains(singleCharInt)) {
					char singleChar = (char) singleCharInt;
					strContent.append(singleChar);
				}
			}
			String finalOutput = StringUtils.rightPad(strContent.toString(), 125).replaceAll("/", " ");
			if(!StringUtils.isEmpty(finalOutput)) {
				out.write(finalOutput.getBytes());
				log.info("Output ack file genereated: " + outputFileName);
			} else {
				log.error("Ouput ack file content is empty");
			}
		}
	}

	/**
	 * This class will read the IPM MPE daily update/full replacement and create the acknowledgement file which needs to be sent to MC.
	 * @param args
	 */
	public static void main(String[] args) {
		McBulkFileAckProcessor loader = null;
		String fileType = null;
		String inputFileName = null;

		try {
			if (args != null) {
				loader = new McBulkFileAckProcessor();
				configManager = ConfigurationManager.getInstance();
				configManager.loadTEMConfiguration(ConfigurationManager.TEM_PROPERTY_FILE_DEFAULT);

				fileType = args[0];
				inputFileName = null;

				if (STR_T067.equals(fileType) || STR_T068.equals(fileType)) {

					if (args.length > 1) {
						inputFileName = args[1];
					}
					else {
						log.error("Input file name cannot be empty, Please provide IPM MPE Daily Update or Full Replacement file name");
						System.exit(-1);
					}

					loader.processFile(inputFileName, fileType);
				}
				else {
					log.error("Invalid File Type. Valid file types are T067 and T068");
				}
			}
			else {
				log.error("Please provide valid args. Arguments valid are <fileType> <inputFileName>");
			}
		}
		catch (Exception e) {
			log.error("Exception occured: " + e);
		}
		finally {
			Runtime.getRuntime().exit(0);
		}
	}

	/**
	 * This method will process the incoming file and call respective method based on the filetype.
	 * @param inputFileName
	 * @param fileType
	 * @throws Exception
	 */
	private void processFile(String inputFileName, String fileType) throws Exception {
		log.debug("inside processFile method, inputFileName: " + inputFileName);
		log.debug("fileType: " + fileType);

		String inputFilePath = null;
		String outputFilePath = null;
		String outputFileName = null;

		try {

			inputFilePath = configManager.getString("IPM_MPE_FILE_INCOMING_PATH");
			outputFilePath = configManager.getString("IPM_MPE_ACK_FILE_OUTGOING_PATH");
			
			log.debug("input file path IPM_MPE_FILE_INCOMING_PATH from te.properties: " + inputFilePath);
			log.debug("output file path IPM_MPE_ACK_FILE_OUTGOING_PATH from te.properties: " + outputFilePath);


			if (!StringUtils.isEmpty(inputFilePath)) {
				inputFileName = inputFilePath + File.separator + inputFileName;
			}

			if (STR_T067.equals(fileType)) {
				outputFileName = generateFilename(T067_FILE_PREFIX, FILE_EXT);
				if (!StringUtils.isEmpty(outputFilePath)) {
					outputFileName = outputFilePath + File.separator + outputFileName;
				}
				loadT067File(inputFileName, outputFileName);
			}
			else {
				outputFileName = generateFilename(T068_FILE_PREFIX, FILE_EXT);
				if (!StringUtils.isEmpty(outputFilePath)) {
					outputFileName = outputFilePath + File.separator + outputFileName;
				}
				loadT068File(inputFileName, outputFileName);
			}

		}
		catch (Exception e) {
			log.error("Exception occured in processFile: " + e);
			throw e;
		}

	}

}
