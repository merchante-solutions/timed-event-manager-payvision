/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/FileTotalsRecord.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-12-11 09:46:45 -0800 (Fri, 11 Dec 2009) $
  Version            : $Revision: 16808 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

public class FileTotalsRecord
{
  protected int       BatchCount      = 0;
  protected double    HashAmount      = 0.0;
  protected int       TranCount       = 0;
  
  public FileTotalsRecord()
  {
  }
  
  public FileTotalsRecord( int batchCount, int tranCount, double hashAmount )
  {
    BatchCount  = batchCount;
    TranCount   = tranCount;
    HashAmount  = hashAmount;
  }
  
  public void   addToTotals( int batchCount, int tranCount, double hashAmount )
  {
    BatchCount  += batchCount;
    TranCount   += tranCount;
    HashAmount  += hashAmount;
  }
  
  public int    getBatchCount()             { return(BatchCount); }
  public void   setBatchCount(int value)    { BatchCount = value; }
  
  public int    getTranCount()              { return(TranCount); }
  public void   setTranCount(int value)     { TranCount = value; }
  
  public double getHashAmount()             { return(HashAmount); }
  public void   setHashAmount(double value) { HashAmount = value; }
}
