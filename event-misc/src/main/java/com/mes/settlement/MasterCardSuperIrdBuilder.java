/*@lineinfo:filename=MasterCardSuperIrdBuilder*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/MasterCardSuperIrdBuilder.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-07-25 09:15:41 -0700 (Thu, 25 Jul 2013) $
  Version            : $Revision: 21336 $

  Change History:
     See SVN database

  Copyright (C) 2000-2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.StringUtilities;
import sqlj.runtime.ResultSetIterator;

public class MasterCardSuperIrdBuilder
  extends FieldBean
{
  protected     Timestamp             EffectiveTimestamp    = null;
  protected     String                EligibleSicCodes      = null;
  protected     HashMap               GpidAndAlmMap         = new HashMap();
  
  public MasterCardSuperIrdBuilder()
  {
  }
  
  public void buildBsaTables()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:62^7*/

//  ************************************************************
//  #sql [Ctx] { insert into mc_ep_ip0090t1
//          (
//            effective_timestamp,
//            active_inactive_code,
//            table_id,
//            issuer_account_range_low,
//            bsa_type,
//            bsa_id_code,
//            card_program_id,
//            issuer_account_range_high,
//            bsa_type_priority,
//            card_program_id_priority,
//            bsa_life_cycle_indicator,
//            bss_enforcement_indicator
//          )
//          select  to_date(substr(raw_data,1,10),
//                          'yyyymmddhh24')   as effective_timestamp,
//                  substr(raw_data,11,1)     as active_inactive_code,
//                  substr(raw_data,12,8)     as table_id,
//                  substr(raw_data,20,19)    as issuer_account_range_low,
//                  substr(raw_data,39,1)     as bsa_type,
//                  substr(raw_data,40,6)     as bsa_id_code,
//                  substr(raw_data,46,3)     as card_program_id,
//                  substr(raw_data,49,19)    as issuer_account_range_high,
//                  substr(raw_data,68,2)     as bsa_type_priority,
//                  substr(raw_data,70,2)     as card_program_id_priority,
//                  substr(raw_data,72,1)     as bsa_life_cycle_indicator,
//                  substr(raw_data,73,1)     as bss_enforcement_indicator
//          from    mc_ep_table_data  ep
//          where   ep.table_id = 'IP0090T1'
//                  and substr(ep.raw_data,46,3) in ( 'DMC','MCC' )
//                  and substr(ep.raw_data,39,7) in 
//                      ( select  distinct substr(ep91.raw_data,26,7)
//                        from    mc_ep_table_data  ep91
//                        where   ep91.table_id = 'IP0091T1'
//                                and substr(ep91.raw_data,33,3) in ( 'DMC','MCC' )
//                                and to_number(substr(ep91.raw_data,20,6)) in
//                                    ( select distinct mc_bin_inet from mbs_banks ))
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mc_ep_ip0090t1\n        (\n          effective_timestamp,\n          active_inactive_code,\n          table_id,\n          issuer_account_range_low,\n          bsa_type,\n          bsa_id_code,\n          card_program_id,\n          issuer_account_range_high,\n          bsa_type_priority,\n          card_program_id_priority,\n          bsa_life_cycle_indicator,\n          bss_enforcement_indicator\n        )\n        select  to_date(substr(raw_data,1,10),\n                        'yyyymmddhh24')   as effective_timestamp,\n                substr(raw_data,11,1)     as active_inactive_code,\n                substr(raw_data,12,8)     as table_id,\n                substr(raw_data,20,19)    as issuer_account_range_low,\n                substr(raw_data,39,1)     as bsa_type,\n                substr(raw_data,40,6)     as bsa_id_code,\n                substr(raw_data,46,3)     as card_program_id,\n                substr(raw_data,49,19)    as issuer_account_range_high,\n                substr(raw_data,68,2)     as bsa_type_priority,\n                substr(raw_data,70,2)     as card_program_id_priority,\n                substr(raw_data,72,1)     as bsa_life_cycle_indicator,\n                substr(raw_data,73,1)     as bss_enforcement_indicator\n        from    mc_ep_table_data  ep\n        where   ep.table_id = 'IP0090T1'\n                and substr(ep.raw_data,46,3) in ( 'DMC','MCC' )\n                and substr(ep.raw_data,39,7) in \n                    ( select  distinct substr(ep91.raw_data,26,7)\n                      from    mc_ep_table_data  ep91\n                      where   ep91.table_id = 'IP0091T1'\n                              and substr(ep91.raw_data,33,3) in ( 'DMC','MCC' )\n                              and to_number(substr(ep91.raw_data,20,6)) in\n                                  ( select distinct mc_bin_inet from mbs_banks ))";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:102^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:104^7*/

//  ************************************************************
//  #sql [Ctx] { insert into mc_ep_ip0091t1
//          (
//            effective_timestamp,
//            active_inactive_code,
//            table_id,
//            acquirer_bin,
//            bsa_type,
//            bsa_id_code,
//            card_program_id,
//            bsa_type_priority,
//            bsa_life_cycle_indicator
//          )
//          select  to_date(substr(raw_data,1,10),
//                          'yyyymmddhh24')   as effective_timestamp,
//                  substr(raw_data,11,1)     as active_inactive_code,
//                  substr(raw_data,12,8)     as table_id,
//                  substr(raw_data,20,6)     as acquirer_bin,
//                  substr(raw_data,26,1)     as bsa_type,
//                  substr(raw_data,27,6)     as bsa_id_code,
//                  substr(raw_data,33,3)     as card_program_id,
//                  substr(raw_data,36,2)     as bsa_type_priority,
//                  substr(raw_data,38,1)     as bsa_life_cycle_indicator
//          from    mc_ep_table_data  ep
//          where   ep.table_id = 'IP0091T1'
//                  and substr(ep.raw_data,33,3) in ( 'DMC','MCC' )
//                  and to_number(substr(ep.raw_data,20,6)) in
//                      ( select distinct mc_bin_inet from mbs_banks )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mc_ep_ip0091t1\n        (\n          effective_timestamp,\n          active_inactive_code,\n          table_id,\n          acquirer_bin,\n          bsa_type,\n          bsa_id_code,\n          card_program_id,\n          bsa_type_priority,\n          bsa_life_cycle_indicator\n        )\n        select  to_date(substr(raw_data,1,10),\n                        'yyyymmddhh24')   as effective_timestamp,\n                substr(raw_data,11,1)     as active_inactive_code,\n                substr(raw_data,12,8)     as table_id,\n                substr(raw_data,20,6)     as acquirer_bin,\n                substr(raw_data,26,1)     as bsa_type,\n                substr(raw_data,27,6)     as bsa_id_code,\n                substr(raw_data,33,3)     as card_program_id,\n                substr(raw_data,36,2)     as bsa_type_priority,\n                substr(raw_data,38,1)     as bsa_life_cycle_indicator\n        from    mc_ep_table_data  ep\n        where   ep.table_id = 'IP0091T1'\n                and substr(ep.raw_data,33,3) in ( 'DMC','MCC' )\n                and to_number(substr(ep.raw_data,20,6)) in\n                    ( select distinct mc_bin_inet from mbs_banks )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:133^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:135^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:138^7*/
    }
    catch( Exception e )
    {
      logEntry("buildBsaTables()",e.toString());
    }
    finally
    {
    }
  }
  
  public void buildSuperIrd()
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    int recCount = 0;//@
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:158^7*/

//  ************************************************************
//  #sql [Ctx] it = { with bsa as ( select  distinct substr(ep91.raw_data,26,7) as bsa_arrangement
//                        from    mc_ep_table_data  ep91
//                        where   ep91.table_id = 'IP0091T1'
//                                and substr(ep91.raw_data,33,3) in ( 'DMC','MCC' )
//                                and to_number(substr(ep91.raw_data,20,6)) in
//                                    ( select distinct mc_bin_inet from mbs_banks ))
//          select  to_date(substr(epd.raw_data,1,10),'yyyymmddhh24mi') 
//                                                as effective_timestamp,
//                  to_date(substr(epd.raw_data,1,8),'yyyymmdd')                                               
//                                                as effective_date,
//                  substr(epd.raw_data,11,1)     as active_inactive_code,
//                  substr(epd.raw_data,12,8)     as table_id,
//                  substr(epd.raw_data,20,3)     as card_program_id,
//                  substr(epd.raw_data,23,1)     as bsa_type,
//                  substr(epd.raw_data,24,6)     as bsa_id_code,
//                  substr(epd.raw_data,30,2)     as ird,
//                  substr(epd.raw_data,32,4)     as message_type_identifier,
//                  substr(epd.raw_data,36,3)     as function_code,
//                  substr(epd.raw_data,39,2)     as processing_code,
//                  substr(epd.raw_data,45,1)     as reversal_allowed,
//                  substr(epd.raw_data,46,1)     as ic_compliance_switch,
//                  substr(epd.raw_data,47,11)    as ptr53_fee_code,
//                  substr(epd.raw_data,58,1)     as product_type_id,
//                  substr(epd.raw_data,59,11)    as ptr59_timeliness_auth_code,
//                  substr(epd.raw_data,70,11)    as ptr57_fee_override,
//                  substr(epd.raw_data,81,2)     as mc_assigned_id_reqd_validated
//          from    mc_ep_table_data epd, bsa
//          where   table_id = 'IP0052T1'
//                  and substr(raw_data,20,3) in ('DMC','MCC')
//                  and substr(raw_data,23,7) = bsa.bsa_arrangement
//                  and substr(raw_data,32,7) = '1240200'   -- msg 1240, func 200
//                  and substr(raw_data,39,2) in ('00','09','12','18','20','28')
//          order by  ird,
//                    processing_code,
//                    card_program_id,
//                    bsa_type,bsa_id_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "with bsa as ( select  distinct substr(ep91.raw_data,26,7) as bsa_arrangement\n                      from    mc_ep_table_data  ep91\n                      where   ep91.table_id = 'IP0091T1'\n                              and substr(ep91.raw_data,33,3) in ( 'DMC','MCC' )\n                              and to_number(substr(ep91.raw_data,20,6)) in\n                                  ( select distinct mc_bin_inet from mbs_banks ))\n        select  to_date(substr(epd.raw_data,1,10),'yyyymmddhh24mi') \n                                              as effective_timestamp,\n                to_date(substr(epd.raw_data,1,8),'yyyymmdd')                                               \n                                              as effective_date,\n                substr(epd.raw_data,11,1)     as active_inactive_code,\n                substr(epd.raw_data,12,8)     as table_id,\n                substr(epd.raw_data,20,3)     as card_program_id,\n                substr(epd.raw_data,23,1)     as bsa_type,\n                substr(epd.raw_data,24,6)     as bsa_id_code,\n                substr(epd.raw_data,30,2)     as ird,\n                substr(epd.raw_data,32,4)     as message_type_identifier,\n                substr(epd.raw_data,36,3)     as function_code,\n                substr(epd.raw_data,39,2)     as processing_code,\n                substr(epd.raw_data,45,1)     as reversal_allowed,\n                substr(epd.raw_data,46,1)     as ic_compliance_switch,\n                substr(epd.raw_data,47,11)    as ptr53_fee_code,\n                substr(epd.raw_data,58,1)     as product_type_id,\n                substr(epd.raw_data,59,11)    as ptr59_timeliness_auth_code,\n                substr(epd.raw_data,70,11)    as ptr57_fee_override,\n                substr(epd.raw_data,81,2)     as mc_assigned_id_reqd_validated\n        from    mc_ep_table_data epd, bsa\n        where   table_id = 'IP0052T1'\n                and substr(raw_data,20,3) in ('DMC','MCC')\n                and substr(raw_data,23,7) = bsa.bsa_arrangement\n                and substr(raw_data,32,7) = '1240200'   -- msg 1240, func 200\n                and substr(raw_data,39,2) in ('00','09','12','18','20','28')\n        order by  ird,\n                  processing_code,\n                  card_program_id,\n                  bsa_type,bsa_id_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.settlement.MasterCardSuperIrdBuilder",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:196^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        clear();                          // clear previous data
        setEffectiveTimestamp(resultSet.getTimestamp("effective_timestamp"));
        setFields(resultSet,false,false); // set field data
        
        loadMaskedValues();   // load CPI/BSA & IRD combo (table 87)
        loadGCMSPIdAndALMCodes();  // load GCMS product id (table 96) and GPID/ALM codes (table 19)
//@        loadSicCodes();       // load sic codes using CAB program (table 75)
        loadTimelinessCodes();// load the auth to clearing timeliness data (table 59)
        loadFleetAtFuelFlag();// load the fleet card at fuel locations flag (mc_ic_program_rules)
        
        storeSuperIrdRecord();
        
        ++recCount;
        System.out.print("Processing record # " + recCount + "                  \r");//@
//@        showData();//@
//@        if ( recCount > 10 ) break;//@
      }
      resultSet.close();
      
      System.out.println();//@
    }
    catch( Exception e )
    {
      logEntry("buildSuperIrd()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  protected void cleanBsaTables()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:236^7*/

//  ************************************************************
//  #sql [Ctx] { truncate table mes.mc_ep_ip0090t1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "truncate table mes.mc_ep_ip0090t1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:239^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:241^7*/

//  ************************************************************
//  #sql [Ctx] { truncate table mes.mc_ep_ip0091t1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "truncate table mes.mc_ep_ip0091t1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:244^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:246^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:249^7*/
    }
    catch( Exception e )
    {
      logEntry("cleanBsaTables()",e.toString());
    }
  }
  
  protected void cleanSuperIrd()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:261^7*/

//  ************************************************************
//  #sql [Ctx] { truncate table mes.mc_super_ird
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "truncate table mes.mc_super_ird";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:264^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:266^7*/

//  ************************************************************
//  #sql [Ctx] { truncate table mes.mc_super_ird_sic_codes
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "truncate table mes.mc_super_ird_sic_codes";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:269^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:271^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:274^7*/
    }
    catch( Exception e )
    {
      logEntry("cleanSuperIrd()",e.toString());
    }
  }
  
  public void clear()
  {
    EffectiveTimestamp = null;
    EligibleSicCodes   = null;
    GpidAndAlmMap.clear();
    fields.resetDefaults();
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    FieldGroup fgroup = new FieldGroup("superIrdFields");
    fields.add(fgroup);
    
    //------------------------------------------------------------------------------------------------------------------------
    //                                fname                       label                  len   size    null ok?   extra...
    //------------------------------------------------------------------------------------------------------------------------
//@    fgroup.add( new Field         ( "effective_timestamp
    fgroup.add( new DateField     ( "effective_date"                , ""                            , false  ) );
    fgroup.add( new Field         ( "table_id"                      , ""            ,  8    , 8     , false  ) );
    fgroup.add( new Field         ( "active_inactive_code"          , ""            ,  1    , 1     , false  ) );
    fgroup.add( new Field         ( "card_program_id"               , ""            ,  3    , 3     , false  ) );
    fgroup.add( new NumberField   ( "bsa_type"                      , ""            ,  1    , 1     , false  , 0 ) );
    fgroup.add( new Field         ( "bsa_id_code"                   , ""            ,  6    , 6     , false  ) );
    fgroup.add( new Field         ( "ird"                           , ""            ,  2    , 2     , false  ) );
    fgroup.add( new Field         ( "eligible_gcms_product_ids"     , ""            ,  400  , 400   , false  ) );
    fgroup.add( new Field         ( "reqd_alm_code"                 , ""            ,  96   , 96    , false  ) );
    fgroup.add( new Field         ( "gcms_timeliness"               , ""            ,  32   , 32    , false  ) );
    fgroup.add( new Field         ( "gcms_auth_reqd"                , ""            ,  8    , 8     , false  ) );
    fgroup.add( new Field         ( "icc_timeliness"                , ""            ,  32   , 32    , false  ) );
    fgroup.add( new Field         ( "icc_auth_reqd"                 , ""            ,  8    , 8     , false  ) );
    fgroup.add( new NumberField   ( "message_type_identifier"       , ""            ,  4    , 4     , false  , 0 ) );
    fgroup.add( new NumberField   ( "function_code"                 , ""            ,  3    , 3     , false  , 0 ) );
    fgroup.add( new NumberField   ( "processing_code"               , ""            ,  2    , 2     , false  , 0 ) );
    fgroup.add( new Field         ( "reversal_allowed"              , ""            ,  1    , 1     , false  ) );
    fgroup.add( new Field         ( "ic_compliance_switch"          , ""            ,  1    , 1     , false  ) );
    fgroup.add( new NumberField   ( "ptr53_fee_code"                , ""            ,  11   , 11    , false  , 0 ) );
    fgroup.add( new NumberField   ( "product_type_id"               , ""            ,  1    , 1     , false  , 0 ) );
    fgroup.add( new NumberField   ( "ptr59_timeliness_auth_code"    , ""            ,  11   , 11    , false  , 0 ) );
    fgroup.add( new NumberField   ( "ptr57_fee_override"            , ""            ,  11   , 11    , false  , 0 ) );
    fgroup.add( new Field         ( "mc_assigned_id_reqd_validated" , ""            ,  2    , 2     , false  ) );
    fgroup.add( new Field         ( "fleet_at_fuel"                 , ""            ,  1    , 1     , false  ) );
    
    // not stored in database
    fgroup.add( new HiddenField   ( "masked_cpid" ) );
    fgroup.add( new HiddenField   ( "masked_bsa_type" ) );
    fgroup.add( new HiddenField   ( "masked_bsa_id_code" ) );
    fgroup.add( new HiddenField   ( "masked_ird" ) );
  }
  
  public String getBsa()
  {
    StringBuffer retVal = new StringBuffer(12);
    retVal.append( StringUtilities.leftJustify(getBsaType(),1,' ') );
    retVal.append( StringUtilities.leftJustify(getBsaIdCode(),6,' ') );
    return( retVal.toString() );
  }
  
  public String getBsaIdCode()
  {
    return( getData("bsa_id_code") );
  }
  
  public String getBsaType()
  {
    return( getData("bsa_type") );
  }
  
  public String getCardProgramId()
  {
    return( getData("card_program_id") );
  }
  
  public String getIrd() 
  {
    return( getData("ird") );
  }
  
  public String getMaskedBsaIdCode()
  {
    return( getMaskedField("masked_bsa_id_code","bsa_id_code") );
  }
  
  public String getMaskedBsaType()
  {
    return( getMaskedField("masked_bsa_type","bsa_type") );
  }
  
  public String getMaskedCardProgramId()
  {
    return( getMaskedField("masked_cpid","card_program_id") );
  }
  
  public String getMaskedIrd() 
  {
    return( getMaskedField("masked_ird","ird") );
  }
  
  protected String getMaskedField( String maskedFieldName, String fieldName )
  {
    return( isFieldBlank(maskedFieldName) ? 
              getData(fieldName) : getData(maskedFieldName) );
  }
  
  protected String getCpiBsaIrdCombo()
  {
    StringBuffer retVal = new StringBuffer(12);
    retVal.append( StringUtilities.leftJustify(getCardProgramId(),3,' ') );
    retVal.append( StringUtilities.leftJustify(getBsaType(),1,' ') );
    retVal.append( StringUtilities.leftJustify(getBsaIdCode(),6,' ') );
    retVal.append( StringUtilities.leftJustify(getIrd(),2,' ') );
    
    return( retVal.toString() );
  }
  
  protected String getMaskedCpiBsaIrdCombo()
  {
    StringBuffer retVal = new StringBuffer(12);
    retVal.append( StringUtilities.leftJustify(getMaskedCardProgramId(),3,' ') );
    retVal.append( StringUtilities.leftJustify(getMaskedBsaType(),1,' ') );
    retVal.append( StringUtilities.leftJustify(getMaskedBsaIdCode(),6,' ') );
    retVal.append( StringUtilities.leftJustify(getMaskedIrd(),2,' ') );
    
    return( retVal.toString() );
  }
  
  public boolean isCashDisbursement()
  {
    return ( "  ".equals( StringUtilities.leftJustify(getIrd(), 2, ' ') ) &&
             "12".equals( getData("processing_code") ) );
  }
  
  protected void loadGCMSPIdAndALMCodes()
  {
    String              keyString19 = null;
    String              keyString96 = null;
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      System.out.print("loadGCMSPIdAndALMCodes\r");//@
      keyString19 = getCpiBsaIrdCombo();
      keyString96 = getMaskedCpiBsaIrdCombo();
      
      /*@lineinfo:generated-code*//*@lineinfo:428^7*/

//  ************************************************************
//  #sql [Ctx] it = { with gpid as 
//          (  
//            select  distinct substr(epd.raw_data,32,3)    as gcms_pid,  96 as epdtable  
//            from    mc_ep_table_data  epd  
//            where   table_id = 'IP0096T1'  
//                    and substr(raw_data,20,12) = :keyString96  
//                    and substr(raw_data,35,1) = 'A'     -- "A"ll not "L"ife cycle only 
//            union 
//            select  distinct substr(epd.raw_data,32,3)    as gcms_pid,  19 as epdtable 
//            from    mc_ep_table_data  epd  
//            where   table_id = 'IP0019T1'  
//                    and substr(raw_data,20,12) = :keyString19  
//                    and substr(raw_data,30, 2) != '  '      -- IRD is not space-space
//                    and substr(epd.raw_data,32,3) != '   '  -- LPID is not space-space-space
//                    and not exists( select  raw_data        -- LPID is not in table 96
//                                    from    mc_ep_table_data 
//                                    where   table_id = 'IP0096T1' 
//                                            and substr(raw_data,20,12) = :keyString96  
//                                            and substr(raw_data,35,1) = 'A'     -- "A"ll not "L"ife cycle only 
//                                            and substr(raw_data,32,3) = substr(epd.raw_data,32,3) 
//                                  ) 
//          ),  
//          gpidalm as  
//          (   
//            select  substr(epd.raw_data,32,3)             as gcms_pid,   
//                    substr(epd.raw_data,35,1)             as alm_code   
//            from    mc_ep_table_data  epd   
//            where   table_id = 'IP0019T1'   
//                    and substr(raw_data,20,12) = :keyString19   
//                    and substr(raw_data,30, 2) != '  '   
//            union  
//            select  '   '                                 as gcms_pid, 
//                    null                                  as alm_code 
//            from    dual 
//            where       not exists( select  raw_data        -- keyString19 is not in table 19 
//                                    from    mc_ep_table_data  epd   
//                                    where   table_id = 'IP0019T1'   
//                                            and substr(raw_data,20,12) = :keyString19   
//                                            and substr(raw_data,30, 2) != '  '   
//                                  )  
//          )         
//          select gpid.gcms_pid, gpidalm.alm_code  
//          from   gpid, gpidalm  
//          where  gpid.gcms_pid = gpidalm.gcms_pid 
//                  or (gpidalm.gcms_pid = '   ' and gpid.epdtable = 96)  
//          order by gpid.gcms_pid, gpidalm.alm_code       
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "with gpid as \n        (  \n          select  distinct substr(epd.raw_data,32,3)    as gcms_pid,  96 as epdtable  \n          from    mc_ep_table_data  epd  \n          where   table_id = 'IP0096T1'  \n                  and substr(raw_data,20,12) =  :1   \n                  and substr(raw_data,35,1) = 'A'     -- \"A\"ll not \"L\"ife cycle only \n          union \n          select  distinct substr(epd.raw_data,32,3)    as gcms_pid,  19 as epdtable \n          from    mc_ep_table_data  epd  \n          where   table_id = 'IP0019T1'  \n                  and substr(raw_data,20,12) =  :2   \n                  and substr(raw_data,30, 2) != '  '      -- IRD is not space-space\n                  and substr(epd.raw_data,32,3) != '   '  -- LPID is not space-space-space\n                  and not exists( select  raw_data        -- LPID is not in table 96\n                                  from    mc_ep_table_data \n                                  where   table_id = 'IP0096T1' \n                                          and substr(raw_data,20,12) =  :3   \n                                          and substr(raw_data,35,1) = 'A'     -- \"A\"ll not \"L\"ife cycle only \n                                          and substr(raw_data,32,3) = substr(epd.raw_data,32,3) \n                                ) \n        ),  \n        gpidalm as  \n        (   \n          select  substr(epd.raw_data,32,3)             as gcms_pid,   \n                  substr(epd.raw_data,35,1)             as alm_code   \n          from    mc_ep_table_data  epd   \n          where   table_id = 'IP0019T1'   \n                  and substr(raw_data,20,12) =  :4    \n                  and substr(raw_data,30, 2) != '  '   \n          union  \n          select  '   '                                 as gcms_pid, \n                  null                                  as alm_code \n          from    dual \n          where       not exists( select  raw_data        -- keyString19 is not in table 19 \n                                  from    mc_ep_table_data  epd   \n                                  where   table_id = 'IP0019T1'   \n                                          and substr(raw_data,20,12) =  :5    \n                                          and substr(raw_data,30, 2) != '  '   \n                                )  \n        )         \n        select gpid.gcms_pid, gpidalm.alm_code  \n        from   gpid, gpidalm  \n        where  gpid.gcms_pid = gpidalm.gcms_pid \n                or (gpidalm.gcms_pid = '   ' and gpid.epdtable = 96)  \n        order by gpid.gcms_pid, gpidalm.alm_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,keyString96);
   __sJT_st.setString(2,keyString19);
   __sJT_st.setString(3,keyString96);
   __sJT_st.setString(4,keyString19);
   __sJT_st.setString(5,keyString19);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.settlement.MasterCardSuperIrdBuilder",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:476^7*/
      resultSet = it.getResultSet();
      
      String fdataGPID  = "";
      String fdataALM   = "";
      String value      = "";
      if ( resultSet.next() )
      {
        fdataGPID = resultSet.getString("gcms_pid");
        do
        {
          if( !fdataGPID.equals(resultSet.getString("gcms_pid")) )
          {
            // store the fdata bpid/alm we have built up in the vector; either append this gpid to existing OR insert new gpid/alm pair
            if ( (value = (String)GpidAndAlmMap.get(fdataALM)) == null )
            {
              GpidAndAlmMap.put(fdataALM,fdataGPID);
            }
            else
            {
              value += ("," + fdataGPID);
              GpidAndAlmMap.put(fdataALM,value);
            }
            
            // reset values
            fdataALM  = "";
            fdataGPID = resultSet.getString("gcms_pid");
          }
          if ( resultSet.getString("alm_code") != null )
          {
            fdataALM += (fdataALM.length() > 0 ? "," : "") + resultSet.getString("alm_code");
          }            
        } while ( resultSet.next() );
      }
      resultSet.close();
      it.close();

      // store the last info (boundary condition)
      if ( (value = (String)GpidAndAlmMap.get(fdataALM)) == null )
      {
        GpidAndAlmMap.put(fdataALM,fdataGPID);
      }
      else
      {
        value += ("," + fdataGPID);
        GpidAndAlmMap.put(fdataALM,value);
      }
    }
    catch( Exception e )
    {
      logEntry("loadGCMSPIdAndALMCodes(" + keyString19 + "," + keyString96 + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  protected void loadFleetAtFuelFlag()
  {
    String              keyString   = null;
    int                 recCount    = 0;
    
    try
    {
      System.out.print("loadFleetAtFuelFlag\r");//@
      
      keyString = getIrd();

      if( "1".equals(getData("product_type_id")) )
      {
        // all Consumer IRDs are "N"
      }
      else
      {
        String bsa = getBsa();

        // all USA Intra-Country Commercial IRDs are in mc_ic_program_rules with a fleet-at-fuel flag of Y or N or null
        // -the four credit/return-only IRDs (39,40,41,42) have no fleet-at-fuel restrictions
        // -the  one purchase-and-return IRD (82) does not allow fleet cards

        // all Inter-Regional Commercial IRDs are in mc_ic_program_rules with a fleet-at-fuel flag
        // (these IRDs are used for both purchases and returns)

        /*@lineinfo:generated-code*//*@lineinfo:560^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)   
//            from    mc_ic_program_rules     rul 
//            where   rul.ird = :keyString 
//                    and 'MB'  =     nvl(rul.card_type       , 'MB') -- null or MB 
//                    and :bsa  like  nvl(rul.bsa_arrangement , :bsa) -- null or this tran's BSA
//                    and 'Y'   =     nvl(rul.fleet_at_fuel   , 'N' ) --         Y
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)    \n          from    mc_ic_program_rules     rul \n          where   rul.ird =  :1  \n                  and 'MB'  =     nvl(rul.card_type       , 'MB') -- null or MB \n                  and  :2   like  nvl(rul.bsa_arrangement ,  :3 ) -- null or this tran's BSA\n                  and 'Y'   =     nvl(rul.fleet_at_fuel   , 'N' ) --         Y";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,keyString);
   __sJT_st.setString(2,bsa);
   __sJT_st.setString(3,bsa);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:568^9*/ 
      }

      setData("fleet_at_fuel", ((recCount>0) ? "Y" : "N"));
    }
    catch( Exception e )
    {
      logEntry("loadFleetAtFuelFlag(" + keyString + ")",e.toString());
    }
    finally
    {
    }
  }
  
  protected void loadHolidays()
  {
    // note: we're only loading the holidays for USA (bsa_arrangement in 101xxxx / 201xxxx);
    //  at present, Brazil has the same holidays AND holidays don't have much affect on Brazilian IRD requirements
    java.util.Date      javaDate    = null;
    String              keyString   = null;
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    Date                sqlDate     = null;
    
    try
    {
      System.out.print("loadHolidays \r");//@
      
      keyString = getBsa();
      
      /*@lineinfo:generated-code*//*@lineinfo:598^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct 
//                  substr(epd.raw_data,34)        holiday_data
//          from    mc_ep_table_data epd
//          where   epd.table_id = 'IP0060T1'
//                  and substr(epd.raw_data,20,3) in ('101','201')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct \n                substr(epd.raw_data,34)        holiday_data\n        from    mc_ep_table_data epd\n        where   epd.table_id = 'IP0060T1'\n                and substr(epd.raw_data,20,3) in ('101','201')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.settlement.MasterCardSuperIrdBuilder",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:605^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        String  holidayData = resultSet.getString("holiday_data");
        
        for( int i = 0; i < 30; ++i )
        {
          javaDate = DateTimeFormatter.parseDate(holidayData.substring((5*i),((5*i)+5)),"yyDDD");
          
          if ( javaDate != null )
          {
            sqlDate = new Date( javaDate.getTime() );
          
            /*@lineinfo:generated-code*//*@lineinfo:620^13*/

//  ************************************************************
//  #sql [Ctx] { update  settlement_excluded_dates
//                set     mc = 'HOL'
//                where   excluded_date = :sqlDate
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  settlement_excluded_dates\n              set     mc = 'HOL'\n              where   excluded_date =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,sqlDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:625^13*/
          
            int updateCount = Ctx.getExecutionContext().getUpdateCount();
            if( updateCount <= 0 )
            {
              /*@lineinfo:generated-code*//*@lineinfo:630^15*/

//  ************************************************************
//  #sql [Ctx] { insert into settlement_excluded_dates
//                    ( excluded_date, mc )
//                  values 
//                    ( :sqlDate, 'HOL' )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into settlement_excluded_dates\n                  ( excluded_date, mc )\n                values \n                  (  :1 , 'HOL' )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,sqlDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:636^15*/
            }
          }
        }
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadHolidays(" + keyString + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  protected void loadMaskedValues()
  {
    String              keyString   = null;
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      System.out.print("loadMaskedValues \r");//@
      
      keyString = getCpiBsaIrdCombo();
      /*@lineinfo:generated-code*//*@lineinfo:665^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  substr(epd.raw_data,32,3)     as masked_cpid,
//                  substr(epd.raw_data,35,1)     as masked_bsa_type,
//                  substr(epd.raw_data,36,6)     as masked_bsa_id_code,
//                  substr(epd.raw_data,42,2)     as masked_ird
//          from    mc_ep_table_data      epd
//          where   epd.table_id = 'IP0087T1'
//                  and substr(epd.raw_data,20,12) = :keyString 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  substr(epd.raw_data,32,3)     as masked_cpid,\n                substr(epd.raw_data,35,1)     as masked_bsa_type,\n                substr(epd.raw_data,36,6)     as masked_bsa_id_code,\n                substr(epd.raw_data,42,2)     as masked_ird\n        from    mc_ep_table_data      epd\n        where   epd.table_id = 'IP0087T1'\n                and substr(epd.raw_data,20,12) =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,keyString);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.settlement.MasterCardSuperIrdBuilder",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:674^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        setFields(resultSet,false,false);
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadMaskedValues(" + keyString + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  protected void loadSicCodes()
  {
    String              keyString   = null;
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      System.out.print("loadSicCodes     \r");//@
    
      EligibleSicCodes = "";
    
      if ( isCashDisbursement() )
      {
        EligibleSicCodes = "6010,6011";
      }
      else
      {
        keyString = getMaskedCpiBsaIrdCombo();
        /*@lineinfo:generated-code*//*@lineinfo:713^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ordered */
//                    distinct
//                    substr(sic.raw_data,21,4)         as sic_code
//            from    mc_ep_table_data    epd,
//                    mc_ep_table_data    sic
//            where   epd.table_id = 'IP0095T1'
//                    and substr(epd.raw_data,20,12) = :keyString
//                    and substr(epd.raw_data,36,1) = 'A'     -- "A"ll not "L"ife cycle only
//                    and sic.table_id = 'IP0075T1'
//                    and substr(sic.raw_data,29,1) = 'A'     -- "A"ll not "L"ife cycle only
//                    and substr(sic.raw_data,31,1) = 'A'     -- "A"ctive not "O"bsolete
//                    and substr(sic.raw_data,25,4) = substr(epd.raw_data,32,4)
//            order by sic_code                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ ordered */\n                  distinct\n                  substr(sic.raw_data,21,4)         as sic_code\n          from    mc_ep_table_data    epd,\n                  mc_ep_table_data    sic\n          where   epd.table_id = 'IP0095T1'\n                  and substr(epd.raw_data,20,12) =  :1 \n                  and substr(epd.raw_data,36,1) = 'A'     -- \"A\"ll not \"L\"ife cycle only\n                  and sic.table_id = 'IP0075T1'\n                  and substr(sic.raw_data,29,1) = 'A'     -- \"A\"ll not \"L\"ife cycle only\n                  and substr(sic.raw_data,31,1) = 'A'     -- \"A\"ctive not \"O\"bsolete\n                  and substr(sic.raw_data,25,4) = substr(epd.raw_data,32,4)\n          order by sic_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,keyString);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.settlement.MasterCardSuperIrdBuilder",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:728^9*/
        resultSet = it.getResultSet();
      
        while ( resultSet.next() )
        {
          EligibleSicCodes += (EligibleSicCodes.length() > 0 ? "," : "") + resultSet.getString("sic_code");
        }
        resultSet.close();
        it.close();
      }        
    }
    catch( Exception e )
    {
      logEntry("loadSicCodes(" + keyString + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  protected void loadTimelinessCodes()
  {
    String              keyString   = null;
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      System.out.print("loadTimeliness   \r");//@

      // load defaults
      setData(  "gcms_timeliness" , "0000NNNN0000NNNN"  );
      setData(  "icc_timeliness"  , "0000NNNN0000NNNN"  );
      setData(  "gcms_auth_reqd"  , "NN"                );
      setData(  "icc_auth_reqd"   , "NN"                );

      keyString = getData("ptr59_timeliness_auth_code");
      
      /*@lineinfo:generated-code*//*@lineinfo:767^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  substr(epd.raw_data,31,1)         as sys_app,
//                  (
//                    substr(epd.raw_data,33,4) ||  -- non_air_days
//                    substr(epd.raw_data,37,4) ||  -- non_air_excl
//                    substr(epd.raw_data,43,4) ||  -- air_days
//                    substr(epd.raw_data,47,4)     -- air_excl
//                  )                                 as days_and_excl,
//                  (
//                    substr(epd.raw_data,41,1) ||  -- non_air_auth_ind
//                    substr(epd.raw_data,51,1)     -- air_auth_ind
//                  )                                 as auth_ind
//          from    mc_ep_table_data  epd
//          where   table_id = 'IP0059T1'
//                  and substr(raw_data,20,11) = :keyString
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  substr(epd.raw_data,31,1)         as sys_app,\n                (\n                  substr(epd.raw_data,33,4) ||  -- non_air_days\n                  substr(epd.raw_data,37,4) ||  -- non_air_excl\n                  substr(epd.raw_data,43,4) ||  -- air_days\n                  substr(epd.raw_data,47,4)     -- air_excl\n                )                                 as days_and_excl,\n                (\n                  substr(epd.raw_data,41,1) ||  -- non_air_auth_ind\n                  substr(epd.raw_data,51,1)     -- air_auth_ind\n                )                                 as auth_ind\n        from    mc_ep_table_data  epd\n        where   table_id = 'IP0059T1'\n                and substr(raw_data,20,11) =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,keyString);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.settlement.MasterCardSuperIrdBuilder",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:783^7*/
      resultSet = it.getResultSet();
      
      String timlinessData  = "";
      String authRequired   = "";
      String sysAppType     = "";
      
      while ( resultSet.next() )
      {
        sysAppType    = resultSet.getString("sys_app");
        timlinessData = resultSet.getString("days_and_excl");
        authRequired  = resultSet.getString("auth_ind");
        
        if ( "1".equals(sysAppType) )
        {
          setData("gcms_timeliness",timlinessData);
          setData("gcms_auth_reqd",authRequired);
        }
        else if ( "2".equals(sysAppType) )
        {
          setData("icc_timeliness",timlinessData);
          setData("icc_auth_reqd",authRequired);
        }          
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadTimelinessCodes(" + keyString + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  public void logEntry(String method, String message)
  {
    try
    {
      MailMessage.sendSystemErrorEmail("Settlement class " + this.getClass().getName() + " failed", method + "\n" + message);
    }
    catch(Exception e)
    {
    }
    super.logEntry(method, message);
  }
  
  protected void setEffectiveTimestamp( Timestamp value )
  {
    EffectiveTimestamp = value;
  }
  
  public void showData()
  {
    Field   f;
    Vector  superIrdFields = ((FieldGroup)getField("superIrdFields")).getFieldsVector();
    
    for( int i = 0; i < superIrdFields.size(); ++i )
    {
      f = (Field)superIrdFields.elementAt(i);
      System.out.println( StringUtilities.leftJustify(f.getName(),30,' ') + " :  " + f.getData() );
    }
  }
  
  protected void storeSuperIrdRecord()
  {
    boolean   autoCommit    = getAutoCommit();
    long      recId         = 0L;
    
    try
    {
      
      System.out.print("storing          \r");//@
//@      System.out.print("storing " + StringUtilities.rightJustify( String.valueOf(EligibleSicCodes.length()),11,'0') + "\r");//@
      
      setAutoCommit(false);
      
      /*@lineinfo:generated-code*//*@lineinfo:862^7*/

//  ************************************************************
//  #sql [Ctx] { select  mc_super_ird_seq.nextval  
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mc_super_ird_seq.nextval   \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:866^7*/
      
      for( Iterator it = GpidAndAlmMap.keySet().iterator(); it.hasNext(); )
      {
        String key    = (String)it.next();
        String value  = (String)GpidAndAlmMap.get(key);
        
        setData("reqd_alm_code"             ,key    );
        setData("eligible_gcms_product_ids" ,value  );

        /*@lineinfo:generated-code*//*@lineinfo:876^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mc_super_ird
//            (
//              rec_id,
//              effective_timestamp,
//              effective_date,
//              table_id,
//              active_inactive_code,
//              card_program_id,
//              bsa_type,
//              bsa_id_code,
//              ird,
//              eligible_gcms_product_ids,
//              reqd_alm_code,
//              gcms_timeliness,
//              gcms_auth_reqd,
//              icc_timeliness,
//              icc_auth_reqd,
//              processing_code,
//              reversal_allowed,
//              ic_compliance_switch,
//              ptr53_fee_code,
//              product_type_id,
//              ptr59_timeliness_auth_code,
//              ptr57_fee_override,
//              mc_assigned_id_reqd_validated,
//              message_type_identifier,
//              fleet_at_fuel,
//              function_code
//            )
//            values
//            (
//              :recId,
//              :EffectiveTimestamp,
//              :getSqlDate("effective_date"),
//              :getData("table_id"),
//              :getData("active_inactive_code"),
//              :getData("card_program_id"),
//              :getData("bsa_type"),
//              :getData("bsa_id_code"),
//              :getData("ird"),
//              :getData("eligible_gcms_product_ids"),
//              :getData("reqd_alm_code"),
//              :getData("gcms_timeliness"),
//              :getData("gcms_auth_reqd"),
//              :getData("icc_timeliness"),
//              :getData("icc_auth_reqd"),
//              :getData("processing_code"),
//              :getData("reversal_allowed"),
//              :getData("ic_compliance_switch"),
//              :getLong("ptr53_fee_code"),
//              :getData("product_type_id"),
//              :getLong("ptr59_timeliness_auth_code"),
//              :getLong("ptr57_fee_override"),
//              :getData("mc_assigned_id_reqd_validated"),
//              :getData("message_type_identifier"),
//              :getData("fleet_at_fuel"),
//              :getData("function_code")
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1468 = getSqlDate("effective_date");
 String __sJT_1469 = getData("table_id");
 String __sJT_1470 = getData("active_inactive_code");
 String __sJT_1471 = getData("card_program_id");
 String __sJT_1472 = getData("bsa_type");
 String __sJT_1473 = getData("bsa_id_code");
 String __sJT_1474 = getData("ird");
 String __sJT_1475 = getData("eligible_gcms_product_ids");
 String __sJT_1476 = getData("reqd_alm_code");
 String __sJT_1477 = getData("gcms_timeliness");
 String __sJT_1478 = getData("gcms_auth_reqd");
 String __sJT_1479 = getData("icc_timeliness");
 String __sJT_1480 = getData("icc_auth_reqd");
 String __sJT_1481 = getData("processing_code");
 String __sJT_1482 = getData("reversal_allowed");
 String __sJT_1483 = getData("ic_compliance_switch");
 long __sJT_1484 = getLong("ptr53_fee_code");
 String __sJT_1485 = getData("product_type_id");
 long __sJT_1486 = getLong("ptr59_timeliness_auth_code");
 long __sJT_1487 = getLong("ptr57_fee_override");
 String __sJT_1488 = getData("mc_assigned_id_reqd_validated");
 String __sJT_1489 = getData("message_type_identifier");
 String __sJT_1490 = getData("fleet_at_fuel");
 String __sJT_1491 = getData("function_code");
   String theSqlTS = "insert into mc_super_ird\n          (\n            rec_id,\n            effective_timestamp,\n            effective_date,\n            table_id,\n            active_inactive_code,\n            card_program_id,\n            bsa_type,\n            bsa_id_code,\n            ird,\n            eligible_gcms_product_ids,\n            reqd_alm_code,\n            gcms_timeliness,\n            gcms_auth_reqd,\n            icc_timeliness,\n            icc_auth_reqd,\n            processing_code,\n            reversal_allowed,\n            ic_compliance_switch,\n            ptr53_fee_code,\n            product_type_id,\n            ptr59_timeliness_auth_code,\n            ptr57_fee_override,\n            mc_assigned_id_reqd_validated,\n            message_type_identifier,\n            fleet_at_fuel,\n            function_code\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setTimestamp(2,EffectiveTimestamp);
   __sJT_st.setDate(3,__sJT_1468);
   __sJT_st.setString(4,__sJT_1469);
   __sJT_st.setString(5,__sJT_1470);
   __sJT_st.setString(6,__sJT_1471);
   __sJT_st.setString(7,__sJT_1472);
   __sJT_st.setString(8,__sJT_1473);
   __sJT_st.setString(9,__sJT_1474);
   __sJT_st.setString(10,__sJT_1475);
   __sJT_st.setString(11,__sJT_1476);
   __sJT_st.setString(12,__sJT_1477);
   __sJT_st.setString(13,__sJT_1478);
   __sJT_st.setString(14,__sJT_1479);
   __sJT_st.setString(15,__sJT_1480);
   __sJT_st.setString(16,__sJT_1481);
   __sJT_st.setString(17,__sJT_1482);
   __sJT_st.setString(18,__sJT_1483);
   __sJT_st.setLong(19,__sJT_1484);
   __sJT_st.setString(20,__sJT_1485);
   __sJT_st.setLong(21,__sJT_1486);
   __sJT_st.setLong(22,__sJT_1487);
   __sJT_st.setString(23,__sJT_1488);
   __sJT_st.setString(24,__sJT_1489);
   __sJT_st.setString(25,__sJT_1490);
   __sJT_st.setString(26,__sJT_1491);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:936^9*/
      }
      
//@      String[] sicCodes = EligibleSicCodes.split(",");
//@      for( int i = 0; i < sicCodes.length; ++i )
//@      {
//@        #sql [Ctx]
//@        {
//@          insert into mc_super_ird_sic_codes ( sic_code, rec_id )
//@          values ( :(sicCodes[i]), :recId )
//@        };
//@      }

      if ( isCashDisbursement() )
      {
        /*@lineinfo:generated-code*//*@lineinfo:951^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mc_super_ird_sic_codes ( rec_id,sic_code )
//            select  :recId,sic_code
//            from    sic_codes
//            where   sic_code in ( '6010','6011' )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mc_super_ird_sic_codes ( rec_id,sic_code )\n          select   :1 ,sic_code\n          from    sic_codes\n          where   sic_code in ( '6010','6011' )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:957^9*/
      }
      else
      {
        String keyString = getMaskedCpiBsaIrdCombo();
        /*@lineinfo:generated-code*//*@lineinfo:962^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mc_super_ird_sic_codes 
//            ( 
//              rec_id,
//              sic_code
//            )
//            select  /*+ ordered */
//                    distinct
//                    :recId                            as rec_id,
//                    substr(sic.raw_data,21,4)         as sic_code
//            from    mc_ep_table_data    epd,
//                    mc_ep_table_data    sic
//            where   epd.table_id = 'IP0095T1'
//                    and substr(epd.raw_data,20,12) = :keyString
//                    and substr(epd.raw_data,36,1) = 'A'     -- "A"ll not "L"ife cycle only
//                    and sic.table_id = 'IP0075T1'
//                    and substr(sic.raw_data,29,1) = 'A'     -- "A"ll not "L"ife cycle only
//                    and substr(sic.raw_data,31,1) = 'A'     -- "A"ctive not "O"bsolete
//                    and substr(sic.raw_data,25,4) = substr(epd.raw_data,32,4)
//            order by sic_code                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mc_super_ird_sic_codes \n          ( \n            rec_id,\n            sic_code\n          )\n          select  /*+ ordered */\n                  distinct\n                   :1                             as rec_id,\n                  substr(sic.raw_data,21,4)         as sic_code\n          from    mc_ep_table_data    epd,\n                  mc_ep_table_data    sic\n          where   epd.table_id = 'IP0095T1'\n                  and substr(epd.raw_data,20,12) =  :2 \n                  and substr(epd.raw_data,36,1) = 'A'     -- \"A\"ll not \"L\"ife cycle only\n                  and sic.table_id = 'IP0075T1'\n                  and substr(sic.raw_data,29,1) = 'A'     -- \"A\"ll not \"L\"ife cycle only\n                  and substr(sic.raw_data,31,1) = 'A'     -- \"A\"ctive not \"O\"bsolete\n                  and substr(sic.raw_data,25,4) = substr(epd.raw_data,32,4)\n          order by sic_code";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.settlement.MasterCardSuperIrdBuilder",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setString(2,keyString);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:983^9*/
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:986^7*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:986^27*/
    }
    catch( Exception e )
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:990^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:990^34*/ } catch( Exception sqle ) {}
//@      System.out.println("storeSuperIrdRecord(" + getCpiBsaIrdCombo() + ") - " + e.toString());
      System.out.println("storeSuperIrdRecord(" + getMaskedCpiBsaIrdCombo() + ") - " + e.toString());
      logEntry("storeSuperIrdRecord(" + getCpiBsaIrdCombo() + ")",e.toString());
      try{ /*@lineinfo:generated-code*//*@lineinfo:994^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:994^32*/ } catch( Exception sqle ) {}
    }
    finally
    {
      setAutoCommit(true);
    }
  }
  
  public static void main( String[] args )
  {
    MasterCardSuperIrdBuilder   irdBuilder = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      irdBuilder = new MasterCardSuperIrdBuilder();
      
      irdBuilder.connect(true);
      irdBuilder.createFields(null);
      
      if ( args.length > 0 && "loadHolidays".equals(args[0]) )
      {
        System.out.println("Loading MC Holidays...");
        irdBuilder.loadHolidays();
      }
      else if ( args.length > 0 && "cleanSuperIrd".equals(args[0]) )
      {
        System.out.println("Cleaning SuperIRD table...");
        irdBuilder.cleanSuperIrd();
      }
      else if ( args.length > 0 && "cleanLoadSuperIrd".equals(args[0]) )
      {
        System.out.println("Cleaning SuperIRD table...");
        irdBuilder.cleanSuperIrd();
        System.out.println("Building SuperIRD table...");
        irdBuilder.buildSuperIrd();
      }
      else if ( args.length > 0 && "cleanBsaTables".equals(args[0]) )
      {
        System.out.println("Cleaning BSA tables...");
        irdBuilder.cleanBsaTables();
      }
      else if ( args.length > 0 && "loadBsaTables".equals(args[0]) )
      {
        System.out.println("Building BSA tables...");
        irdBuilder.buildBsaTables();
      }
      else if ( args.length > 0 && "cleanLoadBsaTables".equals(args[0]) )
      {
        System.out.println("Cleaning BSA tables...");
        irdBuilder.cleanBsaTables();
        System.out.println("Building BSA tables...");
        irdBuilder.buildBsaTables();
      }
      else
      {
        System.out.println("Building SuperIRD table...");
        irdBuilder.buildSuperIrd();
      }        
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ irdBuilder.cleanUp(); } catch( Exception ee ){}
    }
  }
}/*@lineinfo:generated-code*/