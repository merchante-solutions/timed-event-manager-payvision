/*@lineinfo:filename=IpmParmMasterLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/IpmParmMasterLoader.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import com.mes.config.ConfigurationManager;
import com.mes.config.DbProperties;
import com.mes.config.MesDefaults;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.SyncLog;
import com.mes.support.ThreadPool;
import sqlj.runtime.ResultSetIterator;

public class IpmParmMasterLoader
  extends SQLJConnectionBase
{
  public class LoaderThread 
    extends SQLJConnectionBase
    implements Runnable
  {
    private   String        TableId     = null;
    private   int           ThreadId    = -1;
    
    public LoaderThread( String tableId )
    {
      TableId       = tableId;
    }
    
    protected void dumpTableData( String tableId )
    {
      try
      {
//@        System.out.println("delete: " + tableId);
        /*@lineinfo:generated-code*//*@lineinfo:63^9*/

//  ************************************************************
//  #sql [Ctx] { delete 
//            from    mc_ep_table_data 
//            where   table_id = :tableId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n          from    mc_ep_table_data \n          where   table_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.settlement.IpmParmMasterLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,tableId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:68^9*/
      }
      catch( Exception e )
      {
        logEntry("dumpTableData(" + tableId + ")",e.toString());
      }
      finally
      {
      }
    }
  
    protected void insertTableData( String rawData )
    {
      try
      {
        if ( rawData.charAt(10) != 'I' )    // skip inactive records
        {
          //@System.out.println("insert: " + rawData);//@
          /*@lineinfo:generated-code*//*@lineinfo:86^11*/

//  ************************************************************
//  #sql [Ctx] { insert into mc_ep_table_data 
//              (
//                raw_data
//              )
//              values
//              (
//                :rawData
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mc_ep_table_data \n            (\n              raw_data\n            )\n            values\n            (\n               :1 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.settlement.IpmParmMasterLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,rawData);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:96^11*/
          System.out.print("Inserting record # " + ++RecordCount + "           \r");//@
        }          
      }
      catch( Exception e )
      {
        logEntry("insertTableData()",e.toString());
      }
      finally
      {
      }
    }
  
    public void run( )
    {
      boolean             autoCommit  = getAutoCommit();
      BufferedReader      in          = null;
      int                 insertCount = 0;
      String              line        = null;
      
      try
      {
        connect(true);
        dumpTableData( TableId );
      
        setAutoCommit(false);   // disable auto-commit
        
        in = new BufferedReader( new FileReader( TableId + ".dat" ) );
        while( (line = in.readLine()) != null )
        {
          insertTableData( line );
          
          // commit every 100 records
          if ( (++insertCount%100) == 0 ) { /*@lineinfo:generated-code*//*@lineinfo:129^45*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:129^65*/ }
        }
        /*@lineinfo:generated-code*//*@lineinfo:131^9*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:131^29*/
      }
      catch( Exception e )
      {
        // ignore all exceptions
        SyncLog.LogEntry("com.mes.settlement.IpmParmMasterLoader$LoaderThread::run():", e.toString());
      }
      finally
      {
        try{ in.close();                } catch( Exception ee ){}
        try{ setAutoCommit(autoCommit); } catch( Exception ee ){}
        cleanUp();
      }
    }
  }
  
  public class IpmTableData
  {
    public  int             BlockCountLength    = 0;
    public  int             BlockCountOffset    = 0;
    public  int             BlockSize           = 0;
    public  Date            CentralDate         = null;
    public  Date            EffectiveDate       = null;
    public  int             KeyLength           = 0;
    public  int             KeyStart            = 0;
    public  Date            MemberDate          = null;
    public  int             RecLenMax           = 0;
    public  int             RecLenMin           = 0;
    public  int             RowCount            = 0;
    public  String          TableDesc           = null;
    public  String          TableId             = null;
    public  String          Version             = null;
    
    
    public IpmTableData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      BlockCountLength  = resultSet.getInt("block_count_length");
      BlockCountOffset  = resultSet.getInt("block_count_offset");
      BlockSize         = resultSet.getInt("block_size");
      CentralDate       = resultSet.getDate("central_date");
      EffectiveDate     = resultSet.getDate("effective_date");
      KeyLength         = resultSet.getInt("key_length");
      KeyStart          = resultSet.getInt("key_start");
      MemberDate        = resultSet.getDate("member_date");
      RecLenMax         = resultSet.getInt("rec_len_max");
      RecLenMin         = resultSet.getInt("rec_len_min");
      RowCount          = resultSet.getInt("row_count");
      TableDesc         = resultSet.getString("table_desc");
      TableId           = resultSet.getString("table_id");
      Version           = resultSet.getString("version");
    }
    
    public boolean isVariableLengthRecord()
    {
      return( RecLenMin != RecLenMax );
    }
    
    public int getBlockCountOffsetBegin()
    {
      return( BlockCountOffset-1 );
    }
    
    public int getBlockCountOffsetEnd()
    {
      return( getBlockCountOffsetBegin()+BlockCountLength );
    }
    
    public int getBlockSize()
    {
      return( BlockSize );
    }
    
    public int getFixedSize()
    {
      return(RecLenMin - BlockSize);
    }
  }
  
  private   boolean       LoadOracle    = false;  
  private   int           RecordCount   = 0;
  private   ThreadPool    Workers       = new ThreadPool(7);
  
  protected void closeFile( String tableId, BufferedWriter dataFile )
    throws Exception
  {
    if ( dataFile != null )
    {
      dataFile.close(); 
      
      if ( LoadOracle )
      {
        Thread thread = Workers.getThread(new LoaderThread(tableId));
        thread.start();
      }        
    }
  }
  
  protected HashMap loadIpmTableInfo( )
  {
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    HashMap             tableMap      = new HashMap();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:237^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  central_date            as central_date,
//                  effective_date          as effective_date,
//                  key_length              as key_length,
//                  key_start               as key_start,
//                  member_date             as member_date,
//                  rec_len_max             as rec_len_max,
//                  rec_len_min             as rec_len_min,
//                  row_count               as row_count,
//                  table_desc              as table_desc,
//                  table_id                as table_id,
//                  nvl(block_size,0)       as block_size,
//                  block_count_offset      as block_count_offset,
//                  block_count_length      as block_count_length,
//                  version                 as version
//          from    mc_ep_tables  ept
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  central_date            as central_date,\n                effective_date          as effective_date,\n                key_length              as key_length,\n                key_start               as key_start,\n                member_date             as member_date,\n                rec_len_max             as rec_len_max,\n                rec_len_min             as rec_len_min,\n                row_count               as row_count,\n                table_desc              as table_desc,\n                table_id                as table_id,\n                nvl(block_size,0)       as block_size,\n                block_count_offset      as block_count_offset,\n                block_count_length      as block_count_length,\n                version                 as version\n        from    mc_ep_tables  ept";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.settlement.IpmParmMasterLoader",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.settlement.IpmParmMasterLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:254^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        tableMap.put( resultSet.getString("table_id"),
                      new IpmTableData(resultSet) );
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("loadIpmTableInfo()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
    return( tableMap );
  }
  
  public void processFile( String loadFilename )
  {
    BufferedWriter    dataFile          = null;
    String            dateString        = null;
    BufferedReader    in                = null;
    String            lastTableId       = "";
    int               len               = 0;
    String            line              = null;
    int               lineCnt           = 0;
    int               lineLen           = 132;
    BufferedWriter    out               = null;
    String            output            = null;
    int               recLen            = 0;
    boolean           table0            = false;
    IpmTableData      tableData         = null;
    String            tableId           = null;
    HashMap           tableMap          = loadIpmTableInfo();
    
    char replaceChar = (char)0x0c;
    
    try
    {
      String fname = loadFilename;
      if ( fname.indexOf(".") >= 0 )
      {
        fname = fname.substring(0,fname.lastIndexOf("."));
      }
      
      in        = new BufferedReader( new FileReader(loadFilename) );
      out       = new BufferedWriter( new FileWriter(fname+".txt",false) );
      
      while( (line = in.readLine()) != null )
      {
        ++lineCnt;
        //@if ( lineCnt > 10 ) break;
        
        line = line.replace(replaceChar,'\n');
        
        if( line.length() > lineLen )
        {
          while( line.length() > 0 )
          {
            tableData = null;
            if ( !table0 && line.length() > 18 && DateTimeFormatter.parseDate(line.substring(0,8),"yyyyMMdd") != null )
            {
              tableId   = line.substring(11,19);
              tableData = (IpmTableData)tableMap.get(tableId);
            }
            else if ( !table0 && line.length() > 36 && "IP0000T1".equals(line.substring(28,36)) )
            {
              table0 = true;
            }
            else if ( table0 && line.length() > 10 && DateTimeFormatter.parseDate(line.substring(0,10),"yyyy-MM-dd") != null )
            {
              storeTable0Record(line);
            }
            
            if ( tableData != null )
            {
              if ( !tableId.equals(lastTableId) )
              {
                closeFile(lastTableId,dataFile);
                dataFile  = new BufferedWriter( new FileWriter(tableId+".dat",false) );
              }
              
              if ( tableData.isVariableLengthRecord() )
              {
                try
                {
                  int blockCount  = Integer.parseInt(line.substring(tableData.getBlockCountOffsetBegin(),
                                                                    tableData.getBlockCountOffsetEnd()));
                  recLen = tableData.getFixedSize() + (blockCount * tableData.getBlockSize());
                  
//@                  if ( blockCount > 1 ) {
//@                    System.out.println("blockSize: " + tableData.getBlockSize() + "  blockCount: " + blockCount + "  recLen: " + recLen);
//@                  }
                }
                catch( Exception badVarRec )
                {
                  recLen = 0;
                }
              }
              else
              {
                recLen  = tableData.RecLenMax;
              }
              
              if ( recLen > 0 )
              {
                int pad = ((recLen%lineLen) == 0) ? 0 : (lineLen-(recLen%lineLen));
                len = (recLen + pad);
                output = line.substring(0,recLen);
                dataFile.write(output); dataFile.newLine();
              }
              
              lastTableId = tableId;
            }
            else
            {
              len = Math.min(lineLen,line.length());
              output = line.substring(0,len);
            }
            out.write(output);
            out.newLine();
            line = line.substring(len);
          }
        }
        else    // line is < lineLen
        {
          if ( table0 && line.length() > 73 && "END OF TABLE".equals(line.substring(60,72)) )
          {
            table0 = false;
            
            // table 0 was loaded, reload the tableMap
            // to insure that it contains all entries
            tableMap.clear();
            tableMap = loadIpmTableInfo();
          }
          out.write(line);
          out.newLine();
        }
      }
      closeFile(lastTableId,dataFile);
      
      // wait for all extraction threads to complete
      Workers.waitForAllThreads();
      System.out.println();
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }      
    finally
    {
      try{ out.close(); } catch(Exception ee){}
      try{ in.close(); } catch(Exception ee){}
      try{ dataFile.close(); } catch(Exception ee){}
      
    }
  }
  
  public void setLoadOracleFlag( boolean flag )
  {
    LoadOracle = flag;
  }
  
  protected void storeTable0Record( String line )
  {
    int           recCount    = 0;
    String        tableId     = "Invalid";
    try
    {
      tableId = line.substring(15,23);
      
      /*@lineinfo:generated-code*//*@lineinfo:429^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1) 
//          from    mc_ep_tables    ept
//          where   ept.table_id = :tableId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)  \n        from    mc_ep_tables    ept\n        where   ept.table_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.settlement.IpmParmMasterLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tableId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:434^7*/
      
      if ( recCount == 0 )
      {                                                                                             
        /*@lineinfo:generated-code*//*@lineinfo:438^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mc_ep_tables
//            (
//              effective_date,
//              table_id,
//              table_desc,
//              key_length,
//              key_start,
//              rec_len_min,
//              rec_len_max,
//              version,
//              central_date,
//              member_date,
//              row_count
//            )
//            values
//            (
//              to_date(:line.substring(0,13),'yyyy-mm-dd-hh24'),
//              :tableId,
//              :line.substring(25,52),
//              :line.substring(54,59),
//              :line.substring(62,66),
//              :line.substring(68,73),
//              :line.substring(75,80),
//              :line.substring(82,90),
//              to_date(:line.substring(92,106),'yyyy mmdd hh24mi'),
//              to_date(:line.substring(108,122),'yyyy mmdd hh24mi'),
//              :line.substring(123,131)
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1448 = line.substring(0,13);
 String __sJT_1449 = line.substring(25,52);
 String __sJT_1450 = line.substring(54,59);
 String __sJT_1451 = line.substring(62,66);
 String __sJT_1452 = line.substring(68,73);
 String __sJT_1453 = line.substring(75,80);
 String __sJT_1454 = line.substring(82,90);
 String __sJT_1455 = line.substring(92,106);
 String __sJT_1456 = line.substring(108,122);
 String __sJT_1457 = line.substring(123,131);
   String theSqlTS = "insert into mc_ep_tables\n          (\n            effective_date,\n            table_id,\n            table_desc,\n            key_length,\n            key_start,\n            rec_len_min,\n            rec_len_max,\n            version,\n            central_date,\n            member_date,\n            row_count\n          )\n          values\n          (\n            to_date( :1 ,'yyyy-mm-dd-hh24'),\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n            to_date( :9 ,'yyyy mmdd hh24mi'),\n            to_date( :10 ,'yyyy mmdd hh24mi'),\n             :11 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.settlement.IpmParmMasterLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1448);
   __sJT_st.setString(2,tableId);
   __sJT_st.setString(3,__sJT_1449);
   __sJT_st.setString(4,__sJT_1450);
   __sJT_st.setString(5,__sJT_1451);
   __sJT_st.setString(6,__sJT_1452);
   __sJT_st.setString(7,__sJT_1453);
   __sJT_st.setString(8,__sJT_1454);
   __sJT_st.setString(9,__sJT_1455);
   __sJT_st.setString(10,__sJT_1456);
   __sJT_st.setString(11,__sJT_1457);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:468^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:472^9*/

//  ************************************************************
//  #sql [Ctx] { update  mc_ep_tables  ept
//            set     effective_date      = to_date(:line.substring(0,13),'yyyy-mm-dd-hh24'),
//                    table_desc          = :line.substring(25,52),
//                    key_length          = :line.substring(54,59),
//                    key_start           = :line.substring(62,66),
//                    rec_len_min         = :line.substring(68,73),
//                    rec_len_max         = :line.substring(75,80),
//                    version             = :line.substring(82,90),
//                    central_date        = to_date(:line.substring(92,106),'yyyy mmdd hh24mi'),
//                    member_date         = to_date(:line.substring(108,122),'yyyy mmdd hh24mi'),
//                    row_count           = :line.substring(123,131)
//            where   table_id = :tableId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1458 = line.substring(0,13);
 String __sJT_1459 = line.substring(25,52);
 String __sJT_1460 = line.substring(54,59);
 String __sJT_1461 = line.substring(62,66);
 String __sJT_1462 = line.substring(68,73);
 String __sJT_1463 = line.substring(75,80);
 String __sJT_1464 = line.substring(82,90);
 String __sJT_1465 = line.substring(92,106);
 String __sJT_1466 = line.substring(108,122);
 String __sJT_1467 = line.substring(123,131);
   String theSqlTS = "update  mc_ep_tables  ept\n          set     effective_date      = to_date( :1 ,'yyyy-mm-dd-hh24'),\n                  table_desc          =  :2 ,\n                  key_length          =  :3 ,\n                  key_start           =  :4 ,\n                  rec_len_min         =  :5 ,\n                  rec_len_max         =  :6 ,\n                  version             =  :7 ,\n                  central_date        = to_date( :8 ,'yyyy mmdd hh24mi'),\n                  member_date         = to_date( :9 ,'yyyy mmdd hh24mi'),\n                  row_count           =  :10 \n          where   table_id =  :11";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.settlement.IpmParmMasterLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1458);
   __sJT_st.setString(2,__sJT_1459);
   __sJT_st.setString(3,__sJT_1460);
   __sJT_st.setString(4,__sJT_1461);
   __sJT_st.setString(5,__sJT_1462);
   __sJT_st.setString(6,__sJT_1463);
   __sJT_st.setString(7,__sJT_1464);
   __sJT_st.setString(8,__sJT_1465);
   __sJT_st.setString(9,__sJT_1466);
   __sJT_st.setString(10,__sJT_1467);
   __sJT_st.setString(11,tableId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:486^9*/
      }
    }
    catch( Exception e )
    {
      logEntry("storeTable0Record(" + tableId + ")",e.toString());
    }
  }
  
  public void checkTableVersion()
    throws Exception
  {
    int       recCount        = 0;
    
    /*@lineinfo:generated-code*//*@lineinfo:500^5*/

//  ************************************************************
//  #sql [Ctx] { select  count(1) 
//        from    mc_ep_table_data
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)  \n      from    mc_ep_table_data";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.settlement.IpmParmMasterLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:504^5*/
    
    System.out.println();
    System.out.println("****************************************");
    System.out.println("Record Count: " + recCount);
    System.out.println("****************************************");
    System.out.println();
  }
  
  public static void main( String[] args )
  {
    try {
        // open main events properties file
        ConfigurationManager.getInstance().loadTEMConfiguration(ConfigurationManager.TEM_PROPERTY_FILE_DEFAULT);
    }
    catch(Exception e) {
       throw new RuntimeException(e);
    }

    IpmParmMasterLoader     ipmConverter    = null;
    
    try
    {
      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      
      ipmConverter = new IpmParmMasterLoader();
      if ( args.length > 1 )
      {
        if ( args[1].equals("true") )
        {
          ipmConverter.setLoadOracleFlag(true);
        }
        else if ( args[1].equals("testLoad") )
        {
          OracleConnectionPool.getInstance( DbProperties.buildConnectString(null,
                                            MesDefaults.getString(MesDefaults.EPICOR_RAC_BATCH_URL),
                                            MesDefaults.getString(MesDefaults.EPICOR_DATABASE_DRIVER),
                                            MesDefaults.getString(MesDefaults.EPICOR_JACK_USER),
                                            MesDefaults.getString(MesDefaults.EPICOR_JACK_PASSWORD)));
          ipmConverter.setLoadOracleFlag(true);
        }          
      }
      ipmConverter.connect(true);
      
      if ( "checkTableVersion".equals(args[0]) )
      {
        ipmConverter.checkTableVersion();
      }
      else
      {
        ipmConverter.processFile(args[0]);
      }        
      
      Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      long elapsed = (endTime.getTime() - beginTime.getTime());
      
      System.out.println("Begin Time: " + String.valueOf(beginTime));
      System.out.println("End Time  : " + String.valueOf(endTime));
      System.out.println("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));      
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }      
    finally
    {
      try{ ipmConverter.cleanUp(); } catch( Exception ee ){}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/