package com.mes.settlement.fx;

import java.sql.Date;
import java.util.Calendar;

public class VisaCurrencyEntry {
	private String actionCode = null;
	private String baseCurrencyCode = null;
	private double buyRate = 0.0;
	private String counterCurrencyCode = null;
	private Date effectiveDate = null;
	private double sellRate = 0.0;

	public VisaCurrencyEntry() {
	}

	public VisaCurrencyEntry(String rawData) {
		setData(rawData);
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public String getBaseCurrencyCode() {
		return baseCurrencyCode;
	}

	public void setBaseCurrencyCode(String baseCurrencyCode) {
		this.baseCurrencyCode = baseCurrencyCode;
	}

	public double getBuyRate() {
		return buyRate;
	}

	public void setBuyRate(double buyRate) {
		this.buyRate = buyRate;
	}

	public String getCounterCurrencyCode() {
		return counterCurrencyCode;
	}

	public void setCounterCurrencyCode(String counterCurrencyCode) {
		this.counterCurrencyCode = counterCurrencyCode;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public double getSellRate() {
		return sellRate;
	}

	public void setSellRate(double sellRate) {
		this.sellRate = sellRate;
	}

	public void setData(String rawData) {
		actionCode = rawData.substring(0, 1);
		counterCurrencyCode = rawData.substring(1, 4);
		baseCurrencyCode = rawData.substring(4, 7);

		Calendar today = Calendar.getInstance();
		Calendar cal = Calendar.getInstance();

		cal.set(Calendar.MONTH, Integer.parseInt(rawData.substring(7, 9)) - 1);
		cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(rawData.substring(9, 11)));
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		today.add(Calendar.DAY_OF_MONTH, 180);
		if (cal.getTime().after(today.getTime())) {
			cal.add(Calendar.YEAR, -1);
		}
		effectiveDate = new java.sql.Date(cal.getTime().getTime());

		for (int i = 0; i < 2; ++i) {
			String temp = rawData.substring(13 + (i * 8), 19 + (i * 8));
			int decCount = Integer.parseInt(rawData.substring(11 + (i * 8), 13 + (i * 8)));
			String rateStr = "";

			if (decCount > 6) {
				rateStr = "0.";
				for (int j = 0; j < (decCount - 6); ++j) {
					rateStr += "0";
				}
				rateStr += temp;
			}
			else {
				rateStr = (temp.substring(0, 6 - decCount) + "." + temp.substring(6 - decCount));
			}
			double rate = Double.parseDouble(rateStr);

			if (i == 0) {
				buyRate = rate;
			}
			else {
				sellRate = rate;
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VisaCurrencyEntry [ActionCode=").append(actionCode).append(", BaseCurrencyCode=").append(baseCurrencyCode).append(", BuyRate=")
				.append(buyRate).append(", CounterCurrencyCode=").append(counterCurrencyCode).append(", EffectiveDate=").append(effectiveDate)
				.append(", SellRate=").append(sellRate).append("]");
		return builder.toString();
	}
}