package com.mes.settlement.fx;

import java.io.BufferedReader;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.database.MeSQueryHandler;
import com.mes.flatfile.FlatFileRecord;
import com.mes.settlement.DccRatesLoader;
import com.mes.support.DateTimeFormatter;
import com.mes.support.StringUtilities;

public class MasterCardFxRateLoader extends DccRatesLoader<String,Date> {
	private Logger log = Logger.getLogger(MasterCardFxRateLoader.class);

	@Override
	public void processRateFile(String loadFilename, BufferedReader rateFileReader) throws Exception {
		log.info("[processRateFile()] - processing filename="+loadFilename);
		List<Calendar> effectiveDays = null;
		String line = null;
		MeSQueryHandler queryHandler = getMeSQueryHandler();
		SimpleDateFormat datefmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String cutover = getCutoverDate() != null ? datefmt.format(getCutoverDate()) : "0000-00-00 00:00:00";
		FlatFileRecord ffd = getFlatFileRecord(RecordType.MC_T057_DETAIL);

		while ((line = rateFileReader.readLine()) != null) {
			String rawData = StringUtilities.leftJustify(line, 125, ' ');
			String recType = rawData.substring(0, 1);

			if ("H".equals(recType)) {
				effectiveDays = getEffectiveDates(rawData);
				// mastercard provides rates 6 days per week. The rates
				// we receive on Saturday are effective for Sunday and Monday
				deleteRates(effectiveDays);
			}
			else if ("D".equals(recType)) {
				ffd.suck(rawData);
				//@formatter:off
				String updateSql = "insert into mc_settlement_dcc " 
								 + "  (rec_type, counter_currency_code, base_currency_code, rate_class, "
								 + "  effective_date, buy_rate, sell_rate, load_filename, load_file_id,"
								 + "  effective_start_date, effective_end_date) " 
								 + "values " 
								 + "  (?, ?, ?, ?, "
								 + "  ?, (1/ ?), (1/ ?), ?,"
								 + "  load_filename_to_load_file_id(?), ?, ?)";
				//@formatter:on
				String sqlKey = "3com.mes.settlement.DccRatesLoader";
				for (Calendar effectiveCalendar : effectiveDays) {
					Date effectiveDate = new java.sql.Date(effectiveCalendar.getTime().getTime());
					// expiration is (effective + 1 day) - 1 second
					Calendar expirationCalendar = (Calendar) effectiveCalendar.clone();
					if (cutover.equals(datefmt.format(effectiveDate))) {
						expirationCalendar.set(expirationCalendar.get(Calendar.YEAR), expirationCalendar.get(Calendar.MONTH),
								expirationCalendar.get(Calendar.DATE), 12, 4, 59);
					}
					else {
						expirationCalendar.add(Calendar.DAY_OF_MONTH, 1);
						expirationCalendar.add(Calendar.SECOND, -1);
					}
					Date expirationDate = new java.sql.Date(expirationCalendar.getTime().getTime());

					Object[] params = new Object[11];
					params[0] = ffd.getFieldData("rec_type");
					params[1] = ffd.getFieldData("source_currency_code");
					params[2] = ffd.getFieldData("base_currency_code");
					params[3] = ffd.getFieldData("rate_class");
					params[4] = effectiveDate;
					params[5] = ffd.getFieldAsDouble("buy_rate");
					params[6] = ffd.getFieldAsDouble("sell_rate");
					params[7] = loadFilename;
					params[8] = loadFilename;
					params[9] = effectiveDate;
					params[10] = expirationDate;
					queryHandler.executePreparedUpdate(sqlKey, updateSql, params);
				}
			}
		}
	}
	
	protected void cleanData(Date effectiveDate) {
		if (log.isDebugEnabled()) {
			log.debug(String.format("[cleanData()] - effectiveDate=%s", effectiveDate));
		}
		String tableName = "mc_settlement_dcc";
		String sqlString = String.format("delete from %s where effective_date = ?", tableName);
		String sqlKey = String.format("1com.mes.settlement.DccRatesLoader:%s", tableName );
		try {
			Integer rowCount = getMeSQueryHandler().executePreparedUpdate(sqlKey, sqlString, effectiveDate);
			if (log.isDebugEnabled()) {
				log.debug(String.format("[cleanData()] - deleted %s previously processed records for effectiveDate=%s", rowCount, effectiveDate));
			}
		}
		catch (SQLException e) {
			logEntry("cleanData(" + tableName + "," + effectiveDate + ")", e.toString());
		}
	}
	
	protected void deleteRates(List<Calendar> effectiveDays) {
		for (Calendar effDate : effectiveDays) {
			Date deleteDate = new java.sql.Date(effDate.getTime().getTime());
			cleanData(deleteDate);
		}
	}

	@Override
	public List<Calendar> getEffectiveDates(String rawData) throws Exception {
		java.util.Date cutoverDate = getCutoverDate();
		Calendar cal = Calendar.getInstance();
		cal.setTime(DateTimeFormatter.parseDate(rawData.substring(1, 9), "yyyyMMdd"));
		Integer loopCount;
		if (cutoverDate != null && cal.getTime().getTime() >= cutoverDate.getTime()) {
			// effective on file date @ 12:05 PST
			cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 12, 5, 0);
			cal.set(Calendar.MILLISECOND, 0);
			loopCount = (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) ? 2 : 1;
		}
		else {
			// effective date == file date + 1
			cal.add(Calendar.DAY_OF_MONTH, 1);
			loopCount = (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) ? 2 : 1;
		}
		List<Calendar> effDayList = new ArrayList<>(loopCount);
		for (int i = 0; i < loopCount; i++) {
			cal.add(Calendar.DAY_OF_MONTH, i); // the first time this adds a 0 days
			effDayList.add((Calendar) cal.clone());
		}
		return effDayList;
	}

	protected java.util.Date getCutoverDate() throws Exception {
		String cutoverDate = MesDefaults.getString(MesDefaults.MC_ENHANCED_FX_ACTIVE_DATE);
		if (cutoverDate != null) {
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
			return fmt.parse(cutoverDate);
		}
		return null;
	}
}