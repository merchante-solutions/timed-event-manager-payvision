package com.mes.settlement.fx;

import java.io.BufferedReader;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.database.MeSQueryHandler;
import com.mes.flatfile.FlatFileRecord;
import com.mes.settlement.DccRatesLoader;
import com.mes.support.StringUtilities;

public class VisaFxRateLoader extends DccRatesLoader<VisaCurrencyEntry, String> {

	private static final long serialVersionUID = 1L;
	private Logger log = Logger.getLogger(VisaFxRateLoader.class);
	private Map<java.util.Date, Boolean> holidayMap = new HashMap<>();

	@Override
	public void processRateFile(String loadFilename, BufferedReader rateFileReader) throws Exception {
		int end = 0;

		int start = 0;
		MeSQueryHandler queryHandler = null;
		try {
			queryHandler = getMeSQueryHandler();
			queryHandler.setAutoCommit(false);
			cleanData(loadFilename);

			FlatFileRecord[] ffds = new FlatFileRecord[2];
			ffds[0] = getFlatFileRecord(RecordType.VISA_TC56_TCR0);
			ffds[1] = getFlatFileRecord(RecordType.VISA_TC56_TCR1);

			log.info("[VisaFxRateLoader()] - processing Visa DCC File - " + loadFilename);
			String line = null;
			while ((line = rateFileReader.readLine()) != null) {
				String rawData = StringUtilities.leftJustify(line, 168, ' ');
				String tcr = rawData.substring(2, 4);
				FlatFileRecord ffd = null;

				if ("00".equals(tcr)) {
					ffd = ffds[0];
					start = 1;
					end = 5;
				}
				else {
					ffd = ffds[1];
					start = 6;
					end = 11;
				}
				ffd.suck(rawData);
				int updatecount = 0;
				for (int i = start; i <= end; ++i) {
					String fdata = ffd.getFieldData("currency_entry_" + i);
					if (fdata.trim().equals("")) {
						continue;
					} // skip empty entries
					VisaCurrencyEntry entry = new VisaCurrencyEntry(fdata);

					List<Calendar> effectiveDates = getEffectiveDates(entry);
					//@formatter:off
					String updateSql = "insert into visa_settlement_dcc "
							+ "(action_code, counter_currency_code, base_currency_code, effective_date, "
							+ " buy_rate, sell_rate, load_filename, load_file_id,"
							+ " effective_start_date, effective_end_date) "
							+ " values"
							+ "(?, ?, ?, ?, "
							+ " ?, ?, ?, load_filename_to_load_file_id(?),"
							+ " ?, ?)";
					//@formatter:on
					String sqlKey = "5com.mes.settlement.DccRatesLoader";
					for (Calendar effectiveCalendar : effectiveDates) {
						Date effectiveDate = new java.sql.Date(effectiveCalendar.getTime().getTime());
						// expiration is (effective + 1 day) - 1 second
						Calendar expirationCalendar = (Calendar) effectiveCalendar.clone();
						expirationCalendar.add(Calendar.DAY_OF_MONTH, 1);
						expirationCalendar.add(Calendar.SECOND, -1);
						Date expirationDate = new java.sql.Date(expirationCalendar.getTime().getTime());

						Object[] params = new Object[10];
						params[0] = entry.getActionCode();
						params[1] = entry.getCounterCurrencyCode();
						params[2] = entry.getBaseCurrencyCode();
						params[3] = effectiveDate;
						params[4] = entry.getBuyRate();
						params[5] = entry.getSellRate();
						params[6] = loadFilename;
						params[7] = loadFilename;
						params[8] = effectiveDate;
						params[9] = expirationDate;
						queryHandler.executePreparedUpdate(sqlKey, updateSql, params);

						updatecount++;
						if (updatecount % 50 == 0) {
							queryHandler.commit();
						}
					}
				}
			}
		}
		catch (Exception e) {
			logEntry("loadVisaRates()", e.toString());
		}
		finally {
			if (queryHandler != null) {
				queryHandler.commit();
			}
		}
	}

	protected void cleanData(String fileName) {

		String sqlString = String.format("delete from %s where load_file_id=(load_filename_to_load_file_id(?))", "visa_settlement_dcc");
		String sqlKey = String.format("1com.mes.settlement.DccRatesLoader:%s", sqlString);
		try {
			Integer rowCount = getMeSQueryHandler().executePreparedUpdate(sqlKey, sqlString, fileName);
			if (log.isDebugEnabled()) {
				log.debug(String.format("[cleanData()] - deleted %s previously processed records for loadFilename=%s", rowCount, fileName));
			}
		}
		catch (SQLException e) {
			logEntry("cleanData(" + "visa_settlement_dcc" + "," + fileName + ")", e.toString());
		}
	}

	@Override
	public List<Calendar> getEffectiveDates(VisaCurrencyEntry entry) throws Exception {
		Integer loopCount = getLoopCount(entry);
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.setTime(entry.getEffectiveDate());

		List<Calendar> calendarList = new ArrayList<>(loopCount);
		for (int i = 0; i < loopCount; i++) {
			calendarList.add((Calendar) calendar.clone());
			calendarList.get(i).add(Calendar.DAY_OF_MONTH, i); // the first time this adds a 0 days
		}
		return calendarList;
	}

	protected Integer getLoopCount(VisaCurrencyEntry entry) throws SQLException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(entry.getEffectiveDate());

		int loopCount = 0;

		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			// LongWeekend - Monday => update Saturday rates to Sat, Sun, Mon and Tue
			cal.add(Calendar.DAY_OF_WEEK, 2);
			if (isHoliday(cal.getTime())) {
				loopCount = 4;
			}
			// Not a long weekend => update Saturday rates to Sat, Sun and Mon
			else {
				loopCount = 3;
			}
		}
		else if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
			// Long Weekend - Friday => update Friday rates to Fri, sat, sun and mon
			if (isHoliday(cal.getTime())) {
				loopCount = 4;
			}
			// Not a long weekend => update Friday rates to Friday only.
			else {
				loopCount = 1;
			}
		}
		else {
			// Holiday on a weekday (Tue/Wed/Thu) = > update holiday rates to holiday and the day after holiday.
			if (isHoliday(cal.getTime())) {
				loopCount = 2;
			}
			// normal weekday => update rates for the same day only.
			else {
				loopCount = 1;
			}
		}
		return loopCount;
	}

	protected boolean isHoliday(java.util.Date testDate) throws SQLException {
		if (!holidayMap.containsKey(testDate)) {
			try {
				//@formatter:off
				String selectSql = "select visa_dcc from settlement_excluded_dates where excluded_date=? and visa_dcc = 'HOL'";
				//@formatter:on
				String sqlKey = "6com.mes.settlement.DccRatesLoader";
				List<Map<String, Object>> resultList = getMeSQueryHandler().executePreparedStatement(sqlKey, selectSql, testDate);
				holidayMap.put(testDate, !resultList.isEmpty());
			}
			finally {
			}
		}
		return holidayMap.get(testDate);
	}
}