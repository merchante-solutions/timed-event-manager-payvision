/*@lineinfo:filename=SettlementReconLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/SettlementReconLoader.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-09-30 10:04:00 -0700 (Fri, 30 Sep 2011) $
  Version            : $Revision: 19329 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import com.mes.constants.MesFlatFiles;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.support.DateTimeFormatter;
import com.mes.support.StringUtilities;
import com.mes.tools.FileUtils;
import masthead.mastercard.clearingiso.IpmOfflineFile;
import masthead.mastercard.clearingiso.IpmOfflineMessage;
import masthead.mastercard.clearingiso.MicroFocusNetExpressFile;

public class SettlementReconLoader
  extends SQLJConnectionBase
{
  protected void cleanData( String tableName, String loadFilename )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:55^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    :tableName
//          where   load_file_id = load_filename_to_load_file_id(:loadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("delete\n        from     ");
   __sjT_sb.append(tableName);
   __sjT_sb.append(" \n        where   load_file_id = load_filename_to_load_file_id( ? )");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "0com.mes.settlement.SettlementReconLoader:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:60^7*/
    }
    catch( Exception e )
    {
      logEntry("cleanData(" + tableName + "," + loadFilename + ")", e.toString());
    }
  }
  
  protected void loadMasterCardReconData( String ipmFilename )
  {
    String              functionCode      = null;
    IpmOfflineFile      ipmFile           = null;
    IpmOfflineMessage   ipmMsg            = null;
    String              loadFilename      = ipmFilename;
    List                msgs              = null;
    String              msgType           = null;
    
    try
    {
      loadFilename = loadFilename.replace('\\','/');
      if ( loadFilename.indexOf("/") >= 0 )
      {
        loadFilename = loadFilename.substring(loadFilename.lastIndexOf("/")+1);
      }
      /*@lineinfo:generated-code*//*@lineinfo:84^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:loadFilename)  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1 ) \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.settlement.SettlementReconLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^61*/
      System.out.println("Processing MasterCard Recon File - " + loadFilename);
      
      //generate byte data
      byte[] data = FileUtils.getBytesFromFile(ipmFilename);

      //due to variance in files coming in,
      //there may be a parse exception
      try
      {
        // try accessing the data from static method 
        msgs = MicroFocusNetExpressFile.parseAsList(data);
      }
      catch(Exception e)
      {
        // not in MicroFocusNetExpress format
        ipmFile = IpmOfflineFile.parseMsgBlock(data);
        msgs    = ipmFile.getMessages();
      }
      
      if( msgs != null )
      {
        cleanData("mc_settlement_recon",loadFilename);
      
        char debitCreditInd = '\0';
        String amountString = null;
        String feeType = "";
        double[] amounts = new double[7];
        String[] debitCreditInds = new String[7];
        
        Iterator it = msgs.iterator();
        while( it.hasNext() )
        {
          ipmMsg          = (IpmOfflineMessage)it.next();
          
          if ( !"688".equals(ipmMsg.getDataElement(24)) ) continue;
          
          for( int i = 0; i < 7; ++i )
          {
            amountString    = ipmMsg.getPds(390+i);
            
            int offset = 0;
            do    
            {
              debitCreditInd = amountString.charAt(offset);
              feeType += debitCreditInd;
              ++offset; 
            }
            while ( debitCreditInd != 'D' && debitCreditInd != 'C' );
            
            feeType = (feeType.length() == 3 ? feeType.substring(0,2) : "");
            amounts[i] = Double.parseDouble(amountString.substring(offset));
            debitCreditInds[i] = String.valueOf(debitCreditInd);
          }            
          
          for ( int i = 0; i < amounts.length; ++i )
          {
            System.out.println("amount["+i+"] = " + amounts[i]);//@
          }
      
          /*@lineinfo:generated-code*//*@lineinfo:144^11*/

//  ************************************************************
//  #sql [Ctx] { insert into mc_settlement_recon
//              (
//                message_type,
//                function_code,
//                reason_code,
//                currency_exponents,
//                recon_file_id,
//                recon_settlement_activity,
//                fee_type,
//                credits_amount,
//                sales_amount,
//                fees_amount_sales,
//                fees_amount_credits,
//                total_amount,
//                fees_amount,
//                net_amount,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :ipmMsg.getDataElement(0),    -- message_type
//                :ipmMsg.getDataElement(24),   -- function_code
//                :ipmMsg.getDataElement(25),   -- reason_code
//                :ipmMsg.getPds(148),          -- currency_exponents
//                :ipmMsg.getPds(300),          -- recon_file_id
//                :ipmMsg.getPds(359),          -- recon_settlement_activity
//                :feeType,                       -- fee_type
//                :amounts[0] * 0.01,           -- credits_amount
//                :amounts[1] * 0.01,           -- sales_amount
//                :amounts[2] * 0.01,           -- fees_amount_sales
//                :amounts[3] * 0.01,           -- fees_amount_credits
//                :amounts[4] * 0.01 * decode(:debitCreditInds[4],'D',-1,1),           -- total_amount
//                :amounts[5] * 0.01 * decode(:debitCreditInds[5],'D',-1,1),           -- fees_amount
//                :amounts[6] * 0.01 * decode(:debitCreditInds[6],'D',-1,1),           -- net_amount
//                :loadFilename,
//                load_filename_to_load_file_id(:loadFilename)
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1492 = ipmMsg.getDataElement(0);
 String __sJT_1493 = ipmMsg.getDataElement(24);
 String __sJT_1494 = ipmMsg.getDataElement(25);
 String __sJT_1495 = ipmMsg.getPds(148);
 String __sJT_1496 = ipmMsg.getPds(300);
 String __sJT_1497 = ipmMsg.getPds(359);
 double __sJT_1498 = amounts[0];
 double __sJT_1499 = amounts[1];
 double __sJT_1500 = amounts[2];
 double __sJT_1501 = amounts[3];
 double __sJT_1502 = amounts[4];
 String __sJT_1503 = debitCreditInds[4];
 double __sJT_1504 = amounts[5];
 String __sJT_1505 = debitCreditInds[5];
 double __sJT_1506 = amounts[6];
 String __sJT_1507 = debitCreditInds[6];
   String theSqlTS = "insert into mc_settlement_recon\n            (\n              message_type,\n              function_code,\n              reason_code,\n              currency_exponents,\n              recon_file_id,\n              recon_settlement_activity,\n              fee_type,\n              credits_amount,\n              sales_amount,\n              fees_amount_sales,\n              fees_amount_credits,\n              total_amount,\n              fees_amount,\n              net_amount,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1 ,    -- message_type\n               :2 ,   -- function_code\n               :3 ,   -- reason_code\n               :4 ,          -- currency_exponents\n               :5 ,          -- recon_file_id\n               :6 ,          -- recon_settlement_activity\n               :7 ,                       -- fee_type\n               :8  * 0.01,           -- credits_amount\n               :9  * 0.01,           -- sales_amount\n               :10  * 0.01,           -- fees_amount_sales\n               :11  * 0.01,           -- fees_amount_credits\n               :12  * 0.01 * decode( :13 ,'D',-1,1),           -- total_amount\n               :14  * 0.01 * decode( :15 ,'D',-1,1),           -- fees_amount\n               :16  * 0.01 * decode( :17 ,'D',-1,1),           -- net_amount\n               :18 ,\n              load_filename_to_load_file_id( :19 )\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.settlement.SettlementReconLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1492);
   __sJT_st.setString(2,__sJT_1493);
   __sJT_st.setString(3,__sJT_1494);
   __sJT_st.setString(4,__sJT_1495);
   __sJT_st.setString(5,__sJT_1496);
   __sJT_st.setString(6,__sJT_1497);
   __sJT_st.setString(7,feeType);
   __sJT_st.setDouble(8,__sJT_1498);
   __sJT_st.setDouble(9,__sJT_1499);
   __sJT_st.setDouble(10,__sJT_1500);
   __sJT_st.setDouble(11,__sJT_1501);
   __sJT_st.setDouble(12,__sJT_1502);
   __sJT_st.setString(13,__sJT_1503);
   __sJT_st.setDouble(14,__sJT_1504);
   __sJT_st.setString(15,__sJT_1505);
   __sJT_st.setDouble(16,__sJT_1506);
   __sJT_st.setString(17,__sJT_1507);
   __sJT_st.setString(18,loadFilename);
   __sJT_st.setString(19,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:184^11*/
        }
      }
    }
    catch( Exception e )
    {
      logEntry("loadMasterCardReconData()",e.toString());
    }
    finally
    {
    }
  }

  protected void loadVisaReconData( String inputFilename )
  {
    FlatFileRecord      ffd           = null;
    HashMap             flatFileDefs  = new HashMap();
    BufferedReader      in            = null;
    String              key           = null;
    String              line          = null;
    String              loadFilename  = inputFilename;
    
    try
    {
      loadFilename = loadFilename.replace('\\','/');
      if ( loadFilename.indexOf("/") >= 0 )
      {
        loadFilename = loadFilename.substring(loadFilename.lastIndexOf("/")+1);
      }
      /*@lineinfo:generated-code*//*@lineinfo:213^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:loadFilename)  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1 ) \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.settlement.SettlementReconLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:213^61*/
      System.out.println("Processing Visa Recon File - " + loadFilename);
      in = new BufferedReader( new FileReader( inputFilename ) );
      
      while( (line = in.readLine()) != null )
      {
        String rawData = StringUtilities.leftJustify(line,168,' ');
        
        if ( "V3115".equals(rawData.substring(58,63)) )
        {
          key = String.valueOf(MesFlatFiles.DEF_TYPE_VISA_TC46_TCR0_VSS115);
          ffd = (FlatFileRecord)flatFileDefs.get(key);
          if ( ffd == null ) 
          {
            ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_VISA_TC46_TCR0_VSS115);
            flatFileDefs.put(key,ffd);
            cleanData("visa_settlement_recon_vss115",loadFilename);
          }
          ffd.suck(rawData);
          
          /*@lineinfo:generated-code*//*@lineinfo:233^11*/

//  ************************************************************
//  #sql [Ctx] { insert into visa_settlement_recon_vss115
//              (
//                settlement_date,
//                report_date,
//                from_date,
//                to_date,
//                tran_code,
//                tran_code_qualifier,
//                tran_component_seq_num,
//                dest_bin,
//                source_bin,
//                sre_id_reporting_for,
//                sre_id_rollup_to,
//                sre_id_funds_transfer,
//                settlement_service_id,
//                settlement_currency_code,
//                amount_type,
//                business_mode,
//                no_data_indicator,
//                report_group,
//                report_subgroup,
//                report_id_number,
//                report_id_suffix,
//                business_transaction_type,
//                business_transaction_cycle,
//                reversal_indicator,
//                reimbursement_attribute,
//                credits_count,
//                credits_amount,
//                debits_count,
//                debits_amount,
//                total_amount,
//                total_amount_sign,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :ffd.getFieldAsSqlDate("settlement_date"),
//                :ffd.getFieldAsSqlDate("report_date"),
//                :ffd.getFieldAsSqlDate("from_date"),
//                :ffd.getFieldAsSqlDate("to_date"),
//                :ffd.getFieldData("tran_code"),
//                :ffd.getFieldData("tran_code_qualifier"),
//                :ffd.getFieldData("tran_component_seq_num"),
//                :ffd.getFieldData("dest_bin"),
//                :ffd.getFieldData("source_bin"),
//                :ffd.getFieldData("sre_id_reporting_for"),
//                :ffd.getFieldData("sre_id_rollup_to"),
//                :ffd.getFieldData("sre_id_funds_transfer"),
//                :ffd.getFieldData("settlement_service_id"),
//                :ffd.getFieldData("settlement_currency_code"),
//                :ffd.getFieldData("amount_type"),
//                :ffd.getFieldData("business_mode"),
//                :ffd.getFieldData("no_data_indicator"),
//                :ffd.getFieldData("report_group"),
//                :ffd.getFieldData("report_subgroup"),
//                :ffd.getFieldData("report_id_number"),
//                :ffd.getFieldData("report_id_suffix"),
//                :ffd.getFieldData("business_transaction_type"),
//                :ffd.getFieldData("business_transaction_cycle"),
//                :ffd.getFieldData("reversal_indicator"),
//                :ffd.getFieldData("reimbursement_attribute"),
//                :ffd.getFieldAsInt("credits_count"),
//                :ffd.getFieldAsDouble("credits_amount"),
//                :ffd.getFieldAsInt("debits_count"),
//                :ffd.getFieldAsDouble("debits_amount"),
//                :ffd.getFieldAsDouble("total_amount"),
//                :ffd.getFieldData("total_amount_sign"),
//                :loadFilename,
//                load_filename_to_load_file_id(:loadFilename)
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1508 = ffd.getFieldAsSqlDate("settlement_date");
 java.sql.Date __sJT_1509 = ffd.getFieldAsSqlDate("report_date");
 java.sql.Date __sJT_1510 = ffd.getFieldAsSqlDate("from_date");
 java.sql.Date __sJT_1511 = ffd.getFieldAsSqlDate("to_date");
 String __sJT_1512 = ffd.getFieldData("tran_code");
 String __sJT_1513 = ffd.getFieldData("tran_code_qualifier");
 String __sJT_1514 = ffd.getFieldData("tran_component_seq_num");
 String __sJT_1515 = ffd.getFieldData("dest_bin");
 String __sJT_1516 = ffd.getFieldData("source_bin");
 String __sJT_1517 = ffd.getFieldData("sre_id_reporting_for");
 String __sJT_1518 = ffd.getFieldData("sre_id_rollup_to");
 String __sJT_1519 = ffd.getFieldData("sre_id_funds_transfer");
 String __sJT_1520 = ffd.getFieldData("settlement_service_id");
 String __sJT_1521 = ffd.getFieldData("settlement_currency_code");
 String __sJT_1522 = ffd.getFieldData("amount_type");
 String __sJT_1523 = ffd.getFieldData("business_mode");
 String __sJT_1524 = ffd.getFieldData("no_data_indicator");
 String __sJT_1525 = ffd.getFieldData("report_group");
 String __sJT_1526 = ffd.getFieldData("report_subgroup");
 String __sJT_1527 = ffd.getFieldData("report_id_number");
 String __sJT_1528 = ffd.getFieldData("report_id_suffix");
 String __sJT_1529 = ffd.getFieldData("business_transaction_type");
 String __sJT_1530 = ffd.getFieldData("business_transaction_cycle");
 String __sJT_1531 = ffd.getFieldData("reversal_indicator");
 String __sJT_1532 = ffd.getFieldData("reimbursement_attribute");
 int __sJT_1533 = ffd.getFieldAsInt("credits_count");
 double __sJT_1534 = ffd.getFieldAsDouble("credits_amount");
 int __sJT_1535 = ffd.getFieldAsInt("debits_count");
 double __sJT_1536 = ffd.getFieldAsDouble("debits_amount");
 double __sJT_1537 = ffd.getFieldAsDouble("total_amount");
 String __sJT_1538 = ffd.getFieldData("total_amount_sign");
   String theSqlTS = "insert into visa_settlement_recon_vss115\n            (\n              settlement_date,\n              report_date,\n              from_date,\n              to_date,\n              tran_code,\n              tran_code_qualifier,\n              tran_component_seq_num,\n              dest_bin,\n              source_bin,\n              sre_id_reporting_for,\n              sre_id_rollup_to,\n              sre_id_funds_transfer,\n              settlement_service_id,\n              settlement_currency_code,\n              amount_type,\n              business_mode,\n              no_data_indicator,\n              report_group,\n              report_subgroup,\n              report_id_number,\n              report_id_suffix,\n              business_transaction_type,\n              business_transaction_cycle,\n              reversal_indicator,\n              reimbursement_attribute,\n              credits_count,\n              credits_amount,\n              debits_count,\n              debits_amount,\n              total_amount,\n              total_amount_sign,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n               :7 ,\n               :8 ,\n               :9 ,\n               :10 ,\n               :11 ,\n               :12 ,\n               :13 ,\n               :14 ,\n               :15 ,\n               :16 ,\n               :17 ,\n               :18 ,\n               :19 ,\n               :20 ,\n               :21 ,\n               :22 ,\n               :23 ,\n               :24 ,\n               :25 ,\n               :26 ,\n               :27 ,\n               :28 ,\n               :29 ,\n               :30 ,\n               :31 ,\n               :32 ,\n              load_filename_to_load_file_id( :33 )\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.settlement.SettlementReconLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1508);
   __sJT_st.setDate(2,__sJT_1509);
   __sJT_st.setDate(3,__sJT_1510);
   __sJT_st.setDate(4,__sJT_1511);
   __sJT_st.setString(5,__sJT_1512);
   __sJT_st.setString(6,__sJT_1513);
   __sJT_st.setString(7,__sJT_1514);
   __sJT_st.setString(8,__sJT_1515);
   __sJT_st.setString(9,__sJT_1516);
   __sJT_st.setString(10,__sJT_1517);
   __sJT_st.setString(11,__sJT_1518);
   __sJT_st.setString(12,__sJT_1519);
   __sJT_st.setString(13,__sJT_1520);
   __sJT_st.setString(14,__sJT_1521);
   __sJT_st.setString(15,__sJT_1522);
   __sJT_st.setString(16,__sJT_1523);
   __sJT_st.setString(17,__sJT_1524);
   __sJT_st.setString(18,__sJT_1525);
   __sJT_st.setString(19,__sJT_1526);
   __sJT_st.setString(20,__sJT_1527);
   __sJT_st.setString(21,__sJT_1528);
   __sJT_st.setString(22,__sJT_1529);
   __sJT_st.setString(23,__sJT_1530);
   __sJT_st.setString(24,__sJT_1531);
   __sJT_st.setString(25,__sJT_1532);
   __sJT_st.setInt(26,__sJT_1533);
   __sJT_st.setDouble(27,__sJT_1534);
   __sJT_st.setInt(28,__sJT_1535);
   __sJT_st.setDouble(29,__sJT_1536);
   __sJT_st.setDouble(30,__sJT_1537);
   __sJT_st.setString(31,__sJT_1538);
   __sJT_st.setString(32,loadFilename);
   __sJT_st.setString(33,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:307^11*/
        }
      }
    }
    catch( Exception e )
    {
      logEntry("loadVisaReconData()",e.toString());
    }
    finally
    {
    }
  }
  
  public static void main( String[] args )
  {
    SettlementReconLoader     loader  = null;
    
    try
    {
      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      
      loader = new SettlementReconLoader();
      loader.connect(true);
      
      if ( "loadMasterCardReconData".equals(args[0]) )
      {
        loader.loadMasterCardReconData(args[1]);
      }
      else if ( "loadVisaReconData".equals(args[0]) )
      {      
        loader.loadVisaReconData(args[1]);
      }
      else
      {
        System.out.println("Unrecognized command: " + args[0]);
      }
      
      Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      long elapsed = (endTime.getTime() - beginTime.getTime());
      
      System.out.println("Begin Time: " + String.valueOf(beginTime));
      System.out.println("End Time  : " + String.valueOf(endTime));
      System.out.println("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));      
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }      
    finally
    {
      try{ loader.cleanUp(); } catch( Exception ee ){}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/