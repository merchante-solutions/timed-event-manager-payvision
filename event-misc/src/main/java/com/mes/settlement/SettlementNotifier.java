/*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/settlement/SettlementNotifier.java $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2009-12-18 15:00:09 -0800 (Fri, 18 Dec 2009) $
  Version            : $Revision: 16837 $

  Change History:
     See SVN database

  Copyright (C) 2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import com.mes.support.SyncLog;

public class SettlementNotifier
{
  public static void alertMasterCardAdmin( String errorMsg ) 
  {
    try
    {
      // send file 
      MailMessage msg = new MailMessage();
      
      msg.setAddresses(MesEmails.MSG_ADDRS_OUTGOING_MC_FAILURE);
      msg.setSubject("MasterCard Settlement System Failure");
      msg.setText("The following settlement system failure has been reported: \n\n" + errorMsg);
      msg.send();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.SettlementNotifier::alertMasterCardAdmin()",e.toString());
      System.out.println(e.toString());
    }      
    finally
    {
    }
  }

  public static void alertVisaAdmin( String errorMsg, String reportFilename ) 
  {
    try
    {
      // send file 
      MailMessage msg = new MailMessage();
      
      msg.setAddresses(MesEmails.MSG_ADDRS_OUTGOING_VISA_FAILURE);
      msg.setSubject("Visa Settlement System Failure");
      msg.setText("The following settlement system failure has been reported: \n\n" + errorMsg);
      if ( reportFilename != null && !"null".equals(reportFilename) )
      {
        msg.addFile(reportFilename);
      }
      msg.send();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.SettlementNotifier::alertVisaAdmin()",e.toString());
      System.out.println(e.toString());
    }      
    finally
    {
    }
  }
  
  public static void sendMasterCardReports( String reportFilename )
  {
    try
    {
      // send file 
      MailMessage msg = new MailMessage();
      
      msg.setAddresses(MesEmails.MSG_ADDRS_OUTGOING_MC_NOTIFY);
      msg.setSubject("MasterCard Report - " + reportFilename);
      msg.setText("MasterCard Settlement report is attached.");
      msg.addFile(reportFilename);
      msg.send();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.SettlementNotifier::sendMasterCardReports(" + reportFilename + ")",e.toString());
      System.out.println(e.toString());
    }      
    finally
    {
    }
  }

  public static void sendVisaReports( String reportFilename )
  {
    try
    {
      // send file 
      MailMessage msg = new MailMessage();
      
      msg.setAddresses(MesEmails.MSG_ADDRS_OUTGOING_VISA_NOTIFY);
      msg.setSubject("VSS Report - " + reportFilename);
      msg.setText("Visa Settlement Service report is attached.");
      msg.addFile(reportFilename);
      msg.send();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.SettlementNotifier::sendVisaReports(" + reportFilename + ")",e.toString());
      System.out.println(e.toString());
    }      
    finally
    {
    }
  }

  public static void main( String[] args ) 
  {
    try
    {
      com.mes.database.SQLJConnectionBase.initStandalone("DEBUG");
    
      if ( args.length == 0 )
      {
        System.out.println("Function required");
      }
      else if ( "alertMasterCardAdmin".equals(args[0]) )
      {
        SettlementNotifier.alertMasterCardAdmin(args[1]);
      }
      else if ( "alertVisaAdmin".equals(args[0]) )
      {
        String reportFilename = (args.length > 2) ? args[2] : null;
        SettlementNotifier.alertVisaAdmin(args[1],reportFilename);
      }
      else if ( "sendMasterCardReports".equals(args[0]) )
      {
        SettlementNotifier.sendMasterCardReports(args[1]);
      }
      else if ( "sendVisaReports".equals(args[0]) )
      {
        SettlementNotifier.sendVisaReports(args[1]);
      }
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
  }
}