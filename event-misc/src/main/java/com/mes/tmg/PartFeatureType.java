package com.mes.tmg;

import org.apache.log4j.Logger;

public class PartFeatureType extends TmgBean
{
  static Logger log = Logger.getLogger(PartFeatureType.class);

  private String  ftCode;
  private String  ftName;
  private String  requiredFlag;
  private String  exclusiveFlag;
  private int     displayOrder;
  
  public boolean equals(Object o)
  {
    if (o instanceof PartFeatureType)
    {
      PartFeatureType that = (PartFeatureType)o;
      return this.ftCode.equals(that.ftCode);
    }
    return false;
  }

  public int hashCode()
  {
    return ftCode.hashCode();
  }

  public void setFtCode(String ftCode)
  {
    this.ftCode = ftCode;
  }
  public String getFtCode()
  {
    return ftCode;
  }
  
  public void setFtName(String ftName)
  {
    this.ftName = ftName;
  }
  public String getFtName()
  {
    return ftName;
  }

  public void setIsRequired(String requiredFlag)
  {
    validateFlag(requiredFlag,"required");
    this.requiredFlag = requiredFlag;
  }
  public String getIsRequired()
  {
    return flagValue(requiredFlag);
  }
  public boolean isRequired()
  {
    return flagBoolean(requiredFlag);
  }
  
  public void setIsExclusive(String exclusiveFlag)
  {
    validateFlag(exclusiveFlag,"exclusive");
    this.exclusiveFlag = exclusiveFlag;
  }
  public String getIsExclusive()
  {
    return flagValue(exclusiveFlag);
  }
  public boolean isExclusive()
  {
    return flagBoolean(exclusiveFlag);
  }

  public void setDisplayOrder(int displayOrder)
  {
    this.displayOrder = displayOrder;
  }
  public int getDisplayOrder()
  {
    return displayOrder;
  }
  
  public String toString()
  {
    return "PartFeatureType [ "
      + "code: " + ftCode 
      + ", name: " + ftName 
      + ", required: " + requiredFlag
      + ", exclusive: " + exclusiveFlag
      + ", order: " + displayOrder
      + " ]";
  }
}