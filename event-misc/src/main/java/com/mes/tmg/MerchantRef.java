package com.mes.tmg;

public class MerchantRef extends TmgBean
{
  private long    appSeqNum;
  private long    mercCntrlNumber;
  private String  merchNumber;
  private String  merchBusinessName;
  private OlaRef  olaRef;

  public void setAppSeqNum(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
  }
  public long getAppSeqNum()
  {
    return appSeqNum;
  }

  public void setMercCntrlNumber(long mercCntrlNumber)
  {
    this.mercCntrlNumber = mercCntrlNumber;
  }
  public long getMercCntrlNumber()
  {
    return mercCntrlNumber;
  }

  public void setMerchNumber(String merchNumber)
  {
    this.merchNumber = merchNumber;
  }
  public String getMerchNumber()
  {
    return merchNumber;
  }

  public void setMerchBusinessName(String merchBusinessName)
  {
    this.merchBusinessName = merchBusinessName;
  }
  public String getMerchBusinessName()
  {
    return merchBusinessName;
  }

  public void setOlaRef(OlaRef olaRef)
  {
    this.olaRef = olaRef;
  }
  public OlaRef getOlaRef()
  {
    return olaRef;
  }

  public String toString()
  {
    return "MerchantRef ["
      + " app seq num: " + appSeqNum
      + ", ctrl num: " + mercCntrlNumber
      + ", merch num: " + merchNumber
      + ", name: " + merchBusinessName
      + ", app: " + olaRef
      + " ]";
  }
}