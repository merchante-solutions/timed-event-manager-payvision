package com.mes.tmg;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

public class PartHistory
{
  static Logger log = Logger.getLogger(PartHistory.class);

  private List states = new ArrayList();
  private int curIdx = -1;

  /**
   * Adds a state to the state list.  Order/correctness is not checked and it
   * is assumed that states will be added in chronological order...
   */
  public void add(PartState state)
  {
    states.add(state);
  }

  public List getStates()
  {
    return states;
  }

  /**
   * Scans the state history for a date range that the given date falls in,
   * If a state start time is less than or equal to the date and the state 
   * end time is after the date or is null the state is returned.  Returns 
   * null if no state exists for the date.
   */
  public PartState getState(Date date)
  {
    long dateMs = date.getTime();
    for (Iterator i = states.iterator(); i.hasNext();)
    {
      PartState state = (PartState)i.next();
      long startMs = state.getStartDate().getTime();
      long endMs = i.hasNext() ? state.getEndDate().getTime() : -1L;
      if (dateMs >= startMs && (endMs == -1L || dateMs < endMs))
      {
        return state;
      }
    }
    return null;
  }

  public boolean hasNextState()
  {
    return curIdx < states.size() - 1;
  }
  public void setNextState()
  {
    if (hasNextState())
    {
      ++curIdx;
    }
  }

  public boolean hasPriorState()
  {
    return curIdx > 0;
  }
  public void setPriorState()
  {
    if (hasPriorState())
    {
      --curIdx;
    }
  }

  public void setState(long stateId)
  {
    int newIdx = -1;
    for (int i = 0; i < states.size() && newIdx == -1; ++i)
    {
      PartState state = (PartState)states.get(i);
      if (state.getStateId() == stateId)
      {
        newIdx = i;
      }
    }
    if (newIdx == -1)
    {
      throw new NullPointerException("State with id " + stateId 
        + " does not exist in history");
    }
    curIdx = newIdx;
  }
  public void setState(PartState state)
  {
    setState(state.getStateId());
  }

  public PartState getState()
  {
    return (PartState)states.get(curIdx);
  }

  /** 
   * If a state exists at the end of the list for the current time 
   * (i.e. end date is null), that state is returned.
   */
  public PartState getCurrentState()
  {
    if (!states.isEmpty())
    {
      PartState state = (PartState)states.get(states.size() - 1);
      if (state.getEndDate() == null)
      {
        return state;
      }
    }
    return null;
  }

  /**
   * Returns the part with state as of the specified date.
   */
  public Part getPart(Date date)
  {
    PartState state = getState(date);
    return state != null ? state.getPart() : null;
  }

  /**
   * Returns a part in it's current state.
   */
  public Part getCurrentPart()
  {
    PartState state = getCurrentState();
    return state != null ? state.getPart() : null;
  }

  /**
   * Returns the part id of the most recent state.  If no states are present 0
   * is returned.
   */
  public long getPartId()
  {
    long partId = 0;
    if (!states.isEmpty())
    {
      PartState state = (PartState)states.get(states.size() - 1);
      partId = state.getPartId();
    }
    return partId;
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    for (Iterator i = states.iterator(); i.hasNext();)
    {
      if (buf.length() > 0) buf.append(", ");
      buf.append(" " + i.next());
    }
    return "PartHistory [ " + buf + " ]";
  }
}