/**
 * TmgViewBean
 *
 * Class intended to serve as a base for view form field beans.  This is a
 * FieldBean based class, so it has all the standard FieldBean functionality.
 *
 * It is not well suited for session based beans, however, due to the action
 * related initializations done in the main constructor.  In particular the handler
 * and action objects are request scope objects that would be obsolete after
 * the initial request was processed.
 */ 

package com.mes.tmg;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.mvc.Action;
import com.mes.mvc.mes.MesViewBean;

public class TmgViewBean extends MesViewBean
{
  static Logger log = Logger.getLogger(TmgViewBean.class);

  protected TmgDb db;

  public TmgViewBean()
  {
  }
  public TmgViewBean(TmgAction action, String beanName, int beanIdx)
  {
    super(action,beanName,beanIdx);
  }
  public TmgViewBean(TmgAction action, String beanName)
  {
    super(action,beanName);
  }
  public TmgViewBean(TmgAction action)
  {
    super(action);
  }

  public void setTmgDb(TmgDb db)
  {
    this.db = db;
  }

  public void setAction(Action action, String requestName)
  {
    setTmgDb(((TmgAction)action).getDb());
    super.setAction(action,requestName);
  }

  public void initialize(TmgDb db, HttpServletRequest request)
  {
    setTmgDb(((TmgAction)action).getDb());
    autoSetFields(request);
  }

  private static SimpleDateFormat defaultSdf = new SimpleDateFormat(
    "'<nobr>'EEE M/d/yy'</nobr> <nobr>'h:mma'</nobr>'");

  private static String formatHtmlDate(Date date, SimpleDateFormat sdf)
  {
    if (date == null)
    {
      return "--";
    }
    return sdf.format(date);
  }
  public static String formatHtmlDate(Date date)
  {
    return formatHtmlDate(date,defaultSdf);
  }
  public static String formatHtmlDate(Date date, String formatStr)
  {
    return formatHtmlDate(date,new SimpleDateFormat(formatStr));
  }

  public Client getClient()
  {
    return ((TmgRequestHandler)handler).getTmgSession()
                                          .getClient(action.getUser());
  }

  public boolean isMultiClient()
  {
    Client client = getClient();
    return client != null && client.isMes();
  }
}
