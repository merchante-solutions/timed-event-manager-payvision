package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class CallTag extends TmgBean
{
  private long          ctId;
  private long          ordId;
  private Date          createDate;
  private String        userName;
  private String        trackingNum;
  private String        ctStatCode;

  private CallTagStatus callTagStatus;
  private List          callTagParts;
  private List          callTagNotices;

  public void setCtId(long ctId)
  {
    this.ctId = ctId;
  }
  public long getCtId()
  {
    return ctId;
  }

  public void setOrdId(long ordId)
  {
    this.ordId = ordId;
  }
  public long getOrdId()
  {
    return ordId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setTrackingNum(String trackingNum)
  {
    this.trackingNum = trackingNum;
  }
  public String getTrackingNum()
  {
    return trackingNum;
  }

  public void setCtStatCode(String ctStatCode)
  {
    this.ctStatCode = ctStatCode;
  }
  public String getCtStatCode()
  {
    return ctStatCode;
  }

  public void setCallTagStatus(CallTagStatus callTagStatus)
  {
    this.callTagStatus = callTagStatus;
  }
  public CallTagStatus getCallTagStatus()
  {
    return callTagStatus;
  }

  public void setCallTagNotices(List callTagNotices)
  {
    this.callTagNotices = callTagNotices;
  }
  public List getCallTagNotices()
  {
    return callTagNotices;
  }

  public void setCallTagParts(List callTagParts)
  {
    this.callTagParts = callTagParts;
  }
  public List getCallTagParts()
  {
    return callTagParts;
  }

  public boolean isNew()
  {
    return CallTagStatus.CTS_NEW.equals(ctStatCode);
  }
  public boolean isClosed()
  {
    return CallTagStatus.CTS_CLOSED.equals(ctStatCode);
  }
  public boolean isShipped()
  {
    return CallTagStatus.CTS_SHIPPED.equals(ctStatCode);
  }
  public boolean isCanceled()
  {
    return CallTagStatus.CTS_CANCELED.equals(ctStatCode);
  }

  public boolean isActive()
  {
    return isNew() || isShipped();
  }

  public boolean canCancel()
  {
    // only possible while call tag is new
    return isNew();
  }
  public boolean canUncancel()
  {
    return isCanceled();
  }
  public boolean canShip()
  {
    // must be new and have at least one new, non-canceled ct part
    if (isNew())
    {
      for (Iterator i = callTagParts.iterator(); i.hasNext(); )
      {
        if (((CallTagPart)i.next()).isNew()) return true;
      }
    }
    return false;
  }

  public boolean canClose()
  {
    // must be shipped to close
    if (!isShipped()) return false;

    // no call tag parts may be pending
    for (Iterator i = callTagParts.iterator(); i.hasNext(); )
    {
      if (((CallTagPart)i.next()).isPending()) return false;
    }
    return true;
  }

  public boolean hasUncanceledParts()
  {
    // true if any call tag parts are not canceled
    for (Iterator i = callTagParts.iterator(); i.hasNext(); )
    {
      if (!((CallTagPart)i.next()).isCanceled()) return true;
    }
    return false;
  }

  public boolean canAddNotice()
  {
    return true;
  }

  public void cancel()
  {
    if (!canCancel())
    {
      throw new RuntimeException("Cannot cancel call tag");
    }
    ctStatCode = CallTagStatus.CTS_CANCELED;
  }
  public void uncancel()
  {
    if (!canUncancel())
    {
      throw new RuntimeException("Cannot uncancel call tag");
    }
    ctStatCode = CallTagStatus.CTS_NEW;
  }
  public void ship()
  {
    ctStatCode = CallTagStatus.CTS_SHIPPED;
  }
  public void close()
  {
    ctStatCode = CallTagStatus.CTS_CLOSED;
  }

  public boolean receivedPart(long partId)
  {
    for (Iterator i = callTagParts.iterator(); i.hasNext();)
    {
      CallTagPart ctPart = (CallTagPart)i.next();
      if (ctPart.receivedPart(partId))
      {
        return true;
      }
    }
    return false;
  }

  public String toString()
  {
    return "CallTag ["
      + " ct id: " + ctId
      + ", ord id: " + ordId
      + ", status: " + ctStatCode
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + (trackingNum != null ? ", track no.: " + trackingNum : "")
      + " ]";
  }
}