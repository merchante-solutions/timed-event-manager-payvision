package com.mes.tmg.ship;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.Shipper;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class ShipperBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(ShipperBean.class);
  
  public static final String FN_SHIPPER_ID    = "shipperId";
  public static final String FN_SHIPPER_NAME  = "shipperName";
  public static final String FN_TRACKING_URL  = "trackingUrl";
  public static final String FN_SUBMIT_BTN    = "submit";

  private Shipper shipper;

  public ShipperBean(TmgAction action)
  {
    super(action);
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_SHIPPER_ID));
      fields.addAlias(FN_SHIPPER_ID,"editId");
      fields.add(new Field(FN_SHIPPER_NAME,"Name",100,32,false));
      fields.add(new Field(FN_TRACKING_URL,"Name",1000,64,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  /**
   * Persists purchase order.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long shipperId = getField(FN_SHIPPER_ID).asLong();
      
      if (shipperId == 0L)
      {
        shipperId = db.getNewId();
        shipper = new Shipper();
        shipper.setShipperId(shipperId);
      }
      else
      {
        shipper = db.getShipper(shipperId);
      }

      shipper.setShipperName(getData(FN_SHIPPER_NAME));
      shipper.setTrackingUrl(getData(FN_TRACKING_URL));
      
      db.persist(shipper,user);
      
      submitOk = true;
    }
    catch (Exception e)   
    {
      log.error("Error submitting shipper: " + e);
    }

    return submitOk;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long shipperId = getField(FN_SHIPPER_ID).asLong();
      
      if (shipperId != 0L)
      {
        shipper = db.getShipper(shipperId);
        setData(FN_SHIPPER_NAME,shipper.getShipperName());
        setData(FN_TRACKING_URL,shipper.getTrackingUrl());
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading shipper: " + e);
    }
    
    return loadOk;
  }
}