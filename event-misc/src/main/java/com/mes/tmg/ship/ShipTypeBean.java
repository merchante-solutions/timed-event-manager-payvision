package com.mes.tmg.ship;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.ShipType;
import com.mes.tmg.Shipper;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class ShipTypeBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(ShipTypeBean.class);
  
  public static final String FN_SHIP_TYPE_ID    = "shipTypeId";
  public static final String FN_SHIPPER_ID      = "shipperId";
  public static final String FN_SHIP_TYPE_NAME  = "shipTypeName";
  public static final String FN_SUBMIT_BTN      = "submit";

  private ShipType shipType;

  public ShipTypeBean(TmgAction action)
  {
    super(action);
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_SHIP_TYPE_ID));
      fields.addAlias(FN_SHIP_TYPE_ID,"editId");
      // hack: work around since create fields is being called by superclass
      //       constructor, have to access shipper id via the request parm
      Shipper shipper = (Shipper)request.getAttribute("shipper");
      fields.add(new HiddenField(FN_SHIPPER_ID,String.valueOf(shipper.getShipperId())));
      fields.add(new Field(FN_SHIP_TYPE_NAME,"Name",100,32,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public ShipType getShipType()
  {
    return shipType;
  }
  
  /**
   * Persist ShipType bean.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long shipTypeId = getField(FN_SHIP_TYPE_ID).asLong();
      
      if (shipTypeId == 0L)
      {
        shipTypeId = db.getNewId();
        shipType = new ShipType();
        shipType.setShipTypeId(shipTypeId);
        setData(FN_SHIP_TYPE_ID,String.valueOf(shipTypeId));
      }
      else
      {
        shipType = db.getShipType(shipTypeId);
      }

      shipType.setShipperId(getField(FN_SHIPPER_ID).asLong());
      shipType.setShipTypeName(getData(FN_SHIP_TYPE_NAME));
      
      db.persist(shipType,user);
      
      submitOk = true;
    }
    catch (Exception e)   
    {
      log.error("Error submitting shipType: " + e);
    }

    return submitOk;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long shipTypeId = getField(FN_SHIP_TYPE_ID).asLong();
      
      if (shipTypeId != 0L)
      {
        shipType = db.getShipType(shipTypeId);
        setData(FN_SHIP_TYPE_NAME,shipType.getShipTypeName());
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading ship type: " + e);
    }
    
    return loadOk;
  }
}