package com.mes.tmg.ship;

import org.apache.log4j.Logger;
import com.mes.mvc.View;
import com.mes.tmg.ShipType;
import com.mes.tmg.Shipper;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class ShipperAction extends TmgAction
{
  static Logger log = Logger.getLogger(ShipperAction.class);

  public String getActionObject(String actionName)
  {
    if (actionName.matches(".*Shippers?"))
    {
      return "Shipper";
    }
    else if (actionName.matches(".*ShipTypes?"))
    {
      return "Ship type";
    }
    return super.getActionObject(actionName);
  }

  /**
   * Shipper handling
   */
  private void doShowShippers()
  {
    request.setAttribute("showItems",db.getShippers());
    doView(Tmg.VIEW_SHOW_SHIPPERS);
  }
  
  private void doAddEditShipper()
  {
    ShipperBean bean = new ShipperBean(this);

    boolean isEdit = name.equals(Tmg.ACTION_EDIT_SHIPPER);
    
    if (bean.isAutoSubmitOk())
    {
      addFeedback("Shipper " + (isEdit ? "updated" : "added"));
      doActionRedirect(Tmg.ACTION_SHOW_SHIPPERS);
    }
    else
    {
      request.setAttribute("viewBean",bean);
      String viewName = isEdit ? 
                        Tmg.VIEW_ADD_SHIPPER : 
                        Tmg.VIEW_EDIT_SHIPPER;
      String title = (isEdit ? "Edit" : "Add") + " Shipper";
      addFeedback(bean);
      doView(viewName,title);
    }
  }

  private void doDeleteShipper()
  {
    long delId = getParmLong("delId");
  
    if (isConfirmed())
    {
      if (db.getShipper(delId) == null)
      {
        throw new RuntimeException("No shipper with id " + delId + " exists"
          + " to delete");
      }
    
      db.deleteShipper(delId,user);
      addFeedback("Shipper " + delId + " deleted");
      doActionRedirect(Tmg.ACTION_SHOW_SHIPPERS);
    }
    else if (isCanceled())
    {
      addFeedback("Delete canceled");
      doActionRedirect(Tmg.ACTION_SHOW_SHIPPERS);
    }
    else
    {
      confirm("Delete shipper?");
    }
  }

  private Shipper getCurrentShipper()
  {
    Shipper shipper = null;
    long shipperId = getParmLong("shipperId",-1L);
    if (shipperId != -1L)
    {
      shipper = db.getShipper(shipperId);
      if (shipper != null)
      {
        setSessionAttr("shipper",shipper);
      }
    }
    else
    {
      shipper = (Shipper)getSessionAttr("shipper");
    }
    return shipper;
  }

  private void reloadCurrentShipper()
  {
    Shipper shipper = (Shipper)getSessionAttr("shipper");
    if (shipper != null)
    {
      setSessionAttr("shipper",db.getShipper(shipper.getShipperId()));
    }
  }

  /**
   * ShipType handling
   */
  private void doShowShipTypes()
  {
    Shipper shipper = getCurrentShipper();
    setRequestAttr("shipper",shipper);
    setRequestAttr("showItems",shipper.getShipTypes());
    View view = handler.getView(Tmg.VIEW_SHOW_SHIP_TYPES);
    if (view.isTitled())
    {
      doView(Tmg.VIEW_SHOW_SHIP_TYPES,shipper.getShipperName() + " " 
        + view.getTitle());
    }
    else
    {
      doView(Tmg.VIEW_SHOW_SHIP_TYPES);
    }
  }
  
  private void doAddEditShipType()
  {
    setRequestAttr("shipper",getCurrentShipper());
    ShipTypeBean bean = new ShipTypeBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_SHIP_TYPE);

    if (bean.isAutoSubmitOk())
    {
      ShipType shipType = bean.getShipType();
      logAndRedirect(Tmg.ACTION_SHOW_SHIP_TYPES,shipType.getShipTypeName(),shipType.getShipTypeId());
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_SHIP_TYPE : Tmg.VIEW_ADD_SHIP_TYPE);
    }
  }

  private void doDeleteShipType()
  {
    long delId = getParmLong("delId");
    ShipType shipType = db.getShipType(delId);
  
    if (isConfirmed())
    {
      if (shipType == null)
      {
        throw new RuntimeException("No ship type with id " + delId + " exists"
          + " to delete");
      }
    
      db.deleteShipType(delId,user);
      reloadCurrentShipper();
      addFeedback("Ship type '" + shipType.getShipTypeName() + "' deleted");
      doActionRedirect(Tmg.ACTION_SHOW_SHIP_TYPES);
    }
    else if (isCanceled())
    {
      addFeedback("Delete canceled");
      doActionRedirect(Tmg.ACTION_SHOW_SHIP_TYPES);
    }
    else
    {
      confirm("Delete ship type '" + shipType.getShipTypeName() + "'?");
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_SHOW_SHIPPERS))
    {
      doShowShippers();
    }
    else if (name.equals(Tmg.ACTION_EDIT_SHIPPER) ||
             name.equals(Tmg.ACTION_ADD_SHIPPER))
    {
      doAddEditShipper();
    }
    else if (name.equals(Tmg.ACTION_DELETE_SHIPPER))
    {
      doDeleteShipper();
    }
    else if (name.equals(Tmg.ACTION_SHOW_SHIP_TYPES))
    {
      doShowShipTypes();
    }
    else if (name.equals(Tmg.ACTION_EDIT_SHIP_TYPE) ||
             name.equals(Tmg.ACTION_ADD_SHIP_TYPE))
    {
      doAddEditShipType();
    }
    else if (name.equals(Tmg.ACTION_DELETE_SHIP_TYPE))
    {
      doDeleteShipType();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
