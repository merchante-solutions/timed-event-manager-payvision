package com.mes.tmg;

import java.util.HashMap;
import java.util.Map;

public class CallTagStatusManager extends TmgBean
{
  private Map statusMap = new HashMap();

  public class CtPartStatus
  {
    long partId;
    String code;
    String description;
    String serialNum;

    public CtPartStatus(long partId, String code, String description, String serialNum)
    {
      this.partId = partId;
      this.code = code;
      this.description = description;
      this.serialNum = serialNum;
    }

    public long getPartId() { return partId; }
    public String getCode() { return code; }
    public String getDescription() { return description; }
    public String getSerialNum() { return serialNum; }
  }

  public void addStatus(long partId, String code, String description, String serialNum)
  {
    statusMap.put(partId ,new CtPartStatus(partId,code,description,serialNum));
  }

  private CtPartStatus getStatus(long partId)
  {
    CtPartStatus ctps = (CtPartStatus)statusMap.get(partId);
    return ctps != null ? ctps : new CtPartStatus(partId,null,null,null);
  }

  public String getStatusCode(long partId)
  {
    return getStatus(partId).getCode();
  }

  public String getStatusDescription(long partId)
  {
    return getStatus(partId).getDescription();
  }

  public String getSerialNum(long partId)
  {
    return getStatus(partId).getSerialNum();
  }
}
