package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class OrderOutPart extends TmgBean
{
  private long          oopId;
  private long          ordId;
  private String        userName;
  private Date          createDate;
  private String        drCode;
  private long          requestedPtId;
  private String        requestedPcCode;
  private String        requestedDispCode;
  private long          assignedPtId;
  private String        assignedPcCode;
  private String        assignedDispCode;
  private long          assignedInvId;
  private long          partId;
  private String        serialNum;
  private String        cancelFlag;
  private String        internalFlag;

  private DeployReason  deployReason;
  private PartType      requestedType;
  private PartClass     requestedClass;
  private Disposition   requestedDisposition;
  private PartType      assignedType;
  private PartClass     assignedClass;
  private Disposition   assignedDisposition;
  private Inventory     assignedInventory;

  public void setOopId(long oopId)
  {
    this.oopId = oopId;
  }
  public long getOopId()
  {
    return oopId;
  }

  public void setOrdId(long ordId)
  {
    this.ordId = ordId;
  }
  public long getOrdId()
  {
    return ordId;
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setDrCode(String drCode)
  {
    this.drCode = drCode;
  }
  public String getDrCode()
  {
    return drCode;
  }

  public void setDeployReason(DeployReason deployReason)
  {
    this.deployReason = deployReason;
  }
  public DeployReason getDeployReason()
  {
    return deployReason;
  }

  public void setRequestedPtId(long requestedPtId)
  {
    this.requestedPtId = requestedPtId;
  }
  public long getRequestedPtId()
  {
    return requestedPtId;
  }

  public void setRequestedType(PartType requestedType)
  {
    this.requestedType = requestedType;
  }
  public PartType getRequestedType()
  {
    return requestedType;
  }

  public void setRequestedDispCode(String requestedDispCode)
  {
    this.requestedDispCode = requestedDispCode;
  }
  public String getRequestedDispCode()
  {
    return requestedDispCode;
  }

  public void setRequestedDisposition(Disposition requestedDisposition)
  {
    this.requestedDisposition = requestedDisposition;
  }
  public Disposition getRequestedDisposition()
  {
    return requestedDisposition;
  }

  public void setAssignedPtId(long assignedPtId)
  {
    this.assignedPtId = assignedPtId;
  }
  public long getAssignedPtId()
  {
    return assignedPtId;
  }

  public void setAssignedType(PartType assignedType)
  {
    this.assignedType = assignedType;
  }
  public PartType getAssignedType()
  {
    return assignedType;
  }

  public void setRequestedPcCode(String requestedPcCode)
  {
    this.requestedPcCode = requestedPcCode;
  }
  public String getRequestedPcCode()
  {
    return requestedPcCode;
  }

  public void setRequestedClass(PartClass requestedClass)
  {
    this.requestedClass = requestedClass;
  }
  public PartClass getRequestedClass()
  {
    return requestedClass;
  }

  public void setAssignedPcCode(String assignedPcCode)
  {
    this.assignedPcCode = assignedPcCode;
  }
  public String getAssignedPcCode()
  {
    return assignedPcCode;
  }

  public void setAssignedClass(PartClass assignedClass)
  {
    this.assignedClass = assignedClass;
  }
  public PartClass getAssignedClass()
  {
    return assignedClass;
  }

  public void setAssignedDispCode(String assignedDispCode)
  {
    this.assignedDispCode = assignedDispCode;
  }
  public String getAssignedDispCode()
  {
    return assignedDispCode;
  }

  public void setAssignedDisposition(Disposition assignedDisposition)
  {
    this.assignedDisposition = assignedDisposition;
  }
  public Disposition getAssignedDisposition()
  {
    return assignedDisposition;
  }

  public void setAssignedInvId(long assignedInvId)
  {
    this.assignedInvId = assignedInvId;
  }
  public long getAssignedInvId()
  {
    return assignedInvId;
  }

  public void setAssignedInventory(Inventory assignedInventory)
  {
    this.assignedInventory = assignedInventory;
  }
  public Inventory getAssignedInventory()
  {
    return assignedInventory;
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }

  public void setInternalFlag(String internalFlag)
  {
    validateFlag(internalFlag,"internal");
    this.internalFlag = internalFlag;
  }
  public String getInternalFlag()
  {
    return flagValue(internalFlag);
  }
  public boolean isInternal()
  {
    return flagBoolean(internalFlag);
  }

  public void setCancelFlag(String cancelFlag)
  {
    validateFlag(cancelFlag,"cancel");
    this.cancelFlag = cancelFlag;
  }
  public String getCancelFlag()
  {
    return flagValue(cancelFlag);
  }
  public boolean isCanceled()
  {
    return flagBoolean(cancelFlag);
  }

  public boolean canCancel()
  {
    return !isInternal() && !isCanceled();
  }

  public void unlink()
  {
    setPartId(0L);
    setSerialNum(null);
    setAssignedPtId(0L);
    setAssignedPcCode(null);
    setAssignedDispCode(null);
    setAssignedInvId(0L);
  }

  public void cancel()
  {
    setCancelFlag(FLAG_YES);
    unlink();
  }

  public boolean isUnlinked()
  {
    return partId == 0L;
  }

  public boolean hasLinkToPart(Part part)
  {
    return partId != 0L && partId == part.getPartId();
  }

  public String toString()
  {
    return "OrderOutPart [ "
      + "id: " + oopId
      + ", ord id: " + ordId
      + ", user: " + userName
      + ", created: " + formatDate(createDate)
      + ", reason: " + drCode
      + ", req pt: " + requestedPtId
      + ", req class: " + requestedPcCode
      + ", req disp: " + requestedDispCode
      + (assignedPtId > 0L ? ", ass pt: " + assignedPtId : "")
      + (assignedPcCode != null ? ", ass class: " + assignedPcCode : "")
      + (assignedDispCode != null ? ", ass disp: " + assignedDispCode : "")
      + (assignedInvId != 0L ? ", ass inv: " + assignedInvId : "")
      + (partId > 0L ? ", part: " + partId : "")
      + (serialNum != null ? ", s/n: " + serialNum : "")
      + ", internal: " + isInternal()
      + ", canceled: " + isCanceled()
      + " ]";
  }
}