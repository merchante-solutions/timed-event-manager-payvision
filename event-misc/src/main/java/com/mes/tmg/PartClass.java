package com.mes.tmg;

public class PartClass extends TmgBean
{
  private long    pcId;
  private String  pcCode;
  private String  pcName;
  private String  deployFlag;

  public void setPcId(long pcId)
  {
    this.pcId = pcId;
  }
  public long getPcId()
  {
    return pcId;
  }

  public void setPcCode(String pcCode)
  {
    this.pcCode = pcCode;
  }
  public String getPcCode()
  {
    return pcCode;
  }

  public void setPcName(String pcName)
  {
    this.pcName = pcName;
  }
  public String getPcName()
  {
    return pcName;
  }

  public void setDeployFlag(String deployFlag)
  {
    validateFlag(deployFlag,"deploy");
    this.deployFlag = deployFlag;
  }
  public String getDeployFlag()
  {
    return flagValue(deployFlag);
  }
  public boolean canDeploy()
  {
    return flagBoolean(deployFlag);
  }

  public String toString()
  {
    return "PartClass ["
      + " id: " + pcId
      + ", code: " + pcCode
      + ", name: " + pcName
      + ", can deploy: " + canDeploy()
      + " ]";
  }
}
