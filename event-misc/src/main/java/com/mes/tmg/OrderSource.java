package com.mes.tmg;

public class OrderSource extends TmgBean
{
  public static final String OSRC_INTERNAL  = "INT";
  public static final String OSRC_OLA       = "OLA";
  public static final String OSRC_ACR       = "ACR";

  private long ordSrcId;
  private String ordSrcCode;
  private String description;

  public void setOrdSrcId(long ordSrcId)
  {
    this.ordSrcId = ordSrcId;
  }
  public long getOrdSrcId()
  {
    return ordSrcId;
  }

  public void setOrdSrcCode(String ordSrcCode)
  {
    this.ordSrcCode = ordSrcCode;
  }
  public String getOrdSrcCode()
  {
    return ordSrcCode;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public String toString()
  {
    return "OrderSource ["
      + " id: " + ordSrcId
      + ", code: " + ordSrcCode
      + ", desc: " + description
      + " ]";
  }
}