package com.mes.tmg.pos;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CurrencyField;
import com.mes.tmg.PurchaseOrderOptions;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class PoOptionsBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PoOptionsBean.class);
  
  public static final String FN_LARGE_AMOUNT  = "largeAmount";
  public static final String FN_SUBMIT_BTN    = "submit";
  
  public PoOptionsBean(TmgAction action)
  {
    super(action);
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new CurrencyField(FN_LARGE_AMOUNT,"Large Amount Threshold",10,10,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  private PurchaseOrderOptions options;

  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      options = db.getPurchaseOrderOptions();
      if (options != null && options.getLargeAmount() > 0)
      {
        setData(FN_LARGE_AMOUNT,String.valueOf(options.getLargeAmount()));
      }
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading po options: " + e);
    }
    
    return loadOk;
  }
  
  /**
   * Persists purchase order.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      options = db.getPurchaseOrderOptions();
      if (options == null)
      {
        options = new PurchaseOrderOptions();
        options.setOptionId(db.getNewId());
        options.setContactId(db.getNewId());
        options.setAddressId(db.getNewId());
      }

      options.setLargeAmount(getField(FN_LARGE_AMOUNT).asDouble());

      db.persist(options,user);
      
      submitOk = true;
    }
    catch (Exception e)   
    {
      e.printStackTrace();
      log.error("Error submitting po options: " + e);
    }

    return submitOk;
  }

  public List getContacts()
  {
    List contacts = options != null ? options.getContacts() : null;
    return contacts != null ? contacts : new ArrayList();
  }

  public List getAddresses()
  {
    List addresses = options != null ? options.getAddresses() : null;
    return addresses != null ? addresses : new ArrayList();
  }

  public long getContactId()
  {
    return options != null ? options.getContactId() : 0L;
  }

  public long getAddressId()
  {
    return options != null ? options.getAddressId() : 0L;
  }
}