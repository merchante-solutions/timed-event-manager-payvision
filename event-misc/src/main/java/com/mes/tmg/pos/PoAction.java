package com.mes.tmg.pos;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.mvc.Link;
import com.mes.persist.Transaction;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.tmg.Address;
import com.mes.tmg.Contact;
import com.mes.tmg.Document;
import com.mes.tmg.EncryptionType;
import com.mes.tmg.Note;
import com.mes.tmg.NoteAction;
import com.mes.tmg.Part;
import com.mes.tmg.PartOpCode;
import com.mes.tmg.PartType;
import com.mes.tmg.PurchaseOrder;
import com.mes.tmg.PurchaseOrderItem;
import com.mes.tmg.PurchaseOrderOptions;
import com.mes.tmg.PurchaseOrderPart;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.Vendor;
import com.mes.tmg.util.DocAction;

public class PoAction extends TmgAction
{
  static Logger log = Logger.getLogger(PoAction.class);

  public String getActionObject(String actionName)
  {
    if (actionName.matches(".*Po"))
    {
      return "Purchase order";
    }
    else if (actionName.matches(".*PoItem") ||
             actionName.equals(Tmg.ACTION_RECEIVE_PO_PARTS))
    {
      return "Purchase order item";
    }
    else if (actionName.matches(".*PoItems"))
    {
      return "Purchase order items";
    }
    else if (actionName.matches(".*PoPart"))
    {
      return "Purchase order part";
    }
    else if (actionName.matches(".*PoParts"))
    {
      return "Purchase order parts";
    }
    else if (actionName.matches(".*PoInvoice"))
    {
      return "Purchase order invoice";
    }
    return null;
  }

  public String getActionResult(String actionName)
  {
    if (actionName.equals(Tmg.ACTION_READY_PO))
    {
      return "readied";
    }
    else if (actionName.equals(Tmg.ACTION_APPROVE_PO))
    {
      return "approved";
    }
    else if (actionName.equals(Tmg.ACTION_CONFIRM_PO))
    {
      return "confirmed";
    }
    else if (actionName.equals(Tmg.ACTION_RECEIVE_PO))
    {
      return "in receiving";
    }
    else if (actionName.equals(Tmg.ACTION_COMPLETE_PO))
    {
      return "completed";
    }
    else if (actionName.equals(Tmg.ACTION_PAY_PO))
    {
      return "paid";
    }
    else if (actionName.equals(Tmg.ACTION_CANCEL_PO) ||
             actionName.equals(Tmg.ACTION_CANCEL_PO_ITEM))
    {
      return "canceled";
    }
    else if (actionName.equals(Tmg.ACTION_UNCANCEL_PO) ||
             actionName.equals(Tmg.ACTION_UNCANCEL_PO_ITEM))
    {
      return "reactivated";
    }
    else if (actionName.equals(Tmg.ACTION_RECEIVE_PO_PARTS))
    {
      return "parts received";
    }
    else if (actionName.equals(Tmg.ACTION_ADD_PO) ||
             actionName.equals(Tmg.ACTION_ADD_PO_ITEM) ||
             actionName.equals(Tmg.ACTION_ADD_PO_INVOICE))
    {
      return "created";
    }
    else if (actionName.equals(Tmg.ACTION_EDIT_PO) ||
             actionName.equals(Tmg.ACTION_EDIT_NEW_PO) ||
             actionName.equals(Tmg.ACTION_EDIT_PO_ITEM) ||
             actionName.equals(Tmg.ACTION_EDIT_PO_INVOICE))
    {
      return "updated";
    }
    else if (actionName.equals(Tmg.ACTION_DELETE_PO_PART))
    {
      return "removed";
    }
    return null;
  }

  /**
   * P.O. actions
   */

  public void persistPo(PurchaseOrder po)
  {
    db.persist(po,user);
    reloadCurrentPo();
  }

  private PurchaseOrder getPurchaseOrder(long poId, boolean nullAllowed)
  {
    PurchaseOrder po = db.getPurchaseOrder(poId);
    if (!nullAllowed && po == null)
    {
      throw new NullPointerException("Action " + name + " attempted to "
       + " get p.o. #" + poId + " but none was found");
    }
    return po;
  }
  private PurchaseOrder getPurchaseOrder(long poId)
  {
    return getPurchaseOrder(poId,true);
  }
    
  private PoLookupBean refreshPoLookupBean()
  {
    PoLookupBean bean = (PoLookupBean)getSessionAttr(Tmg.SN_PO_LOOKUP_BEAN,null);
    if (bean == null)
    {
      bean = new PoLookupBean(this);
      setSessionAttr(Tmg.SN_PO_LOOKUP_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  private void doLookupPos()
  {
    PoLookupBean bean = refreshPoLookupBean();
    doView(Tmg.VIEW_LOOKUP_POS);
  }
  
  private void doAddPo()
  {
    PoAddBean bean = new PoAddBean(this);
    if (bean.isAutoSubmitOk())
    {
      setBackLink(Tmg.ACTION_EDIT_NEW_PO,getBackLink());
      long poId = bean.getPo().getPoId();
      Link link = handler.getLink(Tmg.ACTION_EDIT_NEW_PO,"poId=" + poId);
      logAndRedirect(link,poId);
    }
    else
    {
      doView(Tmg.VIEW_ADD_PO);
    }
  }

  private void doEditPo()
  {
    PoEditBean bean = new PoEditBean(this);
    if (bean.isAutoSubmitOk())
    {
      long poId = bean.getPo().getPoId();
      if (name.equals(Tmg.ACTION_EDIT_NEW_PO))
      {
        setBackLink(Tmg.ACTION_VIEW_NEW_PO,getBackLink());
        Link link = handler.getLink(Tmg.ACTION_VIEW_NEW_PO,"poId=" + poId);
        logAndRedirect(link,poId);
      }
      else
      {
        PurchaseOrder po = getCurrentPo(true);
        if (po != null && po.getPoStatus() != po.PO_STAT_EDITING)
        {
          persistPoStatusChange(poId,PurchaseOrder.PO_STAT_EDITING);
        }
        logAndRedirect(getBackLink(),poId);
      }
    }
    else
    {
      doView(Tmg.VIEW_EDIT_PO);
    }
  }

  private PurchaseOrder getCurrentPo(boolean nullAllowed)
  {
    PurchaseOrder po = null;
    long poId = getParmLong("poId",-1L);
    if (poId != -1L)
    {
      po = db.getPurchaseOrder(poId);
      if (po != null)
      {
        setSessionAttr("po",po);
      }
    }
    else
    {
      po = (PurchaseOrder)getSessionAttr("po");
    }
    if (!nullAllowed && po == null)
    {
      throw new NullPointerException("Action " + name + " attempted to "
       + " get p.o. #" + poId + " but none was found");
    }
    return po;
  }
  private PurchaseOrder getCurrentPo()
  {
    return getCurrentPo(true);
  }

  private void reloadCurrentPo()
  {
    PurchaseOrder po = (PurchaseOrder)getSessionAttr("po",null);
    if (po != null)
    {
      setSessionAttr("po",db.getPurchaseOrder(po.getPoId()));
    }
  }

  private void doViewPo()
  {
    PurchaseOrder po = getCurrentPo(false);
    setRequestAttr("po",po);
    setNoteItemId(po.getPoId());
    doView(Tmg.VIEW_VIEW_PO,"Purchase Order No. " + po.getPoId());
  }

  private String getPlainTextAddress(Address address)
  {
    StringBuffer buf = new StringBuffer();
    String name = address.getAddrName();
    buf.append((name != null ? name + "\n" : ""));
    buf.append(address.getAddrLine1() + "\n");
    String addrLine2 = address.getAddrLine2();
    if (addrLine2 != null && addrLine2.length() > 0)
    {
      buf.append(addrLine2 + "\n");
    }
    buf.append(address.getCity() + ", " + address.getState() + " "
      + address.getZip() + "\n");
    return buf.toString();
  }

  private String getPlainTextContact(String label, Contact contact)
  {
    StringBuffer buf = new StringBuffer();
    String firstName = contact.getFirstName();
    String lastName = contact.getLastName();
    String email = contact.getEmail();
    String phone = contact.getPhone();
    String fax = contact.getFax();
    buf.append((firstName != null ? firstName + " " : ""));
    buf.append((lastName != null ? lastName : "") + "\n");
    buf.insert(0,buf.length() > 0 ? "Contact " : "");
    if (email != null) buf.append("Email " + email + "\n");
    if (phone != null) buf.append("Phone " + phone + "\n");
    if (fax != null) buf.append("Fax " + fax + "\n");
    return buf.toString();
  }

  private String getPlainTextItem(PurchaseOrderItem item)
  {
    StringBuffer buf = new StringBuffer();
    PartType pt = item.getBucket().getPartType();
    buf.append("Qty " + item.getPartCount() + ", ");
    buf.append("Part " + pt.getModelCode());
    buf.append(" " + pt.getDescription());
    buf.append(" @ " + MesMath.toCurrency(item.getPartPrice()) + " per part\n");
    double itemTotal = item.getPartCount() * item.getPartPrice();
    EncryptionType et = item.getEncryptionType();
    if (et != null)
    {
      buf.append(" Encrypt PINpads for " + et.getEtName() + " (" + et.getEtCode()
        + ") @ " + MesMath.toCurrency(item.getPartFee()) + " per part\n");
      itemTotal += (item.getPartCount() * item.getPartFee());
    }
    buf.append(" Item total " + MesMath.toCurrency(itemTotal) + "\n");
    return buf.toString();
  }

  private String getPlainTextOrder(PurchaseOrder po)
  {
    StringBuffer buf = new StringBuffer();

    // title   
    buf.append("MES Purchase Order\n\n");

    // p.o. no. and date
    Date curDate = Calendar.getInstance().getTime();
    buf.append("P.O. No. " + po.getPoId() + " (Ver. " + po.getVersionNum() + ")\n");
    buf.append(DateTimeFormatter.getFormattedDate(curDate,"MMMM d, yyyy") + "\n\n");

    // p.o. business identification
    buf.append(getPlainTextAddress(po.getCustAddress()) + "\n");

    // p.o. author contact info
    buf.append(getPlainTextContact("Main Contact",po.getCustContact()) + "\n\n");

    // vendor info
    Vendor vendor = po.getVendor();
    buf.append("Vendor\n\n");

    // vendor address
    buf.append(getPlainTextAddress(po.getVendAddress()) + "\n");

    // vendor contact
    buf.append(getPlainTextContact("Vendor Contact",po.getVendContact()) + "\n");

    // vendor customer id
    String custId = vendor.getCustomerId();
    buf.append((custId != null ? "Customer ID " + custId + "\n" : "") + "\n\n");

    // shipping options
    buf.append("Shipping Information\n\n");

    // shipping method and delivery date
    buf.append(db.getShipper(po.getShipType().getShipperId()).getShipperName() 
      + ", " + po.getShipType().getShipTypeName() + "\n");
    if (po.getDeliveryUtilDate() != null)
    {
      buf.append("Delivery by ");
      buf.append(DateTimeFormatter.getFormattedDate(po.getDeliveryUtilDate(),"MMMM d, yyyy") + "\n\n");
    }

    // shipping address
    buf.append(getPlainTextAddress(po.getShipAddress()) + "\n");

    // shipping contact
    buf.append(getPlainTextContact("Shipping Contact",po.getShipContact()) + "\n");

    // order info
    buf.append("Order\n\n");

    double orderTotal = 0;
    for (Iterator i = po.getPurchaseOrderItems().iterator(); i.hasNext();)
    {
      PurchaseOrderItem item = (PurchaseOrderItem)i.next();
      buf.append(getPlainTextItem(item) + "\n");
      orderTotal += item.getPartCount() * (item.getPartPrice() + item.getPartFee());
    }
    buf.append("Order total " + MesMath.toCurrency(orderTotal) + "\n");

    return buf.toString();
  }

  private InputStream getVersionDocInputStream(PurchaseOrder po)
  {
    // enhance here to generate different types of docs (pdf, etc.)
    String text = getPlainTextOrder(po);
    return new ByteArrayInputStream(text.getBytes());
  }

  private void persistPoStatusChange(long poId, int newStatus)
  {
    PurchaseOrder po = getCurrentPo();
    int oldStatus = po.getPoStatus();

    // generate ready note with attached po info doc
    if (newStatus == PurchaseOrder.PO_STAT_READY)
    {
      // check to see if po is large enough to require finance approval
      PurchaseOrderOptions poOptions = db.getPurchaseOrderOptions();
      if (po.getItemTotal() >= poOptions.getLargeAmount() &&
          oldStatus != po.PO_STAT_PENDING)
      {
        // create po note 
        Note note = NoteAction.insertNote(db,user,name,poId,"Purchase order " 
          + poId + " pended due to large amount: " 
          + MesMath.toCurrency(po.getItemTotal()));
        newStatus = po.PO_STAT_PENDING;
      }
      // ok to move directly to ready status, generate a note with
      // version document attachment 
      else
      {
        // increment version num
        po.setVersionNum(po.getVersionNum() + 1);

        // create po note 
        Note note = NoteAction.insertNote(db,user,name,poId,"Purchase order " 
          + poId + " ready with version " + po.getVersionNum());

        // create attachment doc for note
        String docName = "po-v" + po.getVersionNum() + ".txt";
        String docDesc = null;
        InputStream docIn = getVersionDocInputStream(po);

        Document doc = DocAction
          .uploadNewDocFile(db,user,name,docName,docDesc,note.getNoteId(),docIn);
      }
    }

    String oldStatusStr = po.getPoStatusStr();
    po.setPoStatus(newStatus);
    persistPo(po);
    String actionMsg = "Purchase order " + poId + " status changed from " 
      + oldStatusStr + " to " + po.getPoStatusStr();
    logAction(actionMsg,poId);
    addFeedback(actionMsg);
  }

  private void doPoStatusChange(long poId, String confirmPrompt, 
    int[] requiredStatus, int newStatus, Link backLink, boolean confirmRequired)
  {
    if (!confirmRequired || isConfirmed())
    {
      persistPoStatusChange(poId,newStatus);
      doLinkRedirect(backLink);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(backLink,getNegFeedback(poId));
    }
    else
    {
      PurchaseOrder po = getPurchaseOrder(poId,false);
      boolean isLegal = false;
      for (int i = 0; i < requiredStatus.length && !isLegal; ++i)
      {
        if (po.getPoStatus() == requiredStatus[i])
        {
          isLegal = true;
        }
      }
      if (!isLegal)
      {
        redirectWithFeedback(backLink,getIllFeedback(poId));
      }
      else
      {
        confirm(confirmPrompt);
      }
    }
  }
  private void doPoStatusChange(long poId, String confirmPrompt, 
    int[] requiredStatus, int newStatus, Link backLink)
  {
    doPoStatusChange(poId,confirmPrompt,requiredStatus,newStatus,backLink,true);
  }


  private void doCancelPo()
  {
    long poId = getParmLong("poId");
    doPoStatusChange(
      poId,
      "Cancel purchase order " + poId + "?",
      new int [] {
        PurchaseOrder.PO_STAT_EDITING,
        PurchaseOrder.PO_STAT_READY,
        PurchaseOrder.PO_STAT_CONFIRMED },
      PurchaseOrder.PO_STAT_CANCELED,
      getBackLink());
  }

  private void doUncancelPo()
  {
    long poId = getParmLong("poId");
    doPoStatusChange(
      poId,
      "Reactivate purchase order " + poId + "?",
      new int [] {
        PurchaseOrder.PO_STAT_CANCELED } ,
      PurchaseOrder.PO_STAT_EDITING,
      getBackLink(),
      false);
  }

  private void doApprovePo()
  {
    long poId = getParmLong("poId");
    doPoStatusChange(
      poId,
      "Approve large purchase order " + poId + "?",
      new int [] {
        PurchaseOrder.PO_STAT_PENDING } ,
      PurchaseOrder.PO_STAT_READY,
      getBackLink());
  }

  private void doReadyPo()
  {
    long poId = getParmLong("poId");
    doPoStatusChange(
      poId,
      "Purchase order " + poId + " ready to place with vendor?",
      new int [] {
        PurchaseOrder.PO_STAT_EDITING } ,
      PurchaseOrder.PO_STAT_READY,
      getBackLink(),
      false);
  }

  private void doConfirmPo()
  {
    long poId = getParmLong("poId");
    doPoStatusChange(
      poId,
      "Purchase order " + poId + " confirmed by vendor?",
      new int [] {
        PurchaseOrder.PO_STAT_READY } ,
      PurchaseOrder.PO_STAT_CONFIRMED,
      getBackLink(),
      false);
  }

  private void doReceivePo()
  {
    long poId = getParmLong("poId");
    doPoStatusChange(
      poId,
      "Start receiving parts for purchase order " + poId + "?",
      new int [] {
        PurchaseOrder.PO_STAT_CONFIRMED } ,
      PurchaseOrder.PO_STAT_RECEIVING,
      getBackLink());
  }

  private void doCompletePo()
  {
    long poId = getParmLong("poId");
    doPoStatusChange(
      poId,
      "Purchase order " + poId + " completed?",
      new int [] {
        PurchaseOrder.PO_STAT_RECEIVING } ,
      PurchaseOrder.PO_STAT_COMPLETED,
      getBackLink(),
      false);
  }

  private void doPayPo()
  {
    long poId = getParmLong("poId");
    doPoStatusChange(
      poId,
      "Purchase order " + poId + " paid?",
      new int [] {
        PurchaseOrder.PO_STAT_COMPLETED } ,
      PurchaseOrder.PO_STAT_PAID,
      getBackLink(),
      false);
  }

  private void doDeletePo()
  {
    long delId = getParmLong("delId");
  
    if (isConfirmed())
    {
      getPurchaseOrder(delId,false);
      db.deletePurchaseOrder(delId,user);
      addFeedback("Purchase order " + delId + " deleted");
      logAction("Deleted P.O. " + delId,delId);
      doActionRedirect(Tmg.ACTION_LOOKUP_POS);
    }
    else if (isCanceled())
    {
      addFeedback("Purchase order not deleted");
      doActionRedirect(Tmg.ACTION_LOOKUP_POS);
    }
    else
    {
      confirm("Delete purchase order " + delId + "?");
    }
  }

  /**
   * Item actions
   */

  private PurchaseOrderItem getCurrentPoItem(boolean nullAllowed)
  {
    PurchaseOrderItem poItem = null;
    long poItemId = getParmLong("poItemId",-1L);
    if (poItemId != -1L)
    {
      poItem = db.getPoItem(poItemId);
      if (poItem != null)
      {
        setSessionAttr("poItem",poItem);
      }
    }
    else
    {
      poItem = (PurchaseOrderItem)getSessionAttr("poItem");
    }
    if (!nullAllowed && poItem == null)
    {
      throw new NullPointerException("Action " + name + " attempted to "
       + " get purchase order item " + poItemId + " but none was found");
    }
    return poItem;
  }
  private PurchaseOrderItem getCurrentPoItem()
  {
    return getCurrentPoItem(true);
  }

  private void reloadCurrentPoItem()
  {
    PurchaseOrderItem poItem = (PurchaseOrderItem)getSessionAttr("poItem",null);
    if (poItem != null)
    {
      setSessionAttr("poItem",db.getPoItem(poItem.getPoItemId()));
    }
  }

  public void persistPoItem(PurchaseOrderItem poItem)
  {
    db.persist(poItem,user);
    reloadCurrentPoItem();
  }

  private void doAddEditPoItem()
  {
    PurchaseOrder po = getCurrentPo(false);
    request.setAttribute("po",po);
    PoItemBean bean = new PoItemBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_PO_ITEM);

    if (bean.isAutoSubmitOk())
    {
      // if po does not have editting status, revert to editting
      // if changing anything but the received count...
      if (po.getPoStatus() != po.PO_STAT_EDITING && bean.poRevertNeeded())
      {
        persistPoStatusChange(po.getPoId(),po.PO_STAT_EDITING);
      }
      reloadCurrentPo();
      long poItemId = bean.getField(bean.FN_PO_ITEM_ID).asLong();
      logAction("Purchase order item " + poItemId + " updated");
      doLinkRedirect(getBackLink());
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_PO_ITEM : Tmg.VIEW_ADD_PO_ITEM);
    }
  }

  private void persistItemStatus(long poItemId,int newStatus)
  {
    PurchaseOrderItem poItem = db.getPoItem(poItemId);
    String oldStr = poItem.getItemStatusStr();
    poItem.setItemStatus(newStatus);
    String newStr = poItem.getItemStatusStr();
    persistPoItem(poItem);
    reloadCurrentPo();
    String description = "Purchase order item " + poItemId + " status changed from "
      + oldStr + " to " + newStr;
    logAction(description,poItemId);
  }

  private void doCancelPoItem()
  {
    long poItemId = getParmLong("poItemId");
    if (isConfirmed())
    {
      persistItemStatus(poItemId,PurchaseOrderItem.POI_STAT_CANCELED);
      redirectWithFeedback(getBackLink(),getFeedback(poItemId));
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),getNegFeedback(poItemId));
    }
    else
    {
      confirm("Cancel purchase order item " + poItemId + "?");
    }
  }

  private void doUncancelPoItem()
  {
    long poItemId = getParmLong("poItemId");
    persistItemStatus(poItemId,PurchaseOrderItem.POI_STAT_NEW);
    redirectWithFeedback(getBackLink(),getFeedback(poItemId));
  }

  /**
   * Part actions
   */

  private String getPoPartDescriptor(PurchaseOrderPart poPart)
  {
    String ptName = poPart.getBucket().getPartType().getPtName();
    String sn = poPart.getSerialNum();
    return ptName + (sn != null && sn.length() > 0 ? " Ser. No. " + sn  : "");
  }

  private void doReceivePoParts()
  {
    request.setAttribute("po",getCurrentPo(false));
    PurchaseOrderItem poItem = getCurrentPoItem(false);
    request.setAttribute("poItem",poItem);
    PoPartBean bean = new PoPartBean(this);
    if (bean.isAutoSubmitOk())
    {
      // update item status to receiving if needed
      if (poItem.getItemStatus() != poItem.POI_STAT_RECEIVING)
      {
        poItem.setItemStatus(poItem.POI_STAT_RECEIVING);
        persistItemStatus(poItem.getPoItemId(),poItem.POI_STAT_RECEIVING);
        reloadCurrentPoItem();
      }

      // if no change to number of receive slots, return to po edit screen
      if (!bean.recvCountChanged())
      {
        reloadCurrentPo();
        logAndRedirect(getBackLink(),poItem.getPoItemId());
      }
      // changed receive count, so return to receive parts screen with feedback
      else
      {
        reloadCurrentPoItem();
        redirectWithFeedback(name,"Receive count updated");
      }
    }
    else
    {
      doView(Tmg.VIEW_RECEIVE_PO_PARTS);
    }
  }

  private void doDeletePoPart()
  {
    long delId = getParmLong("delId");
    PurchaseOrderPart poPart = db.getPoPart(delId);
    if (poPart == null)
    {
      throw new RuntimeException("Purchase order part with id " + delId
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deletePoPart(delId,user);

      // if deleted p.o. part was only part for it's item, 
      // revert item's status to new
      if (db.getPoPartsForItem(poPart.getPoItemId()).isEmpty())
      {
        persistItemStatus(poPart.getPoItemId(),PurchaseOrderItem.POI_STAT_NEW);
      }

      // do a reload to reflect deleted part in po view
      reloadCurrentPo();

      logAndRedirect(getBackLink(),getPoPartDescriptor(poPart),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),getNegFeedback(delId));
    }
    else
    {
      confirm("Delete purchase order part " + getPoPartDescriptor(poPart) + "?");
    }
  }

  /**
   * Adds the po part as a new inventory part.
   */
  private int addPoPartsToInventory(List poParts)
  {
    int addCount = 0;
    for (Iterator i = poParts.iterator(); i.hasNext();)
    {
      PurchaseOrderPart poPart = (PurchaseOrderPart)i.next();

      if (!poPart.isAdded())
      {
        // build a new part representing this po part
        Part part = new Part();
        part.setPartId(db.getNewId());
        part.setBuckId(poPart.getBuckId());
        part.setPtId(poPart.getBucket().getPtId());
        part.setSerialNum(poPart.getSerialNum());
        part.setCount((int)poPart.getPartCount());
        part.setPcCode(poPart.getPcCode());
        part.setOrigPcCode(poPart.getPcCode());
        part.setCondCode(part.CC_GOOD);
        part.setStatusCode(part.SC_IN);
        part.setDispCode(part.DC_NOT_DEP);
        part.setLocCode(part.LC_WAREHOUSE);
        part.setEtId(poPart.getEtId() == 0 ? 157405L : poPart.getEtId());
        part.setPoItemId(poPart.getPoItemId());
        part.setPoPartId(poPart.getPoPartId());
    
        // add the part to inventory
        PartOpCode poc = db.getOpCode(Tmg.OP_PART_PO_ADD);
        Transaction t = new PoAddPartTransaction(db,user,part,poPart,poc);
        if (!t.run())
        {
          throw new RuntimeException("Failed to persist part");
        }

        // log this action for both the po part and the inventory part
        String description = "Part " + part.getPartId() + " added to inventory"
          + " for purchase order part " + getPoPartDescriptor(poPart);
        logAction(description,part.getPartId());
        logAction(description,poPart.getPoPartId());

        ++addCount;
      }
    }
    return addCount;
  }

  private void doInventoryPartAdd()
  {
    long poPartId = getParmLong("poPartId");
    PurchaseOrderPart poPart = db.getPoPart(poPartId);

    if (poPart == null)
    {
      throw new NullPointerException("Purchase order part " + poPartId 
        + " not found to add");
    }

    if (poPart.isAdded())
    {
      redirectWithFeedback(getBackLink(),"Part already in inventory");
    }

    if (isConfirmed())
    {
      List poParts = new ArrayList();
      poParts.add(poPart);
      addPoPartsToInventory(poParts);
      addFeedback("Purchase order part " + poPartId + " added to inventory");
      reloadCurrentPo();
      doLinkRedirect(getBackLink());
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Purchase order part "
        + poPartId + " not added to inventory");
    }
    else
    {
      confirm("Permanently add purchase order part " + getPoPartDescriptor(poPart)
        + " to inventory?");
    }
  }

  private void doInventoryItemAdd()
  {
    long poItemId = getParmLong("poItemId");
    PurchaseOrderItem poItem = db.getPoItem(poItemId);

    if (poItem == null)
    {
      throw new NullPointerException("Purchase order item " + poItemId
        + " not found to add");
    }

    if (isConfirmed())
    {
      addPoPartsToInventory(poItem.getPurchaseOrderParts());
      addFeedback("All parts in P.O. item " + poItemId + " added to inventory");
      reloadCurrentPo();
      doLinkRedirect(getBackLink());
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"No parts added to inventory from"
        + " P.O. item " + poItemId);
    }
    else
    {
      if (poItem.getAddedCount() >= poItem.getReceivedCount())
      {
        addFeedback("No parts need to be added for P.O. item " + poItemId);
        doLinkRedirect(getBackLink());
      }
      else
      {
        confirm("Permanently add all purchase order parts in P.O. item " 
          + poItemId + "?");
      }
    }
  }

  private void doInventoryAllAdd()
  {
    PurchaseOrder po = getCurrentPo(false);

    if (isConfirmed())
    {
      List poParts = new ArrayList();
      for (Iterator i = po.getPurchaseOrderItems().iterator(); i.hasNext();)
      {
        PurchaseOrderItem poItem = (PurchaseOrderItem)i.next();
        poParts.addAll(poItem.getPurchaseOrderParts());
      }
      addPoPartsToInventory(poParts);
      addFeedback("All parts in P.O. " + po.getPoId() + " added to inventory");
      reloadCurrentPo();
      doLinkRedirect(getBackLink());
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"No parts added to inventory from"
        + " P.O. " + po.getPoId());
    }
    else
    {
      if (po.getAddedCount() >= po.getReceivedCount())
      {
        addFeedback("No parts need to be added for P.O. " + po.getPoId());
        doLinkRedirect(getBackLink());
      }
      else
      {
        confirm("Permanently add all purchase order parts in P.O. " 
          + po.getPoId() + "?");
      }
    }
  }

  private void doAddEditPoInvoice()
  {
    PoInvoiceBean bean = new PoInvoiceBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_PO_INVOICE);
    if (bean.isAutoSubmitOk())
    {
      logAndRedirect(getBackLink(),"P.O. Invoice");
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_PO_INVOICE : Tmg.VIEW_ADD_PO_INVOICE);
    }
  }

  private void doShowPoLogs()
  {
    long poId = getParmLong("poId");
    request.setAttribute("logs",db.getPoActionLogs(poId));
    doView(Tmg.VIEW_SHOW_LOGS,"Purchase Order No. " + poId + " Action Log");
  }

  private void doEditPoOptions()
  {
    PoOptionsBean bean = new PoOptionsBean(this);
    if (bean.isAutoSubmitOk())
    {
      logAndRedirect(handler.getBackLink(),"Purchase Order Options updated");
    }
    else
    {
      PurchaseOrderOptions poOptions = db.getPurchaseOrderOptions();
      setContactWhoId(poOptions.getContactId());
      setAddressWhoId(poOptions.getAddressId());
      doView(Tmg.VIEW_EDIT_PO_OPTIONS);
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_LOOKUP_POS))
    {
      doLookupPos();
    }
    else if (name.equals(Tmg.ACTION_ADD_PO))
    {
      doAddPo();
    }
    else if (name.equals(Tmg.ACTION_EDIT_PO) ||
             name.equals(Tmg.ACTION_EDIT_NEW_PO))
    {
      doEditPo();
    }
    else if (name.equals(Tmg.ACTION_VIEW_PO) ||
             name.equals(Tmg.ACTION_VIEW_NEW_PO))
    {
      doViewPo();
    }
    else if (name.equals(Tmg.ACTION_CANCEL_PO))
    {
      doCancelPo();
    }
    else if (name.equals(Tmg.ACTION_UNCANCEL_PO))
    {
      doUncancelPo();
    }
    else if (name.equals(Tmg.ACTION_DELETE_PO))
    {
      doDeletePo();
    }
    else if (name.equals(Tmg.ACTION_READY_PO))
    {
      doReadyPo();
    }
    else if (name.equals(Tmg.ACTION_APPROVE_PO))
    {
      doApprovePo();
    }
    else if (name.equals(Tmg.ACTION_CONFIRM_PO))
    {
      doConfirmPo();
    }
    else if (name.equals(Tmg.ACTION_RECEIVE_PO))
    {
      doReceivePo();
    }
    else if (name.equals(Tmg.ACTION_COMPLETE_PO))
    {
      doCompletePo();
    }
    else if (name.equals(Tmg.ACTION_PAY_PO))
    {
      doPayPo();
    }
    else if (name.equals(Tmg.ACTION_ADD_PO_ITEM) ||
             name.equals(Tmg.ACTION_EDIT_PO_ITEM))
    {
      doAddEditPoItem();
    }
    else if (name.equals(Tmg.ACTION_CANCEL_PO_ITEM))
    {
      doCancelPoItem();
    }
    else if (name.equals(Tmg.ACTION_UNCANCEL_PO_ITEM))
    {
      doUncancelPoItem();
    }
    else if (name.equals(Tmg.ACTION_RECEIVE_PO_PARTS))
    {
      doReceivePoParts();
    }
    else if (name.equals(Tmg.ACTION_DELETE_PO_PART))
    {
      doDeletePoPart();
    }
    else if (name.equals(Tmg.ACTION_INV_PO_PART))
    {
      doInventoryPartAdd();
    }
    else if (name.equals(Tmg.ACTION_INV_PO_ITEM))
    {
      doInventoryItemAdd();
    }
    else if (name.equals(Tmg.ACTION_INV_PO_ALL))
    {
      doInventoryAllAdd();
    }
    else if (name.equals(Tmg.ACTION_ADD_PO_INVOICE) ||
             name.equals(Tmg.ACTION_EDIT_PO_INVOICE))
    {
      doAddEditPoInvoice();
    }
    else if (name.equals(Tmg.ACTION_SHOW_PO_LOGS))
    {
      doShowPoLogs();
    }
    else if (name.equals(Tmg.ACTION_EDIT_PO_OPTIONS))
    {
      doEditPoOptions();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
