package com.mes.tmg.pos;

import org.apache.log4j.Logger;
import com.mes.tmg.Part;
import com.mes.tmg.PartOpCode;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PartPrice;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.PurchaseOrderItem;
import com.mes.tmg.PurchaseOrderPart;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public class PoAddPartTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(PoAddPartTransaction.class);

  private PurchaseOrderPart poPart;

  public PoAddPartTransaction(TmgDb db, UserBean user, Part part, 
    PurchaseOrderPart poPart, PartOpCode poc)
  {
    super(db,user,part,poc);
    this.poPart = poPart;
  }

  private void logPartPrice(PurchaseOrderPart poPart, Part part)
  {
    PurchaseOrderItem pi = db.getPoItem(poPart.getPoItemId());
    PartPrice pp = new PartPrice();
    pp.setPpId(db.getNewId());
    pp.setPartId(part.getPartId());
    pp.setPartPrice(pi.getPartPrice());
    db.persist(pp,user);
  }

  public int executeOperation(PartOperation op)
  {
    db.addPart(part,op,user); 

    logPartPrice(poPart,part);

    poPart.setPartId(part.getPartId());
    db.persist(poPart,user);

    return COMMIT;
  }
}
