package com.mes.tmg.pos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;
import com.mes.persist.InFilter;
import com.mes.persist.Selection;
import com.mes.tmg.EncryptionType;
import com.mes.tmg.PartState;
import com.mes.tmg.PartType;
import com.mes.tmg.PurchaseOrder;
import com.mes.tmg.PurchaseOrderItem;
import com.mes.tmg.PurchaseOrderPart;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.PartClassTable;
import com.mes.tools.DropDownTable;

/**
 * Used to add or edit a new purchase order part.
 */

public class PoPartBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PoPartBean.class);
  
  public static final String FN_PART_FG       = "partSlot_";
  public static final String FN_PO_PART_ID    = "poPartId_";
  public static final String FN_SERIAL_NUM    = "serialNum_";
  public static final String FN_PART_COUNT    = "partCount_";
  public static final String FN_PC_CODE       = "pcCode_";
  public static final String FN_ET_ID         = "etId_";
  public static final String FN_CLEAR_UNUSED  = "clearUnused";
  public static final String FN_ADD_SLOTS     = "addSlots";
  public static final String FN_SUBMIT_BTN    = "submit";
  
  public PoPartBean(TmgAction action)
  {
    super(action);
  }

  private PurchaseOrder       po;
  private PurchaseOrderItem   poItem;

  private CheckField          fClearUnused;
  private NumberField         fAddSlots;
  private int                 slotCount;
  private boolean             recvCountChangedFlag;

  protected class EncryptTypeTable extends DropDownTable
  {
    public EncryptTypeTable()
    {
      addElement("0","None");
      List etList = db.getEncryptTypes();
      for (Iterator i = etList.iterator(); i.hasNext(); )
      {
        EncryptionType et = (EncryptionType)i.next();
        addElement(String.valueOf(et.getEtId()),et.getEtName());
      }
    }
  }

  public class SlotValidation implements Validation
  {
    private String errorText = null;
    private Field condField;
    private Field etField;

    public SlotValidation(Field condField, Field etField)
    {
      this.condField = condField;
      this.etField = etField;
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String data)
    {
      if (data != null && data.length() > 0)
      {
        boolean etRequired = !(etField instanceof HiddenField);
        boolean dataMissing = condField.isBlank() || 
                             (etRequired && etField.isBlank());
        if (dataMissing)
        {
          errorText = etRequired ?
                      "Select condition and encryption type" :
                      "Select condition";
          return false;
        }
      }
      return true;
    }
  }

  public class SnValidation implements Validation
  {
    boolean validFlag = true;

    public boolean validate(String data)
    {
      return validFlag;
    }

    public String getErrorText()
    {
      return "Duplicate serial number";
    }

    public void setValidFlag(boolean validFlag)
    {
      this.validFlag = validFlag;
    }
  }

  public class DuplicateCheckValidation implements Validation
  {
    List snFields = null;

    public DuplicateCheckValidation(List snFields)
    {
      this.snFields = snFields;
    }

    public String getErrorText()
    {
      return "Duplicate serial numbers found";
    }

    public boolean validate(String data)
    {
      // create a sn hash, look for any dups within the sn's themselves
      Map snMap = new HashMap();
      for (Iterator i = snFields.iterator(); i.hasNext();)
      {
        Field snField = (Field)i.next();
        if (!snField.isBlank())
        {
          String sn = snField.getData();
          Field dupSnField = (Field)snMap.get(sn);
          if (dupSnField != null)
          {
            ((SnValidation)snField.getValidation("snval")).setValidFlag(false);
            ((SnValidation)dupSnField.getValidation("snval")).setValidFlag(false);
          }
          else
          {
            snMap.put(sn,snField);
          }
        }
      }

      // look for dups in existing parts in db
      Selection sel = db.getSelection(PartState.class);
      sel.addFilter(new InFilter("serial_num",snMap.keySet()));
      for (Iterator i = db.selectAll(sel).iterator(); i.hasNext();)
      {
        PartState ps = (PartState)i.next();
        Field snField = (Field)snMap.get(ps.getSerialNum());
        ((SnValidation)snField.getValidation("snval")).setValidFlag(false);
      }

      return true;
    }

    public void addSnField(Field snField)
    {
      snFields.add(snField);
    }
  }

  DropDownTable classTable = new PartClassTable();
  DropDownTable encryptTable = new EncryptTypeTable();

  private void createPartFields(int partSlot, List snFields)
  {
    try
    {
      PartType partType = getPoItem().getBucket().getPartType();
      Field idField = new HiddenField(FN_PO_PART_ID + partSlot);
      Field snField = null;
      Field qtyField = null;
      if (partType.hasSn())
      {
        snField = new Field(FN_SERIAL_NUM + partSlot,"Serial No.",32,32,true);
        qtyField = new HiddenField(FN_PART_COUNT + partSlot,"1");
      }
      else
      {
        snField = new HiddenField(FN_SERIAL_NUM + partSlot);
        qtyField  = new NumberField(FN_PART_COUNT + partSlot,"Quantity",6,6,true,0);
      }
      Field pcField   = new DropDownField(FN_PC_CODE + partSlot,classTable,true);
      Field etField   = null;
      if (partType.hasEncrypt() && getPo().encryptSupported())
      {
        etField = new DropDownField(FN_ET_ID + partSlot,encryptTable,true);
      }
      else
      {
        etField = new HiddenField(FN_ET_ID + partSlot);
      }
      fields.add(idField);
      fields.add(snField);
      fields.add(qtyField);
      fields.add(pcField);
      fields.add(etField);
      if (partType.hasSn())
      {
        snField.addValidation(new SlotValidation(pcField,etField));
        snField.addValidation(new SnValidation(),"snval");
        snFields.add(snField);
      }
      else
      {
        qtyField.addValidation(new SlotValidation(pcField,etField));
      }

      // init to po item order types by default
      etField.setData(String.valueOf(poItem.getEtId()));
      pcField.setData(poItem.getPcCode());
    }
    catch (Exception e)
    {
      log.error("Error creating part field group for part slot " + partSlot);
    }
  }

  private int determineSlotCount(HttpServletRequest request)
  {
    PurchaseOrderItem poItem = getPoItem();
    int slotCount = (int)poItem.getRecvCount();
    int actualCount = poItem.getPurchaseOrderParts().size();
    slotCount = slotCount < actualCount ? actualCount : slotCount;

    // manually set the slot changing fields from request data
    fAddSlots.setData(request.getParameter(FN_ADD_SLOTS));
    fClearUnused.setData(request.getParameter(FN_CLEAR_UNUSED));

    // adjust slot count according to field content
    if (fAddSlots.isValid() && fAddSlots.asInt() > 0)
    {
      slotCount += fAddSlots.asInt();
    }
    else if (fClearUnused.isChecked())
    {
      slotCount = actualCount;
    }

    return slotCount;
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      classTable = new PartClassTable();
      encryptTable = new EncryptTypeTable();
      fClearUnused = new CheckField(FN_CLEAR_UNUSED,"Remove unused spaces","y",false);
      fAddSlots = new NumberField(FN_ADD_SLOTS,"Add Spaces",3,2,true,0,1,999);
      fields.add(fClearUnused);
      fields.add(fAddSlots);
      slotCount = determineSlotCount(request);
      List snFields = new ArrayList();
      for (int partSlot = 1; partSlot <= slotCount; ++partSlot)
      {
        createPartFields(partSlot,snFields);
      }
      fields.addValidation(new DuplicateCheckValidation(snFields));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setFixImage("/images/arrow1_left.gif",10,10);
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
      e.printStackTrace();
    }
  }

  /**
   * Load persistent data into fields.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    try
    {
      List parts = db.getPoPartsForItem(getPoItem());
      for (int partSlot = 1; partSlot <= parts.size(); ++partSlot)
      {
        PurchaseOrderPart part = (PurchaseOrderPart)parts.get(partSlot - 1);
        setData(FN_PO_PART_ID + partSlot,String.valueOf(part.getPoPartId()));
        setData(FN_SERIAL_NUM + partSlot,part.getSerialNum());
        setData(FN_PART_COUNT + partSlot,String.valueOf(part.getPartCount()));
        setData(FN_PC_CODE + partSlot,part.getPcCode());
        setData(FN_ET_ID + partSlot,String.valueOf(part.getEtId()));
      }
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading po parts: " + e);
      e.printStackTrace();
    }
    return loadOk;
  }
  
  /**
   * Persists purchase order.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      PurchaseOrderItem poItem = getPoItem();
      for (int partSlot = 1; partSlot <= slotCount; ++partSlot)
      {
        Field snField = getField(FN_SERIAL_NUM + partSlot);
        Field qtyField = getField(FN_PART_COUNT + partSlot);
        if ((hasSn() && !snField.isBlank()) || (!hasSn() && !qtyField.isBlank()))
        {
          PurchaseOrderPart part = null;
          long poPartId = getField(FN_PO_PART_ID + partSlot).asLong();
          part = poItem.getPoPartWithId(poPartId);
          if (part == null)
          {
            poPartId = db.getNewId();
            setData(FN_PO_PART_ID + partSlot,String.valueOf(poPartId));
            part = new PurchaseOrderPart();
            part.setPoPartId(poPartId);
            part.setPoId(poItem.getPoId());
            part.setPoItemId(poItem.getPoItemId());
            part.setBuckId(poItem.getBuckId());
            part.setCreateDate(Calendar.getInstance().getTime());
            part.setUserName(user.getLoginName());
          }

          String serialNum = snField.getData();
          String pcCode = getData(FN_PC_CODE + partSlot);
          int partCount = qtyField.asInteger();
          long etId = getField(FN_ET_ID + partSlot).asLong();
          if ((hasSn() && !serialNum.equals(part.getSerialNum())) ||
              (!hasSn() && partCount != part.getPartCount()) ||
              !pcCode.equals(part.getPcCode()) ||
              etId != part.getEtId() ||
              partCount != part.getPartCount())
          {
            part.setSerialNum(serialNum);
            part.setPcCode(pcCode);
            part.setEtId(etId);
            part.setPartCount(partCount);
            db.persist(part,user);
          }
        }

        // persist changes to the receive count in poItem
        if (fAddSlots.asInt() > 0 || fClearUnused.isChecked())
        {
          fAddSlots.setData("");
          fClearUnused.reset();
          recvCountChangedFlag = true;
          poItem.setRecvCount(slotCount);
          db.persist(poItem,user);
        }
      }

      submitOk = true;
    }
    catch (Exception e)   
    {
      log.error("Error submitting po parts: " + e);
      e.printStackTrace();
    }

    return submitOk;
  }

  public boolean encryptAllowed()
  {
    return poItem.getBucket().getPartType().hasEncrypt() && po.encryptSupported();
  }

  public boolean hasSn()
  {
    return poItem.getBucket().getPartType().hasSn();
  }

  public boolean recvCountChanged()
  {
    return recvCountChangedFlag;
  }

  public PurchaseOrder getPo()
  {
    // hack: po is not available if create is triggered by autoset in 
    // base class constructor, so it must be manually fetched from the 
    // request here
    if (po == null)
    {
      po = (PurchaseOrder)request.getAttribute("po");
    }
    return po;
  }
  
  public PurchaseOrderItem getPoItem()
  {
    // hack: po item is not available if create is triggered by autoset in 
    // base class constructor, so it must be manually fetched from the 
    // request here
    if (poItem == null)
    {
      poItem = (PurchaseOrderItem)request.getAttribute("poItem");
    }
    return poItem;
  }

  public int getSlotCount()
  {
    return slotCount;
  }
}