package com.mes.tmg.pos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.HiddenField;
import com.mes.tmg.Address;
import com.mes.tmg.Contact;
import com.mes.tmg.PurchaseOrder;
import com.mes.tmg.PurchaseOrderOptions;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.Vendor;
import com.mes.tools.DropDownTable;

/**
 * Used to add a new purchase order.
 */

public class PoEditBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PoEditBean.class);
  
  public static final String FN_PO_ID         = "poId";
  public static final String FN_CUST_CONT_ID  = "custContId";
  public static final String FN_VEND_CONT_ID  = "vendContId";
  public static final String FN_SHIP_CONT_ID  = "shipContId";
  public static final String FN_CUST_ADDR_ID  = "custAddrId";
  public static final String FN_VEND_ADDR_ID  = "vendAddrId";
  public static final String FN_SHIP_ADDR_ID  = "shipAddrId";
  public static final String FN_SHIP_TYPE_ID  = "shipTypeId";
  public static final String FN_DELIVERY_DATE = "deliveryDate";
  public static final String FN_SUBMIT_BTN    = "submit";
  
  private PurchaseOrder po;

  public PoEditBean(TmgAction action)
  {
    super(action);
  }
  
  protected class ContactTable extends DropDownTable
  {
    public ContactTable(List contacts, boolean noneAllowed)
    {
      addElement("",noneAllowed ? "--" : "select one");
      for (Iterator i = contacts.iterator(); i.hasNext(); )
      {
        Contact contact = (Contact)i.next();
        addElement(String.valueOf(contact.getContId()),
          contact.getLabel());
      }
    }
    public ContactTable(long whoId, boolean noneAllowed)
    {
      this(db.getContactsForWho(whoId),noneAllowed);
    }
  }
      
  protected class AddressTable extends DropDownTable
  {
    public AddressTable(List addresses, boolean noneAllowed)
    {
      addElement("",noneAllowed ? "--" : "select one");
      for (Iterator i = addresses.iterator(); i.hasNext(); )
      {
        Address address = (Address)i.next();
        addElement(String.valueOf(address.getAddrId()),
          address.getLabel());
      }
    }
    public AddressTable(long whoId, boolean noneAllowed)
    {
      this(db.getAddressesForWho(whoId),noneAllowed);
    }
  }

  protected class ShipTypeTable extends DropDownTable
  {
    public ShipTypeTable(Vendor vendor)
    {
      addElement("","select one");

      String qs 
        = " select  st.ship_type_id,                              "
        + "         s.shipper_name || ' - ' || st.ship_type_name  "
        + " from    tmg_shippers s,                               "
        + "         tmg_ship_types st,                            "
        + "         tmg_vendor_ship_types vst                     "
        + " where   vst.vendor_id = ?                             "
        + "         and vst.ship_type_id = st.ship_type_id        "
        + "         and st.shipper_id = s.shipper_id              "
        + " order by s.shipper_name                               ";

      PreparedStatement ps = null;
      ResultSet rs = null;

      try
      {
        Connection con = db.getConnection();
        ps = con.prepareStatement(qs);
        ps.setLong(1,vendor.getVendorId());
        rs = ps.executeQuery();
        while (rs.next())
        {
          addElement(rs);
        }

      }
      catch (Exception e)
      {
        log.error("Error loading ship type table: " + e);
        e.printStackTrace();
      }
      finally
      {
        try { rs.close(); } catch (Exception e) { }
        try { ps.close(); } catch (Exception e) { }
        db.doDisconnect();
      }
    }
  }
      
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_PO_ID));
      fields.addAlias(FN_PO_ID,"editId");

      // customer and shipping address/contact lists based on global
      // purchase order options...
      PurchaseOrderOptions poOptions = db.getPurchaseOrderOptions();
      fields.add(new DropDownField(FN_CUST_CONT_ID,new ContactTable(poOptions.getContactId(),false),false));
      fields.add(new DropDownField(FN_CUST_ADDR_ID,new AddressTable(poOptions.getAddressId(),false),false));
      fields.add(new DropDownField(FN_SHIP_CONT_ID,new ContactTable(poOptions.getContactId(),false),false));
      fields.add(new DropDownField(FN_SHIP_ADDR_ID,new AddressTable(poOptions.getAddressId(),false),false));

      // vendor needed from po for vendor contacts/addresses...
      PurchaseOrder po = db.getPurchaseOrder(action.getParmLong("poId"));
      Vendor vendor = po.getVendor();
      fields.add(new DropDownField(FN_VEND_CONT_ID,new ContactTable(vendor.getContacts(),false),false));
      fields.add(new DropDownField(FN_VEND_ADDR_ID,new AddressTable(vendor.getAddresses(),false),false));
      
      fields.add(new DropDownField(FN_SHIP_TYPE_ID,new ShipTypeTable(vendor),false));

      fields.add(new DateStringField(FN_DELIVERY_DATE,"Delivery Date",true,false));

      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
      e.printStackTrace();
    }
  }

  public PurchaseOrder getPo()
  {
    return po;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long poId = getField(FN_PO_ID).asLong();
      
      if (poId != 0L)
      {
        po = db.getPurchaseOrder(poId);
        setData(FN_CUST_CONT_ID,String.valueOf(po.getCustContId()));
        setData(FN_CUST_ADDR_ID,String.valueOf(po.getCustAddrId()));
        setData(FN_VEND_CONT_ID,String.valueOf(po.getVendContId()));
        setData(FN_VEND_ADDR_ID,String.valueOf(po.getVendAddrId()));
        setData(FN_SHIP_CONT_ID,String.valueOf(po.getShipContId()));
        setData(FN_SHIP_ADDR_ID,String.valueOf(po.getShipAddrId()));
        setData(FN_SHIP_TYPE_ID,String.valueOf(po.getShipTypeId()));
        ((DateStringField)getField(FN_DELIVERY_DATE))
          .setUtilDate(po.getDeliveryUtilDate());
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading po: " + e);
    }
    
    return loadOk;
  }
  
  /**
   * Persists purchase order.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      po = db.getPurchaseOrder(getField(FN_PO_ID).asLong());
      po.setCustContId(getField(FN_CUST_CONT_ID).asLong());
      po.setCustAddrId(getField(FN_CUST_ADDR_ID).asLong());
      po.setVendContId(getField(FN_VEND_CONT_ID).asLong());
      po.setVendAddrId(getField(FN_VEND_ADDR_ID).asLong());
      po.setShipContId(getField(FN_SHIP_CONT_ID).asLong());
      po.setShipAddrId(getField(FN_SHIP_ADDR_ID).asLong());
      po.setShipTypeId(getField(FN_SHIP_TYPE_ID).asLong());
      po.setDeliveryUtilDate(
        ((DateStringField)getField(FN_DELIVERY_DATE)).getUtilDate());
      ((PoAction)action).persistPo(po);
      submitOk = true;
    }
    catch (Exception e)   
    {
      e.printStackTrace();
      log.error("Error submitting po: " + e);
    }

    return submitOk;
  }
}