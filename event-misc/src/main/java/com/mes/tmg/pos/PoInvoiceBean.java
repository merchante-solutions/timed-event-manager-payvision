package com.mes.tmg.pos;

import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.TextareaField;
import com.mes.tmg.PurchaseOrderInvoice;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class PoInvoiceBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PoInvoiceBean.class);
  
  public static final String FN_PO_INV_ID       = "poInvId";
  public static final String FN_PO_ID           = "poId";
  public static final String FN_ORDER_NUM       = "orderNum";
  public static final String FN_NOTES           = "notes";
  public static final String FN_INVOICE_NUM     = "invoiceNum";
  public static final String FN_INVOICE_DATE    = "invoiceDate";
  public static final String FN_SHIP_COMPANY    = "shipCompany";
  public static final String FN_SHIP_CHARGE     = "shipCharge";
  public static final String FN_TRACKING_NUM    = "trackingNum";

  public static final String FN_SUBMIT_BTN      = "submit";

  
  public PoInvoiceBean(TmgAction action)
  {
    super(action);
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_PO_INV_ID));
      fields.addAlias(FN_PO_INV_ID,"editId");
      fields.add(new HiddenField(FN_PO_ID));
      fields.add(new Field(FN_ORDER_NUM,"Order No.",64,13,true));
      fields.add(new TextareaField(FN_NOTES,"Notes",300,4,80,true));

      fields.add(new Field(FN_INVOICE_NUM,"Invoice No.",25,13,false));
      fields.add(new DateStringField(FN_INVOICE_DATE,"Invoice Date",true,false));
      fields.add(new Field(FN_SHIP_COMPANY,"Shipping Company",50,13,true));
      fields.add(new NumberField(FN_SHIP_CHARGE,"Shipping Charge",9,13,true,2));
      fields.add(new Field(FN_TRACKING_NUM,"Tracking No.",150,13,true));

      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      db.doConnect();

      long poInvId = getField(FN_PO_INV_ID).asLong();
      
      if (poInvId != 0L)
      {
        PurchaseOrderInvoice invoice = db.getPoInvoice(poInvId);
        setData(FN_PO_ID,String.valueOf(invoice.getPoId()));
        setData(FN_ORDER_NUM,invoice.getOrderNum());
        setData(FN_NOTES,invoice.getNotes());
        setData(FN_INVOICE_NUM,invoice.getInvoiceNum());
        ((DateStringField)getField(FN_INVOICE_DATE))
          .setUtilDate(invoice.getInvoiceUtilDate());
        setData(FN_SHIP_COMPANY,invoice.getShipCompany());
        setData(FN_SHIP_CHARGE,String.valueOf(invoice.getShipCharge()));
        setData(FN_TRACKING_NUM,invoice.getTrackingNum());
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading invoice: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }
    
    return loadOk;
  }
  
  /**
   * Persists invoice.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      db.doConnect();

      long poId = getField(FN_PO_ID).asLong();
      if (poId == 0L)
      {
        throw new Exception("Attempt to submit invoice without poId set");
      }

      long poInvId = getField(FN_PO_INV_ID).asLong();
      PurchaseOrderInvoice invoice = null;
      if (poInvId == 0L)
      {
        poInvId = db.getNewId();
        invoice = new PurchaseOrderInvoice();
        invoice.setPoInvId(poInvId);
        invoice.setPoId(poId);
        invoice.setCreateDate(Calendar.getInstance().getTime());
        invoice.setUserName(user.getLoginName());

        setData(FN_PO_INV_ID,String.valueOf(poInvId));
      }
      else
      {
        invoice = db.getPoInvoice(poInvId);
      }

      invoice.setOrderNum(getData(FN_ORDER_NUM));
      invoice.setNotes(getData(FN_NOTES));      
      invoice.setInvoiceNum(getData(FN_INVOICE_NUM));
      invoice.setInvoiceUtilDate(((DateStringField)getField(FN_INVOICE_DATE)).getUtilDate());
      invoice.setShipCompany(getData(FN_SHIP_COMPANY));
      invoice.setShipCharge(getField(FN_SHIP_CHARGE).asDouble());
      invoice.setTrackingNum(getData(FN_TRACKING_NUM));

      db.persist(invoice,user);

      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting invoice: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }

    return submitOk;
  }
}