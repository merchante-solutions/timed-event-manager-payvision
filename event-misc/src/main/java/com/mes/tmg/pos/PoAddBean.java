package com.mes.tmg.pos;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.HiddenField;
import com.mes.tmg.Inventory;
import com.mes.tmg.PurchaseOrder;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.Vendor;
import com.mes.tools.DropDownTable;

/**
 * Used to add a new purchase order.
 */

public class PoAddBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PoAddBean.class);
  
  public static final String FN_PO_ID       = "poId";
  public static final String FN_INV_ID      = "invId";
  public static final String FN_VENDOR_ID   = "vendorId";
  public static final String FN_SUBMIT_BTN  = "submit";
  
  private PurchaseOrder po;

  public PoAddBean(TmgAction action)
  {
    super(action);
  }
  
  protected class InventoryTable extends DropDownTable
  {
    public InventoryTable()
    {
      addElement("","select one");
      List invs = db.getInventories();
      for (Iterator i = invs.iterator(); i.hasNext(); )
      {
        Inventory inv = (Inventory)i.next();
        addElement(String.valueOf(inv.getInvId()),
          inv.getInvName());
      }
    }
  }
      
  protected class VendorTable extends DropDownTable
  {
    public VendorTable()
    {
      addElement("","select one");
      List vendors = db.getVendors();
      for (Iterator i = vendors.iterator(); i.hasNext(); )
      {
        Vendor vendor = (Vendor)i.next();
        addElement(String.valueOf(vendor.getVendorId()),
          vendor.getVendorName());
      }
    }
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_PO_ID));
      fields.addAlias(FN_PO_ID,"editId");
      fields.add(new DropDownField(FN_VENDOR_ID,new VendorTable(),false));
      fields.add(new DropDownField(FN_INV_ID,new InventoryTable(),false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public PurchaseOrder getPo()
  {
    return po;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long poId = getField(FN_PO_ID).asLong();
      
      if (poId != 0L)
      {
        po = db.getPurchaseOrder(poId);
        setData(FN_INV_ID,String.valueOf(po.getInvId()));
        setData(FN_VENDOR_ID,String.valueOf(po.getVendorId()));
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading po: " + e);
    }
    
    return loadOk;
  }
  
  /**
   * Persists purchase order.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long poId = getField(FN_PO_ID).asLong();
      
      if (poId == 0L)
      {
        poId = db.getNewId();
        po = new PurchaseOrder();
        po.setPoId(poId);
        po.setPoStatus(po.PO_STAT_EDITING);
        po.setCreateDate(Calendar.getInstance().getTime());
        po.setVersionNum(0);
        po.setUserName(user.getLoginName());
        setData(FN_PO_ID,String.valueOf(poId));
      }
      else
      {
        po = db.getPurchaseOrder(poId);
      }

      po.setInvId(getField(FN_INV_ID).asLong());
      po.setVendorId(getField(FN_VENDOR_ID).asLong());
      
      ((PoAction)action).persistPo(po);
      
      submitOk = true;
    }
    catch (Exception e)   
    {
      e.printStackTrace();
      log.error("Error submitting po: " + e);
    }

    return submitOk;
  }
}