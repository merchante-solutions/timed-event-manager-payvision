package com.mes.tmg.pos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.Mvc;
import com.mes.persist.DateRangeFilter;
import com.mes.persist.InFilter;
import com.mes.persist.Selection;
import com.mes.tmg.Inventory;
import com.mes.tmg.PurchaseOrder;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.Vendor;
import com.mes.tools.DropDownTable;

public class PoLookupBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PoLookupBean.class);
  
  public static final String FN_FROM_DATE   = "fromDate";
  public static final String FN_TO_DATE     = "toDate";
  public static final String FN_VENDOR_ID   = "vendorId";
  public static final String FN_INV_ID      = "invId";
  public static final String FN_STATUS      = "status";
  public static final String FN_OTHER       = "other";
  
  public static final String FN_SUBMIT_BTN  = "submit";

  private List pos;

  /**
   * Drop down tables
   */
  protected class InventoryTable extends DropDownTable
  {
    public InventoryTable()
    {
      addElement("","-- any --");
      List invs = db.getInventories();
      for (Iterator i = invs.iterator(); i.hasNext(); )
      {
        Inventory inv = (Inventory)i.next();
        addElement(String.valueOf(inv.getInvId()),inv.getInvName());
      }
    }
  }

  protected class VendorTable extends DropDownTable
  {
    public VendorTable()
    {
      addElement("","-- any --");
      List vendors = db.getVendors();
      for (Iterator i = vendors.iterator(); i.hasNext(); )
      {
        Vendor vendor = (Vendor)i.next();
        addElement(String.valueOf(vendor.getVendorId()),vendor.getVendorName());
      }
    }
  }

  protected class StatusTable extends DropDownTable
  {
    public StatusTable()
    {
      addElement("","-- any --");
      addElements(PurchaseOrder.statusItems);
      addElement("!" + PurchaseOrder.PO_STAT_PAID,"Unpaid");
    }
  }

  public static final String HDR_PO_NUM   = "P.O.";
  public static final String HDR_DATE     = "Date";
  public static final String HDR_AUTHOR   = "Author";
  public static final String HDR_INV      = "Inventory";
  public static final String HDR_VND      = "Vendor";
  public static final String HDR_STATUS   = "Status";
  public static final String HDR_TOTAL    = "Total";
  public static final String HDR_VERSION  = "Version";

  public PoLookupBean(TmgAction action)
  {
    super(action);
    putOrderHeader(HDR_PO_NUM,"po_id");
    putOrderHeader(HDR_DATE,"create_ts");
    putOrderHeader(HDR_AUTHOR,"user_name",false);
    putOrderHeader(HDR_INV,"inv_name",false);
    putOrderHeader(HDR_VND,"vendor_name",false);
    putOrderHeader(HDR_STATUS,"po_status",false);
    putOrderHeader(HDR_TOTAL,"item_total");
    putOrderHeader(HDR_VERSION,"version_num",false);
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(Mvc.DISCARD_ARGS_FLAG,""));
      fields.add(new DateStringField(FN_FROM_DATE,"From Date",false,false));
      fields.add(new DateStringField(FN_TO_DATE,"To Date",true,false));
      fields.add(new DropDownField(FN_VENDOR_ID,"Vendor",new VendorTable(),true));
      fields.add(new DropDownField(FN_INV_ID,"Inventory",new InventoryTable(),true));
      fields.add(new DropDownField(FN_STATUS,"Status",new StatusTable(),true));
      fields.add(new Field(FN_OTHER,30,20,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      //fields.setShowErrorText(true);
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public List getPos()
  {
    // create a new empty list if no list present or an autosubmit failed
    if (pos == null || (isAutoSubmit() && !isAutoValid()))
    {
      pos = new ArrayList();
    }
    return pos;
  }
  public List getRows()
  {
    return getPos();
  }

  private List lookupPurchaseOrders(java.util.Date utilFrom, java.util.Date utilTo, 
    long invId, long vendorId, String poStatus, String lookup)
  {
    long longLookup = -1L;
    try
    {
      longLookup = Long.parseLong(lookup);
    }
    catch (Exception e) { }

    PurchaseOrder po = new PurchaseOrder();
    Selection sel = db.getSelection(po,false);

    // add any indexes
    if (longLookup != -1L)
    {
      po.setPoId(longLookup);
      sel.addIndex(db.IDX_PO_KEY);
    }
    if (invId != 0)
    {
      po.setInvId(invId);
      sel.addIndex(db.IDX_PO_INV_ID);
    }
    if (vendorId != 0)
    {
      po.setVendorId(vendorId);
      sel.addIndex(db.IDX_PO_VND_ID);
    }

    // parse po status string, generate po status filter
    if (poStatus != null && !poStatus.equals(""))
    {
      InFilter filter = null;
      InFilter notFilter = null;
      String[] vals = poStatus.split(" ");
      for (int i = 0; i < vals.length; ++i)
      {
        if (vals[i].startsWith("!"))
        {
          if (notFilter == null) notFilter = new InFilter("po_status",true);
          notFilter.addValue(Integer.parseInt(vals[i].substring(1)));
        }
        else
        {
          if (filter == null) filter = new InFilter("po_status",false);
          filter.addValue(Integer.parseInt(vals[i]));
        }
      }

      if (filter != null) sel.addFilter(filter);
      if (notFilter != null) sel.addFilter(notFilter);
    }

    // create date range filter tied to create_ts in po table
    java.sql.Date sqlFrom = new java.sql.Date(utilFrom.getTime());
    java.sql.Date sqlTo = utilTo != null ?
      new java.sql.Date(utilTo.getTime()) : sqlFrom;
    sel.addFilter(new DateRangeFilter("create_ts",sqlFrom,sqlTo));
    sel.clearOrders();
    sel.addOrder(getOrderName("po_id"),getOrderDirFlag(true));

    return db.selectAll(sel);
  }

  private void loadPurchaseOrders()
  {
    Date    fromDate  = ((DateStringField)getField(FN_FROM_DATE)).getUtilDate();
    Date    toDate    = ((DateStringField)getField(FN_TO_DATE)).getUtilDate();
    long    invId     = getField(FN_INV_ID).asLong();
    long    vendorId  = getField(FN_VENDOR_ID).asLong();
    String  lookup    = getData(FN_OTHER);
    String  poStatus  = getData(FN_STATUS);

    pos = lookupPurchaseOrders(fromDate,toDate,invId,vendorId,poStatus,lookup);
  }
  
  /**
   * Do lookup
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      loadPurchaseOrders();
      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting vendor: " + e);
      e.printStackTrace();
    }

    return submitOk;
  }

  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      // set a default date range if from field is blank
      Calendar cal = Calendar.getInstance();
      DateStringField fromField = (DateStringField)fields.getField(FN_FROM_DATE);
      if (fromField.isBlank())
      {
        DateStringField toField = (DateStringField)fields.getField(FN_TO_DATE);
        toField.setUtilDate(cal.getTime());
        cal.add(cal.MONTH,-1);
        fromField.setUtilDate(cal.getTime());
      }

      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
    }
    
    return loadOk;
  }

  protected void notifyNewOrder(boolean newOrderOk)
  {
    if (newOrderOk)
    {
      loadPurchaseOrders();
    }
  }
}