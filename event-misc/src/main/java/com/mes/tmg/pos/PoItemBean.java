package com.mes.tmg.pos;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;
import com.mes.tmg.Bucket;
import com.mes.tmg.EncryptionType;
import com.mes.tmg.PurchaseOrder;
import com.mes.tmg.PurchaseOrderItem;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.BucketTable;
import com.mes.tmg.util.PartClassTable;
import com.mes.tools.DropDownTable;

/**
 * Used to add or edit a purchase order item
 */

public class PoItemBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PoItemBean.class);
  
  public static final String FN_PO_ITEM_ID    = "poItemId";
  public static final String FN_BUCK_ID       = "buckId";
  public static final String FN_PC_CODE       = "pcCode";
  public static final String FN_PART_COUNT    = "partCount";
  public static final String FN_RECV_COUNT    = "recvCount";
  public static final String FN_PART_PRICE    = "partPrice";
  public static final String FN_PART_FEE      = "partFee";
  public static final String FN_ENCRYPT_FLAG  = "encryptFlag";
  public static final String FN_ET_ID         = "etId";
  public static final String FN_SUBMIT_BTN    = "submit";
  
  public PoItemBean(TmgAction action)
  {
    super(action);
  }

  public class EncryptValidation implements Validation
  {
    public boolean validate(String data)
    {
      // determine the part type (if it's been selected)
      long buckId = getField(FN_BUCK_ID).asLong();
      if (buckId > 0L)
      {
        // load the bucket with the buck id
        Bucket bucket = db.getBucket(buckId);

        // ...and return true if part allows encryption 
        // or encryption was not indicated (field is invalid
        // if part does not support encryption and encryption 
        // was indicated)
        return bucket.getPartType().hasEncrypt() || data.equals("0");
      }
      return true;
    }

    public String getErrorText()
    {
      return "Selected part type does not support encryption";
    }
  }
  
  protected class EncryptTypeTable extends DropDownTable
  {
    public EncryptTypeTable()
    {
      addElement("0","None");
      List etList = db.getEncryptTypes();
      for (Iterator i = etList.iterator(); i.hasNext(); )
      {
        EncryptionType et = (EncryptionType)i.next();
        addElement(String.valueOf(et.getEtId()),et.getEtName());
      }
    }
  }

  private PurchaseOrder po = null;
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_PO_ITEM_ID));
      fields.addAlias(FN_PO_ITEM_ID,"editId");
      // hack: po is not available if create is triggered by autoset in 
      // base class constructor, so it must be manually fetched from the 
      // request here
      po = (PurchaseOrder)request.getAttribute("po");
      fields.add(new DropDownField(FN_BUCK_ID,new BucketTable(db,po.getInvId()),false));
      fields.add(new DropDownField(FN_PC_CODE,new PartClassTable(),false));
      fields.add(new NumberField(FN_PART_COUNT,"Part Count",10,10,false,0));
      fields.add(new NumberField(FN_RECV_COUNT,"Receive Count",10,10,true,0));
      fields.add(new CurrencyField(FN_PART_PRICE,"Part Price",10,10,false,0.01f,9999999999f));
      if (po.encryptSupported())
      {
        fields.add(new DropDownField(FN_ET_ID,new EncryptTypeTable(),false));
        fields.getField(FN_ET_ID).addValidation(new EncryptValidation());
      }
      else
      {
        fields.add(new HiddenField(FN_ET_ID,"0"));
      }
      fields.add(new CurrencyField(FN_PART_FEE,"Part Fee",10,10,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
      e.printStackTrace();
    }
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long poItemId = getField(FN_PO_ITEM_ID).asLong();
      
      if (poItemId != 0L)
      {
        PurchaseOrderItem poItem = db.getPoItem(poItemId);
        setData(FN_BUCK_ID,String.valueOf(poItem.getBuckId()));
        setData(FN_PC_CODE,poItem.getPcCode());
        setData(FN_PART_COUNT,String.valueOf(poItem.getPartCount()));
        setData(FN_RECV_COUNT,String.valueOf(poItem.getRecvCount()));
        setData(FN_PART_PRICE,String.valueOf(poItem.getPartPrice()));
        setData(FN_ET_ID,String.valueOf(poItem.getEtId()));
        setData(FN_PART_FEE,String.valueOf(poItem.getPartFee()));
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading po item: " + e);
    }
    
    return loadOk;
  }

  private PurchaseOrderItem poItem;

  public PurchaseOrderItem getPoItem()
  {
    return poItem;
  }
  
  /**
   * Persists purchase order.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long poItemId = getField(FN_PO_ITEM_ID).asLong();
      
      if (poItemId == 0L)
      {
        poItemId = db.getNewId();
        setData(FN_PO_ITEM_ID,String.valueOf(poItemId));
        poItem = new PurchaseOrderItem();
        poItem.setItemStatus(poItem.POI_STAT_NEW);
        poItem.setPoItemId(poItemId);
        poItem.setCreateDate(Calendar.getInstance().getTime());
        poItem.setUserName(user.getLoginName());

        // lookup s/n type using buck id
        long buckId = getField(FN_BUCK_ID).asLong();
        if (buckId > 0L && db.getBucket(buckId).getPartType().hasSn())
        {
          poItem.setRecvCount(getField(FN_PART_COUNT).asLong());
        }
        else
        {
          poItem.setRecvCount(1);
        }

        // hack: po is not available if create is triggered 
        // by autoset in base class constructor, so it must 
        // be manually fetched from the request here
        PurchaseOrder po = (PurchaseOrder)request.getAttribute("po");
        poItem.setPoId(po.getPoId());
      }
      else
      {
        poItem = db.getPoItem(poItemId);
        poItem.setRecvCount(getField(FN_RECV_COUNT).asLong());
      }

      // have to determine if user is changing anything 
      // besides the receive count
      long    buckId    = getField(FN_BUCK_ID).asLong();
      String  pcCode    = getData(FN_PC_CODE);
      long    partCount = getField(FN_PART_COUNT).asLong();
      double  partPrice = getField(FN_PART_PRICE).asDouble();
      double  partFee   = getField(FN_PART_FEE).asDouble();
      long    etId      = getField(FN_ET_ID).asLong();
      log.debug("partCount=" + partCount);
      log.debug("poItem.getPartCount()=" + poItem.getPartCount());
      if (buckId != poItem.getBuckId() ||
          !pcCode.equals(poItem.getPcCode()) ||
          partCount != poItem.getPartCount() ||
          partPrice != poItem.getPartPrice() ||
          partFee != poItem.getPartFee() ||
          etId != poItem.getEtId())
      {
        nonRecvChangeFlag = true;
      }

      poItem.setBuckId(buckId);
      poItem.setPcCode(pcCode);
      poItem.setPartCount(partCount);
      poItem.setPartPrice(partPrice);
      poItem.setPartFee(partFee);
      poItem.setEtId(etId);

      ((PoAction)action).persistPoItem(poItem);
      
      submitOk = true;
    }
    catch (Exception e)   
    {
      log.error("Error submitting po item: " + e);
      e.printStackTrace();
    }

    return submitOk;
  }

  private boolean nonRecvChangeFlag;

  public boolean poRevertNeeded()
  {
    return nonRecvChangeFlag;
  }
}