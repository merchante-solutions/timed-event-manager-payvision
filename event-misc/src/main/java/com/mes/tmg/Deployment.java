package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class Deployment extends TmgBean
{
  private long          deployId;
  private long          partId;
  private long          stateId;
  private long          startOrdId;
  private String        startUserName;
  private Date          startDate;
  private String        startCode;
  private DeployReason  startReason;
  private long          clientId;
  private Client        client;
  private String        merchNum;
  private String        merchName;
  private long          appSeqNum;
  private long          deployToId;
  private long          endOrdId;
  private String        endUserName;
  private Date          endDate;
  private String        endCode;
  private DeployReason  endReason;

  public void setDeployId(long deployId)
  {
    this.deployId = deployId;
  }
  public long getDeployId()
  {
    return deployId;
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }

  public void setStateId(long stateId)
  {
    this.stateId = stateId;
  }
  public long getStateId()
  {
    return stateId;
  }

  public void setStartOrdId(long startOrdId)
  {
    this.startOrdId = startOrdId;
  }
  public long getStartOrdId()
  {
    return startOrdId;
  }

  public void setStartUserName(String startUserName)
  {
    this.startUserName = startUserName;
  }
  public String getStartUserName()
  {
    return startUserName;
  }

  public void setStartDate(Date startDate)
  {
    this.startDate = startDate;
  }
  public Date getStartDate()
  {
    return startDate;
  }
  public void setStartTs(Timestamp startTs)
  {
    startDate = Tmg.toDate(startTs);
  }
  public Date getStartTs()
  {
    return Tmg.toTimestamp(startDate);
  }

  public void setStartCode(String startCode)
  {
    this.startCode = startCode;
  }
  public String getStartCode()
  {
    return startCode;
  }

  public void setStartReason(DeployReason startReason)
  {
    this.startReason = startReason;
  }
  public DeployReason getStartReason()
  {
    return startReason;
  }

  public void setClientId(long clientId)
  {
    this.clientId = clientId;
  }
  public long getClientId()
  {
    return clientId;
  }

  public void setClient(Client client)
  {
    this.client = client;
  }
  public Client getClient()
  {
    return client;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return merchName;
  }

  public void setAppSeqNum(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
  }
  public long getAppSeqNum()
  {
    return appSeqNum;
  }

  public void setDeployToId(long deployToId)
  {
    this.deployToId = deployToId;
  }
  public long getDeployToId()
  {
    return deployToId;
  }

  public void setEndOrdId(long endOrdId)
  {
    this.endOrdId = endOrdId;
  }
  public long getEndOrdId()
  {
    return endOrdId;
  }

  public void setEndUserName(String endUserName)
  {
    this.endUserName = endUserName;
  }
  public String getEndUserName()
  {
    return endUserName;
  }

  public void setEndDate(Date endDate)
  {
    this.endDate = endDate;
  }
  public Date getEndDate()
  {
    return endDate;
  }
  public void setEndTs(Timestamp endTs)
  {
    endDate = Tmg.toDate(endTs);
  }
  public Date getEndTs()
  {
    return Tmg.toTimestamp(endDate);
  }

  public void setEndCode(String endCode)
  {
    this.endCode = endCode;
  }
  public String getEndCode()
  {
    return endCode;
  }
  public void setEndReason(DeployReason endReason)
  {
    this.endReason = endReason;
  }
  public DeployReason getEndReason()
  {
    return endReason;
  }

  public String toString()
  {
    return "Deployment ["
      + " id: " + deployId
      + ", part id: " + partId
      + ", state id: " + stateId
      + ", start ord: " + startOrdId
      + ", start user: " + startUserName
      + ", start date: " + formatDate(startDate)
      + ", start code: " + startCode
      + ", start reason: " + startReason
      + ", end ord: " + endOrdId
      + ", end user: " + endUserName
      + ", end date: " + formatDate(endDate)
      + ", end code: " + endCode
      + ", end reason: " + endReason
      + ", merch num: " + merchNum
      + ", merch name: " + merchName
      + ", app seq num: " + appSeqNum
      + ", client: " + clientId
      + " ]";
  }
}