package com.mes.tmg;

import java.util.List;

public class Shipper extends TmgBean
{
  private long shipperId;
  private String shipperName;
  private String trackingUrl;
  private List shipTypes;

  public void setShipperId(long shipperId)
  {
    this.shipperId = shipperId;
  }
  public long getShipperId()
  {
    return shipperId;
  }

  public void setShipperName(String shipperName)
  {
    this.shipperName = shipperName;
  }
  public  String getShipperName()
  {
    return shipperName;
  }

  public void setTrackingUrl(String trackingUrl)
  {
    this.trackingUrl = trackingUrl;
  }
  public String getTrackingUrl()
  {
    return trackingUrl;
  }

  public void setShipTypes(List shipTypes)
  {
    this.shipTypes = shipTypes;
  }
  public List getShipTypes()
  {
    return shipTypes;
  }

  public String toString()
  {
    return "Shipper ["
      + " id: " + shipperId
      + ", name: " + shipperName
      + ", trk url: " + trackingUrl
      + " ]";
  }
}