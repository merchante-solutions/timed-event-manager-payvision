package com.mes.tmg.admin;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.TestResultType;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class TestResultTypeBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(TestResultTypeBean.class);
  
  public static final String FN_TRT_ID      = "trtId";
  public static final String FN_TRT_CODE    = "trtCode";
  public static final String FN_TRT_NAME    = "trtName";
  public static final String FN_SUBMIT_BTN  = "submit";

  private TestResultType trt;
  
  public TestResultTypeBean(TmgAction action)
  {
    super(action);
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_TRT_ID));
      fields.addAlias(FN_TRT_ID,"editId");
      fields.add(new Field(FN_TRT_CODE,"Code",32,32,false));
      fields.add(new Field(FN_TRT_NAME,"Name",100,32,false));

      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    try
    {
      db.doConnect();

      long trtId = getField(FN_TRT_ID).asLong();
      
      if (trtId != 0L)
      {
        trt = db.getTestResultType(trtId);
        setData(FN_TRT_CODE,trt.getTrtCode());
        setData(FN_TRT_NAME,trt.getTrtName());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }

    return false;
  }
  
  /**
   * Persists client.
   */
  protected boolean autoSubmit()
  {
    try
    {
      db.doConnect();

      long trtId = getField(FN_TRT_ID).asLong();

      if (trtId == 0L)
      {
        trtId = db.getNewId();
        trt = new TestResultType();
        trt.setTrtId(trtId);
        setData(FN_TRT_ID,String.valueOf(trtId));
      }
      else
      {
        trt = db.getTestResultType(trtId);
      }
      
      trt.setTrtCode(getData(FN_TRT_CODE));
      trt.setTrtName(getData(FN_TRT_NAME));
      db.persist(trt,user);

      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }

    return false;
  }
}