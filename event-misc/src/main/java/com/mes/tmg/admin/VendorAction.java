package com.mes.tmg.admin;

import org.apache.log4j.Logger;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class VendorAction extends TmgAction
{
  static Logger log = Logger.getLogger(VendorAction.class);

  /**
   * Vendor
   */

  private void doShowVendors()
  {
    request.setAttribute("showItems",tmgSession.getDb().getVendors());
    doView(Tmg.VIEW_SHOW_VENDORS);
  }
  
  private void doAddEditVendor()
  {
    VendorBean bean = new VendorBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_VENDOR);
    long vendorId = bean.getField(bean.FN_VENDOR_ID).asLong();
        
    if (bean.isAutoSubmitOk())
    {
      String description = "Vendor " + vendorId + " "
        + (isEdit ? "updated" : "added");
      logAction(description,vendorId);
      addFeedback(description);
      if (isEdit)
      {
        doActionRedirect(Tmg.ACTION_SHOW_VENDORS);
      }
      else
      {
        setBackLink(Tmg.ACTION_EDIT_VENDOR,getBackLink());
        doActionRedirect(Tmg.ACTION_EDIT_VENDOR,"editId=" + vendorId);
      }
    }
    else if (isEdit)
    {
      setContactWhoId(vendorId);
      setAddressWhoId(vendorId);
      doView(Tmg.VIEW_EDIT_VENDOR);
    }
    else
    {
      doView(Tmg.VIEW_ADD_VENDOR);
    }
  }

  private void doDeleteVendor()
  {
    long delId = getParmLong("delId");
  
    if (isConfirmed())
    {
      if (db.getVendor(delId) == null)
      {
        throw new RuntimeException("No vendor with id " + delId + " exists"
          + " to delete");
      }
    
      db.deleteVendor(delId,user);
      String description = "Vendor " + delId + " deleted";
      logAction(description,delId);
      addFeedback(description);
      doActionRedirect(Tmg.ACTION_SHOW_VENDORS);
    }
    else if (isCanceled())
    {
      addFeedback("Delete canceled");
      doActionRedirect(Tmg.ACTION_SHOW_VENDORS);
    }
    else
    {
      confirm("Delete vendor?");
    }
  }

  /**
   * VendorClass
   */

  private void doShowVendorClasses()
  {
    request.setAttribute("showItems",tmgSession.getDb().getVendorClasses());
    doView(Tmg.VIEW_SHOW_VEND_CLASSES);
  }
  
  private void doAddEditVendorClass()
  {
    VendorClassBean bean = new VendorClassBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_VEND_CLASS);
        
    if (bean.isAutoSubmitOk())
    {
      long vpcId = bean.getField(bean.FN_VPC_ID).asLong();
      String description = "Vendor part class " + vpcId + " "
        + (isEdit ? "updated" : "added");
      logAction(description,vpcId);
      addFeedback(description);
      doActionRedirect(Tmg.ACTION_SHOW_VEND_CLASSES);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_VEND_CLASS : Tmg.VIEW_ADD_VEND_CLASS);
    }
  }

  private void doDeleteVendorClass()
  {
    long delId = getParmLong("delId");
  
    if (isConfirmed())
    {
      if (db.getVendorClass(delId) == null)
      {
        throw new RuntimeException("No vendor part class with id " + delId 
          + " exists to delete");
      }
    
      db.deleteVendorClass(delId,user);
      String description = "Vendor part class " + delId + " deleted";
      logAction(description,delId);
      addFeedback(description);
      doActionRedirect(Tmg.ACTION_SHOW_VEND_CLASSES);
    }
    else if (isCanceled())
    {
      addFeedback("Delete canceled");
      doActionRedirect(Tmg.ACTION_SHOW_VEND_CLASSES);
    }
    else
    {
      confirm("Delete vendor part class?");
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_SHOW_VENDORS))
    {
      doShowVendors();
    }
    else if (name.equals(Tmg.ACTION_EDIT_VENDOR) ||
             name.equals(Tmg.ACTION_ADD_VENDOR))
    {
      doAddEditVendor();
    }
    else if (name.equals(Tmg.ACTION_DELETE_VENDOR))
    {
      doDeleteVendor();
    }
    else if (name.equals(Tmg.ACTION_SHOW_VEND_CLASSES))
    {
      doShowVendorClasses();
    }
    else if (name.equals(Tmg.ACTION_EDIT_VEND_CLASS) ||
             name.equals(Tmg.ACTION_ADD_VEND_CLASS))
    {
      doAddEditVendorClass();
    }
    else if (name.equals(Tmg.ACTION_DELETE_VEND_CLASS))
    {
      doDeleteVendorClass();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
