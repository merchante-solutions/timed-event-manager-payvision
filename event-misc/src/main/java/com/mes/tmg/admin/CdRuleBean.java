package com.mes.tmg.admin;

import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.DropDownField;
import com.mes.forms.HiddenField;
import com.mes.tmg.AppTypeRef;
import com.mes.tmg.ClientDeployRule;
import com.mes.tmg.Inventory;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.ClientTable;
import com.mes.tmg.util.InventoryTable;

public class CdRuleBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(CdRuleBean.class);
  
  public static final String FN_CD_RULE_ID      = "cdRuleId";
  public static final String FN_CLIENT_ID       = "clientId";
  public static final String FN_APP_TYPE        = "appType";
  public static final String FN_DEPLOY_TO       = "deployToInvId";
  public static final String FN_PULL_FROM_MES   = "pullFromMes";
  public static final String FN_SUBMIT_BTN      = "submit";

  private ClientDeployRule cdRule;
  
  public CdRuleBean(TmgAction action)
  {
    super(action);
  }

  public class AppTypeTable extends TmgDropDownTable
  {
    public AppTypeTable()
    {
      super(CdRuleBean.this.db);
    }

    public void loadElements()
    {
      for (Iterator i = db.getAppTypeRefs().iterator(); i.hasNext();)
      {
        AppTypeRef atr = (AppTypeRef)i.next();
        addElement(""+atr.getAppTypeCode(),atr.getAppDescription());
      }
    }
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_CD_RULE_ID));
      fields.addAlias(FN_CD_RULE_ID,"editId");
      fields.add(new DropDownField(FN_CLIENT_ID,new ClientTable(db),false));
      fields.add(new DropDownField(FN_APP_TYPE,new AppTypeTable(),false));
      fields.add(new DropDownField(FN_DEPLOY_TO,new InventoryTable(db),false));
      fields.add(new CheckField(FN_PULL_FROM_MES,"Pull From MES Inventory","y",true));

      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    try
    {
      db.doConnect();

      long cdRuleId = getField(FN_CD_RULE_ID).asLong();
      
      if (cdRuleId != 0L)
      {
        cdRule = db.getClientDeployRule(cdRuleId);
        setData(FN_CLIENT_ID,String.valueOf(cdRule.getClientId()));
        setData(FN_APP_TYPE,String.valueOf(cdRule.getAppType()));
        setData(FN_DEPLOY_TO,String.valueOf(cdRule.getDeployToInvId()));
        Inventory inv = cdRule.getPullFromInv();
        if (inv != null)
        {
          setData(FN_PULL_FROM_MES,"y");
        }
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }

    return false;
  }
  
  /**
   * Persists client.
   */
  protected boolean autoSubmit()
  {
    try
    {
      db.doConnect();

      long cdRuleId = getField(FN_CD_RULE_ID).asLong();

      if (cdRuleId == 0L)
      {
        cdRuleId = db.getNewId();
        cdRule = new ClientDeployRule();
        cdRule.setCdRuleId(cdRuleId);
        setData(FN_CD_RULE_ID,String.valueOf(cdRuleId));
      }
      else
      {
        cdRule = db.getClientDeployRule(cdRuleId);
      }
      
      cdRule.setClientId(getField(FN_CLIENT_ID).asLong());
      cdRule.setAppType(getField(FN_APP_TYPE).asLong());
      cdRule.setDeployToInvId(getField(FN_DEPLOY_TO).asLong());
      CheckField pullMes = (CheckField)getField(FN_PULL_FROM_MES);
      if (pullMes.isChecked())
      {
        cdRule.setPullFromInvId(db.getMesInventory().getInvId());
      }
      db.persist(cdRule,user);

      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }

    return false;
  }
}