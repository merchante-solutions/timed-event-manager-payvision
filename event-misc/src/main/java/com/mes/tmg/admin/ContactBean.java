package com.mes.tmg.admin;

import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NameField;
import com.mes.forms.PhoneField;
import com.mes.tmg.Client;
import com.mes.tmg.Contact;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class ContactBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(ContactBean.class);
  
  public static final String FN_CONTACT_ID      = "contId";
  public static final String FN_WHO_ID          = "whoId";
  public static final String FN_LABEL           = "label";
  public static final String FN_NAME            = "name";
  public static final String FN_FIRST_NAME      = FN_NAME + "First";
  public static final String FN_LAST_NAME       = FN_NAME + "Last";
  public static final String FN_EMAIL           = "email";
  public static final String FN_PHONE           = "phone";
  public static final String FN_FAX             = "fax";
  public static final String FN_SUBMIT_BTN      = "submit";
  
  private Client client;

  public ContactBean(TmgAction action)
  {
    super(action);
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_CONTACT_ID));
      fields.addAlias(FN_CONTACT_ID,"editId");
      fields.add(new HiddenField(FN_WHO_ID));
      fields.add(new Field(FN_LABEL,"Label",64,13,false));
      fields.add(new NameField(FN_NAME,"Name",32,false,true));
      fields.add(new EmailField(FN_EMAIL,"Email",64,32,true));
      fields.add(new PhoneField(FN_PHONE,"Phone",true));
      fields.add(new PhoneField(FN_FAX,"Fax",true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      db.doConnect();

      long contactId = getField(FN_CONTACT_ID).asLong();
      
      if (contactId != 0L)
      {
        Contact contact = db.getContact(contactId);
        setData(FN_LABEL,contact.getLabel());
        setData(FN_FIRST_NAME,contact.getFirstName());
        setData(FN_LAST_NAME,contact.getLastName());
        setData(FN_EMAIL,contact.getEmail());
        setData(FN_PHONE,contact.getPhone());
        setData(FN_FAX,contact.getFax());
        setData(FN_WHO_ID,String.valueOf(contact.getWhoId()));
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading contact: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }
    
    return loadOk;
  }
  
  /**
   * Persists client.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      db.doConnect();

      long whoId = getField(FN_WHO_ID).asLong();
      if (whoId == 0L)
      {
        throw new Exception("Attempt to submit contact without whoId set");
      }

      long contactId = getField(FN_CONTACT_ID).asLong();
      Contact contact = null;
      if (contactId == 0L)
      {
        contactId = db.getNewId();
        contact = new Contact();
        contact.setContId(contactId);
        contact.setWhoId(whoId);
        contact.setCreateDate(Calendar.getInstance().getTime());
        setData(FN_CONTACT_ID,String.valueOf(contactId));
      }
      else
      {
        contact = db.getContact(contactId);
      }
      
      contact.setLabel(getData(FN_LABEL));
      contact.setFirstName(getData(FN_FIRST_NAME));
      contact.setLastName(getData(FN_LAST_NAME));
      contact.setEmail(getData(FN_EMAIL));
      contact.setPhone(getData(FN_PHONE));
      contact.setFax(getData(FN_FAX));

      db.persist(contact,user);

      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting contact: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }

    return submitOk;
  }
}