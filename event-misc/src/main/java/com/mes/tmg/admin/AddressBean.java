package com.mes.tmg.admin;

import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CityStateZipField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.Address;
import com.mes.tmg.Client;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class AddressBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(AddressBean.class);
  
  public static final String FN_ADDRESS_ID      = "addrId";
  public static final String FN_WHO_ID          = "whoId";
  public static final String FN_LABEL           = "label";
  public static final String FN_NAME            = "name";
  public static final String FN_ADDR1           = "addr1";
  public static final String FN_ADDR2           = "addr2";
  public static final String FN_CSZ             = "csz";
  public static final String FN_CSZ_CITY        = FN_CSZ + "City";
  public static final String FN_CSZ_STATE       = FN_CSZ + "State";
  public static final String FN_CSZ_ZIP         = FN_CSZ + "Zip";
  public static final String FN_SUBMIT_BTN      = "submit";
  
  private Client client;

  public AddressBean(TmgAction action, String beanName)
  {
    super(action,beanName);
  }
  public AddressBean(TmgAction action)
  {
    super(action);
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_ADDRESS_ID));
      fields.addAlias(FN_ADDRESS_ID,"editId");
      fields.add(new HiddenField(FN_WHO_ID));
      fields.add(new Field(FN_LABEL,"Label",64,13,false));
      fields.add(new Field(FN_NAME,"Name",40,32,false));
      fields.add(new Field(FN_ADDR1,"Line 1",40,32,false));
      fields.add(new Field(FN_ADDR2,"Line 2",40,32,true));
      fields.add(new CityStateZipField(FN_CSZ,"Line 2",32,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      db.doConnect();

      long addressId = getField(FN_ADDRESS_ID).asLong();
      
      if (addressId != 0L)
      {
        Address address = db.getAddress(addressId);
        setData(FN_LABEL,address.getLabel());
        setData(FN_NAME,address.getAddrName());
        setData(FN_ADDR1,address.getAddrLine1());
        setData(FN_ADDR2,address.getAddrLine2());
        setData(FN_CSZ_CITY,address.getCity());
        setData(FN_CSZ_STATE,address.getState());
        setData(FN_CSZ_ZIP,address.getZip());
        setData(FN_WHO_ID,String.valueOf(address.getWhoId()));
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading address: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }
    
    return loadOk;
  }
  
  /**
   * Persists client.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      db.doConnect();

      long whoId = getField(FN_WHO_ID).asLong();
      if (whoId == 0L)
      {
        throw new Exception("Attempt to submit address without whoId set");
      }

      long addressId = getField(FN_ADDRESS_ID).asLong();
      Address address = null;
      if (addressId == 0L)
      {
        addressId = db.getNewId();
        address = new Address();
        address.setAddrId(addressId);
        address.setWhoId(whoId);
        address.setCreateDate(Calendar.getInstance().getTime());
        setData(FN_ADDRESS_ID,String.valueOf(addressId));
      }
      else
      {
        address = db.getAddress(addressId);
      }
      
      address.setLabel(getData(FN_LABEL));
      address.setAddrName(getData(FN_NAME));
      address.setAddrLine1(getData(FN_ADDR1));
      address.setAddrLine2(getData(FN_ADDR2));
      address.setCity(getData(FN_CSZ_CITY));
      address.setState(getData(FN_CSZ_STATE));
      address.setZip(getData(FN_CSZ_ZIP));

      db.persist(address,user);

      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting address: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }

    return submitOk;
  }
}