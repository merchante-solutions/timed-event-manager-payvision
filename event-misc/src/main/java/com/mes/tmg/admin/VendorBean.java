package com.mes.tmg.admin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.tmg.ShipType;
import com.mes.tmg.Shipper;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.Vendor;
import com.mes.tmg.VendorShipType;

public class VendorBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(VendorBean.class);
  
  public static final String FN_VENDOR_ID       = "vendorId";
  public static final String FN_VENDOR_NAME     = "vendorName";
  public static final String FN_ENCRYPT_FLAG    = "encryptFlag";
  public static final String FN_CUSTOMER_ID     = "customerId";

  public static final String FN_SHIP_TYPE       = "shipType_";

  public static final String FN_SUBMIT_BTN    = "submit";
  
  private Vendor vendor;

  public VendorBean(TmgAction action)
  {
    super(action);
  }

  private List shipperGroups;

  public List getShipperGroups()
  {
    return shipperGroups;
  }

  private void createShipTypeFields(HttpServletRequest request)
  {
    List shippers = db.getShippers();
    shipperGroups = new ArrayList();
    for (Iterator i = shippers.iterator(); i.hasNext();)
    {
      Shipper shipper = (Shipper)i.next();
      FieldGroup shipperGroup = new FieldGroup("shipper_" + shipper.getShipperId(),
        shipper.getShipperName());
      for (Iterator j = shipper.getShipTypes().iterator(); j.hasNext();)
      {
        ShipType st = (ShipType)j.next();
        shipperGroup.add(new CheckField(FN_SHIP_TYPE + st.getShipTypeId(),
          st.getShipTypeName(),String.valueOf(st.getShipTypeId()),false));
      }
      fields.add(shipperGroup);
      shipperGroups.add(shipperGroup);
    }
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_VENDOR_ID));
      fields.addAlias(FN_VENDOR_ID,"editId");
      fields.add(new Field(FN_VENDOR_NAME,"Name",100,32,false));
      fields.add(new Field(FN_CUSTOMER_ID,"Name",30,32,true));
      fields.add(new CheckField(FN_ENCRYPT_FLAG,"Key Injection Facilities","y",false));

      createShipTypeFields(request);

      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      db.doConnect();

      long vendorId = getField(FN_VENDOR_ID).asLong();
      
      if (vendorId != 0L)
      {
        vendor = db.getVendor(vendorId);
        setData(FN_VENDOR_NAME,vendor.getVendorName());
        setData(FN_ENCRYPT_FLAG,vendor.getEncryptFlag());
        setData(FN_CUSTOMER_ID,vendor.getCustomerId());

        for (Iterator i = vendor.getVendorShipTypes().iterator(); i.hasNext();)
        {
          VendorShipType vst = (VendorShipType)i.next();
          String shipTypeStr = String.valueOf(vst.getShipTypeId());
          setData(FN_SHIP_TYPE + shipTypeStr,shipTypeStr);
        }
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading vendor: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }
    
    return loadOk;
  }
  
  /**
   * Persists vendor.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      db.doConnect();

      long vendorId = getField(FN_VENDOR_ID).asLong();

      // vendor
      if (vendorId == 0L)
      {
        vendorId = db.getNewId();
        vendor = new Vendor();
        vendor.setVendorId(vendorId);
        setData(FN_VENDOR_ID,String.valueOf(vendorId));
      }
      else
      {
        vendor = db.getVendor(vendorId);
      }
      
      vendor.setVendorName(getData(FN_VENDOR_NAME));
      vendor.setEncryptFlag(getData(FN_ENCRYPT_FLAG));
      vendor.setCustomerId(getData(FN_CUSTOMER_ID));
      db.persist(vendor,user);

      // update ship type options
      db.deleteAllVendorShipTypes(vendorId,user);
      for (Iterator i = shipperGroups.iterator(); i.hasNext();)
      {
        for (Iterator j = ((FieldGroup)i.next()).iterator(); j.hasNext();)
        {
          CheckField cf = (CheckField)j.next();
          if (cf.isChecked())
          {
            VendorShipType vst = new VendorShipType();
            vst.setVendorId(vendorId);
            vst.setShipTypeId(cf.asLong());
            vst.setVstId(db.getNewId());
            db.persist(vst,user);
          }
        }
      }

      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting vendor: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }

    return submitOk;
  }
}