package com.mes.tmg.admin;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.tmg.Client;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class ClientBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(ClientBean.class);
  
  public static final String FN_CLIENT_ID       = "clientId";
  public static final String FN_CLIENT_NAME     = "clientName";
  public static final String FN_HIERARCHY_ID    = "hierarchyId";
  public static final String FN_SUBMIT_BTN      = "submit";
  
  private Client client;

  public ClientBean(TmgAction action)
  {
    super(action);
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_CLIENT_ID));
      fields.addAlias(FN_CLIENT_ID,"editId");
      fields.add(new Field(FN_CLIENT_NAME,"Name",100,32,false));
      fields.add(new NumberField(FN_HIERARCHY_ID,"Hierarchy ID",10,10,true,0));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      db.doConnect();

      long clientId = getField(FN_CLIENT_ID).asLong();
      
      if (clientId != 0L)
      {
        client = db.getClient(clientId);
        setData(FN_CLIENT_NAME,client.getClientName());
        if (client.getHierarchyId() > 0L)
        {
          setData(FN_HIERARCHY_ID,String.valueOf(client.getHierarchyId()));
        }
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading client: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }
    
    return loadOk;
  }
  
  /**
   * Persists client.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      db.doConnect();

      long clientId = getField(FN_CLIENT_ID).asLong();

      // client
      if (clientId == 0L)
      {
        clientId = db.getNewId();
        client = new Client();
        client.setClientId(clientId);
        setData(FN_CLIENT_ID,String.valueOf(clientId));
      }
      else
      {
        client = db.getClient(clientId);
      }
      
      client.setClientName(getData(FN_CLIENT_NAME));
      client.setHierarchyId(getField(FN_HIERARCHY_ID).asLong());
      db.persist(client,user);

      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting client: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }

    return submitOk;
  }
}