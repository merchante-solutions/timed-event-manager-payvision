package com.mes.tmg.admin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.AcrCtrMap;
import com.mes.tmg.AcrType;
import com.mes.tmg.CallTagReason;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class AcrTypeBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(AcrTypeBean.class);
  
  public static final String FN_AT_ID       = "atId";
  public static final String FN_AT_CODE     = "atCode";
  public static final String FN_AT_NAME     = "atName";
  public static final String FN_CTR         = "ctr_";
  public static final String FN_SUBMIT_BTN  = "submit";

  private AcrType at;
  
  public AcrTypeBean(TmgAction action)
  {
    super(action);
  }

  private List actionTypes;

  public List getActionTypes()
  {
    return actionTypes;
  }

  private void createCallTagReasonFields(HttpServletRequest request)
  {
    List types = db.getCallTagReasons();
    actionTypes = new ArrayList();
    for (Iterator i = types.iterator(); i.hasNext();)
    {
      CallTagReason ctr = (CallTagReason)i.next();
      Field ctrField = new CheckField(FN_CTR+ctr.getCtrCode(),
        ctr.getCtrName(),ctr.getCtrCode(),false);
      fields.add(ctrField);
      actionTypes.add(ctrField);
    }
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_AT_ID));
      fields.addAlias(FN_AT_ID,"editId");
      fields.add(new Field(FN_AT_CODE,"Code",32,32,false));
      fields.add(new Field(FN_AT_NAME,"Name",100,32,false));

      createCallTagReasonFields(request);

      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    try
    {
      db.doConnect();

      long atId = getField(FN_AT_ID).asLong();
      
      if (atId != 0L)
      {
        at = db.getAcrType(atId);
        setData(FN_AT_CODE,at.getAtCode());
        setData(FN_AT_NAME,at.getAtName());

        for (Iterator i = at.getAcrCtrMaps().iterator(); i.hasNext();)
        {
          AcrCtrMap map = (AcrCtrMap)i.next();
          setData(FN_CTR + map.getCtrCode(),map.getCtrCode());
        }
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }

    return false;
  }
  
  /**
   * Persists client.
   */
  protected boolean autoSubmit()
  {
    try
    {
      db.doConnect();

      long atId = getField(FN_AT_ID).asLong();

      String origAtCode = null;
      if (atId == 0L)
      {
        atId = db.getNewId();
        at = new AcrType();
        at.setAtId(atId);
        setData(FN_AT_ID,String.valueOf(atId));
      }
      else
      {
        at = db.getAcrType(atId);
        origAtCode = at.getAtCode();
      }
      
      at.setAtCode(getData(FN_AT_CODE));
      at.setAtName(getData(FN_AT_NAME));
      db.persist(at,user);

      // update mapped acr action types
      if (origAtCode != null)
      {
        db.deleteAllAcrCtrMaps(origAtCode,user);
      }
      for (Iterator i = actionTypes.iterator(); i.hasNext();)
      {
        CheckField cf = (CheckField)i.next();
        if (cf.isChecked())
        {
          AcrCtrMap map = new AcrCtrMap();
          map.setAcmId(db.getNewId());
          map.setCtrCode(cf.getData());
          map.setAtCode(at.getAtCode());
          db.persist(map,user);
        }
      }

      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }

    return false;
  }
}