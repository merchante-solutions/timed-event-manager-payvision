package com.mes.tmg.admin;

import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.Vendor;
import com.mes.tmg.VendorClass;
import com.mes.tools.DropDownTable;

public class VendorClassBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(VendorClassBean.class);
  
  public static final String FN_VPC_ID    = "vpcId";
  public static final String FN_VENDOR_ID = "vendorId";
  public static final String FN_VPC_CODE  = "vpcCode";
  public static final String FN_VPC_NAME  = "vpcName";

  public static final String FN_SUBMIT_BTN    = "submit";
  
  public VendorClassBean(TmgAction action)
  {
    super(action);
  }

  public class VendorTable extends DropDownTable
  {
    public VendorTable()
    {
      addElement("","select one");
      List vendors = db.getVendors();
      for (Iterator i = vendors.iterator(); i.hasNext(); )
      {
        Vendor vendor = (Vendor)i.next();
        addElement(String.valueOf(vendor.getVendorId()),vendor.getVendorName());
      }
    }
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_VPC_ID));
      fields.addAlias(FN_VPC_ID,"editId");
      fields.add(new DropDownField(FN_VENDOR_ID,new VendorTable(),false));
      fields.add(new Field(FN_VPC_CODE,"Name",16,32,false));
      fields.add(new Field(FN_VPC_NAME,"Name",100,32,false));

      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      VendorClass vpc = null;
      long vpcId = getField(FN_VPC_ID).asLong();
      
      if (vpcId == 0L)
      {
        log.debug("creating new vpc...");
        vpcId = db.getNewId();
        vpc = new VendorClass();
        vpc.setVpcId(vpcId);
        setData(FN_VPC_ID,String.valueOf(vpcId));
      }
      else
      {
        log.debug("loading vpc id " + vpcId);
        vpc = db.getVendorClass(vpcId);
      }

      vpc.setVendorId(getField(FN_VENDOR_ID).asLong());
      vpc.setVpcCode(getData(FN_VPC_CODE));
      vpc.setVpcName(getData(FN_VPC_NAME));

      log.debug("Persisting vpc: " + vpc);

      db.persist(vpc,user);

      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting vendor class: " + e);
    }
    
    return submitOk;
  }

  protected boolean autoLoad()
  {
    boolean loadOk = false;
    try
    {
      long vpcId = getField(FN_VPC_ID).asLong();
      
      if (vpcId != 0L)
      {
        VendorClass vpc = db.getVendorClass(vpcId);
        setData(FN_VENDOR_ID,String.valueOf(vpc.getVendorId()));
        setData(FN_VPC_CODE,vpc.getVpcCode());
        setData(FN_VPC_NAME,vpc.getVpcName());
      }

      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading vendor class: " + e);
    }
    
    return loadOk;
  }
}