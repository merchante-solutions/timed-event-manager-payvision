package com.mes.tmg.admin;

import org.apache.log4j.Logger;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class ClientAction extends TmgAction
{
  static Logger log = Logger.getLogger(ClientAction.class);

  /**
   * Client
   */

  public void doShowClients()
  {
    request.setAttribute("showItems",db.getClients());
    doView(Tmg.VIEW_SHOW_CLIENTS);
  }
  
  public void doEditClient()
  {
    ClientBean bean = new ClientBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_CLIENT);
        
    if (bean.isAutoSubmitOk())
    {
      long clientId = bean.getField(bean.FN_CLIENT_ID).asLong();
      String description = "Client " + clientId + " "
        + (isEdit ? "updated" : "added");
      logAction(description,clientId);
      addFeedback(description);
      doActionRedirect(Tmg.ACTION_SHOW_CLIENTS);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_CLIENT : Tmg.VIEW_ADD_CLIENT);
    }
  }
  public void doAddClient()
  {
    doEditClient();
  }

  public void doDeleteClient()
  {
    long delId = getParmLong("delId");
  
    if (isConfirmed())
    {
      if (db.getClient(delId) == null)
      {
        throw new RuntimeException("No client with id " + delId + " exists"
          + " to delete");
      }
    
      db.deleteClient(delId,user);
      String description = "Client " + delId + " deleted";
      logAction(description,delId);
      addFeedback(description);
      doActionRedirect(Tmg.ACTION_SHOW_CLIENTS);
    }
    else if (isCanceled())
    {
      addFeedback("Delete canceled");
      doActionRedirect(Tmg.ACTION_SHOW_CLIENTS);
    }
    else
    {
      confirm("Delete client?");
    }
  }

  /**
   * ClientDeployRule
   */

  public void doShowCdRules()
  {
    request.setAttribute("showItems",db.getClientDeployRules());
    doView(Tmg.VIEW_SHOW_CD_RULES);
  }

  public void doEditCdRule()
  {
    CdRuleBean bean = new CdRuleBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_CD_RULE);
        
    if (bean.isAutoSubmitOk())
    {
      long cdRuleId = bean.getField(bean.FN_CD_RULE_ID).asLong();
      String description = "Client Deployment Rule " + cdRuleId + " "
        + (isEdit ? "updated" : "added");
      logAction(description,cdRuleId);
      addFeedback(description);
      doActionRedirect(Tmg.ACTION_SHOW_CD_RULES);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_CD_RULE : Tmg.VIEW_ADD_CD_RULE);
    }
  }
  public void doAddCdRule()
  {
    doEditCdRule();
  }

  public void doDeleteCdRule()
  {
    long delId = getParmLong("delId");
  
    if (isConfirmed())
    {
      if (db.getClientDeployRule(delId) == null)
      {
        throw new RuntimeException("No client with id " + delId + " exists"
          + " to delete");
      }
    
      db.deleteClientDeployRule(delId,user);
      String description = "Client deployment rule " + delId + " deleted";
      logAction(description,delId);
      addFeedback(description);
      doActionRedirect(Tmg.ACTION_SHOW_CD_RULES);
    }
    else if (isCanceled())
    {
      addFeedback("Delete canceled");
      doActionRedirect(Tmg.ACTION_SHOW_CD_RULES);
    }
    else
    {
      confirm("Delete client deployment rule?");
    }
  }
}
