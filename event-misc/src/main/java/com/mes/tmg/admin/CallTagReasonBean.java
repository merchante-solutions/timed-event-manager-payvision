package com.mes.tmg.admin;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.CallTagReason;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class CallTagReasonBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(CallTagReasonBean.class);
  
  public static final String FN_CTR_ID      = "ctrId";
  public static final String FN_CTR_CODE    = "ctrCode";
  public static final String FN_CTR_NAME    = "ctrName";
  public static final String FN_SUBMIT_BTN  = "submit";

  private CallTagReason ctr;
  
  public CallTagReasonBean(TmgAction action)
  {
    super(action);
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_CTR_ID));
      fields.addAlias(FN_CTR_ID,"editId");
      fields.add(new Field(FN_CTR_CODE,"Code",32,32,false));
      fields.add(new Field(FN_CTR_NAME,"Name",100,32,false));

      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    try
    {
      db.doConnect();

      long ctrId = getField(FN_CTR_ID).asLong();
      
      if (ctrId != 0L)
      {
        ctr = db.getCallTagReason(ctrId);
        setData(FN_CTR_CODE,ctr.getCtrCode());
        setData(FN_CTR_NAME,ctr.getCtrName());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }

    return false;
  }
  
  /**
   * Persists client.
   */
  protected boolean autoSubmit()
  {
    try
    {
      db.doConnect();

      long ctrId = getField(FN_CTR_ID).asLong();

      if (ctrId == 0L)
      {
        ctrId = db.getNewId();
        ctr = new CallTagReason();
        ctr.setCtrId(ctrId);
        setData(FN_CTR_ID,String.valueOf(ctrId));
      }
      else
      {
        ctr = db.getCallTagReason(ctrId);
      }
      
      ctr.setCtrCode(getData(FN_CTR_CODE));
      ctr.setCtrName(getData(FN_CTR_NAME));
      db.persist(ctr,user);

      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
      e.printStackTrace();
    }
    finally
    {
      db.doDisconnect();
    }

    return false;
  }
}