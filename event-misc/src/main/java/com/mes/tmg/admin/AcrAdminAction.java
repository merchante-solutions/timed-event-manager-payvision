package com.mes.tmg.admin;

import org.apache.log4j.Logger;
import com.mes.tmg.AcrAction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class AcrAdminAction extends TmgAction
{
  static Logger log = Logger.getLogger(AcrAction.class);

  public void doShowCallTagReasons()
  {
    request.setAttribute("showItems",db.getCallTagReasons());
    doView(Tmg.VIEW_SHOW_CT_REASONS);
  }

  public void doEditCallTagReason()
  {
    CallTagReasonBean bean = new CallTagReasonBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_CT_REASON);

    if (bean.isAutoSubmitOk())
    {
      long ctrId = bean.getField(bean.FN_CTR_ID).asLong();
      String description = "Call tag reason " + ctrId + " "
        + (isEdit ? "updated" : "added");
      logAction(description,ctrId);
      addFeedback(description);
      doActionRedirect(Tmg.ACTION_SHOW_CT_REASONS);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_CT_REASON : 
              Tmg.VIEW_ADD_CT_REASON);
    }
  }

  public void doAddCallTagReason()
  {
    doEditCallTagReason();
  }

  public void doDeleteCallTagReason()
  {
    long delId = getParmLong("delId");
  
    if (isConfirmed())
    {
      if (db.getCallTagReason(delId) == null)
      {
        throw new RuntimeException("No call tag reason with id " + delId 
          + " exists to delete");
      }
    
      db.deleteCallTagReason(delId,user);
      String description = "Call tag reason " + delId + " deleted";
      logAction(description,delId);
      addFeedback(description);
      doActionRedirect(Tmg.ACTION_SHOW_CT_REASONS);
    }
    else if (isCanceled())
    {
      addFeedback("Delete canceled");
      doActionRedirect(Tmg.ACTION_SHOW_CT_REASONS);
    }
    else
    {
      confirm("Delete call tag reason?");
    }
  }

  public void doShowAcrTypes()
  {
    request.setAttribute("showItems",db.getAcrTypes());
    doView(Tmg.VIEW_SHOW_ACR_TYPES);
  }

  public void doEditAcrType()
  {
    AcrTypeBean bean = new AcrTypeBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_ACR_TYPE);

    if (bean.isAutoSubmitOk())
    {
      long atId = bean.getField(bean.FN_AT_ID).asLong();
      String description = "ACR type " + atId + " "
        + (isEdit ? "updated" : "added");
      logAction(description,atId);
      addFeedback(description);
      doActionRedirect(Tmg.ACTION_SHOW_ACR_TYPES);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_ACR_TYPE : Tmg.VIEW_ADD_ACR_TYPE);
    }
  }

  public void doAddAcrType()
  {
    doEditAcrType();
  }

  public void doDeleteAcrType()
  {
    long delId = getParmLong("delId");
  
    if (isConfirmed())
    {
      if (db.getAcrType(delId) == null)
      {
        throw new RuntimeException("No ACR type with id " + delId + " exists"
          + " to delete");
      }
    
      db.deleteAcrType(delId,user);
      String description = "ACR type " + delId + " deleted";
      logAction(description,delId);
      addFeedback(description);
      doActionRedirect(Tmg.ACTION_SHOW_ACR_TYPES);
    }
    else if (isCanceled())
    {
      addFeedback("Delete canceled");
      doActionRedirect(Tmg.ACTION_SHOW_ACR_TYPES);
    }
    else
    {
      confirm("Delete ACR type?");
    }
  }
}
