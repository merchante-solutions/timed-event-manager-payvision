package com.mes.tmg.admin;

import org.apache.log4j.Logger;
import com.mes.tmg.Address;
import com.mes.tmg.Contact;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class AdminAction extends TmgAction
{
  static Logger log = Logger.getLogger(AdminAction.class);

  public void doEditContact()
  {
    ContactBean bean = new ContactBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_CONTACT);
    if (bean.isAutoSubmitOk())
    {
      logAndRedirect(getBackLink(),"Contact");
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_CONTACT : Tmg.VIEW_ADD_CONTACT);
    }
  }
  public void doAddContact()
  {
    doEditContact();
  }

  public void doDeleteContact()
  {
    long delId = getParmLong("delId");
    Contact contact = db.getContact(delId);
    if (contact == null)
    {
      throw new RuntimeException("No contact with id " + delId + " exists"
        + " to delete");
    }
    else if (isConfirmed())
    {
      db.deleteContact(delId,user);
      String description = "Contact '" + contact.getLabel() + "' deleted";
      logAction(description,delId);
      addFeedback(description);
      doLinkRedirect(getBackLink());
    }
    else if (isCanceled())
    {
      addFeedback("Delete canceled");
      doLinkRedirect(getBackLink());
    }
    else
    {
      confirm("Delete contact '" + contact.getLabel() + "'?");
    }
  }

  public void doEditAddress()
  {
    AddressBean bean = new AddressBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_ADDRESS);
    if (bean.isAutoSubmitOk())
    {
      logAndRedirect(getBackLink(),"Address");
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_ADDRESS : Tmg.VIEW_ADD_ADDRESS);
    }
  }
  public void doAddAddress()
  {
    doEditAddress();
  }

  public void doDeleteAddress()
  {
    long delId = getParmLong("delId");
    Address address = db.getAddress(delId);
    if (address == null)
    {
      throw new RuntimeException("No address with id " + delId + " exists"
        + " to delete");
    }
    else if (isConfirmed())
    {
      db.deleteAddress(delId,user);
      String description = "Address '" + address.getLabel() + "' deleted";
      logAction(description,delId);
      addFeedback(description);
      doLinkRedirect(getBackLink());
    }
    else if (isCanceled())
    {
      addFeedback("Delete canceled");
      doLinkRedirect(getBackLink());
    }
    else
    {
      confirm("Delete address '" + address.getLabel() + "'?");
    }
  }

  public void doShowTestResultTypes()
  {
    request.setAttribute("showItems",db.getTestResultTypes());
    doView(Tmg.VIEW_SHOW_TR_TYPES);
  }

  public void doEditTestResultType()
  {
    TestResultTypeBean bean = new TestResultTypeBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_TR_TYPE);
    if (bean.isAutoSubmitOk())
    {
      logAndRedirect(getBackLink(),"Test result type");
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_TR_TYPE : Tmg.VIEW_ADD_TR_TYPE);
    }
  }
  public void doAddTestResultType()
  {
    doEditTestResultType();
  }
}
