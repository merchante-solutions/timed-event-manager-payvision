package com.mes.tmg;

public class Condition
{
  private long condId;
  private String condCode;
  private String condName;

  public void setCondId(long condId)
  {
    this.condId = condId;
  }
  public long getCondId()
  {
    return condId;
  }
  
  public void setCondCode(String condCode)
  {
    this.condCode = condCode;
  }
  public String getCondCode()
  {
    return condCode;
  }

  public void setCondName(String condName)
  {
    this.condName = condName;
  }
  public String getCondName()
  {
    return condName;
  }

  public String toString()
  {
    return "Condition [ "
      +"id: " + condId 
      + ", code: " + condCode
      + ", name: " + condName 
      + " ]";
  }
}