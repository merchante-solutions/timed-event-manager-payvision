package com.mes.tmg;

public class MifRef extends TmgBean
{
  private long    merchantNumber;
  private String  dbaName;

  public void setMerchantNumber(long merchantNumber)
  {
    this.merchantNumber = merchantNumber;
  }
  public long getMerchantNumber()
  {
    return merchantNumber;
  }
  public void setMerchNum(String merchNum)
  {
    setMerchantNumber(Long.parseLong(merchNum));
  }
  public String getMerchNum()
  {
    return String.valueOf(getMerchantNumber());
  }

  public void setDbaName(String dbaName)
  {
    this.dbaName = dbaName;
  }
  public String getDbaName()
  {
    return dbaName;
  }

  public String toString()
  {
    return "MifRef ["
      + ", merch num: " + merchantNumber
      + ", name: " + dbaName
      + " ]";
  }
}