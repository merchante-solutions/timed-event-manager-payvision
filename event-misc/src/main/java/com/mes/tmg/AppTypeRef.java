package com.mes.tmg;

public class AppTypeRef extends TmgBean
{
  private int appTypeCode;
  private int appBankNumber;
  private String appDescription;
  private String pullCredit;
  private String separateInventory;
  private String inventoryOwnerName;
  private int clientCode;
  private String newAppScheme;
  private int rightSubmit;
  private String nonDeployApp;
  private String excludeFromIntMatchResults;
  private String autoApprove;
  private String mesActivation;
  private String clientReview;
  private String autoDataExpand;
  private String separateMmsFile;
  private String restrictMes;
  private String amexEsaSourceCode;
  private String pullCreditConversion;
  private int minDiscountDelayMonths;
  private String delayMonthlyFeesConversion;

  public void setAppTypeCode(int appTypeCode)
  {
    this.appTypeCode = appTypeCode;
  }
  public int getAppTypeCode()
  {
    return appTypeCode;
  }

  public void setAppBankNumber(int appBankNumber)
  {
    this.appBankNumber = appBankNumber;
  }
  public int getAppBankNumber()
  {
    return appBankNumber;
  }

  public void setAppDescription(String appDescription)
  {
    this.appDescription = appDescription;
  }
  public String getAppDescription()
  {
    return appDescription;
  }

  public void setPullCredit(String pullCredit)
  {
    this.pullCredit = pullCredit;
  }
  public String getPullCredit()
  {
    return pullCredit;
  }

  public void setSeparateInventory(String separateInventory)
  {
    this.separateInventory = separateInventory;
  }
  public String getSeparateInventory()
  {
    return separateInventory;
  }

  public void setInventoryOwnerName(String inventoryOwnerName)
  {
    this.inventoryOwnerName = inventoryOwnerName;
  }
  public String getInventoryOwnerName()
  {
    return inventoryOwnerName;
  }

  public void setClientCode(int clientCode)
  {
    this.clientCode = clientCode;
  }
  public int getClientCode()
  {
    return clientCode;
  }

  public void setNewAppScheme(String newAppScheme)
  {
    this.newAppScheme = newAppScheme;
  }
  public String getNewAppScheme()
  {
    return newAppScheme;
  }

  public void setRightSubmit(int rightSubmit)
  {
    this.rightSubmit = rightSubmit;
  }
  public int getRightSubmit()
  {
    return rightSubmit;
  }

  public void setNonDeployApp(String nonDeployApp)
  {
    this.nonDeployApp = nonDeployApp;
  }
  public String getNonDeployApp()
  {
    return nonDeployApp;
  }

  public void setExcludeFromIntMatchResults(String excludeFromIntMatchResults)
  {
    this.excludeFromIntMatchResults = excludeFromIntMatchResults;
  }
  public String getExcludeFromIntMatchResults()
  {
    return excludeFromIntMatchResults;
  }

  public void setAutoApprove(String autoApprove)
  {
    this.autoApprove = autoApprove;
  }
  public String getAutoApprove()
  {
    return autoApprove;
  }

  public void setMesActivation(String mesActivation)
  {
    this.mesActivation = mesActivation;
  }
  public String getMesActivation()
  {
    return mesActivation;
  }

  public void setClientReview(String clientReview)
  {
    this.clientReview = clientReview;
  }
  public String getClientReview()
  {
    return clientReview;
  }

  public void setAutoDataExpand(String autoDataExpand)
  {
    this.autoDataExpand = autoDataExpand;
  }
  public String getAutoDataExpand()
  {
    return autoDataExpand;
  }

  public void setSeparateMmsFile(String separateMmsFile)
  {
    this.separateMmsFile = separateMmsFile;
  }
  public String getSeparateMmsFile()
  {
    return separateMmsFile;
  }

  public void setRestrictMes(String restrictMes)
  {
    this.restrictMes = restrictMes;
  }
  public String getRestrictMes()
  {
    return restrictMes;
  }

  public void setAmexEsaSourceCode(String amexEsaSourceCode)
  {
    this.amexEsaSourceCode = amexEsaSourceCode;
  }
  public String getAmexEsaSourceCode()
  {
    return amexEsaSourceCode;
  }

  public void setPullCreditConversion(String pullCreditConversion)
  {
    this.pullCreditConversion = pullCreditConversion;
  }
  public String getPullCreditConversion()
  {
    return pullCreditConversion;
  }

  public void setMinDiscountDelayMonths(int minDiscountDelayMonths)
  {
    this.minDiscountDelayMonths = minDiscountDelayMonths;
  }
  public int getMinDiscountDelayMonths()
  {
    return minDiscountDelayMonths;
  }

  public void setDelayMonthlyFeesConversion(String delayMonthlyFeesConversion)
  {
    this.delayMonthlyFeesConversion = delayMonthlyFeesConversion;
  }
  public String getDelayMonthlyFeesConversion()
  {
    return delayMonthlyFeesConversion;
  }

  public String toString()
  {
    return "AppTypeRef [ "
      + "app type: " + appTypeCode
      + ", bank num: " + appBankNumber
      + ", desc: " + appDescription
      + ", pull credit: " + pullCredit
      + ", sep inv: " + separateInventory
      + ", inv owner: " + inventoryOwnerName
      + ", client code: " + clientCode
      + ", new app: " + newAppScheme
      + ", submit right: " + rightSubmit
      + ", non deploy: " + nonDeployApp
      + ", exclude match: " + excludeFromIntMatchResults
      + ", auto approve: " + autoApprove
      + ", mes activate: " + mesActivation
      + ", client review: " + clientReview
      + ", auto expand: " + autoDataExpand
      + ", sep mms file: " + separateMmsFile
      + ", restrict mes: " + restrictMes
      + ", esa src code: " + amexEsaSourceCode
      + ", pull cred conv: " + pullCreditConversion
      + ", min disc delay months: " + minDiscountDelayMonths
      + ", delay month fee conv: " + delayMonthlyFeesConversion
      + " ]";
  }
}