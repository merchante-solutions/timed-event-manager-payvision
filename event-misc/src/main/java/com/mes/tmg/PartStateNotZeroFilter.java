package com.mes.tmg;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.mes.persist.AbstractFilter;
import com.mes.persist.TableDef;

public class PartStateNotZeroFilter extends AbstractFilter
{
  public PartStateNotZeroFilter()
  {
    colName = "count";
  }

  public String getWhereClause(TableDef td, String qualifier)
  {
    return td.getColumnDef("count").getQualifiedName(qualifier) + " > 0 ";
  }

  public String getAuditLog()
  {
    return td.getColumnDef("count").getQualifiedName(qualifier) + " > 0 ";
  }

  public int setMarks(PreparedStatement ps, int mark) throws SQLException
  {
    return mark;
  }
}