package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Transfer extends TmgBean
{
  private long      xferId;
  private Date      createDate;
  private String    userName;
  private long      fromInvId;
  private Inventory fromInventory;
  private long      toInvId;
  private Inventory toInventory;
  private List      parts;
  private long      opId;

  public void setXferId(long xferId)
  {
    this.xferId = xferId;
  }
  public long getXferId()
  {
    return xferId;
  }
  
  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setFromInvId(long fromInvId)
  {
    this.fromInvId = fromInvId;
  }
  public long getFromInvId()
  {
    return fromInvId;
  }

  public void setFromInventory(Inventory fromInventory)
  {
    this.fromInventory = fromInventory;
  }
  public Inventory getFromInventory()
  {
    return fromInventory;
  }

  public void setToInvId(long toInvId)
  {
    this.toInvId = toInvId;
  }
  public long getToInvId()
  {
    return toInvId;
  }

  public void setToInventory(Inventory toInventory)
  {
    this.toInventory = toInventory;
  }
  public Inventory getToInventory()
  {
    return toInventory;
  }

  public void setParts(List parts)
  {
    this.parts = parts;
  }
  public List getParts()
  {
    return parts;
  }

  public TransferPart getXferPartWithPartId(long partId)
  {
    for (Iterator i = parts.iterator(); i.hasNext();)
    {
      TransferPart xferPart = (TransferPart)i.next();
      if (xferPart.getPartId() == partId)
      {
        return xferPart;
      }
    }
    return null;
  }

  public void setOpId(long opId)
  {
    this.opId = opId;
  }
  public long getOpId()
  {
    return opId;
  }

  public String toString()
  {
    return "Transfer ["
      + " id: " + xferId 
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + ", from inv id: " + fromInvId
      + ", to inv id: " + toInvId
      + ", op: " + opId
      + " ]";
  }
}