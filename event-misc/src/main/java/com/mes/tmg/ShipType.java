package com.mes.tmg;

public class ShipType extends TmgBean
{
  private long shipTypeId;
  private long shipperId;
  private String shipTypeName;

  public void setShipTypeId(long shipTypeId)
  {
    this.shipTypeId = shipTypeId;
  }
  public long getShipTypeId()
  {
    return shipTypeId;
  }

  public void setShipperId(long shipperId)
  {
    this.shipperId = shipperId;
  }
  public long getShipperId()
  {
    return shipperId;
  }

  public void setShipTypeName(String shipTypeName)
  {
    this.shipTypeName = shipTypeName;
  }
  public  String getShipTypeName()
  {
    return shipTypeName;
  }

  public String toString()
  {
    return "ShipType ["
      + " id: " + shipTypeId
      + " shipper: " + shipperId
      + ", name: " + shipTypeName
      + " ]";
  }
}