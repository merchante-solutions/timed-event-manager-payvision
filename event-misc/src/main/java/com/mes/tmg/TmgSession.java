package com.mes.tmg;

import org.apache.log4j.Logger;
import com.mes.mvc.Action;
import com.mes.user.UserBean;

public class TmgSession
{
  static Logger log = Logger.getLogger(TmgSession.class);

  private TmgDb db;
  private UserBean sessionUser;
  private String sessionLoginName;
  private Client client;

  private Inventory inventory;
  private Bucket partBucket;
  private PartType partType;
  private PurchaseOrder po;
  private PurchaseOrderItem poItem;

  public TmgSession(TmgDb db, UserBean sessionUser)
  {
    this.db = db;
    this.sessionUser = sessionUser;
    sessionLoginName 
      = (sessionUser != null ? sessionUser.getLoginName() : null);
  }
  
  public TmgDb getDb()
  {
    return db;
  }

  public Client getClient(UserBean user)
  {
    if (!user.getLoginName().equals(sessionLoginName))
    {
      sessionLoginName = user.getLoginName();
      sessionUser = user;
      client = null;
    }
    if (client == null)
    {
      client = db.getClientOfUser(user);
    }
    return client;
  }

  public void notifyPersist(Object persisted)
  {
    if (inventory != null && persisted instanceof Inventory)
    {
      Inventory pobj = (Inventory)persisted;
      if (pobj.getInvId() == inventory.getInvId())
      {
        inventory = pobj;
      }
    }
    else if (partBucket != null && persisted instanceof Bucket)
    {
      Bucket pobj = (Bucket)persisted;
      if (pobj.getBuckId() == partBucket.getBuckId())
      {
        partBucket = pobj;
      }
    }
    else if (po != null && persisted instanceof PurchaseOrder)
    {
      PurchaseOrder pobj = (PurchaseOrder)persisted;
      if (pobj.getPoId() == po.getPoId())
      {
        po = pobj;
      }
    }
    else if (poItem != null && persisted instanceof PurchaseOrderItem)
    {
      PurchaseOrderItem pobj = (PurchaseOrderItem)persisted;
      if (pobj.getPoItemId() == poItem.getPoItemId())
      {
        poItem = pobj;
      }
    }
  }

  public Inventory getInventory(Action action)
  {
    long invId = action.getParmLong("invId",-1L);
    if (inventory == null && invId == -1L)
    {
      throw new RuntimeException("Missing inventory id");
    }
    else if (invId != -1L)
    {
      inventory = db.getInventory(invId);
      if (inventory == null)
      {
        throw new RuntimeException("Invalid inventory id " + invId);
      }
    }
    return inventory;
  }

  public Bucket getPartBucket(Action action)
  {
    long buckId = action.getParmLong("buckId",-1L);
    if (partBucket == null && buckId == -1L)
    {
      throw new RuntimeException("Missing bucket id");
    }
    else if (buckId != -1L)
    {
      partBucket = db.getBucket(buckId);
      if (partBucket == null)
      {
        throw new RuntimeException("Invalid bucket id " + buckId);
      }
    }
    return partBucket;
  }

  public PartType getPartType(Action action)
  {
    long ptId = action.getParmLong("ptId",-1L);
    if (partType == null && ptId == -1L)
    {
      throw new RuntimeException("Missing part type id");
    }
    else if (ptId != -1L)
    {
      partType = db.getPartType(ptId);
      if (partType == null)
      {
        throw new RuntimeException("Invalid part type id " + ptId);
      }
    }
    return partType;
  }

  public PurchaseOrder getPurchaseOrder(Action action)
  {
    long poId = action.getParmLong("poId",-1L);
    if (poId != -1L)
    {
      po = db.getPurchaseOrder(poId);
      if (po == null)
      {
        throw new RuntimeException("Invalid purchase order id " + poId);
      }
    }
    return po;
  }

  public PurchaseOrderItem getPurchaseOrderItem(Action action)
  {
    long poItemId = action.getParmLong("poItemId",-1L);
    if (poItem == null && poItemId == -1L)
    {
      throw new RuntimeException("Missing purchase order item id");
    }
    else if (poItemId != -1L)
    {
      poItem = db.getPoItem(poItemId);
      if (poItem == null)
      {
        throw new RuntimeException("Invalid purchase order item id " + poItemId);
      }
    }
    return poItem;
  }


}