package com.mes.tmg;

/**
 * These are applied to parts coming back into inventory, they indicate
 * what if any problem was found with a part after testing.
 */

public class TestResultType extends TmgBean
{
  public static final String TRTC_OK          = "OK";
  public static final String TRTC_CASE        = "CASE";
  public static final String TRTC_DISPLAY     = "DISPLAY";
  public static final String TRTC_KEYBOARD    = "KEYBOARD";
  public static final String TRTC_MAG_RDR     = "MAG_RDR";
  public static final String TRTC_MODEM       = "MODEM";
  public static final String TRTC_PRINTER     = "PRINTER";

  private long    trtId;
  private String  trtCode;
  private String  trtName;

  public void setTrtId(long trtId)
  {
    this.trtId = trtId;
  }
  public long getTrtId()
  {
    return trtId;
  }

  public void setTrtCode(String trtCode)
  {
    this.trtCode = trtCode;
  }
  public String getTrtCode()
  {
    return trtCode;
  }

  public void setTrtName(String trtName)
  {
    this.trtName = trtName;
  }
  public String getTrtName()
  {
    return trtName;
  }

  public String toString()
  {
    return "TestResultType ["
      + " id: " + trtId
      + ", code: " + trtCode
      + ", name: "+trtName
      + " ]";
  }
}