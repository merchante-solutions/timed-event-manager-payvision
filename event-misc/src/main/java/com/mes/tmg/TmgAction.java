package com.mes.tmg;

import org.apache.log4j.Logger;
import com.mes.mvc.Link;
import com.mes.mvc.RequestHandler;
import com.mes.mvc.mes.MesAction;

public class TmgAction extends MesAction
{
  static Logger log = Logger.getLogger(TmgAction.class);

  protected TmgRequestHandler tmgHandler;
  protected TmgSession        tmgSession;
  protected TmgDb             db;
  
  public void setHandler(RequestHandler handler, String name)
  {
    super.setHandler(handler,name);
    if (!(handler instanceof TmgRequestHandler))
    {
      throw new ClassCastException("Attempt to set handler other than "
        + "TmgRequestHandler in TmgAction");
    }
    tmgHandler = (TmgRequestHandler)handler;
    tmgSession = tmgHandler.getTmgSession();
    db = tmgSession.getDb();
  }

  public TmgRequestHandler getTmgHandler()
  {
    return tmgHandler;
  }

  public TmgSession getTmgSession()
  {
    return tmgSession;
  }

  public TmgDb getDb()
  {
    return db;
  }

  /**
   * Generate a tmg_action_log entry using the description and id passed for
   * this action.  An ActionLog is created and inserted into the table.  The
   * generated log object is returned.
   */
  public ActionLog logAction(String description, long id)
  {
    return db.logAction(this,user,description,id);
  }
  public ActionLog logAction(long id)
  {
    return logAction(null,id);
  }
  public ActionLog logAction(String description)
  {
    return logAction(description,-1L);
  }
  public ActionLog logAction()
  {
    return logAction(null);
  }

  protected String getFeedback(long id)
  {
    return getFeedback(String.valueOf(id));
  }

  /**
   * Generic action results (guesses based on action naming conventions).
   * If this doesn't suffice child classes should override.
   */
  public String getActionResult(String actionName)
  {
    String actionResult = null;
    if (actionName.startsWith("edit"))
    {
      actionResult = "updated";
    }
    else if (actionName.startsWith("add"))
    {
      actionResult = "added";
    }
    else if (actionName.startsWith("delete"))
    {
      actionResult = "deleted";
    }
    else if (actionName.startsWith("cancel"))
    {
      actionResult = "canceled";
    }
    else if (actionName.startsWith("uncancel"))
    {
      actionResult = "reactivated";
    }
    return actionResult;
  }

  /**
   * Handle log and redirect logging.  Generates a log description based on the
   * item id and action object description passed in.  At least one of the two
   * must be provided.  Returns the generated log description which should be
   * useable as user feedback text.
   */
  private String larLog(String description, long id)
  {
    if (description == null && id <= 0)
    {
      throw new NullPointerException("No description or id provided for log and"
        + " redirect");
    }
    String logData = 
      description != null ? getFeedback(description) : getFeedback(id);
    if (id > 0)
    {
      logAction(logData,id);
    }
    else
    {
      logAction(logData);
    }
    return logData;
  }

  // log and redirect with action name
  protected void logAndRedirect(String toName, String description, long id)
  {
    redirectWithFeedback(toName,larLog(description,id));
  }
  protected void logAndRedirect(String toName, long id)
  {
    logAndRedirect(toName,null,id);
  }
  protected void logAndRedirect(String toName, String description)
  {
    logAndRedirect(toName,description,0);
  }

  // log and redirect with link
  protected void logAndRedirect(Link toLink, String description, long id)
  {
    redirectWithFeedback(toLink,larLog(description,id));
  }
  protected void logAndRedirect(Link toLink, long id)
  {
    logAndRedirect(toLink,null,id);
  }
  protected void logAndRedirect(Link toLink, String description)
  {
    logAndRedirect(toLink,description,0);
  }

  private void loadNotes(long noteItemId)
  {
    setRequestAttr("noteItemId",String.valueOf(noteItemId));
    NotesListBean bean = new NotesListBean(this);
  }
  private void loadNotes()
  {
    loadNotes(getParmLong("noteItemId"));
  }
  protected void setNoteItemId(long noteItemId)
  {
    loadNotes(noteItemId);
  }

  protected void setContactWhoId(long whoId)
  {
    setRequestAttr("contactWhoId",String.valueOf(whoId));
    setRequestAttr("contacts",db.getContactsForWho(whoId));
  }

  protected void setAddressWhoId(long whoId)
  {
    setRequestAttr("addressWhoId",String.valueOf(whoId));
    setRequestAttr("addresses",db.getAddressesForWho(whoId));
  }

  private void doShowNotes()
  {
    loadNotes();
    doRender(Tmg.VIEW_SHOW_NOTES);
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_SHOW_NOTES))
    {
      doShowNotes();
    }
    else
    {
      super.execute();
    }
  }
}