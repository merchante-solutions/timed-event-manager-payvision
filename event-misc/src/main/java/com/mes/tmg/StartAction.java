package com.mes.tmg;

import org.apache.log4j.Logger;

public class StartAction extends TmgAction
{
  static Logger log = Logger.getLogger(StartAction.class);

  private void doTestAjaxResponse()
  {
    try
    {
      response.getWriter().println("MVC Test Ajax response");
    }
    catch (Exception e)
    {
      log.error("Error in doTestAjaxResponse: " + e);
    }
  }

  private void doTest()
  {
    setRequestAttr("db",db);
    doView(Tmg.VIEW_TEST);
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_TEST))
    {
      doTest();
    }
    else if (name.equals(Tmg.ACTION_TEST_AJAX))
    {
      doTestAjaxResponse();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
