package com.mes.tmg;

import java.util.List;

public class Client extends TmgBean
{
  private long    clientId;
  private String  clientName;
  private List    appTypes;
  private long    hierarchyId;

  public void setClientId(long clientId)
  {
    this.clientId = clientId;
  }
  public long getClientId()
  {
    return clientId;
  }

  public void setClientName(String clientName)
  {
    this.clientName = clientName;
  }
  public String getClientName()
  {
    return clientName;
  }

  public void setAppTypes(List appTypes)
  {
    this.appTypes = appTypes;
  }
  public List getAppTypes()
  {
    return appTypes;
  }

  public void setHierarchyId(long hierarchyId)
  {
    this.hierarchyId = hierarchyId;
  }
  public long getHierarchyId()
  {
    return hierarchyId;
  }

  public boolean isMes()
  {
    return clientName.equals("MES");
  }

  public String toString()
  {
    return "Client [ "
      + "id: " + clientId
      + ", name: " + clientName
      + ", hier id: " + hierarchyId
      + " ]";
  }
}