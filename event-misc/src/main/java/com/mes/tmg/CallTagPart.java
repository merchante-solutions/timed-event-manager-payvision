package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class CallTagPart extends TmgBean
{
  private long              ctPartId;
  private long              ctId;
  private long              ordId;
  private String            drCode;
  private Date              createDate;
  private String            userName;
  private String            ctpStatCode;
  private long              ptId;
  private long              partId;
  private String            serialNum;
  private long              recvPtId;
  private long              recvPartId;
  private String            recvSerialNum;
  private Date              recvDate;
  private String            moFlag;
  private double            partPrice;
  private String            ctrCode;

  private DeployReason      deployReason;
  private CallTagPartStatus callTagPartStatus;
  private PartType          partType;
  private PartType          recvPartType;
  private CallTagReason     callTagReason;

  public void setCtPartId(long ctPartId)
  {
    this.ctPartId = ctPartId;
  }
  public long getCtPartId()
  {
    return ctPartId;
  }

  public void setCtId(long ctId)
  {
    this.ctId = ctId;
  }
  public long getCtId()
  {
    return ctId;
  }

  public void setOrdId(long ordId)
  {
    this.ordId = ordId;
  }
  public long getOrdId()
  {
    return ordId;
  }

  public void setDrCode(String drCode)
  {
    this.drCode = drCode;
  }
  public String getDrCode()
  {
    return drCode;
  }

  public void setDeployReason(DeployReason deployReason)
  {
    this.deployReason = deployReason;
  }
  public DeployReason getDeployReason()
  {
    return deployReason;
  }

  public void setCtrCode(String ctrCode)
  {
    this.ctrCode = ctrCode;
  }
  public String getCtrCode()
  {
    return ctrCode;
  }

  public void setCallTagReason(CallTagReason callTagReason)
  {
    this.callTagReason = callTagReason;
  }
  public CallTagReason getCallTagReason()
  {
    return callTagReason;
  }

  public void setCtpStatCode(String ctpStatCode)
  {
    this.ctpStatCode = ctpStatCode;
  }
  public String getCtpStatCode()
  {
    return ctpStatCode;
  }

  public void setCallTagPartStatus(CallTagPartStatus callTagPartStatus)
  {
    this.callTagPartStatus = callTagPartStatus;
  }
  public CallTagPartStatus getCallTagPartStatus()
  {
    return callTagPartStatus;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setPtId(long ptId)
  {
    this.ptId = ptId;
  }
  public long getPtId()
  {
    return ptId;
  }

  public void setPartType(PartType partType)
  {
    this.partType = partType;
  }
  public PartType getPartType()
  {
    return partType;
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }

  public void setRecvPtId(long recvPtId)
  {
    this.recvPtId = recvPtId;
  }
  public long getRecvPtId()
  {
    return recvPtId;
  }

  public void setRecvPartType(PartType recvPartType)
  {
    this.recvPartType = recvPartType;
  }
  public PartType getRecvPartType()
  {
    return recvPartType;
  }

  public void setRecvPartId(long recvPartId)
  {
    this.recvPartId = recvPartId;
  }
  public long getRecvPartId()
  {
    return recvPartId;
  }

  public void setRecvSerialNum(String recvSerialNum)
  {
    this.recvSerialNum = recvSerialNum;
  }
  public String getRecvSerialNum()
  {
    return recvSerialNum;
  }

  public void setRecvDate(Date recvDate)
  {
    this.recvDate = recvDate;
  }
  public Date getRecvDate()
  {
    return recvDate;
  }
  public void setRecvTs(Timestamp recvTs)
  {
    recvDate = Tmg.toDate(recvTs);
  }
  public Date getRecvTs()
  {
    return Tmg.toTimestamp(recvDate);
  }

  public void setMoFlag(String moFlag)
  {
    validateFlag(moFlag,"mo");
    this.moFlag = moFlag;
  }
  public String getMoFlag()
  {
    return flagValue(moFlag);
  }
  public boolean isMerchantOwned()
  {
    return flagBoolean(moFlag);
  }

  public void setPartPrice(double partPrice)
  {
    this.partPrice = partPrice;
  }
  public double getPartPrice()
  {
    return partPrice;
  }

  public boolean isNew()
  {
    return CallTagPartStatus.CTPS_NEW.equals(ctpStatCode);
  }
  public boolean isPending()
  {
    return CallTagPartStatus.CTPS_PENDING.equals(ctpStatCode);
  }
  public boolean isReceived()
  {
    return CallTagPartStatus.CTPS_RECEIVED.equals(ctpStatCode);
  }
  public boolean isFailed()
  {
    return CallTagPartStatus.CTPS_FAILED.equals(ctpStatCode);
  }
  public boolean isCanceled()
  {
    return CallTagPartStatus.CTPS_NEW_CANCELED.equals(ctpStatCode) || 
           CallTagPartStatus.CTPS_PEND_CANCELED.equals(ctpStatCode);
  }
  public boolean wasNew()
  {
    return CallTagPartStatus.CTPS_NEW_CANCELED.equals(ctpStatCode);
  }
  public boolean isCharged()
  {
    return CallTagPartStatus.CTPS_CHARGED.equals(ctpStatCode);
  }

  public boolean canSetNew()
  {
    return isCanceled() && wasNew();
  }
  public boolean canDelete()
  {
    return isNew();
  }
  public boolean canPend()
  {
    return isNew() || (isCanceled() && !wasNew());
  }
  public boolean canCancel()
  {
    return isNew() || isPending();
  }
  public boolean canUncancel()
  {
    return isCanceled();
  }
  public boolean canReceive()
  {
    return isPending();
  }
  public boolean canFail()
  {
    return isPending();
  }
  public boolean canEdit()
  {
    return isNew() || isPending();
  }

  // part is known if it references known deployed part in inventory
  public boolean isKnown()
  {
    return partId != 0L;
  }

  public void setNew()
  {
    if (!canSetNew())
    {
      throw new RuntimeException("Cannot set call tag part to new");
    }
    ctpStatCode = CallTagPartStatus.CTPS_NEW;
  }
  public void pend()
  {
    if (!canPend())
    {
      throw new RuntimeException("Cannot pend call tag part");
    }
    ctpStatCode = CallTagPartStatus.CTPS_PENDING;
  }
  public void cancel()
  {
    if (!canCancel())
    {
      throw new RuntimeException("Cannot cancel call tag part");
    }
    if (isNew())
    {
      ctpStatCode = CallTagPartStatus.CTPS_NEW_CANCELED;
    }
    else
    {
      ctpStatCode = CallTagPartStatus.CTPS_PEND_CANCELED;
    }
  }
  public void uncancel()
  {
    if (!canUncancel())
    {
      throw new RuntimeException("Cannot uncancel call tag part");
    }
    if (wasNew())
    {
      setNew();
    }
    else
    {
      pend();
    }
  }
  public void receive()
  {
    if (!canReceive())
    {
      throw new RuntimeException("Cannot receive call tag part");
    }
    ctpStatCode = CallTagPartStatus.CTPS_RECEIVED;
  }
  public void fail()
  {
    if (!canFail())
    {
      throw new RuntimeException("Cannot fail call tag part");
    }
    ctpStatCode = CallTagPartStatus.CTPS_FAILED;
  }

  public boolean receivedPart(long partId)
  {
    return partId == recvPartId;
  }

  public String toString()
  {
    return "CallTagPart ["
      + "ct part: " + ctPartId
      + ", ct: " + ctId
      + ", ord id: " + ordId
      + ", deploy reason: " + drCode
      + ", part reason: " + ctrCode
      + ", status: " + ctpStatCode
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + ", part type: " + ptId
      + ", part id: " + partId
      + ", serial num: " + serialNum
      + ", merch owned: " + isMerchantOwned()
      + ", price: " + partPrice
      + " ]";
  }
}