package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class Contact extends TmgBean
{
  private long    contId;
  private long    whoId;
  private Date    createDate;
  private String  label;
  private String  firstName;
  private String  lastName;
  private String  email;
  private String  phone;
  private String  fax;

  public Contact()
  {
  }

  public void setContId(long contId)
  {
    this.contId = contId;
  }
  public long getContId()
  {
    return contId;
  }

  public void setWhoId(long whoId)
  {
    this.whoId = whoId;
  }
  public long getWhoId()
  {
    return whoId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setLabel(String label)
  {
    this.label = label;
  }
  public String getLabel()
  {
    return label;
  }

  public String getName()
  {
    StringBuffer buf = new StringBuffer();
    if (firstName != null) buf.append(firstName);
    if (firstName != null && lastName != null) buf.append(" ");
    if (lastName != null) buf.append(lastName);
    return buf.toString();
  }

  public void setFirstName(String firstName)
  {
    this.firstName = firstName;
  }
  public String getFirstName()
  {
    return firstName;
  }

  public void setLastName(String lastName)
  {
    this.lastName = lastName;
  }
  public String getLastName()
  {
    return lastName;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }
  public String getEmail()
  {
    return email;
  }

  public void setPhone(String phone)
  {
    this.phone = phone;
  }
  public String getPhone()
  {
    return phone;
  }

  public void setFax(String fax)
  {
    this.fax = fax;
  }
  public String getFax()
  {
    return fax;
  }

  public String renderPlainText()
  {
    StringBuffer buf = new StringBuffer();
    if (firstName != null) buf.append(firstName);
    if (firstName != null && lastName != null) buf.append(" ");
    if (lastName != null) buf.append(lastName);
    if (email != null) buf.append("\nemail " + email);
    if (phone != null) buf.append("\nphone " + phone);
    if (fax != null) buf.append("\nfax " + fax);
    return buf.toString();
  }

  public String toString()
  {
    return "Contact [ "
      + "id: " + contId
      + ", who id: " + whoId
      + ", created: " + formatDate(createDate)
      + ", label: " + label
      + ", name: " + firstName + (lastName != null ? " " + lastName : "")
      + ( email != null ? ", email: " + email : "")
      + ( phone != null ? ", phone: " + phone : "")
      + ( fax != null ? ", fax: " + fax : "")
      + " ]";
  }
}
