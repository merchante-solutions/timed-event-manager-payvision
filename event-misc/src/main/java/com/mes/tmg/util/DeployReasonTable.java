package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.DeployReason;
import com.mes.tmg.TmgBean;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tools.DropDownItem;

public class DeployReasonTable extends TmgDropDownTable
{
  // if a flag is not null then the dr item must match it's value
  // so...null allows anything, 'y' only allows 'y', 'n' only allows 'n'
  private String startFlag;
  private String endFlag;

  public DeployReasonTable()
  {
  }
  public DeployReasonTable(String startFlag, String endFlag)
  {
    init(startFlag,endFlag);
  }
  public DeployReasonTable(int options, String startFlag, String endFlag)
  {
    super(options);
    init(startFlag,endFlag);
  }

  private void init(String startFlag, String endFlag)
  {
    if (startFlag != null) TmgBean.validateFlag(startFlag,"start");
    if (endFlag != null) TmgBean.validateFlag(endFlag,"end");
    this.startFlag = startFlag;
    this.endFlag = endFlag;
  }

  public class DrDropDownItem extends DropDownItem
  {
    DeployReason dr;
    public DrDropDownItem(DeployReason dr)
    {
      super(dr.getDrCode(),dr.getDescription());
      this.dr = dr;
    }
    public boolean isEnabled()
    {
      return (startFlag == null || dr.getStartFlag().equals(startFlag)) &&
             (endFlag == null || dr.getEndFlag().equals(endFlag));
    }
  }

  public void loadElements()
  {
    List drs = db.getDeployReasons();
    for (Iterator i = drs.iterator(); i.hasNext(); )
    {
      DeployReason dr = (DeployReason)i.next();
      addElement(new DrDropDownItem(dr));
    }
  }
}
  
