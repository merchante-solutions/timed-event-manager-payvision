package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.EncryptionType;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgDropDownTable;

public class EncryptionTable extends TmgDropDownTable
{
  public EncryptionTable(TmgDb db, int options)
  {
    super(db,options);
  }
  public EncryptionTable(TmgDb db)
  {
    this(db,OPT_SEL_ANY);
  }
  public EncryptionTable(int options)
  {
    super(options);
  }
  public EncryptionTable()
  {
    this(OPT_SEL_ANY);
  }

  public void loadElements()
  {
    List list = db.getEncryptTypes();
    for (Iterator i = list.iterator(); i.hasNext(); )
    {
      EncryptionType et = (EncryptionType)i.next();
      addElement(""+et.getEtId(),et.getEtName());
    }
  }
}