package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.Location;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgDropDownTable;

public class LocationTable extends TmgDropDownTable
{
  public LocationTable(TmgDb db)
  {
    super(db);
  }

  public LocationTable(TmgDb db, int options)
  {
    super(db,options);
  }

  public void loadElements()
  {
    List list = db.getLocations();
    for (Iterator i = list.iterator(); i.hasNext(); )
    {
      Location loc = (Location)i.next();
      addElement(loc.getLocCode(),loc.getLocName());
    }
  }
}