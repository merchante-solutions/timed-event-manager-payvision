package com.mes.tmg.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.tmg.Document;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgTransaction;
import com.mes.user.UserBean;
import oracle.sql.BLOB;

public class DocUploadTransaction extends TmgTransaction
{
  static Logger log = Logger.getLogger(DocUploadTransaction.class);

  private InputStream in;
  private Document doc;

  public DocUploadTransaction(TmgDb db, UserBean user, Document doc, InputStream in)
  {
    super(db,user);
    this.doc = doc;
    this.in = in;
  }

  public int execute()
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      // make sure the document exists in tmg_docs
      if (doc.getDocId() <= 0L)
      {
        doc.setDocId(db.getNewId());
        log.debug("new doc id set " + doc.getDocId());
      }
      if (((TmgDb)db).getDocument(doc.getDocId()) == null)
      {
        db.persist(doc,user);
      }
    
      // set an empty blob in the doc record
      Connection con = db.getConnection();
      ps = con.prepareStatement(
        "update tmg_docs set doc_data = empty_blob() where doc_id = ?");
      ps.setLong(1,doc.getDocId());
      ps.execute();

      // get an output stream to the db blob
      ps = con.prepareStatement(
        "select doc_data from tmg_docs where doc_id = ?");
      ps.setLong(1,doc.getDocId());
      rs = ps.executeQuery();
      if (!rs.next())
      {
        throw new Exception("No result row returned fetching doc data blob");
      }
      BLOB b = (BLOB)rs.getBlob(1);

      // read data from input source, write it out to the blob
      OutputStream out = b.getBinaryOutputStream();

      try
      {
        byte[] buf = new byte[1024 * 10];
        int byteTotal = 0;
        int bufBytes = 0;
        while ((bufBytes = in.read(buf)) != -1)
        {
          out.write(buf,0,bufBytes);
          byteTotal += bufBytes;
        }

        // update the doc size
        doc.setDocSize(byteTotal);
        db.persist(doc,user);
      }
      finally
      {
        out.close();
      }
    }
    catch (Exception e)
    {
      log.error("Error executing document upload: " + e);
      return ROLLBACK;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
    }

    return COMMIT;
  }
}
