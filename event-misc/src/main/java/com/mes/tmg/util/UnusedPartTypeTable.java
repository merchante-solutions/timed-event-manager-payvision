package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.Inventory;
import com.mes.tmg.PartType;
import com.mes.tmg.TmgDb;
import com.mes.tools.DropDownTable;

/**
 * Used in generating a drop down list field containing only those part types that
 * are not already represented in an inventory.
 */
public class UnusedPartTypeTable extends DropDownTable
{
  public UnusedPartTypeTable(TmgDb db, Inventory inventory)
  {
    addElement("","select one");
    List partTypes = db.getUnusedPartTypesForInventory(inventory);
    for (Iterator i = partTypes.iterator(); i.hasNext(); )
    {
      PartType pt = (PartType)i.next();
      addElement(String.valueOf(pt.getPtId()),
        pt.getModelCode() + " - " + pt.getPtName());
    }
  }
}

