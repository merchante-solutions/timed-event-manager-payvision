package com.mes.tmg.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgDropDownTable;

public class PartTypeTable extends TmgDropDownTable
{
  static Logger log = Logger.getLogger(PartTypeTable.class);

  public PartTypeTable(TmgDb db, int options)
  {
    super(db,options);
  }
  public PartTypeTable(TmgDb db)
  {
    this(db,OPT_SEL_ANY);
  }
  public PartTypeTable(int options)
  {
    super(options);
  }
  public PartTypeTable()
  {
    this(OPT_SEL_ANY);
  }

  private static String qs =   

    " select  f.feat_name cat_name,         " +
    "         pt.pt_id,                     " +
    "         pt.model_code,                " +
    "         pt.description                " +
    " from    tmg_part_types pt,            " +
    "         tmg_part_feature_maps fm,     " +
    "         tmg_part_features f           " +
    " where   pt.pt_id = fm.pt_id           " +
    "         and fm.feat_id = f.feat_id    " +
    "         and f.ft_code = 'CATEGORY'    " +
    " order by f.feat_name, pt.description  ";

  public void loadElements()
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      Connection con = db.getConnection();
      ps = con.prepareStatement(qs);
      rs = ps.executeQuery();
      String lastCatName = null;
      int catCount = 0;
      while (rs.next())
      {
        String catName = rs.getString("cat_name");
        String ptId = rs.getString("pt_id");
        String model = rs.getString("model_code");
        String description = rs.getString("description");
        if (lastCatName == null || !lastCatName.equals(catName))
        {
          ++catCount;
          addElement(" ","**** " + catName.toUpperCase() + " ****");
          lastCatName = catName;
        }
        addElement(ptId,"&nbsp;&nbsp;" + description + " - " + model );
      }
    }
    catch (Exception e)
    {
      log.error("Error loading part types: " + e);
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      db.doDisconnect();
    }
  }
}
  
