package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.Client;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgDropDownTable;

public class ClientTable extends TmgDropDownTable
{
  public ClientTable(TmgDb db)
  {
    super(db);
  }
  public ClientTable(TmgDb db, int options)
  {
    super(db,options);
  }

  public void loadElements()
  {
    List clients = db.getClients();
    for (Iterator i = clients.iterator(); i.hasNext(); )
    {
      Client client = (Client)i.next();
      addElement(String.valueOf(client.getClientId()),
        client.getClientName());
    }
  }
}
  
