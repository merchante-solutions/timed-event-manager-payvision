package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.OrderSource;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgDropDownTable;

public class OrderSourceTable extends TmgDropDownTable
{
  public OrderSourceTable(TmgDb db)
  {
    super(db);
  }
  public OrderSourceTable(TmgDb db, int options)
  {
    super(db,options);
  }

  public void loadElements()
  {
    List sources = db.getOrderSources();
    for (Iterator i = sources.iterator(); i.hasNext(); )
    {
      OrderSource source = (OrderSource)i.next();
      addElement(source.getOrdSrcCode(),
        source.getOrdSrcCode() + " - " + source.getDescription());
    }
  }
}
  
