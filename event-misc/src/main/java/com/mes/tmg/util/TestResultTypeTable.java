package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.TestResultType;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgDropDownTable;

public class TestResultTypeTable extends TmgDropDownTable
{
  public TestResultTypeTable(TmgDb db)
  {
    super(db);
  }

  public TestResultTypeTable(TmgDb db, int options)
  {
    super(db,options);
  }

  public void loadElements()
  {
    List list = db.getTestResultTypes();
    for (Iterator i = list.iterator(); i.hasNext(); )
    {
      TestResultType trt = (TestResultType)i.next();
      addElement(trt.getTrtCode(),trt.getTrtName());
    }
  }
}