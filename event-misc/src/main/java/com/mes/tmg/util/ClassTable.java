package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.PartClass;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgDropDownTable;

public class ClassTable extends TmgDropDownTable
{
  public ClassTable(TmgDb db)
  {
    super(db);
  }

  public ClassTable(TmgDb db, int options)
  {
    super(db,options);
  }

  public void loadElements()
  {
    List list = db.getPartClasses();
    for (Iterator i = list.iterator(); i.hasNext(); )
    {
      PartClass pc = (PartClass)i.next();
      addElement(pc.getPcCode(),pc.getPcName());
    }
  }
}