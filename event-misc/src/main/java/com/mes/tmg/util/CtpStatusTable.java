package com.mes.tmg.util;

import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgDropDownTable;

public class CtpStatusTable extends TmgDropDownTable
{
  public CtpStatusTable(TmgDb db)
  {
    super(db);
  }
  public CtpStatusTable(TmgDb db, int options)
  {
    super(db,options);
  }

  public void loadElements()
  {
    // HACK: manually generate elements due to multiple Canceled statuses
    addElement("New","New");
    addElement("Pending","Pending");
    addElement("Received","Received");
    addElement("Not Recovered","Not Recovered");
    addElement("Charged Off","Charged Off");
    addElement("Canceled","Canceled");
  }
}
  
