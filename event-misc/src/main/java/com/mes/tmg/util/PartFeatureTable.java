package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.PartFeature;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tools.DropDownItem;

public class PartFeatureTable extends TmgDropDownTable
{
  private String ftCode;

  public PartFeatureTable()
  {
  }
  public PartFeatureTable(String ftCode)
  {
    this.ftCode = ftCode;
  }
  public PartFeatureTable(int options, String ftCode)
  {
    super(options);
    this.ftCode = ftCode;
  }

  public class FeatureDropDownItem extends DropDownItem
  {
    PartFeature feature;

    public FeatureDropDownItem(PartFeature feature)
    {
      super(String.valueOf(feature.getFeatId()),feature.getFeatName());
      this.feature = feature;
    }
    public boolean isEnabled()
    {
      return ftCode == null || feature.getFtCode().equals(ftCode);
    }
  }

  public void loadElements()
  {
    List features = db.getPartFeatures();
    for (Iterator i = features.iterator(); i.hasNext(); )
    {
      PartFeature feature = (PartFeature)i.next();
      addElement(new FeatureDropDownItem(feature));
    }
  }
}
  
