package com.mes.tmg.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.tmg.Inventory;
import com.mes.tmg.TmgDb;
import com.mes.tools.DropDownTable;

/**
 * Generates drop down list containing an inventory's part type buckets.
 */

public class BucketTable extends DropDownTable
{
  static Logger log = Logger.getLogger(BucketTable.class);

  private static String qs =   

    " select  f.feat_name cat_name,         " +
    "         b.buck_id,                    " +
    "         pt.model_code,                " +
    "         pt.description                " +
    " from    tmg_buckets b,                " +
    "         tmg_part_types pt,            " +
    "         tmg_part_feature_maps fm,     " +
    "         tmg_part_features f           " +
    " where   b.pt_id = pt.pt_id            " +
    "         and pt.pt_id = fm.pt_id       " +
    "         and fm.feat_id = f.feat_id    " +
    "         and f.ft_code = 'CATEGORY'    " +
    "         and b.inv_id = ?              " +
    " order by f.feat_name, pt.description  ";

  private void loadElements(TmgDb db, long invId)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      Connection con = db.getConnection();
      ps = con.prepareStatement(qs);
      ps.setLong(1,invId);
      rs = ps.executeQuery();
      String lastCatName = null;
      int catCount = 0;
      while (rs.next())
      {
        String buckId = rs.getString("buck_id");
        String model = rs.getString("model_code");
        String description = rs.getString("description");
        String catName = rs.getString("cat_name");
        if (lastCatName == null || !lastCatName.equals(catName))
        {
          ++catCount;
          addElement(" ","**** " + catName.toUpperCase() + " ****");
          lastCatName = catName;
        }
        addElement(buckId,"&nbsp;&nbsp;" + description + " - " + model );
      }
    }
    catch (Exception e)
    {
      log.error("Error loading buckets: " + e);
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      db.doDisconnect();
    }
  }
    
  public BucketTable(TmgDb db, Inventory inventory)
  {
    loadElements(db,inventory.getInvId());
  }

  public BucketTable(TmgDb db, long invId)
  {
    loadElements(db,invId);
  }
}

