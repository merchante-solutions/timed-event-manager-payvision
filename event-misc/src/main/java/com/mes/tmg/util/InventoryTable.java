package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.Inventory;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgDropDownTable;

public class InventoryTable extends TmgDropDownTable
{
  public InventoryTable(TmgDb db)
  {
    super(db);
  }
  public InventoryTable(TmgDb db, int options)
  {
    super(db,options);
  }

  public void loadElements()
  {
    List invs = db.getInventories();
    for (Iterator i = invs.iterator(); i.hasNext(); )
    {
      Inventory inv = (Inventory)i.next();
      addElement(String.valueOf(inv.getInvId()),
        inv.getInvName());
    }
  }
}
  
