package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.tmg.Disposition;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tools.DropDownItem;

public class PartDispositionTable extends TmgDropDownTable
{
  static Logger log = Logger.getLogger(PartDispositionTable.class);

  private boolean deployOnly;

  public PartDispositionTable(int options, boolean deployOnly)
  {
    super(options);
    this.deployOnly = deployOnly;
  }
  public PartDispositionTable(boolean deployOnly)
  {
    this.deployOnly = deployOnly;
  }

  public class DispDropDownItem extends DropDownItem
  {
    Disposition disp;
    public DispDropDownItem(Disposition disp)
    {
      super(disp.getDispCode(),null);
      this.disp = disp;
    }
    public String getDescription()
    {
      return deployOnly ? disp.getDeployName() : disp.getDispName();
    }
    public boolean isEnabled()
    {
      return !deployOnly || disp.canDeploy();
    }
  }

  public void loadElements()
  {
    List disps = db.getDispositions();
    for (Iterator i = disps.iterator(); i.hasNext(); )
    {
      Disposition disp = (Disposition)i.next();
      addElement(new DispDropDownItem(disp));
    }
  }
}
