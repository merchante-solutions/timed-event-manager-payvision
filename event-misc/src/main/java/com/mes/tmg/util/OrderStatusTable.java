package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.OrderStatus;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgDropDownTable;

public class OrderStatusTable extends TmgDropDownTable
{
  public OrderStatusTable(TmgDb db)
  {
    super(db);
  }
  public OrderStatusTable(TmgDb db, int options)
  {
    super(db,options);
  }

  public void loadElements()
  {
    List statuses = db.getOrderStatuses();
    for (Iterator i = statuses.iterator(); i.hasNext(); )
    {
      OrderStatus status = (OrderStatus)i.next();
      addElement(status.getOrdStatCode(),status.getDescription());
    }
    addElement("!DONE+!CAN+!PEND","Active");
  }
}
  
