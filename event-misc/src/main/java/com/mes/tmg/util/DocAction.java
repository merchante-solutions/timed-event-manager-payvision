package com.mes.tmg.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.tmg.Document;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;
import oracle.sql.BLOB;

public class DocAction extends TmgAction
{
  static Logger log = Logger.getLogger(DocAction.class);

  public String getActionObject(String actionName)
  {
    return "Document";
  }

  /**
   * Loads document data for a given docId, writes it to the given output
   * stream.
   */
  private static void doDocOutput(TmgDb db, Document doc, OutputStream out)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      log.debug("ouputting " + doc);

      Connection con = db.getConnection();
      ps = con.prepareStatement("select doc_data from tmg_docs where doc_id = ?");
      ps.setLong(1,doc.getDocId());
      rs = ps.executeQuery();

      if (rs.next())
      {
        InputStream in = null;
        try
        {
          BLOB b = (BLOB)rs.getBlob(1);
          in = b.getBinaryStream();

          int curByte = -1;
          while ((curByte = in.read()) != -1)
          {
            out.write(curByte);
          }

          out.flush();
        }
        finally
        {
          try { in.close(); } catch (Exception e) { }
        }
      }
    }
    catch (Exception e)
    {
      log.error("Error outputting doc " + doc + ": " + e);
      e.printStackTrace();
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      db.doDisconnect();
    }
  }

  private void doDownloadDoc()
  {
    try
    {
      Document doc = db.getDocument(getParmLong("docId"));
      response.setContentType("application/binary");
      response.setHeader("Content-Disposition","attachment; filename=\"" 
        + doc.getDocName() + "\";");
      doDocOutput(db,doc,response.getOutputStream());
    }
    catch (Exception e)
    {
      throw new RuntimeException(""+e);
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_DOWNLOAD_DOC))
    {
      doDownloadDoc();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }

  /**
   * Some static utility functions for generating documents in the db and 
   * doing doc file upload transactions.

  /**
   * Create a new document bean object.
   */
  public static Document newDoc(TmgDb     db, 
                                UserBean  user, 
                                String    docName, 
                                String    description, 
                                long      itemId)
  {
    Document doc = new Document();
    doc.setDocId(db.getNewId());
    doc.setCreateDate(Calendar.getInstance().getTime());
    doc.setUserName(user.getLoginName());
    doc.setDescription(description);
    doc.setDocName(docName);
    if (itemId != -1L) doc.setId(itemId);
    return doc;
  }

  /**
   * Create a new document and insert it into the database.
   */
  public static Document insertDoc(TmgDb    db,
                                   UserBean user,
                                   String   actionName,
                                   String   docName,
                                   String   description,
                                   long     itemId)
  {
    Document doc = newDoc(db,user,docName,description,itemId);
    db.persist(doc,user);
    db.logAction(actionName,user,"Created new document '" + doc.getDocName() 
      + "' (" + doc.getDocId() + ")",doc.getDocId());
    return doc;
  }

  /**
   * Upload the contents of an input stream to the specified document in the
   * database.
   */
  public static Document uploadDocFile(TmgDb        db,
                                       Document     doc,
                                       String       actionName,
                                       UserBean     user, 
                                       InputStream  in)
  {
    (new DocUploadTransaction(db,user,doc,in)).run();
    db.logAction(actionName,user,"Uploaded document file '" + doc.getDocName() 
      + "' (" + doc.getDocId() + ", " + doc.getDocSize() + " bytes)",
      doc.getDocId());
    return doc;
  }

  /**
   * Generate a new document in the database and then upload the contents of
   * an input stream to it.
   */
  public static Document uploadNewDocFile(TmgDb       db,
                                          UserBean    user,
                                          String      actionName,
                                          String      docName,
                                          String      description,
                                          long        itemId,
                                          InputStream in)
  {
    Document doc = insertDoc(db,user,actionName,docName,description,itemId);
    return uploadDocFile(db,doc,actionName,user,in);
  }

  /**
   * Associate an id with an existing document.
   */
  public static void attachToDoc(TmgDb    db, 
                                 UserBean user, 
                                 String   actionName,
                                 Document doc,
                                 long     itemId)
  {
    doc.setId(itemId);
    db.persist(doc,user);
    db.logAction(actionName,user,"Document " + doc.getDocId() + " attached to "
      + "item " + itemId,itemId);
  }

  /**
   * Send document data to an output stream.
   */
  public static void outputDoc(TmgDb db, long docId, OutputStream out)
  {
    doDocOutput(db,db.getDocument(docId),out);
  }
}
