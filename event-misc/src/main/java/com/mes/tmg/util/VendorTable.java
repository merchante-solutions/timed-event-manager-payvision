package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.Vendor;

public class VendorTable extends TmgDropDownTable
{
  public VendorTable(TmgDb db)
  {
    super(db);
  }

  public VendorTable(TmgDb db, int options)
  {
    super(db,options);
  }

  public void loadElements()
  {
    List list = db.getVendors();
    for (Iterator i = list.iterator(); i.hasNext(); )
    {
      Vendor ven = (Vendor)i.next();
      addElement(String.valueOf(ven.getVendorId()),ven.getVendorName());
    }
  }
}