package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.Condition;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgDropDownTable;

/**
 * Used in generating a drop down list field containing only those part types that
 * are represented in an inventory.  Conditions may be filtered by vendor flag
 * to allow purchase order conditions to be fetched.
 */

public class ConditionTable extends TmgDropDownTable
{
  public ConditionTable(TmgDb db)
  {
    super(db);
  }

  public ConditionTable(TmgDb db, int options)
  {
    super(db,options);
  }

  public void loadElements()
  {
    List conds = db.getConditions();
    for (Iterator i = conds.iterator(); i.hasNext(); )
    {
      Condition cond = (Condition)i.next();
      addElement(cond.getCondCode(),cond.getCondName());
    }
  }
}

