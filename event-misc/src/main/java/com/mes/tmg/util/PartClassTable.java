package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.PartClass;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tools.DropDownItem;

public class PartClassTable extends TmgDropDownTable
{
  private boolean deployOnly;

  public PartClassTable()
  {
  }
  public PartClassTable(boolean deployOnly)
  {
    this.deployOnly = deployOnly;
  }
  public PartClassTable(int options, boolean deployOnly)
  {
    super(options);
    this.deployOnly = deployOnly;
  }

  public class PcDropDownItem extends DropDownItem
  {
    PartClass partClass;
    public PcDropDownItem(PartClass partClass)
    {
      super(partClass.getPcCode(),partClass.getPcName());
      this.partClass = partClass;
    }
    public boolean isEnabled()
    {
      return !deployOnly || partClass.canDeploy();
    }
  }

  public void loadElements()
  {
    List pcs = db.getPartClasses();
    for (Iterator i = pcs.iterator(); i.hasNext(); )
    {
      PartClass pc = (PartClass)i.next();
      addElement(new PcDropDownItem(pc));
    }
  }
}
  
