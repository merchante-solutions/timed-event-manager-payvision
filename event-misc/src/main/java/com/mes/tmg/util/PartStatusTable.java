package com.mes.tmg.util;

import java.util.Iterator;
import java.util.List;
import com.mes.tmg.PartStatus;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgDropDownTable;

public class PartStatusTable extends TmgDropDownTable
{
  public PartStatusTable(TmgDb db)
  {
    super(db);
  }

  public PartStatusTable(TmgDb db, int options)
  {
    super(db,options);
  }

  public void loadElements()
  {
    List list = db.getPartStatuses();
    for (Iterator i = list.iterator(); i.hasNext(); )
    {
      PartStatus status = (PartStatus)i.next();
      addElement(status.getStatusCode(),status.getStatusName());
    }
  }
}