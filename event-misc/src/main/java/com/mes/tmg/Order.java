package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

public class Order extends TmgBean
{
  static Logger log = Logger.getLogger(Order.class);

  private long        ordId;
  private String      createdBy;
  private Date        createDate;
  private String      workedBy;
  private Date        workDate;
  private long        clientId;
  private Client      client;
  private String      ordSrcCode;
  private OrderSource orderSource;
  private String      ordStatCode;
  private OrderStatus orderStatus;
  private long        addrId;
  private String      merchNum;
  private MerchantRef merchantRef;
  private long        appSeqNum;
  private long        acrId;
  private AcrRef      acrRef;
  private long        shipperId;
  private Shipper     shipper;
  private List        orderOutParts;
  private List        callTags;

  public void setOrdId(long ordId)
  {
    this.ordId = ordId;
  }
  public long getOrdId()
  {
    return ordId;
  }

  public void setOrderOutParts(List orderOutParts)
  {
    this.orderOutParts = orderOutParts;
  }
  public List getOrderOutParts()
  {
    return orderOutParts;
  }

  public void setCallTags(List callTags)
  {
    this.callTags = callTags;
  }
  public List getCallTags()
  {
    return callTags;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setCreatedBy(String createdBy)
  {
    this.createdBy = createdBy;
  }
  public String getCreatedBy()
  {
    return createdBy;
  }

  public void setWorkDate(Date workDate)
  {
    this.workDate = workDate;
  }
  public Date getWorkDate()
  {
    return workDate;
  }
  public void setWorkTs(Timestamp workTs)
  {
    workDate = Tmg.toDate(workTs);
  }
  public Date getWorkTs()
  {
    return Tmg.toTimestamp(workDate);
  }

  public void setWorkedBy(String workedBy)
  {
    this.workedBy = workedBy;
  }
  public String getWorkedBy()
  {
    return workedBy;
  }

  public void setClientId(long clientId)
  {
    this.clientId = clientId;
  }
  public long getClientId()
  {
    return clientId;
  }

  public void setClient(Client client)
  {
    this.client = client;
  }
  public Client getClient()
  {
    return client;
  }

  public void setOrdSrcCode(String ordSrcCode)
  {
    this.ordSrcCode = ordSrcCode;
  }
  public String getOrdSrcCode()
  {
    return ordSrcCode;
  }

  public void setOrderSource(OrderSource orderSource)
  {
    this.orderSource = orderSource;
  }
  public OrderSource getOrderSource()
  {
    return orderSource;
  }

  public void setOrdStatCode(String ordStatCode)
  {
    this.ordStatCode = ordStatCode;
  }
  public String getOrdStatCode()
  {
    return ordStatCode;
  }

  public void setOrderStatus(OrderStatus orderStatus)
  {
    this.orderStatus = orderStatus;
  }
  public OrderStatus getOrderStatus()
  {
    return orderStatus;
  }

  public void setAddrId(long addrId)
  {
    this.addrId = addrId;
  }
  public long getAddrId()
  {
    return addrId;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setAppSeqNum(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
  }
  public long getAppSeqNum()
  {
    return appSeqNum;
  }

  public void setAcrId(long acrId)
  {
    this.acrId = acrId;
  }
  public long getAcrId()
  {
    return acrId;
  }

  public void setMerchantRef(MerchantRef merchantRef)
  {
    this.merchantRef = merchantRef;
  }
  public MerchantRef getMerchantRef()
  {
    return merchantRef;
  }

  public void setAcrRef(AcrRef acrRef)
  {
    this.acrRef = acrRef;
  }
  public AcrRef getAcrRef()
  {
    return acrRef;
  }

  public void setShipperId(long shipperId)
  {
    this.shipperId = shipperId;
  }
  public long getShipperId()
  {
    return shipperId;
  }

  public void setShipper(Shipper shipper)
  {
    this.shipper = shipper;
  }
  public Shipper getShipper()
  {
    return shipper;
  }

  public Inventory getDeployToInv()
  {
    return merchantRef.getOlaRef().getCdRule().getDeployToInv();
  }

  public Inventory getPullFromInv()
  {
    return merchantRef.getOlaRef().getCdRule().getPullFromInv();
  }

  /**
   * Actual business logic
   */

  public class CallTagComp implements Comparator
  {
    public int compare(Object o1, Object o2)
    {
      CallTag c1 = (CallTag)o1;
      CallTag c2 = (CallTag)o2;
      if (c1.getCtId() < c2.getCtId()) return -1;
      if (c1.getCtId() > c2.getCtId()) return 1;
      return 0;
    }
  }

  public CallTag getCurrentCallTag()
  {
    if (callTags != null && !callTags.isEmpty())
    {
      return (CallTag)Collections.max(callTags,new CallTagComp());
    }
    return null;
  }

  public boolean hasCallTag()
  {
    return !callTags.isEmpty();
  }

  public boolean hasOutParts()
  {
    return !getOrderOutParts().isEmpty();
  }

  public boolean hasLinkToPart(Part part)
  {
    for (Iterator i = getOrderOutParts().iterator(); i.hasNext();)
    {
      OrderOutPart oop = (OrderOutPart)i.next();
      if (oop.hasLinkToPart(part))
      {
        return true;
      }
    }
    return false;
  }

  public boolean hasUnlinkedOutPart()
  {
    for (Iterator i = orderOutParts.iterator(); i.hasNext();)
    {
      OrderOutPart oop = (OrderOutPart)i.next();
      if (!oop.isCanceled() && oop.isUnlinked())
      {
        return true;
      }
    }
    return false;
  }

  public boolean hasEmptyCallTag()
  {
    CallTag ct = getCurrentCallTag();
    return ct != null && !ct.hasUncanceledParts();
  }

  public boolean isNew()
  {
    return  OrderStatus.OSC_NEW.equals(ordStatCode);
  }
  public boolean isWorking()
  {
    return  OrderStatus.OSC_WORKING.equals(ordStatCode);
  }
  public boolean isPending()
  {
    return  OrderStatus.OSC_PENDING.equals(ordStatCode);
  }
  public boolean isShipped()
  {
    return  OrderStatus.OSC_SHIPPED.equals(ordStatCode);
  }
  public boolean isCompleted() 
  {
    return  OrderStatus.OSC_COMPLETED.equals(ordStatCode);
  }
  public boolean isCanceled()
  {
    return  OrderStatus.OSC_CANCELED.equals(ordStatCode);
  }

  // order is actively being worked prior to being shipped
  public boolean isActive()
  {
    return isNew() || isWorking();
  }

  // active or has shipped with call tag but is not complete
  public boolean isTagActive()
  {
    return isActive() || isShipped();
  }

  // is new, pending or canceled
  public boolean canWork()
  {
    return isNew() || isPending() || isCanceled();
  }

  // is new, working or pending
  // and call tag (if it exists) can be canceled
  public boolean canCancel()
  {
    return (isNew() || isWorking() || isPending())
      && (!hasCallTag() || getCurrentCallTag().canCancel() 
          || getCurrentCallTag().isCanceled());
  }

  public boolean canUncancel()
  {
    return isCanceled();
  }

  // new or working
  public boolean canPend()
  {
    return isNew() || isWorking();
  }

  public boolean canUnpend()
  {
    return isPending();
  }

  // no unlinked out parts and no call tag or call tag already shipped
  public boolean canComplete()
  {
    // must have all outgoing parts assigned serial number from inventory
    if (hasUnlinkedOutPart())
    {
      return false;
    }

    // if call tag exists it must be closed to complete order
    if (hasCallTag())
    {
      CallTag callTag = getCurrentCallTag();
      if (!callTag.isClosed() && !callTag.isCanceled())
      {
        return false;
      }
      // if closed call tag exists, allow order completion
      // if it is working or has been shipped
      return isWorking() || isShipped();
    }

    // no call tag, only valid status to complete from is working
    return isWorking();
  }

  // ship call tag and out parts: is working, all out parts linked, 
  // call can ship or is shipped
  public boolean canShip()
  {
    CallTag ct = getCurrentCallTag();
    return  isWorking() && !hasUnlinkedOutPart() 
      && (ct != null && (ct.isShipped() || ct.canShip()));
  }

  public void work()
  {
    if (!canWork())
    {
      throw new RuntimeException("Cannot set order status to working: " + this);
    }
    ordStatCode = OrderStatus.OSC_WORKING;
  }

  public void updateWorkedBy(String loginName)
  {
    if (isNew())
    {
      work();
    }
    setWorkedBy(loginName);
    setWorkDate(Calendar.getInstance().getTime());
  }

  public void pend()
  {
    if (!canPend())
    {
      throw new RuntimeException("Order cannot be pended");
    }
    ordStatCode = OrderStatus.OSC_PENDING;
  }

  public void ship()
  {
    if (!canShip())
    {
      throw new RuntimeException("Order with call tag cannot be shipped");
    }
    ordStatCode = OrderStatus.OSC_SHIPPED;
  }

  public void complete()
  {
    if (!canComplete())
    {
      throw new RuntimeException("Order cannot be completed: " + this);
    }
    ordStatCode = OrderStatus.OSC_COMPLETED;
  }

  public boolean receivedPart(long partId)
  {
    for (Iterator i = callTags.iterator(); i.hasNext();)
    {
      CallTag ct = (CallTag)i.next();
      if (ct.receivedPart(partId))
      {
        return true;
      }
    }
    return false;
  }

  public String toString()
  {
    return "Order ["
      + " id: " + ordId
      + ", created: " + formatDate(createDate)
      + ", creator: " + createdBy
      + ( workDate != null ? ", worked: " + formatDate(workDate) 
          + ", worker: " + workedBy : "" )
      + ", source: " + ordSrcCode
      + ", stat: " + ordStatCode
      + ", addr: " + addrId
      + ", merch num: " + merchNum
      + ", app: " + appSeqNum
      + ", acr: " + acrId
      + ", " + client
      + (merchantRef != null ? ", " + merchantRef.getOlaRef().getCdRule() : "")
      + " ]";
  }
}