package com.mes.tmg.pdf;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.lowagie.text.Chunk;
import com.lowagie.text.Element;

public class PdfTemplateContainer extends PdfTemplateItem
{
  protected List items = new ArrayList();
  protected List fields = new ArrayList();

  public void addItem(PdfTemplateItem item)
  {
    items.add(item);
    item.setParent(this);
  }

  public void addField(PdfTemplateField docField)
  {
    addItem(docField);
    fields.add(docField);
  }

  public List getItems()
  {
    return items;
  }

  public List getAllItems()
  {
    List allItems = new ArrayList();
    for (Iterator i = items.iterator(); i.hasNext();)
    {
      PdfTemplateItem item = (PdfTemplateItem)i.next();
      allItems.add(item);
      if (item instanceof PdfTemplateContainer)
      {
        allItems.addAll(((PdfTemplateContainer)item).getAllItems());
      }
    }
    return allItems;
  }

  public List getFields()
  {
    return fields;
  }

  public List getAllFields()
  {
    List allFields = new ArrayList();
    for (Iterator i = items.iterator(); i.hasNext();)
    {
      PdfTemplateItem item = (PdfTemplateItem)i.next();
      if (item instanceof PdfTemplateField)
      {
        allFields.add(item);
      }
      else if (item instanceof PdfTemplateContainer)
      {
        allFields.addAll(((PdfTemplateContainer)item).getAllFields());
      }
    }
    return allFields;
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    if (!items.isEmpty())
    {
      buf.append("<" + getTagContents() + ">\n");
      for (Iterator i = items.iterator(); i.hasNext();)
      {
        PdfTemplateItem item = (PdfTemplateItem)i.next();
        buf.append(item.toString());
      }
      buf.append("</" + getName() + ">\n");
    }
    else
    {
      buf.append("<" + getTagContents() + " />\n");
    }
    return buf.toString();
  }

  // default container behavior is to transparently generate
  // a list of all contained elements
  public List getPdfElements()
  {
    List els = new ArrayList();
    Element lastEl = null;
    for (Iterator i = items.iterator(); i.hasNext();)
    {
      PdfTemplateItem item = (PdfTemplateItem)i.next();
      if (item instanceof PdfTemplateContainer)
      {
        List l = ((PdfTemplateContainer)item).getPdfElements();
        Element el = (Element)l.get(0);
        if (el != null && !el.toString().endsWith("\n") && lastEl != null && 
            lastEl instanceof Chunk && !lastEl.toString().endsWith("\n"))
        {
          els.add(new Chunk(' '));
        }
        els.addAll(l);
        lastEl = null;
      }
      else
      {
        Element el = item.toPdfElement();

        // add a space chunk to separate adjacent 
        // chunks that aren't line breaks
        if (!el.toString().endsWith("\n") && lastEl != null && 
            lastEl instanceof Chunk && !lastEl.toString().endsWith("\n"))
        {
          els.add(new Chunk(' '));
        }

        els.add(el);
        lastEl = el;
      }
    }
    return els;
  }
}
