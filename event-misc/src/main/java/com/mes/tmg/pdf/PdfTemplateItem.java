package com.mes.tmg.pdf;

import com.lowagie.text.Element;
import com.lowagie.text.Font;

public class PdfTemplateItem
{
  private PdfTemplateItem parent;
  private Font font;

  public void setParent(PdfTemplateItem parent)
  {
    this.parent = parent;
  }

  public String getName()
  {
    throw new NullPointerException();
  }

  public String getTagParameters()
  {
    return "";
  }

  public String getTagContents()
  {
    StringBuffer buf = new StringBuffer(getTagParameters());
    if (buf.length() > 0) buf.insert(0," ");
    buf.insert(0,getName());
    return buf.toString();
  }

  public String toString()
  {
    return "<" + getTagContents() + " />\n";
  }

  public PdfTemplateItem getParent()
  {
    return parent;
  }

  public boolean isBold()
  {
    return parent != null ? parent.isBold() : false;
  }

  public int getFontStyle()
  {
    return isBold() ? Font.BOLD : Font.NORMAL;
  }

  public float getFontSize()
  {
    return parent != null ? parent.getFontSize() : Font.DEFAULTSIZE;
  }

  public String getFontFamily()
  {
    return parent != null ? parent.getFontFamily() : "Times New Roman";
  }

  public Font getPdfFont()
  {
    if (font == null)
    {
      font = new Font();
      font.setFamily(getFontFamily());
      font.setSize(getFontSize());
      font.setStyle(getFontStyle());
    }
    return font;
  }

  public boolean isCentered()
  {
    return parent != null ? parent.isCentered() : false;
  }

  public int getAlignment()
  {
    return isCentered() ? Element.ALIGN_CENTER : Element.ALIGN_LEFT;
  }

  public String processData(String data)
  {
    return parent != null ? parent.processData(data) : data;
  }

  public Element toPdfElement()
  {
    throw new NullPointerException(
      "PdfTemplateItem child class must override toPdfElement()");
  }
}

