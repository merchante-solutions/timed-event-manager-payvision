package com.mes.tmg.pdf;

public class PdfTemplateBold extends PdfTemplateContainer
{
  public String getName()
  {
    return "bold";
  }

  public boolean isBold()
  {
    return true;
  }
}
