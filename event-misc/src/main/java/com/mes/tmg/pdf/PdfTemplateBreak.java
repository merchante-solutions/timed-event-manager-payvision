package com.mes.tmg.pdf;

import com.lowagie.text.Chunk;
import com.lowagie.text.Element;
import com.lowagie.text.Font;

public class PdfTemplateBreak extends PdfTemplateItem
{
  public String getName()
  {
    return "br";
  }

  public Element toPdfElement()
  {
    Font font = new Font(getPdfFont());
    font.setColor(255,0,0);
    return new Chunk("\n",font);
  }
}
