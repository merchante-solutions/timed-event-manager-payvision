package com.mes.tmg.pdf;

import java.util.Iterator;
import org.apache.log4j.Logger;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;

public class PdfTemplateBody extends PdfTemplateContainer
{
  static Logger log = Logger.getLogger(PdfTemplateBody.class);

  private float             fontSize;
  private String            fontFamily;
  private PdfDataProcessor  dp;

  public PdfTemplateBody(float fontSize, String fontFamily)
  {
    this.fontSize = fontSize;
    this.fontFamily = fontFamily;
  }

  public void setDataProcessor(PdfDataProcessor dp)
  {
    this.dp = dp;
  }
  public PdfDataProcessor getDataProcessor()
  {
    return dp;
  }

  public float getFontSize()
  {
    return fontSize;
  }

  public String getFontFamily()
  {
    return fontFamily;
  }

  public Font getFont()
  {
    Font font = new Font();
    font.setSize(fontSize);
    font.setFamily(fontFamily);
    return font;
  }

  public String getName()
  {
    return "body";
  }

  public String getTagParameters()
  {
    StringBuffer buf = new StringBuffer();
    if (fontFamily != null)
    {
      buf.append(" font-family='" + fontFamily + "'");
    }
    if (fontSize != 0)
    {
      buf.append(" font-size='" + fontSize + "'");
    }
    return buf.toString();
  }

  public String processData(String data)
  {
    if (dp != null)
    {
      return dp.processData(data);
    }
    return data;
  }

  public void renderPdfData(Document doc)
  {
    try
    {
      // an automaticly generated paragraph holder for
      // elements that aren't explicitly wrapped in a paragraph
      Paragraph autoP = null;
      Element lastEl = null;

      for (Iterator i = getPdfElements().iterator(); i.hasNext();)
      {
        Element el = (Element)i.next();

        // if a raw non-paragraph is encountered, 
        // put it in the auto paragraph
        if (!(el instanceof Paragraph))
        {
          // make a new auto paragraph if one isn't currently
          // available
          if (autoP == null)
          {
            autoP = new Paragraph();
            autoP.setLeading(getFontSize() + 1);
            autoP.add(el);
          }
          else
          {
            autoP.add(el);
          }
        }
        // element is a paragraph, add it directly to document
        else
        {
          // add any prior content contained in the auto paragraph
          if (autoP != null)
          {
            doc.add(autoP);
            autoP = null;
          }
          doc.add(el);
        }
        lastEl = el;
      }

      // add the auto paragraph if it exists
      if (autoP != null)
      {
        doc.add(autoP);
      }
    }
    catch (Exception e)
    {
      log.error("Error rendering pdf data: " + e);
      e.printStackTrace();
    }
  }
}
