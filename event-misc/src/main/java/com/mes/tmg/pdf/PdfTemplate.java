package com.mes.tmg.pdf;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.log4j.Logger;
import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfWriter;

public class PdfTemplate
{
  static Logger log = Logger.getLogger(PdfTemplate.class);

  protected PdfTemplateBody body;

  public PdfTemplate()
  {
  }

  public void setBody(PdfTemplateBody body)
  {
    this.body = body;
    PdfDataProcessor dp = getDataProcessor();
    if (dp != null)
    {
      body.setDataProcessor(dp);
    }
  }

  public PdfDataProcessor getDataProcessor()
  {
    return null;
  }

  public java.util.List getFields()
  {
    java.util.List fields = new ArrayList();
    for (Iterator i = body.getAllFields().iterator(); i.hasNext();)
    {
      PdfTemplateField df = (PdfTemplateField)i.next();
      fields.add(df.getField());
    }
    return fields;
  }

  protected void addHeader(Document doc) throws Exception
  {
  }

  protected void addFooter(Document doc) throws Exception
  {
  }

  public byte[] toPdfBytes()
  {
    try
    {
      // create a pdf document in a buffer
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      Document doc = new Document(PageSize.A4,50,50,50,50);
      PdfWriter.getInstance(doc,baos);
      doc.open();

      // add header
      addHeader(doc);

      // render the body
      body.renderPdfData(doc);

      // add footer
      addFooter(doc);

      // done, return pdf data as byte array
      doc.close();

      // convert pdf data to byte array
      return baos.toByteArray();
    }
    catch (Exception e)
    {
      log.error("Error generating PDF data: " + e);
      e.printStackTrace();
    }

    return null;
  }

  // TODO: add static method(s) to send data out

  public String toString()
  {
    return body.toString();
  }
}
