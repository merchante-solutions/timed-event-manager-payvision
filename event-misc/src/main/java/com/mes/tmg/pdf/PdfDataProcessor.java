package com.mes.tmg.pdf;

public interface PdfDataProcessor
{
  public String processData(String data);
}