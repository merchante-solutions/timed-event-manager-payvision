package com.mes.tmg.pdf;

import com.lowagie.text.Chunk;
import com.lowagie.text.Element;

public class PdfTemplateText extends PdfTemplateItem
{
  private String text;

  public PdfTemplateText(String text)
  {
    this.text = text;
  }

  public String getName()
  {
    return "text";
  }

  public String toString()
  {
    return text + "\n";
  }

  public Element toPdfElement()
  {
    return new Chunk(text,getPdfFont());
  }
}

