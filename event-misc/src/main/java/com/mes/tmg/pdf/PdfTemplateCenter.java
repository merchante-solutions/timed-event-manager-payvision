package com.mes.tmg.pdf;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.lowagie.text.Paragraph;

public class PdfTemplateCenter extends PdfTemplateContainer
{
  public String getName()
  {
    return "center";
  }

  public boolean isCentered()
  {
    return true;
  }

  // centering generates a paragraph wrapper in a list by itself...
  public List getPdfElements()
  {
    Paragraph p = new Paragraph();
    p.setAlignment(p.ALIGN_CENTER);
    p.setLeading(getFontSize() + 1);
    for (Iterator i = super.getPdfElements().iterator(); i.hasNext();)
    {
      p.add(i.next());
    }
    List els = new ArrayList();
    els.add(p);
    return els;
  }
}
