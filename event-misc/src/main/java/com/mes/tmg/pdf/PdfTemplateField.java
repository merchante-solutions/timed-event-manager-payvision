package com.mes.tmg.pdf;

import org.apache.log4j.Logger;
import com.lowagie.text.Chunk;
import com.lowagie.text.Element;
import com.mes.forms.Field;
import com.mes.forms.TextareaField;

public class PdfTemplateField extends PdfTemplateItem
{
  static Logger log = Logger.getLogger(PdfTemplateField.class);

  private String fieldName;
  private String label;
  private String dfltValue;
  private String data;

  private Field field;

  public String getName()
  {
    return "field";
  }

  public void setFieldName(String fieldName)
  {
    this.fieldName = fieldName;
  }
  public String getFieldName()
  {
    return fieldName;
  }

  public void setLabel(String label)
  {
    this.label = label;
  }
  public String getLabel()
  {
    return label;
  }

  public void setData(String data)
  {
    this.data = data;
  }
  public String getData()
  {
    if (data != null)
    {
      return processData(data);
    }
    else if (dfltValue != null)
    {
      return processData(dfltValue);
    }
    return null;
  }

  public void setDfltValue(String dfltValue)
  {
    this.dfltValue = dfltValue;
  }
  public String getDfltValue()
  {
    return dfltValue;
  }

  public Field getField()
  {
    log.debug("getting field " + getLabel());
    if (field == null)
    {
      log.debug("creating field " + getLabel());
      field = new TextareaField(fieldName,label,2000,4,80,true);

      // get the processed version of the data to present to user
      // (it may be dynamic symbol data)
      String fieldData = getData();
      log.debug("field data is " + fieldData);
      if (fieldData != null)
      {
        field.setData(fieldData);
      }
    }
    return field;
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("<field name=\"" + getFieldName() + "\"");
    buf.append(" label=\"" + getLabel() + "\"");
    if (dfltValue != null)
    {
      buf.append(" default=\"" + getDfltValue() + "\"");
    }

    // field may be dynamic, but user may wish to override it
    // need to retain the dynamic symbol if user has not overriden it
    // (i.e. processed data equals the field data)
    // if user has overriden it then just store whatever they 
    // provided as field data and forget about the dynamic symbol
    String procData = getData();
    String fieldData = getField().getData();
    if (fieldData.length() > 0 && !fieldData.equals(procData))
    {
      // user overrides the dynamic symbol (if there was one)
      buf.append(">" + fieldData + "</field>");
    }
    else if (data.length() > 0)
    {
      // store the raw data to retain the dynamic symbol if it is there
      buf.append(">" + data + "</field>");
    }
    else
    {
      buf.append(" />");
    }
    buf.append("\n");
    return buf.toString();
  }

  public Element toPdfElement()
  {
    return new Chunk(getData(),getPdfFont());
  }
}
