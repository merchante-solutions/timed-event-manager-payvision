package com.mes.tmg.pdf;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.util.Iterator;
import javax.servlet.ServletContext;
import org.apache.log4j.Logger;
import org.jdom.Content;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Text;
import org.jdom.input.SAXBuilder;

/**

 Useage:
 
 <body font-size='12' font-family='Helvetica'>

   <center>to center justify use center tags</center>

   centered text creates its own paragraph (with leading space)

   <bold>bold text can appear inside centered text and vice versa</bold>

   to insert a manual carriage return in text use <br />

   to create dynamic text that is tied to a text area form field use:
   
   <field name='fieldName' label='Field Label' default='default field data' />

 </body>

 TODO:

   Revise to something like:

   <body font-size font-family>

     <p justify='left|right|center' font-family font-size font-weight='normal|bold'>

     <font font-family font-size font-weight='normal|bold'>

     <field name label default />

     <image url top left />

     <br />

     <pg />
*/

public class PdfTemplateParser
{
  static Logger log = Logger.getLogger(PdfTemplateParser.class);

  private String parseErrorMsg;

  private ServletContext  sc;

  public PdfTemplateParser()
  {
  }

  public PdfTemplateParser(ServletContext sc)
  {
    this.sc = sc;
  }

  public String getParseErrorMsg()
  {
    return parseErrorMsg;
  }

  private PdfTemplateField loadField(Element el)
  {
    PdfTemplateField df = new PdfTemplateField();
    df.setFieldName(el.getAttribute("name").getValue());
    df.setLabel(el.getAttribute("label").getValue());
    if (el.getAttribute("default") != null)
    {
      df.setDfltValue(el.getAttribute("default").getValue());
    }
    if (el.getText().length() > 0)
    {
      df.setData(el.getText());
    }
    else
    {
      df.setData(df.getDfltValue());
    }
    return df;
  }

  private void loadContainer(PdfTemplateContainer container, Element parent)
  {
    for (Iterator i = parent.getContent().iterator(); i.hasNext();)
    {
      Content content = (Content)i.next();
      if (content instanceof Text)
      {
        Text t = (Text)content;
        String filtered = t.getText().replaceAll("[\\s]+"," ").trim();
        if (filtered.length() > 0)
        {
          container.addItem(new PdfTemplateText(filtered));
        }
      }
      else if (content instanceof Element)
      {
        Element child = (Element)content;
        String childName = child.getName();
        if (childName.equals("field"))
        {
          container.addField(loadField(child));
        }
        else if (childName.equals("br"))
        {
          container.addItem(new PdfTemplateBreak());
        }
        else if (childName.equals("bold"))
        {
          PdfTemplateBold db = new PdfTemplateBold();
          loadContainer(db,child);
          container.addItem(db);
        }
        else if (childName.equals("center"))
        {
          PdfTemplateCenter dc = new PdfTemplateCenter();
          loadContainer(dc,child);
          container.addItem(dc);
        }
        else
        {
          throw new RuntimeException("Invalid doc element name: '" 
            + childName + "'");
        }
      }
      else
      {
        log.debug("ignoring content: " + content);
      }
    }
  }

  private PdfTemplateBody loadPdfTemplateBody(Document doc)
  {
    PdfTemplateBody body = null;

    try
    {
      Element root = doc.getRootElement();
      String fontFamily = root.getAttribute("font-family").getValue();
      float fontSize 
        = Float.parseFloat(root.getAttribute("font-size").getValue());
      body = new PdfTemplateBody(fontSize,fontFamily);
      loadContainer(body,root);
    }
    catch (Exception e)
    {
      throw new RuntimeException(e);
    }

    return body;
  }

  private PdfTemplate parseXml(Reader xmlIn, PdfTemplate pt)
  {
    parseErrorMsg = null;

    try
    {
      SAXBuilder builder 
       = new SAXBuilder("org.apache.xerces.parsers.SAXParser",true);
      org.jdom.Document doc = builder.build(xmlIn);
      PdfTemplateBody body = loadPdfTemplateBody(doc);
      pt = pt == null ? new PdfTemplate() : pt;
      pt.setBody(body);
      log.info("PDF doc template data is valid");
      return pt;
    }
    catch (Exception e)
    {
      log.error("Error parsing xml: " + e);
      e.printStackTrace();
      parseErrorMsg = e.toString();
    }

    return null;
  }
  
  public PdfTemplate parseXmlFile(File xmlFile, PdfTemplate pt)
  {
    try
    {
      return parseXml(new FileReader(xmlFile),pt);
    }
    catch (Exception e)
    {
      log.error("Error parsing xml file: " + e);
    }

    return null;
  }
  public PdfTemplate parseXmlFile(File xmlFile)
  {
    return parseXmlFile(xmlFile,null);
  }

  public PdfTemplate parseXmlData(String xmlData, PdfTemplate pt, URL dtdUrl)
  {
    try
    {
      StringBuffer xmlBuf = new StringBuffer(xmlData);
      if (!xmlData.startsWith("<?xml version"))
      {
        if (dtdUrl == null)
        {
          throw new NullPointerException("DTD url must be provided"
            + " to parse xml data without xml version and doctype tags");
        }

        // implied at top of internally stored xml:
        //  <?xml version="1.0"?>
        //  <!DOCTYPE body SYSTEM "/WEB-INF/pdf-template.dtd">
        xmlBuf.insert(0,"<!DOCTYPE body SYSTEM \"" + dtdUrl + "\">\n");
        xmlBuf.insert(0,"<?xml version=\"1.0\"?>\n");
      }
      return parseXml(new StringReader(xmlBuf.toString()),pt);
    }
    catch (Exception e)
    {
      log.error("Error parsing xml data: " + e);
      e.printStackTrace();
    }

    return null;
  }                                     
  public PdfTemplate parseXmlData(String xmlData, PdfTemplate pt)
  {
    try
    {
      URL dtdUrl = null;
      if (sc != null)
      {
        dtdUrl = sc.getResource("/WEB-INF/pdf-template.dtd");
      }
      return parseXmlData(xmlData,pt,dtdUrl);
    }
    catch (Exception e)
    {
      log.error("Error parsing xml data: " + e);
      e.printStackTrace();
    }

    return null;
  }
  public PdfTemplate parseXmlData(String xmlData)
  {
    return parseXmlData(xmlData,null);
  }
}