package com.mes.tmg;

public class PartOpCode
{
  private String  opCode;
  private String  opName;

  public void setOpCode(String opCode)
  {
    this.opCode = opCode;
  }
  public String getOpCode()
  {
    return opCode;
  }

  public void setOpName(String opName)
  {
    this.opName = opName;
  }
  public String getOpName()
  {
    return opName;
  }

  public String toString()
  {
    return "PartOpCode [ "
      + "code: " + opCode
      + ", name: " + opName
      + " ]";
  }
}