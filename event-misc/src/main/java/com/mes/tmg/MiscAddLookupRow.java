package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class MiscAddLookupRow extends TmgBean
{
  private long maId;
  private Date createDate;
  private String invName;
  private String userName;
  private long maPartId;
  private long partId;
  private String psSerialNum;
  private String maSerialNum;
  private long quantity;
  private String modelCode;
  private String description;
  private String pcCode;
  private String sourceCode;
  private float partPrice;

  public void setMaId(long maId)
  {
    this.maId = maId;
  }
  public long getMaId()
  {
    return maId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setInvName(String invName)
  {
    this.invName = invName;
  }
  public String getInvName()
  {
    return invName;
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setMaPartId(long maPartId)
  {
    this.maPartId = maPartId;
  }
  public long getMaPartId()
  {
    return maPartId;
  }

  public void setMaSerialNum(String maSerialNum)
  {
    this.maSerialNum = maSerialNum;
  }
  public String getMaSerialNum()
  {
    return maSerialNum;
  }

  public void setPsSerialNum(String psSerialNum)
  {
    this.psSerialNum = psSerialNum;
  }
  public String getPsSerialNum()
  {
    return psSerialNum;
  }

  public void setQuantity(long quantity)
  {
    this.quantity = quantity;
  }
  public long getQuantity()
  {
    return quantity;
  }

  public void setModelCode(String modelCode)
  {
    this.modelCode = modelCode;
  }
  public String getModelCode()
  { 
    return modelCode;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }

  public void setPcCode(String pcCode)
  {
    this.pcCode = pcCode;
  }
  public String getPcCode()
  {
    return pcCode;
  }

  public void setSourceCode(String sourceCode)
  {
    this.sourceCode = sourceCode;
  }
  public String getSourceCode()
  {
    return sourceCode;
  }

  public void setPartPrice(float partPrice)
  {
    this.partPrice = partPrice;
  }
  public float getPartPrice()
  {
    return partPrice;
  }
}

