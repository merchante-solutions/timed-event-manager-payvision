package com.mes.tmg;

import java.util.List;
import org.apache.log4j.Logger;

public class Vendor
{
  static Logger log = Logger.getLogger(Vendor.class);

  private long    vendorId;
  private String  vendorName;
  private String  encryptFlag;
  private String  customerId;
  private List    addresses;
  private List    contacts;
  private List    vendorShipTypes;

  public void setVendorId(long vendorId)
  {
    this.vendorId = vendorId;
  }
  public long getVendorId()
  {
    return vendorId;
  }
  
  public void setVendorName(String vendorName)
  {
    this.vendorName = vendorName;
  }
  
  public String getVendorName()
  {
    return vendorName;
  }
  
  public void setEncryptFlag(String encryptFlag)
  {
    if (!encryptFlag.equals("y") && !encryptFlag.equals("n"))
    {
      throw new RuntimeException("Invalid encrypt flag value: '" + encryptFlag + "'");
    }
    this.encryptFlag = encryptFlag;
  }
  public String getEncryptFlag()
  {
    return encryptFlag != null ? encryptFlag : "n";
  }
  public boolean doesEncryption()
  {
    return encryptFlag.equals("y");
  }

  public void setCustomerId(String customerId)
  {
    this.customerId = customerId;
  }
  public String getCustomerId()
  {
    return customerId;
  }

  public void setAddresses(List addresses)
  {
    this.addresses = addresses;
  }
  public List getAddresses()
  {
    return addresses;
  }

  public void setContacts(List contacts)
  {
    this.contacts = contacts;
  }
  public List getContacts()
  {
    return contacts;
  }

  public void setVendorShipTypes(List vendorShipTypes)
  {
    this.vendorShipTypes = vendorShipTypes;
  }
  public List getVendorShipTypes()
  {
    return vendorShipTypes;
  }

  public String toString()
  {
    return "Vendor [ "
      + "id: " + vendorId 
      + ", name: " + vendorName 
      + ", can encrypt: " + encryptFlag
      + (customerId != null ? ", customer id: " + customerId : "")
      + " ]";
  }
}