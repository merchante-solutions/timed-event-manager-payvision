package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class PartState extends Part implements Comparable
{
  private long              stateId;
  private long              priorId;
  private long              opId;
  private Date              startDate;
  private Date              endDate;
  private Part              part;
  private PartOperation     partOp;

  public PartState()
  {
    part = new Part();
  }

  public int compareTo(Object other)
  {
    PartState that = (PartState)other;
    if (this.stateId < that.stateId) return -1;
    if (this.stateId > that.stateId) return 1;
    return 0;
  }

  public boolean equals(Object other)
  {
    if (other instanceof PartState)
    {
      PartState that = (PartState)other;
      return this.stateId == that.stateId;
    }
    return false;
  }

  public boolean stateEquals(Part part)
  {
    return part.stateEquals(this.part);
  }


  /**
   * Constructor creates a state based on a part.  Sets state data as though
   * this were the first state for this part (start date now, no end date,
   * no prior id).
   */
  public PartState(long stateId, PartOperation op, Part part)
  {
    this.stateId = stateId;
    opId = op.getOpId();
    startDate = Calendar.getInstance().getTime();
    this.part = part.clonePart();
  }

  /**
   * Creates a new current state for a part.  The new state is created using
   * the current time as a start date and the new id given as it's state id.
   * The part's state is copied into the new state.  This state's end date
   * is set to the new state's start date.  The new state's prior state id
   * is set to this state's state id.  Returns the new state.
   */
  public PartState createNewState(long newId, PartOperation op, Part part)
  {
    // create a new part state using the op code, part data and new id
    PartState newState = new PartState(newId,op,part);

    // set new state prior id to this state id
    newState.setPriorId(stateId);

    // set the end date/start date of new state making sure 
    // the end date is greater than start date of this state
    endDate = Calendar.getInstance().getTime();
    if (endDate.getTime() == startDate.getTime())
    {
      endDate.setTime(endDate.getTime() + 1);
    }
    newState.setStartDate(endDate);
    return newState;
  }

  /**
   * Used to increment the count in a non-sn type part.  Marks this state as
   * finished and generates a new state based on this state but with the 
   * new state count incremented by count in the given part.
   */
  public PartState addCount(long newId, PartOperation op, Part countPart)
  {
    PartState newState = new PartState(newId,op,part);
    newState.setPriorId(stateId);

    // set the end date/start date of new state making sure 
    // the end date is greater than start date of this state
    endDate = Calendar.getInstance().getTime();
    if (endDate.getTime() == startDate.getTime())
    {
      endDate.setTime(endDate.getTime() + 1);
    }
    newState.setStartDate(endDate);

    // increment the new states count by amount in part
    newState.setCount(getCount() + countPart.getCount());
    return newState;
  }
    
  /**
   * Used to decrement the count in a non-sn type part.  Marks this state as
   * finished and generates a new state based on this state but with the 
   * new state count decremented by count in the given part.  May throw
   * RuntimeException if the current count is less than the count in the
   * part to decrement.
   */
  public PartState subtractCount(long newId, PartOperation op, Part countPart)
  {
    // make sure the count to subtract is not greater than the current count
    if (countPart.getCount() > getCount())
    {
      throw new RuntimeException("Attempt to remove " + part.getCount() 
        + " from part state count but only " + getCount() 
        + " exist for part: " + part);
    }

    PartState newState = new PartState(newId,op,part);
    newState.setPriorId(stateId);

    // set the end date/start date of new state making sure 
    // the end date is greater than start date of this state
    endDate = Calendar.getInstance().getTime();
    if (endDate.getTime() == startDate.getTime())
    {
      endDate.setTime(endDate.getTime() + 1);
    }
    newState.setStartDate(endDate);

    // increment the new states count by amount in part
    newState.setCount(getCount() - countPart.getCount());
    return newState;
  }
    
  /**
   * Copies a part's state.
   */
  public void setState(Part part)
  {
    this.part = part.clonePart();
  }

  /**
   * Generate a part object with this state's information.
   */
  public Part getPart()
  {
    return part;
  }

  public long getStateId()
  {
    return stateId;
  }
  public void setStateId(long stateId)
  {
    this.stateId = stateId;
  }

  public long getPriorId()
  {
    return priorId;
  }
  public void setPriorId(long priorId)
  {
    this.priorId = priorId;
  }

  public long getOpId()
  {
    return opId;
  }
  public void setOpId(long opId)
  {
    this.opId = opId;
  }

  public PartOperation getPartOp()
  {
    return partOp;
  }
  public void setPartOp(PartOperation partOp)
  {
    this.partOp = partOp;
  }

  public Date getStartDate()
  {
    return startDate;
  }
  public void setStartDate(Date startDate)
  {
    this.startDate = startDate;
  }
  public Date getStartTs()
  {
    return Tmg.toTimestamp(startDate);
  }
  public void setStartTs(Timestamp startTs)
  {
    startDate = Tmg.toDate(startTs);
  }

  public Date getEndDate()
  {
    return endDate;
  }
  public void setEndDate(Date endDate)
  {
    this.endDate = endDate;
  }
  public Timestamp getEndTs()
  {
    return Tmg.toTimestamp(endDate);
  }
  public void setEndTs(Timestamp endTs)
  {
    endDate = Tmg.toDate(endTs); 
  }

  public long           getPartId()             { return part.getPartId(); }
  public long           getBuckId()             { return part.getBuckId(); }
  public Bucket         getBucket()             { return part.getBucket(); }
  public Inventory      getInventory()          { return part.getInventory(); }
  public Client         getClient()             { return part.getClient(); }
  public long           getPtId()               { return part.getPtId(); }
  public PartType       getPartType()           { return part.getPartType(); }
  public String         getSerialNum()          { return part.getSerialNum(); }
  public int            getCount()              { return part.getCount(); }
  public String         getPcCode()             { return part.getPcCode(); }
  public PartClass      getPartClass()          { return part.getPartClass(); }
  public String         getOrigPcCode()         { return part.getOrigPcCode(); }
  public PartClass      getOrigPartClass()      { return part.getOrigPartClass(); }
  public String         getLocCode()            { return part.getLocCode(); }
  public Location       getLocation()           { return part.getLocation(); }
  public String         getStatusCode()         { return part.getStatusCode(); }
  public PartStatus     getPartStatus()         { return part.getPartStatus(); }
  public String         getCondCode()           { return part.getCondCode(); }
  public Condition      getCondition()          { return part.getCondition(); }
  public String         getDispCode()           { return part.getDispCode(); }
  public Disposition    getDisposition()        { return part.getDisposition(); }
  public long           getEtId()               { return part.getEtId(); }
  public EncryptionType getEncryptionType()     { return part.getEncryptionType(); }
  public String         getLostFlag()           { return part.getLostFlag(); }
  public String         getOwnerMerchNum()      { return part.getOwnerMerchNum(); }
  public String         getLeaseFlag()          { return part.getLeaseFlag(); }
  public long           getPoItemId()           { return part.getPoItemId(); }
  public long           getPoPartId()           { return part.getPoPartId(); }
  public Date           getReceiveDate()        { return part.getReceiveDate(); }
  public Timestamp      getReceiveTs()          { return part.getReceiveTs(); }
  public Timestamp      getMaReceiveTs()        { return part.getMaReceiveTs(); }
  public long           getPoId()               { return part.getPoId(); }
  public long           getDeployId()           { return part.getDeployId(); }
  public Deployment     getDeployment()         { return part.getDeployment(); }
  public long           getMaId()               { return part.getMaId(); }
  public long           getMaPartId()           { return part.getMaPartId(); }
  public PartPrice      getPartPrice()          { return part.getPartPrice(); }
  public long           getTestId()             { return part.getTestId(); }
  public PartTest       getPartTest()           { return part.getPartTest(); }

  public void setPartId(long partId)            { part.setPartId(partId); }
  public void setBuckId(long buckId)            { part.setBuckId(buckId); }
  public void setBucket(Bucket bucket)          { part.setBucket(bucket); }
  public void setInventory(Inventory inventory) { part.setInventory(inventory); }
  public void setClient(Client client)          { part.setClient(client); }
  public void setPtId(long ptId)                { part.setPtId(ptId); }
  public void setPartType(PartType partType)    { part.setPartType(partType); }
  public void setSerialNum(String serialNum)    { part.setSerialNum(serialNum); }
  public void setCount(int count)               { part.setCount(count); }
  public void setPcCode(String pcCode)          { part.setPcCode(pcCode); }
  public void setPartClass(PartClass partClass) { part.setPartClass(partClass); }
  public void setOrigPcCode(String origPcCode)  { part.setOrigPcCode(origPcCode); }
  public void setOrigPartClass(PartClass origPartClass) 
                                                { part.setOrigPartClass(origPartClass); }
  public void setLocCode(String locCode)        { part.setLocCode(locCode); }
  public void setLocation(Location location)    { part.setLocation(location); }
  public void setStatusCode(String statusCode)  { part.setStatusCode(statusCode); }
  public void setPartStatus(PartStatus partStatus)
                                                { part.setPartStatus(partStatus); }
  public void setCondCode(String condCode)      { part.setCondCode(condCode); }
  public void setCondition(Condition condition) { part.setCondition(condition); }
  public void setDispCode(String dispCode)      { part.setDispCode(dispCode); }
  public void setDisposition(Disposition disposition) 
                                                { part.setDisposition(disposition); }
  public void setEtId(long etId)                { part.setEtId(etId); }
  public void setEncryptionType(EncryptionType encryptionType) 
                                                { part.setEncryptionType(encryptionType); }
  public void setLostFlag(String lostFlag)      { part.setLostFlag(lostFlag); }
  public void setOwnerMerchNum(String ownerMerchNum)
                                                { part.setOwnerMerchNum(ownerMerchNum); }
  public void setLeaseFlag(String leaseFlag)    { part.setLeaseFlag(leaseFlag); }
  public void setPoItemId(long poItemId)        { part.setPoItemId(poItemId); }
  public void setPoPartId(long poPartId)        { part.setPoPartId(poPartId); }
  public void setReceiveTs(Timestamp receiveTs) { part.setReceiveTs(receiveTs); }
  public void setMaReceiveTs(Timestamp maReceiveTs) 
                                                { part.setMaReceiveTs(maReceiveTs); }
  public void setPoId(long poId)                { part.setPoId(poId); }
  public void setDeployId(long deployId)        { part.setDeployId(deployId); }
  public void setDeployment(Deployment deployment)
                                                { part.setDeployment(deployment); }
  public void setMaId(long maId)                { part.setMaId(maId); }
  public void setMaPartId(long maPartId)        { part.setMaPartId(maPartId); }
  public void setPartPrice(PartPrice partPrice) { part.setPartPrice(partPrice); }
  public void setTestId(long testId)            { part.setTestId(testId); }
  public void setPartTest(PartTest partTest)    { part.setPartTest(partTest); }

  public boolean isMerchantOwned()              { return part.isMerchantOwned(); }
  public boolean isLeased()                     { return part.isLeased(); }
  public boolean isLost()                       { return part.isLost(); }
  public boolean isObsolete()                   { return part.isObsolete(); }
  public boolean isSoldAsIs()                   { return part.isSoldAsIs(); }
  public boolean canObsolete()                  { return part.canObsolete(); }
  public boolean canSellAsIs()                  { return part.canSellAsIs(); }
  public boolean canLose()                      { return part.canLose(); }
  public boolean isDeployed()                   { return part.isDeployed(); }
  public boolean isAvailable()                  { return part.isAvailable(); }
  public boolean inTesting()                    { return part.inTesting(); }
  public boolean inNeedOfRepair()               { return part.inNeedOfRepair(); }
  public boolean isMerchantOriginated()         { return part.isMerchantOriginated(); }
  public boolean canChangeDisposition()         { return part.canChangeDisposition(); }

  public boolean canCancelDeployment()
  {
    return isDeployed() && 
           (partOp.getOpCode().equals(Tmg.OP_PART_DEPLOY) ||
            partOp.getOpCode().equals(Tmg.OP_PART_XFER));
  }

  public String toString()
  {
    return "PartState [ "
      + "state: " + stateId
      + ", prior: " + priorId
      + ", op: " + opId
      + ", dates: " + formatDate(startDate)
      + " to " + formatDate(endDate)
      + ", part: " + part
      + " ]";
  }
}
