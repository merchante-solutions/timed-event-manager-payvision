package com.mes.tmg;

public class OrderStatus extends TmgBean
{
  public static final String OSC_NEW        = "NEW";
  public static final String OSC_WORKING    = "WORK";
  public static final String OSC_CANCELED   = "CAN";
  public static final String OSC_COMPLETED  = "DONE";
  public static final String OSC_PENDING    = "PEND";
  public static final String OSC_SHIPPED    = "SHIP";

  private long ordStatId;
  private String ordStatCode;
  private String description;
  private int orderIdx;

  public void setOrdStatId(long ordStatId)
  {
    this.ordStatId = ordStatId;
  }
  public long getOrdStatId()
  {
    return ordStatId;
  }

  public void setOrdStatCode(String ordStatCode)
  {
    this.ordStatCode = ordStatCode;
  }
  public String getOrdStatCode()
  {
    return ordStatCode;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public void setOrderIdx(int orderIdx)
  {
    this.orderIdx = orderIdx;
  }
  public int getOrderIdx()
  {
    return orderIdx;
  }

  public String toString()
  {
    return "OrderStatus ["
      + " id: " + ordStatId
      + ", code: " + ordStatCode
      + ", desc: " + description
      + " ]";
  }
}