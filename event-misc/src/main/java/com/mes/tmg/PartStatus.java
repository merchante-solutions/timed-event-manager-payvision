package com.mes.tmg;

public class PartStatus
{
  private long    statusId;
  private String  statusCode;
  private String  statusName;

  public void setStatusId(long statusId)
  {
    this.statusId = statusId;
  }
  public long getStatusId()
  {
    return statusId;
  }
  
  public void setStatusCode(String statusCode)
  {
    this.statusCode = statusCode;
  }
  public String getStatusCode()
  {
    return statusCode;
  }

  public void setStatusName(String statusName)
  {
    this.statusName = statusName;
  }
  public String getStatusName()
  {
    return statusName;
  }

  public String toString()
  {
    return "PartStatus [ "
      + "id: " + statusId 
      + ", code: " + statusCode
      + ", name: " + statusName 
      + " ]";
  }
}