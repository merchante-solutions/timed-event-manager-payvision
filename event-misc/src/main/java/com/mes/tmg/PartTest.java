package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class PartTest extends TmgBean
{
  private long            testId;
  private Date            createDate;
  private String          userName;
  private String          serialNum;
  private long            partId;
  private long            stateId;
  private long            ctPartId;
  private long            maPartId;
  private long            repairItemId;
  private String          trtCode;
  private String          testDetails;

  private TestResultType  testResultType;

  public void setTestId(long testId)
  {
    this.testId = testId;
  }
  public long getTestId()
  {
    return testId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }

  public void setStateId(long stateId)
  {
    this.stateId = stateId;
  }
  public long getStateId()
  {
    return stateId;
  }

  public void setCtPartId(long ctPartId)
  {
    this.ctPartId = ctPartId;
  }
  public long getCtPartId()
  {
    return ctPartId;
  }

  public void setMaPartId(long maPartId)
  {
    this.maPartId = maPartId;
  }
  public long getMaPartId()
  {
    return maPartId;
  }

  public void setTrtCode(String trtCode)
  {
    this.trtCode = trtCode;
  }
  public String getTrtCode()
  {
    return trtCode;
  }

  public void setTestResultType(TestResultType testResultType)
  {
    this.testResultType = testResultType;
  }
  public TestResultType getTestResultType()
  {
    return testResultType;
  }

  public void setTestDetails(String testDetails)
  {
    this.testDetails = testDetails;
  }
  public String getTestDetails()
  {
    return testDetails;
  }

  public void setRepairItemId(long repairItemId)
  {
    this.repairItemId = repairItemId;
  }
  public long getRepairItemId()
  {
    return repairItemId;
  }

  public String toString()
  {
    return "PartTest ["
      + " id: " + testId
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + (serialNum != null ? ", s/n: " + serialNum : "")
      + ", state id: " + stateId
      + ", part id: " + partId
      + ", ct part: " + ctPartId
      + ", ma part: " + maPartId
      + ", rpr itm: " + repairItemId
      + (testResultType != null ? ", type: " + testResultType.getTrtName() : "")
      + ", details: " + testDetails
      + " ]";
  }
}