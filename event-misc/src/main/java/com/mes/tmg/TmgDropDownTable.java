package com.mes.tmg;

import org.apache.log4j.Logger;
import com.mes.tools.DropDownItem;
import com.mes.tools.DropDownTable;

public class TmgDropDownTable extends DropDownTable
{
  static Logger log = Logger.getLogger(TmgDropDownTable.class);

  public static final int OPT_NONE      = 0;
  public static final int OPT_SEL_ONE   = 0x01;
  public static final int OPT_SEL_ANY   = 0x02;
  public static final int OPT_SEL_NONE  = 0x04;

  protected TmgDb db;
  protected int options;

  private DropDownItem anyItem      = new DropDownItem("any","any");
  private DropDownItem selOneItem   = new DropDownItem("","select one");
  private DropDownItem selNoneItem  = new DropDownItem("","none");

  public TmgDropDownTable(TmgDb db, int options)
  {
    this.db = db;
    this.options = options;
    load();
  }
  public TmgDropDownTable(TmgDb db)
  {
    this(db,OPT_SEL_ONE);
  }
  public TmgDropDownTable()
  {
    this(new TmgDb());
  }
  public TmgDropDownTable(int options)
  {
    this(new TmgDb(),options);
  }

  public void loadOptionElements()
  {
    if ((options & OPT_SEL_ANY) != 0)
    {
      addElement(anyItem);
    }
    if ((options & OPT_SEL_ONE) != 0)
    {
      addElement(selOneItem);
    }
    if ((options & OPT_SEL_NONE) != 0)
    {
      addElement(selNoneItem);
    }
  }

  public void loadElements()
  {
  }

  public void load()
  {
    loadOptionElements();
    loadElements();
  }

  public void setAny(String value, String description)
  {
    anyItem.setValue(value);
    anyItem.setDescription(description);
  }

  public void setSelOne(String value, String description)
  {
    selOneItem.setValue(value);
    selOneItem.setDescription(description);
  }
}
