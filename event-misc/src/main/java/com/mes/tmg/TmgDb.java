package com.mes.tmg;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import com.mes.mvc.Action;
import com.mes.persist.AndFilter;
import com.mes.persist.CodeFilter;
import com.mes.persist.CompoundFilter;
import com.mes.persist.CustomFilter;
import com.mes.persist.DateFilter;
import com.mes.persist.DateRangeFilter;
import com.mes.persist.Filter;
import com.mes.persist.InFilter;
import com.mes.persist.NullFilter;
import com.mes.persist.NumberFilter;
import com.mes.persist.OrFilter;
import com.mes.persist.Selection;
import com.mes.persist.StringFilter;
import com.mes.persist.mes.MesDb;
import com.mes.tmg.repairs.RepairOrder;
import com.mes.tmg.repairs.RepairOrderItem;
import com.mes.user.UserBean;

public class TmgDb extends MesDb
{
  static Logger log = Logger.getLogger(TmgDb.class);

  public static final String TBL_TEST           = "tmg_test_data";
  public static final String IDX_TEST_KEY       = "idx_test_key";

  public static final String TBL_ALOG           = "tmg_action_log";
  public static final String IDX_ALOG_KEY       = "idx_alog_key";
  public static final String IDX_ALOG_ID        = "idx_alog_id";

  public static final String TBL_NOTE           = "tmg_notes";
  public static final String IDX_NOTE_KEY       = "idx_note_key";
  public static final String IDX_NOTE_ID        = "idx_note_id";

  public static final String TBL_DOC            = "tmg_docs";
  public static final String IDX_DOC_KEY        = "idx_doc_key";
  public static final String IDX_DOC_ID         = "idx_doc_id";

  public static final String TBL_FT             = "tmg_part_feature_types";
  public static final String IDX_FT_KEY         = "idx_ft_key";

  public static final String TBL_FEAT           = "tmg_part_features";
  public static final String IDX_FEAT_KEY       = "idx_feat_key";
  public static final String IDX_FEAT_FT_CODE   = "idx_feat_ft_code";

  public static final String TBL_FM             = "tmg_part_feature_maps";
  public static final String IDX_FM_KEY         = "idx_fm_key";
  public static final String IDX_FM_PT_ID       = "idx_fm_pt_id";
  public static final String IDX_FM_FEAT_ID     = "idx_fm_feat_id";

  public static final String TBL_PT             = "tmg_part_types";
  public static final String IDX_PT_KEY         = "idx_pt_key";

  public static final String TBL_COND           = "tmg_part_conditions";
  public static final String IDX_COND_KEY       = "idx_cond_key";
  public static final String IDX_COND_CODE      = "idx_cond_code";

  public static final String TBL_DISP           = "tmg_part_dispositions";
  public static final String IDX_DISP_KEY       = "idx_disp_key";
  public static final String IDX_DISP_CODE      = "idx_disp_code";

  public static final String TBL_LOC            = "tmg_part_locations";
  public static final String IDX_LOC_KEY        = "idx_loc_key";
  public static final String IDX_LOC_CODE       = "idx_loc_code";

  public static final String TBL_STAT           = "tmg_part_statuses";
  public static final String IDX_STAT_KEY       = "idx_stat_key";
  public static final String IDX_STAT_CODE      = "idx_stat_code";

  public static final String TBL_PC             = "tmg_part_classes";
  public static final String IDX_PC_KEY         = "idx_pc_key";
  public static final String IDX_PC_CODE        = "idx_pc_code";

  public static final String TBL_ET             = "tmg_encrypt_types";
  public static final String IDX_ET_KEY         = "idx_et_key";
  public static final String IDX_ET_CODE        = "idx_et_code";

  public static final String TBL_PS             = "tmg_part_states";
  public static final String IDX_PS_KEY         = "idx_ps_key";
  public static final String IDX_PS_PART_ID     = "idx_ps_part_id";
  public static final String IDX_PS_BUCK_ID     = "idx_ps_buck_id";
  public static final String IDX_PS_INV_ID      = "idx_ps_inv_id";
  public static final String IDX_PS_PT_ID       = "idx_ps_pt_id";
  public static final String IDX_PS_SERIAL_NUM  = "idx_ps_serial_num";
  public static final String IDX_PS_PC_CODE     = "idx_ps_pc_code";
  public static final String IDX_PS_LOC_CODE    = "idx_ps_loc_code";
  public static final String IDX_PS_STATUS_CODE = "idx_ps_status_code";
  public static final String IDX_PS_COND_CODE   = "idx_ps_cond_code";
  public static final String IDX_PS_DISP_CODE   = "idx_ps_disp_code";
  public static final String IDX_PS_ET_ID       = "idx_ps_et_id";
  public static final String IDX_PS_LOST_FLAG   = "idx_ps_lost_flag";
  public static final String IDX_PS_MO_FLAG     = "idx_ps_mo_flag";
  public static final String IDX_PS_LEASE_FLAG  = "idx_ps_lease_flag";
  public static final String IDX_PS_MA_ID       = "idx_ps_ma_id";
  public static final String IDX_PS_PART_ID_END_TS = "idx_ps_part_id_end_ts";
  public static final String IDX_PS_MERGE_TEST  = "idx_ps_merge_test";

  public static final String TBL_CLI            = "tmg_clients";
  public static final String IDX_CLI_KEY        = "idx_cli_key";

  public static final String TBL_CLAT           = "tmg_client_app_types";
  public static final String IDX_CLAT_CLIENT_ID = "idx_clat_client_id";
  public static final String IDX_CLAT_APP_TYPE  = "idx_clat_app_type";

  public static final String TBL_INV            = "tmg_inventories";
  public static final String IDX_INV_KEY        = "idx_inv_key";

  public static final String TBL_BUCK           = "tmg_buckets";
  public static final String IDX_BUCK_KEY       = "idx_buck_key";
  public static final String IDX_BUCK_INV_ID    = "idx_buck_inv_id";
  public static final String IDX_BUCK_PT_ID     = "idx_buck_pt_id";

  public static final String TBL_PO             = "tmg_pos";
  public static final String IDX_PO_KEY         = "idx_po_key";
  public static final String IDX_PO_INV_ID      = "idx_po_inv_id";
  public static final String IDX_PO_STATUS      = "idx_po_status";
  public static final String IDX_PO_VND_ID      = "idx_po_vnd_id";
  public static final String IDX_PO_INV_STAT    = "idx_po_inv_stat";

  public static final String TBL_POI            = "tmg_po_items";
  public static final String IDX_POI_KEY        = "idx_poi_key";
  public static final String IDX_POI_PO_ID      = "idx_poi_po_id";

  public static final String TBL_POP            = "tmg_po_parts";
  public static final String IDX_POP_KEY        = "idx_pop_key";
  public static final String IDX_POP_PO_ID      = "idx_pop_po_id";
  public static final String IDX_POP_PO_ITEM_ID = "idx_pop_po_item_id";

  public static final String TBL_OC             = "tmg_op_codes";
  public static final String IDX_OC_KEY         = "idx_oc_key";

  public static final String TBL_OPS            = "tmg_op_log";
  public static final String IDX_OPS_KEY        = "idx_ops_key";
  public static final String IDX_OPS_TRAN       = "idx_ops_tran";

  public static final String TBL_SHPR           = "tmg_shippers";
  public static final String IDX_SHPR_KEY       = "idx_shpr_key";

  public static final String TBL_SHPT           = "tmg_ship_types";
  public static final String IDX_SHPT_KEY       = "idx_shpt_key";
  public static final String IDX_SHPT_SHPR_ID   = "idx_shpt_shpr_id";

  public static final String TBL_VND            = "tmg_vendors";
  public static final String IDX_VND_KEY        = "idx_vnd_key";

  public static final String TBL_VPC            = "tmg_vendor_part_classes";
  public static final String IDX_VPC_KEY        = "idx_vpc_key";
  public static final String IDX_VPC_CODE       = "idx_vpc_code";
  public static final String IDX_VPC_VEND_ID    = "idx_vpc_vend_id";

  public static final String TBL_VPT            = "tmg_vendor_parts";
  public static final String IDX_VPT_KEY        = "idx_vpt_key";

  public static final String TBL_VST            = "tmg_vendor_ship_types";
  public static final String IDX_VST_KEY        = "idx_vst_key";
  public static final String IDX_VST_VND_ID     = "idx_vst_vnd_id";
  public static final String IDX_VST_ST_ID      = "idx_vst_st_id";

  public static final String TBL_ADDR           = "tmg_addresses";
  public static final String IDX_ADDR_KEY       = "idx_addr_key";
  public static final String IDX_ADDR_WHO_ID    = "idx_addr_who_id";
  public static final String IDX_ADDR_SOLO_WHO_ID = "idx_addr_solo_who_id";

  public static final String TBL_CONT           = "tmg_contacts";
  public static final String IDX_CONT_KEY       = "idx_cont_key";
  public static final String IDX_CONT_WHO_ID    = "idx_cont_who_id";
  public static final String IDX_CONT_SOLO_WHO_ID = "idx_cont_solo_who_id";

  public static final String TBL_AUD            = "tmg_audits";
  public static final String IDX_AUD_KEY        = "idx_aud_key";
  public static final String IDX_AUD_CLIENT_ID  = "idx_aud_client_id";

  public static final String TBL_AUDEX          = "tmg_audit_recs";
  public static final String IDX_AUDEX_KEY      = "idx_audex_key";
  public static final String IDX_AUDEX_AUDIT_ID = "idx_audex_audit_id";
  public static final String IDX_AUDEX_SN       = "idx_audex_sn";

  public static final String TBL_MA             = "tmg_misc_adds";
  public static final String IDX_MA_KEY         = "idx_ma_key";

  public static final String TBL_MAP            = "tmg_misc_add_parts";
  public static final String IDX_MAP_KEY        = "idx_map_key";

  public static final String TBL_XFER           = "tmg_transfers";
  public static final String IDX_XFER_KEY       = "idx_xfer_key";
  public static final String IDX_XFER_FROM_INV_ID = "idx_xfer_from_inv_id";
  public static final String IDX_XFER_TO_INV_ID = "idx_xfer_to_inv_id";

  public static final String TBL_MREF           = "merchant";
  public static final String IDX_MREF_KEY       = "idx_mref_key";
  public static final String IDX_MREF_MERCH_NUMBER = "idx_mref_merch_number";

  public static final String TBL_OS             = "tmg_order_sources";
  public static final String IDX_OS_KEY         = "idx_os_key";
  public static final String IDX_OS_ORD_SRC_CODE = "idx_os_ord_src_code";

  public static final String TBL_DR             = "tmg_deploy_reasons";
  public static final String IDX_DR_KEY         = "idx_dr_key";
  public static final String IDX_DR_CODE        = "idx_dr_code";

  public static final String JOIN_NOTE_DOC      = "join_note_doc";
  public static final String JOIN_FEAT_FT       = "join_feat_ft";
  public static final String JOIN_FT_FEAT       = "join_ft_feat";
  public static final String JOIN_FEAT_FM       = "join_feat_fm";
  public static final String JOIN_FM_FEAT       = "join_fm_feat";
  public static final String JOIN_FM_PT         = "join_fm_pt";
  public static final String JOIN_PT_FM         = "join_pt_fm";
  public static final String JOIN_PT_FEAT       = "join_pt_feat";
  public static final String JOIN_PT_FT         = "join_pt_ft";
  public static final String JOIN_INV_PT        = "join_inv_pt";
  public static final String JOIN_BUCK_PT       = "join_buck_pt";
  public static final String JOIN_SHPR_VST      = "join_shpr_vst";
  public static final String JOIN_SHPT_VST      = "join_shpt_vst";
  public static final String JOIN_XFER_PS       = "join_xfer_ps";

  public static final String JOIN_VND_CONT      = "join_vnd_cont";

  public void persist(Object bean, UserBean user)
  {
    super.persist(bean,user);
  }

  private List selectAll(Class beanClass)
  {
    return selectAll(getSelection(beanClass));
  }

  private List selectAll(Object bean, String indexName)
  {
    Selection sel = getSelection(bean,false);
    sel.addIndex(indexName);
    return selectAll(sel);
  }

  private Object select(Object bean)
  {
    return select(getSelection(bean));
  }

  private Object select(Object bean, String indexName)
  {
    Selection sel = getSelection(bean,false);
    sel.addIndex(indexName);
    return select(sel);
  }


  private void deleteBean(Object bean, UserBean user)
  {
    Selection sel = getSelection(bean);
    delete(sel,user);
  }

  private void deleteByIndex(Object bean, String keyIdxName, UserBean user)
  {
    Selection sel = getSelection(bean,false);
    sel.addIndex(keyIdxName);
    delete(sel,user);
  }

  /**************************************************************************
   *
   *  TestBean
   *
   *************************************************************************/

  public TestBean getTestBean(long id)
  {
    TestBean tb = new TestBean();
    tb.setId(id);
    return (TestBean)select(tb);
  }

  public List getTestBeans()
  {
    return selectAll(TestBean.class);
  }

  public void deleteTestBean(long id, UserBean user)
  {
    TestBean tb = new TestBean();
    tb.setId(id);
    Selection sel = getSelection(tb);
    sel.addIndex(IDX_TEST_KEY);
    delete(sel,user);
  }

  /**************************************************************************
   *
   *  ActionLog
   *
   *************************************************************************/

  public ActionLog getActionLog(long actLogId)
  {
    ActionLog log = new ActionLog();
    log.setActLogId(actLogId);
    return (ActionLog)select(log);
  }

  public List getActionLogs()
  {
    return selectAll(ActionLog.class);
  }

  public List getActionLogs(long id)
  {
    ActionLog log = new ActionLog();
    log.setId(id);
    return selectAll(log,IDX_ALOG_ID);
  }

  private static String poActLogQs =

    " select    al.act_log_id,                                    " +
    "           al.id,                                            " +
    "           al.create_ts,                                     " +
    "           al.user_name,                                     " +
    "           al.action,                                        " +
    "           decode(ids.type_code,1,'Order',2,'Item',3,'Part') " +
    "                   || ': ' || al.description description     " +
    " from      tmg_action_log al,                                " +
    "         ( select ? id, 1 type_code from dual                " +
    "           union                                             " +
    "           select po_item_id id, 2 type_code                 " +
    "           from   tmg_po_items                               " +
    "           where  po_id = ?                                  " +
    "           union                                             " +
    "           select po_part_id id, 3 type_code                 " +
    "           from   tmg_po_parts                               " +
    "           where  po_id = ? ) ids                            " +
    " where     al.id = ids.id                                    " +
    " order by  al.create_ts desc                                 ";

  private List getActionLogsFromStatement(PreparedStatement ps) throws SQLException
  {
    ResultSet rs = null;

    try
    {
      rs = ps.executeQuery();
      List actLogs = new ArrayList();
      while (rs.next())
      {
        ActionLog actLog = new ActionLog();
        actLog.setActLogId    (rs.getLong     ("act_log_id"));
        actLog.setId          (rs.getLong     ("id"));
        actLog.setCreateTs    (rs.getTimestamp("create_ts"));
        actLog.setUserName    (rs.getString   ("user_name"));
        actLog.setAction      (rs.getString   ("action"));
        actLog.setDescription (rs.getString   ("description"));
        actLogs.add(actLog);
      }
      return actLogs;
    }
    catch (SQLException e)
    {
      log.error("Error action logs from statement: " + e);
      throw e;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
    }
  }

  public List getPoActionLogs(long poId)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      StringBuffer qs = new StringBuffer(poActLogQs);
      ps = con.prepareStatement(qs.toString());
      ps.setLong(1,poId);
      ps.setLong(2,poId);
      ps.setLong(3,poId);
      return getActionLogsFromStatement(ps);
    }
    catch (Exception e)
    {
      log.error("Error loading action logs for purchase order " + poId + ": " + e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }

    return null;
  }

  public ActionLog logAction(String actionName, UserBean user, String description,
    long itemId)
  {
    //this.log.debug("logAction "+actionName+", "+user.getLoginName()+", "+description+", "+itemId);
    ActionLog log = new ActionLog();
    log.setCreateDate(Calendar.getInstance().getTime());
    log.setUserName(user.getLoginName());
    log.setAction(actionName);
    log.setDescription(description);
    log.setActLogId(getNewId());
    if (itemId != -1L)
    {
      log.setId(itemId);
    }
    persist(log,user);
    return log;
  }

  public ActionLog logAction(Action action, UserBean user, String description,
    long itemId)
  {
    return logAction(action.getName(),user,description,itemId);
  }


  /**************************************************************************
   *
   *  Document
   *
   *************************************************************************/

  public static final String docQs =

    " select    d.doc_id,       " +
    "           d.id,           " +
    "           d.create_ts,    " +
    "           d.user_name,    " +
    "           d.description,  " +
    "           d.doc_name      " +
    " from      tmg_docs d      " +
    " where     1 = 1           ";

  public static final String docIdClause      = " and d.doc_id = ? ";
  public static final String docItemIdClause  = " and d.id = ? ";
  public static final String docOrderClause   = " order by d.create_ts ";

  private List getDocumentsFromStatement(PreparedStatement ps) throws SQLException
  {
    ResultSet rs = null;

    try
    {
      rs = ps.executeQuery();
      List docs = new ArrayList();
      while (rs.next())
      {
        Document doc = new Document();
        doc.setDocId       (rs.getLong     ("doc_id"));
        doc.setId          (rs.getLong     ("id"));
        doc.setCreateTs    (rs.getTimestamp("create_ts"));
        doc.setUserName    (rs.getString   ("user_name"));
        doc.setDescription (rs.getString   ("description"));
        doc.setDocName     (rs.getString   ("doc_name"));
        docs.add(doc);
      }
      return docs;
    }
    catch (SQLException e)
    {
      log.error("Error loading documents from statement: " + e);
      throw e;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
    }
  }

  private List getDocuments(long docId, long id)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      StringBuffer qs = new StringBuffer(docQs);
      if (docId != -1L) qs.append(docIdClause);
      if (id != -1L) qs.append(docItemIdClause);
      qs.append(docOrderClause);
      ps = con.prepareStatement(qs.toString());
      int varCnt = 1;
      if (docId != -1L) ps.setLong(varCnt++,docId);
      if (id != -1L) ps.setLong(varCnt++,id);
      return getDocumentsFromStatement(ps);
    }
    catch (Exception e)
    {
      log.error("Error loading documents (doc id " + docId
        + ", item id " + id + "): " + e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }

    return null;
  }

  public Document getDocument(long docId)
  {
    List docs = getDocuments(docId,-1L);
    return docs != null ? (Document)docs.get(0) : null;
  }

  public List getDocumentsWithId(long id)
  {
    return getDocuments(-1L,id);
  }

  public void deleteDocument(long docId, UserBean user)
  {
    Document doc = new Document();
    doc.setDocId(docId);
    deleteBean(doc,user);
  }


  /**************************************************************************
   *
   *  Note
   *
   *************************************************************************/

  public Note getNote(long noteId)
  {
    Note note = new Note();
    note.setNoteId(noteId);
    return (Note)select(note);
  }

  public List getNotesForId(long id)
  {
    Note note = new Note();
    note.setId(id);
    Selection sel = getSelection(note,false);
    sel.addIndex(IDX_NOTE_ID);
    sel.addOrder("create_ts",true);
    return selectAll(sel);
  }

  /**************************************************************************
   *
   *  EncryptionType
   *
   *************************************************************************/

  public EncryptionType getEncryptType(long etId)
  {
    EncryptionType et = new EncryptionType();
    et.setEtId(etId);
    return (EncryptionType)select(et);
  }

  public List getEncryptTypes()
  {
    return selectAll(EncryptionType.class);
  }

  public EncryptionType getNoneEncryptType()
  {
    EncryptionType et = new EncryptionType();
    et.setEtCode("0");
    Selection sel = getSelection(et,false);
    sel.addIndex(IDX_ET_CODE);
    return (EncryptionType)select(sel);
  }

  public void deleteEncryptType(long etId, UserBean user)
  {
    EncryptionType et = new EncryptionType();
    et.setEtId(etId);
    deleteBean(et,user);
  }

  /**************************************************************************
   *
   *  PartFeatureType
   *
   *************************************************************************/

  public PartFeatureType getPartFeatureType(String ftCode)
  {
    PartFeatureType pft = new PartFeatureType();
    pft.setFtCode(ftCode);
    return (PartFeatureType)select(pft);
  }

  public List getPartFeatureTypes()
  {
    Selection sel = getSelection(PartFeatureType.class);
    sel.addOrder("display_order",false);
    return selectAll(sel);
  }

  public void deletePartFeatureType(String ftCode, UserBean user)
  {
    PartFeatureType pft = new PartFeatureType();
    pft.setFtCode(ftCode);
    deleteBean(pft,user);
  }

  public Map getFtFeatMap()
  {
    Selection sel = getSelection(JOIN_FT_FEAT);
    sel.addMapValueOrder(TBL_FEAT,"display_order",false);
    sel.addMapValueOrder(TBL_FEAT,"feat_name",false);
    return selectJoinMap(sel);
  }

  /**************************************************************************
   *
   *  PartFeature
   *
   *************************************************************************/

  public List getPartFeatures()
  {
    Selection sel = getSelection(PartFeature.class);
    sel.addOrder("ft_code",false);
    sel.addOrder("display_order",false);
    sel.addOrder("feat_name",false);
    return selectAll(sel);
  }

  public PartFeature getPartFeature(long featId)
  {
    PartFeature feature = new PartFeature();
    feature.setFeatId(featId);
    return (PartFeature)select(feature);
  }

  public List getFeaturesWithCode(String ftCode)
  {
    PartFeature feature = new PartFeature();
    feature.setFtCode(ftCode);
    return selectAll(feature,IDX_FEAT_FT_CODE);
  }

  public void deletePartFeature(long featId, UserBean user)
  {
    // setup the reference feature bean with the feature type code
    PartFeature feature = new PartFeature();
    feature.setFeatId(featId);

    // delete any maps joined to this feature
    Selection sel = getSelection(JOIN_FEAT_FM);
    sel.addJoinIndex(IDX_FEAT_KEY,feature);
    delete(sel,user);

    // delete the feature itself
    deleteBean(feature,user);
  }

  public void deleteFeaturesWithCode(String ftCode, UserBean user)
  {
    // setup the reference feature bean with the feature type code
    PartFeature feature = new PartFeature();
    feature.setFtCode(ftCode);

    // delete feature map entries joined to features with the feature type code
    Selection sel = getSelection(JOIN_FEAT_FM);
    sel.addJoinIndex(IDX_FEAT_FT_CODE,feature);
    delete(sel,user);

    // delete features with the feature type code
    sel = getSelection(feature,false);
    sel.addIndex(IDX_FEAT_FT_CODE);
    delete(sel,user);
  }

  public List getPartTypeFeatures(long ptId)
  {
    PartType pt = new PartType();
    pt.setPtId(ptId);
    Selection sel = getSelection(JOIN_PT_FEAT);
    sel.addJoinIndex(IDX_PT_KEY,pt);
    return selectAll(sel);
  }

  public List getFeaturesForPartTypeWithCode(long ptId, String ftCode)
  {
    PartType pt = new PartType();
    pt.setPtId(ptId);
    PartFeature feature = new PartFeature();
    feature.setFtCode(ftCode);
    Selection sel = getSelection(JOIN_PT_FEAT);
    sel.addJoinIndex(IDX_PT_KEY,pt);
    sel.addJoinIndex(IDX_FEAT_FT_CODE,feature);
    return selectAll(sel);
  }

  public void updateFeaturesFtCode(String oldCode, String newCode, UserBean user)
  {
    // TODO: inhance persist package to allow multi-row updating...
    List feats = getFeaturesWithCode(oldCode);
    for (Iterator i = feats.iterator(); i.hasNext();)
    {
      PartFeature feat = (PartFeature)i.next();
      feat.setFtCode(newCode);
      persist(feat,user);
    }
  }

  /**************************************************************************
   *
   *  PartFeatureMap
   *
   *************************************************************************/

  public void deleteFeatureMapsWithPtId(long ptId, UserBean user)
  {
    PartFeatureMap map = new PartFeatureMap();
    map.setPtId(ptId);
    deleteByIndex(map,IDX_FM_PT_ID,user);
  }

  public void deleteFeatureMapsWithFeatId(long featId, UserBean user)
  {
    PartFeatureMap map = new PartFeatureMap();
    map.setFeatId(featId);
    deleteByIndex(map,IDX_FM_FEAT_ID,user);
  }
  public void deleteFeatureMapsWithFeature(PartFeature feature, UserBean user)
  {
    deleteFeatureMapsWithFeatId(feature.getFeatId(),user);
  }

  public List getFeatureMapsForPartType(long ptId)
  {
    PartFeatureMap map = new PartFeatureMap();
    map.setPtId(ptId);
    Selection sel = getSelection(map,false);
    sel.addIndex(IDX_FM_PT_ID);
    return selectAll(sel);
  }

  /**************************************************************************
   *
   *  PartType
   *
   *************************************************************************/

  public PartType getPartType(long ptId)
  {
    PartType pt = new PartType();
    pt.setPtId(ptId);
    return (PartType)select(pt);
  }

  public List getPartTypes()
  {
    Selection sel = getSelection(PartType.class);
    sel.addOrder("model_code",false);
    return selectAll(sel);
  }

  public void deletePartType(long ptId, UserBean user)
  {
    deleteFeatureMapsWithPtId(ptId,user);

    PartType pt = new PartType();
    pt.setPtId(ptId);
    deleteBean(pt,user);
  }

  public List getPartTypesForInventory(Inventory inventory)
  {
    Selection sel = getSelection(JOIN_INV_PT);
    sel.addJoinIndex(IDX_INV_KEY,inventory);
    sel.addOrder("model_code",false);
    return selectAll(sel);
  }
  public List getUnusedPartTypesForInventory(Inventory inventory)
  {
    List partTypes = getPartTypes();
    partTypes.removeAll(getPartTypesForInventory(inventory));
    return partTypes;
  }

  public boolean partTypeHasFeatureOfType(long ptId, String ftCode)
  {
    return !getFeaturesForPartTypeWithCode(ptId,ftCode).isEmpty();
  }
  public boolean partTypeHasFeatureOfType(PartType pt, String ftCode)
  {
    return partTypeHasFeatureOfType(pt.getPtId(),ftCode);
  }

  /**************************************************************************
   *
   *  Condition
   *
   *************************************************************************/

  public Condition getCondition(long condId)
  {
    Condition condition = new Condition();
    condition.setCondId(condId);
    return (Condition)select(condition);
  }

  public List getConditions()
  {
    return selectAll(Condition.class);
  }

  public void deleteCondition(long condId, UserBean user)
  {
    Condition condition = new Condition();
    condition.setCondId(condId);
    deleteBean(condition,user);
  }

  /**************************************************************************
   *
   *  Location
   *
   *************************************************************************/

  public Location getLocation(long locId)
  {
    Location location = new Location();
    location.setLocId(locId);
    return (Location)select(location);
  }

  public List getLocations()
  {
    Selection sel = getSelection(Location.class);
    sel.addOrder("loc_name",false);
    return selectAll(sel);
  }

  public void deleteLocation(long locId, UserBean user)
  {
    Location location = new Location();
    location.setLocId(locId);
    deleteBean(location,user);
  }

  /**************************************************************************
   *
   *  PartStatus
   *
   *************************************************************************/

  public PartStatus getPartStatus(long statusId)
  {
    PartStatus status = new PartStatus();
    status.setStatusId(statusId);
    return (PartStatus)select(status);
  }

  public List getPartStatuses()
  {
    return selectAll(PartStatus.class);
  }

  public void deletePartStatus(long statusId, UserBean user)
  {
    PartStatus status = new PartStatus();
    status.setStatusId(statusId);
    deleteBean(status,user);
  }


  /**************************************************************************
   *
   *  Disposition
   *
   *************************************************************************/

  public Disposition getDisposition(long dispId)
  {
    Disposition disposition = new Disposition();
    disposition.setDispId(dispId);
    return (Disposition)select(disposition);
  }

  public List getDispositions()
  {
    return selectAll(Disposition.class);
  }

  public void deleteDisposition(long dispId, UserBean user)
  {
    Disposition disposition = new Disposition();
    disposition.setDispId(dispId);
    deleteBean(disposition,user);
  }


  /**************************************************************************
   *
   *  PartClass
   *
   *************************************************************************/

  public PartClass getPartClass(long pcId)
  {
    PartClass pc = new PartClass();
    pc.setPcId(pcId);
    return (PartClass)select(pc);
  }

  public PartClass getPartClass(String pcCode)
  {
    PartClass pc = new PartClass();
    pc.setPcCode(pcCode);
    return (PartClass)select(pc,IDX_PC_CODE);
  }

  public List getPartClasses()
  {
    return selectAll(PartClass.class);
  }

  public void deletePartClass(long pcId, UserBean user)
  {
    PartClass pc = new PartClass();
    pc.setPcId(pcId);
    deleteBean(pc,user);
  }


  /**************************************************************************
   *
   *  Client
   *
   *************************************************************************/

  public Client getClient(long clientId)
  {
    Client client = new Client();
    client.setClientId(clientId);
    return (Client)select(client);
  }

  public List getClients()
  {
    Selection sel = getSelection(Client.class);
    sel.addOrder("client_name",false);
    return selectAll(sel);
  }

  public Client getClientOfUser(UserBean user)
  {
    Selection sel = getSelection(Client.class);
    String customClause = " in ( select * from ( select ancestor from"
      + " t_hierarchy where descendent = " + user.getHierarchyNode()
      + " order by relation ) ) and rownum = 1";
    sel.addFilter(new CustomFilter("hierarchy_id",customClause));
    return (Client)select(sel);
  }

  public void deleteClient(long clientId, UserBean user)
  {
    Client client = new Client();
    client.setClientId(clientId);
    deleteBean(client,user);
  }

  public ClientAppMap getClientAppMapForAppType(int appType)
  {
    ClientAppMap cam = new ClientAppMap();
    cam.setAppType(appType);
    return (ClientAppMap)select(cam,IDX_CLAT_APP_TYPE);
  }

  /**************************************************************************
   *
   *  ClientDeployRule
   *
   *************************************************************************/

  public ClientDeployRule getClientDeployRule(long cdRuleId)
  {
    ClientDeployRule cdr = new ClientDeployRule();
    cdr.setCdRuleId(cdRuleId);
    return (ClientDeployRule)select(cdr);
  }

  public List getClientDeployRules()
  {
    Selection sel = getSelection(ClientDeployRule.class);
    sel.addSubtableOrder("tmg_clients","client_name",false);
    return selectAll(sel);
  }

  public void deleteClientDeployRule(long cdRuleId, UserBean user)
  {
    ClientDeployRule cdr = new ClientDeployRule();
    cdr.setCdRuleId(cdRuleId);
    deleteBean(cdr,user);
  }

  public List getAppTypeRefs()
  {
    Selection sel = getSelection(AppTypeRef.class);
    sel.addOrder("app_description",false);
    return selectAll(sel);
  }

  /**************************************************************************
   *
   *  Inventory
   *
   *************************************************************************/

  public Inventory getInventory(long invId)
  {
    Inventory inventory = new Inventory();
    inventory.setInvId(invId);
    return (Inventory)select(inventory);
  }

  public List getInventories()
  {
    Selection sel = getSelection(Inventory.class);
    sel.addOrder("inv_name",false);
    return selectAll(sel);
    //return selectAll(Inventory.class);
  }

  public void deleteInventory(long invId, UserBean user)
  {
    Inventory inventory = new Inventory();
    inventory.setInvId(invId);
    deleteBean(inventory,user);
  }

  // this is a hack to fetch the first inventory named MES (not guaranteed)
  public Inventory getMesInventory()
  {
    Selection sel = getSelection(Inventory.class);
    sel.addFilter(new StringFilter("inv_name","MES"));
    return (Inventory)(selectAll(sel).get(0));
  }

  /**************************************************************************
   *
   *  Bucket
   *
   *************************************************************************/

  public List getBuckets()
  {
    return selectAll(Bucket.class);
  }

  public List getBucketsForInventory(long invId)
  {
    Bucket bucket = new Bucket();
    bucket.setInvId(invId);

    Selection sel = getSelection(bucket,false);
    sel.addIndex(IDX_BUCK_INV_ID);
    sel.addSubtableOrder("tmg_part_types","model_code",false);
    return selectAll(sel);
  }
  public List getBucketsForInventory(Inventory inventory)
  {
    return getBucketsForInventory(inventory.getInvId());
  }

  public Bucket getBucket(long buckId)
  {
    Bucket bucket = new Bucket();
    bucket.setBuckId(buckId);
    return (Bucket)select(bucket);
  }

  public Bucket getBucketForInvPt(long invId, long ptId)
  {
    Bucket bucket = new Bucket();
    bucket.setInvId(invId);
    bucket.setPtId(ptId);
    Selection sel = getSelection(bucket,false);
    sel.addIndex(IDX_BUCK_INV_ID);
    sel.addIndex(IDX_BUCK_PT_ID);
    return (Bucket)select(sel);
  }

  public void deleteBucket(long buckId, UserBean user)
  {
    Bucket bucket = new Bucket();
    bucket.setBuckId(buckId);
    deleteBean(bucket,user);
  }

  /**************************************************************************
   *
   *  PartHistory/PartState
   *
   *************************************************************************/

  public List getPartStatesInBucket(long buckId, java.util.Date date)
  {
    PartState ps = new PartState();
    ps.setBuckId(buckId);
    Selection sel = getSelection(ps,false);
    sel.addIndex(IDX_PS_BUCK_ID);
    Timestamp whenTs = new Timestamp(date.getTime());
    sel.addFilter(new PartStateDateFilter(whenTs));
    sel.addFilter(new PartStateNotZeroFilter());
    return selectAll(sel);
  }
  public List getCurrentPartStatesInBucket(long buckId)
  {
    return getPartStatesInBucket(buckId,Calendar.getInstance().getTime());
  }

  public PartState getPartState(long partId, java.util.Date date)
  {
    PartState ps = new PartState();
    ps.setPartId(partId);
    Selection sel = getSelection(ps,false);
    sel.addIndex(IDX_PS_PART_ID);
    Timestamp whenTs = new Timestamp(date.getTime());
    sel.addFilter(new PartStateDateFilter(whenTs));
    List psList = selectAll(sel);
    if (!psList.isEmpty())
    {
      return (PartState)psList.get(0);
    }
    return null;
  }
  public PartState getCurrentPartState(long partId)
  {
    return getPartState(partId,Calendar.getInstance().getTime());
  }

  public PartState getPartState(long stateId)
  {
    PartState ps = new PartState();
    ps.setStateId(stateId);
    return (PartState)select(ps);
  }

  public PartHistory getPartHistoryForPartId(long partId)
  {
    PartState ps = new PartState();
    ps.setPartId(partId);
    Selection sel = getSelection(ps,false);
    sel.addIndex(IDX_PS_PART_ID);
    sel.addOrder("state_id",false);
    List psList = selectAll(sel);
    PartHistory history = null;
    for (Iterator i = psList.iterator(); i.hasNext();)
    {
      PartState partState = (PartState)i.next();
      if (history == null)
      {
        history = new PartHistory();
      }
      history.add(partState);
    }
    return history;
  }

  public PartHistory getPartHistoryForStateId(long stateId)
  {
    PartState partState = new PartState();
    partState.setStateId(stateId);
    Selection sel = getSelection(partState);
    partState = (PartState)select(sel);
    PartHistory history = getPartHistoryForPartId(partState.getPartId());
    history.setState(partState);
    return history;
  }

  /**
   * NOTE: this is currently assuming serial nums are unique for the purpose
   * of physical inventory audits...
   */
  public PartState getPartStateForSerialNum(String serialNum, java.util.Date when)
  {
    PartState ps = new PartState();
    ps.setSerialNum(serialNum);
    Selection sel = getSelection(ps,false);
    sel.addIndex(IDX_PS_SERIAL_NUM);
    Timestamp whenTs = new Timestamp(when.getTime());
    sel.addFilter(new PartStateDateFilter(whenTs));
    List psList = selectAll(sel);
    if (!psList.isEmpty())
    {
      return (PartState)psList.get(0);
    }
    return null;
  }

  /**************************************************************************
   *
   *  Part
   *
   *************************************************************************/

  public Part getPart(long partId)
  {
    PartState state = getCurrentPartState(partId);
    return state != null ? state.getPart() : null;
  }


  public Part getPartWithSerialNum(String serialNum)
  {
    PartState ps = getPartStateForSerialNum(
            serialNum,Calendar.getInstance().getTime());
    return ps != null ? ps.getPart() : null;
  }

  public List getPartsInBucket(long buckId)
  {
    List states = getCurrentPartStatesInBucket(buckId);
    List parts = new ArrayList();
    for (Iterator i = states.iterator(); i.hasNext();)
    {
      PartState state = (PartState)i.next();
      parts.add(state.getPart());
    }
    return parts;
  }
  public List getPartsInBucket(Bucket bucket)
  {
    return getPartsInBucket(bucket.getBuckId());
  }

  private void persistSnPart(Part part, PartOperation op, UserBean user)
  {
    // look for an existing part state in db for this part
    PartState oldState = getCurrentPartState(part.getPartId());
    if (oldState != null)
    {
      // make sure there is some sort of state change to persist
      if (oldState.stateEquals(part))
      {
        return;
      }
      // generate new state from old state, persist it and the old state
      persist(oldState.createNewState(getNewId(),op,part),user);
      persist(oldState,user);
    }
    else
    {
      // generate new state and persist it
      persist(new PartState(getNewId(),op,part),user);
    }
  }

  /**
   * Adds the given number of parts to the parts state table.  If no part state
   * exists for the given part a new part state is created to represent the part.
   * If a matching part state is found it's count is incremented by the count
   * in the part.
   */
  private long addQtyParts(Part part, PartOperation op, UserBean user)
  {
    // need to determine the final part id to return
    long partId = part.getPartId();

    // start with a new state
    PartState newState = new PartState(-1L,op,part);

    // look for existing state in db to merge this state with
    Selection sel = getSelection(newState,false);
    sel.addIndex(IDX_PS_MERGE_TEST);
    sel.addFilter(new NullFilter("end_ts",true));
    PartState mergeState = (PartState)select(sel);
    if (mergeState != null)
    {
      // generate new state with the updated part count, persist it
      persist(mergeState.addCount(getNewId(),op,part),user);
      persist(mergeState,user);
      partId = mergeState.getPartId();
    }
    else
    {
      // no existing state to merge with, persist the new state after
      // assigning it a new id
      newState.setStateId(getNewId());
      // need to assign a new part id...this is either a new part state or
      // a split off part quantity needing a new part id anyway
      partId = getNewId();
      newState.setPartId(partId);
      persist(newState,user);
    }
    return partId;
  }

  public long addPart(Part part, PartOperation op, UserBean user)
  {
    long partId = part.getPartId();
    if (getPartType(part.getPtId()).hasSn())
    {
      persistSnPart(part,op,user);
    }
    else
    {
      partId = addQtyParts(part,op,user);
    }
    return partId;
  }

  private long removeQtyParts(Part part, PartOperation op, UserBean user)
  {
    // load the parts associated state
    PartState oldState = getCurrentPartState(part.getPartId());
    if (oldState == null)
    {
      throw new NullPointerException("Change requested for part that cannot"
        + " be found in database: " + part);
    }

    // subtract from the
    persist(oldState.subtractCount(getNewId(),op,part),user);
    persist(oldState,user);

    return part.getPartId();
  }

  public long removePart(Part part, PartOperation op, UserBean user)
  {
    long partId = part.getPartId();
    if (getPartType(part.getPtId()).hasSn())
    {
      persistSnPart(part,op,user);
    }
    else
    {
      partId = removeQtyParts(part,op,user);
    }
    return partId;
  }

  public long changePart(Part part, PartOperation op, UserBean user)
  {
    long partId = part.getPartId();

    PartType pt = getPartType(part.getPtId());

    if (pt.hasSn())
    {
      persistSnPart(part,op,user);
    }
    else
    {
      removeQtyParts(part,op,user);
      partId = addQtyParts(part,op,user);
    }
    return partId;
  }

  // should this be allowed?
  public void deletePart(long partId, UserBean user)
  {
    PartState state = new PartState();
    state.setPartId(partId);
    deleteByIndex(state,IDX_PS_PART_ID,user);
  }

  public boolean partHasFeatureOfType(Part part, String ftCode)
  {
    return partTypeHasFeatureOfType(part.getPtId(),ftCode);
  }
  public boolean partHasFeatureOfType(long partId, String ftCode)
  {
    return partHasFeatureOfType(getPart(partId),ftCode);
  }

  /**************************************************************************
   *
   *  PartOperation
   *
   *************************************************************************/

  public PartOpCode getOpCode(String opCode)
  {
    PartOpCode poc = new PartOpCode();
    poc.setOpCode(opCode);
    return (PartOpCode)select(poc);
  }

  public PartOperation getOp(long opId)
  {
    PartOperation op = new PartOperation();
    op.setOpId(opId);
    return (PartOperation)select(op);
  }

  public List getTransactionOps(long tranId)
  {
    PartOperation op = new PartOperation();
    op.setTranId(tranId);
    return selectAll(op,IDX_OPS_TRAN);
  }


  /**************************************************************************
   *
   *  PurchaseOrder
   *
   *************************************************************************/

  public PurchaseOrder getPurchaseOrder(long poId)
  {
    PurchaseOrder po = new PurchaseOrder();
    po.setPoId(poId);
    return (PurchaseOrder)select(po);
  }

  public List getPurchaseOrders()
  {
    return selectAll(PurchaseOrder.class);
  }

  public List getPosForInventory(long invId)
  {
    PurchaseOrder po = new PurchaseOrder();
    po.setInvId(invId);
    return selectAll(po,IDX_PO_INV_ID);
  }
  public List getPosForInventory(Inventory inventory)
  {
    return getPosForInventory(inventory.getInvId());
  }

  public List getPosForInvWithStatus(long invId,int poStatus)
  {
    PurchaseOrder po = new PurchaseOrder();
    po.setInvId(invId);
    po.setPoStatus(poStatus);
    return selectAll(po,IDX_PO_INV_STAT);
  }
  public List getNewPosForInv(long invId)
  {
    return getPosForInvWithStatus(invId,PurchaseOrder.PO_STAT_EDITING);
  }
  public List getClosedPosForInv(long invId)
  {
    List pos = getPosForInvWithStatus(invId,PurchaseOrder.PO_STAT_PAID);
    pos.addAll(getPosForInvWithStatus(invId,PurchaseOrder.PO_STAT_CANCELED));
    return pos;
  }

  public void deletePurchaseOrder(long poId, UserBean user)
  {
    PurchaseOrder po = new PurchaseOrder();
    po.setPoId(poId);
    deleteBean(po,user);
  }

  /**************************************************************************
   *
   *  PurchaseOrderItem
   *
   *************************************************************************/

  public PurchaseOrderItem getPoItem(long poItemId)
  {
    PurchaseOrderItem poItem = new PurchaseOrderItem();
    poItem.setPoItemId(poItemId);
    return (PurchaseOrderItem)select(poItem);
  }

  public List getPoItemsForPo(long poId)
  {
    PurchaseOrderItem poItem = new PurchaseOrderItem();
    poItem.setPoId(poId);
    Selection sel = getSelection(poItem,false);
    sel.addIndex(IDX_POI_PO_ID);
    return selectAll(sel);
  }
  public List getPoItemsForPo(PurchaseOrder po)
  {
    return getPoItemsForPo(po.getPoId());
  }

  public void deletePoItem(long poItemId, UserBean user)
  {
    PurchaseOrderItem poItem = new PurchaseOrderItem();
    poItem.setPoItemId(poItemId);
    deleteBean(poItem,user);
  }


  /**************************************************************************
   *
   *  PurchaseOrderPart
   *
   *************************************************************************/

  public PurchaseOrderPart getPoPart(long poPartId)
  {
    PurchaseOrderPart poPart = new PurchaseOrderPart();
    poPart.setPoPartId(poPartId);
    return (PurchaseOrderPart)select(poPart);
  }

  public List getPoPartsForPo(long poId)
  {
    PurchaseOrderPart poPart = new PurchaseOrderPart();
    poPart.setPoId(poId);
    Selection sel = getSelection(poPart,false);
    sel.addIndex(IDX_POP_PO_ID);
    return selectAll(sel);
  }
  public List getPoPartsForPo(PurchaseOrder po)
  {
    return getPoPartsForPo(po.getPoId());
  }

  public List getPoPartsForItem(long poItemId)
  {
    PurchaseOrderPart poPart = new PurchaseOrderPart();
    poPart.setPoItemId(poItemId);
    Selection sel = getSelection(poPart,false);
    sel.addIndex(IDX_POP_PO_ITEM_ID);
    return selectAll(sel);
  }
  public List getPoPartsForItem(PurchaseOrderItem poItem)
  {
    return getPoPartsForItem(poItem.getPoItemId());
  }

  public void deletePoPart(long poPartId, UserBean user)
  {
    PurchaseOrderPart poPart = new PurchaseOrderPart();
    poPart.setPoPartId(poPartId);
    deleteBean(poPart,user);
  }


  /**************************************************************************
   *
   *  PurchaseOrderInvoice
   *
   *************************************************************************/

  public PurchaseOrderInvoice getPoInvoice(long poInvId)
  {
    PurchaseOrderInvoice poInvoice = new PurchaseOrderInvoice();
    poInvoice.setPoInvId(poInvId);
    return (PurchaseOrderInvoice)select(poInvoice);
  }

  public void deletePoInvoice(long poInvId, UserBean user)
  {
    PurchaseOrderInvoice poInvoice = new PurchaseOrderInvoice();
    poInvoice.setPoInvId(poInvId);
    deleteBean(poInvoice,user);
  }


  /**************************************************************************
   *
   *  PurchaseOrderOptions
   *
   *************************************************************************/

  public PurchaseOrderOptions getPurchaseOrderOptions()
  {
    List list = selectAll(PurchaseOrderOptions.class);
    if (!list.isEmpty())
    {
      return (PurchaseOrderOptions)list.get(0);
    }
    return null;
  }


  /**************************************************************************
   *
   *  Shipper
   *
   *************************************************************************/

  public Shipper getShipper(long shipperId)
  {
    Shipper shipper = new Shipper();
    shipper.setShipperId(shipperId);
    return (Shipper)select(shipper);
  }

  public List getShippers()
  {
    return selectAll(Shipper.class);
  }

  public void deleteShipper(long shipperId, UserBean user)
  {
    Shipper shipper = new Shipper();
    shipper.setShipperId(shipperId);
    deleteShipTypesForShipper(shipperId,user);
    deleteBean(shipper,user);
  }

  /**************************************************************************
   *
   *  ShipType
   *
   *************************************************************************/

  public ShipType getShipType(long shipTypeId)
  {
    ShipType shipType = new ShipType();
    shipType.setShipTypeId(shipTypeId);
    return (ShipType)select(shipType);
  }

  public List getShipTypes()
  {
    return selectAll(ShipType.class);
  }

  public void deleteShipTypesForShipper(long shipperId, UserBean user)
  {
    ShipType shipType = new ShipType();
    shipType.setShipperId(shipperId);

    // delete any vendor ship types using the shipper's ship types
    Selection sel = getSelection(JOIN_SHPT_VST);
    sel.addJoinIndex(IDX_SHPT_SHPR_ID,shipType);
    delete(sel,user);

    // delete all shipper's ship types
    sel = getSelection(shipType,false);
    sel.addIndex(IDX_SHPT_SHPR_ID);
    delete(sel,user);
  }

  public void deleteShipType(long shipTypeId, UserBean user)
  {
    ShipType shipType = new ShipType();
    shipType.setShipTypeId(shipTypeId);
    deleteVstWithShipType(shipTypeId,user);
    deleteBean(shipType,user);
  }

  /**************************************************************************
   *
   *  VendorShipType
   *
   *************************************************************************/

  public void deleteAllVendorShipTypes(long vendorId, UserBean user)
  {
    VendorShipType vst = new VendorShipType();
    vst.setVendorId(vendorId);
    Selection sel = getSelection(vst,false);
    sel.addIndex(IDX_VST_VND_ID);
    delete(sel,user);
  }

  public void deleteVstWithShipType(long shipTypeId, UserBean user)
  {
    VendorShipType vst = new VendorShipType();
    vst.setShipTypeId(shipTypeId);
    deleteByIndex(vst,IDX_VST_ST_ID,user);
  }

  /**************************************************************************
   *
   *  Vendor
   *
   *************************************************************************/

  public Vendor getVendor(long vendorId)
  {
    Vendor vendor = new Vendor();
    vendor.setVendorId(vendorId);
    return (Vendor)select(vendor);
  }

  public List getVendors()
  {
    return selectAll(Vendor.class);
  }

  public void deleteVendor(long vendorId, UserBean user)
  {
    Vendor vendor = new Vendor();
    vendor.setVendorId(vendorId);
    deleteAllVendorShipTypes(vendorId,user);
    deleteBean(vendor,user);
  }

  /**************************************************************************
   *
   *  VendorClass
   *
   *************************************************************************/

  public VendorClass getVendorClass(long vpcId)
  {
    VendorClass vpc = new VendorClass();
    vpc.setVpcId(vpcId);
    return (VendorClass)select(vpc);
  }

  public List getVendorClasses()
  {
    return selectAll(VendorClass.class);
  }

  public void deleteVendorClass(long vpcId, UserBean user)
  {
    VendorClass vpc = new VendorClass();
    vpc.setVpcId(vpcId);
    deleteBean(vpc,user);
  }


  /**************************************************************************
   *
   *  Address
   *
   *************************************************************************/

  public Address getAddress(long addrId)
  {
    Address addr = new Address();
    addr.setAddrId(addrId);
    return (Address)select(addr);
  }

  public List getAddresses()
  {
    return selectAll(Address.class);
  }

  public void deleteAddress(long addrId, UserBean user)
  {
    Address addr = new Address();
    addr.setAddrId(addrId);
    deleteBean(addr,user);
  }


  public Address getFirstAddressForWho(long whoId)
  {
    Address addr = new Address();
    addr.setWhoId(whoId);
    return (Address)select(addr,IDX_ADDR_WHO_ID);
  }

  public List getAddressesForWho(long whoId)
  {
    Address address = new Address();
    address.setWhoId(whoId);
    return selectAll(address,IDX_ADDR_WHO_ID);
  }

  /**************************************************************************
   *
   *  Contact
   *
   *************************************************************************/

  public Contact getContact(long contId)
  {
    Contact contact = new Contact();
    contact.setContId(contId);
    return (Contact)select(contact);
  }

  public List getContacts()
  {
    return selectAll(Contact.class);
  }

  public List getContactsForWho(long whoId)
  {
    Contact contact = new Contact();
    contact.setWhoId(whoId);
    return selectAll(contact,IDX_CONT_WHO_ID);
  }

  public Contact getFirstContactForWho(long whoId)
  {
    Contact contact = new Contact();
    contact.setWhoId(whoId);
    return (Contact)select(contact,IDX_CONT_WHO_ID);
  }

  public void deleteContact(long contId, UserBean user)
  {
    Contact contact = new Contact();
    contact.setContId(contId);
    deleteBean(contact,user);
  }

  /**************************************************************************
   *
   *  Audit / AuditException
   *
   *************************************************************************/

  public Audit getAudit(long auditId)
  {
    Audit audit = new Audit();
    audit.setAuditId(auditId);
    return (Audit)select(audit);
  }

  public void purgeAuditExceptions(long auditId, UserBean user)
  {
    AuditException ex = new AuditException();
    ex.setAuditId(auditId);
    deleteByIndex(ex,IDX_AUDEX_AUDIT_ID,user);
  }

  public AuditException getAuditException(long auditExId)
  {
    AuditException ex = new AuditException();
    ex.setAuditExId(auditExId);
    return (AuditException)select(ex);
  }

  public List getAuditExceptionsForAudit(long auditId)
  {
    AuditException ex = new AuditException();
    ex.setAuditId(auditId);
    return selectAll(ex,IDX_AUDEX_AUDIT_ID);
  }

  public List getAuditExceptionsForResult(long auditId, String resultCode)
  {
    AuditException ex = new AuditException();
    ex.setAuditId(auditId);
    ex.setResultCode(resultCode);
    Selection sel = getSelection(ex,false);
    sel.addIndex("idx_audex_audit_id");
    sel.addIndex("idx_audex_result_code");
    return selectAll(sel);
  }
  public List getMissingAuditExceptions(long auditId)
  {
    return getAuditExceptionsForResult(auditId,AuditException.RESULT_MISSING);
  }
  public List getUnknownAuditExceptions(long auditId)
  {
    return getAuditExceptionsForResult(auditId,AuditException.RESULT_UNKNOWN);
  }

  public AuditException getAuditExceptionForSn(long auditId, String serialNum)
  {
    AuditException ex = new AuditException();
    ex.setAuditId(auditId);
    ex.setSerialNum(serialNum);
    Selection sel = getSelection(ex,false);
    sel.addIndex(IDX_AUDEX_SN);
    return (AuditException)select(sel);
  }

  public List lookupAudits(java.util.Date utilFrom, java.util.Date utilTo,
    long clientId)
  {
    Audit audit = new Audit();
    Selection sel = getSelection(audit,false);

    // 0 client id is a global audit, -1 means finda ALL audits
    if (clientId >= 0L)
    {
      audit.setClientId(clientId);
      sel.addIndex(IDX_AUD_CLIENT_ID);
    }

    java.sql.Date sqlFrom = new java.sql.Date(utilFrom.getTime());
    java.sql.Date sqlTo = utilTo != null ?
      new java.sql.Date(utilTo.getTime()) : sqlFrom;
    sel.addFilter(new DateRangeFilter("create_ts",sqlFrom,sqlTo));

    return selectAll(sel);
  }

  public List getAuditPartStatesInStock(Audit audit)
  {
    // setup part state lookup
    PartState ps = new PartState();
    Selection sel = getSelection(ps,false);

    // parts must be in stock
    ps.setStatusCode(ps.SC_IN);
    sel.addIndex(IDX_PS_STATUS_CODE);

    // in the warehouse or in testing
    OrFilter locFilter = new OrFilter();
    locFilter.addFilter(new StringFilter("loc_code",ps.LC_WAREHOUSE));
    locFilter.addFilter(new StringFilter("loc_code",ps.LC_TESTING));
    locFilter.addFilter(new StringFilter("loc_code",ps.LC_NEED_INJECT));
    sel.addFilter(locFilter);
    
    // not lost
    ps.setLostFlag(ps.FLAG_NO);
    sel.addIndex(IDX_PS_LOST_FLAG);

    // serial num not null
    sel.addFilter(new NullFilter("serial_num",false));

    // TODO: fix persist package to allow joins to have
    // more flexible conditions... (i.e. for imprinters,
    // need ability to do join subsel that filters out
    // parts with featId <> imprinter cat feat id)
    //Selection subSel = getSelection(JOIN_PT_FEAT);
    //PartFeature feat = new PartFeature();
    // HACK: hardcoded imprinter category part feature
    //feat.setFeatId(923L); 
    //subSel.addJoinIndex(IDX_FEAT_KEY,feat);
    //sel.addSubSel(subSel,"pt_id");
    

    // belong to audit client if there is one
    if (audit.getClientId() > 0)
    {
      sel.addSubIndex("idx_cli_key","tmg_clients",audit.getClient());
    }

    // fall around the audit date
    sel.addFilter(
      new PartStateDateFilter(new Timestamp(audit.getAuditDate().getTime())));

    return selectAll(sel);
  }

  /**
   * Looks for all inventory owners that do not match an audit's client type
   * in the set of part states for the given serial number set.  Returns an
   * empty list if the audit is global.
   */
  public List getAuditPartStatesWithBadOwner(Audit audit, Set snSet)
  {
    // make sure audit is not global audit
    List badOwnerList = new ArrayList();
    if (!audit.isGlobal())
    {
      // setup part state lookup
      PartState ps = new PartState();
      Selection sel = getSelection(ps,false);

      // parts must not belong to audit client 
      sel.addFilter(
        new NumberFilter("client_id",
          audit.getClientId(),false),"tmg_clients");

      // must fall around the audit date
      sel.addFilter(new PartStateDateFilter(
        new Timestamp(audit.getAuditDate().getTime())));

      // look for 100 serial number matches at a time
      InFilter snFilter = new InFilter("serial_num");
      sel.addFilter(snFilter);
      int snCount = 0;
      for (Iterator i = snSet.iterator(); i.hasNext();)
      {
        snFilter.addValue(i.next());
        if (++snCount >= 100 || !i.hasNext())
        {
          badOwnerList.addAll(selectAll(sel));
          snFilter.reset();
          snCount = 0;
        }
      }
    }
    return badOwnerList;
  }

  /**
   * Given a set of serial nums, this will return the list of all
   * matching part states found in the db.
   */
  public List getAuditPartStatesForSerialNums(Audit audit, Set snSet)
  {
    List knownList = new ArrayList();

    // setup part state lookup
    PartState ps = new PartState();
    Selection sel = getSelection(ps,false);

    // parts must belong to audit client (unless this is global audit)
    if (!audit.isGlobal())
    {
      sel.addFilter(
        new NumberFilter("client_id",audit.getClientId(),true),"tmg_clients");
    }

    // must fall around the audit date
    sel.addFilter(new PartStateDateFilter(
      new Timestamp(audit.getAuditDate().getTime())));

    // look for 100 serial number matches at a time
    InFilter snFilter = new InFilter("serial_num");
    sel.addFilter(snFilter);
    int snCount = 0;
    for (Iterator i = snSet.iterator(); i.hasNext();)
    {
      snFilter.addValue(i.next());
      if (++snCount >= 100 || !i.hasNext())
      {
        knownList.addAll(selectAll(sel));
        snFilter.reset();
        snCount = 0;
      }
    }

    return knownList;
  }

  /**************************************************************************
   *
   *  MiscAdd / MiscAddPart / MiscAddSource
   *
   *************************************************************************/

  public MiscAddPart getMiscAddPart(long maPartId)
  {
    MiscAddPart map = new MiscAddPart();
    map.setMaPartId(maPartId);
    return (MiscAddPart)select(map);
  }

  public MiscAdd getMiscAdd(long maId)
  {
    MiscAdd ma = new MiscAdd();
    ma.setMaId(maId);
    return (MiscAdd)select(ma);
  }

  public List getMiscAddParts(long maId)
  {
    PartState ps = new PartState();
    Selection sel = getSelection(ps,false);
    ps.setMaId(maId);
    sel.addIndex(IDX_PS_MA_ID);
    java.util.Date now = Calendar.getInstance().getTime();
    Timestamp nowTs = new Timestamp(now.getTime());
    sel.addFilter(new PartStateDateFilter(nowTs));
    return selectAll(sel);
  }

  public MiscAddSource getMiscAddSource(long masId)
  {
    MiscAddSource mas = new MiscAddSource();
    mas.setMasId(masId);
    return (MiscAddSource)select(mas);
  }

  public List getMiscAddSources()
  {
    return selectAll(MiscAddSource.class);
  }

  public void deleteMiscAddSource(long masId, UserBean user)
  {
    MiscAddSource mas = new MiscAddSource();
    mas.setMasId(masId);
    deleteBean(mas,user);
  }

  private static String maLuQs =

    " select  ma.ma_id,                           " +
    "         ma.user_name,                       " +
    "         ma.create_ts,                       " +
    "         map.ma_part_id,                     " +
    "         ps.part_id,                         " +
    "         i.inv_name,                         " +
    "         pt.model_code,                      " +
    "         pt.description,                     " +
    "         map.pc_code,                        " +
    "         map.source_code,                    " +
    "         map.part_price,                     " +
    "         map.part_count,                     " +
    "         map.serial_num    map_serial_num,   " +
    "         ps.serial_num     ps_serial_num     " +
    " from    tmg_misc_adds ma,                   " +
    "         tmg_misc_add_parts map,             " +
    "         tmg_part_states ps,                 " +
    "         tmg_part_types pt,                  " +
    "         tmg_inventories i                   " +
    " where   ma.ma_id = map.ma_id                " +
    "         and map.ma_part_id = ps.ma_part_id  " +
    "         and ps.end_ts is null               " +
    "         and ps.pt_id = pt.pt_id             " +
    "         and map.inv_id = i.inv_id           ";

  public static final String maDatesClause =
    " and trunc(ma.create_ts) between ? and ? ";

  public static final String maInvClause = " and map.inv_id = ? ";

  public static final String maSearchClause =

    " and ( ? in ( ma.ma_id, map.ma_part_id, ps.part_id ) " +
    "       or ps.serial_num = ? or map.serial_num = ?    " +
    "       or ma.user_name = ? )                         ";

  public static final String maOrderClause = " order by ma_id desc, ma_part_id ";

  private List getMiscAddLookupRowsFromStatement(PreparedStatement ps)
    throws SQLException
  {
    ResultSet rs = null;

    try
    {
      rs = ps.executeQuery();
      List rows = new ArrayList();
      while (rs.next())
      {
        MiscAddLookupRow row = new MiscAddLookupRow();
        row.setMaId       (rs.getLong     ("ma_id"));
        row.setUserName   (rs.getString   ("user_name"));
        row.setCreateTs   (rs.getTimestamp("create_ts"));
        row.setMaPartId   (rs.getLong     ("ma_part_id"));
        row.setPartId     (rs.getLong     ("part_id"));
        row.setInvName    (rs.getString   ("inv_name"));
        row.setModelCode  (rs.getString   ("model_code"));
        row.setDescription(rs.getString   ("description"));
        row.setPcCode     (rs.getString   ("pc_code"));
        row.setSourceCode (rs.getString   ("source_code"));
        row.setPartPrice  (rs.getFloat    ("part_price"));
        row.setQuantity   (rs.getInt      ("part_count"));
        row.setMaSerialNum(rs.getString   ("map_serial_num"));
        row.setPsSerialNum(rs.getString   ("ps_serial_num"));
        rows.add(row);
      }
      return rows;
    }
    catch (SQLException e)
    {
      log.error("Error loading misc add lookup rows from statement: " + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
    }
  }

  public List getMiscAddLookupRows(java.util.Date fromDate, java.util.Date toDate,
    long invId, String searchTerm)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      StringBuffer qs = new StringBuffer(maLuQs);
      if (fromDate != null) qs.append(maDatesClause);
      if (invId != -1L) qs.append(maInvClause);
      if (searchTerm != null) qs.append(maSearchClause);
      qs.append(maOrderClause);
      ps = con.prepareStatement(qs.toString());
      int varCnt = 1;
      java.sql.Date sqlFrom = null;
      java.sql.Date sqlTo = null;
      if (fromDate != null)
      {
        sqlFrom = new java.sql.Date(fromDate.getTime());
        if (toDate != null)
        {
          sqlTo = new java.sql.Date(toDate.getTime());
        }
        else
        {
          sqlTo = sqlFrom;
        }
        ps.setDate(varCnt++,sqlFrom);
        ps.setDate(varCnt++,sqlTo);
      }
      if (invId != -1L) ps.setLong(varCnt++,invId);
      if (searchTerm != null)
      {
        long numTerm = 0;
        try { numTerm = Long.parseLong(searchTerm); } catch (Exception e) { }
        ps.setLong(varCnt++,numTerm);
        ps.setString(varCnt++,searchTerm);
        ps.setString(varCnt++,searchTerm);
        ps.setString(varCnt++,searchTerm);
      }
      return getMiscAddLookupRowsFromStatement(ps);
    }
    catch (Exception e)
    {
      log.error("Error loading transfer lookup rows: " + e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }

    return null;
  }


  /**************************************************************************
   *
   *  Transfers
   *
   *************************************************************************/

  public Transfer getTransfer(long xferId)
  {
    Transfer transfer = new Transfer();
    transfer.setXferId(xferId);
    return (Transfer)select(transfer);
  }

  public Transfer getTransferWithOpId(long opId)
  {
    Transfer transfer = new Transfer();
    transfer.setOpId(opId);
    return (Transfer)select(transfer,"idx_xfer_op_id");
  }

  public List getPartsInTransfer(Transfer transfer)
  {
    Selection sel = getSelection(JOIN_XFER_PS);
    sel.addJoinIndex(IDX_XFER_KEY,transfer);
    java.util.Date now = Calendar.getInstance().getTime();
    Timestamp nowTs = new Timestamp(now.getTime());
    sel.addJoinFilter(new PartStateDateFilter(nowTs),TBL_PS);
    return selectAll(sel);
  }

  public TransferPart getTransferPart(long xferPartId)
  {
    TransferPart xferPart = new TransferPart();
    xferPart.setXferPartId(xferPartId);
    return (TransferPart)select(xferPart);
  }

  private List getTransferLookupRowsFromStatement(PreparedStatement ps)
    throws SQLException
  {
    ResultSet rs = null;

    try
    {
      rs = ps.executeQuery();
      List rows = new ArrayList();
      while (rs.next())
      {
        TransferLookupRow row = new TransferLookupRow();
        row.setXferId     (rs.getLong     ("xfer_id"));
        row.setCreateTs   (rs.getTimestamp("create_ts"));
        row.setFromName   (rs.getString   ("from_name"));
        row.setToName     (rs.getString   ("to_name"));
        row.setUserName   (rs.getString   ("user_name"));
        row.setXferPartId (rs.getLong     ("xfer_part_id"));
        row.setSerialNum  (rs.getString   ("serial_num"));
        row.setStateId    (rs.getLong     ("state_id"));
        row.setQuantity   (rs.getLong     ("quantity"));
        row.setModelCode  (rs.getString   ("model_code"));
        row.setDescription(rs.getString   ("description"));
        row.setPartId     (rs.getLong     ("part_id"));
        row.setPcName     (rs.getString   ("pc_name"));
        row.setCancelFlag (rs.getString   ("cancel_flag"));
        row.setCanCancelFlag(rs.getString ("can_cancel_flag"));
        rows.add(row);
      }
      return rows;
    }
    catch (SQLException e)
    {
      log.error("Error loading transfer lookup rows from statement: " + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
    }
  }

  private static String xferLuQs =

    " select  t.xfer_id,                              " +
    "         t.create_ts,                            " +
    "         i_from.inv_name from_name,              " +
    "         i_to.inv_name to_name,                  " +
    "         t.user_name,                            " +
    "         tp.xfer_part_id,                        " +
    "         tp.cancel_flag,                         " +
    "         ps.serial_num,                          " +
    "         ps.state_id,                            " +
    "         tp.quantity,                            " +
    "         pt.model_code,                          " +
    "         pt.description,                         " +
    "         ps.part_id,                             " +
    "         pc.pc_name,                             " +
    "         nvl2(ps.end_ts,'n','y') can_cancel_flag " +
    " from    tmg_transfers t,                        " +
    "         tmg_transfer_parts tp,                  " +
    "         tmg_part_states ps,                     " +
    "         tmg_part_classes pc,                    " +
    "         tmg_part_types pt,                      " +
    "         tmg_inventories i_from,                 " +
    "         tmg_inventories i_to                    " +
    " where   t.from_inv_id = i_from.inv_id           " +
    "         and t.to_inv_id = i_to.inv_id           " +
    "         and t.xfer_id = tp.xfer_id              " +
    "         and tp.part_id = ps.part_id             " +
    "         and ps.pt_id = pt.pt_id                 " +
    "         and ps.pc_code = pc.pc_code             " +
    "         and t.op_id = ps.op_id                  ";

  public static final String xferDatesClause    = " and trunc(t.create_ts) between ? and ? ";
  public static final String xferFromInvClause  = " and t.from_inv_id = ? ";
  public static final String xferToInvClause    = " and t.to_inv_id = ? ";
  public static final String xferPcCodeClause   = " and ps.pc_code = ? ";
  public static final String xferSearchClause   =
    " and ( ? in ( t.xfer_id, tp.xfer_part_id, ps.part_id, t.op_id ) or " +
    " ps.serial_num = ? or t.user_name = ? ) ";
  public static final String xferOrderClause    = " order by i_to.inv_id, t.xfer_id ";

  public List getTransferLookupRows(java.util.Date fromDate, java.util.Date toDate,
    long fromInvId, long toInvId, String pcCode, String searchTerm)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      StringBuffer qs = new StringBuffer(xferLuQs);
      if (fromDate != null) qs.append(xferDatesClause);
      if (fromInvId != -1L) qs.append(xferFromInvClause);
      if (toInvId != -1L) qs.append(xferToInvClause);
      if (pcCode != null) qs.append(xferPcCodeClause);
      if (searchTerm != null) qs.append(xferSearchClause);
      qs.append(xferOrderClause);
      ps = con.prepareStatement(qs.toString());
      int varCnt = 1;
      java.sql.Date sqlFrom = null;
      java.sql.Date sqlTo = null;
      if (fromDate != null)
      {
        sqlFrom = new java.sql.Date(fromDate.getTime());
        if (toDate != null)
        {
          sqlTo = new java.sql.Date(toDate.getTime());
        }
        else
        {
          sqlTo = sqlFrom;
        }
        ps.setDate(varCnt++,sqlFrom);
        ps.setDate(varCnt++,sqlTo);
      }
      if (fromInvId != -1L) ps.setLong(varCnt++,fromInvId);
      if (toInvId != -1L) ps.setLong(varCnt++,toInvId);
      if (pcCode != null) ps.setString(varCnt++,pcCode);
      if (searchTerm != null)
      {
        long numTerm = 0;
        try { numTerm = Long.parseLong(searchTerm); } catch (Exception e) { }
        ps.setLong(varCnt++,numTerm);
        ps.setString(varCnt++,searchTerm);
        ps.setString(varCnt++,searchTerm);
      }
      return getTransferLookupRowsFromStatement(ps);
    }
    catch (Exception e)
    {
      log.error("Error loading transfer lookup rows: " + e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }

    return null;
  }

  /**************************************************************************
   *
   *  Repairs
   *
   *************************************************************************/

  public RepairOrder createOrder(long vId, long cId, String vrn, UserBean user)
  {

    Vendor v = getVendor(vId);
    Client c = getClient(cId);

    RepairOrder order = new RepairOrder();
    order.setBatchId(getNewId());
    order.setVendorName(v.getVendorName());
    order.setVendorId(v.getVendorId());
    order.setClientName(c.getClientName());
    order.setClientId(c.getClientId());
    order.setVRN(vrn);
    order.setUserName(user.getLoginName());
    Calendar cal = Calendar.getInstance();
    order.setOpenDate(cal.getTime());
    persist(order,user);
    return getRepairOrder(order.getBatchId());
  }

  public RepairOrder getRepairOrder(long orderId)
  {
    RepairOrder order = new RepairOrder();
    order.setBatchId(orderId);
    order = (RepairOrder)select(order);
    updateItems(order);
    return order;
  }

  public void updateItems(RepairOrder order)
  {
    for(Iterator i = order.getRepairOrderItems().iterator(); i.hasNext();)
    {
      RepairOrderItem item = (RepairOrderItem)i.next();
      getCurrentItemDetails(item);
    }
  }

  public void getCurrentItemDetails(RepairOrderItem item)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    StringBuffer sb = new StringBuffer(80);
    sb.append("select tps.status_code, tps.pc_code, tpt.pt_name ");
    sb.append("from tmg_part_states tps, tmg_part_types tpt ");
    sb.append("where tps.serial_num = '").append(item.getSerNum());
    sb.append("' and tps.end_ts is null ");
    sb.append(" and tps.pt_id = tpt.pt_id");
    try
    {
      Connection con = getConnection();
      ps = con.prepareStatement(sb.toString());
      rs = ps.executeQuery();
      if(rs.next())
      {
        item.setStatusCode(rs.getString("status_code"));
        item.setClassCode(rs.getString("pc_code"));
        item.setPartType(rs.getString("pt_name"));
      }
    }
    catch (Exception e)
    {
      log.error("Error shipping repair order: " + e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }
  }

/*
  //Will probably need to do something like this in order to get
  //the proper PartState reference...
  // (original code from getPartsInTransfer above)

  public List getItemsInRepairOrder(RepairOrder order)
  {
    Selection sel = getSelection(JOIN_XFER_PS);
    //sel.addJoinIndex(IDX_XFER_KEY,transfer);
    //java.util.Date now = Calendar.getInstance().getTime();
    //Timestamp nowTs = new Timestamp(now.getTime());
    //sel.addJoinFilter(new PartStateDateFilter(nowTs),TBL_PS);
    return selectAll(sel);
  }
*/

  public void deleteRepairPart(RepairOrderItem item, UserBean user)
  {
    deleteBean(item,user);
  }

  public void deleteRepairOrder(RepairOrder order, UserBean user)
  {
    deleteBean(order,user);
  }

  private List getBatchOrdersFromStatement(PreparedStatement ps)
    throws SQLException
  {
    ResultSet rs = null;

    try
    {
      rs = ps.executeQuery();
      List orders = new ArrayList();
      while (rs.next())
      {
        RepairOrder order = new RepairOrder();
        order.setBatchId(rs.getLong("batch_id"));
        order.setVendorName(rs.getString("vendor_name"));
        order.setVendorId(rs.getLong("vendor_id"));
        order.setVRN(rs.getString("vrn"));
        order.setClientName(rs.getString("client_name"));
        order.setClientId(rs.getLong("client_id"));
        order.setOpenDateTs(rs.getTimestamp("open_date_ts"));
        order.setShipDateTs(rs.getTimestamp("ship_date_ts"));
        order.setCloseDateTs(rs.getTimestamp("close_date_ts"));
        orders.add(order);
      }
      return orders;
    }
    catch (SQLException e)
    {
      log.error("Error loading batch repair lookup from statement: " + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
    }
  }

  private static String repairLuQs =
    "select                           " +
    "  r.batch_id,                    " +
    "  r.vendor_id,                   " +
    "  v.vendor_name,                 " +
    "  r.client_id,                   " +
    "  c.client_name,                 " +
    "  r.vrn,                         " +
    "  r.user_name,                   " +
    "  r.open_date_ts,                " +
    "  r.ship_date_ts,                 " +
    "  r.close_date_ts                 " +
    "from                             " +
    "  tmg_repair_order r,            " +
    "  tmg_vendors v,                 " +
    "  tmg_clients c,                 " +
    "  tmg_repair_order_item ri       " +
    "where                            " +
    "  r.vendor_id = v.vendor_id and  " +
    "  r.client_id = c.client_id      ";

  public static final String repairDatesClause    =
    " and trunc(r.open_date_ts) between ? and ?";
  public static final String repairSearchClause   =
    " and "+
    "(    "+
    "  ? = r.batch_id "+
    "  or lower(r.user_name) like lower('%'||?||'%')    " +
    "  or lower(r.vrn) like lower('%'||?||'%')          " +
    "  or lower(v.vendor_name) like lower('%'||?||'%')  " +
    "  or lower(c.client_name) like lower('%'||?||'%')  " +
    "  or ( lower(?) in (lower(ri.ser_num), lower(ri.mid)) and r.BATCH_ID = ri.order_id )"+
    " )    ";
  public static final String repairOrderClause    =
    "group by             " +
    "  r.batch_id,        " +
    "  r.vendor_id,       " +
    "  v.vendor_name,     " +
    "  r.client_id,       " +
    "  c.client_name,     " +
    "  r.vrn,             " +
    "  r.user_name,       " +
    "  r.open_date_ts,    " +
    "  r.ship_date_ts,     " +
    "  r.close_date_ts     " +
    "order by r.batch_id";

  public List getRepairLookupRows(java.util.Date fromDate,
                                  java.util.Date toDate,
                                  String searchTerm)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      StringBuffer qs = new StringBuffer(repairLuQs);
      if (fromDate != null) qs.append(repairDatesClause);
      if (searchTerm != null) qs.append(repairSearchClause);
      qs.append(repairOrderClause);
      ps = con.prepareStatement(qs.toString());
      int varCnt = 1;
      java.sql.Date sqlFrom = null;
      java.sql.Date sqlTo = null;
      if (fromDate != null)
      {
        sqlFrom = new java.sql.Date(fromDate.getTime());
        if (toDate != null)
        {
          sqlTo = new java.sql.Date(toDate.getTime());
        }
        else
        {
          sqlTo = sqlFrom;
        }
        ps.setDate(varCnt++,sqlFrom);
        ps.setDate(varCnt++,sqlTo);
      }
      if (searchTerm != null)
      {
        long numTerm = 0;
        try { numTerm = Long.parseLong(searchTerm); } catch (Exception e) { }
        ps.setLong(varCnt++,numTerm);
        ps.setString(varCnt++,searchTerm);
        ps.setString(varCnt++,searchTerm);
        ps.setString(varCnt++,searchTerm);
        ps.setString(varCnt++,searchTerm);
        ps.setString(varCnt++,searchTerm);
      }
      return getBatchOrdersFromStatement(ps);
    }
    catch (Exception e)
    {
      log.error("Error loading transfer lookup rows: " + e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }

    return null;
  }

  public void updateRepairItem(RepairOrderItem item)
  {
    PreparedStatement ps = null;
    StringBuffer sb = new StringBuffer(64);
    sb.append("update tmg_repair_order_item set ");
    sb.append("problem_as_tested = '").append(item.getProblemAsTested()).append("', ");
    sb.append("acr = '").append(item.getAcr()).append("', ");
    sb.append("mid = ").append(item.getMid()).append(" ");
    sb.append("where id = ");
    sb.append(item.getId());
    try
    {
      Connection con = getConnection();
      ps = con.prepareStatement(sb.toString());
      int i = ps.executeUpdate();
    }
    catch (Exception e)
    {
      log.error("Error updating repair items: " + e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }
  }

  public void shipRepairOrder(RepairOrder order)
  {
    updateRepairOrderTs("ship_date_ts", order);
  }

  public void closeRepairOrder(RepairOrder order)
  {
    updateRepairOrderTs("close_date_ts", order);
  }

  private void updateRepairOrderTs(String colName, RepairOrder order)
  {
    PreparedStatement ps = null;
    StringBuffer sb = new StringBuffer(64);
    sb.append("update tmg_repair_order set ");
    sb.append(colName).append(" = sysdate where batch_id = ");
    sb.append(order.getBatchId());
    try
    {
      Connection con = getConnection();
      ps = con.prepareStatement(sb.toString());
      int i = ps.executeUpdate();
    }
    catch (Exception e)
    {
      log.error("Error shipping repair order: " + e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }
  }

  /**************************************************************************
   *
   *  OlaRef
   *
   *************************************************************************/

  public OlaRef getOlaRef(long appSeqNum)
  {
    OlaRef olaRef = new OlaRef();
    olaRef.setAppSeqNum(appSeqNum);
    return (OlaRef)select(olaRef);
  }

  /**************************************************************************
   *
   *  MerchantRef
   *
   *************************************************************************/

  public MerchantRef getMerchantRef(String merchNum)
  {
    MerchantRef ref = new MerchantRef();
    ref.setMerchNumber(merchNum);
    return (MerchantRef)select(ref,IDX_MREF_MERCH_NUMBER);
  }

  public MerchantRef getMerchantRef(long appSeqNum)
  {
    MerchantRef ref = new MerchantRef();
    ref.setAppSeqNum(appSeqNum);
    return (MerchantRef)select(ref);
  }

  public List getMerchantRefs(String searchTerm)
  {
    if (searchTerm == null || searchTerm.length() == 0) return null;

    Selection sel = getSelection(MerchantRef.class);

    long longTerm = -1L;
    try
    {
      longTerm = Long.parseLong(searchTerm);
    }
    catch (Exception e) { }

    OrFilter orFilt = new OrFilter();
    if (longTerm != -1L)
    {
      orFilt.addFilter(new NumberFilter("app_seq_num",longTerm));
      orFilt.addFilter(new NumberFilter("merc_cntrl_number",longTerm));
    }
    orFilt.addFilter(new NumberFilter("merch_number",longTerm));
    StringFilter nameFilter = new StringFilter("merch_business_name",searchTerm);
    nameFilter.setIsSubstring(true);
    orFilt.addFilter(nameFilter);
    sel.addFilter(orFilt);

    return selectAll(sel);
  }

  /**************************************************************************
   *
   *  AcrRef
   *
   *************************************************************************/

  public AcrRef getAcrRef(long acrId)
  {
    AcrRef ref = new AcrRef();
    ref.setAcrId(acrId);
    return (AcrRef)select(ref);
  }

  /**************************************************************************
   *
   *  OrderSources
   *
   *************************************************************************/

  public OrderSource getOrderSource(long ordSrcId)
  {
    OrderSource os = new OrderSource();
    os.setOrdSrcId(ordSrcId);
    return (OrderSource)select(os);
  }

  public OrderSource getOrderSource(String ordSrcCode)
  {
    OrderSource os = new OrderSource();
    os.setOrdSrcCode(ordSrcCode);
    return (OrderSource)select(os,IDX_OS_ORD_SRC_CODE);
  }

  public List getOrderSources()
  {
    return selectAll(OrderSource.class);
  }

  public void deleteOrderSource(long ordSrcId, UserBean user)
  {
    OrderSource os = new OrderSource();
    os.setOrdSrcId(ordSrcId);
    deleteBean(os,user);
  }


  /**************************************************************************
   *
   *  CallTagPartStatus
   *
   *************************************************************************/

  public CallTagPartStatus getCallTagPartStatus(long ctpStatId)
  {
    CallTagPartStatus ctps = new CallTagPartStatus();
    ctps.setCtpStatId(ctpStatId);
    return (CallTagPartStatus)select(ctps);
  }

  public CallTagPartStatus getCallTagPartStatus(String ctpStatCode)
  {
    CallTagPartStatus ctps = new CallTagPartStatus();
    ctps.setCtpStatCode(ctpStatCode);
    return (CallTagPartStatus)select(ctps,"idx_ctps_code");
  }

  public List getCallTagPartStatuses()
  {
    Selection sel = getSelection(CallTagPartStatus.class);
    sel.addOrder("description",false);
    return selectAll(sel);
  }

  public void deleteCallTagPartStatus(long ctpStatId, UserBean user)
  {
    CallTagPartStatus ctps = new CallTagPartStatus();
    ctps.setCtpStatId(ctpStatId);
    deleteBean(ctps,user);
  }


  /**************************************************************************
   *
   *  OrderStatuses
   *
   *************************************************************************/

  public OrderStatus getOrderStatus(long ordStatId)
  {
    OrderStatus os = new OrderStatus();
    os.setOrdStatId(ordStatId);
    return (OrderStatus)select(os);
  }

  public OrderStatus getOrderStatus(String ordStatCode)
  {
    OrderStatus os = new OrderStatus();
    os.setOrdStatCode(ordStatCode);
    return (OrderStatus)select(os,"idx_ostat_ord_stat_code");
  }

  public List getOrderStatuses()
  {
    Selection sel = getSelection(OrderStatus.class);
    sel.addOrder("order_idx",false);
    return selectAll(sel);
  }

  public void deleteOrderStatus(long ordStatId, UserBean user)
  {
    OrderStatus os = new OrderStatus();
    os.setOrdStatId(ordStatId);
    deleteBean(os,user);
  }

  /**************************************************************************
   *
   *  OrderOutParts
   *
   *************************************************************************/

  public OrderOutPart getOrderOutPart(long oopId)
  {
    OrderOutPart oop = new OrderOutPart();
    oop.setOopId(oopId);
    return (OrderOutPart)select(oop);
  }

  public List getOrderOutPartsForOrder(long ordId)
  {
    OrderOutPart oop = new OrderOutPart();
    oop.setOrdId(ordId);
    return selectAll(oop,"idx_oop_ord_id");
  }

  public void deleteOrderOutPart(long oopId, UserBean user)
  {
    OrderOutPart oop = new OrderOutPart();
    oop.setOopId(oopId);
    deleteBean(oop,user);
  }


  /**************************************************************************
   *
   *  Orders
   *
   *************************************************************************/

  public Order getOrder(long ordId)
  {
    Order order = new Order();
    order.setOrdId(ordId);
    return (Order)select(order);
  }

  /**
   * General order creation.  Generates an order using the merch or acr ref,
   * the source code and the user.
   */
  public Order createOrder(MerchantRef merchRef, AcrRef acrRef,
    String ordSrcCode, UserBean user)
  {
    Order order = new Order();
    order.setOrdId(getNewId());
    order.setCreatedBy(user.getLoginName());
    order.setCreateDate(getCurDate());
    order.setOrdSrcCode(ordSrcCode);
    order.setOrdStatCode(OrderStatus.OSC_NEW);
    order.setMerchNum(merchRef.getMerchNumber());
    order.setClientId(merchRef.getOlaRef().getCdRule().getClientId());
    order.setAppSeqNum(merchRef.getAppSeqNum());
    // HACK: hardcoding fed ex for now
    order.setShipperId(1493L);
    if(acrRef!=null)
    {
      order.setAcrId(acrRef.getAcrId());
    }
    persist(order,user);

    return getOrder(order.getOrdId());
  }

  /**
   * Internal order creation.  Creates an order item for the account indicated
   * by the merch num.
   */
  public Order createInternalOrder(String merchNum, UserBean user)
  {
    return createOrder(getMerchantRef(merchNum),null,OrderSource.OSRC_INTERNAL,user);
  }

  /**
   * OLA order creation.  Creates an order and order out parts requested by the
   * online app.
   */

  private static String olaEquipQs =

    " select                                    " +
    "  pt.pt_id,                                " +
    "  me.equiplendtype_code        lend_type,  " +
    "  me.merchequip_equip_quantity quantity    " +
    " from                                      " +
    "  merchequipment me,                       " +
    "  tmg_part_types pt                        " +
    " where                                     " +
    "  me.equip_model = pt.model_code           " +
    "  and me.equip_model not like 'IPPL%'      " +
    "  and me.app_seq_num = ?                   " +
    "  and me.equiplendtype_code in ( 1, 2, 4 ) ";

  private List getOlaEquipmentFromStatement(PreparedStatement ps)
    throws SQLException
  {
    ResultSet rs = null;

    try
    {
      rs = ps.executeQuery();
      List parts = new ArrayList();
      while (rs.next())
      {
        int quantity = rs.getInt("quantity");
        long ptId = rs.getLong("pt_id");
        int lendType = rs.getInt("lend_type");
        String dispCode = null;
        String pcCode = null;
        if (lendType == 1 || lendType == 4)
        {
          dispCode = Part.DC_SOLD;
        }
        else
        {
          dispCode = Part.DC_RENT;
        }
        if (lendType == 4)
        {
          pcCode = "REFURB";
        }
        else
        {
          pcCode = "NEW";
        }
        for (int i = 0; i < quantity; ++i)
        {
          OrderOutPart oop = new OrderOutPart();
          oop.setDrCode("NEW");
          oop.setRequestedPtId(ptId);
          oop.setRequestedPcCode(pcCode);
          oop.setRequestedDispCode(dispCode);
          parts.add(oop);
        }
      }
      return parts;
    }
    catch (SQLException e)
    {
      log.error("Error generating order out parts from statement: " + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
    }
  }

  public List getOlaEquipment(long appSeqNum)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = con.prepareStatement(olaEquipQs);
      ps.setLong(1,appSeqNum);
      return getOlaEquipmentFromStatement(ps);
    }
    catch (Exception e)
    {
      log.error("Error loading ola requested equipment: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }

    return null;
  }

  public Order createOlaOrder(MerchantRef merchRef, UserBean user)
  {
    Order order = null;
    List parts = getOlaEquipment(merchRef.getAppSeqNum());

    if (!parts.isEmpty())
    {
      order = createOrder(merchRef,null,OrderSource.OSRC_OLA,user);
      for (Iterator i = parts.iterator(); i.hasNext();)
      {
        OrderOutPart oop = (OrderOutPart)i.next();
        oop.setOopId(getNewId());
        oop.setOrdId(order.getOrdId());
        oop.setUserName(user.getLoginName());
        oop.setCreateDate(getCurDate());
        oop.setInternalFlag(oop.FLAG_NO);
        log.debug("persisting ola o            rder out part: " + oop);
        persist(oop,user);
      }
      order.setOrderOutParts(parts);
    }

    return order;
  }
  public Order createOlaOrder(long appSeqNum, UserBean user)
  {
    return createOlaOrder(getMerchantRef(appSeqNum),user);
  }
  public Order createOlaOrder(String merchNum, UserBean user)
  {
    return createOlaOrder(getMerchantRef(merchNum),user);
  }
  // static method for external use
  public static Order makeOlaOrder(long appSeqNum, UserBean user)
  {
    TmgDb db = new TmgDb();
    return db.createOlaOrder(appSeqNum,user);
  }

  /*
   * ACR order creation.  Creates an order and order items requested by the
   * account change request.
   */
  public Order createAcrOrder(AcrRef acrRef, UserBean user)
  {
    Order order = createOrder(getMerchantRef(acrRef.getMerchNum()),acrRef,
      OrderSource.OSRC_ACR,user);
    return order;
  }
  public Order createAcrOrder(long acrId, UserBean user)
  {
    return createAcrOrder(getAcrRef(acrId),user);
  }
  // static method for external use
  public static Order makeAcrOrder(long acrId, UserBean user)
  {
    TmgDb db = new TmgDb();
    return db.createAcrOrder(acrId,user);
  }

  private Filter getCodeExpressionFilter(String colName, String codeStr)
  {
    List filters = new ArrayList();
    Pattern pattern = Pattern.compile("([|+]?)([!]?)([a-zA-Z0-9_-]+)");
    Matcher matcher = pattern.matcher(codeStr);
    String lastOp = null;
    while (matcher.find())
    {
      String op = matcher.group(1);
      boolean isEqual = matcher.group(2).length() == 0;
      String code = matcher.group(3);
      Filter f =  new StringFilter(colName,code,isEqual);
      filters.add(f);
      lastOp = op;
    }
    if (filters.size() > 1)
    {
      boolean isAnd = lastOp != null && !lastOp.equals("|");
      CompoundFilter cf = null;
      if (isAnd)
      {
        cf = new AndFilter();
      }
      else
      {
        cf = new OrFilter();
      }
      for (Iterator i = filters.iterator(); i.hasNext();)
      {
        cf.addFilter((Filter)i.next());
      }
      return cf;
    }
    else if (filters.size() == 1)
    {
      return (Filter)filters.get(0);
    }
    return null;
  }

  public List lookupOrders(java.util.Date fromDate, java.util.Date toDate,
    long clientId, String ordSrcCode, String ordStatCode, String searchTerm)
  {
    Selection sel = getSelection(Order.class);

    sel.addFilter(new DateRangeFilter("create_ts",fromDate,toDate));

    if (clientId != 0)
    {
      sel.addFilter(new NumberFilter("client_id",clientId));
    }

    if (ordSrcCode != null && ordSrcCode.length() > 0)
    {
      sel.addFilter(new StringFilter("ord_src_code",ordSrcCode));
    }

    if (ordStatCode != null && ordStatCode.length() > 0)
    {
      sel.addFilter(new CodeFilter("ord_stat_code",ordStatCode));
      //sel.addFilter(getCodeExpressionFilter("ord_stat_code",ordStatCode));
    }

    if (searchTerm != null && searchTerm.length() > 0)
    {
      long longTerm = -1L;
      try
      {
        longTerm = Long.parseLong(searchTerm);
      }
      catch (Exception e) { }
      OrFilter orFilter = new OrFilter();
      if (longTerm != -1L)
      {
        orFilter.addFilter(new NumberFilter("app_seq_num",longTerm));
        orFilter.addFilter(new NumberFilter("acr_id",longTerm));
        orFilter.addFilter(new NumberFilter("ord_id",longTerm));
      }
      orFilter.addFilter(new StringFilter("merch_num",searchTerm));
      orFilter.addFilter(new StringFilter("created_by",searchTerm));
      StringFilter nameFilter = new StringFilter("merch_business_name",searchTerm);
      nameFilter.setIsSubstring(true);
      orFilter.addFilter(nameFilter);
      String customClause = "in ( select ord_id from tmg_call_tags where"
        + " lower( tracking_num ) = '" + searchTerm.toLowerCase() + "' )";
      orFilter.addFilter(new CustomFilter("ord_id",customClause));
      sel.addFilter(orFilter);
    }

    sel.addOrder("create_ts",true);

    return selectAll(sel);
  }


  /**************************************************************************
   *
   *  DeployReason
   *
   *************************************************************************/

  public DeployReason getDeployReason(long drId)
  {
    DeployReason dr = new DeployReason();
    dr.setDrId(drId);
    return (DeployReason)select(dr);
  }

  public DeployReason getDeployReason(String drCode)
  {
    DeployReason dr = new DeployReason();
    dr.setDrCode(drCode);
    return (DeployReason)select(dr,IDX_DR_CODE);
  }

  public List getDeployReasons()
  {
    return selectAll(DeployReason.class);
  }

  public void deleteDeployReason(long drId, UserBean user)
  {
    DeployReason dr = new DeployReason();
    dr.setDrId(drId);
    deleteBean(dr,user);
  }

  /**************************************************************************
   *
   *  CallTagNoticeType
   *
   *************************************************************************/

  public CallTagNoticeType getCallTagNoticeType(long cntId)
  {
    CallTagNoticeType cnt = new CallTagNoticeType();
    cnt.setCntId(cntId);
    return (CallTagNoticeType)select(cnt);
  }

  public CallTagNoticeType getCallTagNoticeType(String cntCode)
  {
    CallTagNoticeType cnt = new CallTagNoticeType();
    cnt.setCntCode(cntCode);
    return (CallTagNoticeType)select(cnt,"idx_cnt_code");
  }


  public List getCallTagNoticeTypes()
  {
    Selection sel = getSelection(CallTagNoticeType.class);
    sel.addOrder("cnt_code",false);
    return selectAll(sel);
  }

  public void deleteCallTagNoticeType(long cntId, UserBean user)
  {
    CallTagNoticeType cnt = new CallTagNoticeType();
    cnt.setCntId(cntId);
    deleteBean(cnt,user);
  }


  /**************************************************************************
   *
   *  CallTagNotice
   *
   *************************************************************************/

  public CallTagNotice getCallTagNotice(long cnId)
  {
    CallTagNotice cn = new CallTagNotice();
    cn.setCnId(cnId);
    return (CallTagNotice)select(cn);
  }

  public List getCallTagNotices()
  {
    return selectAll(CallTagNotice.class);
  }

  public void deleteCallTagNotice(long cnId, UserBean user)
  {
    CallTagNotice cn = new CallTagNotice();
    cn.setCnId(cnId);
    deleteBean(cn,user);
  }


  /**************************************************************************
   *
   *  CallTagParts
   *
   *************************************************************************/

  public CallTagPart getCallTagPart(long ctPartId)
  {
    CallTagPart ctPart = new CallTagPart();
    ctPart.setCtPartId(ctPartId);
    return (CallTagPart)select(ctPart);
  }

  public void deleteCallTagPart(long ctPartId, UserBean user)
  {
    CallTagPart ctPart = new CallTagPart();
    ctPart.setCtPartId(ctPartId);
    deleteBean(ctPart,user);
  }


  /**************************************************************************
   *
   *  CallTags
   *
   *************************************************************************/

  public CallTag getCallTag(long ctId)
  {
    CallTag ct = new CallTag();
    ct.setCtId(ctId);
    return (CallTag)select(ct);
  }

  /**
   * Selects deployed parts for a merchant prior to a given date
   */
  public List getDeployedPartsForMerchant(String merchNum,
    java.util.Date cutoffDate)
  {
    PartState ps = new PartState();
    Selection sel = getSelection(ps,false);
    Deployment dep = new Deployment();
    dep.setMerchNum(merchNum);
    sel.addSubIndex("idx_dply_merch_num","tmg_deployment",dep);
    sel.addFilter(new NullFilter("end_ts",true));
    sel.addFilter(new NullFilter("end_ts",true),"tmg_deployment");
    sel.addFilter(new DateFilter("start_ts",cutoffDate,DateFilter.DFT_BEFORE),
      "tmg_deployment");
    sel.addSubtableOrder("tmg_deployment","start_ts",false);
    return selectAll(sel);
  }

  private static String ctpStatQs =

    " select  ctp.part_id,                                                  " +
    "         ctp.ctp_stat_code,                                            " +
    "         ctps.description,                                             " +
    "         ctp.serial_num                                                " +
    " from    tmg_call_tag_parts ctp,                                       " +
    "         tmg_call_tags ct,                                             " +
    "         tmg_call_tag_part_statuses ctps                               " +
    " where   nvl( ctp.part_id, 0 ) > 0                                     " +
    "         and ctp.ct_id = ct.ct_id                                      " +
    "         and ctp.ct_part_id =  ( select  max( ctp2.ct_part_id )        " +
    "                                 from    tmg_call_tag_parts ctp2       " +
    "                                 where   ctp2.part_id = ctp.part_id    " +
    "                                         and ctp2.ct_id = ctp.ct_id )  " +
    "         and ctp.ctp_stat_code = ctps.ctp_stat_code                    " +
    "         and ct.ord_id = ?                                             ";

  private CallTagStatusManager getCallTagStatusManagerFromStatement(PreparedStatement ps)
    throws SQLException
  {
    ResultSet rs = null;

    try
    {
      rs = ps.executeQuery();
      CallTagStatusManager mgr = new CallTagStatusManager();
      while (rs.next())
      {
        long partId = rs.getLong("part_id");
        String statCode = rs.getString("ctp_stat_code");
        String statDesc = rs.getString("description");
        String serialNum = rs.getString("serial_num");
        mgr.addStatus(partId,statCode,statDesc,serialNum);
      }
      return mgr;
    }
    catch (SQLException e)
    {
      log.error("Error generating call tag status manager from statement: " + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
    }
  }

  public CallTagStatusManager getCallTagStatusManager(long ordId)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = con.prepareStatement(ctpStatQs);
      ps.setLong(1,ordId);
      return getCallTagStatusManagerFromStatement(ps);
    }
    catch (Exception e)
    {
      log.error("Error loading call tag status manager: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }

    return null;
  }

  /**************************************************************************
   *
   *  Deployment
   *
   *************************************************************************/

  public Deployment getDeployment(long deployId)
  {
    Deployment dep = new Deployment();
    dep.setDeployId(deployId);
    return (Deployment)select(dep);
  }

  public List lookupDeployments(java.util.Date fromDate,
    java.util.Date toDate, long clientId)
  {
    Selection sel = getSelection(Deployment.class);

    sel.addFilter(new DateRangeFilter("start_ts",fromDate,toDate));

    if (clientId != 0)
    {
      sel.addFilter(new NumberFilter("client_id",clientId));
    }

    sel.addOrder("start_ts",false);

    return selectAll(sel);
  }

  private static final String depRptQs =
    " select                                                    " +
    "   d.deploy_id           deploy_id,                        " +
    "   c.client_name         client_name,                      " +
    "   src.client_name       src_inventory,                    " +
    "   ps_recv.start_ts      receive_date,                     " +
    "   d.start_ts            deploy_date,                      " +
    "   r.description         deploy_type,                      " +
    "   m.merch_business_name merch_name,                       " +
    "   d.merch_num           merch_num,                        " +
    "   nvl(mif.dmagent,'--') assoc_num,                        " +
    "   pd.disp_name          disp_name,                        " +
    "   ps.serial_num         serial_num,                       " +
    "   pf_cat.feat_name      category,                         " +
    "   pt.description        part_description,                 " +
    "   pc.pc_name            condition,                        " +
    "   d.part_id             part_id                           " +
    " from                                                      " +
    "   tmg_deployment d,                                       " +
    "   tmg_deploy_reasons r,                                   " +
    "   tmg_clients c,                                          " +
    "   merchant m,                                             " +
    "   tmg_order_out_parts op,                                 " +
    "   tmg_part_dispositions pd,                               " +
    "   tmg_part_states ps,                                     " +
    "   tmg_buckets b,                                          " +
    "   tmg_inventories i,                                      " +
    "   tmg_clients src,                                        " +
    "   tmg_part_types pt,                                      " +
    "   tmg_part_feature_maps pfm_cat,                          " +
    "   tmg_part_features pf_cat,                               " +
    "   tmg_part_classes pc,                                    " +
    "   tmg_part_states ps_recv,                                " +
    "   tmg_op_log ol,                                          " +
    "   mif                                                     " +
    " where                                                     " +
    "   d.start_code = r.dr_code                                " +
    "   and ( d.end_code is null or d.end_code <> 'CANCEL' )    " +
    "   and d.client_id = c.client_id(+)                        " +
    "   and d.merch_num = m.merch_number                        " +
    "   and d.start_ord_id = op.ord_id(+)                       " +
    "   and d.part_id = op.part_id(+)                           " +
    "   and ps.disp_code = pd.disp_code(+)                      " +
    "   and d.part_id = ps.part_id                              " +
    "   and d.deploy_id = ps.deploy_id                          " +
    "   and ps.op_id = ol.op_id                                 " +
    "   and ol.op_code in ( 'PART_DEPLOY', 'PART_DISP_CHANGE' ) " +
    "   and ps.buck_id = b.buck_id                              " +
    "   and b.inv_id = i.inv_id                                 " +
    "   and i.client_id = src.client_id                         " +
    "   and ps.pt_id = pt.pt_id                                 " +
    "   and pt.pt_id = pfm_cat.pt_id                            " +
    "   and pfm_cat.ft_code = 'CATEGORY'                        " +
    "   and pfm_cat.feat_id = pf_cat.feat_id                    " +
    "   and ps.pc_code = pc.pc_code                             " +
    "   and d.part_id = ps_recv.part_id                         " +
    "   and ( ps_recv.prior_id = 0 or                           " +
    "         ps_recv.prior_id is null )                        " +
    "   and d.merch_num = mif.merchant_number(+)                " +
    "   and trunc(d.start_ts) between ? and ?                   ";

  public static final String depRptClientClause = " and d.client_id = ? ";
  public static final String depRptDepRsnClause = " and d.start_code = ? ";
  public static final String depRptDispCodeClause = " and ps.disp_code = ? ";
  public static final String depRptPcCodeClause = " and ps.pc_code = ? ";
  public static final String depRptCatFeatIdClause = " and pf_cat.feat_id = ? ";
  public static final String depRptPtIdClause = " and ps.pt_id = ? ";
  public static final String depRptSearchTermClause =
    " and ( ps.serial_num = ? or m.merch_number = ? or m.asso_number = ?" +
    " or lower(m.merch_business_name) like '%' || ? || '%' ) ";
  public static final String depRptOrderClause = " order by d.start_ts ";

  private List getDepRptRowsFromStmt(PreparedStatement ps)
    throws SQLException
  {
    ResultSet rs = null;

    try
    {
      rs = ps.executeQuery();
      List rows = new ArrayList();
      while (rs.next())
      {
        DeploymentReportRow row = new DeploymentReportRow();
        row.setClientName     (rs.getString("client_name"));
        row.setSourceInventory(rs.getString("src_inventory"));
        row.setReceiveDate    (Tmg.toDate(rs.getTimestamp("receive_date")));
        row.setDeployDate     (Tmg.toDate(rs.getTimestamp("deploy_date")));
        row.setDeployType     (rs.getString("deploy_type"));
        row.setMerchName      (rs.getString("merch_name"));
        row.setMerchNum       (rs.getString("merch_num"));
        row.setAssocNum       (rs.getString("assoc_num"));
        row.setDispName       (rs.getString("disp_name"));
        row.setSerialNum      (rs.getString("serial_num"));
        row.setPartCategory   (rs.getString("category"));
        row.setPartDescription(rs.getString("part_description"));
        row.setPartCondition  (rs.getString("condition"));
        row.setPartId         (rs.getLong("part_id"));
        rows.add(row);
      }
      return rows;
    }
    catch (SQLException e)
    {
      log.error("Error generating deployment report rows from statement: "
        + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
    }
  }

  private PreparedStatement prepareDepRptStatement(Connection con,
    java.util.Date fromDate, java.util.Date toDate, long clientId,
    String drCode, String dispCode, String pcCode, long catFeatId,
    long ptId, String searchTerm, String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      if (toDate == null)
      {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fromDate);
        cal.add(cal.DATE,1);
        toDate = cal.getTime();
      }
      StringBuffer qs = new StringBuffer(depRptQs);
      if (clientId != 0L) qs.append(depRptClientClause);
      if (drCode != null) qs.append(depRptDepRsnClause);
      if (dispCode != null) qs.append(depRptDispCodeClause);
      if (pcCode != null) qs.append(depRptPcCodeClause);
      if (catFeatId != 0L) qs.append(depRptCatFeatIdClause);
      if (ptId != 0L) qs.append(depRptPtIdClause);
      if (searchTerm != null) qs.append(depRptSearchTermClause);
      qs.append(" order by " + orderName + (isDesc ? " desc" : ""));
      ps = con.prepareStatement(qs.toString());
      int varCnt = 1;
      ps.setTimestamp(varCnt++,new Timestamp(fromDate.getTime()));
      ps.setTimestamp(varCnt++,new Timestamp(toDate.getTime()));
      if (clientId != 0L) ps.setLong(varCnt++,clientId);
      if (drCode != null) ps.setString(varCnt++,drCode);
      if (dispCode != null) ps.setString(varCnt++,dispCode);
      if (pcCode != null) ps.setString(varCnt++,pcCode);
      if (catFeatId != 0L) ps.setLong(varCnt++,catFeatId);
      if (ptId != 0L) ps.setLong(varCnt++,ptId);
      if (searchTerm != null)
      {
        long numTerm = 0;
        try { numTerm = Long.parseLong(searchTerm); } catch (Exception e) { }
        ps.setString(varCnt++,searchTerm);  // serial num
        ps.setLong(varCnt++,numTerm);       // merch num
        ps.setLong(varCnt++,numTerm);       // assoc num
        ps.setString(varCnt++,searchTerm.toLowerCase());  // merch name
      }
      return ps;
    }
    catch (Exception e)
    {
      log.error("Error loading deployment report rows: " + e);
      e.printStackTrace();
    }

    return null;
  }

  public List getDeploymentReportRows(java.util.Date fromDate,
    java.util.Date toDate, long clientId, String drCode, String dispCode,
    String pcCode, long catFeatId, long ptId, String searchTerm,
    String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = prepareDepRptStatement(con,fromDate,toDate,clientId,drCode,
        dispCode,pcCode,catFeatId,ptId,searchTerm,orderName,isDesc);
      return getDepRptRowsFromStmt(ps);
    }
    catch (Exception e)
    {
      log.error("Error loading deployment report rows: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }

    return null;
  }

  public void startDeploymentReportDownload(java.util.Date fromDate,
    java.util.Date toDate, long clientId, String drCode, String dispCode,
    String pcCode, long catFeatId, long ptId, String searchTerm,
    String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = prepareDepRptStatement(con,fromDate,toDate,clientId,drCode,
        dispCode,pcCode,catFeatId,ptId,searchTerm,orderName,isDesc);
      startManualStatement(ps);
    }
    catch (Exception e)
    {
      try { ps.close(); } catch (Exception ie) { }
      doDisconnect();
      log.error("Error loading deployment report rows: " + e);
      e.printStackTrace();
    }
  }


  /**************************************************************************
   *
   *  TestResultType
   *
   *************************************************************************/

  public TestResultType getTestResultType(long trtId)
  {
    TestResultType trt = new TestResultType();
    trt.setTrtId(trtId);
    return (TestResultType)select(trt);
  }

  public TestResultType getTestResultType(String trtCode)
  {
    TestResultType trt = new TestResultType();
    trt.setTrtCode(trtCode);
    return (TestResultType)select(trt,"idx_trt_code");
  }

  public List getTestResultTypes()
  {
    Selection sel = getSelection(TestResultType.class);
    sel.addOrder("trt_name",false);
    return selectAll(sel);
  }


  /**************************************************************************
   *
   *  PartTest
   *
   *************************************************************************/

  public PartTest getPartTest(long testId)
  {
    PartTest test = new PartTest();
    test.setTestId(testId);
    return (PartTest)select(test);
  }

  public PartTest getCurrentPartTest(long partId)
  {
    PartState ps = getCurrentPartState(partId);
    if (ps != null)
    {
      PartTest test = new PartTest();
      test.setStateId(ps.getStateId());
      test = (PartTest)select(test,"idx_test_state_id");
      return test;
    }
    return null;
  }

  public void deletePartTest(long testId, UserBean user)
  {
    PartTest test = new PartTest();
    test.setTestId(testId);
    deleteBean(test,user);
  }    


  /**************************************************************************
   *
   *  CallTagReason
   *
   *************************************************************************/

  public CallTagReason getCallTagReason(long ctrId)
  {
    CallTagReason ctr = new CallTagReason();
    ctr.setCtrId(ctrId);
    return (CallTagReason)select(ctr);
  }

  public CallTagReason getCallTagReason(String ctrCode)
  {
    CallTagReason ctr = new CallTagReason();
    ctr.setCtrCode(ctrCode);
    return (CallTagReason)select(ctr,"idx_ctr_code");
  }

  public List getCallTagReasons()
  {
    Selection sel = getSelection(CallTagReason.class);
    sel.addOrder("ctr_code",false);
    return selectAll(sel);
  }

  public void deleteCallTagReason(long ctrId, UserBean user)
  {
    CallTagReason ctr = new CallTagReason();
    ctr.setCtrId(ctrId);
    deleteBean(ctr,user);
  }


  /**************************************************************************
   *
   *  AcrCtrMap
   *
   *************************************************************************/

  public AcrCtrMap getAcrCtrMap(long acmId)
  {
    AcrCtrMap acm = new AcrCtrMap();
    acm.setAcmId(acmId);
    return (AcrCtrMap)select(acm);
  }

  public void deleteAllAcrCtrMaps(String atCode, UserBean user)
  {
    AcrCtrMap acm = new AcrCtrMap();
    acm.setAtCode(atCode);
    Selection sel = getSelection(acm,false);
    sel.addIndex("idx_acm_at_code");
    delete(sel,user);
  }


  /**************************************************************************
   *
   *  AcrType
   *
   *************************************************************************/

  public AcrType getAcrType(long atId)
  {
    AcrType at = new AcrType();
    at.setAtId(atId);
    return (AcrType)select(at);
  }

  public AcrType getAcrType(String atCode)
  {
    AcrType at = new AcrType();
    at.setAtCode(atCode);
    return (AcrType)select(at,"idx_at_code");
  }

  public List getAcrTypes()
  {
    Selection sel = getSelection(AcrType.class);
    sel.addOrder("at_code",false);
    return selectAll(sel);
  }

  public void deleteAcrType(long atId, UserBean user)
  {
    AcrType at = new AcrType();
    at.setAtId(atId);
    deleteBean(at,user);
  }


  /**************************************************************************
   *
   *  Maintenance Requests Report
   *
   *************************************************************************/

  private static final String mreqRptQs =
    " select                                          " +
    "  aa.acr_id            acr_id,                   " +
    "  aa.create_ts         request_ts,               " +
    "  aa.user_name         user_name,                " +
    "  c.client_name        client_name,              " +
    "  dr.description       deploy_type,              " +
    "  ctr.ctr_name         acr_reason,               " +
    "  o.merch_num          merch_num,                " +
    "  m.merch_business_name                          " +
    "                       merch_name,               " +
    "  recv_ps.part_id      part_id,                  " +
    "  recv_ps.serial_num   serial_num,               " +
    "  nvl(recv_pt.description,                       " +
    "     nvl(req_pt.description,                     " +
    "       'Unknown'))                               " +
    "                       part_description,         " +
    "  os.description       order_status,             " +
    "  ctps.description     call_tag_status,          " +
    " decode(ctp.ctp_stat_code,'RECEIVED',            " +
    "   nvl(trt.trt_name,'Not Tested'),               " +
    "   '--')                                         " +
    "                      test_result,               " +
    " nvl2(trt.trt_code,                              " +
    "   nvl2(ro.close_date_ts,                        " +
    "     'Completed',                                " +
    "     nvl2(ro.ship_date_ts,                       " +
    "       'Shipped',                                " +
    "       nvl2(roi.date_added_ts,                   " +
    "         'Started','No Repair'))),               " +
    "   '--')              repair_status              " +
    " from                                            " +
    "  tmg_acr_actions aa,                            " +
    "  tmg_call_tag_reasons ctr,                      " +
    "  tmg_call_tag_parts ctp,                        " +
    "  tmg_call_tag_part_statuses ctps,               " +
    "  tmg_orders o,                                  " +
    "  merchant m,                                    " +
    "  tmg_order_statuses os,                         " +
    "  tmg_clients c,                                 " +
    "  tmg_deploy_reasons dr,                         " +
    "  tmg_deployment d,                              " +
    "  tmg_part_states req_ps,                        " +
    "  tmg_part_types req_pt,                         " +
    "  tmg_part_states recv_ps,                       " +
    "  tmg_part_types recv_pt,                        " +
    "  tmg_repair_order_item roi,                     " +
    "  tmg_repair_order ro,                           " +
    "  tmg_part_tests tst,                            " +
    "  tmg_test_result_types trt                      " +
    " where                                           " +
    "  aa.ord_id = o.ord_id                           " +
    "  and o.ord_stat_code = os.ord_stat_code         " +
    "  and o.client_id = c.client_id                  " +
    "  and o.merch_num = m.merch_number(+)            " +
    "  and aa.ct_part_id = ctp.ct_part_id(+)          " +
    "  and ctp.ctr_code = ctr.ctr_code(+)             " +
    "  and ctp.ctp_stat_code = ctps.ctp_stat_code(+)  " +
    "  and ctp.dr_code = dr.dr_code(+)                " +
    "  and ctp.recv_part_id = d.part_id(+)            " +
    "  and ctp.ord_id = d.end_ord_id(+)               " +
    "  and ctp.part_id = req_ps.part_id(+)            " +
    "  and req_ps.end_ts is null                      " +
    "  and req_ps.pt_id = req_pt.pt_id(+)             " +
    "  and ctp.recv_part_id = recv_ps.part_id(+)      " +
    "  and recv_ps.end_ts is null                     " +
    "  and recv_ps.pt_id = recv_pt.pt_id(+)           " +
    "  and ctp.ct_part_id = tst.ct_part_id(+)         " +
    "  and tst.trt_code = trt.trt_code(+)             " +
    "  and tst.repair_item_id = roi.id(+)       " +
    "  and roi.order_id = ro.batch_id(+)              " +
    "  and trunc(aa.create_ts) between ? and ?        ";

  public static final String mreqRptClientClause = " and o.client_id = ? ";
  public static final String mreqRptSearchTermClause =
    " and ( req_ps.serial_num = ? or recv_ps.serial_num = ? " +
    " or o.merch_num = ? or lower(m.merch_business_name) " +
    " like '%' || ? || '%' ) ";

  private List getMreqRowsFromStmt(PreparedStatement ps)
    throws SQLException
  {
    ResultSet rs = null;

    try
    {
      rs = ps.executeQuery();
      List rows = new ArrayList();
      while (rs.next())
      {
        MaintReqsReportRow row = new MaintReqsReportRow();
        row.setAcrId          (rs.getLong("acr_id"));
        row.setRequestDate    (Tmg.toDate(rs.getTimestamp("request_ts")));
        row.setUserName       (rs.getString("user_name"));
        row.setClientName     (rs.getString("client_name"));
        row.setDeployType     (rs.getString("deploy_type"));
        row.setAcrReason      (rs.getString("acr_reason"));
        row.setMerchNum       (rs.getString("merch_num"));
        row.setMerchName      (rs.getString("merch_name"));
        row.setPartId         (rs.getLong("part_id"));
        row.setSerialNum      (rs.getString("serial_num"));
        row.setPartDescription(rs.getString("part_description"));
        row.setOrderStatus    (rs.getString("order_status"));
        row.setCallTagStatus  (rs.getString("call_tag_status"));
        row.setTestResult     (rs.getString("test_result"));
        row.setRepairStatus   (rs.getString("repair_status"));
        rows.add(row);
      }
      return rows;
    }
    catch (SQLException e)
    {
      log.error("Error generating maintenance requests report rows from"
        + " statement: " + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
    }
  }

  private PreparedStatement prepareMreqRptStatement(Connection con,
    java.util.Date fromDate, java.util.Date toDate, long clientId,
    String searchTerm, String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      if (toDate == null)
      {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fromDate);
        cal.add(cal.DATE,1);
        toDate = cal.getTime();
      }
      StringBuffer qs = new StringBuffer(mreqRptQs);
      if (clientId != 0L) qs.append(mreqRptClientClause);
      if (searchTerm != null) qs.append(mreqRptSearchTermClause);
      qs.append(" order by " + orderName + (isDesc ? " desc" : ""));
      ps = con.prepareStatement(qs.toString());
      int varCnt = 1;
      ps.setTimestamp(varCnt++,new Timestamp(fromDate.getTime()));
      ps.setTimestamp(varCnt++,new Timestamp(toDate.getTime()));
      if (clientId != 0L)
      {
        ps.setLong(varCnt++,clientId);
      }
      if (searchTerm != null)
      {
        ps.setString(varCnt++,searchTerm);  // req serial num
        ps.setString(varCnt++,searchTerm);  // recv serial num
        ps.setString(varCnt++,searchTerm);  // merch num
        ps.setString(varCnt++,searchTerm.toLowerCase());  // merch name
      }
      return ps;
    }
    catch (Exception e)
    {
      log.error("Error loading maintenance requests report rows: " + e);
      e.printStackTrace();
    }

    return null;
  }

  public List getMaintReqsReportRows(java.util.Date fromDate,
    java.util.Date toDate, long clientId, String searchTerm,
    String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = prepareMreqRptStatement(con,fromDate,toDate,clientId,
            searchTerm,orderName,isDesc);
      return getMreqRowsFromStmt(ps);
    }
    catch (Exception e)
    {
      log.error("Error loading maintenance requests report rows: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }

    return null;
  }

  public void startMaintReqsReportDownload(java.util.Date fromDate,
    java.util.Date toDate, long clientId, String searchTerm,
    String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = prepareMreqRptStatement(con,fromDate,toDate,clientId,
            searchTerm,orderName,isDesc);
      startManualStatement(ps);
    }
    catch (Exception e)
    {
      try { ps.close(); } catch (Exception ie) { }
      doDisconnect();
      log.error("Error starting maintenance requests report download: " + e);
      e.printStackTrace();
    }
  }

  /**************************************************************************
   *
   *  On Hand Report
   *
   *************************************************************************/

  private static final String ohRptQs =
    " select                                               " +
    "  pt.pt_name                           description,   " +
    "  sum(decode(ps.pc_code,'NEW',1,0))    new,           " +
    "  sum(decode(ps.pc_code,'USED',1,0))   used,          " +
    "  sum(decode(ps.pc_code,'REFURB',1,0)) refurbished,   " +
    "  count(ps.pc_code)                    total          " +
    " from                                                 " +
    "  tmg_inventories i,                                  " +
    "  tmg_buckets b,                                      " +
    "  tmg_part_states ps,                                 " +
    "  tmg_part_types pt                                   " +
    " where                                                " +
    "  i.client_id = ? and                                 " +
    "  i.inv_id = b.inv_id and                             " +
    "  b.buck_id = ps.buck_id and                          " +
    "  ps.status_code <> 'BUSY' and                        " +
    "  ps.loc_code = 'WAREHOUSE' and                       " +
    "  ps.lost_flag <> 'y' and                             " +
    "  b.pt_id = pt.pt_id and                              " +
    "  ps.start_ts <= ? and                                " +
    "  (ps.end_ts is null or ps.end_ts >= ?)               ";

  private static final String ohRptGroupClause = " group by pt.pt_name ";

  private PreparedStatement prepareOhRptStatement(Connection con,
    java.util.Date theDate, long clientId, String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      StringBuffer qs = new StringBuffer(ohRptQs);
      qs.append(ohRptGroupClause);
      qs.append(" order by " + orderName + (isDesc ? " desc" : ""));
      ps = con.prepareStatement(qs.toString());
      int varCnt = 1;
      ps.setLong(varCnt++,clientId);
      ps.setTimestamp(varCnt++,new Timestamp(theDate.getTime()));
      ps.setTimestamp(varCnt++,new Timestamp(theDate.getTime()));
      return ps;
    }
    catch (Exception e)
    {
      log.error("Error preparing on hand report statement: " + e);
      e.printStackTrace();
    }

    return null;
  }

  private List getOhRowsFromStmt(PreparedStatement ps)
    throws SQLException
  {
    ResultSet rs = null;

    try
    {
      rs = ps.executeQuery();
      List rows = new ArrayList();
      while (rs.next())
      {
        OnHandReportRow row = new OnHandReportRow();
        row.setDescription  (rs.getString("description"));
        row.setNew          (rs.getInt("new"));
        row.setUsed         (rs.getInt("used"));
        row.setRefurbished  (rs.getInt("refurbished"));
        row.setTotal        (rs.getInt("total"));
        rows.add(row);
      }
      return rows;
    }
    catch (SQLException e)
    {
      log.error("Error generating on hand report rows from"
        + " statement: " + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
    }
  }

  public List getOhReportRows(java.util.Date theDate, long clientId,
    String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = prepareOhRptStatement(con,theDate,clientId,orderName,isDesc);
      return getOhRowsFromStmt(ps);
    }
    catch (Exception e)
    {
      log.error("Error loading on hand report rows: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }

    return null;
  }

  public void startOhReportDownload(java.util.Date theDate,
    long clientId, String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = prepareOhRptStatement(con,theDate,clientId,orderName,isDesc);
      startManualStatement(ps);
    }
    catch (Exception e)
    {
      try { ps.close(); } catch (Exception ie) { }
      doDisconnect();
      log.error("Error starting on hand report download: " + e);
      e.printStackTrace();
    }
  }

  /**************************************************************************
   *
   *  Call Tag Report
   *
   *************************************************************************/

  private static final String ctRptQs =
    " select                                                  " +
    "  ctp.create_ts              call_tag_ts,                " +
    "  c.client_name              client_name,                " +
    "  mif.merchant_number        merch_num,                  " +
    "  mif.dba_name               merch_name,                 " +
    "  mif.dmagent                assoc_num,                  " +
    "  dr.description             call_tag_reason,            " +
    "  nvl(ct.tracking_num,'--')  call_tag_num,               " +
    "  nvl(ctp.recv_serial_num,nvl(ctp.serial_num,'--'))      " +
    "                             serial_num,                 " +
    "  nvl(recv_pt.pt_name,nvl(req_pt.pt_name,'--'))          " +
    "                             description,                " +
    "  nvl(pp.part_price,0)       unit_cost,                  " +
    "  ctps.description           call_tag_status             " +
    " from                                                    " +
    "  tmg_call_tag_parts         ctp,                        " +
    "  tmg_call_tags              ct,                         " +
    "  tmg_orders                 o,                          " +
    "  tmg_clients                c,                          " +
    "  tmg_deploy_reasons         dr,                         " +
    "  mif,                                                   " +
    "  tmg_part_types             req_pt,                     " +
    "  tmg_part_types             recv_pt,                    " +
    "  tmg_part_prices            pp,                         " +
    "  tmg_call_tag_part_statuses ctps                        " +
    " where                                                   " +
    "  trunc(ctp.create_ts) between ? and ?                   " +
    "  and ctp.ct_id = ct.ct_id                               " +
    "  and ctp.ord_id = o.ord_id                              " +
    "  and o.client_id = c.client_id                          " +
    "  and o.merch_num = mif.merchant_number                  " +
    "  and ctp.dr_code = dr.dr_code                           " +
    "  and ctp.recv_pt_id = recv_pt.pt_id(+)                  " +
    "  and ctp.pt_id = req_pt.pt_id(+)                        " +
    "  and ctp.part_id = pp.part_id(+)                        " +
    "  and ctp.ctp_stat_code = ctps.ctp_stat_code             ";

  public static final String ctRptClientClause = " and o.client_id = ? ";
  public static final String ctRptDrClause = " and dr.dr_code = ? ";
  public static final String ctRptCtpsClause = " and ctps.description = ? ";
  public static final String ctRptTermClause = 
    " and (o.merch_num = ?                                          " +
    "      or ((ctp.recv_serial_num is null and ctp.serial_num = ?) " +
    "          or ctp.recv_serial_num = ?)                          " +
    "      or lower(mif.dba_name) like '%'||?||'%')                 ";

  private PreparedStatement prepareCtRptStatement(Connection con,
    java.util.Date fromDate, java.util.Date toDate, long clientId, 
    String drCode, String ctpsDesc, String searchTerm, String orderName, 
    boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      if (toDate == null)
      {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fromDate);
        cal.add(cal.DATE,1);
        toDate = cal.getTime();
      }
      StringBuffer qs = new StringBuffer(ctRptQs);
      if (clientId != 0L) qs.append(ctRptClientClause);
      if (drCode != null) qs.append(ctRptDrClause);
      if (ctpsDesc != null) qs.append(ctRptCtpsClause);
      if (searchTerm != null) qs.append(ctRptTermClause);
      qs.append(" order by " + orderName + (isDesc ? " desc" : ""));
      ps = con.prepareStatement(qs.toString());
      int varCnt = 1;
      ps.setTimestamp(varCnt++,new Timestamp(fromDate.getTime()));
      ps.setTimestamp(varCnt++,new Timestamp(toDate.getTime()));
      if (clientId != 0L) ps.setLong(varCnt++,clientId);
      if (drCode != null) ps.setString(varCnt++,drCode);
      if (ctpsDesc != null) ps.setString(varCnt++,ctpsDesc);
      if (searchTerm != null)
      {
        ps.setString(varCnt++,searchTerm);
        ps.setString(varCnt++,searchTerm);
        ps.setString(varCnt++,searchTerm);
        ps.setString(varCnt++,searchTerm.toLowerCase());
      }
      return ps;
    }
    catch (Exception e)
    {
      log.error("Error preparing call tag report statement: " + e);
      e.printStackTrace();
    }

    return null;
  }

  private List getCtRowsFromStmt(PreparedStatement ps)
    throws SQLException
  {
    ResultSet rs = null;

    try
    {
      rs = ps.executeQuery();
      List rows = new ArrayList();
      while (rs.next())
      {
        CallTagReportRow row = new CallTagReportRow();
        row.setCreateDate   (Tmg.toDate(rs.getTimestamp("call_tag_ts")));
        row.setClientName   (rs.getString("client_name"));
        row.setMerchNum     (rs.getString("merch_num"));
        row.setMerchName    (rs.getString("merch_name"));
        row.setAssocNum     (rs.getString("assoc_num"));
        row.setCallTagReason(rs.getString("call_tag_reason"));
        row.setCallTagNum   (rs.getString("call_tag_num"));
        row.setSerialNum    (rs.getString("serial_num"));
        row.setDescription  (rs.getString("description"));
        row.setUnitCost     (rs.getDouble("unit_cost"));
        row.setCallTagStatus(rs.getString("call_tag_status"));
        rows.add(row);
      }
      return rows;
    }
    catch (SQLException e)
    {
      log.error("Error generating call tag report rows from"
        + " statement: " + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
    }
  }

  public List getCtReportRows(java.util.Date fromDate, java.util.Date toDate,
    long clientId, String drCode, String ctpsDesc, String searchTerm, 
    String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = prepareCtRptStatement(con,fromDate,toDate,clientId,drCode,
        ctpsDesc,searchTerm,orderName,isDesc);
      return getCtRowsFromStmt(ps);
    }
    catch (Exception e)
    {
      log.error("Error loading call tag report rows: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }

    return null;
  }

  public void startCtReportDownload(java.util.Date fromDate,
    java.util.Date toDate, long clientId, String drCode, String ctpsDesc,
    String searchTerm, String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = prepareCtRptStatement(con,fromDate,toDate,clientId,drCode,
        ctpsDesc,searchTerm,orderName,isDesc);
      startManualStatement(ps);
    }
    catch (Exception e)
    {
      try { ps.close(); } catch (Exception ie) { }
      doDisconnect();
      log.error("Error starting call tag report download: " + e);
      e.printStackTrace();
    }
  }

  
  /**************************************************************************
   *
   *  Repair Report
   *
   *************************************************************************/

  private static final String rprRptQs =
    " select                                                    " +
    "  pt.pt_name             pt_name,                          " +
    "  ps.serial_num          serial_num,                       " +
    "  ps.part_id             part_id,                          " +
    "  c.client_name          client_name,                      " +
    "  roi.order_id           batch_num,                        " +
    "  roi.date_added_ts      batch_ts,                         " +
    "  ro.ship_date_ts        ship_ts,                          " +
    "  nvl(trt.trt_name,nvl(roi.problem_as_tested,'--'))        " +
    "                         repair_reason,                    " +
    "  nvl(aa.acr_id,0)       acr_id,                           " +
    "  cur_ps.status_code     cur_status_code,                  " +
    "  cur_ps.disp_code       cur_disp_code,                    " +
    "  cur_ps.loc_code        cur_loc_code                      " +
    " from                                                      " +
    "  tmg_part_states        ps,                               " +
    "  tmg_part_types         pt,                               " +
    "  tmg_part_states        cur_ps,                           " +
    "  tmg_op_log             ol,                               " +
    "  tmg_repair_order_item  roi,                              " +
    "  tmg_repair_order       ro,                               " +
    "  tmg_part_tests         tst,                              " +
    "  tmg_test_result_types  trt,                              " +
    "  tmg_buckets            b,                                " +
    "  tmg_inventories        i,                                " +
    "  tmg_acr_actions        aa,                               " +
    "  tmg_clients            c                                 " +
    " where                                                     " +
    "  trunc(ro.ship_date_ts) between ? and ?                   " +
    "  and ps.op_id = ol.op_id                                  " +
    "  and ol.op_code = 'PART_ADD_REPAIR_ORDER'                 " +
    "  and ps.pt_id = pt.pt_id                                  " +
    "  and ps.serial_num = roi.ser_num                          " +
    "  and roi.date_added_ts between ps.start_ts and ps.end_ts  " +
    "  and roi.order_id = ro.batch_id                           " +
    "  and ro.ship_date_ts is not null                          " +
    "  and roi.id = tst.repair_item_id(+)                       " +
    "  and tst.trt_code = trt.trt_code(+)                       " +
    "  and ps.buck_id = b.buck_id                               " +
    "  and b.inv_id = i.inv_id                                  " +
    "  and i.client_id = c.client_id                            " +
    "  and ps.part_id = cur_ps.part_id                          " +
    "  and cur_ps.end_ts is null                                " +
    "  and tst.ct_part_id = aa.ct_part_id(+)                    ";

  public static final String rprRptClientClause = " and c.client_id = ? ";
  public static final String rprRptTermClause = 
    " and (ps.serial_num = ? or ro.batch_id = ?)";

  private PreparedStatement prepareRprRptStatement(Connection con,
    java.util.Date fromDate, java.util.Date toDate, long clientId, 
    String searchTerm, String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      if (toDate == null)
      {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fromDate);
        cal.add(cal.DATE,1);
        toDate = cal.getTime();
      }
      StringBuffer qs = new StringBuffer(rprRptQs);
      if (clientId != 0L) qs.append(rprRptClientClause);
      if (searchTerm != null) qs.append(rprRptTermClause);
      qs.append(" order by " + orderName + (isDesc ? " desc" : ""));
      ps = con.prepareStatement(qs.toString());
      int varCnt = 1;
      ps.setTimestamp(varCnt++,new Timestamp(fromDate.getTime()));
      ps.setTimestamp(varCnt++,new Timestamp(toDate.getTime()));
      if (clientId != 0L) ps.setLong(varCnt++,clientId);
      if (searchTerm != null)
      {
        ps.setString(varCnt++,searchTerm);
        long numTerm = 0L;
        try { numTerm = Long.parseLong(searchTerm); } catch(Exception ne) {}
        ps.setLong(varCnt++,numTerm);
      }
      return ps;
    }
    catch (Exception e)
    {
      log.error("Error preparing repair report statement: " + e);
      e.printStackTrace();
    }

    return null;
  }

  private List getRprRowsFromStmt(PreparedStatement ps)
    throws SQLException
  {
    ResultSet rs = null;

    try
    {
      rs = ps.executeQuery();
      List rows = new ArrayList();
      while (rs.next())
      {
        RepairReportRow row = new RepairReportRow();
        row.setModelCode    (rs.getString("pt_name"));
        row.setSerialNum    (rs.getString("serial_num"));
        row.setClientName   (rs.getString("client_name"));
        row.setBatchNum     (rs.getLong("batch_num"));
        row.setBatchDate    (Tmg.toDate(rs.getTimestamp("batch_ts")));
        row.setShipDate     (Tmg.toDate(rs.getTimestamp("ship_ts")));
        row.setRepairReason (rs.getString("repair_reason"));
        row.setAcrId        (rs.getLong("acr_id"));
        row.setPartId       (rs.getLong("part_id"));
        rows.add(row);
      }
      return rows;
    }
    catch (SQLException e)
    {
      log.error("Error generating repair report rows from"
        + " statement: " + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
    }
  }

  public List getRprReportRows(java.util.Date fromDate, java.util.Date toDate,
    long clientId, String searchTerm, String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = prepareRprRptStatement(con,fromDate,toDate,clientId,
        searchTerm,orderName,isDesc);
      return getRprRowsFromStmt(ps);
    }
    catch (Exception e)
    {
      log.error("Error loading repair report rows: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }

    return null;
  }

  public void startRprReportDownload(java.util.Date fromDate,
    java.util.Date toDate, long clientId, String searchTerm, 
    String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = prepareRprRptStatement(con,fromDate,toDate,clientId,
        searchTerm,orderName,isDesc);
      startManualStatement(ps);
    }
    catch (Exception e)
    {
      try { ps.close(); } catch (Exception ie) { }
      doDisconnect();
      log.error("Error starting repair report download: " + e);
      e.printStackTrace();
    }
  }


  /**************************************************************************
   *
   *  Lost Parts Report
   *
   *************************************************************************/

  private static final String lostRptQs =
    " select                                                  " +
    "  ps.serial_num,                                         " +
    "  ps.part_id,                                            " +
    "  pt.pt_name,                                            " +
    "  pt.model_code,                                         " +
    "  c.client_name,                                         " +
    "  ps.start_ts,                                           " +
    "  ps.end_ts,                                             " +
    "  ol.op_code,                                            " +
    "  ol.op_user                                             " +
    " from                                                    " +
    "  tmg_part_states ps,                                    " +
    "  tmg_part_states ps_p,                                  " +
    "  tmg_part_states ps_c,                                  " +
    "  tmg_buckets b,                                         " +
    "  tmg_inventories i,                                     " +
    "  tmg_clients c,                                         " +
    "  tmg_part_types pt,                                     " +
    "  tmg_op_log ol                                          " +
    " where                                                   " +
    "  ps.lost_flag = 'y'                                     " +
    "  and ps.prior_id = ps_p.state_id(+)                     " +
    "  and (ps_p.lost_flag <> 'y' or ps_p.lost_flag is null)  " +
    "  and ps.part_id = ps_c.part_id                          " +
    "  and ps_c.end_ts is null                                " +
    "  and ps_c.lost_flag = 'y'                               " +
    "  and ps.buck_id = b.buck_id                             " +
    "  and b.inv_id = i.inv_id                                " +
    "  and i.client_id = c.client_id                          " +
    "  and ps.pt_id = pt.pt_id                                " +
    "  and ps.op_id = ol.op_id                                " +
    "  and ol.op_code <> 'PART_IMPORT_ADD'                    " +
    "  and trunc(ps.start_ts)  between ? and ?                " +
    "";

  public static final String lostRptClientClause = " and c.client_id = ? ";

  private PreparedStatement prepareLostRptStatement(Connection con,
    java.util.Date fromDate, java.util.Date toDate, long clientId, 
    String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      if (toDate == null)
      {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fromDate);
        cal.add(cal.DATE,1);
        toDate = cal.getTime();
      }
      StringBuffer qs = new StringBuffer(lostRptQs);
      if (clientId != 0L) qs.append(lostRptClientClause);
      qs.append(" order by " + orderName + (isDesc ? " desc" : ""));
      ps = con.prepareStatement(qs.toString());
      int varCnt = 1;
      ps.setTimestamp(varCnt++,new Timestamp(fromDate.getTime()));
      ps.setTimestamp(varCnt++,new Timestamp(toDate.getTime()));
      if (clientId != 0L) ps.setLong(varCnt++,clientId);
      return ps;
    }
    catch (Exception e)
    {
      log.error("Error preparing lost parts report statement: " + e);
      e.printStackTrace();
    }

    return null;
  }

  private List getLostRowsFromStmt(PreparedStatement ps)
    throws SQLException
  {
    ResultSet rs = null;

    try
    {
      rs = ps.executeQuery();
      List rows = new ArrayList();
      while (rs.next())
      {
        LostPartsReportRow row = new LostPartsReportRow();
        row.setLostDate     (Tmg.toDate(rs.getTimestamp("start_ts")));
        row.setPartId       (rs.getLong("part_id"));
        row.setSerialNum    (rs.getString("serial_num"));
        row.setClientName   (rs.getString("client_name"));
        row.setPtName       (rs.getString("pt_name"));
        row.setModelCode    (rs.getString("model_code"));
        rows.add(row);
      }
      return rows;
    }
    catch (SQLException e)
    {
      log.error("Error generating lost parts report rows from"
        + " statement: " + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
    }
  }

  public List getLostReportRows(java.util.Date fromDate, java.util.Date toDate,
    long clientId, String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = prepareLostRptStatement(con,fromDate,toDate,clientId,
        orderName,isDesc);
      return getLostRowsFromStmt(ps);
    }
    catch (Exception e)
    {
      log.error("Error loading lost parts report rows: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }

    return null;
  }

  public void startLostReportDownload(java.util.Date fromDate,
    java.util.Date toDate, long clientId, String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = prepareLostRptStatement(con,fromDate,toDate,clientId,
        orderName,isDesc);
      startManualStatement(ps);
    }
    catch (Exception e)
    {
      try { ps.close(); } catch (Exception ie) { }
      doDisconnect();
      log.error("Error starting lost parts report download: " + e);
      e.printStackTrace();
    }
  }


  /**************************************************************************
   *
   *  Inventory Growth Report
   *
   *************************************************************************/

  private static final String growthRptQs =
    " select                                          " +
    "  c.client_name            client_name,          " +
    "  pt.pt_name               pt_name,              " +
    "  sysdate                  start_date,           " +
    "  nvl(sc.part_count,0)     start_count,          " +
    "  sysdate                  end_date,             " +
    "  nvl(ec.part_count,0)     end_count,            " +
    "  (nvl(ec.part_count,0) -                        " +
    "    nvl(sc.part_count,0))  growth_count          " +
    " from                                            " +
    "  (select                                        " +
    "    pt.pt_id,                                    " +
    "    c.client_id,                                 " +
    "    count(ps.state_id) part_count                " +
    "   from                                          " +
    "    tmg_part_states ps,                          " +
    "    tmg_part_types pt,                           " +
    "    tmg_buckets b,                               " +
    "    tmg_inventories i,                           " +
    "    tmg_clients c                                " +
    "   where                                         " +
    "    (ps.prior_id is null or ps.prior_id = 0)     " +
    "    and trunc(ps.start_ts) <= ?                  " +
    "    and ps.pt_id = pt.pt_id                      " +
    "    and ps.buck_id = b.buck_id                   " +
    "    and b.inv_id = i.inv_id                      " +
    "    and i.client_id = c.client_id                " +
    "   group by pt.pt_id, c.client_id) sc,           " +
    "  (select                                        " +
    "    pt.pt_id,                                    " +
    "    c.client_id,                                 " +
    "    count(ps.state_id) part_count                " +
    "   from                                          " +
    "    tmg_part_states ps,                          " +
    "    tmg_part_types pt,                           " +
    "    tmg_buckets b,                               " +
    "    tmg_inventories i,                           " +
    "    tmg_clients c                                " +
    "   where                                         " +
    "    (ps.prior_id is null or ps.prior_id = 0)     " +
    "    and trunc(ps.start_ts) <= ?                  " +
    "    and ps.pt_id = pt.pt_id                      " +
    "    and ps.buck_id = b.buck_id                   " +
    "    and b.inv_id = i.inv_id                      " +
    "    and i.client_id = c.client_id                " +
    "   group by pt.pt_id, c.client_id) ec,           " +
    "  tmg_part_types pt,                             " +
    "  tmg_clients c                                  " +
    " where                                           " +
    "  pt.pt_id = ec.pt_id                            " +
    "  and c.client_id = ec.client_id                 " +
    "  and ec.pt_id = sc.pt_id(+)                     " +
    "  and ec.client_id = sc.client_id(+)             " +
    "";

  public static final String growthRptPtClause = " and pt.pt_id = ? ";
  public static final String growthRptClientClause = " and c.client_id = ? ";

  private PreparedStatement prepareGrowthRptStatement(Connection con,
    java.util.Date fromDate, java.util.Date toDate, long clientId, long ptId,
    String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      if (toDate == null)
      {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fromDate);
        cal.add(cal.DATE,1);
        toDate = cal.getTime();
      }
      StringBuffer qs = new StringBuffer(growthRptQs);
      if (ptId != 0L) qs.append(growthRptPtClause);
      if (clientId != 0L) qs.append(growthRptClientClause);
      qs.append(" order by " + orderName + (isDesc ? " desc" : ""));
      ps = con.prepareStatement(qs.toString());
      int varCnt = 1;
      ps.setTimestamp(varCnt++,new Timestamp(fromDate.getTime()));
      ps.setTimestamp(varCnt++,new Timestamp(toDate.getTime()));
      if (ptId != 0L) ps.setLong(varCnt++,ptId);
      if (clientId != 0L) ps.setLong(varCnt++,clientId);
      return ps;
    }
    catch (Exception e)
    {
      log.error("Error preparing inventory growth report statement: " + e);
      e.printStackTrace();
    }

    return null;
  }

  private List getGrowthRowsFromStmt(PreparedStatement ps, 
    java.util.Date startDate, java.util.Date endDate)
    throws SQLException
  {
    ResultSet rs = null;

    try
    {
      rs = ps.executeQuery();
      List rows = new ArrayList();
      while (rs.next())
      {
        InvGrowthReportRow row = new InvGrowthReportRow();
        row.setClientName   (rs.getString("client_name"));
        row.setPtName       (rs.getString("pt_name"));
        row.setStartDate    (startDate);
        row.setStartCount   (rs.getLong("start_count"));
        row.setEndDate      (endDate);
        row.setEndCount     (rs.getLong("end_count"));
        row.setGrowthCount  (rs.getLong("growth_count"));
        rows.add(row);
      }
      return rows;
    }
    catch (SQLException e)
    {
      log.error("Error generating lost parts report rows from"
        + " statement: " + e);
      e.printStackTrace();
      throw e;
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
    }
  }

  public List getGrowthReportRows(java.util.Date fromDate, java.util.Date toDate,
    long clientId, long ptId, String orderName, boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = prepareGrowthRptStatement(con,fromDate,toDate,clientId,ptId,
        orderName,isDesc);
      return getGrowthRowsFromStmt(ps,fromDate,toDate);
    }
    catch (Exception e)
    {
      log.error("Error loading lost parts report rows: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      doDisconnect();
    }

    return null;
  }

  public void startGrowthReportDownload(java.util.Date fromDate,
    java.util.Date toDate, long clientId, long ptId, String orderName, 
    boolean isDesc)
  {
    PreparedStatement ps = null;

    try
    {
      Connection con = getConnection();
      ps = prepareGrowthRptStatement(con,fromDate,toDate,clientId,ptId,
        orderName,isDesc);
      startManualStatement(ps);
    }
    catch (Exception e)
    {
      try { ps.close(); } catch (Exception ie) { }
      doDisconnect();
      log.error("Error starting lost parts report download: " + e);
      e.printStackTrace();
    }
  }


  /**************************************************************************
   *
   *  Service Calls
   *
   *************************************************************************/

  public void createServiceCall(String merchNum, String text, UserBean user)
  {
    PreparedStatement ps = null;

    try
    {

      // based on com.mes.inventory.EndOfDayUpload.insertServiceCalls()
      Connection con = getConnection();
      ps = con.prepareStatement(
        " insert into service_calls (           " +
        "   merchant_number,                    " +
        "   call_date,                          " +
        "   type,                               " +
        "   other_description,                  " +
        "   login_name,                         " +
        "   call_back,                          " +
        "   status,                             " +
        "   notes,                              " +
        "   billable,                           " +
        "   client_generated )                  " +
        " values (                              " +
        "   ?,sysdate,26,'',?,'N',2,?,'N','N' ) ");

      ps.setString(1,merchNum);
      ps.setString(2,user.getLoginName());
      ps.setString(3,text);
      ps.executeUpdate();
    }
    catch (Exception e)
    {
      log.error("Error creating service call: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { ps.close(); } catch (Exception ie) { }
      doDisconnect();
    }
  }
}