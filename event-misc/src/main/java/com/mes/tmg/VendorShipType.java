package com.mes.tmg;

public class VendorShipType extends TmgBean
{
  private long vstId;
  private long vendorId;
  private long shipTypeId;
  private ShipType shipType;

  public void setVstId(long vstId)
  {
    this.vstId = vstId;
  }
  public long getVstId()
  {
    return vstId;
  }
  
  public void setVendorId(long vendorId)
  {
    this.vendorId = vendorId;
  }
  public long getVendorId()
  {
    return vendorId;
  }
  
  public void setShipTypeId(long shipTypeId)
  {
    this.shipTypeId = shipTypeId;
  }
  public long getShipTypeId()
  {
    return shipTypeId;
  }

  public void setShipType(ShipType shipType)
  {
    this.shipType = shipType;
  }
  public ShipType getShipType()
  {
    return shipType;
  }

  public String toString()
  {
    return "VendorShipType ["
      + " id: " + vstId
      + " vendor: " + vendorId
      + " ship type: " + shipType
      + " ]";
  }

}