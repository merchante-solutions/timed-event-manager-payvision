package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Note extends TmgBean
{
  private long    noteId;
  private long    id;
  private Date    createDate;
  private String  userName;
  private String  noteText;
  private List    documents;

  public Note()
  {
  }

  public int hashCode()
  {
    return Long.valueOf(noteId).hashCode();
  }

  public boolean equals(Object that)
  {
    boolean isEqual = false;
    if (that instanceof Note)
    {
      Note thatNote = (Note)that;
      isEqual = thatNote.noteId == this.noteId;
    }
    return isEqual;
  }

  public void setNoteId(long noteId)
  {
    this.noteId = noteId;
  }
  public long getNoteId()
  {
    return noteId;
  }

  public void setId(long id)
  {
    this.id = id;
  }
  public long getId()
  {
    return id;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setNoteText(String noteText)
  {
    this.noteText = noteText;
  }
  public String getNoteText()
  {
    return noteText;
  }

  public String getNoteTextHtml()
  {
    StringBuffer html = new StringBuffer();
    for (int i = 0; i < noteText.length(); ++i)
    {
      String ss = noteText.substring(i,i + 1);
      html.append(ss.equals("\n") ? "<br>" : ss);
    }
    return html.toString();
  }

  public List getDocuments()
  {
    return documents;
  }
  public void setDocuments(List documents)
  {
    this.documents = documents;
  }
  public void addDocument(Document doc)
  {
    documents.add(doc);
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append(this.getClass().getName() + " [ ");
    buf.append("note: " + noteId);
    buf.append(", id: " + id);
    buf.append(", created: " + formatDate(createDate));
    buf.append(", user: " + userName);
    buf.append(", text: " + noteText);
    if (documents != null)
    {
      buf.append(", docs: [ ");
      for (Iterator i = documents.iterator(); i.hasNext();)
      {
        buf.append("" + i.next() + (i.hasNext() ? ", " : ""));
      }
      buf.append(" ]");
    }
    buf.append(" ]");
    return buf.toString();
  }
}
