package com.mes.tmg;

public class MiscAddSource
{
  private long    masId;
  private String  masCode;
  private String  masName;

  public void setMasId(long masId)
  {
    this.masId = masId;
  }
  public long getMasId()
  {
    return masId;
  }
  
  public void setMasCode(String masCode)
  {
    this.masCode = masCode;
  }
  public String getMasCode()
  {
    return masCode;
  }

  public void setMasName(String masName)
  {
    this.masName = masName;
  }
  public String getMasName()
  {
    return masName;
  }

  public String toString()
  {
    return "MiscAddSource [ "
      + "id: " + masId 
      + ", code: " + masCode
      + ", name: " + masName 
      + " ]";
  }
}