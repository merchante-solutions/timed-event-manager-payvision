package com.mes.tmg;

public class CallTagNoticeType extends TmgBean
{
  private long cntId;
  private String cntCode;
  private String description;
  private String template;

  public void setCntId(long cntId)
  {
    this.cntId = cntId;
  }
  public long getCntId()
  {
    return cntId;
  }

  public void setCntCode(String cntCode)
  {
    this.cntCode = cntCode;
  }
  public String getCntCode()
  {
    return cntCode;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public void setTemplate(String template)
  {
    this.template = template;
  }
  public String getTemplate()
  {
    return template;
  }

  public String toString()
  {
    return "CallTagNoticeType ["
      + " id: " + cntId
      + ", code: " + cntCode
      + ", desc: " + description
      + ", template: " + template
      + " ]";
  }
}