package com.mes.tmg;

import java.util.HashMap;
import java.util.Map;

public class AuditResolution
{
  public static final AuditResolution PART_LOST
                        = new AuditResolution("PART_LOST","Part Lost");
  public static final AuditResolution PART_FOUND
                        = new AuditResolution("PART_FOUND","Part Found");
  public static final AuditResolution SN_CORRECTION
                        = new AuditResolution("SN_CORRECTION","SN Correction");
  public static final AuditResolution MISC_ADDITION
                        = new AuditResolution("MISC_ADDITION","Misc. Addition");
  public static final AuditResolution DB_CORRECTION
                        = new AuditResolution("DB_CORRECTION","DB Correction");
  public static final AuditResolution PART_TRANSFER
                        = new AuditResolution("PART_TRANSFER","Part Transfer");
  public static final AuditResolution SCAN_ERROR
                        = new AuditResolution("SCAN_ERROR","Scan Error");

  private static Map nameMap;

  public static AuditResolution getByName(String name)
  {
    return (AuditResolution)nameMap.get(name);
  }

  private String description;
  private String name;

  private AuditResolution(String name, String description)
  {
    this.name = name;
    this.description = description;
    if (nameMap == null)
    {
      nameMap = new HashMap();
    }
    nameMap.put(this.name,this);
  }

  public String getName()
  {
    return name;
  }

  public String getDescription()
  {
    return description;
  }

  public String getActionName()
  {
    StringBuffer buf = new StringBuffer("audit");
    String[] parts = name.split("_");
    for (int i = 0; i < parts.length; ++i)
    {
      buf.append(parts[i].substring(0,1).toUpperCase());
      buf.append(parts[i].substring(1).toLowerCase());
    }
    return buf.toString();
  }

  public String toString()
  {
    return "AuditResolution [ "
      + "name: " + name 
      + ", desc: " + description 
      + " ]";
  }
}
