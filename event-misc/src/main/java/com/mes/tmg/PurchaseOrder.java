package com.mes.tmg;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.mes.mvc.RequestHandler;
import com.mes.tools.DropDownItem;

public class PurchaseOrder extends TmgBean
{
  public static final int   PO_STAT_NONE      = 0;
  public static final int   PO_STAT_EDITING   = 1;
  public static final int   PO_STAT_PENDING   = 2;
  public static final int   PO_STAT_READY     = 3;
  public static final int   PO_STAT_CONFIRMED = 4;
  public static final int   PO_STAT_RECEIVING = 5;
  public static final int   PO_STAT_COMPLETED = 6;
  public static final int   PO_STAT_PAID      = 7;
  public static final int   PO_STAT_CANCELED  = 99;

  public static String getPoStatusStr(int status)
  {
    switch (status)
    {
      case PO_STAT_NONE:        return "None";
      case PO_STAT_CANCELED:    return "Canceled";
      case PO_STAT_EDITING:     return "Editing";
      case PO_STAT_READY:       return "Ready";
      case PO_STAT_CONFIRMED:   return "Confirmed";
      case PO_STAT_RECEIVING:   return "Receiving";
      case PO_STAT_COMPLETED:   return "Completed";
      case PO_STAT_PAID:        return "Paid";
      case PO_STAT_PENDING:     return "Pending";
    }
    return "Unknown status value: " + status;
  }

  public static DropDownItem[] statusItems = new DropDownItem[]
  {
    new DropDownItem(PO_STAT_EDITING,   getPoStatusStr(PO_STAT_EDITING  )),
    new DropDownItem(PO_STAT_PENDING,   getPoStatusStr(PO_STAT_PENDING  )),
    new DropDownItem(PO_STAT_READY,     getPoStatusStr(PO_STAT_READY    )),
    new DropDownItem(PO_STAT_CONFIRMED, getPoStatusStr(PO_STAT_CONFIRMED)),
    new DropDownItem(PO_STAT_RECEIVING, getPoStatusStr(PO_STAT_RECEIVING)),
    new DropDownItem(PO_STAT_COMPLETED, getPoStatusStr(PO_STAT_COMPLETED)),
    new DropDownItem(PO_STAT_PAID,      getPoStatusStr(PO_STAT_PAID     )),
    new DropDownItem(PO_STAT_CANCELED,  getPoStatusStr(PO_STAT_CANCELED ))
  };

  private long              poId;
  private long              invId;
  private Inventory         inventory;
  private long              vendorId;
  private Vendor            vendor;
  private int               poStatus;
  private Date              createDate;
  private String            userName;
  private int               versionNum;
  private List              items;
  private double            itemTotal;
  private int               itemCount;
  private String            vendorName;
  private String            invName;
  private int               receivedCount;
  private int               addedCount;
  private long              custContId;
  private Contact           custContact;
  private long              custAddrId;
  private Address           custAddress;
  private long              vendContId;
  private Contact           vendContact;
  private long              vendAddrId;
  private Address           vendAddress;
  private long              shipContId;
  private Contact           shipContact;
  private long              shipAddrId;
  private Address           shipAddress;
  private long              shipTypeId;
  private ShipType          shipType;
  private Date              deliveryDate;
  private List              invoices;

  public PurchaseOrder()
  {
    setPoStatus(PO_STAT_EDITING);
  }

  public void setItemTotal(double itemTotal)
  {
    this.itemTotal = itemTotal;
  }
  public double getItemTotal()
  {
    return itemTotal;
  }

  public void setItemCount(int itemCount)
  {
    this.itemCount = itemCount;
  }
  public int getItemCount()
  {
    return itemCount;
  }

  public void setVendorName(String vendorName)
  {
    this.vendorName = vendorName;
  }
  public String getVendorName()
  {
    return vendorName;
  }

  public void setInvName(String invName)
  {
    this.invName = invName;
  }
  public String getInvName()
  {
    return invName;
  }

  public void setReceivedCount(int receivedCount)
  {
    this.receivedCount = receivedCount;
  }
  public int getReceivedCount()
  {
    return receivedCount;
  }
  
  public void setAddedCount(int addedCount)
  {
    this.addedCount = addedCount;
  }
  public int getAddedCount()
  {
    return addedCount;
  } 

  public void setCustContId(long custContId)
  {
    this.custContId = custContId;
  }
  public long getCustContId()
  {
    return custContId;
  }
  public void setCustContact(Contact custContact)
  {
    this.custContact = custContact;
  }
  public Contact getCustContact()
  {
    return custContact;
  }
  public void setCustAddrId(long custAddrId)
  {
    this.custAddrId = custAddrId;
  }
  public long getCustAddrId()
  {
    return custAddrId;
  }
  public void setCustAddress(Address custAddress)
  {
    this.custAddress = custAddress;
  }
  public Address getCustAddress()
  {
    return custAddress;
  }

  public void setVendContId(long vendContId)
  {
    this.vendContId = vendContId;
  }
  public long getVendContId()
  {
    return vendContId;
  }
  public void setVendContact(Contact vendContact)
  {
    this.vendContact = vendContact;
  }
  public Contact getVendContact()
  {
    return vendContact;
  }
  public void setVendAddrId(long vendAddrId)
  {
    this.vendAddrId = vendAddrId;
  }
  public long getVendAddrId()
  {
    return vendAddrId;
  }
  public void setVendAddress(Address vendAddress)
  {
    this.vendAddress = vendAddress;
  }
  public Address getVendAddress()
  {
    return vendAddress;
  }

  public void setShipContId(long shipContId)
  {
    this.shipContId = shipContId;
  }
  public long getShipContId()
  {
    return shipContId;
  }
  public void setShipContact(Contact shipContact)
  {
    this.shipContact = shipContact;
  }
  public Contact getShipContact()
  {
    return shipContact;
  }
  public void setShipAddrId(long shipAddrId)
  {
    this.shipAddrId = shipAddrId;
  }
  public long getShipAddrId()
  {
    return shipAddrId;
  }
  public void setShipAddress(Address shipAddress)
  {
    this.shipAddress = shipAddress;
  }
  public Address getShipAddress()
  {
    return shipAddress;
  }

  public void setShipTypeId(long shipTypeId)
  {
    this.shipTypeId = shipTypeId;
  }
  public long getShipTypeId()
  { 
    return shipTypeId;
  }
  public void setShipType(ShipType shipType)
  {
    this.shipType = shipType;
  }
  public ShipType getShipType()
  {
    return shipType;
  }

  public void setVendor(Vendor vendor)
  {
    this.vendor = vendor;
  }
  public Vendor getVendor()
  {
    return vendor;
  }

  public void setInventory(Inventory inventory)
  {
    this.inventory = inventory;
  }
  public Inventory getInventory()
  {
    return inventory;
  }

  public void setPoId(long poId)
  {
    this.poId = poId;
  }
  public long getPoId()
  {
    return poId;
  }

  public void setInvId(long invId)
  {
    this.invId = invId;
  }
  public long getInvId()
  {
    return invId;
  }

  public void setVendorId(long vendorId)
  {
    this.vendorId = vendorId;
  }
  public long getVendorId()
  {
    return vendorId;
  }

  public boolean encryptSupported()
  {
    return vendor.doesEncryption();
  }

  private void validatePoStatus(int status)
  {
    switch (status)
    {
      case PO_STAT_NONE:
      case PO_STAT_CANCELED:
      case PO_STAT_EDITING:
      case PO_STAT_READY:
      case PO_STAT_CONFIRMED:
      case PO_STAT_RECEIVING:
      case PO_STAT_COMPLETED:
      case PO_STAT_PAID:
      case PO_STAT_PENDING:
        break;
      default:
        throw new RuntimeException("Invalid status for P.O. " + poId 
          + ": " + status);
    }
  }

  public void setPoStatus(int poStatus)
  {
    validatePoStatus(poStatus);
    this.poStatus = poStatus;
  }
  public int getPoStatus()
  {
    return poStatus;
  }

  public String getPoStatusStr()
  {
    return getPoStatusStr(poStatus);
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setDeliveryUtilDate(Date deliveryDate)
  {
    this.deliveryDate = deliveryDate;
  }
  public Date getDeliveryUtilDate()
  {
    return deliveryDate;
  }
  public void setDeliveryDate(Timestamp deliveryTs)
  {
    deliveryDate = Tmg.toDate(deliveryTs);
  }
  public Date getDeliveryDate()
  {
    return Tmg.toTimestamp(deliveryDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setVersionNum(int versionNum)
  {
    this.versionNum = versionNum;
  }
  public int getVersionNum()
  {
    return versionNum;
  }

  public void setPurchaseOrderItems(List items)
  {
    this.items = items;
  }
  public List getPurchaseOrderItems()
  {
    return items;
  }

  public void setInvoices(List invoices)
  {
    this.invoices = invoices;
  }
  public List getInvoices()
  {
    return invoices;
  }

  public String toString()
  {
    return "PurchaseOrder [ "
      + "id: " + poId
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + ", status: " + getPoStatusStr()
      + ", versions: " + versionNum
      + ", vendor: " + vendor
      + ", inventory: " + inventory
      + ", customer address: " + custAddress
      + ", vendor address: " + vendAddress
      + ", shipping address: " + shipAddress
      + ", ship type: " + shipType
      + " ]";
  }

  public boolean isCanceled()
  {
    return poStatus == PO_STAT_CANCELED;
  }

  public boolean isEditing()
  {
    return poStatus == PO_STAT_EDITING;
  }

  public boolean isReady()
  {
    return poStatus == PO_STAT_READY;
  }

  public boolean isConfirmed()
  {
    return poStatus == PO_STAT_CONFIRMED;
  }

  public boolean isReceiving()
  {
    return poStatus == PO_STAT_RECEIVING;
  }

  public boolean isCompleted()
  {
    return poStatus == PO_STAT_COMPLETED;
  }

  public boolean isPaid()
  {
    return poStatus == PO_STAT_PAID;
  }

  public boolean isPending()
  {
    return poStatus == PO_STAT_PENDING;
  }

  public boolean canReady()
  {
    return isEditing() && itemCount > 0;
  }

  public boolean canCancel()
  {
    return isEditing() || isPending() || isReady() || isConfirmed();
  }

  public boolean canReceive()
  {
    return isReceiving() || isCompleted() || isPaid();
  }

  public boolean canAddItems()
  {
    return isEditing() || isPending() || isReady();
  }

  public boolean canInvoice()
  {
    return isCompleted() || isPaid();
  }

  // true if it's ok to edit purchase order details (create new version)
  public boolean canModify()
  {
    return isEditing() || isPending() || isReady() || isConfirmed();
  }

  public boolean canAddToInventory()
  {
    return isReceiving() && receivedCount > addedCount;
  }

  public List getOptionsList(RequestHandler handler)
  {
    List links = new ArrayList();
    if (canModify())
      links.add(handler.getLink(Tmg.ACTION_EDIT_PO,"poId=" + poId,"edit"));
    if (canReady())
      links.add(handler.getLink(Tmg.ACTION_READY_PO,"poId=" + poId,"ready"));
    if (isPending() && handler.userHasPermission(Tmg.ACTION_APPROVE_PO))
      links.add(handler.getLink(Tmg.ACTION_APPROVE_PO,"poId=" + poId,"approve"));
    if (isReady())
      links.add(handler.getLink(Tmg.ACTION_CONFIRM_PO,"poId=" + poId,"confirm"));
    if (isConfirmed())
      links.add(handler.getLink(Tmg.ACTION_RECEIVE_PO,"poId=" + poId,"receive"));
    if (canAddToInventory())
      links.add(handler.getLink(Tmg.ACTION_INV_PO_ALL,"poId=" + poId,"add to inv"));
    if (isReceiving())
      links.add(handler.getLink(Tmg.ACTION_COMPLETE_PO,"poId=" + poId,"complete"));
    if (isCompleted())
      links.add(handler.getLink(Tmg.ACTION_PAY_PO,"poId=" + poId,"paid"));
    if (canCancel())
      links.add(handler.getLink(Tmg.ACTION_CANCEL_PO,"poId=" + poId,"cancel"));
    if (isCanceled())
      links.add(handler.getLink(Tmg.ACTION_UNCANCEL_PO,"poId=" + poId,"reactivate"));

    links.add(handler.getLink(Tmg.ACTION_SHOW_PO_LOGS,"poId=" + poId,"logs"));
    return links;
  }
}