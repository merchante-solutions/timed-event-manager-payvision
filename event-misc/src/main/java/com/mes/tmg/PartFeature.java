package com.mes.tmg;

public class PartFeature
{
  private long    featId;
  private String  ftCode;
  private String  featName;
  private int     displayOrder;
  
  public void setFeatId(long featId)
  {
    this.featId = featId;
  }
  public long getFeatId()
  {
    return featId;
  }

  public void setFtCode(String ftCode)
  {
    this.ftCode = ftCode;
  }
  public String getFtCode()
  {
    return ftCode;
  }
  
  public void setFeatName(String featName)
  {
    this.featName = featName;
  }
  public String getFeatName()
  {
    return featName;
  }

  public void setDisplayOrder(int displayOrder)
  {
    this.displayOrder = displayOrder;
  }
  public int getDisplayOrder()
  {
    return displayOrder;
  }
  
  public String toString()
  {
    return "PartFeature [ id: " + featId + ", type: " + ftCode 
      + ", name: " + featName + " ]";
  }
}