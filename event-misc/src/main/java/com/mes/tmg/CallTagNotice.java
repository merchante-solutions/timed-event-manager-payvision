package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class CallTagNotice extends TmgBean
{
  private long    cnId;
  private long    ctId;
  private String  cntCode;
  private Date    createDate;
  private String  userName;
  private String  trackingNum;
  private String  merchNum;
  private String  addrLine1;
  private String  addrLine2;
  private String  city;
  private String  state;
  private String  zip;
  private String  noticeData;

  private CallTagNoticeType   callTagNoticeType;
  private MifRef              mifRef;

  public void setCnId(long cnId)
  {
    this.cnId = cnId;
  }
  public long getCnId()
  {
    return cnId;
  }

  public void setCtId(long ctId)
  {
    this.ctId = ctId;
  }
  public long getCtId()
  {
    return ctId;
  }

  public void setCntCode(String cntCode)
  {
    this.cntCode = cntCode;
  }
  public String getCntCode()
  {
    return cntCode;
  }

  public void setCallTagNoticeType(CallTagNoticeType callTagNoticeType)
  {
    this.callTagNoticeType = callTagNoticeType;
  }
  public CallTagNoticeType getCallTagNoticeType()
  {
    return callTagNoticeType;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setTrackingNum(String trackingNum)
  {
    this.trackingNum = trackingNum;
  }
  public String getTrackingNum()
  {
    return trackingNum;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setMifRef(MifRef mifRef)
  {
    this.mifRef = mifRef;
  }
  public MifRef getMifRef()
  {
    return mifRef;
  }

  public void setAddrLine1(String addrLine1)
  {
    this.addrLine1 = addrLine1;
  }
  public String getAddrLine1()
  {
    return addrLine1;
  }

  public void setAddrLine2(String addrLine2)
  {
    this.addrLine2 = addrLine2;
  }
  public String getAddrLine2()
  {
    return addrLine2;
  }

  public void setCity(String city)
  {
    this.city = city;
  }
  public String getCity()
  {
    return city;
  }

  public void setState(String state)
  {
    this.state = state;
  }
  public String getState()
  {
    return state;
  }

  public void setZip(String zip)
  {
    this.zip = zip;
  }
  public String getZip()
  {
    return zip;
  }

  public String getCsz()
  {
    StringBuffer buf = new StringBuffer();
    if (city != null)
      buf.append(city);
    if (state != null)
      buf.append((city != null ? ", " : "") + state);
    if (zip != null)
      buf.append((buf.length() > 0 ? " " : "") + zip);
    return buf.toString();
  }

  public String renderAddressText()
  {
    StringBuffer buf = new StringBuffer();
    if (addrLine1 != null) 
      buf.append(addrLine1);
    if (addrLine2 != null) 
      buf.append((buf.length() > 0 ? "\n" : "") + addrLine2);
    String csz = getCsz();
    if (csz.length() > 0)
      buf.append((buf.length() > 0 ? "\n" : "") + csz);
    return buf.toString();
  }

  public void setNoticeData(String noticeData)
  {
    this.noticeData = noticeData;
  }
  public String getNoticeData()
  {
    return noticeData;
  }

  public String toString()
  {
    return "CallTagNotice ["
      + " id: " + cnId
      + ", ct id: " + ctId
      + ", type: " + cntCode
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + ", track num: " + trackingNum
      + ", mif: " + mifRef
      + (addrLine1 != null ? ", addr1: " + addrLine1 : "")
      + (addrLine2 != null ? ", addr2: " + addrLine2 : "")
      + (city != null ? ", city: " + city : "")
      + (state != null ? ", state: " + state : "")
      + (zip != null ? ", zip: " + zip : "")
      + ", notice data: " + noticeData
      + " ]";
  }
}