package com.mes.tmg.repairs;

import java.util.Iterator;
import org.apache.log4j.Logger;
import com.mes.tmg.Part;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PartTest;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public class RepairSendOrderTransaction extends RepairOrderTransaction
{
  static Logger log = Logger.getLogger(RepairSendOrderTransaction.class);

  private String location;

  public RepairSendOrderTransaction(TmgDb db, UserBean user, TmgAction tmgAction,
    RepairOrder order, String location)
  {
    super(db,user,tmgAction,db.getOpCode(Tmg.OP_PART_UPDATE_REPAIR_ORDER), order);
    this.location = location;
  }

  public RepairSendOrderTransaction(UserBean user, TmgAction tmgAction,  RepairOrder order, String location)
  {
    this(new TmgDb(),user,tmgAction, order, location);
  }

  //mark all as sent, set timestamp on order
  public int executeOperation(PartOperation op)
  {

    Part p;
    for (Iterator i = order.getRepairOrderItems().iterator(); i.hasNext(); )
    {
      RepairOrderItem item = (RepairOrderItem)i.next();
      p = db.getPartWithSerialNum(item.getSerNum());
      p.setStatusCode(p.SC_MAINTENANCE);
      p.setLocCode(location);

      PartTest test = p.getPartTest();
      if (test == null)
      {
        //synch the testId with the part and create a new
        //PartTest to persist
        p.setTestId(db.getNewId());
        test = new PartTest();
        test.setTestId(p.getTestId());
        test.setCreateDate(db.getCurDate());
        test.setUserName(user.getLoginName());
        test.setSerialNum(p.getSerialNum());
        test.setPartId(p.getPartId());
        test.setStateId(db.getCurrentPartState(p.getPartId()).getStateId());
        test.setRepairItemId(item.getId());
      }

      test.setTrtCode(item.getProblemAsTested());
      db.persist(test, user);

      db.changePart(p,op,user);
      db.logAction(tmgAction,user,"Part " + p.getPartId()
          + " shipped in repair order " + item.getOrderId(),p.getPartId());
    }

    db.shipRepairOrder(order);

    db.logAction(tmgAction,user,"Repair Order " + order.getBatchId() + " shipped",
      order.getBatchId());

    msg = "Order was successfully shipped.";

    return COMMIT;
  }
}