package com.mes.tmg.repairs;

import org.apache.log4j.Logger;
import com.mes.tmg.PartOpCode;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public abstract class RepairOrderTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(RepairOrderTransaction.class);

  protected TmgAction tmgAction;
  protected RepairOrder order;
  protected String msg = "";

  public RepairOrderTransaction(TmgDb db, UserBean user, TmgAction tmgAction,
    PartOpCode opCode, RepairOrder order)
  {
    super(db,user,null,opCode);
    this.tmgAction = tmgAction;
    this.order = order;
  }

  public RepairOrderTransaction(UserBean user, TmgAction tmgAction,
    PartOpCode opCode, RepairOrder order)
  {
    this(new TmgDb(),user,tmgAction,opCode, order);
  }

  //keep pushing lower
  public abstract int executeOperation(PartOperation op);

  public String getMsg()
  {
    return msg;
  }
}