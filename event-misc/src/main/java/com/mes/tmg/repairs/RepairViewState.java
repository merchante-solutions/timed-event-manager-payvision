package com.mes.tmg.repairs;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewState;

public class RepairViewState extends TmgViewState
{
  static Logger log = Logger.getLogger(RepairViewState.class);

  private RepairOrder order;
  private boolean startFlag = false;
  private StringBuffer csv = null;

  public RepairViewState(TmgAction action, String name)
  {
    super(action,name);
    order = new RepairOrder();
  }

  public void startOrder(RepairOrder order)
  {
    this.order = order;
    startFlag = true;
  }

  public boolean isStarted()
  {
    return startFlag;
  }

  public RepairOrder getOrder()
  {
    return order;
  }

  public LinkedHashMap getOrderItems()
  {
    return order.getItems();
  }

  public List getOrderItemList()
  {
    return order.getRepairOrderItems();
  }

  public void clear()
  {
    startFlag = false;
    order = new RepairOrder();
  }

  public Object removeItem(String serNum)
  {
    return getOrderItems().remove(serNum);
  }

  public RepairOrderItem getItem(String serNum)
  {
    return (RepairOrderItem)getOrderItems().get(serNum);
  }

  public boolean updateItem(RepairOrderItem item)
  {
    getOrderItems().put(item.getSerNum(), item);
    return true;
  }

  public void generateCSV()
  {
    RepairOrderItem item;

    initCSV();

    addCSVHeader();

    csv.append("Serial Number\t");
    csv.append("Part Type\t");
    csv.append("Status\t");
    csv.append("Class\t");
    csv.append("Date Added\t");
    csv.append("Tested Problem\t");
    csv.append("ACR\t");
    csv.append("MID\t\n");

    for(Iterator i = getOrderItemList().iterator(); i.hasNext();)
    {
      item = (RepairOrderItem)i.next();
      csv.append(item.getSerNum()).append("\t");
      csv.append(item.getPartType()).append("\t");
      csv.append(item.getStatusCode()).append("\t");
      csv.append(item.getClassCode()).append("\t");
      csv.append(item.getDateAddedStr()).append("\t");
      csv.append(item.getProblemDisplay()).append("\t");
      csv.append(item.getAcrDisplay()).append("\t");
      csv.append(item.getMidDisplay()).append("\t\n");
    }
  }

  private void initCSV()
  {
    if(csv == null)
    {
      csv = new StringBuffer();
    }
    else
    {
      csv.setLength(0);
    }
  }

  private void addCSVHeader()
  {
    csv.append("Client\t");
    csv.append(order.getClientName()).append("\n");
    csv.append("VRN\t");
    csv.append(order.getVRN()).append("\n\n");
  }

  public String getCSVString()
  {
    if(csv != null)
    {
      return csv.toString();
    }
    else
    {
      return "";
    }
  }

}