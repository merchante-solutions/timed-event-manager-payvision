package com.mes.tmg.repairs;

import org.apache.log4j.Logger;
import com.mes.tmg.PartOperation;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public class RepairCloseOrderTransaction extends RepairOrderTransaction
{
  static Logger log = Logger.getLogger(RepairCloseOrderTransaction.class);

  public RepairCloseOrderTransaction(TmgDb db, UserBean user, TmgAction tmgAction,
    RepairOrder order)
  {
    super(db,user,tmgAction,db.getOpCode(Tmg.OP_PART_CLOSE_REPAIR_ORDER), order);
  }

  public RepairCloseOrderTransaction(UserBean user, TmgAction tmgAction,  RepairOrder order)
  {
    this(new TmgDb(),user,tmgAction, order);
  }

  public int executeOperation(PartOperation op)
  {
    log.debug(op.toString());
    db.closeRepairOrder(order);
    db.logAction(tmgAction,user,"Repair Order " + order.getBatchId() + " closed",
      order.getBatchId());
    return COMMIT;
  }
}