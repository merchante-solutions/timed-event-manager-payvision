package com.mes.tmg.repairs;

import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.support.StringUtilities;
import com.mes.tmg.Part;
import com.mes.tmg.PartStatus;
import com.mes.tmg.PartTest;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.TestResultTypeTable;
import com.mes.tools.DropDownTable;

public class RepairUpdateItemBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(RepairUpdateItemBean.class);

  public static final String FN_REPAIR_ITEM_ACR        = "rAcr";
  public static final String FN_REPAIR_ITEM_MID        = "rMid";
  public static final String FN_REPAIR_ITEM_PROBLEM    = "rProblem";
  public static final String FN_REPAIR_ITEM_BTN        = "rBtn";

  private RepairOrderItem item;

  public RepairUpdateItemBean(TmgAction action)
  {
    super(action,"repairUpdateItemBean");
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    try
    {
      Field field;
      AtLeastOneValidation val = new AtLeastOneValidation("Must enter at least one field.");

      field = new Field(FN_REPAIR_ITEM_ACR,FN_REPAIR_ITEM_ACR,true);
      val.addField(field);
      fields.add(field);

      field = new NumberField(FN_REPAIR_ITEM_MID,16,10,true,0);
      val.addField(field);
      fields.add(field);

      //field = new TextareaField(FN_REPAIR_ITEM_PROBLEM,100,2,40,true);
      field = new DropDownField(FN_REPAIR_ITEM_PROBLEM, new TestResultTypeTable(db), true);
      val.addField(field);
      fields.add(field);

      fields.addValidation(val);

      fields.add(new ButtonField(FN_REPAIR_ITEM_BTN,"Update Item"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public boolean setItem(RepairOrderItem _item)
  {
    if(_item != null)
    {
      this.item = _item;
      getField(FN_REPAIR_ITEM_MID).setData(""+item.getMid());
      getField(FN_REPAIR_ITEM_PROBLEM).setData(item.getProblemAsTested());
      getField(FN_REPAIR_ITEM_ACR).setData(item.getAcr());
      return true;
    }
    return false;
  }

  public RepairOrderItem getItem()
  {
    return item;
  }

  public RepairOrderItem updateItem()
  {
    if(isValid())
    {
      //this is done for PartTest purposes
      //IGNORE THIS for now - will be done on the Send Order
      //updateTestResult();

      //item updated
      item.setProblemAsTested(getData(FN_REPAIR_ITEM_PROBLEM));
      item.setAcr(getData(FN_REPAIR_ITEM_ACR));
      item.setMid(StringUtilities.stringToLong(getData(FN_REPAIR_ITEM_MID)));
      db.updateRepairItem(item);
    }
    return item;
  }

  //DISABLED ABOVE
  //allows the PartTest to be properly formed and updated
  //at the time of item change... if the result hasn't changed, then
  //nothing happens
  protected void updateTestResult()
  {
    log.debug(item.toString());
    String newResult = getData(FN_REPAIR_ITEM_PROBLEM);

    if(!newResult.equals(item.getProblemAsTested()))
    {
      log.debug("test result has changed");
      //test result changed, so change the PartTest in db
      Part part = db.getPart(item.getPartId());
      if (part != null)
      {
        log.debug("part is not null");
        PartTest test = part.getPartTest();
        if (test == null)
        {
          log.debug("part test is null... creating new one");
          //synch the testId with the part and create a new
          //PartTest to persist
          part.setTestId(db.getNewId());
          test = new PartTest();
          test.setTestId(part.getTestId());
          test.setCreateDate(db.getCurDate());
          test.setUserName(user.getLoginName());
          test.setSerialNum(part.getSerialNum());
          test.setPartId(part.getPartId());
          test.setStateId(db.getCurrentPartState(part.getPartId()).getStateId());
        }

        test.setTrtCode(newResult);
        db.persist(test, user);
      }
      else
      {
        log.debug("part is null");
      }
    }
  }



  protected class _StatusTable extends DropDownTable
  {
    public _StatusTable()
    {
      List types = db.getPartStatuses();
      for (Iterator i = types.iterator(); i.hasNext(); )
      {
        PartStatus stat = (PartStatus)i.next();
        addElement(stat.getStatusCode(),stat.getStatusName());
      }
    }
  }


}
