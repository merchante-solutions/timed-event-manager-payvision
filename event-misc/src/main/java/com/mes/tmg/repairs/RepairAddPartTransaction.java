package com.mes.tmg.repairs;

import org.apache.log4j.Logger;
import com.mes.tmg.Part;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public class RepairAddPartTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(RepairAddPartTransaction.class);

  private TmgAction tmgAction;
  private long batchId;
  private long mid;
  private String testResult;
  private RepairAddItemBean addBean;

  public RepairAddPartTransaction(TmgDb db, UserBean user, TmgAction tmgAction,
    RepairAddItemBean addBean, Part part, long batchId, long mid, String testResult)
  {
    super(db,user,part,db.getOpCode(Tmg.OP_PART_ADD_REPAIR_ORDER));
    this.tmgAction = tmgAction;
    this.addBean = addBean;
    this.batchId = batchId;
    this.mid = mid;
    this.testResult = testResult;
  }

  public int executeOperation(PartOperation op)
  {
    part.setStatusCode(part.SC_BUSY);
    //change the part, which should update the partstate
    db.changePart(part,op,user);

    //create the new repair item
    RepairOrderItem item = new RepairOrderItem();
    item.setId(db.getNewId());
    item.setOrderId(batchId);
    item.setSerNum(part.getSerialNum());
    item.setPartType(part.getPartType().getPtName());
    item.setDateAdded(db.getCurDate());
    item.setUserName(user.getLoginName());
    item.setStatusCode(part.getStatusCode());
    item.setClassCode(part.getPcCode());
    item.setPartId(part.getPartId());
    item.setMid(mid);
    item.setProblemAsTested(testResult);

    db.persist(item,user);

    db.logAction(tmgAction,user,"Part " + part.getPartId() + " added to repair batch "
      + batchId,batchId);

    addBean.setItem(item);

    return COMMIT;
  }
}
