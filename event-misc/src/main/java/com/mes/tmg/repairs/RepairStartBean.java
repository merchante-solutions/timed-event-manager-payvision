package com.mes.tmg.repairs;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.ClientTable;
import com.mes.tmg.util.VendorTable;

public class RepairStartBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(RepairStartBean.class);

  public static final String  FN_REPAIR_CLIENT      = "rClient";
  public static final String  FN_REPAIR_VENDOR      = "rVendor";
  public static final String  FN_REPAIR_VRN         = "rVRN";
  public static final String  FN_REPAIR_START_BTN   = "startRepairBtn";


  public RepairStartBean(TmgAction action)
  {
    super(action,"repairStartBean");
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      //visible
      fields.add(new DropDownField(FN_REPAIR_CLIENT,new ClientTable(db),false));
      fields.add(new DropDownField(FN_REPAIR_VENDOR,new VendorTable(db),false));
      fields.add(new Field(FN_REPAIR_VRN,"VRN #",30,20,false));

      fields.add(new ButtonField(FN_REPAIR_START_BTN,"start"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

}
