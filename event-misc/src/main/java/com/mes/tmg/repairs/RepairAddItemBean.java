package com.mes.tmg.repairs;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.TestResultTypeTable;

public class RepairAddItemBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(RepairAddItemBean.class);

  public static final String  FN_REPAIR_ADD_ITEM        = "rAddItem";
  public static final String  FN_REPAIR_ADD_ITEM_MID    = "rAddItemMID";
  public static final String  FN_REPAIR_ADD_ITEM_RESULT = "rAddItemResult";
  public static final String  FN_REPAIR_START_BTN       = "rAddStart";

  public static final String  MSG_ADDED     = "Item Added.";
  public static final String  MSG_NOT_FOUND = "ITEM NOT ADDED: Not a valid Item.";
  public static final String  MSG_DUPLICATE = "ITEM NOT ADDED: Item already exists in order.";
  public static final String  MSG_ERROR     = "ITEM NOT ADDED: Error with selected Item.";

  public static final int  INACTIVE       = 0;
  public static final int  ADDED          = 1;
  public static final int  DUPLICATE      = 2;
  public static final int  NOT_FOUND      = 3;

  private int status;
  private RepairOrderItem _item;
  private StringBuffer _msg = new StringBuffer();

  public RepairAddItemBean(TmgAction action)
  {
    super(action,"repairStartBean");
    status = INACTIVE;
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      //visible
      fields.add(new Field(FN_REPAIR_ADD_ITEM,"Serial #",200,20,false));
      fields.add(new Field(FN_REPAIR_ADD_ITEM_MID,"MID #",100,20,true));
      fields.add(new DropDownField(FN_REPAIR_ADD_ITEM_RESULT,new TestResultTypeTable(db), true));
      fields.add(new ButtonField(FN_REPAIR_START_BTN,"Add Item"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }


  public void updateView(RepairViewState viewState)
  {
    if(viewState != null)
    {
      viewState.getOrderItems().put(_item.getSerNum(),_item);
      status = ADDED;
      _msg.append("Serial Number ").append(_item.getSerNum()).append(": ");
      _item = null;
    }
  }

  public String getMsg()
  {
    switch(status)
    {
      case ADDED:
        _msg.append(MSG_ADDED);
        break;

      case NOT_FOUND:
        _msg.append(MSG_NOT_FOUND);
        break;

      case DUPLICATE:
        _msg.append(MSG_DUPLICATE);
        break;

      case INACTIVE:
      default:
        _msg.append(MSG_ERROR);
    }

    String out = _msg.toString();
    _msg.setLength(0);
    return out;

  }

  public boolean isValid(RepairOrder order)
  {
    //check for match on serial number
    if(order.getItems().get(getData(FN_REPAIR_ADD_ITEM)) == null)
    {
      return true;
    }
    else
    {
      status = DUPLICATE;
      return false;
    }
  }

  public void reset()
  {
    setData(FN_REPAIR_ADD_ITEM,"");
    setData(FN_REPAIR_ADD_ITEM_MID,"");
    setData(FN_REPAIR_ADD_ITEM_RESULT,"");
    setData(FN_REPAIR_START_BTN,"");
    _msg.setLength(0);
  }

  public void setItem(RepairOrderItem item)
  {
    _item = item;
  }

}
