package com.mes.tmg.repairs;

import java.sql.Timestamp;
import java.util.Date;
import com.mes.support.DateTimeFormatter;
import com.mes.tmg.Tmg;

public class RepairOrderItem
{
  private long id                   = 0;
  private long orderId              = 0;
  private long partId               = 0;
  private long mid                  = 0;
  private String serNum             = "";
  private String classCode          = "";
  private String partType           = "";
  private String status             = "";
  private Date dateAdded;
  private String acr                = "";
  private String userName           = "";
  private String problemAsTested    = "";

  public void setId(long id)
  {
    this.id = id;
  }

  public long getId()
  {
    return id;
  }

  public void setOrderId(long id)
  {
    this.orderId = id;
  }

  public long getOrderId()
  {
    return orderId;
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }

  public long getPartId()
  {
    return partId;
  }

  public void setMid(long id)
  {
    this.mid = id;
  }

  public long getMid()
  {
    return mid;
  }

  public void setAcr(String value)
  {
    this.acr = value;
  }

  public String getAcr()
  {
    return acr;
  }

  public String getSerNum()
  {
    return serNum;
  }
  public String getPartType()
  {
    return partType;
  }
  public String getStatusCode()
  {
    return status;
  }

  public String getClassCode()
  {
    return classCode;
  }

  public String getProblemAsTested()
  {
    return problemAsTested;
  }

  public String getProblemDisplay()
  {
    return emptyTest(problemAsTested);
  }

  public String getAcrDisplay()
  {
    return emptyTest(acr);
  }

  public String getMidDisplay()
  {
    return emptyTest(""+mid);
  }

  private String emptyTest(String value)
  {
    if( value== null || value.equals(""))
    {
      return "update";
    }
    else
    {
      return value;
    }
  }

  public String getDateAddedStr()
  {
    return DateTimeFormatter.getFormattedDate(dateAdded,"MMM/dd/yyyy");
  }

  public Date getDateAdded()
  {
    return dateAdded;
  }

  public Date getDateAddedTs()
  {
    return Tmg.toTimestamp(dateAdded);
  }

  public String getUserName()
  {
    return userName;
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }

  public void setDateAddedTs(Timestamp dateTs)
  {
    this.dateAdded = Tmg.toDate(dateTs);
  }

  public void setSerNum(String value)
  {
    serNum = value;
  }

  public void setPartType(String value)
  {
    partType = value;
  }

  public void setStatusCode(String value)
  {
    status = value;
  }

  public void setClassCode(String value)
  {
    classCode = value;
  }

  public void setProblemAsTested(String value)
  {
    problemAsTested = value;
  }

  public void setDateAdded(Date value)
  {
    dateAdded = value;
  }
  public String toString()
  {
    StringBuffer sb = new StringBuffer(128);
    sb.append("id = ").append(id).append("\n");
    sb.append("orderId = ").append(orderId).append("\n");
    sb.append("serNum = ").append(serNum).append("\n");
    sb.append("partId = ").append(partId).append("\n");
    sb.append("partType = ").append(partType).append("\n");
    sb.append("status = ").append(status).append("\n");
    sb.append("dateAdded = ").append(dateAdded).append("\n");
    sb.append("userName = ").append(userName).append("\n");
    sb.append("problemAsTested = ").append(problemAsTested).append("\n");
    return sb.toString();
  }

}