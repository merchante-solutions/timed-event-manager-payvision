package com.mes.tmg.repairs;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.ClassTable;
import com.mes.tmg.util.ConditionTable;
import com.mes.tmg.util.EncryptionTable;
import com.mes.tmg.util.LocationTable;
import com.mes.tmg.util.PartStatusTable;
import com.mes.user.UserBean;

public class RepairUpdateBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(RepairUpdateBean.class);

  public static final String FN_REPAIR_STATUS             = "bStatus";
  public static final String FN_REPAIR_CLASS              = "bClass";
  public static final String FN_REPAIR_CONDITION          = "bCondition";
  public static final String FN_REPAIR_ENCRYPT            = "bEncryption";
  public static final String FN_REPAIR_UPDATE_BTN         = "bUpdateBtn";
  public static final String FN_REPAIR_UPDATE_ALL_BTN     = "bUpdateAllBtn";
  public static final String FN_REPAIR_SEND_LOCATION      = "bSendLoc";
  public static final String FN_REPAIR_SEND_BTN           = "bSendBtn";
  public static final String FN_REPAIR_CLOSE_BTN          = "bCloseBtn";


  public static final String UPDATE_BTN_TEXT      = "Update Selected";
  public static final String UPDATE_ALL_BTN_TEXT  = "Update ALL";
  public static final String CLOSE_BTN_TEXT        = "Close Order";
  public static final String SEND_BTN_TEXT        = "Send Batch Items";

  public static final int INVALID             = 0;
  public static final int UPDATE              = 1;
  public static final int UPDATE_ALL          = 2;
  public static final int SEND_FOR_REPAIR     = 3;
  public static final int CLOSE_ORDER         = 4;

  public RepairUpdateBean(TmgAction action)
  {
    super(action,"repairUpdateItemBean");
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    try
    {
      //non-defaulted tables
      fields.add(new DropDownField(FN_REPAIR_STATUS,new PartStatusTable(db,TmgDropDownTable.OPT_SEL_ONE),true));
      fields.add(new DropDownField(FN_REPAIR_ENCRYPT,new EncryptionTable(db,TmgDropDownTable.OPT_SEL_ONE),true));

      //have to default following fields
      Field field = new DropDownField(FN_REPAIR_SEND_LOCATION,new LocationTable(db),true);
      field.setData("REPAIR");
      fields.add(field);

      field = new DropDownField(FN_REPAIR_CONDITION,new ConditionTable(db,TmgDropDownTable.OPT_SEL_ONE),true);
      field.setData("GOOD");
      fields.add(field);

      field = new DropDownField(FN_REPAIR_CLASS,new ClassTable(db,TmgDropDownTable.OPT_SEL_ONE),true);
      field.setData("REFURB");
      fields.add(field);

      //button fields
      fields.add(new ButtonField(FN_REPAIR_UPDATE_BTN,UPDATE_BTN_TEXT));
      fields.add(new ButtonField(FN_REPAIR_UPDATE_ALL_BTN,UPDATE_ALL_BTN_TEXT));
      fields.add(new ButtonField(FN_REPAIR_SEND_BTN,SEND_BTN_TEXT));
      fields.add(new ButtonField(FN_REPAIR_CLOSE_BTN,CLOSE_BTN_TEXT));

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("status = ").append(getData(FN_REPAIR_STATUS)).append("\n");
    sb.append("class = ").append(getData(FN_REPAIR_CLASS)).append("\n");
    sb.append("condition = ").append(getData(FN_REPAIR_CONDITION)).append("\n");
    sb.append("encryption = ").append(getData(FN_REPAIR_ENCRYPT)).append("\n");
    sb.append("update = ").append(getData(FN_REPAIR_UPDATE_BTN)).append("\n");
    sb.append("update all = ").append(getData(FN_REPAIR_UPDATE_ALL_BTN)).append("\n");
    sb.append("send location = ").append(getData(FN_REPAIR_SEND_LOCATION)).append("\n");
    sb.append("send = ").append(getData(FN_REPAIR_SEND_BTN)).append("\n");
    return sb.toString();
  }

  public RepairOrderTransaction getTransaction(TmgDb db, UserBean user,
    TmgAction tmgAction, RepairOrder order)
  {
    String[] ids = null;
    boolean all  = true;
    RepairOrderTransaction oTrans = null;

    switch (getProcess())
    {
      case UPDATE:
        ids = request.getParameterValues("rItem");
        all = false;
        //fall through to...

      case UPDATE_ALL:
        if (all)
        {
          ids = order.getItemIdArray();
        }

        String status = getData(FN_REPAIR_STATUS);
        String _class = getData(FN_REPAIR_CLASS);
        long encrypt = getField(FN_REPAIR_ENCRYPT).asLong();
        String condition = getData(FN_REPAIR_CONDITION);

        oTrans = new RepairUpdateOrderTransaction(db, user, tmgAction,
          order, ids, status, _class, encrypt, condition);
        break;

      case SEND_FOR_REPAIR:

        if(order.allItemsValid())
        {
          String location = getData(FN_REPAIR_SEND_LOCATION);
          oTrans = new RepairSendOrderTransaction(db, user, tmgAction, order, location);
        }
        //else return null - will be handled as such
        break;

      case CLOSE_ORDER:

        oTrans = new RepairCloseOrderTransaction(db, user, tmgAction, order);
        break;

    }

    return oTrans;
  }

  private int getProcess()
  {
    int result = INVALID;

    if( getData(FN_REPAIR_UPDATE_BTN).equals(UPDATE_BTN_TEXT) )
    {
      if(this.isValid())
      {
        result = UPDATE;
      }
    }
    else if ( getData(FN_REPAIR_UPDATE_ALL_BTN).equals(UPDATE_ALL_BTN_TEXT) )
    {
      result = UPDATE_ALL;
    }
    else if ( getData(FN_REPAIR_SEND_BTN).equals(SEND_BTN_TEXT) )
    {
      result = SEND_FOR_REPAIR;
    }
    else if ( getData(FN_REPAIR_CLOSE_BTN).equals(CLOSE_BTN_TEXT) )
    {
      result = CLOSE_ORDER;
    }
    reset();

    return result;
  }

  private void reset()
  {
    setData(FN_REPAIR_UPDATE_BTN,"");
    setData(FN_REPAIR_UPDATE_ALL_BTN,"");
    setData(FN_REPAIR_SEND_BTN,"");
    setData(FN_REPAIR_CLOSE_BTN,"");
  }

}
