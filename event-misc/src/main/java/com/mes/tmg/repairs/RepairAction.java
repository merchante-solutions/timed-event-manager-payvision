package com.mes.tmg.repairs;

import java.io.OutputStream;
import org.apache.log4j.Logger;
import com.mes.mvc.Link;
import com.mes.persist.Transaction;
import com.mes.tmg.Part;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class RepairAction extends TmgAction
{
  static Logger log = Logger.getLogger(RepairAction.class);

  public static String REPAIR_LOOKUP_BEAN = "repairLookupBean";
  public static String REPAIR_START_BEAN = "repairStartBean";
  public static String REPAIR_VIEW_BEAN = "repairViewBean";
  public static String REPAIR_ADD_ITEM_BEAN = "repairAddItemBean";
  public static String REPAIR_UPDATE_BEAN = "repairUpdateBean";
  public static String REPAIR_UPDATE_ITEM_BEAN = "repairUpdateItemBean";

  public static final String KEEP_ERROR_STATE_FLAG = "errorStateFlag";

  public void doRepairView()
  {
    log.debug("in doRepairView()");

    RepairViewState viewState = getRepairViewState();
    //get batch id from request, and populate the corresponding list
    //updating the view as we go
    long batchId;
    try
    {
      batchId = getParmLong("batchId");
    }
    catch (Exception e)
    {
      batchId = viewState.getOrder().getBatchId();
    }
    viewState.startOrder(db.getRepairOrder(batchId));
    request.setAttribute("addBean", getRepairAddItemBean(false));
    request.setAttribute("updateBean", getRepairUpdateBean());
    request.setAttribute("orderItems",viewState.getOrderItemList());
    setNoteItemId(batchId);
    doView(Tmg.VIEW_REPAIR_MAIN);
  }

  public void doRepairStart()
  {
    log.debug("in doRepairStart()");

    RepairStartBean bean = new RepairStartBean(this);
    request.setAttribute("startBean",bean);

    RepairViewState viewState = getRepairViewState();
    RepairAddItemBean addItem = getRepairAddItemBean(false);
    addItem.reset();

    if(viewState.isStarted())
    {
      viewState.clear();
    }
    doView(Tmg.VIEW_REPAIR_START);
  }

  public void doRepair()
  {

    log.debug("in doRepair()");
    RepairStartBean startBean = getRepairStartBean();

    if(startBean.isValid())
    {
      RepairViewState viewState = getRepairViewState();

      if (!viewState.isStarted())
      {
        //set up view based on submission
        long vId = getParmLong(RepairStartBean.FN_REPAIR_VENDOR);
        long cId = getParmLong(RepairStartBean.FN_REPAIR_CLIENT);
        String vrn = getParm(RepairStartBean.FN_REPAIR_VRN);
        RepairOrder order = db.createOrder(vId, cId, vrn, user);
        viewState.startOrder(order);
      }

      RepairAddItemBean addBean = getRepairAddItemBean(false);
      request.setAttribute("addBean",addBean);
      request.setAttribute("orderItems",viewState.getOrderItemList());
      setNoteItemId(viewState.getOrder().getBatchId());
      doView(Tmg.VIEW_REPAIR_MAIN);
    }
    else
    {
      request.setAttribute("startBean",startBean);
      doView(Tmg.VIEW_REPAIR_START);
    }
  }

  public void doRepairLookup()
  {
    log.debug("in doRepairLookup()");
    RepairLookupBean bean = getRepairLookupBean();
    request.setAttribute("viewBean",bean);
    doView(Tmg.VIEW_REPAIR_LOOKUP);
  }

  public void doRepairViewItem()
  {
    log.debug("in doRepairViewItem()");

    RepairViewState viewState = getRepairViewState();
    RepairUpdateItemBean updateItemBean = getRepairUpdateItemBean();
    String serNum = getParm("serNum","");
    if(updateItemBean.setItem(viewState.getItem(serNum)))
    {
      request.setAttribute("updateItemBean",updateItemBean);
      doView(Tmg.VIEW_REPAIR_ITEM);
    }
    else
    {
      redirectWithFeedback(Tmg.VIEW_REPAIR_MAIN,"Unable to find requested item.");
    }
  }

  public void doRepairAddItem()
  {
    log.debug("in doRepairAddItem()");

    RepairViewState viewState = getRepairViewState();
    RepairAddItemBean addBean = getRepairAddItemBean(true);
    String feedback = "";

    // 
    // Rejiggered logic to get user confirmation when merchant
    // owned part is added by user:
    //
    //    if !bean fields valid
    //       add bean feedback
    //    else
    //       if !valid part
    //         add not found feedback
    //       else if !merch owned || is confirmed
    //         add item
    //         add bean feedback
    //       else if canceled
    //         add cancel feedback
    //         reset
    //       else
    //         confirm
    //
    //    redirect to main
    //
    if (!addBean.isValid())
    {
      feedback = addBean.getMsg();
      setSessionAttr(KEEP_ERROR_STATE_FLAG,"keep");
    }
    else
    {
      Part part = db.getPartWithSerialNum(
                      addBean.getData(RepairAddItemBean.FN_REPAIR_ADD_ITEM));
      if (part == null)
      {
        feedback = "ITEM NOT ADDED: Not a valid Item.";
      }
      else if (!part.isMerchantOwned() || isConfirmed())
      {
        long batchId = viewState.getOrder().getBatchId();
        String testResult = addBean.getData(
                                  RepairAddItemBean.FN_REPAIR_ADD_ITEM_RESULT);
        long mid = 0;
        //look for the mid
        try
        {
          mid = Long.parseLong(
                  addBean.getData(RepairAddItemBean.FN_REPAIR_ADD_ITEM_MID));
        }
        catch (NumberFormatException nfe)
        {
          //ignore, leave mid = 0
        }

        //kinda ugly
        //check for validity - item not currently existing in order,
        //A.for valid client items:
        //  part of proper client inventory, and in proper state
        //B.for merchant-owned items
        //  MID field exists in form
        boolean clientMatches = 
          viewState.getOrder().getClientId() == part.getClient().getClientId();
        if(addBean.isValid(viewState.getOrder()) && part != null
            && (mid > 0 || (clientMatches && part.inNeedOfRepair())))
        {
          Transaction t = 
            new RepairAddPartTransaction(
              db,user,this,addBean,part,batchId,mid,testResult);
          if(!t.run())
          {
            throw new RuntimeException("Failed to add item to repair order");
          }
          else
          {
            //updateView resets the item
            //reset resets the fields
            addBean.reset();
            addBean.updateView(viewState);
          }
        }

        feedback = addBean.getMsg();
      }
      else if (isCanceled())
      {
        // work around to deal with confirmation issues
        addBean.reset();
        feedback = "ITEM NOT ADDED: canceled by user.";
      }
      else
      {
        confirm("Add part owned by merchant " 
          + part.getOwnerMerchNum() + "?");
        return;
      }
    }

    // work around to deal with confirmation issues
    Link backLink = handler.getBackLink(Tmg.ACTION_REPAIR_VIEW);
    Link viewLink = handler.getLink(Tmg.ACTION_REPAIR_VIEW);
    viewLink.addArg("batchId",""+viewState.getOrder().getBatchId());
    handler.setBackLink(Tmg.ACTION_REPAIR_VIEW,backLink);
    redirectWithFeedback(viewLink,feedback);
  }

  public void doRepairAddItem_OLD()
  {
    log.debug("in doRepairAddItem()");

    RepairViewState viewState = getRepairViewState();
    RepairAddItemBean addBean = getRepairAddItemBean(true);

    if(addBean.isValid())
    {

      Part part = db.getPartWithSerialNum(addBean.getData(RepairAddItemBean.FN_REPAIR_ADD_ITEM));
      long batchId = viewState.getOrder().getBatchId();
      String testResult = addBean.getData(RepairAddItemBean.FN_REPAIR_ADD_ITEM_RESULT);
      long mid = 0;
      //look for the mid
      try
      {
        mid = Long.parseLong(addBean.getData(RepairAddItemBean.FN_REPAIR_ADD_ITEM_MID));
      }
      catch (NumberFormatException nfe)
      {
        //ignore, leave mid = 0
      }

      //kinda ugly
      //check for validity - item not currently existing in order,
      //A.for valid client items:
      //  part of proper client inventory, and in proper state
      //B.for merchant-owned items
      //  MID field exists in form
      if( addBean.isValid(viewState.getOrder())
          &&
          part != null
          &&
          (
            (
              viewState.getOrder().getClientId() == part.getClient().getClientId()
              &&
              part.inNeedOfRepair()
            )

            ||

            mid > 0
          )
        )
      {

        RepairAddPartTransaction t =
          new RepairAddPartTransaction(db, user, this, addBean,
                                        part, batchId, mid, testResult);

        if(!t.run())
        {
          throw new RuntimeException("Failed to add item to repair order");
        }
        else
        {
          //updateView resets the item
          addBean.updateView(viewState);
          //reset resets the fields
          addBean.reset();
        }
      }
    }

    //getMsg resets the message
    addFeedback(addBean.getMsg());

    request.setAttribute("addBean",addBean);
    request.setAttribute("updateBean",getRepairUpdateBean());
    request.setAttribute("orderItems",viewState.getOrderItemList());

    setNoteItemId(viewState.getOrder().getBatchId());

    doView(Tmg.VIEW_REPAIR_MAIN);

  }

  public void doRepairRemoveItem()
  {
    log.debug("in doRepairRemoveItem()");

    RepairViewState viewState = getRepairViewState();
    Part part = db.getPartWithSerialNum(getParm("serNum",""));
    RepairRemovePartTransaction t =
      new RepairRemovePartTransaction(db,user,this,viewState,part);
    StringBuffer msg = new StringBuffer();

    if(!t.run())
    {
      msg.append("Serial Number ").append(part.getSerialNum()).append(": UNABLE to remove.");
    }
    else
    {
      msg.append("Serial Number ").append(part.getSerialNum()).append(": Removed.");
    }

    request.setAttribute("addBean",getRepairAddItemBean(false));
    request.setAttribute("updateBean",getRepairUpdateBean());
    request.setAttribute("orderItems",viewState.getOrderItemList());
    setNoteItemId(viewState.getOrder().getBatchId());
    addFeedback(msg.toString());

    doView(Tmg.VIEW_REPAIR_MAIN);
  }

  /**
    * doRepairUpdate() -
    * this is either a selective group item update,
    * or it's a complete update to send items for repair.
    * Either way, the UpdateBean will return the appropriate
    * transaction to run, and we'll stay on the main page.
    */
  public void doRepairUpdate()
  {
    log.debug("in doRepairUpdate()");

    RepairUpdateBean uBean = getRepairUpdateBean();
    RepairViewState viewState = getRepairViewState();

    RepairOrderTransaction t =
      uBean.getTransaction(db,user,this,viewState.getOrder());

    log.debug(uBean.toString());

    if( t==null || !t.run() )
    {
      addFeedback("Unable to complete process - check field information.");
      request.setAttribute("addBean",getRepairAddItemBean(false));
      request.setAttribute("updateBean",uBean);
      request.setAttribute("orderItems",viewState.getOrderItemList());
      setNoteItemId(viewState.getOrder().getBatchId());
      doView(Tmg.VIEW_REPAIR_MAIN);
    }
    else
    {
      redirectWithFeedback(Tmg.VIEW_REPAIR_MAIN,t==null?"Unable to complete request: Invalid Transaction":t.getMsg());
    }                     
  }

  public void doRepairUpdateItem()
  {
    log.debug("in doRepairUpdateItem()");

    RepairViewState viewState = getRepairViewState();
    RepairUpdateItemBean updateItemBean = getRepairUpdateItemBean();

    //update the item in the bean (incl db) then view...
    if(updateItemBean.isValid())
    {
      viewState.updateItem(updateItemBean.updateItem());
      addFeedback("Item updated.");
    }
    else
    {
      addFeedback("Unable to update item.");
    }

    request.setAttribute("addBean", getRepairAddItemBean(false));
    request.setAttribute("updateBean", getRepairUpdateBean());
    request.setAttribute("orderItems",viewState.getOrderItemList());
    setNoteItemId(viewState.getOrder().getBatchId());
    doView(Tmg.VIEW_REPAIR_MAIN);
  }

  public void doRepairCancel()
  {
    long batchId = getParmLong("batchId");

    RepairViewState viewState = getRepairViewState();
    RepairLookupBean bean = getRepairLookupBean();
    RepairOrder order = db.getRepairOrder(batchId);
    log.debug("batchId = " + batchId);

    RepairCancelOrderTransaction t =
      new RepairCancelOrderTransaction (db,user,this,order);
    if ( !t.run() )
    {
      //request.setAttribute("batchId",""+batchId);
      redirectWithFeedback(Tmg.ACTION_REPAIR_LOOKUP,"UNABLE to cancel this Order.");
    }
    else
    {
      bean.removeOrder(order.getBatchId());
      redirectWithFeedback(Tmg.ACTION_REPAIR_LOOKUP,"Order was successfully canceled.");
    }
  }

  public void doRepairReportGen()
  {
    RepairViewState viewState = getRepairViewState();
    viewState.generateCSV();
    try
    {
      response.setContentType("application/vnd.ms-excel");
      //response.setContentType("text/csv");
      response.addHeader("Content-disposition", "attachment; filename=maint_rpt.xls" );
      OutputStream out = response.getOutputStream();
      out.write(viewState.getCSVString().getBytes());
      out.flush();
      out.close();
      //response.getWriter().write();
      //response.getWriter().flush();
      //response.getWriter().close();
      return;
    }
    catch (Exception e)
    {
      redirectWithFeedback(Tmg.VIEW_REPAIR_MAIN,"UNABLE to generate file.");
     //doView(Tmg.VIEW_REPAIR_REPORT);
    }
  }

  private RepairLookupBean getRepairLookupBean()
  {
    RepairLookupBean bean = (RepairLookupBean)getSessionAttr(REPAIR_LOOKUP_BEAN,null);
    if (bean == null)
    {
      log.debug("creating RepairLookupBean...");
      bean = new RepairLookupBean(this);
      setSessionAttr(REPAIR_LOOKUP_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  private RepairStartBean getRepairStartBean()
  {
    RepairStartBean bean = (RepairStartBean)getSessionAttr(REPAIR_START_BEAN,null);
    if (bean == null)
    {
      log.debug("creating RepairStartBean...");
      bean = new RepairStartBean(this);
      setSessionAttr(REPAIR_START_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  private RepairAddItemBean getRepairAddItemBean(boolean doOp)
  {
    RepairAddItemBean bean = (RepairAddItemBean)getSessionAttr(REPAIR_ADD_ITEM_BEAN,null);
    log.debug("in getRepairAddItemBean()...");
    if (bean == null)
    {
      log.debug("creating RepairAddItemBean...");
      bean = new RepairAddItemBean(this);
      setSessionAttr(REPAIR_ADD_ITEM_BEAN,bean);
    }
    else
    {
      if(doOp)
      {
        log.debug("setting action...");
        bean.setAction(this);
      }
    }
    Object keepErrorState = getSessionAttr(KEEP_ERROR_STATE_FLAG,null);
    if (keepErrorState != null)
    {
      removeSessionAttr(KEEP_ERROR_STATE_FLAG);
    }
    else
    {
      bean.resetErrorState();
    }
    return bean;
  }

  private RepairUpdateBean getRepairUpdateBean()
  {
    RepairUpdateBean bean = (RepairUpdateBean)getSessionAttr(REPAIR_UPDATE_BEAN,null);
    log.debug("in getRepairUpdateBean()...");
    if (bean == null)
    {
      log.debug("creating RepairUpdateBean...");
      bean = new RepairUpdateBean(this);
      setSessionAttr(REPAIR_UPDATE_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  private RepairUpdateItemBean getRepairUpdateItemBean()
  {
    RepairUpdateItemBean bean = (RepairUpdateItemBean)getSessionAttr(REPAIR_UPDATE_ITEM_BEAN,null);
    log.debug("in getRepairUpdateItemBean()...");
    if (bean == null)
    {
      log.debug("creating RepairUpdateItemBean...");
      bean = new RepairUpdateItemBean(this);
      setSessionAttr(REPAIR_UPDATE_ITEM_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  private RepairViewState getRepairViewState()
  {
    RepairViewState viewState
      = (RepairViewState)getSessionAttr(REPAIR_VIEW_BEAN,null);
    if (viewState == null)
    {
      log.debug("creating RepairViewState...");
      viewState = new RepairViewState(this,REPAIR_VIEW_BEAN);
      setSessionAttr(REPAIR_VIEW_BEAN,viewState);
    }
    else
    {
      viewState.setAction(this);
    }
    return viewState;
  }

  private void clearRepairViewState()
  {
    session.removeAttribute(REPAIR_VIEW_BEAN);
  }
}
