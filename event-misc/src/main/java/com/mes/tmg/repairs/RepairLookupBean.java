package com.mes.tmg.repairs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.Field;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class RepairLookupBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(RepairLookupBean.class);

  public static final String FN_FROM_DATE     = "fromDate";
  public static final String FN_TO_DATE       = "toDate";
  //public static final String FN_CLIENT_ID     = "clientId";
  public static final String FN_SEARCH_TERM   = "searchTerm";
  public static final String FN_SUBMIT_BTN    = "submitBtn";

  private List orders;

  public RepairLookupBean(TmgAction action)
  {
    super(action,"repairLookupBean");
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    try
    {
      fields.add(new DateStringField(FN_FROM_DATE,"From Date",false,false));
      fields.add(new DateStringField(FN_TO_DATE,"To Date",true,false));
      //fields.add(new DropDownField(FN_CLIENT_ID,new ClientTable(db),true));
      fields.add(new Field(FN_SEARCH_TERM,"Search Term",30,20,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public List getRows()
  {
    if (orders == null || (isAutoSubmit() && !isAutoValid()))
    {
      orders = new ArrayList();
    }
    return orders;
  }

  public RepairOrder getOrder(long orderId)
  {
    RepairOrder order = null;
    boolean found = false;

    if( orders != null)
    {
      for( Iterator i = orders.iterator(); i.hasNext();)
      {
        order = (RepairOrder)i.next();
        if(order.getBatchId() == orderId)
        {
          found = true;
          break;
        }
      }
    }

    return found ? order: null;

  }

  public void removeOrder(long orderId)
  {
    RepairOrder order;

    if( orders != null)
    {
      for( Iterator i = orders.iterator(); i.hasNext();)
      {
        order = (RepairOrder)i.next();
        if(order.getBatchId() == orderId)
        {
          i.remove();
          break;
        }
      }
    }
  }

  protected boolean autoSubmit()
  {
    setData(FN_SUBMIT_BTN,"");
    try
    {
      Date fromDate = ((DateStringField)getField(FN_FROM_DATE)).getUtilDate();
      Date toDate = ((DateStringField)getField(FN_TO_DATE)).getUtilDate();
      String searchTerm = getData(FN_SEARCH_TERM);
      searchTerm = searchTerm.length() == 0 ? null : searchTerm;
      orders
        = db.getRepairLookupRows(fromDate,toDate,searchTerm);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error submitting transfer lookup: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      Calendar cal = Calendar.getInstance();
      DateStringField fromField = (DateStringField)fields.getField(FN_FROM_DATE);
      if (fromField.isBlank())
      {
        DateStringField toField = (DateStringField)fields.getField(FN_TO_DATE);
        toField.setUtilDate(cal.getTime());
        cal.add(cal.MONTH,-1);
        fromField.setUtilDate(cal.getTime());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}
