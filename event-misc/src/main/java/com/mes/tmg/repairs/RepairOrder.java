package com.mes.tmg.repairs;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.mes.support.DateTimeFormatter;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgBean;

public class RepairOrder extends TmgBean
{

  private String vendorName = "";
  private long vendorId     = 0L;
  private String clientName = "";
  private long clientId     = 0L;
  private String vrn        = "";
  private long batchId      = 0L;
  private Date openDate     = null;
  private Date shipDate     = null;
  private Date closeDate    = null;
  private String userName   = "";
  private LinkedHashMap items = new LinkedHashMap();

  public boolean equals(Object other)
  {
    if (other instanceof RepairOrder)
    {
      RepairOrder that = (RepairOrder)other;
      return that.batchId == this.batchId;
    }
    return false;
  }

  public void setVendorName(String v)
  {
    vendorName = v;
  }

  public void setVendorId(long vid)
  {
    vendorId = vid;
  }

  public void setClientName(String c)
  {
    clientName = c;
  }

  public void setClientId(long cid)
  {
    clientId = cid;
  }

  public void setVRN(String vrn)
  {
    this.vrn = vrn;
  }

  public void setOpenDate(Date date)
  {
    this.openDate = date;
  }

  public void setOpenDateTs(Timestamp dateTs)
  {
    this.openDate = Tmg.toDate(dateTs);
  }

  public void setShipDate(Date date)
  {
    this.shipDate = date;
  }

  public void setShipDateTs(Timestamp dateTs)
  {
    this.shipDate = Tmg.toDate(dateTs);
  }

  public void setCloseDate(Date date)
  {
    this.closeDate = date;
  }

  public void setCloseDateTs(Timestamp dateTs)
  {
    this.closeDate = Tmg.toDate(dateTs);
  }

  public void setBatchId(long batchId)
  {
    this.batchId = batchId;
  }

  public void setRepairOrderItems(List list)
  {
    if(list != null)
    {
      RepairOrderItem item;
      for(Iterator i = list.iterator();i.hasNext();)
      {
        item = (RepairOrderItem)i.next();
        items.put(item.getSerNum(),item);
      }
    }
  }

  public List getRepairOrderItems()
  {
    return new ArrayList(items.values());
  }

  public LinkedHashMap getItems()
  {
    return items;
  }

  public long getBatchId()
  {
    return batchId;
  }

  public String getBatchIdStr()
  {
    return String.valueOf(batchId);
  }

  public String getVendorName()
  {
    return vendorName;
  }

  public long getVendorId()
  {
    return vendorId;
  }

  public String getClientName()
  {
    return clientName;
  }

  public long getClientId()
  {
    return clientId;
  }

  public String getVRN()
  {
    return vrn;
  }

  public String getOpenDateStr()
  {
    return isNull(openDate)?"--":DateTimeFormatter.getFormattedDate(openDate,"MMM/dd/yyyy");
  }

  public String getShipDateStr()
  {
    return isNull(shipDate)?"--":DateTimeFormatter.getFormattedDate(shipDate,"MMM/dd/yyyy");
  }

  public String getCloseDateStr()
  {
    return isNull(closeDate)?"--":DateTimeFormatter.getFormattedDate(closeDate,"MMM/dd/yyyy");
  }

  public Date getOpenDate()
  {
    return openDate;
  }

  public Date getShipDate()
  {
    return shipDate;
  }

  public Date getCloseDate()
  {
    return closeDate;
  }

  public Date getOpenDateTs()
  {
    return Tmg.toTimestamp(openDate);
  }

  public Date getShipDateTs()
  {
    return Tmg.toTimestamp(shipDate);
  }

  public Date getCloseDateTs()
  {
    return Tmg.toTimestamp(closeDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public boolean hasBeenSent()
  {
    return shipDate != null;
  }

  public boolean hasBeenClosed()
  {
    return closeDate != null;
  }

  private boolean isNull(Object o)
  {
    if (o == null)
    {
      return true;
    }
    return false;
  }

  /**
    * builds a String[] of serial numbers from the keyset
    * of the item list for 'update all' transaction on main page
   **/
  public String[] getItemIdArray()
  {
    String serNum;
    String[] ids = new String[items.size()];
    int counter = 0;
    for(Iterator i = items.keySet().iterator(); i.hasNext();)
    {
      ids[counter++] = (String)i.next();
    }
    return ids;
  }

  //checks all items to ensure that all items have valid
  //test results (false also if list is empty)
  public boolean allItemsValid()
  {
    boolean result = items.size()==0?false:true;
    if(result)
    {
      RepairOrderItem item;
      for(Iterator i = items.values().iterator(); i.hasNext();)
      {
        item = (RepairOrderItem)i.next();
        String pat = item.getProblemAsTested();
        if(pat == null || pat.equals(""))
        {
          result = false;
          break;
        }
      }
    }
    return result;
  }

}