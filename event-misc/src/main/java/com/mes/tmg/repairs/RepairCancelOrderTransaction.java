package com.mes.tmg.repairs;

import java.util.Iterator;
import org.apache.log4j.Logger;
import com.mes.tmg.Part;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public class RepairCancelOrderTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(RepairCancelOrderTransaction.class);

  private TmgAction tmgAction;
  private RepairOrder order;

  public RepairCancelOrderTransaction(TmgDb db, UserBean user, TmgAction tmgAction,
    RepairOrder order)
  {
    super(db,user,null,db.getOpCode(Tmg.OP_PART_CANCEL_REPAIR_ORDER));
    this.tmgAction = tmgAction;
    this.order = order;
  }
  public RepairCancelOrderTransaction(UserBean user, TmgAction tmgAction,  RepairOrder order)
  {
    this(new TmgDb(),user,tmgAction,order);
  }

  public int executeOperation(PartOperation op)
  {
    log.debug(op.toString());
    Part p;
    for (Iterator i = order.getRepairOrderItems().iterator(); i.hasNext(); )
    {
      RepairOrderItem item = (RepairOrderItem)i.next();
      p = db.getPartWithSerialNum(item.getSerNum());
      p.setStatusCode(p.SC_IN);
      db.changePart(p,op,user);
      db.deleteRepairPart(item,user);
      db.logAction(tmgAction,user,"Part " + p.getPartId()
          + " removed from repair order " + item.getOrderId(),p.getPartId());
    }

    db.deleteRepairOrder(order,user);
    db.logAction(tmgAction,user,"Repair Order " + order.getBatchId() + " canceled",
      order.getBatchId());
    return COMMIT;
  }
}
