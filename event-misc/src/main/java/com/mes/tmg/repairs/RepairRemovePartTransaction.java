package com.mes.tmg.repairs;

import org.apache.log4j.Logger;
import com.mes.tmg.Part;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public class RepairRemovePartTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(RepairRemovePartTransaction.class);

  private TmgAction tmgAction;
  private RepairViewState viewState;

  public RepairRemovePartTransaction(TmgDb db, UserBean user, TmgAction tmgAction,
    RepairViewState viewState, Part part)
  {
    super(db,user,part,db.getOpCode(Tmg.OP_PART_REMOVE_REPAIR_ORDER));
    this.tmgAction = tmgAction;
    this.viewState = viewState;
  }

  public int executeOperation(PartOperation op)
  {
    part.setStatusCode(part.SC_IN);
    db.changePart(part,op,user);

    RepairOrderItem item = (RepairOrderItem)viewState.removeItem(part.getSerialNum());

    db.deleteRepairPart(item,user);

    db.logAction(tmgAction,user,"Part " + part.getPartId() + " removed from repair batch "
      + viewState.getOrder().getBatchId(),part.getPartId());

    return COMMIT;
  }
}