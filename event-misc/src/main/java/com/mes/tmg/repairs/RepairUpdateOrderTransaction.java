package com.mes.tmg.repairs;

import java.util.Iterator;
import org.apache.log4j.Logger;
import com.mes.tmg.Part;
import com.mes.tmg.PartOperation;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public class RepairUpdateOrderTransaction extends RepairOrderTransaction
{
  static Logger log = Logger.getLogger(RepairUpdateOrderTransaction.class);

  private String[] ids;
  private String status;
  private String _class;
  private String condition;
  private long encrypt;

  public RepairUpdateOrderTransaction(TmgDb db, UserBean user, TmgAction tmgAction,
    RepairOrder order, String[] ids, String status, String _class, long encrypt, String condition)
  {
    super(db,user,tmgAction,db.getOpCode(Tmg.OP_PART_UPDATE_REPAIR_ORDER), order);
    this.ids = ids;
    this.status = status;
    this._class = _class;
    this.encrypt = encrypt;
    this.condition = condition;
  }

  public RepairUpdateOrderTransaction(UserBean user, TmgAction tmgAction,
    RepairOrder order, String[] ids, String status, String _class, long encrypt, String condition)
  {
    this(new TmgDb(),user,tmgAction, order, ids, status, _class, encrypt, condition);
  }

  //handle the individual String ids - update part status
  public int executeOperation(PartOperation op)
  {
    Part p;
    boolean itemFound = false;
    //look at the selected ids
    for(int j = 0; ids != null && j < ids.length; j++)
    {
      //long id = StringUtilities.stringToLong(ids[j]);
        String currentId = ids[j];
      //compare the id to the order and change if match found
      for (Iterator i = order.getRepairOrderItems().iterator(); i.hasNext(); )
      {
        RepairOrderItem item = (RepairOrderItem)i.next();
        if(currentId.equals(item.getSerNum()))
        {
          p = db.getPartWithSerialNum(item.getSerNum());
          //all use the same status
          if(status.length()>0)
          {
            p.setStatusCode(status);
            //if coming back, auto update to WAREHOUSE
            //remove test Id link
            if(status.equals("IN"))
            {
              p.setLocCode("WAREHOUSE");
              p.setTestId(0);
            }
          }
          if(_class.length()>0)
          {
            p.setPcCode(_class);
          }
          if(condition.length()>0)
          {
            p.setCondCode(condition);
          }
          if(encrypt>0)
          {
            p.setEtId(encrypt);
          }
          db.changePart(p,op,user);
          db.logAction(tmgAction,user,"Part " + p.getPartId()
              + " updated in repair order " + item.getOrderId(),p.getPartId());
          if (!itemFound)
          {
            itemFound = true;
          }
          break;
        }
      }
    }
    if(itemFound)
    {
      msg = "Selected items were successfully updated.";
    }
    else
    {
      msg = "No items were found / updated.";
    }
    return COMMIT;
  }

}