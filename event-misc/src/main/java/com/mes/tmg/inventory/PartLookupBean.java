package com.mes.tmg.inventory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.Downloadable;
import com.mes.persist.NullFilter;
import com.mes.persist.Selection;
import com.mes.support.MesMath;
import com.mes.tmg.Client;
import com.mes.tmg.Deployment;
import com.mes.tmg.Disposition;
import com.mes.tmg.EncryptionType;
import com.mes.tmg.Location;
import com.mes.tmg.PageManagerBean;
import com.mes.tmg.PartClass;
import com.mes.tmg.PartFeature;
import com.mes.tmg.PartPrice;
import com.mes.tmg.PartState;
import com.mes.tmg.PartStateDateFilter;
import com.mes.tmg.PartStateNotZeroFilter;
import com.mes.tmg.PartStateSnFilter;
import com.mes.tmg.PartStatus;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.ClientTable;
import com.mes.tmg.util.PartTypeTable;
import com.mes.tools.DropDownTable;

public class PartLookupBean extends TmgViewBean implements Downloadable
{
  static Logger log = Logger.getLogger(PartLookupBean.class);
  
  public static final String  FN_DATE          = "date";
  public static final String  FN_CLIENT_ID     = "clientId";
  public static final String  FN_PT_ID         = "ptId";
  public static final String  FN_PC_CODE       = "pcCode";
  public static final String  FN_LOC_CODE      = "locCode";
  public static final String  FN_STATUS_CODE   = "statusCode";
  public static final String  FN_COND_CODE     = "condCode";
  public static final String  FN_DISP_CODE     = "dispCode";
  public static final String  FN_ET_ID         = "etId";
  public static final String  FN_LOST_FLAG     = "lostFlag";
  public static final String  FN_MO_FLAG       = "moFlag";
  public static final String  FN_LEASE_FLAG    = "leaseFlag";
  public static final String  FN_SERIAL_NUM    = "serialNum";
  public static final String  FN_CATEGORY      = "category";
  public static final String  FN_MANUFACTURER  = "manufacturer";

  public static final String  FN_SUBMIT_BTN    = "submit";

  public static final String  AN_CLEAR         = "actionClear";
  public static final String  AN_SET_ORDER     = "actionSetOrder";

  public static final String  FN_PAGE_SIZE     = PageManagerBean.FN_PAGE_SIZE;
  public static final String  FN_PAGE_NUM      = PageManagerBean.FN_PAGE_NUM;
  public static final String  FN_ROW_COUNT     = PageManagerBean.FN_ROW_COUNT;

  public static final String  AN_NEW_PAGE_NUM  = PageManagerBean.AN_NEW_PAGE_NUM;

  public final static int     CI_ID           = 0;
  public final static int     CI_FROM         = 1;
  public final static int     CI_TO           = 2;
  public final static int     CI_CLIENT       = 3;
  public final static int     CI_DESCRIPTION  = 4;
  public final static int     CI_SN           = 5;
  public final static int     CI_PRICE        = 6;
  public final static int     CI_COUNT        = 7;
  public final static int     CI_CLASS        = 8;
  public final static int     CI_STATUS       = 9;
  public final static int     CI_LOC          = 10;
  public final static int     CI_COND         = 11;
  public final static int     CI_DISP         = 12;
  public final static int     CI_ENCRYPT      = 13;
  public final static int     CI_LOST         = 14;
  public final static int     CI_MO           = 15;
  public final static int     CI_LEASE        = 16;
  public final static int     CI_MERCH        = 17;
  public final static int     CI_MERCH_NUM    = 18;

  public static final String[] colNames = 
  {
    "ID",
    "From",
    "To",
    "Client",
    "Description",
    "Serial No.",
    "Price",
    "Count",
    "Class",
    "Status",
    "Loc",
    "Cond",
    "Disp",
    "Encrypt",
    "Lost",
    "M.O.",
    "Lease",
    "Merchant",
    "Merch No."
  };

  private PageManagerBean   pageMgr;
  private Selection         lookupSel;
  private List              partStates;
  private int               orderColIdx;
  private boolean           descendingFlag;

  /**
   * Drop down tables
   */

  static String ANY_DD_SEL = "-- any --"
    + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
    + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

  protected class MyPartTypeTable extends PartTypeTable
  {
    public MyPartTypeTable()
    {
      super(PartLookupBean.this.db);
      setAny("",ANY_DD_SEL);
    }
  }

  protected class MyClientTable extends ClientTable
  {
    public MyClientTable()
    {
      super(PartLookupBean.this.db,OPT_SEL_ANY);
      setAny("",ANY_DD_SEL);
    }
  }

  protected class ClassTable extends DropDownTable
  {
    public ClassTable()
    {
      addElement("",ANY_DD_SEL);
      List list = db.getPartClasses();
      for (Iterator i = list.iterator(); i.hasNext(); )
      {
        PartClass pc = (PartClass)i.next();
        addElement(pc.getPcCode(),pc.getPcName());
      }
    }
  }

  protected class LocationTable extends DropDownTable
  {
    public LocationTable()
    {
      addElement("",ANY_DD_SEL);
      List list = db.getLocations();
      for (Iterator i = list.iterator(); i.hasNext(); )
      {
        Location loc = (Location)i.next();
        addElement(loc.getLocCode(),loc.getLocName());
      }
    }
  }

  protected class StatusTable extends DropDownTable
  {
    public StatusTable()
    {
      addElement("",ANY_DD_SEL);
      List list = db.getPartStatuses();
      for (Iterator i = list.iterator(); i.hasNext(); )
      {
        PartStatus status = (PartStatus)i.next();
        addElement(status.getStatusCode(),status.getStatusName());
      }
    }
  }

  protected class ConditionTable extends DropDownTable
  {
    public ConditionTable()
    {
      addElement("",ANY_DD_SEL);
      List list = db.getConditions();
      for (Iterator i = list.iterator(); i.hasNext(); )
      {
        com.mes.tmg.Condition cond = (com.mes.tmg.Condition)i.next();
        addElement(cond.getCondCode(),cond.getCondName());
      }
    }
  }

  protected class DispositionTable extends DropDownTable
  {
    public DispositionTable()
    {
      addElement("",ANY_DD_SEL);
      List list = db.getDispositions();
      for (Iterator i = list.iterator(); i.hasNext(); )
      {
        Disposition disp = (Disposition)i.next();
        addElement(disp.getDispCode(),disp.getDispName());
      }
    }
  }

  protected class EncryptionTable extends DropDownTable
  {
    public EncryptionTable()
    {
      addElement("",ANY_DD_SEL);
      List list = db.getEncryptTypes();
      for (Iterator i = list.iterator(); i.hasNext(); )
      {
        EncryptionType et = (EncryptionType)i.next();
        addElement(String.valueOf(et.getEtId()),et.getEtName());
      }
    }
  }

  protected class FeatureTable extends DropDownTable
  {
    public FeatureTable(String ftCode)
    {
      addElement("",ANY_DD_SEL);
      PartFeature feat = new PartFeature();
      feat.setFtCode(ftCode);
      Selection sel = db.getSelection(feat,false);
      sel.addIndex(db.IDX_FEAT_FT_CODE);
      List list = db.selectAll(sel);
      for (Iterator i = list.iterator(); i.hasNext(); )
      {
        feat = (PartFeature)i.next();
        addElement(String.valueOf(feat.getFeatId()),feat.getFeatName());
      }
    }
  }


  protected class FlagFilterTable extends DropDownTable
  {
    public FlagFilterTable()
    {
      addElement("",ANY_DD_SEL);
      addElement("n","No");
      addElement("y","Yes");
    }
  }

  protected class ActionField extends Field
  {
    public ActionField(String fname)
    {
      super(fname,0,0,true);
    }

    protected void validateData()
    {
      hasError = false;
    }
  }
  
  public PartLookupBean(TmgAction action)
  {
    super(action);
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      pageMgr = new PageManagerBean();
      pageMgr.createPageFields(this);

      fields.add(new DateStringField(FN_DATE,"Date",true,false));
      fields.add(new DropDownField(FN_PT_ID,"Part Type",new MyPartTypeTable(),true));
      fields.add(new DropDownField(FN_CATEGORY,"Category",new FeatureTable("CATEGORY"),true));
      fields.add(new DropDownField(FN_MANUFACTURER,"Manufacturer",new FeatureTable("MANUFACTURER"),true));
      fields.add(new DropDownField(FN_PC_CODE,"Class",new ClassTable(),true));
      fields.add(new DropDownField(FN_LOC_CODE,"Location",new LocationTable(),true));
      fields.add(new DropDownField(FN_STATUS_CODE,"Status",new StatusTable(),true));
      fields.add(new DropDownField(FN_COND_CODE,"Condition",new ConditionTable(),true));
      fields.add(new DropDownField(FN_DISP_CODE,"Disposition",new DispositionTable(),true));
      fields.add(new DropDownField(FN_ET_ID,"Encryption",new EncryptionTable(),true));
      fields.add(new DropDownField(FN_LOST_FLAG,"Lost",new FlagFilterTable(),true));
      fields.add(new DropDownField(FN_MO_FLAG,"Merchant Owned",new FlagFilterTable(),true));
      fields.add(new DropDownField(FN_LEASE_FLAG,"Leased",new FlagFilterTable(),true));
      fields.add(new Field(FN_SERIAL_NUM,"Serial No.",64,20,true));
      Client client = getClient();
      if (client != null && client.isMes())
      {
        fields.add(new DropDownField(FN_CLIENT_ID,new MyClientTable(),true));
      }
      else
      {
        String clientId = (client != null ? ""+client.getClientId() : "0" );
        fields.add(new HiddenField(FN_CLIENT_ID,clientId));
      }

      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
      fields.add(new ButtonField(AN_CLEAR,"Clear"));

      fields.add(new ActionField(AN_SET_ORDER));
      fields.add(new ActionField(AN_NEW_PAGE_NUM));

      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
      e.printStackTrace();
    }
  }

  public List getPartStates()
  {
    // create a new empty list if no list present or an autosubmit failed
    if (partStates == null || (isAutoSubmit() && !isAutoValid()))
    {
      partStates = new ArrayList();
    }
    return partStates;
  }
  public List getRows()
  {
    return getPartStates();
  }

  public PageManagerBean getPageMgr()
  {
    return pageMgr;
  }

  private int getColumnHeaderIndex(String colName)
  {
    int colIdx = -1;
    for (int i = 0; i < colNames.length && colIdx < 0; ++i)
    {
      if (colNames[i].equals(colName)) colIdx = i;
    }
    return colIdx;
  }

  public String getColumnHeader(String colName)
  {
    int colIdx = getColumnHeaderIndex(colName);
    if (colIdx >= 0)
    {
      String actionUrl = handler.getLink(Tmg.ACTION_LOOKUP_PARTS).renderUrl();      
      StringBuffer buf = new StringBuffer();
      buf.append("<a href=\"");
      buf.append(actionUrl);
      buf.append("&" + AN_SET_ORDER + "=" + colIdx + "\">");
      buf.append(colName);
      buf.append("</a>");
      if (colIdx == orderColIdx)
      {
        buf.append(descendingFlag ? "(v)" : "(^)");
      }
      return buf.toString();
    }
    return colName;
  }

  private void setOrder(Selection sel)
  {
    switch (orderColIdx)
    {
      case CI_ID:
      default:
        sel.addOrder("part_id",descendingFlag);
        break;

      case CI_FROM:
        sel.addOrder("start_ts",descendingFlag);
        break;

      case CI_TO:
        sel.addOrder("end_ts",descendingFlag);
        break;

      case CI_CLIENT:
        sel.addSubtableOrder("tmg_clients","client_name",descendingFlag);
        break;

      case CI_DESCRIPTION:
        sel.addSubtableOrder("tmg_part_types","description",descendingFlag);
        break;

      case CI_SN:
        sel.addOrder("serial_num",descendingFlag);
        break;

      case CI_PRICE:
        sel.addSubtableOrder("tmg_part_prices","part_price",descendingFlag);
        break;

      case CI_COUNT:
        sel.addOrder("count",descendingFlag);
        break;

      case CI_CLASS:
        sel.addOrder("pc_code",descendingFlag);
        break;

      case CI_STATUS:
        sel.addOrder("status_code",descendingFlag);
        break;

      case CI_LOC:
        sel.addOrder("loc_code",descendingFlag);
        break;

      case CI_COND:
        sel.addOrder("cond_code",descendingFlag);
        break;

      case CI_DISP:
        sel.addOrder("disp_code",descendingFlag);
        break;

      case CI_ENCRYPT:
        sel.addSubtableOrder("tmg_encrypt_types","et_code",descendingFlag);
        break;

      case CI_LOST:
        sel.addOrder("lost_flag",descendingFlag);
        break;

      case CI_MO:
        sel.addOrder("owner_merch_num",descendingFlag);
        break;

      case CI_LEASE:
        sel.addOrder("lease_flag",descendingFlag);
        break;

      case CI_MERCH:
        sel.addSubtableOrder("tmg_deployment","merch_name",descendingFlag);
        break;

      case CI_MERCH_NUM:
        sel.addSubtableOrder("tmg_deployment","merch_num",descendingFlag);
        break;
    }
  }

  private Selection createLookupSelection()
  {
    // gather filter data
    long clientId     = getField(FN_CLIENT_ID).asLong();
    long ptId         = getField(FN_PT_ID).asLong();
    long catFeatId    = getField(FN_CATEGORY).asLong();
    long manFeatId    = getField(FN_MANUFACTURER).asLong();
    String pcCode     = getData(FN_PC_CODE);
    String locCode    = getData(FN_LOC_CODE);
    String statusCode = getData(FN_STATUS_CODE);
    String condCode   = getData(FN_COND_CODE);
    String dispCode   = getData(FN_DISP_CODE);
    long etId         = getField(FN_ET_ID).asLong();
    String lostFlag   = getData(FN_LOST_FLAG);
    String moFlag     = getData(FN_MO_FLAG);
    String leaseFlag  = getData(FN_LEASE_FLAG);
    String serialNum  = getData(FN_SERIAL_NUM);
    Date partDate = null;
    DateStringField dateField = (DateStringField)getField(FN_DATE);
    if (!dateField.isBlank())
    {
      partDate = dateField.getUtilDate();
    }
    else
    {
      partDate = Calendar.getInstance().getTime();
    }

    // create a selection
    PartState ps = new PartState();
    Selection sel = db.getSelection(ps,false);

    if (clientId != 0L)
    {
      Client client = new Client();
      client.setClientId(clientId);
      sel.addSubIndex("idx_cli_key","tmg_clients",client);
    }
    if (ptId != 0L)
    {
      ps.setPtId(ptId);
      sel.addIndex(db.IDX_PS_PT_ID);
    }
    if (catFeatId != 0L)
    {
      Selection subSel = db.getSelection(db.JOIN_PT_FEAT);
      PartFeature feat = new PartFeature();
      feat.setFeatId(catFeatId);
      subSel.addJoinIndex(db.IDX_FEAT_KEY,feat);
      sel.addSubSel(subSel,"pt_id");
    }
    if (manFeatId != 0L)
    {
      Selection subSel = db.getSelection(db.JOIN_PT_FEAT);
      PartFeature feat = new PartFeature();
      feat.setFeatId(manFeatId);
      subSel.addJoinIndex(db.IDX_FEAT_KEY,feat);
      sel.addSubSel(subSel,"pt_id");
    }
    if (pcCode.length() > 0)
    {
      ps.setPcCode(pcCode);
      sel.addIndex(db.IDX_PS_PC_CODE);
    }
    if (locCode.length() > 0)
    {
      ps.setLocCode(locCode);
      sel.addIndex(db.IDX_PS_LOC_CODE);
    }
    if (statusCode.length() > 0)
    {
      ps.setStatusCode(statusCode);
      sel.addIndex(db.IDX_PS_STATUS_CODE);
    }
    if (condCode.length() > 0)
    {
      ps.setCondCode(condCode);
      sel.addIndex(db.IDX_PS_COND_CODE);
    }
    if (dispCode.length() > 0)
    {
      ps.setDispCode(dispCode);
      sel.addIndex(db.IDX_PS_DISP_CODE);
    }
    if (etId != 0L)
    {
      ps.setEtId(etId);
      sel.addIndex(db.IDX_PS_ET_ID);
    }
    if (lostFlag.length() > 0)
    {
      ps.setLostFlag(lostFlag);
      sel.addIndex(db.IDX_PS_LOST_FLAG);
    }
    if (moFlag.length() > 0)
    {
      sel.addFilter(new NullFilter("owner_merch_num",false));
    }
    if (leaseFlag.length() > 0)
    {
      ps.setLeaseFlag(leaseFlag);
      sel.addIndex(db.IDX_PS_LEASE_FLAG);
    }
    if (serialNum.length() > 0)
    {
      sel.addFilter(new PartStateSnFilter(serialNum));
    }
    sel.addFilter(new PartStateDateFilter(new Timestamp(partDate.getTime())));
    sel.addFilter(new PartStateNotZeroFilter());

    // set ordering
    setOrder(sel);

    return sel;
  }

  private void loadPartStates()
  {
    // fetch part states, load part list with parts from states
    partStates = db.selectAll(lookupSel);
  }

  private void updateSelectionPaging()
  {
    lookupSel.setPageSize(pageMgr.getPageSize());
    lookupSel.setPageNum(pageMgr.getPageNum());
  }

  /**
   * Create a new selection object based on the field data, set page manager
   * data and update the page info in the selection object.
   */
  private void refreshSelection()
  {
    lookupSel = createLookupSelection();
    pageMgr.setRowCount(db.selectCount(lookupSel));
    updateSelectionPaging();
  }

  /**
   * Changes the lookup selection order by storing an order column index and
   * refreshing the lookup selection and reloading part states.
   */
  private boolean changeOrder(String idxStr)
  {
    try
    {
      // store the order column index, toggle flag if same col or reset to false
      int newColIdx = Integer.parseInt(idxStr);
      descendingFlag = newColIdx == orderColIdx ? !descendingFlag : false;
      orderColIdx = newColIdx;

      // refresh lookup sel
      refreshSelection();
      
      // load part states from selection
      loadPartStates();

      return true;
    }
    catch (Exception e)
    {
      log.error("Error changing order: " + e);
      e.printStackTrace();
    }

    return false;
  }

  /**
   * Change page number and reload part list.
   */
  private boolean changePageNum()
  {
    try
    {
      // update page data
      updateSelectionPaging();

      // refresh part list
      loadPartStates();

      return true;
    }
    catch (Exception e)
    {
      log.error("Error changing page: " + e);
      e.printStackTrace();
    }

    return false;
  }

  /**
   * Resets all field data to blanks and clears the part state list.
   */
  private boolean clearFields()
  {
    for (Iterator i = fields.getFieldsVector().iterator(); i.hasNext();)
    {
      Field f = (Field)i.next();
      f.setData("");
    }
    partStates = new ArrayList();
    return true;
  }

  /** 
   * Handle various action requests supported by lookup screen.  These include
   * changing lookup selection ordering, clearing search terms and changing
   * selection page number.
   */
  protected boolean autoAct()
  {
    String actionVal = getData(autoActionName);
    setData(autoActionName,"");
    if (autoActionName.equals(AN_SET_ORDER))
    {
      return changeOrder(actionVal);
    }
    else if (autoActionName.equals(AN_NEW_PAGE_NUM))
    {
      return changePageNum();
    }
    else if (autoActionName.equals(AN_CLEAR))
    {
      return clearFields();
    }
    return false;
  }

  /**
   * Create new lookup selection, configure paging and perform part lookup
   */
  protected boolean autoSubmit()
  {
    setData(FN_SUBMIT_BTN,"");
    try
    {
      // create a new selection object
      refreshSelection();

      // do lookup and load parts list
      loadPartStates();

      return true;
    }
    catch (Exception e)
    {
      log.error("Error submitting part lookup: " + e);
      e.printStackTrace();
    }

    return false;
  }

  /**
   * Does nothing.
   */
  protected boolean autoLoad()
  {
    return true;
  }

  /**
   * Download support
   */
  
  public void startDownload()
  {
    db.startSelection(createLookupSelection());
  }

  public String getDlHeader()
  {
    return "\"Part ID\""
        + ",From"
        + ",To"
        + ",Client"
        + ",Description"
        + ",\"Serial No.\""
        + ",Price"
        + ",Count"
        + ",Class"
        + ",Status"
        + ",Location"
        + ",Condition"
        + ",Disposition"
        + ",Encryption"
        + ",Lost"
        + ",\"Merch. Owned\""
        + ",Leased"
        + ",Merchant"
        + ",\"Merch. No.\"";
  }

  public String getDlNext()
  {
    PartState ps = (PartState)db.getNext();
    if (ps != null)
    {
      StringBuffer buf = new StringBuffer();
      buf.append("\"" + ps.getPartId() + "\"");
      buf.append(",\"" + ps.formatDate(ps.getStartDate()) + "\"");
      buf.append(",\"" + ps.formatDate(ps.getEndDate()) + "\"");
      buf.append(",\"" + ps.getClient().getClientName() + "\"");
      buf.append(",\"" + ps.getPartType().getDescription() + "\"");
      buf.append(",\"" + (ps.getPartType().hasSn() ? ps.getSerialNum() : "--") + "\"");
      PartPrice pp = ps.getPartPrice();
      String priceStr = "";
      if (pp != null)
      {
        priceStr = MesMath.toCurrency(pp.getPartPrice());
      }
      buf.append(",\"" + priceStr + "\"");
      buf.append(",\"" + ps.getCount() + "\"");
      buf.append(",\"" + ps.getPartClass().getPcName() + "\"");
      buf.append(",\"" + ps.getPartStatus().getStatusName() + "\"");
      buf.append(",\"" + ps.getLocation().getLocName() + "\"");
      buf.append(",\"" + ps.getCondition().getCondName() + "\"");
      buf.append(",\"" + ps.getDisposition().getDispName() + "\"");
      buf.append(",\"" + ps.getEncryptionType().getEtName() + "\"");
      buf.append(",\"" + (ps.isLost() ? "Y" : "N") + "\"");
      buf.append(",\"" + (ps.isMerchantOwned() ? "Y" : "N") + "\"");
      buf.append(",\"" + (ps.isLeased() ? "Y" : "N") + "\"");
      String merchNum = null;
      String merchName = null;
      Deployment d = ps.getDeployment();
      if (d != null)
      {
        merchName = d.getMerchName();
        merchNum = d.getMerchNum();
      }
      buf.append(",\"" + (merchNum != null ? merchNum : "--") + "\"");
      buf.append(",\"" + (merchName != null ? merchName : "--") + "\"");
      return buf.toString();
    }
    return null;
  }

  public void finishDownload()
  {
    db.finishSelection();
  }

  public String getDlFilename()
  {
    return "partlookup";
  }
}