package com.mes.tmg.inventory;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.HiddenField;
import com.mes.persist.Transaction;
import com.mes.tmg.Part;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.EncryptionTable;

public class EncryptionTypeBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(EncryptionTypeBean.class);
  
  public static final String FN_PART_ID       = "partId";
  public static final String FN_ET_ID         = "etId";
  
  public static final String FN_SUBMIT_BTN    = "submit";
  
  private Part part;

  public EncryptionTypeBean(TmgAction action)
  {
    super(action);
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_PART_ID));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
      fields.add(new DropDownField(FN_ET_ID,new EncryptionTable(),false));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  public Part getPart()
  {
    return part;
  }

  protected boolean autoSubmit()
  {
    try
    {
      long partId = getField(FN_PART_ID).asLong();
      part = db.getPart(partId);
      part.setEtId(getField(FN_ET_ID).asLong());
      if (part.getEtId() != part.getEncryptionType().getEtId())
      {
        Transaction t 
          = new ChangePartTransaction(
                    db,user,part,db.getOpCode(Tmg.OP_PART_ENCRYPT_TYPE_EDIT));
        if (!t.run())
        {
          throw new RuntimeException(
            "Error changing encryption type in change part type transaction");
        }

        // reload part with updated encryption type
        part = db.getPart(partId);
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      long partId = getField(FN_PART_ID).asLong();
      if (partId != 0L)
      {
        part = db.getPart(partId);
        setData(FN_ET_ID,String.valueOf(part.getEtId()));
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
    }
    return false;
  }
}