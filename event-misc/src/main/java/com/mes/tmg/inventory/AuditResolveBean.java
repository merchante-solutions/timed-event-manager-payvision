package com.mes.tmg.inventory;

import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.HiddenField;
import com.mes.persist.Transaction;
import com.mes.tmg.AuditException;
import com.mes.tmg.AuditResolution;
import com.mes.tmg.Part;
import com.mes.tmg.PartOpCode;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgBean;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;

public class AuditResolveBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(AuditResolveBean.class);
  
  public static final String FN_AUDIT_EX_ID   = "auditExId";
  public static final String FN_RESOLVE_CODE  = "resolveCode";
  public static final String FN_SUBMIT_BTN    = "submit";

  private AuditException auditException;
  
  public AuditResolveBean(TmgAction action)
  {
    super(action);
  }

  public class ResolutionTable extends TmgDropDownTable
  {
    public ResolutionTable()
    {
      super(OPT_SEL_ONE);

      AuditException audEx = getAuditException();
      if (audEx != null)
      {
        for (Iterator i = audEx.getResolutionOptions().iterator();
             i.hasNext();)
        {
          AuditResolution res = (AuditResolution)i.next();
          addElement(res.getName(),res.getDescription());
        }
      }
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new HiddenField(FN_AUDIT_EX_ID));
      fields.addAlias(FN_AUDIT_EX_ID,"editId");
      long auditExId = Long.parseLong(request.getParameter(FN_AUDIT_EX_ID));
      fields.setData(FN_AUDIT_EX_ID,String.valueOf(auditExId));
      getAuditException();
      fields.add(new DropDownField(FN_RESOLVE_CODE,"Resolution",new ResolutionTable(),true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public AuditException getAuditException()
  {
    if (auditException == null)
    {
      long auditExId = getField(FN_AUDIT_EX_ID).asLong();
      if (auditExId != 0L)
      {
        auditException = db.getAuditException(auditExId);
      }
    }
    return auditException;
  }

  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      AuditException ex = getAuditException();
      AuditResolution res 
        = AuditResolution.getByName(getData(FN_RESOLVE_CODE));

      // automatically flag part as lost if resolution indicates it
      if (res.getName().equals("PART_LOST"))
      {
        Part part = db.getPart(ex.getPartId());
        if (!part.isLost())
        {
          part.setLostFlag(TmgBean.FLAG_YES);
          part.setStatusCode(part.SC_REMOVED);
          part.setLocCode(part.LC_GONE);
          PartOpCode poc = db.getOpCode(Tmg.OP_PART_LOST);
          Transaction t = new ChangePartTransaction(db,user,part,poc);
          if (!t.run())
          {
            throw new RuntimeException("Error in part lost transaction");
          }
        }
      }

      ex.resolve(res,user.getLoginName(),db.getCurDate(),0L);
      db.persist(ex,user);
        
      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
    }

    return submitOk;
  }
  
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
    }
    
    return loadOk;
  }
}