package com.mes.tmg.inventory;

import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.tmg.Bucket;
import com.mes.tmg.Disposition;
import com.mes.tmg.EncryptionType;
import com.mes.tmg.Inventory;
import com.mes.tmg.Location;
import com.mes.tmg.Part;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PartStatus;
import com.mes.tmg.PartType;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgSession;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.ConditionTable;
import com.mes.tmg.util.PartClassTable;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;

public class PartBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PartBean.class);
  
  public static final String FN_PART_ID           = "partId";
  public static final String FN_PC_CODE           = "pcCode";
  public static final String FN_ORIG_PC_CODE      = "origPcCode";
  public static final String FN_LOC_CODE          = "locCode";
  public static final String FN_STATUS_CODE       = "statusCode";
  public static final String FN_COND_CODE         = "condCode";
  public static final String FN_DISP_CODE         = "dispCode";
  public static final String FN_SERIAL_NUM        = "serialNum";
  public static final String FN_COUNT             = "count";
  public static final String FN_ET_ID             = "etId";
  public static final String FN_LOST_FLAG         = "lostFlag";
  public static final String FN_OWNER_MERCH_NUM   = "ownerMerchNum";
  public static final String FN_LEASE_FLAG        = "leaseFlag";
  
  public static final String FN_SUBMIT_BTN        = "submit";
  
  protected class StatusTable extends DropDownTable
  {
    public StatusTable()
    {
      addElement("","select one");
      List stats = db.getPartStatuses();
      for (Iterator i = stats.iterator(); i.hasNext(); )
      {
        PartStatus status = (PartStatus)i.next();
        addElement(status.getStatusCode(),status.getStatusName());
      }
    }
  }
  
  protected class LocationTable extends DropDownTable
  {
    public LocationTable()
    {
      addElement("","select one");
      List locs = db.getLocations();
      for (Iterator i = locs.iterator(); i.hasNext(); )
      {
        Location loc = (Location)i.next();
        addElement(String.valueOf(loc.getLocCode()),loc.getLocName());
      }
    }
  }
  
  protected class DispositionTable extends DropDownTable
  {
    public DispositionTable()
    {
      addElement("","select one");
      List disps = db.getDispositions();
      for (Iterator i = disps.iterator(); i.hasNext(); )
      {
        Disposition disp = (Disposition)i.next();
        addElement(disp.getDispCode(),disp.getDispName());
      }
    }
  }
  
  protected class EncryptTypeTable extends DropDownTable
  {
    public EncryptTypeTable()
    {
      addElement("","select one");
      List etList = db.getEncryptTypes();
      for (Iterator i = etList.iterator(); i.hasNext(); )
      {
        EncryptionType et = (EncryptionType)i.next();
        addElement(String.valueOf(et.getEtId()),et.getEtName());
      }
    }
  }
  
  private Inventory inventory;
  private Bucket bucket;
  private PartType partType;
  private Part part;

  public PartBean(TmgAction action)
  {
    super(action);
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      TmgSession tmgSession = ((TmgAction)action).getTmgSession();
      bucket = tmgSession.getPartBucket(action);
      inventory = tmgSession.getInventory(action);
      partType = tmgSession.getPartType(action);

      fields.add(new HiddenField(FN_PART_ID));
      fields.addAlias(FN_PART_ID,"editId");
      fields.add(new DropDownField(FN_STATUS_CODE,new StatusTable(),false));
      fields.add(new DropDownField(FN_PC_CODE,new PartClassTable(),false));
      fields.add(new DropDownField(FN_ORIG_PC_CODE,new PartClassTable(),true));
      fields.add(new DropDownField(FN_LOC_CODE,new LocationTable(),false));
      fields.add(new DropDownField(FN_COND_CODE,new ConditionTable(db),false));
      fields.add(new DropDownField(FN_DISP_CODE,new DispositionTable(),false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
      if (partType.hasSn())
      {
        fields.add(new Field(FN_SERIAL_NUM,"Serial No.",64,32,true));
        fields.add(new HiddenField(FN_COUNT,"1"));
      }
      else
      {
        fields.add(new HiddenField(FN_SERIAL_NUM));
        fields.add(new NumberField(FN_COUNT,"Count",10,10,false,0));
      }
      if (partType.hasEncrypt())
      {
        fields.add(new DropDownField(FN_ET_ID,new EncryptTypeTable(),false));
      }
      else
      {
        fields.add(new HiddenField(FN_ET_ID,String.valueOf(db.getNoneEncryptType().getEtId())));
      }
      fields.add(new CheckField(FN_LOST_FLAG,"Lost","y",true));
      fields.add(new CheckField(FN_LEASE_FLAG,"Leased","y",true));
      fields.add(new Field(FN_OWNER_MERCH_NUM,"Owner Merchant No.",64,32,true));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  public Inventory getInventory()
  {
    return inventory;
  }
  
  public PartType getPartType()
  {
    return partType;
  }
  
  public Bucket getBucket()
  {
    return bucket;
  }
  
  public boolean hasSerialNum()
  {
    return partType.hasSn();
  }
  
  public boolean hasEncrypt()
  {
    return partType.hasEncrypt();
  }
  
  public Part getPart()
  {
    return part;
  }

  public class PartTransaction extends PersistPartTransaction
  {
    private long finalPartId = -1L;
    private boolean isNew;

    public PartTransaction(TmgDb db, UserBean user, Part part, boolean isNew)
    {
      super(db,user,part,
        db.getOpCode(isNew ? Tmg.OP_PART_MAN_ADD : Tmg.OP_PART_MAN_EDIT));
      this.isNew = isNew;
    }

    public int executeOperation(PartOperation op)
    {
      if (isNew)
      {
        finalPartId = db.addPart(part,op,user);
      }
      else
      {
        finalPartId = db.changePart(part,op,user);
      }
      return COMMIT;
    }

    public long getFinalPartId()
    {
      return finalPartId;
    }
  }

  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long partId = getField(FN_PART_ID).asLong();
      
      boolean isNew = partId == 0L;
      if (isNew)
      {
        part = new Part();
        // following is redundant, new part id will be assigned automatically
        //partId = db.getNewId();
        //part.setPartId(partId);
        part.setBuckId(bucket.getBuckId());
        part.setPtId(bucket.getPtId());
      }
      else
      {
        part = db.getPart(partId);
      }
      
      part.setStatusCode(getData(FN_STATUS_CODE));
      part.setPcCode(getData(FN_PC_CODE));
      part.setOrigPcCode(isNew ? part.getPcCode() : getData(FN_ORIG_PC_CODE));
      part.setLocCode(getData(FN_LOC_CODE));
      part.setCondCode(getData(FN_COND_CODE));
      part.setDispCode(getData(FN_DISP_CODE));
      part.setSerialNum(getData(FN_SERIAL_NUM));
      part.setCount(getField(FN_COUNT).asInteger());
      part.setEtId(getField(FN_ET_ID).asLong());
      part.setLostFlag(getData(FN_LOST_FLAG));
      part.setOwnerMerchNum(getData(FN_OWNER_MERCH_NUM));
      part.setLeaseFlag(getData(FN_LEASE_FLAG));
      
      PartTransaction pt = new PartTransaction(db,user,part,isNew);
      if (!pt.run())
      {
        throw new RuntimeException("Failed to persist part");
      }
      // fetch new parts to refresh subbeans
      part = db.getPart(pt.getFinalPartId());

      log.debug("Final part: " + part);

      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting part: " + e);
    }

    return submitOk;
  }

  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long partId = getField(FN_PART_ID).asLong();
      
      if (partId != 0L)
      {
        part = db.getPart(partId);
        setData(FN_STATUS_CODE,part.getStatusCode());
        setData(FN_PC_CODE,part.getPcCode());
        setData(FN_ORIG_PC_CODE,part.getOrigPcCode());
        setData(FN_LOC_CODE,part.getLocCode());
        setData(FN_COND_CODE,String.valueOf(part.getCondCode()));
        setData(FN_DISP_CODE,part.getDispCode());
        setData(FN_SERIAL_NUM,part.getSerialNum());
        setData(FN_COUNT,String.valueOf(part.getCount()));
        setData(FN_ET_ID,String.valueOf(part.getEtId()));
        setData(FN_LOST_FLAG,part.getLostFlag());
        setData(FN_OWNER_MERCH_NUM,part.getOwnerMerchNum());
        setData(FN_LEASE_FLAG,part.getLeaseFlag());
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading part: " + e);
    }
    
    return loadOk;
  }
}