package com.mes.tmg.inventory;

import org.apache.log4j.Logger;
import com.mes.tmg.Deployment;
import com.mes.tmg.Part;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public class ChangeDispositionTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(ChangeDispositionTransaction.class);

  private TmgAction action;

  public ChangeDispositionTransaction(TmgDb db, TmgAction action, UserBean user, 
    Part part)
  {
    super(db,user,part,db.getOpCode(Tmg.OP_PART_DISP_CHANGE));
    this.action = action;
  }

  public int executeOperation(PartOperation op)
  {
    // set end deploy details in associated deployment record
    Deployment oldDep = part.getDeployment();
    if (oldDep == null)
    {
      throw new NullPointerException("Null deployment in part");
    }
    oldDep.setEndUserName(user.getLoginName());
    oldDep.setEndDate(db.getCurDate());
    oldDep.setEndCode("CHANGE");
    db.persist(oldDep,user);
    db.logAction(action,user,"Deployment record " + oldDep.getDeployId()
      + " ended due to disposition change",oldDep.getDeployId());

    // create deployment record for part
    Deployment newDep = new Deployment();
    newDep.setDeployId(db.getNewId());
    newDep.setStartUserName(user.getLoginName());
    newDep.setStartOrdId(oldDep.getStartOrdId());
    newDep.setStartDate(db.getCurDate());
    newDep.setStartCode("CHANGE");
    newDep.setMerchNum(oldDep.getMerchNum());
    newDep.setClientId(oldDep.getClientId());
    newDep.setPartId(oldDep.getPartId());
    db.persist(newDep,user);
    db.logAction(action,user,"Deployment record " + newDep.getDeployId()
      + " created due to disposition change",newDep.getDeployId());
    
    boolean isRental = part.getDispCode().equals(part.DC_RENT);

    // change part disposition and deploy id and merch owner
    part.setDispCode(isRental ? Part.DC_SOLD : Part.DC_RENT);
    part.setDeployId(newDep.getDeployId());
    part.setOwnerMerchNum(isRental ? newDep.getMerchNum() : null);
    db.changePart(part,op,user);
    db.logAction(action,user,"Part " + part.getPartId() + " disposition"
      + " changed (" + part.getDispCode() + ")",part.getPartId());

    return COMMIT;
  }
}
