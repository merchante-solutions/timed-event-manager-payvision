package com.mes.tmg.inventory;

import java.util.List;
import org.apache.log4j.Logger;
import com.mes.mvc.Link;
import com.mes.tmg.Audit;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class AuditAction extends TmgAction
{
  static Logger log = Logger.getLogger(AuditAction.class);
  
  public String getActionObject(String actionName)
  {
    return "Audit";
  }

  public String getActionResult(String actionName)
  {
    if (actionName.equals(Tmg.ACTION_NEW_AUDIT))
    {
      return "started";
    }
    else if (actionName.equals(Tmg.ACTION_COMPLETE_AUDIT))
    {
      return "completed";
    }
    return null;
  }

  public void doNewAudit()
  {
    AuditBean bean = new AuditBean(this);
    if (bean.isAutoSubmitOk())
    {
      setBackLink(Tmg.ACTION_SHOW_NEW_AUDIT,getBackLink());
      long auditId = bean.getAudit().getAuditId();
      Link link = handler.getLink(Tmg.ACTION_SHOW_NEW_AUDIT,"auditId=" + auditId);
      logAndRedirect(link,auditId);
    }
    else
    {
      doView(Tmg.VIEW_NEW_AUDIT);
    }
  }

  private AuditLookupBean getAuditLookupBean()
  {
    AuditLookupBean bean 
      = (AuditLookupBean)getSessionAttr(Tmg.SN_AUDIT_LOOKUP_BEAN,null);
    if (bean == null)
    {
      bean = new AuditLookupBean(db);
      setSessionAttr(Tmg.SN_AUDIT_LOOKUP_BEAN,bean);
    }
    return bean;
  }

  public void doLookupAudits()
  {
    AuditLookupBean bean = getAuditLookupBean();
    bean.autoSetFields(request);
    request.setAttribute("viewBean",bean);
    if (bean.isAutoSubmit() && !bean.isAutoValid())
    {
      addFeedback(bean);
    }
    doView(Tmg.VIEW_LOOKUP_AUDITS);
  }
  
  public void doShowAudit()
  {
    Audit audit = db.getAudit(getParmLong("auditId"));
    request.setAttribute("audit",audit);
    List auditExes = db.getAuditExceptionsForAudit(audit.getAuditId());
    request.setAttribute("showItems",audit.getAuditExceptions());
    doView(Tmg.VIEW_SHOW_AUDIT,"Audit No. " + audit.getAuditId());
  }

  public void doShowNewAudit()
  {
    doShowAudit();
  }

  public void doViewAuditDetails()
  {
    doView(Tmg.VIEW_AUDIT_DETAILS);
  }

  public void doAuditExResolve()
  {
    AuditResolveBean bean = new AuditResolveBean(this);
    request.setAttribute("auditException",bean.getAuditException());
    if (bean.isAutoSubmitOk())
    {
      logAndRedirect(getBackLink(),"Exception resolved");
    }
    else
    {
      doView(Tmg.VIEW_AUDIT_EX_RESOLVE);
    }
  }

  public void doCompleteAudit()
  {
    long auditId = getParmLong("auditId");
    Audit audit = db.getAudit(auditId);
    if (audit == null)
    {
      redirectWithFeedback(getBackLink(),"Audit with id " + auditId
        + " does not exist");
    }
    else if (!audit.canComplete())
    {
      redirectWithFeedback(getBackLink(),
        "Audit cannot currently be completed");
    }
    else if (isConfirmed())
    {
      audit.setFinishDate(db.getCurDate());
      db.persist(audit,user);
      logAndRedirect(getBackLink(),auditId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Audit not completed");
    }
    else
    {
      confirm("Complete audit #" + audit.getAuditId() + "?");
    }
  }
}
