package com.mes.tmg.inventory;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.HiddenField;
import com.mes.tmg.Bucket;
import com.mes.tmg.Inventory;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.UnusedPartTypeTable;

public class BucketBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(BucketBean.class);
  
  public static final String FN_BUCK_ID     = "buckId";
  public static final String FN_PT_ID       = "ptId";
  
  public static final String FN_SUBMIT_BTN  = "submit";
  
  private Inventory inventory;
  private Bucket    bucket;

  public BucketBean(TmgAction action)
  {
    super(action);
  }
  
  public Inventory getInventory()
  {
    return inventory;
  }

  public Bucket getBucket()
  {
    return bucket;
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_BUCK_ID));
      fields.addAlias(FN_BUCK_ID,"editId");

      if (inventory == null)
      {
        inventory = ((TmgAction)action).getTmgSession().getInventory(action);
      }
      fields.add(new DropDownField(FN_PT_ID,new UnusedPartTypeTable(db,inventory),false));
      
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  /**
   * Persists inventory.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long buckId = getField(FN_BUCK_ID).asLong();
      
      if (buckId == 0L)
      {
        buckId = db.getNewId();
        bucket = new Bucket();
        bucket.setBuckId(buckId);
      }
      else
      {
        bucket = db.getBucket(buckId);
      }
      
      bucket.setInvId(inventory.getInvId());
      bucket.setPtId(getField(FN_PT_ID).asLong());
    
      db.persist(bucket,user);
      if (bucket.getPartType() == null)
      {
        // hack: refetch new buckets to fill in part type object, etc.
        bucket = db.getBucket(buckId);
      }
      
      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting bucket: " + e);
      e.printStackTrace();
    }

    return submitOk;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long buckId = getField(FN_BUCK_ID).asLong();
      
      if (buckId != 0L)
      {
        bucket = db.getBucket(buckId);
        setData(FN_PT_ID,
          String.valueOf(bucket.getPtId()));
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading bucket: " + e);
      e.printStackTrace();
    }
    
    return loadOk;
  }
}