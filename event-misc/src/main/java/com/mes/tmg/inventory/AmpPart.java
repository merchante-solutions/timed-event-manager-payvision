package com.mes.tmg.inventory;

import org.apache.log4j.Logger;
import com.mes.tmg.Inventory;
import com.mes.tmg.PartType;

public class AmpPart
{
  static Logger log = Logger.getLogger(AmpBean.class);

  private int       partIdx;
  private Inventory inventory;
  private PartType  partType;
  private String    sourceCode;
  private String    pcCode;
  private double    partPrice;
  private String    serialNum;
  private int       quantity = 1;
  private String    noteText;
  private String    ownerMerchNum;
  private boolean   moFlag;

  public AmpPart(int partIdx, Inventory inventory, PartType partType,
    String sourceCode, String pcCode, double partPrice, String serialNum,
    int quantity, String noteText, boolean moFlag, String ownerMerchNum)
  {
    this.partIdx = partIdx;
    this.inventory = inventory;
    this.partType = partType;
    this.sourceCode = sourceCode;
    this.pcCode = pcCode;
    this.partPrice = partPrice;
    this.serialNum = serialNum;
    quantity = quantity == 0 ? 1 : quantity;
    this.quantity = quantity;
    this.noteText = noteText;
    this.moFlag = moFlag;
    log.debug("moFlag = " + this.moFlag);
    this.ownerMerchNum = ownerMerchNum;
  }
  public AmpPart(int partIdx, AmpBean bean)
  {
    this(partIdx,
         bean.getInventory(),
         bean.getPartType(),
         bean.getData(bean.FN_SOURCE_CODE),
         bean.getData(bean.FN_PC_CODE),
         bean.getField(bean.FN_PART_PRICE).asDouble(),
         bean.getData(bean.FN_SERIAL_NUM),
         bean.getField(bean.FN_QUANTITY).asInt(),
         bean.getData(bean.FN_NOTE_TEXT),
         false, // for now, owner merch num only being used by call tags
         null); 
  }

  public int getPartIdx()
  {
    return partIdx;
  }

  public Inventory getInventory()
  {
    return inventory;
  }

  public PartType getPartType()
  {
    return partType;
  }

  public String getSourceCode()
  {
    return sourceCode;
  }

  public String getPcCode()
  {
    return pcCode;
  }

  public double getPartPrice()
  {
    return partPrice;
  }

  public String getSerialNum()
  {
    return serialNum;
  }

  public int getQuantity()
  {
    return quantity;
  }
  
  public String getNoteText()
  {
    return noteText;
  }

  public boolean isMerchantOwned()
  {
    return moFlag;
  }

  public String getOwnerMerchNum()
  {
    return ownerMerchNum;
  }

  public String toString()
  {
    return this.getClass().getName() + " [ "
      + "idx: " + partIdx
      + ", inv: " + inventory.getInvName()
      + ", pt: " + partType.getPtName()
      + ", source: " + sourceCode
      + ", class: " + pcCode
      + ", price: " + partPrice
      + (serialNum != null ? ", serial num: " + serialNum : "")
      + (serialNum == null ? ", quantity: " + quantity : "")
      + (moFlag ? ", merch owned: true" : "")
      + (ownerMerchNum != null ? ", owner merch num: " + ownerMerchNum : "")
      + " ]";
  }
}

