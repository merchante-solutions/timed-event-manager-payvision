package com.mes.tmg.inventory;

import org.apache.log4j.Logger;
import com.mes.mvc.View;
import com.mes.tmg.Bucket;
import com.mes.tmg.Inventory;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class BucketAction extends TmgAction
{
  static Logger log = Logger.getLogger(BucketAction.class);

  public String getActionObject(String actionName)
  {
    return "Inventory part type";
  }

  private void doShowBuckets()
  {
    Inventory inventory = tmgSession.getInventory(this);
    request.setAttribute("inventory",inventory);
    request.setAttribute("showItems",db.getBucketsForInventory(inventory));
    View view = handler.getView(Tmg.VIEW_SHOW_BUCKETS);
    String viewTitle = null;
    if (view.isTitled())
    {
      viewTitle = inventory.getInvName() + " " + view.getTitle();
      doView(Tmg.VIEW_SHOW_BUCKETS,viewTitle);
    }
    else
    {
      doView(Tmg.VIEW_SHOW_BUCKETS);
    }
  }
  
  private void doAddBucket()
  {
    BucketBean bean = new BucketBean(this);
    if (bean.isAutoSubmitOk())
    {
      Bucket bucket = bean.getBucket();
      logAndRedirect(Tmg.ACTION_SHOW_BUCKETS,
        bucket.getPartType().getPtName(),bucket.getBuckId());
    }
    else
    {
      doView(Tmg.VIEW_ADD_BUCKET);
    }
  }

  private void doDeleteBucket()
  {
    long delId = getParmLong("delId");
    Bucket bucket = db.getBucket(delId);
    if (bucket == null)
    {
      throw new RuntimeException("Inventory part type with id " + delId
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deleteBucket(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_BUCKETS,
        bucket.getPartType().getPtName(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_BUCKETS,
        "Inventory part type not deleted");
    }
    else
    {
      confirm("Delete inventory part type " 
        + bucket.getPartType().getPtName() + "?");
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_SHOW_BUCKETS))
    {
      doShowBuckets();
    }
    else if (name.equals(Tmg.ACTION_ADD_BUCKET))
    {
      doAddBucket();
    }
    else if (name.equals(Tmg.ACTION_DELETE_BUCKET))
    {
      doDeleteBucket();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
