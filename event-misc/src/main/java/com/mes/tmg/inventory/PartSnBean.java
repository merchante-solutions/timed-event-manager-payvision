package com.mes.tmg.inventory;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.Validation;
import com.mes.persist.Transaction;
import com.mes.tmg.Part;
import com.mes.tmg.PartOpCode;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class PartSnBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PartSnBean.class);
  
  public static final String FN_PART_ID       = "partId";
  public static final String FN_SERIAL_NUM    = "serialNum";
  
  public static final String FN_SUBMIT_BTN    = "submit";
  
  private Part part;

  public PartSnBean(TmgAction action)
  {
    super(action);
  }

  public class SnValidation implements Validation
  {
    public String getErrorText()
    {
      return "Serial number already exists in inventory";
    }
    public boolean validate(String data)
    {
      if (data != null & data.length() > 0)
      {
        return db.getPartWithSerialNum(data.trim()) == null;
      }
      return true;
    }
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_PART_ID));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
      fields.add(new Field(FN_SERIAL_NUM,"Serial No.",64,32,true));
      fields.getField(FN_SERIAL_NUM).addValidation(new SnValidation());
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  public Part getPart()
  {
    if (part == null)
    {
      long partId = getField(FN_PART_ID).asLong();
      part = db.getPart(partId);
    }
    return part;
  }

  protected boolean autoSubmit()
  {
    try
    {
      long partId = getField(FN_PART_ID).asLong();
      part = db.getPart(partId);
      part.setSerialNum(getData(FN_SERIAL_NUM));
      PartOpCode poc = db.getOpCode(Tmg.OP_PART_SN_EDIT);
      Transaction t = new ChangePartTransaction(db,user,part,poc);
      if (!t.run())
      {
        throw new RuntimeException(
          "Error changing serial number in change part transaction");
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      long partId = getField(FN_PART_ID).asLong();
      if (partId != 0L)
      {
        part = db.getPart(partId);
        setData(FN_SERIAL_NUM,part.getSerialNum());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
    }
    return false;
  }
}