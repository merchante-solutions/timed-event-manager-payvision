package com.mes.tmg.inventory;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.MiscAddSource;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class AmpSourceBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(AmpSourceBean.class);
  
  public static final String FN_MAS_ID   = "masId";
  public static final String FN_MAS_CODE = "masCode";
  public static final String FN_MAS_NAME = "masName";
  
  public static final String FN_SUBMIT_BTN  = "submit";
  
  private MiscAddSource mas;

  public AmpSourceBean(TmgAction action)
  {
    super(action);
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_MAS_ID));
      fields.addAlias(FN_MAS_ID,"editId");
      fields.add(new Field(FN_MAS_CODE,"Code",16,32,false));
      fields.add(new Field(FN_MAS_NAME,"Name",32,32,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public MiscAddSource getMiscAddSource()
  {
    return mas;
  }
  
  /**
   * Persists part mas.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long masId = getField(FN_MAS_ID).asLong();
      
      if (masId == 0L)
      {
        masId = db.getNewId();
        mas = new MiscAddSource();
        mas.setMasId(masId);
      }
      else
      {
        mas = db.getMiscAddSource(masId);
      }
      
      mas.setMasCode(getData(FN_MAS_CODE));
      mas.setMasName(getData(FN_MAS_NAME));
    
      db.persist(mas,user);
      
      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting part mas: " + e);
    }

    return submitOk;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long masId = getField(FN_MAS_ID).asLong();
      
      if (masId != 0L)
      {
        mas = db.getMiscAddSource(masId);
        setData(FN_MAS_CODE,mas.getMasCode());
        setData(FN_MAS_NAME,mas.getMasName());
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading part mas: " + e);
    }
    
    return loadOk;
  }
}