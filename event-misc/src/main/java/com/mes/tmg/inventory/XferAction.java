package com.mes.tmg.inventory;

import java.util.List;
import org.apache.log4j.Logger;
import com.mes.mvc.Link;
import com.mes.tmg.Part;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.Transfer;
import com.mes.tmg.TransferPart;

public class XferAction extends TmgAction
{
  static Logger log = Logger.getLogger(XferAction.class);

  public static final String SN_XFER_VIEW_STATE = "xferViewState";
  public static final String SN_XFER_PART_LOOKUP_BEAN = "xferPartLookupBean";

  private XferViewState getXferViewState()
  {
    XferViewState viewState 
      = (XferViewState)getSessionAttr(SN_XFER_VIEW_STATE,null);
    if (viewState == null)
    {
      viewState = new XferViewState(this,SN_XFER_VIEW_STATE);
      setSessionAttr(SN_XFER_VIEW_STATE,viewState);
    }
    else
    {
      viewState.setAction(this);
    }
    return viewState;
  }

  private void clearXferViewState()
  {
    session.removeAttribute(SN_XFER_VIEW_STATE);
  }

  private XferPartLookupBean getXferPartLookupBean()
  {
    XferPartLookupBean bean 
      = (XferPartLookupBean)getSessionAttr(SN_XFER_PART_LOOKUP_BEAN,null);
    if (bean == null)
    {
      bean = new XferPartLookupBean(this);
      setSessionAttr(SN_XFER_PART_LOOKUP_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  private void clearXferPartLookupBean()
  {
    session.removeAttribute(SN_XFER_PART_LOOKUP_BEAN);
  }

  public void doXferParts()
  {
    XferViewState viewState = getXferViewState();
    if (!viewState.isStarted())
    {
      XferStartBean bean = new XferStartBean(this);
      if (bean.isAutoSubmitOk())
      {
        viewState.startTransfer(bean);
        redirectWithFeedback(Tmg.ACTION_XFER_PARTS,"Transfer started");
      }
      else
      {
        doView(Tmg.VIEW_XFER_START);
      }
    }
    else
    {
      getXferPartLookupBean();
      request.setAttribute("xferParts",viewState.getParts());
      doView(Tmg.VIEW_XFER_MAIN);
    }
  }

  private void clearTransferData()
  {
    clearXferViewState();
    clearXferPartLookupBean();
  }

  public void doXferCancel()
  {
    if (isConfirmed())
    {
      clearTransferData();
      redirectWithFeedback(Tmg.ACTION_XFER_PARTS,"Transfer canceled");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Transfer not canceled");
    }
    else
    {
      confirm("Cancel part transfer?");
    }
  }

  public void doXferComplete()
  {
    XferViewState viewState = getXferViewState();
    if (isConfirmed())
    {
      List parts = viewState.getParts();
      XferTransaction t = new XferTransaction(db,user,this,viewState);
      if (!t.run())
      {
        throw new RuntimeException("Failed to transfer parts");
      }
      clearTransferData();
      Link viewLink = handler.getLink(Tmg.ACTION_XFER_VIEW);
      viewLink.addArg("xferId",""+t.getXferId());
      handler.setBackLink(Tmg.ACTION_XFER_VIEW,
        handler.generateLink("xferMenu",null,"Back to Transfers"));
      redirectWithFeedback(viewLink,"Part transfer completed");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Transfer not completed");
    }
    else
    {
      String fromInvName = viewState.getFromInventory().getInvName();
      String toInvName = viewState.getToInventory().getInvName();
      confirm("Complete transfer of parts from " + fromInvName 
        + " to " + toInvName + "?");
    }
  }

  public void doXferPartLookup()
  {
    getXferViewState();
    XferPartLookupBean lookupBean = getXferPartLookupBean();
    doView(Tmg.VIEW_XFER_PART_LOOKUP);
  }

  public void doXferAddPart()
  {
    XferViewState viewState = getXferViewState();
    XferPartLookupBean bean = getXferPartLookupBean();
    int partIdx = getParmInt("partIdx");
    Part part = viewState.addPart((Part)bean.getParts().get(partIdx));
    redirectWithFeedback(handler.getBackLink(),"Part added to transfer");
  }

  public void doXferRemovePart()
  {
    XferPartLookupBean bean = getXferPartLookupBean();
    XferViewState viewState = getXferViewState();
    int partIdx = getParmInt("partIdx");
    if (viewState.removePart(partIdx) == null)
    {
      redirectWithFeedback(handler.getBackLink(),"Error removing part");
    }
    else
    {
      redirectWithFeedback(handler.getBackLink(),"Part removed from transfer");
    }
  }

  public static final String SN_XFER_LOOKUP_BEAN = "xferLookupBean";

  private XferLookupBean getXferLookupBean()
  {
    XferLookupBean bean = (XferLookupBean)getSessionAttr(SN_XFER_LOOKUP_BEAN,null);
    if (bean == null)
    {
      bean = new XferLookupBean(this);
      setSessionAttr(SN_XFER_LOOKUP_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doXferLookup()
  {
    XferLookupBean bean = getXferLookupBean();
    request.setAttribute("viewBean",bean);
    doView(Tmg.VIEW_XFER_LOOKUP);
  }

  public void doXferView()
  {
    long xferId = getParmLong("xferId");
    Transfer transfer = db.getTransfer(xferId);
    request.setAttribute("transfer",transfer);
    request.setAttribute("xferParts",db.getPartsInTransfer(transfer));
    setNoteItemId(xferId);
    doView(Tmg.VIEW_XFER_VIEW);
  }

  public void doXferPartCancel()
  {
    long xferPartId = getParmLong("xferPartId");
    if (isConfirmed())
    {
      XferCancelTransaction t = new XferCancelTransaction(db,user,this,xferPartId);
      if (!t.run())
      {
        throw new RuntimeException("Failed to cancel part transfer");
      }
      redirectWithFeedback(getBackLink(),"Part transfer canceled successfully");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Part transfer not canceled");
    }
    else
    {
      TransferPart xferPart = db.getTransferPart(xferPartId);
      Transfer xfer = db.getTransfer(xferPart.getXferId());
      confirm("Cancel transfer of part " + xferPart.getPartId() + " from " 
        + xfer.getFromInventory().getInvName() + " to "
        + xfer.getToInventory().getInvName() + "?");
    }
  }
}
