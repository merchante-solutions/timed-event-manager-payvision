package com.mes.tmg.inventory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewState;

public class AmpViewState extends TmgViewState
{
  static Logger log = Logger.getLogger(AmpViewState.class);

  private Map     parts = new HashMap();
  private AmpPart curPart;
  private int     partCount;
  private boolean editModeFlag;

  public AmpViewState(TmgAction action, String name)
  {
    super(action,name);
  }

  public class PartIdxOrder implements Comparator
  {
    public int compare(Object o1, Object o2)
    {
      AmpPart p1 = (AmpPart)o1;
      AmpPart p2 = (AmpPart)o2;
      if (p1.getPartIdx() < p2.getPartIdx()) return -1;
      if (p1.getPartIdx() > p2.getPartIdx()) return 1;
      return 0;
    }
    public boolean equals(Object that)
    {
      return compare(this,that) == 0;
    }
  }

  public List getParts()
  {
    List partList = new ArrayList(parts.values());
    Collections.sort(partList,new PartIdxOrder());
    return partList;
  }

  private Object partKey(int partIdx)
  {
    return partIdx;
  }

  private AmpPart setPart(int partIdx, AmpBean bean)
  {
    AmpPart part = new AmpPart(partIdx,bean);
    parts.put(partKey(partIdx),part);
    curPart = part;
    return part;
  }

  public AmpPart addPart(AmpBean bean)
  {
    int partIdx = partCount++;
    return setPart(partIdx,bean);
  }

  public AmpPart updatePart(AmpBean bean)
  {
    int partIdx = bean.getField(bean.FN_PART_IDX).asInt();
    return setPart(partIdx,bean);
  }

  public AmpPart getPart(int partIdx)
  {
    return (AmpPart)parts.get(partKey(partIdx));
  }

  public AmpPart deletePart(int partIdx)
  {
    return (AmpPart)parts.remove(partKey(partIdx));
  }

  public boolean setCurPart(int partIdx)
  {
    AmpPart part = getPart(partIdx);
    if (part != null)
    {
      curPart = part;
      return true;
    }
    return false;
  }
  public AmpPart getCurPart()
  {
    return curPart;
  }

  public void loadBeanFromPart(AmpBean bean, AmpPart part)
  {
    if (part != null)
    {
      bean.setData(bean.FN_INV_ID,String.valueOf(part.getInventory().getInvId()));
      bean.setData(bean.FN_PT_ID,String.valueOf(part.getPartType().getPtId()));
      bean.setData(bean.FN_SOURCE_CODE,part.getSourceCode());
      bean.setData(bean.FN_PC_CODE,part.getPcCode());
      bean.setData(bean.FN_PART_PRICE,String.valueOf(part.getPartPrice()));
      bean.setData(bean.FN_SERIAL_NUM,part.getSerialNum());
      bean.setData(bean.FN_QUANTITY,String.valueOf(part.getQuantity()));
      bean.setData(bean.FN_NOTE_TEXT,part.getNoteText());
      if (editModeFlag)
      {
        bean.setData(bean.FN_PART_IDX,String.valueOf(part.getPartIdx()));
      }
    }
  }

  public void loadBeanFromIdx(AmpBean bean,int partIdx)
  {
    loadBeanFromPart(bean,getPart(partIdx));
  }

  public void loadBeanFromCurPart(AmpBean bean)
  {
    loadBeanFromPart(bean,getCurPart());
  }

  public boolean inEditMode()
  {
    return editModeFlag;
  }
  public void setEditMode(boolean editModeFlag)
  {
    this.editModeFlag = editModeFlag;
  }

  public void editPart(int partIdx)
  {
    editModeFlag = setCurPart(partIdx);
  }

  public boolean hasSn(String serialNum)
  {
    if (serialNum != null)
    {
      for (Iterator i = getParts().iterator(); i.hasNext();)
      {
        AmpPart part = (AmpPart)i.next();
        String partSn = part.getSerialNum();
        if (partSn != null && partSn.equals(serialNum))
        {
          return true;
        }
      }
    }
    return false;
  }
}