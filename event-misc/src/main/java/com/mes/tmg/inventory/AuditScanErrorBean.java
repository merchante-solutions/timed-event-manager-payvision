package com.mes.tmg.inventory;

import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.HiddenField;
import com.mes.tmg.AuditException;
import com.mes.tmg.AuditResolution;
import com.mes.tmg.PartState;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;

public class AuditScanErrorBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(AuditScanErrorBean.class);
  
  public static final String FN_AUDIT_EX_ID = "auditExId";
  public static final String FN_OTHER_EX_ID = "otherExId";
  
  public static final String FN_SUBMIT_BTN  = "submit";

  private AuditException auditException;
  
  public AuditException getAuditException()
  {
    return auditException;
  }

  public AuditScanErrorBean(TmgAction action)
  {
    super(action);
  }

  protected class OtherExceptionTable extends TmgDropDownTable
  {
    public OtherExceptionTable() throws Exception
    {
      super(OPT_SEL_NONE);
       
      try
      {
        List otherExes = null;
        String resultCode = auditException.getResultCode();
        long auditId = auditException.getAuditId();
        if (resultCode.equals(AuditException.RESULT_MISSING))
        {
          otherExes = db.getUnknownAuditExceptions(auditId);
        }
        else if (resultCode.equals(AuditException.RESULT_UNKNOWN))
        {
          otherExes = db.getMissingAuditExceptions(auditId);
        }
        else
        {
          throw new RuntimeException("Scan error handling for exception with" 
            + " result code " + auditException.getResultCode() 
            + " not implemented");
        }

        for (Iterator i = otherExes.iterator(); i.hasNext();)
        {
          AuditException ex = (AuditException)i.next();
          PartState ps = ex.getAuditPartState();
          String descriptor = "SN " + ex.getSerialNum() + 
            (ps != null ? " (" + ps.getPartType().getDescription() + ")": "");
          addElement(""+ex.getAuditExId(),descriptor);
        }
      }
      catch (Exception e)
      {
        log.debug("Error: " + e);
        e.printStackTrace();
        throw e;
      }
    }
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    long auditExId = Long.parseLong(request.getParameter(FN_AUDIT_EX_ID));
    auditException = db.getAuditException(auditExId);
    
    try
    {
      fields.add(new HiddenField(FN_AUDIT_EX_ID));
      fields.addAlias(FN_AUDIT_EX_ID,"editId");
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
      fields.add(new DropDownField(FN_OTHER_EX_ID,"Other Exception",new OtherExceptionTable(),true));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      String resultCode = auditException.getResultCode();
      long auditId = auditException.getAuditId();
      long auditExId = auditException.getAuditExId();
      long otherExId = fields.getField(FN_OTHER_EX_ID).asLong();

      // is there a related exception?
      if (otherExId > 0)
      {
        // resolve other exception, associate it with this exception
        AuditException otherEx = db.getAuditException(otherExId);

        otherEx.resolve(AuditResolution.SCAN_ERROR,user.getLoginName(),
          db.getCurDate(),auditExId);
        db.persist(otherEx,user);
        auditException.resolve(AuditResolution.SCAN_ERROR,
          user.getLoginName(),db.getCurDate(),otherExId);
        db.persist(auditException,user);
      }
      else
      {
        // resolve just this exception
        auditException.resolve(AuditResolution.SCAN_ERROR,user.getLoginName(),
          db.getCurDate(),otherExId);
        db.persist(auditException,user);
      }
        
      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
    }

    return submitOk;
  }
  
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
    }
    
    return loadOk;
  }
}