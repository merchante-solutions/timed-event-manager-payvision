package com.mes.tmg.inventory;

import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.HiddenField;
import com.mes.persist.Transaction;
import com.mes.tmg.Bucket;
import com.mes.tmg.Part;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PartType;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.tmg.TmgViewBean;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;

public class PartModelBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PartModelBean.class);
  
  public static final String FN_PART_ID       = "partId";
  public static final String FN_PT_ID         = "ptId";
  
  public static final String FN_SUBMIT_BTN    = "submit";
  
  private Part part;

  public PartModelBean(TmgAction action)
  {
    super(action);
  }

  public class PartTypeTable extends DropDownTable
  {
    public PartTypeTable()
    {
      List partTypes = db.getPartTypes();
      for (Iterator i = partTypes.iterator(); i.hasNext(); )
      {
        PartType pt = (PartType)i.next();
        addElement(String.valueOf(pt.getPtId()),
          pt.getModelCode() + " - " + pt.getPtName());
      }
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_PART_ID));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
      fields.add(new DropDownField(FN_PT_ID,new PartTypeTable(),false));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  public Part getPart()
  {
    return part;
  }

  public class ChangePartTypeTransaction extends ChangePartTransaction
  {
    public ChangePartTypeTransaction(TmgDb db, UserBean user, Part part)
    {
      super(db,user,part,db.getOpCode(Tmg.OP_PART_MODEL_EDIT));
    }

    /**
     * Overrides parent method to make sure a bucket exists for the new
     * part type/inventory combination, if one does not exist it must
     * be created for the part.
     */   
    public int executeOperation(PartOperation op)
    {
      // this will still be the old bucket, but inventory is correct
      long invId = part.getBucket().getInvId();

      // this should be set to the new part type
      long ptId = part.getPtId();
      
      // does inventory have bucket already for the new part type?
      Bucket newBucket = db.getBucketForInvPt(invId,ptId);
      if (newBucket == null)
      {
        newBucket = new Bucket();
        newBucket.setBuckId(db.getNewId());
        newBucket.setInvId(invId);
        newBucket.setPtId(ptId);
        db.persist(newBucket,user);
        newBucket = db.getBucket(newBucket.getBuckId());
      }

      // set the new bucket id and bucket object in the part
      part.setBuckId(newBucket.getBuckId());
      part.setBucket(newBucket);

      // persist the change to the part
      return super.executeOperation(op);
    }
  }

  protected boolean autoSubmit()
  {
    try
    {
      long partId = getField(FN_PART_ID).asLong();
      part = db.getPart(partId);
      part.setPtId(getField(FN_PT_ID).asLong());
      if (part.getPtId() != part.getBucket().getPtId())
      {
        Transaction t = new ChangePartTypeTransaction(db,user,part);
        if (!t.run())
        {
          throw new RuntimeException(
            "Error changing part type in change part type transaction");
        }

        // reload part with new bucket/part type
        part = db.getPart(partId);
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      long partId = getField(FN_PART_ID).asLong();
      if (partId != 0L)
      {
        part = db.getPart(partId);
        setData(FN_PT_ID,String.valueOf(part.getPtId()));
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
    }
    return false;
  }
}