package com.mes.tmg.inventory;

import org.apache.log4j.Logger;
import com.mes.tmg.Part;

public class IndexedPart extends Part implements Comparable
{
  static Logger log = Logger.getLogger(AmpBean.class);

  private Integer key;

  public IndexedPart(Part part, int partIdx)
  {
    key = partIdx;
    copyFrom(part);
    log.debug("Indexed part created, copied state from part: " + this);
  }

  public int getPartIdx()
  {
    return key.intValue();
  }

  public Object getKey()
  {
    return key;
  }

  public static Object makeKey(int partIdx)
  {
    return partIdx;
  }

  public int compareTo(Object o)
  {
    IndexedPart that = (IndexedPart)o;
    return this.key.compareTo(that.key);
  }
}
