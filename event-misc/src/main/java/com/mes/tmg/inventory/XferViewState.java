package com.mes.tmg.inventory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.tmg.Inventory;
import com.mes.tmg.Part;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewState;

public class XferViewState extends TmgViewState
{
  static Logger log = Logger.getLogger(XferViewState.class);

  private Map       parts = new HashMap();
  private Map       idMap = new HashMap();
  private int       partCount;
  private boolean   startFlag;
  private Inventory fromInventory;
  private Inventory toInventory;

  public XferViewState(TmgAction action, String name)
  {
    super(action,name);
  }

  public boolean isStarted()
  {
    return startFlag;
  }

  public void startTransfer(XferStartBean bean)
  {
    long fromInvId = bean.getField(bean.FN_FROM_INV_ID).asLong();
    long toInvId = bean.getField(bean.FN_TO_INV_ID).asLong();
    fromInventory = db.getInventory(fromInvId);
    toInventory = db.getInventory(toInvId);
    startFlag = true;
  }

  public Inventory getFromInventory()
  {
    return fromInventory;
  }

  public Inventory getToInventory()
  {
    return toInventory;
  }

  public List getParts()
  {
    List partList = new ArrayList(parts.values());
    Collections.sort(partList);
    return partList;
  }

  private IndexedPart setPart(Part part, int partIdx)
  {
    IndexedPart idxPart = new IndexedPart(part,partIdx);
    parts.put(idxPart.getKey(),idxPart);
    idMap.put(String.valueOf(part.getPartId()),idxPart);
    return idxPart;
  }

  public IndexedPart addPart(Part part)
  {
    return setPart(part,partCount++);
  }

  public IndexedPart removePart(int partIdx)
  {
    IndexedPart part = (IndexedPart)parts.remove(IndexedPart.makeKey(partIdx));
    log.debug("part with idx " + partIdx + (part != null ? "" : " NOT") + " found");
    if (part != null)
    {
      idMap.remove(String.valueOf(part.getPartId()));
    }
    return part;
  }

  public IndexedPart getPartWithId(long partId)
  {
    return (IndexedPart)idMap.get(String.valueOf(partId));
  }

  public boolean hasPartWithId(long partId)
  {
    return getPartWithId(partId) != null;
  }
}