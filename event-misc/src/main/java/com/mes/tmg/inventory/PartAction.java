package com.mes.tmg.inventory;

import java.util.List;
import org.apache.log4j.Logger;
import com.mes.persist.Transaction;
import com.mes.support.MesMath;
import com.mes.tmg.Bucket;
import com.mes.tmg.Inventory;
import com.mes.tmg.NoteAction;
import com.mes.tmg.Part;
import com.mes.tmg.PartHistory;
import com.mes.tmg.PartOpCode;
import com.mes.tmg.PartState;
import com.mes.tmg.PartTest;
import com.mes.tmg.PartType;
import com.mes.tmg.TestResultType;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgBean;

public class PartAction extends TmgAction
{
  static Logger log = Logger.getLogger(PartAction.class);
  
  public String getActionObject(String actionName)
  {
    return "Part";
  }

  public String getActionResult(String actionName)
  {
    String actionResult = null;
    if (actionName.equals(Tmg.ACTION_OBSOLETE_PART))
    {
      actionResult = "obsoleted";
    }
    else if (actionName.equals(Tmg.ACTION_UNDO_OBSOLETE_PART))
    {
      actionResult = "obsolete status undone";
    }
    else if (actionName.equals(Tmg.ACTION_SELL_PART_AS_IS))
    {
      actionResult = "sold as is";
    }
    else if (actionName.equals(Tmg.ACTION_UNDO_SELL_PART_AS_IS))
    {
      actionResult = "sold as is status undone";
    }
    else if (name.equals(Tmg.ACTION_PART_LOST))
    {
      actionResult = "lost";
    }
    else if (name.equals(Tmg.ACTION_PART_FOUND))
    {
      actionResult = "found";
    }
    else if (name.equals(Tmg.ACTION_EDIT_PART_SN))
    {
      actionResult = "serial no. changed";
    }
    else if (name.equals(Tmg.ACTION_EDIT_PART_MODEL))
    {
      actionResult = "part type changed";
    }
    else if (name.equals(Tmg.ACTION_CHANGE_PART_CLASS) ||
             name.equals(Tmg.ACTION_CHANGE_ENCRYPTION_TYPE) ||
             name.equals(Tmg.ACTION_CHANGE_DISPOSITION) ||
             name.equals(Tmg.ACTION_CHANGE_PART_PRICE))
    {
      actionResult = "updated";
    }
    else if (name.equals(Tmg.ACTION_OUT_OF_BOX_FAILURE))
    {
      actionResult = "marked as out of box failure";
    }
    else if (name.equals(Tmg.ACTION_TESTING_OK))
    {
      actionResult = "tested ok, returned to stock";
    }
    else if (name.equals(Tmg.ACTION_NEEDS_INJECTION))
    {
      actionResult = "moved to needs injection";
    }
    else if (name.equals(Tmg.ACTION_REMOVE_MERCH_OWNED))
    {
      actionResult = "merchant owned status removed";
    }
    else if (name.equals(Tmg.ACTION_CANCEL_DEPLOYMENT))
    {
      actionResult = "deployment canceled";
    }
    else
    {
      actionResult = super.getActionResult(actionName);
    }
    return actionResult;
  }

  public void doShowParts()
  {
    Inventory inventory = tmgSession.getInventory(this);
    Bucket bucket = tmgSession.getPartBucket(this);
    PartType partType = tmgSession.getPartType(this);
    request.setAttribute("showItems",db.getPartsInBucket(bucket));
    request.setAttribute("partType",partType);
    doView(Tmg.VIEW_SHOW_PARTS,inventory.getInvName() + " " 
      + partType.getPtName() + " Parts");
  }

  public void doEditPart()
  {
    PartBean bean = new PartBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_PART);
    if (bean.isAutoSubmitOk())
    {
      Part part = bean.getPart();
      logAndRedirect(getBackLink(),part.getDescriptor(),part.getPartId());
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_PART : Tmg.VIEW_ADD_PART);
    }
  }
  public void doAddPart()
  {
    doEditPart();
  }


  public void doDeletePart()
  {
    long delId = getParmLong("delId");
    Part part = db.getPart(delId);
    if (part == null)
    {
      throw new RuntimeException("Part with id " + delId
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deletePart(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_PARTS,part.getDescriptor(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_PARTS,"Part not deleted");
    }
    else
    {
      confirm("Delete part " + part.getDescriptor() + "?");
    }
  }

  private PartLookupBean getPartLookupBean()
  {
    PartLookupBean bean = (PartLookupBean)getSessionAttr(Tmg.SN_PART_LOOKUP_BEAN,null);
    if (bean == null)
    {
      bean = new PartLookupBean(this);
      setSessionAttr(Tmg.SN_PART_LOOKUP_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doLookupParts()
  {
    PartLookupBean bean = getPartLookupBean();
    request.setAttribute("viewBean",bean);
    if (bean.isAutoSubmit() && !bean.isAutoValid())
    {
      addFeedback(bean);
    }
    doView(Tmg.VIEW_LOOKUP_PARTS);
  }

  public void doDlLookupParts()
  {
    doCsvDownload(getPartLookupBean());
  }

  public void doShowPart()
  {
    PartHistoryBean bean = new PartHistoryBean(this);
    request.setAttribute("viewBean",bean);
    setNoteItemId(bean.getHistory().getPartId());
    doView(Tmg.VIEW_SHOW_PART);
  }

  public void doShowPartHistory()
  {
    PartHistoryBean bean = new PartHistoryBean(this);
    request.setAttribute("viewBean",bean);
    doRender(Tmg.VIEW_PART_HISTORY);
  }

  public void doObsoletePart()
  {
    long partId = getParmLong("partId");
    Part part = db.getPart(partId);
    if (part == null)
    {
      redirectWithFeedback(getBackLink(),"Part with id " + partId 
        + " does not exist");
    }
    else if (!part.canObsolete())
    {
      redirectWithFeedback(getBackLink(),
        "Part cannot currently be marked obsolete");
    }
    else if (isConfirmed())
    {
      part.setCondCode(Part.CC_OBSOLETE);
      part.setStatusCode(Part.SC_REMOVED);
      part.setLocCode(Part.LC_GONE);
      PartOpCode poc = db.getOpCode(Tmg.OP_PART_OBSOLETE);
      Transaction t = new ChangePartTransaction(db,user,part,poc);
      if (!t.run())
      {
        throw new RuntimeException("Error in obsolete part transaction");
      }
      NoteAction.insertNote(db,user,name,partId,"Part rendered obsolete");
      logAndRedirect(getBackLink(),part.getDescriptor(),partId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Part not rendered obsolete");
    }
    else
    {
      confirm("Render part " + part.getDescriptor() + " obsolete?");
    }
  }

  public void doChangeDisposition()
  {
    long partId = getParmLong("partId");
    Part part = db.getPart(partId);
    String desc = part.getDispCode().equals(part.DC_SOLD) ?
                  "from sold to rental" : "from rental to sold";
    if (part == null)
    {
      redirectWithFeedback(getBackLink(),"Part with id " + partId 
        + " does not exist");
    }
    else if (!part.canChangeDisposition())
    {
      redirectWithFeedback(getBackLink(),
        "Part disposition may not be changed");
    }
    else if (isConfirmed())
    {
      Transaction t = new ChangeDispositionTransaction(db,this,user,part);
      if (!t.run())
      {
        throw new RuntimeException("Error in change disposition transaction");
      }
      NoteAction.insertNote(db,user,name,partId,
        "Part disposition changed " + desc);
      redirectWithFeedback(getBackLink(),
        "Part disposition changed " + desc);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Part disposition not changed");
    }
    else
    {
      confirm("Change part " + part.getDescriptor() + " disposition " 
        + desc + "?");
    }
  }

  public void doUndoObsoletePart()
  {
    long partId = getParmLong("partId");
    Part part = db.getPart(partId);
    if (part == null)
    {
      redirectWithFeedback(getBackLink(),"Part with id " + partId 
        + " does not exist");
    }
    else if (!part.isObsolete())
    {
      redirectWithFeedback(getBackLink(),"Part is not obsolete,"
        + " cannot reverse obsolete status");
    }
    else if (isConfirmed())
    {
      PartHistory history = db.getPartHistoryForPartId(partId);
      List states = history.getStates();
      PartState priorState = (PartState)states.get(states.size() - 2);
      part = priorState.getPart();
      PartOpCode poc = db.getOpCode(Tmg.OP_PART_OBSOLETE_UNDO);
      Transaction t = new ChangePartTransaction(db,user,part,poc);
      if (!t.run())
      {
        throw new RuntimeException("Error in reverse obsolete status part"
          + " transaction");
      }
      NoteAction.insertNote(db,user,name,partId,"Obsolete status undone");
      logAndRedirect(getBackLink(),part.getDescriptor(),partId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Part remains obsolete");
    }
    else
    {
      confirm("Reverse obsolete status for part " + part.getDescriptor() + "?");
    }
  }

  public void doSellPartAsIs()
  {
    long partId = getParmLong("partId");
    Part part = db.getPart(partId);
    if (part == null)
    {
      redirectWithFeedback(getBackLink(),"Part with id " + partId 
        + " does not exist");
    }
    else if (!part.canSellAsIs())
    {
      redirectWithFeedback(getBackLink(),"Part cannot currently be sold as is");
    }
    else if (isConfirmed())
    {
      part.setStatusCode(Part.SC_REMOVED);
      part.setLocCode(Part.LC_GONE);
      part.setDispCode(Part.DC_SOLD_AS_IS);
      PartOpCode poc = db.getOpCode(Tmg.OP_PART_SOLD_AS_IS);
      Transaction t = new ChangePartTransaction(db,user,part,poc);
      if (!t.run())
      {
        throw new RuntimeException("Error in sold as is part transaction");
      }
      NoteAction.insertNote(db,user,name,partId,"Part sold as is");
      logAndRedirect(getBackLink(),part.getDescriptor(),partId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Part not sold as is");
    }
    else
    {
      confirm("Designate part " + part.getDescriptor() + " sold as is?");
    }
  }

  public void doUndoSellPartAsIs()
  {
    long partId = getParmLong("partId");
    Part part = db.getPart(partId);
    if (part == null)
    {
      redirectWithFeedback(getBackLink(),"Part with id " + partId 
        + " does not exist");
    }
    else if (!part.isSoldAsIs())
    {
      redirectWithFeedback(getBackLink(),"Part is not sold as is,"
        + " cannot reverse sold as is status");
    }
    else if (isConfirmed())
    {
      PartHistory history = db.getPartHistoryForPartId(partId);
      List states = history.getStates();
      PartState priorState = (PartState)states.get(states.size() - 2);
      part = priorState.getPart();
      PartOpCode poc = db.getOpCode(Tmg.OP_PART_SOLD_AS_IS_UNDO);
      Transaction t = new ChangePartTransaction(db,user,part,poc);
      if (!t.run())
      {
        throw new RuntimeException("Error in reverse sold as is status part"
          + " transaction");
      }
      NoteAction.insertNote(db,user,name,partId,"Part sold as is status undone");
      logAndRedirect(getBackLink(),part.getDescriptor(),partId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Part remains sold as is");
    }
    else
    {
      confirm("Reverse sold as is status for part " + part.getDescriptor() + "?");
    }
  }

  public void doPartLost()
  {
    long partId = getParmLong("partId");
    Part part = db.getPart(partId);
    if (part == null)
    {
      redirectWithFeedback(getBackLink(),"Part with id " + partId 
        + " does not exist");
    }
    else if (!part.canLose())
    {
      redirectWithFeedback(getBackLink(),
        "Part cannot currently be flagged as lost");
    }
    else if (isConfirmed())
    {
      part.setLostFlag(TmgBean.FLAG_YES);
      part.setStatusCode(part.SC_REMOVED);
      part.setLocCode(part.LC_GONE);
      PartOpCode poc = db.getOpCode(Tmg.OP_PART_LOST);
      Transaction t = new ChangePartTransaction(db,user,part,poc);
      if (!t.run())
      {
        throw new RuntimeException("Error in part lost transaction");
      }
      NoteAction.insertNote(db,user,name,partId,"Part flagged as lost");
      logAndRedirect(getBackLink(),part.getDescriptor(),partId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Part not flagged as lost");
    }
    else
    {
      confirm("Designate part " + part.getDescriptor() + " as lost?");
    }
  }

  public void doPartFound()
  {
    long partId = getParmLong("partId");
    Part part = db.getPart(partId);
    if (part == null)
    {
      redirectWithFeedback(getBackLink(),"Part with id " + partId 
        + " does not exist");
    }
    else if (!part.isLost())
    {
      redirectWithFeedback(getBackLink(),
        "Part is not lost, cannot mark as found");
    }
    else if (isConfirmed())
    {
      part.setLostFlag(TmgBean.FLAG_NO);
      part.setStatusCode(part.SC_IN);
      part.setLocCode(part.LC_TESTING);
      PartOpCode poc = db.getOpCode(Tmg.OP_PART_FOUND);
      Transaction t = new ChangePartTransaction(db,user,part,poc);
      if (!t.run())
      {
        throw new RuntimeException("Error in part found transaction");
      }
      NoteAction.insertNote(db,user,name,partId,"Part flagged as not lost");
      logAndRedirect(getBackLink(),part.getDescriptor(),partId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Part remains flagged as lost");
    }
    else
    {
      confirm("Designate part " + part.getDescriptor() + " as not lost?");
    }
  }

  public void doEditPartSn()
  {
    PartSnBean bean = new PartSnBean(this);
    if (bean.isAutoSubmitOk())
    {
      Part part = bean.getPart();
      NoteAction.insertNote(db,user,name,part.getPartId(),
        "Part serial no. changed to '" + part.getSerialNum() + "'");
      logAndRedirect(getBackLink(),part.getDescriptor(),part.getPartId());
    }
    else
    {
      doView(Tmg.VIEW_EDIT_PART_SN);
    }
  }

  public void doEditPartModel()
  {
    PartModelBean bean = new PartModelBean(this);
    if (bean.isAutoSubmitOk())
    {
      Part part = bean.getPart();
      NoteAction.insertNote(db,user,name,part.getPartId(),
        "Part type changed to '" + part.getPartType().getPtName() + "'");
      logAndRedirect(getBackLink(),part.getDescriptor(),part.getPartId());
    }
    else
    {
      doView(Tmg.VIEW_EDIT_PART_MODEL);
    }
  }

  public void doChangePartClass()
  {
    PartClassBean bean = new PartClassBean(this);
    if (bean.isAutoSubmitOk())
    {
      Part part = bean.getPart();
      NoteAction.insertNote(db,user,name,part.getPartId(),
        "Part class changed to '" + part.getPartClass().getPcName() + "'");
      logAndRedirect(getBackLink(),part.getDescriptor(),part.getPartId());
    }
    else
    {
      doView(Tmg.VIEW_CHANGE_PART_CLASS);
    }
  }

  public void doChangeEncryptionType()
  {
    EncryptionTypeBean bean = new EncryptionTypeBean(this);
    if (bean.isAutoSubmitOk())
    {
      Part part = bean.getPart();
      NoteAction.insertNote(db,user,name,part.getPartId(),
        "Part encryption changed to '" + part.getEncryptionType().getEtName() 
        + "'");
      logAndRedirect(getBackLink(),part.getDescriptor(),part.getPartId());
    }
    else
    {
      doView(Tmg.VIEW_CHANGE_ENCRYPTION_TYPE);
    }
  }

  /**
   * Sets test results for a part in testing, relocates it to warehouse.
   * Used by doTestingOk and doNeedsInjection, TestResultBean.autoSubmit.
   */
  public void setPartTestResults(Part part, String trtCode)
  {
    // update part state to reflect results, move to warehouse
    boolean isBroken = !trtCode.equals(TestResultType.TRTC_OK);
    PartTest test = part.getPartTest();
    if (test == null)
    {
      part.setTestId(db.getNewId());
    }
    part.setCondCode(isBroken ? part.CC_BROKEN : part.CC_GOOD);
    part.setLocCode(part.LC_WAREHOUSE);
    PartOpCode poc = db.getOpCode(Tmg.OP_PART_TESTED);
    Transaction t = new ChangePartTransaction(db,user,part,poc);
    if (!t.run())
    {
      throw new RuntimeException("Error in part test results transaction");
    }

    // create new part test obj if necessary
    if (test == null)
    {
      test = new PartTest();
      test.setTestId(part.getTestId());
      test.setCreateDate(db.getCurDate());
      test.setUserName(user.getLoginName());
      test.setSerialNum(part.getSerialNum());
      test.setPartId(part.getPartId());
      test.setStateId(
        db.getCurrentPartState(part.getPartId()).getStateId());
    }

    // save the test results
    test.setTrtCode(trtCode);
    db.persist(test,user);

    NoteAction.insertNote(db,user,name,part.getPartId(),
      "Part test results set (" + trtCode + "), removed from testing");

    addFeedback("Part test results set (" + trtCode + ")");
  }

  public void doTestingOk()
  {
    long partId = getParmLong("partId");
    Part part = db.getPart(partId);
    if (part == null)
    {
      redirectWithFeedback(getBackLink(),"Part with id " + partId 
        + " does not exist");
    }
    else if (!part.inTesting())
    {
      redirectWithFeedback(getBackLink(),
        "Part is not in testing, cannot perform operation");
    }
    else if (isConfirmed())
    {
      setPartTestResults(part,TestResultType.TRTC_OK);
      logAndRedirect(getBackLink(),part.getDescriptor(),partId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Part remains in testing");
    }
    else
    {
      confirm("Testing OK, return part " + part.getDescriptor() + " to warehouse?");
    }
  }

  public void doNeedsInjection()
  {
    long partId = getParmLong("partId");
    Part part = db.getPart(partId);
    if (part == null)
    {
      redirectWithFeedback(getBackLink(),"Part with id " + partId 
        + " does not exist");
    }
    else if (!part.getPartType().hasEncrypt())
    {
      redirectWithFeedback(getBackLink(),"Part is not a pin device");
    }
    else if (isConfirmed())
    {
      // automatically set test results for parts in testing
      // when moving to needs injection
      if (part.inTesting())
      {
        setPartTestResults(part,TestResultType.TRTC_OK);
        // reload the part
        part = db.getPart(partId);
      }

      part.setLocCode(part.LC_NEED_INJECT);
      PartOpCode poc = db.getOpCode(Tmg.OP_PART_NEEDS_INJECTION);
      Transaction t = new ChangePartTransaction(db,user,part,poc);
      if (!t.run())
      {
        throw new RuntimeException("Error in needs injection transaction");
      }

      logAndRedirect(getBackLink(),part.getDescriptor(),partId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),
        "Part not relocated to needs injection location");
    }
    else
    {
      confirm("Relocate part " + part.getDescriptor() 
        + " to needs injection location?");
    }
  }

  public void doSetTestResults()
  {
    long partId = getParmLong("partId");
    Part part = db.getPart(partId);
    if (part == null)
    {
      redirectWithFeedback(getBackLink(),"Part with id " + partId 
        + " does not exist");
    }
    else if (!part.inTesting())
    {
      redirectWithFeedback(getBackLink(),
        "Part is not in testing, cannot perform operation");
    }
    else
    {
      TestResultBean bean = new TestResultBean(this);
      if (bean.isAutoSubmitOk())
      {
        part = bean.getPart();
        logAndRedirect(getBackLink(),part.getDescriptor(),part.getPartId());
      }
      else
      {
        doView(Tmg.VIEW_SET_TEST_RESULTS);
      }
    }
  }

  public void doChangePartPrice()
  {
    PartPriceBean bean = new PartPriceBean(this);
    if (bean.isAutoSubmitOk())
    {
      Part part = bean.getPart();
      NoteAction.insertNote(db,user,name,part.getPartId(),
        "Part price changed to " 
          + MesMath.toCurrency(part.getPartPrice().getPartPrice()));
      logAndRedirect(getBackLink(),part.getDescriptor(),part.getPartId());
    }
    else
    {
      doView(Tmg.VIEW_CHANGE_PART_PRICE);
    }
  }

  public void doOutOfBoxFailure()
  {
    long partId = getParmLong("partId");
    Part part = db.getPart(partId);
    if (part == null)
    {
      redirectWithFeedback(getBackLink(),"Part with id " + partId 
        + " does not exist");
    }
    else if (part.inTesting())
    {
      redirectWithFeedback(getBackLink(),
        "Part is in testing already, cannot perform operation");
    }
    else if (isConfirmed())
    {
      part.setLocCode(part.LC_TESTING);
      part.setCondCode(part.CC_BROKEN);
      PartOpCode poc = db.getOpCode(Tmg.OP_PART_OUT_OF_BOX_FAILURE);
      Transaction t = new ChangePartTransaction(db,user,part,poc);
      if (!t.run())
      {
        throw new RuntimeException(
          "Error in part out of box failure transaction");
      }
      NoteAction.insertNote(db,user,name,partId,"Part flagged as out of box failure");
      logAndRedirect(getBackLink(),part.getDescriptor(),partId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),
        "Part not flagged as out of box failure");
    }
    else
    {
      confirm("Mark part as out of box failure and move it to testing?");
    }
  }

  public void doCancelDeployment()
  {
    long partId = getParmLong("partId");
    Part part = db.getPart(partId);
    if (part == null)
    {
      redirectWithFeedback(getBackLink(),"Part with id " + partId 
        + " does not exist");
    }
    else if (isConfirmed())
    {
      Transaction t = new CancelDeploymentTransaction(db,user,part,this);
      if (!t.run())
      {
        throw new RuntimeException("Error in cancel deployment transaction");
      }
      NoteAction.insertNote(db,user,name,partId,
        "Part merchant owned status removed");
      logAndRedirect(getBackLink(),part.getDescriptor(),partId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Deployment not canceled");
    }
    else
    {
      confirm("Cancel deployment (and possible auto-transfer) of part?");
    }
  }

  public void doRemoveMerchOwned()
  {
    long partId = getParmLong("partId");
    Part part = db.getPart(partId);
    if (part == null)
    {
      redirectWithFeedback(getBackLink(),"Part with id " + partId 
        + " does not exist");
    }
    else if (!part.isMerchantOwned())
    {
      redirectWithFeedback(getBackLink(),
        "Part is not currently merchant owned");
    }
    else if (isConfirmed())
    {
      part.setOwnerMerchNum(null);
      PartOpCode poc = db.getOpCode(Tmg.OP_PART_REMOVE_MERCH_OWNED);
      Transaction t = new ChangePartTransaction(db,user,part,poc);
      if (!t.run())
      {
        throw new RuntimeException(
          "Error in remove merchant owned transaction");
      }
      NoteAction.insertNote(db,user,name,partId,"Part merchant owned status removed");
      logAndRedirect(getBackLink(),part.getDescriptor(),partId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),
        "Part merchant owned status not removed");
    }
    else
    {
      confirm("Remove merchant owned status from part?");
    }
  }
}
