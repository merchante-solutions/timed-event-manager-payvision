package com.mes.tmg.inventory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.InventoryTable;
import com.mes.tmg.util.PartClassTable;

public class XferLookupBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(XferLookupBean.class);

  public static final String FN_FROM_DATE     = "fromDate";
  public static final String FN_TO_DATE       = "toDate";
  public static final String FN_FROM_INV_ID   = "fromInvId";
  public static final String FN_TO_INV_ID     = "toInvId";
  public static final String FN_PC_CODE       = "pcCode";
  public static final String FN_SEARCH_TERM   = "searchTerm";
  public static final String FN_SUBMIT_BTN    = "submitBtn";

  private List xferLuRows;

  public XferLookupBean(TmgAction action)
  {
    super(action,"xferLookupBean");
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new DateStringField(FN_FROM_DATE,"From Date",false,false));
      fields.add(new DateStringField(FN_TO_DATE,"To Date",true,false));
      fields.add(new DropDownField(FN_FROM_INV_ID,new InventoryTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
      fields.add(new DropDownField(FN_TO_INV_ID,new InventoryTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
      fields.add(new DropDownField(FN_PC_CODE,new PartClassTable(PartClassTable.OPT_SEL_ANY,false),true));
      fields.add(new Field(FN_SEARCH_TERM,"Search Term",30,20,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

  public List getRows()
  {
    if (xferLuRows == null || (isAutoSubmit() && !isAutoValid()))
    {
      xferLuRows = new ArrayList();
    }
    return xferLuRows;
  }

  protected boolean autoSubmit()
  {
    setData(FN_SUBMIT_BTN,"");
    try
    {
      Date fromDate = ((DateStringField)getField(FN_FROM_DATE)).getUtilDate();
      Date toDate = ((DateStringField)getField(FN_TO_DATE)).getUtilDate();
      long fromInvId = getField(FN_FROM_INV_ID).asLong();
      fromInvId = fromInvId == 0 ? -1L : fromInvId;
      long toInvId = getField(FN_TO_INV_ID).asLong();
      toInvId = toInvId == 0 ? -1L : toInvId;
      String pcCode = getData(FN_PC_CODE);
      pcCode = pcCode.equals("any") ? null : pcCode;
      String searchTerm = getData(FN_SEARCH_TERM);
      searchTerm = searchTerm.length() == 0 ? null : searchTerm;
      xferLuRows = db.getTransferLookupRows(
                        fromDate,toDate,fromInvId,toInvId,pcCode,searchTerm);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error submitting transfer lookup: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      Calendar cal = Calendar.getInstance();
      DateStringField fromField = (DateStringField)fields.getField(FN_FROM_DATE);
      if (fromField.isBlank())
      {
        DateStringField toField = (DateStringField)fields.getField(FN_TO_DATE);
        toField.setUtilDate(cal.getTime());
        cal.add(cal.MONTH,-1);
        fromField.setUtilDate(cal.getTime());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}
