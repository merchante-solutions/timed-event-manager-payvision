package com.mes.tmg.inventory;

import org.apache.log4j.Logger;
import com.mes.tmg.Part;
import com.mes.tmg.PartOpCode;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public class ChangePartTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(ChangePartTransaction.class);

  public ChangePartTransaction(TmgDb db, UserBean user, Part part, PartOpCode poc)
  {
    super(db,user,part,poc);
  }

  public int executeOperation(PartOperation op)
  {
    db.changePart(part,op,user);
    return COMMIT;
  }
}
