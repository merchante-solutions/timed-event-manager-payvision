package com.mes.tmg.inventory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.Validation;
import com.mes.persist.NumberFilter;
import com.mes.persist.Selection;
import com.mes.tmg.Bucket;
import com.mes.tmg.PartState;
import com.mes.tmg.PartStateDateFilter;
import com.mes.tmg.PartStateNotZeroFilter;
import com.mes.tmg.PartStateSnFilter;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.PartTypeTable;

public class XferPartLookupBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(XferPartLookupBean.class);

  public static final String  FN_FROM_INV_ID      = "fromInvId";
  public static final String  FN_PT_ID            = "ptId";
  public static final String  FN_SERIAL_NUM       = "serialNum";
  public static final String  FN_SUBMIT_BTN       = "submitBtn";

  private XferViewState viewState;
  private List parts;
  private List snMatches;

  public XferPartLookupBean(TmgAction action)
  {
    super(action,"xferPartLookupBean");
  }

  public class PartTypeValidation implements Validation
  {
    public String getErrorText()
    {
      return "Part type required if no serial no. given";
    }

    public boolean validate(String data)
    {
      if (getField(FN_SERIAL_NUM).isBlank() && (data == null || data.length() == 0))
      {
        return false;
      }
      return true;
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      viewState = (XferViewState)action.getSessionAttr("xferViewState");
      fields.add(new DropDownField(FN_PT_ID,new PartTypeTable(db),true));
      fields.add(new Field(FN_SERIAL_NUM,"Serial No.",64,32,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.getField(FN_PT_ID).addValidation(new PartTypeValidation());
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

  public List getParts()
  {
    if (parts == null || (isAutoSubmit() && !isAutoValid()))
    {
      parts = new ArrayList();
    }
    return parts;
  }
  public List getRows()
  {
    return getParts();
  }

  public List getSnMatches()
  {
    if (snMatches == null || (isAutoSubmit() && !isAutoValid()))
    {
      snMatches = new ArrayList();
    }
    return snMatches;
  }

  private List selectPartsInInventory()
  {
    long fromInvId = viewState.getFromInventory().getInvId();
    long ptId = getField(FN_PT_ID).asLong();
    String serialNum = getData(FN_SERIAL_NUM);
    Date partDate = Calendar.getInstance().getTime();

    PartState ps = new PartState();
    Selection sel = db.getSelection(ps,false);

    Bucket bucket = new Bucket();
    bucket.setInvId(fromInvId);
    sel.addSubIndex(db.IDX_BUCK_INV_ID,"tmg_buckets",bucket);

    if (ptId != 0L)
    {
      ps.setPtId(ptId);
      sel.addIndex(db.IDX_PS_PT_ID);
    }

    if (serialNum.length() > 0)
    {
      sel.addFilter(new PartStateSnFilter(serialNum));
    }

    sel.addFilter(new PartStateDateFilter(new Timestamp(partDate.getTime())));
    sel.addFilter(new PartStateNotZeroFilter());

    return db.selectAll(sel);
  }

  private List selectSnMatchesOutOfInventory()
  {
    // lookup parts only if a serial numb was given
    String serialNum = getData(FN_SERIAL_NUM);
    if (serialNum.length() > 0)
    {
      long fromInvId = viewState.getFromInventory().getInvId();
      Date partDate = Calendar.getInstance().getTime();

      PartState ps = new PartState();
      Selection sel = db.getSelection(ps,false);

      ps.setSerialNum(serialNum);
      sel.addIndex(db.IDX_PS_SERIAL_NUM);

      sel.addFilter(new NumberFilter("inv_id",fromInvId),"bucket");
      sel.addFilter(new PartStateDateFilter(new Timestamp(partDate.getTime())));
      sel.addFilter(new PartStateNotZeroFilter());

      return db.selectAll(sel);
    }
    return null;
  }

  protected boolean autoSubmit()
  {
    setData(FN_SUBMIT_BTN,"");
    try
    {
      parts = selectPartsInInventory();
      snMatches = selectSnMatchesOutOfInventory();
      return true;
    }
    catch (Exception e)
    {
      log.error("Error submitting part lookup: " + e);
      e.printStackTrace();
    }

    return false;
  }
}
