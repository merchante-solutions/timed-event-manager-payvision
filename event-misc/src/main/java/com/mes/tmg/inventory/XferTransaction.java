package com.mes.tmg.inventory;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.tmg.Bucket;
import com.mes.tmg.Inventory;
import com.mes.tmg.Part;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.tmg.Transfer;
import com.mes.tmg.TransferPart;
import com.mes.user.UserBean;

public class XferTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(XferTransaction.class);

  private TmgAction action;
  private long xferId;
  private List parts;
  private Inventory fromInv;
  private Inventory toInv;

  public XferTransaction(TmgDb db, UserBean user, TmgAction action, 
    List parts, Inventory fromInv, Inventory toInv)
  {
    super(db,user,null,db.getOpCode(Tmg.OP_PART_XFER));
    this.action = action;
    this.parts = parts;
    this.fromInv = fromInv;
    this.toInv = toInv;
  }
  public XferTransaction(TmgDb db, UserBean user, TmgAction action, 
    XferViewState vs)
  {
    this(db,user,action,vs.getParts(),vs.getFromInventory(),vs.getToInventory());
  }

  private Transfer createTransfer(PartOperation op)
  {
    Transfer transfer = new Transfer();
    transfer.setXferId(db.getNewId());
    transfer.setCreateDate(Calendar.getInstance().getTime());
    transfer.setUserName(user.getLoginName());
    transfer.setFromInvId(fromInv.getInvId());
    transfer.setToInvId(toInv.getInvId());
    transfer.setOpId(op.getOpId());
    db.persist(transfer,user);
    xferId = transfer.getXferId();
    return transfer;
  }

  private Bucket getToInventoryBucket(Part part)
  {
    Bucket bucket = db.getBucketForInvPt(toInv.getInvId(),
      part.getPartType().getPtId());
    if (bucket == null)
    {
      bucket = new Bucket();
      bucket.setBuckId(db.getNewId());
      bucket.setInvId(toInv.getInvId());
      bucket.setPtId(part.getPartType().getPtId());
      db.persist(bucket,user);
    }
    return bucket;
  }

  private TransferPart createTransferPart(Transfer transfer, Part part)
  {
    TransferPart xferPart = new TransferPart();
    xferPart.setXferPartId(db.getNewId());
    xferPart.setXferId(transfer.getXferId());
    xferPart.setUserName(transfer.getUserName());
    xferPart.setCreateDate(transfer.getCreateDate());
    xferPart.setPartId(part.getPartId());
    xferPart.setCancelFlag(xferPart.FLAG_NO);
    xferPart.setQuantity(1);
    db.persist(xferPart,user);
    return xferPart;
  }

  public int executeOperation(PartOperation op)
  {
    Transfer transfer = createTransfer(op);
    for (Iterator i = parts.iterator(); i.hasNext();)
    {
      Part part = (Part)i.next();

      part.setBuckId(getToInventoryBucket(part).getBuckId());
      db.changePart(part,op,user);
      log.debug("persisted " + part);

      TransferPart xferPart = createTransferPart(transfer,part);

      String description = "Part " + part.getPartId() + " transferred from "
        + fromInv.getInvName() + " to " + toInv.getInvName();
      db.logAction(action,user,description,part.getPartId());
      db.logAction(action,user,description,xferPart.getXferPartId());
    }
    db.logAction(action,user,"Part transfer completed",transfer.getXferId());
    return COMMIT;
  }

  public long getXferId()
  {
    return xferId;
  }
}
