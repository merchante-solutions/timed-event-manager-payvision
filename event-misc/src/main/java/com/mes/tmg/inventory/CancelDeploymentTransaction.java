package com.mes.tmg.inventory;

import org.apache.log4j.Logger;
import com.mes.tmg.DeployReason;
import com.mes.tmg.Deployment;
import com.mes.tmg.Part;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PartState;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.tmg.Transfer;
import com.mes.tmg.TransferPart;
import com.mes.user.UserBean;

public class CancelDeploymentTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(CancelDeploymentTransaction.class);

  private TmgAction action;

  public CancelDeploymentTransaction(TmgDb db, UserBean user, Part part, 
    TmgAction action)
  {
    super(db,user,part,db.getOpCode(Tmg.OP_PART_DEPLOY_CANCEL));
    this.action = action;
  }
  public CancelDeploymentTransaction(UserBean user, Part part, TmgAction action)
  {
    this(new TmgDb(),user,part,action);
  }

  public int executeOperation(PartOperation op)
  {
    PartState ps = db.getCurrentPartState(part.getPartId());

    // make sure deployment is legal with the part
    if (!ps.canCancelDeployment())
    {
      log.error("Part deployment can't be canceled");
      return ROLLBACK;
    }

    // look for auto transfer to cancel
    if (ps.getPartOp().getOpCode().equals(Tmg.OP_PART_XFER))
    {
      log.debug("Canceling auto transfer of part");
      Transfer xfer = db.getTransferWithOpId(ps.getOpId());
      TransferPart xferPart = xfer.getXferPartWithPartId(ps.getPartId());
      xferPart.setCancelFlag(xferPart.FLAG_YES);
      db.persist(xferPart,user);

      // get prior part state, it should be the deployment op
      ps = db.getPartState(ps.getPriorId());
    }

    // make sure the part state was the deployment op
    if (!ps.getPartOp().getOpCode().equals(Tmg.OP_PART_DEPLOY))
    {
      log.error("Incorrect op code to cancel deployment encountered: " 
        + ps.getPartOp().getOpCode());
      return ROLLBACK;
    }

    if (ps.getPriorId() <= 0L)
    {
      log.error("Cannot revert part to prior state before " + ps.getStateId()
        + ", it does not exist");
      return ROLLBACK;
    }

    // get deployment record from part state
    Deployment dep = ps.getDeployment();

    // get state prior to deployment and revert the part to it
    log.debug("reverting part to pre-deployment state");

    // this will be usually be the busy state
    ps = db.getPartState(ps.getPriorId());
    if (ps.getStatusCode().equals(ps.SC_BUSY))
    {
      // this will be true former in stock state
      ps = db.getPartState(ps.getPriorId());
    }

    db.changePart(ps.getPart(),op,user);

    // get the new part state
    ps = db.getCurrentPartState(ps.getPartId());

    // end the deployment
    log.debug("ending deployment");
    dep.setEndUserName(user.getLoginName());
    dep.setEndDate(db.getCurDate());
    // HACK: there is no actual order to associate with here...
    dep.setEndOrdId(dep.getStartOrdId()); 
    dep.setEndCode(DeployReason.DRC_CANCEL);
    dep.setStateId(ps.getStateId());
    db.persist(dep,user);

    db.logAction(action,user,"Deployment record " + dep.getDeployId()
      + " canceled",dep.getDeployId());

    return COMMIT;
  }
}
