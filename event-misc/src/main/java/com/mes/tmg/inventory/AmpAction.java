package com.mes.tmg.inventory;

import java.util.List;
import org.apache.log4j.Logger;
import com.mes.mvc.Link;
import com.mes.tmg.MiscAdd;
import com.mes.tmg.MiscAddSource;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class AmpAction extends TmgAction
{
  static Logger log = Logger.getLogger(AmpAction.class);
  
  public static final String SN_AMP_VIEW_STATE  = "ampViewState";
  public static final String SN_AMP_LOOKUP_BEAN = "ampLookupBean";

  public String getActionObject(String actionName)
  {
    if (actionName.matches(".*Source"))
    {
      return "Part Source";
    }
    return "Misc. Addition";
  }

  private AmpViewState getAmpViewState()
  {
    AmpViewState viewState 
      = (AmpViewState)getSessionAttr(SN_AMP_VIEW_STATE,null);
    if (viewState == null)
    {
      viewState = new AmpViewState(this,SN_AMP_VIEW_STATE);
      setSessionAttr(SN_AMP_VIEW_STATE,viewState);
    }
    else
    {
      viewState.setAction(this);
    }
    return viewState;
  }

  private void clearAmpViewState()
  {
    session.removeAttribute(SN_AMP_VIEW_STATE);
  }

  public void doAmpMain()
  {
    AmpViewState viewState = getAmpViewState();
    AmpBean ampBean = new AmpBean(this);
    viewState.loadBeanFromCurPart(ampBean);
    doView(Tmg.VIEW_AMP_MAIN);
  }

  public void doAmpAddPart()
  {
    AmpViewState viewState = getAmpViewState();
    AmpBean bean = new AmpBean(this);
    if (bean.isAutoSubmitOk())
    {
      if (viewState.hasSn(bean.getData(bean.FN_SERIAL_NUM)))
      {
        redirectWithFeedback(Tmg.VIEW_AMP_MAIN,
          "Serial number already among parts to be added");
      }
      else
      {
        viewState.addPart(bean);
        redirectWithFeedback(Tmg.VIEW_AMP_MAIN,"Part added");
      }
    }
    else
    {
      doView(Tmg.VIEW_AMP_MAIN);
    }
  }

  public void doAmpCopyPart()
  {
    AmpViewState viewState = getAmpViewState();
    viewState.setCurPart(getParmInt("partIdx"));
    redirectWithFeedback(Tmg.VIEW_AMP_MAIN,"Part data copied");
  }

  public void doAmpDeletePart()
  {
    getAmpViewState().deletePart(getParmInt("partIdx"));
    redirectWithFeedback(Tmg.VIEW_AMP_MAIN,"Part deleted");
  }

  public void doAmpEditPart()
  {
    AmpViewState viewState = getAmpViewState();
    AmpBean bean = new AmpBean(this);
    if (!bean.getField(bean.FN_CANCEL_EDIT_BTN).isBlank())
    {
      viewState.setEditMode(false);
      viewState.setCurPart(getParmInt("partIdx"));
      redirectWithFeedback(Tmg.VIEW_AMP_MAIN,"Part edit canceled");
    }
    else if (bean.isAutoSubmitOk())
    {
      viewState.setEditMode(false);
      viewState.updatePart(bean);
      redirectWithFeedback(Tmg.VIEW_AMP_MAIN,"Part updated");
    }
    else if (bean.isAutoSubmit())
    {
      doView(Tmg.VIEW_AMP_MAIN);
    }
    else
    {
      viewState.editPart(getParmInt("partIdx"));
      viewState.loadBeanFromCurPart(bean);
      redirectWithFeedback(Tmg.VIEW_AMP_MAIN,"Editing part");
    }
  }

  public void doAmpClear()
  {
    if (isConfirmed())
    {
      clearAmpViewState();
      redirectWithFeedback(Tmg.ACTION_AMP_MAIN,"Part data cleared");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Part data not cleared");
    }
    else
    {
      confirm("Clear all part data?");
    }
  }

  public void doAmpComplete()
  {
    AmpViewState viewState = getAmpViewState();
    if (viewState.getParts().isEmpty())
    {
      redirectWithFeedback(Tmg.VIEW_AMP_MAIN,"Nothing to add, create some"
        +" parts to add first");
    }
    else if (isConfirmed())
    {
      List ampParts = viewState.getParts();
      AmpTransaction t = new AmpTransaction(db,user,this,ampParts);
      if (!t.run())
      {
        throw new RuntimeException("Failed to add parts");
      }
      clearAmpViewState();
      Link viewLink = handler.getLink(Tmg.ACTION_AMP_VIEW);
      viewLink.addArg("maId",""+t.getMaId());
      handler.setBackLink(Tmg.ACTION_AMP_VIEW,
        handler.generateLink("ampMenu",null,"Back to Miscellaneous Additions"));
      redirectWithFeedback(viewLink,"Misellaneous parts added successfully");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Part addition not completed");
    }
    else
    {
      confirm("Add all parts to inventory?");
    }
  }

  private AmpLookupBean getAmpLookupBean()
  {
    AmpLookupBean bean = (AmpLookupBean)getSessionAttr(SN_AMP_LOOKUP_BEAN,null);
    if (bean == null)
    {
      bean = new AmpLookupBean(this);
      setSessionAttr(SN_AMP_LOOKUP_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doAmpLookup()
  {
    AmpLookupBean bean = getAmpLookupBean();
    request.setAttribute("viewBean",bean);
    doView(Tmg.VIEW_AMP_LOOKUP);
  }

  public void doAmpView()
  {
    long maId = getParmLong("maId");
    MiscAdd ma = db.getMiscAdd(maId);
    request.setAttribute("ma",ma);
    request.setAttribute("parts",db.getMiscAddParts(maId));
    setNoteItemId(maId);
    doView(Tmg.VIEW_AMP_VIEW);
  }
  public void doAmpViewPdtl()
  {
    doAmpView();
  }

  public void doAmpShowSources()
  {
    request.setAttribute("showItems",db.getMiscAddSources());
    doView(Tmg.VIEW_AMP_SHOW_SOURCES);
  }
  
  public void doAmpAddSource()
  {
    AmpSourceBean bean = new AmpSourceBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_AMP_EDIT_SOURCE);
    if (bean.isAutoSubmitOk())
    {
      String masName = bean.getMiscAddSource().getMasName();
      long id = bean.getMiscAddSource().getMasId();
      logAndRedirect(Tmg.ACTION_AMP_SHOW_SOURCES,masName,id);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_AMP_EDIT_SOURCE : Tmg.VIEW_AMP_ADD_SOURCE);
    }
  }

  public void doAmpEditSource()
  {
    doAmpAddSource();
  }

  public void doAmpDeleteSource()
  {
    long delId = getParmLong("delId");
    MiscAddSource mas = db.getMiscAddSource(delId);
    if (mas == null)
    {
      throw new RuntimeException("Source with id " + delId 
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deleteMiscAddSource(delId,user);
      logAndRedirect(Tmg.ACTION_AMP_SHOW_SOURCES,mas.getMasName(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_AMP_SHOW_SOURCES,"Source not deleted");
    }
    else
    {
      confirm("Delete source " + mas.getMasName() + "?");
    }
  }
}
