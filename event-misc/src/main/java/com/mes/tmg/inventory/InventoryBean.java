package com.mes.tmg.inventory;

import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.Client;
import com.mes.tmg.Inventory;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tools.DropDownTable;

public class InventoryBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(InventoryBean.class);
  
  public static final String FN_INV_ID      = "invId";
  public static final String FN_INV_NAME    = "invName";
  public static final String FN_CLIENT_ID   = "clientId";
  public static final String FN_MES_FLAG    = "mesFlag";
  
  public static final String FN_SUBMIT_BTN  = "submit";
  
  protected class ClientTable extends DropDownTable
  {
    public ClientTable()
    {
      addElement("","select one");
      List clients = db.getClients();
      for (Iterator i = clients.iterator(); i.hasNext(); )
      {
        Client client = (Client)i.next();
        addElement(String.valueOf(client.getClientId()),client.getClientName());
      }
    }
  }
  
  private Inventory inventory;

  public InventoryBean(TmgAction action)
  {
    super(action);
  }

  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_INV_ID));
      fields.addAlias(FN_INV_ID,"editId");
      fields.add(new Field(FN_INV_NAME,"Name",32,32,false));
      fields.add(new DropDownField(FN_CLIENT_ID,"Client",new ClientTable(),false));
      fields.add(new CheckField(FN_MES_FLAG,"MES Owned","y",true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  public Inventory getInventory()
  {
    return inventory;
  }
  
  /**
   * Persists inv.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long invId = getField(FN_INV_ID).asLong();
      
      if (invId == 0L)
      {
        invId = db.getNewId();
        inventory = new Inventory();
        inventory.setInvId(invId);
      }
      else
      {
        inventory = db.getInventory(invId);
      }
      
      inventory.setInvName(getData(FN_INV_NAME));
      inventory.setClientId(getField(FN_CLIENT_ID).asLong());
      inventory.setMesFlag(getData(FN_MES_FLAG));
    
      db.persist(inventory,user);
      
      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting inventory: " + e);
    }

    return submitOk;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long invId = getField(FN_INV_ID).asLong();
      
      if (invId != 0L)
      {
        inventory = db.getInventory(invId);
        setData(FN_INV_NAME,inventory.getInvName());
        setData(FN_CLIENT_ID,String.valueOf(inventory.getClientId()));
        setData(FN_MES_FLAG,inventory.getMesFlag());
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading inventory: " + e);
    }
    
    return loadOk;
  }
}