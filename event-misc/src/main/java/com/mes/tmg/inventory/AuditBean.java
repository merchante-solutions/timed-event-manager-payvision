package com.mes.tmg.inventory;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.Validation;
import com.mes.tmg.Audit;
import com.mes.tmg.AuditException;
import com.mes.tmg.Client;
import com.mes.tmg.Document;
import com.mes.tmg.PartState;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.DocAction;
import com.mes.tools.DropDownTable;
import com.oreilly.servlet.multipart.FilePart;


/**
 * Used to create a physical inventory audit record.
 */

public class AuditBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(AuditBean.class);

  public static final String FN_AUDIT_ID      = "auditId";
  public static final String FN_AUDIT_DATE    = "auditDate";
  public static final String FN_CLIENT_ID     = "clientId";
  public static final String FN_UPLOAD_FILE   = "uploadFile";
  public static final String FN_SUBMIT_BTN    = "submit";

  private Audit     audit;
  private Document  receivedDoc;

  public AuditBean(TmgAction action)
  {
    super(action);
  }

  protected class ClientTable extends DropDownTable
  {
    public ClientTable()
    {
      addElement("","Global Audit (All Clients)");
      List clients = db.getClients();
      for (Iterator i = clients.iterator(); i.hasNext(); )
      {
        Client client = (Client)i.next();
        addElement(String.valueOf(client.getClientId()),client.getClientName());
      }
    }
  }
  
  public class SnFileValidation implements Validation
  {
    public boolean validate(String data)
    {
      return receivedDoc != null;
    }

    public String getErrorText()
    {
      return "Failed to upload serial number file";
    }
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
     {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_AUDIT_ID));
      fields.addAlias(FN_AUDIT_ID,"editId");
      fields.add(new DateStringField(FN_AUDIT_DATE,"Audit Date",false,false));
      fields.add(new DropDownField(FN_CLIENT_ID,"Client",new ClientTable(),true));
      fields.add(new Field(FN_UPLOAD_FILE,"Upload File",32,32,true));
      fields.getField(FN_UPLOAD_FILE).addValidation(new SnFileValidation());
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public Audit getAudit()
  {
    return audit;
  }

  /**
   * Loading is a moot point, audits can only be created and deleted, not edited.
   */
  protected boolean autoLoad()
  {
    return true;
  }

  /**
   * Upload serial number file.
   */
  protected void receiveFile(FilePart part)
  {
    // create new document in db, upload the file data to it
    receivedDoc = DocAction.uploadNewDocFile(db,user,action.getName(),
      part.getFileName(),"Audit serial number file",-1L,part.getInputStream());
  }
  
  /**
   * Get rid of old audit records to get ready for a new audit.
   */
  private void purgeAuditExceptions()
  {
    try
    {
      db.purgeAuditExceptions(audit.getAuditId(),user);
    }
    catch (Exception e)
    {
      String errorMsg = "Purge of audit recs for audit " + audit.getAuditId()
        + " failed: " + e;
      log.error(errorMsg);
      throw new RuntimeException(errorMsg);
    }
  }

  /**
   * Scans the audit's associated serial number scan file.  This results in a
   * mapping of unique serial numbers to number of times each occured in the
   * scan file.
   */
  private Map getScannedSerialNumCounts()
  {
    try
    {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      DocAction.outputDoc(db,audit.getDocument().getDocId(),baos);
      Map snCounts = new HashMap();
      BufferedReader br 
        = new BufferedReader(new StringReader(baos.toString()));
      String serialNum = null;
      while ((serialNum = br.readLine()) != null)
      {
        if (serialNum.trim().length() > 0 && !serialNum.startsWith("#"))
        {
          Integer count = (Integer)snCounts.get(serialNum);
          if (count == null)
          {
            count = 1;
          }
          else
          {
            count = count.intValue() + 1;
          }
          snCounts.put(serialNum,count);
        }
      }
      return snCounts;
    }
    catch (Exception e)
    {
      String errorMsg = "Failed to parse s/n doc " + audit.getDocument()
        + " for audit " + audit.getAuditId() + ": " + e;
      log.error(errorMsg);
      throw new RuntimeException(errorMsg);
    }
  }

  /**
   * Helper class to store db data on parts.  Contains lists of part states,
   * though usually there should only be one part state per serial num.
   */
  public class DbSnData
  {
    public List states = new ArrayList();
    public String serialNum;

    public DbSnData(PartState ps)
    {
      states.add(ps);
      serialNum = ps.getSerialNum();
    }

    public int getCount()
    {
      return states.size();
    }

    public void addPartState(PartState ps)
    {
      states.add(ps);
    }

    public PartState getPartState()
    {
      return getPartState(1);
    }

    public PartState getPartState(int psNum)
    {
      return (PartState)states.get(psNum - 1);
    }

    public List getPartStates()
    {
      return states;
    }

    public String getSerialNum()
    {
      return serialNum;
    }
  }

  /**
   * Load all part states for audit client with status IN.  Load the
   * part states into DbSnData objs and map serial numbers to them.
   * Returns Map containing the part states.
   */
  private Map getDbMap()
  {
    Map dbMap = new HashMap();
    List dbInParts = db.getAuditPartStatesInStock(audit);
    for (Iterator i = dbInParts.iterator(); i.hasNext();)
    {
      PartState ps = (PartState)i.next();

      // TODO: fix persist package to allow joins to have
      // more flexible conditions... (i.e. for imprinters,
      // need ability to do join subsel that filters out
      // parts with featId <> imprinter cat feat id)

      // filter out imprinters here
      if (ps.getPartType().getCategoryName().equals("Imprinter"))
      {
        continue;
      }

      String serialNum = ps.getSerialNum();
      DbSnData dsd = (DbSnData)dbMap.get(serialNum);
      if (dsd == null)
      {
        dbMap.put(serialNum,new DbSnData(ps));
      }
      else
      {
        dsd.addPartState(ps);
      }
    }
    return dbMap;
  }

  /**
   * Create a new audit exception object.  Does not set the audit rec id,
   * that will need to be done prior to persisting.
   */
  private AuditException createAuditException(String serialNum,
    String resultCode, PartState ps)
  {
    AuditException auditEx = new AuditException();
    auditEx.setAuditId(audit.getAuditId());
    auditEx.setSerialNum(serialNum);
    auditEx.setResultCode(resultCode);
    if (ps != null)
    {
      auditEx.setPartId(ps.getPartId());
      auditEx.setAuditStateId(ps.getStateId());
    }
    return auditEx;
  }

  /**
   * Takes the set of error serial nums and finds all part states in db
   * that match the serial nums but do not match the client.  If the 
   * audit is a global audit it will return an empty map.  Serial nums
   * map to DbSnData objects.
   */
  public Map getBadOwnerMap(Set errorSn)
  {
    Map badOwnerMap = new HashMap();
    if (!audit.isGlobal())
    {
      List badOwnerList = db.getAuditPartStatesWithBadOwner(audit,errorSn);
      for (Iterator i = badOwnerList.iterator(); i.hasNext();)
      {
        PartState ps = (PartState)i.next();
        String serialNum = ps.getSerialNum();
        DbSnData dsd = (DbSnData)badOwnerMap.get(serialNum);
        if (dsd == null)
        {
          badOwnerMap.put(serialNum,new DbSnData(ps));
        }
        else
        {
          dsd.addPartState(ps);
        }
      }
    }
    return badOwnerMap;
  }

  /**
   * Takes the set of error serial nums and finds all part states in db
   * that match the serial nums.  Serial nums map to DbnData objects.
   */
  public Map getBadStatusMap(Set errorSn)
  {
    Map badStatusMap = new HashMap();
    List badStatusList = db.getAuditPartStatesForSerialNums(audit,errorSn);
    for (Iterator i = badStatusList.iterator(); i.hasNext();)
    {
      PartState ps = (PartState)i.next();
      String serialNum = ps.getSerialNum();
      DbSnData dsd = (DbSnData)badStatusMap.get(serialNum);
      if (dsd == null)
      {
        badStatusMap.put(serialNum,new DbSnData(ps));
      }
      else
      {
        dsd.addPartState(ps);
      }
    }
    return badStatusMap;
  }

  /**
   * Compare scanned serial num data to the database state.  Generate
   * exceptions for any discrepencies found.
   */
  private void findAuditExceptions(Map scanMap, Map dbMap)
  {
    List auditExes = new ArrayList();

    // all serial nums in db data but 
    // not in scan data are probably missing
    // missing = db - scan
    Set missingSn = new HashSet(dbMap.keySet());
    missingSn.removeAll(scanMap.keySet());

    // all serial nums in scan data but not in
    // db IN data are probably db status errors
    // error = scan - db
    Set errorSn = new HashSet(scanMap.keySet());
    errorSn.removeAll(dbMap.keySet());

    // separate out inventory owner errors
    Map badOwnerMap = getBadOwnerMap(errorSn);

    // error = error - bad owner
    errorSn.removeAll(badOwnerMap.keySet());

    // separate out known serial nums
    Map badStatusMap = getBadStatusMap(errorSn);

    // unknown = error - bad status
    Set unknownSn = new HashSet(errorSn);
    unknownSn.removeAll(badStatusMap.keySet());

    // generate missing exceptions from missing sn
    for (Iterator i = missingSn.iterator(); i.hasNext();)
    {
      
      String serialNum = ""+i.next();
      DbSnData dsd = (DbSnData)dbMap.get(serialNum);
      PartState ps = dsd.getPartState();
      AuditException auditEx 
        = createAuditException(serialNum,AuditException.RESULT_MISSING,ps);
      auditExes.add(auditEx);
    }

    // generate unknown exceptions
    for (Iterator i = unknownSn.iterator(); i.hasNext();)
    {
      String serialNum = ""+i.next();
      AuditException auditEx 
        = createAuditException(serialNum,AuditException.RESULT_UNKNOWN,null);
      auditExes.add(auditEx);
    }

    // generate inventory owner exceptions
    for (Iterator i = badOwnerMap.keySet().iterator(); i.hasNext();)
    {
      String serialNum = ""+i.next();
      DbSnData dsd = (DbSnData)badOwnerMap.get(serialNum);
      PartState ps = dsd.getPartState();
      AuditException auditEx 
        = createAuditException(serialNum,AuditException.RESULT_OWNER,ps);
      auditExes.add(auditEx);
    }

    // generate bad status exceptions
    for (Iterator i = badStatusMap.keySet().iterator(); i.hasNext();)
    {
      String serialNum = ""+i.next();
      DbSnData dsd = (DbSnData)badStatusMap.get(serialNum);
      PartState ps = dsd.getPartState();
      AuditException auditEx 
        = createAuditException(serialNum,AuditException.RESULT_ERROR,ps);
      auditExes.add(auditEx);
    }

    // persist the exceptions
    for (Iterator i = auditExes.iterator(); i.hasNext();)
    {
      AuditException auditEx = (AuditException)i.next();
      auditEx.setAuditExId(db.getNewId());
      log.debug("audit exception generated: " + auditEx);
      db.persist(auditEx,user);
    }
  }

  /**
   * Do an audit.
   */
  private void doAudit()
  {
    // TODO: Q. why do this?
    //log.debug("Purging audit records...");
    //purgeAuditExceptions();

    log.debug("Scanning serial number file...");
    Map scanMap = getScannedSerialNumCounts();

    log.debug("Loading IN stock part states...");
    Map dbMap = getDbMap();

    log.debug("Auditing inventory...");
    findAuditExceptions(scanMap,dbMap);
  }

  /**
   * Persists note.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long auditId = getField(FN_AUDIT_ID).asLong();
      
      if (auditId == 0L)
      {
        auditId = db.getNewId();
        audit = new Audit();
        audit.setAuditId(auditId);
        setData(FN_AUDIT_ID,String.valueOf(auditId));
        audit.setUserName(user.getLoginName());
        audit.setCreateDate(Calendar.getInstance().getTime());
      }
      else
      {
        audit = db.getAudit(auditId);
      }

      audit
        .setAuditDate(((DateStringField)getField(FN_AUDIT_DATE))
          .getUtilDate());
      audit.setClientId(getField(FN_CLIENT_ID).asLong());
      db.persist(audit,user);

      // attach received doc to this audit
      if (receivedDoc != null)
      {
        DocAction.attachToDoc(db,user,action.getName(),receivedDoc,auditId);
      }

      // reload note to get attached documents
      audit = db.getAudit(auditId);

      // run comparison check db status vs. physical inventory sn file
      doAudit();

      submitOk = true;
    }
    catch (Exception e)   
    {
      e.printStackTrace();
      log.error("Error submitting audit: " + e);
    }

    return submitOk;
  }
}