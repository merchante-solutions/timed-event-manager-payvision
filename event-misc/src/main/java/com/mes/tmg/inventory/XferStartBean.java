package com.mes.tmg.inventory;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.Validation;
import com.mes.tmg.Inventory;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.InventoryTable;

public class XferStartBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(XferStartBean.class);

  public static final String  FN_FROM_INV_ID      = "fromInvId";
  public static final String  FN_TO_INV_ID        = "toInvId";

  public static final String  FN_XFER_START_BTN   = "startXferBtn";


  public XferStartBean(TmgAction action)
  {
    super(action,"xferStartBean");
  }


  public class ToValidation implements Validation
  {
    public String getErrorText()
    {
      return "Different inventories required to transfer";
    }

    public boolean validate(String data)
    {
      Field fromField = fields.getField(FN_FROM_INV_ID);
      if (data != null && data.length() > 0 && !fromField.isBlank())
      {
        return !data.equals(fromField.getData());
      }
      return true;
    }
  }

  public class InterClientValidation implements Validation
  {
    public String getErrorText()
    {
      return "Cannot transfer between client inventories";
    }

    public boolean validate(String data)
    {
      Field fromField = fields.getField(FN_FROM_INV_ID);
      if (data != null && data.length() > 0 && !fromField.isBlank())
      {
        Inventory fromInv = db.getInventory(fromField.asLong());

        Inventory toInv = null;
        try
        {
           toInv = db.getInventory(Long.parseLong(data));
        }
        catch (Exception e) {}

        if ((toInv != null && !toInv.isMesOwned()) && !fromInv.isMesOwned())
        {
          return false;
        }
      }
      return true;
    }
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new DropDownField(FN_FROM_INV_ID,new InventoryTable(db),false));
      fields.add(new DropDownField(FN_TO_INV_ID,new InventoryTable(db),false));
      fields.getField(FN_TO_INV_ID).addValidation(new ToValidation());
      fields.getField(FN_TO_INV_ID).addValidation(new InterClientValidation());
      fields.add(new ButtonField(FN_XFER_START_BTN,"start"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }
}
