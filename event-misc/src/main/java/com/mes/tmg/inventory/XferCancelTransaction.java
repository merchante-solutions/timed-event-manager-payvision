package com.mes.tmg.inventory;

import java.util.List;
import org.apache.log4j.Logger;
import com.mes.tmg.Part;
import com.mes.tmg.PartHistory;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PartState;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.tmg.Transfer;
import com.mes.tmg.TransferPart;
import com.mes.user.UserBean;

public class XferCancelTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(XferCancelTransaction.class);

  private TmgAction tmgAction;
  private long xferPartId;

  public XferCancelTransaction(TmgDb db, UserBean user, TmgAction tmgAction, 
    long xferPartId)
  {
    super(db,user,null,db.getOpCode(Tmg.OP_PART_XFER_CANCEL));
    this.tmgAction = tmgAction;
    this.xferPartId = xferPartId;
  }

  public int executeOperation(PartOperation op)
  {
    TransferPart xferPart = db.getTransferPart(xferPartId);
    Transfer xfer = db.getTransfer(xferPart.getXferId());
    PartHistory history = db.getPartHistoryForPartId(xferPart.getPartId());
    List states = history.getStates();
    PartState curState = (PartState)states.get(states.size() - 1);
    if (curState.getOpId() != xfer.getOpId())
    {
      throw new RuntimeException("Cannot cancel part transfer,"
        + " part state has changed");
    }
    PartState lastState = (PartState)states.get(states.size() - 2);
    Part revertedPart = lastState.getPart();
    db.changePart(revertedPart,op,user);
    xferPart.setCancelFlag(xferPart.FLAG_YES);
    db.persist(xferPart,user);
    db.logAction(tmgAction,user,"Part transfer canceled",xferPartId);
    db.logAction(tmgAction,user,"Part transfer canceled",xferPart.getPartId());
    return COMMIT;
  }
}
