package com.mes.tmg.inventory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.FieldBean;
import com.mes.mvc.tags.Viewable;
import com.mes.tmg.Client;
import com.mes.tmg.TmgDb;
import com.mes.tools.DropDownTable;

public class AuditLookupBean extends FieldBean implements Viewable
{
  static Logger log = Logger.getLogger(AuditLookupBean.class);
  
  public static final String FN_FROM_DATE     = "fromDate";
  public static final String FN_TO_DATE       = "toDate";
  public static final String FN_CLIENT_ID     = "clientId";
  
  public static final String FN_SUBMIT_BTN    = "submit";

  private TmgDb db;
  private List  audits;

  protected class ClientTable extends DropDownTable
  {
    public ClientTable()
    {
      addElement("-1","Any Audit");
      addElement("0","Complete Audits Only");
      List clients = db.getClients();
      for (Iterator i = clients.iterator(); i.hasNext(); )
      {
        Client client = (Client)i.next();
        addElement(String.valueOf(client.getClientId()),client.getClientName());
      }
    }
  }
  
  /**
   * Drop down tables
   */
  public AuditLookupBean(TmgDb db)
  {
    this.db = db;
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new DateStringField(FN_FROM_DATE,"From Date",false,false));
      fields.add(new DateStringField(FN_TO_DATE,"To Date",true,false));
      fields.add(new DropDownField(FN_CLIENT_ID,"Client",new ClientTable(),true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      //fields.setShowErrorText(true);
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public List getAudits()
  {
    // create a new empty list if no list present or an autosubmit failed
    if (audits == null || (isAutoSubmit() && !isAutoValid()))
    {
      audits = new ArrayList();
    }
    return audits;
  }

  public List getRows()
  {
    return getAudits();
  }
  
  /**
   * Do lookup
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      Date fromDate = ((DateStringField)getField(FN_FROM_DATE)).getUtilDate();
      Date toDate = ((DateStringField)getField(FN_TO_DATE)).getUtilDate();
      String auditType = getData(FN_CLIENT_ID);
      long clientId = getField(FN_CLIENT_ID).asLong();
      audits = db.lookupAudits(fromDate,toDate,clientId);
      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error auto submitting: " + e);
      e.printStackTrace();
    }

    return submitOk;
  }

  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      Calendar cal = Calendar.getInstance();
      DateStringField fromField = (DateStringField)fields.getField(FN_FROM_DATE);
      if (fromField.isBlank())
      {
        DateStringField toField = (DateStringField)fields.getField(FN_TO_DATE);
        toField.setUtilDate(cal.getTime());
        cal.add(cal.MONTH,-1);
        fromField.setUtilDate(cal.getTime());
      }
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
    }
    
    return loadOk;
  }

  public boolean isReversed()
  {
    return false;
  }

  public int getLeadCount()
  {
    return 0;
  }

  public int getTrailCount()
  {
    return 0;
  }

  public boolean isExpanded()
  {
    return true;
  }

  public boolean exceedsLimits()
  {
    return false;
  }
}