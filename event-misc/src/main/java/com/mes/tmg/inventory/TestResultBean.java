package com.mes.tmg.inventory;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.HiddenField;
import com.mes.tmg.Part;
import com.mes.tmg.PartTest;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.TestResultTypeTable;

public class TestResultBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(TestResultBean.class);
  
  public static final String FN_PART_ID         = "partId";
  public static final String FN_TRT_CODE        = "trtCode";
  
  public static final String FN_SUBMIT_BTN      = "submit";
  
  private Part part;

  public TestResultBean(TmgAction action)
  {
    super(action);
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_PART_ID));
      fields.add(new DropDownField(FN_TRT_CODE,new TestResultTypeTable(db),false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  public Part getPart()
  {
    if (part == null)
    {
      part = db.getPart(getField(FN_PART_ID).asLong());
    }
    return part;
  }

  protected boolean autoSubmit()
  {
    try
    {
      // look for an existing part test
      getPart();
      ((PartAction)action).setPartTestResults(part,getData(FN_TRT_CODE));
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      long partId = getField(FN_PART_ID).asLong();
      if (partId != 0L)
      {
        PartTest test = db.getCurrentPartTest(partId);
        if (test != null && test.getTrtCode() != null)
        {
          setData(FN_TRT_CODE,test.getTrtCode());
        }
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
    }
    return false;
  }
}