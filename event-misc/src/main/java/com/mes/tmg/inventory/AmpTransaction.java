package com.mes.tmg.inventory;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.tmg.Bucket;
import com.mes.tmg.EncryptionType;
import com.mes.tmg.MiscAdd;
import com.mes.tmg.MiscAddPart;
import com.mes.tmg.NoteAction;
import com.mes.tmg.Part;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PartPrice;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public class AmpTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(AmpTransaction.class);

  private List ampParts;
  private EncryptionType dfltEncType;
  private TmgAction tmgAction;
  private long maId;

  public AmpTransaction(TmgDb db, UserBean user, TmgAction tmgAction, List ampParts)
  {
    super(db,user,null,db.getOpCode(Tmg.OP_PART_MISC_ADD));
    this.tmgAction = tmgAction;
    this.ampParts = ampParts;
    dfltEncType = db.getNoneEncryptType();
  }

  private MiscAdd createMiscAdd()
  {
    MiscAdd miscAdd = new MiscAdd();
    miscAdd.setMaId(db.getNewId());
    miscAdd.setCreateDate(Calendar.getInstance().getTime());
    miscAdd.setUserName(user.getLoginName());
    db.persist(miscAdd,user);
    maId = miscAdd.getMaId();
    return miscAdd;
  }

  private Bucket getBucketForPart(AmpPart ampPart)
  {
    Bucket bucket = db.getBucketForInvPt(ampPart.getInventory().getInvId(),
      ampPart.getPartType().getPtId());
    if (bucket == null)
    {
      bucket = new Bucket();
      bucket.setBuckId(db.getNewId());
      bucket.setInvId(ampPart.getInventory().getInvId());
      bucket.setPtId(ampPart.getPartType().getPtId());
      db.persist(bucket,user);
    }
    return bucket;
  }

  private MiscAddPart createMiscAddPart(MiscAdd miscAdd, AmpPart ampPart)
  {
    // check for bucket
    MiscAddPart maPart = new MiscAddPart();
    maPart.setMaPartId(db.getNewId());
    maPart.setMaId(miscAdd.getMaId());
    maPart.setUserName(miscAdd.getUserName());
    maPart.setCreateDate(miscAdd.getCreateDate());
    maPart.setInvId(ampPart.getInventory().getInvId());
    maPart.setBuckId(getBucketForPart(ampPart).getBuckId());
    maPart.setPtId(ampPart.getPartType().getPtId());
    maPart.setPcCode(ampPart.getPcCode());
    maPart.setSourceCode(ampPart.getSourceCode());
    maPart.setPartPrice(ampPart.getPartPrice());
    maPart.setPartCount(ampPart.getQuantity());
    maPart.setEtCode(dfltEncType.getEtCode());
    maPart.setSerialNum(ampPart.getSerialNum());
    maPart.setMoFlag(ampPart.isMerchantOwned() ? "y" : "n");
    maPart.setOwnerMerchNum(ampPart.getOwnerMerchNum());
    db.persist(maPart,user);
    return maPart;
  }

  private Part createPart(MiscAddPart maPart)
  {
    Part part = new Part();
    part.setPartId(db.getNewId());
    part.setBuckId(maPart.getBuckId());
    part.setPtId(maPart.getPtId());
    part.setSerialNum(maPart.getSerialNum());
    part.setCount(maPart.getPartCount());
    part.setPcCode(maPart.getPcCode());
    part.setOrigPcCode(maPart.getPcCode());
    part.setEtId(dfltEncType.getEtId());
    part.setMaId(maPart.getMaId());
    part.setMaPartId(maPart.getMaPartId());
    part.setMaReceiveTs(new Timestamp(
      Calendar.getInstance().getTimeInMillis()));
    part.setOwnerMerchNum(maPart.getOwnerMerchNum());
    // TODO: add these as variables to ampPart ?
    part.setCondCode(part.CC_GOOD);
    part.setStatusCode(part.SC_IN);
    // if adding merchant owned equipment, turn on the MOE disposition
    // this needs to stick with the part for life, it is flagged as
    // merchant owned AND merchant originated...
    part.setDispCode(maPart.isMerchantOwned() ? part.DC_MOE : part.DC_NOT_DEP);
    // automatically add into testing now
    part.setLocCode(part.LC_TESTING);
    part.setTestId(db.getNewId());
    return part;
  }

  private void createNote(String noteText, long partId)
  {
    if (noteText != null && noteText.length() > 0)
    {
      NoteAction
        .insertNote(db,user,tmgAction.getName(),partId,noteText);
    }
  }

  private void logPartPrice(MiscAddPart maPart, Part part)
  {
    PartPrice pp = new PartPrice();
    pp.setPpId(db.getNewId());
    pp.setPartId(part.getPartId());
    pp.setPartPrice(maPart.getPartPrice());
    db.persist(pp,user);
  }

  public int executeOperation(PartOperation op)
  {
    MiscAdd miscAdd = createMiscAdd();
    for (Iterator i = ampParts.iterator(); i.hasNext();)
    {
      AmpPart ampPart = (AmpPart)i.next();
      MiscAddPart maPart = createMiscAddPart(miscAdd,ampPart);
      part = createPart(maPart);
      logPartPrice(maPart,part);
      db.addPart(part,op,user);
      createPartTest(part,tmgAction,-1L,maPart.getMaPartId());
      createNote(ampPart.getNoteText(),part.getPartId());
      String description = "Part " + part.getPartId() + " added to inventory"
        + " for miscellaneous part addition " + maPart.getMaPartId();
      db.logAction(tmgAction,user,description,part.getPartId());
      db.logAction(tmgAction,user,"Part " + part.getPartId() 
        + " received into testing",part.getPartId());
      db.logAction(tmgAction,user,description,maPart.getMaPartId());
    }
    return COMMIT;
  }

  public long getMaId()
  {
    return maId;
  }
}
