package com.mes.tmg.inventory;

import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.tmg.Inventory;
import com.mes.tmg.MiscAddSource;
import com.mes.tmg.PartType;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.InventoryTable;
import com.mes.tmg.util.PartClassTable;
import com.mes.tmg.util.PartTypeTable;
import com.mes.tools.DropDownTable;

public class AmpBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(AmpBean.class);

  public static final String  FN_PART_IDX         = "partIdx";
  public static final String  FN_INV_ID           = "invId";
  public static final String  FN_PT_ID            = "ptId";
  public static final String  FN_SOURCE_CODE      = "sourceCode";
  public static final String  FN_PC_CODE          = "pcCode";
  public static final String  FN_PART_PRICE       = "partPrice";
  public static final String  FN_SERIAL_NUM       = "serialNum";
  public static final String  FN_QUANTITY         = "quantity";
  public static final String  FN_NOTE_TEXT        = "noteText";

  public static final String  FN_ADD_PART_BTN     = "addPartBtn";
  public static final String  FN_SAVE_PART_BTN    = "savePartBtn";
  public static final String  FN_CANCEL_EDIT_BTN  = "cancelEditBtn";

  private Inventory inventory;
  private PartType partType;

  public AmpBean(TmgAction action)
  {
    super(action,"ampBean");
  }

  // TODO: create amp source database table
  protected class SourceTable extends DropDownTable
  {
    public SourceTable()
    {
      addElement("","select one");
      List sources = db.getMiscAddSources();
      for (Iterator i = sources.iterator(); i.hasNext();)
      {
        MiscAddSource mas = (MiscAddSource)i.next();
        addElement(mas.getMasCode(),mas.getMasName());
      }
    }
  }

  public class SnValidation implements Validation
  {
    String errorText;

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String data)
    {
      PartType pt = getPartType();
      if (pt != null)
      {
        boolean snPresent = data != null && data.length() > 0;
        if (!pt.hasSn() && snPresent)
        {
          errorText = "Serial numbers not allowed with this part type";
          return false;
        }
        if (pt.hasSn() && !snPresent)
        {
          errorText = "Serial number required for this part type";
          return false;
        }
        if (db.getPartWithSerialNum(data) != null)
        {
          errorText = "Duplicate serial number";
          return false;
        }                    }
      return true;
    }
  }
  
  public class QtyValidation implements Validation
  {
    public String getErrorText()
    {
      return "Part quantity required for this part type";
    }
    public boolean validate(String data)
    {
      PartType pt = getPartType();
      if (pt != null && !pt.hasSn() && (data == null || data.length() == 0))
      {
        return false;
      }
      return true;
    }
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_PART_IDX));
      fields.add(new DropDownField(FN_INV_ID,new InventoryTable(db),false));
      fields.add(new DropDownField(FN_PT_ID,new PartTypeTable(db,PartTypeTable.OPT_SEL_ONE),false));
      fields.add(new DropDownField(FN_SOURCE_CODE,new SourceTable(),false));
      fields.add(new DropDownField(FN_PC_CODE,new PartClassTable(),false));
      fields.add(new CurrencyField(FN_PART_PRICE,"Part Price",10,10,false,0.01f,10000f));
      fields.add(new Field(FN_SERIAL_NUM,"Serial No.",64,20,true));
      fields.add(new NumberField(FN_QUANTITY,"Quantity",10,5,true,0));
      fields.add(new TextareaField(FN_NOTE_TEXT,"Note",2000,2,70,false));

      fields.getField(FN_SERIAL_NUM).addValidation(new SnValidation());
      fields.getField(FN_QUANTITY).addValidation(new QtyValidation());

      fields.add(new ButtonField(FN_ADD_PART_BTN,"add"));
      fields.add(new ButtonField(FN_SAVE_PART_BTN,"update"));
      fields.add(new ButtonField(FN_CANCEL_EDIT_BTN,"cancel"));

      fields.setFixImage("/images/arrow1_left.png",10,10);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public Inventory getInventory()
  {
    Field invField = getField(FN_INV_ID);
    if (!invField.isBlank())
    {
      long invId = invField.asLong();
      if (inventory == null || inventory.getInvId() != invId)
      {
        inventory = db.getInventory(invId);
      }
      return inventory;
    }
    return null;
  }

  public PartType getPartType()
  {
    Field ptField = getField(FN_PT_ID);
    if (!ptField.isBlank())
    {
      long ptId = ptField.asLong();
      if (partType == null || partType.getPtId() != ptId)
      {
        partType = db.getPartType(ptId);
      }
      return partType;
    }
    return null;
  }

  protected boolean showFeedback()
  {
    return false;
  }
}
