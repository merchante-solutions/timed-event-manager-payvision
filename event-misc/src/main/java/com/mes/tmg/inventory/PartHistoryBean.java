package com.mes.tmg.inventory;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.HiddenField;
import com.mes.tmg.PartHistory;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class PartHistoryBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PartHistoryBean.class);

  public static final String FN_EXPAND_PART_HISTORY = "expandPartHistory";
  public static final String FN_PART_ID             = "partId";
  public static final String FN_STATE_ID            = "stateId";

  private PartHistory history;

  public PartHistoryBean(TmgAction action)
  {
    super(action);
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_EXPAND_PART_HISTORY));
      fields.add(new HiddenField(FN_PART_ID));
      fields.add(new HiddenField(FN_STATE_ID));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public boolean autoLoad()
  {
    try
    {
      long partId = getField(FN_PART_ID).asLong();
      if (partId == 0L)
      {
        long stateId = getField(FN_STATE_ID).asLong();
        history = db.getPartHistoryForStateId(stateId);
        setData(FN_PART_ID,String.valueOf(history.getPartId()));
      }
      else
      {
        history = db.getPartHistoryForPartId(partId);
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
    }
    return false;
  }

  public PartHistory getHistory()
  {
    return history;
  }

  public List getRows()
  {
    return history.getStates();
  }

  public boolean isExpanded()
  {
    return true;
    //return getData(FN_EXPAND_PART_HISTORY).equals("true");
  }

  public boolean isReversed()
  {
    return true;
  }
}