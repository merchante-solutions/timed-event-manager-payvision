package com.mes.tmg.inventory;

import org.apache.log4j.Logger;
import com.mes.tmg.Inventory;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class InventoryAction extends TmgAction
{
  static Logger log = Logger.getLogger(InventoryAction.class);
  
  public String getActionObject(String actionName)
  {
    return "Inventory";
  }

  private void doShowInventories()
  {
    request.setAttribute("showItems",tmgSession.getDb().getInventories());
    doView(Tmg.VIEW_SHOW_INVENTORIES);
  }
  
  private void doAddEditInventory()
  {
    InventoryBean bean = new InventoryBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_INVENTORY);
    if (bean.isAutoSubmitOk())
    {
      String invName = bean.getInventory().getInvName();
      long id = bean.getInventory().getInvId();
      logAndRedirect(Tmg.ACTION_SHOW_INVENTORIES,invName,id);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_INVENTORY : Tmg.VIEW_ADD_INVENTORY);
    }
  }

  private void doDeleteInventory()
  {
    long delId = getParmLong("delId");
    Inventory inventory = db.getInventory(delId);
    if (inventory == null)
    {
      throw new RuntimeException("Inventory with id " + delId
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deleteInventory(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_INVENTORIES,inventory.getInvName(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_INVENTORIES,"Inventory not deleted");
    }
    else
    {
      confirm("Delete inventory " + inventory.getInvName() + "?");
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_SHOW_INVENTORIES))
    {
      doShowInventories();
    }
    else if (name.equals(Tmg.ACTION_EDIT_INVENTORY) ||
             name.equals(Tmg.ACTION_ADD_INVENTORY))
    {
      doAddEditInventory();
    }
    else if (name.equals(Tmg.ACTION_DELETE_INVENTORY))
    {
      doDeleteInventory();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
