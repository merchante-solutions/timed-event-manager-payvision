package com.mes.tmg.inventory;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CurrencyField;
import com.mes.forms.HiddenField;
import com.mes.tmg.Part;
import com.mes.tmg.PartPrice;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class PartPriceBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PartPriceBean.class);
  
  public static final String FN_PART_ID       = "partId";
  public static final String FN_PART_PRICE    = "partPrice";
  
  public static final String FN_SUBMIT_BTN    = "submit";
  
  private Part part;

  public PartPriceBean(TmgAction action)
  {
    super(action);
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_PART_ID));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
      fields.add(new CurrencyField(FN_PART_PRICE,"Part Price",10,10,false));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }
  
  public Part getPart()
  {
    if (part == null)
    {
      part = db.getPart(getField(FN_PART_ID).asLong());
    }
    return part;
  }

  protected boolean autoSubmit()
  {
    try
    {
      long partId = getField(FN_PART_ID).asLong();
      part = db.getPart(partId);
      double newPartPrice = getField(FN_PART_PRICE).asDouble();
      PartPrice pp = part.getPartPrice();
      if (pp == null)
      {
        pp = new PartPrice();
        pp.setPartId(partId);
      }
      if (pp.getPartPrice() != newPartPrice)
      {
        pp.setPartPrice(newPartPrice);
        db.persist(pp,user);
        // reload part to update part price
        part = db.getPart(partId);
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      long partId = getField(FN_PART_ID).asLong();
      if (partId != 0L)
      {
        part = db.getPart(partId);
        PartPrice pp = part.getPartPrice();
        if (pp != null)
        {
          setData(FN_PART_PRICE,String.valueOf(pp.getPartPrice()));
        }
        else
        {
          pp = new PartPrice();
          pp.setPartId(partId);
          part.setPartPrice(pp);
        }
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
    }
    return false;
  }
}