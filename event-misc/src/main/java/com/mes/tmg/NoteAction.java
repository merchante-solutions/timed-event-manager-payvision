package com.mes.tmg;

import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.mvc.Link;
import com.mes.user.UserBean;

public class NoteAction extends TmgAction
{
  static Logger log = Logger.getLogger(NoteAction.class);

  public String getActionObject(String actionName)
  {
    return "Note";
  }

  /**
   * Create a new note.
   */
  public static Note newNote(TmgDb db, UserBean user, long itemId)
  {
    Note note = new Note();
    note.setNoteId(db.getNewId());
    note.setCreateDate(Calendar.getInstance().getTime());
    note.setUserName(user.getLoginName());
    if (itemId != -1L) note.setId(itemId);
    return note;
  }

  /**
   * Create a new note and insert it into the database.
   */
  public static Note insertNote(TmgDb     db, 
                                UserBean  user, 
                                String    actionName, 
                                long      itemId, 
                                String    noteText)
  {
    Note note = newNote(db,user,itemId);
    note.setNoteText(noteText);
    db.persist(note,user);
    db.logAction(actionName,user,"Note " + note.getNoteId() + " inserted",
      note.getNoteId());
    return note;
  }

  private void doAddEditNote()
  {
    NoteBean bean = new NoteBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_NOTE);

    if (bean.isAutoSubmitOk())
    {
      long noteId = bean.getNote().getNoteId();

      // if save and return pressed, return to back link
      if (!bean.getField(bean.FN_SAVE_AND_RETURN_BTN).isBlank())
      {
        logAndRedirect(getBackLink(),noteId);
      }
      // else remain on edit screen
      else
      {
        // redirect to edit link for possible attachments
        // HACK: need to preserve back link manually since we need to manually
        // generate a link to make add redirect to edit
        Link backLink = getBackLink();
        Link editLink = handler.getLink(Tmg.ACTION_EDIT_NOTE,Tmg.VIEW_EDIT_NOTE,
          "noteId=" + noteId,null);
        setBackLink(Tmg.ACTION_EDIT_NOTE,backLink);
        logAndRedirect(editLink,noteId);
      }
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_NOTE : Tmg.VIEW_ADD_NOTE);
    }
  }

  private void doDeleteNoteDoc()
  {
    long noteId = getParmLong("noteId");
    long docId = getParmLong("docId");

    Link backLink = handler.generateLink(Tmg.ACTION_EDIT_NOTE,Tmg.VIEW_EDIT_NOTE,
      "noteId=" + noteId,null);

    if (isConfirmed())
    {
      Document doc = db.getDocument(docId);
      db.deleteDocument(docId,user);
      logAndRedirect(backLink,"document '" + doc.getDocName() + "'",docId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(backLink,"Document not deleted");
    }
    else
    {
      Document doc = db.getDocument(docId);
      if (doc == null)
      {
        redirectWithFeedback(backLink,
          "Document with id " + docId + " does not exist");
      }
      else
      {
        confirm("Delete document '" + doc.getDocName() + "'?");
      }
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_ADD_NOTE) ||
        name.equals(Tmg.ACTION_EDIT_NOTE))
    {
      doAddEditNote();
    }
    else if (name.equals(Tmg.ACTION_DELETE_NOTE_DOC))
    {
      doDeleteNoteDoc();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
