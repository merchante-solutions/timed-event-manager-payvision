package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class Document extends TmgBean
{
  private long    docId;
  private long    id;
  private Date    createDate;
  private String  userName;
  private String  description;
  private String  docName;
  private long    docSize;

  public Document()
  {
  }

  public void setDocId(long docId)
  {
    this.docId = docId;
  }
  public long getDocId()
  {
    return docId;
  }

  public void setId(long id)
  {
    this.id = id;
  }
  public long getId()
  {
    return id;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public void setDocName(String docName)
  {
    this.docName = docName;
  }
  public String getDocName()
  {
    return docName;
  }

  public void setDocSize(long docSize)
  {
    this.docSize = docSize;
  }
  public long getDocSize()
  {
    return docSize;
  }

  public String toString()
  {
    return "Document [ "
      + "doc id: " + docId
      + ", item id: " + id
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + ", description: " + description
      + ", name: " + docName
      + ", doc size: " + docSize
      + " ]";
  }
}