package com.mes.tmg;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.user.UserBean;

public class AcrActionHelper
{
  static Logger log = Logger.getLogger(AcrActionHelper.class);

  private TmgDb     db;
  private UserBean  user;
  private Order     order;
  private List      callTags;
  private List      acrActions;
  private CallTag   curCt;

  public AcrActionHelper()
  {
    db = new TmgDb();
    reset();
  }

  public void reset()
  {
    order = null;
    callTags = new ArrayList();
    acrActions = new ArrayList();
    curCt = null;
  }

  public boolean inOrder()
  {
    return order != null;
  }

  private void validateNotInOrder()
  {
    if (inOrder())
    {
      throw new RuntimeException("Order already started");
    }
  }

  private void validateInOrder()
  {
    if (!inOrder())
    {
      throw new RuntimeException("Order not started");
    }
  }

  public Order startOrder(long acrId, UserBean user)
  {
    validateNotInOrder();
    this.user = user;
    order = db.createAcrOrder(acrId,user);
    return order;
  }

  public CallTag addCallTag()
  {
    validateInOrder();
    CallTag ct = new CallTag();
    ct.setCtId(db.getNewId());
    ct.setOrdId(order.getOrdId());
    ct.setUserName(user.getLoginName());
    ct.setCreateDate(db.getCurDate());
    ct.setCtStatCode(CallTagStatus.CTS_NEW);
    ct.setCallTagParts(new ArrayList());
    callTags.add(ct);
    curCt = ct;
    return ct;
  }

  private void addAcrAction(CallTagPart ctPart)
  {
    log.debug("addAcrAction(" + ctPart + ")");

    AcrAction aa = new AcrAction();
    aa.setAaId(db.getNewId());
    aa.setCreateDate(db.getCurDate());
    aa.setUserName(user.getLoginName());
    aa.setAcrId(order.getAcrId());
    aa.setOrdId(order.getOrdId());
    if (ctPart != null)
    {
      aa.setCtPartId(ctPart.getCtPartId());
      aa.setSerialNum(ctPart.getSerialNum());
    }
    acrActions.add(aa);

    log.debug("added acr action: " + aa);
  }

  public CallTagPart addCallTagPart(String drCode, long ptId, long partId, 
    String serialNum, String ctrCode)
  {
    validateInOrder();

    if (ctrCode == null)
    {
      throw new NullPointerException("Call tag reason code cannot be null");
    }

    if (curCt == null)
    {
      addCallTag();
    }

    CallTagPart ctPart = new CallTagPart();
    ctPart.setCtPartId(db.getNewId());
    ctPart.setCtId(curCt.getCtId());
    ctPart.setOrdId(order.getOrdId());
    ctPart.setCreateDate(db.getCurDate());
    ctPart.setUserName(user.getLoginName());
    ctPart.setCtpStatCode(CallTagPartStatus.CTPS_NEW);
    ctPart.setDrCode(drCode);
    ctPart.setPtId(ptId);
    ctPart.setPartId(partId);
    ctPart.setSerialNum(serialNum);
    ctPart.setCtrCode(ctrCode);
    curCt.getCallTagParts().add(ctPart);

    addAcrAction(ctPart);

    if (partId != 0L)
    {
      String noteText = "Added to order # " + order.getOrdId() 
        + " call tag for ACR #" + order.getAcrId();
      NoteAction.insertNote(db,user,"makeAcrOrder",partId,noteText);
    }

    return ctPart;
  }
  public CallTagPart addCallTagPart(String drCode, String serialNum, 
    String ctrCode)
  {
    Part part = db.getPartWithSerialNum(serialNum);
    long ptId = part != null ? part.getPtId() : 0L;
    long partId = part != null ? part.getPartId() : 0L;
    return addCallTagPart(drCode,ptId,partId,serialNum,ctrCode);
  }
  public CallTagPart addCallTagPart(String drCode, String serialNum, 
    long ptId, String ctrCode)
  {
    return addCallTagPart(drCode,ptId,0L,serialNum,ctrCode);
  }
  public CallTagPart addCallTagPart(String drCode, long ptId, String ctrCode)
  {
    return addCallTagPart(drCode,ptId,0L,null,ctrCode);
  }

  public void finishOrder()
  {
    validateInOrder();
    for (Iterator i = callTags.iterator(); i.hasNext();)
    {
      CallTag ct = (CallTag)i.next();
      db.persist(ct,user);
      for (Iterator j = ct.getCallTagParts().iterator(); j.hasNext();)
      {
        db.persist(j.next(),user);
      }
    }
    for (Iterator i = acrActions.iterator(); i.hasNext();)
    {
      db.persist(i.next(),user);
    }
    reset();
  }

  public Order getOrder()
  {
    return order;
  }

  public CallTag getCurCt()
  {
    return curCt;
  }

  public List getCallTags()
  {
    return callTags;
  }
}