/**
 * TmgViewState
 *
 * Class intended to serve as a base for view state handling.  May be used to
 * track view states during a session.  Stores refs to handler, action and db
 * objects useful for creating TmgViewBean based objects during action handling.
 * The setAction() methods are useful for updating the view state to be associated 
 * with the current action when used over multiple requests during a session.
 */ 

package com.mes.tmg;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.log4j.Logger;

public class TmgViewState
{
  static Logger log = Logger.getLogger(TmgViewState.class);

  protected TmgRequestHandler handler;
  protected TmgAction         action;
  protected TmgDb             db;
  protected Map               beanMap = new HashMap();

  public static final String AN_SET_ORDER_HEADER = "actionSetOrderHeader";

  public TmgViewState()
  {
  }

  public TmgViewState(TmgAction action, String viewStateName)
  {
    setAction(action,viewStateName);
  }
  public TmgViewState(TmgAction action)
  {
    this(action,null);
  }

  public void setAction(TmgAction action, String viewStateName)
  {
    this.action = action;
    setTmgDb(action.getDb());
    setTmgRequestHandler(action.getTmgHandler());
    action.getRequest()
      .setAttribute(viewStateName != null ? viewStateName : "viewState",this);
    
    for (Iterator i = beanMap.values().iterator(); i.hasNext();)
    {
      ((TmgViewBean)i.next()).setAction(action);
    }
  }
  public void setAction(TmgAction action)
  {
    setAction(action,null);
  }

  public void setTmgDb(TmgDb db)
  {
    this.db = db;
  }

  public void setTmgRequestHandler(TmgRequestHandler handler)
  {
    this.handler = handler;
  }

  public void addViewBean(TmgViewBean viewBean)
  {
    if (viewBean.getBeanName().equals("viewBean"))
    {
      throw new RuntimeException("Invalid view bean, view bean cannot use"
        + " default bean name 'viewBean'");
    }
    beanMap.put(viewBean.getBeanName(),viewBean);
  }

  public TmgViewBean getViewBean(String beanName)
  {
    return (TmgViewBean)beanMap.get(beanName);
  }
}
