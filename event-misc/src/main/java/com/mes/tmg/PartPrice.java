package com.mes.tmg;

public class PartPrice extends TmgBean
{
  private long    ppId;
  private long    partId;
  private double  partPrice;

  public void setPpId(long ppId)
  {
    this.ppId = ppId;
  }
  public long getPpId()
  {
    return ppId;
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }

  public void setPartPrice(double partPrice)
  {
    this.partPrice = partPrice;
  }
  public double getPartPrice()
  {
    return partPrice;
  }

  public String toString()
  {
    return "PartPrice ["
      + " id: " + ppId
      + ", part: " + partId
      + ", price: " + partPrice
      + " ]";
  }
}
