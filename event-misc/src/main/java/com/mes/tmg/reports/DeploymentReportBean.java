package com.mes.tmg.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.Action;
import com.mes.mvc.Downloadable;
import com.mes.tmg.Client;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.ClientTable;
import com.mes.tmg.util.DeployReasonTable;
import com.mes.tmg.util.PartClassTable;
import com.mes.tmg.util.PartDispositionTable;
import com.mes.tmg.util.PartFeatureTable;
import com.mes.tmg.util.PartTypeTable;

public class DeploymentReportBean extends TmgViewBean implements Downloadable
{
  static Logger log = Logger.getLogger(DeploymentReportBean.class);

  public static final String FN_FROM_DATE     = "fromDate";
  public static final String FN_TO_DATE       = "toDate";
  public static final String FN_CLIENT_ID     = "clientId";
  public static final String FN_DR_CODE       = "drCode";
  public static final String FN_DISP_CODE     = "dispCode";
  public static final String FN_PC_CODE       = "pcCode";
  public static final String FN_CAT_FEAT_ID   = "catFeatId";
  public static final String FN_PT_ID         = "ptId";
  public static final String FN_SEARCH_TERM   = "searchTerm";
  public static final String FN_SUBMIT_BTN    = "submitBtn";

  private List reportRows;

  public static final String HDR_CLIENT       = "Client";
  public static final String HDR_SRC_INV      = "Source";
  public static final String HDR_RECV_DATE    = "Received Date";
  public static final String HDR_DPLY_DATE    = "Deployed Date";
  public static final String HDR_DPLY_REASON  = "Deploy Reason";
  public static final String HDR_DBA_NAME     = "DBA Name";
  public static final String HDR_MERCH_NUM    = "Merchant #";
  public static final String HDR_ASSOC_NUM    = "Assoc #";
  public static final String HDR_DISP_TYPE    = "Disposition";
  public static final String HDR_SERIAL_NUM   = "Serial #";
  public static final String HDR_EQUIP_TYPE   = "Equip Type";
  public static final String HDR_EQUIP_DESC   = "Equip Desc";
  public static final String HDR_CONDITION    = "Condition";

  public DeploymentReportBean(TmgAction action)
  {
    super(action,"deploymentReportBean");
    putOrderHeader(HDR_CLIENT,      "client_name");
    putOrderHeader(HDR_SRC_INV,     "src_inventory");
    putOrderHeader(HDR_RECV_DATE,   "receive_date");
    putOrderHeader(HDR_DPLY_DATE,   "deploy_date");
    putOrderHeader(HDR_DPLY_REASON, "deploy_type");
    putOrderHeader(HDR_DBA_NAME,    "merch_name");
    putOrderHeader(HDR_MERCH_NUM,   "merch_num");
    putOrderHeader(HDR_ASSOC_NUM,   "assoc_num");
    putOrderHeader(HDR_DISP_TYPE,   "disp_name");
    putOrderHeader(HDR_SERIAL_NUM,  "serial_num");
    putOrderHeader(HDR_EQUIP_TYPE,  "category");
    putOrderHeader(HDR_EQUIP_DESC,  "part_description");
    putOrderHeader(HDR_CONDITION,   "condition");
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new DateStringField(FN_FROM_DATE,"From Date",false,false));
      fields.add(new DateStringField(FN_TO_DATE,"To Date",true,false));
      Client client = getClient();
      if (client != null && client.isMes())
      {
        fields.add(new DropDownField(FN_CLIENT_ID,new ClientTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
      }
      else
      {
        String clientId = (client != null ? ""+client.getClientId() : "0" );
        fields.add(new HiddenField(FN_CLIENT_ID,clientId));
      }
      fields.add(new DropDownField(FN_DR_CODE,new DeployReasonTable(TmgDropDownTable.OPT_SEL_ANY,"y",null),true));
      fields.add(new DropDownField(FN_DISP_CODE,new PartDispositionTable(TmgDropDownTable.OPT_SEL_ANY,true),true));
      fields.add(new DropDownField(FN_PC_CODE,new PartClassTable(TmgDropDownTable.OPT_SEL_ANY,true),true));
      fields.add(new DropDownField(FN_CAT_FEAT_ID,new PartFeatureTable(TmgDropDownTable.OPT_SEL_ANY,"CATEGORY"),true));
      fields.add(new DropDownField(FN_PT_ID,new PartTypeTable(db),true));
      fields.add(new Field(FN_SEARCH_TERM,"Search Term",30,20,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public void setAction(Action action, String requestName)
  {
    Field clientField = getField(FN_CLIENT_ID);
    if (clientField != null)
    {
      Client client = getClient();
      if (client != null && client.isMes() && 
          clientField instanceof HiddenField)
      {
        fields.deleteField(FN_CLIENT_ID);
        fields.add(new DropDownField(FN_CLIENT_ID,
          new ClientTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
        reportRows = null;
      }
      else if ((client == null || !client.isMes()) && 
               clientField instanceof DropDownField)
      {
        fields.deleteField(FN_CLIENT_ID);
        String clientId = (client != null ? ""+client.getClientId() : "0" );
        fields.add(new HiddenField(FN_CLIENT_ID,clientId));
        reportRows = null;
      }
    }
    super.setAction(action,requestName);
  }
    

  protected boolean showFeedback()
  {
    return false;
  }

  public List getRows()
  {
    if (reportRows == null || (isAutoSubmit() && !isAutoValid()))
    {
      reportRows = new ArrayList();
    }
    return reportRows;
  }

  private boolean loadRows()
  {
    try
    {
      Date fromDate = ((DateStringField)getField(FN_FROM_DATE)).getUtilDate();
      Date toDate = ((DateStringField)getField(FN_TO_DATE)).getUtilDate();
      long clientId = getField(FN_CLIENT_ID).asLong();
      String drCode = getData(FN_DR_CODE);
      drCode = (!drCode.equals("any") ? drCode : null);
      String dispCode = getData(FN_DISP_CODE);
      dispCode = (!dispCode.equals("any") ? dispCode : null);
      String pcCode = getData(FN_PC_CODE);
      pcCode = (!pcCode.equals("any") ? pcCode : null);
      long catFeatId = getField(FN_CAT_FEAT_ID).asLong();
      long ptId = getField(FN_PT_ID).asLong();
      String searchTerm = getData(FN_SEARCH_TERM);
      searchTerm = (searchTerm.length() > 0 ? searchTerm : null);
      reportRows = db.getDeploymentReportRows(fromDate,toDate,clientId,
                                              drCode,dispCode,pcCode,
                                              catFeatId,ptId,searchTerm,
                                              getOrderName("deploy_date"),
                                              getOrderDirFlag(false));
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoSubmit()
  {
    setData(FN_SUBMIT_BTN,"");
    return loadRows();
  }

  protected void notifyNewOrder(boolean newOrderOk)
  {
    if (newOrderOk)
    {
      loadRows();
    }
  }

  protected boolean autoLoad()
  {
    try
    {
      Calendar cal = Calendar.getInstance();
      DateStringField fromField = (DateStringField)fields.getField(FN_FROM_DATE);
      if (fromField.isBlank())
      {
        DateStringField toField = (DateStringField)fields.getField(FN_TO_DATE);
        toField.setUtilDate(cal.getTime());
        cal.add(cal.DATE,-1);
        fromField.setUtilDate(cal.getTime());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Download support
   */
  
  public void startDownload()
  {
    Date fromDate = ((DateStringField)getField(FN_FROM_DATE)).getUtilDate();
    Date toDate = ((DateStringField)getField(FN_TO_DATE)).getUtilDate();
    long clientId = getField(FN_CLIENT_ID).asLong();
    String drCode = getData(FN_DR_CODE);
    drCode = (!drCode.equals("any") ? drCode : null);
    String dispCode = getData(FN_DISP_CODE);
    dispCode = (!dispCode.equals("any") ? dispCode : null);
    String pcCode = getData(FN_PC_CODE);
    pcCode = (!pcCode.equals("any") ? pcCode : null);
    long catFeatId = getField(FN_CAT_FEAT_ID).asLong();
    long ptId = getField(FN_PT_ID).asLong();
    String searchTerm = getData(FN_SEARCH_TERM);
    searchTerm = (searchTerm.length() > 0 ? searchTerm : null);
    db.startDeploymentReportDownload(fromDate,toDate,clientId,
      drCode,dispCode,pcCode,catFeatId,ptId,searchTerm,
      getOrderName("deploy_date"),getOrderDirFlag(false));
  }

  public String getDlHeader()
  {
    return (isMultiClient() ? "Client," : "")
      + "Source"
      + ",\"Receive Date\""
      + ",\"Deploy Date\""
      + ",\"Deploy Reason\""
      + ",\"DBA Name\""
      + ",\"Merchant #\""
      + ",\"Assoc #\""
      + ",\"Disposition Type\""
      + ",\"Serial Number\""
      + ",\"Equip Type\""
      + ",\"Equip Desc\""
      + ",Condition";
  }

  public String getDlNext()
  {
    try
    {
      ResultSet rs = db.getDlRs();
      if (rs.next())
      {
        StringBuffer buf = new StringBuffer();
        if (isMultiClient())
        {
          buf.append("\"" + rs.getString("client_name") + "\",");
        }
        buf.append("\"" + rs.getString("src_inventory") + "\"");
        Date receiveDate = Tmg.toDate(rs.getTimestamp("receive_date"));
        buf.append(",\"" + db.formatDlDate(receiveDate) + "\"");
        Date deployDate = Tmg.toDate(rs.getTimestamp("deploy_date"));
        buf.append(",\"" + db.formatDlDate(deployDate) + "\"");
        buf.append(",\"" + rs.getString("deploy_type") + "\"");
        buf.append(",\"" + rs.getString("merch_name") + "\"");
        buf.append(",\"" + rs.getString("merch_num") + "\"");
        buf.append(",\"" + rs.getString("assoc_num") + "\"");
        buf.append(",\"" + rs.getString("disp_name") + "\"");
        buf.append(",\"" + rs.getString("serial_num") + "\"");
        buf.append(",\"" + rs.getString("category") + "\"");
        buf.append(",\"" + rs.getString("part_description") + "\"");
        buf.append(",\"" + rs.getString("condition") + "\"");
        return buf.toString();
      }
    }
    catch (Exception e)
    {
      log.error("Error getting next download row: " + e);
      e.printStackTrace();
    }
    return null;
  }

  public void finishDownload()
  {
    db.finishManualStatement();
  }

  public String getDlFilename()
  {
    return "deploymentreport";
  }
}
