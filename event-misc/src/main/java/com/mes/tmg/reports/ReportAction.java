package com.mes.tmg.reports;

import org.apache.log4j.Logger;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class ReportAction extends TmgAction
{
  static Logger log = Logger.getLogger(ReportAction.class);

  public static final String SN_MAINT_REQS_REPORT_BEAN 
                                          = "maintReqsReportBean";
  private MaintReqsReportBean getMaintReqsReportBean()
  {
    MaintReqsReportBean bean = (MaintReqsReportBean)
      getSessionAttr(SN_MAINT_REQS_REPORT_BEAN,null);
    if (bean == null)
    {
      bean = new MaintReqsReportBean(this);
      setSessionAttr(SN_MAINT_REQS_REPORT_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doMaintReqsReport()
  {
    MaintReqsReportBean bean = getMaintReqsReportBean();
    request.setAttribute("viewBean",bean);
    doView(Tmg.VIEW_MAINT_REQS_REPORT);
  }

  public void doDlMaintReqsReport()
  {
    doCsvDownload(getMaintReqsReportBean());
  }

  public static final String SN_ON_HAND_REPORT_BEAN 
                                          = "onHandReportBean";
  private OnHandReportBean getOnHandReportBean()
  {
    OnHandReportBean bean = (OnHandReportBean)
      getSessionAttr(SN_ON_HAND_REPORT_BEAN,null);
    if (bean == null)
    {
      bean = new OnHandReportBean(this);
      setSessionAttr(SN_ON_HAND_REPORT_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doOnHandReport()
  {
    OnHandReportBean bean = getOnHandReportBean();
    request.setAttribute("viewBean",bean);
    doView(Tmg.VIEW_ON_HAND_REPORT);
  }

  public void doDlOnHandReport()
  {
    doCsvDownload(getOnHandReportBean());
  }

  public static final String SN_CALL_TAG_REPORT_BEAN 
                                          = "callTagReportBean";
  private CallTagReportBean getCallTagReportBean()
  {
    CallTagReportBean bean = (CallTagReportBean)
      getSessionAttr(SN_CALL_TAG_REPORT_BEAN,null);
    if (bean == null)
    {
      bean = new CallTagReportBean(this);
      setSessionAttr(SN_CALL_TAG_REPORT_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doCallTagReport()
  {
    CallTagReportBean bean = getCallTagReportBean();
    request.setAttribute("viewBean",bean);
    doView(Tmg.VIEW_CALL_TAG_REPORT);
  }

  public void doDlCallTagReport()
  {
    doCsvDownload(getCallTagReportBean());
  }

  public static final String SN_REPAIR_REPORT_BEAN 
                                          = "repairReportBean";
  private RepairReportBean getRepairReportBean()
  {
    RepairReportBean bean = (RepairReportBean)
      getSessionAttr(SN_REPAIR_REPORT_BEAN,null);
    if (bean == null)
    {
      bean = new RepairReportBean(this);
      setSessionAttr(SN_REPAIR_REPORT_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doRepairReport()
  {
    RepairReportBean bean = getRepairReportBean();
    request.setAttribute("viewBean",bean);
    doView(Tmg.VIEW_REPAIR_REPORT);
  }

  public void doDlRepairReport()
  {
    doCsvDownload(getRepairReportBean());
  }

  public static final String SN_DEPLOYMENT_REPORT_BEAN 
                                                = "deploymentReportBean";

  private DeploymentReportBean getDeploymentReportBean()
  {
    DeploymentReportBean bean 
      = (DeploymentReportBean)getSessionAttr(SN_DEPLOYMENT_REPORT_BEAN,null);
    if (bean == null)
    {
      bean = new DeploymentReportBean(this);
      setSessionAttr(SN_DEPLOYMENT_REPORT_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doDeploymentReport()
  {
    DeploymentReportBean bean = getDeploymentReportBean();
    request.setAttribute("viewBean",bean);
    doView(Tmg.VIEW_DEPLOYMENT_REPORT);
  }

  public void doDlDeploymentReport()
  {
    doCsvDownload(getDeploymentReportBean());
  }

  public static final String SN_LOST_PARTS_REPORT_BEAN 
                                                = "lostPartsReportBean";

  private LostPartsReportBean getLostPartsReportBean()
  {
    LostPartsReportBean bean 
      = (LostPartsReportBean)getSessionAttr(SN_LOST_PARTS_REPORT_BEAN,null);
    if (bean == null)
    {
      bean = new LostPartsReportBean(this);
      setSessionAttr(SN_LOST_PARTS_REPORT_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doLostPartsReport()
  {
    LostPartsReportBean bean = getLostPartsReportBean();
    request.setAttribute("viewBean",bean);
    doView(Tmg.VIEW_LOST_PARTS_REPORT);
  }

  public void doDlLostPartsReport()
  {
    doCsvDownload(getLostPartsReportBean());
  }

  public static final String SN_INV_GROWTH_REPORT_BEAN 
                                                = "invGrowthReportBean";

  private InvGrowthReportBean getInvGrowthReportBean()
  {
    InvGrowthReportBean bean 
      = (InvGrowthReportBean)getSessionAttr(SN_INV_GROWTH_REPORT_BEAN,null);
    if (bean == null)
    {
      bean = new InvGrowthReportBean(this);
      setSessionAttr(SN_INV_GROWTH_REPORT_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doInvGrowthReport()
  {
    InvGrowthReportBean bean = getInvGrowthReportBean();
    request.setAttribute("viewBean",bean);
    doView(Tmg.VIEW_INV_GROWTH_REPORT);
  }

  public void doDlInvGrowthReport()
  {
    doCsvDownload(getInvGrowthReportBean());
  }
}
