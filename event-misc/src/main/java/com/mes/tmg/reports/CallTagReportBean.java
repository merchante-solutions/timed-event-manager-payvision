package com.mes.tmg.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.Action;
import com.mes.mvc.Downloadable;
import com.mes.tmg.Client;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.ClientTable;
import com.mes.tmg.util.CtpStatusTable;
import com.mes.tmg.util.DeployReasonTable;

public class CallTagReportBean extends TmgViewBean implements Downloadable
{
  static Logger log = Logger.getLogger(CallTagReportBean.class);

  public static final String FN_FROM_DATE         = "fromDate";
  public static final String FN_TO_DATE           = "toDate";
  public static final String FN_CLIENT_ID         = "clientId";
  public static final String FN_DR_CODE           = "drCode";
  public static final String FN_CTPS_DESCRIPTION  = "ctpsDescription";
  public static final String FN_SEARCH_TERM       = "searchTerm";
  public static final String FN_SUBMIT_BTN        = "submitBtn";

  private List reportRows;

  public static final String HDR_CT_DATE          = "Date";
  public static final String HDR_CLIENT           = "Client";
  public static final String HDR_MERCH_NUM        = "Merchant #";
  public static final String HDR_MERCH_NAME       = "DBA Name";
  public static final String HDR_ASSOC_NUM        = "Assoc #";
  public static final String HDR_CT_REASON        = "Reason";
  public static final String HDR_CT_NUM           = "Call Tag #";
  public static final String HDR_SERIAL_NUM       = "Serial #";
  public static final String HDR_DESCRIPTION      = "Equip Type";
  public static final String HDR_UNIT_COST        = "Unit Cost";
  public static final String HDR_CT_STATUS        = "Call Tag Status";

  public CallTagReportBean(TmgAction action)
  {
    super(action,"CallTagReportBean");
    putOrderHeader(HDR_CT_DATE,     "call_tag_ts");
    putOrderHeader(HDR_CLIENT,      "client_name");
    putOrderHeader(HDR_MERCH_NUM,   "merch_num");
    putOrderHeader(HDR_MERCH_NAME,  "merch_name");
    putOrderHeader(HDR_ASSOC_NUM,   "assoc_num");
    putOrderHeader(HDR_CT_REASON,   "call_tag_reason");
    putOrderHeader(HDR_CT_NUM,      "call_tag_num");
    putOrderHeader(HDR_SERIAL_NUM,  "serial_num");
    putOrderHeader(HDR_DESCRIPTION, "description");
    putOrderHeader(HDR_UNIT_COST,   "unit_cost");
    putOrderHeader(HDR_CT_STATUS,   "call_tag_status");
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new DateStringField(FN_FROM_DATE,"From Date",false,false));
      fields.add(new DateStringField(FN_TO_DATE,"To Date",true,false));
      Client client = getClient();
      if (client != null && client.isMes())
      {
        fields.add(new DropDownField(FN_CLIENT_ID,new ClientTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
      }
      else
      {
        String clientId = (client != null ? ""+client.getClientId() : "0" );
        fields.add(new HiddenField(FN_CLIENT_ID,clientId));
      }
      fields.add(new DropDownField(FN_DR_CODE,new DeployReasonTable(TmgDropDownTable.OPT_SEL_ANY,null,"y"),true));
      fields.add(new DropDownField(FN_CTPS_DESCRIPTION,new CtpStatusTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
      fields.add(new Field(FN_SEARCH_TERM,"Search Term",30,20,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public void setAction(Action action, String requestName)
  {
    Field clientField = getField(FN_CLIENT_ID);
    if (clientField != null)
    {
      Client client = getClient();
      if (client != null && client.isMes() && 
          clientField instanceof HiddenField)
      {
        fields.deleteField(FN_CLIENT_ID);
        fields.add(new DropDownField(FN_CLIENT_ID,
          new ClientTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
        reportRows = null;
      }
      else if ((client == null || !client.isMes()) && 
               clientField instanceof DropDownField)
      {
        fields.deleteField(FN_CLIENT_ID);
        String clientId = (client != null ? ""+client.getClientId() : "0" );
        fields.add(new HiddenField(FN_CLIENT_ID,clientId));
        reportRows = null;
      }
    }
    super.setAction(action,requestName);
  }
    

  protected boolean showFeedback()
  {
    return false;
  }

  public List getRows()
  {
    if (reportRows == null || (isAutoSubmit() && !isAutoValid()))
    {
      reportRows = new ArrayList();
    }
    return reportRows;
  }

  private boolean loadRows()
  {
    try
    {
      Date fromDate = ((DateStringField)getField(FN_FROM_DATE)).getUtilDate();
      Date toDate = ((DateStringField)getField(FN_TO_DATE)).getUtilDate();
      long clientId = getField(FN_CLIENT_ID).asLong();
      String drCode = getData(FN_DR_CODE);
      drCode = (!drCode.equals("any") ? drCode : null);
      String ctpsDesc = getData(FN_CTPS_DESCRIPTION);
      ctpsDesc = (!ctpsDesc.equals("any") ? ctpsDesc : null);
      String searchTerm = getData(FN_SEARCH_TERM);
      searchTerm = (searchTerm.length() > 0 ? searchTerm : null);
      reportRows = db.getCtReportRows(fromDate,toDate,clientId,
                                      drCode,ctpsDesc,searchTerm,
                                      getOrderName("call_tag_ts"),
                                      getOrderDirFlag(false));
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoSubmit()
  {
    setData(FN_SUBMIT_BTN,"");
    return loadRows();
  }

  protected void notifyNewOrder(boolean newOrderOk)
  {
    if (newOrderOk)
    {
      loadRows();
    }
  }

  protected boolean autoLoad()
  {
    try
    {
      Calendar cal = Calendar.getInstance();
      DateStringField fromField = (DateStringField)fields.getField(FN_FROM_DATE);
      // default to prior month
      if (fromField.isBlank())
      {
        DateStringField toField = (DateStringField)fields.getField(FN_TO_DATE);
        // set to date to last day of prior month
        cal.set(cal.DATE,1);
        cal.add(cal.DATE,-1);
        toField.setUtilDate(cal.getTime());
        // set from date to first day of prior month
        cal.set(cal.DATE,1);
        fromField.setUtilDate(cal.getTime());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Download support
   */
  
  public void startDownload()
  {
    Date fromDate = ((DateStringField)getField(FN_FROM_DATE)).getUtilDate();
    Date toDate = ((DateStringField)getField(FN_TO_DATE)).getUtilDate();
    long clientId = getField(FN_CLIENT_ID).asLong();
    String drCode = getData(FN_DR_CODE);
    drCode = (!drCode.equals("any") ? drCode : null);
    String ctpsDesc = getData(FN_CTPS_DESCRIPTION);
    ctpsDesc = (!ctpsDesc.equals("any") ? ctpsDesc : null);
    String searchTerm = getData(FN_SEARCH_TERM);
    searchTerm = (searchTerm.length() > 0 ? searchTerm : null);
    db.startCtReportDownload(fromDate,toDate,clientId,drCode,ctpsDesc,
      searchTerm,getOrderName("call_tag_ts"),getOrderDirFlag(false));
  }

  public String getDlHeader()
  {
    return "Date"
      + (isMultiClient() ? ", Client" : "")
      + ",\"Merchant #\""
      + ",\"DBA Name\""
      + ",\"Assoc #\""
      + ", Reason"
      + ",\"Call Tag #\""
      + ",\"Serial #\""
      + ",\"Equip Type\""
      + ",\"Unit Cost\""
      + ",\"Call Tag Status\"";
  }

  public String getDlNext()
  {
    try
    {
      ResultSet rs = db.getDlRs();
      if (rs.next())
      {
        StringBuffer buf = new StringBuffer();
        Date ctDate = Tmg.toDate(rs.getTimestamp("call_tag_ts"));
        buf.append("\"" + db.formatDlDate(ctDate) + "\"");
        if (isMultiClient())
        {
          buf.append(",\"" + rs.getString("client_name") + "\"");
        }
        buf.append(",\"" + rs.getString("merch_num") + "\"");
        buf.append(",\"" + rs.getString("merch_name") + "\"");
        buf.append(",\"" + rs.getString("assoc_num") + "\"");
        buf.append(",\"" + rs.getString("call_tag_reason") + "\"");
        buf.append(",\"" + rs.getString("call_tag_num") + "\"");
        buf.append(",\"" + rs.getString("serial_num") + "\"");
        buf.append(",\"" + rs.getString("description") + "\"");
        buf.append(",\"" + rs.getDouble("unit_cost") + "\"");
        buf.append(",\"" + rs.getString("call_tag_status") + "\"");
        return buf.toString();
      }
    }
    catch (Exception e)
    {
      log.error("Error getting next download row: " + e);
      e.printStackTrace();
    }
    return null;
  }

  public void finishDownload()
  {
    db.finishManualStatement();
  }

  public String getDlFilename()
  {
    return "calltagreport";
  }
}
