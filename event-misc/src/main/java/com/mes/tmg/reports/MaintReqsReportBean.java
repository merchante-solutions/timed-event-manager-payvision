package com.mes.tmg.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.Action;
import com.mes.mvc.Downloadable;
import com.mes.tmg.Client;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.ClientTable;

public class MaintReqsReportBean extends TmgViewBean implements Downloadable
{
  static Logger log = Logger.getLogger(MaintReqsReportBean.class);

  public static final String FN_FROM_DATE     = "fromDate";
  public static final String FN_TO_DATE       = "toDate";
  public static final String FN_CLIENT_ID     = "clientId";
  public static final String FN_SEARCH_TERM   = "searchTerm";
  public static final String FN_SUBMIT_BTN    = "submitBtn";

  private List reportRows;

  public static final String HDR_ACR_ID       = "ACR";
  public static final String HDR_REQ_DATE     = "Requested";
  public static final String HDR_USER_NAME    = "User";
  public static final String HDR_CLIENT       = "Client";
  public static final String HDR_ACR_REASON   = "Reason";
  public static final String HDR_DPLY_TYPE    = "Deploy Type";
  public static final String HDR_DBA_NAME     = "DBA Name";
  public static final String HDR_MERCH_NUM    = "Merchant #";
  public static final String HDR_SERIAL_NUM   = "Serial #";
  public static final String HDR_EQUIP_DESC   = "Equipment";
  public static final String HDR_ORDER_STAT   = "Order";
  public static final String HDR_CT_STAT      = "Call Tag";
  public static final String HDR_TEST_RESULT  = "Testing";
  public static final String HDR_RPR_STATUS   = "Repair";

  public MaintReqsReportBean(TmgAction action)
  {
    super(action,"MaintReqsReportBean");
    putOrderHeader(HDR_ACR_ID,      "acr_id");
    putOrderHeader(HDR_REQ_DATE,    "request_ts");
    putOrderHeader(HDR_USER_NAME,   "user_name");
    putOrderHeader(HDR_CLIENT,      "client_name");
    putOrderHeader(HDR_DPLY_TYPE,   "deploy_type");
    putOrderHeader(HDR_ACR_REASON,  "acr_reason");
    putOrderHeader(HDR_DBA_NAME,    "merch_name");
    putOrderHeader(HDR_MERCH_NUM,   "merch_num");
    putOrderHeader(HDR_SERIAL_NUM,  "serial_num");
    putOrderHeader(HDR_EQUIP_DESC,  "part_description");
    putOrderHeader(HDR_ORDER_STAT,  "order_status");
    putOrderHeader(HDR_CT_STAT,     "call_tag_status");
    putOrderHeader(HDR_TEST_RESULT, "test_result");
    putOrderHeader(HDR_RPR_STATUS,  "repair_status");
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new DateStringField(FN_FROM_DATE,"From Date",false,false));
      fields.add(new DateStringField(FN_TO_DATE,"To Date",true,false));
      Client client = getClient();
      if (client != null && client.isMes())
      {
        fields.add(new DropDownField(FN_CLIENT_ID,new ClientTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
      }
      else
      {
        String clientId = (client != null ? ""+client.getClientId() : "0" );
        fields.add(new HiddenField(FN_CLIENT_ID,clientId));
      }
      fields.add(new Field(FN_SEARCH_TERM,"Search Term",30,20,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public void setAction(Action action, String requestName)
  {
    Field clientField = getField(FN_CLIENT_ID);
    if (clientField != null)
    {
      Client client = getClient();
      if (client != null && client.isMes() && 
          clientField instanceof HiddenField)
      {
        fields.deleteField(FN_CLIENT_ID);
        fields.add(new DropDownField(FN_CLIENT_ID,
          new ClientTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
        reportRows = null;
      }
      else if ((client == null || !client.isMes()) && 
               clientField instanceof DropDownField)
      {
        fields.deleteField(FN_CLIENT_ID);
        String clientId = (client != null ? ""+client.getClientId() : "0" );
        fields.add(new HiddenField(FN_CLIENT_ID,clientId));
        reportRows = null;
      }
    }
    super.setAction(action,requestName);
  }
    

  protected boolean showFeedback()
  {
    return false;
  }

  public List getRows()
  {
    if (reportRows == null || (isAutoSubmit() && !isAutoValid()))
    {
      reportRows = new ArrayList();
    }
    return reportRows;
  }

  private boolean loadRows()
  {
    try
    {
      Date fromDate = ((DateStringField)getField(FN_FROM_DATE)).getUtilDate();
      Date toDate = ((DateStringField)getField(FN_TO_DATE)).getUtilDate();
      long clientId = getField(FN_CLIENT_ID).asLong();
      String searchTerm = getData(FN_SEARCH_TERM);
      searchTerm = (searchTerm.length() > 0 ? searchTerm : null);
      reportRows = db.getMaintReqsReportRows(fromDate,toDate,
                                             clientId,searchTerm,
                                             getOrderName("request_ts"),
                                             getOrderDirFlag(false));
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoSubmit()
  {
    setData(FN_SUBMIT_BTN,"");
    return loadRows();
  }

  protected void notifyNewOrder(boolean newOrderOk)
  {
    if (newOrderOk)
    {
      loadRows();
    }
  }

  protected boolean autoLoad()
  {
    try
    {
      Calendar cal = Calendar.getInstance();
      DateStringField fromField = (DateStringField)fields.getField(FN_FROM_DATE);
      if (fromField.isBlank())
      {
        DateStringField toField = (DateStringField)fields.getField(FN_TO_DATE);
        toField.setUtilDate(cal.getTime());
        cal.add(cal.DATE,-1);
        fromField.setUtilDate(cal.getTime());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Download support
   */
  
  public void startDownload()
  {
    Date fromDate = ((DateStringField)getField(FN_FROM_DATE)).getUtilDate();
    Date toDate = ((DateStringField)getField(FN_TO_DATE)).getUtilDate();
    long clientId = getField(FN_CLIENT_ID).asLong();
    String searchTerm = getData(FN_SEARCH_TERM);
    searchTerm = (searchTerm.length() > 0 ? searchTerm : null);
    db.startMaintReqsReportDownload(fromDate,toDate,clientId,
      searchTerm,getOrderName("request_ts"),getOrderDirFlag(false));
  }

  public String getDlHeader()
  {
    return "ACR"
      + ", Requested"
      + ", User"
      + (isMultiClient() ? ", Client" : "")
      + ",\"Deploy Type\""
      + ",\"DBA Name\""
      + ",\"Merchant #\""
      + ",\"Equip Desc\""
      + ",\"Serial #\""
      + ",\"Order Status\""
      + ",\"Call Tag Status\""
      + ",\"ACR Reason\""
      + ",\"Testing\""
      + ",Repair";
  }

  public String getDlNext()
  {
    try
    {
      ResultSet rs = db.getDlRs();
      if (rs.next())
      {
        StringBuffer buf = new StringBuffer();
        buf.append("\"" + rs.getString("acr_id") + "\"");
        Date requestDate = Tmg.toDate(rs.getTimestamp("request_ts"));
        buf.append(",\"" + db.formatDlDate(requestDate) + "\"");
        buf.append(",\"" + rs.getString("user_name") + "\"");
        if (isMultiClient())
        {
          buf.append(",\"" + rs.getString("client_name") + "\"");
        }
        buf.append(",\"" + rs.getString("deploy_type") + "\"");
        buf.append(",\"" + rs.getString("merch_name") + "\"");
        buf.append(",\"" + rs.getString("merch_num") + "\"");
        buf.append(",\"" + rs.getString("part_description") + "\"");
        buf.append(",\"" + rs.getString("serial_num") + "\"");
        buf.append(",\"" + rs.getString("order_status") + "\"");
        buf.append(",\"" + rs.getString("call_tag_status") + "\"");
        buf.append(",\"" + rs.getString("acr_reason") + "\"");
        buf.append(",\"" + rs.getString("test_result") + "\"");
        buf.append(",\"" + rs.getString("repair_status") + "\"");
        return buf.toString();
      }
    }
    catch (Exception e)
    {
      log.error("Error getting next download row: " + e);
      e.printStackTrace();
    }
    return null;
  }

  public void finishDownload()
  {
    db.finishManualStatement();
  }

  public String getDlFilename()
  {
    return "maintreqsreport";
  }
}
