package com.mes.tmg.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.Action;
import com.mes.mvc.Downloadable;
import com.mes.tmg.Client;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.ClientTable;

public class RepairReportBean extends TmgViewBean implements Downloadable
{
  static Logger log = Logger.getLogger(RepairReportBean.class);

  public static final String FN_FROM_DATE         = "fromDate";
  public static final String FN_TO_DATE           = "toDate";
  public static final String FN_CLIENT_ID         = "clientId";
  public static final String FN_SEARCH_TERM       = "searchTerm";
  public static final String FN_SUBMIT_BTN        = "submitBtn";

  private List reportRows;

  public static final String HDR_EQUIP_TYPE       = "Equip Type";
  public static final String HDR_SERIAL_NUM       = "Serial #";
  public static final String HDR_CLIENT           = "Client";
  public static final String HDR_BATCH_NUM        = "Batch #";
  public static final String HDR_BATCH_DATE       = "Add Date";
  public static final String HDR_SHIP_DATE        = "Ship Date";
  public static final String HDR_REPAIR_REASON    = "Repair Reason";
  public static final String HDR_ACR_ID           = "ACR #";

  public RepairReportBean(TmgAction action)
  {
    super(action,"RepairReportBean");
    putOrderHeader(HDR_EQUIP_TYPE,    "pt_name");
    putOrderHeader(HDR_SERIAL_NUM,    "serial_num");
    putOrderHeader(HDR_CLIENT,        "client_name");
    putOrderHeader(HDR_BATCH_NUM,     "batch_num");
    putOrderHeader(HDR_BATCH_DATE,    "batch_ts");
    putOrderHeader(HDR_SHIP_DATE,     "ship_ts");
    putOrderHeader(HDR_REPAIR_REASON, "repair_reason");
    putOrderHeader(HDR_ACR_ID,        "acr_id");
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new DateStringField(FN_FROM_DATE,"From Date",false,false));
      fields.add(new DateStringField(FN_TO_DATE,"To Date",true,false));
      Client client = getClient();
      if (client != null && client.isMes())
      {
        fields.add(new DropDownField(FN_CLIENT_ID,new ClientTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
      }
      else
      {
        String clientId = (client != null ? ""+client.getClientId() : "0" );
        fields.add(new HiddenField(FN_CLIENT_ID,clientId));
      }
      fields.add(new Field(FN_SEARCH_TERM,"Search Term",30,20,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public void setAction(Action action, String requestName)
  {
    Field clientField = getField(FN_CLIENT_ID);
    if (clientField != null)
    {
      Client client = getClient();
      if (client != null && client.isMes() && 
          clientField instanceof HiddenField)
      {
        fields.deleteField(FN_CLIENT_ID);
        fields.add(new DropDownField(FN_CLIENT_ID,
          new ClientTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
        reportRows = null;
      }
      else if ((client == null || !client.isMes()) && 
               clientField instanceof DropDownField)
      {
        fields.deleteField(FN_CLIENT_ID);
        String clientId = (client != null ? ""+client.getClientId() : "0" );
        fields.add(new HiddenField(FN_CLIENT_ID,clientId));
        reportRows = null;
      }
    }
    super.setAction(action,requestName);
  }
    

  protected boolean showFeedback()
  {
    return false;
  }

  public List getRows()
  {
    if (reportRows == null || (isAutoSubmit() && !isAutoValid()))
    {
      reportRows = new ArrayList();
    }
    return reportRows;
  }

  private boolean loadRows()
  {
    try
    {
      Date fromDate = ((DateStringField)getField(FN_FROM_DATE)).getUtilDate();
      Date toDate = ((DateStringField)getField(FN_TO_DATE)).getUtilDate();
      long clientId = getField(FN_CLIENT_ID).asLong();
      String searchTerm = getData(FN_SEARCH_TERM);
      searchTerm = (searchTerm.length() > 0 ? searchTerm : null);
      reportRows = db.getRprReportRows(fromDate,toDate,clientId,searchTerm,
                                      getOrderName("batch_ts"),
                                      getOrderDirFlag(false));
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoSubmit()
  {
    setData(FN_SUBMIT_BTN,"");
    return loadRows();
  }

  protected void notifyNewOrder(boolean newOrderOk)
  {
    if (newOrderOk)
    {
      loadRows();
    }
  }

  protected boolean autoLoad()
  {
    try
    {
      Calendar cal = Calendar.getInstance();
      DateStringField fromField = (DateStringField)fields.getField(FN_FROM_DATE);
      // default to prior month
      if (fromField.isBlank())
      {
        DateStringField toField = (DateStringField)fields.getField(FN_TO_DATE);
        // set to date to last day of prior month
        cal.set(cal.DATE,1);
        cal.add(cal.DATE,-1);
        toField.setUtilDate(cal.getTime());
        // set from date to first day of prior month
        cal.set(cal.DATE,1);
        fromField.setUtilDate(cal.getTime());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Download support
   */
  
  public void startDownload()
  {
    Date fromDate = ((DateStringField)getField(FN_FROM_DATE)).getUtilDate();
    Date toDate = ((DateStringField)getField(FN_TO_DATE)).getUtilDate();
    long clientId = getField(FN_CLIENT_ID).asLong();
    String searchTerm = getData(FN_SEARCH_TERM);
    searchTerm = (searchTerm.length() > 0 ? searchTerm : null);
    db.startRprReportDownload(fromDate,toDate,clientId,searchTerm,
        getOrderName("batch_ts"),getOrderDirFlag(false));
  }

  public String getDlHeader()
  {
    return "Equip Type"
      + ",\"Serial #\""
      + (isMultiClient() ? ", Client" : "")
      + ",\"Batch #\""
      + ",\"Batch Date\""
      + ",\"Ship Date\""
      + ", Reason"
      + ",\"ACR #\"";
  }

  public String getDlNext()
  {
    try
    {
      ResultSet rs = db.getDlRs();
      if (rs.next())
      {
        StringBuffer buf = new StringBuffer();
        buf.append("\"" + rs.getString("pt_name") + "\"");
        buf.append(",\"" + rs.getString("serial_num") + "\"");
        if (isMultiClient())
        {
          buf.append(",\"" + rs.getString("client_name") + "\"");
        }
        buf.append(",\"" + rs.getString("batch_num") + "\"");
        Date bDate = Tmg.toDate(rs.getTimestamp("batch_ts"));
        buf.append(",\"" + db.formatDlDate(bDate) + "\"");
        Date sDate = Tmg.toDate(rs.getTimestamp("ship_ts"));
        buf.append(",\"" + db.formatDlDate(sDate) + "\"");
        buf.append(",\"" + rs.getString("repair_reason") + "\"");
        buf.append(",\"" + rs.getLong("acr_id") + "\"");
        return buf.toString();
      }
    }
    catch (Exception e)
    {
      log.error("Error getting next download row: " + e);
      e.printStackTrace();
    }
    return null;
  }

  public void finishDownload()
  {
    db.finishManualStatement();
  }

  public String getDlFilename()
  {
    return "maintenancereport";
  }
}
