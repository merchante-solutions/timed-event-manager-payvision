package com.mes.tmg.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.Action;
import com.mes.mvc.Downloadable;
import com.mes.tmg.Client;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.ClientTable;

public class OnHandReportBean extends TmgViewBean implements Downloadable
{
  static Logger log = Logger.getLogger(OnHandReportBean.class);

  public static final String FN_DATE          = "date";
  public static final String FN_CLIENT_ID     = "clientId";
  public static final String FN_SUBMIT_BTN    = "submitBtn";

  private List reportRows;

  public static final String HDR_DESCRIPTION  = "Description";
  public static final String HDR_NEW          = "New";
  public static final String HDR_USED         = "Used";
  public static final String HDR_REFURBISHED  = "Refurbished";
  public static final String HDR_TOTAL        = "Total";

  public OnHandReportBean(TmgAction action)
  {
    super(action,"OnHandReportBean");
    putOrderHeader(HDR_DESCRIPTION, "description");
    putOrderHeader(HDR_NEW,         "new");
    putOrderHeader(HDR_USED,        "used");
    putOrderHeader(HDR_REFURBISHED, "refurbished");
    putOrderHeader(HDR_TOTAL,       "total");
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new DateStringField(FN_DATE,"Date",false,false));
      Client client = getClient();
      if (client != null && client.isMes())
      {
        fields.add(new DropDownField(FN_CLIENT_ID,new ClientTable(db,TmgDropDownTable.OPT_SEL_ONE),false));
      }
      else
      {
        String clientId = (client != null ? ""+client.getClientId() : "0" );
        fields.add(new HiddenField(FN_CLIENT_ID,clientId));
      }
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public void setAction(Action action, String requestName)
  {
    Field clientField = getField(FN_CLIENT_ID);
    if (clientField != null)
    {
      Client client = getClient();
      if (client != null && client.isMes() && 
          clientField instanceof HiddenField)
      {
        fields.deleteField(FN_CLIENT_ID);
        fields.add(new DropDownField(FN_CLIENT_ID,
          new ClientTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
        reportRows = null;
      }
      else if ((client == null || !client.isMes()) && 
               clientField instanceof DropDownField)
      {
        fields.deleteField(FN_CLIENT_ID);
        String clientId = (client != null ? ""+client.getClientId() : "0" );
        fields.add(new HiddenField(FN_CLIENT_ID,clientId));
        reportRows = null;
      }
    }
    super.setAction(action,requestName);
  }
    

  protected boolean showFeedback()
  {
    return false;
  }

  public List getRows()
  {
    if (reportRows == null || (isAutoSubmit() && !isAutoValid()))
    {
      reportRows = new ArrayList();
    }
    return reportRows;
  }

  private boolean loadRows()
  {
    try
    {
      Date theDate = ((DateStringField)getField(FN_DATE)).getUtilDate();
      long clientId = getField(FN_CLIENT_ID).asLong();
      reportRows = db.getOhReportRows(theDate,clientId,
        getOrderName("description"),getOrderDirFlag(false));
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoSubmit()
  {
    setData(FN_SUBMIT_BTN,"");
    return loadRows();
  }

  protected void notifyNewOrder(boolean newOrderOk)
  {
    if (newOrderOk)
    {
      loadRows();
    }
  }

  protected boolean autoLoad()
  {
    try
    {
      DateStringField dateField = (DateStringField)fields.getField(FN_DATE);
      if (dateField.isBlank())
      {
        Calendar cal = Calendar.getInstance();
        dateField.setUtilDate(cal.getTime());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Download support
   */
  
  public void startDownload()
  {
    Date theDate = ((DateStringField)getField(FN_DATE)).getUtilDate();
    long clientId = getField(FN_CLIENT_ID).asLong();
    db.startOhReportDownload(theDate,clientId,getOrderName("description"),
      getOrderDirFlag(false));
  }

  public String getDlHeader()
  {
    return "Description"
      + ", New"
      + ", Used"
      + ", Refurbished"
      + ", Total";
  }

  public String getDlNext()
  {
    try
    {
      ResultSet rs = db.getDlRs();
      if (rs.next())
      {
        StringBuffer buf = new StringBuffer();
        buf.append("\"" + rs.getString("description") + "\"");
        buf.append(",\"" + rs.getString("new") + "\"");
        buf.append(",\"" + rs.getString("used") + "\"");
        buf.append(",\"" + rs.getString("refurbished") + "\"");
        buf.append(",\"" + rs.getString("total") + "\"");
        return buf.toString();
      }
    }
    catch (Exception e)
    {
      log.error("Error getting next download row: " + e);
      e.printStackTrace();
    }
    return null;
  }

  public void finishDownload()
  {
    db.finishManualStatement();
  }

  public String getDlFilename()
  {
    return "onhandreport";
  }
}
