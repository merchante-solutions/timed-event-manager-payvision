package com.mes.tmg;

public class VendorClass
{
  private long    vpcId;
  private long    vendorId;
  private String  vpcCode;
  private String  vpcName;

  public void setVpcId(long vpcId)
  {
    this.vpcId = vpcId;
  }
  public long getVpcId()
  {
    return vpcId;
  }

  public void setVendorId(long vendorId)
  {
    this.vendorId = vendorId;
  }
  public long getVendorId()
  {
    return vendorId;
  }

  public void setVpcCode(String vpcCode)
  {
    this.vpcCode = vpcCode;
  }
  public String getVpcCode()
  {
    return vpcCode;
  }

  public void setVpcName(String vpcName)
  {
    this.vpcName = vpcName;
  }
  public String getVpcName()
  {
    return vpcName;
  }

  public String toString()
  {
    return "VendorClass ["
      + " id: " + vpcId
      + ", vendor: " + vendorId
      + ", code: " + vpcCode
      + ", name: " + vpcName
      + " ]";
  }
}
