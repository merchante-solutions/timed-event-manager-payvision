package com.mes.tmg;

public class CallTagPartStatus extends TmgBean
{
  public static String CTPS_NEW           = "NEW";
  public static String CTPS_PENDING       = "PENDING";
  public static String CTPS_RECEIVED      = "RECEIVED";
  public static String CTPS_FAILED        = "FAILED";
  public static String CTPS_NEW_CANCELED  = "NEW_CANCELED";
  public static String CTPS_PEND_CANCELED = "PEND_CANCELED";
  public static String CTPS_CHARGED       = "CHARGED";

  private long ctpStatId;
  private String ctpStatCode;
  private String description;

  public void setCtpStatId(long ctpStatId)
  {
    this.ctpStatId = ctpStatId;
  }
  public long getCtpStatId()
  {
    return ctpStatId;
  }

  public void setCtpStatCode(String ctpStatCode)
  {
    this.ctpStatCode = ctpStatCode;
  }
  public String getCtpStatCode()
  {
    return ctpStatCode;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public String toString()
  {
    return "CallTagPartStatus ["
      + " id: " + ctpStatId
      + ", code: " + ctpStatCode
      + ", desc: " + description
      + " ]";
  }
}