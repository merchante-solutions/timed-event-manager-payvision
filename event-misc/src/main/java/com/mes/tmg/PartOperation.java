package com.mes.tmg;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class PartOperation
{
  private long        opId;
  private String      opCode;
  private long        tranId;
  private Date        opDate;
  private String      opUser;
  private PartOpCode  poc;

  public PartOperation()
  {
    opDate = Calendar.getInstance().getTime();
  }

  public void setOpId(long opId)
  {
    this.opId = opId;
  }
  public long getOpId()
  {
    return opId;
  }

  public void setOpCode(String opCode)
  {
    this.opCode = opCode;
  }
  public String getOpCode()
  {
    return opCode;
  }

  public void setPoc(PartOpCode poc)
  {
    this.poc = poc;
  }
  public PartOpCode getPoc()
  {
    return poc;
  }

  public void setTranId(long tranId)
  {
    this.tranId = tranId;
  }
  public long getTranId()
  {
    return tranId;
  }

  public void setOpDate(Date opDate)
  {
    this.opDate = opDate;
  }
  public Date getOpDate()
  {
    return opDate;
  }
  public void setOpTs(Timestamp opTs)
  {
    opDate = opTs != null ? new Date(opTs.getTime()) : null;
  }
  public Timestamp getOpTs()
  {
    return opDate != null ? new Timestamp(opDate.getTime()) : null;
  }

  public void setOpUser(String opUser)
  {
    this.opUser = opUser;
  }
  public String getOpUser()
  {
    return opUser;
  }

  public String toString()
  {
    DateFormat df 
      = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,DateFormat.SHORT);

    return "PartOperation [ "
      + "id: " + opId
      + ", code: " + opCode
      + ", tran: " + tranId
      + ", date: " + df.format(opDate)
      + ", user: " + opUser
      + " ]";
  }
}