package com.mes.tmg;

public class AcrRef extends TmgBean
{
  private long    acrId;
  private long    appSeqNum;
  private long    acrDefId;
  private String  status;
  private String  merchNum;

  public void setAcrId(long acrId)
  {
    this.acrId = acrId;
  }
  public long getAcrId()
  {
    return acrId;
  }
  // acr seq num maps to acr id
  public void setAcrSeqNum(long acrSeqNum)
  {
    setAcrId(acrSeqNum);
  }
  public long getAcrSeqNum()
  {
    return getAcrId();
  }

  public void setAppSeqNum(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
  }
  public long getAppSeqNum()
  {
    return appSeqNum;
  }

  public void setAcrDefId(long acrDefId)
  {
    this.acrDefId = acrDefId;
  }
  public long getAcrDefId()
  {
    return acrDefId;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }
  // map merch_number to merchNum
  public void setMerchNumber(String merchNumber)
  {
    setMerchNum(merchNumber);
  }
  public String getMerchNumber()
  {
    return getMerchNum();
  }

  public void setStatus(String status)
  {
    this.status = status;
  }
  public String getStatus()
  {
    return status;
  }

  public String toString()
  {
    return "AcrRef ["
      + " acr id: " + acrId
      + ", app seq num: " + appSeqNum
      + ", acr def id: " + acrDefId
      + ", status: " + status
      + ", merch num: " + merchNum
      + " ]";
  }
}