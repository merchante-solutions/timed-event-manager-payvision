package com.mes.tmg;

import java.util.Date;

public class MaintReqsReportRow extends TmgBean
{
  private long    acrId;
  private Date    requestDate;
  private String  userName;
  private String  clientName;
  private String  deployType;
  private String  acrReason;
  private String  merchNum;
  private String  merchName;
  private long    partId;
  private String  serialNum;
  private String  partDescription;
  private String  orderStatus;
  private String  callTagStatus;
  private String  testResult;
  private String  repairStatus;

  public void setAcrId(long acrId)
  {
    this.acrId = acrId;
  }
  public long getAcrId()
  {
    return acrId;
  }

  public void setRequestDate(Date requestDate)
  {
    this.requestDate = requestDate;
  }
  public Date getRequestDate()
  {
    return requestDate;
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setClientName(String clientName)
  {
    this.clientName = clientName;
  }
  public String getClientName()
  {
    return clientName;
  }

  public void setDeployType(String deployType)
  {
    this.deployType = deployType;
  }
  public String getDeployType()
  {
    return deployType;
  }

  public void setAcrReason(String acrReason)
  {
    this.acrReason = acrReason;
  }
  public String getAcrReason()
  {
    return acrReason;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return merchName;
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }

  public void setPartDescription(String partDescription)
  {
    this.partDescription = partDescription;
  }
  public String getPartDescription()
  {
    return partDescription;
  }

  public void setOrderStatus(String orderStatus)
  {
    this.orderStatus = orderStatus;
  }
  public String getOrderStatus()
  {
    return orderStatus;
  }

  public void setCallTagStatus(String callTagStatus)
  {
    this.callTagStatus = callTagStatus;
  }
  public String getCallTagStatus()
  {
    return callTagStatus;
  }

  public void setTestResult(String testResult)
  {
    this.testResult = testResult;
  }
  public String getTestResult()
  {
    return testResult;
  }

  public void setRepairStatus(String repairStatus)
  {
    this.repairStatus = repairStatus;
  }
  public String getRepairStatus()
  {
    return repairStatus;
  }

  public String toString()
  {
    return "MaintReqsReportRow [ "
      + "acr id: " + acrId
      + ", client: " + clientName
      + ", request date: " + requestDate
      + ", user:" + userName
      + ", deploy type: " + deployType
      + ", acr reason: " + acrReason
      + ", merch: " + merchName
      + ", merch num: " + merchNum
      + ", s/n: " + serialNum
      + ", part: " + partDescription
      + ", order status: " + orderStatus
      + ", call tag status: " + callTagStatus
      + ", test result: " + testResult
      + ", repair status: " + repairStatus
      + " ]";
  }
}