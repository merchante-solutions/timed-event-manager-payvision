package com.mes.tmg;

public class Location
{
  private long    locId;
  private String  locCode;
  private String  locName;

  public void setLocId(long locId)
  {
    this.locId = locId;
  }
  public long getLocId()
  {
    return locId;
  }
  
  public void setLocCode(String locCode)
  {
    this.locCode = locCode;
  }
  public String getLocCode()
  {
    return locCode;
  }
  
  public void setLocName(String locName)
  {
    this.locName = locName;
  }
  public String getLocName()
  {
    return locName;
  }
  
  public String toString()
  {
    return "Location [ "
      + "id: " + locId 
      + ", code: " + locCode
      + ", name: " + locName 
      + " ]";
  }
}