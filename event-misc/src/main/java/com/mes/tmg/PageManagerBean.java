package com.mes.tmg;

import com.mes.forms.DropDownField;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.tools.DropDownTable;

public class PageManagerBean
{
  public static final String FN_PAGE_SIZE     = "pageSize";
  public static final String FN_PAGE_NUM      = "pageNum";
  public static final String FN_ROW_COUNT     = "rowCount";

  public static final String AN_NEW_PAGE_NUM  = "actionNewPage";

  private FieldBean bean;
  private int       maxRange = 8;

  protected class PageSizeTable extends DropDownTable
  {
    public PageSizeTable()
    {
      addElement("20","20");
      addElement("50","50");
      addElement("100","100");
      addElement("200","200");
    }
  }

  public void createPageFields(FieldBean bean)
  {
    this.bean = bean;
    bean.getFields().add(new DropDownField(FN_PAGE_SIZE,"Page Size",new PageSizeTable(),true));
    bean.getFields().add(new HiddenField(FN_PAGE_NUM,"1"));
    bean.getFields().add(new HiddenField(FN_ROW_COUNT));
  }

  public void setMaxRange(int maxRange)
  {
    this.maxRange = maxRange;
  }

  public void setRowCount(int rowCount)
  {
    bean.setData(FN_ROW_COUNT,String.valueOf(rowCount));
    checkPageNum();
  }
  public int getRowCount()
  {
    return bean.getField(FN_ROW_COUNT).asInteger();
  }

  public int getPageNum()
  {
    return bean.getField(FN_PAGE_NUM).asInteger();
  }

  public int getPageSize()
  {
    return bean.getField(FN_PAGE_SIZE).asInteger();
  }

  public int getPageCount()
  {
    int rowCount = getRowCount();
    int pageSize = getPageSize();
    return (rowCount / pageSize) + (rowCount % pageSize > 0 ? 1 : 0);
  }

  public int getStartRowNum()
  {
    return (getPageSize() * (getPageNum() - 1)) + 1;
  }

  public void checkPageNum()
  {
    int pageNum = getPageNum();
    int pageCount = getPageCount();
    if (pageNum < 1)
    {
      bean.setData(FN_PAGE_NUM,"1");
    }
    else if (pageNum > pageCount)
    {
      bean.setData(FN_PAGE_NUM,String.valueOf(pageCount));
    }
  }

  public String renderPageLinks(String actionUrlBase)
  {
    StringBuffer buf = new StringBuffer();

    int pageNum     = getPageNum();
    int pageCount   = getPageCount();
    int prevPageNum = pageNum > 1 ? pageNum - 1 : 0;
    int nextPageNum = pageNum < pageCount ? pageNum + 1 : 0;

    boolean isFirstArg = actionUrlBase.indexOf("?") == -1;
    String actionUrl = actionUrlBase + (isFirstArg ? "?" : "&") 
      + AN_NEW_PAGE_NUM + "=1&" + FN_PAGE_NUM + "=";

    // current page indicator
    buf.append("        <span class=\"current\">" + pageNum + "</span>\n");

    int rangeCnt = 0;
    int lowPg = pageNum - 1;
    int highPg = pageNum + 1;
    while (rangeCnt < maxRange && (lowPg >= 1 || highPg <= pageCount))
    {
      if (lowPg >= 1)
      {
        buf.insert(0,"        <a href=\"" + actionUrl + lowPg + "\">" + lowPg + "</a>\n");
        --lowPg;
        ++rangeCnt;
      }
      if (highPg <= pageCount && rangeCnt < maxRange)
      {
        buf.append("        <a href=\"" + actionUrl + highPg + "\">" + highPg + "</a>\n");
        ++highPg;
        ++rangeCnt;
      }
    }

    // add page 1 link if needed (use ellipses if low page higher than 2)
    if (lowPg > 1)
    {
      buf.insert(0,"        <span>...</span>\n");
    }
    if (lowPg > 0)
    {
      buf.insert(0,"        <a href=\"" + actionUrl + "1\">1</a>\n");
    }

    // add last page link if needed 
    // (use ellipses if high page lower than last page - 1)
    if (highPg < pageCount)
    {
      buf.append("        <span>...</span>\n");
    }
    if (highPg <= pageCount)
    {
      buf.append("        <a href=\"" + actionUrl + pageCount + "\">" 
        + pageCount + "</a>\n");
    }
      
    // insert previous page link
    String prevPageText = "&#171; Previous";
    if (prevPageNum > 0)
    {
      buf.insert(0,"        <a href=\"" + actionUrl + prevPageNum + "\">"
        + prevPageText + "</a>\n");
    }
    else
    {
      buf.insert(0,"        <span class=\"nextprev\">" + prevPageText + "</span>\n");
    }

    // append next page link
    String nextPageText = "Next &#187;";
    if (nextPageNum > 0)
    {
      buf.append("        <a href=\"" + actionUrl + nextPageNum + "\">"
        + nextPageText + "</a>\n");
    }
    else
    {
      buf.append("        <span class=\"nextprev\">" + nextPageText + "</span>\n");
    }

    // wrapper div and table
    buf.insert(0,"<div class=\"pageFrame\">\n"
                    +"  <table class=\"pages\">\n    <tr>\n      <td>\n");
    buf.append("      </td>\n    </tr>\n  </table>\n</div>\n");

    return buf.toString();
  }
}
