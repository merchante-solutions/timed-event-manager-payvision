package com.mes.tmg;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PartType
{
  private long    ptId;
  private String  ptName;
  private String  modelCode;
  private String  description;
  private String  snFlag;
  private String  encryptFlag;
  private List    featureMaps;

  public boolean equals(Object o)
  {
    if (o instanceof PartType)
    {
      PartType that = (PartType)o;
      return that.ptId == this.ptId;
    }
    return false;
  }
  
  public void setPtId(long ptId)
  {
    this.ptId = ptId;
  }
  public long getPtId()
  {
    return ptId;
  }
  
  public void setPtName(String ptName)
  {
    this.ptName = ptName;
  }
  public String getPtName()
  {
    return ptName;
  }

  public void setModelCode(String modelCode)
  {
    this.modelCode = modelCode;
  }
  public String getModelCode()
  {
    return modelCode;
  }
  
  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public void setSnFlag(String snFlag)
  {
    if (!snFlag.equals("y") && !snFlag.equals("n"))
    {
      throw new RuntimeException("Invalid s/n flag value: '" + snFlag + "'");
    }
    this.snFlag = snFlag;
  }
  public String getSnFlag()
  {
    return snFlag != null ? snFlag : "n";
  }
  public boolean hasSn()
  {
    return snFlag != null && snFlag.equals("y");
  }

  public void setEncryptFlag(String encryptFlag)
  {
    if (!encryptFlag.equals("y") && !encryptFlag.equals("n"))
    {
      throw new RuntimeException("Invalid s/n flag value: '" + encryptFlag + "'");
    }
    this.encryptFlag = encryptFlag;
  }
  public String getEncryptFlag()
  {
    return encryptFlag != null ? encryptFlag : "n";
  }
  public boolean hasEncrypt()
  {
    return encryptFlag != null && encryptFlag.equals("y");
  }

  public void setFeatureMaps(List featureMaps)
  {
    this.featureMaps = featureMaps;
  }
  public List getFeatureMaps()
  {
    return featureMaps;
  }

  public List getFeatures(String ftCode)
  {
    List features = new ArrayList();
    for (Iterator i = featureMaps.iterator(); i.hasNext();)
    {
      PartFeatureMap featMap = (PartFeatureMap)i.next();
      if (ftCode == null || featMap.getFtCode().equals(ftCode))
      {
        features.add(featMap.getFeature());
      }
    }
    return features;
  }

  public String getCategoryName()
  {
    List l = getFeatures("CATEGORY");
    if (l.size() > 0)
    {
      return ((PartFeature)l.get(0)).getFeatName();
    }
    return null;
  }

  public String getManufacturerName()
  {
    List l = getFeatures("MANUFACTURER");
    if (l.size() > 0)
    {
      return ((PartFeature)l.get(0)).getFeatName();
    }
    return null;
  }

  public String toString()
  {
    return "PartType ["
      + " id: " + ptId 
      + ", name: " + ptName 
      + ", model: " + modelCode
      + ", description: " + description 
      + ", has s/n: " + snFlag
      + ", encryptable: " + encryptFlag
      + " ]";
  }
}