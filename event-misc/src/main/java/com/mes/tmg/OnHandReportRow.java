package com.mes.tmg;

public class OnHandReportRow extends TmgBean
{
  private String description;
  private int newCount;
  private int used;
  private int refurbished;
  private int total;

  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public void setNew(int newCount)
  {
    this.newCount = newCount;
  }
  public int getNew()
  {
    return newCount;
  }

  public void setUsed(int used)
  {
    this.used = used;
  }
  public int getUsed()
  {
    return used;
  }

  public void setRefurbished(int refurbished)
  {
    this.refurbished = refurbished;
  }
  public int getRefurbished()
  {
    return refurbished;
  }

  public void setTotal(int total)
  {
    this.total = total;
  }
  public int getTotal()
  {
    return total;
  }

  public String toString()
  {
    return "OnHandReportRow [ "
      + "part type: " + description
      + ", new: " + newCount
      + ", used: " + used
      + ", refurb: " + refurbished
      + ", total: " + total
      + " ]";
  }
}