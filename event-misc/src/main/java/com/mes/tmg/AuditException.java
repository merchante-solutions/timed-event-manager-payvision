package com.mes.tmg;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

public class AuditException extends TmgBean
{
  static Logger log = Logger.getLogger(AuditException.class);

  public static final String RESULT_MISSING   = "MISSING";
  public static final String RESULT_UNKNOWN   = "UNKNOWN";
  public static final String RESULT_ERROR     = "ERROR";
  public static final String RESULT_OWNER     = "OWNER";

  private long      auditExId;
  private long      auditId;
  private String    serialNum;
  private long      partId;
  private String    resultCode;
  private long      auditStateId;
  private PartState auditPartState;
  private long      resolveStateId;
  private PartState resolvePartState;
  private String    resolveCode;
  private String    resolveUser;
  private Date      resolveDate;
  private long      otherExId;

  public void setAuditExId(long auditExId)
  {
    this.auditExId = auditExId;
  }
  public long getAuditExId()
  {
    return auditExId;
  }

  public void setAuditId(long auditId)
  {
    this.auditId = auditId;
  }
  public long getAuditId()
  {
    return auditId;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }

  public void setAuditStateId(long auditStateId)
  {
    this.auditStateId = auditStateId;
  }
  public long getAuditStateId()
  {
    return auditStateId;
  }

  public void setAuditPartState(PartState auditPartState)
  {
    this.auditPartState = auditPartState;
  }
  public PartState getAuditPartState()
  {
    return auditPartState;
  }

  public void setResolveStateId(long resolveStateId)
  {
    this.resolveStateId = resolveStateId;
  }
  public long getResolveStateId()
  {
    return resolveStateId;
  }

  public void setResolvePartState(PartState resolvePartState)
  {
    this.resolvePartState = resolvePartState;
  }
  public PartState getResolvePartState()
  {
    return resolvePartState;
  }

  public void setResultCode(String resultCode)
  {
    this.resultCode = resultCode;
  }
  public String getResultCode()
  {
    return resultCode;
  }

  public void setResolveCode(String resolveCode)
  {
    this.resolveCode = resolveCode;
  }
  public String getResolveCode()
  {
    return resolveCode;
  }

  public List getResolutionOptions()
  {
    List resolveOpts = new ArrayList();
    if (resultCode.equals(RESULT_MISSING))
    {
      resolveOpts.add(AuditResolution.PART_LOST);
      resolveOpts.add(AuditResolution.PART_FOUND);
      resolveOpts.add(AuditResolution.SCAN_ERROR);
    }
    else if (resultCode.equals(RESULT_UNKNOWN))
    {
      resolveOpts.add(AuditResolution.SN_CORRECTION);
      resolveOpts.add(AuditResolution.MISC_ADDITION);
      resolveOpts.add(AuditResolution.SCAN_ERROR);
    }
    else if (resultCode.equals(RESULT_ERROR))
    {
      resolveOpts.add(AuditResolution.DB_CORRECTION);
      resolveOpts.add(AuditResolution.SCAN_ERROR);
    }
    else if (resultCode.equals(RESULT_OWNER))
    {
      resolveOpts.add(AuditResolution.PART_TRANSFER);
      resolveOpts.add(AuditResolution.SCAN_ERROR);
    }
    return resolveOpts;
  }

  public AuditResolution getResolution()
  {
    if (resolveCode != null)
    {
      return AuditResolution.getByName(resolveCode);
    }
    return null;
  }

  public boolean isResolved()
  {
    return resolveCode != null;
  }

  public void setResolveUser(String resolveUser)
  {
    this.resolveUser = resolveUser;
  }
  public String getResolveUser()
  {
    return resolveUser;
  }

  public void setResolveDate(Date resolveDate)
  {
    this.resolveDate = resolveDate;
  }
  public Date getResolveDate()
  {
    return resolveDate;
  }
  public void setResolveTs(Timestamp resolveTs)
  {
    resolveDate = Tmg.toDate(resolveTs);
  }
  public Timestamp getResolveTs()
  {
    return Tmg.toTimestamp(resolveDate);
  }

  public void setOtherExId(long otherExId)
  {
    if (this.otherExId != 0L)
    {
      throw new RuntimeException(
        "Audit exception already associated with other ex id: " + otherExId);
    }
    this.otherExId = otherExId;
  }
  public long getOtherExId()
  { 
    return otherExId;
  }
  public boolean isAssociated()
  {
    return otherExId != 0L;
  }
  // this step is in place to diminish chances of leaving a dangling
  // other exception association...
  public void unassociate()
  {
    otherExId = 0L;
  }

  public void resolve(AuditResolution resolution, String resolveUser, 
    Date resolveDate, long otherExId)
  {
    resolveCode = resolution.getName();
    this.resolveUser = resolveUser;
    this.resolveDate = resolveDate;
    if (otherExId != 0L)
    {
      setOtherExId(otherExId);
    }
  }
  public void resolve(AuditResolution resolution, String resolveUser, 
    Date resolveDate)
  {
    resolve(resolution,resolveUser,resolveDate,0L);
  }

  public void unresolve()
  {
    resolveCode = null;
    resolveUser = null;
    resolveDate = null;
    resolveStateId = 0L;
    if (isAssociated())
    {
      unassociate();
    }
  }

  public String toString()
  {
    return "AuditException ["
      + " ex id: " + auditExId
      + ", audit id: " + auditId
      + ", serial num: " + serialNum
      + ", part id: " + partId
      + ", result: " + resultCode
      + ", audit state: " + auditPartState
      + ", resolve: " + resolveCode
      + ", resolve state: " + resolvePartState
      + ", user: " + resolveUser
      + ", date: " + formatDate(resolveDate)
      + ", other ex: " + otherExId
      + " ]";
  }
}