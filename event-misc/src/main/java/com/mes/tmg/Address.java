package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class Address extends TmgBean
{
  private long    addrId;
  private long    whoId;
  private Date    createDate;
  private String  label;
  private String  addrName;
  private String  addrLine1;
  private String  addrLine2;
  private String  city;
  private String  state;
  private String  zip;

  public Address()
  {
  }

  public void setAddrId(long addrId)
  {
    this.addrId = addrId;
  }
  public long getAddrId()
  {
    return addrId;
  }

  public void setWhoId(long whoId)
  {
    this.whoId = whoId;
  }
  public long getWhoId()
  {
    return whoId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setLabel(String label)
  {
    this.label = label;
  }
  public String getLabel()
  {
    return label;
  }

  public void setAddrName(String addrName)
  {
    this.addrName = addrName;
  }
  public String getAddrName()
  {
    return addrName;
  }

  public void setAddrLine1(String addrLine1)
  {
    this.addrLine1 = addrLine1;
  }
  public String getAddrLine1()
  {
    return addrLine1;
  }

  public void setAddrLine2(String addrLine2)
  {
    this.addrLine2 = addrLine2;
  }
  public String getAddrLine2()
  {
    return addrLine2;
  }

  public void setCity(String city)
  {
    this.city = city;
  }
  public String getCity()
  {
    return city;
  }

  public void setState(String state)
  {
    this.state = state;
  }
  public String getState()
  {
    return state;
  }

  public void setZip(String zip)
  {
    this.zip = zip;
  }
  public String getZip()
  {
    return zip;
  }

  public String getCsz()
  {
    return city + ", " + state + " " + zip;
  }

  public String renderPlainText()
  {
    StringBuffer buf = new StringBuffer();
    if (addrName != null) buf.append(addrName + "\n");
    buf.append(addrLine1);
    if (addrLine2 != null) buf.append("\n" + addrLine2);
    buf.append("\n" + city + ", " + state + " " + zip);
    return buf.toString();
  }

  public String toString()
  {
    return "Address [ "
      + "addr id: " + addrId
      + ", who id: " + whoId
      + ", created: " + formatDate(createDate)
      + (addrName != null ? ", name: " + addrName : "")
      + ", line1: " + addrLine1
      + (addrLine2 != null ? ", line2: " + addrLine2 : "")
      + ", csz: " + city + ", " + state + " " + zip
      + " ]";
  }
}
