package com.mes.tmg;

public class Disposition extends TmgBean
{
  private long    dispId;
  private String  dispCode;
  private String  dispName;
  private String  deployFlag;
  private String  deployName;

  public void setDispId(long dispId)
  {
    this.dispId = dispId;
  }
  public long getDispId()
  {
    return dispId;
  }
  
  public void setDispCode(String dispCode)
  {
    this.dispCode = dispCode;
  }
  public String getDispCode()
  {
    return dispCode;
  }
  
  public void setDispName(String dispName)
  {
    this.dispName = dispName;
  }
  public String getDispName()
  {
    return dispName;
  }

  public void setDeployFlag(String deployFlag)
  {
    validateFlag(deployFlag,"deploy");
    this.deployFlag = deployFlag;
  }
  public String getDeployFlag()
  {
    return flagValue(deployFlag);
  }
  public boolean canDeploy()
  {
    return flagBoolean(deployFlag);
  }

  public void setDeployName(String deployName)
  {
    this.deployName = deployName;
  }
  public String getDeployName()
  {
    return deployName;
  }

  public String toString()
  {
    return "Disposition [ "
      +"id: " + dispId
      +", code: " + dispCode
      + ", name: " + dispName 
      + (canDeploy() ? ", deployed as: " + deployName : "")
      + " ]";
  }
}