package com.mes.tmg;

public class ClientAppMap extends TmgBean
{
  private long clientId;
  private long appType;
  private long invId;
  private Inventory inventory;

  public void setClientId(long clientId)
  {
    this.clientId = clientId;
  }
  public long getClientId()
  {
    return clientId;
  }

  public void setAppType(long appType)
  {
    this.appType = appType;
  }
  public long getAppType()
  {
    return appType;
  }

  public void setInvId(long invId)
  {
    this.invId = invId;
  }
  public long getInvId()
  {
    return invId;
  }

  public void setInventory(Inventory inventory)
  {
    this.inventory = inventory;
  }
  public Inventory getInventory()
  {
    return inventory;
  }

  public String toString()
  {
    return "ClientAppMap [ "
      + "id: " + clientId
      + ", app type: " + appType
      + ", inv: " + invId
      + " ]";
  }
}