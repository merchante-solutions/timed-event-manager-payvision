package com.mes.tmg;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.mes.persist.AbstractFilter;
import com.mes.persist.TableDef;

public class PartStateSnFilter extends AbstractFilter
{
  private String snFragment;

  public PartStateSnFilter(String snFragment)
  {
    this.snFragment = snFragment;
    colName = "serial_num";
  }

  public String getWhereClause(TableDef td, String qualifier)
  {
    return td.getColumnDef("serial_num").getQualifiedName(qualifier) + " = ? ";
    //  + " like '%' || ? || '%' ";
  }

  public String getAuditLog()
  {
    return td.getColumnDef("serial_num").getQualifiedName(qualifier) 
      + " = '" + snFragment + "'";
    //  + " like '%" + snFragment + "%' ";
  }

  public int setMarks(PreparedStatement ps, int mark) throws SQLException
  {
    ps.setString(mark++,snFragment);
    return mark;
  }
}