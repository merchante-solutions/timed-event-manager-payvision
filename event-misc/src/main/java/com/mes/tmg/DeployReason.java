package com.mes.tmg;

public class DeployReason extends TmgBean
{
  public static final String DRC_NEW      = "NEW";
  public static final String DRC_ADD      = "ADD";
  public static final String DRC_SWAP     = "SWAP";
  public static final String DRC_LOAN     = "LOAN";
  public static final String DRC_CLOSE    = "CLOSE";
  public static final String DRC_EXCHANGE = "EXCHANGE";
  public static final String DRC_RETURN   = "RETURN";
  public static final String DRC_CANCEL   = "CANCEL";

  private long drId;
  private String drCode;
  private String description;
  private String startFlag;
  private String endFlag;

  public void setDrId(long drId)
  {
    this.drId = drId;
  }
  public long getDrId()
  {
    return drId;
  }

  public void setDrCode(String drCode)
  {
    this.drCode = drCode;
  }
  public String getDrCode()
  {
    return drCode;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public void setStartFlag(String startFlag)
  {
    validateFlag(startFlag,"start");
    this.startFlag = startFlag;
  }
  public String getStartFlag()
  {
    return flagValue(startFlag);
  }
  public boolean canStart()
  {
    return flagBoolean(startFlag);
  }

  public void setEndFlag(String endFlag)
  {
    validateFlag(endFlag,"end");
    this.endFlag = endFlag;
  }
  public String getEndFlag()
  {
    return flagValue(endFlag);
  }
  public boolean canEnd()
  {
    return flagBoolean(endFlag);
  }

  public String toString()
  {
    return "DeployReason ["
      + " id: " + drId
      + ", code: " + drCode
      + ", desc: " + description
      + ", start: " + startFlag
      + ", end: " + endFlag
      + " ]";
  }
}