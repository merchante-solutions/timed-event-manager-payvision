package com.mes.tmg;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.mes.forms.HtmlBlock;
import com.mes.forms.HtmlRenderable;
import com.mes.mvc.Link;
import com.mes.mvc.RequestHandler;

public class TmgBean
{
  public static final String FLAG_YES   = "y";
  public static final String FLAG_NO    = "n";

  public static DateFormat defaultDf 
    = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,DateFormat.SHORT);

  protected DateFormat df = defaultDf;

  public void setDf(DateFormat df)
  {
    this.df = df;
  }
  public DateFormat getDf()
  {
    return df;
  }

  public String formatDate(Date date)
  {
    return date != null ? df.format(date) : "--";
  }

  public static void validateFlag(String flag, String val1, String val2, 
    String descriptor)
  {
    if (!flag.equals(val1) &&!flag.equals(val2))
    {
      throw new RuntimeException("Invalid " + descriptor 
        + " flag value '" +  flag + "'" 
        + ", must be '" + val1 + "' or '" + val2 + "'");
    }
  }
  public static void validateFlag(String flag, String descriptor)
  {
    validateFlag(flag,FLAG_YES,FLAG_NO,descriptor);
  }
  protected String flagValue(String flag)
  {
    return flag != null ? flag : FLAG_NO;
  }
  protected boolean flagBoolean(String flag)
  {
    return flag != null && flag.equals(FLAG_YES);
  }

  public List getOptionsList(RequestHandler handler)
  {
    return new ArrayList();
  }

  public HtmlRenderable getOptionLinks(RequestHandler handler)
  {
    List mvcLinks = getOptionsList(handler);
    HtmlBlock htmlLinks = new HtmlBlock();
    for (int i = 0; i < mvcLinks.size(); ++i)
    {
      if (i > 0) htmlLinks.add(" ");
      Link mvcLink = (Link)mvcLinks.get(i);
      htmlLinks.add(mvcLink.getHtmlLink());
    }
    return htmlLinks;
  }
}

