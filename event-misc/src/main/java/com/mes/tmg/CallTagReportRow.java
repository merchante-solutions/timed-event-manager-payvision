package com.mes.tmg;

import java.util.Date;

public class CallTagReportRow extends TmgBean
{
  private Date createDate;
  private String clientName;
  private String merchNum;
  private String merchName;
  private String assocNum;
  private String callTagReason;
  private String callTagNum;
  private String serialNum;
  private String description;
  private double unitCost;
  private String callTagStatus;

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }

  public void setClientName(String clientName)
  {
    this.clientName = clientName;
  }
  public String getClientName()
  {
    return clientName;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return merchName;
  }

  public void setAssocNum(String assocNum)
  {
    this.assocNum = assocNum;
  }
  public String getAssocNum()
  {
    return assocNum;
  }

  public void setCallTagReason(String callTagReason)
  {
    this.callTagReason = callTagReason;
  }
  public String getCallTagReason()
  {
    return callTagReason;
  }

  public void setCallTagNum(String callTagNum)
  {
    this.callTagNum = callTagNum;
  }
  public String getCallTagNum()
  {
    return callTagNum;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public void setUnitCost(double unitCost)
  {
    this.unitCost = unitCost;
  }
  public double getUnitCost()
  {
    return unitCost;
  }

  public void setCallTagStatus(String callTagStatus)
  {
    this.callTagStatus = callTagStatus;
  }
  public String getCallTagStatus()
  {
    return callTagStatus;
  }

  public String toString()
  {
    return "CallTagReportRow [ "
      + " date: " + createDate
      + ", client: " + clientName
      + ", merch num: " + merchNum
      + ", assoc: " + assocNum
      + ", ct reas: " + callTagReason
      + ", track num: " + callTagNum
      + ", ser num: " + serialNum
      + ", part: " + description
      + ", cost: " + unitCost
      + ", ct stat: " + callTagStatus
      + " ]";
  }
}