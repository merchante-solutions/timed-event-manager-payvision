package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class Shipment extends TmgBean
{
  private long shipId;
  private long linkId;
  private Date createDate;
  private String userName;
  private long shipperId;
  private long shipTypeId;
  private long addrId;
  private String trackingNum;

  public void setShipId(long shipId)
  {
    this.shipId = shipId;
  }
  public long getShipId()
  {
    return shipId;
  }

  public void setLinkId(long linkId)
  {
    this.linkId = linkId;
  }
  public long getLinkId()
  {
    return linkId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setShipperId(long shipperId)
  {
    this.shipperId = shipperId;
  }
  public long getShipperId()
  {
    return shipperId;
  }

  public void setShipTypeId(long shipTypeId)
  {
    this.shipTypeId = shipTypeId;
  }
  public long getShipTypeId()
  {
    return shipTypeId;
  }

  public void setAddrId(long addrId)
  {
    this.addrId = addrId;
  }
  public long getAddrId()
  {
    return addrId;
  }

  public void setTrackingNum(String trackingNum)
  {
    this.trackingNum = trackingNum;
  }
  public String getTrackingNum()
  {
    return trackingNum;
  }

  public String toString()
  {
    return "Shipment ["
      + "ship id: " + shipId
      + ", link id: " + linkId
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + ", shipper: " + shipperId
      + ", ship type: " + shipTypeId
      + ", addr: " + addrId
      + ", tracking num: " + trackingNum
      + " ]";
  }
}