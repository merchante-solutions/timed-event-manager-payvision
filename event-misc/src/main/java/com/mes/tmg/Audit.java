package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

public class Audit extends TmgBean
{
  static Logger log = Logger.getLogger(Audit.class);
  
  private long      auditId;
  private String    userName;
  private long      clientId;
  private Client    client;
  private Date      createDate;
  private Date      auditDate;
  private Date      finishDate;
  private Document  document;
  private List      auditExceptions;

  public void setAuditId(long auditId)
  {
    this.auditId = auditId;
  }
  public long getAuditId()
  {
    return auditId;
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setClientId(long clientId)
  {
    this.clientId = clientId;
  }
  public long getClientId()
  {
    return clientId;
  }

  public void setClient(Client client)
  {
    this.client = client;
  }
  public Client getClient()
  {
    return client;
  }
  public String getClientName()
  {
    return client != null ? client.getClientName() : "Global";
  }
  public boolean isGlobal()
  {
    return client == null;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setAuditDate(Date auditDate)
  {
    this.auditDate = auditDate;
  }
  public Date getAuditDate()
  {
    return auditDate;
  }
  public void setAuditTs(Timestamp auditTs)
  {
    auditDate = Tmg.toDate(auditTs);
  }
  public Timestamp getAuditTs()
  {
    return Tmg.toTimestamp(auditDate);
  }

  public void setFinishDate(Date finishDate)
  {
    this.finishDate = finishDate;
  }
  public Date getFinishDate()
  {
    return finishDate;
  }
  public void setFinishTs(Timestamp finalizedTs)
  {
    finishDate = Tmg.toDate(finalizedTs);
  }
  public Date getFinishTs()
  {
    return Tmg.toTimestamp(finishDate);
  }

  public void setDocument(Document document)
  {
    this.document = document;
  }
  public Document getDocument()
  {
    return document;
  }

  public void setAuditExceptions(List auditExceptions)
  {
    this.auditExceptions = auditExceptions;
  }
  public List getAuditExceptions()
  {
    return auditExceptions;
  }

  public boolean canComplete()
  {
    boolean canComplete = true;
    for (Iterator i = auditExceptions.iterator(); i.hasNext();)
    {
      AuditException audEx = (AuditException)i.next();
      if (!audEx.isResolved())
      {
        canComplete = false;
        break;
      }
    }
    return canComplete;
  }

  public boolean isComplete()
  {
    return finishDate != null;
  }

  public String toString()
  {
    return "Audit ["
      + " audit id: " + auditId
      + ", user: " + userName
      + (client != null ? ", client: " + client.getClientName() : "")
      + (document != null ? ", document: " + document : "")
      + ", created: " + formatDate(createDate)
      + ", audit date: " + formatDate(auditDate)
      + ", finished: " + formatDate(finishDate)
      + " ]";
  }
}