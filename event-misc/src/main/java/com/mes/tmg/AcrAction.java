package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class AcrAction extends TmgBean
{
  private long            aaId;
  private Date            createDate;
  private String          userName;
  private long            acrId;
  private long            ordId;
  private long            ctPartId;
  private String          serialNum;

  public void setAaId(long aaId)
  {
    this.aaId = aaId;
  }
  public long getAaId()
  {
    return aaId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setAcrId(long acrId)
  {
    this.acrId = acrId;
  }
  public long getAcrId()
  {
    return acrId;
  }

  public void setOrdId(long ordId)
  {
    this.ordId = ordId;
  }
  public long getOrdId()
  {
    return ordId;
  }

  public void setCtPartId(long ctPartId)
  {
    this.ctPartId = ctPartId;
  }
  public long getCtPartId()
  {
    return ctPartId;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }

  public String toString()
  {
    return "AcrAction ["
      + " id: " + aaId
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + ", acr id: " + acrId
      + ", ord id: " + ordId
      + ", ct part: " + ctPartId
      + (serialNum != null ? ", s/n: " + serialNum : "")
      + " ]";
  }
}