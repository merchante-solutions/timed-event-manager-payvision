package com.mes.tmg;

import org.apache.log4j.Logger;
import com.mes.persist.Db;
import com.mes.persist.mes.MesTransaction;
import com.mes.persist.mes.MesUser;
import com.mes.user.UserBean;

public abstract class TmgTransaction extends MesTransaction
{
  static Logger log = Logger.getLogger(TmgTransaction.class);

  /**
   * Constructor.  Wraps user bean in an MesUser object, calls super.
   */
  public TmgTransaction(Db db, UserBean user)
  {
    super(db,new MesUser(user));
  }
}
