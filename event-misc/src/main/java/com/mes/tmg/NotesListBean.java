package com.mes.tmg;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.HiddenField;

public class NotesListBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(NotesListBean.class);

  public static final String FN_EXPAND_NOTES  = "expandNotes";
  public static final String FN_NOTE_ITEM_ID  = "noteItemId";

  private List notes;

  public NotesListBean(TmgAction action)
  {
    super(action,"notesBean");
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_EXPAND_NOTES));
      fields.add(new HiddenField(FN_NOTE_ITEM_ID));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public boolean autoLoad()
  {
    try
    {
      long noteItemId = getField(FN_NOTE_ITEM_ID).asLong();
      if (noteItemId == 0L)
      {
        String idStr = (String)request.getAttribute("noteItemId");
        noteItemId = Long.parseLong(idStr);
        setData(FN_NOTE_ITEM_ID,idStr);
      }
      notes = db.getNotesForId(noteItemId);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoLoad: " + e);
    }
    return false;
  }

  public List getRows()
  {
    return notes;
  }

  public boolean isExpanded()
  {
    return true;
    //return getData(FN_EXPAND_NOTES).equals("true");
  }
}