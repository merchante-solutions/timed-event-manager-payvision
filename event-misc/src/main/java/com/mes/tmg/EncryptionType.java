package com.mes.tmg;

public class EncryptionType
{
  private long    etId;
  private String  etCode;
  private String  etName;
  
  public void setEtId(long etId)
  {
    this.etId = etId;
  }
  public long getEtId()
  {
    return etId;
  }

  public void setEtCode(String etCode)
  {
    this.etCode = etCode;
  }
  public String getEtCode()
  {
    return etCode;
  }
  
  public void setEtName(String etName)
  {
    this.etName = etName;
  }
  public String getEtName()
  {
    return etName;
  }
  
  public String toString()
  {
    return "EncryptionType [ id: " + etId + ", type: " + etCode 
      + ", name: " + etName + " ]";
  }
}