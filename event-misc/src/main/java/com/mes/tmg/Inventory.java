package com.mes.tmg;

public class Inventory extends TmgBean
{
  private long invId;
  private String invName;
  private long clientId;
  private String mesFlag;

  public boolean equals(Object other)
  {
    if (other instanceof Inventory)
    {
      Inventory that = (Inventory)other;
      return that.invId == this.invId;
    }
    return false;
  }

  public void setInvId(long invId)
  {
    this.invId = invId;
  }
  public long getInvId()
  {
    return invId;
  }
  
  public void setInvName(String invName)
  {
    this.invName = invName;
  }
  public String getInvName()
  {
    return invName;
  }

  public void setClientId(long clientId)
  {
    this.clientId = clientId;
  }
  public long getClientId()
  {
    return clientId;
  }

  public void setMesFlag(String mesFlag)
  {
    validateFlag(mesFlag,"MES owned");
    this.mesFlag = mesFlag;
  }
  public String getMesFlag()
  {
    return flagValue(mesFlag);
  }
  public boolean isMesOwned()
  {
    return flagBoolean(mesFlag);
  }
  
  public String toString()
  {
    return "Inventory [ "
      + "id: " + invId
      + ", name: " + invName
      + ", client: " + clientId
      + (isMesOwned() ? ", MES owned" : "")
      + " ]";
  }
}