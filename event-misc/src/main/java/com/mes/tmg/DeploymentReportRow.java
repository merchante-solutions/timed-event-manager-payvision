package com.mes.tmg;

import java.util.Date;

public class DeploymentReportRow extends TmgBean
{
  private String  clientName;
  private String  sourceInventory;
  private Date    receiveDate;
  private Date    deployDate;
  private String  deployType;
  private String  merchName;
  private String  merchNum;
  private String  assocNum;
  private String  dispName;
  private String  serialNum;
  private String  partCategory;
  private String  partDescription;
  private String  partCondition;
  private long    partId;


  public void setClientName(String clientName)
  {
    this.clientName = clientName;
  }
  public String getClientName()
  {
    return clientName;
  }

  public void setSourceInventory(String sourceInventory)
  {
    this.sourceInventory = sourceInventory;
  }
  public String getSourceInventory()
  {
    return sourceInventory;
  }

  public void setReceiveDate(Date receiveDate)
  {
    this.receiveDate = receiveDate;
  }
  public Date getReceiveDate()
  {
    return receiveDate;
  }

  public void setDeployDate(Date deployDate)
  {
    this.deployDate = deployDate;
  }
  public Date getDeployDate()
  {
    return deployDate;
  }

  public void setDeployType(String deployType)
  {
    this.deployType = deployType;
  }
  public String getDeployType()
  {
    return deployType;
  }

  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return merchName;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setAssocNum(String assocNum)
  {
    this.assocNum = assocNum;
  }
  public String getAssocNum()
  {
    return assocNum;
  }

  public void setDispName(String dispName)
  {
    this.dispName = dispName;
  }
  public String getDispName()
  {
    return dispName;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }

  public void setPartCategory(String partCategory)
  {
    this.partCategory = partCategory;
  }
  public String getPartCategory()
  {
    return partCategory;
  }

  public void setPartDescription(String partDescription)
  {
    this.partDescription = partDescription;
  }
  public String getPartDescription()
  {
    return partDescription;
  }

  public void setPartCondition(String partCondition)
  {
    this.partCondition = partCondition;
  }
  public String getPartCondition()
  {
    return partCondition;
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }

  public String toString()
  {
    return "DeploymentReportRow [ "
      + "client: " + clientName
      + ", src inv: " + sourceInventory
      + ", rcvd: " + receiveDate
      + ", deployed: " + deployDate
      + ", reason: " + deployType
      + ", merch: " + merchName
      + ", merch num: " + merchNum
      + ", assoc: " + assocNum
      + ", terms: " + dispName
      + ", serial num: " + serialNum
      + ", part cat: " + partCategory
      + ", part desc: " + partDescription
      + ", part cond: " + partCondition
      + ", part id: " + partId
      + " ]";
  }
}