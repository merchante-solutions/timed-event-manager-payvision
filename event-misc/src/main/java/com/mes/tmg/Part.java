package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class Part extends TmgBean
{
  public static final String  SC_IN           = "IN";
  public static final String  SC_OUT          = "OUT";
  public static final String  SC_MAINTENANCE  = "MAINTENANCE";
  public static final String  SC_BUSY         = "BUSY";
  public static final String  SC_REMOVED      = "REMOVED";

  public static final String  CC_GOOD         = "GOOD";
  public static final String  CC_BROKEN       = "BROKEN";
  public static final String  CC_DEAD         = "DEAD";
  public static final String  CC_OBSOLETE     = "OBSOLETE";

  public static final String  DC_NOT_DEP      = "NOT_DEP";
  public static final String  DC_RENT         = "RENT";
  public static final String  DC_SOLD         = "SOLD";
  public static final String  DC_LOAN         = "LOAN";
  public static final String  DC_SOLD_AS_IS   = "SOLD_AS_IS";
  public static final String  DC_INT_LOAN     = "INT_LOAN";
  public static final String  DC_MOE          = "MOE";

  public static final String  LC_INJECT       = "INJECT";
  public static final String  LC_INT_LOAN     = "INT_LOAN";
  public static final String  LC_MERCHANT     = "MERCHANT";
  public static final String  LC_REPAIR       = "REPAIR";
  public static final String  LC_WAREHOUSE    = "WAREHOUSE";
  public static final String  LC_GONE         = "GONE";
  public static final String  LC_TESTING      = "TESTING";
  public static final String  LC_NEED_INJECT  = "NEED_INJECT";

  private long                partId;
  private long                buckId;
  private long                ptId;
  private String              serialNum;
  private int                 count;
  private String              pcCode;
  private String              origPcCode;
  private String              locCode;
  private String              statusCode;
  private String              condCode;
  private String              dispCode;
  private long                etId;
  private String              lostFlag;
  private String              leaseFlag;
  private long                poItemId;
  private long                poPartId;
  private long                poId;
  private long                deployId;
  private long                maId;
  private long                maPartId;
  private long                testId;

  private String              ownerMerchNum;

  private Timestamp           receiveTs;
  private Timestamp           maReceiveTs;

  private Bucket              bucket;
  private Inventory           inventory;
  private Client              client;
  private PartType            partType;
  private PartClass           partClass;
  private PartClass           origPartClass;
  private Location            location;
  private PartStatus          partStatus;
  private Condition           condition;
  private Disposition         disposition;
  private EncryptionType      encryptionType;
  private Deployment          deployment;
  private PartPrice           partPrice;
  private PartTest            partTest;

  protected void copyFrom(Part part)
  {
    partId          = part.getPartId();
    buckId          = part.getBuckId();
    ptId            = part.getPtId();
    serialNum       = part.getSerialNum();
    count           = part.getCount();
    pcCode          = part.getPcCode();
    origPcCode      = part.getOrigPcCode();
    locCode         = part.getLocCode();
    statusCode      = part.getStatusCode();
    condCode        = part.getCondCode();
    dispCode        = part.getDispCode();
    etId            = part.getEtId();
    lostFlag        = part.getLostFlag();
    ownerMerchNum   = part.getOwnerMerchNum();
    leaseFlag       = part.getLeaseFlag();
    poItemId        = part.getPoItemId();
    poPartId        = part.getPoPartId();
    poId            = part.getPoId();
    deployId        = part.getDeployId();
    maId            = part.getMaId();
    maPartId        = part.getMaPartId();
    receiveTs       = part.getReceiveTs();
    maReceiveTs     = part.getMaReceiveTs();
    bucket          = part.getBucket();
    inventory       = part.getInventory();
    client          = part.getClient();
    partType        = part.getPartType();
    partClass       = part.getPartClass();
    origPartClass   = part.getOrigPartClass();
    location        = part.getLocation();
    partStatus      = part.getPartStatus();
    condition       = part.getCondition();
    disposition     = part.getDisposition();
    encryptionType  = part.getEncryptionType();
    deployment      = part.getDeployment();
    partPrice       = part.getPartPrice();
    testId          = part.getTestId();
    partTest        = part.getPartTest();
  }

  protected void copyTo(Part part)
  {
    part.copyFrom(this);
  }

  public Part clonePart()
  {
    Part clone = new Part();
    copyTo(clone);
    return clone;
  }

  public boolean equals(Object other)
  {
    if (other instanceof Part)
    {
      Part that = (Part)other;
      return this.partId == that.getPartId();
    }
    return false;
  }

  public boolean stateEquals(Part that)
  {
    return  that != null
            && partId == that.getPartId()
            && buckId == that.getBuckId()
            && ptId == that.getPtId()
            && serialNum.equals(that.getSerialNum())
            && count == that.getCount()
            && pcCode.equals(that.getPcCode())
            && origPcCode.equals(that.getOrigPcCode())
            && locCode.equals(that.getLocCode())
            && statusCode.equals(that.getStatusCode())
            && condCode.equals(that.getCondCode())
            && dispCode.equals(that.getDispCode())
            && etId == that.getEtId()
            && lostFlag.equals(that.getLostFlag())
            && ((ownerMerchNum == null && that.getOwnerMerchNum() == null)
                || ownerMerchNum.equals(that.getOwnerMerchNum()))
            && leaseFlag.equals(that.getLeaseFlag())
            && poItemId == that.getPoItemId()
            && poPartId == that.getPoPartId()
            && poId == that.getPoId()
            && deployId == that.getDeployId()
            && testId == that.getTestId();
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }

  public void setBuckId(long buckId)
  {
    this.buckId = buckId;
  }
  public long getBuckId()
  {
    return buckId;
  }

  public void setBucket(Bucket bucket)
  {
    this.bucket = bucket;
  }
  public Bucket getBucket()
  {
    return bucket;
  }

  public void setInventory(Inventory inventory)
  {
    this.inventory = inventory;
  }
  public Inventory getInventory()
  {
    return inventory;
  }

  public void setClient(Client client)
  {
    this.client = client;
  }
  public Client getClient()
  {
    return client;
  }

  public void setPtId(long ptId)
  {
    this.ptId = ptId;
  }
  public long getPtId()
  {
    return ptId;
  }

  public void setPartType(PartType partType)
  {
    this.partType = partType;
  }
  public PartType getPartType()
  {
    return partType;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }

  public void setCount(int count)
  {
    this.count = count;
  }
  public int getCount()
  {
    return count;
  }

  public void setPcCode(String pcCode)
  {
    this.pcCode = pcCode;
  }
  public String getPcCode()
  {
    return pcCode;
  }

  public void setPartClass(PartClass partClass)
  {
    this.partClass = partClass;
  }
  public PartClass getPartClass()
  {
    return partClass;
  }

  public void setOrigPcCode(String origPcCode)
  {
    this.origPcCode = origPcCode;
  }
  public String getOrigPcCode()
  {
    return origPcCode;
  }

  public void setOrigPartClass(PartClass origPartClass)
  {
    this.origPartClass = origPartClass;
  }
  public PartClass getOrigPartClass()
  {
    return origPartClass;
  }

  public void setLocCode(String locCode)
  {
    this.locCode = locCode;
  }
  public String getLocCode()
  {
    return locCode;
  }

  public void setLocation(Location location)
  {
    this.location = location;
  }
  public Location getLocation()
  {
    return location;
  }

  public void setStatusCode(String statusCode)
  {
    this.statusCode = statusCode;
  }
  public String getStatusCode()
  {
    return statusCode;
  }

  public void setPartStatus(PartStatus partStatus)
  {
    this.partStatus = partStatus;
  }
  public PartStatus getPartStatus()
  {
    return partStatus;
  }

  public void setCondCode(String condCode)
  {
    this.condCode = condCode;
  }
  public String getCondCode()
  {
    return condCode;
  }

  public void setCondition(Condition condition)
  {
    this.condition = condition;
  }
  public Condition getCondition()
  {
    return condition;
  }

  public void setDispCode(String dispCode)
  {
    this.dispCode = dispCode;
  }
  public String getDispCode()
  {
    return dispCode;
  }

  public void setDisposition(Disposition disposition)
  {
    this.disposition = disposition;
  }
  public Disposition getDisposition()
  {
    return disposition;
  }

  public void setEtId(long etId)
  {
    this.etId = etId;
  }
  public long getEtId()
  {
    return etId;
  }

  public void setEncryptionType(EncryptionType encryptionType)
  {
    this.encryptionType = encryptionType;
  }
  public EncryptionType getEncryptionType()
  {
    return encryptionType;
  }

  public void setLostFlag(String lostFlag)
  {
    validateFlag(lostFlag,"lost");
    this.lostFlag = lostFlag;
  }
  public String getLostFlag()
  {
    return flagValue(lostFlag);
  }
  public boolean isLost()
  {
    return flagBoolean(lostFlag);
  }

  public void setOwnerMerchNum(String ownerMerchNum)
  {
    this.ownerMerchNum = ownerMerchNum;
  }
  public String getOwnerMerchNum()
  {
    return ownerMerchNum;
  }

  public void setLeaseFlag(String leaseFlag)
  {
    validateFlag(leaseFlag,"lease");
    this.leaseFlag = leaseFlag;
  }
  public String getLeaseFlag()
  {
    return flagValue(leaseFlag);
  }
  public boolean isLeased()
  {
    return flagBoolean(leaseFlag);
  }

  public void setPoItemId(long poItemId)
  {
    this.poItemId = poItemId;
  }
  public long getPoItemId()
  {
    return poItemId;
  }

  public void setPartPrice(PartPrice partPrice)
  {
    this.partPrice = partPrice;
  }
  public PartPrice getPartPrice()
  {
    return partPrice;
  }

  public void setPoPartId(long poPartId)
  {
    this.poPartId = poPartId;
  }
  public long getPoPartId()
  {
    return poPartId;
  }

  public Date getReceiveDate()
  {
    if (receiveTs != null)
    {
      return Tmg.toDate(receiveTs);
    }
    else if (maReceiveTs != null)
    {
      return Tmg.toDate(maReceiveTs);
    }
    return null;
  }

  public void setMaReceiveTs(Timestamp maReceiveTs)
  {
    this.maReceiveTs = maReceiveTs;
  }
  public Timestamp getMaReceiveTs()
  {
    return maReceiveTs;
  }

  public void setReceiveTs(Timestamp receiveTs)
  {
    this.receiveTs = receiveTs;
  }
  public Timestamp getReceiveTs()
  {
    return receiveTs;
  }

  public void setPoId(long poId)
  {
    this.poId = poId;
  }
  public long getPoId()
  {
    return poId;
  }

  public void setDeployId(long deployId)
  {
    this.deployId = deployId;
  }
  public long getDeployId()
  {
    return deployId;
  }

  public void setDeployment(Deployment deployment)
  {
    this.deployment = deployment;
  }
  public Deployment getDeployment()
  {
    return deployment;
  }

  public void setMaId(long maId)
  {
    this.maId = maId;
  }
  public long getMaId()
  {
    return maId;
  }

  public void setMaPartId(long maPartId)
  {
    this.maPartId = maPartId;
  }
  public long getMaPartId()
  {
    return maPartId;
  }

  public void setTestId(long testId)
  {
    this.testId = testId;
  }
  public long getTestId()
  {
    return testId;
  }

  public void setPartTest(PartTest partTest)
  {
    this.partTest = partTest;
  }
  public PartTest getPartTest()
  {
    return partTest;
  }

  public boolean isObsolete()
  {
    return condCode.equals(CC_OBSOLETE);
  }

  public boolean isSoldAsIs()
  {
    return dispCode.equals(DC_SOLD_AS_IS);
  }

  public boolean canObsolete()
  {
    return !isObsolete() && !isSoldAsIs() && !isLost();
  }

  public boolean canSellAsIs()
  {
    return !isSoldAsIs() && !isObsolete() && !isLost();
  }

  public boolean canLose()
  {
    // should probably change this to be something like isDeployed()
    return !isSoldAsIs() && !isObsolete() && !isLost();
  }

  public boolean isDeployed()
  {
    return deployment != null;
  }

  // is part in stock, not busy and in working condition?
  // used to determine if part is available to add to an order
  public boolean isAvailable()
  {
    return dispCode.equals(DC_NOT_DEP) &&
           condCode.equals(CC_GOOD) &&
           statusCode.equals(SC_IN) &&
           locCode.equals(LC_WAREHOUSE);
  }

  public boolean inTesting()
  {
    return locCode.equals(LC_TESTING);
  }

  // is part in stock, not busy and in NOT in working condition?
  // used to determine if part is available to add to a repair order
  public boolean inNeedOfRepair()
  {
    return true;
    /*
    //Currently turned off so that any item can be added to
    //a maintenance order
    return dispCode.equals(DC_NOT_DEP) &&
           !condCode.equals(CC_GOOD) &&
           statusCode.equals(SC_IN) &&
           locCode.equals(LC_WAREHOUSE);
    */
  }

  // indicates if merchant owned equipment was never part
  public boolean isMerchantOwned()
  {
    return ownerMerchNum != null;
  }

  // indicates if merchant owned equipment was never part
  public boolean isMerchantOriginated()
  {
    return DC_MOE.equals(dispCode);
  }

  public boolean canChangeDisposition()
  {
    return DC_RENT.equals(dispCode) || DC_SOLD.equals(dispCode);
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("Part [");
    buf.append(" id: " + partId );
    buf.append(", inv: " + inventory.getInvName());
    buf.append(", type: " + partType.getPtName());
    buf.append(", s/n: " + serialNum);
    buf.append(", count: " + count);
    buf.append(", class (orig): " + partClass.getPcName() );
    buf.append(" (" + origPartClass.getPcName() + ")");
    buf.append(", location: " + location.getLocName());
    buf.append(", status: " + partStatus.getStatusName());
    buf.append(", disposition: " + disposition.getDispName());
    buf.append(", condition: " + condition.getCondName() + " (" + condCode + ")");
    buf.append((encryptionType != null ? ", enc. type: " + encryptionType.getEtName() : ""));
    buf.append((isLost() ? ", lost" : ""));
    buf.append((isMerchantOwned() ? ", merchant owned: " + ownerMerchNum : ""));
    buf.append((isLeased() ? ", leased" : ""));
    buf.append((receiveTs != null ? ", po id: " + poId : ""));
    buf.append((getReceiveDate() != null ? ", received: " + formatDate(getReceiveDate()) : ""));
    buf.append((deployment != null ? ", " + deployment : ""));
    buf.append((maId != 0L ? ", ma id: " + maId : ""));
    buf.append((maPartId != 0L ? ", ma part id: " + maPartId : ""));
    buf.append((partPrice != null ? ", price: " + partPrice.getPartPrice() : ""));
    buf.append((partTest != null ? ", test: " + partTest.getTestId() : ""));
    buf.append(" ]");
    return buf.toString();
  }

  public String getDescriptor()
  {
    String ptName = partType.getPtName();
    String snDesc = serialNum != null && serialNum.length() > 0
      ? " (s/n " + serialNum + ")" : "";
    return ptName + snDesc;
  }

}