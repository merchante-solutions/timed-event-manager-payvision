package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class TransferPart extends TmgBean
{
  private long      xferPartId;
  private long      xferId;
  private Date      createDate;
  private String    userName;
  private String    cancelFlag;
  private long      partId;
  private long      quantity;

  public void setXferPartId(long xferPartId)
  {
    this.xferPartId = xferPartId;
  }
  public long getXferPartId()
  {
    return xferPartId;
  }
  
  public void setXferId(long xferId)
  {
    this.xferId = xferId;
  }
  public long getXferId()
  {
    return xferId;
  }
  
  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setCancelFlag(String cancelFlag)
  {
    validateFlag(cancelFlag,"cancel");
    this.cancelFlag = cancelFlag;
  }
  public String getCancelFlag()
  {
    return flagValue(cancelFlag);
  }
  public boolean isCanceled()
  {
    return flagBoolean(cancelFlag);
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }

  public void setQuantity(long quantity)
  {
    this.quantity = quantity;
  }
  public long getQuantity()
  {
    return quantity;
  }

  public String toString()
  {
    return "TransferPart ["
      + " xfer part id: " + xferPartId 
      + ", xfer id: " + xferId 
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + ", canceled: " + isCanceled()
      + ", part id: " + partId
      + ", quantity: " + quantity
      + " ]";
  }
}