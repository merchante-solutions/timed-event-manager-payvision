package com.mes.tmg;

import org.apache.log4j.Logger;
import com.mes.mvc.mes.MesRequestHandler;
import com.mes.persist.Schema;

public class TmgRequestHandler extends MesRequestHandler
{
  static Logger log = Logger.getLogger(TmgRequestHandler.class);

  public static Schema tmgSchema = null;

  private TmgSession tmgSession;
  
  public TmgSession getTmgSession()
  {
    return tmgSession;
  }

  /**
   * Retrieve tmg session object if it exists from the session scope or
   * create a new one.  
   */
  protected void initializeHandler()
  {
    try
    {
      tmgSession = (TmgSession)session.getAttribute("tmgSession");
      if (tmgSession == null)
      {
        TmgDb db = new TmgDb();
        tmgSession = new TmgSession(db,user);
        session.setAttribute("tmgSession",tmgSession);
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new RuntimeException("Error getting TmgSession: " + e.toString());
    }
  }
}
