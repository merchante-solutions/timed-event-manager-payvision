package com.mes.tmg;

import java.util.Date;

public class RepairReportRow extends TmgBean
{
  private String serialNum;
  private String clientName;
  private String modelCode;
  private long batchNum;
  private Date batchDate;
  private Date shipDate;
  private String repairReason;
  private long acrId;
  private long partId;

  public void setBatchDate(Date batchDate)
  {
    this.batchDate = batchDate;
  }
  public Date getBatchDate()
  {
    return batchDate;
  }

  public void setShipDate(Date shipDate)
  {
    this.shipDate = shipDate;
  }
  public Date getShipDate()
  {
    return shipDate;
  }

  public void setClientName(String clientName)
  {
    this.clientName = clientName;
  }
  public String getClientName()
  {
    return clientName;
  }

  public void setRepairReason(String repairReason)
  {
    this.repairReason = repairReason;
  }
  public String getRepairReason()
  {
    return repairReason;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }

  public void setModelCode(String modelCode)
  {
    this.modelCode = modelCode;
  }
  public String getModelCode()
  {
    return modelCode;
  }

  public void setBatchNum(long batchNum)
  {
    this.batchNum = batchNum;
  }
  public long getBatchNum()
  {
    return batchNum;
  }

  public void setAcrId(long acrId)
  {
    this.acrId = acrId;
  }
  public long getAcrId()
  {
    return acrId;
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }

  public String toString()
  {
    return "RepairReportRow [ "
      + " ser num: " + serialNum
      + ", part id: " + partId
      + ", part type: " + modelCode
      + ", client: " + clientName
      + ", batch: " + batchNum
      + ", add date: " + batchDate
      + ", ship date: " + shipDate
      + ", reason: " + repairReason
      + ", acr: " + acrId
      + " ]";
  }
}