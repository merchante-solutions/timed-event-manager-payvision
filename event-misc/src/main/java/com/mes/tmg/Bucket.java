package com.mes.tmg;

public class Bucket
{
  private long      buckId;
  private long      invId;
  private long      ptId;
  private Inventory inventory;
  private PartType  partType;

  public boolean equals(Object o)
  {
    if (o instanceof Bucket)
    {
      Bucket that = (Bucket)o;
      return that.buckId == this.buckId;
    }
    return false;
  }

  public int hashCode()
  {
    return Long.valueOf(buckId).hashCode();
  }
  
  public void setBuckId(long buckId)
  {
    this.buckId = buckId;
  }
  public long getBuckId()
  {
    return buckId;
  }

  public void setInvId(long invId)
  {
    this.invId = invId;
  }
  public long getInvId()
  {
    return invId;
  }

  public void setInventory(Inventory inventory)
  {
    this.inventory = inventory;
  }
  public Inventory getInventory()
  {
    return inventory;
  }
  
  public void setPtId(long ptId)
  {
    this.ptId = ptId;
  }
  public long getPtId()
  {
    return ptId;
  }

  public void setPartType(PartType partType)
  {
    this.partType = partType;
  }
  public PartType getPartType()
  {
    return partType;
  }

  public String toString()
  {
    return "Bucket [ "
      + "bucket: " + buckId 
      + ", inv: " + inventory
      + ", part: " + partType
      + " ]";
  }
}