package com.mes.tmg.parts;

import org.apache.log4j.Logger;
import com.mes.tmg.PartFeature;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class FeatureAction extends TmgAction
{
  static Logger log = Logger.getLogger(FeatureAction.class);

  public String getActionObject(String actionName)
  {
    return "Feature";
  }

  private void doShowFeatures()
  {
    request.setAttribute("showItems",db.getPartFeatures());
    doView(Tmg.VIEW_SHOW_FEATURES);
  }

  private void doAddEditFeature()
  {
    FeatureBean bean = new FeatureBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_FEATURE);
    if (bean.isAutoSubmitOk())
    {
      String featName = bean.getFeature().getFeatName();
      long featId = bean.getFeature().getFeatId();
      logAndRedirect(Tmg.ACTION_SHOW_FEATURES,featName,featId);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_FEATURE : Tmg.VIEW_ADD_FEATURE);
    }
  }

  private void doDeleteFeature()
  {
    long delId = getParmLong("delId");
    PartFeature feature = db.getPartFeature(delId);
    if (feature == null)
    {
      throw new RuntimeException("Feature with id " + delId
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deletePartFeature(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_FEATURES,feature.getFeatName(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_FEATURES,"Feature not deleted");
    }
    else
    {
      confirm("Delete part feature " + feature.getFeatName() + "?");
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_SHOW_FEATURES))
    {
      doShowFeatures();
    }
    else if (name.equals(Tmg.ACTION_EDIT_FEATURE) ||
             name.equals(Tmg.ACTION_ADD_FEATURE))
    {
      doAddEditFeature();
    }
    else if (name.equals(Tmg.ACTION_DELETE_FEATURE))
    {
      doDeleteFeature();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
