package com.mes.tmg.parts;

import org.apache.log4j.Logger;
import com.mes.tmg.Condition;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class ConditionAction extends TmgAction
{
  static Logger log = Logger.getLogger(ConditionAction.class);
  
  public String getActionObject(String actionName)
  {
    return "Condition";
  }

  private void doShowConditions()
  {
    request.setAttribute("showItems",tmgSession.getDb().getConditions());
    doView(Tmg.VIEW_SHOW_CONDITIONS);
  }
  
  private void doAddEditCondition()
  {
    ConditionBean bean = new ConditionBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_CONDITION);
    if (bean.isAutoSubmitOk())
    {
      String condName = bean.getCondition().getCondName();
      long condId = bean.getCondition().getCondId();
      logAndRedirect(Tmg.ACTION_SHOW_CONDITIONS,condName,condId);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_CONDITION : Tmg.VIEW_ADD_CONDITION);
    }
  }

  private void doDeleteCondition()
  {
    long delId = getParmLong("delId");
    Condition condition = db.getCondition(delId);
    if (condition == null)
    {
      throw new RuntimeException("Condition with id " + delId
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deleteCondition(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_CONDITIONS,condition.getCondName(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_CONDITIONS,"Condition not deleted");
    }
    else
    {
      confirm("Delete part condition " + condition.getCondName() + "?");
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_SHOW_CONDITIONS))
    {
      doShowConditions();
    }
    else if (name.equals(Tmg.ACTION_EDIT_CONDITION) ||
             name.equals(Tmg.ACTION_ADD_CONDITION))
    {
      doAddEditCondition();
    }
    else if (name.equals(Tmg.ACTION_DELETE_CONDITION))
    {
      doDeleteCondition();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
