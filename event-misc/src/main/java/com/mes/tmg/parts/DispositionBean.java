package com.mes.tmg.parts;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.Disposition;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class DispositionBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(DispositionBean.class);
  
  public static final String FN_DISP_ID     = "dispId";
  public static final String FN_DISP_CODE   = "dispCode";
  public static final String FN_DISP_NAME   = "dispName";
  public static final String FN_DEPLOY_FLAG = "deployFlag";
  public static final String FN_DEPLOY_NAME = "deployName";
  
  public static final String FN_SUBMIT_BTN  = "submit";
  
  private Disposition disposition;

  public DispositionBean(TmgAction action)
  {
    super(action);
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_DISP_ID));
      fields.addAlias(FN_DISP_ID,"editId");
      fields.add(new Field(FN_DISP_CODE,"Code",16,32,false));
      fields.add(new Field(FN_DISP_NAME,"Name",32,32,false));
      fields.add(new CheckField(FN_DEPLOY_FLAG,"Can Deploy As","y",true));
      fields.add(new Field(FN_DEPLOY_NAME,"Deploy Name",32,32,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public Disposition getDisposition()
  {
    return disposition;
  }
  
  /**
   * Persists part disposition.
   */
  protected boolean autoSubmit()
  {
    try
    {
      long dispId = getField(FN_DISP_ID).asLong();
      if (dispId == 0L)
      {
        dispId = db.getNewId();
        disposition = new Disposition();
        disposition.setDispId(dispId);
      }
      else
      {
        disposition = db.getDisposition(dispId);
      }
      disposition.setDispCode(getData(FN_DISP_CODE));
      disposition.setDispName(getData(FN_DISP_NAME));
      disposition.setDeployFlag(getData(FN_DEPLOY_FLAG));
      disposition.setDeployName(getData(FN_DEPLOY_NAME));
      db.persist(disposition,user);
      return true;      
    }
    catch (Exception e)
    {
      log.error("Error submitting: " + e);
    }

    return false;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    try
    {
      long dispId = getField(FN_DISP_ID).asLong();
      if (dispId != 0L)
      {
        disposition = db.getDisposition(dispId);
        setData(FN_DISP_CODE,disposition.getDispCode());
        setData(FN_DISP_NAME,disposition.getDispName());
        setData(FN_DEPLOY_FLAG,disposition.getDeployFlag());
        setData(FN_DEPLOY_NAME,disposition.getDeployName());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error loading: " + e);
    }
    return false;
  }
}