package com.mes.tmg.parts;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class ConditionBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(ConditionBean.class);
  
  public static final String FN_COND_ID     = "condId";
  public static final String FN_COND_CODE   = "condCode";
  public static final String FN_COND_NAME   = "condName";
  
  public static final String FN_SUBMIT_BTN  = "submit";
  
  private com.mes.tmg.Condition condition;

  public ConditionBean(TmgAction action)
  {
    super(action);
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_COND_ID));
      fields.addAlias(FN_COND_ID,"editId");
      fields.add(new Field(FN_COND_CODE,"Code",16,32,false));
      fields.add(new Field(FN_COND_NAME,"Name",32,32,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public com.mes.tmg.Condition getCondition()
  {
    return condition;
  }
  
  /**
   * Persists part condition.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long condId = getField(FN_COND_ID).asLong();
      
      if (condId == 0L)
      {
        condId = db.getNewId();
        condition = new com.mes.tmg.Condition();
        condition.setCondId(condId);
      }
      else
      {
        condition = db.getCondition(condId);
      }
      
      condition.setCondCode(getData(FN_COND_CODE));
      condition.setCondName(getData(FN_COND_NAME));
    
      db.persist(condition,user);
      
      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting condition: " + e);
    }

    return submitOk;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long condId = getField(FN_COND_ID).asLong();
      
      if (condId != 0L)
      {
        condition = db.getCondition(condId);
        setData(FN_COND_CODE,condition.getCondCode());
        setData(FN_COND_NAME,condition.getCondName());
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading condition: " + e);
    }
    
    return loadOk;
  }
}