package com.mes.tmg.parts;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.HtmlBlock;
import com.mes.forms.HtmlDiv;
import com.mes.forms.RadioField;
import com.mes.tmg.PartFeature;
import com.mes.tmg.PartFeatureMap;
import com.mes.tmg.PartFeatureType;
import com.mes.tmg.PartType;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class PartTypeBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PartTypeBean.class);
  
  public static final String FN_PT_ID         = "ptId";
  public static final String FN_PT_NAME       = "ptName";
  public static final String FN_MODEL_CODE    = "modelCode";
  public static final String FN_DESCRIPTION   = "description";
  public static final String FN_SN_FLAG       = "snFlag";
  public static final String FN_ENCRYPT_FLAG  = "encryptFlag";
  
  public static final String FN_SUBMIT_BTN    = "submit";
  
  private PartType  pt;
  private HtmlBlock featuresBlock;
  private List      featureFieldList;
  private Map       featureDivMap;

  public PartTypeBean(TmgAction action)
  {
    super(action);
  }

  class FtComparator implements Comparator
  {
    public int compare(Object o1, Object o2)
    {
      PartFeatureType ft1 = (PartFeatureType)o1;
      PartFeatureType ft2 = (PartFeatureType)o2;
      if (ft1.getDisplayOrder() < ft2.getDisplayOrder()) return -1;
      if (ft1.getDisplayOrder() > ft2.getDisplayOrder()) return 1;
      return ft1.getFtCode().compareTo(ft2.getFtCode());
    }
  }

  private void createFeatureFields()
  {
    featureFieldList = new ArrayList();
    featureDivMap = new TreeMap(new FtComparator());
    Map ftFeatMap = db.getFtFeatMap();
    int minWidth = 140;
    for (Iterator i = ftFeatMap.entrySet().iterator(); i.hasNext();)
    {
      Map.Entry me = (Map.Entry)i.next();
      PartFeatureType ft = (PartFeatureType)me.getKey();
      List feats = (List)me.getValue();
      HtmlDiv fieldDiv = new HtmlDiv();
      if (ft.getIsExclusive().equals("y"))
      {
        String fieldName = "feat_type_" + ft.getFtCode();
        RadioField rf = new RadioField(fieldName,ft.getFtName());
        if (!ft.getIsRequired().equals("y"))
        {
          rf.addButton("none","None");
        }
        for (Iterator j = feats.iterator(); j.hasNext();)
        {
          PartFeature feat = (PartFeature)j.next();
          rf.addButton("" + feat.getFeatId(),feat.getFeatName());
        }
        fields.add(rf);
        for (int idx = 0; idx < rf.getSize(); ++idx)
        {
          HtmlDiv ctrlDiv = new HtmlDiv();
          ctrlDiv.setProperty("class","featureControl");
          //ctrlDiv.setProperty("style","min-width: " + minWidth + "px; float: left");
          ctrlDiv.add(rf.getInputTag(idx));
          ctrlDiv.add(rf.getLabel(idx));
          fieldDiv.add(ctrlDiv);
        }

        featureFieldList.add(rf);
        fields.add(new HiddenField("h_" + fieldName, ft.getFtCode()));
      }
      else
      {
        for (Iterator j = feats.iterator(); j.hasNext();)
        {
          PartFeature feat = (PartFeature)j.next();
          String fieldName = "feat_" + feat.getFeatId();
          CheckField cf = new CheckField(fieldName,
            feat.getFeatName(),"" + feat.getFeatId(),false);
          fields.add(cf);
          HtmlDiv ctrlDiv = new HtmlDiv();
          ctrlDiv.setProperty("class","featureControl");
          //ctrlDiv.setProperty("style","min-width: " + minWidth + "px; float: left");
          ctrlDiv.add(cf);
          fieldDiv.add(ctrlDiv);
          featureFieldList.add(cf);
          fields.add(new HiddenField("h_" + fieldName, ft.getFtCode()));
        }
      }
      featureDivMap.put(ft,fieldDiv);
    }
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_PT_ID));
      fields.addAlias(FN_PT_ID,"editId");
      fields.add(new Field(FN_PT_NAME,"Name",32,32,false));
      fields.add(new Field(FN_MODEL_CODE,"Model",60,32,false));
      fields.add(new Field(FN_DESCRIPTION,"Description",300,32,false));
      fields.add(new CheckField(FN_SN_FLAG,"Has Serial No.","y",true));
      fields.add(new CheckField(FN_ENCRYPT_FLAG,"PIN Device supporting key injection (encryption)","y",true));
      fields.add(new ButtonField("submit","Submit"));
      createFeatureFields();
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
      e.printStackTrace();
    }
  }

  public PartType getPartType()
  {
    return pt;
  }
  
  /**
   * Persiste feature mappings.
   */
  private void submitFeatures(long ptId)
  {
    db.deleteFeatureMapsWithPtId(ptId,user);
    
    for (Iterator i = featureFieldList.iterator(); i.hasNext();)
    {
      Field field = (Field)i.next();
      if (field instanceof CheckField)
      {
        if (!((CheckField)field).isChecked())
        {
          continue;
        }
      }
      if (field instanceof RadioField && field.getData().equals("none"))
      {
        continue;
      }
      PartFeatureMap fm = new PartFeatureMap();
      fm.setMapId(db.getNewId());
      fm.setPtId(ptId);
      fm.setFeatId(field.asLong());
      fm.setFtCode(fields.getData("h_" + field.getName()));
      db.persist(fm,user);
    }
  }

  /**
   * Persists part type.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long ptId = getField(FN_PT_ID).asLong();
      
      if (ptId == 0L)
      {
        ptId = db.getNewId();
        pt = new PartType();
        pt.setPtId(ptId);
      }
      else
      {
        pt = db.getPartType(ptId);
      }
      
      pt.setPtName(getData(FN_PT_NAME));
      pt.setModelCode(getData(FN_MODEL_CODE));
      pt.setDescription(getData(FN_DESCRIPTION));
      pt.setSnFlag(getData(FN_SN_FLAG));
      pt.setEncryptFlag(getData(FN_ENCRYPT_FLAG));

      submitFeatures(ptId);
    
      db.persist(pt,user);
      
      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting part type: " + e);
    }

    return submitOk;
  }
  
  /**
   * Loads a part type's feature settings into the feature fields.
   */
  private void loadFeatures(long ptId)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  fe.feat_id,                           "
                + "         fe.ft_code,                           "
                + "         nvl(ft.is_exclusive,'n') is_exclusive "
                + " from    tmg_part_feature_maps   fm,           "
                + "         tmg_part_features       fe,           "
                + "         tmg_part_feature_types  ft            "
                + " where   fm.feat_id = fe.feat_id               "
                + "         and fe.ft_code = ft.ft_code           "
                + "         and fm.pt_id = ?                      ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,ptId);
      rs = ps.executeQuery();

      while (rs.next())
      {
        long featId = rs.getLong("feat_id");
        String ftCode = rs.getString("ft_code");
        boolean isExclusive = rs.getString("is_exclusive").equals("y");

        if (isExclusive)
        {
          setData("feat_type_" + ftCode,"" + featId);
        }
        else
        {
          setData("feat_" + featId,"" + featId);
        }
      }
    }
    catch (Exception e)
    {
      log.error("Error loading feature data: " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long ptId = getField(FN_PT_ID).asLong();
      
      if (ptId != 0L)
      {
        pt = db.getPartType(ptId);
        
        setData(FN_PT_NAME,pt.getPtName());
        setData(FN_MODEL_CODE,pt.getModelCode());
        setData(FN_DESCRIPTION,pt.getDescription());
        setData(FN_SN_FLAG,pt.getSnFlag());
        setData(FN_ENCRYPT_FLAG,pt.getEncryptFlag());
        loadFeatures(ptId);
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading part type: " + e);
    }
    
    return loadOk;
  }

  public Map getFeatureDivMap()
  {
    return featureDivMap;
  }
}