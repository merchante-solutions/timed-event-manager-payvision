package com.mes.tmg.parts;

import org.apache.log4j.Logger;
import com.mes.tmg.Location;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class LocationAction extends TmgAction
{
  static Logger log = Logger.getLogger(LocationAction.class);
  
  public String getActionObject(String actionName)
  {
    return "Location";
  }

  private void doShowLocations()
  {
    request.setAttribute("showItems",tmgSession.getDb().getLocations());
    doView(Tmg.VIEW_SHOW_LOCATIONS);
  }
  
  private void doAddEditLocation()
  {
    LocationBean bean = new LocationBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_LOCATION);
    if (bean.isAutoSubmitOk())
    {
      String locName = bean.getLocation().getLocName();
      long id = bean.getLocation().getLocId();
      logAndRedirect(Tmg.ACTION_SHOW_LOCATIONS,locName,id);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_LOCATION : Tmg.VIEW_ADD_LOCATION);
    }
  }

  private void doDeleteLocation()
  {
    long delId = getParmLong("delId");
    Location location = db.getLocation(delId);
    if (location == null)
    {
      throw new RuntimeException("Location with id " + delId
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deleteLocation(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_LOCATIONS,location.getLocName(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_LOCATIONS,"Location not deleted");
    }
    else
    {
      confirm("Delete location " + location.getLocName() + "?");
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_SHOW_LOCATIONS))
    {
      doShowLocations();
    }
    else if (name.equals(Tmg.ACTION_EDIT_LOCATION) ||
             name.equals(Tmg.ACTION_ADD_LOCATION))
    {
      doAddEditLocation();
    }
    else if (name.equals(Tmg.ACTION_DELETE_LOCATION))
    {
      doDeleteLocation();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
