package com.mes.tmg.parts;

import org.apache.log4j.Logger;
import com.mes.tmg.EncryptionType;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class EncryptTypeAction extends TmgAction
{
  static Logger log = Logger.getLogger(EncryptTypeAction.class);

  public String getActionObject(String actionName)
  {
    return "Encryption type";
  }

  private void doShowEncryptTypes()
  {
    request.setAttribute("showItems",db.getEncryptTypes());
    doView(Tmg.VIEW_SHOW_ENCRYPT_TYPES);
  }

  private void doAddEditEncryptType()
  {
    EncryptTypeBean bean = new EncryptTypeBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_ENCRYPT_TYPE);
    if (bean.isAutoSubmitOk())
    {
      String etName = bean.getEncryptType().getEtName();
      long etId = bean.getEncryptType().getEtId();
      logAndRedirect(Tmg.ACTION_SHOW_ENCRYPT_TYPES,etName,etId);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_ENCRYPT_TYPE : Tmg.VIEW_ADD_ENCRYPT_TYPE);
    }
  }

  private void doDeleteEncryptType()
  {
    long delId = getParmLong("delId");
    EncryptionType encryptType = db.getEncryptType(delId);
    if (encryptType == null)
    {
      throw new RuntimeException("Encryption type with id " + delId
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deleteEncryptType(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_ENCRYPT_TYPES,encryptType.getEtName(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_ENCRYPT_TYPES,
        "Encryption type not deleted");
    }
    else
    {
      confirm("Delete encryption type " + encryptType.getEtName() + "?");
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_SHOW_ENCRYPT_TYPES))
    {
      doShowEncryptTypes();
    }
    else if (name.equals(Tmg.ACTION_EDIT_ENCRYPT_TYPE) ||
             name.equals(Tmg.ACTION_ADD_ENCRYPT_TYPE))
    {
      doAddEditEncryptType();
    }
    else if (name.equals(Tmg.ACTION_DELETE_ENCRYPT_TYPE))
    {
      doDeleteEncryptType();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
