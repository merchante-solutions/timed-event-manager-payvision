package com.mes.tmg.parts;

import org.apache.log4j.Logger;
import com.mes.tmg.Disposition;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class DispositionAction extends TmgAction
{
  static Logger log = Logger.getLogger(DispositionAction.class);
  
  public String getActionObject(String actionName)
  {
    return "Disposition";
  }

  private void doShowDispositions()
  {
    request.setAttribute("showItems",tmgSession.getDb().getDispositions());
    doView(Tmg.VIEW_SHOW_DISPOSITIONS);
  }
  
  private void doAddEditDisposition()
  {
    DispositionBean bean = new DispositionBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_DISPOSITION);
    if (bean.isAutoSubmitOk())
    {
      String dispName = bean.getDisposition().getDispName();
      long dispId = bean.getDisposition().getDispId();
      logAndRedirect(Tmg.ACTION_SHOW_DISPOSITIONS,dispName,dispId);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_DISPOSITION : Tmg.VIEW_ADD_DISPOSITION);
    }
  }

  private void doDeleteDisposition()
  {
    long delId = getParmLong("delId");
    Disposition disposition = db.getDisposition(delId);
    if (disposition == null)
    {
      throw new RuntimeException("Disposition with id " + delId
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deleteDisposition(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_DISPOSITIONS,disposition.getDispName(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_DISPOSITIONS,"Disposition not deleted");
    }
    else
    {
      confirm("Delete part disposition " + disposition.getDispName() + "?");
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_SHOW_DISPOSITIONS))
    {
      doShowDispositions();
    }
    else if (name.equals(Tmg.ACTION_EDIT_DISPOSITION) ||
             name.equals(Tmg.ACTION_ADD_DISPOSITION))
    {
      doAddEditDisposition();
    }
    else if (name.equals(Tmg.ACTION_DELETE_DISPOSITION))
    {
      doDeleteDisposition();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
