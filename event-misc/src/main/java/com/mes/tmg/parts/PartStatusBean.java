package com.mes.tmg.parts;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.PartStatus;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class PartStatusBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PartStatusBean.class);
  
  public static final String FN_STATUS_ID   = "statusId";
  public static final String FN_STATUS_CODE = "statusCode";
  public static final String FN_STATUS_NAME = "statusName";
  
  public static final String FN_SUBMIT_BTN  = "submit";
  
  private PartStatus status;

  public PartStatusBean(TmgAction action)
  {
    super(action);
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_STATUS_ID));
      fields.addAlias(FN_STATUS_ID,"editId");
      fields.add(new Field(FN_STATUS_CODE,"Code",16,32,false));
      fields.add(new Field(FN_STATUS_NAME,"Name",32,32,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public PartStatus getPartStatus()
  {
    return status;
  }
  
  /**
   * Persists part status.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long statusId = getField(FN_STATUS_ID).asLong();
      
      if (statusId == 0L)
      {
        statusId = db.getNewId();
        status = new PartStatus();
        status.setStatusId(statusId);
      }
      else
      {
        status = db.getPartStatus(statusId);
      }
      
      status.setStatusCode(getData(FN_STATUS_CODE));
      status.setStatusName(getData(FN_STATUS_NAME));
    
      db.persist(status,user);
      
      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting part status: " + e);
    }

    return submitOk;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long statusId = getField(FN_STATUS_ID).asLong();
      
      if (statusId != 0L)
      {
        status = db.getPartStatus(statusId);
        setData(FN_STATUS_CODE,status.getStatusCode());
        setData(FN_STATUS_NAME,status.getStatusName());
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading part status: " + e);
    }
    
    return loadOk;
  }
}