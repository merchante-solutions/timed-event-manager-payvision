package com.mes.tmg.parts;

import org.apache.log4j.Logger;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class PartClassAction extends TmgAction
{
  static Logger log = Logger.getLogger(PartClassAction.class);
  
  public String getActionObject(String actionName)
  {
    return "PartClass";
  }

  private void doShowPartClasses()
  {
    request.setAttribute("showItems",tmgSession.getDb().getPartClasses());
    doView(Tmg.VIEW_SHOW_PART_CLASSES);
  }
  
  private void doAddEditPartClass()
  {
    PartClassBean bean = new PartClassBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_PART_CLASS);
    if (bean.isAutoSubmitOk())
    {
      String pcName = bean.getPartClass().getPcName();
      long pcId = bean.getPartClass().getPcId();
      logAndRedirect(Tmg.ACTION_SHOW_PART_CLASSES,pcName,pcId);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_PART_CLASS : Tmg.VIEW_ADD_PART_CLASS);
    }
  }

  private void doDeletePartClass()
  {
    long delId = getParmLong("delId");
    
    if (isConfirmed())
    {
      if (db.getPartClass(delId) == null)
      {
        throw new RuntimeException("No part class with id " + delId 
          + " exists to delete");
      }

      db.deletePartClass(delId,user);
      String description = "Part class " + delId + " deleted";
      logAction(description,delId);
      addFeedback(description);
      doActionRedirect(Tmg.ACTION_SHOW_PART_CLASSES);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_PART_CLASSES,"Delete canceled");
    }
    else
    {
      confirm("Delete part class?");
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_SHOW_PART_CLASSES))
    {
      doShowPartClasses();
    }
    else if (name.equals(Tmg.ACTION_EDIT_PART_CLASS) ||
             name.equals(Tmg.ACTION_ADD_PART_CLASS))
    {
      doAddEditPartClass();
    }
    else if (name.equals(Tmg.ACTION_DELETE_PART_CLASS))
    {
      doDeletePartClass();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
