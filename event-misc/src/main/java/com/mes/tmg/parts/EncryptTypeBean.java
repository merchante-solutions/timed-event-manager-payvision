package com.mes.tmg.parts;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.EncryptionType;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class EncryptTypeBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(EncryptTypeBean.class);
  
  public static final String FN_ET_ID       = "etId";
  public static final String FN_ET_NAME     = "etName";
  public static final String FN_ET_CODE     = "etCode";
  
  public static final String FN_SUBMIT_BTN  = "submit";
  
  private EncryptionType encryptType;

  public EncryptTypeBean(TmgAction action)
  {
    super(action);
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_ET_ID));
      fields.addAlias(FN_ET_ID,"editId");
      fields.add(new Field(FN_ET_NAME,"Name",32,32,false));
      fields.add(new Field(FN_ET_CODE,"BIN",6,6,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public EncryptionType getEncryptType()
  {
    return encryptType;
  }
  
  /**
   * Persists ft.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long etId = getField(FN_ET_ID).asLong();
      
      if (etId == 0L)
      {
        etId = db.getNewId();
        encryptType = new EncryptionType();
        encryptType.setEtId(etId);
        setData(FN_ET_ID,String.valueOf(etId));
      }
      else
      {
        encryptType = db.getEncryptType(etId);
      }
      
      encryptType.setEtName(getData(FN_ET_NAME));
      encryptType.setEtCode(getData(FN_ET_CODE));
    
      db.persist(encryptType,user);
      
      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting encryption type: " + e);
    }

    return submitOk;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long etId = getField(FN_ET_ID).asLong();
      
      if (etId != 0L)
      {
        encryptType = db.getEncryptType(etId);
        setData(FN_ET_NAME,encryptType.getEtName());
        setData(FN_ET_CODE,encryptType.getEtCode());
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading encryption type: " + e);
    }
    
    return loadOk;
  }
}