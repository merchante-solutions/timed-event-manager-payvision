package com.mes.tmg.parts;

import org.apache.log4j.Logger;
import com.mes.tmg.PartStatus;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class PartStatusAction extends TmgAction
{
  static Logger log = Logger.getLogger(PartStatusAction.class);
  
  public String getActionObject(String actionName)
  {
    return "PartStatus";
  }

  private void doShowPartStatuses()
  {
    request.setAttribute("showItems",tmgSession.getDb().getPartStatuses());
    doView(Tmg.VIEW_SHOW_PART_STATUSES);
  }
  
  private void doAddEditPartStatus()
  {
    PartStatusBean bean = new PartStatusBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_PART_STATUS);
    if (bean.isAutoSubmitOk())
    {
      String statusName = bean.getPartStatus().getStatusName();
      long id = bean.getPartStatus().getStatusId();
      logAndRedirect(Tmg.ACTION_SHOW_PART_STATUSES,statusName,id);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_PART_STATUS : Tmg.VIEW_ADD_PART_STATUS);
    }
  }

  private void doDeletePartStatus()
  {
    long delId = getParmLong("delId");
    PartStatus status = db.getPartStatus(delId);
    if (status == null)
    {
      throw new RuntimeException("PartStatus with id " + delId 
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deletePartStatus(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_PART_STATUSES,status.getStatusName(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_PART_STATUSES,"PartStatus not deleted");
    }
    else
    {
      confirm("Delete status " + status.getStatusName() + "?");
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_SHOW_PART_STATUSES))
    {
      doShowPartStatuses();
    }
    else if (name.equals(Tmg.ACTION_EDIT_PART_STATUS) ||
             name.equals(Tmg.ACTION_ADD_PART_STATUS))
    {
      doAddEditPartStatus();
    }
    else if (name.equals(Tmg.ACTION_DELETE_PART_STATUS))
    {
      doDeletePartStatus();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
