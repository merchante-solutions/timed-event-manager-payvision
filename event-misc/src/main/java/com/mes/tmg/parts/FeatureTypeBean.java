package com.mes.tmg.parts;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.tmg.PartFeatureType;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class FeatureTypeBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(FeatureTypeBean.class);
  
  public static final String FN_ORIG_FT_CODE  = "origFtCode";
  public static final String FN_FT_CODE       = "ftCode";
  public static final String FN_FT_NAME       = "ftName";
  public static final String FN_IS_REQUIRED   = "isRequired";
  public static final String FN_IS_EXCLUSIVE  = "isExclusive";
  public static final String FN_DISPLAY_ORDER = "displayOrder";
  
  public static final String FN_SUBMIT_BTN    = "submit";
  
  private PartFeatureType ft;

  public FeatureTypeBean(TmgAction action)
  {
    super(action);
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_ORIG_FT_CODE));
      fields.addAlias(FN_ORIG_FT_CODE,"editId");
      fields.add(new Field(FN_FT_CODE,"Code",64,32,false));
      fields.add(new Field(FN_FT_NAME,"Name",100,32,false));
      fields.add(new CheckField(FN_IS_REQUIRED,"Required type","y",false));
      fields.add(new CheckField(FN_IS_EXCLUSIVE,"Mutually exclusive ","y",false));
      fields.add(new NumberField(FN_DISPLAY_ORDER,"Display Order",5,32,true,0));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public PartFeatureType getFeatureType()
  {
    return ft;
  }
  
  /**
   * Persists ft.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      String ftCode       = getData(FN_FT_CODE);
      String origFtCode   = getData(FN_ORIG_FT_CODE);

      boolean isUpdate    = origFtCode.length() > 0;
      boolean codeExists  = db.getPartFeatureType(ftCode) != null;
      boolean isNewCode   = isUpdate && !ftCode.equals(origFtCode);

      // look for attempt to override another existing code
      if (isUpdate && codeExists && isNewCode)
      {
        throw new Exception("Feature type code already exists");
      }

      // fetch original version of feature type if it exists
      if (isUpdate)
      {
        ft = db.getPartFeatureType(origFtCode);
      }
      // create a new empty feature type if it does not exist
      else
      {
        ft = new PartFeatureType();
      }

      // load field data into the feature type
      ft.setFtCode(ftCode);
      ft.setFtName(getData(FN_FT_NAME));
      ft.setIsRequired(getData(FN_IS_REQUIRED));
      ft.setIsExclusive(getData(FN_IS_EXCLUSIVE));
      ft.setDisplayOrder(getField(FN_DISPLAY_ORDER).asInteger());

      // persist the feature type
      db.persist(ft,user);
      
      // remove old feature type if updating to have new code
      // and update features with the old code
      if (isUpdate && isNewCode)
      {
        db.deletePartFeatureType(origFtCode,user);
        db.updateFeaturesFtCode(origFtCode,ftCode,user);
      }

      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting feature type: " + e);
      addError(e.getMessage());
    }

    return submitOk;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      String ftCode = getData(FN_ORIG_FT_CODE);
      
      if (ftCode.length() > 0)
      {
        ft = db.getPartFeatureType(ftCode);
        setData(FN_FT_CODE,ftCode);
        setData(FN_FT_NAME,ft.getFtName());
        setData(FN_IS_REQUIRED,ft.getIsRequired());
        setData(FN_IS_EXCLUSIVE,ft.getIsExclusive());
        setData(FN_DISPLAY_ORDER,String.valueOf(ft.getDisplayOrder()));
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading ft: " + e);
    }
    
    return loadOk;
  }
}