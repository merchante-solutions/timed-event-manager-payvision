package com.mes.tmg.parts;

import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.tmg.PartFeature;
import com.mes.tmg.PartFeatureType;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tools.DropDownTable;

public class FeatureBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(FeatureBean.class);
  
  public static final String FN_FEAT_ID       = "featId";
  public static final String FN_FEAT_NAME     = "featName";
  public static final String FN_FT_CODE       = "ftCode";
  public static final String FN_DISPLAY_ORDER = "displayOrder";
  
  public static final String FN_SUBMIT_BTN    = "submit";
  
  private PartFeature feature;

  public FeatureBean(TmgAction action)
  {
    super(action);
  }
  
  protected class FeatureTypeTable extends DropDownTable
  {
    public FeatureTypeTable()
    {
      addElement("","select one");
      List types = db.getPartFeatureTypes();
      for (Iterator i = types.iterator(); i.hasNext(); )
      {
        PartFeatureType ft = (PartFeatureType)i.next();
        addElement(ft.getFtCode(),ft.getFtName());
      }
    }
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_FEAT_ID));
      fields.addAlias(FN_FEAT_ID,"editId");
      fields.add(new Field(FN_FEAT_NAME,"Name",32,32,false));
      fields.add(new DropDownField(FN_FT_CODE,new FeatureTypeTable(),false));
      fields.add(new NumberField(FN_DISPLAY_ORDER,"Display Order",5,32,true,0));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
      e.printStackTrace();
    }
  }

  public PartFeature getFeature()
  {
    return feature;
  }
  
  /**
   * Persists ft.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long featId = getField(FN_FEAT_ID).asLong();
      
      if (featId == 0L)
      {
        featId = db.getNewId();
        feature = new PartFeature();
        feature.setFeatId(featId);
      }
      else
      {
        feature = db.getPartFeature(featId);
      }
      
      feature.setFeatName(getData(FN_FEAT_NAME));
      feature.setFtCode(getData(FN_FT_CODE));
      feature.setDisplayOrder(getField(FN_DISPLAY_ORDER).asInteger());
    
      db.persist(feature,user);
      
      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting feature: " + e);
    }

    return submitOk;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long featId = getField(FN_FEAT_ID).asLong();
      
      if (featId != 0L)
      {
        feature = db.getPartFeature(featId);
        setData(FN_FEAT_NAME,feature.getFeatName());
        setData(FN_FT_CODE,feature.getFtCode());
        setData(FN_DISPLAY_ORDER,String.valueOf(feature.getDisplayOrder()));
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading feature: " + e);
    }
    
    return loadOk;
  }
}