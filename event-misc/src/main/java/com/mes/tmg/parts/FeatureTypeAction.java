package com.mes.tmg.parts;

import org.apache.log4j.Logger;
import com.mes.tmg.PartFeatureType;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class FeatureTypeAction extends TmgAction
{
  static Logger log = Logger.getLogger(FeatureTypeAction.class);

  public String getActionObject(String actionName)
  {
    return "Feature type";
  }

  private void doShowFeatureTypes()
  {
    request.setAttribute("showItems",tmgSession.getDb().getPartFeatureTypes());
    doView(Tmg.VIEW_SHOW_FEATURE_TYPES);
  }
  
  private void doAddEditFeatureType()
  {
    FeatureTypeBean bean = new FeatureTypeBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_FEATURE_TYPE);
    if (bean.isAutoSubmitOk())
    {
      String ftCode = bean.getFeatureType().getFtCode();
      logAndRedirect(Tmg.ACTION_SHOW_FEATURE_TYPES,ftCode);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_FEATURE_TYPE : Tmg.VIEW_ADD_FEATURE_TYPE);
    }
  }

  private void doDeleteFeatureType()
  {
    String delId = getParm("delId");
    PartFeatureType ft = db.getPartFeatureType(delId);
    if (ft == null)
    {
      throw new RuntimeException("Feature type " + delId + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deleteFeaturesWithCode(delId,user);
      db.deletePartFeatureType(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_FEATURE_TYPES,delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_FEATURE_TYPES,
        "Feature type not deleted");
    }
    else
    {
      confirm("Delete part feature type " + delId + "?");
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_SHOW_FEATURE_TYPES))
    {
      doShowFeatureTypes();
    }
    else if (name.equals(Tmg.ACTION_EDIT_FEATURE_TYPE) ||
             name.equals(Tmg.ACTION_ADD_FEATURE_TYPE))
    {
      doAddEditFeatureType();
    }
    else if (name.equals(Tmg.ACTION_DELETE_FEATURE_TYPE))
    {
      doDeleteFeatureType();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
