package com.mes.tmg.parts;

import org.apache.log4j.Logger;
import com.mes.tmg.PartType;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class PartTypeAction extends TmgAction
{
  static Logger log = Logger.getLogger(PartTypeAction.class);

  public String getActionObject(String actionName)
  {
    return "Part type";
  }

  private void doShowPartTypes()
  {
    request.setAttribute("showItems",tmgSession.getDb().getPartTypes());
    doView(Tmg.VIEW_SHOW_PART_TYPES);
  }
  
  private void doAddEditPartType()
  {
    PartTypeBean bean = new PartTypeBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_PART_TYPE);
    if (bean.isAutoSubmitOk())
    {
      String ptName = bean.getPartType().getPtName();
      long id = bean.getPartType().getPtId();
      logAndRedirect(Tmg.ACTION_SHOW_PART_TYPES,ptName,id);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_PART_TYPE : Tmg.VIEW_ADD_PART_TYPE);
    }
  }

  private void doDeletePartType()
  {
    long delId = getParmLong("delId");
    PartType pt = db.getPartType(delId);
    if (pt == null)
    {
      throw new RuntimeException("Part type with id " + delId
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deletePartType(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_PART_TYPES,pt.getPtName(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_PART_TYPES,"Part type not deleted");
    }
    else
    {
      confirm("Delete part type " + pt.getPtName() + "?");
    }
  }

  public void execute()
  {
    if (name.equals(Tmg.ACTION_SHOW_PART_TYPES))
    {
      doShowPartTypes();
    }
    else if (name.equals(Tmg.ACTION_EDIT_PART_TYPE) ||
             name.equals(Tmg.ACTION_ADD_PART_TYPE))
    {
      doAddEditPartType();
    }
    else if (name.equals(Tmg.ACTION_DELETE_PART_TYPE))
    {
      doDeletePartType();
    }
    else
    {
      throw new RuntimeException("Invalid action requested: " + name);
    }
  }
}
