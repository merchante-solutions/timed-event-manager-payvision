package com.mes.tmg.parts;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.Location;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class LocationBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(LocationBean.class);
  
  public static final String FN_LOC_ID      = "locId";
  public static final String FN_LOC_CODE    = "locCode";
  public static final String FN_LOC_NAME    = "locName";
  
  public static final String FN_SUBMIT_BTN  = "submit";
  
  private Location location;

  public LocationBean(TmgAction action)
  {
    super(action);
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_LOC_ID));
      fields.addAlias(FN_LOC_ID,"editId");
      fields.add(new Field(FN_LOC_CODE,"Code",16,32,false));
      fields.add(new Field(FN_LOC_NAME,"Name",32,32,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public Location getLocation()
  {
    return location;
  }
  
  /**
   * Persists part location.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long locId = getField(FN_LOC_ID).asLong();
      
      if (locId == 0L)
      {
        locId = db.getNewId();
        location = new Location();
        location.setLocId(locId);
      }
      else
      {
        location = db.getLocation(locId);
      }
      
      location.setLocCode(getData(FN_LOC_CODE));
      location.setLocName(getData(FN_LOC_NAME));
    
      db.persist(location,user);
      
      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting location: " + e);
    }

    return submitOk;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long locId = getField(FN_LOC_ID).asLong();
      
      if (locId != 0L)
      {
        location = db.getLocation(locId);
        setData(FN_LOC_CODE,location.getLocCode());
        setData(FN_LOC_NAME,location.getLocName());
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading location: " + e);
    }
    
    return loadOk;
  }
}