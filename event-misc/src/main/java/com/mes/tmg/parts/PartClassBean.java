package com.mes.tmg.parts;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.PartClass;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class PartClassBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(PartClassBean.class);
  
  public static final String FN_PC_ID      = "pcId";
  public static final String FN_PC_CODE    = "pcCode";
  public static final String FN_PC_NAME    = "pcName";
  
  public static final String FN_SUBMIT_BTN  = "submit";

  private PartClass partClass;
  
  public PartClassBean(TmgAction action)
  {
    super(action);
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_PC_ID));
      fields.addAlias(FN_PC_ID,"editId");
      fields.add(new Field(FN_PC_CODE,"Code",16,32,false));
      fields.add(new Field(FN_PC_NAME,"Name",32,32,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public PartClass getPartClass()
  {
    return partClass;
  }

  /**
   * Persists part location.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long pcId = getField(FN_PC_ID).asLong();
      
      if (pcId == 0L)
      {
        pcId = db.getNewId();
        partClass = new PartClass();
        partClass.setPcId(pcId);
      }
      else
      {
        partClass = db.getPartClass(pcId);
      }
      
      partClass.setPcCode(getData(FN_PC_CODE));
      partClass.setPcName(getData(FN_PC_NAME));
    
      db.persist(partClass,user);

      submitOk = true;
    }
    catch (Exception e)
    {
      log.error("Error submitting part class: " + e);
    }

    return submitOk;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long pcId = getField(FN_PC_ID).asLong();
      
      if (pcId != 0L)
      {
        partClass = db.getPartClass(pcId);
        setData(FN_PC_CODE,partClass.getPcCode());
        setData(FN_PC_NAME,partClass.getPcName());
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading part class: " + e);
    }
    
    return loadOk;
  }
}