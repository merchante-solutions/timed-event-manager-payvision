package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class PurchaseOrderItem extends TmgBean
{
  public static final int   POI_STAT_NONE       = 0;
  public static final int   POI_STAT_CANCELED   = 1;
  public static final int   POI_STAT_NEW        = 2;
  public static final int   POI_STAT_CONFIRMED  = 3;
  public static final int   POI_STAT_RECEIVING  = 4;
  public static final int   POI_STAT_RECEIVED   = 5;
  
  private long              poItemId;
  private Date              createDate;
  private String            userName;
  private long              poId;
  private int               itemStatus;
  private long              partCount;
  private long              recvCount;
  private double            partPrice;
  private double            partFee;
  private long              buckId;
  private Bucket            bucket;
  private String            description;
  private String            pcCode;
  private PartClass         partClass;
  private long              etId;
  private EncryptionType    encryptionType;
  private List              parts;
  private int               receivedCount;
  private int               addedCount;


  public void setPoItemId(long poItemId)
  {
    this.poItemId = poItemId;
  }
  public long getPoItemId()
  {
    return poItemId;
  }

  public void setReceivedCount(int receivedCount)
  {
    this.receivedCount = receivedCount;
  }
  public int getReceivedCount()
  {
    return receivedCount;
  }
  
  public void setAddedCount(int addedCount)
  {
    this.addedCount = addedCount;
  }
  public int getAddedCount()
  {
    return addedCount;
  }

  private void validateItemStatus(int status)
  {
    switch (status)
    {
      case POI_STAT_NONE:
      case POI_STAT_CANCELED:
      case POI_STAT_NEW:
      case POI_STAT_CONFIRMED:
      case POI_STAT_RECEIVING:
      case POI_STAT_RECEIVED:
        break;
      default:
        throw new RuntimeException("Invalid status for P.O. item " + poItemId
          + ": " + status);
    }
  }

  public void setItemStatus(int itemStatus)
  {
    validateItemStatus(itemStatus);
    this.itemStatus = itemStatus;
  }
  public int getItemStatus()
  {
    return itemStatus;
  }

  public String getItemStatusStr()
  {
    switch (itemStatus)
    {
      case POI_STAT_NONE:
        return "None";
      case POI_STAT_CANCELED:
        return "Canceled";
      case POI_STAT_NEW:
        return "New";
      case POI_STAT_CONFIRMED:
        return "Confirmed";
      case POI_STAT_RECEIVING:
        return "Receiving";
      case POI_STAT_RECEIVED:
        return "Received";
    }
    return "Unknown status: " + itemStatus;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setPoId(long poId)
  {
    this.poId = poId;
  }
  public long getPoId()
  {
    return poId;
  }

  public void setBuckId(long buckId)
  {
    this.buckId = buckId;
  }
  public long getBuckId()
  {
    return buckId;
  }

  public void setBucket(Bucket bucket)
  {
    this.bucket = bucket;
  }
  public Bucket getBucket()
  {
    return bucket;
  }

  public void setPcCode(String pcCode)
  {
    this.pcCode = pcCode;
  }
  public String getPcCode()
  {
    return pcCode;
  }

  public void setPartClass(PartClass partClass)
  {
    this.partClass = partClass;
  }
  public PartClass getPartClass()
  {
    return partClass;
  }

  public void setPartCount(long partCount)
  {
    this.partCount = partCount;
  }
  public long getPartCount()
  {
    return partCount;
  }

  public void setRecvCount(long recvCount)
  {
    this.recvCount = recvCount;
  }
  public long getRecvCount()
  {
    return recvCount;
  }

  public void setPartPrice(double partPrice)
  {
    this.partPrice = partPrice;
  }
  public double getPartPrice()
  {
    return partPrice;
  }

  public void setPartFee(double partFee)
  {
    this.partFee = partFee;
  }
  public double getPartFee()
  {
    return partFee;
  }

  public void setEtId(long etId)
  {
    this.etId = etId;
  }
  public long getEtId()
  {
    return etId;
  }

  public void setEncryptionType(EncryptionType encryptionType)
  {
    this.encryptionType = encryptionType;
  }
  public EncryptionType getEncryptionType()
  {
    return encryptionType;
  }

  public void setPurchaseOrderParts(List parts)
  {
    this.parts = parts;
  }
  public List getPurchaseOrderParts()
  {
    return parts;
  }

  public PurchaseOrderPart getPoPartWithId(long poPartId)
  {
    for (Iterator i = parts.iterator(); i.hasNext();)
    {
      PurchaseOrderPart nextPart = (PurchaseOrderPart)i.next();
      if (nextPart.getPoPartId() == poPartId)
      {
        return nextPart;
      }
    }
    return null;
  }

  public String toString()
  {
    return "PurchaseOrderItem [ "
      + "id: " + poItemId
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + ", po: " + poId
      + ", part bucket: " + bucket
      + ", part class: " + partClass
      + ", count: " + partCount
      + ", price: " + partPrice
      + (encryptionType != null ? ", encryption: " + encryptionType : "")
      + ", fee: " + partFee
      + ", received: " + receivedCount
      + ", added: " + addedCount
      + " ]";
  }
}