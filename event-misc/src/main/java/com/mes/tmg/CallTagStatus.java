package com.mes.tmg;

public class CallTagStatus extends TmgBean
{
  public static final String CTS_NEW      = "NEW";
  public static final String CTS_CLOSED   = "CLOSED";
  public static final String CTS_SHIPPED  = "SHIPPED";
  public static final String CTS_CANCELED = "CANCELED";

  private long ctStatId;
  private String ctStatCode;
  private String description;

  public void setCtStatId(long ctStatId)
  {
    this.ctStatId = ctStatId;
  }
  public long getCtStatId()
  {
    return ctStatId;
  }

  public void setCtStatCode(String ctStatCode)
  {
    this.ctStatCode = ctStatCode;
  }
  public String getCtStatCode()
  {
    return ctStatCode;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public String toString()
  {
    return "CallTagStatus ["
      + " id: " + ctStatId
      + ", code: " + ctStatCode
      + ", desc: " + description
      + " ]";
  }
}