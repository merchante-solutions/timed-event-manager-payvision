package com.mes.tmg;

public class PartFeatureMap
{
  private long mapId;
  private long ptId;
  private long featId;
  private String ftCode;

  private PartFeature feature;
  
  public void setMapId(long mapId)
  {
    this.mapId = mapId;
  }
  public long getMapId()
  {
    return mapId;
  }

  public void setPtId(long ptId)
  {
    this.ptId = ptId;
  }
  public long getPtId()
  {
    return ptId;
  }
  
  public void setFeatId(long featId)
  {
    this.featId = featId;
  }
  public long getFeatId()
  {
    return featId;
  }

  public void setFeature(PartFeature feature)
  {
    this.feature = feature;
  }
  public PartFeature getFeature()
  {
    return feature;
  }
  
  public void setFtCode(String ftCode)
  {
    this.ftCode = ftCode;
  }
  public String getFtCode()
  {
    return ftCode;
  }
    
  public String toString()
  {
    return "PartFeatureMap [ "
      + "id: " + mapId 
      + ", part type: " + ptId
      + ", ft code: " + ftCode
      + ", feature: " + feature 
      + " ]";
  }
}