package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class TransferLookupRow extends TmgBean
{
  private long xferId;
  private Date createDate;
  private String fromName;
  private String toName;
  private String userName;
  private long xferPartId;
  private long partId;
  private long stateId;
  private String serialNum;
  private long quantity;
  private String modelCode;
  private String description;
  private String pcName;
  private String cancelFlag;
  private String canCancelFlag;

  public void setXferId(long xferId)
  {
    this.xferId = xferId;
  }
  public long getXferId()
  {
    return xferId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setFromName(String fromName)
  {
    this.fromName = fromName;
  }
  public String getFromName()
  {
    return fromName;
  }

  public void setToName(String toName)
  {
    this.toName = toName;
  }
  public String getToName()
  {
    return toName;
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setXferPartId(long xferPartId)
  {
    this.xferPartId = xferPartId;
  }
  public long getXferPartId()
  {
    return xferPartId;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }

  public void setStateId(long stateId)
  {
    this.stateId = stateId;
  }
  public long getStateId()
  {
    return stateId;
  }

  public void setQuantity(long quantity)
  {
    this.quantity = quantity;
  }
  public long getQuantity()
  {
    return quantity;
  }

  public void setModelCode(String modelCode)
  {
    this.modelCode = modelCode;
  }
  public String getModelCode()
  { 
    return modelCode;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }

  public void setPcName(String pcName)
  {
    this.pcName = pcName;
  }
  public String getPcName()
  {
    return pcName;
  }

  public void setCancelFlag(String cancelFlag)
  {
    validateFlag(cancelFlag,"cancel");
    this.cancelFlag = cancelFlag;
  }
  public String getCancelFlag()
  {
    return flagValue(cancelFlag);
  }
  public boolean isCanceled()
  {
    return flagBoolean(cancelFlag);
  }

  public void setCanCancelFlag(String canCancelFlag)
  {
    validateFlag(canCancelFlag,"can cancel");
    this.canCancelFlag = canCancelFlag;
  }
  public String getCanCancelFlag()
  {
    return flagValue(canCancelFlag);
  }
  public boolean canCancel()
  {
    return flagBoolean(canCancelFlag);
  }
}

