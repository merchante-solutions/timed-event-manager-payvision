package com.mes.tmg;

public class AcrCtrMap extends TmgBean
{
  private long            acmId;
  private String          atCode;
  private String          ctrCode;
  private CallTagReason   callTagReason;

  public void setAcmId(long acmId)
  {
    this.acmId = acmId;
  }
  public long getAcmId()
  {
    return acmId;
  }

  public void setAtCode(String atCode)
  {
    this.atCode = atCode;
  }
  public String getAtCode()
  {
    return atCode;
  }

  public void setCtrCode(String ctrCode)
  {
    this.ctrCode = ctrCode;
  }
  public String getCtrCode()
  {
    return ctrCode;
  }

  public void setCallTagReason(CallTagReason callTagReason)
  {
    this.callTagReason = callTagReason;
  }
  public CallTagReason getCallTagReason()
  { 
    return callTagReason;
  }

  public String toString()
  {
    return "AcrType ["
      + " id: " + acmId
      + ", acr type: " + atCode
      + ", acr action type: " + ctrCode
      + " ]";
  }
}