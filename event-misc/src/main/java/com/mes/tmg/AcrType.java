package com.mes.tmg;

import java.util.List;

public class AcrType extends TmgBean
{
  private long    atId;
  private String  atCode;
  private String  atName;
  private List    acrCtrMaps;

  public void setAtId(long atId)
  {
    this.atId = atId;
  }
  public long getAtId()
  {
    return atId;
  }

  public void setAtCode(String atCode)
  {
    this.atCode = atCode;
  }
  public String getAtCode()
  {
    return atCode;
  }

  public void setAtName(String atName)
  {
    this.atName = atName;
  }
  public String getAtName()
  {
    return atName;
  }

  public void setAcrCtrMaps(List acrCtrMaps)
  {
    this.acrCtrMaps = acrCtrMaps;
  }
  public List getAcrCtrMaps()
  {
    return acrCtrMaps;
  }

  public String toString()
  {
    return "AcrType ["
      + " id: " + atId
      + ", code: " + atCode
      + ", name: "+atName
      + " ]";
  }
}