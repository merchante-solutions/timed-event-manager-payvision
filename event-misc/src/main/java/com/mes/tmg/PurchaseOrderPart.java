package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class PurchaseOrderPart extends TmgBean
{
  private long            poPartId;
  private long            poItemId;
  private long            poId;
  private Date            createDate;
  private String          userName;
  private String          serialNum;
  private long            partCount;
  private long            partId;
  private double          partPrice;
  private double          partFee;
  private long            buckId;
  private String          pcCode;
  private long            etId;

  private Bucket          bucket;
  private PartClass       partClass;
  private EncryptionType  encryptionType;

  public PurchaseOrderPart()
  {
  }

  public void setPoPartId(long poPartId)
  {
    this.poPartId = poPartId;
  }
  public long getPoPartId()
  {
    return poPartId;
  }

  public void setPoItemId(long poItemId)
  {
    this.poItemId = poItemId;
  }
  public long getPoItemId()
  {
    return poItemId;
  }

  public void setPoId(long poId)
  {
    this.poId = poId;
  }
  public long getPoId()
  {
    return poId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }

  public void setPartCount(long partCount)
  {
    this.partCount = partCount;
  }
  public long getPartCount()
  {
    return partCount;
  }

  public void setPartPrice(double partPrice)
  {
    this.partPrice = partPrice;
  }
  public double getPartPrice()
  {
    return partPrice;
  }

  public void setPartFee(double partFee)
  {
    this.partFee = partFee;
  }
  public double getPartFee()
  {
    return partFee;
  }

  public void setBuckId(long buckId)
  {
    this.buckId = buckId;
  }
  public long getBuckId()
  {
    return buckId;
  }

  public void setBucket(Bucket bucket)
  {
    this.bucket = bucket;
  }
  public Bucket getBucket()
  {
    return bucket;
  }

  public void setPcCode(String pcCode)
  {
    this.pcCode = pcCode;
  }
  public String getPcCode()
  {
    return pcCode;
  }

  public void setPartClass(PartClass partClass)
  {
    this.partClass = partClass;
  }
  public PartClass getPartClass()
  {
    return partClass;
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }
  public boolean isAdded()
  {
    return partId > 0L;
  }

  public void setEtId(long etId)
  {
    this.etId = etId;
  }
  public long getEtId()
  {
    return etId;
  }

  public void setEncryptionType(EncryptionType encryptionType)
  {
    this.encryptionType = encryptionType;
  }
  public EncryptionType getEncryptionType()
  {
    return encryptionType;
  }

  public String toString()
  {
    return "PurchaseOrderPart [ "
      + "id: " + poPartId
      + ", item: " + poItemId
      + ", po: " + poId
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + ", bucket: " + bucket
      + ", class: " + partClass.getPcName()
      + (encryptionType != null ? ", enc. type: " + encryptionType.getEtName() : "")
      + ", price: " + partPrice
      + ", fee: " + partFee
      + ", s/n: " + serialNum
      + ", count: " + partCount
      + " ]";
  }
}