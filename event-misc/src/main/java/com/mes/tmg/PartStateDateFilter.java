package com.mes.tmg;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import com.mes.persist.AbstractFilter;
import com.mes.persist.TableDef;

public class PartStateDateFilter extends AbstractFilter
{
  private Timestamp targetTs;

  public PartStateDateFilter(Timestamp targetTs)
  {
    this.targetTs = targetTs;
    colName = "start_ts";
  }
  public PartStateDateFilter(java.util.Date targetDate)
  {
    this(new Timestamp(targetDate.getTime()));
  }

  public String getWhereClause(TableDef td, String qualifier)
  {
    String startTsName  = td.getColumnDef("start_ts").getQualifiedName(qualifier);
    String endTsName    = td.getColumnDef("end_ts").getQualifiedName(qualifier);
    return " ? >= " + startTsName
         + " and ( ? < " + endTsName + " or " + endTsName + " is null ) ";
  }

  public String getAuditLog()
  {
    String startTsName  = td.getColumnDef("start_ts").getQualifiedName(qualifier);
    String endTsName    = td.getColumnDef("end_ts").getQualifiedName(qualifier);
    return " " + targetTs + " >= " + startTsName
         + " and ( " + targetTs + " < " + endTsName + " or " 
         + endTsName + " is null ) ";
  }

  public int setMarks(PreparedStatement ps, int mark) throws SQLException
  {
    ps.setTimestamp(mark++,targetTs);
    ps.setTimestamp(mark++,targetTs);
    return mark;
  }
}