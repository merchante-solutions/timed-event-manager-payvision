package com.mes.tmg;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.TextareaField;
import com.mes.tmg.util.DocAction;
import com.oreilly.servlet.multipart.FilePart;


public class NoteBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(NoteBean.class);

  public static final String FN_NOTE_ID             = "noteId";
  public static final String FN_ID                  = "id";
  public static final String FN_NOTE_TEXT           = "noteText";
  public static final String FN_UPLOAD_FILE         = "uploadFile";
  public static final String FN_SAVE_BTN            = "save";
  public static final String FN_SAVE_AND_RETURN_BTN = "saveAndReturn";

  private Note note;

  public NoteBean(TmgAction action)
  {
    super(action);
  }
  
  /**
   * Initialize fields.
   */
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_NOTE_ID));
      fields.addAlias(FN_NOTE_ID,"editId");
      fields.add(new HiddenField(FN_ID));
      fields.add(new TextareaField(FN_NOTE_TEXT,"Note",2000,4,80,false));
      fields.add(new Field(FN_UPLOAD_FILE,"Upload File",32,32,true));
      fields.add(new ButtonField(FN_SAVE_BTN,"Save"));
      fields.add(new ButtonField(FN_SAVE_AND_RETURN_BTN,"Save & Return"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public Note getNote()
  {
    return note;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    boolean loadOk = false;
    
    try
    {
      long noteId = getField(FN_NOTE_ID).asLong();
      
      if (noteId != 0L)
      {
        note = db.getNote(noteId);
        setData(FN_NOTE_TEXT,note.getNoteText());
        setData(FN_ID,String.valueOf(note.getId()));
      }
        
      loadOk = true;
    }
    catch (Exception e)
    {
      log.error("Error loading note: " + e);
    }
    
    return loadOk;
  }

  private List receivedDocs;

  /**
   * Upload file attachments.
   */
  protected void receiveFile(FilePart part)
  {
    // create new document in db, upload the file data to it
    Document doc = DocAction.uploadNewDocFile(db,user,action.getName(),
      part.getFileName(),null,-1L,part.getInputStream());

    // add doc to the list of received documents
    if (receivedDocs == null)
    {
      receivedDocs = new ArrayList();
    }
    receivedDocs.add(doc);
  }
  
  /**
   * Persists note.
   */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    
    try
    {
      long noteId = getField(FN_NOTE_ID).asLong();
      
      if (noteId == 0L)
      {
        note = NoteAction.newNote(db,user,getField(FN_ID).asLong());
        noteId = note.getNoteId();
        setData(FN_NOTE_ID,String.valueOf(noteId));
      }
      else
      {
        note = db.getNote(noteId);
      }

      note.setNoteText(getData(FN_NOTE_TEXT));
      db.persist(note,user);

      // if files were uploaded, attach them to this note
      if (receivedDocs != null)
      {
        for (Iterator i = receivedDocs.iterator(); i.hasNext();)
        {
          DocAction.attachToDoc(db,user,action.getName(),(Document)i.next(),noteId);
        }
      }

      // reload note to get attached documents
      note = db.getNote(noteId);

      submitOk = true;
    }
    catch (Exception e)   
    {
      e.printStackTrace();
      log.error("Error submitting note: " + e);
    }

    return submitOk;
  }
}