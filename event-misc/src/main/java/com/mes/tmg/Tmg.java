package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;
import com.mes.mvc.mes.Mes;

public class Tmg extends Mes
{

  // testing
  public static final String ACTION_TEST                = "test";
  public static final String ACTION_TEST_AJAX           = "testAjax";

  public static final String VIEW_TEST                  = "test";


  // notes
  public static final String ACTION_ADD_NOTE            = "addNote";
  public static final String ACTION_EDIT_NOTE           = "editNote";
  public static final String ACTION_DELETE_NOTE_DOC     = "deleteNoteDoc";
  public static final String ACTION_SHOW_NOTES          = "showNotes";

  public static final String VIEW_ADD_NOTE              = "addNote";
  public static final String VIEW_EDIT_NOTE             = "editNote";
  public static final String VIEW_SHOW_NOTES            = "showNotes";


  // documents
  public static final String ACTION_DOWNLOAD_DOC        = "downloadDoc";


  // part feature type
  public static final String ACTION_SHOW_FEATURE_TYPES  = "showFeatureTypes";
  public static final String ACTION_ADD_FEATURE_TYPE    = "addFeatureType";
  public static final String ACTION_EDIT_FEATURE_TYPE   = "editFeatureType";
  public static final String ACTION_DELETE_FEATURE_TYPE = "deleteFeatureType";

  public static final String VIEW_SHOW_FEATURE_TYPES    = "showFeatureTypes";
  public static final String VIEW_EDIT_FEATURE_TYPE     = "editFeatureType";
  public static final String VIEW_ADD_FEATURE_TYPE      = "addFeatureType";


  // part feature
  public static final String ACTION_SHOW_FEATURES       = "showFeatures";
  public static final String ACTION_ADD_FEATURE         = "addFeature";
  public static final String ACTION_EDIT_FEATURE        = "editFeature";
  public static final String ACTION_DELETE_FEATURE      = "deleteFeature";

  public static final String VIEW_SHOW_FEATURES         = "showFeatures";
  public static final String VIEW_EDIT_FEATURE          = "editFeature";
  public static final String VIEW_ADD_FEATURE           = "addFeature";


  // category
  public static final String ACTION_SHOW_CATEGORIES     = "showCategories";
  public static final String ACTION_ADD_CATEGORY        = "addCategory";
  public static final String ACTION_EDIT_CATEGORY       = "editCategory";
  public static final String ACTION_DELETE_CATEGORY     = "deleteCategory";

  public static final String VIEW_SHOW_CATEGORIES       = "showCategories";
  public static final String VIEW_EDIT_CATEGORY         = "editCategory";
  public static final String VIEW_ADD_CATEGORY          = "addCategory";


  // part type
  public static final String ACTION_SHOW_PART_TYPES     = "showPartTypes";
  public static final String ACTION_ADD_PART_TYPE       = "addPartType";
  public static final String ACTION_EDIT_PART_TYPE      = "editPartType";
  public static final String ACTION_DELETE_PART_TYPE    = "deletePartType";

  public static final String VIEW_SHOW_PART_TYPES       = "showPartTypes";
  public static final String VIEW_EDIT_PART_TYPE        = "editPartType";
  public static final String VIEW_ADD_PART_TYPE         = "addPartType";


  // part
  public static final String SN_PART_LOOKUP_BEAN        = "partLookupBean";

  public static final String ACTION_SHOW_PARTS          = "showParts";
  public static final String ACTION_LOOKUP_PARTS        = "lookupParts";
  public static final String ACTION_DL_LOOKUP_PARTS     = "dlLookupParts";
  public static final String ACTION_ADD_PART            = "addPart";
  public static final String ACTION_EDIT_PART           = "editPart";
  public static final String ACTION_DELETE_PART         = "deletePart";
  public static final String ACTION_SHOW_PART           = "showPart";
  public static final String ACTION_OBSOLETE_PART       = "obsoletePart";
  public static final String ACTION_UNDO_OBSOLETE_PART  = "undoObsoletePart";
  public static final String ACTION_SELL_PART_AS_IS     = "sellPartAsIs";
  public static final String ACTION_UNDO_SELL_PART_AS_IS = "undoSellPartAsIs";
  public static final String ACTION_PART_LOST           = "partLost";
  public static final String ACTION_PART_FOUND          = "partFound";
  public static final String ACTION_EDIT_PART_SN        = "editPartSn";
  public static final String ACTION_EDIT_PART_MODEL     = "editPartModel";
  public static final String ACTION_CHANGE_PART_CLASS   = "changePartClass";
  public static final String ACTION_SHOW_PART_HISTORY   = "showPartHistory";
  public static final String ACTION_TESTING_OK          = "testingOk";
  public static final String ACTION_SET_TEST_RESULTS    = "setTestResults";
  public static final String ACTION_CHANGE_DISPOSITION  = "changeDisposition";
  public static final String ACTION_CHANGE_ENCRYPTION_TYPE
                                                        = "changeEncryptionType";
  public static final String ACTION_CHANGE_PART_PRICE   = "changePartPrice";
  public static final String ACTION_OUT_OF_BOX_FAILURE  = "outOfBoxFailure";
  public static final String ACTION_REMOVE_MERCH_OWNED  = "removeMerchOwned";
  public static final String ACTION_CANCEL_DEPLOYMENT   = "cancelDeployment";
  public static final String ACTION_NEEDS_INJECTION     = "needsInjection";

  public static final String VIEW_SHOW_PARTS            = "showParts";
  public static final String VIEW_LOOKUP_PARTS          = "lookupParts";
  public static final String VIEW_EDIT_PART             = "editPart";
  public static final String VIEW_ADD_PART              = "addPart";
  public static final String VIEW_SHOW_PART             = "showPart";
  public static final String VIEW_EDIT_PART_SN          = "editPartSn";
  public static final String VIEW_EDIT_PART_MODEL       = "editPartModel";
  public static final String VIEW_CHANGE_PART_CLASS     = "changePartClass";
  public static final String VIEW_PART_HISTORY          = "partHistory";
  public static final String VIEW_PART_DETAILS          = "partDetails";
  public static final String VIEW_CHANGE_ENCRYPTION_TYPE
                                                        = "changeEncryptionType";
  public static final String VIEW_CHANGE_PART_PRICE     = "changePartPrice";
  public static final String VIEW_SET_TEST_RESULTS      = "setTestResults";


  // add miscellaneous parts
  public static final String ACTION_AMP_MAIN            = "ampMain";
  public static final String ACTION_AMP_COMPLETE        = "ampComplete";
  public static final String ACTION_AMP_CLEAR           = "ampClear";
  public static final String ACTION_AMP_ADD_PART        = "ampAddPart";
  public static final String ACTION_AMP_EDIT_PART       = "ampEditPart";
  public static final String ACTION_AMP_COPY_PART       = "ampCopyPart";
  public static final String ACTION_AMP_DELETE_PART     = "ampDeletePart";
  public static final String ACTION_AMP_LOOKUP          = "ampLookup";
  public static final String ACTION_AMP_VIEW            = "ampView";
  public static final String ACTION_AMP_VIEW_PDTL       = "ampViewPdtl";

  public static final String ACTION_AMP_SHOW_SOURCES    = "ampShowSources";
  public static final String ACTION_AMP_ADD_SOURCE      = "ampAddSource";
  public static final String ACTION_AMP_EDIT_SOURCE     = "ampEditSource";
  public static final String ACTION_AMP_DELETE_SOURCE   = "ampDeleteSource";

  public static final String VIEW_AMP_MAIN              = "ampMain";
  public static final String VIEW_AMP_LOOKUP            = "ampLookup";
  public static final String VIEW_AMP_VIEW              = "ampView";

  public static final String VIEW_AMP_SHOW_SOURCES      = "ampShowSources";
  public static final String VIEW_AMP_ADD_SOURCE        = "ampAddSource";
  public static final String VIEW_AMP_EDIT_SOURCE       = "ampEditSource";

  // inventory transfers
  public static final String ACTION_XFER_PARTS          = "xferParts";
  public static final String ACTION_XFER_CANCEL         = "xferCancel";
  public static final String ACTION_XFER_PART_LOOKUP    = "xferPartLookup";
  public static final String ACTION_XFER_ADD_PART       = "xferAddPart";
  public static final String ACTION_XFER_REMOVE_PART    = "xferRemovePart";
  public static final String ACTION_XFER_COMPLETE       = "xferComplete";
  public static final String ACTION_XFER_LOOKUP         = "xferLookup";
  public static final String ACTION_XFER_VIEW           = "xferView";
  public static final String ACTION_XFER_CANCEL_PART    = "xferPartCancel";

  public static final String VIEW_XFER_START            = "xferStart";
  public static final String VIEW_XFER_MAIN             = "xferMain";
  public static final String VIEW_XFER_PART_LOOKUP      = "xferPartLookup";
  public static final String VIEW_XFER_LOOKUP           = "xferLookup";
  public static final String VIEW_XFER_VIEW             = "xferView";

  // condition
  public static final String ACTION_SHOW_CONDITIONS     = "showConditions";
  public static final String ACTION_ADD_CONDITION       = "addCondition";
  public static final String ACTION_EDIT_CONDITION      = "editCondition";
  public static final String ACTION_DELETE_CONDITION    = "deleteCondition";

  public static final String VIEW_SHOW_CONDITIONS       = "showConditions";
  public static final String VIEW_EDIT_CONDITION        = "editCondition";
  public static final String VIEW_ADD_CONDITION         = "addCondition";


  // disposition
  public static final String ACTION_SHOW_DISPOSITIONS   = "showDispositions";
  public static final String ACTION_ADD_DISPOSITION     = "addDisposition";
  public static final String ACTION_EDIT_DISPOSITION    = "editDisposition";
  public static final String ACTION_DELETE_DISPOSITION  = "deleteDisposition";

  public static final String VIEW_SHOW_DISPOSITIONS     = "showDispositions";
  public static final String VIEW_EDIT_DISPOSITION      = "editDisposition";
  public static final String VIEW_ADD_DISPOSITION       = "addDisposition";


  // part class
  public static final String ACTION_SHOW_PART_CLASSES   = "showPartClasses";
  public static final String ACTION_ADD_PART_CLASS      = "addPartClass";
  public static final String ACTION_EDIT_PART_CLASS     = "editPartClass";
  public static final String ACTION_DELETE_PART_CLASS   = "deletePartClass";

  public static final String VIEW_SHOW_PART_CLASSES     = "showPartClasses";
  public static final String VIEW_EDIT_PART_CLASS       = "editPartClass";
  public static final String VIEW_ADD_PART_CLASS        = "addPartClass";


  // location
  public static final String ACTION_SHOW_LOCATIONS      = "showLocations";
  public static final String ACTION_ADD_LOCATION        = "addLocation";
  public static final String ACTION_EDIT_LOCATION       = "editLocation";
  public static final String ACTION_DELETE_LOCATION     = "deleteLocation";

  public static final String VIEW_SHOW_LOCATIONS        = "showLocations";
  public static final String VIEW_EDIT_LOCATION         = "editLocation";
  public static final String VIEW_ADD_LOCATION          = "addLocation";


  // part status
  public static final String ACTION_SHOW_PART_STATUSES  = "showPartStatuses";
  public static final String ACTION_ADD_PART_STATUS     = "addPartStatus";
  public static final String ACTION_EDIT_PART_STATUS    = "editPartStatus";
  public static final String ACTION_DELETE_PART_STATUS  = "deletePartStatus";

  public static final String VIEW_SHOW_PART_STATUSES    = "showPartStatuses";
  public static final String VIEW_EDIT_PART_STATUS      = "editPartStatus";
  public static final String VIEW_ADD_PART_STATUS       = "addPartStatus";


  // encryption type
  public static final String ACTION_SHOW_ENCRYPT_TYPES  = "showEncryptTypes";
  public static final String ACTION_ADD_ENCRYPT_TYPE    = "addEncryptType";
  public static final String ACTION_EDIT_ENCRYPT_TYPE   = "editEncryptType";
  public static final String ACTION_DELETE_ENCRYPT_TYPE = "deleteEncryptType";

  public static final String VIEW_SHOW_ENCRYPT_TYPES    = "showEncryptTypes";
  public static final String VIEW_EDIT_ENCRYPT_TYPE     = "editEncryptType";
  public static final String VIEW_ADD_ENCRYPT_TYPE      = "addEncryptType";


  // admin
  public static final String ACTION_ADD_CONTACT         = "addContact";
  public static final String ACTION_EDIT_CONTACT        = "editContact";
  public static final String ACTION_DELETE_CONTACT      = "deleteContact";

  public static final String VIEW_ADD_CONTACT           = "addContact";
  public static final String VIEW_EDIT_CONTACT          = "editContact";
  public static final String VIEW_DELETE_CONTACT        = "deleteContact";

  public static final String ACTION_ADD_ADDRESS         = "addAddress";
  public static final String ACTION_EDIT_ADDRESS        = "editAddress";
  public static final String ACTION_DELETE_ADDRESS      = "deleteAddress";

  public static final String VIEW_ADD_ADDRESS           = "addAddress";
  public static final String VIEW_EDIT_ADDRESS          = "editAddress";
  public static final String VIEW_DELETE_ADDRESS        = "deleteAddress";

  public static final String ACTION_SHOW_TR_TYPES       = "showTestResultTypes";
  public static final String ACTION_EDIT_TR_TYPE        = "editTestResultType";
  public static final String ACTION_ADD_TR_TYPE         = "addTestResultType";
  public static final String ACTION_DELETE_TR_TYPE      = "deleteTestResultType";

  public static final String VIEW_SHOW_TR_TYPES         = "showTestResultTypes";
  public static final String VIEW_EDIT_TR_TYPE          = "editTestResultType";
  public static final String VIEW_ADD_TR_TYPE           = "addTestResultType";


  // acr admin
  public static final String ACTION_SHOW_CT_REASONS     = "showCallTagReasons";
  public static final String ACTION_ADD_CT_REASON       = "addCallTagReason";
  public static final String ACTION_EDIT_CT_REASON      = "editCallTagReason";
  public static final String ACTION_DELETE_CT_REASON    = "deleteCallTagReason";

  public static final String ACTION_SHOW_ACR_TYPES      = "showAcrTypes";
  public static final String ACTION_ADD_ACR_TYPE        = "addAcrType";
  public static final String ACTION_EDIT_ACR_TYPE       = "editAcrType";
  public static final String ACTION_DELETE_ACR_TYPE     = "deleteAcrType";

  public static final String VIEW_SHOW_CT_REASONS       = "showCallTagReasons";
  public static final String VIEW_ADD_CT_REASON         = "addCallTagReason";
  public static final String VIEW_EDIT_CT_REASON        = "editCallTagReason";

  public static final String VIEW_SHOW_ACR_TYPES        = "showAcrTypes";
  public static final String VIEW_ADD_ACR_TYPE          = "addAcrType";
  public static final String VIEW_EDIT_ACR_TYPE         = "editAcrType";


  // client admin
  public static final String ACTION_SHOW_CLIENTS        = "showClients";
  public static final String ACTION_ADD_CLIENT          = "addClient";
  public static final String ACTION_EDIT_CLIENT         = "editClient";
  public static final String ACTION_DELETE_CLIENT       = "deleteClient";

  public static final String ACTION_SHOW_CD_RULES       = "showCdRules";
  public static final String ACTION_EDIT_CD_RULE        = "editCdRule";
  public static final String ACTION_ADD_CD_RULE         = "addCdRule";
  public static final String ACTION_DELETE_CD_RULE      = "deleteCdRule";

  public static final String VIEW_SHOW_CLIENTS          = "showClients";
  public static final String VIEW_EDIT_CLIENT           = "editClient";
  public static final String VIEW_ADD_CLIENT            = "addClient";

  public static final String VIEW_SHOW_CD_RULES         = "showCdRules";
  public static final String VIEW_EDIT_CD_RULE          = "editCdRule";
  public static final String VIEW_ADD_CD_RULE           = "addCdRule";


  // inventory
  public static final String ACTION_SHOW_INVENTORIES    = "showInventories";
  public static final String ACTION_ADD_INVENTORY       = "addInventory";
  public static final String ACTION_EDIT_INVENTORY      = "editInventory";
  public static final String ACTION_DELETE_INVENTORY    = "deleteInventory";

  public static final String VIEW_SHOW_INVENTORIES      = "showInventories";
  public static final String VIEW_EDIT_INVENTORY        = "editInventory";
  public static final String VIEW_ADD_INVENTORY         = "addInventory";


  // bucket
  public static final String ACTION_SHOW_BUCKETS        = "showBuckets";
  public static final String ACTION_ADD_BUCKET          = "addBucket";
  public static final String ACTION_EDIT_BUCKET         = "editBucket";
  public static final String ACTION_DELETE_BUCKET       = "deleteBucket";

  public static final String VIEW_SHOW_BUCKETS          = "showBuckets";
  public static final String VIEW_EDIT_BUCKET           = "editBucket";
  public static final String VIEW_ADD_BUCKET            = "addBucket";


  // shipper/ship type
  public static final String ACTION_SHOW_SHIPPERS       = "showShippers";
  public static final String ACTION_ADD_SHIPPER         = "addShipper";
  public static final String ACTION_EDIT_SHIPPER        = "editShipper";
  public static final String ACTION_DELETE_SHIPPER      = "deleteShipper";

  public static final String VIEW_SHOW_SHIPPERS         = "showShippers";
  public static final String VIEW_EDIT_SHIPPER          = "editShipper";
  public static final String VIEW_ADD_SHIPPER           = "addShipper";

  public static final String ACTION_SHOW_SHIP_TYPES     = "showShipTypes";
  public static final String ACTION_ADD_SHIP_TYPE       = "addShipType";
  public static final String ACTION_EDIT_SHIP_TYPE      = "editShipType";
  public static final String ACTION_DELETE_SHIP_TYPE    = "deleteShipType";

  public static final String VIEW_SHOW_SHIP_TYPES       = "showShipTypes";
  public static final String VIEW_EDIT_SHIP_TYPE        = "editShipType";
  public static final String VIEW_ADD_SHIP_TYPE         = "addShipType";


  // vendor class
  public static final String ACTION_SHOW_VEND_CLASSES   = "showVendorClasses";
  public static final String ACTION_ADD_VEND_CLASS      = "addVendorClass";
  public static final String ACTION_EDIT_VEND_CLASS     = "editVendorClass";
  public static final String ACTION_DELETE_VEND_CLASS   = "deleteVendorClass";

  public static final String VIEW_SHOW_VEND_CLASSES     = "showVendorClasses";
  public static final String VIEW_EDIT_VEND_CLASS       = "editVendorClass";
  public static final String VIEW_ADD_VEND_CLASS        = "addVendorClass";


  // vendor
  public static final String ACTION_SHOW_VENDORS        = "showVendors";
  public static final String ACTION_ADD_VENDOR          = "addVendor";
  public static final String ACTION_EDIT_VENDOR         = "editVendor";
  public static final String ACTION_DELETE_VENDOR       = "deleteVendor";

  public static final String VIEW_SHOW_VENDORS          = "showVendors";
  public static final String VIEW_EDIT_VENDOR           = "editVendor";
  public static final String VIEW_ADD_VENDOR            = "addVendor";


  // purchase order
  public static final String SN_PO_LOOKUP_BEAN          = "poLookupBean";

  public static final String ACTION_LOOKUP_POS          = "lookupPos";
  public static final String ACTION_SHOW_POS            = "showPos";
  public static final String ACTION_ADD_PO              = "addPo";
  public static final String ACTION_EDIT_NEW_PO         = "editNewPo";
  public static final String ACTION_EDIT_PO             = "editPo";
  public static final String ACTION_VIEW_NEW_PO         = "viewNewPo";
  public static final String ACTION_VIEW_PO             = "viewPo";
  public static final String ACTION_CANCEL_PO           = "cancelPo";
  public static final String ACTION_UNCANCEL_PO         = "uncancelPo";
  public static final String ACTION_READY_PO            = "readyPo";
  public static final String ACTION_APPROVE_PO          = "approvePo";
  public static final String ACTION_CONFIRM_PO          = "confirmPo";
  public static final String ACTION_RECEIVE_PO          = "receivePo";
  public static final String ACTION_COMPLETE_PO         = "completePo";
  public static final String ACTION_PAY_PO              = "payPo";
  public static final String ACTION_DELETE_PO           = "deletePo";
  public static final String ACTION_SHOW_PO_LOGS        = "showPoLogs";


  public static final String ACTION_ADD_PO_ITEM         = "addPoItem";
  public static final String ACTION_EDIT_PO_ITEM        = "editPoItem";
  public static final String ACTION_CANCEL_PO_ITEM      = "cancelPoItem";
  public static final String ACTION_UNCANCEL_PO_ITEM    = "uncancelPoItem";

  public static final String ACTION_RECEIVE_PO_PARTS    = "receivePoParts";
  public static final String ACTION_DELETE_PO_PART      = "deletePoPart";

  public static final String ACTION_INV_PO_PART         = "poInvPoPart";
  public static final String ACTION_INV_PO_ITEM         = "poInvPoItem";
  public static final String ACTION_INV_PO_ALL          = "poInvPoAll";

  public static final String ACTION_ADD_PO_INVOICE      = "addPoInvoice";
  public static final String ACTION_EDIT_PO_INVOICE     = "editPoInvoice";

  public static final String ACTION_EDIT_PO_OPTIONS     = "editPoOptions";

  public static final String VIEW_LOOKUP_POS            = "lookupPos";
  public static final String VIEW_ADD_PO                = "addPo";
  public static final String VIEW_EDIT_PO               = "editPo";
  public static final String VIEW_VIEW_PO               = "viewPo";
  public static final String VIEW_SHOW_POS              = "showPos";

  public static final String VIEW_SHOW_PO_ITEMS         = "showPoItems";
  public static final String VIEW_ADD_PO_ITEM           = "addPoItem";
  public static final String VIEW_EDIT_PO_ITEM          = "editPoItem";

  public static final String VIEW_RECEIVE_PO_PARTS      = "receivePoParts";
  public static final String VIEW_RECEIVE_PO_QTY_PARTS  = "receivePoQtyParts";
  public static final String VIEW_RECEIVE_PO_SN_PARTS   = "receivePoSnParts";

  public static final String VIEW_ADD_PO_INVOICE        = "addPoInvoice";
  public static final String VIEW_EDIT_PO_INVOICE       = "editPoInvoice";

  public static final String VIEW_SHOW_LOGS             = "showLogs";

  public static final String VIEW_EDIT_PO_OPTIONS       = "editPoOptions";


  // audit
  public static final String SN_AUDIT_LOOKUP_BEAN       = "auditLookupBean";

  public static final String ACTION_NEW_AUDIT           = "newAudit";
  public static final String ACTION_LOOKUP_AUDITS       = "lookupAudits";
  public static final String ACTION_SHOW_AUDIT          = "showAudit";
  public static final String ACTION_SHOW_NEW_AUDIT      = "showNewAudit";
  public static final String ACTION_AUDIT_DETAILS       = "auditDetails";
  public static final String ACTION_AUDIT_EX_RESOLVE    = "auditExResolve";
  public static final String ACTION_COMPLETE_AUDIT      = "completeAudit";

  public static final String VIEW_NEW_AUDIT             = "newAudit";
  public static final String VIEW_LOOKUP_AUDITS         = "lookupAudits";
  public static final String VIEW_SHOW_AUDIT            = "showAudit";
  public static final String VIEW_AUDIT_DETAILS         = "auditDetails";
  public static final String VIEW_AUDIT_EX_RESOLVE      = "auditExResolve";


  // deployment admin
  public static final String ACTION_SHOW_ORDER_SOURCES  = "showOrderSources";
  public static final String ACTION_ADD_ORDER_SOURCE    = "addOrderSource";
  public static final String ACTION_EDIT_ORDER_SOURCE   = "editOrderSource";
  public static final String ACTION_DELETE_ORDER_SOURCE = "deleteOrderSource";

  public static final String ACTION_SHOW_ORDER_STATS    = "showOrderStatuses";
  public static final String ACTION_ADD_ORDER_STAT      = "addOrderStatus";
  public static final String ACTION_EDIT_ORDER_STAT     = "editOrderStatus";
  public static final String ACTION_DELETE_ORDER_STAT   = "deleteOrderStatus";

  public static final String ACTION_SHOW_DEPLOY_REASONS = "showDeployReasons";
  public static final String ACTION_ADD_DEPLOY_REASON   = "addDeployReason";
  public static final String ACTION_EDIT_DEPLOY_REASON  = "editDeployReason";
  public static final String ACTION_DELETE_DEPLOY_REASON = "deleteDeployReason";

  public static final String ACTION_SHOW_CT_NOTICE_TYPES
                                                        = "showCtNoticeTypes";
  public static final String ACTION_EDIT_CT_NOTICE_TYPE = "editCtNoticeType";
  public static final String ACTION_ADD_CT_NOTICE_TYPE  = "addCtNoticeType";
  public static final String ACTION_DELETE_CT_NOTICE_TYPE
                                                        = "deleteCtNoticeType";

  public static final String VIEW_SHOW_ORDER_SOURCES    = "showOrderSources";
  public static final String VIEW_EDIT_ORDER_SOURCE     = "editOrderSource";
  public static final String VIEW_ADD_ORDER_SOURCE      = "addOrderSource";

  public static final String VIEW_SHOW_ORDER_STATS      = "showOrderStatuses";
  public static final String VIEW_EDIT_ORDER_STAT       = "editOrderStatus";
  public static final String VIEW_ADD_ORDER_STAT        = "addOrderStatus";

  public static final String VIEW_SHOW_DEPLOY_REASONS   = "showDeployReasons";
  public static final String VIEW_EDIT_DEPLOY_REASON    = "editDeployReason";
  public static final String VIEW_ADD_DEPLOY_REASON     = "addDeployReason";

  public static final String VIEW_SHOW_CT_NOTICE_TYPES  = "showCtNoticeTypes";
  public static final String VIEW_EDIT_CT_NOTICE_TYPE   = "editCtNoticeType";
  public static final String VIEW_ADD_CT_NOTICE_TYPE    = "addCtNoticeType";


  // orders
  public static final String ACTION_ADD_ORDER           = "addOrder";
  public static final String ACTION_ADD_ORDER_FOR_MERCH = "addMerchOrder";
  public static final String ACTION_LOOKUP_ORDERS       = "lookupOrders";
  public static final String ACTION_SHOW_ORDER          = "viewOrder";
  public static final String ACTION_SHIP_ORDER          = "shipOrder";
  public static final String ACTION_COMPLETE_ORDER      = "completeOrder";
  public static final String ACTION_CANCEL_ORDER        = "cancelOrder";
  public static final String ACTION_UNCANCEL_ORDER      = "uncancelOrder";
  public static final String ACTION_PEND_ORDER          = "pendOrder";
  public static final String ACTION_UNPEND_ORDER        = "unpendOrder";

  // outgoing order parts
  public static final String ACTION_ADD_ORDER_OUT_PART  = "addOrderOutPart";
  public static final String ACTION_DELETE_ORDER_OUT_PART
                                                        = "deleteOrderOutPart";
  public static final String ACTION_CANCEL_ORDER_OUT_PART
                                                        = "cancelOrderOutPart";
  public static final String ACTION_UNCANCEL_ORDER_OUT_PART
                                                        = "uncancelOrderOutPart";
  public static final String ACTION_ASSIGN_ORDER_OUT_PART
                                                        = "assignOrderOutPart";
  public static final String ACTION_UNASSIGN_ORDER_OUT_PART
                                                        = "unassignOrderOutPart";

  // call tags
  public static final String ACTION_ADD_CALL_TAG        = "addCallTag";
  public static final String ACTION_EDIT_CALL_TAG       = "editCallTag";
  public static final String ACTION_SHIP_CALL_TAG       = "shipCallTag";
  public static final String ACTION_CLOSE_CALL_TAG      = "closeCallTag";
  public static final String ACTION_CANCEL_CALL_TAG     = "cancelCallTag";
  public static final String ACTION_UNCANCEL_CALL_TAG   = "uncancelCallTag";

  // call tag notices
  public static final String ACTION_ADD_CALL_TAG_NOTICE = "addCallTagNotice";
  public static final String ACTION_EDIT_CALL_TAG_NOTICE
                                                        = "editCallTagNotice";
  public static final String ACTION_DELETE_CALL_TAG_NOTICE
                                                        = "deleteCallTagNotice";
  public static final String ACTION_DOWNLOAD_NOTICE_PDF = "downloadNoticePdf";

  // call tag parts
  public static final String ACTION_ADD_CALL_TAG_PART   = "addCallTagPart";
  public static final String ACTION_EDIT_CALL_TAG_PART  = "editCallTagPart";
  public static final String ACTION_DELETE_CALL_TAG_PART 
                                                        = "deleteCallTagPart";
  public static final String ACTION_CANCEL_CALL_TAG_PART 
                                                        = "cancelCallTagPart";
  public static final String ACTION_UNCANCEL_CALL_TAG_PART 
                                                        = "uncancelCallTagPart";
  public static final String ACTION_RECEIVE_CALL_TAG_PART 
                                                        = "receiveCallTagPart";
  public static final String ACTION_RECEIVE_UNKNOWN_CALL_TAG_PART 
                                                        = "receiveUnknownCallTagPart";
  public static final String ACTION_RECEIVE_UNEXPECTED_CT_PART 
                                                        = "receiveUnexpectedCtPart";
  public static final String ACTION_FAIL_CALL_TAG_PART  = "failCallTagPart";

  // views
  public static final String VIEW_ADD_ORDER             = "addOrder";
  public static final String VIEW_LOOKUP_ORDERS         = "lookupOrders";
  public static final String VIEW_SHOW_ORDER            = "viewOrder";

  public static final String VIEW_ADD_ORDER_OUT_PART    = "addOrderOutPart";
  public static final String VIEW_ASSIGN_ORDER_OUT_PART = "assignOrderOutPart";

  public static final String VIEW_ADD_CALL_TAG          = "addCallTag";
  public static final String VIEW_EDIT_CALL_TAG         = "editCallTag";

  public static final String VIEW_ADD_CALL_TAG_NOTICE   = "addCallTagNotice";
  public static final String VIEW_EDIT_CALL_TAG_NOTICE  = "editCallTagNotice";

  public static final String VIEW_ADD_CALL_TAG_PART     = "addCallTagPart";
  public static final String VIEW_EDIT_CALL_TAG_PART    = "editCallTagPart";
  public static final String VIEW_RECEIVE_CALL_TAG_PART = "receiveCallTagPart";
  public static final String VIEW_RECEIVE_UNEXPECTED_CT_PART 
                                                        = "receiveUnexpectedCtPart";

  // reports
  public static final String ACTION_MAINT_REQS_REPORT   = "maintReqsReport";
  public static final String ACTION_DL_MAINT_REQS_REPORT
                                                        = "dlMaintReqsReport";
  public static final String ACTION_ON_HAND_REPORT      = "onHandReport";
  public static final String ACTION_DL_ON_HAND_REPORT   = "dlOnHandReport";
  public static final String ACTION_CALL_TAG_REPORT     = "callTagReport";
  public static final String ACTION_DL_CALL_TAG_REPORT  = "dlCallTagReport";
  public static final String ACTION_REPAIR_REPORT       = "repairReport";
  public static final String ACTION_DL_REPAIR_REPORT    = "dlRepairReport";
  public static final String ACTION_DEPLOYMENT_REPORT   = "deploymentReport";
  public static final String ACTION_DL_DEPLOYMENT_REPORT
                                                        = "dlDeploymentReport";
  public static final String ACTION_LOST_PARTS_REPORT   = "lostPartsReport";
  public static final String ACTION_DL_LOST_PARTS_REPORT
                                                        = "dlLostPartsReport";
  public static final String ACTION_INV_GROWTH_REPORT   = "invGrowthReport";
  public static final String ACTION_DL_INV_GROWTH_REPORT
                                                        = "dlInvGrowthReport";

  public static final String VIEW_MAINT_REQS_REPORT     = "maintReqsReport";
  public static final String VIEW_ON_HAND_REPORT        = "onHandReport";
  public static final String VIEW_CALL_TAG_REPORT       = "callTagReport";
  public static final String VIEW_REPAIR_REPORT         = "repairReport";
  public static final String VIEW_DEPLOYMENT_REPORT     = "deploymentReport";
  public static final String VIEW_LOST_PARTS_REPORT     = "lostPartsReport";
  public static final String VIEW_INV_GROWTH_REPORT     = "invGrowthReport";


  // operation codes
  public static final String OP_PART_ADD_ORDER          = "PART_ADD_ORDER";
  public static final String OP_PART_ADD_REPAIR_ORDER   = "PART_ADD_REPAIR_ORDER";
  public static final String OP_PART_SOLD_AS_IS         = "PART_SOLD_AS_IS";
  public static final String OP_PART_SOLD_AS_IS_UNDO    = "PART_SOLD_AS_IS_UNDO";
  public static final String OP_PART_CANCEL_REPAIR_ORDER= "PART_CANCEL_REPAIR_ORDER";
  public static final String OP_PART_CLASS_EDIT         = "PART_CLASS_EDIT";
  public static final String OP_PART_CLOSE_REPAIR_ORDER = "PART_CLOSE_REPAIR_ORDER";
  public static final String OP_PART_DEPLOY             = "PART_DEPLOY";
  public static final String OP_PART_DEPLOY_CANCEL      = "PART_DEPLOY_CANCEL";
  public static final String OP_PART_DISP_CHANGE        = "PART_DISP_CHANGE";
  public static final String OP_PART_ENCRYPT_TYPE_EDIT  = "PART_ENCRYPT_TYPE_EDIT";
  public static final String OP_PART_FOUND              = "PART_FOUND";
  public static final String OP_PART_LOST               = "PART_LOST";
  public static final String OP_PART_MAN_ADD            = "PART_MAN_ADD";
  public static final String OP_PART_MAN_EDIT           = "PART_MAN_EDIT";
  public static final String OP_PART_MISC_ADD           = "PART_MISC_ADD";
  public static final String OP_PART_MODEL_EDIT         = "PART_MODEL_EDIT";
  public static final String OP_PART_NEEDS_INJECTION    = "PART_NEEDS_INJECTION";
  public static final String OP_PART_OBSOLETE           = "PART_OBLT";
  public static final String OP_PART_OBSOLETE_UNDO      = "PART_OBLT_UNDO";
  public static final String OP_PART_OUT_OF_BOX_FAILURE = "PART_OUT_OF_BOX_FAILURE";
  public static final String OP_PART_PO_ADD             = "PART_PO_ADD";
  public static final String OP_PART_RECV_CALL_TAG      = "PART_RECV_CALL_TAG";
  public static final String OP_PART_RECV_TEST          = "PART_RECV_TEST";
  public static final String OP_PART_REMOVE_MERCH_OWNED = "PART_REMOVE_MERCH_OWNED";
  public static final String OP_PART_REMOVE_ORDER       = "PART_REMOVE_ORDER";
  public static final String OP_PART_REMOVE_REPAIR_ORDER= "PART_REMOVE_REPAIR_ORDER";
  public static final String OP_PART_SEND_REPAIR_ORDER  = "PART_SEND_REPAIR_ORDER";
  public static final String OP_PART_SN_EDIT            = "PART_SN_EDIT";
  public static final String OP_PART_TESTED             = "PART_TESTED";
  public static final String OP_PART_TEST_OK            = "PART_TEST_OK";
  public static final String OP_PART_UPDATE_REPAIR_ORDER= "PART_UPDATE_REPAIR_ORDER";
  public static final String OP_PART_XFER               = "PART_XFER";
  public static final String OP_PART_XFER_CANCEL        = "PART_XFER_CANCEL";
  public static final String OP_TESTING                 = "TESTING";

  //part feature type codes
  public static final String FC_PART_CATEGORY           = "CATEGORY";
  public static final String FC_SERIAL_NUM              = "SERIAL_NUM";
  public static final String FC_MANUFACTURER            = "MANUFACTURER";
  public static final String FC_GENERAL                 = "GENERAL";

  // repairs
  public static final String ACTION_REPAIR              = "repair";
  public static final String ACTION_REPAIR_START        = "repairStart";
  public static final String ACTION_REPAIR_LOOKUP       = "repairLookup";
  public static final String ACTION_REPAIR_VIEW         = "repairView";
  public static final String ACTION_REPAIR_VIEW_ITEM    = "repairViewItem";
  public static final String ACTION_REPAIR_ADD_ITEM     = "repairAddItem";
  public static final String ACTION_REPAIR_REMOVE_ITEM  = "repairRemoveItem";
  public static final String ACTION_REPAIR_UPDATE_ITEM  = "repairUpdateItem";
  public static final String ACTION_REPAIR_UPDATE       = "repairUpdate";
  public static final String ACTION_REPAIR_CANCEL       = "repairCancel";
  public static final String ACTION_REPAIR_REPORT_GEN   = "repairReportGen";

  public static final String VIEW_REPAIR_START          = "repairStart";
  public static final String VIEW_REPAIR_LOOKUP         = "repairLookup";
  public static final String VIEW_REPAIR_MAIN           = "repairView";
  public static final String VIEW_REPAIR_ITEM           = "repairViewItem";


  public static Date toDate(Timestamp ts)
  {
    return ts != null ? new Date(ts.getTime()) : null;
  }
  public static Timestamp toTimestamp(Date date)
  {
    return date != null ? new Timestamp(date.getTime()) : null;
  }
}
