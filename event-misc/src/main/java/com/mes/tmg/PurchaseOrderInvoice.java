package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class PurchaseOrderInvoice extends TmgBean
{
  private long poInvId;
  private long poId;
  private Date createDate;
  private String userName;
  private String orderNum;
  private String notes;
  private String invoiceNum;
  private Date invoiceUtilDate;
  private String shipCompany;
  private double shipCharge;
  private String trackingNum;

  public void setInvoiceNum(String invoiceNum)
  {
    this.invoiceNum = invoiceNum;
  }
  public String getInvoiceNum()
  {
    return invoiceNum;
  }

  public void setInvoiceDate(Timestamp invoiceDate)
  {
    invoiceUtilDate = Tmg.toDate(invoiceDate);
  }
  public Timestamp getInvoiceDate()
  {
    return Tmg.toTimestamp(invoiceUtilDate);
  }
  public void setInvoiceUtilDate(Date invoiceUtilDate)
  {
    this.invoiceUtilDate = invoiceUtilDate;
  }
  public Date getInvoiceUtilDate()
  {
    return invoiceUtilDate;
  }

  public void setShipCompany(String shipCompany)
  {
    this.shipCompany = shipCompany;
  }
  public String getShipCompany()
  {
    return shipCompany;
  }

  public void setShipCharge(double shipCharge)
  {
    this.shipCharge = shipCharge;
  }
  public double getShipCharge()
  {
    return shipCharge;
  }

  public void setTrackingNum(String trackingNum)
  {
    this.trackingNum = trackingNum;
  }
  public String getTrackingNum()
  {
    return trackingNum;
  }

  public void setPoInvId(long poInvId)
  {
    this.poInvId = poInvId;
  }
  public long getPoInvId()
  {
    return poInvId;
  }

  public void setPoId(long poId)
  {
    this.poId = poId;
  }
  public long getPoId()
  {
    return poId;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setOrderNum(String orderNum)
  {
    this.orderNum = orderNum;
  }
  public String getOrderNum()
  {
    return orderNum;
  }

  public void setNotes(String notes)
  {
    this.notes = notes;
  }
  public String getNotes()
  {
    return notes;
  }

  public String toString()
  {
    return "PurchaseOrderInvoice [ "
      + "id: " + poInvId
      + ", po: " + poId
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + ", order num: " + orderNum
      + ", notes: " + notes
      + ", inv. num: " + invoiceNum
      + ", inv. date: " + formatDate(invoiceUtilDate)
      + ", ship co.: " + shipCompany
      + ", ship charge: " + shipCharge
      + ", tracking num: " + trackingNum
      + " ]";
  }
}