package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class ActionLog extends TmgBean
{
  private long    actLogId;
  private long    id;
  private Date    createDate;
  private String  userName;
  private String  action;
  private String  description;

  public ActionLog()
  {
  }

  public void setActLogId(long actLogId)
  {
    this.actLogId = actLogId;
  }
  public long getActLogId()
  {
    return actLogId;
  }

  public void setId(long id)
  {
    this.id = id;
  }
  public long getId()
  {
    return id;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setAction(String action)
  {
    this.action = action;
  }
  public String getAction()
  {
    return action;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }
  public String getDescription()
  {
    return description;
  }

  public String toString()
  {
    return "ActionLog [ "
      + "log id: " + actLogId
      + ", id: " + id
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + ", action: " + action
      + ", description: " + description
      + " ]";
  }
}