package com.mes.tmg;

import org.apache.log4j.Logger;
import com.mes.user.UserBean;

public abstract class PersistPartTransaction extends TmgTransaction
{
  static Logger log = Logger.getLogger(PersistPartTransaction.class);

  protected Part part;
  protected PartOpCode poc;
  protected UserBean user;
  protected TmgDb db;

  public PersistPartTransaction(TmgDb db, UserBean user, Part part, PartOpCode poc)
  {
    super(db,user);
    this.part = part;
    this.poc = poc;
    this.user = user;
    this.db = db;
  }

  public abstract int executeOperation(PartOperation op);

  /**
   * Takes an op code and generates a new op object.  The op is logged and 
   * returned for use in transaction execution.
   */
  protected PartOperation generateOp(PartOpCode poc)
  {
    if (!runFlag)
    {
      throw new RuntimeException("Illegal op generation attempt");
    }

    PartOperation op = new PartOperation();
    op.setOpId(db.getNewId());
    op.setOpCode(poc.getOpCode());
    op.setTranId(tranId);
    op.setOpUser(user.getLoginName());
    db.persist(op,user);
    return op;
  }

  public int execute()
  {
    //PartOperation op = generateOp(poc);
    //tmgDb.persist(part,op,userBean);
    //return COMMIT;
    return executeOperation(generateOp(poc));
  }

  public Part getPart()
  {
    return part;
  }

  /**
   * Creates a part test record.  Use when receiving parts into testing
   * (see AmpTransaction and ReceiveCallTagPartTransaction).  The test id
   * needs to be set in the part object in order for this to work.
   *
   * HACK: this has to accept both ctPartId and maPartId in order to
   *       set the appropriate associated id in the test record.  Caller
   *       should set the unneeded id(s) to -1L...
   */
  protected void createPartTest(Part part, TmgAction action, long ctPartId, 
    long maPartId)
  {
    PartState ps = db.getCurrentPartState(part.getPartId());
    PartTest test = new PartTest();
    test.setTestId(part.getTestId());
    test.setCreateDate(db.getCurDate());
    test.setUserName(user.getLoginName());
    test.setSerialNum(part.getSerialNum());
    test.setPartId(part.getPartId());
    test.setStateId(ps.getStateId());
    if (ctPartId > 0L)
    {
      test.setCtPartId(ctPartId);
    }
    if (maPartId > 0L)
    {
      test.setMaPartId(maPartId);
    }
    db.persist(test,user);
    db.logAction(action,user,"Part test record " + test.getTestId()
      + " created",test.getTestId());
  }
}
