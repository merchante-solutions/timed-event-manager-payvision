package com.mes.tmg;

public class ClientDeployRule extends TmgBean
{
  private long cdRuleId;
  private long clientId;
  private long appType;
  private long deployToInvId;
  private long pullFromInvId;

  private Client client;
  private AppTypeRef appTypeRef;
  private Inventory deployToInv;
  private Inventory pullFromInv;

  public void setCdRuleId(long cdRuleId)
  {
    this.cdRuleId = cdRuleId;
  }
  public long getCdRuleId()
  {
    return cdRuleId;
  }

  public void setClientId(long clientId)
  {
    this.clientId = clientId;
  }
  public long getClientId()
  {
    return clientId;
  }
  public void setClient(Client client)
  {
    this.client = client;
  }
  public Client getClient()
  {
    return client;
  }

  public void setAppType(long appType)
  {
    this.appType = appType;
  }
  public long getAppType()
  {
    return appType;
  }
  public void setAppTypeRef(AppTypeRef appTypeRef)
  {
    this.appTypeRef = appTypeRef;
  }
  public AppTypeRef getAppTypeRef()
  {
    return appTypeRef;
  }

  public void setDeployToInvId(long deployToInvId)
  {
    this.deployToInvId = deployToInvId;
  }
  public long getDeployToInvId()
  {
    return deployToInvId;
  }
  public void setDeployToInv(Inventory deployToInv)
  {
    this.deployToInv = deployToInv;
  }
  public Inventory getDeployToInv()
  {
    return deployToInv;
  }

  public void setPullFromInvId(long pullFromInvId)
  {
    this.pullFromInvId = pullFromInvId;
  }
  public long getPullFromInvId()
  {
    return pullFromInvId;
  }
  public void setPullFromInv(Inventory pullFromInv)
  {
    this.pullFromInv = pullFromInv;
  }
  public Inventory getPullFromInv()
  {
    return pullFromInv;
  }

  public String toString()
  {
    return "ClientDeployRule [ "
      + "rule: " + cdRuleId
      + "client: " + client.getClientName()
      + ", app type: " + appType
      + ", deploy to: " + deployToInv.getInvName()
      + ( pullFromInv != null ? ", pull from: " + pullFromInv.getInvName() : "")
      + " ]";
  }
}