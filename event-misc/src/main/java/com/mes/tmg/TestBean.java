package com.mes.tmg;

public class TestBean
{
  private long id;
  private String someData;

  public long getId()
  {
    return id;
  }
  public void setId(long id)
  {
    this.id = id;
  }

  public String getSomeData()
  {
    return someData;
  }
  public void setSomeData(String someData)
  {
    this.someData = someData;
  }

  public String toString()
  {
    return "TestBean ["
      + " id: " + id
      + " data: " + someData
      + " ]";
  }
}