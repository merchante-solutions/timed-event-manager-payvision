package com.mes.tmg;

import java.util.List;

public class PurchaseOrderOptions extends TmgBean
{
  private long    optionId;
  private double  largeAmount;
  private long    contactId;
  private List    contacts;
  private long    addressId;
  private List    addresses;

  public void setOptionId(long optionId)
  {
    this.optionId = optionId;
  }
  public long getOptionId()
  {
    return optionId;
  }

  public void setLargeAmount(double largeAmount)
  {
    this.largeAmount = largeAmount;
  }
  public double getLargeAmount()
  {
    return largeAmount;
  }

  public void setContactId(long contactId)
  {
    this.contactId = contactId;
  }
  public long getContactId()
  {
    return contactId;
  }

  public void setContacts(List contacts)
  {
    this.contacts = contacts;
  }
  public List getContacts()
  {
    return contacts;
  }

  public void setAddressId(long addressId)
  {
    this.addressId = addressId;
  }
  public long getAddressId()
  {
    return addressId;
  }

  public void setAddresses(List addresses)
  {
    this.addresses = addresses;
  }
  public List getAddresses()
  {
    return addresses;
  }

  public String toString()
  {
    return "PurchaseOrderOptions [ "
      + "id: " + optionId
      + ", large amount: " + largeAmount
      + ", contact id: " + contactId
      + ", address id: " + addressId
      + " ]";
  }
}