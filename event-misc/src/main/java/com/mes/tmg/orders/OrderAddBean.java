package com.mes.tmg.orders;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class OrderAddBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(OrderAddBean.class);

  public static final String FN_SEARCH_TERM = "searchTerm";
  public static final String FN_SUBMIT_BTN  = "submitBtn";


  public OrderAddBean(TmgAction action)
  {
    super(action);
  }


  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new Field(FN_SEARCH_TERM,"Search Term",30,20,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  private List merchantRefs;

  public List getMerchantRefs()
  {
    return merchantRefs != null ? merchantRefs : new ArrayList();
  }
  public List getRows()
  {
    return getMerchantRefs();
  }

  public boolean autoSubmit()
  {
    try
    {
      merchantRefs = db.getMerchantRefs(fields.getData(FN_SEARCH_TERM));
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit: " + e);
    }
    return false;
  }

  protected boolean showFeedback()
  {
    return false;
  }
}
