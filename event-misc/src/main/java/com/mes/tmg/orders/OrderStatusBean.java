package com.mes.tmg.orders;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.tmg.OrderStatus;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class OrderStatusBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(OrderStatusBean.class);
  
  public static final String FN_ORD_STAT_ID   = "ordStatId";
  public static final String FN_ORD_STAT_CODE = "ordStatCode";
  public static final String FN_DESCRIPTION   = "description";
  public static final String FN_ORDER_IDX     = "orderIdx";
  
  public static final String FN_SUBMIT_BTN    = "submit";
  
  private OrderStatus orderStatus;

  public OrderStatusBean(TmgAction action)
  {
    super(action);
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_ORD_STAT_ID));
      fields.addAlias(FN_ORD_STAT_ID,"editId");
      fields.add(new Field(FN_ORD_STAT_CODE,"Code",32,32,false));
      fields.add(new Field(FN_DESCRIPTION,"Name",100,32,false));
      fields.add(new NumberField(FN_ORDER_IDX,"Order Index",5,32,true,0));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public OrderStatus getOrderStatus()
  {
    return orderStatus;
  }
  
  /**
   * Persists part condition.
   */
  protected boolean autoSubmit()
  {
    try
    {
      long ordStatId = getField(FN_ORD_STAT_ID).asLong();
      if (ordStatId == 0L)
      {
        ordStatId = db.getNewId();
        orderStatus = new OrderStatus();
        orderStatus.setOrdStatId(ordStatId);
      }
      else
      {
        orderStatus = db.getOrderStatus(ordStatId);
      }
      orderStatus.setOrdStatCode(getData(FN_ORD_STAT_CODE));
      orderStatus.setDescription(getData(FN_DESCRIPTION));
      orderStatus.setOrderIdx(getField(FN_ORDER_IDX).asInt());
      db.persist(orderStatus,user);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error submitting order status: " + e);
    }
    return false;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    try
    {
      long ordStatId = getField(FN_ORD_STAT_ID).asLong();
      if (ordStatId != 0L)
      {
        orderStatus = db.getOrderStatus(ordStatId);
        setData(FN_ORD_STAT_CODE,orderStatus.getOrdStatCode());
        setData(FN_DESCRIPTION,orderStatus.getDescription());
        setData(FN_ORDER_IDX,String.valueOf(orderStatus.getOrderIdx()));
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error loading order status: " + e);
    }
    return false;
  }
}