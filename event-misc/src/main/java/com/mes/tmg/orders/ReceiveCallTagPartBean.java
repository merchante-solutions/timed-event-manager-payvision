package com.mes.tmg.orders;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.Validation;
import com.mes.persist.Transaction;
import com.mes.tmg.CallTagPart;
import com.mes.tmg.Deployment;
import com.mes.tmg.Order;
import com.mes.tmg.Part;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.PartTypeTable;

public class ReceiveCallTagPartBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(ReceiveCallTagPartBean.class);

  public static final String FN_CT_PART_ID      = "ctPartId";
  public static final String FN_RECV_PT_ID      = "recvPtId";
  public static final String FN_RECV_SERIAL_NUM = "recvSerialNum";
  public static final String FN_RECV_PART_ID    = "recvPtId";
  public static final String FN_MO_FLAG         = "moFlag";
  public static final String FN_PART_PRICE      = "partPrice";
  public static final String FN_SUBMIT_BTN      = "submitBtn";

  private CallTagPart ctPart;
  private Order order;
  private Part recvPart;

  public CallTagPart getCtPart()
  {
    if (ctPart == null)
    {
      long ctPartId = getField(FN_CT_PART_ID).asLong();
      ctPart = db.getCallTagPart(ctPartId);
    }
    return ctPart;
  }

  public Order getOrder()
  {
    getCtPart();
    if (order == null && ctPart != null)
    {
      order = db.getOrder(ctPart.getOrdId());
    }
    return order;
  }

  public ReceiveCallTagPartBean(TmgAction action)
  {
    super(action);
  }

  public class SerialNumVal implements Validation
  {
    private String errorText;

    public String getErrorText()
    {
      return errorText;
    }

    // determine if the received serial number is valid:
    // must either be a new unknown serial number or 
    // it must be deployed to the order merchant
    public boolean validate(String serialNum)
    {
      // is this a known part?
      Part part = recvPart;
      if (part == null)
      {
        part = db.getPartWithSerialNum(serialNum);
        recvPart = part;
      }
      if (part != null)
      {
        // is part deployed to this merchant?
        Order order = getOrder();
        long depId = part.getDeployId();
        Deployment dep = null;
        if (depId != 0L)
        {
          dep = db.getDeployment(depId);
        }

        // part is not deployed at all
        if (dep == null)
        {
          errorText = "Serial number is not deployed to this merchant";
          return false;
        }

        // part is deployed to another merchant
        if (!dep.getMerchNum().equals(order.getMerchNum()))
        {
          errorText = "Serial No. is deployed to another merchant (" 
            + dep.getMerchName() + ")";
          return false;
        }
      }

      return true;
    }
  }

  public class MoFlagVal implements Validation
  {
    private String errorText;

    public String getErrorText()
    {
      return errorText;
    }

    // moFlag can be set to true only if serial number is unknown
    public boolean validate(String moFlag)
    {
      Part part = recvPart;
      if (part == null)
      {
        part = db.getPartWithSerialNum(getData(FN_RECV_SERIAL_NUM));
        recvPart = part;
      }
      if (part != null && moFlag.equals("y"))
      {
        errorText = "Serial no. in system, part can't be flagged merchant owned";
        return false;
      }
      return true;
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_CT_PART_ID));
      fields.add(new DropDownField(FN_RECV_PT_ID,new PartTypeTable(db,TmgDropDownTable.OPT_SEL_ONE),false));
      fields.add(new Field(FN_RECV_SERIAL_NUM,"Serial No.",64,32,false));
      fields.getField(FN_RECV_SERIAL_NUM).addValidation(new SerialNumVal());
      fields.add(new CheckField(FN_MO_FLAG,"Flag unknown part as merchant owned","y",true));
      fields.getField(FN_MO_FLAG).addValidation(new MoFlagVal());
      fields.add(new CurrencyField(FN_PART_PRICE,"Part Price",10,10,false));
      fields.getField(FN_PART_PRICE).setOptionalCondition(
        new com.mes.forms.Condition() {
          public boolean isMet() { return ((CheckField)fields.getField(FN_MO_FLAG)).isChecked(); };
        } );
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      long ctPartId = getField(FN_CT_PART_ID).asLong();
      CallTagPart ctPart = db.getCallTagPart(ctPartId);

      // if receive part data fields are empty, default them to
      // the requested values
      if (getData(FN_RECV_PT_ID).equals(""))
      {
        setData(FN_RECV_PT_ID,String.valueOf(ctPart.getPtId()));
      }

      String sn = ctPart.getSerialNum();
      String recvSn = getData(FN_RECV_SERIAL_NUM);
      if (recvSn.equals(""))
      {
        setData(FN_RECV_SERIAL_NUM,ctPart.getSerialNum());
      }

      if (getData(FN_PART_PRICE).equals(""))
      {
        if (recvSn.equals("") && sn != null)
        {
          Part part = db.getPartWithSerialNum(sn);
          if (part != null)
          {
            setData(FN_PART_PRICE,String.valueOf(part.getPartPrice().getPartPrice()));
          }
        }
        else if (!recvSn.equals("") && !recvSn.equals(sn))
        {
          Part part = db.getPartWithSerialNum(recvSn);
          if (part != null)
          {
            setData(FN_PART_PRICE,String.valueOf(part.getPartPrice().getPartPrice()));
          }
        }
      }

      return true;
    }
    catch (Exception e)
    {
      log.error("Error loading: " + e);
    }
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      // fetch call tag part and order
      CallTagPart ctPart = getCtPart();
      Order order = getOrder();

      // set receive data
      ctPart.setRecvSerialNum(getData(FN_RECV_SERIAL_NUM));
      ctPart.setRecvPtId(getField(FN_RECV_PT_ID).asLong());
      ctPart.setMoFlag(getData(FN_MO_FLAG));
      ctPart.setPartPrice(getField(FN_PART_PRICE).asDouble());

      // receive the part into testing
      Transaction t = new ReceiveCallTagPartTransaction(db,user,
        (TmgAction)action,ctPart);
      if (!t.run())
      {
        throw new RuntimeException("Failed to receive call tag part");
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error submitting: " + e);
    }
    return false;
  }
}
