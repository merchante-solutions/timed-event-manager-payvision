package com.mes.tmg.orders;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.tmg.CallTag;
import com.mes.tmg.CallTagPart;
import com.mes.tmg.Deployment;
import com.mes.tmg.Order;
import com.mes.tmg.OrderOutPart;
import com.mes.tmg.Part;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PartState;
import com.mes.tmg.PartType;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public class DeployPartsTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(DeployPartsTransaction.class);

  private TmgAction action;
  private Order order;

  public DeployPartsTransaction(TmgDb db, UserBean user, TmgAction action, 
    Order order)
  {
    super(db,user,null,db.getOpCode(Tmg.OP_PART_DEPLOY));
    this.action = action;
    this.order = order;
  }
  public DeployPartsTransaction(UserBean user, TmgAction action,  Order order)
  {
    this(new TmgDb(),user,action,order);
  }

  private void deployOutgoingParts(PartOperation op)
  {
    // create deployment record for parts, set part state to deployed
    for (Iterator i = order.getOrderOutParts().iterator(); i.hasNext(); )
    {
      OrderOutPart oop = (OrderOutPart)i.next();

      log.debug("deploying out part: " + oop);

      // skip over canceled outgoing parts
      if (oop.isCanceled())
      {
        continue;
      }

      // see if part was deployed and then returned via call tag on this order...
      if (order.receivedPart(oop.getPartId()))
      {
        continue;
      }

      // fetch the part, if it's already deployed (shipped earlier)
      // don't deploy it again...
      Part part = db.getPart(oop.getPartId());
      if (part.isDeployed())
      {
        continue;
      }

      // create deployment record for part
      Deployment dep = new Deployment();
      dep.setDeployId(db.getNewId());
      dep.setStartOrdId(oop.getOrdId());
      dep.setStartUserName(user.getLoginName());
      dep.setStartDate(db.getCurDate());
      dep.setStartCode(oop.getDrCode());
      dep.setMerchNum(order.getMerchNum());
      dep.setClientId(order.getClientId());

      // change part state to show deployed
      part.setStatusCode(part.SC_OUT);
      part.setDispCode(oop.getAssignedDispCode());
      part.setLocCode(part.LC_MERCHANT); // may need to support non-merchant locations...
      part.setDeployId(dep.getDeployId());
      if (part.getDispCode().equals(part.DC_SOLD))
      {
        part.setOwnerMerchNum(order.getMerchNum());
      }
      db.changePart(part,op,user);
      db.logAction(action,user,"Part " + part.getPartId() 
        + " deployed for order " + order.getOrdId(),part.getPartId());

      // load new part state, link it to the deploy rec
      PartState ps = db.getCurrentPartState(part.getPartId());
      dep.setPartId(ps.getPartId());
      dep.setStateId(ps.getStateId());
      db.persist(dep,user);
    }
  }

  private void validateCallTagStatus()
  {
    // see if call tag is present in order
    CallTag ct = order.getCurrentCallTag();
    if (ct != null && !ct.isShipped() && !ct.isClosed() &&!ct.isCanceled())
    {
      throw new RuntimeException(
        "Cannot deploy parts for order, call tag has not been shipped");
    }
  }

  private void updateOrderStatus()
  {
    // update the order status
    if (order.hasCallTag() && !order.getCurrentCallTag().isClosed() && 
        !order.getCurrentCallTag().isCanceled())
    {
      // an incomplete uncanceled call tag means order 
      // can be shipped but not yet completed
      order.ship();
    }
    else
    {
      // everything is done, complete
      order.complete();
    }
    db.persist(order,user);
    db.logAction(action,user,"Order " + order.getOrdId() 
      + (order.hasCallTag() ? " shipped" : " completed"),order.getOrdId());

    // check for received call tag parts   
    if (order.isCompleted() && order.hasCallTag() && !order.hasEmptyCallTag())
    {
      // look for received call tag parts and generate service call note text
      List ctParts = order.getCurrentCallTag().getCallTagParts();
      StringBuffer buf = new StringBuffer();
      for (Iterator i = ctParts.iterator(); i.hasNext();)
      {
        CallTagPart ctPart = (CallTagPart)i.next();
        if (ctPart.isReceived())
        {
          if (buf.length() == 0)
          {
            buf.append("Call tag order " + order.getOrdId() 
              + " complete, parts received: ");
          }
          else
          {
            buf.append(", ");
          }
          PartType pt = ctPart.getRecvPartType();
          buf.append(pt.getModelCode() + " " + pt.getPtName());
          buf.append(" Ser. # " + ctPart.getRecvSerialNum());
        }
      }

      // create service call if a note was generated
      if (buf.length() > 0)
      {
        db.createServiceCall(order.getMerchNum(),""+buf,user);
      }
    }

  }

  public int executeOperation(PartOperation op)
  {
    // make sure call tag, if present, has been shipped
    validateCallTagStatus();

    // create deployment records for outgoing parts
    deployOutgoingParts(op);

    // update order to either shipped with call tag open or complete
    updateOrderStatus();

    return COMMIT;
  }
}
