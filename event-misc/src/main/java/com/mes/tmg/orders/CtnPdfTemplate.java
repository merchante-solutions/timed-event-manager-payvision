package com.mes.tmg.orders;

import java.net.URL;
import java.util.Calendar;
import java.util.Iterator;
import org.apache.log4j.Logger;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.mes.support.DateTimeFormatter;
import com.mes.tmg.CallTag;
import com.mes.tmg.CallTagNotice;
import com.mes.tmg.CallTagPart;
import com.mes.tmg.OrderOutPart;
import com.mes.tmg.PartType;
import com.mes.tmg.TmgDb;
import com.mes.tmg.pdf.PdfDataProcessor;
import com.mes.tmg.pdf.PdfTemplate;

public class CtnPdfTemplate extends PdfTemplate
{
  static Logger log = Logger.getLogger(CtnPdfTemplate.class);

  private CallTag ct;
  private CallTagNotice cn;
  private URL logoUrl;
  private TmgDb db = new TmgDb();

  public CtnPdfTemplate(CallTagNotice cn, CallTag ct, URL logoUrl)
  {
    this.cn = cn;
    this.ct = ct;
    this.logoUrl = logoUrl;
  }

  public class CtnDataProcessor implements PdfDataProcessor
  {
    public String processData(String data)
    {
      String pd = data;
      if (data != null && data.startsWith("@"))
      {
        if (data.equals("@ctparts"))
        {
          StringBuffer buf = new StringBuffer();
          for (Iterator i = ct.getCallTagParts().iterator(); i.hasNext();)
          {
            CallTagPart ctPart = (CallTagPart)i.next();
            if (buf.length() > 0) buf.append("\n\n");
            buf.append(ctPart.getPartType().getDescription() + "\n");
            buf.append("Ser. # ");
            if (ctPart.getSerialNum().length() != 0)
            {
              buf.append(ctPart.getSerialNum());
            }
            else
            {
              buf.append("Unknown");
            }
          }
          pd = buf.toString();
        }
        else if (data.equals("@outparts"))
        {
          StringBuffer buf = new StringBuffer();
          java.util.List parts = db.getOrderOutPartsForOrder(ct.getOrdId());
          for (Iterator i = parts.iterator(); i.hasNext();)
          {
            OrderOutPart part = (OrderOutPart)i.next();
            if (buf.length() > 0) buf.append("\n\n");
            PartType at = part.getAssignedType();
            if (at != null)
            {
              buf.append(at.getDescription() + "\n");
              buf.append("Ser. # ");
              if (part.getSerialNum().length() != 0)
              {
                buf.append(part.getSerialNum());
              }
              else
              {
                buf.append("Unknown");
              }
            }
          }
          pd = buf.toString();
        }
      }
      return pd;
    }
  }

  public PdfDataProcessor getDataProcessor()
  {
    return new CtnDataProcessor();
  }

  protected void addHeader(Document doc) throws Exception
  {
    // logo
    if (logoUrl != null)
    {
      Image logo = Image.getInstance(logoUrl);

      logo.setAbsolutePosition(48,743);
      logo.setAlignment(Image.UNDERLYING);

      doc.add(logo);
    }

    // get font from body
    Font font = new Font(body.getFont());
    float leading = body.getFontSize() + 1;

    // mes address and date, upper right corner
    String addr = "920 N Argonne, Suite 200\n"
                + "Spokane, WA 99212\n";
    String date = DateTimeFormatter.getFormattedDate(
      Calendar.getInstance().getTime(),"MMMMM d, yyyy");
    Paragraph p = new Paragraph(addr + date,font);
    p.setAlignment(p.ALIGN_RIGHT);
    p.setLeading(leading);
    doc.add(p);

    // merchant num, left
    p = new Paragraph(cn.getMifRef().getDbaName(),font);
    p.setLeading(leading + 50);
    doc.add(p);

    // merch name
    p = new Paragraph(""+cn.getMifRef().getMerchantNumber(),font);
    p.setLeading(leading);
    doc.add(p);

    // tracking number
    if (ct.getTrackingNum() != null)
    {
      p = new Paragraph("\nFed-Ex Tracking ",font);
      Font font2 = new Font(font);
      font2.setStyle("bold");
      p.add(new Chunk("#" + ct.getTrackingNum(),font2));
      doc.add(p);
    }
  }
}