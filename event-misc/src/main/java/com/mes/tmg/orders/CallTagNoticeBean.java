package com.mes.tmg.orders;

import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.CallTag;
import com.mes.tmg.CallTagNotice;
import com.mes.tmg.MifRef;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.pdf.PdfTemplate;
import com.mes.tmg.pdf.PdfTemplateParser;

public class CallTagNoticeBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(CallTagNoticeBean.class);
  
  public static final String FN_CN_ID       = "cnId";
  public static final String FN_CT_ID       = "ctId";

  public static final String FN_NOTICE_TYPE = "noticeType";
  public static final String FN_TEMPLATE    = "template";
  public static final String FN_MERCH_NUM   = "merchNum";
  public static final String FN_MERCH_NAME  = "merchName";
  
  public static final String FN_ADDR1       = "addrLine1";
  public static final String FN_ADDR2       = "addrLine2";
  public static final String FN_CSZ         = "csz";
  public static final String FN_CSZ_CITY    = FN_CSZ + "City";
  public static final String FN_CSZ_STATE   = FN_CSZ + "State";
  public static final String FN_CSZ_ZIP     = FN_CSZ + "Zip";

  public static final String FN_TMP_DATA    = "tmpData";

  public static final String FN_SUBMIT_BTN  = "submit";

  private CallTagNotice ctNotice;
  private CallTag       callTag;
  private PdfTemplate   template;
  private List          tmpFields;

  public CallTagNoticeBean(TmgAction action, CallTag callTag)
  {
    super(action);
    this.callTag = callTag;
  }

  public List getTmpFields()
  {
    return tmpFields;
  }

  // loads the call tag's associated call tag type and 
  // generates fields for the pdf template
  private void createTemplateFields(HttpServletRequest request)
  {
    CallTagNotice cn = null;
    CallTag ct = null;
    try
    {
      cn = db.getCallTagNotice(
            Long.parseLong(request.getParameter(FN_CN_ID)));
      ct = db.getCallTag(
            Long.parseLong(request.getParameter(FN_CT_ID)));
    }
    catch(Exception le)
    {
      log.error("Unable to create template fields: " + le);
      le.printStackTrace();
      throw new RuntimeException(
        "Unable to create template fields: " + le);
    }

    PdfTemplateParser parser 
      = new PdfTemplateParser(action.getHandler().getServletContext());
    template = new CtnPdfTemplate(cn,ct,null);
    parser.parseXmlData(cn.getNoticeData(),template);
    tmpFields = template.getFields();
    int count = 1;
    for (Iterator i = tmpFields.iterator(); i.hasNext(); ++count)
    {
      Field tmpField = (Field)i.next();
      fields.add(tmpField);
      fields.addAlias(tmpField.getName(),FN_TMP_DATA + count);
      log.debug(tmpField.getLabel() + " tmpField.getData() = " + tmpField.getData());
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new HiddenField(FN_CN_ID));
      fields.addAlias(FN_CN_ID,"editId");
      fields.add(new HiddenField(FN_CT_ID));
      //fields.add(new Field(FN_ADDR1,"Line 1",40,32,false));
      //fields.add(new Field(FN_ADDR2,"Line 2",40,32,true));
      //fields.add(new CityStateZipField(FN_CSZ,"Line 2",32,false));
      fields.add(new HiddenField(FN_ADDR1));
      fields.add(new HiddenField(FN_ADDR2));
      fields.add(new HiddenField(FN_CSZ_CITY));
      fields.add(new HiddenField(FN_CSZ_STATE));
      fields.add(new HiddenField(FN_CSZ_ZIP));
      fields.add(new HiddenField(FN_TEMPLATE));
      fields.add(new HiddenField(FN_NOTICE_TYPE));
      fields.add(new HiddenField(FN_MERCH_NUM));
      fields.add(new HiddenField(FN_MERCH_NAME));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));

      createTemplateFields(request);

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public CallTagNotice getCallTagNotice()
  {
    return ctNotice;
  }

  public CallTag getCallTag()
  {
    return callTag;
  }

  protected boolean autoSubmit()
  {
    try
    {
      long cnId = getField(FN_CN_ID).asLong();
      ctNotice = db.getCallTagNotice(cnId);
      ctNotice.setAddrLine1(getData(FN_ADDR1));
      ctNotice.setAddrLine2(getData(FN_ADDR2));
      ctNotice.setCity(getData(FN_CSZ_CITY));
      ctNotice.setState(getData(FN_CSZ_STATE));
      ctNotice.setZip(getData(FN_CSZ_ZIP));
      ctNotice.setNoticeData(template.toString());
      db.persist(ctNotice,user);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error submitting: " + e);
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      long cnId = getField(FN_CN_ID).asLong();
      ctNotice = db.getCallTagNotice(cnId);
      MifRef mr = ctNotice.getMifRef();
      setData(FN_NOTICE_TYPE,ctNotice.getCallTagNoticeType().getDescription());
      setData(FN_TEMPLATE,ctNotice.getCallTagNoticeType().getTemplate());
      setData(FN_MERCH_NUM,ctNotice.getMerchNum());
      setData(FN_MERCH_NAME,(mr != null ? mr.getDbaName() : ""));
      setData(FN_ADDR1,ctNotice.getAddrLine1());
      setData(FN_ADDR2,ctNotice.getAddrLine2());
      setData(FN_CSZ_CITY,ctNotice.getCity());
      setData(FN_CSZ_STATE,ctNotice.getState());
      setData(FN_CSZ_ZIP,ctNotice.getZip());
      return true;
    }
    catch (Exception e)
    {
      log.error("Error loading: " + e);
    }
    return false;
  }
}