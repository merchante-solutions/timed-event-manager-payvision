package com.mes.tmg.orders;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.ClientTable;
import com.mes.tmg.util.OrderSourceTable;
import com.mes.tmg.util.OrderStatusTable;

public class OrderLookupBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(OrderLookupBean.class);

  public static final String FN_FROM_DATE     = "fromDate";
  public static final String FN_TO_DATE       = "toDate";
  public static final String FN_CLIENT_ID     = "clientId";
  public static final String FN_ORD_SRC_CODE  = "ordSrcCode";
  public static final String FN_ORD_STAT_CODE = "ordStatCode";
  public static final String FN_SEARCH_TERM   = "searchTerm";
  public static final String FN_SUBMIT_BTN    = "submitBtn";

  private List orders;

  public OrderLookupBean(TmgAction action)
  {
    super(action,"orderLookupBean");
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new DateStringField(FN_FROM_DATE,"From Date",false,false));
      fields.add(new DateStringField(FN_TO_DATE,"To Date",true,false));
      fields.add(new DropDownField(FN_CLIENT_ID,new ClientTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
      fields.add(new DropDownField(FN_ORD_SRC_CODE,new OrderSourceTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
      fields.add(new DropDownField(FN_ORD_STAT_CODE,new OrderStatusTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
      fields.add(new Field(FN_SEARCH_TERM,"Search Term",30,20,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

  public List getRows()
  {
    if (orders == null || (isAutoSubmit() && !isAutoValid()))
    {
      orders = new ArrayList();
    }
    return orders;
  }

  protected boolean autoSubmit()
  {
    setData(FN_SUBMIT_BTN,"");
    try
    {
      // HACK: get around the character conversion 
      // turning '+' into space in request parms
      if (getData(FN_ORD_STAT_CODE).equals("!DONE !CAN !PEND"))
      {
        setData(FN_ORD_STAT_CODE,"!DONE+!CAN+!PEND");
      }
      Date fromDate = ((DateStringField)getField(FN_FROM_DATE)).getUtilDate();
      Date toDate = ((DateStringField)getField(FN_TO_DATE)).getUtilDate();
      long clientId = getField(FN_CLIENT_ID).asLong();
      String ordSrcCode = getData(FN_ORD_SRC_CODE);
      ordSrcCode = ordSrcCode.equals("any") ? "" : ordSrcCode;
      String ordStatCode = getData(FN_ORD_STAT_CODE);
      ordStatCode = ordStatCode.equals("any") ? "" : ordStatCode;
      String searchTerm = getData(FN_SEARCH_TERM);
      orders = db.lookupOrders(fromDate,toDate,clientId,ordSrcCode,ordStatCode,searchTerm);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error submitting order lookup: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      Calendar cal = Calendar.getInstance();
      DateStringField fromField = (DateStringField)fields.getField(FN_FROM_DATE);
      if (fromField.isBlank())
      {
        DateStringField toField = (DateStringField)fields.getField(FN_TO_DATE);
        toField.setUtilDate(cal.getTime());
        cal.add(cal.DATE,-7);
        fromField.setUtilDate(cal.getTime());
      }
      if (getField(FN_ORD_STAT_CODE).isBlank())
      {
        setData(FN_ORD_STAT_CODE,"!DONE+!CAN+!PEND");
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}
