package com.mes.tmg.orders;

import org.apache.log4j.Logger;
import com.mes.tmg.OrderOutPart;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public class DeleteOrderOutPartTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(DeleteOrderOutPartTransaction.class);

  private TmgAction tmgAction;
  private OrderOutPart oop;

  public DeleteOrderOutPartTransaction(TmgDb db, UserBean user, TmgAction tmgAction,
    OrderOutPart oop)
  {
    super(db,user,db.getPart(oop.getPartId()),
      db.getOpCode(Tmg.OP_PART_REMOVE_ORDER));
    this.tmgAction = tmgAction;
    this.oop = oop;
  }
  public DeleteOrderOutPartTransaction(UserBean user, TmgAction tmgAction, 
    OrderOutPart oop)
  {
    this(new TmgDb(),user,tmgAction,oop);
  }

  public int executeOperation(PartOperation op)
  {
    // if there was a part assigned to the outgoing part, un-busy it
    if (part != null)
    {
      part.setStatusCode(part.SC_IN);
      db.changePart(part,op,user);
      long ordId = oop.getOrdId();
      db.logAction(tmgAction,user,"Part " + part.getPartId()
        + " removed from order " + ordId,part.getPartId());
      db.logAction(tmgAction,user,"Part " + part.getPartId()
        + " removed from order " + ordId,ordId);
    }

    // delete the outgoing order part
    db.deleteOrderOutPart(oop.getOopId(),user);

    return COMMIT;
  }
}
