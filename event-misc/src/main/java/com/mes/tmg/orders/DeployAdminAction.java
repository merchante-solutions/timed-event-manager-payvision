package com.mes.tmg.orders;

import org.apache.log4j.Logger;
import com.mes.tmg.CallTagNoticeType;
import com.mes.tmg.DeployReason;
import com.mes.tmg.OrderSource;
import com.mes.tmg.OrderStatus;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;

public class DeployAdminAction extends TmgAction
{
  static Logger log = Logger.getLogger(DeployAdminAction.class);

  public String getActionObject(String actionName)
  {
    if (actionName.matches(".*DeployReasons?"))
    {
      return "Deployment reason";
    }
    else if (actionName.matches(".*OrderStatus(es)?"))
    {
      return "Order status";
    }
    else if (actionName.matches(".*OrderSources?"))
    {
      return "Order source";
    }
    else if (actionName.matches(".*CtNoticeTypes?"))
    {
      return "CallTag notice type";
    }
    return super.getActionObject(actionName);
  }

  /**
   * Order sources
   */

  public void doShowOrderSources()
  {
    request.setAttribute("showItems",db.getOrderSources());
    doView(Tmg.VIEW_SHOW_ORDER_SOURCES);
  }

  public void doEditOrderSource()
  {
    OrderSourceBean bean = new OrderSourceBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_ORDER_SOURCE);
    if (bean.isAutoSubmitOk())
    {
      String description = bean.getOrderSource().getDescription();
      long ordSrcId = bean.getOrderSource().getOrdSrcId();
      logAndRedirect(Tmg.ACTION_SHOW_ORDER_SOURCES,description,ordSrcId);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_ORDER_SOURCE : Tmg.VIEW_ADD_ORDER_SOURCE);
    }
  }

  public void doAddOrderSource()
  {
    doEditOrderSource();
  }

  public void doDeleteOrderSource()
  {
    long delId = getParmLong("delId");
    OrderSource orderSource = db.getOrderSource(delId);
    if (orderSource == null)
    {
      throw new RuntimeException("Order source with id " + delId
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deleteOrderSource(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_ORDER_SOURCES,orderSource.getDescription(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_ORDER_SOURCES,"Order source not deleted");
    }
    else
    {
      confirm("Delete order source '" + orderSource.getDescription() + "'?");
    }
  }

  /**
   * Order statuses
   */

  public void doShowOrderStatuses()
  {
    request.setAttribute("showItems",db.getOrderStatuses());
    doView(Tmg.VIEW_SHOW_ORDER_STATS);
  }

  public void doEditOrderStatus()
  {
    OrderStatusBean bean = new OrderStatusBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_ORDER_STAT);
    if (bean.isAutoSubmitOk())
    {
      String description = bean.getOrderStatus().getDescription();
      long ordStatId = bean.getOrderStatus().getOrdStatId();
      logAndRedirect(Tmg.ACTION_SHOW_ORDER_STATS,description,ordStatId);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_ORDER_STAT : Tmg.VIEW_ADD_ORDER_STAT);
    }
  }

  public void doAddOrderStatus()
  {
    doEditOrderStatus();
  }

  public void doDeleteOrderStatus()
  {
    long delId = getParmLong("delId");
    OrderStatus orderStatus = db.getOrderStatus(delId);
    if (orderStatus == null)
    {
      throw new RuntimeException("Order status with id " + delId
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deleteOrderStatus(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_ORDER_STATS,orderStatus.getDescription(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_ORDER_STATS,"Order status not deleted");
    }
    else
    {
      confirm("Delete order status '" + orderStatus.getDescription() + "'?");
    }
  }

  /**
   * Deploy reasons
   */

  public void doShowDeployReasons()
  {
    request.setAttribute("showItems",db.getDeployReasons());
    doView(Tmg.VIEW_SHOW_DEPLOY_REASONS);
  }

  public void doEditDeployReason()
  {
    DeployReasonBean bean = new DeployReasonBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_DEPLOY_REASON);
    if (bean.isAutoSubmitOk())
    {
      String description = bean.getDeployReason().getDescription();
      long drId = bean.getDeployReason().getDrId();
      logAndRedirect(Tmg.ACTION_SHOW_DEPLOY_REASONS,description,drId);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_DEPLOY_REASON : Tmg.VIEW_ADD_DEPLOY_REASON);
    }
  }

  public void doAddDeployReason()
  {
    doEditDeployReason();
  }

  public void doDeleteDeployReason()
  {
    long delId = getParmLong("delId");
    DeployReason deployReason = db.getDeployReason(delId);
    if (deployReason == null)
    {
      throw new RuntimeException("Deployment reason with id " + delId
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deleteDeployReason(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_DEPLOY_REASONS,deployReason.getDescription(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_DEPLOY_REASONS,"Deployment reason not deleted");
    }
    else
    {
      confirm("Delete deployment reason '" + deployReason.getDescription() + "'?");
    }
  }

  /**
   * CallTag notice types
   */

  public void doShowCtNoticeTypes()
  {
    request.setAttribute("showItems",db.getCallTagNoticeTypes());
    doView(Tmg.VIEW_SHOW_CT_NOTICE_TYPES);
  }

  public void doEditCtNoticeType()
  {
    CalltagNoticeTypeBean bean = new CalltagNoticeTypeBean(this);
    boolean isEdit = name.equals(Tmg.ACTION_EDIT_CT_NOTICE_TYPE);
    if (bean.isAutoSubmitOk())
    {
      String description = bean.getCallTagNoticeType().getDescription();
      long cntId = bean.getCallTagNoticeType().getCntId();
      logAndRedirect(Tmg.ACTION_SHOW_CT_NOTICE_TYPES,description,cntId);
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_CT_NOTICE_TYPE : Tmg.VIEW_ADD_CT_NOTICE_TYPE);
    }
  }

  public void doAddCtNoticeType()
  {
    doEditCtNoticeType();
  }

  public void doDeleteCtNoticeType()
  {
    long delId = getParmLong("delId");
    CallTagNoticeType cnt = db.getCallTagNoticeType(delId);
    if (cnt == null)
    {
      throw new RuntimeException("CallTag notie type with id " + delId
        + " does not exist");
    }
    else if (isConfirmed())
    {
      db.deleteCallTagNoticeType(delId,user);
      logAndRedirect(Tmg.ACTION_SHOW_CT_NOTICE_TYPES,cnt.getDescription(),delId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(Tmg.ACTION_SHOW_CT_NOTICE_TYPES,"CallTag notice type not deleted");
    }
    else
    {
      confirm("Delete callTag notice type reason '" + cnt.getDescription() + "'?");
    }
  }
}
