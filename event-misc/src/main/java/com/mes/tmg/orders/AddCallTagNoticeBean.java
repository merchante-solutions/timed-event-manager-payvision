package com.mes.tmg.orders;

import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.HiddenField;
import com.mes.tmg.CallTagNotice;
import com.mes.tmg.CallTagNoticeType;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;

public class AddCallTagNoticeBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(AddCallTagNoticeBean.class);
  
  public static final String FN_CN_ID       = "cnId";
  public static final String FN_CT_ID       = "ctId";
  public static final String FN_MERCH_NUM   = "merchNum";
  public static final String FN_CNT_CODE    = "cntCode";
  
  public static final String FN_SUBMIT_BTN  = "submit";
  
  private CallTagNotice cn;

  public AddCallTagNoticeBean(TmgAction action)
  {
    super(action);
  }
  
  public class CallTagNoticeTypeTable extends TmgDropDownTable
  {
    public CallTagNoticeTypeTable()
    {
      super(OPT_SEL_ONE);
    }

    public void loadElements()
    {
      List cntList = db.getCallTagNoticeTypes();
      for (Iterator i = cntList.iterator(); i.hasNext(); )
      {
        CallTagNoticeType cnt = (CallTagNoticeType)i.next();
        addElement(cnt.getCntCode(),cnt.getDescription());
      }
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_CN_ID));
      fields.addAlias(FN_CN_ID,"editId");
      fields.add(new HiddenField(FN_CT_ID));
      fields.add(new HiddenField(FN_MERCH_NUM));
      fields.add(new DropDownField(FN_CNT_CODE,new CallTagNoticeTypeTable(),false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public CallTagNotice getCallTagNotice()
  {
    return cn;
  }

  protected boolean autoSubmit()
  {
    try
    {
      long cnId = getField(FN_CN_ID).asLong();
      CallTagNoticeType cnt = null;
      if (cnId == 0L)
      {
        cnId = db.getNewId();
        cn = new CallTagNotice();
        cn.setCnId(cnId);
        cn.setCtId(getField(FN_CT_ID).asLong());
        cn.setCreateDate(db.getCurDate());
        cn.setUserName(user.getLoginName());
        cn.setMerchNum(getData(FN_MERCH_NUM));
        cnt = db.getCallTagNoticeType(getData(FN_CNT_CODE));
        log.debug("cnt = " + cnt);
      }
      else
      {
        cn = db.getCallTagNotice(cnId);
        cnt = cn.getCallTagNoticeType();
      }

      cn.setCntCode(getData(FN_CNT_CODE));
      cn.setNoticeData(cnt.getTemplate());
      db.persist(cn,user);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error submitting: " + e);
      e.printStackTrace();
    }
    return false;
  }
  
  protected boolean autoLoad()
  {
    try
    {
      long cnId = getField(FN_CN_ID).asLong();
      if (cnId != 0L)
      {
        cn = db.getCallTagNotice(cnId);
        setData(FN_CT_ID,String.valueOf(cn.getCtId()));
        setData(FN_CNT_CODE,cn.getCntCode());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error loading: " + e);
    }
    return false;
  }
}