package com.mes.tmg.orders;

import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.mvc.Link;
import com.mes.persist.Transaction;
import com.mes.tmg.CallTag;
import com.mes.tmg.CallTagNotice;
import com.mes.tmg.CallTagPart;
import com.mes.tmg.Order;
import com.mes.tmg.OrderOutPart;
import com.mes.tmg.Part;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.tmg.inventory.XferTransaction;
import com.mes.tmg.pdf.PdfTemplate;
import com.mes.tmg.pdf.PdfTemplateParser;

public class OrderAction extends TmgAction
{
  static Logger log = Logger.getLogger(OrderAction.class);

  public String getActionObject(String actionName)
  {
    if (actionName.matches(".*Orders?"))
    {
      return "Order";
    }
    else if (actionName.matches(".*OrderParts?"))
    {
      return "Order part";
    }
    else if (actionName.matches(".*OrderOutParts?"))
    {
      return "Outgoing order part";
    }
    else if (actionName.matches(".*CallTags?"))
    {
      return "Call tag";
    }
    return super.getActionObject(actionName);
  }

  public String getActionResult(String actionName)
  {
    if (actionName.matches(".*ssign.*"))
    {
      return "updated";
    }
    return super.getActionResult(actionName);
  }

  private void deployOrderParts(Order order)
  {
    // run complete tran, update order status to call tagged
    Transaction t = new DeployPartsTransaction(user,this,order);
    if (!t.run())
    {
      throw new RuntimeException("Failed to deploy parts for order");
    }

    // auto transfer any outgoing parts that need it
    List xferParts = new ArrayList();
    for (Iterator i = order.getOrderOutParts().iterator(); i.hasNext(); )
    {
      OrderOutPart oop = (OrderOutPart)i.next();

      // skip over canceled outgoing parts
      if (oop.isCanceled())
      {
        continue;
      }

      Part part = db.getPart(oop.getPartId());
      if (part.getInventory().getInvId() != order.getDeployToInv().getInvId())
      {
        if (part.getInventory().getInvId() != order.getPullFromInv().getInvId())
        {
          throw new RuntimeException("Can't auto transfer parts for order, " + part 
            + " is not in client's pull from inv, " + order.getPullFromInv());
        }
        xferParts.add(part);
        log.debug("adding oop to xfer list: " + oop);
      }
    }

    // HACK: if error occurs there's no way to roll back the deployment
    // transaction...need a way to string transactions together...
    if (xferParts.size() > 0)
    {
      // it is assumed here that the parts are
      XferTransaction t2 = new XferTransaction(new TmgDb(),user,this,xferParts,
        order.getPullFromInv(),order.getDeployToInv());
      if (!t2.run())
      {
        throw new RuntimeException("Auto transfer failed in order");
      }
    }
  }

  private void shipCallTag(CallTag ct)
  {
    // make sure the call tag isn't already shipped
    if (ct.isShipped())
    {
      return;
    }

    // pend call tag parts
    for (Iterator i = ct.getCallTagParts().iterator(); i.hasNext();)
    {
      CallTagPart ctPart = (CallTagPart)i.next();

      // don't pend ct parts already pended 
      // (may be recovering from partial pended failed state)
      if (!ctPart.isPending())
      {
        ctPart.pend();
        db.persist(ctPart,user);
      }
    }

    // ship the call tag
    ct.ship();
    db.persist(ct,user);
  }
  private void shipCallTag(long ctId)
  {
    shipCallTag(db.getCallTag(getParmLong("ctId")));
  }

  /**
   * Order actions
   */

  public void doAddMerchOrder()
  {
    String merchNum = getParm("merchNum",null);
    if (merchNum != null)
    {
      setBackLink(Tmg.ACTION_SHOW_ORDER,handler.generateLink("deployMenu",
        null,"Back to Deployment"));
      String srcType = getParm("srcType",null);
      Order order = null;
      if (srcType == null)
      {
        order = db.createInternalOrder(merchNum,user);
      }
      else if (srcType.equals("OLA"))
      {
        order = db.createOlaOrder(merchNum,user);
        if (order == null)
        {
          redirectWithFeedback(getBackLink(),
            "OLA order not created, no equipment detected for order");
          return;
        }
      }
      else if (srcType.equals("ACR"))
      {
        redirectWithFeedback("deployMenu","ACR order generation is under constrution");
        return;
      }
      long ordId = order.getOrdId();
      Link link = handler.getLink(Tmg.ACTION_SHOW_ORDER,"ordId=" + ordId);
      logAndRedirect(link,ordId);
    }
    else
    {
      redirectWithFeedback("deployMenu","Error adding order for null merch num");
    }
  }

  public void doAddOrder()
  {
    OrderAddBean bean = new OrderAddBean(this);
    if (bean.isAutoSubmitOk())
    {
      request.setAttribute("merchRefs",bean.getMerchantRefs());
      doView(Tmg.VIEW_ADD_ORDER);
    }
    else
    {
      doView(Tmg.VIEW_ADD_ORDER);
    }
  }

  public static final String SN_ORDER_LOOKUP_BEAN = "orderLookupBean";

  private OrderLookupBean getOrderLookupBean()
  {
    OrderLookupBean bean 
      = (OrderLookupBean)getSessionAttr(SN_ORDER_LOOKUP_BEAN,null);
    if (bean == null)
    {
      bean = new OrderLookupBean(this);
      setSessionAttr(SN_ORDER_LOOKUP_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doLookupOrders()
  {
    OrderLookupBean bean = getOrderLookupBean();
    request.setAttribute("viewBean",bean);
    doView(Tmg.VIEW_LOOKUP_ORDERS);
  }

  public void doViewOrder()
  {
    Order order = db.getOrder(getParmLong("ordId"));

    // update the worked by info, set to working if this is new
    order.updateWorkedBy(user.getLoginName());
    db.persist(order,user); 
    order = db.getOrder(order.getOrdId());

    request.setAttribute("order",order);
    request.setAttribute("deployedParts",
      db.getDeployedPartsForMerchant(order.getMerchNum(),order.getCreateDate()));
    request.setAttribute("ctsMgr",db.getCallTagStatusManager(order.getOrdId()));

    setNoteItemId(order.getOrdId());

    doView(Tmg.VIEW_SHOW_ORDER,"Order No. " + order.getOrdId());
  }

  public void doPendOrder()
  {
    Order order = db.getOrder(getParmLong("ordId"));
    order.pend();
    db.persist(order,user);
    redirectWithFeedback(getBackLink(),"Order pended");
  }

  public void doUnpendOrder()
  {
    Order order = db.getOrder(getParmLong("ordId"));
    order.work();
    db.persist(order,user);
    redirectWithFeedback(getBackLink(),"Order unpended");
  }

  public void doCancelOrder()
  {
    Order order = db.getOrder(getParmLong("ordId"));
    if (isConfirmed())
    {
      CancelOrderTransaction t = new CancelOrderTransaction(user,this,order);
      if (!t.run())
      {
        throw new RuntimeException("Failed to cancel order");
      }
      redirectWithFeedback(getBackLink(),"Order " + order.getOrdId()
        + " canceled");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Order not canceled");
    }
    else
    {
      confirm("Cancel order and release all assigned parts?");
    }
  }

  public void doUncancelOrder()
  {
    Order order = db.getOrder(getParmLong("ordId"));
    order.work();
    db.persist(order,user);
    for (Iterator i = order.getCallTags().iterator(); i.hasNext();)
    {
      CallTag ct = (CallTag)i.next();
      if (ct.canUncancel())
      {
        ct.uncancel();
        db.persist(ct,user);
      }
    }
    redirectWithFeedback(getBackLink(),"Order uncanceled");
  }

  public void doCompleteOrder()
  {
    Order order = db.getOrder(getParmLong("ordId"));

    if (isConfirmed())
    {
      deployOrderParts(order);
      redirectWithFeedback(getBackLink(),"Order " + order.getOrdId()
        + " completed");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Order not completed");
    }
    else
    {
      confirm("Complete order"
        + (order.hasOutParts() ? ", deploy outgoing parts" : "") + "?");
    }
  }

  public void doShipOrder()
  {
    Order order = db.getOrder(getParmLong("ordId"));

    if (isConfirmed())
    {
      shipCallTag(order.getCurrentCallTag());
      deployOrderParts(order);
      redirectWithFeedback(getBackLink(),"Order " + order.getOrdId()
        + " shipped");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Order not shipped");
    }
    else
    {
      confirm("Ship order"
        + (order.hasOutParts() ? ", deploy outgoing parts" : "")
        + ", ship call tag?");
    }
  }

  /**
   * OrderOutPart actions
   */

  public static final String SN_ADD_ORDER_OUT_PART_BEAN = "addOrderOutPartBean";

  private AddOrderOutPartBean getAddOrderOutPartBean()
  {
    AddOrderOutPartBean bean 
      = (AddOrderOutPartBean)getSessionAttr(SN_ADD_ORDER_OUT_PART_BEAN,null);
    if (bean == null)
    {
      bean = new AddOrderOutPartBean(this);
      setSessionAttr(SN_ADD_ORDER_OUT_PART_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
    return bean;
  }

  public void doAddOrderOutPart()
  {
    AddOrderOutPartBean bean = getAddOrderOutPartBean();
    if (bean.isAutoSubmitOk())
    {
      // if add & return pressed, return to back link
      if (bean.returnToOrder())
      {
        logAndRedirect(getBackLink(),bean.getOrderOutPart().getOopId());
      }
      // else remain on add screen
      else
      {
        // redirect to add parts screen for more parts
        // HACK: need to preserve back link manually 
        // since we need to manually generate a link 
        // to make add redirect to 
        Link backLink = getBackLink();
        Link addLink = handler.getLink(Tmg.ACTION_ADD_ORDER_OUT_PART,
         Tmg.VIEW_ADD_ORDER_OUT_PART,"ordId=" + bean.getData(bean.FN_ORD_ID),
         null);
        setBackLink(Tmg.ACTION_ADD_ORDER_OUT_PART,backLink);
        logAndRedirect(addLink,bean.getOrderOutPart().getOopId());
      }
    }
    else
    {
      doView(Tmg.VIEW_ADD_ORDER_OUT_PART);
    }
  }

  public void doDeleteOrderOutPart()
  {
    long oopId = getParmLong("oopId");
    OrderOutPart oop = db.getOrderOutPart(oopId);
    long ordId = oop.getOrdId();

    if (isConfirmed())
    {
      Transaction t =
        new DeleteOrderOutPartTransaction(user,this,oop);
      if (!t.run())
      {
        throw new RuntimeException("Failed to remove outgoing order part");
      }

      db.deleteOrderOutPart(oopId,user);

      redirectWithFeedback(getBackLink(),"Outgoing part " + oop.getOopId()
        + " removed from order");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Outgoing part not removed from order");
    }                                                              else
    {
      confirm("Remove outgoing part " + oop.getOopId() + " from order?");
    }
  }

  public void doCancelOrderOutPart()
  {
    OrderOutPart oop = db.getOrderOutPart(getParmLong("oopId"));
    if (oop.getPartId() != 0L)
    {
      Transaction t = new CancelOrderOutPartTransaction(db,user,this,oop);
      if (!t.run())
      {
        throw new RuntimeException("Failed to cancel outgoing order part");
      }
    }
    else
    {
      oop.setCancelFlag(oop.FLAG_YES);
      db.persist(oop,user);
    }
    redirectWithFeedback(getBackLink(),"Requested outgoing part canceled");
  }

  public void doUncancelOrderOutPart()
  {
    OrderOutPart oop = db.getOrderOutPart(getParmLong("oopId"));
    oop.setCancelFlag(oop.FLAG_NO);
    db.persist(oop,user);
    redirectWithFeedback(getBackLink(),"Requested outgoing part uncanceled");
  }

  public void doAssignOrderOutPart()
  {
    AssignOrderOutPartBean bean = new AssignOrderOutPartBean(this);
    if (bean.isAutoSubmitOk())
    {
      long oopId = bean.getField(bean.FN_OOP_ID).asLong();
      long ordId = bean.getField(bean.FN_ORD_ID).asLong();
      String serialNum = bean.getData(bean.FN_SERIAL_NUM);
      String dispCode = bean.getData(bean.FN_ASSIGNED_DISP_CODE);
      Transaction t = new AssignOrderOutPartTransaction(
                            db,user,this,oopId,ordId,serialNum,dispCode);
      if (!t.run())
      {
        throw new RuntimeException("Failed to assign part");
      }
      logAndRedirect(getBackLink(),oopId);
    }
    else
    {
      doView(Tmg.VIEW_ASSIGN_ORDER_OUT_PART);
    }
  }

  public void doUnassignOrderOutPart()
  {
    OrderOutPart oop = db.getOrderOutPart(getParmLong("oopId"));
    Transaction t = new UnassignOrderOutPartTransaction(db,user,this,oop);
    if (!t.run())
    {
      throw 
        new RuntimeException("Failed to remove s/n from outgoing order part");
    }
    redirectWithFeedback(getBackLink(),"Serial No. removed from outgoing part ");
  }

  /**
   * CallTag actions
   */

  public void doAddCallTag()
  {
    boolean isEdit = name.matches("edit*");
    log.debug("action name: " + name);
    CallTagBean bean = new CallTagBean(this);
    if(bean.isAutoSubmitOk())
    {
      logAndRedirect(getBackLink(),bean.getCallTag().getCtId());
    }
    else
    {
      doView(isEdit ? Tmg.VIEW_EDIT_CALL_TAG : Tmg.VIEW_ADD_CALL_TAG);
    }
  }
  public void doEditCallTag()
  {
    doAddCallTag();
  }

  public void doShipCallTag()
  {
    if (isConfirmed())
    {
      shipCallTag(getParmLong("ctId"));
      redirectWithFeedback(getBackLink(),"Call tag shipped");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Call tag not shipped");
    }
    else
    {
      confirm("Ship call tag?");
    }
  }

  // NEW: this is now a manual function and will not be done automatically
  // in receive call tag part transaction
  public void doCloseCallTag()
  {
    if (isConfirmed())
    {
      CallTag ct = db.getCallTag(getParmLong("ctId"));
      ct.close();
      db.persist(ct,user);
      redirectWithFeedback(getBackLink(),"Call tag closed");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Call tag not closed");
    }
    else
    {
      confirm("Close call tag ?");
    }
  }

  public void doCancelCallTag()
  {
    if (isConfirmed())
    {
      CallTag ct = db.getCallTag(getParmLong("ctId"));
      for (Iterator i = ct.getCallTagParts().iterator(); i.hasNext();)
      {
        CallTagPart ctPart = (CallTagPart)i.next();
        if (!ctPart.isCanceled()) ctPart.cancel();
        db.persist(ctPart,user);
      }
      ct.cancel();
      db.persist(ct,user);
      redirectWithFeedback(getBackLink(),"Call tag shipped");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Call tag not shipped");
    }
    else
    {
      confirm("Cancel all call tag parts and cancel call tag?");
    }
  }

  public void doUncancelCallTag()
  {
    CallTag ct = db.getCallTag(getParmLong("ctId"));
    ct.uncancel();
    db.persist(ct,user);
    redirectWithFeedback(getBackLink(),"Call tag uncanceled");
  }


  /**
   * Call tag notice actions
   */

  public void doAddCallTagNotice()
  {
    AddCallTagNoticeBean bean = new AddCallTagNoticeBean(this);
    if (bean.isAutoSubmitOk())
    {
      setBackLink(Tmg.ACTION_EDIT_CALL_TAG_NOTICE,getBackLink());
      CallTagNotice cn = bean.getCallTagNotice();
      long cnId = cn.getCnId();
      Link link = handler.getLink(Tmg.ACTION_EDIT_CALL_TAG_NOTICE,
        "cnId=" + cnId + "&ctId=" + cn.getCtId());
      logAndRedirect(link,"Call tag notice");
    }
    else
     {
      doView(Tmg.VIEW_ADD_CALL_TAG_NOTICE);
    }
  }

  public void doEditCallTagNotice()
  {
    CallTag ct = db.getCallTag(getParmLong("ctId"));
    CallTagNoticeBean bean = new CallTagNoticeBean(this,ct);
    if (bean.isAutoSubmitOk())
    {
      logAndRedirect(getBackLink(),"Call tag notice");
    }
    else
    {
      doView(Tmg.VIEW_EDIT_CALL_TAG_NOTICE);
    }
  }

  public void doDeleteCallTagNotice()
  {
    long cnId = getParmLong("cnId");
    CallTagNotice cn = db.getCallTagNotice(cnId);
    if (isConfirmed())
    {
      db.deleteCallTagNotice(cnId,user);
      redirectWithFeedback(getBackLink(),"Call tag notice " + cnId
        + " removed from order");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Call tag notice not removed");
    }
    else
    {
      confirm("Remove call tag notice " + cnId + " from order?");
    }
  }

  public void doDownloadNoticePdf()
  {
    try
    {
      // get the call tag and ct notice
      CallTagNotice cn = db.getCallTagNotice(getParmLong("cnId"));
      CallTag ct = db.getCallTag(cn.getCtId());

      // TODO: need to establish an editable notice MES address
      //Address headerAddr = db.getAddress(834360L); 

      // determine which logo to use (use merch num in cn.mifRef ?)
      URL logoUrl 
          = handler.getServletContext().getResource("/logos/ms_logo2.gif");

      // make a custom call tag notice pdf template
      PdfTemplate pt = new CtnPdfTemplate(cn,ct,logoUrl);

      // get a parser, parse the notice data into the template
      PdfTemplateParser parser 
        = new PdfTemplateParser(handler.getServletContext());
      parser.parseXmlData(cn.getNoticeData(),pt);

      // send the pdf data out as pdf response
      response.setContentType("application/x-pdf");
      response.setHeader("Content-Disposition","attachment; filename=\""
        + "notice.pdf" + "\";");
      OutputStream out = response.getOutputStream();
      out.write(pt.toPdfBytes());
      out.flush();
    }
    catch (Exception e)
    {
      log.error("Error generating PDF: " + e);
      e.printStackTrace();
      throw new RuntimeException(""+e);
    }
  }

  /**
   * Call tag part actions
   */

  public void doAddCallTagPart()
  {
    // check for ctId first
    long ctId = getParmLong("ctId",-1L);
    if (ctId == -1L)
    {
      // make sure a call tag is available to add the ct part to
      Order order = db.getOrder(getParmLong("ordId"));
      List callTags = order.getCallTags();
      if (callTags == null || callTags.isEmpty())
      {
        redirectWithFeedback(getBackLink(),"No call tags availabe to add part to");
      }
    }

    // invoke the ct part bean
    CallTagPartBean bean = new CallTagPartBean(this);
    if (bean.isAutoSubmitOk())
    {
      logAndRedirect(getBackLink(),"Call tag part added");
    }
    else
    {
      doView(Tmg.VIEW_ADD_CALL_TAG_PART);
    }
  }

  public void doEditCallTagPart()
  {
    CallTagPartBean bean = new CallTagPartBean(this);
    if (bean.isAutoSubmitOk())
    {
      logAndRedirect(getBackLink(),"Call tag part updated");
    }
    else
    {
      doView(Tmg.VIEW_EDIT_CALL_TAG_PART);
    }
  }

  public void doCancelCallTagPart()
  {
    if (isConfirmed())
    {
      CallTagPart ctPart = db.getCallTagPart(getParmLong("ctPartId"));
      ctPart.cancel();
      db.persist(ctPart,user);
      redirectWithFeedback(getBackLink(),"Call tag part canceled");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Call tag part not canceled");
    }
    else
    {
      confirm("Cancel call tag part?");
    }
  }

  public void doUncancelCallTagPart()
  {
    CallTagPart ctPart = db.getCallTagPart(getParmLong("ctPartId"));
    ctPart.uncancel();
    db.persist(ctPart,user);
    redirectWithFeedback(getBackLink(),"Call tag part uncanceled");
  }

  public void doDeleteCallTagPart()
  {
    long ctPartId = getParmLong("ctPartId");
    if (isConfirmed())
    {
      db.deleteCallTagPart(ctPartId,user);
      redirectWithFeedback(getBackLink(),"Call tag part removed");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Call tag part not removed");
    }
    else
    {
      confirm("Remove call tag part?");
    }
  }

  public void doFailCallTagPart()
  {
    if (isConfirmed())
    {
      CallTagPart ctPart = db.getCallTagPart(getParmLong("ctPartId"));
      ctPart.fail();
      db.persist(ctPart,user);
      CallTag ct = db.getCallTag(ctPart.getCtId());
      redirectWithFeedback(getBackLink(),
        "Call tag part marked as failed to receive");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),
        "Call tag part not failed, still pending");
    }
    else
    {
      confirm("Failed to receive call tag part?");
    }
  }

  public void doReceiveCallTagPart()
  {
    ReceiveCallTagPartBean bean = new ReceiveCallTagPartBean(this);
    if (bean.isAutoSubmitOk())
    {
      logAndRedirect(getBackLink(),"Call tag part received");
    }
    else
    {
      doView(Tmg.VIEW_RECEIVE_CALL_TAG_PART);
    }
  }

  public void doReceiveUnexpectedCtPart()
  {
    ReceiveUnexpectedCtPartBean bean = new ReceiveUnexpectedCtPartBean(this);
    if (bean.isAutoSubmitOk())
    {
      logAndRedirect(getBackLink(),"Call tag part received");
    }
    else
    {
      doView(Tmg.VIEW_RECEIVE_UNEXPECTED_CT_PART);
    }
  }
}
