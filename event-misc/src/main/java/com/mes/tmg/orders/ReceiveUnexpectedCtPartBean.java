package com.mes.tmg.orders;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.Validation;
import com.mes.persist.Transaction;
import com.mes.tmg.CallTag;
import com.mes.tmg.CallTagPart;
import com.mes.tmg.CallTagPartStatus;
import com.mes.tmg.Deployment;
import com.mes.tmg.Order;
import com.mes.tmg.Part;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.DeployReasonTable;
import com.mes.tmg.util.PartTypeTable;

public class ReceiveUnexpectedCtPartBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(ReceiveUnexpectedCtPartBean.class);

  public static final String FN_CT_ID       = "ctId";
  public static final String FN_PT_ID       = "recvPtId";
  public static final String FN_SERIAL_NUM  = "recvSerialNum";
  public static final String FN_MO_FLAG     = "moFlag";
  public static final String FN_PART_PRICE  = "partPrice";
  public static final String FN_DR_CODE     = "drCode";
  public static final String FN_SUBMIT_BTN  = "submitBtn";

  private CallTag callTag;
  private Order order;
  private Part part;

  public CallTag getCallTag()
  {
    if (callTag == null)
    {
      callTag = db.getCallTag(getField(FN_CT_ID).asLong());
    }
    return callTag;
  }

  public Order getOrder()
  {
    getCallTag();
    if (order == null)
    {
      order = db.getOrder(callTag.getOrdId());
    }
    return order;
  }

  private String serialNum;

  public Part getPart()
  {
    if (part == null)
    {
      String fsn = getData(FN_SERIAL_NUM);
      if (!fsn.equals(serialNum))
      {
        serialNum = fsn;
        part = db.getPartWithSerialNum(serialNum);
      }
    }
    return part;
  }

  public ReceiveUnexpectedCtPartBean(TmgAction action)
  {
    super(action);
  }

  public class SerialNumVal implements Validation
  {
    private String errorText;

    public String getErrorText()
    {
      return errorText;
    }

    // determine if the received serial number is valid:
    // must either be a new unknown serial number or 
    // it must be deployed to the order merchant
    public boolean validate(String serialNum)
    {
      // is this a known part?
      getPart();
      if (part != null)
      {
        // is part deployed to this merchant?
        Order order = getOrder();
        long depId = part.getDeployId();
        Deployment dep = null;
        if (depId != 0L)
        {
          dep = db.getDeployment(depId);
        }

        // part is not deployed at all
        if (dep == null)
        {
          errorText = "Serial number is not deployed to this merchant";
          return false;
        }

        // part is deployed to another merchant
        if (!dep.getMerchNum().equals(order.getMerchNum()))
        {
          errorText = "Serial number is deployed to another merchant (" 
            + dep.getMerchName() + ")";
          return false;
        }
      }

      return true;
    }
  }

  public class MoFlagVal implements Validation
  {
    private String errorText;

    public String getErrorText()
    {
      return errorText;
    }

    // moFlag can be set to true only if serial number is unknown
    public boolean validate(String moFlag)
    {
      getPart();
      if (part != null && moFlag.equals("y"))
      {
        errorText = "Serial number in system, part can't be flagged merchant owned";
        return false;
      }
      return true;
    }
  }

  public class PartTypeVal implements Validation
  {
    public String getErrorText()
    {
      return "Part type required for unknown serial numbers";
    }

    public boolean validate(String ptStr)
    {
      getPart();
      if (part == null && (ptStr == null || ptStr.length() == 0))
      {
        return false;
      }
      return true;
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_CT_ID));
      fields.add(new Field(FN_SERIAL_NUM,"Serial No.",64,32,false));
      fields.getField(FN_SERIAL_NUM).addValidation(new SerialNumVal());
      fields.add(new DropDownField(FN_PT_ID,new PartTypeTable(db,TmgDropDownTable.OPT_SEL_ANY),true));
      fields.getField(FN_PT_ID).addValidation(new PartTypeVal());
      fields.add(new CheckField(FN_MO_FLAG,"Flag unknown part as merchant owned","y",true));
      fields.getField(FN_MO_FLAG).addValidation(new MoFlagVal());
      fields.add(new CurrencyField(FN_PART_PRICE,"Part Price",10,10,false));
      fields.getField(FN_PART_PRICE).setOptionalCondition(
        new com.mes.forms.Condition() {
          public boolean isMet() { return ((CheckField)fields.getField(FN_MO_FLAG)).isChecked(); };
        } );
      fields.add(new DropDownField(FN_DR_CODE,new DeployReasonTable(null,"y"),false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      getCallTag();
      getOrder();
      getPart();

      // create call tag part
      CallTagPart ctPart = new CallTagPart();
      ctPart.setCtPartId(db.getNewId());
      ctPart.setCtId(callTag.getCtId());
      ctPart.setOrdId(order.getOrdId());
      ctPart.setCreateDate(db.getCurDate());
      ctPart.setUserName(user.getLoginName());
      ctPart.setCtpStatCode(CallTagPartStatus.CTPS_PENDING);
      ctPart.setMoFlag(getData(FN_MO_FLAG));
      ctPart.setPartPrice(getField(FN_PART_PRICE).asDouble());
      ctPart.setDrCode(getData(FN_DR_CODE));

      // serial num 
      String serialNum = getData(FN_SERIAL_NUM);
      ctPart.setSerialNum(serialNum);
      ctPart.setRecvSerialNum(serialNum);

      // is serial num a known part?
      if (part != null)
      {
        ctPart.setRecvPtId(part.getPtId());
        ctPart.setPtId(part.getPtId());
      }
      else
      {
        long ptId = getField(FN_PT_ID).asLong();
        ctPart.setRecvPtId(ptId);
        ctPart.setPtId(ptId);
      }

      // receive the part into testing
      Transaction t = new ReceiveCallTagPartTransaction(db,user,
        (TmgAction)action,ctPart);
      if (!t.run())
      {
        throw new RuntimeException("Failed to receive call tag part");
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error submitting: " + e);
    }
    return false;
  }
}
