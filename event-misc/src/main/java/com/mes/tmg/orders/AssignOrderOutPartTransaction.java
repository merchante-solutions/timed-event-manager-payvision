package com.mes.tmg.orders;

import org.apache.log4j.Logger;
import com.mes.tmg.NoteAction;
import com.mes.tmg.OrderOutPart;
import com.mes.tmg.Part;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public class AssignOrderOutPartTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(AssignOrderOutPartTransaction.class);

  private TmgAction tmgAction;
  private long oopId;
  private long ordId;
  private String dispCode;
  long invId;

  public AssignOrderOutPartTransaction(TmgDb db, UserBean user, TmgAction tmgAction, 
    long oopId, long ordId, String serialNum, String dispCode)
  {
    super(db,user,db.getPartWithSerialNum(serialNum),
      db.getOpCode(Tmg.OP_PART_ADD_ORDER));
    this.tmgAction = tmgAction;
    this.oopId = oopId;
    this.ordId = ordId;
    this.dispCode = dispCode;
  }
  public AssignOrderOutPartTransaction(UserBean user, TmgAction tmgAction, 
    long oopId, long ordId, String serialNum, String dispCode)
  {
    this(new TmgDb(),user,tmgAction,oopId,ordId,serialNum,dispCode);
  }

  public int executeOperation(PartOperation op)
  {
    // un-busy any part that was previously assigned to the order part
    OrderOutPart oop = db.getOrderOutPart(oopId);
    if (oop.getPartId() != 0L && oop.getPartId() != part.getPartId())
    {
      Part oldPart = db.getPart(oop.getPartId());
      oldPart.setStatusCode(oldPart.SC_IN);
      db.changePart(oldPart,op,user);
      db.logAction(tmgAction,user,"Part " + oldPart.getPartId() 
        + " removed from order " + ordId,part.getPartId());
      db.logAction(tmgAction,user,"Part " + oldPart.getPartId()
        + " removed from order " + ordId,ordId);
    }

    // busy the part being assigned to the order part
    part.setStatusCode(part.SC_BUSY);
    db.changePart(part,op,user);

    // assign the part to the order part
    oop.setPartId(part.getPartId());
    oop.setSerialNum(part.getSerialNum());
    oop.setAssignedPtId(part.getPtId());
    oop.setAssignedPcCode(part.getPcCode());
    oop.setAssignedDispCode(dispCode);
    oop.setAssignedInvId(part.getInventory().getInvId());
    db.persist(oop,user);

    db.logAction(tmgAction,user,"Part " + part.getPartId() + " added to order " 
      + ordId,part.getPartId());
    db.logAction(tmgAction,user,"Part " + part.getPartId() + " added to order " 
      + ordId,ordId);

    // make a note of the order part is being added to on the part
    NoteAction.insertNote(db,user,tmgAction.getName(),part.getPartId(),
      "Part added to order " + ordId);

    return COMMIT;
  }
}
