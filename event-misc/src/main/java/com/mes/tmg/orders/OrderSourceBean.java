package com.mes.tmg.orders;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.OrderSource;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class OrderSourceBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(OrderSourceBean.class);
  
  public static final String FN_ORD_SRC_ID    = "ordSrcId";
  public static final String FN_ORD_SRC_CODE  = "ordSrcCode";
  public static final String FN_DESCRIPTION   = "description";
  
  public static final String FN_SUBMIT_BTN    = "submit";
  
  private com.mes.tmg.OrderSource orderSource;

  public OrderSourceBean(TmgAction action)
  {
    super(action);
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_ORD_SRC_ID));
      fields.addAlias(FN_ORD_SRC_ID,"editId");
      fields.add(new Field(FN_ORD_SRC_CODE,"Code",32,32,false));
      fields.add(new Field(FN_DESCRIPTION,"Name",100,32,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public OrderSource getOrderSource()
  {
    return orderSource;
  }
  
  /**
   * Persists part condition.
   */
  protected boolean autoSubmit()
  {
    try
    {
      long ordSrcId = getField(FN_ORD_SRC_ID).asLong();
      if (ordSrcId == 0L)
      {
        ordSrcId = db.getNewId();
        orderSource = new OrderSource();
        orderSource.setOrdSrcId(ordSrcId);
      }
      else
      {
        orderSource = db.getOrderSource(ordSrcId);
      }
      orderSource.setOrdSrcCode(getData(FN_ORD_SRC_CODE));
      orderSource.setDescription(getData(FN_DESCRIPTION));
      db.persist(orderSource,user);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error submitting order source: " + e);
    }
    return false;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    try
    {
      long ordSrcId = getField(FN_ORD_SRC_ID).asLong();
      if (ordSrcId != 0L)
      {
        orderSource = db.getOrderSource(ordSrcId);
        setData(FN_ORD_SRC_CODE,orderSource.getOrdSrcCode());
        setData(FN_DESCRIPTION,orderSource.getDescription());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error loading order source: " + e);
    }
    return false;
  }
}