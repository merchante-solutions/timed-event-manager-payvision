package com.mes.tmg.orders;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.CallTag;
import com.mes.tmg.CallTagPart;
import com.mes.tmg.CallTagPartStatus;
import com.mes.tmg.Order;
import com.mes.tmg.Part;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.DeployReasonTable;
import com.mes.tmg.util.PartTypeTable;

public class CallTagPartBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(CallTagPartBean.class);

  public static final String FN_CT_PART_ID  = "ctPartId";
  public static final String FN_CT_ID       = "ctId";
  public static final String FN_ORD_ID      = "ordId";
  public static final String FN_PART_ID     = "partId";
  public static final String FN_DR_CODE     = "drCode";
  public static final String FN_PT_ID       = "ptId";
  public static final String FN_SERIAL_NUM  = "serialNum";
  public static final String FN_SUBMIT_BTN  = "submitBtn";

  public CallTagPartBean(TmgAction action)
  {
    super(action);
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_CT_PART_ID));
      fields.addAlias(FN_CT_PART_ID,"editId");
      fields.add(new HiddenField(FN_ORD_ID));
      fields.add(new HiddenField(FN_PART_ID));

      // call tag id is optional, don't provide hidden field holder
      // for it if it's not present (screws up the getParmLong upon submit)
      String ctId = request.getParameter("ctId");
      boolean isCtKnown = ctId != null && ctId.length() > 0;
      if (isCtKnown)
      {
        fields.add(new HiddenField(FN_CT_ID));
      }

      // if specific part id not given then need to provide some fields
      // to describe the part type and serial no. (optional)
      String partId = request.getParameter("partId");
      boolean isKnownPart = partId != null && !partId.equals("") && !partId.equals("0");
      if (!isKnownPart)
      {
        fields.add(new DropDownField(FN_PT_ID,new PartTypeTable(db,TmgDropDownTable.OPT_SEL_ONE),false));
        fields.add(new Field(FN_SERIAL_NUM,"Serial No.",64,32,true));
      }

      fields.add(new DropDownField(FN_DR_CODE,new DeployReasonTable(null,"y"),false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public boolean autoLoad()
  {
    try
    {
      long ctPartId = getField(FN_CT_PART_ID).asLong();
      if (ctPartId != 0L)
      {
        CallTagPart ctPart = db.getCallTagPart(ctPartId);
        setData(FN_ORD_ID,String.valueOf(ctPart.getOrdId()));
        setData(FN_PART_ID,String.valueOf(ctPart.getPartId()));
        setData(FN_DR_CODE,ctPart.getDrCode());
        if (ctPart.getPartId() == 0L)
        {
          setData(FN_PT_ID,String.valueOf(ctPart.getPtId()));
          setData(FN_SERIAL_NUM,ctPart.getSerialNum());
        }
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("autoLoad error: " + e);
    }
    return false;
  }

  public boolean autoSubmit()
  {
    try
    {
      long ctPartId = getField(FN_CT_PART_ID).asLong();
      CallTagPart ctPart = null;
      // is this a new call tag part (ctPartId == 0)?
      if (ctPartId == 0L)
      {
        // need to determine the associated order and call tag
        // if ctId is present, use it to determine the call tag
        // and order, otherwise use the ordId to determine the 
        // order and use it's current call tag as the call tag
        Order order = null;
        long ctId = getField(FN_CT_ID) != null ? 
          getField(FN_CT_ID).asLong() : 0L;
        if (ctId != 0L)
        {
          CallTag ct = db.getCallTag(ctId);
          order = db.getOrder(ct.getOrdId());
        }
        else
        {
          long ordId = getField(FN_ORD_ID).asLong();
          order = db.getOrder(ordId);
          ctId = order.getCurrentCallTag().getCtId();
          setData(FN_CT_ID,String.valueOf(ctId));
        }

        ctPartId = db.getNewId();
        ctPart = new CallTagPart();
        ctPart.setCtPartId(ctPartId);
        ctPart.setCtId(ctId);
        ctPart.setOrdId(order.getOrdId());
        ctPart.setCreateDate(db.getCurDate());
        ctPart.setUserName(user.getLoginName());
        ctPart.setCtpStatCode(CallTagPartStatus.CTPS_NEW);

        // if partId is present, load part type info from the part state
        long partId = getField(FN_PART_ID).asLong();
        if (partId != 0L)
        {
          Part part = db.getPart(partId);
          ctPart.setPartId(part.getPartId());
          ctPart.setPtId(part.getPtId());
          ctPart.setSerialNum(part.getSerialNum());
        }
      }
      // this is existing call tag part...
      else
      {
        ctPart = db.getCallTagPart(ctPartId);
      }

      ctPart.setDrCode(getData(FN_DR_CODE));

      // if partId is not given, load the user selected part type
      // fields into the call tag part
      if (getField(FN_PART_ID).asLong() == 0L)
      {
        ctPart.setPtId(getField(FN_PT_ID).asLong());
        ctPart.setSerialNum(getData(FN_SERIAL_NUM));
      }

      db.persist(ctPart,user);
      return true;
    }
    catch (Exception e)
    {
      log.error("autoSubmit error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean showFeedback()
  {
    return false;
  }
}
