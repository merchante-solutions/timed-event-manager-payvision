package com.mes.tmg.orders;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.CallTag;
import com.mes.tmg.CallTagStatus;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class CallTagBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(CallTagBean.class);

  public static final String FN_CT_ID               = "ctId";
  public static final String FN_ORD_ID              = "ordId";
  public static final String FN_TRACKING_NUM        = "trackingNum";
  public static final String FN_SUBMIT_BTN          = "submitBtn";

  private CallTag ct;

  public CallTagBean(TmgAction action)
  {
    super(action);
  }

  public CallTag getCallTag()
  {
    return ct;
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_CT_ID));
      fields.addAlias(FN_CT_ID,"editId");
      fields.add(new HiddenField(FN_ORD_ID));
      fields.add(new Field(FN_TRACKING_NUM,64,20,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public boolean autoLoad()
  {
    try
    {
      long ctId = getField(FN_CT_ID).asLong();
      if (ctId != 0L)
      {
        ct = db.getCallTag(ctId);
        setData(FN_ORD_ID,String.valueOf(ct.getOrdId()));
        setData(FN_TRACKING_NUM,ct.getTrackingNum());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("autoLoad error: " + e);
    }
    return false;
  }

  public boolean autoSubmit()
  {
    try
    {
      long ctId = getField(FN_CT_ID).asLong();
      if (ctId == 0L)
      {
        ctId = db.getNewId();
        ct = new CallTag();
        ct.setCtId(ctId);
        ct.setOrdId(getField(FN_ORD_ID).asLong());
        ct.setUserName(user.getLoginName());
        ct.setCreateDate(db.getCurDate());
        ct.setCtStatCode(CallTagStatus.CTS_NEW);
      }
      else
      {
        ct = db.getCallTag(ctId);
      }
      Field ftn = getField(FN_TRACKING_NUM);
      if (!ftn.isBlank())
      {
        ct.setTrackingNum(ftn.getData());
      }
      else
      {
        ct.setTrackingNum(null);
      }
      db.persist(ct,user);
      return true;
    }
    catch (Exception e)
    {
      log.error("autoSubmit error: " + e);
    }
    return false;
  }

  protected boolean showFeedback()
  {
    return false;
  }
}
