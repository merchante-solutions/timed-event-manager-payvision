package com.mes.tmg.orders;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.tmg.CallTagNoticeType;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.pdf.PdfTemplateParser;

public class CalltagNoticeTypeBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(CalltagNoticeTypeBean.class);
  
  public static final String FN_CNT_ID      = "cntId";
  public static final String FN_CNT_CODE    = "cntCode";
  public static final String FN_DESCRIPTION = "description";
  public static final String FN_TEMPLATE    = "template";
  
  public static final String FN_SUBMIT_BTN  = "submit";
  
  private CallTagNoticeType cnt;

  public CalltagNoticeTypeBean(TmgAction action)
  {
    super(action);
  }

  public class TemplateValidation implements Validation
  {
    private String errorText;

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String data)
    {
      if (data == null || data.length() == 0) return true;
      PdfTemplateParser parser 
        = new PdfTemplateParser(action.getHandler().getServletContext());

      if (parser.parseXmlData(data) == null)
      {
        errorText = "Invalid template format - " + parser.getParseErrorMsg();
        return false;
      }

      return true;
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_CNT_ID));
      fields.addAlias(FN_CNT_ID,"editId");
      fields.add(new Field(FN_CNT_CODE,"Code",32,32,false));
      fields.add(new Field(FN_DESCRIPTION,"Name",100,32,false));
      fields.add(new TextareaField(FN_TEMPLATE,"Template",2000,20,80,false));
      fields.getField(FN_TEMPLATE).addValidation(new TemplateValidation());
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public CallTagNoticeType getCallTagNoticeType()
  {
    return cnt;
  }
  
  /**
   * Persists part condition.
   */
  protected boolean autoSubmit()
  {
    try
    {
      long cntId = getField(FN_CNT_ID).asLong();
      if (cntId == 0L)
      {
        cntId = db.getNewId();
        cnt = new CallTagNoticeType();
        cnt.setCntId(cntId);
      }
      else
      {
        cnt = db.getCallTagNoticeType(cntId);
      }
      cnt.setCntCode(getData(FN_CNT_CODE));
      cnt.setDescription(getData(FN_DESCRIPTION));
      cnt.setTemplate(getData(FN_TEMPLATE));
      db.persist(cnt,user);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error submitting: " + e);
    }
    return false;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    try
    {
      long cntId = getField(FN_CNT_ID).asLong();
      if (cntId != 0L)
      {
        cnt = db.getCallTagNoticeType(cntId);
        setData(FN_CNT_CODE,cnt.getCntCode());
        setData(FN_DESCRIPTION,cnt.getDescription());
        setData(FN_TEMPLATE,cnt.getTemplate());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error loading: " + e);
    }
    return false;
  }
}