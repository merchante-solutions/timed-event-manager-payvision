package com.mes.tmg.orders;

import java.util.Iterator;
import org.apache.log4j.Logger;
import com.mes.tmg.CallTag;
import com.mes.tmg.Order;
import com.mes.tmg.OrderOutPart;
import com.mes.tmg.OrderStatus;
import com.mes.tmg.Part;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.user.UserBean;

public class CancelOrderTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(CancelOrderTransaction.class);

  private TmgAction tmgAction;
  private Order order;

  public CancelOrderTransaction(TmgDb db, UserBean user, TmgAction tmgAction, 
    Order order)
  {
    super(db,user,null,db.getOpCode(Tmg.OP_PART_REMOVE_ORDER));
    this.tmgAction = tmgAction;
    this.order = order;
  }
  public CancelOrderTransaction(UserBean user, TmgAction tmgAction,  Order order)
  {
    this(new TmgDb(),user,tmgAction,order);
  }

  public int executeOperation(PartOperation op)
  {
    // unlink associated out order parts
    long ordId = order.getOrdId();
    for (Iterator i = order.getOrderOutParts().iterator(); i.hasNext();)
    {
      OrderOutPart oop = (OrderOutPart)i.next();
      long partId = oop.getPartId();
      if (partId != 0L)
      {
        // unassign part from out part
        oop.unlink();
        db.persist(oop,user);

        // un-busy part
        Part part = db.getPart(partId);
        part.setStatusCode(part.SC_IN);
        db.changePart(part,op,user);

        db.logAction(tmgAction,user,"Part " + partId
          + " removed from order " + ordId,partId);
        db.logAction(tmgAction,user,"Part " + partId
          + " removed from order " + ordId,ordId);
      }
    }

    // cancel any call tags
    for (Iterator i = order.getCallTags().iterator(); i.hasNext();)
    {
      CallTag ct = (CallTag)i.next();
      if (ct.canCancel())
      {
        ct.cancel();
        db.persist(ct,user);
      }
    }

    // set order status to canceled
    order.setOrdStatCode(OrderStatus.OSC_CANCELED);
    db.persist(order,user);
    db.logAction(tmgAction,user,"Order " + order.getOrdId() + " canceled",
      order.getOrdId());

    return COMMIT;
  }
}
