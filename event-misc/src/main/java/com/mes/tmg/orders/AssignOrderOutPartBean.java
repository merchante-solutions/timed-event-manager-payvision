package com.mes.tmg.orders;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.Validation;
import com.mes.tmg.DeployReason;
import com.mes.tmg.Order;
import com.mes.tmg.OrderOutPart;
import com.mes.tmg.Part;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.PartDispositionTable;

public class AssignOrderOutPartBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(AssignOrderOutPartBean.class);

  public static final String FN_OOP_ID              = "oopId";
  public static final String FN_ORD_ID              = "ordId";
  public static final String FN_SERIAL_NUM          = "serialNum";
  public static final String FN_ASSIGNED_DISP_CODE  = "assignedDispCode";
  public static final String FN_SUBMIT_BTN          = "submitBtn";

  private Part part;
  private Order order;
  private OrderOutPart oop;

  public AssignOrderOutPartBean(TmgAction action)
  {
    super(action);
  }

  public class SnValidation implements Validation
  {
    private String errorText = null;

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String data)
    {
      // don't invalidate null data here...
      if (data == null || data.length() == 0) return true;
      if (part == null)
      {
        part = db.getPartWithSerialNum(data);
      }
      if (part == null)
      {
        errorText = "Unknown serial no.";
        return false;
      }
      if (order == null)
      {
        order = db.getOrder(getField(FN_ORD_ID).asLong());
      }

      // don't validate serial num if it isn't changing
      OrderOutPart oop = db.getOrderOutPart(getField(FN_OOP_ID).asLong());
      if (oop.getPartId() == 0L || oop.getPartId() != part.getPartId())
      {
        // make sure has valid state
        if (!part.getDispCode().equals(part.DC_NOT_DEP) && !part.getDispCode().equals(part.DC_MOE))
        {
          errorText = "Part must not be deployed (" + part.getDispCode() + ")";
          return false;
        }
        if (!part.getLocCode().equals(part.LC_WAREHOUSE))
        {
          errorText = "Part not in warehouse (" + part.getLocCode() + ")";
          return false;
        }
        if (!part.getCondCode().equals(part.CC_GOOD))
        {
          errorText = "Part not in good condition (" + part.getCondCode() + ")";
          return false;
        }
        if (!part.getStatusCode().equals(part.SC_IN))
        {
          errorText = "Part not available (" + part.getStatusCode() + ")";
          return false;
        }

        // make sure part is not merchant owned 
        // or belongs to the order merchant
        if (part.isMerchantOwned() && 
            !part.getOwnerMerchNum().equals(order.getMerchNum()))
        {
          errorText = "Part is owned by another merchant (" 
            + part.getOwnerMerchNum() + ")";
          return false;
        }
      }

      // make sure part is in allowed inventory (either the 
      // deploy to or pull from inv in the order
      long partInvId = part.getInventory().getInvId();
      long toInvId = order.getDeployToInv().getInvId();
      long fromInvId = (order.getPullFromInv() == null ? 0L : 
        order.getPullFromInv().getInvId());
      if (partInvId != toInvId && partInvId != fromInvId)
      {
        errorText = "Part not in valid inventory, must be " + order.getDeployToInv().getInvName()
          + (fromInvId != 0L ? " or " + order.getPullFromInv().getInvName() : "");
        return false;
      }

      return true;
    }
  }

  public class DispValidation implements Validation
  {
    String errorText;

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String data)
    {
      // ignore missing or null data in this validator
      if (data == null || data.length() == 0)
      {
        return true;
      }

      if (part == null)
      {
        part = db.getPartWithSerialNum(getData(FN_SERIAL_NUM));
      }
      if (oop == null)
      {
        oop = db.getOrderOutPart(getField(FN_OOP_ID).asLong());
      }

      // make sure the part's merchant owned status and the outgoing
      // disposition match up properly
      if (part != null)
      {
        if (part.isMerchantOriginated())
        {
          if (!part.DC_MOE.equals(data))
          {
            errorText = "Part is merchant owned, it must be assigned merchant"
              + " owned disposition";
            return false;
          }
        }
        else if (part.DC_MOE.equals(data))
        {
          errorText = "Part is not merchant owned, cannot assign merchant"
            + " owned disposition";
          return false;
        }
      }

      // make sure internal loaner disposition 
      // matches up with internal loan reasons
      if (DeployReason.DRC_LOAN.equals(oop.getDrCode()))
      {
        if (!Part.DC_INT_LOAN.equals(data))
        {
          errorText = "Loan deploy reason requires internal loan disposition";
          return false;
        }
      }
      else if (Part.DC_INT_LOAN.equals(data))
      {
        errorText = "Internal loan disposition allowed only with loan reason";
        return false;
      }

      // make sure internal loaners are only assigned to 
      return true;
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_OOP_ID));
      fields.addAlias(FN_OOP_ID,"editId");
      fields.add(new HiddenField(FN_ORD_ID));
      fields.add(new Field(FN_SERIAL_NUM,"Serial No.",64,32,false));
      fields.getField(FN_SERIAL_NUM).addValidation(new SnValidation());
      fields.add(new DropDownField(FN_ASSIGNED_DISP_CODE,new PartDispositionTable(true),false));
      fields.getField(FN_ASSIGNED_DISP_CODE).addValidation(new DispValidation());
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public boolean autoLoad()
  {
    try
    {
      long oopId = getField(FN_OOP_ID).asLong();
      if (oopId != 0L)
      {
        oop = db.getOrderOutPart(oopId);
        setData(FN_SERIAL_NUM,oop.getSerialNum());
        setData(FN_ASSIGNED_DISP_CODE,oop.getAssignedDispCode());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("autoLoad error: " + e);
    }
    return false;
  }

  protected boolean showFeedback()
  {
    return false;
  }
}
