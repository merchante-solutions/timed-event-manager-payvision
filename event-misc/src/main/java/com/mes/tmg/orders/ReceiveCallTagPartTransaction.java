package com.mes.tmg.orders;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.tmg.CallTag;
import com.mes.tmg.CallTagPart;
import com.mes.tmg.Deployment;
import com.mes.tmg.Inventory;
import com.mes.tmg.NoteAction;
import com.mes.tmg.Order;
import com.mes.tmg.PartOperation;
import com.mes.tmg.PartState;
import com.mes.tmg.PartType;
import com.mes.tmg.PersistPartTransaction;
import com.mes.tmg.Tmg;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDb;
import com.mes.tmg.inventory.AmpPart;
import com.mes.tmg.inventory.AmpTransaction;
import com.mes.user.UserBean;

public class ReceiveCallTagPartTransaction extends PersistPartTransaction
{
  static Logger log = Logger.getLogger(ReceiveCallTagPartTransaction.class);

  private TmgAction action;
  private CallTagPart ctPart;
  private Order order;

  public ReceiveCallTagPartTransaction(TmgDb db, UserBean user, 
    TmgAction action, CallTagPart ctPart)
  {
    super(db,user,db.getPartWithSerialNum(ctPart.getRecvSerialNum()),
      db.getOpCode(Tmg.OP_PART_RECV_CALL_TAG));
    this.action = action;
    this.ctPart = ctPart;
  }
  public ReceiveCallTagPartTransaction(UserBean user, TmgAction action, 
    CallTagPart ctPart)
  {
    this(new TmgDb(),user,action,ctPart);
  }

  private void doAmpTransaction()
  {
    // HACK: this is an inner transaction...if it succeeds but
    // the transaction fails later, result may be sorrowful
    PartType partType = db.getPartType(ctPart.getRecvPtId());
    Inventory inventory = 
      order.getMerchantRef().getOlaRef().getCdRule().getDeployToInv();
    String noteText = "Part added to inventory from call tag on order "
      + order.getOrdId() + " for merchant " 
      + order.getMerchantRef().getMerchBusinessName()
      + " (" + order.getMerchNum() + ") with serial number " 
      + ctPart.getRecvSerialNum();
    AmpPart ampPart = new AmpPart(0,inventory,partType,"MERCHANT",
      "USED",ctPart.getPartPrice(),ctPart.getRecvSerialNum(),1,noteText,
      ctPart.isMerchantOwned(),
      (ctPart.isMerchantOwned() ? order.getMerchNum() : null));
    List list = new ArrayList();
    list.add(ampPart);
    PersistPartTransaction t 
      = new AmpTransaction(new TmgDb(),user,action,list);
    if (!t.run())
    {
      throw new RuntimeException("Misc. addition transaction failed");
    }

    // get the created part (reload to fill in subtable objects)
    part = t.getPart();
    part = db.getPart(part.getPartId());
    log.debug("misc added part: " + part);
  }

  private void doEndDeployment(PartOperation op)
  {
    // fetch the associated deployment record for the part
    Deployment dep = part.getDeployment();
    if (dep == null)
    {
      throw new NullPointerException("Null deployment in part");
    }

    // set codes to indicate in/testing/not deployed and 
    // remove the deploy id from the part if it's set
    // also change part class to used
    // also unset the owner merch num
    part.setStatusCode(part.SC_IN);
    part.setDispCode(part.DC_NOT_DEP);
    part.setLocCode(part.LC_TESTING);
    part.setPcCode("USED");
    part.setDeployId(0);
    // HACK: need to assign a unique new id to the part test 
    //       record that is about to be created
    part.setTestId(db.getNewId());
    part.setOwnerMerchNum(null);
    db.changePart(part,op,user);
    db.logAction(action,user,"Part " + part.getPartId() 
      + " received into testing",part.getPartId());

    // load new part state to link it to the deploy rec
    PartState ps = db.getCurrentPartState(part.getPartId());

    // set end deploy details in associated deployment record
    dep.setEndUserName(user.getLoginName());
    dep.setEndDate(db.getCurDate());
    dep.setEndOrdId(ctPart.getOrdId());
    dep.setEndCode(ctPart.getDrCode());
    dep.setStateId(ps.getStateId());
    db.persist(dep,user);
    db.logAction(action,user,"Deployment record " + dep.getDeployId()
      + " marked as ended",dep.getDeployId());

    createPartTest(part,action,ctPart.getCtPartId(),-1L);
  }

  private void receiveCallTagPart()
  {
    // set the received part data from the part
    ctPart.setRecvPartId(part.getPartId());
    ctPart.setRecvDate(db.getCurDate());

    // change ctPart to received, persist it
    ctPart.receive();
    db.persist(ctPart,user);
    db.logAction(action,user,"Call tag part received into testing",
      ctPart.getCtPartId());

    // generate note on received part
    CallTag ct = db.getCallTag(ctPart.getCtId());
    String noteText = "Part received on order " + order.getOrdId()
      + " call tag with tracking number " + ct.getTrackingNum()
      + " for merchant " + order.getMerchantRef().getMerchBusinessName()
      + " (merchant number " + order.getMerchNum() 
      + ") with serial number " + ctPart.getRecvSerialNum()
      + order.getMerchNum() + (order.getAcrRef() != null ? " initiated by ACR "
      + order.getAcrId() : "") + ".";
    NoteAction.insertNote(db,user,action.getName(),part.getPartId(),noteText);
  }

  public int executeOperation(PartOperation op)
  {
    // load the associated order
    order = db.getOrder(ctPart.getOrdId());

    // if call tag part is unknown, part will be null
    if (part == null)
    {
      doAmpTransaction();
    }
    // this is a known deployed part, end deployment 
    else
    {
      doEndDeployment(op);
    }

    // change call tag part status to received
    receiveCallTagPart();

    return COMMIT;
  }
}
