package com.mes.tmg.orders;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tmg.DeployReason;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgViewBean;

public class DeployReasonBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(DeployReasonBean.class);
  
  public static final String FN_DR_ID       = "drId";
  public static final String FN_DR_CODE     = "drCode";
  public static final String FN_START_FLAG  = "startFlag";
  public static final String FN_END_FLAG    = "endFlag";
  public static final String FN_DESCRIPTION = "description";
  
  public static final String FN_SUBMIT_BTN  = "submit";
  
  private com.mes.tmg.DeployReason deployReason;

  public DeployReasonBean(TmgAction action)
  {
    super(action);
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_DR_ID));
      fields.addAlias(FN_DR_ID,"editId");
      fields.add(new Field(FN_DR_CODE,"Code",32,32,false));
      fields.add(new Field(FN_DESCRIPTION,"Name",100,32,false));
      fields.add(new CheckField(FN_START_FLAG,"Can start deployment","y",true));
      fields.add(new CheckField(FN_END_FLAG,"Can end deployment","y",true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"Submit"));
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public DeployReason getDeployReason()
  {
    return deployReason;
  }
  
  /**
   * Persists part condition.
   */
  protected boolean autoSubmit()
  {
    try
    {
      long drId = getField(FN_DR_ID).asLong();
      if (drId == 0L)
      {
        drId = db.getNewId();
        deployReason = new DeployReason();
        deployReason.setDrId(drId);
      }
      else
      {
        deployReason = db.getDeployReason(drId);
      }
      deployReason.setDrCode(getData(FN_DR_CODE));
      deployReason.setDescription(getData(FN_DESCRIPTION));
      deployReason.setStartFlag(getData(FN_START_FLAG));
      deployReason.setEndFlag(getData(FN_END_FLAG));
      db.persist(deployReason,user);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error submitting: " + e);
    }
    return false;
  }
  
  /**
   * Loads persistent data into fields if an id is given.
   */
  protected boolean autoLoad()
  {
    try
    {
      long drId = getField(FN_DR_ID).asLong();
      if (drId != 0L)
      {
        deployReason = db.getDeployReason(drId);
        setData(FN_DR_CODE,deployReason.getDrCode());
        setData(FN_DESCRIPTION,deployReason.getDescription());
        setData(FN_START_FLAG,deployReason.getStartFlag());
        setData(FN_END_FLAG,deployReason.getEndFlag());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error loading: " + e);
    }
    return false;
  }
}