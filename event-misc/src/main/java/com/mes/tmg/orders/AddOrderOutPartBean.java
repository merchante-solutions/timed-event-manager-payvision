package com.mes.tmg.orders;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.HiddenField;
import com.mes.tmg.OrderOutPart;
import com.mes.tmg.TmgAction;
import com.mes.tmg.TmgDropDownTable;
import com.mes.tmg.TmgViewBean;
import com.mes.tmg.util.DeployReasonTable;
import com.mes.tmg.util.PartClassTable;
import com.mes.tmg.util.PartDispositionTable;
import com.mes.tmg.util.PartTypeTable;

public class AddOrderOutPartBean extends TmgViewBean
{
  static Logger log = Logger.getLogger(AddOrderOutPartBean.class);

  public static final String FN_OOP_ID              = "oopId";
  public static final String FN_ORD_ID              = "ordId";
  public static final String FN_DR_CODE             = "drCode";
  public static final String FN_REQUESTED_PT_ID     = "requestedPtId";
  public static final String FN_REQUESTED_PC_CODE   = "requestedPcCode";
  public static final String FN_REQUESTED_DISP_CODE = "requestedDispCode";
  public static final String FN_ADD_BTN             = "addBtn";
  public static final String FN_ADD_RETURN_BTN      = "addReturnBtn";

  private OrderOutPart oop;

  public AddOrderOutPartBean(TmgAction action)
  {
    super(action);
  }

  public OrderOutPart getOrderOutPart()
  {
    return oop;
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_OOP_ID));
      fields.addAlias(FN_OOP_ID,"editId");
      fields.add(new HiddenField(FN_ORD_ID));
      fields.add(new DropDownField(FN_DR_CODE,new DeployReasonTable("y",null),false));
      fields.add(new DropDownField(FN_REQUESTED_PT_ID,new PartTypeTable(db,TmgDropDownTable.OPT_SEL_ONE),false));
      fields.add(new DropDownField(FN_REQUESTED_DISP_CODE,new PartDispositionTable(true),false));
      fields.add(new DropDownField(FN_REQUESTED_PC_CODE,new PartClassTable(),false));
      fields.add(new ButtonField(FN_ADD_BTN,"add"));
      fields.add(new ButtonField(FN_ADD_RETURN_BTN,"add & return"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public boolean autoLoad()
  {
    try
    {
      long oopId = getField(FN_OOP_ID).asLong();
      if (oopId != 0L)
      {
        oop = db.getOrderOutPart(oopId);
        setData(FN_ORD_ID,String.valueOf(oop.getOrdId()));
        setData(FN_DR_CODE,oop.getDrCode());
        setData(FN_REQUESTED_PT_ID,String.valueOf(oop.getRequestedPtId()));
        setData(FN_REQUESTED_PC_CODE,oop.getRequestedPcCode());
        setData(FN_REQUESTED_DISP_CODE,oop.getRequestedDispCode());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("autoLoad error: " + e);
    }
    return false;
  }

  private boolean returnToOrderFlag;

  public boolean returnToOrder()
  {
    return returnToOrderFlag;
  }

  public boolean autoSubmit()
  {
    // first determine if user pushed the return button
    returnToOrderFlag = !getField(FN_ADD_RETURN_BTN).isBlank();

    // then clear button fields since this bean 
    // is now stored in the session
    setData(FN_ADD_BTN,"");
    setData(FN_ADD_RETURN_BTN,"");

    try
    {
      long oopId = getField(FN_OOP_ID).asLong();
      if (oopId == 0L)
      {
        oopId = db.getNewId();
        oop = new OrderOutPart();
        oop.setOopId(oopId);
        oop.setOrdId(getField(FN_ORD_ID).asLong());
        oop.setUserName(user.getLoginName());
        oop.setCreateDate(db.getCurDate());
        oop.setDrCode(getData(FN_DR_CODE));
      }
      else
      {
        oop = db.getOrderOutPart(oopId);
      }
      oop.setRequestedPtId(getField(FN_REQUESTED_PT_ID).asLong());
      oop.setRequestedPcCode(getData(FN_REQUESTED_PC_CODE));
      oop.setRequestedDispCode(getData(FN_REQUESTED_DISP_CODE));
      oop.setCancelFlag(oop.FLAG_NO);
      oop.setInternalFlag(oop.FLAG_YES);
      db.persist(oop,user);
      return true;
    }
    catch (Exception e)
    {
      log.error("autoSubmit error: " + e);
    }
    return false;
  }

  protected boolean showFeedback()
  {
    return false;
  }
}
