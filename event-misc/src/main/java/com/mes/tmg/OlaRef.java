package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class OlaRef extends TmgBean
{
  private long              appSeqNum;
  private Date              createDate;
  private int               appType;
  private String            userName;
  private ClientDeployRule  cdRule;

  public void setAppSeqNum(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
  }
  public long getAppSeqNum()
  {
    return appSeqNum;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setAppCreatedDate(Timestamp appCreatedDate)
  {
    createDate = Tmg.toDate(appCreatedDate);
  }
  public Date getAppCreatedDate()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setAppType(int appType)
  {
    this.appType = appType;
  }
  public int getAppType()
  {
    return appType;
  }

  public void setAppUserLogin(String appUserLogin)
  {
    userName = appUserLogin;
  }
  public String getAppUserLogin()
  {
    return userName;
  }
  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setCdRule(ClientDeployRule cdRule)
  {
    this.cdRule = cdRule;
  }
  public ClientDeployRule getCdRule()
  {
    return cdRule;
  }

  public String toString()
  {
    return "OlaRef ["
      + " app seq num: " + appSeqNum
      + ", app type: " + appType
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + ", cd rule: " + cdRule
      + " ]";
  }
}