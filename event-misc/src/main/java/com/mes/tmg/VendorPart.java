package com.mes.tmg;

public class VendorPart
{
  private long vendPtId;
  private long vendorId;
  private long ptId;
  private long condId;
  private double unitCost;

  public void setVendPtId(long vendPtId)
  {
    this.vendPtId = vendPtId;
  }

  public long getVendPtId()
  {
    return vendPtId;
  }

  public void setVendorId(long vendorId)
  {
    this.vendorId = vendorId;
  }
  
  public long getVendorId()
  {
    return vendorId;
  }

  public void setPtId(long ptId)
  {
    this.ptId = ptId;
  }

  public long getPtId()
  {
    return ptId;
  }

  public void setCondId(long condId)
  {
    this.condId = condId;
  }

  public long getCondId()
  {
    return condId;
  }

  public void setUnitCost(double unitCost)
  {
    this.unitCost = unitCost;
  }

  public double getUnitCost()
  {
    return unitCost;
  }
  
  public String toString()
  {
    return "VendorPart [ "
      + "id: " + vendPtId 
      + ", vendorId: " + vendorId 
      + ", ptId: " + ptId
      + ", condId: " + condId
      + ", cost: " + unitCost
      + " ]";
  }
}