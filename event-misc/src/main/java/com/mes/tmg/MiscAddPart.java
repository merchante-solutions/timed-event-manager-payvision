package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;

public class MiscAddPart extends TmgBean
{
  private long      maPartId;
  private long      maId;
  private Date      createDate;
  private String    userName;
  private long      invId;
  private Inventory inventory;
  private long      buckId;
  private Bucket    bucket;
  private long      ptId;
  private PartType  partType;
  private String    pcCode;
  private String    sourceCode;
  private double    partPrice;
  private int       partCount;
  private String    etCode;
  private String    serialNum;
  private String    ownerMerchNum;
  private String    moFlag;

  public void setMaPartId(long maPartId)
  {
    this.maPartId = maPartId;
  }
  public long getMaPartId()
  {
    return maPartId;
  }
  
  public void setMaId(long maId)
  {
    this.maId = maId;
  }
  public long getMaId()
  {
    return maId;
  }
  
  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setInvId(long invId)
  {
    this.invId = invId;
  }
  public long getInvId()
  {
    return invId;
  }
  public void setInventory(Inventory inventory)
  {
    this.inventory = inventory;
  }
  public Inventory getInventory()
  {
    return inventory;
  }

  public void setBuckId(long buckId)
  {
    this.buckId = buckId;
  }
  public long getBuckId()
  {
    return buckId;
  }
  public void setBucket(Bucket bucket)
  {
    this.bucket = bucket;
  }
  public Bucket getBucket()
  {
    return bucket;
  }

  public void setPtId(long ptId)
  {
    this.ptId = ptId;
  }
  public long getPtId()
  {
    return ptId;
  }
  public void setPartType(PartType partType)
  {
    this.partType = partType;
  }
  public PartType getPartType()
  {
    return partType;
  }

  public void setPcCode(String pcCode)
  {
    this.pcCode = pcCode;
  }
  public String getPcCode()
  {
    return pcCode;
  }

  public void setSourceCode(String sourceCode)
  {
    this.sourceCode = sourceCode;
  }
  public String getSourceCode()
  {
    return sourceCode;
  }

  public void setPartPrice(double partPrice)
  {
    this.partPrice = partPrice;
  }
  public double getPartPrice()
  {
    return partPrice;
  }

  public void setPartCount(int partCount)
  {
    this.partCount = partCount;
  }
  public int getPartCount()
  {
    return partCount == 0 ? 1 : partCount;
  }

  public void setEtCode(String etCode)
  {
    this.etCode = etCode;
  }
  public String getEtCode()
  {
    return etCode;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }

  public void setOwnerMerchNum(String ownerMerchNum)
  {
    this.ownerMerchNum = ownerMerchNum;
  }
  public String getOwnerMerchNum()
  {
    return ownerMerchNum;
  }

  public void setMoFlag(String moFlag)
  {
    validateFlag(moFlag,"mo");
    this.moFlag = moFlag;
  }
  public String getMoFlag()
  {
    return flagValue(moFlag);
  }
  public boolean isMerchantOwned()
  {
    return flagBoolean(moFlag);
  }

  public String toString()
  {
    return "MiscAdd ["
      + " ma part id: " + maPartId 
      + ", ma id: " + maId 
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + ", inv id: " + invId
      + ", buck id: " + buckId
      + ", pt id: " + ptId
      + ", class: " + pcCode
      + ", source: " + sourceCode
      + ", price: " + partPrice
      + ", count: " + partCount
      + ", enc: " + etCode
      + ", serial num: " + serialNum
      + ", mo flag: " + moFlag
      + ", owner merch num: " + ownerMerchNum
      + " ]";
  }
}