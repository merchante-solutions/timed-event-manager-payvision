Notes for package com.mes.tmg

Core package for new inventory management system.

"tmg" = "Terminal Management Group" - something to call the the inventory management
module besides "inventory."

Classes

  com.mes.tmg.Tmg

    constants

  com.mes.tmg.TmgDb 

    database object, wraps com.mes.persist.Db, handles most of the persistence
    tasks for persistent tmg objects
