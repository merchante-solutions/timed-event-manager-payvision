package com.mes.tmg;

import java.util.Date;

public class LostPartsReportRow extends TmgBean
{
  private Date lostDate;
  private long partId;
  private String serialNum;
  private String clientName;
  private String ptName;
  private String modelCode;

  public void setLostDate(Date lostDate)
  {
    this.lostDate = lostDate;
  }
  public Date getLostDate()
  {
    return lostDate;
  }

  public void setPartId(long partId)
  {
    this.partId = partId;
  }
  public long getPartId()
  {
    return partId;
  }

  public void setSerialNum(String serialNum)
  {
    this.serialNum = serialNum;
  }
  public String getSerialNum()
  {
    return serialNum;
  }

  public void setClientName(String clientName)
  {
    this.clientName = clientName;
  }
  public String getClientName()
  {
    return clientName;
  }

  public void setPtName(String ptName)
  {
    this.ptName = ptName;
  }
  public String getPtName()
  {
    return ptName;
  }

  public void setModelCode(String modelCode)
  {
    this.modelCode = modelCode;
  }
  public String getModelCode()
  {
    return modelCode;
  }

  public String toString()
  {
    return "LostReportRow [ "
      + "part id: " + partId
      + ", client: " + clientName
      + ", ser num: " + serialNum
      + ", part type: " + ptName
      + ", lost date: " + lostDate
      + " ]";
  }
}