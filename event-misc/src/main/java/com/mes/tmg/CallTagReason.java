package com.mes.tmg;

public class CallTagReason extends TmgBean
{
  // swap codes
  public static final String CTRC_CASE        = "CASE";
  public static final String CTRC_DISPLAY     = "DISPLAY";
  public static final String CTRC_KEYBOARD    = "KEYBOARD";
  public static final String CTRC_MAG_RDR     = "MAG_RDR";
  public static final String CTRC_MODEM       = "MODEM";
  public static final String CTRC_PRINTER     = "PRINTER";
  public static final String CTRC_ENCRYPT     = "ENCRYPT";
  public static final String CTRC_PROGRAM     = "PROGRAM";

  // pickup/return codes
  public static final String CTRC_ACCT_CLOSE  = "ACCT_CLOSE";
  public static final String CTRC_RENT_RETURN = "RENT_RETURN";
  public static final String CTRC_DEFECTIVE   = "DEFECTIVE";

  // upgrade codes
  public static final String CTRC_UPGRADE     = "UPGRADE";

  // add codes
  public static final String CTRC_NEW         = "NEW";

  public static final String CTRC_OTHER       = "OTHER";

  private long    ctrId;
  private String  ctrCode;
  private String  ctrName;

  public void setCtrId(long ctrId)
  {
    this.ctrId = ctrId;
  }
  public long getCtrId()
  {
    return ctrId;
  }

  public void setCtrCode(String ctrCode)
  {
    this.ctrCode = ctrCode;
  }
  public String getCtrCode()
  {
    return ctrCode;
  }

  public void setCtrName(String ctrName)
  {
    this.ctrName = ctrName;
  }
  public String getCtrName()
  {
    return ctrName;
  }

  public String toString()
  {
    return "CallTagReason ["
      + " id: " + ctrId
      + ", code: " + ctrCode
      + ", name: "+ctrName
      + " ]";
  }
}