package com.mes.tmg;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class MiscAdd extends TmgBean
{
  private long    maId;
  private Date    createDate;
  private String  userName;
  private List    addParts;

  public void setMaId(long maId)
  {
    this.maId = maId;
  }
  public long getMaId()
  {
    return maId;
  }
  
  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = Tmg.toDate(createTs);
  }
  public Date getCreateTs()
  {
    return Tmg.toTimestamp(createDate);
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setAddParts(List addParts)
  {
    this.addParts = addParts;
  }
  public List getAddParts()
  {
    return addParts;
  }

  public String toString()
  {
    return "MiscAdd ["
      + " id: " + maId 
      + ", created: " + formatDate(createDate)
      + ", user: " + userName
      + " ]";
  }
}