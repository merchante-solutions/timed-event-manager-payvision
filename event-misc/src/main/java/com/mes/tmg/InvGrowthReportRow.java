package com.mes.tmg;

import java.util.Date;

public class InvGrowthReportRow extends TmgBean
{
  private String clientName;
  private String ptName;
  private long startCount;
  private Date startDate;
  private long endCount;
  private Date endDate;
  private long growthCount;

  public void setClientName(String clientName)
  {
    this.clientName = clientName;
  }
  public String getClientName()
  {
    return clientName;
  }

  public void setPtName(String ptName)
  {
    this.ptName = ptName;
  }
  public String getPtName()
  {
    return ptName;
  }

  public void setStartDate(Date startDate)
  {
    this.startDate = startDate;
  }
  public Date getStartDate()
  {
    return startDate;
  }

  public void setStartCount(long startCount)
  {
    this.startCount = startCount;
  }
  public long getStartCount()
  {
    return startCount;
  }

  public void setEndDate(Date endDate)
  {
    this.endDate = endDate;
  }
  public Date getEndDate()
  {
    return endDate;
  }

  public void setEndCount(long endCount)
  {
    this.endCount = endCount;
  }
  public long getEndCount()
  {
    return endCount;
  }

  public void setGrowthCount(long growthCount)
  {
    this.growthCount = growthCount;
  }
  public long getGrowthCount()
  {
    return growthCount;
  }

  public String toString()
  {
    return "InvGrowthReportRow [ "
      + ", client: " + clientName
      + ", part type: " + ptName
      + ", start date: " + startDate
      + ", start count: " + startCount
      + ", end date: " + endDate
      + ", end count: " + endCount
      + ", growth: " + growthCount
      + " ]";
  }
}