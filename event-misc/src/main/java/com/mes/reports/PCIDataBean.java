/*@lineinfo:filename=PCIDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;


public class PCIDataBean extends ReportSQLJBean
{

  public static String NA_STRING      = "--";

  protected long transactionCount     = 0L;
  protected int  merchLevel           = 0;
  protected String pciName            = "";
  protected String pciEmail           = "";
  protected String pciPhone           = "";
  protected List pciHistoryList       = new LinkedList();
  protected List pciScanHistoryList   = new LinkedList();

  protected boolean autoLoad()
  {
    loadPciData(getReportHierarchyNode());
    return( true );
  }

  protected boolean autoSubmit( )
  {
    String        fname       = getAutoSubmitName();
    boolean       retVal      = true;

    if ( fname.equals("submitPci") )
    {
      storePciData();
    }

    return( retVal );
  }

  protected void createFields(HttpServletRequest request)
  {
    FieldGroup      fgroup        = null;
    long            merchantId    = getReportHierarchyNode();

    fgroup = new FieldGroup("pciFields");
    fgroup.add(new DropDownField( "pciLevel", "PCI Level", new PCILevelTable(merchantId), true ));
    fgroup.add(new DropDownField( "pciVendor", "PCI Vendor", new PCIVendorTable(merchantId), true ));
    fgroup.add(new Field        ( "pciVendorOther",  "Other Vendor Name", 40,40, true ));
    fgroup.add(new Field        ( "pciAppType",  "Payment Application", 40,40, true ));
    fgroup.add(new Field        ( "pciAppTypeVer",  "Payment Application Version", 40,40, true ));
    fgroup.getField("pciVendorOther").addValidation(new PCIOtherValidation(fgroup.getField("pciVendor")));
    fgroup.add(new DropDownField( "pciScanStatus","Scan Compliance Status", new ScanComplianceTable(), true ));
    fgroup.add(new DateField    ( "quizDateLast", "Last Quiz Date", true ));
    fgroup.add(new DateField    ( "pciLastScanDate", "Last Scan Date", true ));
    fgroup.add(new DateField    ( "pciNextScanDate", "Next Scan Date", true ));
    fgroup.add(new DropDownField( "pciCompliant","Compliant", new ComplianceStatusTable(), true ));
    fgroup.add(new ButtonField  ( "submitPci","Update") );
    ((DateField)fgroup.getField("quizDateLast")).setDateFormat("MM/dd/yyyy");
    ((DateField)fgroup.getField("pciLastScanDate")).setDateFormat("MM/dd/yyyy");
    ((DateField)fgroup.getField("pciNextScanDate")).setDateFormat("MM/dd/yyyy");

    fields.add(fgroup);

    // BB&T specific fields (for now at least)
    if(getReportBankId() == 3867)
    {
      FieldGroup bbtPci = new FieldGroup("bbtPciFields");
      bbtPci.add(new RadioButtonField ("pciValRequiredInd",     yesNoRadioList,-1,true,"Required"));
      bbtPci.add(new RadioButtonField ("thirdPartySwInd",       yesNoRadioList,-1,true,"Required"));
      bbtPci.add(new Field            ("thirdPartySwName",      75,30,true));
      bbtPci.add(new Field            ("thirdPartySwVer",       75,30,true));
      bbtPci.add(new Field            ("thirdPartyName",        75,30,true));
      bbtPci.add(new RadioButtonField ("paymentAppValConfInd",  yesNoRadioList,-1,true,"Required"));
      bbtPci.add(new RadioButtonField ("thirdPartyAgentInd",    yesNoRadioList,-1,true,"Required"));
      bbtPci.add(new Field            ("thirdPartyAgentName1",  75,15,true));
      bbtPci.add(new Field            ("thirdPartyAgentName2",  75,15,true));
      bbtPci.add(new Field            ("thirdPartyAgentDesc1",  75,15,true));
      bbtPci.add(new Field            ("thirdPartyAgentDesc2",  75,15,true));

      // override existing PCI Compliant field
      bbtPci.add(new RadioButtonField ("pciCompliant",          yesNoRadioList,-1,true,"Required"));

      fields.add(bbtPci);
    }

    fields.setHtmlExtra("class=\"formFields\"");
  }


  public class PCIHistory
  {
    private Date quizDate;
    private Date quizDateNext;
    private String result;
    private String trackNum;
    private String user;
    private String compliant;

    public PCIHistory()
    {
      trackNum = "";
      quizDate = null;
      quizDateNext = null;
      result = "";
      user = "";
      compliant = "";
    }

    public PCIHistory(String trackNum, Date quizDate, String result, String user, String compliant)
    {
      this.trackNum   = trackNum;
      this.quizDate   = quizDate;
      this.result     = result;
      this.user       = user;
      this.compliant  = compliant;
    }

    public String getTrackingNumber()
    {
      return trackNum;
    }

    public String getUser()
    {
      return user;
    }

    public String getCompliant()
    {
      return compliant;
    }

    public boolean isSameDate(Date incoming)
    {
      System.out.println("history date = " + quizDate.getTime());
      System.out.println("incoming date = " + incoming.getTime());

      if(quizDate.getTime() == incoming.getTime())
      {
        return true;
      }
      return false;
    }

    public String getQuizDateStr()
    {
      return decodeDate(quizDate);
    }

    public void setQuizDateNext(Date nDate)
    {
      quizDateNext = nDate;
    }

    public String getQuizDateNextStr()
    {
      return decodeDate(quizDateNext);
    }

    public String getResult()
    {
      return result==null?"":result.toUpperCase();
    }

    private String decodeDate(Date d)
    {
      String dateStr = "n/a";
      if(d!=null)
      {
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy");
        dateStr = df.format(d);
      }

      return dateStr;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer();
      sb.append(getResult()).append(" ").append(getQuizDateStr()).append(" ");
      sb.append("<a href = '../credit/contact_info.jsp?userName=").append(getUser()).append("'>");
      sb.append(getUser()).append("</a>");
      return sb.toString();
    }
  }

  public class PCIScanHistory
  {
    private Date scanDateLast;
    private Date scanDateNext;
    private int status;
    String username;

    public PCIScanHistory()
    {
      scanDateLast = null;
      scanDateNext = null;
      status = 0;
      username = "";
    }

    public PCIScanHistory(int status, Date l, Date n, String u)
    {
      this.status   = status;
      scanDateLast  = l;
      scanDateNext  = n;
      username = u;
    }

    public String getLastDateStr()
    {
      return decodeDate(scanDateLast);
    }

    public String getNextDateStr()
    {
      return decodeDate(scanDateNext);
    }

    public String getUsername()
    {
      return username;
    }

    public String getStatus()
    {
      String result;
      switch(status)
      {
        case 0:
          result = "No";
          break;
        case 1:
          result = "Yes";
          break;
        case 2:
          result = "In Progress";
          break;
        case 3:
        default:
          result = "NA";
          break;
      }
      return result;
    }

    private String decodeDate(Date d)
    {
      String dateStr = "n/a";
      if(d!=null)
      {
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy");
        dateStr = df.format(d);
      }

      return dateStr;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer();
      sb.append(getStatus()).append(" ").append(getLastDateStr()).append(" ").append(getUsername());
      return sb.toString();
    }
  }

  private String notNull(String obj)
  {
    if (obj == null)
    {
      return "";
    }
    return obj;
  }

  public void loadPciData( long merchantId )
  {
    ResultSetIterator     it            = null;
    ResultSet             resultSet     = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:319^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    risk_pci_level               as pci_level,
//                    risk_pci_vendor_code         as pci_vendor,
//                    risk_pci_vendor_other        as pci_vendor_other,
//                    risk_pci_last_scan_date      as pci_last_scan_date,
//                    risk_pci_next_scan_date      as pci_next_scan_date,
//                    risk_pci_compliant           as pci_compliant
//          from      merchant
//          where     merch_number = :merchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    risk_pci_level               as pci_level,\n                  risk_pci_vendor_code         as pci_vendor,\n                  risk_pci_vendor_other        as pci_vendor_other,\n                  risk_pci_last_scan_date      as pci_last_scan_date,\n                  risk_pci_next_scan_date      as pci_next_scan_date,\n                  risk_pci_compliant           as pci_compliant\n        from      merchant\n        where     merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.PCIDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.PCIDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:329^7*/
      resultSet = it.getResultSet();

      if( resultSet.next() )
      {
        setFields(resultSet,false);  // set fields, do not scan result set
      }
      resultSet.close();
      it.close();

      //load PCI History List
      loadPCIHistory(merchantId);

      //Load Scan Status history (if applicable)
      /*@lineinfo:generated-code*//*@lineinfo:343^7*/

//  ************************************************************
//  #sql [Ctx] it = { select
//            status              as status,
//            last_scan_date      as last_scan_date,
//            last_scan_date + 91 as next_scan_date,
//            username            as username
//          from
//            pci_scan_compliance
//          where
//            merchant_number = :merchantId
//          order by
//            last_scan_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n          status              as status,\n          last_scan_date      as last_scan_date,\n          last_scan_date + 91 as next_scan_date,\n          username            as username\n        from\n          pci_scan_compliance\n        where\n          merchant_number =  :1 \n        order by\n          last_scan_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.PCIDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.PCIDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:356^7*/
      resultSet = it.getResultSet();

      PCIScanHistory sHistory;
      boolean loadedCurrent = false;

      while( resultSet.next() )
      {
        //populate quiz history
        sHistory = new PCIScanHistory( resultSet.getInt("status"),
                                       resultSet.getDate("last_scan_date"),
                                       resultSet.getDate("next_scan_date"),
                                       resultSet.getString("username")
                                     );
        pciScanHistoryList.add(sHistory);

        if(!loadedCurrent)
        {
          //populate the current fields
          ((DateField)fields.getField("pciLastScanDate")).setUtilDate(resultSet.getDate("last_scan_date"));
          ((DateField)fields.getField("pciNextScanDate")).setUtilDate(resultSet.getDate("next_scan_date"));
          loadedCurrent = true;
        }
      }
      resultSet.close();
      it.close();

      //Set Merchant Level and transaction count
      //same as in PCIQuestionnaire
      //OLD select  nvl(round(((sum(sm.vmc_sales_count)/count(sm.active_date))*12)),0)
      long count = 0;
      /*@lineinfo:generated-code*//*@lineinfo:387^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(sm.vmc_sales_count),0)
//          
//          from    monthly_extract_summary sm
//          where   sm.merchant_number = :merchantId and
//                  sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(sm.vmc_sales_count),0)\n         \n        from    monthly_extract_summary sm\n        where   sm.merchant_number =  :1  and\n                sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.PCIDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:394^7*/

      this.transactionCount = count;

      if(this.transactionCount > 6000000L)
      {
        merchLevel = 1;
      }
      else if(6000000L >= this.transactionCount && this.transactionCount >= 1000000L)
      {
        merchLevel = 2;
      }
      else
      {
        //check only ecommerce transactions
        /*@lineinfo:generated-code*//*@lineinfo:409^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(sm.vmc_sales_count),0)
//            
//            from    monthly_extract_summary   sm,
//                    merch_pos mp,
//                    pos_category pc,
//                    merchant m
//            where   sm.merchant_number = :merchantId and
//                    sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month') and
//                    sm.merchant_number = m.merch_number and
//                    m.app_seq_num = mp.app_seq_num and
//                    mp.pos_code = pc.pos_code and
//                    pc.pos_type not in (1, 4) --everything else could be considered e-commerce
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(sm.vmc_sales_count),0)\n           \n          from    monthly_extract_summary   sm,\n                  merch_pos mp,\n                  pos_category pc,\n                  merchant m\n          where   sm.merchant_number =  :1  and\n                  sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month') and\n                  sm.merchant_number = m.merch_number and\n                  m.app_seq_num = mp.app_seq_num and\n                  mp.pos_code = pc.pos_code and\n                  pc.pos_type not in (1, 4) --everything else could be considered e-commerce";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.PCIDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:423^9*/

        if(count < 20000)
        {
         merchLevel = 4;
        }
        else
        {
         merchLevel = 3;
        }
      }


      //Establish extra BBT data
      if(getReportBankId() == 3867)
      {
        // retrieve other PCI data fields
        /*@lineinfo:generated-code*//*@lineinfo:440^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  bbt.pci_val_required_ind      pci_val_required_ind,
//                    bbt.third_party_sw_ind        third_party_sw_ind,
//                    bbt.third_party_sw_name       third_party_sw_name,
//                    bbt.third_party_name          third_party_name,
//                    bbt.third_party_sw_ver        third_party_sw_ver,
//                    bbt.payment_app_val_conf_ind  payment_app_val_conf_ind,
//                    bbt.third_party_agent_ind     third_party_agent_ind,
//                    bbt.third_party_agent_name_1  third_party_agent_name_1,
//                    bbt.third_party_agent_name_2  third_party_agent_name_2,
//                    bbt.third_party_agent_desc_1  third_party_agent_desc_1,
//                    bbt.third_party_agent_desc_2  third_party_agent_desc_2
//            from    app_merch_bbt bbt,
//                    merchant      mr
//            where   mr.merch_number = :merchantId and
//                    mr.app_seq_num = bbt.app_seq_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bbt.pci_val_required_ind      pci_val_required_ind,\n                  bbt.third_party_sw_ind        third_party_sw_ind,\n                  bbt.third_party_sw_name       third_party_sw_name,\n                  bbt.third_party_name          third_party_name,\n                  bbt.third_party_sw_ver        third_party_sw_ver,\n                  bbt.payment_app_val_conf_ind  payment_app_val_conf_ind,\n                  bbt.third_party_agent_ind     third_party_agent_ind,\n                  bbt.third_party_agent_name_1  third_party_agent_name_1,\n                  bbt.third_party_agent_name_2  third_party_agent_name_2,\n                  bbt.third_party_agent_desc_1  third_party_agent_desc_1,\n                  bbt.third_party_agent_desc_2  third_party_agent_desc_2\n          from    app_merch_bbt bbt,\n                  merchant      mr\n          where   mr.merch_number =  :1  and\n                  mr.app_seq_num = bbt.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.PCIDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.PCIDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:457^9*/

        resultSet = it.getResultSet();

        if(resultSet.next())
        {
          setFields(resultSet,false);
        }

        resultSet.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("loadPciData()", e.toString());
    }
    finally
    {
      try{ resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch( Exception e ) {}
    }
  }


  private void loadPCIHistory(long merchantId)
  {
    ResultSetIterator     it            = null;
    ResultSet             resultSet     = null;

    //ditch whatever was in the list previously
    pciHistoryList.clear();

    try
    {
      //Load PCI Quiz History and Complaince data
      /*@lineinfo:generated-code*//*@lineinfo:493^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(pcd.m_contactname,'')     as pci_name,
//                  nvl(pcd.m_email,'')           as pci_email,
//                  nvl(pcd.m_telephone,'')       as pci_phone,
//                  nvl(pcd.paymentapp,'')        as app,
//                  nvl(pcd.paymentappver,'')     as app_ver,
//                  nvl(pcd.compliant,'')         as compliant,
//                  nvl(qh.tracking_number,'')    as tracking_number,
//                  qh.date_taken                 as quiz_date,
//                  qh.date_taken + 365           as quiz_date_next,
//                  qh.result                     as result,
//                  nvl(qh.username,'--')         as username
//          from    pci_compliance_data pcd,
//                  quiz_history qh
//          where   qh.merch_number = :merchantId
//          and     qh.tracking_number = pcd.tracking_number(+)
//          order by
//                  qh.id desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(pcd.m_contactname,'')     as pci_name,\n                nvl(pcd.m_email,'')           as pci_email,\n                nvl(pcd.m_telephone,'')       as pci_phone,\n                nvl(pcd.paymentapp,'')        as app,\n                nvl(pcd.paymentappver,'')     as app_ver,\n                nvl(pcd.compliant,'')         as compliant,\n                nvl(qh.tracking_number,'')    as tracking_number,\n                qh.date_taken                 as quiz_date,\n                qh.date_taken + 365           as quiz_date_next,\n                qh.result                     as result,\n                nvl(qh.username,'--')         as username\n        from    pci_compliance_data pcd,\n                quiz_history qh\n        where   qh.merch_number =  :1 \n        and     qh.tracking_number = pcd.tracking_number(+)\n        order by\n                qh.id desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.PCIDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.PCIDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:512^7*/
      resultSet = it.getResultSet();

      boolean loadedCurrent = false;

      PCIHistory history;
      while( resultSet.next() )
      {
        if(!loadedCurrent)
        {
          pciName   = notNull(resultSet.getString("pci_name"));
          pciEmail  = notNull(resultSet.getString("pci_email"));
          pciPhone  = PhoneField.format(notNull(resultSet.getString("pci_phone")));
          setData("pciAppType",notNull(resultSet.getString("app")));
          setData("pciAppTypeVer",notNull(resultSet.getString("app_ver")));
          ((DateField)getField("quizDateLast")).setUtilDate(resultSet.getDate("quiz_date"));
          loadedCurrent = true;
        }

        //populate quiz history
        history = new PCIHistory( resultSet.getString("tracking_number"),
                                  resultSet.getDate("quiz_date"),
                                  resultSet.getString("result"),
                                  resultSet.getString("username"),
                                  resultSet.getString("compliant")
                                );
        history.setQuizDateNext(resultSet.getDate("quiz_date_next"));
        pciHistoryList.add(history);

      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadPciData()", e.toString());
    }
    finally
    {
      try{ resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch( Exception e ) {}
    }
  }

  protected void storePciData( )
  {
    int           appCount    = 0;
    int           mifCount    = 0;
    long          merchantId  = getReportHierarchyNode();

    System.out.println("calling storePciData()");

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:568^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(mf.merchant_number)               as mf_count,
//                  sum( decode(mr.merch_number,null,0,1) ) as mr_count
//          
//          from    mif             mf,
//                  merchant        mr
//          where   mf.merchant_number = :merchantId and
//                  mr.merch_number(+) = mf.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mf.merchant_number)               as mf_count,\n                sum( decode(mr.merch_number,null,0,1) ) as mr_count\n         \n        from    mif             mf,\n                merchant        mr\n        where   mf.merchant_number =  :1  and\n                mr.merch_number(+) = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.PCIDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mifCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   appCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:577^7*/

      if ( mifCount > 0 )
      {
        Date        lastScanDate      = null;
        Date        nextScanDate      = null;

        if ( appCount == 0 )
        {
          int     appType = 0;

          /*@lineinfo:generated-code*//*@lineinfo:588^11*/

//  ************************************************************
//  #sql [Ctx] { select  oa.app_type 
//              from    t_hierarchy           th,
//                      org_app               oa,
//                      mif                   mf
//              where   th.hier_type = 1 and
//                      th.ancestor = oa.hierarchy_node and
//                      th.entity_type = 4 and
//                      th.relation =
//                      (
//                        select min(thi.relation)
//                        from   t_hierarchy thi,
//                               mif         mfi
//                        where  thi.hier_type = 1 and
//                               thi.ancestor in
//                               (
//                                  select oai.hierarchy_node
//                                  from    org_app   oai
//                               ) and
//                               thi.entity_type = 4 and
//                               mfi.association_node = thi.descendent and
//                               mfi.merchant_number = :merchantId
//                      ) and
//                      mf.association_node = th.descendent and
//                      mf.merchant_number = :merchantId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  oa.app_type  \n            from    t_hierarchy           th,\n                    org_app               oa,\n                    mif                   mf\n            where   th.hier_type = 1 and\n                    th.ancestor = oa.hierarchy_node and\n                    th.entity_type = 4 and\n                    th.relation =\n                    (\n                      select min(thi.relation)\n                      from   t_hierarchy thi,\n                             mif         mfi\n                      where  thi.hier_type = 1 and\n                             thi.ancestor in\n                             (\n                                select oai.hierarchy_node\n                                from    org_app   oai\n                             ) and\n                             thi.entity_type = 4 and\n                             mfi.association_node = thi.descendent and\n                             mfi.merchant_number =  :1 \n                    ) and\n                    mf.association_node = th.descendent and\n                    mf.merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.PCIDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:614^11*/

          // this procedure will only create the app
          // if one does not already exist
          /*@lineinfo:generated-code*//*@lineinfo:618^11*/

//  ************************************************************
//  #sql [Ctx] { call create_shadow_app(:merchantId, :appType )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN create_shadow_app( :1 ,  :2  )\n          \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.PCIDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setInt(2,appType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:621^11*/
        }

        lastScanDate = ((DateField)fields.getField("pciLastScanDate")).getSqlDate();
        nextScanDate = ((DateField)fields.getField("pciNextScanDate")).getSqlDate();

        //keep it in sync with the new PCI tables... not exactly legacy since
        //we'll still need this data for level 1-3 merchants as questionnaire
        //currently only applies to level 4 merchants
        /*@lineinfo:generated-code*//*@lineinfo:630^9*/

//  ************************************************************
//  #sql [Ctx] { update merchant
//            set     risk_pci_level          = :getData("pciLevel"),
//                    risk_pci_vendor_code    = :getData("pciVendor"),
//                    risk_pci_vendor_other   = :getData("pciVendorOther"),
//                    risk_pci_last_scan_date = :lastScanDate,
//                    risk_pci_next_scan_date = :nextScanDate,
//                    risk_pci_compliant      = :getData("pciCompliant")
//            where   merch_number = :merchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1218 = getData("pciLevel");
 String __sJT_1219 = getData("pciVendor");
 String __sJT_1220 = getData("pciVendorOther");
 String __sJT_1221 = getData("pciCompliant");
   String theSqlTS = "update merchant\n          set     risk_pci_level          =  :1 ,\n                  risk_pci_vendor_code    =  :2 ,\n                  risk_pci_vendor_other   =  :3 ,\n                  risk_pci_last_scan_date =  :4 ,\n                  risk_pci_next_scan_date =  :5 ,\n                  risk_pci_compliant      =  :6 \n          where   merch_number =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.reports.PCIDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1218);
   __sJT_st.setString(2,__sJT_1219);
   __sJT_st.setString(3,__sJT_1220);
   __sJT_st.setDate(4,lastScanDate);
   __sJT_st.setDate(5,nextScanDate);
   __sJT_st.setString(6,__sJT_1221);
   __sJT_st.setLong(7,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:640^9*/

        //insert scan compliance table - number of records won't matter
        //but don't add a record if nothing was selected
        if(getField("pciScanStatus").asInteger() > -1)
        {
          /*@lineinfo:generated-code*//*@lineinfo:646^11*/

//  ************************************************************
//  #sql [Ctx] { insert into pci_scan_compliance
//              (
//                merchant_number,
//                status,
//                last_scan_date,
//                username
//              )
//              values
//              (
//                :merchantId,
//                :getData("pciScanStatus"),
//                :lastScanDate,
//                :getUserLoginName()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1222 = getData("pciScanStatus");
 String __sJT_1223 = getUserLoginName();
   String theSqlTS = "insert into pci_scan_compliance\n            (\n              merchant_number,\n              status,\n              last_scan_date,\n              username\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.reports.PCIDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setString(2,__sJT_1222);
   __sJT_st.setDate(3,lastScanDate);
   __sJT_st.setString(4,__sJT_1223);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:662^11*/
        }

        //update new PCI tables, since CIF changes trump PCI submissions
        //if no existing history, or current PCIHistory was a MES internal
        //generate a new MES internal
        boolean recordChanged = true;
        boolean quizExists    = true;

        //-1 means shadow quiz and no track num,
        //0 means augmenting existing quiz (using same track num)
        int id                = -1;
        String trackNum       = null;

        Date lastQuizDate = ((DateField)fields.getField("quizDateLast")).getSqlDate();

        //need to load this again
        loadPCIHistory(merchantId);

        if(pciHistoryList.size() > 0)
        {
          PCIHistory currentHistory = (PCIHistory)pciHistoryList.get(0);
          try
          {

            System.out.println(currentHistory.toString()+" compliant?... "+currentHistory.getCompliant());

            //ensure that there's a tracking number
            if(!currentHistory.getTrackingNumber().equals(""))
            {
              //do not create a shadow, just update the existing one
              quizExists  = true;
              id          = 0;
              trackNum    = currentHistory.getTrackingNumber();
            }

            if( getData("pciCompliant").equals(currentHistory.getCompliant()) &&
                currentHistory.isSameDate(lastQuizDate)
              )
            {
              recordChanged = false;
            }
          }
          catch(Exception e1)
          {
            e1.printStackTrace();
            //ignore this
          }
        }

        //if record changes, insert a new quiz history,
        if(recordChanged)
        {

          /*@lineinfo:generated-code*//*@lineinfo:716^11*/

//  ************************************************************
//  #sql [Ctx] { insert into quiz_history
//              (
//                quiz_id,
//                merch_number,
//                date_taken,
//                result,
//                tracking_number,
//                username
//              )
//              values
//              (
//                :id,
//                :merchantId,
//                :lastQuizDate,
//                :decodeComplianceFieldToResult(),
//                :trackNum,
//                :getUserLoginName()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1224 = decodeComplianceFieldToResult();
 String __sJT_1225 = getUserLoginName();
   String theSqlTS = "insert into quiz_history\n            (\n              quiz_id,\n              merch_number,\n              date_taken,\n              result,\n              tracking_number,\n              username\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.reports.PCIDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,id);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setDate(3,lastQuizDate);
   __sJT_st.setString(4,__sJT_1224);
   __sJT_st.setString(5,trackNum);
   __sJT_st.setString(6,__sJT_1225);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:736^11*/
        }

        //always update the fields in pci_compliance_data if the quiz exists
        //this may introduce "non-recognized" compliance information ('P', 'U')
        //but that shouldn't cause an issue with the quiz mechanism -
        //NEW ADD: always update the date taken if it's not null
        if(quizExists)
        {

          /*@lineinfo:generated-code*//*@lineinfo:746^11*/

//  ************************************************************
//  #sql [Ctx] { update
//                pci_compliance_data
//              set
//                compliant     = :getData("pciCompliant"),
//                paymentapp    = :getData("pciAppType"),
//                paymentappver = :getData("pciAppTypeVer")
//              where
//                tracking_number = :trackNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1226 = getData("pciCompliant");
 String __sJT_1227 = getData("pciAppType");
 String __sJT_1228 = getData("pciAppTypeVer");
  try {
   String theSqlTS = "update\n              pci_compliance_data\n            set\n              compliant     =  :1 ,\n              paymentapp    =  :2 ,\n              paymentappver =  :3 \n            where\n              tracking_number =  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.PCIDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1226);
   __sJT_st.setString(2,__sJT_1227);
   __sJT_st.setString(3,__sJT_1228);
   __sJT_st.setString(4,trackNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:756^11*/

          if(lastQuizDate!=null)
          {
            /*@lineinfo:generated-code*//*@lineinfo:760^13*/

//  ************************************************************
//  #sql [Ctx] { update
//                  quiz_history
//                set
//                  date_taken     = :lastQuizDate
//                where
//                  tracking_number = :trackNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "update\n                quiz_history\n              set\n                date_taken     =  :1 \n              where\n                tracking_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.PCIDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,lastQuizDate);
   __sJT_st.setString(2,trackNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:768^13*/
          }
        }


        // store BB&T-specific PCI data
        if(getReportBankId() == 3867)
        {
          int appSeqNum   = 0;
          int pciCount    = 0;

          /*@lineinfo:generated-code*//*@lineinfo:779^11*/

//  ************************************************************
//  #sql [Ctx] { select  app_seq_num
//              
//              from    merchant
//              where   merch_number = :merchantId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_seq_num\n             \n            from    merchant\n            where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.PCIDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:785^11*/

          /*@lineinfo:generated-code*//*@lineinfo:787^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//              
//              from    app_merch_bbt
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n             \n            from    app_merch_bbt\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.PCIDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   pciCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:793^11*/

          if(pciCount == 0)
          {
            // insert new record
            /*@lineinfo:generated-code*//*@lineinfo:798^13*/

//  ************************************************************
//  #sql [Ctx] { insert into app_merch_bbt
//                (
//                  app_seq_num,
//                  pci_val_required_ind,
//                  third_party_name,
//                  third_party_sw_ind,
//                  third_party_sw_name,
//                  third_party_sw_ver,
//                  payment_app_val_conf_ind,
//                  third_party_agent_ind,
//                  third_party_agent_name_1,
//                  third_party_agent_name_2,
//                  third_party_agent_desc_1,
//                  third_party_agent_desc_2
//                )
//                values
//                (
//                  :appSeqNum,
//                  :getData("pciValRequiredInd"),
//                  :getData("thirdPartyName"),
//                  :getData("thirdPartySwInd"),
//                  :getData("thirdPartySwName"),
//                  :getData("thirdPartySwVer"),
//                  :getData("paymentAppValConfInd"),
//                  :getData("thirdPartyAgentInd"),
//                  :getData("thirdPartyAgentName1"),
//                  :getData("thirdPartyAgentName2"),
//                  :getData("thirdPartyAgentDesc1"),
//                  :getData("thirdPartyAgentDesc2")
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1229 = getData("pciValRequiredInd");
 String __sJT_1230 = getData("thirdPartyName");
 String __sJT_1231 = getData("thirdPartySwInd");
 String __sJT_1232 = getData("thirdPartySwName");
 String __sJT_1233 = getData("thirdPartySwVer");
 String __sJT_1234 = getData("paymentAppValConfInd");
 String __sJT_1235 = getData("thirdPartyAgentInd");
 String __sJT_1236 = getData("thirdPartyAgentName1");
 String __sJT_1237 = getData("thirdPartyAgentName2");
 String __sJT_1238 = getData("thirdPartyAgentDesc1");
 String __sJT_1239 = getData("thirdPartyAgentDesc2");
   String theSqlTS = "insert into app_merch_bbt\n              (\n                app_seq_num,\n                pci_val_required_ind,\n                third_party_name,\n                third_party_sw_ind,\n                third_party_sw_name,\n                third_party_sw_ver,\n                payment_app_val_conf_ind,\n                third_party_agent_ind,\n                third_party_agent_name_1,\n                third_party_agent_name_2,\n                third_party_agent_desc_1,\n                third_party_agent_desc_2\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                 :5 ,\n                 :6 ,\n                 :7 ,\n                 :8 ,\n                 :9 ,\n                 :10 ,\n                 :11 ,\n                 :12 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.reports.PCIDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   __sJT_st.setString(2,__sJT_1229);
   __sJT_st.setString(3,__sJT_1230);
   __sJT_st.setString(4,__sJT_1231);
   __sJT_st.setString(5,__sJT_1232);
   __sJT_st.setString(6,__sJT_1233);
   __sJT_st.setString(7,__sJT_1234);
   __sJT_st.setString(8,__sJT_1235);
   __sJT_st.setString(9,__sJT_1236);
   __sJT_st.setString(10,__sJT_1237);
   __sJT_st.setString(11,__sJT_1238);
   __sJT_st.setString(12,__sJT_1239);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:830^13*/
          }
          else
          {
            /*@lineinfo:generated-code*//*@lineinfo:834^13*/

//  ************************************************************
//  #sql [Ctx] { update  app_merch_bbt
//                set     pci_val_required_ind      = :getData("pciValRequiredInd"),
//                        third_party_name          = :getData("thirdPartyName"),
//                        third_party_sw_ind        = :getData("thirdPartySwInd"),
//                        third_party_sw_name       = :getData("thirdPartySwName"),
//                        third_party_sw_ver        = :getData("thirdPartySwVer"),
//                        payment_app_val_conf_ind  = :getData("paymentAppValConfInd"),
//                        third_party_agent_ind     = :getData("thirdPartyAgentInd"),
//                        third_party_agent_name_1  = :getData("thirdPartyAgentName1"),
//                        third_party_agent_name_2  = :getData("thirdPartyAgentName2"),
//                        third_party_agent_desc_1  = :getData("thirdPartyAgentDesc1"),
//                        third_party_agent_desc_2  = :getData("thirdPartyAgentDesc2")
//                where   app_seq_num = :appSeqNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1240 = getData("pciValRequiredInd");
 String __sJT_1241 = getData("thirdPartyName");
 String __sJT_1242 = getData("thirdPartySwInd");
 String __sJT_1243 = getData("thirdPartySwName");
 String __sJT_1244 = getData("thirdPartySwVer");
 String __sJT_1245 = getData("paymentAppValConfInd");
 String __sJT_1246 = getData("thirdPartyAgentInd");
 String __sJT_1247 = getData("thirdPartyAgentName1");
 String __sJT_1248 = getData("thirdPartyAgentName2");
 String __sJT_1249 = getData("thirdPartyAgentDesc1");
 String __sJT_1250 = getData("thirdPartyAgentDesc2");
   String theSqlTS = "update  app_merch_bbt\n              set     pci_val_required_ind      =  :1 ,\n                      third_party_name          =  :2 ,\n                      third_party_sw_ind        =  :3 ,\n                      third_party_sw_name       =  :4 ,\n                      third_party_sw_ver        =  :5 ,\n                      payment_app_val_conf_ind  =  :6 ,\n                      third_party_agent_ind     =  :7 ,\n                      third_party_agent_name_1  =  :8 ,\n                      third_party_agent_name_2  =  :9 ,\n                      third_party_agent_desc_1  =  :10 ,\n                      third_party_agent_desc_2  =  :11 \n              where   app_seq_num =  :12";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.reports.PCIDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1240);
   __sJT_st.setString(2,__sJT_1241);
   __sJT_st.setString(3,__sJT_1242);
   __sJT_st.setString(4,__sJT_1243);
   __sJT_st.setString(5,__sJT_1244);
   __sJT_st.setString(6,__sJT_1245);
   __sJT_st.setString(7,__sJT_1246);
   __sJT_st.setString(8,__sJT_1247);
   __sJT_st.setString(9,__sJT_1248);
   __sJT_st.setString(10,__sJT_1249);
   __sJT_st.setString(11,__sJT_1250);
   __sJT_st.setInt(12,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:849^13*/
          }
        }
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
      logEntry("storePciData()",e.toString());
    }
    finally
    {
      cleanUp();

      //reload all data
      autoLoad();
    }
  }

  //returns either 'Complete' or 'Incomplete' depending on
  //the compliance dropdown field... assumes a definitive answer
  //Y indicates the quiz is Complete, in progress or non-compliant
  //indicates Incomplete and a blank = none
  private String decodeComplianceFieldToResult()
  {
    String result = "None";

    String compliance = getData("pciCompliant");
    if(compliance.equals("P") || compliance.equals("N"))
    {
      result = "Incomplete";
    }
    else if(compliance.equals("Y"))
    {
      result = "Complete";
    }

    return result;

  }

  //hijacked - need this for simplicity in PCI field description
  public String renderHtml(String fieldName)
  {
    if(fieldName.equals("pciQuizResults"))
    {
      PCIHistory history;
      StringBuffer sb = new StringBuffer();
      int count = 1;
      for(Iterator i = pciHistoryList.iterator();i.hasNext() && count<9;)
      {
        count++;
        history = (PCIHistory)i.next();
        sb.append(history.toString()).append("<br>");
      }
      //ditch the last <br>
      if(sb.length()>4)
      {
        sb.setLength(sb.length()-4);
      }

      if(sb.length()==0)
      {
        sb.append(NA_STRING);
      }

      return sb.toString();
    }
    else if(fieldName.equals("scanDateHistory"))
    {
      PCIScanHistory history;
      StringBuffer sb = new StringBuffer();
      int count = 1;
      for(Iterator i = pciScanHistoryList.iterator();i.hasNext() && count<9;)
      {
        count++;
        history = (PCIScanHistory)i.next();
        sb.append(history.toString()).append("<br>");
      }
      //ditch the last <br>
      if(sb.length()>4)
      {
        sb.setLength(sb.length()-4);
      }
      return sb.toString();
    }
    else if(fieldName.equals("pciLevelRec"))
    {
      return ""+merchLevel;
    }
    else if(fieldName.equals("pciLevelTrans"))
    {
      return ""+ transactionCount;
    }
    else if(fieldName.equals("pciName"))
    {
      return pciName.equals("")?NA_STRING:pciName;
    }
    else if(fieldName.equals("pciEmail"))
    {
      return pciEmail.equals("")?NA_STRING:pciEmail;
    }
    else if(fieldName.equals("pciPhone"))
    {
      return pciPhone.equals("")?NA_STRING:pciPhone;
    }
/*    else if(fieldName.equals("quizDateLast"))
    {
      if(pciHistoryList.size()>0)
      {
        return ((PCIHistory)pciHistoryList.get(0)).getQuizDateStr();
      }
      else
      {
        return NA_STRING;
      }
    }
*/  else if(fieldName.equals("quizDateNext"))
    {
      if(pciHistoryList.size()>0)
      {
        return ((PCIHistory)pciHistoryList.get(0)).getQuizDateNextStr();
      }
      else
      {
        return NA_STRING;
      }
    }
    else if(fieldName.equals("pciNextScanDate"))
    {
      if(pciScanHistoryList.size()>0)
      {
        return ((PCIScanHistory)pciScanHistoryList.get(0)).getNextDateStr();
      }
      else
      {
        return NA_STRING;
      }
    }
    else
    {
      return super.renderHtml(fieldName);
    }
  }

  protected static final String[][] yesNoRadioList =
  {
    { "No",  "N" },
    { "Yes", "Y" }
  };

  public static class PCIOtherValidation implements Validation
  {
    private String  ErrorMessage  = null;
    private Field   VendorCode    = null;

    public PCIOtherValidation( Field vendorCode )
    {
      VendorCode = vendorCode;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      String      vendorCode    = VendorCode.getData();

      ErrorMessage = null;
      if ( vendorCode.equals("OTHER") &&
           (fdata == null || fdata.equals("")) )
      {
        ErrorMessage = "Must specify other PCI vendor name";
      }
      return( ErrorMessage == null );
    }
  }
  public static class PCILevelTable extends DropDownTable
  {
    public PCILevelTable( long merchantId )
    {
      ResultSetIterator       it = null;
      ResultSet               rs = null;

      try
      {
        connect();

        addElement("",  "select");

        /*@lineinfo:generated-code*//*@lineinfo:1041^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  level_id,
//                    level_desc
//            from    t_hierarchy           th,
//                    risk_pci_levels       pcl,
//                    mif                   mf
//            where   th.hier_type = 1 and
//                    th.ancestor = pcl.hierarchy_node and
//                    th.entity_type = 4 and
//                    th.relation =
//                    (
//                      select min(to_number(thi.relation))
//                      from   t_hierarchy thi,
//                             mif         mfi
//                      where  thi.hier_type = 1 and
//                             thi.ancestor in
//                             (
//                               select pcli.hierarchy_node
//                               from   risk_pci_levels pcli
//                             ) and
//                             thi.entity_type = 4 and
//                             mfi.association_node = thi.descendent and
//                             mfi.merchant_number = :merchantId
//                    ) and
//                    mf.association_node = th.descendent and
//                    mf.merchant_number = :merchantId
//            order by pcl.display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  level_id,\n                  level_desc\n          from    t_hierarchy           th,\n                  risk_pci_levels       pcl,\n                  mif                   mf\n          where   th.hier_type = 1 and\n                  th.ancestor = pcl.hierarchy_node and\n                  th.entity_type = 4 and\n                  th.relation =\n                  (\n                    select min(to_number(thi.relation))\n                    from   t_hierarchy thi,\n                           mif         mfi\n                    where  thi.hier_type = 1 and\n                           thi.ancestor in\n                           (\n                             select pcli.hierarchy_node\n                             from   risk_pci_levels pcli\n                           ) and\n                           thi.entity_type = 4 and\n                           mfi.association_node = thi.descendent and\n                           mfi.merchant_number =  :1 \n                  ) and\n                  mf.association_node = th.descendent and\n                  mf.merchant_number =  :2 \n          order by pcl.display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.reports.PCIDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.reports.PCIDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1069^9*/
        rs = it.getResultSet();

        while( rs.next() )
        {
          addElement(rs);
        }

        rs.close();
        it.close();
      }
      catch( Exception e )
      {
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }

  public static class ComplianceStatusTable extends DropDownTable
  {
    public ComplianceStatusTable( )
    {
      addElement("",  "None");
      addElement("Y", "Compliant");
      addElement("N", "Non-Compliant");
    }
  }

  public static class ScanComplianceTable extends DropDownTable
  {
    public ScanComplianceTable( )
    {
      addElement("-1",  "select");
      addElement("1", "Yes");
      addElement("0", "No");
      addElement("2", "In Progress");
      addElement("3", "Not Applicable");
    }
  }

  public static class PCIVendorTable extends DropDownTable
  {
    public PCIVendorTable( long merchantId )
    {
      ResultSetIterator     it          = null;
      ResultSet             resultSet   = null;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:1125^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  pv.vendor_code,
//                    pv.vendor_name
//            from    t_hierarchy           th,
//                    risk_pci_vendors      pv,
//                    mif                   mf
//            where   th.hier_type = 1 and
//                    th.ancestor = pv.hierarchy_node and
//                    th.entity_type = 4 and
//                    th.relation =
//                    (
//                      select min(to_number(thi.relation))
//                      from   t_hierarchy thi,
//                             mif         mfi
//                      where  thi.hier_type = 1 and
//                             thi.ancestor in
//                             (
//                               select pvi.hierarchy_node
//                               from   risk_pci_vendors pvi
//                             ) and
//                             thi.entity_type = 4 and
//                             mfi.association_node = thi.descendent and
//                             mfi.merchant_number = :merchantId
//                    ) and
//                    mf.association_node = th.descendent and
//                    mf.merchant_number = :merchantId
//            order by pv.display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pv.vendor_code,\n                  pv.vendor_name\n          from    t_hierarchy           th,\n                  risk_pci_vendors      pv,\n                  mif                   mf\n          where   th.hier_type = 1 and\n                  th.ancestor = pv.hierarchy_node and\n                  th.entity_type = 4 and\n                  th.relation =\n                  (\n                    select min(to_number(thi.relation))\n                    from   t_hierarchy thi,\n                           mif         mfi\n                    where  thi.hier_type = 1 and\n                           thi.ancestor in\n                           (\n                             select pvi.hierarchy_node\n                             from   risk_pci_vendors pvi\n                           ) and\n                           thi.entity_type = 4 and\n                           mfi.association_node = thi.descendent and\n                           mfi.merchant_number =  :1 \n                  ) and\n                  mf.association_node = th.descendent and\n                  mf.merchant_number =  :2 \n          order by pv.display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.reports.PCIDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.reports.PCIDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1153^9*/
        resultSet = it.getResultSet();

        addElement("","select");
        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      catch( Exception e )
      {
        logEntry("PCIVendorTable()",e.toString());
      }
      finally
      {
        try{ it.close(); } catch(Exception e){}
        cleanUp();
      }
    }
  }

}/*@lineinfo:generated-code*/