/*@lineinfo:filename=CSActivityDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CSActivityDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 12/08/03 1:50p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.ComboDateField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class CSActivityDataBean extends ReportSQLJBean
{
  public  static final int    MC_OPEN       = 0;
  public  static final int    MC_CLOSED     = 1;
  public  static final int    MC_CONVERTED  = 2;
  public  static final int    MC_ACTIVATED  = 3;
  public  static final int    MC_TOTAL      = 4;
  public  static final int    MC_COUNT      = 5;
  
  public class RowData
  {
    public long       HierarchyNode           = 0L;
    public int[]      MerchantCounts          = new int[MC_COUNT];
    public int        MerchCount              = 0;
    public int        MerchCountMonthly       = 0;
    public long       OrgId                   = 0L;
    public String     OrgName                 = null;
    public double     SalesAmount             = 0.0;
    public double     SalesAmountActivated    = 0.0;
    public double     SalesAmountClosed       = 0.0;
    public double     SalesAmountMonthly      = 0.0;
    public double     SalesAverageDaily       = 0.0;
    public double     SalesAverageMonthly     = 0.0;
    
    public RowData( long           orgId,
                    long           node,
                    String         orgName )
    {
      HierarchyNode       = node;
      OrgId               = orgId;
      OrgName             = orgName;
      
      for(int i = 0; i < MerchantCounts.length; ++i)
      {
        MerchantCounts[i] = 0;
      }
    }
    
    public void setActivatedMerchSales( double salesAmount )
    {
      SalesAmountActivated  = salesAmount;
    }
    
    public void setClosedMerchSales( double salesAmount )
    {
      SalesAmountClosed = salesAmount;
    }
    
    public void setDailyAvgSales( double salesAmount, int merchCount, double avg )
    {
      SalesAmount       = salesAmount;
      MerchCount        = merchCount;
      SalesAverageDaily = avg;
    }
    
    public void setMerchCount( int index, int count )
    {
      try
      {
        MerchantCounts[index] = count;
      }
      catch( ArrayIndexOutOfBoundsException e )
      {
      }
    }
    
    public void setMonthlyAvgSales( double salesAmount, int merchCount, double avg )
    {
      SalesAmountMonthly    = salesAmount;
      MerchCountMonthly     = merchCount;
      SalesAverageMonthly   = avg;
    }
  }
  
  private Date          ReportDateLastMonth     = null;
  
  public CSActivityDataBean( )
  {
    super(true);    // use FieldBean support
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    line.append("\"$ Sales\",");
    line.append("\"$ Sales Activated\",");
    line.append("\"# Merchants\",");
    line.append("\"$ Avg Daily Sales\",");
    line.append("\"$ Sales " + DateTimeFormatter.getFormattedDate(ReportDateLastMonth,"MMM yyyy")  + "\",");
    line.append("\"# Merchants " + DateTimeFormatter.getFormattedDate(ReportDateLastMonth,"MMM yyyy")  + "\",");
    line.append("\"$ Avg Sales " + DateTimeFormatter.getFormattedDate(ReportDateLastMonth,"MMM yyyy")  + "\",");
    line.append("\"# Open\",");
    line.append("\"# Closed\",");
    line.append("\"# Converted\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData       record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.HierarchyNode );
    line.append( ",\"" );
    line.append( record.OrgName );
    line.append( "\"," );
    line.append( record.SalesAmount );
    line.append( "," );
    line.append( record.SalesAmountActivated );
    line.append( "," );
    line.append( record.MerchCount );
    line.append( "," );
    line.append( record.SalesAverageDaily );
    line.append( "," );
    line.append( record.SalesAmountMonthly );
    line.append( "," );
    line.append( record.MerchCountMonthly );
    line.append( "," );
    line.append( record.SalesAverageMonthly );
    line.append( "," );
    line.append( record.MerchantCounts[MC_OPEN] );
    line.append( "," );
    line.append( record.MerchantCounts[MC_CLOSED] );
    line.append( "," );
    line.append( record.MerchantCounts[MC_CONVERTED] );
  }
  
  protected RowData findRowData( long orgId )
  {
    RowData     retVal    = null;
    
    for( int i = 0; i < ReportRows.size(); ++i )
    {
      if ( ((RowData)ReportRows.elementAt(i)).OrgId == orgId )
      {
        retVal = (RowData)ReportRows.elementAt(i);
        break;
      }
    }
    return(retVal);
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMddyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_cs_summary_");
    
    // build the first date into the filename
    filename.append( rptDate.format( getReportDateBegin() ) );
    if ( getReportDateBegin().equals(getReportDateEnd()) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( getReportDateEnd() ) );
    }      
    return ( filename.toString() );
  }
  
  public Date getReportDateLastMonth( )
  {
    return( ReportDateLastMonth );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( )
  {
    Date    beginDate = ((ComboDateField)getField("beginDate")).getSqlDate();
    Date    endDate   = ((ComboDateField)getField("endDate")).getSqlDate();
    long    orgId     = hierarchyNodeToOrgId( getLong("nodeId") );
    
    loadData( orgId, beginDate, endDate );
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    int                           bankNum           = getOrgBankId(orgId);
    ResultSetIterator             it                = null;
    long                          lastNode          = 0L;
    RowData                       rebateData        = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      // Certegy transaction data is loaded into the 
      // front end file database tables.  Switch
      // all Certegy queries to pull from tran data
      // from the front end.
      if ( bankNum == mesConstants.BANK_ID_CERTEGY )
      {
        /*@lineinfo:generated-code*//*@lineinfo:258^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  po.org_num                  as org_num,
//                    org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,
//                    org.org_name                as org_name,
//                    sum(ds.BANK_SALES_AMOUNT)   as sales_amount,
//                    sum(ds.BANK_SALES_COUNT)    as sales_count,
//                    count(distinct ds.merchant_number)   as merchant_count,
//                    round( (sum( ds.bank_sales_amount)/
//                                 decode(count(gm.merchant_number),
//                                         0,1,count(distinct ds.merchant_number)))/
//                                         ((:endDate-:beginDate)+1),2)   as daily_avg
//            from    parent_org                    po,
//                    organization                  org,
//                    group_merchant                gm,
//                    group_rep_merchant            grm,
//                    daily_detail_file_ext_summary ds        
//            where   po.parent_org_num = :orgId and
//                    org.org_num = po.org_num and
//                    gm.org_num = org.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    ds.merchant_number = gm.merchant_number and
//                    ds.batch_date between :beginDate and :endDate
//            group by po.org_num, org.org_name
//            order by org.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  po.org_num                  as org_num,\n                  org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,\n                  org.org_name                as org_name,\n                  sum(ds.BANK_SALES_AMOUNT)   as sales_amount,\n                  sum(ds.BANK_SALES_COUNT)    as sales_count,\n                  count(distinct ds.merchant_number)   as merchant_count,\n                  round( (sum( ds.bank_sales_amount)/\n                               decode(count(gm.merchant_number),\n                                       0,1,count(distinct ds.merchant_number)))/\n                                       (( :1 - :2 )+1),2)   as daily_avg\n          from    parent_org                    po,\n                  organization                  org,\n                  group_merchant                gm,\n                  group_rep_merchant            grm,\n                  daily_detail_file_ext_summary ds        \n          where   po.parent_org_num =  :3  and\n                  org.org_num = po.org_num and\n                  gm.org_num = org.org_num and\n                  grm.user_id(+) =  :4  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :5  = -1 ) and        \n                  ds.merchant_number = gm.merchant_number and\n                  ds.batch_date between  :6  and  :7 \n          group by po.org_num, org.org_name\n          order by org.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.CSActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,endDate);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setLong(3,orgId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setLong(5,AppFilterUserId);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.CSActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:285^9*/
      }
      else    // use back end files
      {
        /*@lineinfo:generated-code*//*@lineinfo:289^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  po.org_num                  as org_num,
//                    org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,
//                    org.org_name                as org_name,
//                    sum(ds.BANK_SALES_AMOUNT)   as sales_amount,
//                    sum(ds.BANK_SALES_COUNT)    as sales_count,
//                    count(distinct ds.merchant_number)   as merchant_count,
//                    round( (sum( ds.bank_sales_amount)/
//                                 decode(count(gm.merchant_number),
//                                         0,1,count(distinct ds.merchant_number)))/
//                                         ((:endDate-:beginDate)+1),2)   as daily_avg
//            from    parent_org                po,
//                    organization              org,
//                    group_merchant            gm,
//                    group_rep_merchant        grm,
//                    daily_detail_file_summary ds        
//            where   po.parent_org_num = :orgId and
//                    org.org_num = po.org_num and
//                    gm.org_num = org.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    ds.merchant_number = gm.merchant_number and
//                    ds.batch_date between :beginDate and :endDate
//            group by po.org_num, org.org_name
//            order by org.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  po.org_num                  as org_num,\n                  org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,\n                  org.org_name                as org_name,\n                  sum(ds.BANK_SALES_AMOUNT)   as sales_amount,\n                  sum(ds.BANK_SALES_COUNT)    as sales_count,\n                  count(distinct ds.merchant_number)   as merchant_count,\n                  round( (sum( ds.bank_sales_amount)/\n                               decode(count(gm.merchant_number),\n                                       0,1,count(distinct ds.merchant_number)))/\n                                       (( :1 - :2 )+1),2)   as daily_avg\n          from    parent_org                po,\n                  organization              org,\n                  group_merchant            gm,\n                  group_rep_merchant        grm,\n                  daily_detail_file_summary ds        \n          where   po.parent_org_num =  :3  and\n                  org.org_num = po.org_num and\n                  gm.org_num = org.org_num and\n                  grm.user_id(+) =  :4  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :5  = -1 ) and        \n                  ds.merchant_number = gm.merchant_number and\n                  ds.batch_date between  :6  and  :7 \n          group by po.org_num, org.org_name\n          order by org.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.CSActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,endDate);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setLong(3,orgId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setLong(5,AppFilterUserId);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.CSActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:316^9*/
      }        
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        setDailyAvgSalesRowData(  resultSet.getLong("org_num"),
                                  resultSet.getLong("hierarchy_node"),
                                  resultSet.getString("org_name"),
                                  resultSet.getDouble("sales_amount"),
                                  resultSet.getInt("merchant_count"),
                                  resultSet.getDouble("daily_avg") );
      }
      it.close();   // this will also close the resultSet
      
      /*@lineinfo:generated-code*//*@lineinfo:331^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  po.org_num                  as org_num,
//                  org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,
//                  org.org_name                as org_name,
//                  count(mf.merchant_number)   as accounts_total
//          from    parent_org          po,
//                  organization        org,
//                  group_merchant      gm,
//                  group_rep_merchant  grm,
//                  mif                 mf
//          where   po.parent_org_num = :orgId and
//                  org.org_num = po.org_num and
//                  gm.org_num = org.org_num and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                  mf.merchant_number = gm.merchant_number and
//                  mf.date_stat_chgd_to_dcb  is null
//          group by po.org_num, org.org_name
//          order by org.org_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  po.org_num                  as org_num,\n                org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,\n                org.org_name                as org_name,\n                count(mf.merchant_number)   as accounts_total\n        from    parent_org          po,\n                organization        org,\n                group_merchant      gm,\n                group_rep_merchant  grm,\n                mif                 mf\n        where   po.parent_org_num =  :1  and\n                org.org_num = po.org_num and\n                gm.org_num = org.org_num and\n                grm.user_id(+) =  :2  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :3  = -1 ) and        \n                mf.merchant_number = gm.merchant_number and\n                mf.date_stat_chgd_to_dcb  is null\n        group by po.org_num, org.org_name\n        order by org.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.CSActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.CSActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:352^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        setMerchCountRowData(  MC_TOTAL,
                               resultSet.getLong("org_num"),
                               resultSet.getLong("hierarchy_node"),
                               resultSet.getString("org_name"),
                               resultSet.getInt("accounts_total") );
      }
      it.close();   // this will also close the resultSet
      
      
      /*@lineinfo:generated-code*//*@lineinfo:366^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  po.org_num                  as org_num,
//                  org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,
//                  org.org_name                as org_name,
//                  count(mf.merchant_number)   as accounts_opened
//          from    parent_org          po,
//                  organization        org,
//                  group_merchant      gm,
//                  group_rep_merchant  grm,
//                  mif                 mf
//          where   po.parent_org_num = :orgId and
//                  org.org_num = po.org_num and
//                  gm.org_num = org.org_num and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                  mf.merchant_number = gm.merchant_number and
//                  to_date(decode(mf.date_opened,
//                                 '000000','010100',
//                                 mf.date_opened),'mmddrr') between :beginDate and :endDate
//          group by po.org_num, org.org_name
//          order by org.org_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  po.org_num                  as org_num,\n                org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,\n                org.org_name                as org_name,\n                count(mf.merchant_number)   as accounts_opened\n        from    parent_org          po,\n                organization        org,\n                group_merchant      gm,\n                group_rep_merchant  grm,\n                mif                 mf\n        where   po.parent_org_num =  :1  and\n                org.org_num = po.org_num and\n                gm.org_num = org.org_num and\n                grm.user_id(+) =  :2  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :3  = -1 ) and        \n                mf.merchant_number = gm.merchant_number and\n                to_date(decode(mf.date_opened,\n                               '000000','010100',\n                               mf.date_opened),'mmddrr') between  :4  and  :5 \n        group by po.org_num, org.org_name\n        order by org.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.CSActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.CSActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:389^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        setMerchCountRowData(  MC_OPEN,
                               resultSet.getLong("org_num"),
                               resultSet.getLong("hierarchy_node"),
                               resultSet.getString("org_name"),
                               resultSet.getInt("accounts_opened") );
      }
      it.close();   // this will also close the resultSet
      
      if ( bankNum == mesConstants.BANK_ID_CERTEGY )
      {
        // load the activated accounts
        /*@lineinfo:generated-code*//*@lineinfo:405^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  po.org_num                  as org_num,
//                    org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,
//                    org.org_name                as org_name,
//                    count(mf.merchant_number)   as accounts_activated,
//                    sum(nvl(ds.sales_amount,0)) as sales_amount
//            from    parent_org                po,
//                    organization              org,
//                    group_merchant            gm,
//                    group_rep_merchant        grm,
//                    mif                       mf,
//                    ( select  sm.merchant_number          as merchant_number,
//                              sum(sm.BANK_SALES_AMOUNT)   as sales_amount,
//                              sum(sm.BANK_SALES_COUNT)    as sales_count  
//                      from    group_merchant                gm,
//                              mif                           mf,
//                              daily_detail_file_ext_summary sm
//                      where   gm.org_num = :orgId and
//                              mf.merchant_number = gm.merchant_number and
//                              mf.activation_date between :beginDate and :endDate and
//                              sm.merchant_number = mf.merchant_number and
//                              sm.batch_date between :beginDate and :endDate
//                      group by sm.merchant_number
//                    )                         ds                                               
//            where   po.parent_org_num = :orgId and
//                    org.org_num = po.org_num and
//                    gm.org_num = org.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    mf.merchant_number = gm.merchant_number and
//                    mf.activation_date between :beginDate and :endDate and
//                    ds.merchant_number(+) = mf.merchant_number
//            group by po.org_num, org.org_name
//            order by org.org_name      
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  po.org_num                  as org_num,\n                  org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,\n                  org.org_name                as org_name,\n                  count(mf.merchant_number)   as accounts_activated,\n                  sum(nvl(ds.sales_amount,0)) as sales_amount\n          from    parent_org                po,\n                  organization              org,\n                  group_merchant            gm,\n                  group_rep_merchant        grm,\n                  mif                       mf,\n                  ( select  sm.merchant_number          as merchant_number,\n                            sum(sm.BANK_SALES_AMOUNT)   as sales_amount,\n                            sum(sm.BANK_SALES_COUNT)    as sales_count  \n                    from    group_merchant                gm,\n                            mif                           mf,\n                            daily_detail_file_ext_summary sm\n                    where   gm.org_num =  :1  and\n                            mf.merchant_number = gm.merchant_number and\n                            mf.activation_date between  :2  and  :3  and\n                            sm.merchant_number = mf.merchant_number and\n                            sm.batch_date between  :4  and  :5 \n                    group by sm.merchant_number\n                  )                         ds                                               \n          where   po.parent_org_num =  :6  and\n                  org.org_num = po.org_num and\n                  gm.org_num = org.org_num and\n                  grm.user_id(+) =  :7  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :8  = -1 ) and        \n                  mf.merchant_number = gm.merchant_number and\n                  mf.activation_date between  :9  and  :10  and\n                  ds.merchant_number(+) = mf.merchant_number\n          group by po.org_num, org.org_name\n          order by org.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.CSActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setLong(6,orgId);
   __sJT_st.setLong(7,AppFilterUserId);
   __sJT_st.setLong(8,AppFilterUserId);
   __sJT_st.setDate(9,beginDate);
   __sJT_st.setDate(10,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.CSActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:441^9*/
      }
      else    // use the back end files
      {
        // load the activated accounts
        /*@lineinfo:generated-code*//*@lineinfo:446^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  po.org_num                  as org_num,
//                    org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,
//                    org.org_name                as org_name,
//                    count(mf.merchant_number)   as accounts_activated,
//                    sum(nvl(ds.sales_amount,0)) as sales_amount
//            from    parent_org                po,
//                    organization              org,
//                    group_merchant            gm,
//                    group_rep_merchant        grm,
//                    mif                       mf,
//                    ( select  sm.merchant_number          as merchant_number,
//                              sum(sm.BANK_SALES_AMOUNT)   as sales_amount,
//                              sum(sm.BANK_SALES_COUNT)    as sales_count  
//                      from    group_merchant            gm,
//                              mif                       mf,
//                              daily_detail_file_summary sm
//                      where   gm.org_num = :orgId and
//                              mf.merchant_number = gm.merchant_number and
//                              mf.activation_date between :beginDate and :endDate and
//                              sm.merchant_number = mf.merchant_number and
//                              sm.batch_date between :beginDate and :endDate
//                      group by sm.merchant_number
//                    )                         ds                                               
//            where   po.parent_org_num = :orgId and
//                    org.org_num = po.org_num and
//                    gm.org_num = org.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    mf.merchant_number = gm.merchant_number and
//                    mf.activation_date between :beginDate and :endDate and
//                    ds.merchant_number(+) = mf.merchant_number
//            group by po.org_num, org.org_name
//            order by org.org_name      
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  po.org_num                  as org_num,\n                  org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,\n                  org.org_name                as org_name,\n                  count(mf.merchant_number)   as accounts_activated,\n                  sum(nvl(ds.sales_amount,0)) as sales_amount\n          from    parent_org                po,\n                  organization              org,\n                  group_merchant            gm,\n                  group_rep_merchant        grm,\n                  mif                       mf,\n                  ( select  sm.merchant_number          as merchant_number,\n                            sum(sm.BANK_SALES_AMOUNT)   as sales_amount,\n                            sum(sm.BANK_SALES_COUNT)    as sales_count  \n                    from    group_merchant            gm,\n                            mif                       mf,\n                            daily_detail_file_summary sm\n                    where   gm.org_num =  :1  and\n                            mf.merchant_number = gm.merchant_number and\n                            mf.activation_date between  :2  and  :3  and\n                            sm.merchant_number = mf.merchant_number and\n                            sm.batch_date between  :4  and  :5 \n                    group by sm.merchant_number\n                  )                         ds                                               \n          where   po.parent_org_num =  :6  and\n                  org.org_num = po.org_num and\n                  gm.org_num = org.org_num and\n                  grm.user_id(+) =  :7  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :8  = -1 ) and        \n                  mf.merchant_number = gm.merchant_number and\n                  mf.activation_date between  :9  and  :10  and\n                  ds.merchant_number(+) = mf.merchant_number\n          group by po.org_num, org.org_name\n          order by org.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.CSActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setLong(6,orgId);
   __sJT_st.setLong(7,AppFilterUserId);
   __sJT_st.setLong(8,AppFilterUserId);
   __sJT_st.setDate(9,beginDate);
   __sJT_st.setDate(10,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.CSActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:482^9*/
      }
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        setMerchCountRowData(  MC_ACTIVATED,
                               resultSet.getLong("org_num"),
                               resultSet.getLong("hierarchy_node"),
                               resultSet.getString("org_name"),
                               resultSet.getInt("accounts_activated") );
        setActivatedMerchSales( resultSet.getLong("org_num"), resultSet.getDouble("sales_amount") );                               
      }
      it.close();   // this will also close the resultSet
      
      /*@lineinfo:generated-code*//*@lineinfo:497^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  po.org_num                    as org_num,
//                  org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,
//                  org.org_name                  as org_name,
//                  count(mf.merchant_number)     as accounts_closed,
//                  sum(nvl(sm.monthly_sales,0)*
//                      (:endDate-:beginDate)/30) as proj_lost_sales
//          from    parent_org              po,
//                  organization            org,
//                  group_merchant          gm,
//                  group_rep_merchant      grm,
//                  mif                     mf,
//                  ( select  sm.merchant_number          as merchant_number,
//                            round( decode( sum(nvl(sm.vmc_sales_amount,0)),
//                                           0, 0,
//                                           sum(nvl(sm.vmc_sales_amount,0))/
//                                           sum( decode(nvl(sm.vmc_sales_amount,0),0,0,1) ) )
//                                           , 2 )
//                                                        as monthly_sales
//                    from    group_merchant            gm,
//                            mif                       mf,
//                            monthly_extract_summary   sm
//                    where   gm.org_num = :orgId and
//                            mf.merchant_number = gm.merchant_number and
//                            not mf.date_stat_chgd_to_dcb is null and
//                            sm.merchant_number = mf.merchant_number and
//                            sm.active_date between (mf.date_stat_chgd_to_dcb-365) and mf.date_stat_chgd_to_dcb
//                    group by sm.merchant_number
//                  )                       sm
//          where   po.parent_org_num = :orgId and
//                  org.org_num = po.org_num and
//                  gm.org_num = org.org_num and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                  mf.merchant_number = gm.merchant_number and
//                  mf.date_stat_chgd_to_dcb between :beginDate and :endDate and
//                  sm.merchant_number = mf.merchant_number
//          group by po.org_num, org.org_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  po.org_num                    as org_num,\n                org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,\n                org.org_name                  as org_name,\n                count(mf.merchant_number)     as accounts_closed,\n                sum(nvl(sm.monthly_sales,0)*\n                    ( :1 - :2 )/30) as proj_lost_sales\n        from    parent_org              po,\n                organization            org,\n                group_merchant          gm,\n                group_rep_merchant      grm,\n                mif                     mf,\n                ( select  sm.merchant_number          as merchant_number,\n                          round( decode( sum(nvl(sm.vmc_sales_amount,0)),\n                                         0, 0,\n                                         sum(nvl(sm.vmc_sales_amount,0))/\n                                         sum( decode(nvl(sm.vmc_sales_amount,0),0,0,1) ) )\n                                         , 2 )\n                                                      as monthly_sales\n                  from    group_merchant            gm,\n                          mif                       mf,\n                          monthly_extract_summary   sm\n                  where   gm.org_num =  :3  and\n                          mf.merchant_number = gm.merchant_number and\n                          not mf.date_stat_chgd_to_dcb is null and\n                          sm.merchant_number = mf.merchant_number and\n                          sm.active_date between (mf.date_stat_chgd_to_dcb-365) and mf.date_stat_chgd_to_dcb\n                  group by sm.merchant_number\n                )                       sm\n        where   po.parent_org_num =  :4  and\n                org.org_num = po.org_num and\n                gm.org_num = org.org_num and\n                grm.user_id(+) =  :5  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :6  = -1 ) and        \n                mf.merchant_number = gm.merchant_number and\n                mf.date_stat_chgd_to_dcb between  :7  and  :8  and\n                sm.merchant_number = mf.merchant_number\n        group by po.org_num, org.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.CSActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,endDate);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setLong(3,orgId);
   __sJT_st.setLong(4,orgId);
   __sJT_st.setLong(5,AppFilterUserId);
   __sJT_st.setLong(6,AppFilterUserId);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.CSActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:537^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        setMerchCountRowData(  MC_CLOSED,
                               resultSet.getLong("org_num"),
                               resultSet.getLong("hierarchy_node"),
                               resultSet.getString("org_name"),
                               resultSet.getInt("accounts_closed") );
        setClosedMerchSales( resultSet.getLong("org_num"), resultSet.getDouble("proj_lost_sales") );
      }
      it.close();   // this will also close the resultSet
      
      /*@lineinfo:generated-code*//*@lineinfo:551^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  po.org_num                  as org_num,
//                  org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,
//                  org.org_name                as org_name,
//                  count(mf.merchant_number)   as accounts_converted
//          from    parent_org          po,
//                  organization        org,
//                  group_merchant      gm,
//                  group_rep_merchant  grm,
//                  mif                 mf
//          where   po.parent_org_num = :orgId and
//                  org.org_num = po.org_num and
//                  gm.org_num = org.org_num and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                  mf.merchant_number = gm.merchant_number and
//                  mf.ACTIVATION_DATE between :beginDate and :endDate and
//                  substr(mf.user_data_4,16,1) = 'C'
//          group by po.org_num, org.org_name                                                               
//          order by org.org_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  po.org_num                  as org_num,\n                org_num_to_hierarchy_node(po.org_num)          as hierarchy_node,\n                org.org_name                as org_name,\n                count(mf.merchant_number)   as accounts_converted\n        from    parent_org          po,\n                organization        org,\n                group_merchant      gm,\n                group_rep_merchant  grm,\n                mif                 mf\n        where   po.parent_org_num =  :1  and\n                org.org_num = po.org_num and\n                gm.org_num = org.org_num and\n                grm.user_id(+) =  :2  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :3  = -1 ) and        \n                mf.merchant_number = gm.merchant_number and\n                mf.ACTIVATION_DATE between  :4  and  :5  and\n                substr(mf.user_data_4,16,1) = 'C'\n        group by po.org_num, org.org_name                                                               \n        order by org.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.CSActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.CSActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:573^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        setMerchCountRowData(  MC_CONVERTED,
                               resultSet.getLong("org_num"),
                               resultSet.getLong("hierarchy_node"),
                               resultSet.getString("org_name"),
                               resultSet.getInt("accounts_converted") );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    super.postHandleRequest( request );
    
    if ( HttpHelper.getString(request,"beginDate.month",null) == null )
    {
      Calendar cal = Calendar.getInstance();
      cal.add( Calendar.DAY_OF_MONTH, -7 );
      ((ComboDateField)getField("beginDate")).setUtilDate( cal.getTime() );
    }      
    
    // set the default portfolio id
    if ( getLong("nodeId") == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setData("nodeId","394100000" );
    }
  }
  
  protected void setActivatedMerchSales( long orgId, double salesAmount )
  {
    RowData       row    = findRowData( orgId );
    
    row.setActivatedMerchSales(salesAmount);
  }
  
  protected void setClosedMerchSales( long orgId, double salesAmount )
  {
    RowData       row    = findRowData( orgId );
    
    row.setClosedMerchSales(salesAmount);
  }
  
  protected void setDailyAvgSalesRowData( long orgId, long nodeId, String name,
                                            double salesAmount, int merchCount, double average )
  {
    RowData       row    = findRowData( orgId );
    
    if ( row == null )
    {
      row = new RowData(orgId,nodeId,name);
      ReportRows.addElement(row);
    }
    row.setDailyAvgSales(salesAmount,merchCount,average);
  }                            

  protected void setMonthlyAvgSalesRowData( long orgId, long nodeId, String name,
                                            double salesAmount, int merchCount, double average )
  {
    RowData       row    = findRowData( orgId );
    
    if ( row == null )
    {
      row = new RowData(orgId,nodeId,name);
      ReportRows.addElement(row);
    }
    row.setMonthlyAvgSales(salesAmount,merchCount,average);
  }                              

  protected void setMerchCountRowData(  int index, long orgId, 
                                        long nodeId, String name, int count )
  {
    RowData       row    = findRowData( orgId );
    
    if ( row == null )
    {
      row = new RowData(orgId,nodeId,name);
      ReportRows.addElement(row);
    }
    row.setMerchCount(index,count);
  }                         
  
  public void setProperties(HttpServletRequest request)
  {
    autoSetFields(request);   // stubbed so download servlet works
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/