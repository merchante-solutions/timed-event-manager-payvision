/*@lineinfo:filename=SabreReconcileDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/SabreReconcileDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2000-2005,2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ComboDateField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class SabreReconcileDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public String     CardNumber       = null;  
    public String     DocLocator       = null;  
    public long       MerchantId       = 0L;    
    public String     PassengerName    = null;  
    public String     Pcc              = null;  
    public String     PurchaseId       = null;  
    public String     ReferenceNumber  = null;  
    public String     TicketNumber     = null;  
    public double     TranAmount       = 0.0;   
    public String     TranCode         = null;  
    public Date       TranDate         = null;  
  
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      Pcc              = processString(resultSet.getString("pcc"));
      MerchantId       = resultSet.getLong("merchant_number");
      CardNumber       = processString(resultSet.getString("card_number"));
      TranCode         = processString(resultSet.getString("tran_code"));
      TranDate         = resultSet.getDate("tran_date");
      TranAmount       = resultSet.getDouble("tran_amount");
      ReferenceNumber  = processString(resultSet.getString("ref_num"));
      TicketNumber     = processString(resultSet.getString("ticket_number"));
      PurchaseId       = processString(resultSet.getString("purchase_id"));
      DocLocator       = processString(resultSet.getString("doc_locator"));
      PassengerName    = processString(resultSet.getString("passenger_name"));
    }
  }
  
  public SabreReconcileDataBean( )
  {
    super(true);    // enable field bean support
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup        fgroup;
    Field             field;
    
    super.createFields(request);
    
    fgroup = (FieldGroup)getField("searchFields");
    
    fgroup.deleteField("portfolioId");
    fgroup.add( new Field( "pcc", "PCC (leave blank for all)", 4, 4, true ) );
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    encodeHeader(line, ",");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    encodeLine(obj,line,",");
  }
  
  protected void encodeHeaderFixed( StringBuffer line )
  {
    encodeHeader(line, "\t");
  }
  
  protected void encodeLineFixed( Object obj, StringBuffer line )
  {
    encodeLine(obj,line,"\t");
  }
  
  protected void encodeHeader( StringBuffer line, String delim )
  {
    line.setLength(0);
    line.append("\"PCC\"");
    line.append(delim);
    line.append("\"Merchant Number\"");
    line.append(delim);
    line.append("\"Card Number\"");
    line.append(delim);
    line.append("\"Tran Code\"");
    line.append(delim);
    line.append("\"Tran Date\"");
    line.append(delim);
    line.append("\"Tran Amount\"");
    line.append(delim);
    line.append("\"Reference Number\"");
    line.append(delim);
    line.append("\"Ticket Number\"");
    line.append(delim);
    line.append("\"Purchase ID\"");
    line.append(delim);
    line.append("\"Doc Locator\"");
    line.append(delim);
    line.append("\"Passenger Name\"");
  }
  
  protected void encodeLine( Object obj, StringBuffer line, String delim )
  {
    line.setLength(0);
    
    RowData   record = (RowData)obj;
      
    line.append("\"");
    line.append(record.Pcc);
    line.append("\"");
    line.append(delim);
    line.append(record.MerchantId);
    line.append(delim);
    line.append("\"");
    line.append(record.CardNumber);
    line.append("\"");
    line.append(delim);
    line.append("\"");
    line.append(record.TranCode);
    line.append("\"");
    line.append(delim);
    line.append(DateTimeFormatter.getFormattedDate(record.TranDate,"MM/dd/yyyy"));
    line.append(delim);
    line.append(record.TranAmount);
    line.append(delim);
    line.append("\"");
    line.append(record.ReferenceNumber);
    line.append("\"");
    line.append(delim);
    line.append("\"");
    line.append(record.TicketNumber);
    line.append("\"");
    line.append(delim);
    line.append("\"");
    line.append(record.PurchaseId);
    line.append("\"");
    line.append(delim);
    line.append("\"");
    line.append(record.DocLocator);
    line.append("\"");
    line.append(delim);
    line.append("\"");
    line.append(record.PassengerName);
    line.append("\"");
  }
  
  public String getDownloadFilenameBase()
  {
    Date              beginDate   = getReportDateBegin();
    Date              endDate     = getReportDateEnd();
    StringBuffer      filename    = new StringBuffer("sabre_reconcile_detail_");
    

    filename.append( DateTimeFormatter.getFormattedDate(beginDate,"MMddyy") );
    if ( beginDate.equals(endDate) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(endDate,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_FIXED:
      case FF_CSV:
        retVal = true;
        break;
        
      default:
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it          = null;
    long                nodeId      = orgIdToHierarchyNode(orgId);
    String              pcc         = getData("pcc");
    ResultSet           resultSet   = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( pcc.equals("") )
      {
        pcc = "*";
      }

      for( int i = 0; i < 3; ++i )
      {      
        switch(i)
        {
          case 0:
            /*@lineinfo:generated-code*//*@lineinfo:247^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                            index(dt idx_ddf_ext_dt_merch_numer)
//                         */
//                        mrs.pcc                                   as pcc,
//                        dt.merchant_number                        as merchant_number,
//                        dt.card_number                            as card_number,
//                        dt.tran_code                              as tran_code,                  
//                        dt.transaction_date                       as tran_date,
//                        ( dt.transaction_amount *
//                          decode(dt.debit_credit_indicator,'C',-1,1) )
//                                                                  as tran_amount,
//                        dt.reference_number                       as ref_num,
//                        decode(length(nvl(dt.purchase_id,'bad')) +
//                               is_number(nvl(dt.purchase_id,'bad')),
//                               14, dt.purchase_id,
//                               null)                              as ticket_number,
//                        dt.purchase_id                            as purchase_id,
//                        dt.acq_invoice_number                     as doc_locator,
//                        dt.passenger_name                         as passenger_name
//                from    organization                  o,
//                        group_merchant                gm,
//                        merchant                      mr,
//                        merchant_sabre                mrs,
//                        daily_detail_file_ext_dt      dt
//                where   o.org_group = :nodeId and
//                        gm.org_num = o.org_num and
//                        mr.merch_number = gm.merchant_number and
//                        mrs.app_seq_num = mr.app_seq_num and
//                        ( :pcc = '*' or upper(mrs.pcc) = upper(:pcc) ) and
//                        dt.merchant_number = mr.merch_number and        
//                        dt.batch_date between :beginDate and :endDate and
//                        not dt.card_type in ( 'DS' )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                          index(dt idx_ddf_ext_dt_merch_numer)\n                       */\n                      mrs.pcc                                   as pcc,\n                      dt.merchant_number                        as merchant_number,\n                      dt.card_number                            as card_number,\n                      dt.tran_code                              as tran_code,                  \n                      dt.transaction_date                       as tran_date,\n                      ( dt.transaction_amount *\n                        decode(dt.debit_credit_indicator,'C',-1,1) )\n                                                                as tran_amount,\n                      dt.reference_number                       as ref_num,\n                      decode(length(nvl(dt.purchase_id,'bad')) +\n                             is_number(nvl(dt.purchase_id,'bad')),\n                             14, dt.purchase_id,\n                             null)                              as ticket_number,\n                      dt.purchase_id                            as purchase_id,\n                      dt.acq_invoice_number                     as doc_locator,\n                      dt.passenger_name                         as passenger_name\n              from    organization                  o,\n                      group_merchant                gm,\n                      merchant                      mr,\n                      merchant_sabre                mrs,\n                      daily_detail_file_ext_dt      dt\n              where   o.org_group =  :1  and\n                      gm.org_num = o.org_num and\n                      mr.merch_number = gm.merchant_number and\n                      mrs.app_seq_num = mr.app_seq_num and\n                      (  :2  = '*' or upper(mrs.pcc) = upper( :3 ) ) and\n                      dt.merchant_number = mr.merch_number and        \n                      dt.batch_date between  :4  and  :5  and\n                      not dt.card_type in ( 'DS' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.SabreReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setString(2,pcc);
   __sJT_st.setString(3,pcc);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.SabreReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:281^13*/
            break;
            
          case 1:
            /*@lineinfo:generated-code*//*@lineinfo:285^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  mrs.pcc                                   as pcc,
//                        adj.merchant_account_number               as merchant_number,
//                        nvl(td.ach_tran_desc,
//                            'Adjustment Code ' || 
//                             adj.transacction_code)               as card_number,
//                        'ADJ'                                     as tran_code,                  
//                        adj.transaction_date_mmddccyy             as tran_date,
//                        ( adj.adjustment_amount *
//                          decode(adj.debit_credit_ind,'D',-1,1) ) as tran_amount,
//                        adj.reference_number                      as ref_num,
//                        null                                      as ticket_number,
//                        null                                      as purchase_id,
//                        null                                      as doc_locator,
//                        null                                      as passenger_name
//                from    organization                  o,
//                        group_merchant                gm,
//                        merchant                      mr,
//                        merchant_sabre                mrs,
//                        daily_detail_file_adjustment  adj,
//                        vital_ach_tran_desc           td
//                where   o.org_group = :nodeId and
//                        gm.org_num = o.org_num and
//                        mr.merch_number = gm.merchant_number and
//                        mrs.app_seq_num = mr.app_seq_num and
//                        ( :pcc = '*' or upper(mrs.pcc) = upper(:pcc) ) and
//                        adj.merchant_account_number = mr.merch_number and
//                        adj.batch_date between (:beginDate+1) and (:endDate+1) and
//                        td.ach_tran_code(+) = adj.transacction_code
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mrs.pcc                                   as pcc,\n                      adj.merchant_account_number               as merchant_number,\n                      nvl(td.ach_tran_desc,\n                          'Adjustment Code ' || \n                           adj.transacction_code)               as card_number,\n                      'ADJ'                                     as tran_code,                  \n                      adj.transaction_date_mmddccyy             as tran_date,\n                      ( adj.adjustment_amount *\n                        decode(adj.debit_credit_ind,'D',-1,1) ) as tran_amount,\n                      adj.reference_number                      as ref_num,\n                      null                                      as ticket_number,\n                      null                                      as purchase_id,\n                      null                                      as doc_locator,\n                      null                                      as passenger_name\n              from    organization                  o,\n                      group_merchant                gm,\n                      merchant                      mr,\n                      merchant_sabre                mrs,\n                      daily_detail_file_adjustment  adj,\n                      vital_ach_tran_desc           td\n              where   o.org_group =  :1  and\n                      gm.org_num = o.org_num and\n                      mr.merch_number = gm.merchant_number and\n                      mrs.app_seq_num = mr.app_seq_num and\n                      (  :2  = '*' or upper(mrs.pcc) = upper( :3 ) ) and\n                      adj.merchant_account_number = mr.merch_number and\n                      adj.batch_date between ( :4 +1) and ( :5 +1) and\n                      td.ach_tran_code(+) = adj.transacction_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.SabreReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setString(2,pcc);
   __sJT_st.setString(3,pcc);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.SabreReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:315^13*/
            break;
            
          case 2:
            /*@lineinfo:generated-code*//*@lineinfo:319^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  mrs.pcc                                   as pcc,
//                        ach.merchant_number                       as merchant_number,
//                        'Statement Fees'                          as card_number,
//                        'STMT FEES'                               as tran_code,                  
//                        ach.post_date_option                      as tran_date,
//                        ( ach.amount_of_transaction *
//                          decode(tc.debit_credit_indicator,
//                                 'D',-1,
//                                 1) )                             as tran_amount,
//                        ach.trace_sequence_number                 as ref_num,
//                        null                                      as ticket_number,
//                        null                                      as purchase_id,
//                        null                                      as doc_locator,
//                        null                                      as passenger_name
//                from    organization                  o,
//                        group_merchant                gm,
//                        merchant                      mr,
//                        merchant_sabre                mrs,
//                        ach_detail                    ach,
//                        ach_detail_tran_code          tc
//                where   o.org_group = :nodeId and
//                        gm.org_num = o.org_num and
//                        mr.merch_number = gm.merchant_number and
//                        mrs.app_seq_num = mr.app_seq_num and
//                        ( :pcc = '*' or upper(mrs.pcc) = upper(:pcc) ) and
//                        ach.merchant_number = mr.merch_number and
//                        ach.post_date_option between :beginDate and :endDate and
//                        ach.load_filename like 'fee%' and                      
//                        tc.ach_detail_trans_code(+) = ach.transaction_code 
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mrs.pcc                                   as pcc,\n                      ach.merchant_number                       as merchant_number,\n                      'Statement Fees'                          as card_number,\n                      'STMT FEES'                               as tran_code,                  \n                      ach.post_date_option                      as tran_date,\n                      ( ach.amount_of_transaction *\n                        decode(tc.debit_credit_indicator,\n                               'D',-1,\n                               1) )                             as tran_amount,\n                      ach.trace_sequence_number                 as ref_num,\n                      null                                      as ticket_number,\n                      null                                      as purchase_id,\n                      null                                      as doc_locator,\n                      null                                      as passenger_name\n              from    organization                  o,\n                      group_merchant                gm,\n                      merchant                      mr,\n                      merchant_sabre                mrs,\n                      ach_detail                    ach,\n                      ach_detail_tran_code          tc\n              where   o.org_group =  :1  and\n                      gm.org_num = o.org_num and\n                      mr.merch_number = gm.merchant_number and\n                      mrs.app_seq_num = mr.app_seq_num and\n                      (  :2  = '*' or upper(mrs.pcc) = upper( :3 ) ) and\n                      ach.merchant_number = mr.merch_number and\n                      ach.post_date_option between  :4  and  :5  and\n                      ach.load_filename like 'fee%' and                      \n                      tc.ach_detail_trans_code(+) = ach.transaction_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.SabreReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setString(2,pcc);
   __sJT_st.setString(3,pcc);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.SabreReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:350^13*/
            break;
            
          default:
            it = null;
            break;
        }
        
        if ( it != null )
        {
          resultSet = it.getResultSet();
          while( resultSet.next() )
          {
            ReportRows.addElement( new RowData( resultSet ) );
          }
          resultSet.close();
          it.close();
        }          
      }        
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    super.postHandleRequest( request );
    
    if ( HttpHelper.getString(request,"beginDate.month",null) == null )
    {
      Calendar cal = Calendar.getInstance();
    
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      ((ComboDateField)getField("beginDate")).setUtilDate( cal.getTime() );
      ((ComboDateField)getField("endDate")).setUtilDate( cal.getTime() );
    }      
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/