package com.mes.reports.quiz;

import java.io.Serializable;
import java.util.ArrayList;
import com.mes.forms.Field;
import com.mes.forms.RadioButtonField;

public class QuizQuestion extends QuizBase implements Serializable
{

  private Field field           = null;
  private String label          = "";
  private String hiddenLabel    = "";
  private String text           = "";
  private String answer         = "";
  private int type              = -1;
  private ArrayList answers     = new ArrayList();
  private long aId              = 0; //for the selected answer

  public static final int NONE      = 0;
  public static final int RADIO     = 1;
  public static final int RADIO2    = 2;
  public static final int TEXT      = 3;
  public static final int CHECKBOX  = 4;
  public static final int TEXTAREA  = 5;
  public static final int RADIO3    = 6;


  public QuizQuestion(long id)
  {
    super(id);
  }

  public QuizQuestion(long id, String text)
  {
    super(id);
    this.text = text;
  }

  public String getText()
  {
    return text;
  }

  public void setText(String text)
  {
    this.text = text;
  }

  public String getLabel()
  {
    if (label==null)
    {
      return "";
    }
    return label;
  }

  public boolean isSubLabel()
  {
    if(label != null && (label.indexOf(".") != label.lastIndexOf(".")))
    {
      return true;
    }
    return false;
  }

  public void setHiddenLabel(String hiddenLabel)
  {
    this.hiddenLabel = hiddenLabel;
  }

  public String getHiddenLabel()
  {
    return hiddenLabel;
  }

  public void setLabel(String label)
  {
    this.label = label;
  }

  public void setType(int type)
  {
    this.type = type;
  }

  public int getType()
  {
    return type;
  }


  public String getFieldName()
  {
    if(field !=null)
    {
      return field.getName();
    }
    else
    {
      return "";
    }
  }

  public Field getField()
  {
    return field;
  }

  public void buildField(Object answerFormat)
    throws ClassCastException
  {

    Field f = null;


    switch(getType())
    {
      case RADIO:
      case RADIO2:
      case RADIO3:
        //doesn't matter right now
        String[][] arr = (String[][])answerFormat;
        String fName = "q_"+getId();

        f = new RadioButtonField(fName, arr, false, "Please provide an answer.");
        ((RadioButtonField)f).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);

        break;

      case NONE:
      case TEXT:
      case CHECKBOX:
      case TEXTAREA:
      default:
        break;

    }

    this.field = f;

  }

  public String renderHtml()
  {
    if (field != null)
    {
      return field.renderHtml();
    }
    return "";
  }


/*
  public void addAnswer(QuizAnswer a)
  {
    answers.add(a);
  }

  public void removeAnswer(long id)
  {
    QuizAnswer a;
    for(int i=0;i<answers.size();i++)
    {
      a = (QuizAnswer)answers.get(i);
      if(a.getId()==id)
      {
        //remove this one
        answers.remove(i);

        //rebuild the field
        buildField();

        break;
      }
    }
  }

  //this is set when checking answers to the question
  public long getAnswerId()
  {
    return aId;
  }

  public String getSelectedAnswer()
  {
    String answerText = "";

    QuizAnswer a;
    for(int i=0;i<answers.size();i++)
    {
      a = (QuizAnswer)answers.get(i);
      if(aId == a.getId())
      {
        answerText = a.getText();
        break;
      }
    }
    return answerText;
  }
*/
  //this all happens post validation, so we don't need
  //to worry about anything except how the answer is
  //classifed compared to the users response
/*
  public boolean isCorrect()
  {
    boolean result = false;

    answer = field.getData();

    QuizAnswer a;
    for(int i=0;i<answers.size();i++)
    {
      a = (QuizAnswer)answers.get(i);
      String idStr = ""+a.getId();
      //if this is the selected answer in the field
      if(idStr.equals(answer))
      {
        //check the answer to see if it's correct
        result = a.isCorrect();

        //set the answer Id for the save process
        aId = a.getId();
        break;
      }
    }

    return result;
  }
*/


}