package com.mes.reports.quiz;

import java.io.Serializable;

public class QuizAnswer extends QuizBase implements Serializable
{

  private String text               = "";
  private String support            = "";
  private boolean isCorrect         = false;
  private boolean requiresComment   = false;

  public QuizAnswer(long id)
  {
    super(id);
  }

  public QuizAnswer(long id, String text)
  {
    super(id);
    this.text = text;
  }

  public String getText()
  {
    return text;
  }

  public void setText(String text)
  {
    this.text = text;
  }

  public String getSupport()
  {
    return support;
  }

  public void setSupport(String support)
  {
    this.support = support;
  }

  public void setCorrect(boolean isCorrect)
  {
    this.isCorrect = isCorrect;
  }

  public boolean isCorrect()
  {
    return isCorrect;
  }

  public boolean requiresComment()
  {
    return requiresComment;
  }

  public void setRequiresComment(boolean requiresComment)
  {
    this.requiresComment = requiresComment;
  }

}