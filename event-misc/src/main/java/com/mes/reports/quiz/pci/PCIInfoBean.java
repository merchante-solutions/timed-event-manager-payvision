/*@lineinfo:filename=PCIInfoBean*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.reports.quiz.pci;

import java.net.URLDecoder;
import java.sql.ResultSet;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
// log4j
import org.apache.log4j.Logger;
import com.mes.forms.CheckField;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.Validation;
import com.mes.forms.WebAddressField;
import com.mes.forms.ZipField;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class PCIInfoBean extends FieldBean
{

  // create class log category
  static Logger log = Logger.getLogger(PCIInfoBean.class);

  private PCIQuestionnaire.TrackingNumber trackingNum = null;

  //for validation, 3 different FieldGroups
  private FieldGroup part1                            = new FieldGroup("part1");
  private FieldGroup part2                            = new FieldGroup("part2");
  private FieldGroup part3                            = new FieldGroup("part3");
  private boolean part1Complete                       = false;
  private boolean part2Complete                       = false;
  private boolean part3Complete                       = false;

  //PART 1
  //SAME FOR A,B,C,D
  //Qualified Security Assessor Information 'qsa'
  private FieldGroup qsaGroup            = new FieldGroup("qsa");
  /*
  protected String qsaCompanyName        = "";
  protected String qsaContactName        = "";
  protected String qsaTitle              = "";
  protected String qsaTelephone          = "";
  protected String qsaEmail              = "";
  protected String qsaBizAddress         = "";
  protected String qsaState              = "";
  protected String qsaCountry            = "";
  protected String qsaZip                = "";
  protected String qsaURL                = "";
  */

  //PART 2
  //SAME FOR A,B,C,D
  //Merchant Information 'm'
  private FieldGroup mGroup            = new FieldGroup("m");
  /*
  protected String mCompanyName        = "";
  protected String mDBANames           = "";
  protected String mContactName        = "";
  protected String mTitle              = "";
  protected String mTelephone          = "";
  protected String mEmail              = "";
  protected String mBizAddress         = "";
  protected String mCity               = "";
  protected String mState              = "";
  protected String mCountry            = "";
  protected String mZip                = "";
  protected String mURL                = "";
  */

  //Merchant Business Types 'b'
  //SAME FOR A,B,C,D
  //formatted in the DB as int:String, to be
  //broken out when checkbox fields are created
  private FieldGroup bGroup             = new FieldGroup("b");
  /*
  protected String bRetailer            = "";
  protected String bTeleComm            = "";
  protected String bGrocery             = "";
  protected String bPetroleum           = "";
  protected String bECommerce           = "";
  protected String bMailOrder           = "";
  protected String bOther               = "";
  protected String bFacilities          = "";
  */

  //Relationships 'r'
  //SAME FOR A,B,C,D
  private FieldGroup rGroup             = new FieldGroup("r");
  /*
  protected String rThirdParty          = "";
  protected String rMultipleAcquirers   = "";
  */

  //Transaction Processing 't'
  //SAME FOR B,C,D - NO A
  private FieldGroup tGroup             = new FieldGroup("t");
  /*
  protected String tPaymentApp          = "";
  protected String tPaymentAppVer       = "";
  */

  //Eligibility to Complete 'e'
  private FieldGroup eGroup             = new FieldGroup("e");
  /*
  //D - n/a
  //A, B, C base
  protected String eNotStored           = "";
  protected String eStoredNotElec       = "";
  //A only (in addition to base)
  protected String eThirdParty          = "";
  protected String eThirdPartyCompliant = "";
  //B only (in addition to base)
  protected String eImprint             = "";
  protected String eIsolated            = "";
  //C only (in addition to base)
  protected String eSameDevice          = "";
  protected String eNotShared           = "";
  protected String eSecure              = "";
  */


  //PART 3
  //SAME FOR A,B,C,D
  //PCI DSS Validation 'v'
  private FieldGroup vGroup             = new FieldGroup("v");
  /*
  protected String vCompliant           = "";
  protected String vComplianceDate      = "";
  */

  //Confirmation of Complaint Status 'c'
  //name of each var is completely subjective
  //SAME FOR A,B,C,D
  private FieldGroup cGroup             = new FieldGroup("c");
  /*
  protected String cComplete            = "";
  protected String cFair                = "";
  protected String cMaintain            = "";
  //Add FOR B,C,D
  protected String cNoStore             = "";
  protected String cNoEvidence          = "";
  */

  //Acknowledgment 'a'
  //SAME FOR A,B,C,D
  private FieldGroup aGroup            = new FieldGroup("a");
  /*
  protected String aOfficerName           = "";
  protected String aOfficerTitle          = "";
  protected String aCompanyRepresented    = "";
  protected String aDate                  = "";
  */


  //PCI DSS Action Plan for Non-Compliance 'ap'
  //D uses 1-12
  //C uses 1-9, 11, 12
  //B uses 3,4,7,9,12
  //A uses 9,12
  private FieldGroup apGroup              = new FieldGroup("ap");
  /*
  protected String ap1                    = "";
  protected String ap2                    = "";
  protected String ap3                    = "";
  protected String ap4                    = "";
  protected String ap5                    = "";
  protected String ap6                    = "";
  protected String ap7                    = "";
  protected String ap8                    = "";
  protected String ap9                    = "";
  protected String ap10                   = "";
  protected String ap11                   = "";
  protected String ap12                   = "";

  protected String ap1Action              = "";
  protected String ap2Action              = "";
  protected String ap3Action              = "";
  protected String ap4Action              = "";
  protected String ap5Action              = "";
  protected String ap6Action              = "";
  protected String ap7Action              = "";
  protected String ap8Action              = "";
  protected String ap9Action              = "";
  protected String ap10Action             = "";
  protected String ap11Action             = "";
  protected String ap12Action             = "";
  */
  /**
   * Bean used to hold Attestation of Compliance for all PCI questionnaires
   * will be mutated based on actual PCI quiz type, based on TrackingNumber
   */
  public PCIInfoBean(UserBean user, PCIQuestionnaire.TrackingNumber trackingNum)
  {
    super(user);
    this.trackingNum = trackingNum;
  }

  public PCIInfoBean(PCIQuestionnaire.TrackingNumber trackingNum)
  {
    super();
    this.trackingNum = trackingNum;
  }

  //BUILD
  //build all fields
  //if tracking number != null, call load(trackNum)
  public void build()
  {
    log.debug("building pci infomation...");

    Field field;
    Validation val;

    //PART 1 of Attestation (compliance1.jsp)

    //build qsa
    field  = new Field("qsaCompanyName",30,30,true);
    qsaGroup.add(field);
    field  = new Field("qsaContactName",30,30,true);
    qsaGroup.add(field);
    field  = new Field("qsaTitle",30,30,true);
    qsaGroup.add(field);
    field  = new PhoneField("qsaTelephone",true);
    qsaGroup.add(field);
    field  = new EmailField("qsaEmail",50,50,true);
    qsaGroup.add(field);
    field  = new Field("qsaBizAddress",30,30,true);
    qsaGroup.add(field);
    field  = new Field("qsaCity",30,30,true);
    mGroup.add(field);
    field  = new DropDownField("qsaState", "state",new StateDropDownTable(),true);
    qsaGroup.add(field);
    field  = new Field("qsaCountry",3,3,true);
    qsaGroup.add(field);
    field  = new ZipField("qsaZip",true);
    qsaGroup.add(field);
    field  = new WebAddressField("qsaURL",50,50,true);
    qsaGroup.add(field);

    //add qsa into base group
    part1.add(qsaGroup);

    //build m
    field  = new Field("mCompanyName",30,30,false);
    mGroup.add(field);
    field  = new Field("mDBANames",30,30,false);
    mGroup.add(field);
    field  = new Field("mContactName",30,30,false);
    mGroup.add(field);
    field  = new Field("mTitle",30,30,false);
    mGroup.add(field);
    field  = new PhoneField("mTelephone",false);
    mGroup.add(field);
    field  = new EmailField("mEmail",50,50,false);
    mGroup.add(field);
    field  = new Field("mBizAddress",30,30,false);
    mGroup.add(field);
    field  = new Field("mCity",30,30,false);
    mGroup.add(field);
    field  = new DropDownField("mState", "state",new StateDropDownTable(),false);
    mGroup.add(field);
    field  = new Field("mCountry",3,3,false);
    mGroup.add(field);
    field  = new ZipField("mZip",false);
    mGroup.add(field);
    //turn off required val for level A and B quiz
    boolean removeVal = (trackingNum.isLevel(PCIQuestionnaire.LEVEL_B) || trackingNum.isLevel(PCIQuestionnaire.LEVEL_A));
    field  = new WebAddressField("mURL",50,50,removeVal?true:false);
    //field  = new WebAddressField("mURL",30,30,true);
    mGroup.add(field);

    //add m into base group
    part1.add(mGroup);

    //build b
    AtLeastOneValidation bVal = new AtLeastOneValidation("You must select at least one Business Type");
    field = new CheckField("bRetailer","Retailer","Y",false);
    bVal.addField(field);
    field.addValidation(bVal);
    bGroup.add(field);
    field = new CheckField("bTeleComm","Telecommunication","Y",false);
    bVal.addField(field);
    field.addValidation(bVal);
    bGroup.add(field);
    field = new CheckField("bGrocery","Grocery and Supermarket","Y",false);
    bVal.addField(field);
    field.addValidation(bVal);
    bGroup.add(field);
    field = new CheckField("bPetroleum","Petroleum","Y",false);
    bVal.addField(field);
    field.addValidation(bVal);
    bGroup.add(field);
    field = new CheckField("bECommerce","E-Commerce","Y",false);
    bVal.addField(field);
    field.addValidation(bVal);
    bGroup.add(field);
    field = new CheckField("bMailOrder","Mail/Telelphone Order","Y",false);
    bVal.addField(field);
    field.addValidation(bVal);
    bGroup.add(field);
    //field = new CheckField("bOther","Others","Y",false);
    //val = new RequiredIfYesValidation(field,"Please specify 'Other Type' of merchant business.");
    //bVal.addField(field);
    //field.addValidation(bVal);
    //bGroup.add(field);
    //field = new Field("bOtherDetails",30,30,true);
    //field.addValidation(val);
    //bGroup.add(field);
    field = new Field("bFacilities",50,50,false);
    bGroup.add(field);

    //add b into base group
    part1.add(bGroup);

    //build r
    field = new RadioButtonField("rThirdParty", yesNo, false, "Please provide an answer.");
    ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
    rGroup.add(field);
    field = new RadioButtonField("rMultipleAcquirers", yesNo, false, "Please provide an answer.");
    ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
    rGroup.add(field);
    part1.add(rGroup);

    //build t (only for B,C,D)
    //if(!trackingNum.isLevel(PCIQuestionnaire.LEVEL_A))
    //{
      field = new Field("tPaymentApp",50,50,false);
      //val = new RequiredIfYesValidation(field,"Please specify Payment Application Version.");
      tGroup.add(field);
      field = new Field("tPaymentAppVer",20,20,false);
      //field.addValidation(val);
      tGroup.add(field);
      part1.add(tGroup);
    //}

    //build e (if not D)
    if(!trackingNum.isLevel(PCIQuestionnaire.LEVEL_D))
    {
      field = new CheckField("eNotStored","","Y",false);
      val = new CheckFieldRequiredValidation("This checkbox selection is required in order to continue.");
      field.addValidation(val);
      eGroup.add(field);
      field = new CheckField("eStoredNotElec","","Y",false);
      val = new CheckFieldRequiredValidation("This checkbox selection is required in order to continue.");
      field.addValidation(val);
      eGroup.add(field);

        //if A
      if(trackingNum.isLevel(PCIQuestionnaire.LEVEL_A))
      {
        field = new CheckField("eThirdParty","","Y",false);
        val = new CheckFieldRequiredValidation("This checkbox selection is required in order to continue.");
        field.addValidation(val);
        eGroup.add(field);
        field = new CheckField("eThirdPartyCompliant","","Y",false);
        val = new CheckFieldRequiredValidation("This checkbox selection is required in order to continue.");
        field.addValidation(val);
        eGroup.add(field);
      }
        //if B
      else if(trackingNum.isLevel(PCIQuestionnaire.LEVEL_B))
      {
        val = new OnlyOneValidation("Please choose either A or B.");
        AtLeastOneValidation val2 = new AtLeastOneValidation("This checkbox selection is required in order to continue.");

        field = new CheckField("eImprint","A.","Y",false);
        ((OnlyOneValidation)val).addField(field);
        val2.addField(field);
        field.addValidation(val);
        field.addValidation(val2);
        eGroup.add(field);

        field = new CheckField("eIsolated","B.","Y",false);
        ((OnlyOneValidation)val).addField(field);
        val2.addField(field);
        field.addValidation(val);
        field.addValidation(val2);
        eGroup.add(field);
      }
        //if C
      else if(trackingNum.isLevel(PCIQuestionnaire.LEVEL_C))
      {
        field = new CheckField("eSameDevice","","Y",false);
        val = new CheckFieldRequiredValidation("This checkbox selection is required in order to continue.");
        field.addValidation(val);
        eGroup.add(field);
        field = new CheckField("eNotShared","","Y",false);
        val = new CheckFieldRequiredValidation("This checkbox selection is required in order to continue.");
        field.addValidation(val);
        eGroup.add(field);
        field = new CheckField("eSecure","","Y",false);
        val = new CheckFieldRequiredValidation("This checkbox selection is required in order to continue.");
        field.addValidation(val);
        eGroup.add(field);
      }

      part1.add(eGroup);
    }


    fields.add(part1);

    //PART 2 of Attestation (compliance2.jsp)

    //build v
    //field = new RadioButtonField("vCompliant", compliantArr, false, "Please select either Compliant or Non-compliant");
    //((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_VERTICAL);

    //this validation is for only selecting one checkbox - layout issue
    OnlyOneValidation oneVal = new OnlyOneValidation("Must choose either Compliant or Non-Compliant");

    field = new CheckField("vCompliantY","Compliant","Y",false);
    field.addValidation(oneVal);
    oneVal.addField(field);
    //this validation is for group c (below) - those checkboxes are only required
    //if vCompliantY = 'Y'
    RequiredIfYesValidation vVal = new RequiredIfYesValidation(field, "This checkbox is required if 'Compliant' is checked");
    vGroup.add(field);

    field = new CheckField("vCompliantN","Non-Compliant","Y",false);
    field.addValidation(oneVal);
    oneVal.addField(field);
    val = new RequiredIfNoValidation(field,"Please specify 'Target Date' for Compliance");
    vGroup.add(field);

    field = new DateField("vComplianceDate",true);
    field.addValidation(val);
    vGroup.add(field);
    part2.add(vGroup);

    //build c
    field = new CheckField("cComplete","","Y",false);
    //val = new CheckFieldRequiredValidation("This checkbox selection is required in order to continue.");
    //field.addValidation(val);
    field.addValidation(vVal);
    cGroup.add(field);
    field = new CheckField("cFair","","Y",false);
    //val = new CheckFieldRequiredValidation("This checkbox selection is required in order to continue.");
    //field.addValidation(val);
    field.addValidation(vVal);
    cGroup.add(field);
    field = new CheckField("cMaintain","","Y",false);
    //val = new CheckFieldRequiredValidation("This checkbox selection is required in order to continue.");
    //field.addValidation(val);
    field.addValidation(vVal);
    cGroup.add(field);
    if(!trackingNum.isLevel(PCIQuestionnaire.LEVEL_A))
    {
      field = new CheckField("cNoStore","","Y",false);
      //val = new CheckFieldRequiredValidation("This checkbox selection is required in order to continue.");
      //field.addValidation(val);
      field.addValidation(vVal);
      cGroup.add(field);
      field = new CheckField("cNoEvidence","","Y",false);
      //val = new CheckFieldRequiredValidation("This checkbox selection is required in order to continue.");
      //field.addValidation(val);
      field.addValidation(vVal);
      cGroup.add(field);
    }
    part2.add(cGroup);

    //build a
    field  = new Field("aOfficerName",30,30,false);
    aGroup.add(field);
    field  = new Field("aOfficerTitle",30,30,false);
    aGroup.add(field);
    field  = new Field("aCompanyRepresented",30,30,false);
    aGroup.add(field);
    field  = new DateField("aDate",false);
    aGroup.add(field);
    part2.add(aGroup);


    fields.add(part2);

    //build ap
    //D uses 1-12
    //C uses 1-9, 11, 12
    //B uses 3,4,7,9,12
    //A uses 9,12



    field = new RadioButtonField("ap9", yesNo, false, "Please select Compliance Status for Requirement 9");
    ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
    val = new RequiredIfNoValidation(field,"Please specify Remediation Date and Actions to resolve Requirement 9 Non-compliance.");
    apGroup.add(field);
    field  = new DateField("ap9Date",true);
    field.addValidation(val);
    apGroup.add(field);
    field  = new Field("ap9Action",30,30,true);
    field.addValidation(val);
    apGroup.add(field);

    field = new RadioButtonField("ap12", yesNo, false, "Please select Compliance Status for Requirement 12");
    ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
    val = new RequiredIfNoValidation(field,"Please specify Remediation Date and Actions to resolve Requirement 12 Non-compliance.");
    apGroup.add(field);
    field  = new DateField("ap12Date",true);
    field.addValidation(val);
    apGroup.add(field);
    field  = new Field("ap12Action",30,30,true);
    field.addValidation(val);
    apGroup.add(field);

    if(!trackingNum.isLevel(PCIQuestionnaire.LEVEL_A))
    {
      field = new RadioButtonField("ap3", yesNo, false, "Please select Compliance Status for Requirement 3");
      ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
      val = new RequiredIfNoValidation(field,"Please specify Remediation Date and Actions to resolve Requirement 3 Non-compliance.");
      apGroup.add(field);
      field  = new DateField("ap3Date",true);
      field.addValidation(val);
      apGroup.add(field);
      field  = new Field("ap3Action",30,30,true);
      field.addValidation(val);
      apGroup.add(field);

      field = new RadioButtonField("ap4", yesNo, false, "Please select Compliance Status for Requirement 4");
      ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
      val = new RequiredIfNoValidation(field,"Please specify Remediation Date and Actions to resolve Requirement 4 Non-compliance.");
      apGroup.add(field);
      field  = new DateField("ap4Date",true);
      field.addValidation(val);
      apGroup.add(field);
      field  = new Field("ap4Action",30,30,true);
      field.addValidation(val);
      apGroup.add(field);

      field = new RadioButtonField("ap7", yesNo, false, "Please select Compliance Status for Requirement 7");
      ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
      val = new RequiredIfNoValidation(field,"Please specify Remediation Date and Actions to resolve Requirement 7 Non-compliance.");
      apGroup.add(field);
      field  = new DateField("ap7Date",true);
      field.addValidation(val);
      apGroup.add(field);
      field  = new Field("ap7Action",30,30,true);
      field.addValidation(val);
      apGroup.add(field);

      if(!trackingNum.isLevel(PCIQuestionnaire.LEVEL_B))
      {

        field = new RadioButtonField("ap1", yesNo, false, "Please select Compliance Status for Requirement 1");
        ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
        val = new RequiredIfNoValidation(field,"Please specify Remediation Date and Actions to resolve Requirement 1 Non-compliance.");
        apGroup.add(field);
        field  = new DateField("ap1Date",true);
        field.addValidation(val);
        apGroup.add(field);
        field  = new Field("ap1Action",30,30,true);
        field.addValidation(val);
        apGroup.add(field);

        field = new RadioButtonField("ap2", yesNo, false, "Please select Compliance Status for Requirement 2");
        ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
        val = new RequiredIfNoValidation(field,"Please specify Remediation Date and Actions to resolve Requirement 2 Non-compliance.");
        apGroup.add(field);
        field  = new DateField("ap2Date",true);
        field.addValidation(val);
        apGroup.add(field);
        field  = new Field("ap2Action",30,30,true);
        field.addValidation(val);
        apGroup.add(field);

        field = new RadioButtonField("ap5", yesNo, false, "Please select Compliance Status for Requirement 5");
        ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
        val = new RequiredIfNoValidation(field,"Please specify Remediation Date and Actions to resolve Requirement 5 Non-compliance.");
        apGroup.add(field);
        field  = new DateField("ap5Date",true);
        field.addValidation(val);
        apGroup.add(field);
        field  = new Field("ap5Action",30,30,true);
        field.addValidation(val);
        apGroup.add(field);

        field = new RadioButtonField("ap6", yesNo, false, "Please select Compliance Status for Requirement 6");
        ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
        val = new RequiredIfNoValidation(field,"Please specify Remediation Date and Actions to resolve Requirement 6 Non-compliance.");
        apGroup.add(field);
        field  = new DateField("ap6Date",true);
        field.addValidation(val);
        apGroup.add(field);
        field  = new Field("ap6Action",30,30,true);
        field.addValidation(val);
        apGroup.add(field);

        field = new RadioButtonField("ap8", yesNo, false, "Please select Compliance Status for Requirement 8");
        ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
        val = new RequiredIfNoValidation(field,"Please specify Remediation Date and Actions to resolve Requirement 8 Non-compliance.");
        apGroup.add(field);
        field  = new DateField("ap8Date",true);
        field.addValidation(val);
        apGroup.add(field);
        field  = new Field("ap8Action",30,30,true);
        field.addValidation(val);
        apGroup.add(field);

        field = new RadioButtonField("ap11", yesNo, false, "Please select Compliance Status for Requirement 11");
        ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
        val = new RequiredIfNoValidation(field,"Please specify Remediation Date and Actions to resolve Requirement 11 Non-compliance.");
        apGroup.add(field);
        field  = new DateField("ap11Date",true);
        field.addValidation(val);
        apGroup.add(field);
        field  = new Field("ap11Action",30,30,true);
        field.addValidation(val);
        apGroup.add(field);

        if(trackingNum.isLevel(PCIQuestionnaire.LEVEL_D))
        {

          field = new RadioButtonField("ap10", yesNo, false, "Please select Compliance Status for Requirement 10");
          ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
          val = new RequiredIfNoValidation(field,"Please specify Remediation Date and Actions to resolve Requirement 10 Non-compliance.");
          apGroup.add(field);
          field  = new DateField("ap10Date",true);
          field.addValidation(val);
          apGroup.add(field);
          field  = new Field("ap10Action",30,30,true);
          field.addValidation(val);
          apGroup.add(field);

        }
      }
    }

    part3.add(apGroup);

    fields.add(part3);

    getFields().setFixImage("/images/tpg_red_flag.gif",16,16);

  }

  public void setFields(HttpServletRequest request, String currentJSPPage)
  {
    FieldGroup group;

    //need to track checkboxes that were unchecked
    //so we need to differentiate the FieldGroups being
    //set... kind of clumsy
    if(currentJSPPage.indexOf("compliance1")>-1)
    {
      group = part1;
    }
    else if(currentJSPPage.indexOf("compliance2")>-1)
    {
      //need to have parts 2 and 3 added together here
      group = new FieldGroup("2and3");
      group.add(part2);
      group.add(part3);
    }
    else
    {
      group = new FieldGroup("empty");
    }

    setLocalFields(request, group);

  }

  private void setLocalFields(HttpServletRequest request, Field field)
  {
    String value;

    if(field instanceof FieldGroup)
    {
      for(Iterator it = ((FieldGroup)field).iterator(); it.hasNext();)
      {
        setLocalFields(request,(Field)it.next());
      }
    }
    else
    {
      value = request.getParameter(field.getName());

      log.debug("name: "+field.getName()+"-- value: "+value );

      try
      {
        if( value != null )
        {
          value = URLDecoder.decode(value, "UTF-8");
        }
      }
      catch(Exception e)
      {
        logEntry("PCIInfoBean::setFieldsWithStandardRequest()", e.toString());
      }

      field.setData(value);

      //for checkbox unclicked
      if(field instanceof CheckField)
      {
        if(value == null && ((CheckField)field).isChecked())
        {
          field.setData("N");
        }
      }
    }
  }

  //LOAD
  //use tracking number to grab all data
  public void load()
  {
    //test the tracking number against the DB to ensure that it's been submitted
    if(loadable())
    {
      log.debug("loading pci information...");
      //grab the data and load the fields
      ResultSetIterator it = null;
      ResultSet rs = null;
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:724^9*/

//  ************************************************************
//  #sql [Ctx] it = { select
//              nvl(TRACKING_NUMBER,'') as TRACKING_NUMBER,
//              nvl(QSA_COMPANYNAME,'') as QSA_COMPANYNAME,
//              nvl(QSA_CONTACTNAME,'') as QSA_CONTACTNAME,
//              nvl(QSA_TITLE,'') as QSA_TITLE,
//              nvl(QSA_TELEPHONE,'') as QSA_TELEPHONE,
//              nvl(QSA_EMAIL,'') as QSA_EMAIL,
//              nvl(QSA_BIZADDRESS,'') as QSA_BIZADDRESS,
//              nvl(QSA_CITY,'') as QSA_CITY,
//              nvl(QSA_STATE,'') as QSA_STATE,
//              nvl(QSA_COUNTRY,'') as QSA_COUNTRY,
//              nvl(QSA_ZIP,'') as QSA_ZIP,
//              nvl(QSA_URL,'') as QSA_URL,
//              M_COMPANYNAME,
//              M_DBANAMES,
//              M_CONTACTNAME,
//              M_TITLE,
//              M_TELEPHONE,
//              M_EMAIL,
//              M_BIZADDRESS,
//              M_CITY,
//              M_STATE,
//              M_COUNTRY,
//              M_ZIP,
//              M_URL,
//              RETAILER,
//              TELECOMM,
//              GROCERY,
//              PETROLEUM,
//              ECOMMERCE,
//              MAILORDER,
//              OTHER,
//              FACILITIES,
//              THIRDPARTY_SERVICE,
//              MULTIPLEACQUIRERS,
//              PAYMENTAPP,
//              PAYMENTAPPVER,
//              NOTSTORED,
//              STOREDNOTELEC,
//              THIRDPARTY,
//              THIRDPARTYCOMPLIANT,
//              IMPRINT,
//              ISOLATED,
//              SAMEDEVICE,
//              NOTSHARED,
//              SECURE,
//              nvl(COMPLIANT,'') as COMPLIANT,
//              COMPLIANCEDATE,
//              COMPLETE,
//              FAIR,
//              MAINTAIN,
//              NOSTORE,
//              NOEVIDENCE,
//              OFFICERNAME,
//              OFFICERTITLE,
//              COMPANYREPRESENTED,
//              SUBMITDATE
//            from
//              pci_compliance_data
//            where
//              tracking_number = :trackingNum.getTrackingNumber()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1333 = trackingNum.getTrackingNumber();
  try {
   String theSqlTS = "select\n            nvl(TRACKING_NUMBER,'') as TRACKING_NUMBER,\n            nvl(QSA_COMPANYNAME,'') as QSA_COMPANYNAME,\n            nvl(QSA_CONTACTNAME,'') as QSA_CONTACTNAME,\n            nvl(QSA_TITLE,'') as QSA_TITLE,\n            nvl(QSA_TELEPHONE,'') as QSA_TELEPHONE,\n            nvl(QSA_EMAIL,'') as QSA_EMAIL,\n            nvl(QSA_BIZADDRESS,'') as QSA_BIZADDRESS,\n            nvl(QSA_CITY,'') as QSA_CITY,\n            nvl(QSA_STATE,'') as QSA_STATE,\n            nvl(QSA_COUNTRY,'') as QSA_COUNTRY,\n            nvl(QSA_ZIP,'') as QSA_ZIP,\n            nvl(QSA_URL,'') as QSA_URL,\n            M_COMPANYNAME,\n            M_DBANAMES,\n            M_CONTACTNAME,\n            M_TITLE,\n            M_TELEPHONE,\n            M_EMAIL,\n            M_BIZADDRESS,\n            M_CITY,\n            M_STATE,\n            M_COUNTRY,\n            M_ZIP,\n            M_URL,\n            RETAILER,\n            TELECOMM,\n            GROCERY,\n            PETROLEUM,\n            ECOMMERCE,\n            MAILORDER,\n            OTHER,\n            FACILITIES,\n            THIRDPARTY_SERVICE,\n            MULTIPLEACQUIRERS,\n            PAYMENTAPP,\n            PAYMENTAPPVER,\n            NOTSTORED,\n            STOREDNOTELEC,\n            THIRDPARTY,\n            THIRDPARTYCOMPLIANT,\n            IMPRINT,\n            ISOLATED,\n            SAMEDEVICE,\n            NOTSHARED,\n            SECURE,\n            nvl(COMPLIANT,'') as COMPLIANT,\n            COMPLIANCEDATE,\n            COMPLETE,\n            FAIR,\n            MAINTAIN,\n            NOSTORE,\n            NOEVIDENCE,\n            OFFICERNAME,\n            OFFICERTITLE,\n            COMPANYREPRESENTED,\n            SUBMITDATE\n          from\n            pci_compliance_data\n          where\n            tracking_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.quiz.pci.PCIInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1333);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.quiz.pci.PCIInfoBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:787^11*/

          rs = it.getResultSet();

          //setFields(rs, false);

          log.debug("made it to the set...");

          if(rs.next())
          {
            setData("qsaCompanyName",rs.getString("QSA_COMPANYNAME"));
            setData("qsaContactName",rs.getString("QSA_CONTACTNAME"));
            setData("qsaTitle",rs.getString("QSA_TITLE"));
            setData("qsaTelephone",rs.getString("QSA_TELEPHONE"));
            setData("qsaEmail",rs.getString("QSA_EMAIL"));
            setData("qsaBizAddress",rs.getString("QSA_BIZADDRESS"));
            setData("qsaCity",rs.getString("QSA_CITY"));
            setData("qsaState",rs.getString("QSA_STATE"));
            setData("qsaCountry",rs.getString("QSA_COUNTRY"));
            setData("qsaZip",rs.getString("QSA_ZIP"));
            setData("qsaURL",rs.getString("QSA_URL"));
            setData("mCompanyName",rs.getString("M_COMPANYNAME"));
            setData("mDBANames",rs.getString("M_DBANAMES"));
            setData("mContactName",rs.getString("M_CONTACTNAME"));
            setData("mTitle",rs.getString("M_TITLE"));
            setData("mTelephone",rs.getString("M_TELEPHONE"));
            setData("mEmail",rs.getString("M_EMAIL"));
            setData("mBizAddress",rs.getString("M_BIZADDRESS"));
            setData("mCity",rs.getString("M_CITY"));
            setData("mState",rs.getString("M_STATE"));
            setData("mCountry",rs.getString("M_COUNTRY"));
            setData("mZip",rs.getString("M_ZIP"));
            setData("mURL",rs.getString("M_URL"));
            setData("bRetailer",rs.getString("RETAILER"));
            setData("bTeleComm",rs.getString("TELECOMM"));
            setData("bGrocery",rs.getString("GROCERY"));
            setData("bPetroleum",rs.getString("PETROLEUM"));
            setData("bECommerce",rs.getString("ECOMMERCE"));
            setData("bMailOrder",rs.getString("MAILORDER"));
            //setData("bOther",rs.getString("OTHER"));
            setData("bFacilities",rs.getString("FACILITIES"));
            setData("rThirdParty",rs.getString("THIRDPARTY_SERVICE"));
            setData("rMultipleAcquirers",rs.getString("MULTIPLEACQUIRERS"));
            setData("tPaymentApp",rs.getString("PAYMENTAPP"));
            setData("tPaymentAppVer",rs.getString("PAYMENTAPPVER"));
            setData("eNotStored",rs.getString("NOTSTORED"));
            setData("eStoredNotElec",rs.getString("STOREDNOTELEC"));
            setData("eThirdParty",rs.getString("THIRDPARTY"));
            setData("eThirdPartyCompliant",rs.getString("THIRDPARTYCOMPLIANT"));
            setData("eImprint",rs.getString("IMPRINT"));
            setData("eIsolated",rs.getString("ISOLATED"));
            setData("eSameDevice",rs.getString("SAMEDEVICE"));
            setData("eNotShared",rs.getString("NOTSHARED"));
            setData("eSecure",rs.getString("SECURE"));
            //Compliant holds Y, N or P - set the individual checkboxes
            decodeComplianceAnswer(rs.getString("COMPLIANT"));
            ((DateField)getField("vComplianceDate")).setUtilDate(rs.getDate("COMPLIANCEDATE"));
            setData("cComplete",rs.getString("COMPLETE"));
            setData("cFair",rs.getString("FAIR"));
            setData("cMaintain",rs.getString("MAINTAIN"));
            setData("cNoStore",rs.getString("NOSTORE"));
            setData("cNoEvidence",rs.getString("NOEVIDENCE"));
            setData("aOfficerName",rs.getString("OFFICERNAME"));
            setData("aOfficerTitle",rs.getString("OFFICERTITLE"));
            setData("aCompanyRepresented",rs.getString("COMPANYREPRESENTED"));
            ((DateField)getField("aDate")).setUtilDate(rs.getDate("SUBMITDATE"));
          };

          //format the date fields
          ((DateField)getField("vComplianceDate")).setDateFormat("MM/dd/yyyy");
          ((DateField)getField("aDate")).setDateFormat("MM/dd/yyyy");


        //check for action plan load
        /*@lineinfo:generated-code*//*@lineinfo:861^9*/

//  ************************************************************
//  #sql [Ctx] it = { select
//              'ap'||REQ_NUMBER as apLabel,
//              STATUS,
//              REMEDIATION_DATE,
//              ACTION
//            from
//              pci_compliance_action_plan
//            where
//              tracking_number = :trackingNum.getTrackingNumber()
//            order by
//              REQ_NUMBER asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1334 = trackingNum.getTrackingNumber();
  try {
   String theSqlTS = "select\n            'ap'||REQ_NUMBER as apLabel,\n            STATUS,\n            REMEDIATION_DATE,\n            ACTION\n          from\n            pci_compliance_action_plan\n          where\n            tracking_number =  :1 \n          order by\n            REQ_NUMBER asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.quiz.pci.PCIInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1334);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.quiz.pci.PCIInfoBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:874^9*/

        rs = it.getResultSet();

        while(rs.next())
        {
          String apLabel = rs.getString("apLabel");
          setData(apLabel,rs.getString("STATUS"));
          ((DateField)getField(apLabel+"Date")).setUtilDate(rs.getDate("REMEDIATION_DATE"));
          ((DateField)getField(apLabel+"Date")).setDateFormat("MM/dd/yyyy");
          setData(apLabel+"Action",rs.getString("ACTION"));
        }
      }
      catch(Exception e)
      {
        e.printStackTrace();
        logEntry("PCInfoBean::load()",e.toString());
      }
      finally
      {
        cleanUp();
      }
    } //end if loadable
  }

  //SAVE - delete, then insert
  public void save() throws Exception
  {
    log.debug("Calling save()...");
    try
    {
      connect();

      setAutoCommit(false);
      log.debug("deleting()...");
      //remove any existing information
      /*@lineinfo:generated-code*//*@lineinfo:910^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from pci_compliance_data
//          where tracking_number = :trackingNum.getTrackingNumber()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1335 = trackingNum.getTrackingNumber();
  try {
   String theSqlTS = "delete\n        from pci_compliance_data\n        where tracking_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.quiz.pci.PCIInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1335);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:915^7*/

      /*@lineinfo:generated-code*//*@lineinfo:917^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from pci_compliance_action_plan
//          where tracking_number = :trackingNum.getTrackingNumber()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1336 = trackingNum.getTrackingNumber();
  try {
   String theSqlTS = "delete\n        from pci_compliance_action_plan\n        where tracking_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.quiz.pci.PCIInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1336);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:922^7*/


        //insert new run of data
      log.debug("inserting()...");
      /*@lineinfo:generated-code*//*@lineinfo:927^7*/

//  ************************************************************
//  #sql [Ctx] { insert into PCI_COMPLIANCE_DATA
//          (
//            TRACKING_NUMBER,
//            QSA_COMPANYNAME,
//            QSA_CONTACTNAME,
//            QSA_TITLE,
//            QSA_TELEPHONE,
//            QSA_EMAIL,
//            QSA_BIZADDRESS,
//            QSA_CITY,
//            QSA_STATE,
//            QSA_COUNTRY,
//            QSA_ZIP,
//            QSA_URL,
//            M_COMPANYNAME,
//            M_DBANAMES,
//            M_CONTACTNAME,
//            M_TITLE,
//            M_TELEPHONE,
//            M_EMAIL,
//            M_BIZADDRESS,
//            M_CITY,
//            M_STATE,
//            M_COUNTRY,
//            M_ZIP,
//            M_URL,
//            RETAILER,
//            TELECOMM,
//            GROCERY,
//            PETROLEUM,
//            ECOMMERCE,
//            MAILORDER,
//            OTHER,
//            FACILITIES,
//            THIRDPARTY_SERVICE,
//            MULTIPLEACQUIRERS,
//            PAYMENTAPP,
//            PAYMENTAPPVER,
//            NOTSTORED,
//            STOREDNOTELEC,
//            THIRDPARTY,
//            THIRDPARTYCOMPLIANT,
//            IMPRINT,
//            ISOLATED,
//            SAMEDEVICE,
//            NOTSHARED,
//            SECURE,
//            COMPLIANT,
//            COMPLIANCEDATE,
//            COMPLETE,
//            FAIR,
//            MAINTAIN,
//            NOSTORE,
//            NOEVIDENCE,
//            OFFICERNAME,
//            OFFICERTITLE,
//            COMPANYREPRESENTED,
//            SUBMITDATE
//          )
//          values
//          (
//            :trackingNum.getTrackingNumber(),
//            :getData("qsaCompanyName") ,
//            :getData("qsaContactName") ,
//            :getData("qsaTitle") ,
//            :getData("qsaTelephone") ,
//            :getData("qsaEmail") ,
//            :getData("qsaBizAddress") ,
//            :getData("qsaCity") ,
//            :getData("qsaState") ,
//            :getData("qsaCountry") ,
//            :getData("qsaZip") ,
//            :getData("qsaURL") ,
//            :getData("mCompanyName") ,
//            :getData("mDBANames") ,
//            :getData("mContactName") ,
//            :getData("mTitle") ,
//            :getData("mTelephone") ,
//            :getData("mEmail") ,
//            :getData("mBizAddress") ,
//            :getData("mCity") ,
//            :getData("mState") ,
//            :getData("mCountry") ,
//            :getData("mZip") ,
//            :getData("mURL") ,
//            :getData("bRetailer") ,
//            :getData("bTeleComm") ,
//            :getData("bGrocery") ,
//            :getData("bPetroleum") ,
//            :getData("bECommerce") ,
//            :getData("bMailOrder") ,
//            null,
//            :getData("bFacilities") ,
//            :getData("rThirdParty") ,
//            :getData("rMultipleAcquirers") ,
//            :getData("tPaymentApp") ,
//            :getData("tPaymentAppVer") ,
//            :getData("eNotStored") ,
//            :getData("eStoredNotElec") ,
//            :getData("eThirdParty") ,
//            :getData("eThirdPartyCompliant") ,
//            :getData("eImprint") ,
//            :getData("eIsolated") ,
//            :getData("eSameDevice") ,
//            :getData("eNotShared") ,
//            :getData("eSecure") ,
//            :getComplianceAnswer() ,
//            : ((DateField)getField("vComplianceDate")).getSqlDate() ,--DATE
//            :getData("cComplete") ,
//            :getData("cFair") ,
//            :getData("cMaintain") ,
//            :getData("cNoStore") ,
//            :getData("cNoEvidence") ,
//            :getData("aOfficerName") ,
//            :getData("aOfficerTitle") ,
//            :getData("aCompanyRepresented") ,
//            : ((DateField)getField("aDate")).getSqlDate() --DATE
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1337 = trackingNum.getTrackingNumber();
 String __sJT_1338 = getData("qsaCompanyName");
 String __sJT_1339 = getData("qsaContactName");
 String __sJT_1340 = getData("qsaTitle");
 String __sJT_1341 = getData("qsaTelephone");
 String __sJT_1342 = getData("qsaEmail");
 String __sJT_1343 = getData("qsaBizAddress");
 String __sJT_1344 = getData("qsaCity");
 String __sJT_1345 = getData("qsaState");
 String __sJT_1346 = getData("qsaCountry");
 String __sJT_1347 = getData("qsaZip");
 String __sJT_1348 = getData("qsaURL");
 String __sJT_1349 = getData("mCompanyName");
 String __sJT_1350 = getData("mDBANames");
 String __sJT_1351 = getData("mContactName");
 String __sJT_1352 = getData("mTitle");
 String __sJT_1353 = getData("mTelephone");
 String __sJT_1354 = getData("mEmail");
 String __sJT_1355 = getData("mBizAddress");
 String __sJT_1356 = getData("mCity");
 String __sJT_1357 = getData("mState");
 String __sJT_1358 = getData("mCountry");
 String __sJT_1359 = getData("mZip");
 String __sJT_1360 = getData("mURL");
 String __sJT_1361 = getData("bRetailer");
 String __sJT_1362 = getData("bTeleComm");
 String __sJT_1363 = getData("bGrocery");
 String __sJT_1364 = getData("bPetroleum");
 String __sJT_1365 = getData("bECommerce");
 String __sJT_1366 = getData("bMailOrder");
 String __sJT_1367 = getData("bFacilities");
 String __sJT_1368 = getData("rThirdParty");
 String __sJT_1369 = getData("rMultipleAcquirers");
 String __sJT_1370 = getData("tPaymentApp");
 String __sJT_1371 = getData("tPaymentAppVer");
 String __sJT_1372 = getData("eNotStored");
 String __sJT_1373 = getData("eStoredNotElec");
 String __sJT_1374 = getData("eThirdParty");
 String __sJT_1375 = getData("eThirdPartyCompliant");
 String __sJT_1376 = getData("eImprint");
 String __sJT_1377 = getData("eIsolated");
 String __sJT_1378 = getData("eSameDevice");
 String __sJT_1379 = getData("eNotShared");
 String __sJT_1380 = getData("eSecure");
 String __sJT_1381 = getComplianceAnswer();
 java.sql.Date __sJT_1382 =  ((DateField)getField("vComplianceDate")).getSqlDate();
 String __sJT_1383 = getData("cComplete");
 String __sJT_1384 = getData("cFair");
 String __sJT_1385 = getData("cMaintain");
 String __sJT_1386 = getData("cNoStore");
 String __sJT_1387 = getData("cNoEvidence");
 String __sJT_1388 = getData("aOfficerName");
 String __sJT_1389 = getData("aOfficerTitle");
 String __sJT_1390 = getData("aCompanyRepresented");
 java.sql.Date __sJT_1391 =  ((DateField)getField("aDate")).getSqlDate();
   String theSqlTS = "insert into PCI_COMPLIANCE_DATA\n        (\n          TRACKING_NUMBER,\n          QSA_COMPANYNAME,\n          QSA_CONTACTNAME,\n          QSA_TITLE,\n          QSA_TELEPHONE,\n          QSA_EMAIL,\n          QSA_BIZADDRESS,\n          QSA_CITY,\n          QSA_STATE,\n          QSA_COUNTRY,\n          QSA_ZIP,\n          QSA_URL,\n          M_COMPANYNAME,\n          M_DBANAMES,\n          M_CONTACTNAME,\n          M_TITLE,\n          M_TELEPHONE,\n          M_EMAIL,\n          M_BIZADDRESS,\n          M_CITY,\n          M_STATE,\n          M_COUNTRY,\n          M_ZIP,\n          M_URL,\n          RETAILER,\n          TELECOMM,\n          GROCERY,\n          PETROLEUM,\n          ECOMMERCE,\n          MAILORDER,\n          OTHER,\n          FACILITIES,\n          THIRDPARTY_SERVICE,\n          MULTIPLEACQUIRERS,\n          PAYMENTAPP,\n          PAYMENTAPPVER,\n          NOTSTORED,\n          STOREDNOTELEC,\n          THIRDPARTY,\n          THIRDPARTYCOMPLIANT,\n          IMPRINT,\n          ISOLATED,\n          SAMEDEVICE,\n          NOTSHARED,\n          SECURE,\n          COMPLIANT,\n          COMPLIANCEDATE,\n          COMPLETE,\n          FAIR,\n          MAINTAIN,\n          NOSTORE,\n          NOEVIDENCE,\n          OFFICERNAME,\n          OFFICERTITLE,\n          COMPANYREPRESENTED,\n          SUBMITDATE\n        )\n        values\n        (\n           :1 ,\n           :2  ,\n           :3  ,\n           :4  ,\n           :5  ,\n           :6  ,\n           :7  ,\n           :8  ,\n           :9  ,\n           :10  ,\n           :11  ,\n           :12  ,\n           :13  ,\n           :14  ,\n           :15  ,\n           :16  ,\n           :17  ,\n           :18  ,\n           :19  ,\n           :20  ,\n           :21  ,\n           :22  ,\n           :23  ,\n           :24  ,\n           :25  ,\n           :26  ,\n           :27  ,\n           :28  ,\n           :29  ,\n           :30  ,\n          null,\n           :31  ,\n           :32  ,\n           :33  ,\n           :34  ,\n           :35  ,\n           :36  ,\n           :37  ,\n           :38  ,\n           :39  ,\n           :40  ,\n           :41  ,\n           :42  ,\n           :43  ,\n           :44  ,\n           :45  ,\n           :46  ,--DATE\n           :47  ,\n           :48  ,\n           :49  ,\n           :50  ,\n           :51  ,\n           :52  ,\n           :53  ,\n           :54  ,\n           :55  --DATE\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.reports.quiz.pci.PCIInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1337);
   __sJT_st.setString(2,__sJT_1338);
   __sJT_st.setString(3,__sJT_1339);
   __sJT_st.setString(4,__sJT_1340);
   __sJT_st.setString(5,__sJT_1341);
   __sJT_st.setString(6,__sJT_1342);
   __sJT_st.setString(7,__sJT_1343);
   __sJT_st.setString(8,__sJT_1344);
   __sJT_st.setString(9,__sJT_1345);
   __sJT_st.setString(10,__sJT_1346);
   __sJT_st.setString(11,__sJT_1347);
   __sJT_st.setString(12,__sJT_1348);
   __sJT_st.setString(13,__sJT_1349);
   __sJT_st.setString(14,__sJT_1350);
   __sJT_st.setString(15,__sJT_1351);
   __sJT_st.setString(16,__sJT_1352);
   __sJT_st.setString(17,__sJT_1353);
   __sJT_st.setString(18,__sJT_1354);
   __sJT_st.setString(19,__sJT_1355);
   __sJT_st.setString(20,__sJT_1356);
   __sJT_st.setString(21,__sJT_1357);
   __sJT_st.setString(22,__sJT_1358);
   __sJT_st.setString(23,__sJT_1359);
   __sJT_st.setString(24,__sJT_1360);
   __sJT_st.setString(25,__sJT_1361);
   __sJT_st.setString(26,__sJT_1362);
   __sJT_st.setString(27,__sJT_1363);
   __sJT_st.setString(28,__sJT_1364);
   __sJT_st.setString(29,__sJT_1365);
   __sJT_st.setString(30,__sJT_1366);
   __sJT_st.setString(31,__sJT_1367);
   __sJT_st.setString(32,__sJT_1368);
   __sJT_st.setString(33,__sJT_1369);
   __sJT_st.setString(34,__sJT_1370);
   __sJT_st.setString(35,__sJT_1371);
   __sJT_st.setString(36,__sJT_1372);
   __sJT_st.setString(37,__sJT_1373);
   __sJT_st.setString(38,__sJT_1374);
   __sJT_st.setString(39,__sJT_1375);
   __sJT_st.setString(40,__sJT_1376);
   __sJT_st.setString(41,__sJT_1377);
   __sJT_st.setString(42,__sJT_1378);
   __sJT_st.setString(43,__sJT_1379);
   __sJT_st.setString(44,__sJT_1380);
   __sJT_st.setString(45,__sJT_1381);
   __sJT_st.setDate(46,__sJT_1382);
   __sJT_st.setString(47,__sJT_1383);
   __sJT_st.setString(48,__sJT_1384);
   __sJT_st.setString(49,__sJT_1385);
   __sJT_st.setString(50,__sJT_1386);
   __sJT_st.setString(51,__sJT_1387);
   __sJT_st.setString(52,__sJT_1388);
   __sJT_st.setString(53,__sJT_1389);
   __sJT_st.setString(54,__sJT_1390);
   __sJT_st.setDate(55,__sJT_1391);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1047^7*/

      //check for action plan information...
      //Only insert the non-compliant ones;
      //by default the others will be deemed Y during load

      for(int i = 1; i < 13; i++)
      {

        String apLabel = "ap"+i;
        log.debug("looking for action plan..."+apLabel);

        if(!getData(apLabel).equals(""))
        {
          log.debug("FOUND... inserting "+apLabel);
          /*@lineinfo:generated-code*//*@lineinfo:1062^11*/

//  ************************************************************
//  #sql [Ctx] { INSERT into PCI_COMPLIANCE_ACTION_PLAN
//              (
//                TRACKING_NUMBER,
//                REQ_NUMBER,
//                STATUS,
//                REMEDIATION_DATE,
//                ACTION
//              )
//              VALUES
//              (
//                :trackingNum.getTrackingNumber(),
//                :i,
//                :getData(apLabel),
//                :((DateField)getField(apLabel+"Date")).getSqlDateString(),
//                :getData(apLabel+"Action")
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1392 = trackingNum.getTrackingNumber();
 String __sJT_1393 = getData(apLabel);
 String __sJT_1394 = ((DateField)getField(apLabel+"Date")).getSqlDateString();
 String __sJT_1395 = getData(apLabel+"Action");
   String theSqlTS = "INSERT into PCI_COMPLIANCE_ACTION_PLAN\n            (\n              TRACKING_NUMBER,\n              REQ_NUMBER,\n              STATUS,\n              REMEDIATION_DATE,\n              ACTION\n            )\n            VALUES\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.reports.quiz.pci.PCIInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1392);
   __sJT_st.setInt(2,i);
   __sJT_st.setString(3,__sJT_1393);
   __sJT_st.setString(4,__sJT_1394);
   __sJT_st.setString(5,__sJT_1395);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1080^11*/
        }  //end if
      } //end for

      log.debug("inserting()... DONE");

      commit();

    }
    catch(Exception e)
    {
      //e.printStackTrace();
      rollback();
      logEntry("PCInfoBean::save()",e.toString());
      throw e;
    }
    finally
    {
      cleanUp();
    }
  }

  //TODO - will have to build in some sort of annual check here as well,
  //since at quiz also won't be loadable if more than a year has passed
  public boolean loadable()
  {

    if(trackingNum == null)
    {
      return false;
    }

    int count = 0;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1118^7*/

//  ************************************************************
//  #sql [Ctx] { select count(tracking_number) 
//          from PCI_COMPLIANCE_DATA
//          where tracking_number = :trackingNum.getTrackingNumber()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1396 = trackingNum.getTrackingNumber();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(tracking_number)  \n        from PCI_COMPLIANCE_DATA\n        where tracking_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.quiz.pci.PCIInfoBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_1396);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1123^7*/

    }
    catch (Exception e)
    {
      logEntry("PCInfoBean::loadable()",e.toString());
    }
    finally
    {
      cleanUp();
    }

    return count > 0;

  }

  public void reset()
  {
    fields.reset();
  }

  //only validate compliance pages
  public boolean isValid(String currentJSPPage)
  {
    log.debug("Calling isValid() on page..."+currentJSPPage);

    //ignore this before page 6
    if(currentJSPPage.indexOf("compliance")== -1)
    {
      return true;
    }

    if(part1Complete && part2Complete & part3Complete)
    {
      return true;
    }

    boolean result = true;

    //try part 1
    if(!part1Complete && currentJSPPage.indexOf("compliance1") > -1)
    {
      if((result = part1.isValid()) == true)
      {
        //need to call save here, in case they don't finish
        //the quiz this time around
        try
        {
          save();
          part1Complete = true;
        }
        catch(Exception e)
        {
          result = false;
        }
      }
    }
    else if( (!part2Complete || !part3Complete) &&
              currentJSPPage.indexOf("compliance2")> -1
           )
    {
      if(!part2Complete && part2.isValid()==true)
      {
        part2Complete = true;
      }

      if(!part3Complete && part3.isValid()==true)
      {
        part3Complete = true;
      }

      result = (part2Complete && part3Complete);

    }

    return result;
  }

  //Only returns "Y" if vComplaintY checkbox is checked
  public String getComplianceAnswer()
  {
    if(getData("vCompliantY").equals("Y"))
    {
      return "Y";
    }
    else if(getData("vCompliantN").equals("Y"))
    {
      return "N";
    }
    else
    {
      return "";
    }
  }

  public boolean isCompliant()
  {
    return getComplianceAnswer().equals("Y");
  }

  private void decodeComplianceAnswer(String dbResult)
  {
    if(dbResult != null)
    {
      if(dbResult.equals("Y"))
      {
        setData("vCompliantY", "Y");
        setData("vCompliantN", "N");
      }
      else if(dbResult.equals("N"))
      {
        setData("vCompliantY", "N");
        setData("vCompliantN", "Y");
      }
      else
      {
        setData("vCompliantY", "N");
        setData("vCompliantN", "N");
      }
    }
    else
    {
      setData("vCompliantY", "N");
      setData("vCompliantN", "N");
    }
  }


  public boolean isLevel(String level)
  {
    return trackingNum.isLevel(level);
  }

  public String getLevel()
  {
    return trackingNum.getPCILevel();
  }

  //D uses 1-12 (all)
  //C uses 1-9, 11, 12
  //B uses 3,4,7,9,12
  //A uses 9,12
  public boolean showAction(int req)
  {
    boolean result = false;

    if(isLevel(PCIQuestionnaire.LEVEL_D))
    {
      result = true;
    }
    else if (isLevel(PCIQuestionnaire.LEVEL_C) &&
             req != 10)
    {
      result = true;
    }
    else if(isLevel(PCIQuestionnaire.LEVEL_B) &&
             (req == 3 ||
              req == 4 ||
              req == 7 ||
              req == 9 ||
              req == 12)
            )
    {
      result = true;
    }
    else if(isLevel(PCIQuestionnaire.LEVEL_A) &&
             (req == 9 ||req == 12)  )
    {
      result = true;
    }
    return result;
  }

  public static String[][] yesNo =
  {
    {"Yes", "Y"},
    {"No","N"}
  };

  public static String[][] compliantArr =
  {
    {"Compliant", "Y"},
    {"Non-<br>Compliant","N"}
  };

  public class CheckFieldRequiredValidation implements Validation
  {
    String          ErrorMessage              = "Checking this box is required.";

    public CheckFieldRequiredValidation()
    {
    }

    public CheckFieldRequiredValidation( String msg )
    {
      ErrorMessage    = msg;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fieldData)
    {
      boolean             retVal        = true;

      try
      {
        if ( fieldData != null && !fieldData.toUpperCase().equals("Y") )
        {
          retVal = false;
        }
      }
      catch( Exception e )
      {
        // ingore and consider this to be failed validation
      }
      return( retVal );
    }
  }

  public String getBizTypeData()
  {
    StringBuffer sb = new StringBuffer();

    if(getData("bRetailer").equals("Y"))
    {
      sb.append("Retailer").append(", ");
    }
    if(getData("bTeleComm").equals("Y"))
    {
      sb.append("Telecommunications").append(", ");
    }
    if(getData("bPetroleum").equals("Y"))
    {
      sb.append("Petroleum").append(", ");
    }
    if(getData("bECommerce").equals("Y"))
    {
      sb.append("E-Commerce").append(", ");
    }
    if(getData("bMailOrder").equals("Y"))
    {
      sb.append("Mail Order").append(", ");
    }
    /*
    if(getData("bOther").equals("Y"))
    {
      sb.append(getData("bOtherDetails")).append(", ");
    }
    */
    if(sb.length()>2)
    {
      sb.setLength(sb.length()-2);
    }

    return sb.toString();
  }

}/*@lineinfo:generated-code*/