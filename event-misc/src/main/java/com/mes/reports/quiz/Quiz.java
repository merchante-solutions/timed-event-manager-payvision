package com.mes.reports.quiz;

import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.FieldBean;
import com.mes.user.UserBean;

public abstract class Quiz extends FieldBean implements Serializable
{
  //defines the type of quiz
  public static String QUIZ_TYPE_TAG        ="qtype";

  //defines the quiz id, or track number etc
  public static String QUIZ_ID_TAG          ="quid";

  public static String QUIZ_PAGE_TAG        ="pgNum";
  public static String QUIZ_HISTORY_TAG     ="hisId";
  public static String QUIZ_ACTION_TAG      ="action";

  //generic radiobutton [][]
  public static final String[][] YesNo =
  {
    { "Yes", "Y"},
    { "No",  "N"}
  };

  public static final String[][] YesNoNa =
  {
    { "Yes", "Y"},
    { "No",  "N"},
    { "N/A", "NA"}
  };

  //actions
  public static String ACTION_NEW               = "New";
  public static String ACTION_BUILD             = "Build";
  public static String ACTION_PAGE_MOVE_UP      = "Next Page";
  public static String ACTION_PAGE_MOVE_DOWN    = "Prev Page";
  public static String ACTION_SUBMIT            = "Submit";
  public static String ACTION_COMPLETE          = "Main Menu";
  public static String ACTION_LOAD              = "Load";
  public static String ACTION_SEARCH            = "Search";
  public static String ACTION_RESET             = "Reset";

  //states
  public static int STATE_ACTIVE             = 1;
  public static int STATE_VIEW               = 2;

  protected StringBuffer messenger         = null;

  public abstract void build() throws Exception;
  public abstract boolean validate();
  public abstract void score();
  public abstract void save() throws Exception;
  public abstract void load();
  public abstract String getCurrentJSPPage();
  public abstract void moveToErrorPage();

  protected int id;
  protected String name;
  protected int state = STATE_ACTIVE;
  protected boolean isValid = false;

  public Quiz ()
  {
  }

  public Quiz (UserBean user)
  {
    super(user);
  }

  //usually overridden at child level, but defaults to id
  public String getIDTag()
  {
    return ""+id;
  }

  /**
  * basic action - quiz will handle internally everything that needs to be
  * handled
  **/
  public void processAction(HttpServletRequest request, StringBuffer queryString)
  {
  }

  /**
  * important 'catch-all' method for processing unanticipated processes
  * from within the (attempted) generic QuizServlet
  * child classes can basically do what they need to do with the request and user
  **/
  public boolean doOutsideProcess(HttpServletRequest request, StringBuffer queryString)
  {
    return true;
  }

  public String getMessage()
  {
    if(messenger != null)
    {
      String message = messenger.toString();
      messenger = null;
      return message;
    }
    else
    {
      return "";
    }
  }

  public boolean hasMessage()
  {
    if (messenger != null && messenger.length() > 0)
    {
      return true;
    }
    return false;
  }


  public void setId(int id)
  {
    this.id = id;
  }

  public int getId()
  {
    return id;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getName()
  {
    return name;
  }

  public void setState(int state)
  {
    this.state = state;
  }

  public int getState()
  {
    return state;
  }

  public boolean isValid()
  {
    return isValid;
  }

  public void setValid(boolean isValid)
  {
    this.isValid = isValid;
  }

}