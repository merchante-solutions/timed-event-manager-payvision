/*@lineinfo:filename=PCIQuestionnaire*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.reports.quiz.pci;

import java.io.Serializable;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.TextareaField;
import com.mes.reports.quiz.Quiz;
import com.mes.reports.quiz.QuizComponent;
import com.mes.reports.quiz.QuizQuestion;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class PCIQuestionnaire extends Quiz implements Serializable
{
  // create class log category
  static Logger log = Logger.getLogger(PCIQuestionnaire.class);

  public static String LEVEL_TAG = "qLevel";
  public static String LEVEL_A   = "A";
  public static String LEVEL_B   = "B";
  public static String LEVEL_C   = "C";
  public static String LEVEL_D   = "D";

  public static String STATUS_NONE        = "None";
  public static String STATUS_COMPLETE    = "Complete";
  public static String STATUS_INCOMPLETE  = "Incomplete";
  public static String STATUS_RESET       = "Reset";

  //every quiz has a base count of 6 to the first compliance page
  public static int INIT_PAGE_COUNT       = 6;

  public static final String[][] YesNoCcu =
  {
    { "Yes",         "Y"},
    { "No",          "N"},
    { "Special",     "CCU"}
  };

  protected String[] ccuSuf = {"_constraint","_objective","_risk","_def", "_validation", "_maintenance"};

  protected String PCI_QUIZ_TAG = "pciquiz";

  protected String title          = "Payment Card Industry (PCI)<br>Self-Assessment Questionnaire";
  protected String intro1         = "/jsp/reports/quiz/pci/intro1.jsp";
  protected String intro2         = "/jsp/reports/quiz/pci/intro2.jsp";
  protected String intro3         = "/jsp/reports/quiz/pci/intro3.jsp";
  protected String intro4         = "/jsp/reports/quiz/pci/intro4.jsp";
  protected String cover          = "/jsp/reports/quiz/pci/cover.jsp";
  protected String compliance     = "/jsp/reports/quiz/pci/compliance1.jsp";
  protected String compliance2    = "/jsp/reports/quiz/pci/compliance2.jsp";
  protected String specialAction  = "/jsp/reports/quiz/pci/specialaction.jsp";
  protected String mainPage       = "/jsp/reports/quiz/pci/main.jsp";
  protected String scorePage      = "/jsp/reports/quiz/pci/score.jsp";
  protected String resultPage     = "/jsp/reports/quiz/pci/result.jsp";
  protected String cifPage        = "/jsp/maintenance/view_account.jsp";
  protected String errorPage      = "/jsp/reports/quiz/pci/error.jsp";

  //VARIABLES
  protected TrackingNumber trackingNumber  = null;
  protected PCIInfoBean pciBean            = null;

  protected boolean built                  = false;
  protected long merchNum                  = 0;
  protected String version                 = "v1.1";
  protected String userName                = "";
  protected int currentPage                = 1;
  protected String currentJSPPage          = intro1;
  protected int lastPageOfQuiz             = INIT_PAGE_COUNT;


  protected long volume                    = 0L;
  protected long transactionCount          = 0L;
  protected double cnpTransPercentage      = 0.0d;
  protected String dateTaken               = "";
  protected int merchLevel                 = 0;
  protected int delayDays                  = 0;
  protected String validationDetails       = STATUS_NONE; //operates as a status
  protected boolean isValid                = true;

 //contains QuizComponents, order is important
  //as it determines page order on generic JSP page
  protected QuizComponent currentComponent = null;
  protected LinkedList components          = new LinkedList();

  protected boolean specialActionFieldsBuilt  = false;
  protected boolean specialActionFieldsLoaded = false;

  protected FieldGroup specialActionFields = null;

  protected LinkedList specialQList        = null;

  protected boolean isAPass                = true;
  protected long dirtyId                   = 0L;

  //pos type dictates some question display - this breaks the concept
  //of the design to some degree, but allows us to avoid a great deal of
  //duplication and maintenance.
  protected int posType                    = 0;


  public PCIQuestionnaire() {}

  public PCIQuestionnaire(UserBean user)
  {
    super(user);

    if (user!=null)
    {
      this.merchNum = user.getMerchId();
      this.userName = user.getLoginName();
    }

    init();

  }

  public PCIQuestionnaire(String merchNum)
  {
    super(null);

    try
    {
      this.merchNum = Long.parseLong(merchNum);
    }
    catch(Exception e){}


    init();

  }

  protected void setId()
  {
    setId(mesConstants.QUIZ_PCI_QUESTIONNAIRE);
  }

  protected void init()
  {
    log.debug("init()...");

    setId();

    //query for relevent merchant info
    setMerchantVariables();

    //query status - returns true if in an existing state
    if ((isValid=setStatus())==true)
    {
      log.debug("isValid is holding as true in init()...");
      try
      {
        build();
        load();
        updateSpecialValidation(false);
        currentPage = 5;
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
    }

  }

  public void build() throws Exception
  {
    log.debug("building PCI quiz...");

    //first build the PCIInfoBean used for the attestation of compliance
    pciBean = new PCIInfoBean(trackingNumber);
    pciBean.build();

    //then build the questions, per the type of questionnaire (A,B,C or D)
    buildQuiz();

    //then generate a base page count to end of questions,
    //so not including any CCU or NA answers
    //will be used for backward navigation
    generatePageCount();
  }

  protected void setVersion(int qId)
  {
    if(qId > 4)
    {
      version = "v1.2";
    }
  }

  public String getVersion()
  {
    return version;
  }


  public void processAction(HttpServletRequest request, StringBuffer queryString)
  {
    messenger = new StringBuffer();

    String action = request.getParameter(Quiz.QUIZ_ACTION_TAG);
    log.debug("processAction::action = "+ action);

    log.debug("validationStatus = "+getValidationDetails());

    //if null, leave everything and return as submitted
    if(action==null)
    {
      return;
    }

    if(action.equals(Quiz.ACTION_NEW))
    {
      try
      {
        if( trackingNumber == null)
        {
          generateTrackingNumber(request);
        }

        build();
        load();

        currentPage++;
      }
      catch(Exception e)
      {
        e.printStackTrace();
        messenger.append(e.getMessage());
        //logEntry("PCIQuestionnaire::processAction()",e.toString());
      }
    }
    else if(action.equals(Quiz.ACTION_PAGE_MOVE_UP))
    {
      //always set the field data
      setFields(request);

      if(validate())
      {
        boolean saveOK = true;

        if(currentPage == 6)
        {
          //this syncs the merchant table to the pci tables
          //immediately after they submit the first attestation page
          //but only if this quiz hasn't been access before (new)
          if(!isValid)
          {
            updateMerchantStatus();
          }
        }

        //save on every advance of page, once at quiz start
        if(currentPage >= 6 && currentPage < 15)
        {
          try
          {
            save();
          }
          catch (Exception e)
          {
            messenger.append("There was an problem saving your data. Please try again.<br>If the problem persists, please contact you Customer Service representative.");
            saveOK = false;
          }
        }

        if(saveOK)
        {
          //advance the page
          currentPage++;

          //Only affects LEVEL_D as no other quiz goes to this page count
          if(currentPage == 13)
          {
            //go to pg 14
            currentPage++;

            if( levelDCCUNeeded() )
            {
              log.debug("levelDCCUNeeded == true");

              //stay at pg 14 and load new and/or existing SA fields
              loadSpecial(true);

            }
            else
            {
              //CCU level not needed, so set this to null
              //in case there were existing elements that
              //were changed during the quiz processs
              specialQList = null;
              specialActionFieldsBuilt = false;

              //go to 15 to hit compliance2 page
              currentPage++;
            }
          }
        }
      }
      else
      {
        messenger.append("Please correct errors as indicated ( <font color='red'>!</font> ) below.");
      }
    }
    else if(action.equals(Quiz.ACTION_PAGE_MOVE_DOWN))
    {
      if(currentPage == 5)
      {
        messenger.append("Unable to navigate back to this page.<br>Please use the reset button to start the process again.<br>");
      }
      else
      {
        //always set the field data, in case they were filling it
        //out before the wanted to back step
        setFields(request);

        currentPage--;

        //need to do this to avoid first component/compliance validation issue
        //we've stepped off the first page of questions back to the compliance
        //so we have to make sure we don't try to validate the loaded component
        //in the event they haven't completed it
        if(currentPage == 6)
        {
          currentComponent = null;
        }

        //this scenario only happens when you are in D, Compensating Controls
        //and have decided to go back to change answers
        if (currentPage == 13)
        {
          //first need to remove validation on the special CCU fields
          //this will be re-established upon forward movement
          updateSpecialValidation(false);

          //drop an extra page to reflect the jump back
          currentPage--;
        }

        //if you're in SAQ D, you'll need to drop 2 more...
        if(currentPage == 14)
        {
          currentPage = 12;
        }

      }
    }
    else if(action.equals(Quiz.ACTION_SUBMIT))
    {
      //always set the field data
      setFields(request);

      if(validate())
      {
        //process the entire thing
        score();

        //final save of the data
        try
        {
          save();
          //set this to 16, which is the result page
          currentPage = 16;
        }
        catch(Exception e)
        {
          //page stays where it is...
          messenger.append("There was an problem saving your data. Please try again.<br>If the problem persists, please contact you Customer Service representative.");
        }

      }
      else
      {
        messenger.append("Please correct errors as indicated ( <font color='red'>!</font> ) below.");
      }
    }
    else if(action.equals(Quiz.ACTION_COMPLETE))
    {
      //even valid anymore?
    }
    else if(action.equals(Quiz.ACTION_LOAD))
    {
      log.debug("loading quiz...");
      dirtyId = 0L;
      try
      {
        clear();
        trackingNumber = new TrackingNumber(request.getParameter(Quiz.QUIZ_HISTORY_TAG));
        build();
        load();
        currentPage = 6;
        setUserName( ((UserBean)request.getSession().getAttribute("UserLogin")).getLoginName());
      }
      catch(Exception e)
      {
        log.debug(e.getMessage());//messenger.append(e.getMessage());
        logEntry("PCIQuestionnaire::processAction()",e.toString());
      }
    }
    else if(action.equals(Quiz.ACTION_RESET))
    {
      delete();
      currentPage = 1;
    }
    else
    {
      //same as null
      return;
    }

  }

//===================================================
  protected void buildQuiz() throws Exception
  {
    log.debug("building PCIQuestionnaire...");

    ResultSetIterator it = null;
    ResultSet rs         = null;

    int internalQType = decodeLevel();
    //build quiz and components
    //for each component, build components if existing
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:438^7*/

//  ************************************************************
//  #sql [Ctx] it = { select
//            q.name                as name,
//            q.id                  as id,
//            --q.max_attempts        as max_attempts,
//            q.delay_required_days as req_delay,
//            qc.id                 as comp_id,
//            qc.name               as comp_name,
//            nvl(qc.description,' ')        as comp_desc,
//            qc1.id                as sub_comp_id,
//            qc1.name              as sub_comp_name,
//            nvl(qc1.description,' ')        as sub_comp_desc
//          from
//            quiz q,
//            quiz_comp_map qcm,
//            quiz_component qc,
//            quiz_component qc1,
//            quiz_subcomp_map qsm
//          where
//            q.id = :internalQType and
//            q.id = qcm.q_id and
//            qcm.comp_id = qc.id and
//            qc.id = qsm.parent_id and
//            qsm.child_id = qc1.id(+)and
//            qsm.quiz_id = :internalQType
//          order by
//            qcm.display_order, qsm.display_order asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n          q.name                as name,\n          q.id                  as id,\n          --q.max_attempts        as max_attempts,\n          q.delay_required_days as req_delay,\n          qc.id                 as comp_id,\n          qc.name               as comp_name,\n          nvl(qc.description,' ')        as comp_desc,\n          qc1.id                as sub_comp_id,\n          qc1.name              as sub_comp_name,\n          nvl(qc1.description,' ')        as sub_comp_desc\n        from\n          quiz q,\n          quiz_comp_map qcm,\n          quiz_component qc,\n          quiz_component qc1,\n          quiz_subcomp_map qsm\n        where\n          q.id =  :1  and\n          q.id = qcm.q_id and\n          qcm.comp_id = qc.id and\n          qc.id = qsm.parent_id and\n          qsm.child_id = qc1.id(+)and\n          qsm.quiz_id =  :2 \n        order by\n          qcm.display_order, qsm.display_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,internalQType);
   __sJT_st.setInt(2,internalQType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.quiz.pci.PCIQuestionnaire",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:466^7*/

      rs = it.getResultSet();

      QuizComponent comp = null;
      long lastId = 0;

      while(rs.next())
      {
        //first time in set overall data
        if(lastId == 0)
        {
          log.debug("building Quiz...");
          this.setName(rs.getString("name"));
          this.delayDays = rs.getInt("req_delay");
          setVersion(rs.getInt("id"));
        }

        //build components
        long compId = rs.getLong("comp_id");
        if(compId != lastId)
        {
          log.debug("building Component...");

          //build component
          comp = new QuizComponent(compId);
          comp.setName(rs.getString("comp_name"));
          comp.setDescription(rs.getString("comp_desc"));

          //add it to list
          components.add(comp);

          //create FieldGroup here
          getBaseGroup().add(comp.getFieldGroup());

          //set lastId
          lastId = compId;
        }

        //always look for a sub component
        long subId = rs.getLong("sub_comp_id");
        if(subId != 0)
        {
          log.debug("building subComponent...");

          QuizComponent subComp = new QuizComponent(subId);
          subComp.setName(rs.getString("sub_comp_name"));
          subComp.setDescription(rs.getString("sub_comp_desc"));

          //add Comment Box on all sub-components
          //include an empty validation that will be used only
          //if necessary (determined when questions are created)
          //Field field = new TextareaField(""+subId+"_comment",1000,6,90,true);
          //field.setShowErrorText(true);
          //field.addValidation(new IfNAProvideCommentValidation(), ""+subId+"_val");
          //subComp.addField(field);

          //add it to list
          comp.addComponent(subComp);

          //add FieldGroup to parent component
          comp.addField(subComp.getFieldGroup());

        }
      }

      //now add questions
      for(int i = 0; i < components.size();i++)
      {
        comp = (QuizComponent)components.get(i);
        buildQuestions(comp,internalQType);
      }

      //finally, set error image
      getFields().setFixImage("/images/tpg_red_flag.gif",16,16);

      log.debug("Done with the build");

      built = true;

    }
    catch (Exception e)
    {
      messenger.append(e.getMessage());
      //logEntry("PCIQuestionnaire::buildQuiz()",e.toString());
    }
    finally
    {
      try{it.close();}catch(Exception a){}
      try{rs.close();}catch(Exception a){}
      cleanUp();
    }

  }

  protected void buildQuestions(QuizComponent comp, int qLevel)
  {
    ResultSetIterator it = null;
    ResultSet rs         = null;

    log.debug("building question list...");

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:572^7*/

//  ************************************************************
//  #sql [Ctx] it = { select
//            q.id              as question_id,
//            nvl(q.label,'')   as question_label,
//            q.text            as question_text,
//            q.type            as question_type
//          from
//            quiz_question q,
//            quiz_comp_questions qcq
//          where
//            qcq.comp_id = :comp.getId() and
//            qcq.q_id = q.id and
//            qcq.quiz_id = :qLevel
//          order by
//            qcq.display_order
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1397 = comp.getId();
  try {
   String theSqlTS = "select\n          q.id              as question_id,\n          nvl(q.label,'')   as question_label,\n          q.text            as question_text,\n          q.type            as question_type\n        from\n          quiz_question q,\n          quiz_comp_questions qcq\n        where\n          qcq.comp_id =  :1  and\n          qcq.q_id = q.id and\n          qcq.quiz_id =  :2 \n        order by\n          qcq.display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1397);
   __sJT_st.setInt(2,qLevel);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.quiz.pci.PCIQuestionnaire",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:588^7*/

      rs = it.getResultSet();

      QuizQuestion q            = null;
      long lastId               = 0;
      String lastVisibleLabel   = "";

      while(rs.next())
      {
        //build question
        long id = rs.getLong("question_id");
        q = new QuizQuestion(id);
        q.setText(rs.getString("question_text"));
        q.setType(rs.getInt("question_type"));
        q.setLabel(rs.getString("question_label"));

        //need a hidden label here, for those
        //questions that have no label
        if(!q.getLabel().equals(""))
        {
          lastVisibleLabel = q.getLabel();
        }
        else
        {
          q.setHiddenLabel(lastVisibleLabel);
        }

        //build field
        try
        {
          q.buildField(determineAnswerFormat(q));
        }
        catch(Exception e)
        {
          //TODO
          //shouldn't happen since we know it's a radio that
          //needs a String[][]
        }

        //add to component
        comp.addQuestion(q);

      }

      //process subcomponents
      if(comp.hasComponents())
      {
        for(int i=0; i<comp.getComponents().size();i++)
        {
          QuizComponent sub = (QuizComponent)comp.getComponents().get(i);
          buildQuestions(sub,qLevel);
        }
      }

    }
    catch(Exception e)
    {
      messenger.append(e.getMessage());
      //logEntry("PCIQuestionnaire::buildQuestions()",e.toString());
    }

  }

  protected String[][] determineAnswerFormat(QuizQuestion q)
  {

    String[][] answerformat = Quiz.YesNo;

    //only level D has different style radios
    if(q != null && trackingNumber.isLevel(LEVEL_D))
    {
      log.debug("quizType = " + q.getType());
      if (q.getType()==QuizQuestion.RADIO)
      {
        answerformat = YesNoCcu;
      }
      else if(q.getType()==QuizQuestion.RADIO2)
      {
        answerformat = Quiz.YesNoNa;
      }
    }

    return answerformat;
  }

  public String getIDTag()
  {
    return PCI_QUIZ_TAG;
  }


  public void load()
  {

    log.debug("calling load()...");

    long historyId = trackingNumber.getHistoryId();

    log.debug("dirtyId..."+dirtyId);
    log.debug("historyId..."+historyId);

    if( dirtyId != historyId )
    {
      //clear all info on fields
      fields.reset();
      pciBean.reset();

      ResultSetIterator it = null;
      ResultSet rs         = null;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:703^9*/

//  ************************************************************
//  #sql [Ctx] it = { select
//              'q_' || qr.q_id as name,
//              answer
//            from
//              quiz_results qr,
//              quiz_history qh
//            where
//              qh.tracking_number   = :getTrackingNumber() and
//              qr.history_id(+)     = qh.id
//            order by
//              qr.q_id asc
//            };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1398 = getTrackingNumber();
  try {
   String theSqlTS = "select\n            'q_' || qr.q_id as name,\n            answer\n          from\n            quiz_results qr,\n            quiz_history qh\n          where\n            qh.tracking_number   =  :1  and\n            qr.history_id(+)     = qh.id\n          order by\n            qr.q_id asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1398);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.quiz.pci.PCIQuestionnaire",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:716^10*/

         rs = it.getResultSet();

         //set the fields from the result set
         Field field;

         while(rs.next())
         {
          field = (Field)getField(rs.getString("name"));

          if ( field != null )
          {
            //log.debug("loading: "+rs.getString("name")+"--"+rs.getString("answer"));
            field.setData(rs.getString("answer"));
          }
         }

         //load the attestation information
         pciBean.load();

         //load any special info
         //loadSpecial(getLevel().equals(LEVEL_D));
         loadSpecial(true);

         //set the id so this won't reload if rehit
         dirtyId = historyId;

        //set this to view only, if it's considered complete and compliant
        //effectively locking out the user
        if( getValidationDetails().equals(STATUS_COMPLETE) &&
            pciBean.isCompliant())
        {
          state = STATE_VIEW;
        }

       }
       catch(Exception e)
       {
         messenger.append(e.getMessage());
         logEntry("PCIQuestionnaire::load()",e.toString());
       }
     }
  }

  //thing here is that, since CCU is dynamic, we'll wait until
  //we get there to load... and at that time we'll determine whether
  //we need to add or remove data depending on most recent answers
  public void loadSpecial(boolean tryLoad)
  {
    //only look for the special actions if needed
    if(tryLoad)
    {
      ResultSetIterator it = null;
      ResultSet rs         = null;

      //OK, start by building newest data...
      //this reflects current question answers
      buildSpecialActionFields();


      //first time in
      //we need to determine if there was existing data
      //if so, we need to match the fields with the data...
      //at the end, we need to delete anything that exists, but
      //wasn't found in the recent answers
      if(!specialActionFieldsLoaded)
      {
        try
        {
          connect();

           //first look for entries, quick and dirty
          int count = 0;

          /*@lineinfo:generated-code*//*@lineinfo:791^11*/

//  ************************************************************
//  #sql [Ctx] { select count(q_id)
//              
//              from pci_special_action
//              where tracking_number = :getTrackingNumber()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1399 = getTrackingNumber();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(q_id)\n             \n            from pci_special_action\n            where tracking_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_1399);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:797^11*/

          //if you find any...
          if(count > 0)
          {
            //load from pci_special_actions
            /*@lineinfo:generated-code*//*@lineinfo:803^13*/

//  ************************************************************
//  #sql [Ctx] it = { select
//                  q_id,
//                  'q_'||q_id as prefix,
//                  nvl(constraint,'')  as constraint,
//                  nvl(objective,'')   as objective,
//                  nvl(risk,'')        as risk,
//                  nvl(definition,'')  as definition,
//                  nvl(validation,'')  as validation,
//                  nvl(maintenance,'') as maintenance
//                from
//                  pci_special_action
//                where
//                  tracking_number = :getTrackingNumber()
//                order by q_id asc
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1400 = getTrackingNumber();
  try {
   String theSqlTS = "select\n                q_id,\n                'q_'||q_id as prefix,\n                nvl(constraint,'')  as constraint,\n                nvl(objective,'')   as objective,\n                nvl(risk,'')        as risk,\n                nvl(definition,'')  as definition,\n                nvl(validation,'')  as validation,\n                nvl(maintenance,'') as maintenance\n              from\n                pci_special_action\n              where\n                tracking_number =  :1 \n              order by q_id asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1400);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.quiz.pci.PCIQuestionnaire",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:819^13*/

            rs = it.getResultSet();

            //look for a match, either load or list for deletion
            ArrayList dead = null;

            while(rs.next())
            {
              long qId              = rs.getLong("q_id");
              boolean foundExisting = false;

              //loop through specialQList
              QuizQuestion q = null;
              for(Iterator qIt = specialQList.iterator(); qIt.hasNext();)
              {
                q = (QuizQuestion)qIt.next();
                if( q.getId() == qId )
                {
                  //found a match, so load existing data
                  //but only load if the current data is empty
                  //as there's a chance we got here a second time within
                  //the quiz process which means we want to keep the active
                  //not persisted data
                  String prefix = rs.getString("prefix");

                  if(getData(prefix+"_constraint").equals(""))
                  {
                    setData(prefix+"_constraint",rs.getString("constraint"));
                  }
                  if(getData(prefix+"_objective").equals(""))
                  {
                    setData(prefix+"_objective",rs.getString("objective"));
                  }
                  if(getData(prefix+"_risk").equals(""))
                  {
                    setData(prefix+"_risk",rs.getString("risk"));
                  }
                  if(getData(prefix+"_def").equals(""))
                  {
                  setData(prefix+"_def",rs.getString("definition"));
                  }
                  if(getData(prefix+"_validation").equals(""))
                  {
                  setData(prefix+"_validation",rs.getString("validation"));
                  }
                  if(getData(prefix+"_maintenance").equals(""))
                  {
                  setData(prefix+"_maintenance",rs.getString("maintenance"));
                  }
                  foundExisting = true;
                  break;
                }
              }

              if(!foundExisting)
              {
                if(dead == null)
                {
                  dead  = new ArrayList();
                }
                dead.add(q);
              }
            }

            //delete those found in the db that don't match existing
            //question answers - we have no need for historical reference
            if(dead != null)
            {
              StringBuffer sb = new StringBuffer();

              QuizQuestion q;
              for(int i=0;i<dead.size();i++)
              {
                q = (QuizQuestion)dead.get(i);
                sb.append(q.getId()).append(",");
              }
              //lose last comma
              sb.deleteCharAt(sb.length()-1);

              /*@lineinfo:generated-code*//*@lineinfo:899^15*/

//  ************************************************************
//  #sql [Ctx] { delete from pci_special_action
//                  where tracking_number = :getTrackingNumber()
//                  and q_id in ( :sb.toString() )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1401 = getTrackingNumber();
 String __sJT_1402 = sb.toString();
   String theSqlTS = "delete from pci_special_action\n                where tracking_number =  :1 \n                and q_id in (  :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1401);
   __sJT_st.setString(2,__sJT_1402);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:904^15*/
            }
          }

          specialActionFieldsLoaded = true;

        }
        catch(Exception e)
        {
          //e.printStackTrace();
          messenger.append(e.getMessage());
          //logEntry("PCIQuestionnaire::loadSpecial()",e.toString());
        }
        finally
        {
          cleanUp();
        }

      }//end if specialActionFieldsLoaded

    }//end if
  }

  protected void updateMerchantStatus()
  {
    if(state==STATE_VIEW)
    {
      return;
    }

    log.debug("calling updateMerchantStatus...");
    try
    {

      connect();

      //update the merchant table to reflect current compliance state and level
      //assumption is that by starting the questionnaire, they are a level 4 merchant
      //in a state of non-compliance
      /*@lineinfo:generated-code*//*@lineinfo:943^7*/

//  ************************************************************
//  #sql [Ctx] { update
//           merchant
//          set
//           risk_pci_level     = 4,
//           risk_pci_compliant = 'N'
//          where
//           merch_number = :merchNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "update\n         merchant\n        set\n         risk_pci_level     = 4,\n         risk_pci_compliant = 'N'\n        where\n         merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:952^7*/

      //set the compliance answer to match to ensure subsequent saves
      //don't reset the Non-Compliant(Incomplete) status
      pciBean.setData("vCompliantN","Y");
      pciBean.setData("vCompliantY","N");

      updateValidationStatus(STATUS_INCOMPLETE);
    }
    catch(Exception e)
    {
      messenger.append(e.getMessage());
      //logEntry("PCIQuestionnaire::loadSpecial()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  public String getCurrentJSPPage()
  {

    log.debug("getCurrentJSPPage() -- page:"+currentPage);

    switch(currentPage)
    {
      case 1:
        currentJSPPage = intro1;
        break;
      case 2:
        currentJSPPage = intro2;
        break;
      case 3:
        currentJSPPage = intro3;
        break;
      case 4:
        currentJSPPage = intro4;
        break;
      case 5:
        currentJSPPage = cover;
        break;
      case 6:
        currentJSPPage = compliance;
        break;
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 12:
      case 13:
        setCurrentComponent(currentPage-7);
        //if you've run out of components
        //move on to the compliance2 page;
        //otherwise, stay on the main component page
        if(currentComponent!=null)
        {
          currentJSPPage = mainPage;
        }
        else
        {
          currentJSPPage = compliance2;
        }
        break;

      case 14:
        //this is a special page for LEVEL_D
        currentJSPPage = specialAction;
        break;
      case 15:
        //only for D as well
        currentJSPPage = compliance2;
        break;
      case 16:
        currentJSPPage = resultPage;
        break;
      case 98:
        currentJSPPage = cifPage;
        break;
      case 99:
      default:
        currentJSPPage = errorPage;
        break;
    }

    return currentJSPPage;
  }

  public boolean validate()
  {

    log.debug("calling validate()...");

    boolean _isValid = true;

    //view-only state has no validation
    if(state==STATE_VIEW)
    {
      return _isValid;
    }

    //use FieldBean field validation on this.
    //ignore validation if currentComponent is null
    _isValid = currentComponent != null? currentComponent.validate():true;

    log.debug(_isValid?"component is either null or valid":"component is invalid");

    //also validate the pciBean
    if(_isValid)
    {
      _isValid = pciBean != null? pciBean.isValid(currentJSPPage):true;
      log.debug(_isValid?"pciBean is either null or valid":"pciBean is invalid");
    }



    //and the Level D specials, if existing
    if(_isValid)
    {
      _isValid = specialQList != null? getField("special").isValid():true;
      log.debug(_isValid?"specialQList is either null or valid":"specialQList is invalid");
    }

    return _isValid;

  }


  /**
   * just use setFieldsWithStandardRequest to access/set the fields
  **/
  public void setFields(HttpServletRequest request)
  {
    setFieldsWithStandardRequest(request);

    if(pciBean != null)
    {
      pciBean.setFields(request,currentJSPPage);
    }

  }

  protected void delete()
  {
    //quiz_history - set result to delete
    //that way, if someone accidently resets they can
    //be reinstated since everything is keyed on the
    //tracking number
    try
    {
      connect();
      setAutoCommit(false);

      //update history to 'Reset' (deleted) for a hidden record
      /*@lineinfo:generated-code*//*@lineinfo:1108^7*/

//  ************************************************************
//  #sql [Ctx] { update
//            quiz_history
//          set
//            result = :STATUS_RESET
//          where
//            tracking_number = :getTrackingNumber()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1403 = getTrackingNumber();
  try {
   String theSqlTS = "update\n          quiz_history\n        set\n          result =  :1 \n        where\n          tracking_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,STATUS_RESET);
   __sJT_st.setString(2,__sJT_1403);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1116^7*/

      //don't need to keep these, do we?
      /*@lineinfo:generated-code*//*@lineinfo:1119^7*/

//  ************************************************************
//  #sql [Ctx] { delete from quiz_results
//          where history_id = :trackingNumber.getHistoryId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1404 = trackingNumber.getHistoryId();
   String theSqlTS = "delete from quiz_results\n        where history_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1404);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1123^7*/

      //reset this too to reflect change in merchant status for CIF
      /*@lineinfo:generated-code*//*@lineinfo:1126^7*/

//  ************************************************************
//  #sql [Ctx] { update
//           merchant
//          set
//           risk_pci_compliant = ''
//          where
//           merch_number = :merchNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "update\n         merchant\n        set\n         risk_pci_compliant = ''\n        where\n         merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1134^7*/

      //empty this object
      clear();

      commit();

    }
    catch(Exception e)
    {
      e.printStackTrace();
      //logEntry("PCIQuestionnaire::delete()",e.toString());
      rollback();
    }
    finally
    {
      cleanUp();
    }
  }

  protected void clear()
  {

    log.debug("clearing all fields and information...");

    //trash existing questions
    fields              = new FieldGroup("baseGroup");

    //remove tracking number to force redo,
    //and any special groups/lists
    trackingNumber      = null;
    components          = new LinkedList();
    specialActionFields = new FieldGroup("special");
    specialQList        = null;

    //revert back to not built
    validationDetails         = STATUS_NONE;
    state                     = STATE_ACTIVE;
    isValid                   = false;
    built                     = false;
    specialActionFieldsBuilt  = false;
    specialActionFieldsLoaded = false;
    setDateTaken("");

  }

  public void score()
  {
    if(state==STATE_VIEW)
    {
      return;
    }

    boolean isComplete = false;

    if(pciBean.getData("vCompliantY").equals("Y"))
    {
      //indicates if a 'N' (no) checkbox found
      boolean noWasFound = false;

      //no longer a need to score, just update status/validationDetails
      //have to check all fields to ensure a "Y" answer to compliment
      //one incorrect answer renders the quiz incomplete
      Field field;
      for(Iterator fIt = fields.iterator(); fIt.hasNext();)
      {
        field = (Field)fIt.next();
        if( ( noWasFound = lookForNo(field, getLevel().equals(LEVEL_D)) ) == true)
        {
          //change the compliance data to reflect this in pciBean
          pciBean.setData("vCompliantY","N");
          pciBean.setData("vCompliantN","Y");
          break;
        }
      }

      //set isComplete to the opp of noWasFound
      isComplete = !noWasFound;
    }

    validationDetails = isComplete?STATUS_COMPLETE:STATUS_INCOMPLETE;
  }

  protected boolean lookForNo(Field field, boolean isLevelD)
  {
    boolean result = false;

    if(field !=null)
    {
      if(field instanceof FieldGroup)
      {
        for( Iterator fIt = ((FieldGroup)field).iterator(); fIt.hasNext();)
        {
          if((result = lookForNo((Field)fIt.next(),isLevelD))==true)
          {
            break;
          }
        }
      }
      else if(field.getData().equals("N") )
      {
        result = true;
      }
      else if(isLevelD && field.getData().equals("CCU"))
      {
        //TODO
        //are we really going to do this - ???
        //there is already required status on these fields
        //if CCU is checked... shouldn't that be enough?

        //original thought...
        //need to ensure that any CCU answer (LEVEL_D only)
        //has the corresponding details answered as well

        //field.getFieldName()+"_constraint"
        //field.getFieldName()+"_objective"
        //field.getFieldName()+"_risk"
        //field.getFieldName()+"_def"
        //result = true;
        //break;
      }
    }

    return result;
  }

  public String getDateTakenStr()
  {
    return dateTaken;
  }

  protected void setDateTaken(String dateTakenString)
  {
    dateTaken = dateTakenString;
  }

  protected void generatePageCount()
  {
    //always reset this
    lastPageOfQuiz = INIT_PAGE_COUNT;

    try
    {
      connect();

      int count = 0;
      /*@lineinfo:generated-code*//*@lineinfo:1280^7*/

//  ************************************************************
//  #sql [Ctx] { select count(distinct parent_id) 
//          from quiz_subcomp_map
//          where quiz_id=:decodeLevel()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1405 = decodeLevel();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(distinct parent_id)  \n        from quiz_subcomp_map\n        where quiz_id= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,__sJT_1405);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1285^7*/

      lastPageOfQuiz = lastPageOfQuiz+count;
      log.debug("Page count to end of quiz = "+ lastPageOfQuiz);
    }
    catch (Exception e)
    {
      log.debug("unable to generate pageCount!");
    }
  }

  //First generate history ID
  //then create the TrackingNumber with the included type from request
  protected void generateTrackingNumber(HttpServletRequest request)
  {
    log.debug("calling generateTrackingNumber...");
    try
    {
      String quizType = request.getParameter(LEVEL_TAG);

      if(isValidType(quizType))
      {

        log.debug("quizType: "+quizType+" is valid");
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:1311^9*/

//  ************************************************************
//  #sql [Ctx] { insert into quiz_history
//            (
//              quiz_id,
//              merch_number,
//              username,
//              result
//            )
//            values
//            (
//              :decodeLevel(quizType),
//              :merchNum,
//              :userName,
//              :STATUS_NONE
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1406 = decodeLevel(quizType);
   String theSqlTS = "insert into quiz_history\n          (\n            quiz_id,\n            merch_number,\n            username,\n            result\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_1406);
   __sJT_st.setLong(2,merchNum);
   __sJT_st.setString(3,userName);
   __sJT_st.setString(4,STATUS_NONE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1327^9*/

        ResultSetIterator it  = null;
        ResultSet rs          = null;

        log.debug("generateTrackingNumber::decodeLevel(quizType)="+decodeLevel(quizType));

        //get the id back
        /*@lineinfo:generated-code*//*@lineinfo:1335^9*/

//  ************************************************************
//  #sql [Ctx] it = { select
//              id,
//              date_taken
//            from
//              quiz_history
//            where
//              quiz_id = :decodeLevel(quizType) and
//              merch_number = :merchNum and
//              result != 'deleted'
//            order by
//              date_taken desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1407 = decodeLevel(quizType);
  try {
   String theSqlTS = "select\n            id,\n            date_taken\n          from\n            quiz_history\n          where\n            quiz_id =  :1  and\n            merch_number =  :2  and\n            result != 'deleted'\n          order by\n            date_taken desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_1407);
   __sJT_st.setLong(2,merchNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.reports.quiz.pci.PCIQuestionnaire",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1348^9*/

        rs = it.getResultSet();

        if(rs.next())
        {
          log.debug("building tracking number...");
          trackingNumber = new TrackingNumber(rs.getLong("id"), quizType);
          setDateTaken(DateTimeFormatter.getFormattedDate(rs.getDate("date_taken"), "MM/dd/yyyy"));
        }

        if(trackingNumber != null)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1361^11*/

//  ************************************************************
//  #sql [Ctx] { update quiz_history
//              set tracking_number = :getTrackingNumber()
//              where id = :trackingNumber.getHistoryId()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1408 = getTrackingNumber();
 long __sJT_1409 = trackingNumber.getHistoryId();
   String theSqlTS = "update quiz_history\n            set tracking_number =  :1 \n            where id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1408);
   __sJT_st.setLong(2,__sJT_1409);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1366^11*/
        }

        try{it.close();}catch(Exception e){}
        try{rs.close();}catch(Exception e){}

      }
      else
      {
        throw new Exception("Not a valid PCI SAQ Level.");
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
      messenger.append(e.getMessage());
      //logEntry("PCIQuestionnaire::generateTrackingNumber()",e.toString());
      rollback();
    }
    finally
    {
      cleanUp();
    }
  }

  protected boolean isValidType(String level)
  {
    return (level != null &&
              ( level.equals(LEVEL_A) ||
                level.equals(LEVEL_B) ||
                level.equals(LEVEL_C) ||
                level.equals(LEVEL_D)
               )
             );
  }


  public void save() throws Exception
  {
    log.debug("calling save()");

    //no saving if view-only
    if(state==STATE_VIEW)
    {
      return;
    }

    long qhId = trackingNumber.getHistoryId();
    QuizComponent comp;

    try
    {
      connect();
      setAutoCommit(false);

      //first off, clear any previous results
      /*@lineinfo:generated-code*//*@lineinfo:1422^7*/

//  ************************************************************
//  #sql [Ctx] { delete from quiz_results
//          where history_id = :qhId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from quiz_results\n        where history_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,qhId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1426^7*/

      //input the answers into QuizResults
      //process each component
      for(Iterator c = components.iterator(); c.hasNext();)
      {
        comp = (QuizComponent)c.next();
        saveComponent(qhId, comp);
      }

      //call method to save any special information
      saveSpecial();


      /*@lineinfo:generated-code*//*@lineinfo:1440^7*/

//  ************************************************************
//  #sql [Ctx] { update
//           quiz_history
//          set
//           result = :getValidationDetails(),
//           date_taken = sysdate
//          where
//           tracking_number = :getTrackingNumber()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1410 = getValidationDetails();
 String __sJT_1411 = getTrackingNumber();
  try {
   String theSqlTS = "update\n         quiz_history\n        set\n         result =  :1 ,\n         date_taken = sysdate\n        where\n         tracking_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1410);
   __sJT_st.setString(2,__sJT_1411);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1449^7*/

      //update the merchant table to reflect current compliance state
      /*@lineinfo:generated-code*//*@lineinfo:1452^7*/

//  ************************************************************
//  #sql [Ctx] { update
//           merchant
//          set
//           risk_pci_compliant = :pciBean.getComplianceAnswer()
//          where
//           merch_number = :merchNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1412 = pciBean.getComplianceAnswer();
  try {
   String theSqlTS = "update\n         merchant\n        set\n         risk_pci_compliant =  :1 \n        where\n         merch_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1412);
   __sJT_st.setLong(2,merchNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1460^7*/

      commit();

    }
    catch(Exception e)
    {
      rollback();
      logEntry("PCIQuestionnaire::save()",e.toString());
      throw e;
    }
    finally
    {
      cleanUp();
    }

    //save any data in the PCI bean
    pciBean.save();

  }

  protected boolean doSaveSpecial()
  {
    return getLevel().equals(LEVEL_D) && currentPage == 14;
  }

  protected void saveSpecial()
  throws Exception
  {
    saveSpecial(doSaveSpecial());
  }

  protected void saveSpecial(boolean doIt)
  throws Exception
  {
    if(doIt)
    {
      log.debug("calling saveSpecial...");

      //first off, clear any previous results
      /*@lineinfo:generated-code*//*@lineinfo:1500^7*/

//  ************************************************************
//  #sql [Ctx] { delete from pci_special_action
//          where tracking_number = :getTrackingNumber()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1413 = getTrackingNumber();
   String theSqlTS = "delete from pci_special_action\n        where tracking_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1413);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1504^7*/

      //check for any special actions here

      if( specialQList != null )
      {
        log.debug("saving special action...");

        QuizQuestion q;
        for(Iterator it = specialQList.iterator();it.hasNext();)
        {
          q = (QuizQuestion)it.next();
          //get the fields related to this question
          //save them to PCI_SPECIAL_ACTIONS
          String constraint = getData(q.getFieldName()+"_constraint");
          String objective = getData(q.getFieldName()+"_objective");
          String risk = getData(q.getFieldName()+"_risk");
          String def = getData(q.getFieldName()+"_def");
          String val = getData(q.getFieldName()+"_validation");
          String main = getData(q.getFieldName()+"_maintenance");

          /*@lineinfo:generated-code*//*@lineinfo:1525^11*/

//  ************************************************************
//  #sql [Ctx] { insert into pci_special_action
//              (
//                tracking_number,
//                q_id,
//                constraint,
//                objective,
//                risk,
//                definition,
//                validation,
//                maintenance
//              )
//              values
//              (
//               :getTrackingNumber(),
//               :q.getId(),
//               :constraint,
//               :objective,
//               :risk,
//               :def,
//               :val,
//               :main
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1414 = getTrackingNumber();
 long __sJT_1415 = q.getId();
   String theSqlTS = "insert into pci_special_action\n            (\n              tracking_number,\n              q_id,\n              constraint,\n              objective,\n              risk,\n              definition,\n              validation,\n              maintenance\n            )\n            values\n            (\n              :1 ,\n              :2 ,\n              :3 ,\n              :4 ,\n              :5 ,\n              :6 ,\n              :7 ,\n              :8 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1414);
   __sJT_st.setLong(2,__sJT_1415);
   __sJT_st.setString(3,constraint);
   __sJT_st.setString(4,objective);
   __sJT_st.setString(5,risk);
   __sJT_st.setString(6,def);
   __sJT_st.setString(7,val);
   __sJT_st.setString(8,main);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1549^11*/
        }
      }

    }//end doSave
  }

  protected void saveComponent(long id, QuizComponent comp)
    throws Exception
  {
    log.debug("calling saveComponent...");

    QuizComponent subComp;
    QuizQuestion question;

    //score any questions
    if(comp.hasQuestions())
    {
      List qList = comp.getQuestions();
      for(Iterator q = qList.iterator(); q.hasNext();)
      {
        question = (QuizQuestion)q.next();

        //since some questions are "labels" only, have to check for null
        Field field;
        if( (field = question.getField()) != null)
        {
          //log.debug("saving id: "+question.getId()+"::value: "+question.getField().getData());

          /*@lineinfo:generated-code*//*@lineinfo:1578^11*/

//  ************************************************************
//  #sql [Ctx] { insert into quiz_results
//              (
//                history_id,
//                q_id,
//                answer
//              )
//              values
//              (
//                :id,
//                :question.getId(),
//                :question.getField().getData()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1416 = question.getId();
 String __sJT_1417 = question.getField().getData();
   String theSqlTS = "insert into quiz_results\n            (\n              history_id,\n              q_id,\n              answer\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setLong(2,__sJT_1416);
   __sJT_st.setString(3,__sJT_1417);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1592^11*/
        }
      }
    }

    //score any subcomponents
    if(comp.hasComponents())
    {
      for(Iterator s = comp.getComponents().iterator(); s.hasNext();)
      {
        subComp = (QuizComponent)s.next();
        saveComponent(id, subComp);
      }
    }
  }

  protected boolean levelDCCUNeeded()
  {
    //this should never happen
    if(!getLevel().equals(LEVEL_D))
    {
      return false;
    }

    //assume that if the fields have been built, then this is true
    if(!specialActionFieldsBuilt)
    {
      //basically loop through all fields looking for a CCU
      Field field;
      for(Iterator it = fields.getFieldsVector().iterator(); it.hasNext();)
      {
        //log.debug("looking for CCU...");
        field = (Field)it.next();
        if(field != null && field.getData().equals("CCU"))
        {
          //get out of here as soon as you
          //find the first special field selected
          log.debug("CCU FOUND!");
          return true;
        }
      }
    }
    else
    {
      return true;
    }

    //default
    return false;
  }


  public List getSpecialActionQuestions()
  {
    return specialQList;
  }

  public boolean hasCCUQuestions()
  {
    return (specialQList!=null && specialQList.size()>0);
  }

  protected void buildSpecialActionFields()
  {
    //always start with a fresh fieldgroup
    specialActionFields = new FieldGroup("special");

    log.debug("building special action fields...");
    QuizComponent comp;

    //loop through each comp and build
    for( Iterator it = components.iterator(); it.hasNext();)
    {
      comp = (QuizComponent)it.next();
      buildSpecialActionFields(comp);
    }

    //ensure that all fields are set to required
    //since validation gets removed if the user navigates
    //backward away from this page
    if(currentPage==14)
    {
      updateSpecialValidation(true);
    }

    //add to base group after all have been built
    fields.add(specialActionFields);

    //flag it
    specialActionFieldsBuilt = true;
  }

  protected void buildSpecialActionFields(QuizComponent comp)
  {
    //loop through questions and build fields if needed
    if(comp.hasQuestions())
    {
      //iterate questions
      QuizQuestion q;
      for(Iterator it = comp.iterator(); it.hasNext();)
      {
        q = (QuizQuestion)it.next();
        buildSpecialActionFields(q);
      }
    }

    if(comp.hasComponents())
    {
      QuizComponent temp;
      for(Iterator i = comp.getComponents().iterator(); i.hasNext();)
      {
        temp = (QuizComponent)i.next();
        buildSpecialActionFields(temp);
      }
    }
  }

  //this is a top in process, based on the current state of the questions
  //we need to be aware of the fact that this could be the 1..Nth time coming in
  //so we have to handle the list accordingly.
  protected void buildSpecialActionFields(QuizQuestion q)
  {
    QuizQuestion q2;

    Field field = q.getField();
    //we're looking for those questions that were answered
    //'CCU' meaning that they have to fill out further info
    if(field != null)
    {
      if (field.getData().equals("CCU"))
      {
        boolean alreadyExists = false;

        //if first one found, start new list
        if(specialQList == null)
        {
          specialQList = new LinkedList();
        }
        else
        {
          //check to see if this has already been built/added
          //if so, ignore this as we need to retain any existing data
          for(Iterator it = specialQList.iterator(); it.hasNext();)
          {
            q2 =(QuizQuestion)it.next();
            if(q.getId() == q2.getId())
            {
              log.debug("This SA already exists... keeping same build");

              Field f;
              //have to add the existing fields back in though
              for(int i = 0; i < ccuSuf.length; i++)
              {
                f = getField(q.getFieldName()+ccuSuf[i]);
                if(f!=null)
                {
                  specialActionFields.add(f);
                }
              }

              alreadyExists = true;

              break;
            }
          }
        }

        if(!alreadyExists)
        {
          //add the question to the new list
          boolean added = false;

          for(int i=0;i<specialQList.size();i++)
          {
            //try to make sure they're in order,
            //since this list drives display
            q2 = (QuizQuestion)specialQList.get(i);

            if(q.getId() > q2.getId())
            {
              continue;
            }
            else
            {
              specialQList.add(i,q);
              added = true;
              break;
            }
          }

          if(!added)
          {
            specialQList.add(q);
          }

          log.debug("SA built --> "+q.getFieldName());

          //build new fields for compensating controls
          Field newField;

          newField  = new TextareaField(q.getFieldName()+"_constraint",500,2,80,false);
          specialActionFields.add(newField);

          newField  = new TextareaField(q.getFieldName()+"_objective",500,2,80,false);
          specialActionFields.add(newField);

          newField  = new TextareaField(q.getFieldName()+"_risk",500,2,80,false);
          specialActionFields.add(newField);

          newField  = new TextareaField(q.getFieldName()+"_def",500,2,80,false);
          specialActionFields.add(newField);

          newField  = new TextareaField(q.getFieldName()+"_validation",500,2,80,false);
          specialActionFields.add(newField);

          newField  = new TextareaField(q.getFieldName()+"_maintenance",500,2,80,false);
          specialActionFields.add(newField);


        }
      }
      else
      {
        //check the special Q list to ensure that this item isn't there
        //if it is, remove it as the status of the CCU has obviously changed
        if(specialQList != null)
        {
          for(Iterator it = specialQList.iterator(); it.hasNext();)
          {
            q2 = (QuizQuestion)it.next();
            if(q2.getId()==q.getId())
            {
              log.debug("found an invalid CCU question... removing");

              //remove question from list
              it.remove();

              //have to remove the fields as well
              for(int i = 0; i < ccuSuf.length; i++)
              {
                fields.deleteField(q.getFieldName()+ccuSuf[i]);
              }
              break;
            }
          }

          //if we've removed the last of the questions from this list...
          if(specialQList.size() < 1)
          {
            specialQList = null;
            specialActionFieldsBuilt = false;
          }
        }
      }//end if "CCU"
    }
  }

  protected void updateSpecialValidation(boolean makeRequired)
  {

    log.debug("updateSpecialValidation() setting to: "+makeRequired);

    if(specialQList!= null)
    {
      QuizQuestion q;
      Field f;

      for(Iterator it = specialQList.iterator(); it.hasNext();)
      {
        //look at each question in the list
        q = (QuizQuestion)it.next();

        for(int i = 0; i < ccuSuf.length; i++)
        {
          //check for each of the six fields
          f = getField(q.getFieldName()+ccuSuf[i]);

          //if they're found (should be), set appropriate validation
          if(f != null)
          {
            if(makeRequired)
            {
              f.makeRequired();
            }
            else
            {
              f.makeOptional();
            }
          }

        }

      }

    }
  }

  public void moveToErrorPage()
  {
    currentPage = 99;
  }

  public String getTitle()
  {
    return title;
  }

  //GET/SET
  public List getComponents()
  {
    return components;
  }


  public void setCurrentPage(int pgNum)
  {
    log.debug("TODO");
  }

  public int getCurrentPage()
  {
    return currentPage;
  }

  protected void setCurrentComponent(int num)
  {
    //only grab component if we know it exists
    if( num > -1 && num < components.size())
    {
      currentComponent = (QuizComponent)components.get(num);
    }
    else
    {
      currentComponent = null;
    }
  }

  public QuizComponent getCurrentComponent()
  {
    return currentComponent;
  }

  public String getLevel()
  {
    if(trackingNumber != null)
    {
      return trackingNumber.getPCILevel();
    }
    return "";
  }

  public PCIInfoBean getInfoBean()
  {
    return pciBean;
  }

  public String getTrackingNumber()
  {
    if(trackingNumber != null)
    {
      return trackingNumber.getTrackingNumber();
    }
    return "";
  }


  public int decodeLevel(String level)
  {

    int decodeLevel = 0;

    //always use the tracking number if possible
    //even if it means ignoring the param
    if(trackingNumber != null)
    {
      level = trackingNumber.getPCILevel();
    }
    else if(level == null)
    {
      level = "";
    }

    //convert the String into its int equivalent
    if(level.equals(LEVEL_A))
    {
      decodeLevel = 1;
    }
    else if(level.equals(LEVEL_B))
    {
      decodeLevel = 2;
    }
    else if(level.equals(LEVEL_C))
    {
      decodeLevel = 3;
    }
    else if(level.equals(LEVEL_D))
    {
      decodeLevel = 4;
    }

    return decodeLevel;
  }

  public int decodeLevel()
  {
    return decodeLevel(null);
  }

  public long getVolume()
  {
    return volume;
  }

  public String getVolumeStr()
  {
    return NumberFormatter.getDoubleString(volume, NumberFormatter.WEB_CURRENCY_FORMAT);
  }

  public long getTransactionCount()
  {
    return transactionCount;
  }

  public int getMerchLevel()
  {
    return merchLevel;
  }

  public double getCNPTransPercentage()
  {
    return cnpTransPercentage;
  }

  public String getCNPTransPercentageStr()
  {
    return NumberFormatter.getPercentString(cnpTransPercentage);
  }

  public void setMerchantVariables()
  {
      log.debug("setMerchantVariables()...");
    //query #1 - how many visa transactions performed by merchant
    //visaCount > 6,000,000 = Level 1
    //1,000,000 <= visaCount <= 6,000,000 = Level 2

    //query #2 - how many e-commerce transactions performed by merchant
    //ecommerceCount >= 20,000 = Level 3
    //everything else = Level 4

    //query #3 - annualized sales volume and perecentage card-not-present

    try
    {
      long count = 0;

      connect();

      //Set Merchant Level
      /*@lineinfo:generated-code*//*@lineinfo:2050^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(sm.vmc_sales_count),0)
//        
//        from    monthly_extract_summary sm
//        where   sm.merchant_number = :merchNum and
//                sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(sm.vmc_sales_count),0)\n       \n      from    monthly_extract_summary sm\n      where   sm.merchant_number =  :1  and\n              sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2057^7*/

      if(count > 6000000L)
      {
        merchLevel = 1;
      }
      else if(6000000L >= count && count >= 1000000L)
      {
        merchLevel = 2;
      }
      else
      {
        //check only ecommerce transactions
        /*@lineinfo:generated-code*//*@lineinfo:2070^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(sm.vmc_sales_count),0)
//            
//            from    monthly_extract_summary   sm,
//                    merch_pos mp,
//                    pos_category pc,
//                    merchant m
//            where   sm.merchant_number = :merchNum and
//                    sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month') and
//                    sm.merchant_number = m.merch_number and
//                    m.app_seq_num = mp.app_seq_num and
//                    mp.pos_code = pc.pos_code and
//                    pc.pos_type not in (1, 4) --everything else could be considered e-commerce
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(sm.vmc_sales_count),0)\n           \n          from    monthly_extract_summary   sm,\n                  merch_pos mp,\n                  pos_category pc,\n                  merchant m\n          where   sm.merchant_number =  :1  and\n                  sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month') and\n                  sm.merchant_number = m.merch_number and\n                  m.app_seq_num = mp.app_seq_num and\n                  mp.pos_code = pc.pos_code and\n                  pc.pos_type not in (1, 4) --everything else could be considered e-commerce";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2084^9*/

        if(count < 20000)
        {
         merchLevel = 4;
        }
        else
        {
         merchLevel = 3;
        }
      }

      ResultSet rs          = null;
      ResultSetIterator it  = null;

      //Set volume and card not present transaction percentage
      /*@lineinfo:generated-code*//*@lineinfo:2100^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(dt.transaction_amount)                vol,
//                  count(dt.pos_entry_mode)                  count,
//                  round(sum(decode(dt.pos_entry_mode,
//                    '01', 1,
//                    '81', 1,
//                    0)) / count(dt.pos_entry_mode), 4)  percentage
//          from    daily_detail_file_ext_dt dt
//          where   dt.merchant_number = :merchNum and
//                  dt.batch_date between trunc(sysdate-365, 'month')
//                    and trunc(trunc(sysdate, 'month')-1,'month')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum(dt.transaction_amount)                vol,\n                count(dt.pos_entry_mode)                  count,\n                round(sum(decode(dt.pos_entry_mode,\n                  '01', 1,\n                  '81', 1,\n                  0)) / count(dt.pos_entry_mode), 4)  percentage\n        from    daily_detail_file_ext_dt dt\n        where   dt.merchant_number =  :1  and\n                dt.batch_date between trunc(sysdate-365, 'month')\n                  and trunc(trunc(sysdate, 'month')-1,'month')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.reports.quiz.pci.PCIQuestionnaire",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2112^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        volume              = rs.getLong("vol");
        transactionCount    = rs.getLong("count");
        cnpTransPercentage  = rs.getDouble("percentage");
      }

    }
    catch (Exception e)
    {
      //e.printStackTrace();
      //leave them as is
    }
    finally
    {
      cleanUp();
    }
  }

  public boolean isValid()
  {
    return isValid;
  }

  public String getValidationDetails()
  {
    return validationDetails;
  }

  public void setValidationDetails(String status)
  {
    validationDetails = status;
  }

  protected void updateValidationStatus(String status)
  {

    setValidationDetails(status);
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2158^7*/

//  ************************************************************
//  #sql [Ctx] { update quiz_history
//          set result = :status
//          where tracking_number = :getTrackingNumber()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1418 = getTrackingNumber();
   String theSqlTS = "update quiz_history\n        set result =  :1 \n        where tracking_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,status);
   __sJT_st.setString(2,__sJT_1418);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2163^7*/
    }
    catch(Exception e)
    {
    }
    finally
    {
      cleanUp();
    }
  }


  //TODO
  //Only need 3 states... none(default), incomplete(date), complete(date)
  protected boolean setStatus()
  {
    log.debug("calling setStatus...");
    boolean exists = false;

    //set the validation details for display
    //use the merchNum to check the records
    log.debug("merchNum = "+merchNum);

    if(merchNum > 0)
    {
      try
      {
        connect();

        int count = 0;

        /*@lineinfo:generated-code*//*@lineinfo:2194^9*/

//  ************************************************************
//  #sql [Ctx] { select count(tracking_number)
//              
//            from
//              quiz_history
//            where
//              merch_number = :merchNum and
//              result != :STATUS_RESET and
//              date_taken > (sysdate - 350)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(tracking_number)\n             \n          from\n            quiz_history\n          where\n            merch_number =  :1  and\n            result !=  :2  and\n            date_taken > (sysdate - 350)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setString(2,STATUS_RESET);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2204^9*/

        if(count>0)
        {
          ResultSet rs          = null;
          ResultSetIterator it  = null;

          /*@lineinfo:generated-code*//*@lineinfo:2211^11*/

//  ************************************************************
//  #sql [Ctx] it = { select
//                quiz_id,
//                tracking_number,
//                date_taken,
//                result,
//                username
//              from
//                quiz_history
//              where
//                merch_number = :merchNum and
//                result != :STATUS_RESET and
//                date_taken > (sysdate - 350)
//              order by date_taken desc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n              quiz_id,\n              tracking_number,\n              date_taken,\n              result,\n              username\n            from\n              quiz_history\n            where\n              merch_number =  :1  and\n              result !=  :2  and\n              date_taken > (sysdate - 350)\n            order by date_taken desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.reports.quiz.pci.PCIQuestionnaire",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setString(2,STATUS_RESET);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"25com.mes.reports.quiz.pci.PCIQuestionnaire",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2226^11*/

          rs = it.getResultSet();

          //take first one, if there
          if(rs.next())
          {
            //if the last entry was a MES internal user,
            //consider -1 the same as no previous quiz
            //consider 0 as an alteration on an exisiting questionnaire
            if(rs.getInt("quiz_id") > -1)
            {
              log.debug("found tracking number in db..." + rs.getString("tracking_number"));
              validationDetails = rs.getString("result");
              trackingNumber = new TrackingNumber(rs.getString("tracking_number"));
              setDateTaken(DateTimeFormatter.getFormattedDate(rs.getDate("date_taken"), "MM/dd/yyyy"));
              setUserName(rs.getString("username"));
              log.debug("date taken: "+getDateTakenStr());
              exists = true;

            }
            else
            {
              validationDetails = STATUS_INCOMPLETE;
            }
          }

        }
        else
        {
          validationDetails = STATUS_NONE;
        }
      }
      catch(Exception e)
      {
        e.printStackTrace();
        //logEntry("PCIQuestionnaire::setStatus()",e.toString());
      }
      finally
      {
        cleanUp();
      }

    }

    return exists;
  }

  //basically a utility class to centralize data harvest from track number
  //format is "PCI"+quizType(A,B,C,D)+leaders+left padded historyId = 14
  public class TrackingNumber
  {
    protected String trackNum;
    protected long historyId;
    protected String level;

    public TrackingNumber(long historyId, String level)
    {
      this.historyId = historyId;
      this.level = level;

      StringBuffer sb = new StringBuffer();
      try
      {
        for(int i=0; i < 10; ++i)
        {
          sb.append("0");
        }

        DecimalFormat df = new DecimalFormat(sb.toString());

        //reset
        sb.delete(0, sb.length());
        sb.append("PCI").append(level.toUpperCase()).append(df.format(historyId));
      }
      catch(Exception e)
      {
        e.printStackTrace();
        sb.delete(0, sb.length());
        sb.append("PCI").append(level).append(historyId);
      }
      trackNum = sb.toString();
    }

    public TrackingNumber(String trackingNumber)
    {
      this.trackNum = trackingNumber;
      setPCILevel();
      setHistoryId();
    }

    public String getTrackingNumber()
    {
      return trackNum;
    }

    public String getPCILevel()
    {
      return level;
    }

    public long getHistoryId()
    {
      return historyId;
    }

    protected void setHistoryId()
    {
      try
      {
        historyId = Long.parseLong(trackNum.substring(4));
      }
      catch(Exception e)
      {
        historyId = 0;
      }
    }

    protected void setPCILevel()
    {
      this.level = trackNum.substring(3,4);
    }

    public boolean isLevel(String level)
    {
      if(level != null && getPCILevel().equalsIgnoreCase(level))
      {
        return true;
      }
      return false;
    }

  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }

  public String getUserName()
  {
    return userName;
  }
}/*@lineinfo:generated-code*/