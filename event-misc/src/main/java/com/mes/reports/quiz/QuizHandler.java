/*@lineinfo:filename=QuizHandler*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.reports.quiz;

import java.sql.ResultSet;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.reports.quiz.pci.PCIQuestionnaire;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class QuizHandler extends SQLJConnectionBase
{

  // create class log category
  static Logger log = Logger.getLogger(QuizHandler.class);

  public static Quiz build(String id, UserBean user)
  throws Exception
  {

    QuizHandler qh = new QuizHandler();
    return qh.buildQuiz(id, user);

  }

  public Quiz buildQuiz(String id, UserBean user)
  throws Exception
  {

    Quiz quiz = null;

    int qid = Integer.parseInt(id);

    //Add future quizzes here
    switch(qid)
    {
      case mesConstants.QUIZ_PCI_QUESTIONNAIRE:
        quiz = new com.mes.reports.quiz.pci.PCIQuestionnaire(user);
        break;

      case mesConstants.QUIZ_PCI_QUESTIONNAIRE_V2:
        quiz = new com.mes.reports.quiz.pci.PCIQuestionnaire_v2(user);
        break;

      default:
        throw new Exception("Quiz type not defined.");
    }

     return quiz;
  }

  public static Quiz buildPCIQuiz(String merchNum)
  throws Exception
  {

    QuizHandler qh = new QuizHandler();
    return qh._buildPCIQuiz(merchNum);

  }

  private Quiz _buildPCIQuiz(String merchNum)
  throws Exception
  {

    Quiz quiz = null;

    int qid = 1;

    connect();

    ResultSet rs          = null;
    ResultSetIterator it  = null;

    /*@lineinfo:generated-code*//*@lineinfo:81^5*/

//  ************************************************************
//  #sql [Ctx] it = { select
//          decode(quiz_id, 5, 2, 6, 2, 7, 2, 8, 2, 1)  q_type
//        from
//          quiz_history
//        where
//          merch_number = :merchNum and
//          result != :PCIQuestionnaire.STATUS_RESET and
//          date_taken > (sysdate - 350)
//        order by date_taken desc
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n        decode(quiz_id, 5, 2, 6, 2, 7, 2, 8, 2, 1)  q_type\n      from\n        quiz_history\n      where\n        merch_number =  :1  and\n        result !=  :2  and\n        date_taken > (sysdate - 350)\n      order by date_taken desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.quiz.QuizHandler",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   __sJT_st.setString(2,PCIQuestionnaire.STATUS_RESET);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.quiz.QuizHandler",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:92^5*/

    rs = it.getResultSet();

    //take first one, if there
    if(rs.next())
    {
      qid = rs.getInt("q_type");
    }

    cleanUp();

    switch(qid)
    {
      case mesConstants.QUIZ_PCI_QUESTIONNAIRE:
        quiz = new com.mes.reports.quiz.pci.PCIQuestionnaire(merchNum);
        break;

      case mesConstants.QUIZ_PCI_QUESTIONNAIRE_V2:
      default:
        quiz = new com.mes.reports.quiz.pci.PCIQuestionnaire_v2(merchNum);
        break;
    }

     return quiz;
  }


  public static boolean validateUser(UserBean user, Quiz quiz)
  {
    QuizHandler qh = new QuizHandler();
    return qh._validateUser(user, quiz);
  }

  public static boolean validateQuizStatus(UserBean user, Quiz quiz)
  {
    QuizHandler qh = new QuizHandler();
    return qh._validateQuizStatus(user, quiz);
  }

  //currently this is coded for only the PCIQuestionnaire...
  protected boolean _validateUser(UserBean user, Quiz quiz)
  {
    //DEV
    return true;
/*
    //if quiz hasn't already been validated...
    if(!quiz.isValid())
    {
      log.debug("validating user...");
      //PROD
      int qId           = quiz.getId();
      long merchNum     = user.getMerchId();
      String loginName  = user.getLoginName();

      switch(qId)
      {

        case mesConstants.QUIZ_PCI_QUESTIONNAIRE:

          //check to see if user is merchant-level for this merchant
          try
          {
            connect();
            int count = 0;

            #sql [Ctx]
            {
              select count(1) into :count
              from users u, mif mf
              where u.login_name = :loginName and
              mf.merchant_number = u.hierarchy_node and
              mf.merchant_number = :merchNum
            };

            if(count != 1)
            {
              quiz.addError("Unable to access test: Not a valid Merchant Level User.");
            }
            else
            {
              if(user.hasRight(MesUsers.RIGHT_PCI_QUIZ_VIEW) ||
                 user.hasRight(MesUsers.RIGHT_PCI_QUIZ_SUBMIT))
              {
                quiz.setValid(true);
              }
              else
              {
                quiz.addError("Unable to access test: Not a valid Merchant Level User.");
              }
            }
          }
          catch (Exception e)
          {
            quiz.addError("Unable to take PCI Questionnaire: Please contant your Customer Service Representative.");
          }
          break;

        default:

      }//end switch
    }//end if

   if(quiz.hasErrors())
   {
     for (Iterator it = quiz.getErrors().iterator(); it.hasNext();)
     {
       log.debug("quiz error = "+ (String)it.next());
     }
   }

   return quiz.isValid();
   */
  }

  protected boolean _validateQuizStatus(UserBean user, Quiz quiz)
  {
    int qId           = quiz.getId();
    long merchNum     = user.getMerchId();
    String loginName  = user.getLoginName();

    boolean isValid   = false;
    ResultSet rs      = null;

    try
    {
      rs = getQuizHistoryDetails(qId, merchNum);

      int count = 0;

      while(rs.next())
      {
        //everyone can take test a year after, regardless
        if(rs.getInt("delay")<365)
        {
          //check the count
          if(++count > rs.getInt("max"))
          {
            quiz.addError("Unable to take test:<br>Maximum attempts reached.");
            break;
          }

          //check the result - disable if already passed
          if(rs.getString("result").equals("PASS"))
          {
            quiz.addError("Not necessary to take test:<br>Test already marked a PASS.");
            break;
          }

          //check first record and its delay
          int dif = rs.getInt("delay_required") - rs.getInt("delay");
          if( count == 1 && dif > 0)
          {
            quiz.addError("Unable to take test:<br>Mandatory delay in effect: "+dif+" days until quiz is reopened.");
            break;
          }
        }
      }
    }
    catch(Exception e)
    {
      quiz.addError("Unable to take test:<br>System currently down.");
    }

    return isValid;
  }


  public ResultSet getQuizHistoryDetails(int qId, long merchNum)
    throws Exception
  {
    ResultSetIterator it = null;

    /*@lineinfo:generated-code*//*@lineinfo:265^5*/

//  ************************************************************
//  #sql [Ctx] it = { select
//          trunc(qh.date_taken)                          as date_taken,
//          round(trunc(sysdate) - trunc(qh.date_taken))  as time_since_last_quiz,
//          qh.result                                     as result,
//          q.max_attempts                                as max,
//          q.delay_required_days                         as delay_required
//        from
//          mes.quiz q,
//          mes.quiz_history qh
//        where
//          q.id  = :qId and
//          qh.quiz_id = q.id and
//          qh.merch_number = :merchNum and
//          round(trunc(sysdate) - trunc(qh.date_taken)) < 365
//        order by
//          qh.date_taken desc
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n        trunc(qh.date_taken)                          as date_taken,\n        round(trunc(sysdate) - trunc(qh.date_taken))  as time_since_last_quiz,\n        qh.result                                     as result,\n        q.max_attempts                                as max,\n        q.delay_required_days                         as delay_required\n      from\n        mes.quiz q,\n        mes.quiz_history qh\n      where\n        q.id  =  :1  and\n        qh.quiz_id = q.id and\n        qh.merch_number =  :2  and\n        round(trunc(sysdate) - trunc(qh.date_taken)) < 365\n      order by\n        qh.date_taken desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.quiz.QuizHandler",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,qId);
   __sJT_st.setLong(2,merchNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.quiz.QuizHandler",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:283^5*/

    return it.getResultSet();
  }


}/*@lineinfo:generated-code*/