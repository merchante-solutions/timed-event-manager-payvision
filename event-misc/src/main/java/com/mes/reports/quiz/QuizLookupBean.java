/*@lineinfo:filename=QuizLookupBean*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.reports.quiz;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.LinkedList;
import javax.servlet.http.HttpServletRequest;
// log4j
import org.apache.log4j.Logger;
import com.mes.forms.ComboDateField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import sqlj.runtime.ResultSetIterator;

public class QuizLookupBean extends FieldBean
{

  private LinkedList results = new LinkedList();

  // create class log category
  static Logger log = Logger.getLogger(QuizLookupBean.class);

  public QuizLookupBean()
  {
    super();
    fields.add(new ComboDateField("fromDate", true));
    ComboDateField field = new ComboDateField("toDate", true);
    field.addDays(1);
    fields.add(field);
    fields.add(new Field("param",40,20, true));
  }


  public synchronized void load(HttpServletRequest request)
  {
    log.debug("loading search results...");

    //empty the list
    results.clear();

    //set field information
    setFields(request);

    String            stringLookup        = "";
    long              longLookup          = -1;
    java.sql.Date     fromDateSQL         = ((ComboDateField)fields.getField("fromDate")).getSqlDate();
    java.sql.Date     toDateSQL           = ((ComboDateField)fields.getField("toDate")).getSqlDate();

    //set the long search value
    String param = fields.getField("param").getData().trim();

    try
    {
      longLookup = Long.parseLong(param);
    }
    catch (Exception e)
    {
      longLookup = -1;
    }

    //set the String search value
    if(param.equals(""))
    {
      stringLookup = "passall";
    }
    else
    {
      stringLookup = "%" + param.toUpperCase() + "%";
    }

    ResultSet rs          = null;
    ResultSetIterator it  = null;

    log.debug("stringLookup = "+stringLookup);
    log.debug("longLookup = "+longLookup);
    log.debug("fromDateSQL = "+fromDateSQL);
    log.debug("toDateSQL = "+toDateSQL);

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:84^7*/

//  ************************************************************
//  #sql [Ctx] it = { select
//            qh.id             as history_id,
//            qh.tracking_number as track_num,
//            qh.quiz_id        as quiz_id,
//            qh.merch_number   as merch_num,
//            qh.date_taken     as date_taken,
//            nvl(qh.result,'') as result,
//            nvl(mf.dba_name, nvl(m.merch_business_name,' ')) as merch_name
//          from
//            t_hierarchy th,
//            mif mf,
//            quiz q,
//            quiz_history qh,
//            merchant m
//          where
//            th.ancestor = :user.getHierarchyNode() and
//            th.descendent = mf.association_node and
//            mf.merchant_number = qh.merch_number and
//            trunc(qh.date_taken) between :fromDateSQL and :toDateSQL
//            and
//            (
//              qh.merch_number       = :longLookup or
//              qh.id                 = :longLookup or
//              upper(m.merch_business_name) like :stringLookup or
//              upper(mf.dba_name) like :stringLookup or
//              upper(q.name) like :stringLookup or
//              substr(qh.tracking_number,4,1) = :stringLookup or
//              m.risk_pci_level      = :longLookup or
//              'passall' = :stringLookup
//            )
//            and
//            qh.quiz_id = q.id and
//            qh.merch_number = m.merch_number(+)
//          order by
//            qh.date_taken desc
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1332 = user.getHierarchyNode();
  try {
   String theSqlTS = "select\n          qh.id             as history_id,\n          qh.tracking_number as track_num,\n          qh.quiz_id        as quiz_id,\n          qh.merch_number   as merch_num,\n          qh.date_taken     as date_taken,\n          nvl(qh.result,'') as result,\n          nvl(mf.dba_name, nvl(m.merch_business_name,' ')) as merch_name\n        from\n          t_hierarchy th,\n          mif mf,\n          quiz q,\n          quiz_history qh,\n          merchant m\n        where\n          th.ancestor =  :1  and\n          th.descendent = mf.association_node and\n          mf.merchant_number = qh.merch_number and\n          trunc(qh.date_taken) between  :2  and  :3 \n          and\n          (\n            qh.merch_number       =  :4  or\n            qh.id                 =  :5  or\n            upper(m.merch_business_name) like  :6  or\n            upper(mf.dba_name) like  :7  or\n            upper(q.name) like  :8  or\n            substr(qh.tracking_number,4,1) =  :9  or\n            m.risk_pci_level      =  :10  or\n            'passall' =  :11 \n          )\n          and\n          qh.quiz_id = q.id and\n          qh.merch_number = m.merch_number(+)\n        order by\n          qh.date_taken desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.quiz.QuizLookupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1332);
   __sJT_st.setDate(2,fromDateSQL);
   __sJT_st.setDate(3,toDateSQL);
   __sJT_st.setLong(4,longLookup);
   __sJT_st.setLong(5,longLookup);
   __sJT_st.setString(6,stringLookup);
   __sJT_st.setString(7,stringLookup);
   __sJT_st.setString(8,stringLookup);
   __sJT_st.setString(9,stringLookup);
   __sJT_st.setLong(10,longLookup);
   __sJT_st.setString(11,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.quiz.QuizLookupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:122^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        log.debug("creating QuizData...");
        log.debug("quiz_id = "+rs.getInt("quiz_id"));

        results.add(new QuizData(
                      rs.getString("merch_name"),
                      rs.getLong("merch_num"),
                      rs.getInt("quiz_id"),
                      rs.getLong("history_id"),
                      rs.getString("track_num"),
                      rs.getString("date_taken"),
                      rs.getString("result")
                      )
                    );
      }
    }
    catch (Exception e)
    {
      //TODO
      e.printStackTrace();
    }
    finally
    {
      cleanUp();
    }

  }

  public Iterator getResults()
  {
    return results.iterator();
  }

  public boolean hasResults()
  {
    return results != null && results.size()>0;
  }


  //local class for data bucket
  public class QuizData
  {
    private String merchName;
    private long   merchNum;
    private long   historyId;
    private int    quizId;
    private String dateTaken;
    private String trackNum;
    private String result;

    public QuizData(  String merchName,
                      long merchNum,
                      int quizId,
                      long historyId,
                      String trackNum,
                      String dateTaken,
                      String result)
    {
      this.merchName   = merchName;
      this.merchNum    = merchNum;
      this.quizId      = quizId;
      this.historyId   = historyId;
      this.trackNum    = trackNum;
      this.dateTaken   = dateTaken;
      this.result      = result;
    }

    public String getMerchName ()
    {
      return merchName;
    }

    public String getDateTaken ()
    {
      return dateTaken;
    }

    public String getResult ()
    {
      return result;
    }

    public String getTrackNum ()
    {
      return trackNum;
    }

    public long getMerchNum ()
    {
      return merchNum;
    }

    public long getHistoryId ()
    {
      return historyId;
    }

    public int getQuizId ()
    {
      return quizId;
    }

    public int getQuizType()
    {
      int type = 1;
      if(quizId > 4 && quizId < 9)
      {

        type = 2;
      }

      log.debug("getQuizType = "+type);

      return type;
    }

  }

}/*@lineinfo:generated-code*/