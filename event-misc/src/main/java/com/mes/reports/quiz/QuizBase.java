package com.mes.reports.quiz;

import java.io.Serializable;
import com.mes.forms.FieldGroup;

public class QuizBase implements Serializable
{

  protected long id;
  protected FieldGroup fields;

  public QuizBase(long id)
  {
    this.id = id;
  }

  public long getId()
  {
    return id;
  }

}