/*@lineinfo:filename=PCIQuestionnaire_v2*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.reports.quiz.pci;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.TextareaField;
import com.mes.reports.quiz.Quiz;
import com.mes.reports.quiz.QuizComponent;
import com.mes.reports.quiz.QuizQuestion;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class PCIQuestionnaire_v2 extends PCIQuestionnaire implements Serializable
{
  // create class log category
  static Logger log = Logger.getLogger(PCIQuestionnaire_v2.class);

  public final static String REASON_TAG = "_nareason";

  public static final String[][] YesNoCcuNa =
  {
    { "Yes",         "Y"},
    { "No",          "N"},
    { "Special",     "CCU"},
    { "N/A",         "NA"},
  };

  public final static String[] naSuf = { REASON_TAG };

  protected boolean specialNAFieldsBuilt  = false;
  protected boolean specialNAFieldsLoaded = false;
  protected FieldGroup specialNAFields    = null;
  protected LinkedList specialNAList      = null;


  public PCIQuestionnaire_v2() {}

  public PCIQuestionnaire_v2(UserBean user)
  {
    super(user);
  }

  public PCIQuestionnaire_v2(String merchNum)
  {
    super(merchNum);
  }

  //set ID to new version quiz
  protected void setId()
  {
    setId(mesConstants.QUIZ_PCI_QUESTIONNAIRE_V2);
  }

  //need to overwrite the decodeLevel, since we now tie ids 5-8 into the levels A-D
  public int decodeLevel(String level)
  {

    log.debug("new decode method");

    int decodeLevel = 0;

    //always use the tracking number if possible
    //even if it means ignoring the param
    if(trackingNumber != null)
    {
      level = trackingNumber.getPCILevel();
    }
    else if(level == null)
    {
      level = "";
    }

    //convert the String into its int equivalent
    if(level.equals(LEVEL_A))
    {
      decodeLevel = 5;
    }
    else if(level.equals(LEVEL_B))
    {
      decodeLevel = 6;
    }
    else if(level.equals(LEVEL_C))
    {
      decodeLevel = 7;
    }
    else if(level.equals(LEVEL_D))
    {
      decodeLevel = 8;
    }

    return decodeLevel;
  }

  //all PCI levels, all questions now have this answer format,
  //regardless of qType
  protected String[][] determineAnswerFormat(QuizQuestion q)
  {
    log.debug("calling new format YesNoCcuNa");
    return YesNoCcuNa;
  }

  protected boolean levelDCCUNeeded()
  {

    //basically loop through all fields looking for a CCU or NA
    Field field;
    for(Iterator it = fields.getFieldsVector().iterator(); it.hasNext();)
    {
      log.debug("looking for CCU or NA...");

      field = (Field)it.next();
      if(field != null && ( field.getData().equals("CCU") || field.getData().equals("NA")) )
      {
        //get out of here as soon as you
        //find the first special field selected
        log.debug("CCU or NA FOUND!");
        return true;
      }
    }

    //default
    return false;
  }

  public void processAction(HttpServletRequest request, StringBuffer queryString)
  {
    String action = request.getParameter(Quiz.QUIZ_ACTION_TAG);
    messenger = new StringBuffer();

    log.debug("action = "+ action);

    //Have to alter how page up and down work due to expanded answering validation
    if(action != null && ( action.equals(Quiz.ACTION_PAGE_MOVE_DOWN) ||
                           action.equals(Quiz.ACTION_PAGE_MOVE_UP)
                         )
      )
    {

      if(action.equals(Quiz.ACTION_PAGE_MOVE_UP))
      {
        //always set the field data
        setFields(request);

        if(validate())
        {

          if(currentPage == 6)
          {
            //this syncs the merchant table to the pci tables
            //immediately after they submit the first attestation page
            //but only if this quiz hasn't been access before (new)
            if(!isValid)
            {
              updateMerchantStatus();
            }
          }

          boolean saveOK = true;

          //save on every advance of page, once at quiz start
          if(currentPage > 5 && currentPage < 16)
          {
            try
            {
              save();
            }
            catch (Exception e)
            {
              messenger.append("There was an problem saving your data. Please try again.<br>If the problem persists, please contact you Customer Service representative.");
              saveOK = false;
            }
          }

          if(saveOK)
          {
            //advance the page
            currentPage++;

            //if we're valid, and have moved past page 14, then just go there.
            if(currentPage > 14)
            {
              return;
            }

            //check status of page count, if they're beyond the last page
            //have to check for CCU and NA answers
            if(currentPage > lastPageOfQuiz)
            {

              //need to check for CCU or NA
              if( levelDCCUNeeded() )
              {
                log.debug("special action needed == true");

                //load new and/or existing special fields
                loadSpecial(true);
                updateSpecialValidation(true);

                //need to be on page 14, specialaction.jsp
                currentPage = 14;

              }
              else
              {
                //CCU/NA level not needed, so set this to null
                //in case there were existing elements that
                //were changed during the quiz processs
                log.debug("special action needed == false");

                specialQList = null;
                specialActionFieldsBuilt = false;

                specialNAList = null;
                specialNAFieldsBuilt = false;

                //go to 15 to hit compliance2 page
                currentPage = 15;
              }
            }
          }//end saveOK
        }
        else
        {
          messenger.append("Please correct errors as indicated ( <font color='red'>!</font> ) below.");
        }
      }
      else if(action.equals(Quiz.ACTION_PAGE_MOVE_DOWN))
      {
        if(currentPage == 5)
        {
          messenger.append("Unable to navigate back to this page.<br>Please use the reset button to start the process again.<br>");
        }
        else
        {
          //always set the field data, in case they were filling it
          //out before the wanted to back step
          setFields(request);

          currentPage--;

          //need to do this to avoid first component/compliance validation issue
          //we've stepped off the first page of questions back to the compliance
          //so we have to make sure we don't try to validate the loaded component
          //in the event they haven't completed it
          if(currentPage == 6)
          {
            currentComponent = null;
          }

          //this scenario happens when you are in Compensating Controls/NA
          //and have decided to go back to change answers
          if (currentPage == 13)
          {
            //need to remove validation on the special CCU fields
            //this will be re-established upon forward movement
            updateSpecialValidation(false);
          }

          if (currentPage == 13 || currentPage == 14)
          {
            if(currentPage == 14 && (specialNAFieldsBuilt || specialActionFieldsBuilt) )
            {
              //leave page num as is
            }
            else
            {
              //drop pages to reflect the jump back, depending on quiz type
              currentPage = lastPageOfQuiz;
            }
          }
        }
      }
    }
    else
    {
      super.processAction(request, queryString);
    }
  }

  public void loadSpecial(boolean tryLoad)
  {
    log.debug("calling loadSpecial");
    super.loadSpecial(tryLoad);

    loadNA(tryLoad);
  }

  protected void updateSpecialValidation(boolean makeRequiredFlag)
  {
    log.debug("calling updateSpecialValidation");

    //call update on the CCU (original)
    super.updateSpecialValidation(makeRequiredFlag);

    //call update on the NA fields
    updateNAValidation(makeRequiredFlag);
  }

  protected void updateNAValidation(boolean makeRequiredFlag)
  {
    log.debug("calling updateNAValidation");

    if(specialNAList!= null)
    {
      QuizQuestion q;
      Field f;

      for(Iterator it = specialNAList.iterator(); it.hasNext();)
      {
        //look at each question in the list
        q = (QuizQuestion)it.next();

        log.debug(q.getFieldName()+REASON_TAG);

        f = getField(q.getFieldName()+REASON_TAG);

        if(f != null)
        {
          if(makeRequiredFlag)
          {
            f.makeRequired();
          }
          else
          {
            f.makeOptional();
          }
        }

        /*
        log.debug("before");
        log.debug(""+naSuf.length);
        log.debug("after");
        for(int i = 0; i < naSuf.length; i++)
        {

          log.debug(q.getFieldName()==null?" getFieldName is null":q.getFieldName());
          log.debug(naSuf[i]==null?" naSuf is null":naSuf[i]);

          //check for each of the six fields
          f = getField(q.getFieldName()+naSuf[i]);

          log.debug(f==null?"null":"not null");

          //if they're found (should be), set appropriate validation
          if(f != null)
          {
            if(makeRequiredFlag)
            {
              f.makeRequired();
            }
            else
            {
              f.makeOptional();
            }
          }

        }
        */
      }
    }
  }

  protected void loadNA(boolean tryLoad)
  {
    log.debug("calling loadNA");
    //only look for the special actions if needed
    if(tryLoad)
    {
      ResultSetIterator it = null;
      ResultSet rs         = null;

      //OK, start by building newest data...
      //this reflects current question answers
      buildNAValidationFields();

      //first time in
      //we need to determine if there was existing data
      //if so, we need to match the fields with the data...
      //at the end, we need to delete anything that exists, but
      //wasn't found in the recent answers
      if(!specialNAFieldsLoaded)
      {
        try
        {
          connect();

           //first look for entries, quick and dirty
          int count = 0;

          /*@lineinfo:generated-code*//*@lineinfo:401^11*/

//  ************************************************************
//  #sql [Ctx] { select count(q_id)
//              
//              from pci_na_reason
//              where tracking_number = :getTrackingNumber()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1419 = getTrackingNumber();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(q_id)\n             \n            from pci_na_reason\n            where tracking_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.quiz.pci.PCIQuestionnaire_v2",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_1419);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:407^11*/

          //if you find any...
          if(count > 0)
          {
            //load from pci_special_actions
            /*@lineinfo:generated-code*//*@lineinfo:413^13*/

//  ************************************************************
//  #sql [Ctx] it = { select
//                  q_id,
//                  'q_'||q_id as prefix,
//                  nvl(reason,'')  as reason
//                from
//                  pci_na_reason
//                where
//                  tracking_number = :getTrackingNumber()
//                order by q_id asc
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1420 = getTrackingNumber();
  try {
   String theSqlTS = "select\n                q_id,\n                'q_'||q_id as prefix,\n                nvl(reason,'')  as reason\n              from\n                pci_na_reason\n              where\n                tracking_number =  :1 \n              order by q_id asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.quiz.pci.PCIQuestionnaire_v2",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1420);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.quiz.pci.PCIQuestionnaire_v2",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:424^13*/

            rs = it.getResultSet();

            //look for a match, either load or list for deletion
            ArrayList dead = null;

            while(rs.next())
            {
              long qId              = rs.getLong("q_id");
              boolean foundExisting = false;

              //loop through specialQList
              QuizQuestion q = null;
              for(Iterator qIt = specialNAList.iterator(); qIt.hasNext();)
              {
                q = (QuizQuestion)qIt.next();
                if( q.getId() == qId )
                {
                  //found a match, so load existing data
                  //but only load if the current data is empty
                  //as there's a chance we got here a second time within
                  //the quiz process which means we want to keep the active
                  //not persisted data
                  String prefix = rs.getString("prefix");

                  if(getData(prefix+REASON_TAG).equals(""))
                  {
                    setData(prefix+REASON_TAG,rs.getString("reason"));
                  }
                  foundExisting = true;
                  break;
                }
              }

              if(!foundExisting)
              {
                if(dead == null)
                {
                  dead  = new ArrayList();
                }
                dead.add(q);
              }
            }

            //delete those found in the db that don't match existing
            //question answers - we have no need for historical reference
            if(dead != null)
            {
              StringBuffer sb = new StringBuffer();

              QuizQuestion q;
              for(int i=0;i<dead.size();i++)
              {
                q = (QuizQuestion)dead.get(i);
                sb.append(q.getId()).append(",");
              }
              //lose last comma
              sb.deleteCharAt(sb.length()-1);

              /*@lineinfo:generated-code*//*@lineinfo:484^15*/

//  ************************************************************
//  #sql [Ctx] { delete from pci_na_reason
//                  where tracking_number = :getTrackingNumber()
//                  and q_id in ( :sb.toString() )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1421 = getTrackingNumber();
 String __sJT_1422 = sb.toString();
   String theSqlTS = "delete from pci_na_reason\n                where tracking_number =  :1 \n                and q_id in (  :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.reports.quiz.pci.PCIQuestionnaire_v2",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1421);
   __sJT_st.setString(2,__sJT_1422);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:489^15*/
            }
          }

          specialNAFieldsLoaded = true;

        }
        catch(Exception e)
        {
          //e.printStackTrace();
          messenger.append(e.getMessage());
          //logEntry("PCIQuestionnaire::loadSpecial()",e.toString());
        }
        finally
        {
          cleanUp();
        }

      }//end if specialActionFieldsLoaded

    }//end if
  }

  //always do this, for all levels, on page 14
  protected boolean doSaveSpecial()
  {
    return currentPage == 14;
  }

  protected void saveSpecial(boolean doIt)
  throws Exception
  {
    if(doIt)
    {
      //call super to handle CCU
      super.saveSpecial(true);

      //then handle NA answers
      log.debug("deleting NA answers...");

      //first off, clear any previous results
      /*@lineinfo:generated-code*//*@lineinfo:530^7*/

//  ************************************************************
//  #sql [Ctx] { delete from pci_na_reason
//          where tracking_number = :getTrackingNumber()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1423 = getTrackingNumber();
   String theSqlTS = "delete from pci_na_reason\n        where tracking_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.reports.quiz.pci.PCIQuestionnaire_v2",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1423);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:534^7*/

      //check for any NA responses here
      if( specialNAList != null )
      {
        log.debug("saving NA answers...");

        QuizQuestion q;
        for(Iterator it = specialNAList.iterator();it.hasNext();)
        {
          q = (QuizQuestion)it.next();
          //get the fields related to this question
          //save them to PCI_NA_REASON
          String reason = getData(q.getFieldName()+REASON_TAG);

          /*@lineinfo:generated-code*//*@lineinfo:549^11*/

//  ************************************************************
//  #sql [Ctx] { insert into pci_na_reason
//              (
//                tracking_number,
//                q_id,
//                reason
//              )
//              values
//              (
//               :getTrackingNumber(),
//               :q.getId(),
//               :reason
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1424 = getTrackingNumber();
 long __sJT_1425 = q.getId();
   String theSqlTS = "insert into pci_na_reason\n            (\n              tracking_number,\n              q_id,\n              reason\n            )\n            values\n            (\n              :1 ,\n              :2 ,\n              :3 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.reports.quiz.pci.PCIQuestionnaire_v2",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1424);
   __sJT_st.setLong(2,__sJT_1425);
   __sJT_st.setString(3,reason);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:563^11*/
        }
      }
    }
  }

  protected void clear()
  {
    super.clear();
    specialNAFieldsBuilt  = false;
    specialNAFieldsLoaded = false;
    specialNAFields       = null;
    specialNAList         = null;
  }

  public boolean validate()
  {
    boolean _isValid = super.validate();
    log.debug(_isValid?"validate is true":"validate is false");

    if (_isValid)
    {
      //check NA fields
      _isValid = specialNAList != null?getField("nafields").isValid():true;

      log.debug(_isValid?"specialNAList is either null or valid":"specialNAList is invalid");
    }

    return _isValid;
  }

  protected void buildNAValidationFields()
  {
    //always start with a fresh fieldgroup
    specialNAFields = new FieldGroup("nafields");

    log.debug("building n/a fields...");
    QuizComponent comp;

    //loop through each comp and build
    for( Iterator it = components.iterator(); it.hasNext();)
    {
      comp = (QuizComponent)it.next();
      buildNAValidationFields(comp);
    }

    //ensure that all fields are set to required
    //since validation gets removed if the user navigates
    //backward away from this page
    if(currentPage==14)
    {
      updateSpecialValidation(true);
    }

    //add to base group after all have been built
    fields.add(specialNAFields);

    //flag it
    specialNAFieldsBuilt = true;
  }

  protected void buildNAValidationFields(QuizComponent comp)
  {
    //loop through questions and build fields if needed
    if(comp.hasQuestions())
    {
      //iterate questions
      QuizQuestion q;
      for(Iterator it = comp.iterator(); it.hasNext();)
      {
        q = (QuizQuestion)it.next();
        buildNAValidationFields(q);
      }
    }

    if(comp.hasComponents())
    {
      QuizComponent temp;
      for(Iterator i = comp.getComponents().iterator(); i.hasNext();)
      {
        temp = (QuizComponent)i.next();
        buildNAValidationFields(temp);
      }
    }
  }

  //this is a top in process, based on the current state of the questions
  //we need to be aware of the fact that this could be the 1..Nth time coming in
  //so we have to handle the list accordingly.
  protected void buildNAValidationFields(QuizQuestion q)
  {
    QuizQuestion q2;

    Field field = q.getField();
    //we're looking for those questions that were answered
    //'CCU' meaning that they have to fill out further info
    if(field != null)
    {
      if (field.getData().equals("NA"))
      {
        boolean alreadyExists = false;

        //if first one found, start new list
        if(specialNAList == null)
        {
          specialNAList = new LinkedList();
        }
        else
        {
          //check to see if this has already been built/added
          //if so, ignore this as we need to retain any existing data
          for(Iterator it = specialNAList.iterator(); it.hasNext();)
          {
            q2 =(QuizQuestion)it.next();
            if(q.getId() == q2.getId())
            {
              log.debug("This NA already exists... keeping same build");

              Field f;
              //have to add the existing fields back in though

              f = getField(q.getFieldName()+REASON_TAG);
              if(f!=null)
              {
                specialNAFields.add(f);
              }

              alreadyExists = true;

              break;
            }
          }
        }

        if(!alreadyExists)
        {
          //add the question to the new list
          boolean added = false;

          for(int i=0;i<specialNAList.size();i++)
          {
            //try to make sure they're in order,
            //since this list drives display
            q2 = (QuizQuestion)specialNAList.get(i);

            if(q.getId() > q2.getId())
            {
              continue;
            }
            else
            {
              specialNAList.add(i,q);
              added = true;
              break;
            }
          }

          if(!added)
          {
            specialNAList.add(q);
          }

          log.debug("NA built --> "+q.getFieldName());

          //build new fields for na
          Field newField;

          newField  = new TextareaField(q.getFieldName()+REASON_TAG,500,2,80,false);
          specialNAFields.add(newField);
        }
      }
      else
      {
        //check the special NA list to ensure that this item isn't there
        //if it is, remove it as the status of the CCU has obviously changed
        if(specialNAList != null)
        {
          for(Iterator it = specialNAList.iterator(); it.hasNext();)
          {
            q2 = (QuizQuestion)it.next();
            if(q2.getId()==q.getId())
            {
              log.debug("found an invalid NA question... removing");

              //remove question from list
              it.remove();
              fields.deleteField(q.getFieldName()+REASON_TAG);
              /*
              //have to remove the fields as well
              for(int i = 0; i < naSuf.length; i++)
              {
                fields.deleteField(q.getFieldName()+naSuf[i]);
              }
              */
              break;
            }
          }

          //if we've removed the last of the questions from this list...
          if(specialNAList.size() < 1)
          {
            specialNAList = null;
            specialNAFieldsBuilt = false;
          }
        }
      }//end if "NA"
    }
  }

  public List getNAValidationQuestions()
  {
    return specialNAList;
  }

  public boolean hasNAQuestions()
  {
    return (specialNAList!=null && specialNAList.size()>0);
  }
}/*@lineinfo:generated-code*/