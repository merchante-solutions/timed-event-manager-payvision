package com.mes.reports.quiz;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;

public class QuizComponent extends QuizBase implements Serializable
{
  private LinkedList components = new LinkedList();
  private LinkedList questions  = new LinkedList();

  private String name           = "";
  private String description    = "";

  //for validation and general HTTP organization
  private FieldGroup fields;

  public QuizComponent(long id)
  {
    super(id);
    fields = new FieldGroup("comp_"+id);
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getName()
  {
    return name;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getDescription()
  {
    return description;
  }

  public boolean hasQuestions()
  {
    return questions.size() > 0;
  }

  public List getQuestions()
  {
    return questions;
  }

  public Iterator iterator()
  {
    return questions.iterator();
  }

  public void addQuestion(QuizQuestion q)
  {
    addQuestion(-1, q);
  }

  public void addQuestion(int pos, QuizQuestion q)
  {
    if(pos > -1)
    {
      questions.add(pos, q);
    }
    else
    {
      questions.add(q);
    }

    if(q.getField() != null)
    {
      addField(q.getField());
    }
  }

  public boolean hasComponents()
  {
    return components.size() > 0;
  }

  public List getComponents()
  {
    return components;
  }

  public void addComponent(QuizComponent comp)
  {
    components.add(comp);
  }

  public void addComponent(int pos, QuizComponent comp)
  {
    if(pos > 0 && pos <= components.size())
    {
      components.add(pos, comp);
    }
    else
    {
      addComponent(comp);
    }
  }

  public void addField(Field field)
  {
    fields.add(field);
  }

  public FieldGroup getFieldGroup()
  {
    return fields;
  }

  public Field getField(String name)
  {
    return fields.getField(name);
  }

  public boolean validate()
  {
    return fields.isValid();
  }

  //completely contrived... but a quick fix
  public String getHelpLink()
  {
    StringBuffer sb = new StringBuffer();

    //only for requirement components
    if(name.indexOf("Requirement")==0)
    {
      sb.append("<a href='../help/pci/");
      sb.append("Requirement_").append(name.substring(12));
      sb.append(".htm'>help<//a>");
    }

    return sb.toString();
  }


}