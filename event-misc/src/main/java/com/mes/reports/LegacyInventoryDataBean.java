/*@lineinfo:filename=LegacyInventoryDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/LegacyInventoryDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2000-2005,2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.Field;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class LegacyInventoryDataBean extends ReportSQLJBean
{
  public static final int           RT_CALL_TAGS      = RT_USER + 0;
  public static final int           RT_TRANSFERS      = RT_USER + 1;
  
  public class CallTagData
  {
    public long               Association       = 0L;
    public String             CallTagNumber     = null;
    public Timestamp          CallTagTS         = null;
    public String             CallTagType       = null;
    public String             Client            = null;
    public String             DbaName           = null;
    public String             Description       = null;
    public long               MerchantId        = 0L;
    public String             ReferenceNumber   = null;
    public String             SerialNumber      = null;
    public String             Status            = null;
    public double             UnitCost          = 0.0;
    
    public CallTagData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      Association       = resultSet.getLong("association_number");
      CallTagNumber     = processString( resultSet.getString("calltag_number") );
      CallTagTS         = resultSet.getTimestamp("calltag_date");
      CallTagType       = processString( resultSet.getString("calltag_type") );
      Client            = processString( resultSet.getString("client_name") );
      DbaName           = processString( resultSet.getString("dba_name") );
      Description       = processString( resultSet.getString("calltag_description") );
      MerchantId        = resultSet.getLong("merchant_number");
      ReferenceNumber   = processString( resultSet.getString("reference_number") );
      SerialNumber      = processString( resultSet.getString("calltag_serial_num") );
      Status            = processString( resultSet.getString("calltag_status") );
      UnitCost          = resultSet.getDouble("calltag_unit_cost");
    }
  }
  
  public class TransferData
  {
    public String             EquipDesc         = null;
    public String             EquipType         = null;
    public String             MoveCondition     = null;
    public String             MovedFrom         = null;
    public String             MovedTo           = null;
    public String             SerialNumber      = null;
    public Timestamp          TransferTS        = null;
    public double             UnitCost          = 0.0;
  
    public TransferData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      EquipDesc       = processString( resultSet.getString("equip_desc") );
      EquipType       = processString( resultSet.getString("equip_type") );
      MoveCondition   = processString( resultSet.getString("transfer_condition") );
      MovedFrom       = processString( resultSet.getString("moved_from") );
      MovedTo         = processString( resultSet.getString("moved_to") );
      SerialNumber    = processString( resultSet.getString("serial_number") );
      TransferTS      = resultSet.getTimestamp("transfer_date");
      UnitCost        = resultSet.getDouble("unit_cost");
    }
  }
  
  public LegacyInventoryDataBean( )
  {
    super(true);    // enable field bean support
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    // transfer report fields
    fields.add(new Field("client",50,0,true));
    fields.add(new Field("equipType",50,0,true));
    fields.add(new Field("equipModel",50,0,true));
    
    // call tag fields
    fields.add(new Field("lookupValue",50,0,true));
    fields.add(new Field("calltagType",50,0,true));
    fields.add(new Field("calltagStatus",50,0,true));
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    switch( getReportType() )
    {
      case RT_CALL_TAGS:
        line.append("\"Call Tag Date\",");
        line.append("\"Client\",");
        line.append("\"DBA Name\",");
        line.append("\"Merchant #\",");
        line.append("\"Assoc #\",");
        line.append("\"Call Tag Type\",");
        line.append("\"Call Tag #\",");
        line.append("\"Reference #\",");
        line.append("\"Serial #\",");
        line.append("\"Description\",");
        line.append("\"Unit Cost\",");
        line.append("\"Call Tag Status\"");
        break;
        
      case RT_TRANSFERS:
        line.append("\"Serial Number\",");
        line.append("\"Equip Type\",");
        line.append("\"Equip Desc\",");
        line.append("\"Cost\",");
        line.append("\"Date Transferred\",");
        line.append("\"Moved From\",");
        line.append("\"Moved To\",");
        line.append("\"Condition\"");
        break;
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    line.setLength(0);
    
    switch( getReportType() )
    {
      case RT_CALL_TAGS:
      {
        CallTagData   record = (CallTagData)obj;

        line.append("\"");
        line.append(DateTimeFormatter.getFormattedDate(record.CallTagTS,"MM/dd/yyyy hh:mm:ss a"));
        line.append("\",\"");
        line.append(record.Client);
        line.append("\",\"");
        line.append(record.DbaName);
        line.append("\",");
        line.append(encodeHierarchyNode(record.MerchantId));
        line.append(",\"");
        line.append(record.Association);
        line.append("\",\"");
        line.append(record.CallTagType);
        line.append("\",\"'");
        line.append(record.CallTagNumber);
        line.append("\",\"");
        line.append(record.ReferenceNumber);
        line.append("\",\"");
        line.append(record.SerialNumber);
        line.append("\",\"");
        line.append(record.Description);
        line.append("\",");
        line.append(record.UnitCost);
        line.append(",\"");
        line.append(record.Status);
        line.append("\"");
        break;
      }        
        
      case RT_TRANSFERS:
      {
        TransferData   record = (TransferData)obj;
        
        line.append("\"");
        line.append(record.SerialNumber);
        line.append("\",\"");
        line.append(record.EquipType);
        line.append("\",\"");
        line.append(record.EquipDesc);
        line.append("\",");
        line.append(record.UnitCost);
        line.append(",\"");
        line.append(DateTimeFormatter.getFormattedDate(record.TransferTS,"MM/dd/yyyy hh:mm:ss a"));
        line.append("\",\"");
        line.append(record.MovedFrom);
        line.append("\",\"");
        line.append(record.MovedTo);
        line.append("\",\"");
        line.append(record.MoveCondition);
        line.append("\"");
        break;
      }        
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Date            beginDate   = getReportDateBegin();
    Date            endDate     = getReportDateEnd();
    StringBuffer    filename    = new StringBuffer();
    
    switch( getReportType() )
    {
      case RT_CALL_TAGS:
        filename.append("call_tags_");
        break;
        
      case RT_TRANSFERS:
        filename.append("equip_tranfers_");
        break;
        
      default:
        filename.append("no_filename_");
        break;
    }
    
    filename.append( DateTimeFormatter.getFormattedDate( beginDate,"MMddyyyy" ) );
    if ( beginDate.equals(endDate) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( endDate,"MMddyyyy" ) );
    }
  
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
        
      default:
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      switch( getReportType() )
      {
        case RT_CALL_TAGS:
          loadDataCallTag(beginDate,endDate);
          break;
          
        case RT_TRANSFERS:
          loadDataTransfers(beginDate,endDate);
          break;
          
        default:
          break;
      }          
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
    }
  }
  
  protected void loadDataCallTag( Date beginDate, Date endDate )
  {
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
  
    try
    {
      // extract query params to simple data types
      int     client          = getInt("client",-1);
      int     calltagStatus   = getInt("calltagStatus",-1);
      int     calltagType     = getInt("calltagType",-1);
      long    longLookup      = getLong("lookupValue",-1L);
      String  stringLookup    = getData("lookupValue");
      String  stringLookup2   = stringLookup;
      
      // wildcard string
      if( stringLookup.equals("") )
      {
        stringLookup = "passall";
      }
      else
      {
        stringLookup = "%" + stringLookup + "%";
      }

      // all upper string
      if( stringLookup2.equals("") )
      {
        stringLookup2 = "passall";
      }
      else
      {
        stringLookup2 = stringLookup2.toUpperCase();
      }
  
      for ( int i = 0; i < 3; ++i )
      {
        switch(i)
        {
          case 0:   // call tags for swaps
          {
            /*@lineinfo:generated-code*//*@lineinfo:345^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  mt.merchant_name                dba_name,
//                        mt.merchant_type_desc           client_name,
//                        mt.merchant_number              merchant_number,
//                        mt.association_number           association_number,
//                        emt.action_date                 calltag_date,
//                        emta.action_description         calltag_type,
//                        ecs.calltag_description         calltag_status,
//                        es.SWAP_REF_NUM                 reference_number,
//                        es.call_tag_num                 calltag_number,
//                        es.serial_num_in                calltag_serial_num,
//                        equip.equip_descriptor          calltag_description,
//                        ei.ei_unit_cost                 calltag_unit_cost
//                from    merchant_types                  mt,
//                        equip_merchant_tracking         emt,
//                        equip_merchant_tracking_action  emta,
//                        equip_calltag_status            ecs,
//                        equip_swap                      es,
//                        equip_inventory                 ei,
//                        equipment                       equip
//                where   trunc(emt.action_date) between :beginDate and :endDate and
//                        emt.action = 2          and 
//                        emt.cancel_date is null and
//                        emt.merchant_number     = mt.merchant_number  and
//                        emt.action              = emta.action_code    and
//                        emt.ref_num_serial_num  = es.swap_ref_num     and
//                        es.swap_STATUS          = ecs.calltag_code    and
//                        (-1 = :client or mt.merchant_type = :client)  and
//                        (-1 = :calltagStatus or es.swap_STATUS = :calltagStatus) and
//                        (
//                          'passall'              = :stringLookup or
//                          mt.merchant_number     = :longLookup or
//                          mt.association_number  = :longLookup or
//                          emt.ref_num_serial_num = :stringLookup2 or 
//                          upper(es.call_tag_num) = :stringLookup2 or 
//                          upper(mt.merchant_name) like :stringLookup
//                        ) and
//                        es.serial_num_in  = ei.ei_serial_number(+)    and
//                        ei.ei_part_number = equip.equip_model(+)
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mt.merchant_name                dba_name,\n                      mt.merchant_type_desc           client_name,\n                      mt.merchant_number              merchant_number,\n                      mt.association_number           association_number,\n                      emt.action_date                 calltag_date,\n                      emta.action_description         calltag_type,\n                      ecs.calltag_description         calltag_status,\n                      es.SWAP_REF_NUM                 reference_number,\n                      es.call_tag_num                 calltag_number,\n                      es.serial_num_in                calltag_serial_num,\n                      equip.equip_descriptor          calltag_description,\n                      ei.ei_unit_cost                 calltag_unit_cost\n              from    merchant_types                  mt,\n                      equip_merchant_tracking         emt,\n                      equip_merchant_tracking_action  emta,\n                      equip_calltag_status            ecs,\n                      equip_swap                      es,\n                      equip_inventory                 ei,\n                      equipment                       equip\n              where   trunc(emt.action_date) between  :1  and  :2  and\n                      emt.action = 2          and \n                      emt.cancel_date is null and\n                      emt.merchant_number     = mt.merchant_number  and\n                      emt.action              = emta.action_code    and\n                      emt.ref_num_serial_num  = es.swap_ref_num     and\n                      es.swap_STATUS          = ecs.calltag_code    and\n                      (-1 =  :3  or mt.merchant_type =  :4 )  and\n                      (-1 =  :5  or es.swap_STATUS =  :6 ) and\n                      (\n                        'passall'              =  :7  or\n                        mt.merchant_number     =  :8  or\n                        mt.association_number  =  :9  or\n                        emt.ref_num_serial_num =  :10  or \n                        upper(es.call_tag_num) =  :11  or \n                        upper(mt.merchant_name) like  :12 \n                      ) and\n                      es.serial_num_in  = ei.ei_serial_number(+)    and\n                      ei.ei_part_number = equip.equip_model(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.LegacyInventoryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setInt(3,client);
   __sJT_st.setInt(4,client);
   __sJT_st.setInt(5,calltagStatus);
   __sJT_st.setInt(6,calltagStatus);
   __sJT_st.setString(7,stringLookup);
   __sJT_st.setLong(8,longLookup);
   __sJT_st.setLong(9,longLookup);
   __sJT_st.setString(10,stringLookup2);
   __sJT_st.setString(11,stringLookup2);
   __sJT_st.setString(12,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.LegacyInventoryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:385^13*/
            break;
          }
          
          case 1:     // call tags for repairs
          {
            /*@lineinfo:generated-code*//*@lineinfo:391^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  get_dba_name(emt.merchant_number)     dba_name,
//                        get_association(emt.merchant_number)  association_number,
//                        emt.merchant_number                   merchant_number,
//                        emt.action_date                       calltag_date,
//                        emta.action_description               calltag_type,
//                        ecs.calltag_description               calltag_status,
//                        er.REPAIR_REF_NUM                     reference_number,
//                        er.call_tag_num                       calltag_number,
//                        app.inventory_owner_name              client_name,
//                        er.serial_num                         calltag_serial_num,
//                        equip.equip_descriptor                calltag_description,
//                        ei.ei_unit_cost                       calltag_unit_cost
//                from    equip_merchant_tracking               emt,
//                        equip_merchant_tracking_action        emta,
//                        equip_calltag_status                  ecs,
//                        equip_repair                          er,
//                        app_type                              app,
//                        equip_inventory                       ei,
//                        equipment                             equip
//                where   trunc(emt.action_date) between :beginDate and :endDate and
//                        emt.action = 3          and 
//                        emt.cancel_date is null and
//                        emt.owner               = app.app_type_code(+)  and
//                        emt.action              = emta.action_code      and
//                        emt.ref_num_serial_num  = er.repair_ref_num  and
//                        er.REPAIR_STATUS = ecs.calltag_code and
//                        (-1 = :client        or emt.owner = :client) and
//                        (-1 = :calltagStatus or er.REPAIR_STATUS = :calltagStatus) and
//                        (
//                          'passall'              = :stringLookup or
//                          emt.merchant_number    = :longLookup or
//                          emt.ref_num_serial_num = :stringLookup2 or 
//                          upper(er.call_tag_num) = :stringLookup2
//                        ) and
//                        er.serial_num     = ei.ei_serial_number(+)    and
//                        ei.ei_part_number = equip.equip_model(+)
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  get_dba_name(emt.merchant_number)     dba_name,\n                      get_association(emt.merchant_number)  association_number,\n                      emt.merchant_number                   merchant_number,\n                      emt.action_date                       calltag_date,\n                      emta.action_description               calltag_type,\n                      ecs.calltag_description               calltag_status,\n                      er.REPAIR_REF_NUM                     reference_number,\n                      er.call_tag_num                       calltag_number,\n                      app.inventory_owner_name              client_name,\n                      er.serial_num                         calltag_serial_num,\n                      equip.equip_descriptor                calltag_description,\n                      ei.ei_unit_cost                       calltag_unit_cost\n              from    equip_merchant_tracking               emt,\n                      equip_merchant_tracking_action        emta,\n                      equip_calltag_status                  ecs,\n                      equip_repair                          er,\n                      app_type                              app,\n                      equip_inventory                       ei,\n                      equipment                             equip\n              where   trunc(emt.action_date) between  :1  and  :2  and\n                      emt.action = 3          and \n                      emt.cancel_date is null and\n                      emt.owner               = app.app_type_code(+)  and\n                      emt.action              = emta.action_code      and\n                      emt.ref_num_serial_num  = er.repair_ref_num  and\n                      er.REPAIR_STATUS = ecs.calltag_code and\n                      (-1 =  :3         or emt.owner =  :4 ) and\n                      (-1 =  :5  or er.REPAIR_STATUS =  :6 ) and\n                      (\n                        'passall'              =  :7  or\n                        emt.merchant_number    =  :8  or\n                        emt.ref_num_serial_num =  :9  or \n                        upper(er.call_tag_num) =  :10 \n                      ) and\n                      er.serial_num     = ei.ei_serial_number(+)    and\n                      ei.ei_part_number = equip.equip_model(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.LegacyInventoryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setInt(3,client);
   __sJT_st.setInt(4,client);
   __sJT_st.setInt(5,calltagStatus);
   __sJT_st.setInt(6,calltagStatus);
   __sJT_st.setString(7,stringLookup);
   __sJT_st.setLong(8,longLookup);
   __sJT_st.setString(9,stringLookup2);
   __sJT_st.setString(10,stringLookup2);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.LegacyInventoryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:429^13*/
            break;
          }                
          
          case 2:     // call tags for exchanges
          {
            /*@lineinfo:generated-code*//*@lineinfo:435^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  mt.merchant_name                dba_name,
//                        mt.merchant_type_desc           client_name,
//                        mt.merchant_number              merchant_number,
//                        mt.association_number           association_number,
//                        emt.action_date                 calltag_date,
//                        emta.action_description         calltag_type,
//                        ecs.calltag_description         calltag_status,
//                        ee.EXCHANGE_REF_NUM             reference_number,
//                        ee.call_tag_num                 calltag_number,
//                        ee.serial_num_in                calltag_serial_num,
//                        equip.equip_descriptor          calltag_description,
//                        ei.ei_unit_cost                 calltag_unit_cost
//                from    merchant_types                  mt,
//                        equip_merchant_tracking         emt,
//                        equip_merchant_tracking_action  emta,
//                        equip_calltag_status            ecs,
//                        equip_exchange                  ee,
//                        equip_inventory                 ei,
//                        equipment                       equip
//                where   trunc(emt.action_date) between :beginDate and :endDate and
//                        emt.action = 6          and 
//                        emt.cancel_date is null and
//                        emt.merchant_number     = mt.merchant_number  and
//                        emt.action              = emta.action_code    and
//                        emt.ref_num_serial_num  = ee.exchange_ref_num and
//                        ee.EXCHANGE_STATUS      = ecs.calltag_code    and
//                        (-1 = :client         or mt.merchant_type = :client)          and
//                        (-1 = :calltagStatus  or ee.EXCHANGE_STATUS = :calltagStatus) and
//                        (
//                          'passall'              = :stringLookup or
//                          mt.merchant_number     = :longLookup or
//                          mt.association_number  = :longLookup or
//                          emt.ref_num_serial_num = :stringLookup2 or 
//                          upper(ee.call_tag_num) = :stringLookup2 or 
//                          upper(mt.merchant_name) like :stringLookup
//                        ) and
//                        ee.serial_num_in  = ei.ei_serial_number(+)    and
//                        ei.ei_part_number = equip.equip_model(+)
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mt.merchant_name                dba_name,\n                      mt.merchant_type_desc           client_name,\n                      mt.merchant_number              merchant_number,\n                      mt.association_number           association_number,\n                      emt.action_date                 calltag_date,\n                      emta.action_description         calltag_type,\n                      ecs.calltag_description         calltag_status,\n                      ee.EXCHANGE_REF_NUM             reference_number,\n                      ee.call_tag_num                 calltag_number,\n                      ee.serial_num_in                calltag_serial_num,\n                      equip.equip_descriptor          calltag_description,\n                      ei.ei_unit_cost                 calltag_unit_cost\n              from    merchant_types                  mt,\n                      equip_merchant_tracking         emt,\n                      equip_merchant_tracking_action  emta,\n                      equip_calltag_status            ecs,\n                      equip_exchange                  ee,\n                      equip_inventory                 ei,\n                      equipment                       equip\n              where   trunc(emt.action_date) between  :1  and  :2  and\n                      emt.action = 6          and \n                      emt.cancel_date is null and\n                      emt.merchant_number     = mt.merchant_number  and\n                      emt.action              = emta.action_code    and\n                      emt.ref_num_serial_num  = ee.exchange_ref_num and\n                      ee.EXCHANGE_STATUS      = ecs.calltag_code    and\n                      (-1 =  :3          or mt.merchant_type =  :4 )          and\n                      (-1 =  :5   or ee.EXCHANGE_STATUS =  :6 ) and\n                      (\n                        'passall'              =  :7  or\n                        mt.merchant_number     =  :8  or\n                        mt.association_number  =  :9  or\n                        emt.ref_num_serial_num =  :10  or \n                        upper(ee.call_tag_num) =  :11  or \n                        upper(mt.merchant_name) like  :12 \n                      ) and\n                      ee.serial_num_in  = ei.ei_serial_number(+)    and\n                      ei.ei_part_number = equip.equip_model(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.LegacyInventoryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setInt(3,client);
   __sJT_st.setInt(4,client);
   __sJT_st.setInt(5,calltagStatus);
   __sJT_st.setInt(6,calltagStatus);
   __sJT_st.setString(7,stringLookup);
   __sJT_st.setLong(8,longLookup);
   __sJT_st.setLong(9,longLookup);
   __sJT_st.setString(10,stringLookup2);
   __sJT_st.setString(11,stringLookup2);
   __sJT_st.setString(12,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.LegacyInventoryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:475^13*/
            break;
          }
        }
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          ReportRows.addElement( new CallTagData( resultSet ) );
        }
        resultSet.close();
        it.close();
      }
    }
    catch( Exception e )
    {
      logEntry("loadDataCallTag()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  protected void loadDataTransfers( Date beginDate, Date endDate )
  {
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    
    try
    {
      int     client      = getInt("client",-1);
      String  equipType   = getData("equipType");
      String  equipModel  = getData("equipModel");
    
      // force to query defaults if the value is blank
      if ( equipType.equals("") ) { equipType = "-1"; }
      if ( equipModel.equals("") ) { equipModel = "-1"; }
  
      /*@lineinfo:generated-code*//*@lineinfo:514^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_serial_number             serial_number,
//                  ei.ei_part_number               part_number,
//                  ei.ei_unit_cost                 unit_cost,
//                  ei.ei_status                    status,
//                  et.equiptype_description        equip_type,
//                  eq.equip_descriptor             equip_desc,
//                  ecd.move_date                   transfer_date,
//                  at1.inventory_owner_name        moved_from,
//                  at1.app_type_code               moved_from_code,
//                  at2.inventory_owner_name        moved_to,
//                  at2.app_type_code               moved_to_code,
//                  ec.ec_class_name                transfer_condition
//          from    equipment                       eq,
//                  equiptype                       et,
//                  equip_inventory                 ei,
//                  equip_class                     ec,
//                  app_type                        at1,
//                  app_type                        at2,
//                  equip_client_deployment         ecd
//          where   trunc(ecd.move_date) between :beginDate and :endDate      and
//                  ecd.serial_number     = ei.ei_serial_number               and
//                  ei.EI_PART_NUMBER     = eq.equip_model                    and
//                  ecd.condition         = ec.ec_class_id                    and
//                  ecd.from_client       = at1.app_type_code                 and 
//                  ecd.to_client         = at2.app_type_code                 and
//                  eq.equiptype_code     = et.equiptype_code                 and 
//                  (-1   = :client     or ecd.to_client      = :client)      and
//                  ('-1' = :equipType  or eq.equiptype_code  = :equipType)   and
//                  ('-1' = :equipModel or eq.equip_model     = :equipModel)  and
//                  ecd.cancel_date is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_serial_number             serial_number,\n                ei.ei_part_number               part_number,\n                ei.ei_unit_cost                 unit_cost,\n                ei.ei_status                    status,\n                et.equiptype_description        equip_type,\n                eq.equip_descriptor             equip_desc,\n                ecd.move_date                   transfer_date,\n                at1.inventory_owner_name        moved_from,\n                at1.app_type_code               moved_from_code,\n                at2.inventory_owner_name        moved_to,\n                at2.app_type_code               moved_to_code,\n                ec.ec_class_name                transfer_condition\n        from    equipment                       eq,\n                equiptype                       et,\n                equip_inventory                 ei,\n                equip_class                     ec,\n                app_type                        at1,\n                app_type                        at2,\n                equip_client_deployment         ecd\n        where   trunc(ecd.move_date) between  :1  and  :2       and\n                ecd.serial_number     = ei.ei_serial_number               and\n                ei.EI_PART_NUMBER     = eq.equip_model                    and\n                ecd.condition         = ec.ec_class_id                    and\n                ecd.from_client       = at1.app_type_code                 and \n                ecd.to_client         = at2.app_type_code                 and\n                eq.equiptype_code     = et.equiptype_code                 and \n                (-1   =  :3      or ecd.to_client      =  :4 )      and\n                ('-1' =  :5   or eq.equiptype_code  =  :6 )   and\n                ('-1' =  :7  or eq.equip_model     =  :8 )  and\n                ecd.cancel_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.LegacyInventoryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setInt(3,client);
   __sJT_st.setInt(4,client);
   __sJT_st.setString(5,equipType);
   __sJT_st.setString(6,equipType);
   __sJT_st.setString(7,equipModel);
   __sJT_st.setString(8,equipModel);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.LegacyInventoryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:546^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement( new TransferData( resultSet ) );
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("loadDataTransfers()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/