/*@lineinfo:filename=CardInfoLookup*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/reports/CardInfoLookup.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-04-20 20:32:57 -0700 (Sat, 20 Apr 2013) $
  Version            : $Revision: 21046 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.TridentTools;
import sqlj.runtime.ResultSetIterator;

public class CardInfoLookup 
  extends SQLJConnectionBase
{
  public String[] getCardInfo( String cnFull )
  {
    ResultSetIterator   it              = null;
    ResultSet           resultSet       = null;
    String[]            retVal          = new String[4];
    
    try
    {
      String  cardType          = TridentTools.decodeVisakCardType(cnFull);
      String  cardClass         = "UNKNOWN";
      String  issuerCountryCode = "UNKNOWN";
    
      if ( "VS".equals(cardType) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:51^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ard.issuer_country_code       as issuer_country_code,
//                    decode( ard.funding_source,
//                            'C' , 'CREDIT',   -- Credit
//                            'D' , 'DEBIT',    -- Debit
//                            'P' , 'PREPAID',  -- Prepaid
//                            'H' , 'CREDIT',   -- Charge
//                            'R' , 'CREDIT',   -- Deferred Debit
//                            'UNKNOWN')            as card_class
//            from    visa_ardef_table    ard
//            where   substr(:cnFull,1,9) between ard.range_low and ard.range_high
//                    and ard.account_number_length in (0,length(:cnFull))
//                    and rownum = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ard.issuer_country_code       as issuer_country_code,\n                  decode( ard.funding_source,\n                          'C' , 'CREDIT',   -- Credit\n                          'D' , 'DEBIT',    -- Debit\n                          'P' , 'PREPAID',  -- Prepaid\n                          'H' , 'CREDIT',   -- Charge\n                          'R' , 'CREDIT',   -- Deferred Debit\n                          'UNKNOWN')            as card_class\n          from    visa_ardef_table    ard\n          where   substr( :1 ,1,9) between ard.range_low and ard.range_high\n                  and ard.account_number_length in (0,length( :2 ))\n                  and rownum = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.CardInfoLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,cnFull);
   __sJT_st.setString(2,cnFull);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.CardInfoLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:65^9*/
      }
      else if ( "MC".equals(cardType) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:69^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  c.country_code                                as issuer_country_code,
//                    case
//                      when ard.card_program_id = 'MCC' then 'CREDIT'
//                      when ard.card_program_id = 'DMC' then 
//                        case 
//                          when instr(lower(msp.product_desc),'prepaid') > 0 then 'PREPAID'
//                          else 'DEBIT'
//                        end
//                      else 'UNKNOWN'
//                    end                                           as card_class
//            from    mc_ardef_table            ard,
//                    country                   c,
//                    mc_settlement_products    msp
//            where   to_number(rpad(:cnFull,19,'0')) between ard.range_low and ard.range_high
//                    and ard.active_inactive_code = 'A'
//                    and c.iso_country_code(+) = ard.country_code_numeric
//                    and msp.gcms_product_id(+) = ard.gcms_product_id
//                    and msp.licensed_product_id(+) = ard.licensed_product_id
//            order by ard.priority_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  c.country_code                                as issuer_country_code,\n                  case\n                    when ard.card_program_id = 'MCC' then 'CREDIT'\n                    when ard.card_program_id = 'DMC' then \n                      case \n                        when instr(lower(msp.product_desc),'prepaid') > 0 then 'PREPAID'\n                        else 'DEBIT'\n                      end\n                    else 'UNKNOWN'\n                  end                                           as card_class\n          from    mc_ardef_table            ard,\n                  country                   c,\n                  mc_settlement_products    msp\n          where   to_number(rpad( :1 ,19,'0')) between ard.range_low and ard.range_high\n                  and ard.active_inactive_code = 'A'\n                  and c.iso_country_code(+) = ard.country_code_numeric\n                  and msp.gcms_product_id(+) = ard.gcms_product_id\n                  and msp.licensed_product_id(+) = ard.licensed_product_id\n          order by ard.priority_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.CardInfoLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,cnFull);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.CardInfoLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:90^9*/
      }
      else if ( "DS".equals(cardType) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:94^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  c.country_code                                as issuer_country_code,
//                    decode(ard.card_product_id,
//                          '001','CREDIT'  , -- Consumer Credit - Rewards
//                          '002','CREDIT'  , -- Commercial Credit
//                          '003','DEBIT'   , -- Consumer Debit
//                          '004','DEBIT'   , -- Commercial Debit
//                          '005','PREPAID' , -- Prepaid Gift
//                          '006','PREPAID' , -- Prepaid ID Known
//                          '007','CREDIT'  , -- Consumer Credit - Premium
//                          '008','CREDIT'  , -- Consumer Credit � Core
//                          '009','CREDIT'  , -- Private Label Credit Card
//                          '010','CREDIT'  , -- Commercial Credit - Executive Business
//                          '011','CREDIT'  , -- Consumer Credit - Premium Plus                  
//                          'UNKNOWN')                              as card_class
//            from    discover_ardef_table    ard,
//                    country                 c
//            where  substr(:cnFull,1,ard.iin_prefix_length) = ard.iin_prefix                   
//                   and length(:cnFull) between ard.account_number_length_min and ard.account_number_length_max
//                   and c.iso_country_code(+) = ard.issuer_country_code
//            order by ard.iin_prefix_length desc, ard.iin_prefix
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  c.country_code                                as issuer_country_code,\n                  decode(ard.card_product_id,\n                        '001','CREDIT'  , -- Consumer Credit - Rewards\n                        '002','CREDIT'  , -- Commercial Credit\n                        '003','DEBIT'   , -- Consumer Debit\n                        '004','DEBIT'   , -- Commercial Debit\n                        '005','PREPAID' , -- Prepaid Gift\n                        '006','PREPAID' , -- Prepaid ID Known\n                        '007','CREDIT'  , -- Consumer Credit - Premium\n                        '008','CREDIT'  , -- Consumer Credit � Core\n                        '009','CREDIT'  , -- Private Label Credit Card\n                        '010','CREDIT'  , -- Commercial Credit - Executive Business\n                        '011','CREDIT'  , -- Consumer Credit - Premium Plus                  \n                        'UNKNOWN')                              as card_class\n          from    discover_ardef_table    ard,\n                  country                 c\n          where  substr( :1 ,1,ard.iin_prefix_length) = ard.iin_prefix                   \n                 and length( :2 ) between ard.account_number_length_min and ard.account_number_length_max\n                 and c.iso_country_code(+) = ard.issuer_country_code\n          order by ard.iin_prefix_length desc, ard.iin_prefix";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.CardInfoLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,cnFull);
   __sJT_st.setString(2,cnFull);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.CardInfoLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:116^9*/
      }
    
      if ( it != null )
      {
        resultSet = it.getResultSet();
        if ( resultSet.next() )
        {
          cardClass         = resultSet.getString("card_class");
          issuerCountryCode = resultSet.getString("issuer_country_code");
        }
        resultSet.close();
      }
    
      retVal[0] = cardType;
      retVal[1] = TridentTools.encodeCardNumber(cnFull);
      retVal[2] = cardClass;
      retVal[3] = issuerCountryCode;
    }
    catch( Exception e )
    {
      logEntry("getCardInfo()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
    return( retVal );
  }
  
  public static void main( String[] args )
  {
    CardInfoLookup    bean      = null;
    
    try
    {
      bean = new CardInfoLookup();
      bean.connect();
      
      String[] data = bean.getCardInfo(args[0]);
      for ( int i = 0; i < data.length; ++i )
      {
        System.out.print( (i==0) ? "" : "\t" );
        System.out.print( data[i] );
      }
      System.out.println();
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ bean.cleanUp(); } catch( Exception e ){}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) { }
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/