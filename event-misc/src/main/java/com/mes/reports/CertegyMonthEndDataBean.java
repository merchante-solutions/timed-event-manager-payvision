/*@lineinfo:filename=CertegyMonthEndDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CertegyMonthEndDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 10/04/04 12:08p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ComboDateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class CertegyMonthEndDataBean extends ReportSQLJBean
{
  public static final int     RT_NON_CHAIN_SUMMARY      = (RT_USER + 0);
  
  public class SummaryData
  {
    public  double          ChargebackAmount      = 0.0;
    public  double          ChargebackAmountYTD   = 0.0;
    public  int             ChargebackCount       = 0;
    public  int             ChargebackCountYTD    = 0;
    public  double          CreditsAmount         = 0.0;
    public  double          CreditsAmountYTD      = 0.0;
    public  int             CreditsCount          = 0;
    public  int             CreditsCountYTD       = 0;
    public  Date            DateOpened            = null;
    public  boolean         DetailsNext           = false;
    public  long            HierarchyNode         = 0L;
    public  String          MonthsOpen            = null;
    public  long            OrgId                 = 0L;
    public  String          OrgName               = null;
    public  String          Relationship          = null;
    public  double          SalesAmount           = 0.0;
    public  double          SalesAmountYTD        = 0.0;
    public  int             SalesCount            = 0;
    public  int             SalesCountYTD         = 0;
    public  String          SicCode               = "";
    
    // Certegy Hierarchy Fields
    public  String          Corporate             = "";     // group 1
    public  String          Region                = "";     // group 2
    public  String          Principal             = "";     // group 3
    public  String          Associate             = "";     // group 4
    public  String          Chain                 = "";     // association
    
    
    
    public SummaryData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      ChargebackAmount    = resultSet.getDouble("cb_amount");
      ChargebackCount     = resultSet.getInt("cb_count");
      CreditsAmount       = resultSet.getDouble("credits_amount");
      CreditsCount        = resultSet.getInt("credits_count");
      DetailsNext         = ((resultSet.getInt("detail_next") == 0) ? false : true);
      OrgName             = processString(resultSet.getString("org_name"));
      HierarchyNode       = resultSet.getLong("node_id");
      OrgId               = resultSet.getLong("org_num");
      SalesAmount         = resultSet.getDouble("sales_amount");
      SalesCount          = resultSet.getInt("sales_count");
      
      // extract YTD values
      CreditsAmountYTD    = resultSet.getDouble("credits_amount_ytd");
      CreditsCountYTD     = resultSet.getInt("credits_count_ytd");
      SalesAmountYTD      = resultSet.getDouble("sales_amount_ytd");
      SalesCountYTD       = resultSet.getInt("sales_count_ytd");
      ChargebackAmountYTD = resultSet.getDouble("cb_amount_ytd");
      ChargebackCountYTD  = resultSet.getInt("cb_count_ytd");
      
      if ( isNodeAssociation( HierarchyNode ) ||
           isNodeMerchant( HierarchyNode ) )
      {
        loadCertegyHierarchy(this);
        
        if ( isNodeMerchant( HierarchyNode ) )
        {
          // every row will have the data
          DateOpened  = resultSet.getDate("date_opened");
          SicCode     = processString(resultSet.getString("sic_code"));  
          setMonthsOpen(processString(resultSet.getString("months_open")));
        }
        else  // association
        {
          // load the sic and date opened from 
          // the first account based on date opened
          loadFirstMerchantData(this);
        }
      }        
    }      
    
    public void setMonthsOpen( String val )
    {
      MonthsOpen = val;
      
      // Certegy wants the min value to be 1
      if ( MonthsOpen.equals("0") )
      {
        MonthsOpen = "1";
      }
    }
  }
  
  public static class CertegyFilterTable extends DropDownTable
  {
    public CertegyFilterTable()
    {
      addElement("",        "Full Service");
      addElement("002,116", "IPAY");
    }
  }    
  
  
  protected boolean           CertegyHierarchyLoaded    = false;
  protected int               FilterCorp                = 0;
  protected int               FilterRegion              = 0;
  protected String            FilterString              = "";
  
  public CertegyMonthEndDataBean( )
  {
    super(true);   // enable field bean support
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup        fgroup;
    Field             field;
    
    super.createFields(request);
    
    fgroup = (FieldGroup)getField("searchFields");
    fgroup.deleteField("beginDate");
    fgroup.deleteField("endDate");
    fgroup.deleteField("zip2Node");
    
    field = new ComboDateField("beginDate", "Report Begin Date", false, false, 2000, false);
    ((ComboDateField)field).setExpansionFlag(false);
    fgroup.add(field);
    
    field = new ComboDateField("endDate", "Report End Date", false, false, 2000, false);
    ((ComboDateField)field).setExpansionFlag(false);
    fgroup.add(field);
    
    field = new ComboDateField("accountDate", "Accounts Added on/after Date", true, false,1970, false);
    ((ComboDateField)field).setExpansionFlag(false);
    fgroup.add(field);
    
    field = new DropDownField("filter", "Filter", new CertegyFilterTable(), true);
    fgroup.add(field);
  }
  
  public void encodeFilterParams( StringBuffer buffer )
  {
    buffer.append( ( ( buffer.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
    buffer.append( "filter=" );
    buffer.append( FilterString );
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
  
    if ( isCertegyHierarchyLoaded() )
    {
      line.append("\"Prin\",");
      line.append("\"Assc\",");
      line.append("\"Chain\",");
      line.append("\"Relationship\",");
    }
  
    line.append("\"Org Id\",");
    line.append("\"DBA Name\",");
    line.append("\"MCC\",");
    line.append("\"Date Opened\",");
    line.append("\"Months Open\",");
    line.append("\"Sales Count\",");
    line.append("\"Sales Amount\",");
    line.append("\"Sales Count YTD\",");
    line.append("\"Sales Amount YTD\",");
    line.append("\"Credits Count\",");
    line.append("\"Credits Amount\",");
    line.append("\"Credits Count YTD\",");
    line.append("\"Credits Amount YTD\",");
    line.append("\"Chargeback Count\",");
    line.append("\"Chargeback Amount\",");
    line.append("\"Chargeback Count YTD\",");
    line.append("\"Chargeback Amount YTD\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    SummaryData           record    = (SummaryData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    
    if ( isCertegyHierarchyLoaded() )
    {
      line.append(record.Principal);
      line.append(",");
      line.append(record.Associate);
      line.append(",");
      line.append(record.Chain);
      line.append(",\"");
      line.append(record.Relationship);
      line.append("\",");
    }
    
    line.append(encodeHierarchyNode(record.HierarchyNode));
    line.append(",\"");
    line.append(record.OrgName);
    line.append("\",");
    line.append(record.SicCode);
    line.append(",");
    line.append(DateTimeFormatter.getFormattedDate(record.DateOpened,"MM/dd/yyyy"));
    line.append(",");
    line.append(record.MonthsOpen);
    line.append(",");
    line.append(record.SalesCount);
    line.append(",");
    line.append(record.SalesAmount);
    line.append(",");
    line.append(record.SalesCountYTD);
    line.append(",");
    line.append(record.SalesAmountYTD);
    line.append(",");
    line.append(record.CreditsCount);
    line.append(",");
    line.append(record.CreditsAmount);
    line.append(",");
    line.append(record.CreditsCountYTD);
    line.append(",");
    line.append(record.CreditsAmountYTD);
    line.append(",");
    line.append(record.ChargebackCount);
    line.append(",");
    line.append(record.ChargebackAmount);
    line.append(",");
    line.append(record.ChargebackCountYTD);
    line.append(",");
    line.append(record.ChargebackAmountYTD);
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long childNodeId, Date beginDate, Date endDate )
  {
    super.encodeNodeUrl(buffer,childNodeId,beginDate,endDate);
    
    // add the account date if necessary
    Field f = getField("accountDate");
    if ( f != null )
    {
      buffer.append("&");
      buffer.append( ((ComboDateField)f).getHtmlDate() );
    }      
  }
  
  public String getDisplayName( )
  {
    StringBuffer      retVal    = new StringBuffer(super.getDisplayName());
    
    if ( ReportType == RT_DETAILS )
    {
      if ( isNodeAssociation() == true )
      {
        retVal.append(" (Chain)");
      }  
      else
      {
        retVal.append(" (Non-Chain)");
      }
    }
    return(retVal.toString());
  }
  
  public String getDownloadFilenameBase()
  {
    Date                    tempDate  = null;
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_certegy_me_");
    
    // build the first date into the filename
    tempDate = getReportDateBegin();
    filename.append( DateTimeFormatter.getFormattedDate(tempDate,"MMddyy") );
    if ( tempDate.equals(getReportDateEnd()) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(getReportDateEnd(),"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isCertegyHierarchyLoaded()
  {
    long          nodeId      = getReportHierarchyNode();
    boolean       retVal      = true;
    
    if ( nodeId == 100000000L )
    {
      retVal = false;
    }
         
    return(retVal);
  }
  
  public boolean isCurrentFilter( String filterString )
  {
    boolean     retVal    = false;
    
    if ( FilterString.equals(filterString) )
    {
      retVal = true;
    }
    return( retVal );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  protected void loadCertegyHierarchy( SummaryData row )
  {
    try
    {
      if ( isNodeMerchant( row.HierarchyNode ) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:375^9*/

//  ************************************************************
//  #sql [Ctx] { select  lpad(ch.corporate,3,'0'),
//                    lpad(ch.principal,3,'0'),
//                    lpad(ch.region   ,3,'0'),
//                    lpad(ch.associate,3,'0'),
//                    lpad(ch.chain    ,3,'0')        
//            
//            from    mif                     mf,
//                    mif_certegy_hierarchy   ch
//            where   mf.merchant_number = :row.HierarchyNode and
//                    ch.association = substr( mf.association_node,5 )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  lpad(ch.corporate,3,'0'),\n                  lpad(ch.principal,3,'0'),\n                  lpad(ch.region   ,3,'0'),\n                  lpad(ch.associate,3,'0'),\n                  lpad(ch.chain    ,3,'0')        \n           \n          from    mif                     mf,\n                  mif_certegy_hierarchy   ch\n          where   mf.merchant_number =  :1  and\n                  ch.association = substr( mf.association_node,5 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.CertegyMonthEndDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,row.HierarchyNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 5) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(5,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   row.Corporate = (String)__sJT_rs.getString(1);
   row.Principal = (String)__sJT_rs.getString(2);
   row.Region = (String)__sJT_rs.getString(3);
   row.Associate = (String)__sJT_rs.getString(4);
   row.Chain = (String)__sJT_rs.getString(5);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:391^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:393^9*/

//  ************************************************************
//  #sql [Ctx] { select  o.org_name      
//            from    mif           mf,
//                    groups        g,
//                    organization  o
//            where   mf.merchant_number = :row.HierarchyNode and
//                    g.assoc_number = mf.association_node and
//                    o.org_group = g.group_3
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  o.org_name       \n          from    mif           mf,\n                  groups        g,\n                  organization  o\n          where   mf.merchant_number =  :1  and\n                  g.assoc_number = mf.association_node and\n                  o.org_group = g.group_3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.CertegyMonthEndDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,row.HierarchyNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   row.Relationship = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:402^9*/
        
        CertegyHierarchyLoaded = true;
      }
      else if ( isNodeAssociation( row.HierarchyNode ) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:408^9*/

//  ************************************************************
//  #sql [Ctx] { select  lpad(ch.corporate,3,'0'),
//                    lpad(ch.principal,3,'0'),
//                    lpad(ch.region   ,3,'0'),
//                    lpad(ch.associate,3,'0'),
//                    lpad(ch.chain    ,3,'0')        
//            
//            from    mif_certegy_hierarchy   ch
//            where   ch.association = substr( :row.HierarchyNode, 5 )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  lpad(ch.corporate,3,'0'),\n                  lpad(ch.principal,3,'0'),\n                  lpad(ch.region   ,3,'0'),\n                  lpad(ch.associate,3,'0'),\n                  lpad(ch.chain    ,3,'0')        \n           \n          from    mif_certegy_hierarchy   ch\n          where   ch.association = substr(  :1 , 5 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.CertegyMonthEndDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,row.HierarchyNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 5) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(5,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   row.Corporate = (String)__sJT_rs.getString(1);
   row.Principal = (String)__sJT_rs.getString(2);
   row.Region = (String)__sJT_rs.getString(3);
   row.Associate = (String)__sJT_rs.getString(4);
   row.Chain = (String)__sJT_rs.getString(5);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:422^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:424^9*/

//  ************************************************************
//  #sql [Ctx] { select  o.org_name      
//            from    groups        g,
//                    organization  o
//            where   g.assoc_number = :row.HierarchyNode and
//                    o.org_group = g.group_3                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  o.org_name       \n          from    groups        g,\n                  organization  o\n          where   g.assoc_number =  :1  and\n                  o.org_group = g.group_3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.CertegyMonthEndDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,row.HierarchyNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   row.Relationship = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:431^9*/
        
        CertegyHierarchyLoaded = true;
      }
    }
    catch( Exception e )
    {
      logEntry("loadCertegyHierarchy()",e.toString());
    }
  }
  
  public void loadData( )
  {
    Date    beginDate = ((ComboDateField)getField("beginDate")).getSqlDate();
    Date    endDate   = ((ComboDateField)getField("endDate")).getSqlDate();
    
    loadData( getLong("nodeId"), beginDate, endDate );
  }
  
  public void loadData( long nodeId, Date beginDate, Date endDate )
  {
    Date                  accountDate = null;
    int                   assoc       = 0;
    int                   chain       = 0;
    ResultSetIterator     it          = null;
    ResultSet             resultSet   = null;
    boolean               skip        = false;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      accountDate = ((ComboDateField)getField("accountDate")).getSqlDate();
      
      if ( ReportType == RT_DETAILS )
      {
        assoc = (isNodeAssociation( nodeId ) ? 1 : 0);
        
        /*@lineinfo:generated-code*//*@lineinfo:470^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num                                   as org_num,
//                    mf.merchant_number                          as node_id,
//                    mf.dba_name                                 as org_name,
//                    0                                           as detail_next,
//                    mf.sic_code                                 as sic_code,
//                    decode(ch.chain,0,0,1)                      as is_chain,
//  --                  sc.merchant_type                            as sic_desc,
//                    to_date(nvl(mf.date_opened,'010100'),'mmddrr') as date_opened,
//                    round(months_between(last_day(:beginDate),greatest((last_day(:beginDate)-366),to_date(nvl(mf.date_opened,'010100'),'mmddrr'))),0)
//                                                                as months_open,
//                    sum( decode( sm.active_date,
//                                 :beginDate, nvl(sm.vmc_credits_count,0),
//                                 0
//                               ) )                              as credits_count, 
//                    sum( decode( sm.active_date,
//                                 :beginDate, nvl(sm.vmc_credits_amount,0),
//                                 0
//                               ) )                              as credits_amount, 
//                    sum( decode( sm.active_date,
//                                 :beginDate, nvl(sm.vmc_sales_count,0),
//                                 0
//                               ) )                              as sales_count, 
//                    sum( decode( sm.active_date,
//                                 :beginDate, nvl(sm.vmc_sales_amount,0),
//                                 0
//                               ) )                              as sales_amount,
//                    sum( decode( sm.active_date,
//                                 :beginDate, nvl(sm.incoming_chargeback_count,0),
//                                 0
//                               ) )                              as cb_count,
//                    sum( decode( sm.active_date,
//                                 :beginDate, nvl(sm.incoming_chargeback_amount,0),
//                                 0
//                               ) )                              as cb_amount,
//                    sum( nvl(sm.vmc_credits_count,0) )          as credits_count_ytd, 
//                    sum( nvl(sm.vmc_credits_amount,0) )         as credits_amount_ytd, 
//                    sum( nvl(sm.vmc_sales_count,0) )            as sales_count_ytd, 
//                    sum( nvl(sm.vmc_sales_amount,0) )           as sales_amount_ytd,
//                    sum( nvl(sm.incoming_chargeback_count,0) )  as cb_count_ytd,
//                    sum( nvl(sm.incoming_chargeback_amount,0) ) as cb_amount_ytd
//            from    t_hierarchy                     th,
//                    mif                             mf,
//                    monthly_extract_summary         sm, 
//                    organization                    o,
//                    mif_certegy_hierarchy           ch
//  --                  ,
//  --                  sic_codes2                      sc
//            where   th.hier_type = 1 and
//                    th.ancestor = :nodeId and 
//                    th.entity_type = 4 and
//                    mf.association_node = th.descendent and
//                    to_date(nvl(mf.date_opened,'010100'),'mmddrr') >= :accountDate and
//                    nvl(mf.date_last_activity,'01-JAN-2000') >= (:beginDate-45) and
//                    sm.merchant_number = mf.merchant_number and
//                    sm.active_date between trunc((last_day(:beginDate)-360),'month') and
//                                          :beginDate and      
//                    o.org_group = mf.merchant_number and
//                    ch.association = substr(mf.association_node,5) and
//                    ( :FilterCorp = 0 or ch.corporate = :FilterCorp ) and
//                    ( :FilterRegion = 0 or ch.region = :FilterRegion )
//  --                  and
//  --                  sc.sic_code(+) = mf.sic_code
//            group by o.org_num, mf.merchant_number, mf.dba_name, 
//                     mf.sic_code, nvl(mf.date_opened,'010100'),
//                     decode(ch.chain,0,0,1)
//  --                   , sc.merchant_type
//            order by mf.dba_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num                                   as org_num,\n                  mf.merchant_number                          as node_id,\n                  mf.dba_name                                 as org_name,\n                  0                                           as detail_next,\n                  mf.sic_code                                 as sic_code,\n                  decode(ch.chain,0,0,1)                      as is_chain,\n--                  sc.merchant_type                            as sic_desc,\n                  to_date(nvl(mf.date_opened,'010100'),'mmddrr') as date_opened,\n                  round(months_between(last_day( :1 ),greatest((last_day( :2 )-366),to_date(nvl(mf.date_opened,'010100'),'mmddrr'))),0)\n                                                              as months_open,\n                  sum( decode( sm.active_date,\n                                :3 , nvl(sm.vmc_credits_count,0),\n                               0\n                             ) )                              as credits_count, \n                  sum( decode( sm.active_date,\n                                :4 , nvl(sm.vmc_credits_amount,0),\n                               0\n                             ) )                              as credits_amount, \n                  sum( decode( sm.active_date,\n                                :5 , nvl(sm.vmc_sales_count,0),\n                               0\n                             ) )                              as sales_count, \n                  sum( decode( sm.active_date,\n                                :6 , nvl(sm.vmc_sales_amount,0),\n                               0\n                             ) )                              as sales_amount,\n                  sum( decode( sm.active_date,\n                                :7 , nvl(sm.incoming_chargeback_count,0),\n                               0\n                             ) )                              as cb_count,\n                  sum( decode( sm.active_date,\n                                :8 , nvl(sm.incoming_chargeback_amount,0),\n                               0\n                             ) )                              as cb_amount,\n                  sum( nvl(sm.vmc_credits_count,0) )          as credits_count_ytd, \n                  sum( nvl(sm.vmc_credits_amount,0) )         as credits_amount_ytd, \n                  sum( nvl(sm.vmc_sales_count,0) )            as sales_count_ytd, \n                  sum( nvl(sm.vmc_sales_amount,0) )           as sales_amount_ytd,\n                  sum( nvl(sm.incoming_chargeback_count,0) )  as cb_count_ytd,\n                  sum( nvl(sm.incoming_chargeback_amount,0) ) as cb_amount_ytd\n          from    t_hierarchy                     th,\n                  mif                             mf,\n                  monthly_extract_summary         sm, \n                  organization                    o,\n                  mif_certegy_hierarchy           ch\n--                  ,\n--                  sic_codes2                      sc\n          where   th.hier_type = 1 and\n                  th.ancestor =  :9  and \n                  th.entity_type = 4 and\n                  mf.association_node = th.descendent and\n                  to_date(nvl(mf.date_opened,'010100'),'mmddrr') >=  :10  and\n                  nvl(mf.date_last_activity,'01-JAN-2000') >= ( :11 -45) and\n                  sm.merchant_number = mf.merchant_number and\n                  sm.active_date between trunc((last_day( :12 )-360),'month') and\n                                         :13  and      \n                  o.org_group = mf.merchant_number and\n                  ch.association = substr(mf.association_node,5) and\n                  (  :14  = 0 or ch.corporate =  :15  ) and\n                  (  :16  = 0 or ch.region =  :17  )\n--                  and\n--                  sc.sic_code(+) = mf.sic_code\n          group by o.org_num, mf.merchant_number, mf.dba_name, \n                   mf.sic_code, nvl(mf.date_opened,'010100'),\n                   decode(ch.chain,0,0,1)\n--                   , sc.merchant_type\n          order by mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.CertegyMonthEndDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,beginDate);
   __sJT_st.setLong(9,nodeId);
   __sJT_st.setDate(10,accountDate);
   __sJT_st.setDate(11,beginDate);
   __sJT_st.setDate(12,beginDate);
   __sJT_st.setDate(13,beginDate);
   __sJT_st.setInt(14,FilterCorp);
   __sJT_st.setInt(15,FilterCorp);
   __sJT_st.setInt(16,FilterRegion);
   __sJT_st.setInt(17,FilterRegion);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.CertegyMonthEndDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:539^9*/
      }
      else if ( nodeId == 100000000L )    // certegy top level
      {
        /*@lineinfo:generated-code*//*@lineinfo:543^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num                                   as org_num,
//                    o.org_group                                 as node_id,
//                    --o.org_name ||
//                    decode(decode(ch.chain,0,0,1),
//                           0,' Non-Chain Merchants',
//                           ' Chain Merchants')                  as org_name,
//                    (1-decode(ch.chain,0,0,1))                  as detail_next,
//                    -1                                          as is_chain,
//                    sum( decode( sm.active_date,
//                                 :beginDate, nvl(sm.vmc_credits_count,0),
//                                 0
//                               ) )                              as credits_count, 
//                    sum( decode( sm.active_date,
//                                 :beginDate, nvl(sm.vmc_credits_amount,0),
//                                 0
//                               ) )                              as credits_amount, 
//                    sum( decode( sm.active_date,
//                                 :beginDate, nvl(sm.vmc_sales_count,0),
//                                 0
//                               ) )                              as sales_count, 
//                    sum( decode( sm.active_date,
//                                 :beginDate, nvl(sm.vmc_sales_amount,0),
//                                 0
//                               ) )                              as sales_amount,
//                    sum( decode( sm.active_date,
//                                 :beginDate, nvl(sm.incoming_chargeback_count,0),
//                                 0
//                               ) )                              as cb_count,
//                    sum( decode( sm.active_date,
//                                 :beginDate, nvl(sm.incoming_chargeback_amount,0),
//                                 0
//                               ) )                              as cb_amount,
//                    sum( nvl(sm.vmc_credits_count,0) )          as credits_count_ytd, 
//                    sum( nvl(sm.vmc_credits_amount,0) )         as credits_amount_ytd, 
//                    sum( nvl(sm.vmc_sales_count,0) )            as sales_count_ytd, 
//                    sum( nvl(sm.vmc_sales_amount,0) )           as sales_amount_ytd,
//                    sum( nvl(sm.incoming_chargeback_count,0) )  as cb_count_ytd,
//                    sum( nvl(sm.incoming_chargeback_amount,0) ) as cb_amount_ytd
//            from    t_hierarchy                     th, 
//                    organization                    o, 
//                    monthly_extract_summary         sm,
//                    mif                             mf,
//                    mif_certegy_hierarchy           ch
//            where   th.hier_type = 1 and 
//                    th.ancestor in ( 1000999002, 1000999003 ) and
//                    th.entity_type = 4 and 
//                    mf.association_node = th.descendent and
//                    to_date(nvl(mf.date_opened,'010100'),'mmddrr') >= :accountDate and
//                    nvl(mf.date_last_activity,'01-JAN-2000') >= (:beginDate-45) and
//                    sm.merchant_number = mf.merchant_number and
//                    sm.active_date between trunc((last_day(:beginDate)-360),'month') and
//                                           :beginDate and      
//                    o.org_group = th.ancestor and
//                    ch.association(+) = substr(mf.association_node,5) and
//                    ( :FilterCorp = 0 or ch.corporate = :FilterCorp ) and
//                    ( :FilterRegion = 0 or ch.region = :FilterRegion )
//            group by  decode(ch.chain,0,0,1) ,o.org_num, o.org_group, o.org_name      
//            order by org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num                                   as org_num,\n                  o.org_group                                 as node_id,\n                  --o.org_name ||\n                  decode(decode(ch.chain,0,0,1),\n                         0,' Non-Chain Merchants',\n                         ' Chain Merchants')                  as org_name,\n                  (1-decode(ch.chain,0,0,1))                  as detail_next,\n                  -1                                          as is_chain,\n                  sum( decode( sm.active_date,\n                                :1 , nvl(sm.vmc_credits_count,0),\n                               0\n                             ) )                              as credits_count, \n                  sum( decode( sm.active_date,\n                                :2 , nvl(sm.vmc_credits_amount,0),\n                               0\n                             ) )                              as credits_amount, \n                  sum( decode( sm.active_date,\n                                :3 , nvl(sm.vmc_sales_count,0),\n                               0\n                             ) )                              as sales_count, \n                  sum( decode( sm.active_date,\n                                :4 , nvl(sm.vmc_sales_amount,0),\n                               0\n                             ) )                              as sales_amount,\n                  sum( decode( sm.active_date,\n                                :5 , nvl(sm.incoming_chargeback_count,0),\n                               0\n                             ) )                              as cb_count,\n                  sum( decode( sm.active_date,\n                                :6 , nvl(sm.incoming_chargeback_amount,0),\n                               0\n                             ) )                              as cb_amount,\n                  sum( nvl(sm.vmc_credits_count,0) )          as credits_count_ytd, \n                  sum( nvl(sm.vmc_credits_amount,0) )         as credits_amount_ytd, \n                  sum( nvl(sm.vmc_sales_count,0) )            as sales_count_ytd, \n                  sum( nvl(sm.vmc_sales_amount,0) )           as sales_amount_ytd,\n                  sum( nvl(sm.incoming_chargeback_count,0) )  as cb_count_ytd,\n                  sum( nvl(sm.incoming_chargeback_amount,0) ) as cb_amount_ytd\n          from    t_hierarchy                     th, \n                  organization                    o, \n                  monthly_extract_summary         sm,\n                  mif                             mf,\n                  mif_certegy_hierarchy           ch\n          where   th.hier_type = 1 and \n                  th.ancestor in ( 1000999002, 1000999003 ) and\n                  th.entity_type = 4 and \n                  mf.association_node = th.descendent and\n                  to_date(nvl(mf.date_opened,'010100'),'mmddrr') >=  :7  and\n                  nvl(mf.date_last_activity,'01-JAN-2000') >= ( :8 -45) and\n                  sm.merchant_number = mf.merchant_number and\n                  sm.active_date between trunc((last_day( :9 )-360),'month') and\n                                          :10  and      \n                  o.org_group = th.ancestor and\n                  ch.association(+) = substr(mf.association_node,5) and\n                  (  :11  = 0 or ch.corporate =  :12  ) and\n                  (  :13  = 0 or ch.region =  :14  )\n          group by  decode(ch.chain,0,0,1) ,o.org_num, o.org_group, o.org_name      \n          order by org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.CertegyMonthEndDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,accountDate);
   __sJT_st.setDate(8,beginDate);
   __sJT_st.setDate(9,beginDate);
   __sJT_st.setDate(10,beginDate);
   __sJT_st.setInt(11,FilterCorp);
   __sJT_st.setInt(12,FilterCorp);
   __sJT_st.setInt(13,FilterRegion);
   __sJT_st.setInt(14,FilterRegion);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.CertegyMonthEndDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:603^9*/
      }
      else  // chain summary
      {
        if ( ReportType == RT_NON_CHAIN_SUMMARY )
        {
          // show non-chains summarized by the certegy specified groups
          /*@lineinfo:generated-code*//*@lineinfo:610^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num                                   as org_num,
//                      o.org_group                                 as node_id,
//                      o.org_name                                  as org_name,
//                      1                                           as detail_next,
//                      decode(ch.chain,0,0,1)                      as is_chain,
//                      sum( decode( sm.active_date,
//                                   :beginDate, nvl(sm.vmc_credits_count,0),
//                                   0
//                                 ) )                              as credits_count, 
//                      sum( decode( sm.active_date,
//                                   :beginDate, nvl(sm.vmc_credits_amount,0),
//                                   0
//                                 ) )                              as credits_amount, 
//                      sum( decode( sm.active_date,
//                                   :beginDate, nvl(sm.vmc_sales_count,0),
//                                   0
//                                 ) )                              as sales_count, 
//                      sum( decode( sm.active_date,
//                                   :beginDate, nvl(sm.vmc_sales_amount,0),
//                                   0
//                                 ) )                              as sales_amount,
//                      sum( decode( sm.active_date,
//                                   :beginDate, nvl(sm.incoming_chargeback_count,0),
//                                   0
//                                 ) )                              as cb_count,
//                      sum( decode( sm.active_date,
//                                   :beginDate, nvl(sm.incoming_chargeback_amount,0),
//                                   0
//                                 ) )                              as cb_amount,
//                      sum( nvl(sm.vmc_credits_count,0) )          as credits_count_ytd, 
//                      sum( nvl(sm.vmc_credits_amount,0) )         as credits_amount_ytd, 
//                      sum( nvl(sm.vmc_sales_count,0) )            as sales_count_ytd, 
//                      sum( nvl(sm.vmc_sales_amount,0) )           as sales_amount_ytd,
//                      sum( nvl(sm.incoming_chargeback_count,0) )  as cb_count_ytd,
//                      sum( nvl(sm.incoming_chargeback_amount,0) ) as cb_amount_ytd
//              from    t_hierarchy                     th, 
//                      organization                    o, 
//                      mif                             mf,
//                      monthly_extract_summary         sm,
//                      mif_certegy_hierarchy           ch
//              where   th.hier_type = 1 and 
//                      (
//                        ( 
//                          :nodeId = 1000999002 and
//                          th.ancestor in (1000100303,
//                                          1000101830,
//                                          1000110299,
//                                          1000118738,
//                                          1000118768,
//                                          1000118840) 
//                        ) or
//                        ( 
//                          :nodeId = 1000999003 and
//                          th.ancestor in (1000100125,
//                                          1000100665,
//                                          1000118854)
//                        )
//                      ) and
//                      th.entity_type = 4 and 
//                      mf.association_node = th.descendent and
//                      to_date(nvl(mf.date_opened,'010100'),'mmddrr') >= :accountDate and
//                      nvl(mf.date_last_activity,'01-JAN-2000') >= (:beginDate-45) and
//                      sm.merchant_number = mf.merchant_number and
//                      sm.active_date between trunc((last_day(:beginDate)-360),'month') and
//                                             :beginDate and      
//                      o.org_group = th.ancestor and
//                      ch.association = substr(mf.association_node,5) and
//                      ( :FilterCorp = 0 or ch.corporate = :FilterCorp ) and
//                      ( :FilterRegion = 0 or ch.region = :FilterRegion )
//              group by o.org_num, o.org_group, o.org_name,
//                      decode(ch.chain,0,0,1)
//              order by o.org_name        
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num                                   as org_num,\n                    o.org_group                                 as node_id,\n                    o.org_name                                  as org_name,\n                    1                                           as detail_next,\n                    decode(ch.chain,0,0,1)                      as is_chain,\n                    sum( decode( sm.active_date,\n                                  :1 , nvl(sm.vmc_credits_count,0),\n                                 0\n                               ) )                              as credits_count, \n                    sum( decode( sm.active_date,\n                                  :2 , nvl(sm.vmc_credits_amount,0),\n                                 0\n                               ) )                              as credits_amount, \n                    sum( decode( sm.active_date,\n                                  :3 , nvl(sm.vmc_sales_count,0),\n                                 0\n                               ) )                              as sales_count, \n                    sum( decode( sm.active_date,\n                                  :4 , nvl(sm.vmc_sales_amount,0),\n                                 0\n                               ) )                              as sales_amount,\n                    sum( decode( sm.active_date,\n                                  :5 , nvl(sm.incoming_chargeback_count,0),\n                                 0\n                               ) )                              as cb_count,\n                    sum( decode( sm.active_date,\n                                  :6 , nvl(sm.incoming_chargeback_amount,0),\n                                 0\n                               ) )                              as cb_amount,\n                    sum( nvl(sm.vmc_credits_count,0) )          as credits_count_ytd, \n                    sum( nvl(sm.vmc_credits_amount,0) )         as credits_amount_ytd, \n                    sum( nvl(sm.vmc_sales_count,0) )            as sales_count_ytd, \n                    sum( nvl(sm.vmc_sales_amount,0) )           as sales_amount_ytd,\n                    sum( nvl(sm.incoming_chargeback_count,0) )  as cb_count_ytd,\n                    sum( nvl(sm.incoming_chargeback_amount,0) ) as cb_amount_ytd\n            from    t_hierarchy                     th, \n                    organization                    o, \n                    mif                             mf,\n                    monthly_extract_summary         sm,\n                    mif_certegy_hierarchy           ch\n            where   th.hier_type = 1 and \n                    (\n                      ( \n                         :7  = 1000999002 and\n                        th.ancestor in (1000100303,\n                                        1000101830,\n                                        1000110299,\n                                        1000118738,\n                                        1000118768,\n                                        1000118840) \n                      ) or\n                      ( \n                         :8  = 1000999003 and\n                        th.ancestor in (1000100125,\n                                        1000100665,\n                                        1000118854)\n                      )\n                    ) and\n                    th.entity_type = 4 and \n                    mf.association_node = th.descendent and\n                    to_date(nvl(mf.date_opened,'010100'),'mmddrr') >=  :9  and\n                    nvl(mf.date_last_activity,'01-JAN-2000') >= ( :10 -45) and\n                    sm.merchant_number = mf.merchant_number and\n                    sm.active_date between trunc((last_day( :11 )-360),'month') and\n                                            :12  and      \n                    o.org_group = th.ancestor and\n                    ch.association = substr(mf.association_node,5) and\n                    (  :13  = 0 or ch.corporate =  :14  ) and\n                    (  :15  = 0 or ch.region =  :16  )\n            group by o.org_num, o.org_group, o.org_name,\n                    decode(ch.chain,0,0,1)\n            order by o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.CertegyMonthEndDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setLong(7,nodeId);
   __sJT_st.setLong(8,nodeId);
   __sJT_st.setDate(9,accountDate);
   __sJT_st.setDate(10,beginDate);
   __sJT_st.setDate(11,beginDate);
   __sJT_st.setDate(12,beginDate);
   __sJT_st.setInt(13,FilterCorp);
   __sJT_st.setInt(14,FilterCorp);
   __sJT_st.setInt(15,FilterRegion);
   __sJT_st.setInt(16,FilterRegion);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.CertegyMonthEndDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:684^11*/
        }
        else    // show chains summarized by association number
        {
          /*@lineinfo:generated-code*//*@lineinfo:688^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num                                   as org_num,
//                      o.org_group                                 as node_id,
//                      o.org_name                                  as org_name,
//                      1                                           as detail_next,
//                      decode(ch.chain,0,0,1)                      as is_chain,
//                      sum( decode( sm.active_date,
//                                   :beginDate, nvl(sm.vmc_credits_count,0),
//                                   0
//                                 ) )                              as credits_count, 
//                      sum( decode( sm.active_date,
//                                   :beginDate, nvl(sm.vmc_credits_amount,0),
//                                   0
//                                 ) )                              as credits_amount, 
//                      sum( decode( sm.active_date,
//                                   :beginDate, nvl(sm.vmc_sales_count,0),
//                                   0
//                                 ) )                              as sales_count, 
//                      sum( decode( sm.active_date,
//                                   :beginDate, nvl(sm.vmc_sales_amount,0),
//                                   0
//                                 ) )                              as sales_amount,
//                      sum( decode( sm.active_date,
//                                   :beginDate, nvl(sm.incoming_chargeback_count,0),
//                                   0
//                                 ) )                              as cb_count,
//                      sum( decode( sm.active_date,
//                                   :beginDate, nvl(sm.incoming_chargeback_amount,0),
//                                   0
//                                 ) )                              as cb_amount,
//                      sum( nvl(sm.vmc_credits_count,0) )          as credits_count_ytd, 
//                      sum( nvl(sm.vmc_credits_amount,0) )         as credits_amount_ytd, 
//                      sum( nvl(sm.vmc_sales_count,0) )            as sales_count_ytd, 
//                      sum( nvl(sm.vmc_sales_amount,0) )           as sales_amount_ytd,
//                      sum( nvl(sm.incoming_chargeback_count,0) )  as cb_count_ytd,
//                      sum( nvl(sm.incoming_chargeback_amount,0) ) as cb_amount_ytd
//              from    t_hierarchy                     th, 
//                      organization                    o, 
//                      mif                             mf,
//                      monthly_extract_summary         sm,
//                      mif_certegy_hierarchy           ch
//              where   th.hier_type = 1 and 
//                      th.ancestor = :nodeId and
//                      th.entity_type = 4 and 
//                      mf.association_node = th.descendent and
//                      to_date(nvl(mf.date_opened,'010100'),'mmddrr') >= :accountDate and
//                      nvl(mf.date_last_activity,'01-JAN-2000') >= (:beginDate-45) and
//                      sm.merchant_number = mf.merchant_number and
//                      sm.active_date between trunc((last_day(:beginDate)-360),'month') and
//                                             :beginDate and      
//                      o.org_group = mf.association_node and
//                      ch.association = substr(mf.association_node,5) and
//                      ( :FilterCorp = 0 or ch.corporate = :FilterCorp ) and
//                      ( :FilterRegion = 0 or ch.region = :FilterRegion )
//              group by o.org_num, o.org_group, o.org_name,
//                       decode(ch.chain,0,0,1)
//              order by o.org_name        
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num                                   as org_num,\n                    o.org_group                                 as node_id,\n                    o.org_name                                  as org_name,\n                    1                                           as detail_next,\n                    decode(ch.chain,0,0,1)                      as is_chain,\n                    sum( decode( sm.active_date,\n                                  :1 , nvl(sm.vmc_credits_count,0),\n                                 0\n                               ) )                              as credits_count, \n                    sum( decode( sm.active_date,\n                                  :2 , nvl(sm.vmc_credits_amount,0),\n                                 0\n                               ) )                              as credits_amount, \n                    sum( decode( sm.active_date,\n                                  :3 , nvl(sm.vmc_sales_count,0),\n                                 0\n                               ) )                              as sales_count, \n                    sum( decode( sm.active_date,\n                                  :4 , nvl(sm.vmc_sales_amount,0),\n                                 0\n                               ) )                              as sales_amount,\n                    sum( decode( sm.active_date,\n                                  :5 , nvl(sm.incoming_chargeback_count,0),\n                                 0\n                               ) )                              as cb_count,\n                    sum( decode( sm.active_date,\n                                  :6 , nvl(sm.incoming_chargeback_amount,0),\n                                 0\n                               ) )                              as cb_amount,\n                    sum( nvl(sm.vmc_credits_count,0) )          as credits_count_ytd, \n                    sum( nvl(sm.vmc_credits_amount,0) )         as credits_amount_ytd, \n                    sum( nvl(sm.vmc_sales_count,0) )            as sales_count_ytd, \n                    sum( nvl(sm.vmc_sales_amount,0) )           as sales_amount_ytd,\n                    sum( nvl(sm.incoming_chargeback_count,0) )  as cb_count_ytd,\n                    sum( nvl(sm.incoming_chargeback_amount,0) ) as cb_amount_ytd\n            from    t_hierarchy                     th, \n                    organization                    o, \n                    mif                             mf,\n                    monthly_extract_summary         sm,\n                    mif_certegy_hierarchy           ch\n            where   th.hier_type = 1 and \n                    th.ancestor =  :7  and\n                    th.entity_type = 4 and \n                    mf.association_node = th.descendent and\n                    to_date(nvl(mf.date_opened,'010100'),'mmddrr') >=  :8  and\n                    nvl(mf.date_last_activity,'01-JAN-2000') >= ( :9 -45) and\n                    sm.merchant_number = mf.merchant_number and\n                    sm.active_date between trunc((last_day( :10 )-360),'month') and\n                                            :11  and      \n                    o.org_group = mf.association_node and\n                    ch.association = substr(mf.association_node,5) and\n                    (  :12  = 0 or ch.corporate =  :13  ) and\n                    (  :14  = 0 or ch.region =  :15  )\n            group by o.org_num, o.org_group, o.org_name,\n                     decode(ch.chain,0,0,1)\n            order by o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.CertegyMonthEndDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setLong(7,nodeId);
   __sJT_st.setDate(8,accountDate);
   __sJT_st.setDate(9,beginDate);
   __sJT_st.setDate(10,beginDate);
   __sJT_st.setDate(11,beginDate);
   __sJT_st.setInt(12,FilterCorp);
   __sJT_st.setInt(13,FilterCorp);
   __sJT_st.setInt(14,FilterRegion);
   __sJT_st.setInt(15,FilterRegion);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.CertegyMonthEndDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:746^11*/
        }
      }        
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        chain = resultSet.getInt("is_chain");
        skip  = false;
        
        switch( ReportType )
        {
          case RT_DETAILS:
            if ( ( chain != 0 && assoc == 0 ) ||
                 ( chain == 0 && assoc == 1 ) )
            {
              skip = true;
            }                 
            break;
        
          case RT_NON_CHAIN_SUMMARY:
            if ( chain != 0 )     // non-chain, skip rows with chain
            {
              skip = true;
            }
            break;
          
          default:      // RT_SUMMARY
            if ( nodeId != 100000000L )    // certegy top level
            {
              if ( chain == 0 )     // chain report, skip rows without a chain
              {
                skip = true;
              }
            }
            break;
        }
        
        if ( skip == false )
        {
          ReportRows.addElement( new SummaryData( resultSet ) );
        }          
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void loadFirstMerchantData( SummaryData row )
  {
    Date                  beginDate = getReportDateBegin();
    ResultSetIterator     it        = null;
    ResultSet             resultSet = null;
    
    try
    {
      // load the sic and date opened from the first account, per certegy
      /*@lineinfo:generated-code*//*@lineinfo:811^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_date(nvl(mf.date_opened,'010100'),'mmddrr')  as date_opened,
//                  mf.sic_code                       as sic_code,
//                  round(months_between(last_day(:beginDate),greatest((last_day(:beginDate)-366),to_date(nvl(mf.date_opened,'010100'),'mmddrr'))),0)
//                                                    as months_open
//          from    monthly_extract_summary     sm,
//                  mif                         mf
//          where   mf.association_node = :row.HierarchyNode and
//                  nvl(mf.date_last_activity,'01-JAN-2000') >= (:beginDate-45) and
//                  sm.merchant_number = mf.merchant_number and
//                  sm.active_date = :beginDate 
//          order by to_date(nvl(mf.date_opened,'010100'),'mmddrr')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  to_date(nvl(mf.date_opened,'010100'),'mmddrr')  as date_opened,\n                mf.sic_code                       as sic_code,\n                round(months_between(last_day( :1 ),greatest((last_day( :2 )-366),to_date(nvl(mf.date_opened,'010100'),'mmddrr'))),0)\n                                                  as months_open\n        from    monthly_extract_summary     sm,\n                mif                         mf\n        where   mf.association_node =  :3  and\n                nvl(mf.date_last_activity,'01-JAN-2000') >= ( :4 -45) and\n                sm.merchant_number = mf.merchant_number and\n                sm.active_date =  :5  \n        order by to_date(nvl(mf.date_opened,'010100'),'mmddrr')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.CertegyMonthEndDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setLong(3,row.HierarchyNode);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.reports.CertegyMonthEndDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:824^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        row.SicCode     = resultSet.getString("sic_code");
        row.DateOpened  = resultSet.getDate("date_opened");
        row.setMonthsOpen(resultSet.getString("months_open"));
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadFirstMerchantData()",e.toString());
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    super.postHandleRequest( request );
    
    ComboDateField    field = (ComboDateField)getField("accountDate");
    
    if ( field != null )
    {
      if ( HttpHelper.getString(request,"accountDate.month",null) == null )
      {
        Calendar  cal = Calendar.getInstance();
        cal.set(Calendar.MONTH,Calendar.JANUARY);
        cal.set(Calendar.DAY_OF_MONTH,1);
        cal.set(Calendar.YEAR,1970);
        field.setUtilDate(cal.getTime());
      }
    }
    
    if ( HttpHelper.getString(request,"beginDate.month",null) == null )
    {
      Calendar cal = Calendar.getInstance();
    
      cal.add( Calendar.MONTH, -1 );
      cal.set( Calendar.DAY_OF_MONTH, 1 );
    
      ((ComboDateField)getField("beginDate")).setUtilDate( cal.getTime() );
      ((ComboDateField)getField("endDate")).setUtilDate( cal.getTime() );
    }      
    
    // set the default portfolio id
    if ( getLong("nodeId") == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setData("nodeId","100000000" );
    }
    
    // load the existing class variables
    // with the data from the field class
    FilterString = getData("filter");
    if ( !FilterString.equals("") )
    {
      try
      {
        String            token   = null;
        StringTokenizer   tokens  = new StringTokenizer( FilterString, "," );
      
        FilterRegion  = Integer.parseInt(tokens.nextToken());
        FilterCorp    = Integer.parseInt(tokens.nextToken());
      }
      catch( Exception e )
      {
        // ignore
      }        
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    autoSetFields(request);   // stubbed so download servlet works
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/