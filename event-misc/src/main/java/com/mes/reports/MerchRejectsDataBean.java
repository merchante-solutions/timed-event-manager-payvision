/*@lineinfo:filename=MerchRejectsDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchRejectsDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-09-12 14:42:06 -0700 (Wed, 12 Sep 2007) $
  Version            : $Revision: 14136 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class MerchRejectsDataBean extends MerchTranDataBean
{
  public class RejectDetailRow extends DetailRow
  {
    public String     RejectReason          = null;
    
    public RejectDetailRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super(resultSet);
      
      RejectReason = resultSet.getString("reason_desc");
    }
  }
  
  public MerchRejectsDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    if ( ReportType == RT_SUMMARY )
    {
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");
      line.append("\"Reject Cnt\",");
      line.append("\"Reject Amt\",");
    }
    else  // RT_DETAILS
    {
      super.encodeHeaderCSV(line);
      line.append(",\"Reject Reason\"");
    }      
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    line.setLength(0);
    
    if ( ReportType == RT_SUMMARY )
    {
      MesOrgSummaryEntry  record    = (MesOrgSummaryEntry)obj;
    
      line.append( encodeHierarchyNode(record.getHierarchyNode()) );
      line.append( ",\"" );
      line.append( record.getOrgName() );
      line.append( "\"," );
      line.append( record.getCount() );
      line.append( "," );
      line.append( record.getAmount() );
    }
    else
    {
      super.encodeLineCSV(obj,line);
      
      RejectDetailRow  record    = (RejectDetailRow)obj;
    
      line.append( ",\"" );
      line.append( record.RejectReason );
      line.append( "\"" );
    }      
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    
    if ( ReportType == RT_DETAILS )
    {
      filename.append("_reject_details_");
    }
    else    // RT_SUMMARY
    {
      filename.append("_reject_summary_");
    }      
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin,"MMddyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd,"MMddyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    long                          merchantId        = 0L;
    ResultSet                     resultSet         = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:146^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ORDERED USE_NL (dt pd) FIRST_ROWS */
//                  mf.merchant_number                as hierarchy_node,
//                  mf.dba_name                       as org_name,
//                  dt.reference_number               as ref_num,   
//                  dt.card_type                      as card_type, 
//                  dt.transaction_type               as tran_type,
//                  dt.authorization_num              as auth_code, 
//                  dt.auth_amt                       as auth_amount, 
//                  ( dt.transaction_amount *
//                    decode(dt.debit_credit_indicator,
//                           'C',-1,1) )              as tran_amount,
//                  dt.transaction_date               as tran_date, 
//                  nvl(pd.pos_entry_desc,
//                      dt.pos_entry_mode )           as pos_entry_mode, 
//                  dt.cardholder_account_number      as card_number,
//                  dt.card_number_enc                as card_number_enc,
//                  null                              as passenger_name,
//                  dt.debit_credit_indicator         as debit_credit_ind, 
//                  dt.batch_number                   as batch_number, 
//                  ltrim(rtrim(dt.purchase_id))      as purchase_id,
//                  nvl(dt.merchant_id,1)             as terminal_number,
//                  dt.merchant_batch_number          as merchant_batch_number,
//                  nvl(rd.reject_reason_desc,
//                      'Reason Code ' || dt.reject_reason ) as reason_desc,
//                  dt.batch_date                     as batch_date,
//                  null                              as file_prefix,
//                  dt.merchant_account_number        as hierarchy_node,
//                  null                              as pos_tid
//          from    group_merchant            gm,
//                  mif                       mf,
//                  daily_detail_file_dt      dt, 
//                  pos_entry_mode_desc       pd,
//                  reject_reason_desc        rd
//          where   gm.org_num                  = :orgId and
//                  mf.merchant_number          = gm.merchant_number and
//                  dt.merchant_account_number  = mf.merchant_number and
//                  dt.batch_date between :beginDate and :endDate and
//                  not dt.reject_reason is null and
//                  rd.reject_reason(+) = dt.reject_reason and
//                  pd.pos_entry_code(+)        = dt.pos_entry_mode
//          order by dt.batch_date, dt.batch_number, dt.cardholder_account_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ ORDERED USE_NL (dt pd) FIRST_ROWS */\n                mf.merchant_number                as hierarchy_node,\n                mf.dba_name                       as org_name,\n                dt.reference_number               as ref_num,   \n                dt.card_type                      as card_type, \n                dt.transaction_type               as tran_type,\n                dt.authorization_num              as auth_code, \n                dt.auth_amt                       as auth_amount, \n                ( dt.transaction_amount *\n                  decode(dt.debit_credit_indicator,\n                         'C',-1,1) )              as tran_amount,\n                dt.transaction_date               as tran_date, \n                nvl(pd.pos_entry_desc,\n                    dt.pos_entry_mode )           as pos_entry_mode, \n                dt.cardholder_account_number      as card_number,\n                dt.card_number_enc                as card_number_enc,\n                null                              as passenger_name,\n                dt.debit_credit_indicator         as debit_credit_ind, \n                dt.batch_number                   as batch_number, \n                ltrim(rtrim(dt.purchase_id))      as purchase_id,\n                nvl(dt.merchant_id,1)             as terminal_number,\n                dt.merchant_batch_number          as merchant_batch_number,\n                nvl(rd.reject_reason_desc,\n                    'Reason Code ' || dt.reject_reason ) as reason_desc,\n                dt.batch_date                     as batch_date,\n                null                              as file_prefix,\n                dt.merchant_account_number        as hierarchy_node,\n                null                              as pos_tid\n        from    group_merchant            gm,\n                mif                       mf,\n                daily_detail_file_dt      dt, \n                pos_entry_mode_desc       pd,\n                reject_reason_desc        rd\n        where   gm.org_num                  =  :1  and\n                mf.merchant_number          = gm.merchant_number and\n                dt.merchant_account_number  = mf.merchant_number and\n                dt.batch_date between  :2  and  :3  and\n                not dt.reject_reason is null and\n                rd.reject_reason(+) = dt.reject_reason and\n                pd.pos_entry_code(+)        = dt.pos_entry_mode\n        order by dt.batch_date, dt.batch_number, dt.cardholder_account_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchRejectsDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchRejectsDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:189^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new RejectDetailRow( resultSet ) );
      }
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    
    try
    {
      if ( hasAssocDistricts() )
      // returns false unless the current node is an
      // association and it has districts under it.
      {
        if ( District == DISTRICT_NONE )
        {
          /*@lineinfo:generated-code*//*@lineinfo:220^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+
//                         ordered
//                         use_nl(gm dt)
//                         index( idx_ddf_dt_bdate_merch_acct )                     
//                      */        
//                      o.org_num                                 as org_num,
//                      o.org_group                               as hierarchy_node,
//                      nvl(mf.district,:DISTRICT_UNASSIGNED)     as district,
//                      nvl(ad.district_desc,decode( mf.district,
//                                  null,'Unassigned',
//                                 ('District ' || to_char(mf.district, '0009')))) as org_name,
//                      count(dt.reject_reason)                   as item_count,
//                      sum( (dt.transaction_amount *
//                            decode(dt.debit_credit_indicator,
//                            'C',-1,1)) )                        as item_amount
//              from    organization              o,
//                      group_merchant            gm,
//                      group_rep_merchant        grm,
//                      daily_detail_file_dt      dt,
//                      mif                       mf,
//                      assoc_districts           ad
//              where   o.org_num           = :orgId and
//                      gm.org_num          = o.org_num and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      dt.merchant_account_number(+) = mf.merchant_number and
//                      dt.batch_date between :beginDate and :endDate and
//                      not dt.reject_reason is null and
//                      ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                      ad.district(+)      = mf.district
//              group by  o.org_num, o.org_group, 
//                        mf.district, ad.district_desc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+\n                       ordered\n                       use_nl(gm dt)\n                       index( idx_ddf_dt_bdate_merch_acct )                     \n                    */        \n                    o.org_num                                 as org_num,\n                    o.org_group                               as hierarchy_node,\n                    nvl(mf.district, :1 )     as district,\n                    nvl(ad.district_desc,decode( mf.district,\n                                null,'Unassigned',\n                               ('District ' || to_char(mf.district, '0009')))) as org_name,\n                    count(dt.reject_reason)                   as item_count,\n                    sum( (dt.transaction_amount *\n                          decode(dt.debit_credit_indicator,\n                          'C',-1,1)) )                        as item_amount\n            from    organization              o,\n                    group_merchant            gm,\n                    group_rep_merchant        grm,\n                    daily_detail_file_dt      dt,\n                    mif                       mf,\n                    assoc_districts           ad\n            where   o.org_num           =  :2  and\n                    gm.org_num          = o.org_num and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    dt.merchant_account_number(+) = mf.merchant_number and\n                    dt.batch_date between  :5  and  :6  and\n                    not dt.reject_reason is null and\n                    ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                    ad.district(+)      = mf.district\n            group by  o.org_num, o.org_group, \n                      mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchRejectsDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchRejectsDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:256^11*/
        }
        else    // a district was specified
        {
          /*@lineinfo:generated-code*//*@lineinfo:260^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+
//                         ordered
//                         use_nl(gm dt)
//                         index( idx_ddf_dt_bdate_merch_acct )                     
//                      */        
//                      o.org_num                                 as org_num,
//                      mf.merchant_number                        as hierarchy_node,
//                      mf.dba_name                               as org_name,
//                      :District                                 as district,
//                      count(dt.reject_reason)                   as item_count,
//                      sum( (dt.transaction_amount *
//                            decode(dt.debit_credit_indicator,
//                            'C',-1,1)) )                        as item_amount
//              from    group_merchant            gm,
//                      group_rep_merchant        grm,
//                      daily_detail_file_dt      dt,
//                      mif                       mf,
//                      organization              o
//              where   gm.org_num          = :orgId and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      nvl(mf.district,-1) = :District and
//                      dt.merchant_account_number = mf.merchant_number and
//                      dt.batch_date between :beginDate and :endDate and
//                      not dt.reject_reason is null and
//                      o.org_group = mf.merchant_number
//              group by  o.org_num, mf.merchant_number, mf.dba_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+\n                       ordered\n                       use_nl(gm dt)\n                       index( idx_ddf_dt_bdate_merch_acct )                     \n                    */        \n                    o.org_num                                 as org_num,\n                    mf.merchant_number                        as hierarchy_node,\n                    mf.dba_name                               as org_name,\n                     :1                                  as district,\n                    count(dt.reject_reason)                   as item_count,\n                    sum( (dt.transaction_amount *\n                          decode(dt.debit_credit_indicator,\n                          'C',-1,1)) )                        as item_amount\n            from    group_merchant            gm,\n                    group_rep_merchant        grm,\n                    daily_detail_file_dt      dt,\n                    mif                       mf,\n                    organization              o\n            where   gm.org_num          =  :2  and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    nvl(mf.district,-1) =  :5  and\n                    dt.merchant_account_number = mf.merchant_number and\n                    dt.batch_date between  :6  and  :7  and\n                    not dt.reject_reason is null and\n                    o.org_group = mf.merchant_number\n            group by  o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchRejectsDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchRejectsDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:291^11*/
        }
      }
      else // just standard child report, no districts
      {
        /*@lineinfo:generated-code*//*@lineinfo:296^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+
//                       ordered
//                       use_nl(gm dt)
//                       index( idx_ddf_dt_bdate_merch_acct )                     
//                    */        
//                    o.org_num                                 as org_num,
//                    o.org_group                               as hierarchy_node,
//                    o.org_name                                as org_name,
//                    0                                         as district,
//                    count(dt.reject_reason)                   as item_count,
//                    sum( (dt.transaction_amount *
//                          decode(dt.debit_credit_indicator,
//                          'C',-1,1)) )                        as item_amount
//            from    parent_org                po,
//                    organization              o,
//                    group_merchant            gm,
//                    group_rep_merchant        grm,
//                    daily_detail_file_dt    dt
//            where   po.parent_org_num   = :orgId and
//                    o.org_num           = po.org_num and
//                    gm.org_num          = o.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    dt.merchant_account_number = gm.merchant_number and
//                    dt.batch_date between :beginDate and :endDate and
//                    not dt.reject_reason is null
//            group by o.org_num, o.org_group, o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+\n                     ordered\n                     use_nl(gm dt)\n                     index( idx_ddf_dt_bdate_merch_acct )                     \n                  */        \n                  o.org_num                                 as org_num,\n                  o.org_group                               as hierarchy_node,\n                  o.org_name                                as org_name,\n                  0                                         as district,\n                  count(dt.reject_reason)                   as item_count,\n                  sum( (dt.transaction_amount *\n                        decode(dt.debit_credit_indicator,\n                        'C',-1,1)) )                        as item_amount\n          from    parent_org                po,\n                  organization              o,\n                  group_merchant            gm,\n                  group_rep_merchant        grm,\n                  daily_detail_file_dt    dt\n          where   po.parent_org_num   =  :1  and\n                  o.org_num           = po.org_num and\n                  gm.org_num          = o.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  dt.merchant_account_number = gm.merchant_number and\n                  dt.batch_date between  :4  and  :5  and\n                  not dt.reject_reason is null\n          group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchRejectsDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.MerchRejectsDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:326^9*/
      }
      processSummaryData( it.getResultSet() );
      
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/