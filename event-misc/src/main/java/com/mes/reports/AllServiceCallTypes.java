/*@lineinfo:filename=AllServiceCallTypes*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AllServiceCallTypes.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/25/04 3:27p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class AllServiceCallTypes extends DropDownTable
{
  public AllServiceCallTypes()
  {
    ResultSetIterator it    = null;
    ResultSet         rs    = null;
    
    try
    {
      connect();
      
      addElement("-1", "All Service Calls");
      
      /*@lineinfo:generated-code*//*@lineinfo:44^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  type,
//                  description
//          from    service_call_types
//          where   type > 0
//          order by display_order
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  type,\n                description\n        from    service_call_types\n        where   type > 0\n        order by display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AllServiceCallTypes",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AllServiceCallTypes",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:51^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        addElement(rs);
      }
    }
    catch(Exception e)
    {
      logEntry("AllServiceCallTypes()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
  }
}/*@lineinfo:generated-code*/