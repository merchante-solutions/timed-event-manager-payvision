/*@lineinfo:filename=RiskAchDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/RiskAchDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-08-06 16:55:08 -0700 (Mon, 06 Aug 2012) $
  Version            : $Revision: 20434 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.TreeSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.ComboDateField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesMath;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class RiskAchDataBean extends RiskBaseDataBean
{
  public class MerchantExportData
  {
    public Date                   DateOpened;
    public double                 LastDepositAmount;
    public Date                   LastDepositDate;
    public double                 McDiscRate;
    public double                 McPerItem;
    public String                 MerchantStatus;
    public String                 RepCode;
    public double                 VisaDiscRate;
    public double                 VisaPerItem;
    
    public MerchantExportData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      DateOpened        = resultSet.getDate  ( "date_opened" );
      LastDepositAmount = resultSet.getDouble( "last_deposit_amount" );
      LastDepositDate   = resultSet.getDate  ( "last_deposit_date" );
      McDiscRate        = resultSet.getDouble( "mc_rate" );
      McPerItem         = resultSet.getDouble( "mc_per_item" );
      MerchantStatus    = resultSet.getString( "merchant_status" );
      RepCode           = resultSet.getString( "rep_code" );
      VisaDiscRate      = resultSet.getDouble( "vs_rate" );
      VisaPerItem       = resultSet.getDouble( "vs_per_item" );
      
      if ( MerchantStatus == null )
      {
        MerchantStatus = EMPTY_TEXT_FIELD;
      }
    }
  }
  
  public class RiskAchDetailRecord extends RiskRecordBase
  {
    public  double    Amount            = 0.0;
    public  Date      BatchDate         = null;
    public  int       TierId            = 0;
    
    public RiskAchDetailRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );
      
      Amount          = resultSet.getDouble("batch_amount");
      BatchDate       = resultSet.getDate("batch_date");
      TierId          = resultSet.getInt("tier_id");
    }
    
    public int compareTo( Object object )
    {
      RiskAchDetailRecord entry         = (RiskAchDetailRecord) object;
      int                 retVal        = 0;
      
      if ( ( retVal = (int)(TierId - entry.TierId) ) == 0 )
      {
        if ( ( retVal = (int)(MerchantId - entry.MerchantId) ) == 0 )
        {
          // date in descending order
          retVal = entry.BatchDate.compareTo(BatchDate);
        }
      }        
      return( retVal );
    }
  }
  
  public class RiskNewAccountRecord extends RiskRecordBase
  {
    public  Date      DateActivated       = null;
    public  Date      DateOpened          = null;
    public  double    LastDepositAmount   = 0.0;
    public  Date      LastDepositDate     = null;
    public  double    LastBatchAmount     = 0.0;
    public  int       LastBatchCount      = 0;
    public  Date      LastBatchDate       = null;
    public  double    TotalDepositAmount  = 0.0;
    public  int       TotalDepositCount   = 0;
    
    public RiskNewAccountRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );
      
      DateActivated     = resultSet.getDate("date_activated");
      DateOpened        = resultSet.getDate("date_opened");
      LastDepositAmount = resultSet.getDouble("last_deposit_amount");
      LastDepositDate   = resultSet.getDate("last_deposit_date");
      LastBatchCount    = resultSet.getInt("last_batch_count");
      LastBatchAmount   = resultSet.getDouble("last_batch_amount");
      LastBatchDate     = resultSet.getDate("last_batch_date");
    }
    
    public int compareTo( Object obj )
    {
      RiskNewAccountRecord  compareObj  = (RiskNewAccountRecord)obj;
      int                   retVal      = 0;
      
      // descending order
      if ( ( retVal = (int)(compareObj.LastDepositAmount-LastDepositAmount) ) == 0 )
      {
        // default sort is by name, by merchant id
        retVal = super.compareTo(obj);
      }        
      return( retVal );
    }
  }
  
  public class RiskLargeDepositRecord extends RiskRecordBase
  {
    public  double    BatchAmount         = 0.0;
    public  double    BatchAmountMTD      = 0.0;
    public  double    BatchAmountYTD      = 0.0;
    public  int       BatchCount          = 0;
    public  int       BatchCountMTD       = 0;
    public  int       BatchCountYTD       = 0;
    public  Date      BatchDate           = null;
    public  Date      DateActivated       = null;
    public  Date      DateOpened          = null;
    
    public RiskLargeDepositRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );
      
      DateActivated     = resultSet.getDate("date_activated");
      DateOpened        = resultSet.getDate("date_opened");
      BatchDate         = resultSet.getDate("batch_date");
      BatchCount        = resultSet.getInt("batch_count");
      BatchAmount       = resultSet.getDouble("batch_amount");
      
//@      loadTotalsMonthlyYearly(this);
    }
    
    public int compareTo( Object obj )
    {
      RiskLargeDepositRecord  compareObj  = (RiskLargeDepositRecord)obj;
      int                     retVal      = 0;
      
      // descending order
      if ( ( retVal = (int)(compareObj.BatchAmount-BatchAmount) ) == 0 )
      {
        // default sort is by name, by merchant id
        retVal = super.compareTo(obj);
      }        
      return( retVal );
    }
  }
  
  private long                ExportMerchantId      = 0L;
  
  public RiskAchDataBean( )
  {
    super(true);  // enable field bean support
  }
  
  public boolean checkingForOpenItems( )
  {
    return( getData("checkOpenItems").equals("true") );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup        fgroup;
    Field             field;
    
    super.createFields(request);
    
    fgroup = (FieldGroup)getField("searchFields");
    fgroup.add( new HiddenField("riskReportType") );
    fgroup.add( new HiddenField("reportStyle") );
    
    setData("riskReportType",String.valueOf(RISK_TYPE_ACH));  // default
    setData("reportStyle",HttpHelper.getString(request,"reportStyle",String.valueOf(RISK_STYLE_DETAILS)));
    
    switch( getInt("reportStyle") )
    {
      case RISK_STYLE_DETAILS:
        ReportTitle = "Inactive Merchants";
        fgroup.deleteField("beginDate");  // not used
        fgroup.deleteField("endDate");
        field = new NumberField("dayCount", "Max Days of Inactivity", 4,4, false,0);
        field.setData("90");
        fgroup.add( field );
        field = new HiddenField("checkOpenItems");
        field.setData("false");
        fgroup.add(field);
        break;
        
      case RISK_STYLE_NEW_ACCT_DEPOSITS:
        ReportTitle   = "New Account Deposits";
        fgroup.deleteField("beginDate");  // not used
        fgroup.deleteField("endDate");
        field = new NumberField("minDepositAmount", "Minimum Deposit Amount", 6,6, false,0);
        field.setData("500");
        fgroup.add( field );
        break;
        
      case RISK_STYLE_LARGE_DEPOSITS:
        ReportTitle = "Large Deposits";
        field = new NumberField("minDepositAmount", "Minimum Deposit Amount", 6,6, false,0);
        field.setData("5000");
        fgroup.add( field );
        break;
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    // reset the first merchant id
    ExportMerchantId = 0L;
    
    switch( getReportStyle() )
    {
      case RISK_STYLE_DETAILS:
        line.append("\"Merchant Id\",");
        line.append("\"DBA Name\",");
        line.append("\"City\",");
        line.append("\"State\",");
        line.append("\"Zip\",");
        line.append("\"Assoc Number\",");
        line.append("\"Contact\",");
        line.append("\"Business Phone\",");
        line.append("\"Alt. Phone\",");
        line.append("\"Tier\",");
        line.append("\"Last Deposit Date\",");
        line.append("\"Last Deposit Amount\",");
        line.append("\"Visa Rate\",");
        line.append("\"Visa Per Item\",");
        line.append("\"MC Rate\",");
        line.append("\"MC Per Item\",");
        line.append("\"Date Opened\",");
        line.append("\"Rep Code\",");
        line.append("\"SIC Code\",");
        line.append("\"Status\"");
        break;
        
      case RISK_STYLE_NEW_ACCT_DEPOSITS:
        line.append("\"Merchant Id\",");
        line.append("\"DBA Name\",");
        switch( getReportBankId() )
        {
          case mesConstants.BANK_ID_CERTEGY:
          case mesConstants.BANK_ID_MES:
          case mesConstants.BANK_ID_MES_WF:
            line.append("\"Portfolio Node\",");
            break;
        }
        line.append("\"Date Opened\",");
        line.append("\"Date Activated\",");
        line.append("\"Last Deposit Date\",");
        line.append("\"Last Deposit Amount\",");
        line.append("\"Last Batch Count\",");
        line.append("\"Last Batch Amount\",");
        line.append("\"Total Deposit Count\",");
        line.append("\"Total Deposit Amount\"");
        break;
        
      case RISK_STYLE_LARGE_DEPOSITS:
        line.append("\"Merchant Id\",");
        line.append("\"DBA Name\",");
        switch( getReportBankId() )
        {
          case mesConstants.BANK_ID_CERTEGY:
          case mesConstants.BANK_ID_MES:
          case mesConstants.BANK_ID_MES_WF:
            line.append("\"Portfolio Node\",");
            break;
        }
        line.append("\"Date Opened\",");
        line.append("\"Date Activated\",");
        line.append("\"Last Batch Date\",");
        line.append("\"Last Batch Count\",");
        line.append("\"Last Batch Amount\",");
        line.append("\"Batch Count MTD\",");
        line.append("\"Batch Amount MTD\",");
        line.append("\"Batch Count YTD\",");
        line.append("\"Batch Amount YTD\"");
        break;
        
      default:
        break;
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    line.setLength(0);
    
    switch( getReportStyle() )
    {
      case RISK_STYLE_DETAILS:
      {
        ReportSQLJBean.ContactRecord    contactRecord = null;
        MerchantExportData              merchData     = null;
        RiskAchDetailRecord             record        = null;
        
        record        = (RiskAchDetailRecord)obj;
        
        // if the current record is for a merchant
        // we have already processed, then skip
        if ( record.MerchantId == ExportMerchantId )
        {
          break;    // exit case statement
        }
        
        // load the contact and merchant specific data.
        contactRecord = getMerchantContactData( record.MerchantId );
        merchData     = loadMerchantExportData( record.MerchantId );
        
        // update the last merchant exported
        ExportMerchantId = record.MerchantId;
        
        // fill the line with the data
        line.append( record.MerchantId );
        line.append( ",\"" );
        line.append( record.DbaName );
        line.append( "\",\"" );
        line.append( contactRecord.City );
        line.append( "\",\"" );
        line.append( contactRecord.State );
        line.append( "\",\"" );
        line.append( contactRecord.Zip );
        line.append( "\"," );
        line.append( record.AssocNumber );
        line.append( ",\"" );
        line.append( contactRecord.ContactName );
        line.append( "\",\"" );
        line.append( contactRecord.BusinessPhone );
        line.append( "\",\"" );
        line.append( contactRecord.ContactPhone );
        line.append( "\"," );
        line.append( record.TierId );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate( merchData.LastDepositDate,"MM/dd/yyyy" ) );
        line.append( "," );
        line.append( merchData.LastDepositAmount );
        line.append( "," );
        line.append( merchData.VisaDiscRate );
        line.append( "," );
        line.append( merchData.VisaPerItem );
        line.append( "," );
        line.append( merchData.McDiscRate );
        line.append( "," );
        line.append( merchData.McPerItem );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate( merchData.DateOpened,"MM/dd/yyyy" ) );
        line.append( ",\"" );
        line.append( merchData.RepCode );
        line.append( "\"," );
        line.append( record.SicCode );
        line.append( ",\"" );
        line.append( merchData.MerchantStatus );
        line.append( "\"" );
        break;
      }
      
      case RISK_STYLE_NEW_ACCT_DEPOSITS:
      {
        RiskNewAccountRecord            record        = null;
        
        record        = (RiskNewAccountRecord)obj;
        loadAccountTotals(record);
        
        // fill the line with the data
        line.append( record.MerchantId );
        line.append( ",\"" );
        line.append( record.DbaName );
        line.append( "\"," );
        switch( getReportBankId() )
        {
          case mesConstants.BANK_ID_CERTEGY:
            line.append(encodeHierarchyNode(getMerchantGroup1(record.MerchantId)));
            line.append(",");
            break;
          
          case mesConstants.BANK_ID_MES:
          case mesConstants.BANK_ID_MES_WF:
            line.append(encodeHierarchyNode(getMerchantGroup2(record.MerchantId)));
            line.append(",");
            break;
        }
        line.append( DateTimeFormatter.getFormattedDate( record.DateOpened,"MM/dd/yyyy" ) );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate( record.DateActivated,"MM/dd/yyyy" ) );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate( record.LastDepositDate,"MM/dd/yyyy" ) );
        line.append( "," );
        line.append( record.LastDepositAmount );
        line.append( "," );
        line.append( record.LastBatchCount );
        line.append( "," );
        line.append( record.LastBatchAmount );
        line.append( "," );
        line.append( record.TotalDepositCount );
        line.append( "," );
        line.append( record.TotalDepositAmount );
        break;
      }
      
      case RISK_STYLE_LARGE_DEPOSITS:
      {
        RiskLargeDepositRecord            record        = null;
        
        record        = (RiskLargeDepositRecord)obj;
        loadTotalsMonthlyYearly(record);
        
        // fill the line with the data
        line.append( record.MerchantId );
        line.append( ",\"" );
        line.append( record.DbaName );
        line.append( "\"," );
        switch( getReportBankId() )
        {
          case mesConstants.BANK_ID_CERTEGY:
            line.append(encodeHierarchyNode(getMerchantGroup1(record.MerchantId)));
            line.append(",");
            break;
          
          case mesConstants.BANK_ID_MES:
          case mesConstants.BANK_ID_MES_WF:
            line.append(encodeHierarchyNode(getMerchantGroup2(record.MerchantId)));
            line.append(",");
            break;
        }
        line.append( DateTimeFormatter.getFormattedDate( record.DateOpened,"MM/dd/yyyy" ) );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate( record.DateActivated,"MM/dd/yyyy" ) );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate( record.BatchDate,"MM/dd/yyyy" ) );
        line.append( "," );
        line.append( record.BatchCount );
        line.append( "," );
        line.append( record.BatchAmount );
        line.append( "," );
        line.append( record.BatchCountMTD );
        line.append( "," );
        line.append( record.BatchAmountMTD );
        line.append( "," );
        line.append( record.BatchCountYTD );
        line.append( "," );
        line.append( record.BatchAmountYTD );
        break;
      }
        
      default:
        break;
    }
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    super.encodeNodeUrl(buffer,nodeId,beginDate,endDate);
    buffer.append("&checkOpenItems=");
    buffer.append(getData("checkOpenItems"));
    
    switch( getInt("reportStyle") )
    {
      case RISK_STYLE_DETAILS:
        buffer.append("&dayCount=");
        buffer.append(getData("dayCount"));
        break;
        
      case RISK_STYLE_NEW_ACCT_DEPOSITS:
      case RISK_STYLE_LARGE_DEPOSITS:
        buffer.append("&minDepositAmount=");
        buffer.append(getData("minDepositAmount"));
        break;
    }
  }
  
  public int getCountMonthly( long merchantId, Date postDate )
  {
    int     retVal = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:523^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum( ach.CREDIT_COUNT + ach.DEBIT_COUNT ) 
//          from    ach_summary ach
//          where   ach.merchant_number = :merchantId and
//                  ach.post_date between
//                    to_date(to_char(:postDate,'mm/yyyy'),'mm/yyyy') and
//                    last_day( :postDate )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( ach.CREDIT_COUNT + ach.DEBIT_COUNT )  \n        from    ach_summary ach\n        where   ach.merchant_number =  :1  and\n                ach.post_date between\n                  to_date(to_char( :2 ,'mm/yyyy'),'mm/yyyy') and\n                  last_day(  :3  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.RiskAchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,postDate);
   __sJT_st.setDate(3,postDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:531^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return(retVal);
  }
  
  public int getCountQuarterly( long merchantId, Date postDate )
  {
    int     retVal = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:545^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum( ach.CREDIT_COUNT + ach.DEBIT_COUNT ) 
//          from    ach_summary ach
//          where   ach.merchant_number = :merchantId and
//                  ach.post_date between
//                    quarter_date_begin(:postDate) and 
//                    (round((quarter_date_begin(:postDate)+60),'Q')-1)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( ach.CREDIT_COUNT + ach.DEBIT_COUNT )  \n        from    ach_summary ach\n        where   ach.merchant_number =  :1  and\n                ach.post_date between\n                  quarter_date_begin( :2 ) and \n                  (round((quarter_date_begin( :3 )+60),'Q')-1)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.RiskAchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,postDate);
   __sJT_st.setDate(3,postDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:553^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return(retVal);
  }
  
  public int getCountYearly( long merchantId, Date postDate )
  {
    Calendar    cal         = Calendar.getInstance();
    int         retVal      = 0;
    int         yr          = 0;
    
    try
    {
      cal.setTime(postDate);
      yr = cal.get(Calendar.YEAR);
    
      /*@lineinfo:generated-code*//*@lineinfo:572^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum( ach.CREDIT_COUNT + ach.DEBIT_COUNT ) 
//          from    ach_summary ach
//          where   ach.merchant_number = :merchantId and
//                  ach.post_date between
//                    to_date( ('01/01/' || :yr ), 'mm/dd/yyyy' ) and 
//                    to_date( ('12/31/' || :yr ), 'mm/dd/yyyy' )      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( ach.CREDIT_COUNT + ach.DEBIT_COUNT )  \n        from    ach_summary ach\n        where   ach.merchant_number =  :1  and\n                ach.post_date between\n                  to_date( ('01/01/' ||  :2  ), 'mm/dd/yyyy' ) and \n                  to_date( ('12/31/' ||  :3  ), 'mm/dd/yyyy' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.RiskAchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setInt(2,yr);
   __sJT_st.setInt(3,yr);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:580^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return(retVal);
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );

    switch( getReportStyle() )
    {
      case RISK_STYLE_DETAILS:
        filename.append("_inactive_accounts_");
        break;
        
      case RISK_STYLE_NEW_ACCT_DEPOSITS:
        filename.append("_new_acct_deposits_");
        break;
        
      case RISK_STYLE_LARGE_DEPOSITS:
        filename.append("_large_deposits_");
        break;
        
      default:
        break;
    }
    filename.append( DateTimeFormatter.getFormattedDate( getReportDateBegin(),"MMddyyyy" ) );
    return ( filename.toString() );
  }
  
  public double getLastDepositAmount( long merchantId )
  {
    double  retVal = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:621^7*/

//  ************************************************************
//  #sql [Ctx] { select  mf.last_deposit_amount    
//          from    mif mf
//          where   mf.merchant_number = :merchantId 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mf.last_deposit_amount     \n        from    mif mf\n        where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.RiskAchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:626^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return(retVal);
  }
  
  public Date getLastDepositDate( long merchantId )
  {
    Date    retVal = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:640^7*/

//  ************************************************************
//  #sql [Ctx] { select  mf.last_deposit_date    
//          from    mif mf
//          where   mf.merchant_number = :merchantId 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mf.last_deposit_date     \n        from    mif mf\n        where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.RiskAchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:645^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return(retVal);
  }
  
  public int getOpenChargebackCount( long merchantId )
  {
    int retVal      = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:659^7*/

//  ************************************************************
//  #sql [Ctx] { select  count( cb.cb_load_sec )       
//          from    network_chargebacks   cb
//          where   cb.merchant_number = :merchantId and
//                  cb.incoming_date between (trunc(sysdate)-120) and trunc(sysdate) and
//                  not exists ( select cba.action_date
//                               from   network_chargeback_activity cba
//                               where  cba.cb_load_sec = cb.cb_load_sec )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( cb.cb_load_sec )        \n        from    network_chargebacks   cb\n        where   cb.merchant_number =  :1  and\n                cb.incoming_date between (trunc(sysdate)-120) and trunc(sysdate) and\n                not exists ( select cba.action_date\n                             from   network_chargeback_activity cba\n                             where  cba.cb_load_sec = cb.cb_load_sec )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.RiskAchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:668^7*/

    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public Date getOpenChargebackDateMin( long merchantId )
  {
    Date retVal      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:683^7*/

//  ************************************************************
//  #sql [Ctx] { select  min( cb.incoming_date )       
//          from    network_chargebacks   cb
//          where   cb.merchant_number = :merchantId and
//                  cb.incoming_date between (trunc(sysdate)-120) and trunc(sysdate) and
//                  not exists ( select cba.action_date
//                               from   network_chargeback_activity cba
//                               where  cba.cb_load_sec = cb.cb_load_sec )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  min( cb.incoming_date )        \n        from    network_chargebacks   cb\n        where   cb.merchant_number =  :1  and\n                cb.incoming_date between (trunc(sysdate)-120) and trunc(sysdate) and\n                not exists ( select cba.action_date\n                             from   network_chargeback_activity cba\n                             where  cba.cb_load_sec = cb.cb_load_sec )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.RiskAchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:692^7*/

    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public int getOpenChargebackRetrievalCount( long merchantId )
  {
    return( getOpenChargebackCount(merchantId) + 
            getOpenRetrievalCount(merchantId) );
  }
  
  public int getOpenRetrievalCount( long merchantId ) 
  {
    int retVal      = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:713^7*/

//  ************************************************************
//  #sql [Ctx] { select count( rt.RETR_LOAD_SEC )      
//          from   network_retrievals rt
//          where  rt.MERCHANT_NUMBER = :merchantId and
//                 rt.RECEIVED_DATE is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count( rt.RETR_LOAD_SEC )       \n        from   network_retrievals rt\n        where  rt.MERCHANT_NUMBER =  :1  and\n               rt.RECEIVED_DATE is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.RiskAchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:719^7*/

    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public Date getOpenRetrievalDateMin( long merchantId )
  {
    Date retVal      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:734^7*/

//  ************************************************************
//  #sql [Ctx] { select min( rt.incoming_date ) 
//          from   network_retrievals rt
//          where  rt.MERCHANT_NUMBER = :merchantId and
//                 rt.RECEIVED_DATE is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select min( rt.incoming_date )  \n        from   network_retrievals rt\n        where  rt.MERCHANT_NUMBER =  :1  and\n               rt.RECEIVED_DATE is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.RiskAchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:740^7*/

    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public String getReportTitle( )
  {
    String        title = super.getReportTitle();
    
    if ( checkingForOpenItems() )
    {
      title = title.concat( " with Open Risk Items" );
    }
    return( title );
  }
  
  public String getTierDescription( int tierId )
  {
    int           inactiveDays    = 0;
    double        lowerRange      = 0.0;
    StringBuffer  retVal          = new StringBuffer("");
    double        upperRange      = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:769^7*/

//  ************************************************************
//  #sql [Ctx] { select  rat.inactive_days,
//                  rat.lower_range,
//                  rat.upper_range 
//          from    risk_activity_tiers    rat
//          where   rat.tier_id = :tierId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  rat.inactive_days,\n                rat.lower_range,\n                rat.upper_range  \n        from    risk_activity_tiers    rat\n        where   rat.tier_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.RiskAchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,tierId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   inactiveDays = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   lowerRange = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   upperRange = __sJT_rs.getDouble(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:776^7*/
      
      retVal.append("Tier " );
      retVal.append( tierId );
      retVal.append( ": Monthly Volume between ");
      retVal.append( MesMath.toCurrency( lowerRange ) );
      retVal.append(" and " );
      retVal.append( MesMath.toCurrency( upperRange ) );
      retVal.append(".  No activity in the last " );
      retVal.append( inactiveDays );
      retVal.append(" days.");
      
    }
    catch( java.sql.SQLException e )
    {
    }
    return(retVal.toString());
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    
    if( fileFormat == FF_CSV )
    {
      retVal = true;
    }
    return(retVal);
  }
  
  public void loadAccountTotals( RiskNewAccountRecord record )
  {
    double  amount  = 0.0;
    int     count   = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:813^7*/

//  ************************************************************
//  #sql [Ctx] { select count( ddf.bank_amount ),
//                 sum( ddf.bank_amount + ddf.nonbank_amount ) 
//          from   daily_detail_file_summary      ddf
//          where  ddf.merchant_number = :record.MerchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count( ddf.bank_amount ),\n               sum( ddf.bank_amount + ddf.nonbank_amount )  \n        from   daily_detail_file_summary      ddf\n        where  ddf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.RiskAchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,record.MerchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amount = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:819^7*/
      record.TotalDepositCount  = count;
      record.TotalDepositAmount = amount;
    }
    catch( java.sql.SQLException e)
    {
      logEntry("loadAccountTotals()",e.toString());
    }
  }
  
  public void loadData( int reportStyle, long orgId, Date beginDate, Date endDate, String[] args )
  {
    Date                          activeDate        = null;
    double                        amountThreshold   = 0;
    ResultSetIterator             it                = null;
    long                          lastMerchantId    = -1L;
    long                          merchantId        = 0L;
    int                           minCount          = 0;
    int                           openCount         = 0;
    double                        percentThreshold  = 0;
    ResultSet                     resultSet         = null;
    TreeSet                       sortedSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( reportStyle == RISK_STYLE_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:849^9*/

//  ************************************************************
//  #sql [Ctx] { select  trunc((trunc(sysdate,'month')-1),'month') 
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc((trunc(sysdate,'month')-1),'month')  \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.RiskAchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activeDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:853^9*/
        
        int dayCount = getInt("dayCount");
        
        /*@lineinfo:generated-code*//*@lineinfo:857^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+
//                    index (gm pkgroup_merchant)
//                    */
//                   mf.MERCHANT_NUMBER               as merchant_number,
//                   mf.DBA_NAME                      as dba_name,
//                   mf.SIC_CODE                      as sic_code,
//                   rat.TIER_ID                      as tier_id,                   
//                   mf.last_deposit_date             as last_deposit_date,
//                   mf.last_deposit_amount           as last_deposit_amount,
//                   ddf.batch_date                   as batch_date,
//                   (ddf.bank_amount +
//                    ddf.nonbank_amount)             as batch_amount,
//                   (mf.bank_number || mf.dmagent)   as assoc_number
//            from   group_merchant                 gm,
//                   group_rep_merchant             grm,
//                   mif                            mf,
//                   monthly_extract_gn             gn,
//                   risk_activity_tiers            rat,                  
//                   daily_detail_file_summary      ddf
//            where  gm.org_num = :orgId and
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                   mf.merchant_number = gm.merchant_number and
//                   mf.DATE_STAT_CHGD_TO_DCB is null and
//                   mf.risk_activation_date < ( sysdate - 45 ) and
//                   mf.last_deposit_date between (sysdate - :dayCount) and (sysdate - rat.inactive_days) and
//                   gn.hh_merchant_number = mf.merchant_number and
//                   gn.hh_active_date = :activeDate and
//                   (nvl(gn.t1_tot_amount_of_sales,0) - nvl(gn.t1_tot_amount_of_credits,0)) between rat.lower_range and rat.upper_range and
//                   ddf.merchant_number = mf.merchant_number and
//                   ddf.batch_date between ( mf.last_deposit_date - 5 ) and mf.last_deposit_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+\n                  index (gm pkgroup_merchant)\n                  */\n                 mf.MERCHANT_NUMBER               as merchant_number,\n                 mf.DBA_NAME                      as dba_name,\n                 mf.SIC_CODE                      as sic_code,\n                 rat.TIER_ID                      as tier_id,                   \n                 mf.last_deposit_date             as last_deposit_date,\n                 mf.last_deposit_amount           as last_deposit_amount,\n                 ddf.batch_date                   as batch_date,\n                 (ddf.bank_amount +\n                  ddf.nonbank_amount)             as batch_amount,\n                 (mf.bank_number || mf.dmagent)   as assoc_number\n          from   group_merchant                 gm,\n                 group_rep_merchant             grm,\n                 mif                            mf,\n                 monthly_extract_gn             gn,\n                 risk_activity_tiers            rat,                  \n                 daily_detail_file_summary      ddf\n          where  gm.org_num =  :1  and\n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and        \n                 mf.merchant_number = gm.merchant_number and\n                 mf.DATE_STAT_CHGD_TO_DCB is null and\n                 mf.risk_activation_date < ( sysdate - 45 ) and\n                 mf.last_deposit_date between (sysdate -  :4 ) and (sysdate - rat.inactive_days) and\n                 gn.hh_merchant_number = mf.merchant_number and\n                 gn.hh_active_date =  :5  and\n                 (nvl(gn.t1_tot_amount_of_sales,0) - nvl(gn.t1_tot_amount_of_credits,0)) between rat.lower_range and rat.upper_range and\n                 ddf.merchant_number = mf.merchant_number and\n                 ddf.batch_date between ( mf.last_deposit_date - 5 ) and mf.last_deposit_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.RiskAchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setInt(4,dayCount);
   __sJT_st.setDate(5,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.reports.RiskAchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:891^9*/
        resultSet = it.getResultSet();
  
        while( resultSet.next() )
        {
          merchantId = resultSet.getLong("merchant_number");
          
          // if we are checking for open items and
          // the last merchant we loaded an item count
          // for is not this merchant, then reload the 
          // item count from the database.
          if ( ( checkingForOpenItems() == true ) &&
               ( lastMerchantId != merchantId ) )
          {
            openCount       = getOpenChargebackRetrievalCount( merchantId );
            lastMerchantId  = merchantId;
          }
          
          // if we are checking for open chargeback and retrievals,
          // and there are none for this merchant, then skip 
          // this entry in the result set.
          if ( ( checkingForOpenItems() == true ) && ( openCount == 0 ) )
          {
            continue;
          }
          
          ReportRows.add( new RiskAchDetailRecord( resultSet ) );
        }
        it.close();   // this will also close the resultSet
        
        if ( ReportRows.size() > 0 )
        {
          sortedSet = new TreeSet( ReportRows );
          ReportRows.removeAllElements();
          ReportRows.addAll(sortedSet);
        }
      }
      else if ( getReportStyle() == RISK_STYLE_NEW_ACCT_DEPOSITS )
      {
        int threshold = getInt("minDepositAmount");
        
        /*@lineinfo:generated-code*//*@lineinfo:932^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+
//                    index (gm pkgroup_merchant)
//                    */
//                   mf.MERCHANT_NUMBER               as merchant_number,
//                   mf.DBA_NAME                      as dba_name,
//                   mf.SIC_CODE                      as sic_code,
//                   to_date(mf.date_opened,'mmddrr') as date_opened,
//                   mf.activation_date               as date_activated,
//                   mf.risk_activation_date          as date_risk_activated,
//                   mf.last_deposit_date             as last_deposit_date,
//                   mf.last_deposit_amount           as last_deposit_amount,
//                   mf.association_node              as assoc_number,
//                   ddf.batch_date                   as last_batch_date,
//                   (ddf.bank_count  +
//                    ddf.nonbank_count )             as last_batch_count,
//                   (ddf.bank_amount +
//                    ddf.nonbank_amount)             as last_batch_amount
//            from   group_merchant                 gm,
//                   mif                            mf,
//                   daily_detail_file_summary      ddf
//            where  gm.org_num = :orgId and
//                   mf.merchant_number = gm.merchant_number and
//                   mf.DATE_STAT_CHGD_TO_DCB is null and
//                   mf.risk_activation_date >= (sysdate - 90) and
//                   ddf.merchant_number = mf.merchant_number and
//                   ddf.batch_date between 
//                      ( mf.last_deposit_date - 5 ) and mf.last_deposit_date and
//                   (ddf.bank_amount + ddf.nonbank_amount) >= :threshold 
//            order by mf.merchant_number,
//                     ddf.batch_date desc,
//                     to_date(substr(ddf.load_filename,-14,6),'mmddrr') desc,
//                     to_number(substr(ddf.load_filename,-7,3)) desc          
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+\n                  index (gm pkgroup_merchant)\n                  */\n                 mf.MERCHANT_NUMBER               as merchant_number,\n                 mf.DBA_NAME                      as dba_name,\n                 mf.SIC_CODE                      as sic_code,\n                 to_date(mf.date_opened,'mmddrr') as date_opened,\n                 mf.activation_date               as date_activated,\n                 mf.risk_activation_date          as date_risk_activated,\n                 mf.last_deposit_date             as last_deposit_date,\n                 mf.last_deposit_amount           as last_deposit_amount,\n                 mf.association_node              as assoc_number,\n                 ddf.batch_date                   as last_batch_date,\n                 (ddf.bank_count  +\n                  ddf.nonbank_count )             as last_batch_count,\n                 (ddf.bank_amount +\n                  ddf.nonbank_amount)             as last_batch_amount\n          from   group_merchant                 gm,\n                 mif                            mf,\n                 daily_detail_file_summary      ddf\n          where  gm.org_num =  :1  and\n                 mf.merchant_number = gm.merchant_number and\n                 mf.DATE_STAT_CHGD_TO_DCB is null and\n                 mf.risk_activation_date >= (sysdate - 90) and\n                 ddf.merchant_number = mf.merchant_number and\n                 ddf.batch_date between \n                    ( mf.last_deposit_date - 5 ) and mf.last_deposit_date and\n                 (ddf.bank_amount + ddf.nonbank_amount) >=  :2  \n          order by mf.merchant_number,\n                   ddf.batch_date desc,\n                   to_date(substr(ddf.load_filename,-14,6),'mmddrr') desc,\n                   to_number(substr(ddf.load_filename,-7,3)) desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.RiskAchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setInt(2,threshold);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.reports.RiskAchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:966^9*/
        resultSet = it.getResultSet();
  
        while( resultSet.next() )
        {
          // only need one row per merchant
          merchantId = resultSet.getLong("merchant_number");
          if ( merchantId == lastMerchantId )
          {
            continue;
          }
        
          ReportRows.add( new RiskNewAccountRecord( resultSet ) );
          
          lastMerchantId = merchantId;
        }
        it.close();   // this will also close the resultSet
        
        if ( ReportRows.size() > 0 )
        {
          sortedSet = new TreeSet( ReportRows );
          ReportRows.removeAllElements();
          ReportRows.addAll(sortedSet);
        }
      }
      else if ( getReportStyle() == RISK_STYLE_LARGE_DEPOSITS )
      {
        int threshold = getInt("minDepositAmount");
        
        /*@lineinfo:generated-code*//*@lineinfo:995^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+
//                    index (gm pkgroup_merchant)
//                    */
//                   mf.MERCHANT_NUMBER               as merchant_number,
//                   mf.DBA_NAME                      as dba_name,
//                   mf.SIC_CODE                      as sic_code,
//                   to_date(mf.date_opened,'mmddrr') as date_opened,
//                   mf.activation_date               as date_activated,
//                   mf.risk_activation_date          as date_risk_activated,
//                   ddf.batch_date                   as batch_date,
//                   mf.association_node              as assoc_number,
//                   (ddf.bank_count  +
//                    ddf.nonbank_count  )            as batch_count,
//                   (ddf.bank_amount +
//                    ddf.nonbank_amount )            as batch_amount
//            from   group_merchant                 gm,
//                   mif                            mf,
//                   daily_detail_file_summary      ddf
//            where  gm.org_num = :orgId and
//                   mf.merchant_number = gm.merchant_number and
//                   ddf.merchant_number = mf.merchant_number and
//                   ddf.batch_date between :beginDate and :endDate and
//                   (ddf.bank_amount + ddf.nonbank_amount) >= :threshold 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+\n                  index (gm pkgroup_merchant)\n                  */\n                 mf.MERCHANT_NUMBER               as merchant_number,\n                 mf.DBA_NAME                      as dba_name,\n                 mf.SIC_CODE                      as sic_code,\n                 to_date(mf.date_opened,'mmddrr') as date_opened,\n                 mf.activation_date               as date_activated,\n                 mf.risk_activation_date          as date_risk_activated,\n                 ddf.batch_date                   as batch_date,\n                 mf.association_node              as assoc_number,\n                 (ddf.bank_count  +\n                  ddf.nonbank_count  )            as batch_count,\n                 (ddf.bank_amount +\n                  ddf.nonbank_amount )            as batch_amount\n          from   group_merchant                 gm,\n                 mif                            mf,\n                 daily_detail_file_summary      ddf\n          where  gm.org_num =  :1  and\n                 mf.merchant_number = gm.merchant_number and\n                 ddf.merchant_number = mf.merchant_number and\n                 ddf.batch_date between  :2  and  :3  and\n                 (ddf.bank_amount + ddf.nonbank_amount) >=  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.RiskAchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setInt(4,threshold);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.RiskAchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1020^9*/
        resultSet = it.getResultSet();
  
        while( resultSet.next() )
        {
          ReportRows.add( new RiskLargeDepositRecord( resultSet ) );
        }
        it.close();   // this will also close the resultSet
        
        if ( ReportRows.size() > 0 )
        {
          sortedSet = new TreeSet( ReportRows );
          ReportRows.removeAllElements();
          ReportRows.addAll(sortedSet);
        }
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public MerchantExportData loadMerchantExportData( long merchantId )
  {
    ResultSetIterator           it          = null;
    ResultSet                   resultSet   = null;
    MerchantExportData          retVal      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1055^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.last_deposit_amount                as last_deposit_amount,
//                  mf.last_deposit_date                  as last_deposit_date,
//                  (mf.VISA_DISC_RATE * 0.001)           as vs_rate,
//                  (mf.VISA_PER_ITEM * 0.001)            as vs_per_item,
//                  (mf.MASTCD_DISC_RATE * 0.001)         as mc_rate,
//                  (mf.MASTCD_PER_ITEM * 0.001)          as mc_per_item,
//                  mf.rep_code                           as rep_code,
//                  to_date(decode(mf.date_opened,
//                                 '000000','010100',
//                                 mf.date_opened),
//                                 'mmddrr')              as date_opened,
//                  sm.merchant_status                    as merchant_status
//          from    mif                         mf,
//                  monthly_extract_summary     sm
//          where   mf.merchant_number = :merchantId and
//                  sm.merchant_number(+) = mf.merchant_number and
//                  sm.active_date(+) = 
//                    to_date(to_char((to_date(to_char(sysdate, 'mm/yyyy'),'mm/yyyy')-1), 'mm/yyyy'),'mm/yyyy')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.last_deposit_amount                as last_deposit_amount,\n                mf.last_deposit_date                  as last_deposit_date,\n                (mf.VISA_DISC_RATE * 0.001)           as vs_rate,\n                (mf.VISA_PER_ITEM * 0.001)            as vs_per_item,\n                (mf.MASTCD_DISC_RATE * 0.001)         as mc_rate,\n                (mf.MASTCD_PER_ITEM * 0.001)          as mc_per_item,\n                mf.rep_code                           as rep_code,\n                to_date(decode(mf.date_opened,\n                               '000000','010100',\n                               mf.date_opened),\n                               'mmddrr')              as date_opened,\n                sm.merchant_status                    as merchant_status\n        from    mif                         mf,\n                monthly_extract_summary     sm\n        where   mf.merchant_number =  :1  and\n                sm.merchant_number(+) = mf.merchant_number and\n                sm.active_date(+) = \n                  to_date(to_char((to_date(to_char(sysdate, 'mm/yyyy'),'mm/yyyy')-1), 'mm/yyyy'),'mm/yyyy')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.RiskAchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.reports.RiskAchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1075^7*/
      resultSet = it.getResultSet();
      if ( resultSet.next() )
      {
        retVal = new MerchantExportData( resultSet );
      }
    }
    catch( java.sql.SQLException e )
    { 
      logEntry( "loadExportMerchantData(" + merchantId + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
    return( retVal );
  }
  
  public void loadTotalsMonthlyYearly( RiskLargeDepositRecord record )
  {
    double  mtdAmount   = 0.0;
    int     mtdCount    = 0;
    double  ytdAmount   = 0.0;
    int     ytdCount    = 0;
    
    try
    {
      Date beginDate = getReportDateBegin();
      
      /*@lineinfo:generated-code*//*@lineinfo:1104^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum( decode( trunc(ddf.batch_date,'month'),
//                               trunc(:beginDate,'month'), (ddf.bank_count  + ddf.nonbank_count ),
//                               0 ) ),
//                  sum( decode( trunc(ddf.batch_date,'month'),
//                               trunc(:beginDate,'month'), (ddf.bank_amount + ddf.nonbank_amount),
//                               0 ) ),
//                  sum( ddf.bank_count  + ddf.nonbank_count ),
//                  sum( ddf.bank_amount + ddf.nonbank_amount) 
//          from   daily_detail_file_summary      ddf
//          where  ddf.merchant_number = :record.MerchantId and
//                 ddf.batch_date >= (:beginDate-365)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( decode( trunc(ddf.batch_date,'month'),\n                             trunc( :1 ,'month'), (ddf.bank_count  + ddf.nonbank_count ),\n                             0 ) ),\n                sum( decode( trunc(ddf.batch_date,'month'),\n                             trunc( :2 ,'month'), (ddf.bank_amount + ddf.nonbank_amount),\n                             0 ) ),\n                sum( ddf.bank_count  + ddf.nonbank_count ),\n                sum( ddf.bank_amount + ddf.nonbank_amount)  \n        from   daily_detail_file_summary      ddf\n        where  ddf.merchant_number =  :3  and\n               ddf.batch_date >= ( :4 -365)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.reports.RiskAchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setLong(3,record.MerchantId);
   __sJT_st.setDate(4,beginDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mtdCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mtdAmount = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   ytdCount = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   ytdAmount = __sJT_rs.getDouble(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1117^7*/
      record.BatchCountMTD  = mtdCount;
      record.BatchAmountMTD = mtdAmount;
      record.BatchAmountYTD = ytdAmount;
      record.BatchCountYTD  = ytdCount;
    }
    catch( java.sql.SQLException e)
    {
      logEntry("loadTotalsMonthlyYearly()",e.toString());
    }
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    super.postHandleRequest( request );
    
    // set the default portfolio id to top of 3941 hierarchy
    if ( getLong("nodeId") == HierarchyTree.DEFAULT_HIERARCHY_NODE &&
         (HttpHelper.getLong(request,"portfolioId",-1L) != -1L) )
    {
      setData("nodeId","394100000");
      setData("portfolioId",getData("nodeId"));
    }
    
    if ( HttpHelper.getString(request,"beginDate.month",null) == null )
    {
      switch( getReportStyle() )
      {
        case RISK_STYLE_LARGE_DEPOSITS:
          Calendar cal = Calendar.getInstance();
          ((ComboDateField)getField("endDate")).setUtilDate( cal.getTime() );
          cal.add( Calendar.DAY_OF_MONTH, -1 );
          ((ComboDateField)getField("beginDate")).setUtilDate( cal.getTime() );
          break;
        
        //case RISK_STYLE_DETAILS:
        //case RISK_STYLE_NEW_ACCT_DEPOSITS:
        default:    
          break;
      }
    }      
  }
  
  public boolean showContactInfo( )
  {
    return(true);
  }
}/*@lineinfo:generated-code*/