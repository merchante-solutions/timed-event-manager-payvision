/*@lineinfo:filename=AmexSettlementDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AmexSettlementDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2000-2005,2006 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

public class AmexSettlementDataBean extends SettlementDataBean
{
  public AmexSettlementDataBean( )
  {
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer filename  = new StringBuffer(super.getDownloadFilenameBase());
    filename.insert(0,"amex_");
    return ( filename.toString() );
  }
  
  public void loadBatchDetails( long merchantId, Date beginDate, int batchNumber )
  {
    ResultSetIterator       it                = null;
    ResultSet               resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:74^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_number          as merchant_number,
//                  dt.dba_name                 as dba_name,
//                  dt.amex_se_number           as settlement_id,
//                  dt.batch_date               as batch_date,
//                  dt.batch_number             as batch_number,
//                  dt.card_number              as card_number,
//                  dt.transaction_date         as tran_date,
//                  dt.approval_code            as auth_code,
//                  dt.debit_credit_indicator   as debit_credit_ind,
//                  ( dt.transaction_amount *
//                    decode(dt.debit_credit_indicator,
//                           'C',-1,1) )        as tran_amount,
//                  dt.reference_number         as ref_num,
//                  dt.load_filename            as load_filename,
//                  dt.output_filename          as output_filename
//          from    amex_settlement             dt
//          where   dt.merchant_number = :merchantId and
//                  dt.batch_date = :beginDate and
//                  dt.batch_number = :batchNumber
//          order by dt.card_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_number          as merchant_number,\n                dt.dba_name                 as dba_name,\n                dt.amex_se_number           as settlement_id,\n                dt.batch_date               as batch_date,\n                dt.batch_number             as batch_number,\n                dt.card_number              as card_number,\n                dt.transaction_date         as tran_date,\n                dt.approval_code            as auth_code,\n                dt.debit_credit_indicator   as debit_credit_ind,\n                ( dt.transaction_amount *\n                  decode(dt.debit_credit_indicator,\n                         'C',-1,1) )        as tran_amount,\n                dt.reference_number         as ref_num,\n                dt.load_filename            as load_filename,\n                dt.output_filename          as output_filename\n        from    amex_settlement             dt\n        where   dt.merchant_number =  :1  and\n                dt.batch_date =  :2  and\n                dt.batch_number =  :3 \n        order by dt.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AmexSettlementDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setInt(3,batchNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AmexSettlementDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:96^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new BatchDetail(resultSet) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( Exception e )
    {
      logEntry("loadBatchDetails()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadBatchSummary( String loadFilename )
  {
    ResultSetIterator       it                = null;
    ResultSet               resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:125^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_number          as merchant_number,
//                  mf.dba_name                 as dba_name,
//                  dt.amex_se_number           as settlement_id,
//                  dt.batch_date               as batch_date,
//                  dt.batch_number             as batch_number,
//                  sum( decode(dt.debit_credit_indicator,  
//                              'D',1,
//                              0 ) )           as debit_count,
//                  sum( decode(dt.debit_credit_indicator,  
//                              'D',dt.transaction_amount,
//                              0 ) )           as debit_amount,
//                  sum( decode(dt.debit_credit_indicator,
//                              'C',1,
//                              0 ) )           as credit_count,
//                  sum( decode(dt.debit_credit_indicator,
//                              'C',dt.transaction_amount,
//                              0 ) )           as credit_amount,
//                  sum( dt.transaction_amount *
//                       decode(dt.debit_credit_indicator,
//                              'C',-1,1) )     as net_amount,
//                  dt.load_filename            as load_filename,
//                  dt.output_filename          as output_filename
//          from    amex_settlement           dt,
//                  mif                       mf
//          where   dt.load_file_id = load_filename_to_load_file_id(:loadFilename) and
//                  mf.merchant_number(+) = dt.merchant_number
//          group by  dt.merchant_number,mf.dba_name,dt.amex_se_number,
//                    dt.batch_date,dt.batch_number,dt.load_filename,
//                    dt.output_filename
//          order by dt.merchant_number      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_number          as merchant_number,\n                mf.dba_name                 as dba_name,\n                dt.amex_se_number           as settlement_id,\n                dt.batch_date               as batch_date,\n                dt.batch_number             as batch_number,\n                sum( decode(dt.debit_credit_indicator,  \n                            'D',1,\n                            0 ) )           as debit_count,\n                sum( decode(dt.debit_credit_indicator,  \n                            'D',dt.transaction_amount,\n                            0 ) )           as debit_amount,\n                sum( decode(dt.debit_credit_indicator,\n                            'C',1,\n                            0 ) )           as credit_count,\n                sum( decode(dt.debit_credit_indicator,\n                            'C',dt.transaction_amount,\n                            0 ) )           as credit_amount,\n                sum( dt.transaction_amount *\n                     decode(dt.debit_credit_indicator,\n                            'C',-1,1) )     as net_amount,\n                dt.load_filename            as load_filename,\n                dt.output_filename          as output_filename\n        from    amex_settlement           dt,\n                mif                       mf\n        where   dt.load_file_id = load_filename_to_load_file_id( :1 ) and\n                mf.merchant_number(+) = dt.merchant_number\n        group by  dt.merchant_number,mf.dba_name,dt.amex_se_number,\n                  dt.batch_date,dt.batch_number,dt.load_filename,\n                  dt.output_filename\n        order by dt.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.AmexSettlementDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.AmexSettlementDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:157^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new BatchSummary(resultSet) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( Exception e )
    {
      logEntry( "loadBatchSummary(" + loadFilename + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadFileSummary( Date beginDate, Date endDate )
  {
    ResultSetIterator       it                = null;
    ResultSet               resultSet         = null;
    String                  showPending       = getData("showPending");
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:187^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.output_filename          as output_filename,
//                  ams.load_filename           as load_filename,
//                  --ams.output_filename         as outgoing_file_list,  
//                  trunc(ams.process_end_date) as transmit_date,
//                  sum( decode(dt.debit_credit_indicator,  
//                              'D',1,
//                              0 ) )           as debit_count,
//                  sum( decode(dt.debit_credit_indicator,  
//                              'D',dt.transaction_amount,
//                              0 ) )           as debit_amount,
//                  sum( decode(dt.debit_credit_indicator,
//                              'C',1,
//                              0 ) )           as credit_count,
//                  sum( decode(dt.debit_credit_indicator,
//                              'C',dt.transaction_amount,
//                              0 ) )           as credit_amount,
//                  sum( dt.transaction_amount *
//                       decode(dt.debit_credit_indicator,
//                              'C',-1,1) )     as net_amount                  
//          from    amex_settlement_process   ams,
//                  load_file_index           lfi,
//                  amex_settlement           dt
//          where   ( trunc(ams.process_end_date) between :beginDate and :endDate or
//                    ams.process_sequence is null and :showPending = 'Y' ) and
//                  ams.process_type = 0 and
//                  lfi.load_filename(+) = ams.load_filename and
//                  (
//                    (not lfi.load_file_id is null and
//                     dt.load_file_id = lfi.load_file_id) or
//                    (lfi.load_file_id is null and
//                     dt.load_filename = lfi.load_filename)
//                  )
//          group by  dt.output_filename,
//                    ams.load_filename,                  
//                    trunc(ams.process_end_date)
//          order by dt.output_filename,ams.load_filename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.output_filename          as output_filename,\n                ams.load_filename           as load_filename,\n                --ams.output_filename         as outgoing_file_list,  \n                trunc(ams.process_end_date) as transmit_date,\n                sum( decode(dt.debit_credit_indicator,  \n                            'D',1,\n                            0 ) )           as debit_count,\n                sum( decode(dt.debit_credit_indicator,  \n                            'D',dt.transaction_amount,\n                            0 ) )           as debit_amount,\n                sum( decode(dt.debit_credit_indicator,\n                            'C',1,\n                            0 ) )           as credit_count,\n                sum( decode(dt.debit_credit_indicator,\n                            'C',dt.transaction_amount,\n                            0 ) )           as credit_amount,\n                sum( dt.transaction_amount *\n                     decode(dt.debit_credit_indicator,\n                            'C',-1,1) )     as net_amount                  \n        from    amex_settlement_process   ams,\n                load_file_index           lfi,\n                amex_settlement           dt\n        where   ( trunc(ams.process_end_date) between  :1  and  :2  or\n                  ams.process_sequence is null and  :3  = 'Y' ) and\n                ams.process_type = 0 and\n                lfi.load_filename(+) = ams.load_filename and\n                (\n                  (not lfi.load_file_id is null and\n                   dt.load_file_id = lfi.load_file_id) or\n                  (lfi.load_file_id is null and\n                   dt.load_filename = lfi.load_filename)\n                )\n        group by  dt.output_filename,\n                  ams.load_filename,                  \n                  trunc(ams.process_end_date)\n        order by dt.output_filename,ams.load_filename";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.AmexSettlementDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setString(3,showPending);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.AmexSettlementDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:225^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new SummaryData(resultSet) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( Exception e )
    {
      logEntry( "loadFileSummary()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
}/*@lineinfo:generated-code*/