/*@lineinfo:filename=NewAppRevenueDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/NewAppRevenueDataBean.sqlj $

  Description:
  
    This report replaces the old Sales Activity Report.

    Apps are now selected by "channel" which are one or more app types as 
    defined by the new tables app_channel and app_channel_type.  This allows 
    multiple app types to be viewed at once.  

    The main function of the report is to list apps of a particular channel 
    that were submitted between a specified date range along with their 
    calculated revenue generation (based on app estimates of the merchant's 
    V/MC volume and average ticket). Each app's pricing and cost type is 
    determined from application data and linked to various cost constants as 
    defined by the app_pricing_cost table and pricing type info in the 
    app_pricing_type table.  These tables are linked to from app data in 
    the tranchrg and merchant tables in one of two ways:

    Old App Style

     tranchrg.tranchrg_discrate_type -> app_pricing_maps.discrate_type
     merchant.pricing_grid -> app_pricing_maps.pricing_grid
     app_pricing_maps.cost_type -> app_pricing_cost.cost_type
     app_pricing_maps.pricing_type -> app_pricing_type.pricing_type

    or

    New App Style

     tranchrg.tranchrg_interchangefee_type -> appo_ic_bets.bet_type
     tranchrg.tranchrg_interchangefee_fee -> appo_ic_bets.combo_id
     appo_ic_bets.cost_type -> app_pricing_cost.cost_type
     appo_ic_bets.pricing_type -> app_pricing_type.pricing_type

    Apps are only viewable if their app type is defined in the app_channel 
    tables and they have records in the appo_ic_bets table (which has had 
    several columns added to link it to app_pricing_cost and app_pricing_type) 
    or in the new table app_pricing_maps.  Newer apps use the appo_ic_bets 
    table, older apps must have records manually created for them in the 
    app_pricing_maps table.  Going forward additional new apps can be made 
    visible in this report (assuming the new apps use the appo_ic_bets table) 
    by simply adding a channel for the app.  Old existing apps that are not 
    visible can be made visible by adding cost and pricing type mapping 
    records to app_pricing_maps (channels have already been defined for all 
    existing app types as of 10-1-2004).
    
    
    Handy query for examining a particular app types various mapping combos:

        select  distinct a.app_type,
                t.tranchrg_discrate_type,
                m.pricing_grid,
                t.tranchrg_interchangefee_type,
                t.tranchrg_interchangefee_fee
        from    tranchrg t,
                merchant m,
                application a
        where   trunc(a.app_created_date) between sysdate - 7 and sysdate
                and a.app_type = :appType
                and a.app_seq_num = t.app_seq_num
                and t.cardtype_code = 1
                and t.app_seq_num = m.app_seq_num

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 10/08/04 10:10a $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class NewAppRevenueDataBean extends com.mes.forms.DateRangeReport
{
  private List    details                   = new ArrayList();
  private Map     appCodes                  = new HashMap();
  private int     totalCount                = -1;
  private double  totalAnnualSales          = 0;
  private double  totalNetRevenue           = 0;
  private int     totalRevenueInBasisPoints = 0;
  private int     totalTicketCount          = 0;
  private double  averageTicket             = 0;
  private double  averageMinimum            = 0;
  
  protected class ChannelTable extends DropDownTable
  {
    public ChannelTable() throws java.sql.SQLException
    {
      ResultSetIterator it  = null;
      
      try
      {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:117^9*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct act.channel_num,
//                   act.description
//            from   app_channel_type act,
//                   app_channel chn
//            where  act.channel_num = chn.channel_num(+)
//                   and chn.app_type in
//                    ( select  distinct app_type
//                      from    appo_ic_bets
//                      union
//                      select  distinct app_type
//                      from    app_pricing_maps )
//            order by act.description
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct act.channel_num,\n                 act.description\n          from   app_channel_type act,\n                 app_channel chn\n          where  act.channel_num = chn.channel_num(+)\n                 and chn.app_type in\n                  ( select  distinct app_type\n                    from    appo_ic_bets\n                    union\n                    select  distinct app_type\n                    from    app_pricing_maps )\n          order by act.description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.NewAppRevenueDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.NewAppRevenueDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:131^9*/
        ResultSet rs = it.getResultSet();
        while (rs.next())
        {
          addElement(rs);
        }
      }
      finally
      {
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }
  
  protected class StatusTable extends DropDownTable
  {
    public StatusTable()
    {
      addElement("ANY", "Any");
      addElement("NEW", "New");
      addElement("APRV","Approved");
      addElement("DECL","Declined");
      addElement("PEND","Pending");
      addElement("CAN", "Canceled");
      addElement("ACT", "Activated");
    }
  }
  
  public NewAppRevenueDataBean()
  {
  }
  
  /*
  ** private String getDefaultChannel() throws Exception
  **
  ** Currently, only MES users with a user record hierarchy node of 9999999999 
  ** may view multiple app types.  All other users may only view the default 
  ** channel defined for their app type which is determined by doing a lookup 
  ** of their default app type based on their user hierarchy node.  The lookup
  ** also verifies that the user's default app type is represented in either
  ** appo_ic_bets or app_pricing_maps.  If a default app type cannot be 
  ** determined an error occurs and the report will not display.
  **
  ** RETURNS: default channel num as a String (for use as field data).
  **
  ** Throws exception if no default channel is found.
  */
  private String getDefaultChannel() throws Exception
  {
    ResultSetIterator it = null;
    String defaultChannel = null;
    long userNode = user.getHierarchyNode();
    
    if (userNode == 9999999999L)
    {
      defaultChannel = "1";
    }
    else
    {
      try
      {
        connect();
    
        /*@lineinfo:generated-code*//*@lineinfo:195^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ct.channel_num default_channel_num
//            from    app_channel_type ct,
//                    ( select  oa.app_type app_type
//                      from    org_app oa,
//                              t_hierarchy h,
//                              ( select  min(h2.relation) relation
//                                from    org_app oa2,
//                                        t_hierarchy h2
//                                where   h2.descendent = :userNode
//                                        and oa2.hierarchy_node = h2.ancestor ) minh
//                      where   h.descendent = :userNode
//                              and oa.hierarchy_node = h.ancestor
//                              and h.relation = minh.relation ) dflt_app_type,
//                    ( select  distinct act.channel_num channel_num
//                      from    app_channel_type act,
//                              app_channel chn
//                      where   act.channel_num = chn.channel_num(+)
//                              and chn.app_type in
//                              ( select  distinct app_type
//                                from    appo_ic_bets
//                                union
//                                select  distinct app_type
//                                from    app_pricing_maps ) ) allowed_chn
//            where   ct.channel_num = dflt_app_type.app_type + 1000
//                    and ct.channel_num = allowed_chn.channel_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ct.channel_num default_channel_num\n          from    app_channel_type ct,\n                  ( select  oa.app_type app_type\n                    from    org_app oa,\n                            t_hierarchy h,\n                            ( select  min(h2.relation) relation\n                              from    org_app oa2,\n                                      t_hierarchy h2\n                              where   h2.descendent =  :1 \n                                      and oa2.hierarchy_node = h2.ancestor ) minh\n                    where   h.descendent =  :2 \n                            and oa.hierarchy_node = h.ancestor\n                            and h.relation = minh.relation ) dflt_app_type,\n                  ( select  distinct act.channel_num channel_num\n                    from    app_channel_type act,\n                            app_channel chn\n                    where   act.channel_num = chn.channel_num(+)\n                            and chn.app_type in\n                            ( select  distinct app_type\n                              from    appo_ic_bets\n                              union\n                              select  distinct app_type\n                              from    app_pricing_maps ) ) allowed_chn\n          where   ct.channel_num = dflt_app_type.app_type + 1000\n                  and ct.channel_num = allowed_chn.channel_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.NewAppRevenueDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userNode);
   __sJT_st.setLong(2,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.NewAppRevenueDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:222^9*/
        ResultSet rs = it.getResultSet();
        
        if (rs.next())
        {
          defaultChannel = rs.getString("default_channel_num");
        }
        else
        {
          throw new Exception("Unable to determine default channel for user "
            + user.getLoginName());
        }
      }
      finally
      {
        try { it.close(); } catch (Exception e) { }
        cleanUp();
      }
    }
      
    return defaultChannel;
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // determine default app type
      String defaultChannel = getDefaultChannel();
      
      // create a drop down or hidden app type field
      // for now base the field type on the user's default app type
      // if user is mes (has default app type of 28, MES New) then
      // show drop down options, otherwise make it hidden and only
      // allow user to view apps of their own default app type
      Field channelField = null;
      if (defaultChannel.equals("1"))
      {
        channelField = new DropDownField("channel","Application Type",
          new ChannelTable(),true,defaultChannel);
      }
      // not MES, so do not show app type options, force to
      // always view their default app type (for non-MES users)
      else if (defaultChannel != null)
      {
        channelField = new HiddenField("channel",defaultChannel);
      }
      // if default app type was not found, problem...
      else
      {
        throw new Exception("Channel could not be determined for user "
            + user.getLoginName());
      }
      fields.add(channelField);
      
      fields.add(new DropDownField("status","App Status",new StatusTable(),
          true,"ANY"));
      
      fields.add(new CheckboxField("appInfo","App Info",false));
      fields.add(new CheckboxField("costDetails","Cost Details",false));
      fields.add(new CheckboxField("assocCol","Association #",false));
      fields.add(new CheckboxField("nonBankInfo","Non-Bank Fees",false));
      
      fields.add(new ButtonField("Select"));
    
      fields.setGroupHtmlExtra("class=\"smallText\"");
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }

  public class DetailRow
  {
    public int getAppSeqNum()
    {
      return appSeqNum;
    }
    
    public int getAppType()
    {
      return appType;
    }
    
    public String getAppCode()
    {
      return appCode;
    }
    
    public String getAppDate()
    {
      return appDate;
    }
    
    public String getActivationStatus()
    {
      return activationStatus;
    }
    
    public int getAppUserId()
    {
      return appUserId;
    }
    
    public String getLoginName()
    {
      return loginName;
    }
    
    public String getTRepCode()
    {
      return tRepCode;
    }

    public String getMerchName()
    {
      return merchName;
    }
    
    public String getMerchNum()
    {
      return merchNum;
    }
    
    public double getAverageTicket()
    {
      return averageTicket;
    }
    
    public double getVmcMonthlySales()
    {
      return vmcMonthlySales;
    }
    
    public double getVmcAnnualSales()
    {
      return vmcAnnualSales;
    }
    
    public double getVmcRate()
    {
      return vmcRate;
    }
    
    public double getMonthlyMinimum()
    {
      return monthlyMinimum;
    }
    
    public double getVmcPerTran()
    {
      return vmcPerTran;
    }
    
    public double getVmcPerAuth()
    {
      return vmcPerAuth;
    }
    
    public int getPricingType()
    {
      return pricingType;
    }
    
    public String getPricingCode()
    {
      return pricingCode;
    }
    
    public double getInetPerTran()
    {
      return inetPerTran;
    }
    
    public int getCostType()
    {
      return costType;
    }
    
    public String getCostCode()
    {
      return costCode;
    }
    
    public double getCostRate()
    {
      return costRate;
    }

    public double getCostFixed()
    {
      return costFixed;
    }
    
    public String getAssocNum()
    {
      return assocNum;
    }
    
    public int getRevenueInBasisPoints()
    {
      return revenueInBasisPoints;
    }
    
    public double getNetRevenue()
    {
      return netRevenue;
    }
    
    public boolean getAmexAccepted()
    {
      return amexAccepted;
    }
    
    public double getAmexPerAuth()
    {
      return amexPerAuth;
    }
    
    public boolean getAmexSplitDial()
    {
      return amexSplitDial;
    }
    
    public boolean getDiscAccepted()
    {
      return discAccepted;
    }
    
    public double getDiscPerAuth()
    {
      return discPerAuth;
    }
    
    public int getAnnualTicketCount() {
        return annualTicketCount;
    }

    private int       appSeqNum;
    private int       appType;
    private String    appCode;
    private String    appDate;
    private int       appUserId;
    private String    loginName;
    private String    tRepCode;
    private String    merchName;
    private String    merchNum;
    private String    creditStatus;
    private String    activationStatus;
    private boolean   isActivated;
    private double    vmcMonthlySales;
    private double    vmcAnnualSales;
    private double    averageTicket;
    private double    vmcRate;
    private double    monthlyMinimum;
    private double    vmcPerTran;
    private double    vmcPerAuth;
    private int       pricingType;
    private String    pricingCode;
    private double    inetPerTran;
    private boolean   amexAccepted;
    private double    amexPerAuth;
    private boolean   amexSplitDial;
    private boolean   discAccepted;
    private double    discPerAuth;
    private int       costType;
    private String    costCode;
    private double    costRate;
    private double    costFixed;
    private String    assocNum;
    
    private int       revenueInBasisPoints;
    private double    netRevenue;
    private int       annualTicketCount;
    
    public static final int PT_STANDARD           = 1;
    public static final int PT_INTERCHANGE_PLUS   = 2;
    public static final int PT_VARIABLE           = 3;

    public DetailRow(ResultSet rs)
    {
      try
      {
        appSeqNum         = rs.getInt   ("app_seq_num");
        appType           = rs.getInt   ("app_type");
        appCode           = (String)appCodes.get(rs.getString("app_type"));
        appDate           = rs.getString("app_date");
        appUserId         = rs.getInt   ("app_user_id");
        loginName         = rs.getString("login_name");
        tRepCode          = rs.getString("t_rep_code");
        merchName         = rs.getString("merch_name");
        merchNum          = rs.getString("merch_num");
        activationStatus  = rs.getString("activation_status");
        vmcMonthlySales   = rs.getDouble("vmc_monthly_sales");
        averageTicket     = rs.getDouble("average_ticket");
        vmcRate           = rs.getDouble("vmc_rate");
        monthlyMinimum    = rs.getDouble("monthly_minimum");
        vmcPerTran        = rs.getDouble("vmc_per_tran");
        vmcPerAuth        = rs.getDouble("vmc_per_auth");
        inetPerTran       = rs.getDouble("inet_per_tran");
        amexAccepted      = rs.getString("amex_accepted").equals("Y");
        amexPerAuth       = rs.getDouble("amex_per_auth");
        amexSplitDial     = rs.getString("amex_split_dial").equals("Y");
        discAccepted      = rs.getString("disc_accepted").equals("Y");
        discPerAuth       = rs.getDouble("disc_per_auth");
        pricingType       = rs.getInt   ("pricing_type");
        pricingCode       = rs.getString("pricing_code");
        costType          = rs.getInt   ("cost_type");
        costCode          = rs.getString("cost_code");
        costRate          = rs.getDouble("cost_rate");
        costFixed         = rs.getDouble("cost_fixed");
        assocNum          = rs.getString("assoc_num");
        
        vmcAnnualSales = vmcMonthlySales * 12;
        annualTicketCount = (int)(vmcAnnualSales / averageTicket);
        
        // calculate revenue numbers
        switch (pricingType)
        {
          case PT_STANDARD:
          case PT_INTERCHANGE_PLUS:
            calculateStandardAndInterchangePricing();
            break;
              
          case PT_VARIABLE:
            calculateVariablePricing();
            break;

          default:
            throw new Exception("Unsupported pricing type (" 
                + pricingType + ")");
        }
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + "::DetailRow(): "
          + e.toString());
        e.printStackTrace();
        logEntry("DetailRow()",e.toString());
      }
    }
    
    private void calculateStandardAndInterchangePricing()
    {
      double RR   = 0;                          // raw revenue rate
      double NR   = 0;                          // generated net discount revenue
      double TKT  = averageTicket;              // average ticket
      double AS   = vmcAnnualSales;             // annual sales
      
      // make sure average ticket and annual sales are both above 0
      if (TKT > 0 && AS > 0)
      {
        double DR   = vmcRate / 100;            // discount rate
        double PT   = vmcPerTran + vmcPerAuth;  // per tran fixed income
        double CR   = costRate / 100;           // cost percentage
        double CF   = costFixed;                // fixed cost amount
        double MM   = monthlyMinimum;           // monthly discount minimum
    
        // revenue rate
        RR = ( DR - CR ) + ( ( PT - CF ) / TKT );
    
        // annual net revenue dollar amount
        NR = (int) ( ( ( AS * RR ) * 100 ) / 100 );
    
        /*
        
          IF so desired you could use monthly minimum as revenue...
        
        // if net revenue is under monthly minimum then recalculate
        // numbers using monthly minimum
        if (NR / 12 < MM)
        {
          NR = MM * 12;
          RR = NR / AS;
        }
        
        */
    
        // store results
        revenueInBasisPoints = (int) ( ( RR * 10000 ) + 0.5 );
        netRevenue = NR;
      }
    }
    
    private void calculateVariablePricing()
    {
      revenueInBasisPoints = 0;
      netRevenue = 0;
    }

  }

  protected boolean autoSubmit()
  {
    return autoLoad();
  }

  protected boolean autoLoad()
  {
    boolean           loadOk  = false;
    ResultSetIterator it      = null;

    try
    {
      int channelNum = fields.getField("channel").asInt();
      String status = fields.getData("status");
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:635^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct app_type,
//                  appsrctype_code app_code
//          from    org_app
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct app_type,\n                appsrctype_code app_code\n        from    org_app";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.NewAppRevenueDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.NewAppRevenueDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:640^7*/
      ResultSet rs = it.getResultSet();
      while (rs.next())
      {
        appCodes.put(rs.getString("app_type"),rs.getString("app_code"));
      }
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:648^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    app_seq_num,
//                    app_type,
//                    to_char( submit_date, 'fmmm-dd-fmyy' ) app_date,
//                    app_user_id,
//                    login_name,
//                    t_rep_code,
//                    merch_name,
//                    merch_num,
//                    nvl( to_char( activate_date, 'fmmm-dd-fmyy' ),
//                      credit_status ) activation_status,
//                    average_ticket,
//                    vmc_monthly_sales,
//                    pricing_type,
//                    pricing_code,
//                    vmc_rate,
//                    vmc_per_tran,
//                    vmc_per_auth,
//                    inet_per_tran,
//                    amex_accepted,
//                    amex_per_auth,
//                    amex_split_dial,
//                    disc_accepted,
//                    disc_per_auth,
//                    monthly_minimum,
//                    cost_type,
//                    cost_code,
//                    cost_rate,
//                    cost_fixed,
//                    assoc_num
//          from      ( select  app.app_seq_num                           app_seq_num,
//                              app.app_type                              app_type,
//                              app.app_created_date                      submit_date,
//                              app.app_user_id                           app_user_id,
//                              users.login_name                          login_name,
//                              nvl( to_char( tmerch.rep_code ), '---' )  t_rep_code,
//                              merch.merch_business_name                 merch_name,
//                              nvl2( to_char( merch.merch_number ),
//                                    to_char( merch.merch_number ), '---' )
//                                                                        merch_num,
//                              merch.merch_average_cc_tran               average_ticket,
//                              merch.date_activated                      activate_date,
//                              decode( merch.merch_credit_status, 1,'APRV',
//                                      2,'DECL',3,'PEND',4,'CAN',5,'NEW','---' )
//                                                                        credit_status,
//                              merch.merch_month_visa_mc_sales           vmc_monthly_sales,
//                              vmc.tranchrg_disc_rate                    vmc_rate,
//                              nvl(vmc.tranchrg_mmin_chrg, 0)            monthly_minimum,
//                              ( nvl(vmc.tranchrg_pass_thru, 0) +
//                                nvl(vmc.tranchrg_per_tran, 0) )         vmc_per_tran,
//                              ( nvl(vmc.tranchrg_per_auth,  0) +
//                                nvl(inet.tranchrg_pass_thru, 0) +
//                                nvl(inet.tranchrg_per_tran, 0) )        vmc_per_auth,
//                              ( nvl(inet.tranchrg_pass_thru, 0) +
//                                nvl(inet.tranchrg_per_tran, 0) )        inet_per_tran,
//                              nvl2(amex.app_seq_num,'Y','N')            amex_accepted,
//                              nvl(amex.tranchrg_per_tran, 0)            amex_per_auth,
//                              nvl(amex.split_dial,'N')                  amex_split_dial,
//                              nvl2(disc.app_seq_num,'Y','N')            disc_accepted,
//                              nvl(disc.tranchrg_per_tran, 0)            disc_per_auth,
//                              pc.pricing_type                           pricing_type,
//                              pc.pricing_code                           pricing_code,
//                              pc.cost_type                              cost_type,
//                              pc.cost_code                              cost_code,
//                              pc.percent                                cost_rate,
//                              pc.fixed                                  cost_fixed,
//                              nvl(mif.dmagent,'---')                    assoc_num
//                      from    ( select  *
//                                from    application
//                                where   app_type in ( select  app_type
//                                                      from    app_channel
//                                                      where   channel_num = :channelNum )
//                                        and trunc(app_created_date)
//                                          between : fromField.getSqlDateString() 
//                                            and : toField.getSqlDateString() )
//                                                    app,
//                              merchant              merch,
//                              users                 users,
//                              mif                   mif,
//                              transcom_merchant     tmerch,
//                              ( select  *
//                                from    tranchrg
//                                where   cardtype_code = 1 )
//                                                    vmc,
//                              ( select  *
//                                from    tranchrg
//                                where   cardtype_code = 20 )
//                                                    inet,
//                              ( select  t.*,
//                                        mpo.merchpo_split_dial split_dial
//                                from    tranchrg t,
//                                        merchpayoption mpo
//                                where   t.cardtype_code = 16
//                                        and t.app_seq_num = mpo.app_seq_num(+)
//                                        and mpo.cardtype_code = 16 )
//                                                    amex,
//                              ( select  *
//                                from    tranchrg
//                                where   cardtype_code = 14 )
//                                                    disc,
//                              -- this subquery merges newer appo_ic_bets apps 
//                              -- with any older apps that have entries in 
//                              -- app_pricing_maps
//                              ( select  aicb.app_type,
//                                        cost.cost_type,
//                                        cost.short_description cost_code,
//                                        cost.percent,
//                                        cost.fixed,
//                                        apt.pricing_type,
//                                        apt.short_description pricing_code,
//                                        aicb.bet_type,
//                                        aicb.combo_id,
//                                        null pricing_grid,
//                                        null discrate_type
//                                from    app_pricing_cost cost,
//                                        app_pricing_type apt,
//                                        appo_ic_bets aicb
//                                where   cost.cost_type = aicb.cost_type
//                                        and apt.pricing_type = aicb.pricing_type
//                                union
//                                select  maps.app_type,
//                                        cost.cost_type,
//                                        cost.short_description cost_code,
//                                        cost.percent,
//                                        cost.fixed,
//                                        apt.pricing_type,
//                                        apt.short_description pricing_code,
//                                        null bet_type,
//                                        null combo_id,
//                                        maps.pricing_grid,
//                                        maps.discrate_type     
//                                from    app_pricing_cost cost,
//                                        app_pricing_type apt,
//                                        app_pricing_maps maps
//                                where   cost.cost_type = maps.cost_type
//                                        and apt.pricing_type = maps.pricing_type )
//                                                    pc
//                      where     app.app_user_id = users.user_id
//                                and app.app_seq_num = merch.app_seq_num
//                                and app.app_seq_num = vmc.app_seq_num
//                                and app.app_seq_num = inet.app_seq_num(+)
//                                and app.app_seq_num = amex.app_seq_num(+)
//                                and app.app_seq_num = disc.app_seq_num(+)
//                                and app.app_type = pc.app_type
//                                and app.app_seq_num = tmerch.app_seq_num(+)
//                                and (   ( vmc.tranchrg_interchangefee_type = pc.bet_type
//                                          and vmc.tranchrg_interchangefee_fee = pc.combo_id )
//                                     or ( vmc.tranchrg_discrate_type = pc.discrate_type
//                                          and ( merch.pricing_grid = pc.pricing_grid 
//                                                or pc.pricing_grid = -1 ) ) 
//                                     -- special transcom clause
//                                     or ( app.app_type = 8      -- transcom app type
//                                          and pc.bet_type = 42  -- bet type for transcom
//                                          and tmerch.bet_table = pc.combo_id ) )
//                                and merch.merch_number = mif.merchant_number(+)
//                      order by  t_rep_code, submit_date desc )
//          where     ( :status = 'ANY'
//                      or :status = credit_status
//                      or ( :status = 'ACT' and activate_date is not null ) )
//                    and instr( lower( merch_name ),'test') = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1216 =  fromField.getSqlDateString();
 String __sJT_1217 =  toField.getSqlDateString();
  try {
   String theSqlTS = "select    app_seq_num,\n                  app_type,\n                  to_char( submit_date, 'fmmm-dd-fmyy' ) app_date,\n                  app_user_id,\n                  login_name,\n                  t_rep_code,\n                  merch_name,\n                  merch_num,\n                  nvl( to_char( activate_date, 'fmmm-dd-fmyy' ),\n                    credit_status ) activation_status,\n                  average_ticket,\n                  vmc_monthly_sales,\n                  pricing_type,\n                  pricing_code,\n                  vmc_rate,\n                  vmc_per_tran,\n                  vmc_per_auth,\n                  inet_per_tran,\n                  amex_accepted,\n                  amex_per_auth,\n                  amex_split_dial,\n                  disc_accepted,\n                  disc_per_auth,\n                  monthly_minimum,\n                  cost_type,\n                  cost_code,\n                  cost_rate,\n                  cost_fixed,\n                  assoc_num\n        from      ( select  app.app_seq_num                           app_seq_num,\n                            app.app_type                              app_type,\n                            app.app_created_date                      submit_date,\n                            app.app_user_id                           app_user_id,\n                            users.login_name                          login_name,\n                            nvl( to_char( tmerch.rep_code ), '---' )  t_rep_code,\n                            merch.merch_business_name                 merch_name,\n                            nvl2( to_char( merch.merch_number ),\n                                  to_char( merch.merch_number ), '---' )\n                                                                      merch_num,\n                            merch.merch_average_cc_tran               average_ticket,\n                            merch.date_activated                      activate_date,\n                            decode( merch.merch_credit_status, 1,'APRV',\n                                    2,'DECL',3,'PEND',4,'CAN',5,'NEW','---' )\n                                                                      credit_status,\n                            merch.merch_month_visa_mc_sales           vmc_monthly_sales,\n                            vmc.tranchrg_disc_rate                    vmc_rate,\n                            nvl(vmc.tranchrg_mmin_chrg, 0)            monthly_minimum,\n                            ( nvl(vmc.tranchrg_pass_thru, 0) +\n                              nvl(vmc.tranchrg_per_tran, 0) )         vmc_per_tran,\n                            ( nvl(vmc.tranchrg_per_auth,  0) +\n                              nvl(inet.tranchrg_pass_thru, 0) +\n                              nvl(inet.tranchrg_per_tran, 0) )        vmc_per_auth,\n                            ( nvl(inet.tranchrg_pass_thru, 0) +\n                              nvl(inet.tranchrg_per_tran, 0) )        inet_per_tran,\n                            nvl2(amex.app_seq_num,'Y','N')            amex_accepted,\n                            nvl(amex.tranchrg_per_tran, 0)            amex_per_auth,\n                            nvl(amex.split_dial,'N')                  amex_split_dial,\n                            nvl2(disc.app_seq_num,'Y','N')            disc_accepted,\n                            nvl(disc.tranchrg_per_tran, 0)            disc_per_auth,\n                            pc.pricing_type                           pricing_type,\n                            pc.pricing_code                           pricing_code,\n                            pc.cost_type                              cost_type,\n                            pc.cost_code                              cost_code,\n                            pc.percent                                cost_rate,\n                            pc.fixed                                  cost_fixed,\n                            nvl(mif.dmagent,'---')                    assoc_num\n                    from    ( select  *\n                              from    application\n                              where   app_type in ( select  app_type\n                                                    from    app_channel\n                                                    where   channel_num =  :1  )\n                                      and trunc(app_created_date)\n                                        between  :2  \n                                          and  :3  )\n                                                  app,\n                            merchant              merch,\n                            users                 users,\n                            mif                   mif,\n                            transcom_merchant     tmerch,\n                            ( select  *\n                              from    tranchrg\n                              where   cardtype_code = 1 )\n                                                  vmc,\n                            ( select  *\n                              from    tranchrg\n                              where   cardtype_code = 20 )\n                                                  inet,\n                            ( select  t.*,\n                                      mpo.merchpo_split_dial split_dial\n                              from    tranchrg t,\n                                      merchpayoption mpo\n                              where   t.cardtype_code = 16\n                                      and t.app_seq_num = mpo.app_seq_num(+)\n                                      and mpo.cardtype_code = 16 )\n                                                  amex,\n                            ( select  *\n                              from    tranchrg\n                              where   cardtype_code = 14 )\n                                                  disc,\n                            -- this subquery merges newer appo_ic_bets apps \n                            -- with any older apps that have entries in \n                            -- app_pricing_maps\n                            ( select  aicb.app_type,\n                                      cost.cost_type,\n                                      cost.short_description cost_code,\n                                      cost.percent,\n                                      cost.fixed,\n                                      apt.pricing_type,\n                                      apt.short_description pricing_code,\n                                      aicb.bet_type,\n                                      aicb.combo_id,\n                                      null pricing_grid,\n                                      null discrate_type\n                              from    app_pricing_cost cost,\n                                      app_pricing_type apt,\n                                      appo_ic_bets aicb\n                              where   cost.cost_type = aicb.cost_type\n                                      and apt.pricing_type = aicb.pricing_type\n                              union\n                              select  maps.app_type,\n                                      cost.cost_type,\n                                      cost.short_description cost_code,\n                                      cost.percent,\n                                      cost.fixed,\n                                      apt.pricing_type,\n                                      apt.short_description pricing_code,\n                                      null bet_type,\n                                      null combo_id,\n                                      maps.pricing_grid,\n                                      maps.discrate_type     \n                              from    app_pricing_cost cost,\n                                      app_pricing_type apt,\n                                      app_pricing_maps maps\n                              where   cost.cost_type = maps.cost_type\n                                      and apt.pricing_type = maps.pricing_type )\n                                                  pc\n                    where     app.app_user_id = users.user_id\n                              and app.app_seq_num = merch.app_seq_num\n                              and app.app_seq_num = vmc.app_seq_num\n                              and app.app_seq_num = inet.app_seq_num(+)\n                              and app.app_seq_num = amex.app_seq_num(+)\n                              and app.app_seq_num = disc.app_seq_num(+)\n                              and app.app_type = pc.app_type\n                              and app.app_seq_num = tmerch.app_seq_num(+)\n                              and (   ( vmc.tranchrg_interchangefee_type = pc.bet_type\n                                        and vmc.tranchrg_interchangefee_fee = pc.combo_id )\n                                   or ( vmc.tranchrg_discrate_type = pc.discrate_type\n                                        and ( merch.pricing_grid = pc.pricing_grid \n                                              or pc.pricing_grid = -1 ) ) \n                                   -- special transcom clause\n                                   or ( app.app_type = 8      -- transcom app type\n                                        and pc.bet_type = 42  -- bet type for transcom\n                                        and tmerch.bet_table = pc.combo_id ) )\n                              and merch.merch_number = mif.merchant_number(+)\n                    order by  t_rep_code, submit_date desc )\n        where     (  :4  = 'ANY'\n                    or  :5  = credit_status\n                    or (  :6  = 'ACT' and activate_date is not null ) )\n                  and instr( lower( merch_name ),'test') = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.NewAppRevenueDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,channelNum);
   __sJT_st.setString(2,__sJT_1216);
   __sJT_st.setString(3,__sJT_1217);
   __sJT_st.setString(4,status);
   __sJT_st.setString(5,status);
   __sJT_st.setString(6,status);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.NewAppRevenueDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:809^7*/
      
      rs = it.getResultSet();
      totalCount = 0;
      while (rs.next())
      {
        DetailRow row     = new DetailRow(rs);
        totalAnnualSales += row.getVmcAnnualSales();
        totalNetRevenue  += row.getNetRevenue();
        totalTicketCount += row.getAnnualTicketCount();
        averageMinimum   += row.getMonthlyMinimum();
        ++totalCount;
        details.add(row);
      }
      if (totalCount > 0)
      {
        averageTicket = totalAnnualSales / totalTicketCount;
        averageMinimum /= totalCount;
        totalRevenueInBasisPoints =
          (int)(0.5 + ((totalNetRevenue / totalAnnualSales) * 10000));
      }

      loadOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::autoLoad(): "
        + e.toString());
      logEntry("autoLoad()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) { }
      cleanUp();
    }

    return loadOk;
  }

  public Iterator getDetailsIterator()
  {
    return details.iterator();
  }

  public boolean mayViewMultipleAppTypes()
  {
    Field channel = getField("channel");
    return (channel != null && channel instanceof DropDownField);
  }
  
  public int getTotalCount()
  {
    return totalCount;
  }
  
  public double getTotalAnnualSales()
  {
    return totalAnnualSales;
  }
  
  public double getTotalNetRevenue()
  {
    return totalNetRevenue;
  }

  public int getTotalRevenueInBasisPoints()
  {
    return totalRevenueInBasisPoints;
  }
  
  public int getTotalTicketCount()
  {
    return totalTicketCount;
  }
  
  public double getAverageTicket()
  {
    return averageTicket;
  }
  
  public double getAverageMinimum()
  {
    return averageMinimum;
  }
}/*@lineinfo:generated-code*/