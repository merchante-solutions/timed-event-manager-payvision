/*@lineinfo:filename=MerchAuthCaptDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchAuthCaptDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/25/03 5:22p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class MerchAuthCaptDataBean extends ReportSQLJBean
{
  public class RowData implements Comparable
  {
    public  Date                ActivityDate        = null;
    public  double              AuthAmount          = 0.0;
    public  int                 AuthCount           = 0;
    public  String              DbaName             = null;
    public  long                MerchantId          = 0L;
    public  double              SettledAmount       = 0.0;
    public  int                 SettledCount        = 0;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      ActivityDate        = resultSet.getDate("activity_date");
      AuthAmount          = resultSet.getDouble("auth_amount");
      AuthCount           = resultSet.getInt("auth_count");
      DbaName             = resultSet.getString("dba_name");
      MerchantId          = resultSet.getLong("merchant_number");
      SettledAmount       = resultSet.getDouble("settled_amount");
      SettledCount        = resultSet.getInt("settled_count");
    }
    
    public int compareTo( Object obj )
    {
      RowData       compareObj  = (RowData) obj;
      int           retVal      = 0;
      
      if ( (retVal = DbaName.compareTo(compareObj.DbaName)) == 0 )
      {
        if ( (retVal = (int)(MerchantId - compareObj.MerchantId)) == 0 )
        {
          retVal = ActivityDate.compareTo( compareObj.ActivityDate );
        }
      }
      return( retVal );
    }
  }
  
  public MerchAuthCaptDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append( "\"Merchant Id\"," );
    line.append( "\"Dba Name\"," );
    line.append( "\"Activity Date\"," );
    line.append( "\"Auth Count\"," );
    line.append( "\"Auth Amount\"," );
    line.append( "\"Settled Count\"," );
    line.append( "\"Settled Amount\"" );
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.MerchantId );
    line.append( ",\"" );
    line.append( record.DbaName );
    line.append( "\"," );
    line.append( record.AuthCount );
    line.append( "," );
    line.append( record.AuthAmount);
    line.append( "," );
    line.append( record.SettledCount );
    line.append( "," );
    line.append( record.SettledAmount);
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_pending_batches_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:163^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (gm pkgroup_merchant) 
//                      index (rs pk_tc33_risk_summary) */
//                  rs.merchant_number                  as merchant_number,
//                  mf.dba_name                         as dba_name,
//                  rs.transaction_date                 as activity_date,
//                  sum(rs.VMC_AUTH_APPROVED_COUNT)     as auth_count,
//                  sum(rs.vmc_auth_approved_amount)    as auth_amount,
//                  sum(rs.VMC_SALES_COUNT)             as settled_count,
//                  sum(rs.vmc_sales_amount)            as settled_amount
//          from    organization            o,
//                  group_merchant          gm,
//                  group_rep_merchant      grm,
//                  tc33_risk_summary       rs,
//                  mif                     mf
//          where   --o.org_group = 386700000 and 
//                  o.org_num = :orgId and
//                  gm.org_num = o.org_num and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                  rs.merchant_number = gm.merchant_number and
//                  rs.TRANSACTION_DATE between :beginDate and :endDate and
//                  mf.merchant_number = rs.merchant_number
//          group by rs.merchant_number, mf.dba_name, rs.transaction_date
//          order by settled_count
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (gm pkgroup_merchant) \n                    index (rs pk_tc33_risk_summary) */\n                rs.merchant_number                  as merchant_number,\n                mf.dba_name                         as dba_name,\n                rs.transaction_date                 as activity_date,\n                sum(rs.VMC_AUTH_APPROVED_COUNT)     as auth_count,\n                sum(rs.vmc_auth_approved_amount)    as auth_amount,\n                sum(rs.VMC_SALES_COUNT)             as settled_count,\n                sum(rs.vmc_sales_amount)            as settled_amount\n        from    organization            o,\n                group_merchant          gm,\n                group_rep_merchant      grm,\n                tc33_risk_summary       rs,\n                mif                     mf\n        where   --o.org_group = 386700000 and \n                o.org_num =  :1  and\n                gm.org_num = o.org_num and\n                grm.user_id(+) =  :2  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :3  = -1 ) and        \n                rs.merchant_number = gm.merchant_number and\n                rs.TRANSACTION_DATE between  :4  and  :5  and\n                mf.merchant_number = rs.merchant_number\n        group by rs.merchant_number, mf.dba_name, rs.transaction_date\n        order by settled_count";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchAuthCaptDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchAuthCaptDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:190^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        if ( resultSet.getInt("auth_count") > 0 && 
             resultSet.getInt("settled_count") == 0 )
        {
          ReportRows.addElement( new RowData( resultSet ) );
        }          
        else if ( resultSet.getInt("settled_count") > 0 )
        {
          // sorted by this value, so we are done with results
          break;      
        }
      }
      resultSet.close();
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      int         inc           = -1;
      
      if ( ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.MONDAY ) ||
           ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.TUESDAY ) )
      {
        // friday data available on monday,
        // sat,sun,mon data available on tuesday
        inc = -3;     
      }
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, inc );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      
      cal.setTime( ReportDateEnd );
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }    
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/