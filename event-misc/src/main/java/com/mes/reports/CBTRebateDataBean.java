/*@lineinfo:filename=CBTRebateDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CBTRebateDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 2/18/03 5:55p $
  Version            : $Revision: 9 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.support.HttpHelper;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class CBTRebateDataBean extends ReportSQLJBean
{
  public static final int     MC_OPENED                       = 0;
  public static final int     MC_CLOSED                       = 1;
  public static final int     MC_ACTIVE                       = 2;
  public static final int     MC_COUNT                        = 3;
  
  // note:  this reused the RT_SUMMARY and RT_DETAILS values
  public static final int     RT_AFFILIATE_REBATE             = 0;
  public static final int     RT_AGENT_REBATE                 = 1;
  public static final int     RT_AFFILIATE_REBATE_MANAGEMENT  = 2;
  public static final int     RT_AGENT_REBATE_MANAGEMENT      = 3;

  // these are types in the bank_relationships table  
  public static final int     AFFILIATE_BANK                  = 0;
  public static final int     AGENT_BANK                      = 1;
  
  public static final int     INC_BASIS_STD                   = 0;
  public static final int     INC_BASIS_NO_BANK               = 1;
  public static final int     INC_BASIS_NO_BANK_NET_PL        = 2;
  
  private static final String[]    CountFields =
  {
    "opened_accounts",        // MC_OPENED
    "closed_accounts",        // MC_CLOSED
    "active_accounts",        // MC_ACTIVE
  };
  
  public class AffiliateRebateData
  {
    private double      BankIncome                  = 0.0;
    private double      BankIncomeYTD               = 0.0;
    public  double      CBTIncome                   = 0.0;
    public  double      CBTIncomeYTD                = 0.0;
    public  long        HierarchyNode               = 0L;
    private int         IncomeBasis                 = INC_BASIS_STD;
    public  long        OrgId                       = 0L;
    public  String      OrgName                     = null;
    public  double      SalesVolume                 = 0.0;
    public  int[]       MerchantCountsMTD           = null;
    public  int[]       MerchantCountsYTD           = null;
    public  double      SalesVolumeYTD              = 0.0;
    public  int         SortOrder                   = 0;
    
    public AffiliateRebateData( ResultSet     resultSet )
      throws java.sql.SQLException
    {
      OrgId             = resultSet.getLong("org_num");
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      IncomeBasis       = resultSet.getInt("income_basis");
      OrgName           = resultSet.getString("org_name");
      SalesVolume       = resultSet.getDouble("sales_volume");
      CBTIncome         = resultSet.getDouble("cbt_inc");
      BankIncome        = resultSet.getDouble("bank_inc");
      SortOrder         = resultSet.getInt("sort_order");
    
      MerchantCountsMTD = new int[MC_COUNT];
      for( int i = 0; i < MC_COUNT; ++i )
      {
        MerchantCountsMTD[i] = resultSet.getInt( CountFields[i] );
      }
    }
    
    public double getBankIncome( )
    {
      double        retVal    = 0.0;
      
      if ( IncomeBasis == INC_BASIS_STD )
      {
        retVal = BankIncome;
      }
      return( retVal );
    }
    
    public double getBankIncomeYTD( )
    {
      double        retVal    = 0.0;
      
      if ( IncomeBasis == INC_BASIS_STD )
      {
        retVal = BankIncomeYTD;
      }
      return( retVal );
    }
    
    public String getBankIncomeString( )
    {
      String              retVal    = "N/A";
      
      if ( IncomeBasis == INC_BASIS_STD )
      {
        retVal = MesMath.toCurrency( BankIncome );
      }
      return( retVal );
    }
    
    public String getBankIncomeYTDString( )
    {
      String              retVal    = "N/A";
      
      if ( IncomeBasis == INC_BASIS_STD )
      {
        retVal = MesMath.toCurrency( BankIncomeYTD );
      }
      return( retVal );
    }
    
    public void setYTDTotals( ResultSet resultSet )
    {
      try
      {
        SalesVolumeYTD  = resultSet.getDouble("sales_volume");
        CBTIncomeYTD    = resultSet.getDouble("cbt_inc");
        BankIncomeYTD   = resultSet.getDouble("bank_inc");

        //
        // load the year to date merchant counts.  note that
        // this is one element shorter because active merchants
        // does not have a ytd value.        
        //
        MerchantCountsYTD = new int[MC_COUNT-1];
        for( int i = 0; i < MerchantCountsYTD.length; ++i )
        {
          MerchantCountsYTD[i] = resultSet.getInt( CountFields[i] );
        }
      }
      catch( java.sql.SQLException e )
      {
        logEntry( "AffiliateRebateData.setYTDTotals()", e.toString() );
      }        
    }
  }

  private int                 BankId        = 0;
  
  public CBTRebateDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    line.append("\"Opened Acct\",");
    line.append("\"Opened Acct YTD\",");
    line.append("\"Closed Acct\",");
    line.append("\"Closed Acct YTD\",");
    line.append("\"Active Acct\",");
    line.append("\"Sales Volume\",");
    line.append("\"Sales Volume YTD\",");
    line.append("\"Income To Bank\",");
    line.append("\"Income To Bank YTD\"");
    if ( ( ReportType == RT_AFFILIATE_REBATE_MANAGEMENT ) ||
         ( ReportType == RT_AGENT_REBATE_MANAGEMENT ) )
    {
      line.append(",\"Income To CBT\",");
      line.append("\"Income To CBT YTD\"");
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    AffiliateRebateData record = (AffiliateRebateData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.HierarchyNode );
    line.append( ",\"" );
    line.append( record.OrgName );
    line.append( "\"," );
    line.append( record.MerchantCountsMTD[MC_OPENED] );
    line.append( "," );
    line.append( record.MerchantCountsYTD[MC_OPENED] );
    line.append( "," );
    line.append( record.MerchantCountsMTD[MC_CLOSED] );
    line.append( "," );
    line.append( record.MerchantCountsYTD[MC_CLOSED] );
    line.append( "," );
    line.append( record.MerchantCountsMTD[MC_ACTIVE] );
    line.append( "," );
    line.append( record.SalesVolume );
    line.append( "," );
    line.append( record.SalesVolumeYTD );
    line.append( "," );
    line.append( record.BankIncome );
    line.append( "," );
    line.append( record.BankIncomeYTD );
    if ( ( ReportType == RT_AFFILIATE_REBATE_MANAGEMENT ) ||
         ( ReportType == RT_AGENT_REBATE_MANAGEMENT ) )
    {
      line.append(",");
      line.append( record.CBTIncome );
      line.append(",");
      line.append( record.CBTIncomeYTD );
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMMyyyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    
    switch( ReportType )
    {
      case RT_AFFILIATE_REBATE:
        filename.append("_affiliate_rebate_report_");
        break;
      case RT_AGENT_REBATE:
        filename.append("_agent_rebate_report_");
        break;
      case RT_AFFILIATE_REBATE_MANAGEMENT:
        filename.append("_affiliate_rebate_mgmt_report_");
        break;
      case RT_AGENT_REBATE_MANAGEMENT:
        filename.append("_agent_rebate_mgmt_report_");
        break;
    }
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( isSameMonthYear( ReportDateBegin, ReportDateEnd ) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  public String getReportTitle()
  {
    String        retVal      = "Invalid";
    
    switch( ReportType )
    {
      case RT_AFFILIATE_REBATE:
        retVal = "CB&T All Affiliate Bank Rebate Report";
        break;
      case RT_AGENT_REBATE:
        retVal = "CB&T All Agent Bank Rebate Report";
        break;
      case RT_AFFILIATE_REBATE_MANAGEMENT:
        retVal = "CB&T Management Affiliate Bank Rebate Report";
        break;
      case RT_AGENT_REBATE_MANAGEMENT:
        retVal = "CB&T Management Agent Bank Rebate Report";
        break;
    }
    return( retVal );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    AffiliateRebateData           rebateData        = null;
    int                           relationship      = 0;
    ResultSet                     resultSet         = null;

    switch( ReportType )
    {
      case RT_AGENT_REBATE:
      case RT_AGENT_REBATE_MANAGEMENT:
        relationship = AGENT_BANK;
        break;
        
      default:
        relationship = AFFILIATE_BANK;
        break;
    }    
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:342^7*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                    INDEX (o IDX_ORGANIZATION_ORG_GROUP)
//                    index (cr IDX_CBT_REBATE_ASSOC_DATE)
//                 */
//                 o.org_num                                as ORG_NUM,
//                 o.org_group                              as hierarchy_node,
//                 o.org_name                               as org_name,
//                 br.income_basis                          as income_basis,
//                 decode( :relationship,:AGENT_BANK,0,
//                         decode(substr(o.org_group,5,3),
//                         '904',0,   -- CB&T Market (pilot banks & direct merch)
//                         '905',0,   -- CB&T Military Accounts
//                         1) )                             as sort_order,
//                 sum( decode(cr.ACCOUNT_OPENED,'Y',1,0) ) as opened_accounts,
//                 sum( decode(cr.ACCOUNT_CLOSED,'Y',1,0) ) as closed_accounts,
//                 sum( decode(cr.account_active,'Y',1,0) ) as active_accounts,
//                 sum( decode(nvl(br.include_cash_adv_sales,'N'),
//                             'Y', (cr.sales_vol_amount + cr.cash_adv_vol_amount),
//                             decode(cr.CASH_ADV_ACCOUNT,'Y',0,
//                                    cr.SALES_VOL_AMOUNT) 
//                             ) )                          as sales_volume,
//                 sum( cr.INCOME_TO_BANK )                 as bank_inc,
//                 sum( cr.income_to_cbt )                  as cbt_inc
//          from   bank_relationships       br,
//                 organization             o,
//                 group_assoc              ga,
//                 cbt_rebate               cr                              
//          where  br.bank_number       = :BankId and
//                 br.relationship      = :relationship and
//                 o.org_group          = br.group_number and
//                 ga.group_number      = o.org_group and
//                 cr.assoc_number      = ga.assoc_number and
//                 cr.active_date       = :ReportDateBegin
//          group by o.org_num, o.org_group, br.income_basis, o.org_name
//          order by sort_order, o.org_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                  INDEX (o IDX_ORGANIZATION_ORG_GROUP)\n                  index (cr IDX_CBT_REBATE_ASSOC_DATE)\n               */\n               o.org_num                                as ORG_NUM,\n               o.org_group                              as hierarchy_node,\n               o.org_name                               as org_name,\n               br.income_basis                          as income_basis,\n               decode(  :1 , :2 ,0,\n                       decode(substr(o.org_group,5,3),\n                       '904',0,   -- CB&T Market (pilot banks & direct merch)\n                       '905',0,   -- CB&T Military Accounts\n                       1) )                             as sort_order,\n               sum( decode(cr.ACCOUNT_OPENED,'Y',1,0) ) as opened_accounts,\n               sum( decode(cr.ACCOUNT_CLOSED,'Y',1,0) ) as closed_accounts,\n               sum( decode(cr.account_active,'Y',1,0) ) as active_accounts,\n               sum( decode(nvl(br.include_cash_adv_sales,'N'),\n                           'Y', (cr.sales_vol_amount + cr.cash_adv_vol_amount),\n                           decode(cr.CASH_ADV_ACCOUNT,'Y',0,\n                                  cr.SALES_VOL_AMOUNT) \n                           ) )                          as sales_volume,\n               sum( cr.INCOME_TO_BANK )                 as bank_inc,\n               sum( cr.income_to_cbt )                  as cbt_inc\n        from   bank_relationships       br,\n               organization             o,\n               group_assoc              ga,\n               cbt_rebate               cr                              \n        where  br.bank_number       =  :3  and\n               br.relationship      =  :4  and\n               o.org_group          = br.group_number and\n               ga.group_number      = o.org_group and\n               cr.assoc_number      = ga.assoc_number and\n               cr.active_date       =  :5 \n        group by o.org_num, o.org_group, br.income_basis, o.org_name\n        order by sort_order, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.CBTRebateDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,relationship);
   __sJT_st.setInt(2,AGENT_BANK);
   __sJT_st.setInt(3,BankId);
   __sJT_st.setInt(4,relationship);
   __sJT_st.setDate(5,ReportDateBegin);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.CBTRebateDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:379^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        // create a new rebate object to hold this row.
        rebateData = new AffiliateRebateData( resultSet );
        
        // load the year to date data for this row.
        loadYTDTotals( rebateData );
        
        // add this record to the vector
        ReportRows.add(rebateData);
      }
      resultSet.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadYTDTotals( AffiliateRebateData rebateData )
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:411^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                    INDEX (cr idx_cbt_rebate_assoc_date)  
//                 */
//                  sum( decode(cr.ACCOUNT_OPENED,'Y',1,0) ) as opened_accounts,
//                  sum( decode(cr.ACCOUNT_CLOSED,'Y',1,0) ) as closed_accounts,
//                  sum( decode(nvl(br.include_cash_adv_sales,'N'),
//                              'Y', (cr.sales_vol_amount + cr.cash_adv_vol_amount),
//                              decode(cr.CASH_ADV_ACCOUNT,'Y',0,
//                                     cr.SALES_VOL_AMOUNT) 
//                              ) )                          as sales_volume,
//                  sum( cr.INCOME_TO_BANK )                 as bank_inc,
//                  sum( cr.income_to_cbt )                  as cbt_inc                
//          from    group_assoc         ga,
//                  bank_relationships  br,
//                  cbt_rebate          cr
//          where   ga.group_number = :rebateData.HierarchyNode and
//                  br.group_number(+) = ga.group_number and
//                  cr.assoc_number   = ga.assoc_number and
//                  cr.active_date between trunc(:ReportDateBegin,'year') and :ReportDateBegin
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                  INDEX (cr idx_cbt_rebate_assoc_date)  \n               */\n                sum( decode(cr.ACCOUNT_OPENED,'Y',1,0) ) as opened_accounts,\n                sum( decode(cr.ACCOUNT_CLOSED,'Y',1,0) ) as closed_accounts,\n                sum( decode(nvl(br.include_cash_adv_sales,'N'),\n                            'Y', (cr.sales_vol_amount + cr.cash_adv_vol_amount),\n                            decode(cr.CASH_ADV_ACCOUNT,'Y',0,\n                                   cr.SALES_VOL_AMOUNT) \n                            ) )                          as sales_volume,\n                sum( cr.INCOME_TO_BANK )                 as bank_inc,\n                sum( cr.income_to_cbt )                  as cbt_inc                \n        from    group_assoc         ga,\n                bank_relationships  br,\n                cbt_rebate          cr\n        where   ga.group_number =  :1  and\n                br.group_number(+) = ga.group_number and\n                cr.assoc_number   = ga.assoc_number and\n                cr.active_date between trunc( :2 ,'year') and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.CBTRebateDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,rebateData.HierarchyNode);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateBegin);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.CBTRebateDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:432^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        rebateData.setYTDTotals(resultSet);
      }        
      resultSet.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadYTDCounts()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    BankId      = HttpHelper.getInt(request,"bankId",mesConstants.BANK_ID_CBT);
    
    // load ReportType again to allow this to set the default.
    ReportType  = HttpHelper.getInt(request,"reportType",RT_AFFILIATE_REBATE);
    
    // if the from date is today, then set the default to rolling previous week
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range
      cal.setTime( ReportDateBegin );
      
      if ( cal.get( Calendar.MONTH ) == Calendar.JANUARY )
      {
        cal.set( Calendar.MONTH, Calendar.DECEMBER );
        cal.set( Calendar.YEAR,  ( cal.get( Calendar.YEAR ) - 1 ) );
      }
      else
      {
        cal.set( Calendar.MONTH, ( cal.get( Calendar.MONTH ) - 1 ) );
      }
      
      // since this report is for one month at a time,
      // make sure that the from date always starts with
      // the first day of the specified month.
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      
      // set both the begin and end dates
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd  ( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/