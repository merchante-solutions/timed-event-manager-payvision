/*@lineinfo:filename=ProfVitalDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ProfVitalDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 5/28/04 4:45p $
  Version            : $Revision: 12 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

class ProfVitalDataBean extends ProfProcDataBean
{
  public ProfVitalDataBean(  )
  {
  }
  
  protected void loadDetailAuth( long merchantId )
  {
    Date                          activeDate            = null;
    double                        amount                = 0.0;
    int                           betType;
    StringBuffer                  desc                  = new StringBuffer("");
    ResultSetIterator             it                    = null;
    double                        perItem               = 0.0;
    Date                          perItemDate           = null;
    ResultSetIterator             perItemIt             = null;
    ResultSet                     perItemResultSet      = null;
    ContractTypes.PerItemRecord   record                = null;
    ResultSet                     resultSet             = null;
    int                           tranCount             = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:59^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                                  as active_date,
//                  ap.a1_plan_type                                 as plan_type,
//                  ap.auth_count_total                             as count,
//                  ap.auth_income_total                            as income,
//                 ( ap.auth_income_total / ap.auth_count_total )   as per_item,
//                  nvl(ap.a1_avs_number,0)                         as avs_count,
//                  nvl(ap.a1_avs_income,0)                         as avs_income,
//                  decode( nvl(ap.a1_avs_number,0),
//                          0, 0,
//                          (ap.a1_avs_income/ap.a1_avs_number) )   as avs_per_item
//          from    monthly_extract_summary   sm,
//                  monthly_extract_ap        ap  
//          where   sm.merchant_number = :merchantId and
//                  sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                  ap.HH_LOAD_SEC = sm.hh_load_sec and
//                  ap.auth_count_total != 0
//          order by ap.a1_plan_type, sm.active_date      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                                  as active_date,\n                ap.a1_plan_type                                 as plan_type,\n                ap.auth_count_total                             as count,\n                ap.auth_income_total                            as income,\n               ( ap.auth_income_total / ap.auth_count_total )   as per_item,\n                nvl(ap.a1_avs_number,0)                         as avs_count,\n                nvl(ap.a1_avs_income,0)                         as avs_income,\n                decode( nvl(ap.a1_avs_number,0),\n                        0, 0,\n                        (ap.a1_avs_income/ap.a1_avs_number) )   as avs_per_item\n        from    monthly_extract_summary   sm,\n                monthly_extract_ap        ap  \n        where   sm.merchant_number =  :1  and\n                sm.active_date between  :2  and last_day(  :3  ) and\n                ap.HH_LOAD_SEC = sm.hh_load_sec and\n                ap.auth_count_total != 0\n        order by ap.a1_plan_type, sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:78^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        betType = BankContractBean.getAuthBetType( resultSet.getString("PLAN_TYPE") );
        if ( betType != BankContractBean.BET_NONE )
        {
          record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, resultSet.getInt( "COUNT" ), resultSet.getDouble("PER_ITEM") );
          desc.setLength(0);
          desc.append( mesConstants.getVitalExtractCardTypeDesc( resultSet.getString("PLAN_TYPE" ) ) );
          desc.append( " Authorizations - " );
          desc.append( DateTimeFormatter.getFormattedDate( activeDate,"MMM yyyy" ) );
          record.setDescription( desc.toString() );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_INCOME, record );
        }
        
        if ( resultSet.getDouble("avs_income") > 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate, 
                                                    ContractTypes.CONTRACT_SOURCE_VITAL, 
                                                    BankContractBean.BET_AVS_AUTHORIZATION_FEE,
                                                    resultSet.getInt("avs_count"), 
                                                    resultSet.getDouble("avs_per_item") );
          desc.setLength(0);
          desc.append( mesConstants.getVitalExtractCardTypeDesc( resultSet.getString("PLAN_TYPE" ) ) );
          desc.append( " AVS - " );
          desc.append( DateTimeFormatter.getFormattedDate( activeDate,"MMM yyyy" ) );
          record.setDescription( desc.toString() );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_INCOME, record );
        }
      }
      it.close();
      
      if ( ContractDataBean.includeVitalExpense() == true )
      {
        // setup a generic BET type for the any summary level items.
        betType = BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_AUTH_FEES );
        
        // since we only have expense as a total, do a query
        // that gives the per item amount for each month in 
        // the date range.
        /*@lineinfo:generated-code*//*@lineinfo:123^9*/

//  ************************************************************
//  #sql [Ctx] perItemIt = { select   sm.active_date                       as active_date,
//                     ( sm.TOT_EXP_AUTHORIZATION /
//                       sum( ap.auth_count_total  ) )      as per_item
//            from     monthly_extract_summary    sm,
//                     monthly_extract_ap         ap
//            where    sm.merchant_number = :merchantId and
//                     sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                     sm.TOT_EXP_AUTHORIZATION != 0 and
//                     ap.hh_load_sec = sm.hh_load_sec
//            group by sm.active_date, sm.tot_exp_authorization
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   sm.active_date                       as active_date,\n                   ( sm.TOT_EXP_AUTHORIZATION /\n                     sum( ap.auth_count_total  ) )      as per_item\n          from     monthly_extract_summary    sm,\n                   monthly_extract_ap         ap\n          where    sm.merchant_number =  :1  and\n                   sm.active_date between  :2  and last_day(  :3  ) and\n                   sm.TOT_EXP_AUTHORIZATION != 0 and\n                   ap.hh_load_sec = sm.hh_load_sec\n          group by sm.active_date, sm.tot_exp_authorization";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   perItemIt = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:135^9*/
        perItemResultSet = perItemIt.getResultSet();
      
        // now do a second query that gives the counts by card type
        /*@lineinfo:generated-code*//*@lineinfo:139^9*/

//  ************************************************************
//  #sql [Ctx] it = { select   sm.active_date                     as active_date,
//                     ap.a1_plan_type                    as plan_type,
//                     sum( ap.auth_count_total )         as tran_count
//            from     monthly_extract_summary  sm,
//                     monthly_extract_ap       ap
//            where    sm.merchant_number = :merchantId and
//                     sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                     sm.TOT_EXP_AUTHORIZATION != 0 and
//                     ap.hh_load_sec = sm.hh_load_sec
//            group by ap.a1_plan_type, sm.active_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   sm.active_date                     as active_date,\n                   ap.a1_plan_type                    as plan_type,\n                   sum( ap.auth_count_total )         as tran_count\n          from     monthly_extract_summary  sm,\n                   monthly_extract_ap       ap\n          where    sm.merchant_number =  :1  and\n                   sm.active_date between  :2  and last_day(  :3  ) and\n                   sm.TOT_EXP_AUTHORIZATION != 0 and\n                   ap.hh_load_sec = sm.hh_load_sec\n          group by ap.a1_plan_type, sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:151^9*/
        resultSet = it.getResultSet();
      
        // there are not details available for individual plan expenses.
        // just add a single entry for the sum of the amount assessed
        // by Vital over the given date range.
        while( resultSet.next() )
        {
          activeDate   = resultSet.getDate( "ACTIVE_DATE" );
          
          // if the perItemDate is not initialized, or
          // the auth date does not match it, then scan
          // for the per item amount for the auth plan
          // month.  since both of these result sets
          // are sorted there should be a 1 to 1 mapping.
          if ( ( perItemDate == null ) ||
               ( activeDate.equals(perItemDate) == false ) )
          {
            while( perItemResultSet.next() )
            {
              perItemDate = perItemResultSet.getDate("ACTIVE_DATE");
              if ( activeDate.equals(perItemDate) )
              {
                perItem = perItemResultSet.getDouble("PER_ITEM");
                break;
              }
            }
          }
          
          tranCount  = resultSet.getInt("TRAN_COUNT");
        
          if ( perItem != 0.0 && tranCount != 0 )
          {
            record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, tranCount, perItem );
            desc.setLength(0);
            desc.append( mesConstants.getVitalExtractCardTypeDesc( resultSet.getString("PLAN_TYPE" ) ) );
            desc.append( " Authorizations - " );
            desc.append( DateTimeFormatter.getFormattedDate( activeDate, "MMM yyyy" ) );
            record.setDescription( desc.toString() );
            ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                ContractTypes.CONTRACT_TYPE_EXPENSE, record );
          }                              
        }
        it.close();
        perItemIt.close();
      } 
    }
    catch( java.sql.SQLException e )
    {
      ContractDataBean.logEntry("loadDetailAuth(): ", e.toString());
      ContractDataBean.addError("loadDetailAuth: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      try{ perItemIt.close(); } catch( Exception e ) {}
    }
  }
  
  protected void loadDetailCapture( long merchantId )
  {
    Date                            activeDate            = null;
    double                          amount                = 0.0;
    int                             betType;
    double                          captureExpense        = 0.0;
    ResultSetIterator               it                    = null;
    ContractTypes.PerItemRecord     record                = null;
    ResultSet                       resultSet             = null;
  
    try
    {
      betType = BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_CAPTURE_FEES );
    
      /*@lineinfo:generated-code*//*@lineinfo:224^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   c1.c1_inc_paper_items_captured  inc_paper_items_captured,
//                   c1.c1_inc_paper_stored          inc_paper_stored,
//                   c1.c1_inc_edc_stored            inc_edc_stored,
//                   c1.c1_inc_paper_batch_headers   inc_paper_batch_headers,
//                   c1.c1_inc_edc_transmittals      inc_edc_transmittals,
//                   c1.c1_inc_edc_items_captured    inc_edc_items_captured
//          from     monthly_extract_summary  sm,
//                   monthly_extract_c1       c1
//          where    sm.merchant_number = :merchantId and
//                   sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                   c1.hh_load_sec = sm.hh_load_sec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   c1.c1_inc_paper_items_captured  inc_paper_items_captured,\n                 c1.c1_inc_paper_stored          inc_paper_stored,\n                 c1.c1_inc_edc_stored            inc_edc_stored,\n                 c1.c1_inc_paper_batch_headers   inc_paper_batch_headers,\n                 c1.c1_inc_edc_transmittals      inc_edc_transmittals,\n                 c1.c1_inc_edc_items_captured    inc_edc_items_captured\n        from     monthly_extract_summary  sm,\n                 monthly_extract_c1       c1\n        where    sm.merchant_number =  :1  and\n                 sm.active_date between  :2  and last_day(  :3  ) and\n                 c1.hh_load_sec = sm.hh_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:237^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        
        if ( ( amount = resultSet.getDouble( "inc_paper_items_captured" ) ) != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, amount );
          record.setDescription( "Paper Items Captured Income " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_INCOME, record );
        }
        if ( ( amount = resultSet.getDouble( "inc_paper_stored" ) ) != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, amount );
          record.setDescription( "Paper Items Stored Income " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_INCOME, record );
        }
        if ( ( amount = resultSet.getDouble( "inc_edc_stored" ) ) != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, amount );
          record.setDescription( "EDC Stored Income " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy")  );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_INCOME, record );
        }
        if ( ( amount = resultSet.getDouble( "inc_paper_batch_headers" ) ) != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, amount );
          record.setDescription( "Paper Batch Headers Income " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy")  );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_INCOME, record );
        }
        if ( ( amount = resultSet.getDouble( "inc_edc_transmittals" ) ) != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, amount );
          record.setDescription( "EDC Transmittals Income " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy")  );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_INCOME, record );
        }
        if ( ( amount = resultSet.getDouble( "inc_edc_items_captured" ) ) != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, amount );
          record.setDescription( "EDC Items Captured Income " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy")  );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_INCOME, record );
        }
      }
      resultSet.close();
      it.close();
      
      if ( ContractDataBean.includeVitalExpense() == true )
      {
        /*@lineinfo:generated-code*//*@lineinfo:292^9*/

//  ************************************************************
//  #sql [Ctx] it = { select   sm.active_date,
//                     sm.TOT_EXP_CAPTURE
//            from     monthly_extract_summary    sm
//            where    sm.merchant_number = :merchantId and
//                     sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   sm.active_date,\n                   sm.TOT_EXP_CAPTURE\n          from     monthly_extract_summary    sm\n          where    sm.merchant_number =  :1  and\n                   sm.active_date between  :2  and last_day(  :3  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:299^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          activeDate = resultSet.getDate("active_date");
          captureExpense = resultSet.getDouble("tot_exp_capture");
          
          if ( captureExpense != 0.0 )
          {
            record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, captureExpense );
            record.setDescription( "TSYS Capture Expense " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
            ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                ContractTypes.CONTRACT_TYPE_EXPENSE, record );
          }
        }
      }
    }
    catch( java.sql.SQLException e )
    {
      ContractDataBean.logEntry("loadDetailCapture(): ", e.toString());
      ContractDataBean.addError("loadDetailCapture: " + e.toString());
    }                 
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  protected void loadDetailDCE( long merchantId )
  {
    Date                          activeDate    = null;
    StringBuffer                  desc          = new StringBuffer("");
    ResultSetIterator             it            = null;
    ContractTypes.PerItemRecord   record        = null;
    ResultSet                     resultSet     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:338^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                            as active_date,
//                  ( adj.adjustment_amount *
//                   decode(adj.debit_credit_ind,'C',-1,1) )  as adj_amount,
//                 adj.batch_date                             as batch_date,
//                 gl.gl_desc                                 as adj_desc,
//                 decode( gl.revenue_offset,
//                         'D',:BankContractBean.BET_DISC_IC_DCE_ADJUSTMENTS,
//                         :BankContractBean.BET_FEE_DCE_ADJUSTMENTS) as bet_type
//          from   mif                            mf,
//                 monthly_extract_summary        sm,
//                 daily_detail_file_adjustment   adj,
//                 bank_dce_ref_to_gl             gl
//          where  mf.merchant_number = :merchantId and
//                 sm.merchant_number = mf.merchant_Number and
//                 sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                 adj.merchant_account_number = sm.merchant_number and
//                 adj.batch_date between sm.month_begin_date and sm.month_end_date and
//               ( adj.transacction_code in (9072,9078) or -- always take 9072 and 9078
//                 (adj.transacction_code = 9071 and       -- only take 9071 if the acct
//                  mf.sic_code in ( 6010,6011 )) )  and  -- is cash advance (6010,6011)
//                 ( gl.bank_number = mf.bank_number or
//                   gl.bank_number is null ) and
//                 gl.DCE_REF_NUM = substr(adj.reference_number,length(adj.reference_number)-1,2) and
//                 not gl.revenue_offset is null
//          order by batch_date, adj_desc               
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                            as active_date,\n                ( adj.adjustment_amount *\n                 decode(adj.debit_credit_ind,'C',-1,1) )  as adj_amount,\n               adj.batch_date                             as batch_date,\n               gl.gl_desc                                 as adj_desc,\n               decode( gl.revenue_offset,\n                       'D', :1 ,\n                        :2 ) as bet_type\n        from   mif                            mf,\n               monthly_extract_summary        sm,\n               daily_detail_file_adjustment   adj,\n               bank_dce_ref_to_gl             gl\n        where  mf.merchant_number =  :3  and\n               sm.merchant_number = mf.merchant_Number and\n               sm.active_date between  :4  and  :5  and\n               adj.merchant_account_number = sm.merchant_number and\n               adj.batch_date between sm.month_begin_date and sm.month_end_date and\n             ( adj.transacction_code in (9072,9078) or -- always take 9072 and 9078\n               (adj.transacction_code = 9071 and       -- only take 9071 if the acct\n                mf.sic_code in ( 6010,6011 )) )  and  -- is cash advance (6010,6011)\n               ( gl.bank_number = mf.bank_number or\n                 gl.bank_number is null ) and\n               gl.DCE_REF_NUM = substr(adj.reference_number,length(adj.reference_number)-1,2) and\n               not gl.revenue_offset is null\n        order by batch_date, adj_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,BankContractBean.BET_DISC_IC_DCE_ADJUSTMENTS);
   __sJT_st.setInt(2,BankContractBean.BET_FEE_DCE_ADJUSTMENTS);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setDate(4,ReportDateBegin);
   __sJT_st.setDate(5,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:365^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        
        record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, resultSet.getInt("bet_type"), 1, resultSet.getDouble("adj_amount") );
    
        desc.setLength(0);
        desc.append( resultSet.getString("adj_desc") );
        desc.append( " on " );
        desc.append( DateTimeFormatter.getFormattedDate( resultSet.getDate("batch_date"), "MM/dd/yyyy" ) );
        
        record.setDescription( desc.toString() );
    
        // all DCE offset the direct merchant income
        ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                             ContractTypes.CONTRACT_TYPE_INCOME, record );
      }
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:387^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                            as active_date,
//                  ( adj.amount *
//                   decode(adj.credit_debit_ind,'C',-1,1) )  as adj_amount,
//                 trunc(adj.date_transmitted)                as batch_date,
//                 bad.long_description                       as adj_desc,      
//                 decode( bad.revenue_offset,
//                         'D',:BankContractBean.BET_DISC_IC_DCE_ADJUSTMENTS,
//                         :BankContractBean.BET_FEE_DCE_ADJUSTMENTS) as bet_type
//          from   mif                            mf,
//                 monthly_extract_summary        sm,
//                 bankserv_ach_detail            adj,
//                 bankserv_ach_descriptions      bad
//          where  mf.merchant_number = :merchantId and
//                 sm.merchant_number = mf.merchant_Number and
//                 sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                 adj.merchant_number = sm.merchant_number and
//                 trunc(adj.date_transmitted) between sm.month_begin_date and sm.month_end_date and
//                 bad.description = adj.entry_description and
//                 not bad.revenue_offset is null
//          order by batch_date, adj_desc               
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                            as active_date,\n                ( adj.amount *\n                 decode(adj.credit_debit_ind,'C',-1,1) )  as adj_amount,\n               trunc(adj.date_transmitted)                as batch_date,\n               bad.long_description                       as adj_desc,      \n               decode( bad.revenue_offset,\n                       'D', :1 ,\n                        :2 ) as bet_type\n        from   mif                            mf,\n               monthly_extract_summary        sm,\n               bankserv_ach_detail            adj,\n               bankserv_ach_descriptions      bad\n        where  mf.merchant_number =  :3  and\n               sm.merchant_number = mf.merchant_Number and\n               sm.active_date between  :4  and  :5  and\n               adj.merchant_number = sm.merchant_number and\n               trunc(adj.date_transmitted) between sm.month_begin_date and sm.month_end_date and\n               bad.description = adj.entry_description and\n               not bad.revenue_offset is null\n        order by batch_date, adj_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,BankContractBean.BET_DISC_IC_DCE_ADJUSTMENTS);
   __sJT_st.setInt(2,BankContractBean.BET_FEE_DCE_ADJUSTMENTS);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setDate(4,ReportDateBegin);
   __sJT_st.setDate(5,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:409^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        
        record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, resultSet.getInt("bet_type"), 1, resultSet.getDouble("adj_amount") );
    
        desc.setLength(0);
        desc.append( resultSet.getString("adj_desc") );
        desc.append( " on " );
        desc.append( DateTimeFormatter.getFormattedDate( resultSet.getDate("batch_date"), "MM/dd/yyyy" ) );
        
        record.setDescription( desc.toString() );
    
        // all DCE offset the direct merchant income
        ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                             ContractTypes.CONTRACT_TYPE_INCOME, record );
      }
      it.close();
      
      // load the revenue adjusting ACH rejects
      /*@lineinfo:generated-code*//*@lineinfo:432^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date          as active_date,
//                  ar.amount               as adj_amount,
//                  ar.report_date          as batch_date,
//                  nvl(arc.description,
//                      'Not Available')    as adj_desc
//          from    monthly_extract_summary   sm,
//                  ach_rejects               ar,
//                  ach_reject_status         ars,
//                  ach_reject_reason_codes   arc
//          where   sm.merchant_number = :merchantId and
//                  sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                  ar.merchant_number = sm.merchant_number and
//                  ar.report_date between sm.month_begin_date and sm.month_end_date and
//                  substr(ar.reason_code,1,1) != 'C' and
//                  ars.reject_seq_num = ar.reject_seq_num and                
//                  ars.reject_status = 'REVERSAL_OF_REVENUE' and
//                  arc.reason_code(+) = ar.reason_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date          as active_date,\n                ar.amount               as adj_amount,\n                ar.report_date          as batch_date,\n                nvl(arc.description,\n                    'Not Available')    as adj_desc\n        from    monthly_extract_summary   sm,\n                ach_rejects               ar,\n                ach_reject_status         ars,\n                ach_reject_reason_codes   arc\n        where   sm.merchant_number =  :1  and\n                sm.active_date between  :2  and  :3  and\n                ar.merchant_number = sm.merchant_number and\n                ar.report_date between sm.month_begin_date and sm.month_end_date and\n                substr(ar.reason_code,1,1) != 'C' and\n                ars.reject_seq_num = ar.reject_seq_num and                \n                ars.reject_status = 'REVERSAL_OF_REVENUE' and\n                arc.reason_code(+) = ar.reason_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:451^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        
        record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, BankContractBean.BET_FEE_DCE_ADJUSTMENTS, 1, resultSet.getDouble("adj_amount") );
    
        desc.setLength(0);
        desc.append( resultSet.getString("adj_desc") );
        desc.append( " on " );
        desc.append( DateTimeFormatter.getFormattedDate( resultSet.getDate("batch_date"), "MM/dd/yyyy" ) );
        
        record.setDescription( desc.toString() );
    
        // all DCE offset the direct merchant income
        ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                             ContractTypes.CONTRACT_TYPE_INCOME, record );
      }
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      ContractDataBean.logEntry("loadDetailDCE(): ", e.toString());
      ContractDataBean.addError("loadDetailDCE: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  protected void loadDetailDebit( long merchantId )
  {
    Date                          activeDate            = null;
    int                           betType;
    String                        catField;
    String                        catName               = "";
    int                           creditsCount          = 0;
    double                        creditsInc            = 0.0;
    StringBuffer                  desc                  = new StringBuffer("");
    ResultSetIterator             it                    = null;
    double                        perItem               = 0.0;
    ContractTypes.PerItemRecord   record                = null;
    ResultSet                     resultSet             = null;
    int                           salesCount            = 0;
    double                        salesInc              = 0.0;
    double                        vitalExpense          = 0.0;
    
    try
    {
      betType = BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_DEBIT_FEES );
    
      /*@lineinfo:generated-code*//*@lineinfo:505^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    sm.active_date                                as ACTIVE_DATE,
//                  nvl( dn.cat_01_sales_count, 0 )               as cat_01_sales_count,
//                  nvl( dn.cat_01_credits_count, 0 )             as cat_01_credits_count,
//                  nvl( dn.cat_02_sales_count, 0 )               as cat_02_sales_count,
//                  nvl( dn.cat_02_credits_count, 0 )             as cat_02_credits_count,
//                  nvl( dn.cat_03_sales_count, 0 )               as cat_03_sales_count,
//                  nvl( dn.cat_03_credits_count, 0 )             as cat_03_credits_count,
//                  nvl( dn.cat_04_sales_count, 0 )               as cat_04_sales_count,
//                  nvl( dn.cat_04_credits_count, 0 )             as cat_04_credits_count,
//                  nvl( dn.cat_05_sales_count, 0 )               as cat_05_sales_count,
//                  nvl( dn.cat_05_credits_count, 0 )             as cat_05_credits_count,
//                  nvl( dn.cat_06_sales_count, 0 )               as cat_06_sales_count,
//                  nvl( dn.cat_06_credits_count, 0 )             as cat_06_credits_count,
//                  nvl( dn.cat_07_sales_count, 0 )               as cat_07_sales_count,
//                  nvl( dn.cat_07_credits_count, 0 )             as cat_07_credits_count,
//                  nvl( dn.cat_08_sales_count, 0 )               as cat_08_sales_count,
//                  nvl( dn.cat_08_credits_count, 0 )             as cat_08_credits_count,
//                  nvl( dn.cat_09_sales_count, 0 )               as cat_09_sales_count,
//                  nvl( dn.cat_09_credits_count, 0 )             as cat_09_credits_count,
//                  nvl( dn.cat_10_sales_count, 0 )               as cat_10_sales_count,
//                  nvl( dn.cat_10_credits_count, 0 )             as cat_10_credits_count,
//                  dn.N1_INC_CAT_01_SALES                        as cat_01_sales_inc,
//                  dn.N1_INC_CAT_01_CREDIT                       as cat_01_credits_inc,
//                  dn.N1_INC_CAT_02_SALES                        as cat_02_sales_inc,
//                  dn.N1_INC_CAT_02_CREDIT                       as cat_02_credits_inc,
//                  dn.N1_INC_CAT_03_SALES                        as cat_03_sales_inc,
//                  dn.N1_INC_CAT_03_CREDIT                       as cat_03_credits_inc,
//                  dn.N1_INC_CAT_04_SALES                        as cat_04_sales_inc,
//                  dn.N1_INC_CAT_04_CREDIT                       as cat_04_credits_inc,
//                  dn.N1_INC_CAT_05_SALES                        as cat_05_sales_inc,
//                  dn.N1_INC_CAT_05_CREDIT                       as cat_05_credits_inc,
//                  dn.N1_INC_CAT_06_SALES                        as cat_06_sales_inc,
//                  dn.N1_INC_CAT_06_CREDIT                       as cat_06_credits_inc,
//                  dn.N1_INC_CAT_07_SALES                        as cat_07_sales_inc,
//                  dn.N1_INC_CAT_07_CREDIT                       as cat_07_credits_inc,
//                  dn.N1_INC_CAT_08_SALES                        as cat_08_sales_inc,
//                  dn.N1_INC_CAT_08_CREDIT                       as cat_08_credits_inc,
//                  dn.N1_INC_CAT_09_SALES                        as cat_09_sales_inc,
//                  dn.N1_INC_CAT_09_CREDIT                       as cat_09_credits_inc,
//                  dn.N1_INC_CAT_10_SALES                        as cat_10_sales_inc,
//                  dn.N1_INC_CAT_10_CREDIT                       as cat_10_credits_inc
//          from    monthly_extract_summary   sm,
//                  monthly_extract_dn        dn
//          where   sm.merchant_number = :merchantId and
//                  sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                  dn.hh_load_sec = sm.hh_load_sec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    sm.active_date                                as ACTIVE_DATE,\n                nvl( dn.cat_01_sales_count, 0 )               as cat_01_sales_count,\n                nvl( dn.cat_01_credits_count, 0 )             as cat_01_credits_count,\n                nvl( dn.cat_02_sales_count, 0 )               as cat_02_sales_count,\n                nvl( dn.cat_02_credits_count, 0 )             as cat_02_credits_count,\n                nvl( dn.cat_03_sales_count, 0 )               as cat_03_sales_count,\n                nvl( dn.cat_03_credits_count, 0 )             as cat_03_credits_count,\n                nvl( dn.cat_04_sales_count, 0 )               as cat_04_sales_count,\n                nvl( dn.cat_04_credits_count, 0 )             as cat_04_credits_count,\n                nvl( dn.cat_05_sales_count, 0 )               as cat_05_sales_count,\n                nvl( dn.cat_05_credits_count, 0 )             as cat_05_credits_count,\n                nvl( dn.cat_06_sales_count, 0 )               as cat_06_sales_count,\n                nvl( dn.cat_06_credits_count, 0 )             as cat_06_credits_count,\n                nvl( dn.cat_07_sales_count, 0 )               as cat_07_sales_count,\n                nvl( dn.cat_07_credits_count, 0 )             as cat_07_credits_count,\n                nvl( dn.cat_08_sales_count, 0 )               as cat_08_sales_count,\n                nvl( dn.cat_08_credits_count, 0 )             as cat_08_credits_count,\n                nvl( dn.cat_09_sales_count, 0 )               as cat_09_sales_count,\n                nvl( dn.cat_09_credits_count, 0 )             as cat_09_credits_count,\n                nvl( dn.cat_10_sales_count, 0 )               as cat_10_sales_count,\n                nvl( dn.cat_10_credits_count, 0 )             as cat_10_credits_count,\n                dn.N1_INC_CAT_01_SALES                        as cat_01_sales_inc,\n                dn.N1_INC_CAT_01_CREDIT                       as cat_01_credits_inc,\n                dn.N1_INC_CAT_02_SALES                        as cat_02_sales_inc,\n                dn.N1_INC_CAT_02_CREDIT                       as cat_02_credits_inc,\n                dn.N1_INC_CAT_03_SALES                        as cat_03_sales_inc,\n                dn.N1_INC_CAT_03_CREDIT                       as cat_03_credits_inc,\n                dn.N1_INC_CAT_04_SALES                        as cat_04_sales_inc,\n                dn.N1_INC_CAT_04_CREDIT                       as cat_04_credits_inc,\n                dn.N1_INC_CAT_05_SALES                        as cat_05_sales_inc,\n                dn.N1_INC_CAT_05_CREDIT                       as cat_05_credits_inc,\n                dn.N1_INC_CAT_06_SALES                        as cat_06_sales_inc,\n                dn.N1_INC_CAT_06_CREDIT                       as cat_06_credits_inc,\n                dn.N1_INC_CAT_07_SALES                        as cat_07_sales_inc,\n                dn.N1_INC_CAT_07_CREDIT                       as cat_07_credits_inc,\n                dn.N1_INC_CAT_08_SALES                        as cat_08_sales_inc,\n                dn.N1_INC_CAT_08_CREDIT                       as cat_08_credits_inc,\n                dn.N1_INC_CAT_09_SALES                        as cat_09_sales_inc,\n                dn.N1_INC_CAT_09_CREDIT                       as cat_09_credits_inc,\n                dn.N1_INC_CAT_10_SALES                        as cat_10_sales_inc,\n                dn.N1_INC_CAT_10_CREDIT                       as cat_10_credits_inc\n        from    monthly_extract_summary   sm,\n                monthly_extract_dn        dn\n        where   sm.merchant_number =  :1  and\n                sm.active_date between  :2  and last_day(  :3  ) and\n                dn.hh_load_sec = sm.hh_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:553^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        
        for ( int cat = 0; cat < 10; ++cat )
        {
          catName   = loadIcCatDesc( mesConstants.VITAL_ME_AP_DEBIT, (cat+1) );
          catField  = "cat_" + NumberFormatter.getPaddedInt((cat+1),2);
          
          creditsCount  = resultSet.getInt    ( catField + "_credits_count" );
          creditsInc    = resultSet.getDouble ( catField + "_credits_inc" );
          salesCount    = resultSet.getInt    ( catField + "_sales_count" );
          salesInc      = resultSet.getDouble ( catField + "_sales_inc" );
          
          if ( salesInc != 0.0 )
          {
            if ( salesCount == 0 )
            {
              record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, salesInc );
            }
            else
            {
              /*@lineinfo:generated-code*//*@lineinfo:578^15*/

//  ************************************************************
//  #sql [Ctx] { select  ( :salesInc / :salesCount ) 
//                  from    dual
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  (  :1  /  :2  )  \n                from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.ProfVitalDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDouble(1,salesInc);
   __sJT_st.setInt(2,salesCount);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   perItem = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:582^15*/
              record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, salesCount, perItem );
            }
            desc.setLength(0);
            desc.append( catName );
            desc.append( " Sales Income - " );
            desc.append( DateTimeFormatter.getFormattedDate( activeDate, "MMM yyyy" ) );
            record.setDescription( desc.toString() );
            ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                ContractTypes.CONTRACT_TYPE_INCOME, record );
          }
          
          if ( creditsInc != 0.0 )
          {
            if ( creditsCount == 0 )
            {
              record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, creditsInc );
            }
            else
            {
              /*@lineinfo:generated-code*//*@lineinfo:602^15*/

//  ************************************************************
//  #sql [Ctx] { select  ( :creditsInc / :creditsCount ) 
//                  from    dual
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  (  :1  /  :2  )  \n                from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.ProfVitalDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDouble(1,creditsInc);
   __sJT_st.setInt(2,creditsCount);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   perItem = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:606^15*/
              record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, creditsCount, perItem );
            }
            desc.setLength(0);
            desc.append( catName );
            desc.append( " Credit Income - " );
            desc.append( DateTimeFormatter.getFormattedDate( activeDate,"MMM yyyy" ) );
            record.setDescription( desc.toString() );
            ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                ContractTypes.CONTRACT_TYPE_INCOME, record );
          }
        }
      }
      resultSet.close();
      it.close();
      
      if ( ContractDataBean.includeVitalExpense() == true )
      {
        /*@lineinfo:generated-code*//*@lineinfo:624^9*/

//  ************************************************************
//  #sql [Ctx] it = { select   sm.active_date,
//                     sm.TOT_EXP_DEBIT
//            from     monthly_extract_summary sm
//            where    sm.merchant_number = :merchantId and
//                     sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   sm.active_date,\n                   sm.TOT_EXP_DEBIT\n          from     monthly_extract_summary sm\n          where    sm.merchant_number =  :1  and\n                   sm.active_date between  :2  and last_day(  :3  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:631^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          activeDate    = resultSet.getDate("active_date");
          vitalExpense  = resultSet.getDouble("tot_exp_debit");
          
          if ( vitalExpense != 0.0 )
          {
            record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, vitalExpense );
            desc.setLength(0);
            desc.append( "TSYS Debit Networks Expense " );
            desc.append( DateTimeFormatter.getFormattedDate( activeDate,"MMM yyyy" ) );
            record.setDescription( desc.toString() );
            ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                ContractTypes.CONTRACT_TYPE_EXPENSE, record );
          }
        }          
      }
    }
    catch( java.sql.SQLException e )
    {
      ContractDataBean.logEntry("loadDetailDebit(): ", e.toString());
      ContractDataBean.addError("loadDetailDebit: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  protected void loadDetailDiscount( long merchantId )
  {
    Date                          activeDate            = null;
    double                        adjAmount             = 0.0;
    int                           adjCount              = 0;
    double                        betIcExpense          = 0.0;
    int                           betType               = BankContractBean.BET_NONE;
    double                        calculatedDiscIncome  = 0.0;
    double                        collectedDiscIncome   = 0.0;
    StringBuffer                  desc                  = new StringBuffer("");
    double                        discExpense           = 0.0;
    double                        icTemp                = 0.0;
    ResultSetIterator             it                    = null;
    ContractTypes.PerItemRecord   record                = null;
    ResultSet                     resultSet             = null;
    
    try
    {
      betType = BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC );
    
      // first load the discount data
      /*@lineinfo:generated-code*//*@lineinfo:684^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date              as active_date,
//                  pl.pl_plan_type             as plan_type,
//                  pl.pl_disc_rate             as disc_rate,
//                  pl.pl_sales_amount          as sales_amount,
//                  pl.pl_correct_disc_amt      as calc_disc,
//                  ( pl.pl_correct_disc_amt - 
//                    (pl.pl_disc_rate_per_item * 
//                     pl.pl_number_of_sales))  as discount_amount,
//                  pl.pl_disc_rate_per_item    as per_item_rate,
//                  pl.pl_number_of_sales       as sales_count,
//                  ( pl.pl_disc_rate_per_item * 
//                    pl.pl_number_of_sales )   as per_item_amount
//          from    monthly_extract_summary sm,
//                  monthly_extract_pl      pl
//          where   sm.merchant_number = :merchantId and
//                  sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                  pl.hh_load_sec = sm.hh_load_sec and
//                  nvl(pl.pl_correct_disc_amt,0) != 0
//                  --( pl.pl_plan_type like 'V%' or 
//                  --  pl.pl_plan_type like 'M%' )
//          order by sm.active_date, pl.pl_plan_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date              as active_date,\n                pl.pl_plan_type             as plan_type,\n                pl.pl_disc_rate             as disc_rate,\n                pl.pl_sales_amount          as sales_amount,\n                pl.pl_correct_disc_amt      as calc_disc,\n                ( pl.pl_correct_disc_amt - \n                  (pl.pl_disc_rate_per_item * \n                   pl.pl_number_of_sales))  as discount_amount,\n                pl.pl_disc_rate_per_item    as per_item_rate,\n                pl.pl_number_of_sales       as sales_count,\n                ( pl.pl_disc_rate_per_item * \n                  pl.pl_number_of_sales )   as per_item_amount\n        from    monthly_extract_summary sm,\n                monthly_extract_pl      pl\n        where   sm.merchant_number =  :1  and\n                sm.active_date between  :2  and last_day(  :3  ) and\n                pl.hh_load_sec = sm.hh_load_sec and\n                nvl(pl.pl_correct_disc_amt,0) != 0\n                --( pl.pl_plan_type like 'V%' or \n                --  pl.pl_plan_type like 'M%' )\n        order by sm.active_date, pl.pl_plan_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:707^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        record = new ContractTypes.VolumeRecord(  activeDate, 
                                                  ContractTypes.CONTRACT_SOURCE_VITAL, 
                                                  betType, resultSet.getInt("SALES_COUNT" ),
                                                  resultSet.getDouble("SALES_AMOUNT"),
                                                  resultSet.getDouble("PER_ITEM_RATE" ),
                                                  resultSet.getDouble("DISC_RATE"),
                                                  resultSet.getDouble("DISCOUNT_AMOUNT") );
         
        calculatedDiscIncome  += ( resultSet.getDouble("DISCOUNT_AMOUNT" ) +
                                   resultSet.getDouble("PER_ITEM_AMOUNT" ) );
                                   
        desc.setLength(0);
        desc.append( mesConstants.getVitalExtractCardTypeDesc( resultSet.getString("PLAN_TYPE" ) ) );
        desc.append( " Volume - " );
        desc.append( DateTimeFormatter.getFormattedDate( activeDate, "MMM yyyy" ) );
        record.setDescription( desc.toString() );
        
        // load income & expense summary records
        ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT, ContractTypes.CONTRACT_TYPE_INCOME, record );
      }   // end while( resultSet.next() )
      resultSet.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:736^7*/

//  ************************************************************
//  #sql [Ctx] { select sum(sm.tot_inc_discount) 
//          from   monthly_extract_summary sm
//          where  sm.merchant_number = :merchantId and
//                 sm.active_date between :ReportDateBegin and :ReportDateEnd
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sum(sm.tot_inc_discount)  \n        from   monthly_extract_summary sm\n        where  sm.merchant_number =  :1  and\n               sm.active_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.ProfVitalDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   collectedDiscIncome = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:742^7*/
      
      //
      // if these two values do not match, then the difference
      // is minimun discount charges.  Since the floating point
      // math in Java is noisy, it is necessary to mask noise in the 
      // decimal digits beyond 0.01.  Otherwise, the software will
      // occasionally display a Minimun Discount of $0.00
      //
      if ( Math.abs( collectedDiscIncome - calculatedDiscIncome ) >= 0.01 )
      {
        desc.setLength(0);
        desc.append("Minimum Discount Fees ");
        desc.append( DateTimeFormatter.getFormattedDate( ReportDateBegin, "MMM yyyy" ) );
        
        // compare the month and year.  if either value
        // does not match then display the ending date.
        if ( ! ContractDataBean.isSameMonthYear( ReportDateBegin, ReportDateEnd ) )
        {
          desc.append( " - " );
          desc.append( DateTimeFormatter.getFormattedDate( ReportDateEnd, "MMM yyyy" ) );
        }
        
        record = new ContractTypes.PerItemRecord( null, ContractTypes.CONTRACT_SOURCE_VITAL, BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC ),
                                                    -1, ( collectedDiscIncome - calculatedDiscIncome ) );
        record.setDescription( desc.toString() );                            
        
        ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT, ContractTypes.CONTRACT_TYPE_INCOME, record );
      }

      // offset income with any Discount/Interchange related DCE adjustments      
      /*@lineinfo:generated-code*//*@lineinfo:773^7*/

//  ************************************************************
//  #sql [Ctx] { select sum( sm.disc_ic_dce_adj_count ),
//                 sum( sm.disc_ic_dce_adj_amount ) 
//          from   monthly_extract_summary  sm
//          where  sm.merchant_number = :merchantId and
//                 sm.active_date between :ReportDateBegin and last_day(:ReportDateEnd)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sum( sm.disc_ic_dce_adj_count ),\n               sum( sm.disc_ic_dce_adj_amount )  \n        from   monthly_extract_summary  sm\n        where  sm.merchant_number =  :1  and\n               sm.active_date between  :2  and last_day( :3 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.ProfVitalDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   adjCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   adjAmount = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:780^7*/
      
      if ( adjCount != 0 )
      {
        desc.setLength(0);
        desc.append("Disc/IC DCE Adjustments ");
        desc.append( DateTimeFormatter.getFormattedDate( ReportDateBegin, "MMM yyyy" ) );
        
        // compare the month and year.  if either value
        // does not match then display the ending date.
        if ( ! ContractDataBean.isSameMonthYear( ReportDateBegin, ReportDateEnd ) )
        {
          desc.append( " - " );
          desc.append( DateTimeFormatter.getFormattedDate( ReportDateEnd, "MMM yyyy" ) );
        }
        
        record = new ContractTypes.AdjustmentRecord( null, BankContractBean.BET_DISC_IC_DCE_ADJUSTMENTS, adjCount, adjAmount );
        record.setDescription( desc.toString() );                            
        
        ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT, ContractTypes.CONTRACT_TYPE_INCOME, record );
      }
    }
    catch( java.sql.SQLException e )
    {
      ContractDataBean.logEntry("loadDiscountDetails()", e.toString());
      ContractDataBean.addError("loadDiscountDetails: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  protected void loadDetailEquip( long merchantId )
  {
    Date                            activeDate            = null;
    int                             betType;
    StringBuffer                    desc                  = new StringBuffer("");
    ResultSetIterator               it                    = null;
    ContractTypes.PerItemRecord     record                = null;
    ResultSet                       resultSet             = null;
    int                             sales                 = -1;
    double                          totalCalculatedIncome = 0.0;
    double                          totalCollectedIncome  = 0.0;
    double                          vitalExpense          = 0.0;
    
    try
    {
      // setup a generic BET type for the any summary level items.
      betType = BankContractBean.groupIndexToBetType( ContractDataBean.getReportBetGroup() );
      
      if ( ContractDataBean.getReportBetGroup() == BankContractBean.BET_GROUP_TAX_COLLECTED )
      {
        /*@lineinfo:generated-code*//*@lineinfo:833^9*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct 
//                   sm.active_date                             as active_date,
//                   decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, 
//                           0, 1,
//                           st.ST_NUMBER_OF_ITEMS_ON_STMT )    as item_count,
//                   decode( st.ST_AMOUNT_OF_ITEM_ON_STMT,
//                           0, ( decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, 
//                                        0, st.ST_FEE_AMOUNT,
//                                        st.ST_FEE_AMOUNT / st.ST_NUMBER_OF_ITEMS_ON_STMT ) ),
//                           st.ST_AMOUNT_OF_ITEM_ON_STMT )     as per_item,                       
//                   st.ST_STATEMENT_DESC                       as item_desc,
//                   st.ST_FEE_AMOUNT                           as total_amount            
//            from   monthly_extract_summary  sm,
//                   monthly_extract_st       st,
//                   monthly_extract_cg       cg
//            where  sm.merchant_number = :merchantId and
//                   sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                   st.hh_load_sec = sm.hh_load_sec and
//                   st.st_fee_amount != 0 and
//                   st.st_statement_desc like '%TAX%' and
//                   cg.hh_load_sec = sm.hh_load_sec and
//                   cg.cg_charge_record_type in ('POS','IMP') and
//                   cg.CG_MESSAGE_FOR_STMT = st.ST_STATEMENT_DESC
//            order by st.ST_STATEMENT_DESC
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct \n                 sm.active_date                             as active_date,\n                 decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, \n                         0, 1,\n                         st.ST_NUMBER_OF_ITEMS_ON_STMT )    as item_count,\n                 decode( st.ST_AMOUNT_OF_ITEM_ON_STMT,\n                         0, ( decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, \n                                      0, st.ST_FEE_AMOUNT,\n                                      st.ST_FEE_AMOUNT / st.ST_NUMBER_OF_ITEMS_ON_STMT ) ),\n                         st.ST_AMOUNT_OF_ITEM_ON_STMT )     as per_item,                       \n                 st.ST_STATEMENT_DESC                       as item_desc,\n                 st.ST_FEE_AMOUNT                           as total_amount            \n          from   monthly_extract_summary  sm,\n                 monthly_extract_st       st,\n                 monthly_extract_cg       cg\n          where  sm.merchant_number =  :1  and\n                 sm.active_date between  :2  and last_day(  :3  ) and\n                 st.hh_load_sec = sm.hh_load_sec and\n                 st.st_fee_amount != 0 and\n                 st.st_statement_desc like '%TAX%' and\n                 cg.hh_load_sec = sm.hh_load_sec and\n                 cg.cg_charge_record_type in ('POS','IMP') and\n                 cg.CG_MESSAGE_FOR_STMT = st.ST_STATEMENT_DESC\n          order by st.ST_STATEMENT_DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:859^9*/
      }
      else    // sales or rental
      {
        sales = ( ContractDataBean.getReportBetGroup() == BankContractBean.BET_GROUP_EQUIP_SALES ) ? 1 : 0;
      
        /*@lineinfo:generated-code*//*@lineinfo:865^9*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct 
//                   sm.active_date                             as active_date,
//                   decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, 
//                           0, 1,
//                           st.ST_NUMBER_OF_ITEMS_ON_STMT )    as item_count,
//                   decode( st.ST_AMOUNT_OF_ITEM_ON_STMT,
//                           0, ( decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, 
//                                        0, st.ST_FEE_AMOUNT,
//                                        st.ST_FEE_AMOUNT / st.ST_NUMBER_OF_ITEMS_ON_STMT ) ),
//                           st.ST_AMOUNT_OF_ITEM_ON_STMT )     as per_item,                       
//                   st.ST_STATEMENT_DESC                       as item_desc,
//                   st.ST_FEE_AMOUNT                           as total_amount            
//            from   monthly_extract_summary  sm,
//                   monthly_extract_st       st,
//                   monthly_extract_cg       cg
//            where  sm.merchant_number = :merchantId and
//                   sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                      /* take all the statement records that were 
//                         generated as a result of a POS or IMP 
//                         charge record  */
//                     (
//                       ( :sales = 1 and
//                         st.hh_load_sec = sm.hh_load_sec and
//                         st.st_fee_amount != 0 and
//                         not st.st_statement_desc like '%RENT%' and
//                         not st.st_statement_desc like '%TAX%' and
//                         cg.hh_load_sec = sm.hh_load_sec and
//                         cg.cg_charge_record_type in ('POS','IMP') and
//                         cg.CG_MESSAGE_FOR_STMT = st.ST_STATEMENT_DESC ) or
//                       ( :sales = 0 and
//                         st.hh_load_sec = sm.hh_load_sec and
//                         st.st_fee_amount != 0 and
//                         st.st_statement_desc like '%RENT%' and
//                         not st.st_statement_desc like '%TAX%' and
//                         cg.hh_load_sec = sm.hh_load_sec and
//                         cg.cg_charge_record_type in ('POS','IMP') and
//                         cg.CG_MESSAGE_FOR_STMT = st.ST_STATEMENT_DESC ) 
//                     )
//            order by st.ST_STATEMENT_DESC
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct \n                 sm.active_date                             as active_date,\n                 decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, \n                         0, 1,\n                         st.ST_NUMBER_OF_ITEMS_ON_STMT )    as item_count,\n                 decode( st.ST_AMOUNT_OF_ITEM_ON_STMT,\n                         0, ( decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, \n                                      0, st.ST_FEE_AMOUNT,\n                                      st.ST_FEE_AMOUNT / st.ST_NUMBER_OF_ITEMS_ON_STMT ) ),\n                         st.ST_AMOUNT_OF_ITEM_ON_STMT )     as per_item,                       \n                 st.ST_STATEMENT_DESC                       as item_desc,\n                 st.ST_FEE_AMOUNT                           as total_amount            \n          from   monthly_extract_summary  sm,\n                 monthly_extract_st       st,\n                 monthly_extract_cg       cg\n          where  sm.merchant_number =  :1  and\n                 sm.active_date between  :2  and last_day(  :3  ) and\n                    /* take all the statement records that were \n                       generated as a result of a POS or IMP \n                       charge record  */\n                   (\n                     (  :4  = 1 and\n                       st.hh_load_sec = sm.hh_load_sec and\n                       st.st_fee_amount != 0 and\n                       not st.st_statement_desc like '%RENT%' and\n                       not st.st_statement_desc like '%TAX%' and\n                       cg.hh_load_sec = sm.hh_load_sec and\n                       cg.cg_charge_record_type in ('POS','IMP') and\n                       cg.CG_MESSAGE_FOR_STMT = st.ST_STATEMENT_DESC ) or\n                     (  :5  = 0 and\n                       st.hh_load_sec = sm.hh_load_sec and\n                       st.st_fee_amount != 0 and\n                       st.st_statement_desc like '%RENT%' and\n                       not st.st_statement_desc like '%TAX%' and\n                       cg.hh_load_sec = sm.hh_load_sec and\n                       cg.cg_charge_record_type in ('POS','IMP') and\n                       cg.CG_MESSAGE_FOR_STMT = st.ST_STATEMENT_DESC ) \n                   )\n          order by st.ST_STATEMENT_DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   __sJT_st.setInt(4,sales);
   __sJT_st.setInt(5,sales);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:906^9*/
      }        
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, resultSet.getInt( "ITEM_COUNT" ),
                                    resultSet.getDouble("PER_ITEM") );
        desc.setLength(0);
        desc.append( resultSet.getString("ITEM_DESC") );
        desc.append( " - " );
        desc.append( DateTimeFormatter.getFormattedDate( activeDate,"MMM yyyy" ) );
        record.setDescription( desc.toString() );
        ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                            ContractTypes.CONTRACT_TYPE_INCOME, record );
        totalCalculatedIncome += resultSet.getDouble("TOTAL_AMOUNT");                            
      }
      resultSet.close();
      it.close();
      
      // if the contract does not exclude COGS, then add them to 
      // the equip sales/rental income/expense reporting.
      // no need to load any expenses when we are just loading
      // the sales tax collected from the merchant account.
      if ( ContractDataBean.isEquipmentExpIncluded() &&
           (betType != BankContractBean.BET_GROUP_TAX_COLLECTED) )
      {
        loadDetailMesEquip( merchantId, betType, sales, ContractTypes.CONTRACT_TYPE_EXPENSE );
      }
    }
    catch( java.sql.SQLException e )
    {
      ContractDataBean.logEntry("loadDetailEquip(): ", e.toString());
      ContractDataBean.addError("loadDetailEquip: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  protected void loadDetailPlan( long merchantId )
  {
    Date                            activeDate            = null;
    double                          amount                = 0.0;
    int                             betType;
    StringBuffer                    desc                  = new StringBuffer("");
    ResultSetIterator               it                    = null;
    ContractTypes.PerItemRecord     record                = null;
    ResultSet                       resultSet             = null;
    double                          totalCalculatedIncome = 0.0;
    double                          totalCollectedIncome  = 0.0;
    double                          vitalExpense          = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:963^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                  as active_date,
//                  pl.pl_plan_type                 as plan_type,
//                  pl.pl_disc_rate                 as disc_rate,
//                  pl.pl_sales_amount              as sales_amount,
//                  (pl.pl_correct_disc_amt - 
//                   ( pl.pl_disc_rate_per_item * 
//                     pl.pl_number_of_sales ))     as discount_amount,
//                  pl.pl_disc_rate_per_item        as per_item,
//                  pl.pl_number_of_sales           as sales_count,
//                  ( pl.pl_disc_rate_per_item * 
//                    pl.pl_number_of_sales )       as per_item_amount
//          from    monthly_extract_summary sm,
//                  monthly_extract_pl      pl
//          where   sm.merchant_number = :merchantId and
//                  sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                  pl.hh_load_sec = sm.hh_load_sec and
//                  pl.pl_plan_type in 
//                    (
//                      :mesConstants.VITAL_ME_PL_DINERS,
//                      :mesConstants.VITAL_ME_PL_DISC,
//                      :mesConstants.VITAL_ME_PL_AMEX,
//                      :mesConstants.VITAL_ME_PL_EBT,
//                      :mesConstants.VITAL_ME_PL_JCB,
//                      :mesConstants.VITAL_ME_PL_PL1,
//                      :mesConstants.VITAL_ME_PL_PL2,
//                      :mesConstants.VITAL_ME_PL_PL3
//                    )
//          order by sm.active_date, pl.pl_plan_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                  as active_date,\n                pl.pl_plan_type                 as plan_type,\n                pl.pl_disc_rate                 as disc_rate,\n                pl.pl_sales_amount              as sales_amount,\n                (pl.pl_correct_disc_amt - \n                 ( pl.pl_disc_rate_per_item * \n                   pl.pl_number_of_sales ))     as discount_amount,\n                pl.pl_disc_rate_per_item        as per_item,\n                pl.pl_number_of_sales           as sales_count,\n                ( pl.pl_disc_rate_per_item * \n                  pl.pl_number_of_sales )       as per_item_amount\n        from    monthly_extract_summary sm,\n                monthly_extract_pl      pl\n        where   sm.merchant_number =  :1  and\n                sm.active_date between  :2  and last_day(  :3  ) and\n                pl.hh_load_sec = sm.hh_load_sec and\n                pl.pl_plan_type in \n                  (\n                     :4 ,\n                     :5 ,\n                     :6 ,\n                     :7 ,\n                     :8 ,\n                     :9 ,\n                     :10 ,\n                     :11 \n                  )\n        order by sm.active_date, pl.pl_plan_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   __sJT_st.setString(4,mesConstants.VITAL_ME_PL_DINERS);
   __sJT_st.setString(5,mesConstants.VITAL_ME_PL_DISC);
   __sJT_st.setString(6,mesConstants.VITAL_ME_PL_AMEX);
   __sJT_st.setString(7,mesConstants.VITAL_ME_PL_EBT);
   __sJT_st.setString(8,mesConstants.VITAL_ME_PL_JCB);
   __sJT_st.setString(9,mesConstants.VITAL_ME_PL_PL1);
   __sJT_st.setString(10,mesConstants.VITAL_ME_PL_PL2);
   __sJT_st.setString(11,mesConstants.VITAL_ME_PL_PL3);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:993^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        activeDate  = resultSet.getDate("activeDate");
        amount      = resultSet.getDouble( "PER_ITEM_AMOUNT" );
        
        if ( amount != 0.0 )
        { 
          betType = BankContractBean.getPlanBetType( resultSet.getString("PLAN_TYPE") );
          if ( betType != BankContractBean.BET_NONE )
          {
            record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, resultSet.getInt( "SALES_COUNT" ), resultSet.getDouble("PER_ITEM") );
            desc.setLength(0);
            desc.append( mesConstants.getVitalExtractCardTypeDesc( resultSet.getString("PLAN_TYPE" ) ) );
            desc.append( " Volume - " );
            desc.append( DateTimeFormatter.getFormattedDate( activeDate, "MMM yyyy" ) );
            record.setDescription( desc.toString() );
            ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                ContractTypes.CONTRACT_TYPE_INCOME, record );
          }
          totalCalculatedIncome += amount;
        }
      }
      resultSet.close();
      it.close();
      
      // setup a generic BET type for the any summary level items.
      betType = BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_PLAN_FEES );
      
      /*@lineinfo:generated-code*//*@lineinfo:1024^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum( sm.tot_inc_ind_plans ) 
//          from    monthly_extract_summary sm
//          where    sm.merchant_number = :merchantId and
//                   sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( sm.tot_inc_ind_plans )  \n        from    monthly_extract_summary sm\n        where    sm.merchant_number =  :1  and\n                 sm.active_date between  :2  and last_day(  :3  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.reports.ProfVitalDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   totalCollectedIncome = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1030^7*/
      
      // if the bank has BET tables setup to bill the merchant
      // for individual items (i.e. CB&T Ind. Plan BET # 0002
      // charged $0.29 for Dialpay) then there will be income
      // in the total record, but no details in the DRTs (plan
      // records).  Add this item as a generic detail item.
      if ( totalCalculatedIncome != totalCollectedIncome )
      {
        record = new ContractTypes.PerItemRecord( null, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, ( totalCollectedIncome - totalCalculatedIncome ) );
        desc.setLength(0);
        desc.append( "TSYS Individual Plan Income " );
        desc.append( DateTimeFormatter.getFormattedDate( ReportDateBegin, "MMM yyyy" ) );
        
        if ( ! ContractDataBean.isSameMonthYear( ReportDateBegin, ReportDateEnd ) ) 
        {
          desc.append( " - " );
          desc.append( DateTimeFormatter.getFormattedDate( ReportDateEnd, "MMM yyyy" ) );
        }          
        record.setDescription( desc.toString() );
        ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                            ContractTypes.CONTRACT_TYPE_INCOME, record );
      }
      
      if ( ContractDataBean.includeVitalExpense() == true )
      {
        // add up the sum of individual plan expense reported
        // over the specified date range.
        /*@lineinfo:generated-code*//*@lineinfo:1058^9*/

//  ************************************************************
//  #sql [Ctx] it = { select   sm.active_date           as active_date,
//                     sm.TOT_EXP_IND_PLANS     as plan_exp
//            from     monthly_extract_summary sm
//            where    sm.merchant_number = :merchantId and
//                     sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   sm.active_date           as active_date,\n                   sm.TOT_EXP_IND_PLANS     as plan_exp\n          from     monthly_extract_summary sm\n          where    sm.merchant_number =  :1  and\n                   sm.active_date between  :2  and last_day(  :3  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1065^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          activeDate = resultSet.getDate("active_date");
          vitalExpense = resultSet.getDouble("plan_exp");
          
          // there are not details available for individual plan expenses.
          // just add a single entry for the sum of the amount assessed
          // by Vital over the given date range.
          if ( vitalExpense != 0.0 )
          {
            record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, vitalExpense );
            desc.setLength(0);
            desc.append( "TSYS Individual Plan Expense " );
            desc.append( DateTimeFormatter.getFormattedDate( activeDate, "MMM yyyy" ) );
            record.setDescription( desc.toString() );
            ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                ContractTypes.CONTRACT_TYPE_EXPENSE, record );
          }
        }          
      }
    }
    catch( java.sql.SQLException e )
    {
      ContractDataBean.logEntry("loadDetailPlan(): ", e.toString());
      ContractDataBean.addError("loadDetailPlan: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  protected void loadDetailSysFees( long merchantId )
  {
    Date                            activeDate            = null;
    double                          adjAmount             = 0.0;
    int                             adjCount              = 0;
    int                             betType;
    long                            contractNode;
    StringBuffer                    desc                  = new StringBuffer("");
    double                          expense               = 0.0;
    ResultSetIterator               it                    = null;
    ContractTypes.PerItemRecord     record                = null;
    ResultSet                       resultSet             = null;
    double                          totalCalculatedIncome = 0.0;
    double                          totalCollectedIncome  = 0.0;
    
    try
    {
      // setup a generic BET type for the any summary level items.
      betType = BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_SYSTEM_FEES );
      
      // get the liability contract node to use when masking revenue
      contractNode = ContractDataBean.getContractNode( merchantId, ContractTypes.CONTRACT_SOURCE_LIABILITY );
    
      /*@lineinfo:generated-code*//*@lineinfo:1123^7*/

//  ************************************************************
//  #sql [Ctx] it = { select sm.active_date                             as active_date,
//                 decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, 
//                         0, 1,
//                         st.ST_NUMBER_OF_ITEMS_ON_STMT )    as item_count,
//                 decode( st.ST_AMOUNT_OF_ITEM_ON_STMT,
//                         0, ( decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, 
//                                      0, st.ST_FEE_AMOUNT,
//                                      st.ST_FEE_AMOUNT / st.ST_NUMBER_OF_ITEMS_ON_STMT ) ),
//                         st.ST_AMOUNT_OF_ITEM_ON_STMT )     as per_item,                       
//                 st.ST_STATEMENT_DESC                       as item_desc,
//                 st.ST_FEE_AMOUNT                           as total_amount
//          from   monthly_extract_summary  sm,
//                 monthly_extract_st       st,
//                 monthly_extract_cg       cg
//          where    sm.merchant_number = :merchantId and
//                   sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                      /* take all the statement records that were 
//                         generated as a result of a charge record 
//                         except POS and IMP record types          */
//                   st.hh_load_sec = sm.hh_load_sec and
//                   st.st_fee_amount != 0 and
//                   cg.hh_load_sec(+) = st.hh_load_sec and
//                   cg.CG_MESSAGE_FOR_STMT(+) = st.ST_STATEMENT_DESC and
//                   ( 
//                      not cg.cg_charge_record_type in ('POS','IMP') or  
//                      st.st_statement_desc like 'CHARGEBACK FEE%' 
//                    )
//          order by st.ST_STATEMENT_DESC      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select sm.active_date                             as active_date,\n               decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, \n                       0, 1,\n                       st.ST_NUMBER_OF_ITEMS_ON_STMT )    as item_count,\n               decode( st.ST_AMOUNT_OF_ITEM_ON_STMT,\n                       0, ( decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, \n                                    0, st.ST_FEE_AMOUNT,\n                                    st.ST_FEE_AMOUNT / st.ST_NUMBER_OF_ITEMS_ON_STMT ) ),\n                       st.ST_AMOUNT_OF_ITEM_ON_STMT )     as per_item,                       \n               st.ST_STATEMENT_DESC                       as item_desc,\n               st.ST_FEE_AMOUNT                           as total_amount\n        from   monthly_extract_summary  sm,\n               monthly_extract_st       st,\n               monthly_extract_cg       cg\n        where    sm.merchant_number =  :1  and\n                 sm.active_date between  :2  and last_day(  :3  ) and\n                    /* take all the statement records that were \n                       generated as a result of a charge record \n                       except POS and IMP record types          */\n                 st.hh_load_sec = sm.hh_load_sec and\n                 st.st_fee_amount != 0 and\n                 cg.hh_load_sec(+) = st.hh_load_sec and\n                 cg.CG_MESSAGE_FOR_STMT(+) = st.ST_STATEMENT_DESC and\n                 ( \n                    not cg.cg_charge_record_type in ('POS','IMP') or  \n                    st.st_statement_desc like 'CHARGEBACK FEE%' \n                  )\n        order by st.ST_STATEMENT_DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1153^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, resultSet.getInt( "ITEM_COUNT" ),
                                    resultSet.getDouble("PER_ITEM") );
        desc.setLength(0);
        desc.append( resultSet.getString("ITEM_DESC") );
        desc.append( " - " );
        desc.append( DateTimeFormatter.getFormattedDate( activeDate,"MMM yyyy" ) );
        record.setDescription( desc.toString() );
        ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                            ContractTypes.CONTRACT_TYPE_INCOME, record );
        totalCalculatedIncome += resultSet.getDouble("TOTAL_AMOUNT");                            
      }
      resultSet.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:1173^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum( sm.tot_inc_sys_generated -
//                       ( sm.equip_rental_income +
//                         sm.equip_sales_income +
//                         nvl(sm.equip_sales_tax_collected,0) ) ) 
//          from    monthly_extract_summary sm
//          where   sm.merchant_number = :merchantId and
//                  sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( sm.tot_inc_sys_generated -\n                     ( sm.equip_rental_income +\n                       sm.equip_sales_income +\n                       nvl(sm.equip_sales_tax_collected,0) ) )  \n        from    monthly_extract_summary sm\n        where   sm.merchant_number =  :1  and\n                sm.active_date between  :2  and last_day(  :3  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.reports.ProfVitalDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   totalCollectedIncome = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1182^7*/
      
      // if Vital does not provide all the details,
      // then add a generic record to account for
      // the missing system fees income.
      if ( totalCalculatedIncome != totalCollectedIncome )
      {
        record = new ContractTypes.PerItemRecord( null, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, ( totalCollectedIncome - totalCalculatedIncome ) );
        desc.setLength(0);
        desc.append( "TSYS System Generated Income " );
        desc.append( DateTimeFormatter.getFormattedDate( ReportDateBegin, "MMM yyyy" ) );
        
        if ( ! ContractDataBean.isSameMonthYear( ReportDateBegin, ReportDateEnd ) ) 
        {
          desc.append( " - " );
          desc.append( DateTimeFormatter.getFormattedDate( ReportDateEnd, "MMM yyyy" ) );
        }          
        record.setDescription( desc.toString() );
        ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                            ContractTypes.CONTRACT_TYPE_INCOME, record );
      }
      
      if ( ContractDataBean.includeSupplyExpense() )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1206^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date          as active_date,
//                    nvl(sm.supply_cost,0)   as supply_cost
//            from    monthly_extract_summary sm
//            where   sm.merchant_number = :merchantId and
//                    sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date          as active_date,\n                  nvl(sm.supply_cost,0)   as supply_cost\n          from    monthly_extract_summary sm\n          where   sm.merchant_number =  :1  and\n                  sm.active_date between  :2  and last_day(  :3  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1213^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          expense     = resultSet.getDouble("supply_cost");
          activeDate  = resultSet.getDate("active_date");
      
          if ( expense != 0.0 )
          {
            record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, expense );
            desc.setLength(0);
            desc.append( "Supply Expense " );
            desc.append( DateTimeFormatter.getFormattedDate( activeDate, "MMM yyyy" ) );
            record.setDescription( desc.toString() );
            ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                ContractTypes.CONTRACT_TYPE_EXPENSE, record );
          }
        }
        resultSet.close();
        it.close();
      }        
      
      if ( ContractDataBean.includeVitalExpense() == true )
      {
        // add up the sum of individual plan expense reported
        // over the specified date range.
        /*@lineinfo:generated-code*//*@lineinfo:1240^9*/

//  ************************************************************
//  #sql [Ctx] it = { select   sm.active_date,
//                     sm.TOT_EXP_SYS_GENERATED
//            from     monthly_extract_summary sm
//            where    sm.merchant_number = :merchantId and
//                     sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   sm.active_date,\n                   sm.TOT_EXP_SYS_GENERATED\n          from     monthly_extract_summary sm\n          where    sm.merchant_number =  :1  and\n                   sm.active_date between  :2  and last_day(  :3  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.reports.ProfVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"23com.mes.reports.ProfVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1247^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          expense     = resultSet.getDouble("tot_exp_sys_generated");
          activeDate  = resultSet.getDate("active_date");
          
          // Vital does not provide any detail for system
          // generated expense, so make a completely generic 
          // record for the details page to display
          if ( expense != 0.0 )
          {
            record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1, expense );
            desc.setLength(0);
            desc.append( "TSYS System Generated Expense " );
            desc.append( DateTimeFormatter.getFormattedDate( activeDate, "MMM yyyy" ) );
            record.setDescription( desc.toString() );
            ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                ContractTypes.CONTRACT_TYPE_EXPENSE, record );
          }
        }          
        resultSet.close();
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      ContractDataBean.logEntry("loadDetailSysFees(): ", e.toString());
      ContractDataBean.addError("loadDetailSysFees: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  protected String loadIcCatDesc( String cardType, int cat )
  {
    String        retVal = "Undefined IC Category";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1290^7*/

//  ************************************************************
//  #sql [Ctx] { select description 
//          from   ic_category_desc
//          where  card_type = :cardType and
//                 category  = :cat
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select description  \n        from   ic_category_desc\n        where  card_type =  :1  and\n               category  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.reports.ProfVitalDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,cardType);
   __sJT_st.setInt(2,cat);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1296^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/