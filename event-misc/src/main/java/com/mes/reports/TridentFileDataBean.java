/*@lineinfo:filename=TridentFileDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/TridentFileDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-01 11:46:29 -0700 (Tue, 01 Jul 2008) $
  Version            : $Revision: 15021 $

  Change History:
     See VSS database

  Copyright (C) 2000-2005,2006 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.FieldGroup;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class TridentFileDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public  int             BankNumber        = 0;
    public  double          CreditsAmount     = 0.0;
    public  double          DebitsAmount      = 0.0;
    public  String          LoadFilename      = null;
    public  double          NetAmount         = 0.0;
    public  Date            TransmitDate      = null;
    public  double          TSYSCreditsAmount = 0.0;
    public  double          TSYSDebitsAmount  = 0.0;
    public  double          TSYSNetAmount     = 0.0;
    public  double          VSNetAmount       = 0.0;
    public  double          MCNetAmount       = 0.0;
    public  double          DBNetAmount       = 0.0;
    public  double          OtherNetAmount    = 0.0;
    public  String          Bin               = null;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      BankNumber        = resultSet.getInt("bank_number");
      CreditsAmount     = resultSet.getDouble("credit_total");
      DebitsAmount      = resultSet.getDouble("debit_total");
      LoadFilename      = resultSet.getString("load_filename");
      NetAmount         = resultSet.getDouble("net_amount");
      TransmitDate      = resultSet.getDate("transmit_date");
      TSYSCreditsAmount = resultSet.getDouble("tsys_credit_total");
      TSYSDebitsAmount  = resultSet.getDouble("tsys_debit_total");
      TSYSNetAmount     = resultSet.getDouble("tsys_net_amount");
      VSNetAmount       = resultSet.getDouble("vs_net_amount");
      MCNetAmount       = resultSet.getDouble("mc_net_amount");
      DBNetAmount       = resultSet.getDouble("db_net_amount");
      Bin               = resultSet.getString("bin"); 
      OtherNetAmount    = TSYSNetAmount - VSNetAmount - MCNetAmount - DBNetAmount;
    }
  }
  
  public TridentFileDataBean( )
  {
    super(true);    // use FieldBean support
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup    fgroup;
    
    super.createFields(request);
    
    fgroup = (FieldGroup)getField("searchFields");
    fgroup.deleteField("portfolioId");
    fgroup.deleteField("zip2Node");
  }
    
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Filename\",");
    line.append("\"Bank Number\",");
    line.append("\"BIN\",");
    line.append("\"Transmit Date\",");
    line.append("\"Debits Amount\",");
    line.append("\"Credits Amount\",");
    line.append("\"Net Amount\",");
    line.append("\"TSYS Debits Amount\",");
    line.append("\"TSYS Credits Amount\",");
    line.append("\"VS\",");
    line.append("\"MC\",");
    line.append("\"DB\",");
    line.append("\"Other\",");
    line.append("\"TSYS Net Amount\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData   record = (RowData)obj;
    
    line.setLength(0);
      
    line.append("\"");
    line.append(record.LoadFilename);
    line.append("\",");
    line.append(record.BankNumber);
    line.append(",");
    line.append(record.Bin);
    line.append(",");
    line.append(DateTimeFormatter.getFormattedDate(record.TransmitDate,"MM/dd/yyyy"));
    line.append(",");
    line.append(record.DebitsAmount);
    line.append(",");
    line.append(record.CreditsAmount);
    line.append(",");
    line.append(record.NetAmount);
    line.append(",");
    line.append(record.TSYSDebitsAmount);
    line.append(",");
    line.append(record.TSYSCreditsAmount);
    line.append(",");
    line.append(record.VSNetAmount);
    line.append(",");
    line.append(record.MCNetAmount);
    line.append(",");
    line.append(record.DBNetAmount);
    line.append(",");
    line.append(record.OtherNetAmount);    
    line.append(",");
    line.append(record.NetAmount);
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append("trident_transmit_summary_");
    filename.append( DateTimeFormatter.getFormattedDate(getReportDateBegin(),"MMddyy") );
    if ( getReportDateBegin().equals(getReportDateEnd()) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(getReportDateEnd(),"MMddyy") );
    }
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
        
      case FF_FIXED:
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    long                          nodeId            = 0L;
    ResultSet                     resultSet         = null;
    RowData                       row               = null;

    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:202^7*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+
//                    ordered
//                    index(dt idx_ddf_ext_dt_filename)
//                    use_nl(th dt) 
//                  */ 
//                  tt.load_filename            as load_filename,
//                  tt.transmit_bank_num        as bank_number,
//                  th.transmit_date            as transmit_date,
//                  tt.debit_total              as debit_total,
//                  tt.credit_total             as credit_total,
//                  ( tt.debit_total - 
//                    tt.credit_total )         as net_amount,
//                  db.bin                      as bin,                
//                  sum( decode(dt.debit_credit_indicator,  
//                              'D',dt.transaction_amount,
//                              0 ) )           as tsys_debit_total,
//                  sum( decode(dt.debit_credit_indicator,
//                              'C',dt.transaction_amount,
//                              0 ) )           as tsys_credit_total,
//                  sum( dt.transaction_amount *
//                       decode(dt.debit_credit_indicator,
//                              'C',-1,1) )     as tsys_net_amount,
//                  sum( decode(dt.CARD_TYPE,'VS',dt.TRANSACTION_AMOUNT,0) * 
//                       decode(dt.debit_credit_indicator,'C',-1,1) )
//                                              as vs_net_amount,
//                  sum( decode(dt.CARD_TYPE,'MC',dt.TRANSACTION_AMOUNT,0) * 
//                       decode(dt.debit_credit_indicator,'C',-1,1) )         
//                                              as mc_net_amount,
//                  sum( decode(dt.CARD_TYPE,'DB',dt.TRANSACTION_AMOUNT,0) * 
//                       decode(dt.debit_credit_indicator,'C',-1,1))         
//                                              as db_net_amount
//          from    daily_detail_file_d256_th   th,
//                  daily_detail_file_d256_tt   tt,
//                  daily_detail_file_ext_dt    dt,
//                  trident_debit_bins          db 
//          where   th.transmit_date between :beginDate and :endDate and
//                  th.load_filename like '256m%' and
//                  tt.load_filename = th.load_filename and
//                  dt.load_filename = th.load_filename and
//                  not dt.card_type in ('AM','DS') and
//                  substr(db.HIERARCHY_NODE,1,4) = tt.transmit_bank_num
//          group by  tt.load_filename,
//                    tt.transmit_bank_num,
//                    th.transmit_date,
//                    tt.debit_total,
//                    tt.credit_total,
//                    db.bin
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+\n                  ordered\n                  index(dt idx_ddf_ext_dt_filename)\n                  use_nl(th dt) \n                */ \n                tt.load_filename            as load_filename,\n                tt.transmit_bank_num        as bank_number,\n                th.transmit_date            as transmit_date,\n                tt.debit_total              as debit_total,\n                tt.credit_total             as credit_total,\n                ( tt.debit_total - \n                  tt.credit_total )         as net_amount,\n                db.bin                      as bin,                \n                sum( decode(dt.debit_credit_indicator,  \n                            'D',dt.transaction_amount,\n                            0 ) )           as tsys_debit_total,\n                sum( decode(dt.debit_credit_indicator,\n                            'C',dt.transaction_amount,\n                            0 ) )           as tsys_credit_total,\n                sum( dt.transaction_amount *\n                     decode(dt.debit_credit_indicator,\n                            'C',-1,1) )     as tsys_net_amount,\n                sum( decode(dt.CARD_TYPE,'VS',dt.TRANSACTION_AMOUNT,0) * \n                     decode(dt.debit_credit_indicator,'C',-1,1) )\n                                            as vs_net_amount,\n                sum( decode(dt.CARD_TYPE,'MC',dt.TRANSACTION_AMOUNT,0) * \n                     decode(dt.debit_credit_indicator,'C',-1,1) )         \n                                            as mc_net_amount,\n                sum( decode(dt.CARD_TYPE,'DB',dt.TRANSACTION_AMOUNT,0) * \n                     decode(dt.debit_credit_indicator,'C',-1,1))         \n                                            as db_net_amount\n        from    daily_detail_file_d256_th   th,\n                daily_detail_file_d256_tt   tt,\n                daily_detail_file_ext_dt    dt,\n                trident_debit_bins          db \n        where   th.transmit_date between  :1  and  :2  and\n                th.load_filename like '256m%' and\n                tt.load_filename = th.load_filename and\n                dt.load_filename = th.load_filename and\n                not dt.card_type in ('AM','DS') and\n                substr(db.HIERARCHY_NODE,1,4) = tt.transmit_bank_num\n        group by  tt.load_filename,\n                  tt.transmit_bank_num,\n                  th.transmit_date,\n                  tt.debit_total,\n                  tt.credit_total,\n                  db.bin";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.TridentFileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.TridentFileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:251^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData(resultSet) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    autoSetFields(request);   // stubbed so download servlet works
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/