/*@lineinfo:filename=RiskMerchProcDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/RiskMerchProcDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-02-20 13:23:01 -0800 (Wed, 20 Feb 2008) $
  Version            : $Revision: 14602 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class RiskMerchProcDataBean extends RiskMerchHistoryBase
{
  public class RowData
  {
    public Date           ActiveDate        = null;
    public double         AvgTicket         = 0.0;
    public double         CashAdvanceAmount = 0.0;
    public int            CashAdvanceCount  = 0;
    public double         ChargebackAmount  = 0.0;
    public int            ChargebackCount   = 0;
    public double         ChargebackRatio   = 0.0;
    public double         CreditsAmount     = 0.0;
    public int            CreditsCount      = 0;
    public double         CreditsRatio      = 0.0;
    public double         KeyedAmount       = 0.0;
    public int            KeyedCount        = 0;
    public double         KeyedRatio        = 0.0;
    public Date           MonthBeginDate    = null;
    public Date           MonthEndDate      = null;
    public double         SalesAmount       = 0.0;
    public int            SalesCount        = 0;
    public double         ReversalAmount    = 0.0;
    public int            ReversalCount     = 0;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      ActiveDate        = resultSet.getDate("active_date");
      MonthBeginDate    = resultSet.getDate("month_begin_date");
      MonthEndDate      = resultSet.getDate("month_end_date");
      AvgTicket         = resultSet.getDouble("avg_ticket");
      CashAdvanceAmount = resultSet.getDouble("cash_adv_amount");
      CashAdvanceCount  = resultSet.getInt("cash_adv_count");
      ChargebackAmount  = resultSet.getDouble("cb_amount");
      ChargebackCount   = resultSet.getInt("cb_count");
      ChargebackRatio   = resultSet.getDouble("cb_ratio");
      CreditsAmount     = resultSet.getDouble("credits_amount");
      CreditsCount      = resultSet.getInt("credits_count");
      CreditsRatio      = resultSet.getDouble("credits_ratio");
      SalesAmount       = resultSet.getDouble("sales_amount");
      SalesCount        = resultSet.getInt("sales_count");
      ReversalAmount    = resultSet.getDouble("reversed_amount");
      ReversalCount     = resultSet.getInt("reversed_count");
      KeyedAmount       = resultSet.getDouble("keyed_amount");
      KeyedCount        = resultSet.getInt("keyed_count");

      // calculate the keyed ratio      
      if ( SalesCount > 0 )
      {
        KeyedRatio = (double)((double)KeyedCount/(double)SalesCount);
      }
      else if ( KeyedCount != 0 )   // should never happen
      {
        KeyedRatio = 1;
      }
      else
      {
        KeyedRatio = 0;
      }
    }
  }
  
  public RiskMerchProcDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    super.encodeHeaderCSV(line);

    line.append("\"Active Date\",");
    line.append("\"Sales Count\",");
    line.append("\"Sales Amount\",");
    line.append("\"Avg Ticket\",");
    line.append("\"Cash Advance Count\",");
    line.append("\"Cash Advance Amount\",");
    line.append("\"Credits Count\",");
    line.append("\"Credits Amount\",");
    line.append("\"Credits Ratio\",");
    line.append("\"CB Count\",");
    line.append("\"CB Amount\",");
    line.append("\"CB Ratio\",");
    line.append("\"Keyed Count\",");
    line.append("\"Keyed Amount\",");
    line.append("\"Keyed Ratio\",");
    line.append("\"CB Reversal Count\",");
    line.append("\"CB Reversal Amount\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( DateTimeFormatter.getFormattedDate(record.ActiveDate,"MM/dd/yyyy") );
    line.append( "," );
    line.append( record.SalesCount );
    line.append( "," );
    line.append( record.SalesAmount );
    line.append( "," );
    line.append( record.AvgTicket );
    line.append( "," );
    line.append( record.CashAdvanceCount );
    line.append( "," );
    line.append( record.CashAdvanceAmount );
    line.append( "," );
    line.append( record.CreditsCount );
    line.append( "," );
    line.append( record.CreditsAmount );
    line.append( "," );
    line.append( record.CreditsRatio );
    line.append( "," );
    line.append( record.ChargebackCount );
    line.append( "," );
    line.append( record.ChargebackAmount );
    line.append( "," );
    line.append( record.ChargebackRatio );
    line.append( "," );
    line.append( record.KeyedCount );
    line.append( "," );
    line.append( record.KeyedAmount );
    line.append( "," );
    line.append( record.KeyedRatio );
    line.append( "," );
    line.append( record.ReversalCount );
    line.append( "," );
    line.append( record.ReversalAmount );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_proc_summary_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( )
  {
    loadData( getReportMerchantId(), ReportDateBegin, ReportDateEnd );
  }
  
  public void loadData( long merchantId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      MerchData = null;
      
      /*@lineinfo:generated-code*//*@lineinfo:224^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number                  as merchant_number,
//                  mf.dba_name                         as dba_name,
//                  mr.merch_legal_name                 as legal_name,
//                  to_date(mf.date_opened,'mmddrr')    as date_opened,
//                  mf.sic_code                         as sic_code,
//                  trunc(min(nvl(sm.batch_date,sysdate)),'month') as active_date,
//                  trunc(min(nvl(sm.batch_date,sysdate)),'month') as month_begin_date,
//                  max(nvl(sm.batch_date,trunc(sysdate)))         as month_end_date,                
//                  (
//                    sum(sm.visa_sales_count + sm.mc_sales_count) -
//                    nvl(ca.cash_adv_count,0)
//                  )                                   as sales_count,
//                  ( 
//                    sum(sm.visa_sales_amount + sm.mc_sales_amount) -
//                    nvl(ca.cash_adv_amount,0)
//                  )                                   as sales_amount,
//                  ca.cash_adv_count                   as cash_adv_count,
//                  ca.cash_adv_amount                  as cash_adv_amount,
//                  decode( sum(sm.visa_sales_count + sm.mc_sales_count),
//                          0, 0,
//                          sum(sm.visa_sales_amount + sm.mc_sales_amount)/
//                          sum(sm.visa_sales_count + sm.mc_sales_count)
//                        )                             as avg_ticket,
//                  sum(sm.visa_credits_count +
//                      sm.mc_credits_count)            as credits_count,
//                  sum(sm.visa_credits_amount +
//                      sm.mc_credits_amount)           as credits_amount,
//                  decode( (sum(sm.visa_sales_amount + sm.mc_sales_amount) - nvl(ca.cash_adv_amount,0)),
//                          0, decode( sum(sm.visa_credits_amount + sm.mc_credits_amount), 0, 0, 1 ),
//                          ( sum(sm.visa_credits_amount + sm.mc_credits_amount) / (sum(sm.visa_sales_amount + sm.mc_sales_amount) - nvl(ca.cash_adv_amount,0)) )
//                        )                             as credits_ratio,
//                  cb.cb_count                         as cb_count,
//                  cb.cb_amount                        as cb_amount,
//                  decode( sum(sm.visa_sales_amount + sm.mc_sales_amount),
//                          0, decode( cb.cb_amount, 0, 0, 1 ),
//                          ( nvl(cb.cb_amount,0) / sum(sm.visa_sales_amount + sm.mc_sales_amount) )
//                        )                             as cb_ratio,
//                  nvl(cb.reversed_count,0)            as reversed_count,
//                  nvl(cb.reversed_amount,0)           as reversed_amount,
//                  nvl(keyed.keyed_count,0)            as keyed_count,
//                  nvl(keyed.keyed_amount,0)           as keyed_amount
//          from    mif                         mf,
//                  merchant                    mr,
//                  daily_detail_file_summary   sm,      
//                  (
//                    select  mf.merchant_number              as merchant_number,
//                            trunc(sysdate,'month')          as active_date,
//                            count(cb.incoming_date)         as cb_count,
//                            sum(cb.tran_amount)             as cb_amount,
//                            sum(decode(cba.action_code,
//                                       null,0,
//                                       1))                  as reversed_count,
//                            sum(decode(cba.action_code,
//                                       null,0,
//                                       cb.tran_amount))     as reversed_amount
//                    from    mif                           mf,
//                            network_chargebacks           cb,
//                            network_chargeback_activity   cba
//                    where   mf.merchant_number = :merchantId and
//                            cb.merchant_number(+) = mf.merchant_number and
//                            cb.incoming_date(+) >= trunc(sysdate,'month') and
//                            cba.cb_load_sec(+) = cb.cb_load_sec and
//                            cba.action_code(+) = 'C' -- REMC only
//                    group by mf.merchant_number                                     
//                  )                           cb,
//                  (
//                    select  mf.merchant_number              as merchant_number,
//                            trunc(sysdate,'month')          as active_date,
//                            count(dt.transacction_code)     as cash_adv_count,
//                            sum(decode(dt.debit_credit_indicator,'C',-1,1) *
//                                dt.transaction_amount )     as cash_adv_amount
//                    from    mif                           mf,
//                            daily_detail_file_dt          dt
//                    where   mf.merchant_number = :merchantId and
//                            dt.merchant_account_number(+) = mf.merchant_number and
//                            dt.batch_date(+) >= trunc(sysdate,'month') and
//                            nvl(dt.transacction_code,0) = 102
//                    group by mf.merchant_number                                     
//                  )                           ca,
//                  (
//                    select mf.merchant_number                                     as merchant_number,
//                           trunc( sysdate, 'month' )                              as active_date,
//                           sum( dt.TRANSACTION_AMOUNT *
//                                decode( dt.DEBIT_CREDIT_INDICATOR, 'C', -1, 1 ) ) as keyed_amount,
//                           count( dt.transaction_amount )                         as keyed_count
//                    from   mif                      mf,
//                           daily_detail_file_dt     dt
//                    where  mf.merchant_number = :merchantId and
//                           dt.batch_date(+) >= trunc(sysdate,'month') and
//                           dt.merchant_account_number(+) = mf.merchant_number and
//                           (
//                            dt.card_type is null or
//                            substr(dt.CARD_TYPE,1,1) in ( 'V','M' ) 
//                           ) and
//                           dt.pos_entry_mode(+) = '01' and
//                           dt.debit_credit_indicator(+) = 'D' 
//                    group by mf.merchant_number                         
//                  )                           keyed
//          where   mf.merchant_number = :merchantId and
//                  mr.merch_number(+) = mf.merchant_number and
//                  sm.merchant_number(+) = mf.merchant_number and
//                  sm.batch_date(+) >= trunc(sysdate,'month') and
//                  cb.merchant_number(+) = mf.merchant_number and
//                  ca.merchant_number(+) = mf.merchant_number and
//                  cb.active_date(+) = trunc(sysdate,'month') and
//                  keyed.active_date(+) = trunc(sysdate,'month')
//          group by  mf.merchant_number, mf.dba_name,
//                    mr.merch_legal_name, mf.date_opened,
//                    mf.sic_code,
//                    cb.cb_count, cb.cb_amount, 
//                    ca.cash_adv_count, ca.cash_adv_amount,                  
//                    cb.reversed_count, cb.reversed_amount,
//                    keyed.keyed_count, keyed.keyed_amount                
//          order by active_date desc      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number                  as merchant_number,\n                mf.dba_name                         as dba_name,\n                mr.merch_legal_name                 as legal_name,\n                to_date(mf.date_opened,'mmddrr')    as date_opened,\n                mf.sic_code                         as sic_code,\n                trunc(min(nvl(sm.batch_date,sysdate)),'month') as active_date,\n                trunc(min(nvl(sm.batch_date,sysdate)),'month') as month_begin_date,\n                max(nvl(sm.batch_date,trunc(sysdate)))         as month_end_date,                \n                (\n                  sum(sm.visa_sales_count + sm.mc_sales_count) -\n                  nvl(ca.cash_adv_count,0)\n                )                                   as sales_count,\n                ( \n                  sum(sm.visa_sales_amount + sm.mc_sales_amount) -\n                  nvl(ca.cash_adv_amount,0)\n                )                                   as sales_amount,\n                ca.cash_adv_count                   as cash_adv_count,\n                ca.cash_adv_amount                  as cash_adv_amount,\n                decode( sum(sm.visa_sales_count + sm.mc_sales_count),\n                        0, 0,\n                        sum(sm.visa_sales_amount + sm.mc_sales_amount)/\n                        sum(sm.visa_sales_count + sm.mc_sales_count)\n                      )                             as avg_ticket,\n                sum(sm.visa_credits_count +\n                    sm.mc_credits_count)            as credits_count,\n                sum(sm.visa_credits_amount +\n                    sm.mc_credits_amount)           as credits_amount,\n                decode( (sum(sm.visa_sales_amount + sm.mc_sales_amount) - nvl(ca.cash_adv_amount,0)),\n                        0, decode( sum(sm.visa_credits_amount + sm.mc_credits_amount), 0, 0, 1 ),\n                        ( sum(sm.visa_credits_amount + sm.mc_credits_amount) / (sum(sm.visa_sales_amount + sm.mc_sales_amount) - nvl(ca.cash_adv_amount,0)) )\n                      )                             as credits_ratio,\n                cb.cb_count                         as cb_count,\n                cb.cb_amount                        as cb_amount,\n                decode( sum(sm.visa_sales_amount + sm.mc_sales_amount),\n                        0, decode( cb.cb_amount, 0, 0, 1 ),\n                        ( nvl(cb.cb_amount,0) / sum(sm.visa_sales_amount + sm.mc_sales_amount) )\n                      )                             as cb_ratio,\n                nvl(cb.reversed_count,0)            as reversed_count,\n                nvl(cb.reversed_amount,0)           as reversed_amount,\n                nvl(keyed.keyed_count,0)            as keyed_count,\n                nvl(keyed.keyed_amount,0)           as keyed_amount\n        from    mif                         mf,\n                merchant                    mr,\n                daily_detail_file_summary   sm,      \n                (\n                  select  mf.merchant_number              as merchant_number,\n                          trunc(sysdate,'month')          as active_date,\n                          count(cb.incoming_date)         as cb_count,\n                          sum(cb.tran_amount)             as cb_amount,\n                          sum(decode(cba.action_code,\n                                     null,0,\n                                     1))                  as reversed_count,\n                          sum(decode(cba.action_code,\n                                     null,0,\n                                     cb.tran_amount))     as reversed_amount\n                  from    mif                           mf,\n                          network_chargebacks           cb,\n                          network_chargeback_activity   cba\n                  where   mf.merchant_number =  :1  and\n                          cb.merchant_number(+) = mf.merchant_number and\n                          cb.incoming_date(+) >= trunc(sysdate,'month') and\n                          cba.cb_load_sec(+) = cb.cb_load_sec and\n                          cba.action_code(+) = 'C' -- REMC only\n                  group by mf.merchant_number                                     \n                )                           cb,\n                (\n                  select  mf.merchant_number              as merchant_number,\n                          trunc(sysdate,'month')          as active_date,\n                          count(dt.transacction_code)     as cash_adv_count,\n                          sum(decode(dt.debit_credit_indicator,'C',-1,1) *\n                              dt.transaction_amount )     as cash_adv_amount\n                  from    mif                           mf,\n                          daily_detail_file_dt          dt\n                  where   mf.merchant_number =  :2  and\n                          dt.merchant_account_number(+) = mf.merchant_number and\n                          dt.batch_date(+) >= trunc(sysdate,'month') and\n                          nvl(dt.transacction_code,0) = 102\n                  group by mf.merchant_number                                     \n                )                           ca,\n                (\n                  select mf.merchant_number                                     as merchant_number,\n                         trunc( sysdate, 'month' )                              as active_date,\n                         sum( dt.TRANSACTION_AMOUNT *\n                              decode( dt.DEBIT_CREDIT_INDICATOR, 'C', -1, 1 ) ) as keyed_amount,\n                         count( dt.transaction_amount )                         as keyed_count\n                  from   mif                      mf,\n                         daily_detail_file_dt     dt\n                  where  mf.merchant_number =  :3  and\n                         dt.batch_date(+) >= trunc(sysdate,'month') and\n                         dt.merchant_account_number(+) = mf.merchant_number and\n                         (\n                          dt.card_type is null or\n                          substr(dt.CARD_TYPE,1,1) in ( 'V','M' ) \n                         ) and\n                         dt.pos_entry_mode(+) = '01' and\n                         dt.debit_credit_indicator(+) = 'D' \n                  group by mf.merchant_number                         \n                )                           keyed\n        where   mf.merchant_number =  :4  and\n                mr.merch_number(+) = mf.merchant_number and\n                sm.merchant_number(+) = mf.merchant_number and\n                sm.batch_date(+) >= trunc(sysdate,'month') and\n                cb.merchant_number(+) = mf.merchant_number and\n                ca.merchant_number(+) = mf.merchant_number and\n                cb.active_date(+) = trunc(sysdate,'month') and\n                keyed.active_date(+) = trunc(sysdate,'month')\n        group by  mf.merchant_number, mf.dba_name,\n                  mr.merch_legal_name, mf.date_opened,\n                  mf.sic_code,\n                  cb.cb_count, cb.cb_amount, \n                  ca.cash_adv_count, ca.cash_adv_amount,                  \n                  cb.reversed_count, cb.reversed_amount,\n                  keyed.keyed_count, keyed.keyed_amount                \n        order by active_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.RiskMerchProcDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setLong(4,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.RiskMerchProcDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:340^7*/
      resultSet = it.getResultSet();
    
      if( resultSet.next() )
      {
        if ( MerchData == null )
        {
          MerchData = new MerchantData( resultSet );
        }
        ReportRows.addElement( new RowData( resultSet ) );
      }
      resultSet.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:354^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                  as merchant_number,
//                  mf.dba_name                         as dba_name,
//                  mr.merch_legal_name                 as legal_name,
//                  to_date(mf.date_opened,'mmddrr')    as date_opened,
//                  mf.sic_code                         as sic_code,
//                  sm.active_date                      as active_date,
//                  nvl(sm.month_begin_date,
//                      sm.active_date)                 as month_begin_date,
//                  sm.month_end_date                   as month_end_date,
//                  sm.vmc_sales_count                  as sales_count,
//                  sm.vmc_sales_amount                 as sales_amount,
//                  nvl(sm.cash_advance_vol_count,0)    as cash_adv_count,
//                  nvl(sm.cash_advance_vol_amount,0)   as cash_adv_amount,
//                  sm.vmc_avg_ticket                   as avg_ticket,
//                  sm.vmc_credits_count                as credits_count,
//                  sm.vmc_credits_amount               as credits_amount,
//                  decode( sm.vmc_sales_amount,
//                          0, decode( sm.vmc_credits_amount, 0, 0, 1 ),
//                          ( sm.vmc_credits_amount / sm.vmc_sales_amount )
//                        )                             as credits_ratio,
//                  sm.incoming_chargeback_count        as cb_count,
//                  nvl(sm.incoming_chargeback_amount,0)as cb_amount,
//                  decode( sm.vmc_sales_amount,
//                          0, decode( sm.incoming_chargeback_amount, 0, 0, 1 ),
//                          ( nvl(sm.incoming_chargeback_amount,0) / sm.vmc_sales_amount )
//                        )                             as cb_ratio,
//                  nvl(rev.reversed_count,0)           as reversed_count,
//                  nvl(rev.reversed_amount,0)          as reversed_amount,
//                  nvl(keyed.keyed_count,0)            as keyed_count,
//                  nvl(keyed.keyed_amount,0)           as keyed_amount
//          from    monthly_extract_summary     sm,
//                  mif                         mf,
//                  merchant                    mr,
//                  (
//                    select  cb.merchant_number                as merchant_number,
//                            trunc(cb.incoming_date,'month')   as active_date,
//                            count(cba.action_code)            as reversed_count,
//                            sum(cb.tran_amount)               as reversed_amount
//                    from    network_chargebacks           cb,
//                            network_chargeback_activity   cba
//                    where   cb.merchant_number = :merchantId and
//                            cb.incoming_date between trunc( (:ReportDateBegin - 515), 'month') and :ReportDateBegin and
//                            cba.cb_load_sec = cb.cb_load_sec and
//                            cba.action_code = 'C' -- REMC only
//                    group by cb.merchant_number, trunc(cb.incoming_date,'month')                                     
//                  )                           rev,
//                  (
//                    select trunc( dt.batch_date, 'month' )                        as active_date,
//                           sum( dt.TRANSACTION_AMOUNT *
//                                decode( dt.DEBIT_CREDIT_INDICATOR, 'C', -1, 1 ) ) as keyed_amount,
//                           count( dt.transaction_amount )                         as keyed_count
//                    from   daily_detail_file_dt     dt
//                    where  dt.batch_date between trunc( (:ReportDateBegin - 515), 'month') and :ReportDateBegin and
//                           dt.merchant_account_number = :merchantId and
//                           dt.pos_entry_mode = '01' and
//                           dt.debit_credit_indicator = 'D' and
//                           substr(dt.CARD_TYPE,1,1) in ('V','M')
//                    group by trunc(dt.batch_date,'month')                         
//                  )                           keyed
//          where   mf.merchant_number = :merchantId and
//                  mr.merch_number(+) = mf.merchant_number and
//                  sm.merchant_number = mf.merchant_number and
//                  sm.active_date between trunc( (:ReportDateBegin - 515), 'month') and :ReportDateBegin and
//                  rev.merchant_number(+) = sm.merchant_number and
//                  rev.active_date(+) = sm.active_date and
//                  keyed.active_date(+) = sm.active_date
//          order by sm.active_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                  as merchant_number,\n                mf.dba_name                         as dba_name,\n                mr.merch_legal_name                 as legal_name,\n                to_date(mf.date_opened,'mmddrr')    as date_opened,\n                mf.sic_code                         as sic_code,\n                sm.active_date                      as active_date,\n                nvl(sm.month_begin_date,\n                    sm.active_date)                 as month_begin_date,\n                sm.month_end_date                   as month_end_date,\n                sm.vmc_sales_count                  as sales_count,\n                sm.vmc_sales_amount                 as sales_amount,\n                nvl(sm.cash_advance_vol_count,0)    as cash_adv_count,\n                nvl(sm.cash_advance_vol_amount,0)   as cash_adv_amount,\n                sm.vmc_avg_ticket                   as avg_ticket,\n                sm.vmc_credits_count                as credits_count,\n                sm.vmc_credits_amount               as credits_amount,\n                decode( sm.vmc_sales_amount,\n                        0, decode( sm.vmc_credits_amount, 0, 0, 1 ),\n                        ( sm.vmc_credits_amount / sm.vmc_sales_amount )\n                      )                             as credits_ratio,\n                sm.incoming_chargeback_count        as cb_count,\n                nvl(sm.incoming_chargeback_amount,0)as cb_amount,\n                decode( sm.vmc_sales_amount,\n                        0, decode( sm.incoming_chargeback_amount, 0, 0, 1 ),\n                        ( nvl(sm.incoming_chargeback_amount,0) / sm.vmc_sales_amount )\n                      )                             as cb_ratio,\n                nvl(rev.reversed_count,0)           as reversed_count,\n                nvl(rev.reversed_amount,0)          as reversed_amount,\n                nvl(keyed.keyed_count,0)            as keyed_count,\n                nvl(keyed.keyed_amount,0)           as keyed_amount\n        from    monthly_extract_summary     sm,\n                mif                         mf,\n                merchant                    mr,\n                (\n                  select  cb.merchant_number                as merchant_number,\n                          trunc(cb.incoming_date,'month')   as active_date,\n                          count(cba.action_code)            as reversed_count,\n                          sum(cb.tran_amount)               as reversed_amount\n                  from    network_chargebacks           cb,\n                          network_chargeback_activity   cba\n                  where   cb.merchant_number =  :1  and\n                          cb.incoming_date between trunc( ( :2  - 515), 'month') and  :3  and\n                          cba.cb_load_sec = cb.cb_load_sec and\n                          cba.action_code = 'C' -- REMC only\n                  group by cb.merchant_number, trunc(cb.incoming_date,'month')                                     \n                )                           rev,\n                (\n                  select trunc( dt.batch_date, 'month' )                        as active_date,\n                         sum( dt.TRANSACTION_AMOUNT *\n                              decode( dt.DEBIT_CREDIT_INDICATOR, 'C', -1, 1 ) ) as keyed_amount,\n                         count( dt.transaction_amount )                         as keyed_count\n                  from   daily_detail_file_dt     dt\n                  where  dt.batch_date between trunc( ( :4  - 515), 'month') and  :5  and\n                         dt.merchant_account_number =  :6  and\n                         dt.pos_entry_mode = '01' and\n                         dt.debit_credit_indicator = 'D' and\n                         substr(dt.CARD_TYPE,1,1) in ('V','M')\n                  group by trunc(dt.batch_date,'month')                         \n                )                           keyed\n        where   mf.merchant_number =  :7  and\n                mr.merch_number(+) = mf.merchant_number and\n                sm.merchant_number = mf.merchant_number and\n                sm.active_date between trunc( ( :8  - 515), 'month') and  :9  and\n                rev.merchant_number(+) = sm.merchant_number and\n                rev.active_date(+) = sm.active_date and\n                keyed.active_date(+) = sm.active_date\n        order by sm.active_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.RiskMerchProcDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateBegin);
   __sJT_st.setDate(4,ReportDateBegin);
   __sJT_st.setDate(5,ReportDateBegin);
   __sJT_st.setLong(6,merchantId);
   __sJT_st.setLong(7,merchantId);
   __sJT_st.setDate(8,ReportDateBegin);
   __sJT_st.setDate(9,ReportDateBegin);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.RiskMerchProcDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:423^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        if ( MerchData == null )
        {
          MerchData = new MerchantData( resultSet );
        }
        ReportRows.addElement( new RowData( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",merchantId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/