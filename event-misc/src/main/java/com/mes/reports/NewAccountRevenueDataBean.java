/*@lineinfo:filename=NewAccountRevenueDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/NewAccountRevenueDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-11-14 11:12:34 -0800 (Fri, 14 Nov 2008) $
  Version            : $Revision: 15530 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class NewAccountRevenueDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public  long        NodeId                = 0L;
    public  String      OrgName               = null;
    public  Date        DateActivated         = null;
    public  Date        DateOpened            = null;
    public  String      AccountStatus         = null;
    public  int         VmcSalesCount         = 0;
    public  double      VmcSalesAmount        = 0.0;
    public  int         VmcCreditsCount       = 0;
    public  double      VmcCreditsAmount      = 0.0;
    public  int         CashAdvanceCount      = 0;
    public  double      CashAdvanceAmount     = 0.0;
    public  double      Revenue               = 0.0;
    public  int         VmcSalesCountYTD      = 0;
    public  double      VmcSalesAmountYTD     = 0.0;
    public  int         VmcCreditsCountYTD    = 0;
    public  double      VmcCreditsAmountYTD   = 0.0;
    public  int         CashAdvanceCountYTD   = 0;
    public  double      CashAdvanceAmountYTD  = 0.0;
    public  double      RevenueYTD            = 0.0;
    public  boolean     MerchantData          = false;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      NodeId                = resultSet.getLong  ("node_id");
      OrgName               = processString(resultSet.getString("org_name"));
      MerchantData          = (resultSet.getInt("is_merchant") != 0);
      if ( MerchantData )
      {
        DateActivated         = resultSet.getDate("date_activated");
        DateOpened            = resultSet.getDate("date_opened");
        AccountStatus         = processString(resultSet.getString("account_status"));
      }        
      VmcSalesCount         = resultSet.getInt   ("vmc_sales_count");
      VmcSalesAmount        = resultSet.getDouble("vmc_sales_amount");
      VmcCreditsCount       = resultSet.getInt   ("vmc_credits_count");
      VmcCreditsAmount      = resultSet.getDouble("vmc_credits_amount");
      CashAdvanceCount      = resultSet.getInt   ("cash_advance_vol_count");
      CashAdvanceAmount     = resultSet.getDouble("cash_advance_vol_amount");
      Revenue               = resultSet.getDouble("revenue");
      VmcSalesCountYTD      = resultSet.getInt   ("vmc_sales_count_ytd");
      VmcSalesAmountYTD     = resultSet.getDouble("vmc_sales_amount_ytd");
      VmcCreditsCountYTD    = resultSet.getInt   ("vmc_credits_count_ytd");
      VmcCreditsAmountYTD   = resultSet.getDouble("vmc_credits_amount_ytd");
      CashAdvanceCountYTD   = resultSet.getInt   ("cash_advance_vol_count_ytd");
      CashAdvanceAmountYTD  = resultSet.getDouble("cash_advance_vol_amount_ytd");
      RevenueYTD            = resultSet.getDouble("revenue_ytd");
    }
  }
  
  protected Date                ActiveDate    = null;
  
  public NewAccountRevenueDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    if ( ReportType == RT_DETAILS )
    {
      line.append("\"Merchant ID\",");
      line.append("\"DBA Name\",");
      line.append("\"Date Opened\",");
      line.append("\"Date Activated\",");
      line.append("\"Account Status\",");
    }
    else // RT_SUMMARY
    {
      line.append("\"Node ID\",");
      line.append("\"Org Name\",");
    }
    line.append("\"VMC Sales Count\",");
    line.append("\"VMC Sales Amount\",");
    line.append("\"VMC Credits Count\",");
    line.append("\"VMC Credits Amount\",");
    line.append("\"Cash Advance Count\",");
    line.append("\"Cash Advance Amount\",");
    line.append("\"Revenue\",");
    line.append("\"VMC Sales Count YTD\",");
    line.append("\"VMC Sales Amount YTD\",");
    line.append("\"VMC Credits Count YTD\",");
    line.append("\"VMC Credits Amount YTD\",");
    line.append("\"Cash Advance Count YTD\",");
    line.append("\"Cash Advance Amount YTD\",");
    line.append("\"Revenue YTD\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    
    line.append( encodeHierarchyNode(record.NodeId) );
    line.append(",\"");            
    line.append( record.OrgName );
    line.append("\",");
    if ( ReportType == RT_DETAILS )
    {
      line.append( DateTimeFormatter.getFormattedDate(record.DateOpened,"MM/dd/yyyy") );
      line.append(",");            
      line.append( DateTimeFormatter.getFormattedDate(record.DateActivated,"MM/dd/yyyy") );
      line.append(",");            
      line.append( record.AccountStatus );
      line.append(",");         
    }      
    line.append( record.VmcSalesCount );
    line.append(",");         
    line.append( record.VmcSalesAmount );
    line.append(",");        
    line.append( record.VmcCreditsCount );
    line.append(",");       
    line.append( record.VmcCreditsAmount );
    line.append(",");      
    line.append( record.CashAdvanceCount );
    line.append(",");      
    line.append( record.CashAdvanceAmount );
    line.append(",");     
    line.append( record.Revenue );
    line.append(",");               
    line.append( record.VmcSalesCountYTD );
    line.append(",");      
    line.append( record.VmcSalesAmountYTD );
    line.append(",");     
    line.append( record.VmcCreditsCountYTD );
    line.append(",");    
    line.append( record.VmcCreditsAmountYTD );
    line.append(",");   
    line.append( record.CashAdvanceCountYTD );
    line.append(",");   
    line.append( record.CashAdvanceAmountYTD );
    line.append(",");  
    line.append( record.RevenueYTD );
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    Calendar      cal     = Calendar.getInstance();
    
    super.encodeNodeUrl( buffer, nodeId, beginDate, endDate );
    
    cal.setTime(ActiveDate);
    buffer.append("&activeMonth=");
    buffer.append(cal.get(Calendar.MONTH));
    buffer.append("&activeDay=");
    buffer.append(cal.get(Calendar.DAY_OF_MONTH));
    buffer.append("&activeYear=");
    buffer.append(cal.get(Calendar.YEAR));
  }
  
  public Date getActiveDate( )
  {
    return( ActiveDate );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_new_acct_revenue_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMMyyyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMMyyyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    long                          nodeId            = 0L;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      nodeId = orgIdToHierarchyNode(orgId);
      
      if ( ReportType == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:256^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    sm.merchant_number                            as node_id,
//                      mf.dba_name                                   as org_name,
//                      to_date(mf.date_opened,'mmddrr')              as date_opened,
//                      mf.activation_date                            as date_activated,
//                      1                                             as is_merchant,
//                      nvl(mf.dmacctst,'A')                          as account_status,
//                      sum( decode( sm.active_date,
//                                   :ActiveDate, sm.vmc_sales_count,
//                                   0 ) )                            as vmc_sales_count,
//                      sum( decode( sm.active_date,
//                                   :ActiveDate, sm.vmc_sales_amount,
//                                   0 ) )                            as vmc_sales_amount,
//                      sum( decode( sm.active_date,
//                                   :ActiveDate, sm.vmc_credits_count,
//                                   0 ) )                            as vmc_credits_count,
//                      sum( decode( sm.active_date,
//                                   :ActiveDate, sm.vmc_credits_amount,
//                                   0 ) )                            as vmc_credits_amount,
//                      sum( decode( sm.active_date,
//                                   :ActiveDate, sm.vmc_cash_advance_count,
//                                   0 ) )                            as cash_advance_vol_count,
//                      sum( decode( sm.active_date,
//                                   :ActiveDate, sm.vmc_cash_advance_amount,
//                                   0 ) )                            as cash_advance_vol_amount,
//                      sum( decode( sm.active_date,
//                                   :ActiveDate,((sm.tot_inc_discount +
//                                                 sm.tot_inc_interchange +
//                                                 sm.tot_inc_authorization +
//                                                 sm.tot_inc_capture +
//                                                 sm.tot_inc_debit +
//                                                 sm.tot_inc_ind_plans +
//                                                 sm.tot_inc_sys_generated +
//                                                 nvl(sm.disc_ic_dce_adj_amount,0) + 
//                                                 nvl(sm.fee_dce_adj_amount,0) +
//                                                 nvl(mcrf.tot_ndr,0) +
//                                                 nvl(mcrf.tot_authorization,0) +
//                                                 nvl(mcrf.tot_capture,0) +
//                                                 nvl(mcrf.tot_debit,0) +
//                                                 nvl(mcrf.tot_ind_plans,0) +
//                                                 nvl(mcrf.tot_sys_generated,0) +
//                                                 nvl(mcrf.tot_equip_rental,0) +
//                                                 nvl(mcrf.tot_equip_sales,0) ) -
//                                                (decode( sm.interchange_expense,
//                                                         0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),
//                                                         sm.interchange_expense ) +
//                                                 sm.vmc_assessment_expense + 
//                                                 sm.tot_exp_authorization +
//                                                 sm.tot_exp_capture +
//                                                 sm.tot_exp_debit +
//                                                 sm.tot_exp_ind_plans +
//                                                 sm.tot_exp_sys_generated +
//                                                 decode(sm.bank_number, 3858, sm.equip_sales_income, 0)) ),
//                                   0 ) )                            as revenue,
//                      sum( sm.vmc_sales_count )                     as vmc_sales_count_ytd,
//                      sum( sm.vmc_sales_amount )                    as vmc_sales_amount_ytd,
//                      sum( sm.vmc_credits_count )                   as vmc_credits_count_ytd,
//                      sum( sm.vmc_credits_amount )                  as vmc_credits_amount_ytd,
//                      sum( sm.vmc_cash_advance_count )              as cash_advance_vol_count_ytd,
//                      sum( sm.vmc_cash_advance_amount )             as cash_advance_vol_amount_ytd,  
//                      sum( ((sm.tot_inc_discount +
//                             sm.tot_inc_interchange +
//                             sm.tot_inc_authorization +
//                             sm.tot_inc_capture +
//                             sm.tot_inc_debit +
//                             sm.tot_inc_ind_plans +
//                             sm.tot_inc_sys_generated +
//                             nvl(sm.disc_ic_dce_adj_amount,0) + 
//                             nvl(sm.fee_dce_adj_amount,0) +
//                             nvl(mcrf.tot_ndr,0) +
//                             nvl(mcrf.tot_authorization,0) +
//                             nvl(mcrf.tot_capture,0) +
//                             nvl(mcrf.tot_debit,0) +
//                             nvl(mcrf.tot_ind_plans,0) +
//                             nvl(mcrf.tot_sys_generated,0) +
//                             nvl(mcrf.tot_equip_rental,0) +
//                             nvl(mcrf.tot_equip_sales,0) ) -
//                             (decode( sm.interchange_expense,
//                                      0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),
//                                      sm.interchange_expense ) +
//                             sm.vmc_assessment_expense + 
//                             sm.tot_exp_authorization +
//                             sm.tot_exp_capture +
//                             sm.tot_exp_debit +
//                             sm.tot_exp_ind_plans +
//                             sm.tot_exp_sys_generated +
//                             decode(sm.bank_number, 3858, sm.equip_sales_income, 0))) ) as revenue_ytd
//            from      t_hierarchy                 th,
//                      monthly_extract_summary     sm,
//                      mif                         mf,                  
//                      monthly_extract_contract    mcli,
//                      monthly_extract_contract    mcrf,
//                      monthly_extract_contract    mcrs,
//                      monthly_extract_contract    mcex,
//                      organization                mo
//            where     th.hier_type = 1 and
//                      th.ancestor = :nodeId and
//                      th.entity_type = 4 and
//                      sm.assoc_hierarchy_node = th.descendent and
//                      sm.active_date between trunc(:ActiveDate,'year') and trunc( (trunc(:ActiveDate,'year')+345), 'month' ) and
//                      mf.merchant_number = sm.merchant_number and 
//                      --not mf.activation_date is null and
//                      (
//                        (mf.bank_number = 3858 and mif_date_opened(mf.date_opened) between :beginDate and last_day(:endDate) ) or
//                        (mf.bank_number != 3858 and mf.activation_date is not null and mf.activation_date between :beginDate and last_day(:endDate) )
//                      ) and                  
//                      --mf.activation_date between :beginDate and last_day(:endDate) and                  
//                      mcli.hh_load_sec(+) = sm.hh_load_sec and
//                      mcli.merchant_number(+) = sm.merchant_number and
//                      mcli.contract_type(+) = 1 and
//                      mcrf.hh_load_sec(+) = sm.hh_load_sec and
//                      mcrf.merchant_number(+) = sm.merchant_number and
//                      mcrf.contract_type(+) = 2 and
//                      mcrs.hh_load_sec(+) = sm.hh_load_sec and
//                      mcrs.merchant_number(+) = sm.merchant_number and
//                      mcrs.contract_type(+) = 3 and
//                      mcex.hh_load_sec(+) = sm.hh_load_sec and
//                      mcex.merchant_number(+) = sm.merchant_number and
//                      mcex.contract_type(+) = 4 and
//                      mo.org_group = sm.merchant_number
//            group by  sm.merchant_number,
//                      mf.dba_name,
//                      to_date(mf.date_opened,'mmddrr'),
//                      mf.activation_date,
//                      nvl(mf.dmacctst,'A')
//            order by mf.dba_name, sm.merchant_number      
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    sm.merchant_number                            as node_id,\n                    mf.dba_name                                   as org_name,\n                    to_date(mf.date_opened,'mmddrr')              as date_opened,\n                    mf.activation_date                            as date_activated,\n                    1                                             as is_merchant,\n                    nvl(mf.dmacctst,'A')                          as account_status,\n                    sum( decode( sm.active_date,\n                                  :1 , sm.vmc_sales_count,\n                                 0 ) )                            as vmc_sales_count,\n                    sum( decode( sm.active_date,\n                                  :2 , sm.vmc_sales_amount,\n                                 0 ) )                            as vmc_sales_amount,\n                    sum( decode( sm.active_date,\n                                  :3 , sm.vmc_credits_count,\n                                 0 ) )                            as vmc_credits_count,\n                    sum( decode( sm.active_date,\n                                  :4 , sm.vmc_credits_amount,\n                                 0 ) )                            as vmc_credits_amount,\n                    sum( decode( sm.active_date,\n                                  :5 , sm.vmc_cash_advance_count,\n                                 0 ) )                            as cash_advance_vol_count,\n                    sum( decode( sm.active_date,\n                                  :6 , sm.vmc_cash_advance_amount,\n                                 0 ) )                            as cash_advance_vol_amount,\n                    sum( decode( sm.active_date,\n                                  :7 ,((sm.tot_inc_discount +\n                                               sm.tot_inc_interchange +\n                                               sm.tot_inc_authorization +\n                                               sm.tot_inc_capture +\n                                               sm.tot_inc_debit +\n                                               sm.tot_inc_ind_plans +\n                                               sm.tot_inc_sys_generated +\n                                               nvl(sm.disc_ic_dce_adj_amount,0) + \n                                               nvl(sm.fee_dce_adj_amount,0) +\n                                               nvl(mcrf.tot_ndr,0) +\n                                               nvl(mcrf.tot_authorization,0) +\n                                               nvl(mcrf.tot_capture,0) +\n                                               nvl(mcrf.tot_debit,0) +\n                                               nvl(mcrf.tot_ind_plans,0) +\n                                               nvl(mcrf.tot_sys_generated,0) +\n                                               nvl(mcrf.tot_equip_rental,0) +\n                                               nvl(mcrf.tot_equip_sales,0) ) -\n                                              (decode( sm.interchange_expense,\n                                                       0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),\n                                                       sm.interchange_expense ) +\n                                               sm.vmc_assessment_expense + \n                                               sm.tot_exp_authorization +\n                                               sm.tot_exp_capture +\n                                               sm.tot_exp_debit +\n                                               sm.tot_exp_ind_plans +\n                                               sm.tot_exp_sys_generated +\n                                               decode(sm.bank_number, 3858, sm.equip_sales_income, 0)) ),\n                                 0 ) )                            as revenue,\n                    sum( sm.vmc_sales_count )                     as vmc_sales_count_ytd,\n                    sum( sm.vmc_sales_amount )                    as vmc_sales_amount_ytd,\n                    sum( sm.vmc_credits_count )                   as vmc_credits_count_ytd,\n                    sum( sm.vmc_credits_amount )                  as vmc_credits_amount_ytd,\n                    sum( sm.vmc_cash_advance_count )              as cash_advance_vol_count_ytd,\n                    sum( sm.vmc_cash_advance_amount )             as cash_advance_vol_amount_ytd,  \n                    sum( ((sm.tot_inc_discount +\n                           sm.tot_inc_interchange +\n                           sm.tot_inc_authorization +\n                           sm.tot_inc_capture +\n                           sm.tot_inc_debit +\n                           sm.tot_inc_ind_plans +\n                           sm.tot_inc_sys_generated +\n                           nvl(sm.disc_ic_dce_adj_amount,0) + \n                           nvl(sm.fee_dce_adj_amount,0) +\n                           nvl(mcrf.tot_ndr,0) +\n                           nvl(mcrf.tot_authorization,0) +\n                           nvl(mcrf.tot_capture,0) +\n                           nvl(mcrf.tot_debit,0) +\n                           nvl(mcrf.tot_ind_plans,0) +\n                           nvl(mcrf.tot_sys_generated,0) +\n                           nvl(mcrf.tot_equip_rental,0) +\n                           nvl(mcrf.tot_equip_sales,0) ) -\n                           (decode( sm.interchange_expense,\n                                    0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),\n                                    sm.interchange_expense ) +\n                           sm.vmc_assessment_expense + \n                           sm.tot_exp_authorization +\n                           sm.tot_exp_capture +\n                           sm.tot_exp_debit +\n                           sm.tot_exp_ind_plans +\n                           sm.tot_exp_sys_generated +\n                           decode(sm.bank_number, 3858, sm.equip_sales_income, 0))) ) as revenue_ytd\n          from      t_hierarchy                 th,\n                    monthly_extract_summary     sm,\n                    mif                         mf,                  \n                    monthly_extract_contract    mcli,\n                    monthly_extract_contract    mcrf,\n                    monthly_extract_contract    mcrs,\n                    monthly_extract_contract    mcex,\n                    organization                mo\n          where     th.hier_type = 1 and\n                    th.ancestor =  :8  and\n                    th.entity_type = 4 and\n                    sm.assoc_hierarchy_node = th.descendent and\n                    sm.active_date between trunc( :9 ,'year') and trunc( (trunc( :10 ,'year')+345), 'month' ) and\n                    mf.merchant_number = sm.merchant_number and \n                    --not mf.activation_date is null and\n                    (\n                      (mf.bank_number = 3858 and mif_date_opened(mf.date_opened) between  :11  and last_day( :12 ) ) or\n                      (mf.bank_number != 3858 and mf.activation_date is not null and mf.activation_date between  :13  and last_day( :14 ) )\n                    ) and                  \n                    --mf.activation_date between :beginDate and last_day(:endDate) and                  \n                    mcli.hh_load_sec(+) = sm.hh_load_sec and\n                    mcli.merchant_number(+) = sm.merchant_number and\n                    mcli.contract_type(+) = 1 and\n                    mcrf.hh_load_sec(+) = sm.hh_load_sec and\n                    mcrf.merchant_number(+) = sm.merchant_number and\n                    mcrf.contract_type(+) = 2 and\n                    mcrs.hh_load_sec(+) = sm.hh_load_sec and\n                    mcrs.merchant_number(+) = sm.merchant_number and\n                    mcrs.contract_type(+) = 3 and\n                    mcex.hh_load_sec(+) = sm.hh_load_sec and\n                    mcex.merchant_number(+) = sm.merchant_number and\n                    mcex.contract_type(+) = 4 and\n                    mo.org_group = sm.merchant_number\n          group by  sm.merchant_number,\n                    mf.dba_name,\n                    to_date(mf.date_opened,'mmddrr'),\n                    mf.activation_date,\n                    nvl(mf.dmacctst,'A')\n          order by mf.dba_name, sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.NewAccountRevenueDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,ActiveDate);
   __sJT_st.setDate(2,ActiveDate);
   __sJT_st.setDate(3,ActiveDate);
   __sJT_st.setDate(4,ActiveDate);
   __sJT_st.setDate(5,ActiveDate);
   __sJT_st.setDate(6,ActiveDate);
   __sJT_st.setDate(7,ActiveDate);
   __sJT_st.setLong(8,nodeId);
   __sJT_st.setDate(9,ActiveDate);
   __sJT_st.setDate(10,ActiveDate);
   __sJT_st.setDate(11,beginDate);
   __sJT_st.setDate(12,endDate);
   __sJT_st.setDate(13,beginDate);
   __sJT_st.setDate(14,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.NewAccountRevenueDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:383^9*/
      }
      else    // RT_SUMMARY
      {
        int assoc = ( isOrgAssociation(orgId) ? 1 : 0 );
        
        /*@lineinfo:generated-code*//*@lineinfo:389^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    o.org_group                                   as node_id,
//                      o.org_name                                    as org_name,
//                      0                                             as is_merchant,
//                      sum( decode( sm.active_date,
//                                   :ActiveDate, sm.vmc_sales_count,
//                                   0 ) )                            as vmc_sales_count,
//                      sum( decode( sm.active_date,
//                                   :ActiveDate, sm.vmc_sales_amount,
//                                   0 ) )                            as vmc_sales_amount,
//                      sum( decode( sm.active_date,
//                                   :ActiveDate, sm.vmc_credits_count,
//                                   0 ) )                            as vmc_credits_count,
//                      sum( decode( sm.active_date,
//                                   :ActiveDate, sm.vmc_credits_amount,
//                                   0 ) )                            as vmc_credits_amount,
//                      sum( decode( sm.active_date,
//                                   :ActiveDate, sm.vmc_cash_advance_count,
//                                   0 ) )                            as cash_advance_vol_count,
//                      sum( decode( sm.active_date,
//                                   :ActiveDate, sm.vmc_cash_advance_amount,
//                                   0 ) )                            as cash_advance_vol_amount,
//                      sum( decode( sm.active_date,
//                                   :ActiveDate,((sm.tot_inc_discount +
//                                                 sm.tot_inc_interchange +
//                                                 sm.tot_inc_authorization +
//                                                 sm.tot_inc_capture +
//                                                 sm.tot_inc_debit +
//                                                 sm.tot_inc_ind_plans +
//                                                 sm.tot_inc_sys_generated +
//                                                 nvl(sm.disc_ic_dce_adj_amount,0) + 
//                                                 nvl(sm.fee_dce_adj_amount,0) +
//                                                 nvl(mcrf.tot_ndr,0) +
//                                                 nvl(mcrf.tot_authorization,0) +
//                                                 nvl(mcrf.tot_capture,0) +
//                                                 nvl(mcrf.tot_debit,0) +
//                                                 nvl(mcrf.tot_ind_plans,0) +
//                                                 nvl(mcrf.tot_sys_generated,0) +
//                                                 nvl(mcrf.tot_equip_rental,0) +
//                                                 nvl(mcrf.tot_equip_sales,0) ) -
//                                                (decode( sm.interchange_expense,
//                                                         0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),
//                                                         sm.interchange_expense ) +
//                                                 sm.vmc_assessment_expense + 
//                                                 sm.tot_exp_authorization +
//                                                 sm.tot_exp_capture +
//                                                 sm.tot_exp_debit +
//                                                 sm.tot_exp_ind_plans +
//                                                 sm.tot_exp_sys_generated +
//                                                 decode(sm.bank_number, 3858, sm.equip_sales_income, 0)) ),
//                                   0 ) )                            as revenue,
//                      sum( sm.vmc_sales_count )                     as vmc_sales_count_ytd,
//                      sum( sm.vmc_sales_amount )                    as vmc_sales_amount_ytd,
//                      sum( sm.vmc_credits_count )                   as vmc_credits_count_ytd,
//                      sum( sm.vmc_credits_amount )                  as vmc_credits_amount_ytd,
//                      sum( sm.vmc_cash_advance_count )              as cash_advance_vol_count_ytd,
//                      sum( sm.vmc_cash_advance_amount )             as cash_advance_vol_amount_ytd,  
//                      sum( ((sm.tot_inc_discount +
//                             sm.tot_inc_interchange +
//                             sm.tot_inc_authorization +
//                             sm.tot_inc_capture +
//                             sm.tot_inc_debit +
//                             sm.tot_inc_ind_plans +
//                             sm.tot_inc_sys_generated +
//                             nvl(sm.disc_ic_dce_adj_amount,0) + 
//                             nvl(sm.fee_dce_adj_amount,0) +
//                             nvl(mcrf.tot_ndr,0) +
//                             nvl(mcrf.tot_authorization,0) +
//                             nvl(mcrf.tot_capture,0) +
//                             nvl(mcrf.tot_debit,0) +
//                             nvl(mcrf.tot_ind_plans,0) +
//                             nvl(mcrf.tot_sys_generated,0) +
//                             nvl(mcrf.tot_equip_rental,0) +
//                             nvl(mcrf.tot_equip_sales,0) ) -
//                             (decode( sm.interchange_expense,
//                                      0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),
//                                      sm.interchange_expense ) +
//                             sm.vmc_assessment_expense + 
//                             sm.tot_exp_authorization +
//                             sm.tot_exp_capture +
//                             sm.tot_exp_debit +
//                             sm.tot_exp_ind_plans +
//                             sm.tot_exp_sys_generated +
//                             decode(sm.bank_number, 3858, sm.equip_sales_income, 0))) )  as revenue_ytd
//            from      t_hierarchy                 th,
//                      t_hierarchy                 thc,
//                      monthly_extract_summary     sm,
//                      mif                         mf,
//                      monthly_extract_contract    mcli,
//                      monthly_extract_contract    mcrf,
//                      monthly_extract_contract    mcrs,
//                      monthly_extract_contract    mcex,
//                      organization                o
//            where     th.hier_type = 1 and
//                      th.ancestor = :nodeId and
//                      thc.hier_type = 1 and
//                      th.relation = (1 - :assoc) and
//                      thc.ancestor = th.descendent and
//                      thc.entity_type = 4 and                    
//                      sm.assoc_hierarchy_node = thc.descendent and
//                      sm.active_date between trunc(:ActiveDate,'year') and trunc( (trunc(:ActiveDate,'year')+345), 'month' ) and
//                      mf.merchant_number = sm.merchant_number and
//                      --not mf.activation_date is null and
//                      (
//                        (mf.bank_number = 3858 and mif_date_opened(mf.date_opened) between :beginDate and last_day(:endDate) ) or
//                        (mf.bank_number != 3858 and mf.activation_date is not null and mf.activation_date between :beginDate and last_day(:endDate) )
//                      ) and                  
//                      --mf.activation_date between :beginDate and last_day(:endDate) and                  
//                      mcli.hh_load_sec(+) = sm.hh_load_sec and
//                      mcli.merchant_number(+) = sm.merchant_number and
//                      mcli.contract_type(+) = 1 and
//                      mcrf.hh_load_sec(+) = sm.hh_load_sec and
//                      mcrf.merchant_number(+) = sm.merchant_number and
//                      mcrf.contract_type(+) = 2 and
//                      mcrs.hh_load_sec(+) = sm.hh_load_sec and
//                      mcrs.merchant_number(+) = sm.merchant_number and
//                      mcrs.contract_type(+) = 3 and
//                      mcex.hh_load_sec(+) = sm.hh_load_sec and
//                      mcex.merchant_number(+) = sm.merchant_number and
//                      mcex.contract_type(+) = 4 and
//                      o.org_group = th.descendent
//            group by  o.org_group, o.org_name
//            order by o.org_name, o.org_group
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    o.org_group                                   as node_id,\n                    o.org_name                                    as org_name,\n                    0                                             as is_merchant,\n                    sum( decode( sm.active_date,\n                                  :1 , sm.vmc_sales_count,\n                                 0 ) )                            as vmc_sales_count,\n                    sum( decode( sm.active_date,\n                                  :2 , sm.vmc_sales_amount,\n                                 0 ) )                            as vmc_sales_amount,\n                    sum( decode( sm.active_date,\n                                  :3 , sm.vmc_credits_count,\n                                 0 ) )                            as vmc_credits_count,\n                    sum( decode( sm.active_date,\n                                  :4 , sm.vmc_credits_amount,\n                                 0 ) )                            as vmc_credits_amount,\n                    sum( decode( sm.active_date,\n                                  :5 , sm.vmc_cash_advance_count,\n                                 0 ) )                            as cash_advance_vol_count,\n                    sum( decode( sm.active_date,\n                                  :6 , sm.vmc_cash_advance_amount,\n                                 0 ) )                            as cash_advance_vol_amount,\n                    sum( decode( sm.active_date,\n                                  :7 ,((sm.tot_inc_discount +\n                                               sm.tot_inc_interchange +\n                                               sm.tot_inc_authorization +\n                                               sm.tot_inc_capture +\n                                               sm.tot_inc_debit +\n                                               sm.tot_inc_ind_plans +\n                                               sm.tot_inc_sys_generated +\n                                               nvl(sm.disc_ic_dce_adj_amount,0) + \n                                               nvl(sm.fee_dce_adj_amount,0) +\n                                               nvl(mcrf.tot_ndr,0) +\n                                               nvl(mcrf.tot_authorization,0) +\n                                               nvl(mcrf.tot_capture,0) +\n                                               nvl(mcrf.tot_debit,0) +\n                                               nvl(mcrf.tot_ind_plans,0) +\n                                               nvl(mcrf.tot_sys_generated,0) +\n                                               nvl(mcrf.tot_equip_rental,0) +\n                                               nvl(mcrf.tot_equip_sales,0) ) -\n                                              (decode( sm.interchange_expense,\n                                                       0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),\n                                                       sm.interchange_expense ) +\n                                               sm.vmc_assessment_expense + \n                                               sm.tot_exp_authorization +\n                                               sm.tot_exp_capture +\n                                               sm.tot_exp_debit +\n                                               sm.tot_exp_ind_plans +\n                                               sm.tot_exp_sys_generated +\n                                               decode(sm.bank_number, 3858, sm.equip_sales_income, 0)) ),\n                                 0 ) )                            as revenue,\n                    sum( sm.vmc_sales_count )                     as vmc_sales_count_ytd,\n                    sum( sm.vmc_sales_amount )                    as vmc_sales_amount_ytd,\n                    sum( sm.vmc_credits_count )                   as vmc_credits_count_ytd,\n                    sum( sm.vmc_credits_amount )                  as vmc_credits_amount_ytd,\n                    sum( sm.vmc_cash_advance_count )              as cash_advance_vol_count_ytd,\n                    sum( sm.vmc_cash_advance_amount )             as cash_advance_vol_amount_ytd,  \n                    sum( ((sm.tot_inc_discount +\n                           sm.tot_inc_interchange +\n                           sm.tot_inc_authorization +\n                           sm.tot_inc_capture +\n                           sm.tot_inc_debit +\n                           sm.tot_inc_ind_plans +\n                           sm.tot_inc_sys_generated +\n                           nvl(sm.disc_ic_dce_adj_amount,0) + \n                           nvl(sm.fee_dce_adj_amount,0) +\n                           nvl(mcrf.tot_ndr,0) +\n                           nvl(mcrf.tot_authorization,0) +\n                           nvl(mcrf.tot_capture,0) +\n                           nvl(mcrf.tot_debit,0) +\n                           nvl(mcrf.tot_ind_plans,0) +\n                           nvl(mcrf.tot_sys_generated,0) +\n                           nvl(mcrf.tot_equip_rental,0) +\n                           nvl(mcrf.tot_equip_sales,0) ) -\n                           (decode( sm.interchange_expense,\n                                    0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),\n                                    sm.interchange_expense ) +\n                           sm.vmc_assessment_expense + \n                           sm.tot_exp_authorization +\n                           sm.tot_exp_capture +\n                           sm.tot_exp_debit +\n                           sm.tot_exp_ind_plans +\n                           sm.tot_exp_sys_generated +\n                           decode(sm.bank_number, 3858, sm.equip_sales_income, 0))) )  as revenue_ytd\n          from      t_hierarchy                 th,\n                    t_hierarchy                 thc,\n                    monthly_extract_summary     sm,\n                    mif                         mf,\n                    monthly_extract_contract    mcli,\n                    monthly_extract_contract    mcrf,\n                    monthly_extract_contract    mcrs,\n                    monthly_extract_contract    mcex,\n                    organization                o\n          where     th.hier_type = 1 and\n                    th.ancestor =  :8  and\n                    thc.hier_type = 1 and\n                    th.relation = (1 -  :9 ) and\n                    thc.ancestor = th.descendent and\n                    thc.entity_type = 4 and                    \n                    sm.assoc_hierarchy_node = thc.descendent and\n                    sm.active_date between trunc( :10 ,'year') and trunc( (trunc( :11 ,'year')+345), 'month' ) and\n                    mf.merchant_number = sm.merchant_number and\n                    --not mf.activation_date is null and\n                    (\n                      (mf.bank_number = 3858 and mif_date_opened(mf.date_opened) between  :12  and last_day( :13 ) ) or\n                      (mf.bank_number != 3858 and mf.activation_date is not null and mf.activation_date between  :14  and last_day( :15 ) )\n                    ) and                  \n                    --mf.activation_date between :beginDate and last_day(:endDate) and                  \n                    mcli.hh_load_sec(+) = sm.hh_load_sec and\n                    mcli.merchant_number(+) = sm.merchant_number and\n                    mcli.contract_type(+) = 1 and\n                    mcrf.hh_load_sec(+) = sm.hh_load_sec and\n                    mcrf.merchant_number(+) = sm.merchant_number and\n                    mcrf.contract_type(+) = 2 and\n                    mcrs.hh_load_sec(+) = sm.hh_load_sec and\n                    mcrs.merchant_number(+) = sm.merchant_number and\n                    mcrs.contract_type(+) = 3 and\n                    mcex.hh_load_sec(+) = sm.hh_load_sec and\n                    mcex.merchant_number(+) = sm.merchant_number and\n                    mcex.contract_type(+) = 4 and\n                    o.org_group = th.descendent\n          group by  o.org_group, o.org_name\n          order by o.org_name, o.org_group";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.NewAccountRevenueDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,ActiveDate);
   __sJT_st.setDate(2,ActiveDate);
   __sJT_st.setDate(3,ActiveDate);
   __sJT_st.setDate(4,ActiveDate);
   __sJT_st.setDate(5,ActiveDate);
   __sJT_st.setDate(6,ActiveDate);
   __sJT_st.setDate(7,ActiveDate);
   __sJT_st.setLong(8,nodeId);
   __sJT_st.setInt(9,assoc);
   __sJT_st.setDate(10,ActiveDate);
   __sJT_st.setDate(11,ActiveDate);
   __sJT_st.setDate(12,beginDate);
   __sJT_st.setDate(13,endDate);
   __sJT_st.setDate(14,beginDate);
   __sJT_st.setDate(15,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.NewAccountRevenueDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:513^9*/
      }
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    Calendar    cal           = Calendar.getInstance();
  
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.MONTH, -1 );
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd( ReportDateBegin );
    }    
    else
    {
      cal.setTime(ReportDateBegin);
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }      
    
    // set the month-end activity date
    try
    {
      // set from date
      cal.set( Calendar.MONTH,        Integer.parseInt(request.getParameter("activeMonth")));
      cal.set( Calendar.DAY_OF_MONTH, Integer.parseInt(request.getParameter("activeDay")) );
      cal.set( Calendar.YEAR,         Integer.parseInt(request.getParameter("activeYear")));
      ActiveDate = new java.sql.Date( cal.getTime().getTime() );
    }
    catch (Exception e)
    {
      cal = Calendar.getInstance();
      cal.add(Calendar.MONTH,-1);
      cal.set(Calendar.DAY_OF_MONTH,1);
      ActiveDate = new java.sql.Date( cal.getTime().getTime() );
    }
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/