/*@lineinfo:filename=RiskMerchHistoryBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/RiskMerchHistoryBase.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 2/03/04 4:37p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class RiskMerchHistoryBase extends ReportSQLJBean
{
  public class MerchantData 
  {                                         
    public double         ChargebackAmountYTD   = 0.0;
    public double         CreditsAmountYTD      = 0.0;
    public Date           DateOpened            = null;
    public String         DbaName               = null;
    public String         LegalName             = null;
    public long           MerchantId            = 0L;
    public double         SalesAmountYTD        = 0.0;
    public int            SicCode               = 0;
    
    public MerchantData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      MerchantId        = resultSet.getLong("merchant_number");
      DbaName           = resultSet.getString("dba_name");
      DateOpened        = resultSet.getDate("date_opened");
      LegalName         = resultSet.getString("legal_name");
      SicCode           = resultSet.getInt("sic_code");
      
      // load the YTD numbers
      loadYTDData(this);
    }
  }
  
  protected MerchantData            MerchData     = null;
  
  public RiskMerchHistoryBase( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Merchant Id\",");
    line.append( encodeHierarchyNode(MerchData.MerchantId) );
    line.append(",,\"$ Sales YTD\",");
    line.append( MerchData.SalesAmountYTD );
    line.append(",,\"MCC\",");
    line.append( MerchData.SicCode );
    line.append(",,,,,,,\n");
    
    line.append("\"DBA Name\",\"");
    line.append( MerchData.DbaName );
    line.append("\",,\"$ Credits YTD\",");
    line.append( MerchData.CreditsAmountYTD );
    line.append(",,\"Date Opened\",");
    line.append( DateTimeFormatter.getFormattedDate(MerchData.DateOpened,"MM/dd/yyyy") );
    line.append(",,,,,,,\n");
    
    line.append("\"Legal Name\",\"");
    line.append( MerchData.LegalName );
    line.append("\",,\"$ CB YTD\",");
    line.append( MerchData.ChargebackAmountYTD );
    line.append(",,\"# of Outlets\",");
    line.append(",");     // insert # outlets here
    line.append(",,,,,,,\n");
    line.append("\n");
  }
  
  public MerchantData getMerchantData( )
  {
    return(MerchData);
  }
  
  protected void loadYTDData( MerchantData merchData )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:122^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(sm.visa_sales_amount +
//                      sm.mc_sales_amount)         as sales_amount,
//                  sum(sm.visa_credits_amount +
//                      sm.mc_credits_amount)       as credits_amount      
//          from    daily_detail_file_summary   sm
//          where   sm.merchant_number = :merchData.MerchantId and
//                  sm.batch_date >= (sysdate-365)        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum(sm.visa_sales_amount +\n                    sm.mc_sales_amount)         as sales_amount,\n                sum(sm.visa_credits_amount +\n                    sm.mc_credits_amount)       as credits_amount      \n        from    daily_detail_file_summary   sm\n        where   sm.merchant_number =  :1  and\n                sm.batch_date >= (sysdate-365)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.RiskMerchHistoryBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchData.MerchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.RiskMerchHistoryBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:131^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        merchData.CreditsAmountYTD  = resultSet.getDouble("credits_amount");
        merchData.SalesAmountYTD    = resultSet.getDouble("sales_amount");
      }        
      resultSet.close();
      it.close();
      
      // load the chargeback YTD
      /*@lineinfo:generated-code*//*@lineinfo:143^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(cb.tran_amount)             as cb_amount
//          from    network_chargebacks   cb
//          where   cb.merchant_number = :merchData.MerchantId and
//                  cb.incoming_date >= (sysdate-365) and
//                  cb.first_time_chargeback = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum(cb.tran_amount)             as cb_amount\n        from    network_chargebacks   cb\n        where   cb.merchant_number =  :1  and\n                cb.incoming_date >= (sysdate-365) and\n                cb.first_time_chargeback = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.RiskMerchHistoryBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchData.MerchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.RiskMerchHistoryBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:150^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        merchData.ChargebackAmountYTD = resultSet.getDouble("cb_amount");
      }        
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadYTDData()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/