/*@lineinfo:filename=TransmissionReconcileBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/TransmissionReconcileBean.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 9/11/02 4:13p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.NumberFormatter;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class TransmissionReconcileBean extends ReportSQLJBean
{
  public static final int     FILE_TYPE_ACH     = 0;
  public static final int     FILE_TYPE_DDF     = 1;

  public static final int     MAX_FILE_TYPES    = 2;

  public int            fileType                = FILE_TYPE_ACH;

  public String[]       fileTypeDescriptions =
                        {
                          "ACH (Deposits) File",
                          "DDF (Transactions) File"
                        };

  public long           bankNumber              = 0;
  public double         fileNet                 = 0.0;

  private Vector        mifMismatches           = new Vector();
  private Vector        hierarchyMismatches     = new Vector();
  
  public Vector getMifMismatches()
  {
    return this.mifMismatches;
  }
  public Vector getHierarchyMismatches()
  {
    return this.hierarchyMismatches;
  }

  public void setProperties(HttpServletRequest request)
  {
    super.setProperties(request);

    // set the file type from download
    setFileType(HttpHelper.getString(request, "fileType"));

    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();

      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, -1 );

      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }

    setFileType(HttpHelper.getString(request, "fileType"));
  }

  public void setBankNumber(UserBean user)
  {
    try
    {
      long node = user.getHierarchyNode();

      // default to MES if its at the super node
      if(node == 9999999999L)
      {
        this.bankNumber = 3941L;
      }
      else if(node % 100000L == 0L)
      {
        // bank node
        this.bankNumber = node / 100000L;
      }
      else
      {
        // not a bank user or MES user so give them nothing
        this.bankNumber = 0L;
      }
    }
    catch(Exception e)
    {
      logEntry("setBanknumber(" + user.getHierarchyNode() + ")", e.toString());
    }
  }

  public void getFileTotal(Date beginDate)
  {
    ResultSetIterator   it      = null;

    try
    {
      if(fileType == FILE_TYPE_ACH)
      {
        // ** note that the debit/credits appear backwards.  
        //    this is because the query is from the MES perspective
        //    when all the other reports view the ACHs from the
        //    merchant account perspective.
        /*@lineinfo:generated-code*//*@lineinfo:136^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(ad.transaction_code)        as count,
//                    ad.post_date_option               as activity_date,
//                    sum(
//                        ad.amount_of_transaction *
//                        decode(tc.debit_credit_indicator,'D',0,1)
//                        )                             as debits,
//                    sum(ad.amount_of_transaction *
//                        decode(tc.debit_credit_indicator,'D',1,0)
//                       )                              as credits,
//                    sum(ad.amount_of_transaction *
//                        decode(tc.debit_credit_indicator,'D',1,0)
//                       )                              as net,
//                    sum(ad.amount_of_transaction)     as gross
//            from    ach_detail            ad,
//                    ach_detail_tran_code  tc
//            where   ad.post_date_option = :beginDate and
//                    ad.load_filename like 'ach'||:bankNumber||'_%' and
//                    tc.ach_detail_trans_code(+) = ad.transaction_code               
//            group by ad.post_date_option
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(ad.transaction_code)        as count,\n                  ad.post_date_option               as activity_date,\n                  sum(\n                      ad.amount_of_transaction *\n                      decode(tc.debit_credit_indicator,'D',0,1)\n                      )                             as debits,\n                  sum(ad.amount_of_transaction *\n                      decode(tc.debit_credit_indicator,'D',1,0)\n                     )                              as credits,\n                  sum(ad.amount_of_transaction *\n                      decode(tc.debit_credit_indicator,'D',1,0)\n                     )                              as net,\n                  sum(ad.amount_of_transaction)     as gross\n          from    ach_detail            ad,\n                  ach_detail_tran_code  tc\n          where   ad.post_date_option =  :1  and\n                  ad.load_filename like 'ach'|| :2 ||'_%' and\n                  tc.ach_detail_trans_code(+) = ad.transaction_code               \n          group by ad.post_date_option";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.TransmissionReconcileBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setLong(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.TransmissionReconcileBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:157^9*/
      }
      else if(fileType == FILE_TYPE_DDF)
      {
        /** disabled until we can figure out a better way to handle ddf data
        #sql [Ctx] it =
        {
          select  count(dt.transaction_amount)      count,
                  dt.batch_date                     activity_date,
                  sum(dt.transaction_amount *
                    decode(dt.debit_credit_indicator,'D',1,'C',0,1))  debits,
                  sum(dt.transaction_amount *
                    decode(dt.debit_credit_indicator,'D',0,'C',1,0))  credits,
                  sum(dt.transaction_amount *
                    decode(dt.debit_credit_indicator,'D',1,'C',-1,1)) net,
                  sum(dt.transaction_amount)        gross
          from    daily_detail_file_dt    dt
          where   dt.batch_date = :beginDate and
                  dt.load_filename like 'ddf'||:bankNumber||'_%'
          group by dt.batch_date
        };
        **/
      }

      ResultSet rs = it.getResultSet();
      if(rs.next())
      {
        ReportRows.addElement(new FileTotalRow("ACH File Totals", rs));
        fileNet = rs.getDouble("net");
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getFileTotal(" + bankNumber + ")", e.toString());
    }
  }

  public void getMifTotal(Date beginDate)
  {
    ResultSetIterator   it      = null;

    try
    {
      if(fileType == FILE_TYPE_ACH)
      {
        // ** note that the debit/credits appear backwards.  
        //    this is because the query is from the MES perspective
        //    when all the other reports view the ACHs from the
        //    merchant account perspective.
        /*@lineinfo:generated-code*//*@lineinfo:208^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(ad.transaction_code)                      as count,
//                    ad.post_date_option                             as activity_date,
//                    sum(ad.amount_of_transaction *
//                        decode(tc.debit_credit_indicator,'D',0,1))  as debits,
//                    sum(ad.amount_of_transaction *
//                        decode(tc.debit_credit_indicator,'D',1,0))  as credits,
//                    sum(ad.amount_of_transaction *
//                        decode(tc.debit_credit_indicator,'D',-1,1)) as net,
//                    sum(ad.amount_of_transaction)                   as gross
//            from    ach_detail            ad,
//                    mif                   m,
//                    ach_detail_tran_code  tc
//            where   ad.post_date_option = :beginDate and
//                    ad.merchant_number = m.merchant_number and
//                    m.bank_number = :bankNumber and
//                    tc.ach_detail_trans_code(+) = ad.transaction_code               
//            group by ad.post_date_option
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(ad.transaction_code)                      as count,\n                  ad.post_date_option                             as activity_date,\n                  sum(ad.amount_of_transaction *\n                      decode(tc.debit_credit_indicator,'D',0,1))  as debits,\n                  sum(ad.amount_of_transaction *\n                      decode(tc.debit_credit_indicator,'D',1,0))  as credits,\n                  sum(ad.amount_of_transaction *\n                      decode(tc.debit_credit_indicator,'D',-1,1)) as net,\n                  sum(ad.amount_of_transaction)                   as gross\n          from    ach_detail            ad,\n                  mif                   m,\n                  ach_detail_tran_code  tc\n          where   ad.post_date_option =  :1  and\n                  ad.merchant_number = m.merchant_number and\n                  m.bank_number =  :2  and\n                  tc.ach_detail_trans_code(+) = ad.transaction_code               \n          group by ad.post_date_option";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.TransmissionReconcileBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setLong(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.TransmissionReconcileBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:227^9*/
      }
      else if(fileType == FILE_TYPE_DDF)
      {
        /** disabled until we can figure out a better way to handle ddf data
        #sql [Ctx] it =
        {
          select  count(dt.transaction_amount)      count,
                  dt.batch_date                     activity_date,
                  sum(dt.transaction_amount *
                    decode(dt.debit_credit_indicator,'D',1,'C',0,1))  debits,
                  sum(dt.transaction_amount *
                    decode(dt.debit_credit_indicator,'D',0,'C',1,0))  credits,
                  sum(dt.transaction_amount *
                    decode(dt.debit_credit_indicator,'D',1,'C',-1,1)) net,
                  sum(dt.transaction_amount)        gross
          from    daily_detail_file_dt    dt,
                  mif m
          where   dt.batch_date = :beginDate and
                  dt.merchant_account_number = m.merchant_number and
                  m.bank_number = :bankNumber
          group by dt.batch_date
        };
        **/
      }

      ResultSet rs = it.getResultSet();
      while(rs.next())
      {
        ReportRows.addElement(new FileTotalRow("MIF Totals", rs));
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getMifTotal(" + bankNumber + ")", e.toString());
    }
  }

  public void getHierarchyTotal(Date beginDate)
  {
    ResultSetIterator   it      = null;

    try
    {
      if(fileType == FILE_TYPE_ACH)
      {
        // ** note that the debit/credits appear backwards.  
        //    this is because the query is from the MES perspective
        //    when all the other reports view the ACHs from the
        //    merchant account perspective.
        /*@lineinfo:generated-code*//*@lineinfo:279^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(ad.transaction_code)                      as count,
//                    ad.post_date_option                             as activity_date,
//                    sum(ad.amount_of_transaction * 
//                        decode(tc.debit_credit_indicator,'D',0,1))  as debits,
//                    sum(ad.amount_of_transaction * 
//                        decode(tc.debit_credit_indicator,'D',1,0))  as credits,
//                    sum(ad.amount_of_transaction * 
//                        decode(tc.debit_credit_indicator,'D',-1,1)) as net,
//                    sum(ad.amount_of_transaction)                   as gross
//            from    ach_detail            ad,
//                    group_merchant        gm,
//                    organization          o,
//                    ach_detail_tran_code  tc
//            where   ad.post_date_option = :beginDate and
//                    ad.merchant_number = gm.merchant_number and
//                    gm.org_num = o.org_num and
//                    o.org_group = (to_number(:bankNumber) * 100000) and
//                    tc.ach_detail_trans_code(+) = ad.transaction_code
//            group by ad.post_date_option
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(ad.transaction_code)                      as count,\n                  ad.post_date_option                             as activity_date,\n                  sum(ad.amount_of_transaction * \n                      decode(tc.debit_credit_indicator,'D',0,1))  as debits,\n                  sum(ad.amount_of_transaction * \n                      decode(tc.debit_credit_indicator,'D',1,0))  as credits,\n                  sum(ad.amount_of_transaction * \n                      decode(tc.debit_credit_indicator,'D',-1,1)) as net,\n                  sum(ad.amount_of_transaction)                   as gross\n          from    ach_detail            ad,\n                  group_merchant        gm,\n                  organization          o,\n                  ach_detail_tran_code  tc\n          where   ad.post_date_option =  :1  and\n                  ad.merchant_number = gm.merchant_number and\n                  gm.org_num = o.org_num and\n                  o.org_group = (to_number( :2 ) * 100000) and\n                  tc.ach_detail_trans_code(+) = ad.transaction_code\n          group by ad.post_date_option";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.TransmissionReconcileBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setLong(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.TransmissionReconcileBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:300^9*/
      }
      else if(fileType == FILE_TYPE_DDF)
      {
        /** disabled until we can figure out a better way to handle ddf data
        #sql [Ctx] it =
        {
          select  /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */
/*                  count(dt.transaction_amount)      count,
                  dt.batch_date                     activity_date,
                  sum(dt.transaction_amount *
                    decode(dt.debit_credit_indicator,'D',1,'C',0,1))  debits,
                  sum(dt.transaction_amount *
                    decode(dt.debit_credit_indicator,'D',0,'C',1,0))  credits,
                  sum(dt.transaction_amount *
                    decode(dt.debit_credit_indicator,'D',1,'C',-1,1)) net,
                  sum(dt.transaction_amount)        gross
          from    daily_detail_file_dt    dt,
                  group_merchant          gm,
                  organization            o
          where   dt.batch_date = :beginDate and
                  dt.merchant_account_number = gm.merchant_number and
                  gm.org_num = o.org_num and
                  o.org_group = (to_number(:bankNumber) * 100000)
          group by dt.batch_date
        };
**/
      }

      ResultSet rs = it.getResultSet();
      while(rs.next())
      {
        ReportRows.addElement(new FileTotalRow("Deposit Summary Totals", rs));
        ReportRows.addElement(new FileTotalRow("Hierarchy Totals", rs));
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getHierarchyTotal(" + bankNumber + ")", e.toString());
    }
  }
  
  public void getMifMismatches(Date beginDate)
  {
    ResultSetIterator   it      = null;
    
    try
    {
      if(fileType == FILE_TYPE_ACH)
      {
        /*@lineinfo:generated-code*//*@lineinfo:352^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ach.amount_of_transaction       tran_amount,
//                    ach.post_date_option            tran_date,        
//                    ach.merchant_number             merchant_number,
//                    ach.individual_name             merchant_name
//            from    ach_detail      ach,
//                    mif             m
//            where   ach.post_date_option    = :beginDate and
//                    ach.load_filename like 'ach'||:bankNumber||'_%' and
//                    ach.merchant_number     = m.merchant_number(+) and
//                    m.merchant_number is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ach.amount_of_transaction       tran_amount,\n                  ach.post_date_option            tran_date,        \n                  ach.merchant_number             merchant_number,\n                  ach.individual_name             merchant_name\n          from    ach_detail      ach,\n                  mif             m\n          where   ach.post_date_option    =  :1  and\n                  ach.load_filename like 'ach'|| :2 ||'_%' and\n                  ach.merchant_number     = m.merchant_number(+) and\n                  m.merchant_number is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.TransmissionReconcileBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setLong(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.TransmissionReconcileBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:364^9*/
      }
      else if(fileType == FILE_TYPE_DDF)
      {
        /** disabled until we can figure out a better way to handle ddf data
        #sql [Ctx] it =
        {
          select  dt.transaction_amount       tran_amount,
                  dt.batch_date               tran_date,
                  dt.merchant_account_number  merchant_number,
                  dt.merchant_name            merchant_name
          from    daily_detail_file_dt        dt,
                  mif                         m
          where   dt.batch_date               = :beginDate and
                  dt.load_filename like 'ddf'||:bankNumber||'_%' and
                  dt.merchant_account_number  = m.merchant_number(+) and
                  m.merchant_number is null
        };
        **/
      }
      
      ResultSet rs  = it.getResultSet();
      
      while(rs.next())
      {
        // add to mifMismatches vector
        mifMismatches.addElement(new FileMismatchRow(rs));
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getMifMismatches(" + bankNumber + ")", e.toString());
    }
  }
  
  public void getHierarchyMismatches(Date beginDate)
  {
    ResultSetIterator   it      = null;
    int                 orgNum  = 0;
    
    try
    {
      // get the org num for this bank number first
      /*@lineinfo:generated-code*//*@lineinfo:410^7*/

//  ************************************************************
//  #sql [Ctx] { select  org_num
//          
//          from    organization
//          where   org_group = (:bankNumber * 100000)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  org_num\n         \n        from    organization\n        where   org_group = ( :1  * 100000)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.TransmissionReconcileBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,bankNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orgNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:416^7*/
      
      if(fileType == FILE_TYPE_ACH)
      {
        /*@lineinfo:generated-code*//*@lineinfo:420^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ach.amount_of_transaction       tran_amount,
//                    ach.post_date_option            tran_date,        
//                    ach.merchant_number             merchant_number,
//                    ach.individual_name             merchant_name
//            from    ach_detail      ach,
//                    mif             m,
//                    group_merchant  gm
//            where   ach.merchant_number     = m.merchant_number and
//                    m.bank_number           = :bankNumber and
//                    ach.post_date_option    = :beginDate and
//                    ach.merchant_number     = gm.merchant_number(+) and
//                    :orgNum                 = gm.org_num(+) and
//                    gm.merchant_number is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ach.amount_of_transaction       tran_amount,\n                  ach.post_date_option            tran_date,        \n                  ach.merchant_number             merchant_number,\n                  ach.individual_name             merchant_name\n          from    ach_detail      ach,\n                  mif             m,\n                  group_merchant  gm\n          where   ach.merchant_number     = m.merchant_number and\n                  m.bank_number           =  :1  and\n                  ach.post_date_option    =  :2  and\n                  ach.merchant_number     = gm.merchant_number(+) and\n                   :3                  = gm.org_num(+) and\n                  gm.merchant_number is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.TransmissionReconcileBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,bankNumber);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setInt(3,orgNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.TransmissionReconcileBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:435^9*/
      }
      
      ResultSet rs  = it.getResultSet();
      
      while(rs.next())
      {
        // add to hierarchyMismatches vector
        hierarchyMismatches.addElement(new FileMismatchRow(rs));
      }
      
      rs.close();
      it.close();
      
    }
    catch(Exception e)
    {
      logEntry("getHierarchyMismatches(" + bankNumber + ")", e.toString());
    }
  }

  public void loadData(long orgId, Date beginDate, Date endDate)
  {
    try
    {
      //@connect();

      // empty the current contents of the report rows vector
      ReportRows.clear();

      //*****
      // Build summary totals
      //*****
      // get the file totals
      getFileTotal(beginDate);

      // get Hierarchy Totals
      getHierarchyTotal(beginDate);
      
      // get mif totals
      getMifTotal(beginDate);

      //*****
      // Build mismatch details
      //*****
      getMifMismatches(beginDate);
      
      getHierarchyMismatches(beginDate);
    }
    catch(Exception e)
    {
      logEntry("loadData(" + orgId + ", " + beginDate + ", " + endDate + ")", e.toString());
    }
    finally
    {
      //@cleanUp();
    }
  }

  public void setFileType(String fileType)
  {
    try
    {
      int   value = Integer.parseInt(fileType);

      if(value < FILE_TYPE_ACH || value >= MAX_FILE_TYPES)
      {
        this.fileType = FILE_TYPE_ACH;
      }
      else
      {
        this.fileType = value;
      }
    }
    catch(Exception e)
    {
      this.fileType = FILE_TYPE_ACH;
    }
  }

  public int getFileType()
  {
    return this.fileType;
  }

  public String getFileTypeDescription()
  {
    return(getFileTypeDescription(this.fileType));
  }

  public String getFileTypeDescription(int idx)
  {
    String result = "Unknown";

    try
    {
      result = fileTypeDescriptions[idx];
    }
    catch(Exception e)
    {
      logEntry("getFileTypeDescription(" + idx + ")", e.toString());
    }

    return result;
  }

  public long getBankNumber()
  {
    return this.bankNumber;
  }

  public class FileTotalRow
  {
    public String     method        = "";
    public String     activityDate  = "";
    public String     count         = "";
    public String     debits        = "";
    public String     credits       = "";
    public String     net           = "";
    public String     gross         = "";
    public double     netAmt        = 0.0;

    public FileTotalRow(String method, ResultSet rs)
    {
      try
      {
        this.method = method;

        this.count        = rs.getString("count");
        this.activityDate = DateTimeFormatter.getFormattedDate(rs.getDate("activity_date"), "MM/dd/yy");
        this.debits       = NumberFormatter.getDoubleString(rs.getDouble("debits"), NumberFormatter.CURRENCY_FORMAT);
        this.credits      = NumberFormatter.getDoubleString(rs.getDouble("credits"), NumberFormatter.CURRENCY_FORMAT);
        this.net          = NumberFormatter.getDoubleString(rs.getDouble("net"), NumberFormatter.CURRENCY_FORMAT);
        this.gross        = NumberFormatter.getDoubleString(rs.getDouble("gross"), NumberFormatter.CURRENCY_FORMAT);
        this.netAmt       = rs.getDouble("net");
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::constructor", e.toString());
      }
    }
  }
  
  public class FileMismatchRow
  {
    public String   amount          = "";
    public String   activityDate    = "";
    public String   merchantNumber  = "";
    public String   merchantName    = "";
    
    public FileMismatchRow(ResultSet rs)
    {
      try
      {
        this.amount         = NumberFormatter.getDoubleString(rs.getDouble("tran_amount"), NumberFormatter.CURRENCY_FORMAT);
        this.activityDate   = DateTimeFormatter.getFormattedDate(rs.getDate("tran_date"), "MM/dd/yy");
        this.merchantNumber = rs.getString("merchant_number");
        this.merchantName   = rs.getString("merchant_name");
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::constructor", e.toString());
      }
    }
  }

}/*@lineinfo:generated-code*/