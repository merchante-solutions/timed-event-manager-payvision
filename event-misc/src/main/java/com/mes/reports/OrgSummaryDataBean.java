/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/OrgSummaryDataBean.java $

  Description:


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2010-11-04 12:18:38 -0700 (Thu, 04 Nov 2010) $
  Version            : $Revision: 18071 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.support.HttpHelper;

abstract public class OrgSummaryDataBean extends ReportSQLJBean
{
  public static final int   DT_TRANSACTIONS         = 0;
  public static final int   DT_DEPOSITS             = 1;
  public static final int   DT_DOWNGRADES           = 2;
  public static final int   DT_ADJUSTMENTS          = 3;
  public static final int   DT_REJECTS              = 4;
  public static final int   DT_RECONCILE            = 5;
  public static final int   DT_STATEMENTS           = 6;
  public static final int   DT_AUTHORIZATION        = 7;
  public static final int   DT_DDF_DEPOSITS         = 8;
  public static final int   DT_EXT_TRANSACTIONS     = 9;
  public static final int   DT_DDF_RECONCILE        = 10;
  public static final int   DT_CERTEGY_CHECK        = 11;
  public static final int   DT_BANKSERV_ADJUSTMENTS = 12;
  public static final int   DT_REJECTS_MBS          = 13;
  public static final int   DT_PENDING_ACH          = 14;

  public static final int   DT_ACHP_SETTLEMENT      = 15;
  public static final int   DT_ACHP_RETURNS         = 16;


  protected int           ReportDataType        = -1;
  protected SortByType    SortBy                = null;
  protected CardTypeFunding CtFunding           = null;

  public class CardTypeFunding
  {
    boolean   amexFunding     = false;
    boolean   discoverFunding = false;
    boolean   dinersFunding   = false;
    boolean   jcbFunding      = false;
    boolean   pl1Funding      = false;
    boolean   pl2Funding      = false;
    boolean   pl3Funding      = false;
    
    public CardTypeFunding( ResultSet rs )
    {
      try
      {
        amexFunding = ( ("D").equals(rs.getString("amex_plan")) );
        discoverFunding = ( ("D").equals(rs.getString("discover_plan")) );
        dinersFunding = ( ("D").equals(rs.getString("diners_plan")) );
        jcbFunding = ( ("D").equals(rs.getString("jcb_plan")) );
        pl1Funding = ( ("D").equals(rs.getString("pl1_plan")) );
        pl2Funding = ( ("D").equals(rs.getString("pl2_plan")) );
        pl3Funding = ( ("D").equals(rs.getString("pl3_plan")) );
      }
      catch(Exception e) {}
    }
  }
  
  public OrgSummaryDataBean( boolean fieldBeanEnabled )
  {
    super(fieldBeanEnabled);
  }

  public OrgSummaryDataBean( )
  {
  }

  protected void createFields(HttpServletRequest request)
  {
    FieldGroup    fgroup;

    super.createFields(request);

    if ( isFieldBeanEnabled() )
    {
      fgroup = (FieldGroup)getField("searchFields");
      fgroup.add( new HiddenField("dataType") );
      fgroup.add( new HiddenField("com.mes.Zip2Merchant") );
    }
  }

  public boolean mesFunding( int ct )
  { 
    boolean retVal = false;
    
    try
    {
      switch( ct )
      {
        case MesTransactionSummary.CARD_TYPE_VISA:  retVal = true; break;
        case MesTransactionSummary.CARD_TYPE_MC:    retVal = true; break;
        case MesTransactionSummary.CARD_TYPE_DEBT:  retVal = true; break;
        case MesTransactionSummary.CARD_TYPE_BML:   retVal = true; break;
        case MesTransactionSummary.CARD_TYPE_AMEX:  retVal = CtFunding.amexFunding; break;
        case MesTransactionSummary.CARD_TYPE_DISC:  retVal = CtFunding.discoverFunding; break;
        case MesTransactionSummary.CARD_TYPE_DINR:  retVal = CtFunding.dinersFunding; break;
        case MesTransactionSummary.CARD_TYPE_JCB:   retVal = CtFunding.jcbFunding; break;
        case MesTransactionSummary.CARD_TYPE_PL:    retVal = CtFunding.pl1Funding; break;
        default: retVal = false; break;
      }
    }
    catch(Exception e)
    {
      logEntry("mesFunding(" + ct + ")", e.toString());
    }
    
    return( retVal );
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    // put the data type in first so that it matches the menu item URL
    buffer.append( ( ( buffer.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
    buffer.append("dataType=");
    buffer.append(getReportDataType());

    // add the standard report params (i.e. date)
    super.encodeNodeUrl(buffer,nodeId,beginDate,endDate);
  }

  protected MesOrgSummaryEntry findSummaryRow( long node )
  {
    return( findSummaryRow(node,ReportSQLJBean.DISTRICT_UNASSIGNED) );
  }

  protected MesOrgSummaryEntry findSummaryRow( long node, int district )
  {
    MesOrgSummaryEntry    retVal    = null;
    MesOrgSummaryEntry    temp      = null;

    for ( int i = 0; i < ReportRows.size(); ++i )
    {
      temp = (MesOrgSummaryEntry)ReportRows.elementAt(i);
      if( temp.getHierarchyNode() == node &&
          temp.getDistrict() == district )
      {
        retVal = temp;
        break;
      }
    }
    return( retVal );
  }

  public int getReportDataType()
  {
    return( getInt("dataType",ReportDataType) );
  }

  public SortByType getSortBy()
  {
    return( SortBy );
  }

  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    try
    {
      // empty the current contents
      ReportRows.clear();

      if ( getReportType() == RT_SUMMARY )
      {
        loadSummaryData( orgId, beginDate, endDate );
      }
      else    // RT_DETAILS
      {
        loadDetailData( orgId, beginDate, endDate );
      }
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
    }
  }

  abstract public void loadDetailData( long orgId, Date beginDate, Date endDate );
  abstract public void loadSummaryData( long orgId, Date beginDate, Date endDate );

  protected void processSummaryData( ResultSet resultSet )
    throws java.sql.SQLException
  {
    processSummaryData( resultSet, false );
  }

  protected void processSummaryData( ResultSet resultSet, boolean addToExistingRow )
    throws java.sql.SQLException
  {
    MesOrgSummaryEntry        summary     = null;

    while( resultSet.next() )
    {
      // if the result set contains additional
      // data for existing summary rows, then
      // attempt to locate the row in the vector
      // add append the current result set row
      // to the data set.  if the summary row
      // is not found in the vector, then
      if ( addToExistingRow == true )
      {
        // attempt to locate an existing row.
        summary = findSummaryRow( resultSet.getLong("hierarchy_node"),
                                  resultSet.getInt("district") );
        if ( summary != null )
        {
          summary.addData( resultSet );
          continue;     // continue processing the result set
        }
      }

      // add the row to the vector
      ReportRows.addElement( new MesOrgSummaryEntry( resultSet, SortBy ) );
    }
  }

  public void setProperties( HttpServletRequest request )
  {
    super.setProperties(request);

    ReportDataType  = HttpHelper.getInt(request,"dataType",-1);

    setSortOrder(HttpHelper.getInt(request,"sortOrder",SortByType.SB_NONE));

    if ( usingDefaultReportDates() )
    {
      Calendar          cal = Calendar.getInstance();

      cal.setTime(ReportDateBegin);

      // set default date to one day back unless special rights indicate otherwise
      if( ReportUserBean != null && !ReportUserBean.hasRight(MesUsers.RIGHT_REPORT_DEFAULT_CURR_DATE) )
      {
        cal.add(Calendar.DAY_OF_MONTH,-1);
      }

      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }
  }

  protected void setSortOrder( int sortOrder )
  {
    if ( sortOrder == SortByType.SB_NONE )
    {
      if ( ( getReportDataType() == DT_STATEMENTS ) &&
           ( getReportType()     == RT_DETAILS ) )
      {
        sortOrder = MesOrgSummaryEntry.SB_ENTRY_DATE;
      }
      else
      {
        sortOrder = MesOrgSummaryEntry.SB_NAME;
      }
    }
    SortBy = new SortByType( sortOrder  );
  }

  public void showData( java.io.PrintStream out )
  {
  }
}