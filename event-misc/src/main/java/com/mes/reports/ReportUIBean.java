/*@lineinfo:filename=ReportUIBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ReportUIBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/17/01 5:43p $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import com.mes.database.SQLJConnectionBase;
import com.mes.user.UserAppType;
import sqlj.runtime.ResultSetIterator;

public class ReportUIBean extends SQLJConnectionBase
{
  protected String                        SecureHeaderFilename    = null;
  protected String                        SecureFooterFilename    = null;
  private   int                           width                   = 0;
  private   int                           height                  = 0;
  private   String                        gif                     = "";

  //private static final int                RP_SECURE_HEADER        = 1;
  
  public ReportUIBean( )
  {
  }
 
  public void loadOrgUIProfile(int orgNum)
  {
    UserAppType       uat             = new UserAppType();
    long              hierarchyNode   = 0L;
    ResultSet         rs              = null;
    ResultSetIterator it              = null;

    try
    {
      if(uat.orgExists(orgNum))
      {
        connect();
        
        // get closest parent's hierarchy node
        hierarchyNode = uat.getClosestParent(orgNum, "org_gif2");
        
        // get data from org_gif2
        /*@lineinfo:generated-code*//*@lineinfo:65^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  template_file,
//                    footer_file,
//                    gif_name,
//                    width,
//                    height
//            from    org_gif2
//            where   hierarchy_node = :hierarchyNode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  template_file,\n                  footer_file,\n                  gif_name,\n                  width,\n                  height\n          from    org_gif2\n          where   hierarchy_node =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ReportUIBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ReportUIBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:74^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          SecureHeaderFilename = rs.getString("template_file"); 
          SecureFooterFilename = rs.getString("footer_file");
          gif                  = rs.getString("gif_name");
          width                = rs.getInt("width");
          height               = rs.getInt("height");
        }
        
        rs.close();
      }
      else
      {
        // org did not exist
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::loadOrgUIProfile()", orgNum + " not found");
      }
    }
    catch(Exception e)
    {                                                              
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "loadOrgUIProfile: " + e.toString());
    }
    finally
    {
      if(it != null)
      {
        try{it.close();}catch(Exception e){}
      }
      
      cleanUp();
    }
  }
  
  public String getSecureHeaderFilename( )
  {
    return( SecureHeaderFilename );
  }
  
  public String getSecureFooterFilename()
  {
    return( SecureFooterFilename );
  }
  
  public void setOrgId( String orgIdString )
  {
    try
    {
      loadOrgUIProfile( Integer.parseInt( orgIdString ) );
    }
    catch(Exception e)
    {
    }
  }

  public String getGifFileName( )
  {
    return( gif );
  }
  public int getWidth()
  {
    return( width );
  }
  public int getHeight()
  {
    return( height );
  }
}/*@lineinfo:generated-code*/