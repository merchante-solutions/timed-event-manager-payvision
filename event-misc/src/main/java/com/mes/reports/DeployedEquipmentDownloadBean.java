/*@lineinfo:filename=DeployedEquipmentDownloadBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/DeployedEquipmentDownloadBean.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 5/12/03 2:58p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class DeployedEquipmentDownloadBean extends ReportSQLJBean
{

  private String              lookupValue         = "";
  private int                 client              = -1;
  private int                 lrb                 = -1;
  private int                 condition           = -1;
  private int                 deploymentType      = -1;
  private String              equipType           = "-1";
  private String              equipModel          = "-1";

  public class RowData
  {
    public String      serialNum;
    public String      merchantNumber;
    public String      associationNumber;
    public String      dbaName;
    public String      client;
    public String      deployDate;
    public String      receiveDate;
    public String      deployType;
    public String      lrb;
    public String      equipType;
    public String      equipDesc;
    public String      cost;
    public String      condition;

    public RowData( ResultSet resultSet ) throws java.sql.SQLException
    {
      serialNum         = processString(resultSet.getString("serial_number"));
      merchantNumber    = processString(resultSet.getString("merchant_number"));
      associationNumber = processString(resultSet.getString("association_number"));
      dbaName           = processString(resultSet.getString("dba_name"));
      client            = processString(resultSet.getString("client_name"));
      deployDate        = processString(DateTimeFormatter.getFormattedDate(resultSet.getTimestamp("deploy_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));
      receiveDate       = processString(DateTimeFormatter.getFormattedDate(resultSet.getTimestamp("receive_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));
      deployType        = processString(resultSet.getString("deploy_type"));
      lrb               = processString(resultSet.getString("lrb"));
      equipType         = processString(resultSet.getString("equip_type"));
      equipDesc         = processString(resultSet.getString("equip_desc"));
      cost              = processString(resultSet.getString("unit_cost"));
      condition         = processString(resultSet.getString("condition"));
    }
  }

  public DeployedEquipmentDownloadBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Client\",");
    line.append("\"Receive Date\",");
    line.append("\"Deploy Date\",");
    line.append("\"Deploy Type\",");
    line.append("\"DBA Name\",");
    line.append("\"Merchant #\",");
    line.append("\"Assoc #\",");
    line.append("\"Lend Type\",");
    line.append("\"Serial #\",");
    line.append("\"Equip Type\",");
    line.append("\"Equip Desc\",");
    line.append("\"Condition\",");
    line.append("\"Cost\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( "\"" );
    line.append( record.client );
    line.append( "\",\"" );
    line.append( record.receiveDate );
    line.append( "\",\"" );
    line.append( record.deployDate );
    line.append( "\",\"" );
    line.append( record.deployType );
    line.append( "\",\"" );
    line.append( record.dbaName );
    line.append( "\",\"" );
    line.append( record.merchantNumber );
    line.append( "\",\"" );
    line.append( record.associationNumber );
    line.append( "\",\"" );
    line.append( record.lrb );
    line.append( "\",\"" );
    line.append( record.serialNum );
    line.append( "\",\"" );
    line.append( record.equipType );
    line.append( "\",\"" );
    line.append( record.equipDesc );
    line.append( "\",\"" );
    line.append( record.condition );
    line.append( "\",\"" );
    line.append( record.cost );
    line.append( "\"" );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_deployed_equipment_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData()
  {
    loadData( ReportUserBean, ReportDateBegin, ReportDateEnd );
  }

  public void loadData( UserBean user, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                  = null;
    ResultSet                     resultSet           = null;

    try
    {
      // empty the current contents
      ReportRows.clear();

      // set up longLookup value (numeric version of lookup value)
      long longLookup;
      try
      {
        longLookup = Long.parseLong(lookupValue.trim());
      }
      catch (Exception e) 
      {
        longLookup = -1;
      }
    
      // add wildcards to stringLookup so we can match partial strings
      String stringLookup = "";
      if(lookupValue.trim().equals(""))
      {
        stringLookup = "passall";
      }
      else
      {
        stringLookup = "%" + lookupValue.toUpperCase() + "%";
      }

      // no wildcards to stringLookup
      String stringLookup2 = "";
      if(lookupValue.trim().equals(""))
      {
        stringLookup2 = "passall";
      }
      else
      {
        stringLookup2 = lookupValue.toUpperCase();
      }
     

      /*@lineinfo:generated-code*//*@lineinfo:232^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mt.merchant_name                dba_name,
//                  mt.merchant_type_desc           client_name,
//                  mt.merchant_number              merchant_number,
//                  mt.association_number           association_number,
//                  emt.action_date                 deploy_date,
//                  emta.action_description         deploy_type,
//                  elt.equiplendtype_description   lrb,
//                  et.equiptype_description        equip_type,
//                  eq.equip_descriptor             equip_desc,
//                  ei.EI_SERIAL_NUMBER             serial_number,
//                  ei.ei_unit_cost                 unit_cost,
//                  ec.ec_class_name                condition,
//                  ei.ei_received_date             receive_date
//  
//          from    merchant_types                  mt,
//                  equip_merchant_tracking         emt,
//                  equip_merchant_tracking_action  emta,
//                  equipment                       eq,
//                  equiplendtype                   elt,
//                  equiptype                       et,
//                  equip_class                     ec,
//                  equip_inventory                 ei
//  
//          where   trunc(emt.action_date) between :beginDate and :endDate and
//                  emt.action in (1,4,5,7) and emt.cancel_date is null and
//                  emt.merchant_number = mt.merchant_number and
//                  emt.action = emta.action_code and
//                  emt.action_desc = elt.equiplendtype_code and
//                  emt.ref_num_serial_num = ei.ei_serial_number and
//                  ei.ei_class = ec.ec_class_id and
//                  ei.EI_PART_NUMBER = eq.equip_model and
//                  eq.equiptype_code = et.equiptype_code and 
//                  (-1 = :client or mt.merchant_type = :client) and
//                  (-1 = :lrb or emt.action_desc = :lrb) and
//                  (-1 = :deploymentType or emt.action = :deploymentType) and
//                  (-1 = :condition or ei.ei_class = :condition) and
//                  ('-1' = :equipType or eq.equiptype_code = :equipType) and
//                  ('-1' = :equipModel or eq.equip_model = :equipModel) and
//                  (
//                    'passall'              = :stringLookup or
//                    mt.merchant_number     = :longLookup or
//                    mt.association_number  = :longLookup or
//                    upper(emt.ref_num_serial_num) = :stringLookup2 or 
//                    upper(mt.merchant_name) like :stringLookup
//                  )          
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mt.merchant_name                dba_name,\n                mt.merchant_type_desc           client_name,\n                mt.merchant_number              merchant_number,\n                mt.association_number           association_number,\n                emt.action_date                 deploy_date,\n                emta.action_description         deploy_type,\n                elt.equiplendtype_description   lrb,\n                et.equiptype_description        equip_type,\n                eq.equip_descriptor             equip_desc,\n                ei.EI_SERIAL_NUMBER             serial_number,\n                ei.ei_unit_cost                 unit_cost,\n                ec.ec_class_name                condition,\n                ei.ei_received_date             receive_date\n\n        from    merchant_types                  mt,\n                equip_merchant_tracking         emt,\n                equip_merchant_tracking_action  emta,\n                equipment                       eq,\n                equiplendtype                   elt,\n                equiptype                       et,\n                equip_class                     ec,\n                equip_inventory                 ei\n\n        where   trunc(emt.action_date) between  :1  and  :2  and\n                emt.action in (1,4,5,7) and emt.cancel_date is null and\n                emt.merchant_number = mt.merchant_number and\n                emt.action = emta.action_code and\n                emt.action_desc = elt.equiplendtype_code and\n                emt.ref_num_serial_num = ei.ei_serial_number and\n                ei.ei_class = ec.ec_class_id and\n                ei.EI_PART_NUMBER = eq.equip_model and\n                eq.equiptype_code = et.equiptype_code and \n                (-1 =  :3  or mt.merchant_type =  :4 ) and\n                (-1 =  :5  or emt.action_desc =  :6 ) and\n                (-1 =  :7  or emt.action =  :8 ) and\n                (-1 =  :9  or ei.ei_class =  :10 ) and\n                ('-1' =  :11  or eq.equiptype_code =  :12 ) and\n                ('-1' =  :13  or eq.equip_model =  :14 ) and\n                (\n                  'passall'              =  :15  or\n                  mt.merchant_number     =  :16  or\n                  mt.association_number  =  :17  or\n                  upper(emt.ref_num_serial_num) =  :18  or \n                  upper(mt.merchant_name) like  :19 \n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.DeployedEquipmentDownloadBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setInt(3,client);
   __sJT_st.setInt(4,client);
   __sJT_st.setInt(5,lrb);
   __sJT_st.setInt(6,lrb);
   __sJT_st.setInt(7,deploymentType);
   __sJT_st.setInt(8,deploymentType);
   __sJT_st.setInt(9,condition);
   __sJT_st.setInt(10,condition);
   __sJT_st.setString(11,equipType);
   __sJT_st.setString(12,equipType);
   __sJT_st.setString(13,equipModel);
   __sJT_st.setString(14,equipModel);
   __sJT_st.setString(15,stringLookup);
   __sJT_st.setLong(16,longLookup);
   __sJT_st.setLong(17,longLookup);
   __sJT_st.setString(18,stringLookup2);
   __sJT_st.setString(19,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.DeployedEquipmentDownloadBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:279^7*/
      
      resultSet = it.getResultSet();
    
      while(resultSet.next())
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
    
      it.close();
   
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadData", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    this.lookupValue      = HttpHelper.getString(request, "lookupValue",    "");
    this.client           = HttpHelper.getInt(request,    "client",         -1);
    this.lrb              = HttpHelper.getInt(request,    "lrb",            -1);
    this.deploymentType   = HttpHelper.getInt(request,    "deploymentType", -1);
    this.condition        = HttpHelper.getInt(request,    "condition",      -1);
    this.equipType        = HttpHelper.getString(request, "equipType",      "-1");
    this.equipModel       = HttpHelper.getString(request, "equipModel",     "-1");

  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/