/*@lineinfo:filename=MerchSettlementDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchSettlementDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/15/04 10:48a $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class MerchSettlementDataBean extends ReportSQLJBean
{
  public static final int   RT_EST_DEPOSIT_SUMMARY    = (RT_USER + 0); // 2
  public static final int   RT_EST_DEPOSIT_DETAILS    = (RT_USER + 1); // 3
  
  public class RowData implements Comparable
  {
    public double               AchCreditsAmount  = 0.0;
    public Date                 AchDateMax        = null;
    public Date                 AchDateMin        = null;
    public double               AchDebitsAmount   = 0.0;
    public Date                 BatchDateMax      = null;
    public Date                 BatchDateMin      = null;
    public double               CBCreditsAmount   = 0.0;
    public double               CBDebitsAmount    = 0.0;
    public Date                 CBDateMax         = null;
    public Date                 CBDateMin         = null;
    public double               ChargebacksAmount = 0.0;
    public double               CreditsAmount     = 0.0;
    public int                  CreditsCount      = 0;
    public Date                 EstDepositDate    = null;
    public double               EstDisc           = 0.0;
    public long                 HierarchyNode     = 0L;
    public long                 OrgId             = 0L;
    public String               OrgName           = null;
    public long                 ParentNode        = 0L;
    public double               RejectsAmount     = 0.0;
    public double               SalesAmount       = 0.0;
    public int                  SalesCount        = 0;
    
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      OrgId             = resultSet.getLong("org_num");
      OrgName           = resultSet.getString("org_name");
      ParentNode        = resultSet.getLong("parent_node");
    }
    
    public void addAchItems( ResultSet resultSet )
    {
      try
      {
        AchCreditsAmount  = resultSet.getDouble("ach_credit_amount");
        AchDebitsAmount   = resultSet.getInt("ach_debit_amount");
        
        // store the batch date range
        AchDateMin = resultSet.getDate("post_date_min");
        AchDateMax = resultSet.getDate("post_date_max");
      }
      catch( Exception e )
      {
        logEntry("addAchItems()",e.toString());
      }
    }
    
    public void addChargebackItems( ResultSet resultSet )
    {
      try
      {
        CBCreditsAmount   += resultSet.getDouble("worked_credits");
        CBDebitsAmount    += resultSet.getDouble("worked_debits");
        ChargebacksAmount = (CBDebitsAmount - CBCreditsAmount);
        
        // store the batch date range
        CBDateMin = resultSet.getDate("incoming_date_min");
        CBDateMax = resultSet.getDate("incoming_date_max");
      }
      catch( Exception e )
      {
        logEntry("addChargebackItems()",e.toString());
      }
    }
    
    public void addRejectItems( ResultSet resultSet )
    {
      try
      {
        RejectsAmount = resultSet.getDouble("rejects_amount");
      }
      catch( Exception e )
      {
        logEntry("addRejectItems()",e.toString());
      }
    }
    
    public void addSettledItems( ResultSet resultSet )
    {
      try
      {
        SalesAmount   += resultSet.getDouble("sales_amount");
        SalesCount    += resultSet.getInt("sales_count");
        CreditsAmount += resultSet.getDouble("credits_amount");
        CreditsCount  += resultSet.getInt("credits_count");
        EstDisc       += resultSet.getDouble("est_disc");
        
        // store the batch date range
        BatchDateMin = resultSet.getDate("batch_date_min");
        BatchDateMax = resultSet.getDate("batch_date_max");
        
        try
        {
          EstDepositDate = resultSet.getDate("est_deposit_date");
        }
        catch(Exception e)
        {
          // ignore
        }
      }
      catch( Exception e )
      {
        logEntry("addSettledItems()",e.toString());
      }
    }
    
    public int compareTo( Object obj )
    {
      RowData           compareObj    = (RowData) obj;
      double            diff          = 0.0;
      int               retVal        = 0;
      
      if ( ReportType == RT_DETAILS )
      {
        if ( (retVal = (int)(ParentNode - compareObj.ParentNode)) == 0 )
        {
          if ( (retVal = OrgName.compareTo(compareObj.OrgName)) == 0 )
          {
            if ( (retVal = (int)(HierarchyNode - compareObj.HierarchyNode)) == 0 )
            {
              retVal = (int)(OrgId - compareObj.OrgId);
            }
          }
        }
      }
      else if ( ReportType == RT_EST_DEPOSIT_DETAILS )
      {
        if ( ( retVal = (int)(ParentNode - compareObj.ParentNode)) == 0 )
        {
          if ( (retVal = OrgName.compareTo(compareObj.OrgName)) == 0 )
          {
            if ( (retVal = (int)(HierarchyNode - compareObj.HierarchyNode)) == 0 )
            {
              retVal = (int)(OrgId - compareObj.OrgId);
            }
          }
        }
      }
      else    // default is by node
      {
        if ( (retVal = OrgName.compareTo(compareObj.OrgName)) == 0 )
        {
          if ( (retVal = (int)(HierarchyNode - compareObj.HierarchyNode)) == 0 )
          {
            retVal = (int)(OrgId - compareObj.OrgId);
          }
        }
      }        
      return( retVal );
    }
    
    public String getDateRangeCSV( )
    {
      StringBuffer      retVal      = new StringBuffer();
      
      if( BatchDateMin != null )
      {
        retVal.append( DateTimeFormatter.getFormattedDate(BatchDateMin,"MM/dd/yyyy") );
        if ( !BatchDateMax.equals(BatchDateMin) )
        {
          retVal.append("-");
          retVal.append( DateTimeFormatter.getFormattedDate(BatchDateMax,"MM/dd/yyyy") );
        }
      }        
      return( retVal.toString() );
    }
    
    public String getDateRangeHtml( )
    {
      StringBuffer      retVal      = new StringBuffer();
      
      if( BatchDateMin != null )
      {
        retVal.append( DateTimeFormatter.getFormattedDate(BatchDateMin,"MM/dd/yyyy") );
        if ( !BatchDateMax.equals(BatchDateMin) )
        {
          retVal.append("<br>");
          retVal.append( DateTimeFormatter.getFormattedDate(BatchDateMax,"MM/dd/yyyy") );
        }
      }        
      return( retVal.toString() );
    }
    
    public double getEstDeposit( )
    {
      return( SalesAmount - CreditsAmount - EstDisc - RejectsAmount - ChargebacksAmount );
    }
  }
  
  public MerchSettlementDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    if ( ReportType == RT_DETAILS )
    {
      line.append("\"Assoc Number\",");
      line.append("\"Assoc Name\",");
    }
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");

    switch( ReportType )
    {
      case RT_DETAILS:
      case RT_SUMMARY:
        line.append("\"Credits\",");
        line.append("\"Debits\",");
        line.append("\"Net Deposit\",");
        line.append("\"Chargebacks\",");
        line.append("\"Rejects\",");
        line.append("\"Tran Dates\",");
        line.append("\"Sales Count\",");
        line.append("\"Sales Amount\",");
        line.append("\"Credits Count\",");
        line.append("\"Credits Amount\",");
        line.append("\"Net Amount\",");
        line.append("\"Est Disc\",");
        line.append("\"Est Deposit\"");
        break;
        
      case RT_EST_DEPOSIT_DETAILS:
      case RT_EST_DEPOSIT_SUMMARY:
        line.append("\"Tran Dates\",");
        line.append("\"Sales Count\",");
        line.append("\"Sales Amount\",");
        line.append("\"Credits Count\",");
        line.append("\"Credits Amount\",");
        line.append("\"Net Amount\",");
        line.append("\"Est Disc\",");
        line.append("\"Est Deposit\",");
        line.append("\"Est Deposit Date\"");
        break;
    }    
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    if ( ReportType == RT_DETAILS )
    {
      line.append(encodeHierarchyNode(record.ParentNode));
      line.append(",\"");
      line.append(getNodeName(record.ParentNode));
      line.append("\",");
    }
    line.append(encodeHierarchyNode(record.HierarchyNode));
    line.append(",\"");
    line.append(record.OrgName);
    line.append("\",");
    
    switch( ReportType )
    {
      case RT_DETAILS:
      case RT_SUMMARY:
        line.append(record.AchCreditsAmount);
        line.append(",");
        line.append(record.AchDebitsAmount);
        line.append(",");
        line.append(record.AchCreditsAmount-record.AchDebitsAmount);
        line.append(",");
        line.append(record.ChargebacksAmount);
        line.append(",");
        line.append(record.RejectsAmount);
        line.append(",");
        break;
    }    
    
    line.append("\"");
    line.append(record.getDateRangeCSV());
    line.append("\",");
    line.append(record.SalesCount);
    line.append(",");
    line.append(record.SalesAmount);
    line.append(",");
    line.append(record.CreditsCount);
    line.append(",");
    line.append(record.CreditsAmount);
    line.append(",");
    line.append(record.SalesAmount-record.CreditsAmount);
    line.append(",");
    line.append(record.EstDisc);
    line.append(",");
    line.append( record.getEstDeposit() );
    
    if ( ReportType == RT_EST_DEPOSIT_DETAILS )
    {
    line.append(",");
    line.append( DateTimeFormatter.getFormattedDate(record.EstDepositDate,"MM/dd/yyyy") );
    }
  }
  
  public RowData findRowData( long nodeId )
  {
    RowData     retVal      = null;
    RowData     row         = null;
    
    for( int i = 0; i < ReportRows.size(); ++i )
    {
      row = (RowData)ReportRows.elementAt(i);
      
      if ( row.HierarchyNode == nodeId )
      {
        retVal = row;
        break;
      }
    }
    return(retVal);
  }
  
  public Date getChargebackDate( )
  {
    Date        beginDate   = getReportDateBegin();
    Date        retVal      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:380^7*/

//  ************************************************************
//  #sql [Ctx] { select  ( 
//                    (:beginDate-1) -
//                    decode( to_char( (:beginDate-1), 'd' ),
//                            '1', 2, 
//                            '7', 1,
//                             0 )
//                  ) 
//          from dual                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ( \n                  ( :1 -1) -\n                  decode( to_char( ( :2 -1), 'd' ),\n                          '1', 2, \n                          '7', 1,\n                           0 )\n                )  \n        from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchSettlementDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,beginDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:390^7*/
    }
    catch(Exception e)
    {
      logEntry("getChargebackDate()",e.toString());
    }
    return( retVal );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_deposit_reconcile_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( )
  {
    long      nodeId    = getReportHierarchyNode();
    
    switch( ReportType )
    {
      case RT_EST_DEPOSIT_DETAILS:
      case RT_EST_DEPOSIT_SUMMARY:
        loadSettlementData( nodeId, ReportDateBegin, ReportDateEnd );
        break;
        
      case RT_SUMMARY:
      case RT_DETAILS:
        loadDepositData( nodeId, ReportDateBegin, ReportDateEnd );
        break;
    } 
  }
  
  public void loadDepositData( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator     it            = null;
    ResultSet             resultSet     = null;
    RowData               row           = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( ReportType == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:459^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    mf.association_node         as parent_node, 
//                      o.org_num                   as org_num, 
//                      o.org_group                 as hierarchy_node,
//                      o.org_name                  as org_name, 
//                      min(ach.post_date)          as post_date_min,
//                      max(ach.post_date)          as post_date_max,
//                      sum(ach.credit_amount)      as ach_credit_amount, 
//                      sum(ach.debit_amount)       as ach_debit_amount 
//            from      organization                op, 
//                      group_merchant              gm, 
//                      mif                         mf, 
//                      ach_summary                 ach,
//                      organization                o
//            where     op.org_group = :nodeId and
//                      gm.org_num = op.org_num and 
//                      mf.merchant_number = gm.merchant_number and 
//                      ach.merchant_number = mf.merchant_number and 
//                      ach.post_date between :beginDate and :endDate and
//                      o.org_group = mf.merchant_number
//            group by  mf.association_node, o.org_num,o.org_group,o.org_name 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mf.association_node         as parent_node, \n                    o.org_num                   as org_num, \n                    o.org_group                 as hierarchy_node,\n                    o.org_name                  as org_name, \n                    min(ach.post_date)          as post_date_min,\n                    max(ach.post_date)          as post_date_max,\n                    sum(ach.credit_amount)      as ach_credit_amount, \n                    sum(ach.debit_amount)       as ach_debit_amount \n          from      organization                op, \n                    group_merchant              gm, \n                    mif                         mf, \n                    ach_summary                 ach,\n                    organization                o\n          where     op.org_group =  :1  and\n                    gm.org_num = op.org_num and \n                    mf.merchant_number = gm.merchant_number and \n                    ach.merchant_number = mf.merchant_number and \n                    ach.post_date between  :2  and  :3  and\n                    o.org_group = mf.merchant_number\n          group by  mf.association_node, o.org_num,o.org_group,o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchSettlementDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchSettlementDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:481^9*/
      }
      else  // summary
      {
        /*@lineinfo:generated-code*//*@lineinfo:485^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    op.org_group                as parent_node, 
//                      o.org_num                   as org_num, 
//                      o.org_group                 as hierarchy_node,
//                      o.org_name                  as org_name, 
//                      min(ach.post_date)          as post_date_min,
//                      max(ach.post_date)          as post_date_max,
//                      sum(ach.credit_amount)      as ach_credit_amount, 
//                      sum(ach.debit_amount)       as ach_debit_amount 
//            from      organization                op, 
//                      parent_org                  po, 
//                      organization                o, 
//                      group_merchant              gm, 
//                      mif                         mf, 
//                      ach_summary                 ach
//            where     op.org_group = :nodeId and 
//                      po.parent_org_num = op.org_num and 
//                      o.org_num = po.org_num and 
//                      gm.org_num = o.org_num and 
//                      mf.merchant_number = gm.merchant_number and 
//                      ach.merchant_number = mf.merchant_number and 
//                      ach.post_date between :beginDate and :endDate
//            group by  op.org_group, o.org_num,o.org_group,o.org_name 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    op.org_group                as parent_node, \n                    o.org_num                   as org_num, \n                    o.org_group                 as hierarchy_node,\n                    o.org_name                  as org_name, \n                    min(ach.post_date)          as post_date_min,\n                    max(ach.post_date)          as post_date_max,\n                    sum(ach.credit_amount)      as ach_credit_amount, \n                    sum(ach.debit_amount)       as ach_debit_amount \n          from      organization                op, \n                    parent_org                  po, \n                    organization                o, \n                    group_merchant              gm, \n                    mif                         mf, \n                    ach_summary                 ach\n          where     op.org_group =  :1  and \n                    po.parent_org_num = op.org_num and \n                    o.org_num = po.org_num and \n                    gm.org_num = o.org_num and \n                    mf.merchant_number = gm.merchant_number and \n                    ach.merchant_number = mf.merchant_number and \n                    ach.post_date between  :2  and  :3 \n          group by  op.org_group, o.org_num,o.org_group,o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchSettlementDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchSettlementDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:509^9*/
      }        
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        row = new RowData( resultSet );
        row.addAchItems( resultSet );
        ReportRows.addElement( row );
      }
      it.close();   // this will also close the resultSet
      
      Calendar    cal           = Calendar.getInstance();
      int         binc          = 1;
      int         einc          = 1;
      
      cal.setTime( beginDate );
      switch( cal.get( Calendar.DAY_OF_WEEK ) )
      {
        case Calendar.MONDAY:
          einc = 2;
        case Calendar.TUESDAY:
          binc = 2;
          break;
          
        default: 
          break;
      }
      
      //
      // SETTLED ITEMS (DDF)
      //
      if ( ReportType == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:543^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.association_node         as parent_node, 
//                    o.org_num                   as org_num, 
//                    o.org_group                 as hierarchy_node,
//                    o.org_name                  as org_name,
//                    min(ds.batch_date)          as batch_date_min,
//                    max(ds.batch_date)          as batch_date_max,
//                    sum(ds.sales_count)         as sales_count, 
//                    sum(ds.sales_amount)        as sales_amount, 
//                    sum(ds.credits_count)       as credits_count, 
//                    sum(ds.credits_amount)      as credits_amount,
//                    sum(ds.disc_estimated)      as est_disc
//            from    organization                op, 
//                    group_merchant              gm, 
//                    mif                         mf, 
//                    daily_detail_disc_summary   ds,
//                    organization                o
//            where   op.org_group = :nodeId and 
//                    gm.org_num = op.org_num and 
//                    mf.merchant_number = gm.merchant_number and
//                    ds.merchant_number= mf.merchant_number and 
//                    ds.batch_date between  
//                      ( 
//                        (:beginDate-nvl(mf.suspended_days,0)-1) -
//                        decode( to_char( (:beginDate-nvl(mf.suspended_days,0)-1), 'd' ),
//                                '1', 1, 
//                                '7', 1,
//                                 0 )
//                      ) and
//                      ( 
//                        (:beginDate-nvl(mf.suspended_days,0)-1) -
//                        decode( to_char( (:beginDate-nvl(mf.suspended_days,0)-1), 'd' ),
//                                '7', 1,
//                                 0 )
//                      ) and
//                    o.org_group = mf.merchant_number 
//            group by mf.association_node, o.org_num,o.org_group,o.org_name      
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.association_node         as parent_node, \n                  o.org_num                   as org_num, \n                  o.org_group                 as hierarchy_node,\n                  o.org_name                  as org_name,\n                  min(ds.batch_date)          as batch_date_min,\n                  max(ds.batch_date)          as batch_date_max,\n                  sum(ds.sales_count)         as sales_count, \n                  sum(ds.sales_amount)        as sales_amount, \n                  sum(ds.credits_count)       as credits_count, \n                  sum(ds.credits_amount)      as credits_amount,\n                  sum(ds.disc_estimated)      as est_disc\n          from    organization                op, \n                  group_merchant              gm, \n                  mif                         mf, \n                  daily_detail_disc_summary   ds,\n                  organization                o\n          where   op.org_group =  :1  and \n                  gm.org_num = op.org_num and \n                  mf.merchant_number = gm.merchant_number and\n                  ds.merchant_number= mf.merchant_number and \n                  ds.batch_date between  \n                    ( \n                      ( :2 -nvl(mf.suspended_days,0)-1) -\n                      decode( to_char( ( :3 -nvl(mf.suspended_days,0)-1), 'd' ),\n                              '1', 1, \n                              '7', 1,\n                               0 )\n                    ) and\n                    ( \n                      ( :4 -nvl(mf.suspended_days,0)-1) -\n                      decode( to_char( ( :5 -nvl(mf.suspended_days,0)-1), 'd' ),\n                              '7', 1,\n                               0 )\n                    ) and\n                  o.org_group = mf.merchant_number \n          group by mf.association_node, o.org_num,o.org_group,o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchSettlementDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.MerchSettlementDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:581^9*/
      }
      else // summary
      {
        /*@lineinfo:generated-code*//*@lineinfo:585^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  op.org_group                as parent_node, 
//                    o.org_num                   as org_num, 
//                    o.org_group                 as hierarchy_node,
//                    o.org_name                  as org_name,
//                    min(ds.batch_date)          as batch_date_min,
//                    max(ds.batch_date)          as batch_date_max,
//                    sum(ds.sales_count)         as sales_count, 
//                    sum(ds.sales_amount)        as sales_amount, 
//                    sum(ds.credits_count)       as credits_count, 
//                    sum(ds.credits_amount)      as credits_amount,
//                    sum(ds.disc_estimated)      as est_disc
//            from    organization                op, 
//                    parent_org                  po,                     
//                    organization                o, 
//                    group_merchant              gm, 
//                    mif                         mf, 
//                    daily_detail_disc_summary   ds
//            where   op.org_group = :nodeId and 
//                    po.parent_org_num = op.org_num and 
//                    o.org_num = po.org_num and 
//                    gm.org_num = o.org_num and 
//                    mf.merchant_number = gm.merchant_number and
//                    ds.merchant_number= mf.merchant_number and 
//                    ds.batch_date between  
//                      ( 
//                        (:beginDate-nvl(mf.suspended_days,0)-1) -
//                        decode( to_char( (:beginDate-nvl(mf.suspended_days,0)-1), 'd' ),
//                                '1', 1, 
//                                '7', 1,
//                                 0 )
//                      ) and
//                      ( 
//                        (:beginDate-nvl(mf.suspended_days,0)-1) -
//                        decode( to_char( (:beginDate-nvl(mf.suspended_days,0)-1), 'd' ),
//                                '7', 1,
//                                 0 )
//                      )
//            group by op.org_group, o.org_num,o.org_group,o.org_name      
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  op.org_group                as parent_node, \n                  o.org_num                   as org_num, \n                  o.org_group                 as hierarchy_node,\n                  o.org_name                  as org_name,\n                  min(ds.batch_date)          as batch_date_min,\n                  max(ds.batch_date)          as batch_date_max,\n                  sum(ds.sales_count)         as sales_count, \n                  sum(ds.sales_amount)        as sales_amount, \n                  sum(ds.credits_count)       as credits_count, \n                  sum(ds.credits_amount)      as credits_amount,\n                  sum(ds.disc_estimated)      as est_disc\n          from    organization                op, \n                  parent_org                  po,                     \n                  organization                o, \n                  group_merchant              gm, \n                  mif                         mf, \n                  daily_detail_disc_summary   ds\n          where   op.org_group =  :1  and \n                  po.parent_org_num = op.org_num and \n                  o.org_num = po.org_num and \n                  gm.org_num = o.org_num and \n                  mf.merchant_number = gm.merchant_number and\n                  ds.merchant_number= mf.merchant_number and \n                  ds.batch_date between  \n                    ( \n                      ( :2 -nvl(mf.suspended_days,0)-1) -\n                      decode( to_char( ( :3 -nvl(mf.suspended_days,0)-1), 'd' ),\n                              '1', 1, \n                              '7', 1,\n                               0 )\n                    ) and\n                    ( \n                      ( :4 -nvl(mf.suspended_days,0)-1) -\n                      decode( to_char( ( :5 -nvl(mf.suspended_days,0)-1), 'd' ),\n                              '7', 1,\n                               0 )\n                    )\n          group by op.org_group, o.org_num,o.org_group,o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.MerchSettlementDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.MerchSettlementDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:625^9*/
      }        
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        // look for an existing row for this node
        row = findRowData( resultSet.getLong("hierarchy_node") );
    
        if ( row == null )
        {
          row = new RowData( resultSet );
          ReportRows.addElement( row );
        }
        row.addSettledItems( resultSet );
      }
      it.close();   // this will also close the resultSet
      
      //
      // CHARGEBACKS
      //
      if ( ReportType == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:648^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    mf.association_node         as parent_node, 
//                      o.org_num                   as org_num, 
//                      o.org_group                 as hierarchy_node,
//                      o.org_name                  as org_name, 
//                      min(cb.incoming_date)       as incoming_date_min,
//                      max(cb.incoming_date)       as incoming_date_max,
//                      sum( decode(cba.action_code,
//                                  'D',cb.tran_amount,
//                                  0
//                                  ) )             as worked_debits,
//                      sum( decode(cba.action_code,
//                                  'C',cb.tran_amount,
//                                  'T',cb.tran_amount,
//                                  0
//                                  ) )             as worked_credits
//            from      organization                op,
//                      group_merchant              gm, 
//                      mif                         mf, 
//                      network_chargebacks         cb,
//                      network_chargeback_activity cba,
//                      organization                o
//            where     op.org_group = :nodeId and
//                      gm.org_num = op.org_num and 
//                      mf.merchant_number = gm.merchant_number and 
//                      cb.merchant_number = mf.merchant_number and 
//                      cb.incoming_date >= (:beginDate-180) and
//                      cba.cb_load_sec = cb.cb_load_sec and
//                      cba.action_code in ('D','C','T') and
//                      cba.action_date = 
//                        ( 
//                          (:beginDate-1) -
//                          decode( to_char( (:beginDate-1), 'd' ),
//                                  '1', 2, 
//                                  '7', 1,
//                                   0 )
//                        ) and
//                      not cba.load_filename is null and
//                      o.org_group = mf.merchant_number
//            group by  mf.association_node, o.org_num,o.org_group,o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mf.association_node         as parent_node, \n                    o.org_num                   as org_num, \n                    o.org_group                 as hierarchy_node,\n                    o.org_name                  as org_name, \n                    min(cb.incoming_date)       as incoming_date_min,\n                    max(cb.incoming_date)       as incoming_date_max,\n                    sum( decode(cba.action_code,\n                                'D',cb.tran_amount,\n                                0\n                                ) )             as worked_debits,\n                    sum( decode(cba.action_code,\n                                'C',cb.tran_amount,\n                                'T',cb.tran_amount,\n                                0\n                                ) )             as worked_credits\n          from      organization                op,\n                    group_merchant              gm, \n                    mif                         mf, \n                    network_chargebacks         cb,\n                    network_chargeback_activity cba,\n                    organization                o\n          where     op.org_group =  :1  and\n                    gm.org_num = op.org_num and \n                    mf.merchant_number = gm.merchant_number and \n                    cb.merchant_number = mf.merchant_number and \n                    cb.incoming_date >= ( :2 -180) and\n                    cba.cb_load_sec = cb.cb_load_sec and\n                    cba.action_code in ('D','C','T') and\n                    cba.action_date = \n                      ( \n                        ( :3 -1) -\n                        decode( to_char( ( :4 -1), 'd' ),\n                                '1', 2, \n                                '7', 1,\n                                 0 )\n                      ) and\n                    not cba.load_filename is null and\n                    o.org_group = mf.merchant_number\n          group by  mf.association_node, o.org_num,o.org_group,o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.MerchSettlementDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.MerchSettlementDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:689^9*/
      }
      else  // summary
      {
        /*@lineinfo:generated-code*//*@lineinfo:693^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    op.org_group                as parent_node, 
//                      o.org_num                   as org_num, 
//                      o.org_group                 as hierarchy_node,
//                      o.org_name                  as org_name, 
//                      min(cb.incoming_date)       as incoming_date_min,
//                      max(cb.incoming_date)       as incoming_date_max,
//                      sum( decode(cba.action_code,
//                                  'D',cb.tran_amount,
//                                  0
//                                  ) )             as worked_debits,
//                      sum( decode(cba.action_code,
//                                  'C',cb.tran_amount,
//                                  'T',cb.tran_amount,
//                                  0
//                                  ) )             as worked_credits
//            from      organization                op, 
//                      parent_org                  po, 
//                      organization                o, 
//                      group_merchant              gm, 
//                      mif                         mf, 
//                      network_chargebacks         cb,
//                      network_chargeback_activity cba
//            where     op.org_group = :nodeId and 
//                      po.parent_org_num = op.org_num and 
//                      o.org_num = po.org_num and 
//                      gm.org_num = o.org_num and 
//                      mf.merchant_number = gm.merchant_number and 
//                      cb.merchant_number = mf.merchant_number and 
//                      cb.incoming_date >= (:beginDate-180) and
//                      cba.cb_load_sec = cb.cb_load_sec and
//                      cba.action_code in ('D','C','T') and
//                      cba.action_date = 
//                        ( 
//                          (:beginDate-1) -
//                          decode( to_char( (:beginDate-1), 'd' ),
//                                  '1', 2, 
//                                  '7', 1,
//                                   0 )
//                        ) and
//                      not cba.load_filename is null
//            group by  op.org_group, o.org_num,o.org_group,o.org_name 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    op.org_group                as parent_node, \n                    o.org_num                   as org_num, \n                    o.org_group                 as hierarchy_node,\n                    o.org_name                  as org_name, \n                    min(cb.incoming_date)       as incoming_date_min,\n                    max(cb.incoming_date)       as incoming_date_max,\n                    sum( decode(cba.action_code,\n                                'D',cb.tran_amount,\n                                0\n                                ) )             as worked_debits,\n                    sum( decode(cba.action_code,\n                                'C',cb.tran_amount,\n                                'T',cb.tran_amount,\n                                0\n                                ) )             as worked_credits\n          from      organization                op, \n                    parent_org                  po, \n                    organization                o, \n                    group_merchant              gm, \n                    mif                         mf, \n                    network_chargebacks         cb,\n                    network_chargeback_activity cba\n          where     op.org_group =  :1  and \n                    po.parent_org_num = op.org_num and \n                    o.org_num = po.org_num and \n                    gm.org_num = o.org_num and \n                    mf.merchant_number = gm.merchant_number and \n                    cb.merchant_number = mf.merchant_number and \n                    cb.incoming_date >= ( :2 -180) and\n                    cba.cb_load_sec = cb.cb_load_sec and\n                    cba.action_code in ('D','C','T') and\n                    cba.action_date = \n                      ( \n                        ( :3 -1) -\n                        decode( to_char( ( :4 -1), 'd' ),\n                                '1', 2, \n                                '7', 1,\n                                 0 )\n                      ) and\n                    not cba.load_filename is null\n          group by  op.org_group, o.org_num,o.org_group,o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.MerchSettlementDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.MerchSettlementDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:736^9*/
      }        
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        // look for an existing row for this node
        row = findRowData( resultSet.getLong("hierarchy_node") );
    
        if ( row == null )
        {
          row = new RowData( resultSet );
          ReportRows.addElement( row );
        }
        row.addChargebackItems( resultSet );
      }
      it.close();   // this will also close the resultSet
      
      //
      // REJECTS
      //
      if ( ReportType == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:759^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    mf.association_node           as parent_node, 
//                      o.org_num                     as org_num, 
//                      o.org_group                   as hierarchy_node,
//                      o.org_name                    as org_name, 
//                      sum( decode(dt.debit_credit_indicator,
//                                  'C',-1,1) *
//                           dt.transaction_amount )  as rejects_amount
//            from      organization                op,
//                      group_merchant              gm, 
//                      mif                         mf, 
//                      daily_detail_file_dt        dt,
//                      organization                o
//            where     op.org_group = :nodeId and
//                      gm.org_num = op.org_num and 
//                      mf.merchant_number = gm.merchant_number and 
//                      dt.merchant_account_number = mf.merchant_number and 
//                      dt.batch_date between
//                      ( 
//                        (:beginDate-nvl(mf.suspended_days,0)-1) -
//                        decode( to_char( (:beginDate-nvl(mf.suspended_days,0)-1), 'd' ),
//                                '1', 1, 
//                                '7', 1,
//                                 0 )
//                      ) and
//                      ( 
//                        (:beginDate-nvl(mf.suspended_days,0)-1) -
//                        decode( to_char( (:beginDate-nvl(mf.suspended_days,0)-1), 'd' ),
//                                '7', 1,
//                                 0 )
//                      ) and
//                      not dt.reject_reason is null and
//                      o.org_group = mf.merchant_number
//            group by  mf.association_node, o.org_num,o.org_group,o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mf.association_node           as parent_node, \n                    o.org_num                     as org_num, \n                    o.org_group                   as hierarchy_node,\n                    o.org_name                    as org_name, \n                    sum( decode(dt.debit_credit_indicator,\n                                'C',-1,1) *\n                         dt.transaction_amount )  as rejects_amount\n          from      organization                op,\n                    group_merchant              gm, \n                    mif                         mf, \n                    daily_detail_file_dt        dt,\n                    organization                o\n          where     op.org_group =  :1  and\n                    gm.org_num = op.org_num and \n                    mf.merchant_number = gm.merchant_number and \n                    dt.merchant_account_number = mf.merchant_number and \n                    dt.batch_date between\n                    ( \n                      ( :2 -nvl(mf.suspended_days,0)-1) -\n                      decode( to_char( ( :3 -nvl(mf.suspended_days,0)-1), 'd' ),\n                              '1', 1, \n                              '7', 1,\n                               0 )\n                    ) and\n                    ( \n                      ( :4 -nvl(mf.suspended_days,0)-1) -\n                      decode( to_char( ( :5 -nvl(mf.suspended_days,0)-1), 'd' ),\n                              '7', 1,\n                               0 )\n                    ) and\n                    not dt.reject_reason is null and\n                    o.org_group = mf.merchant_number\n          group by  mf.association_node, o.org_num,o.org_group,o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.MerchSettlementDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.MerchSettlementDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:794^9*/
      }
      else  // summary
      {
        /*@lineinfo:generated-code*//*@lineinfo:798^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    op.org_group                  as parent_node, 
//                      o.org_num                     as org_num, 
//                      o.org_group                   as hierarchy_node,
//                      o.org_name                    as org_name, 
//                      sum( decode(dt.debit_credit_indicator,
//                                  'C',-1,1) *
//                           dt.transaction_amount )  as rejects_amount
//            from      organization                op, 
//                      parent_org                  po, 
//                      organization                o, 
//                      group_merchant              gm, 
//                      mif                         mf, 
//                      daily_detail_file_dt        dt
//            where     op.org_group = :nodeId and 
//                      po.parent_org_num = op.org_num and 
//                      o.org_num = po.org_num and 
//                      gm.org_num = o.org_num and 
//                      mf.merchant_number = gm.merchant_number and 
//                      dt.merchant_account_number = mf.merchant_number and 
//                      dt.batch_date between
//                      ( 
//                        (:beginDate-nvl(mf.suspended_days,0)-1) -
//                        decode( to_char( (:beginDate-nvl(mf.suspended_days,0)-1), 'd' ),
//                                '1', 1, 
//                                '7', 1,
//                                 0 )
//                      ) and
//                      ( 
//                        (:beginDate-nvl(mf.suspended_days,0)-1) -
//                        decode( to_char( (:beginDate-nvl(mf.suspended_days,0)-1), 'd' ),
//                                '7', 1,
//                                 0 )
//                      ) and
//                      not dt.reject_reason is null
//            group by  op.org_group, o.org_num,o.org_group,o.org_name 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    op.org_group                  as parent_node, \n                    o.org_num                     as org_num, \n                    o.org_group                   as hierarchy_node,\n                    o.org_name                    as org_name, \n                    sum( decode(dt.debit_credit_indicator,\n                                'C',-1,1) *\n                         dt.transaction_amount )  as rejects_amount\n          from      organization                op, \n                    parent_org                  po, \n                    organization                o, \n                    group_merchant              gm, \n                    mif                         mf, \n                    daily_detail_file_dt        dt\n          where     op.org_group =  :1  and \n                    po.parent_org_num = op.org_num and \n                    o.org_num = po.org_num and \n                    gm.org_num = o.org_num and \n                    mf.merchant_number = gm.merchant_number and \n                    dt.merchant_account_number = mf.merchant_number and \n                    dt.batch_date between\n                    ( \n                      ( :2 -nvl(mf.suspended_days,0)-1) -\n                      decode( to_char( ( :3 -nvl(mf.suspended_days,0)-1), 'd' ),\n                              '1', 1, \n                              '7', 1,\n                               0 )\n                    ) and\n                    ( \n                      ( :4 -nvl(mf.suspended_days,0)-1) -\n                      decode( to_char( ( :5 -nvl(mf.suspended_days,0)-1), 'd' ),\n                              '7', 1,\n                               0 )\n                    ) and\n                    not dt.reject_reason is null\n          group by  op.org_group, o.org_num,o.org_group,o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.MerchSettlementDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.reports.MerchSettlementDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:835^9*/
      }        
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        // look for an existing row for this node
        row = findRowData( resultSet.getLong("hierarchy_node") );
    
        if ( row == null )
        {
          row = new RowData( resultSet );
          ReportRows.addElement( row );
        }
        row.addRejectItems( resultSet );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDepositData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSettlementData( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator     it            = null;
    ResultSet             resultSet     = null;
    RowData               row           = null;
  
    try
    {
      //
      // SETTLED ITEMS (DDF)
      //
      if ( ReportType == RT_EST_DEPOSIT_DETAILS || isNodeMerchant(nodeId) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:876^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.association_node         as parent_node, 
//                    o.org_num                   as org_num, 
//                    mf.merchant_number          as hierarchy_node,
//                    mf.dba_name                 as org_name,
//                    (:beginDate+nvl(mf.suspended_days,0)) +
//                    decode( to_char( (:beginDate+nvl(mf.suspended_days,0)), 'd' ),
//                            '1', 1, 
//                            '7', 1,
//                             0 )                as est_deposit_date,
//                    min(ds.batch_date)          as batch_date_min,
//                    max(ds.batch_date)          as batch_date_max,
//                    sum(ds.sales_count)         as sales_count, 
//                    sum(ds.sales_amount)        as sales_amount, 
//                    sum(ds.credits_count)       as credits_count, 
//                    sum(ds.credits_amount)      as credits_amount,
//                    sum(ds.disc_estimated)      as est_disc
//            from    organization                op, 
//                    group_merchant              gm, 
//                    mif                         mf, 
//                    daily_detail_disc_summary   ds,
//                    organization                o
//            where   op.org_group = :nodeId and 
//                    gm.org_num = op.org_num and 
//                    mf.merchant_number = gm.merchant_number and
//                    ds.merchant_number= mf.merchant_number and 
//                    ds.batch_date between :beginDate and :endDate and
//                    o.org_group = mf.merchant_number 
//            group by mf.association_node, o.org_num,mf.merchant_number,
//                      mf.dba_name, mf.suspended_days
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.association_node         as parent_node, \n                  o.org_num                   as org_num, \n                  mf.merchant_number          as hierarchy_node,\n                  mf.dba_name                 as org_name,\n                  ( :1 +nvl(mf.suspended_days,0)) +\n                  decode( to_char( ( :2 +nvl(mf.suspended_days,0)), 'd' ),\n                          '1', 1, \n                          '7', 1,\n                           0 )                as est_deposit_date,\n                  min(ds.batch_date)          as batch_date_min,\n                  max(ds.batch_date)          as batch_date_max,\n                  sum(ds.sales_count)         as sales_count, \n                  sum(ds.sales_amount)        as sales_amount, \n                  sum(ds.credits_count)       as credits_count, \n                  sum(ds.credits_amount)      as credits_amount,\n                  sum(ds.disc_estimated)      as est_disc\n          from    organization                op, \n                  group_merchant              gm, \n                  mif                         mf, \n                  daily_detail_disc_summary   ds,\n                  organization                o\n          where   op.org_group =  :3  and \n                  gm.org_num = op.org_num and \n                  mf.merchant_number = gm.merchant_number and\n                  ds.merchant_number= mf.merchant_number and \n                  ds.batch_date between  :4  and  :5  and\n                  o.org_group = mf.merchant_number \n          group by mf.association_node, o.org_num,mf.merchant_number,\n                    mf.dba_name, mf.suspended_days";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.MerchSettlementDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setLong(3,nodeId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.MerchSettlementDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:907^9*/
      }
      else // est deposit summary
      {
        /*@lineinfo:generated-code*//*@lineinfo:911^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  op.org_group                as parent_node, 
//                    o.org_num                   as org_num, 
//                    o.org_group                 as hierarchy_node,
//                    o.org_name                  as org_name,
//                    min(ds.batch_date)          as batch_date_min,
//                    max(ds.batch_date)          as batch_date_max,
//                    sum(ds.sales_count)         as sales_count, 
//                    sum(ds.sales_amount)        as sales_amount, 
//                    sum(ds.credits_count)       as credits_count, 
//                    sum(ds.credits_amount)      as credits_amount,
//                    sum(ds.disc_estimated)      as est_disc
//            from    organization                op, 
//                    parent_org                  po,                     
//                    organization                o, 
//                    group_merchant              gm, 
//                    mif                         mf, 
//                    daily_detail_disc_summary   ds
//            where   op.org_group = :nodeId and 
//                    po.parent_org_num = op.org_num and 
//                    o.org_num = po.org_num and 
//                    gm.org_num = o.org_num and 
//                    mf.merchant_number = gm.merchant_number and
//                    ds.merchant_number= mf.merchant_number and 
//                    ds.batch_date between :beginDate and :endDate
//            group by op.org_group, o.org_num,o.org_group,o.org_name      
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  op.org_group                as parent_node, \n                  o.org_num                   as org_num, \n                  o.org_group                 as hierarchy_node,\n                  o.org_name                  as org_name,\n                  min(ds.batch_date)          as batch_date_min,\n                  max(ds.batch_date)          as batch_date_max,\n                  sum(ds.sales_count)         as sales_count, \n                  sum(ds.sales_amount)        as sales_amount, \n                  sum(ds.credits_count)       as credits_count, \n                  sum(ds.credits_amount)      as credits_amount,\n                  sum(ds.disc_estimated)      as est_disc\n          from    organization                op, \n                  parent_org                  po,                     \n                  organization                o, \n                  group_merchant              gm, \n                  mif                         mf, \n                  daily_detail_disc_summary   ds\n          where   op.org_group =  :1  and \n                  po.parent_org_num = op.org_num and \n                  o.org_num = po.org_num and \n                  gm.org_num = o.org_num and \n                  mf.merchant_number = gm.merchant_number and\n                  ds.merchant_number= mf.merchant_number and \n                  ds.batch_date between  :2  and  :3 \n          group by op.org_group, o.org_num,o.org_group,o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.MerchSettlementDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.MerchSettlementDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:938^9*/
      }        
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        // look for an existing row for this node
        row = findRowData( resultSet.getLong("hierarchy_node") );
    
        if ( row == null )
        {
          row = new RowData( resultSet );
          ReportRows.addElement( row );
        }
        row.addSettledItems( resultSet );
      }
      it.close();   // this will also close the resultSet
    }
    catch(Exception e)
    {
      logEntry("loadSettlementData()",e.toString());
    }
    finally
    {
      try{it.close();}catch(Exception e){}
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range to be last day
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }    
    
    // only support a single date
    setReportDateEnd( ReportDateBegin );
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/