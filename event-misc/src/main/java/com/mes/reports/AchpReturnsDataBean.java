package com.mes.reports;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

public class AchpReturnsDataBean extends AchpOrgSummaryDataBean
{
  static Logger log = Logger.getLogger(AchpReturnsDataBean.class);

  public AchpReturnsDataBean()
  {
    super(true);   // enable field bean support
  }

  /**
   * FIELD BEAN
   */

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    createStandardAchpFields(request);
  }

  /**
   * REPORT DATA LOADING
   */

  public class DetailRow
  {
    public long       tranId;
    public java.util.Date
                      tranDate;
    public BigDecimal tranAmount;
    public BigDecimal returnAmount;
    public String     tranSource;
    public String     destination;
    public String     tranType;
    public String     secCode;
    public String     accountNum;
    public String     merchNum;
    public String     profileId;
    public String     companyName;
    public long       returnId;
    public java.util.Date
                      returnDate;
    public String     returnType;
    public String     returnCode;
    public String     controlNum;
    public String     descriptor;
    public String     retDesc;
    public String     traceNum;
    public String     refNum;
    public String     transitNum;
    public String     accountType;
    public String     custName;
    public String     custId;
    public String     recurFlag;
    public String     cdCode;
    public String     entryDesc;
  }

  private DetailRow createDetailRow(ResultSet rs) throws Exception
  {
    DetailRow row     = new DetailRow();
    row.tranId        = rs.getLong      ("tran_id");
    row.tranDate      = toDate(rs.getTimestamp
                                        ("tran_ts"));
    row.tranAmount    = rs.getBigDecimal("tran_amount").setScale(2);
    row.returnAmount  = rs.getBigDecimal("return_amount").setScale(2);
    row.tranSource    = rs.getString    ("tran_source");
    row.destination   = rs.getString    ("destination");
    row.tranType      = rs.getString    ("tran_type");
    row.secCode       = rs.getString    ("sec_code");
    row.accountNum    = rs.getString    ("account_num");
    row.merchNum      = rs.getString    ("merch_num");
    row.profileId     = rs.getString    ("profile_id");
    row.companyName   = rs.getString    ("company_name");
    row.returnId      = rs.getLong      ("return_id");
    row.returnDate    = toDate(rs.getTimestamp
                                        ("create_ts"));
    row.returnType    = rs.getString    ("return_type");
    row.returnCode    = rs.getString    ("return_code");
    row.controlNum    = rs.getString    ("bank_control_num");
    row.descriptor    = rs.getString    ("descriptor");
    row.retDesc       = rs.getString    ("return_description");
    row.traceNum      = rs.getString    ("mes_trace_num");
    row.refNum        = rs.getString    ("ref_num");
    row.transitNum    = rs.getString    ("transit_num");
    row.accountType   = rs.getString    ("account_type");
    row.custName      = rs.getString    ("cust_name");
    row.custId        = rs.getString    ("cust_id");
    row.recurFlag     = rs.getString    ("recur_flag");
    row.cdCode        = rs.getString    ("cd_code");
    row.entryDesc     = rs.getString    ("entry_description");
    return row;
  }

  /**
   * Load settlement funding entry data from achp_sf_entries to display
   * in achp_settlement_detail.jsp.  Data is loaded into DetailRow objects
   * in ReportSQLJBean.ReportRows.
   */
  public void loadDetailData(long orgId, Date beginDate, Date endDate)
  {
    Statement s = null;

    try
    {
      String bDate = formatOraDate(beginDate);
      String eDate = formatOraDate(endDate != null ? endDate : beginDate);
      String profileId = getData("profileId");
      String pCls = 
        profileId.length() > 0 ? 
          " and p.profile_id = '" + profileId + "' " : "";

      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select                                    " +
        "  t.tran_id,                               " +
        "  t.tran_ts,                               " +
        "  decode(d.cd_code,                        " +
        "   'C',-t.amount,                          " +
        "   t.amount) as tran_amount,               " +
        "  decode(r.return_type,                    " +
        "   'R', decode(d.cd_code,                  " +
        "         'C',t.amount,                     " +
        "         -t.amount),0) as return_amount,   " +
        "  t.tran_source,                           " +
        "  d.destination,                           " +
        "  t.tran_type,                             " +
        "  t.sec_code,                              " +
        "  t.account_num,                           " +
        "  t.merch_num,                             " +
        "  r.profile_id,                            " +
        "  p.company_name,                          " +
        "  r.return_id,                             " +
        "  r.create_ts,                             " +
        "  r.return_type ||                         " +
        "   to_char(r.reason_code,'FM00')           " +
        "     as return_code,                       " +
        "  r.bank_control_num,                      " +
        "  r.descriptor,                            " +
        "  r.return_description,                    " +
        "  r.mes_trace_num,                         " +
        "  t.ref_num,                               " +
        "  r.return_type,                           " +
        "  t.transit_num,                           " +
        "  t.account_type,                          " +
        "  t.cust_name,                             " +
        "  t.cust_id,                               " +
        "  t.recur_flag,                            " +
        "  d.recur_code,                            " +
        "  d.cd_code,                               " +
        "  d.entry_description                      " +
        " from                                      " +
        "  group_merchant     g,                    " +
        "  achp_transactions  t,                    " +
        "  achp_returns       r,                    " +
        "  achp_profiles      p,                    " +
        "  achp_file_details  d                     " +
        " where                                     " +
        "  g.org_num = " + orgId + "                " +
        "  and g.merchant_number = t.merch_num      " +
        "  and t.profile_id = p.profile_id          " +
        "  and t.tran_id = r.tran_id                " +
        "  and r.test_flag = 'N'                    " +
        "  and trunc(r.create_ts)                   " +
        "   between " + bDate + " and " + eDate + " " +
        " " + pCls +
        "  and r.tran_id = d.source_id              " +
        " order by                                  " +
        "  r.profile_id,                            " +
        "  r.return_type,                           " +
        "  r.create_ts                              ");
      while (rs.next())
      {
        ReportRows.add(createDetailRow(rs));
      }
    }
    catch (Exception e)
    {
      log.error(""+e);
      e.printStackTrace();
      logEntry(buildMethodName("loadDetailData",orgId,beginDate,endDate),""+e);
    }
    finally
    {
      try { s.close(); } catch (Exception e) { }
    }
  }

  public class SummaryRow
  {
    public String     merchNum;
    public String     profileId;
    public String     companyName;
    public int        returnCount;
    public int        nocCount;
    public BigDecimal returnAmount;
  }

  public SummaryRow createSummaryRow(ResultSet rs) throws Exception
  {
    SummaryRow row    = new SummaryRow();
    row.merchNum      = rs.getString    ("merch_num");
    row.profileId     = rs.getString    ("profile_id");
    row.companyName   = rs.getString    ("company_name");
    row.returnCount   = rs.getInt       ("return_count");
    row.nocCount      = rs.getInt       ("noc_count");
    row.returnAmount  = rs.getBigDecimal("return_amount").setScale(2);
    return row;
  }

  /**
   * Load settlement funding summary data from achp_sf_summaries by profile
   * to display in achp_settlement_summary.jsp.
   */
  private void loadMerchSummaryData(long orgId, Date beginDate, Date endDate)
  {
    Statement s = null;

    try
    {
      String bDate = formatOraDate(beginDate);
      String eDate = formatOraDate(endDate != null ? endDate : beginDate);
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select                                              " +
        "  p.merch_num,                                       " +
        "  p.profile_id,                                      " +
        "  p.company_name,                                    " +
        "  sum(decode(r.return_type,'R',1,0)) return_count,   " +
        "  sum(decode(r.return_type,'C',1,0)) noc_count,      " +
        "  sum(decode(r.return_type,                          " +
        "   'R',decode(d.cd_code,                             " +
        "        'C',d.amount,-d.amount), 0)) return_amount   " +
        " from                                                " +
        "  group_merchant       g,                            " +
        "  achp_profiles        p,                            " +
        "  achp_returns         r,                            " +
        "  achp_file_details    d                             " +
        " where                                               " +
        "  g.org_num = " + orgId + "                          " +
        "  and g.merchant_number = p.merch_num                " +
        "  and p.profile_id = r.profile_id(+)                 " +
        "  and trunc(r.create_ts(+)) between                  " +
        "   " + bDate + " and " + eDate + "                   " +
        "  and r.test_flag(+) = 'N'                           " +
        "  and r.destination(+) = 'CUST'                      " +
        "  and r.tran_id = d.source_id(+)                     " +
        " group by p.merch_num, p.profile_id, p.company_name  " +
        " order by p.profile_id                               ");
      while (rs.next())
      {
        ReportRows.add(createSummaryRow(rs));
      }
    }
    catch (Exception e)
    {
      log.error(""+e);
      e.printStackTrace();
      logEntry(
        buildMethodName("loadMerchSummaryData",orgId,beginDate,endDate),""+e);
    }
    finally
    {
      try { s.close(); } catch (Exception e) { }
    }
  }

  /**
   * Load settlement funding summary data from achp_sf_summaries by merchant
   * to display in org_summary.jsp.
   */
  private void loadOrgSummaryData(long orgId, Date beginDate, Date endDate)
  {
    Statement s = null;

    try
    {
      String bDate = formatOraDate(beginDate);
      String eDate = formatOraDate(endDate != null ? endDate : beginDate);
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select                                                " +
        "  o.org_num                            org_num,        " +
        "  o.org_group                          hierarchy_node, " +
        "  o.org_name                           org_name,       " +
        "  0                                    district,       " +
        "  sum(decode(r.return_type,'R',1,0))   item_count,     " +
        "  sum(decode(r.return_type,'C',1,0))   non_bank_count, " +
        "  sum(decode(r.return_type,                            " +
        "   'R',decode(d.cd_code,                               " +
        "        'C',r.amount,-r.amount), 0))   item_amount     " +
        " from                                                  " +
        "  parent_org         pr,                               " +
        "  organization       o,                                " +
        "  group_merchant     g,                                " +
        "  achp_profiles      p,                                " +
        "  achp_returns       r,                                " +
        "  achp_file_details  d                                 " +
        " where                                                 " +
        "  pr.parent_org_num = " + orgId + "                    " +                
        "  and pr.org_num = o.org_num                           " +
        "  and o.org_num = g.org_num                            " +
        "  and g.merchant_number = p.merch_num(+)               " +
        "  and p.profile_id = r.profile_id(+)                   " +
        "  and r.test_flag(+) = 'N'                             " +
        "  and trunc(r.create_ts(+))                            " +
        "   between " + bDate + " and " + eDate + "             " +
        "  and r.tran_id = d.source_id(+)                       " +
        "  and r.destination(+) = 'CUST'                        " +
        " group by o.org_num, o.org_group, o.org_name           " +
        " order by o.org_num                                    ");
      processSummaryData(rs);
    }
    catch (Exception e)
    {
      log.error(""+e);
      e.printStackTrace();
      logEntry(
        buildMethodName("loadOrgSummaryData",orgId,beginDate,endDate),""+e);
    }
    finally
    {
      try { s.close(); } catch (Exception e) { }
    }
  }

  /**
   * Load summary data.  Determines if viewing at organization (org_summary.jsp)
   * or merchant summary level (achp_settlement_summary.jsp) and loads data
   * accordingly.
   */
  public void loadSummaryData(long orgId, Date beginDate, Date endDate)
  {
    if (isNodeMerchant())
    {
      loadMerchSummaryData(orgId,beginDate,endDate);
    }
    else
    {
      loadOrgSummaryData(orgId,beginDate,endDate);
    }
  }

  /**
   * Set report specific properties.
   */
  public void setProperties(HttpServletRequest request)
  {
    super.setProperties(request);
  }

  /**
   * CSV DOWNLOAD
   */

  public String getBeanSpecificFilenameFragment()
  {
    return "achp_returns";
  }

  /**
   * Merchant detail row
   */
  private void encodeDetailLineCSV(Object lineObj, StringBuffer line)
  {
    DetailRow row = (DetailRow)lineObj;
    encodeCSV(row.tranId,line);
    line.append(",");
    encodeCSV(row.merchNum,line);
    line.append(",");
    encodeCSV("P"+row.profileId,line);
    line.append(",");
    encodeCSV(row.companyName,line);
    line.append(",");
    encodeCSV(row.returnDate,line);
    line.append(",");
    encodeCSV(""+row.returnAmount,line);
    line.append(",");
    encodeCSV(row.returnCode,line);
    line.append(",");
    encodeCSV(row.descriptor,line);
    line.append(",");
    encodeCSV(row.retDesc,line);
    line.append(",");
    encodeCSV(row.tranDate,line);
    line.append(",");
    encodeCSV(row.tranType,line);
    line.append(",");
    encodeCSV(row.accountNum,line);
    line.append(",");
    encodeCSV(row.accountType,line);
    line.append(",");
    encodeCSV(row.refNum,line);
    line.append(",");
    encodeCSV(row.custName,line);
    line.append(",");
    encodeCSV(row.custId,line);
    line.append(",");
    encodeCSV(""+row.tranAmount,line);
  }

  /**
   * Merchant detail row
   */
  private void encodeDetailHeaderCSV(StringBuffer line)
  {
    line.append("\"Transaction ID\",");
    line.append("\"Merchant Number\",");
    line.append("\"Profile ID\",");
    line.append("\"Company Name\",");
    line.append("\"Return Date\",");
    line.append("\"Return Amount\",");
    line.append("\"Code\",");
    line.append("\"Description\",");
    line.append("\"Data\",");
    line.append("\"Tran Date\",");
    line.append("\"Tran Type\",");
    line.append("\"Account Number\",");
    line.append("\"Account Type\",");
    line.append("\"Reference Number\",");
    line.append("\"Customer Name\",");
    line.append("\"Customer ID\",");
    line.append("\"Amount\"");
  }

  /**
   * Merchant summary header
   */
  private void encodeSummaryLineCSV(Object lineObj, StringBuffer line)
  {
    SummaryRow row = (SummaryRow)lineObj;
    encodeCSV(row.merchNum,line);
    line.append(",");
    encodeCSV("P"+row.profileId,line);
    line.append(",");
    encodeCSV(row.companyName,line);
    line.append(",");
    encodeCSV(row.returnCount,line);
    line.append(",");
    encodeCSV(row.nocCount,line);
    line.append(",");
    encodeCSV(""+row.returnAmount,line);
  }

  /**
   * Merchant summary row
   */
  private void encodeSummaryHeaderCSV(StringBuffer line)
  {
    line.append("\"Merchant Number\",");
    line.append("\"Profile Id\",");
    line.append("\"Return Cnt\",");
    line.append("\"NOC Cnt\",");
    line.append("\"Return Amt\"");
  }

  /**
   * Org summary header
   */
  private void encodeOrgSummaryHeaderCSV(StringBuffer line)
  {
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    line.append("\"Return Cnt\",");
    line.append("\"NOC Cnt\",");
    line.append("\"Total\"");
  }

  /**
   * Org summary row
   */
  private void encodeOrgSummaryLineCSV(Object lineObj, StringBuffer line)
  {
    MesOrgSummaryEntry row = (MesOrgSummaryEntry)lineObj;
    encodeCSV(encodeHierarchyNode(row.getHierarchyNode()),line,false);
    line.append(",");
    encodeCSV(row.getOrgName(),line);
    line.append(",");
    encodeCSV(row.getCount(),line);
    line.append(",");
    encodeCSV(row.getCount(MesOrgSummaryEntry.ADJ_INDEX),line);
    line.append(",");
    encodeCSV(row.getTotalAmount(),line);
  }

  /**
   * Generate download file header.
   *
   * Supports three download flavors, org_summary style, merchant summary 
   * and merchant detail.
   */
  protected void encodeHeaderCSV(StringBuffer line)
  {
    line.setLength(0);

    // merchant detail
    if (getReportType() == RT_DETAILS)
    {
      encodeDetailHeaderCSV(line);
    }
    else
    {
      // merchant summary
      if ((getReportMerchantId() != 0L))
      {
        encodeSummaryHeaderCSV(line);
      }
      // org summary
      else  
      {
        encodeOrgSummaryHeaderCSV(line);
      }
    }
  }

  /**
   * Generate download file row.
   */
  protected void encodeLineCSV(Object lineObj, StringBuffer line)
  {
    line.setLength(0);

    // merchant detail
    if (getReportType() == RT_DETAILS)
    {
      encodeDetailLineCSV(lineObj,line);
    }
    else
    {
      // merchant summary
      if ((getReportMerchantId() != 0L))
      {
        encodeSummaryLineCSV(lineObj,line);
      }
      // org summary
      else  
      {
        encodeOrgSummaryLineCSV(lineObj,line);
      }
    }
  }
}