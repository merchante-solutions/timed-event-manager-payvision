/*@lineinfo:filename=EquipExpDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/EquipExpDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 12/12/01 1:11p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import sqlj.runtime.ResultSetIterator;

public class EquipExpDataBean extends ReportSQLJBean
{
  static public final int           PRICE_SALE      = 0;
  static public final int           PRICE_RENTAL    = 1;
  static public final int           PRICE_COUNT     = 2;

  static public final int           ET_NONE         = 0;
  static public final int           ET_RENTAL       = 1;
  static public final int           ET_SALES        = 2;
  
  static public final int           ST_IDLE         = 0;
  static public final int           ST_PART_NUM     = 1;
  static public final int           ST_QUANTITY     = 2;
  
  public class EquipData
  {
    public    Date          ActiveDate      = null;
    public    double        BasePriceRental = 0.0;
    public    double        BasePriceSale   = 0.0;
    public    int           ItemCount       = 0;
    public    String        ModelDesc       = "";
    public    String        ModelId         = "";
    public    boolean       Rental          = false;
    public    int           Type            = ET_NONE;
    
    public EquipData( String modelName, 
                      String modelDesc,
                      int itemCount, 
                      Date activeDate, 
                      int type,
                      double[] basePrices )
    {
      ActiveDate      = activeDate;
      BasePriceRental = basePrices[PRICE_RENTAL];
      BasePriceSale   = basePrices[PRICE_SALE];
      ItemCount       = itemCount;
      ModelId         = modelName;
      ModelDesc       = modelDesc;
      Type            = type;
    }
    
    public String getTypeString( )
    {
      String    retVal = Integer.toString(Type);
      
      switch( Type )
      {
        case ET_SALES:
          retVal = "Sale";
          break;
          
        case ET_RENTAL:
          retVal = "Rental";
          break;
      }
      return( retVal );
    }
    
    public void showData( java.io.PrintStream out )
    {
      out.println( " Model Id          : " + ModelId );
      out.println( " Model Name        : " + ModelDesc );
      out.println( " Item Count        : " + ItemCount );
      out.println( " Rental Base Price : " + BasePriceRental );
      out.println( " Sales Base Price  : " + BasePriceSale );
      out.println( " Active Date       : " + ActiveDate );
      out.println( " Type              : " + getTypeString() );
      out.println();
    }
  }
  
  public EquipExpDataBean( )
  {
  }
  
  protected void extractEquipModelIds( Date activeDate, String msg, int defaultCount, Date expireDate )
  {
    char            ch;
    StringBuffer    count       = new StringBuffer("");
    int             itemCount   = 0;
    StringBuffer    model       = new StringBuffer("");
    int             state       = 0;
    int             type        = ET_NONE;
    
    try
    {
      if ( (msg.indexOf("RENT") != -1) || (expireDate == null) )
      {
        type = ET_RENTAL;
      }
      else
      {
        type = ET_SALES;
      }
      
      for( int i = 0; i < msg.length(); ++i )
      {
        ch = msg.charAt(i);
        switch( state )
        {
          case ST_IDLE:       // waiting for model to start
            if ( ch == '*' )
            {
              state = 1;
            }
            break;
            
          case ST_PART_NUM:       // model id
            if ( ( ch == ',' ) || ( ch == '*' ) )
            {
              ReportRows.addElement( new EquipData(model.toString(),
                                                   getModelName(model.toString()),
                                                   defaultCount, 
                                                   activeDate, 
                                                   type,
                                                   loadBaseCost(model.toString(),activeDate) ) );
              
              // reset the string buffers
              count.setLength(0);
              model.setLength(0);
              
              state = ((ch=='*') ? ST_IDLE : ST_PART_NUM);
            }
            else if ( ch == '-' )
            {
              state = ST_QUANTITY;
            }
            else
            {
              model.append(ch);
            }              
            break;
            
          case ST_QUANTITY:     // item count
            if ( ( ch == ',' ) || ( ch == '*' ) )
            {
              try
              {
                itemCount = Integer.parseInt(count.toString());
              }
              catch(NumberFormatException e)
              {
                itemCount = defaultCount;
              }
              ReportRows.addElement( new EquipData(model.toString(),
                                                   getModelName(model.toString()),
                                                   itemCount, 
                                                   activeDate, 
                                                   type,
                                                   loadBaseCost(model.toString(),activeDate) ) );
              
              
              
              
              
              // reset the string buffers
              count.setLength(0);
              model.setLength(0);
              
              state = ((ch=='*') ? ST_IDLE : ST_PART_NUM);
            }
            else
            {
              count.append(ch);
            }
            break;
            
          default:
            state = 0;
            continue;
        }
      }
    }
    catch( Exception e )
    {
    }
  }
  
  public String getModelName( String modelId )
  {
    String          retVal    = "Unknown";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:220^7*/

//  ************************************************************
//  #sql [Ctx] { select eq.equip_descriptor 
//          from   equipment  eq
//          where  eq.equip_model = :modelId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select eq.equip_descriptor  \n        from   equipment  eq\n        where  eq.equip_model =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.EquipExpDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,modelId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:225^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public double[] loadBaseCost( String modelId, Date activeDate )
  {
    double[]          prices    = new double[PRICE_COUNT];
    
    try
    {
      for( int i = 0; i < prices.length; ++i )
      {
        prices[i] = 0.0;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:244^7*/

//  ************************************************************
//  #sql [Ctx] { select nvl(ep.PRICE_BASE,0),
//                 nvl(ep.PRICE_RENTAL,0)         
//          from   equip_price ep
//          where  ep.EQUIP_MODEL = :modelId and
//                 :activeDate between ep.VALID_DATE_BEGIN and ep.VALID_DATE_END
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select nvl(ep.PRICE_BASE,0),\n               nvl(ep.PRICE_RENTAL,0)          \n        from   equip_price ep\n        where  ep.EQUIP_MODEL =  :1  and\n                :2  between ep.VALID_DATE_BEGIN and ep.VALID_DATE_END";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.EquipExpDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,modelId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   prices[PRICE_SALE] = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   prices[PRICE_RENTAL] = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:251^7*/
    }
    catch( java.sql.SQLException e )
    {
      System.out.println(e.toString());  
    }
    return( prices );
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator         it            = null;
    ResultSet                 resultSet     = null;
    
    try
    {
      ReportRows.removeAllElements();
      
      /*@lineinfo:generated-code*//*@lineinfo:269^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  gn.hh_active_date                         as active_date,
//                  cg.CG_CHARGE_AMOUNT                       as charge_amount,
//                  cg.cg_expiration_date                     as expiration_date,
//                  decode(cg.CG_NUMBER_POS_IMPRINTERS,
//                         0,1,
//                         cg.cg_number_pos_imprinters)       as num_items,
//                  cg.CG_MESSAGE_FOR_STMT                    as message
//          from    monthly_extract_gn gn,
//                  monthly_extract_cg cg
//          where   gn.hh_merchant_number in
//                    ( select merchant_number
//                      from   group_merchant
//                      where  org_num = :orgId ) and                
//                  gn.hh_active_date between :beginDate and last_day(:endDate) and
//                  cg.hh_load_sec = gn.hh_load_sec and
//                  cg.cg_start_date <= gn.hh_active_date and
//                  substr( cg.CG_BILLING_FREQ_MASK,
//                         to_number( to_char( gn.hh_active_date, 'mm' ) ), 1 ) = 'Y' and
//                  cg.cg_charge_record_type in ( 'POS','IMP','MEM' ) and
//                  cg.CG_MESSAGE_FOR_STMT like '%*%*%'                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  gn.hh_active_date                         as active_date,\n                cg.CG_CHARGE_AMOUNT                       as charge_amount,\n                cg.cg_expiration_date                     as expiration_date,\n                decode(cg.CG_NUMBER_POS_IMPRINTERS,\n                       0,1,\n                       cg.cg_number_pos_imprinters)       as num_items,\n                cg.CG_MESSAGE_FOR_STMT                    as message\n        from    monthly_extract_gn gn,\n                monthly_extract_cg cg\n        where   gn.hh_merchant_number in\n                  ( select merchant_number\n                    from   group_merchant\n                    where  org_num =  :1  ) and                \n                gn.hh_active_date between  :2  and last_day( :3 ) and\n                cg.hh_load_sec = gn.hh_load_sec and\n                cg.cg_start_date <= gn.hh_active_date and\n                substr( cg.CG_BILLING_FREQ_MASK,\n                       to_number( to_char( gn.hh_active_date, 'mm' ) ), 1 ) = 'Y' and\n                cg.cg_charge_record_type in ( 'POS','IMP','MEM' ) and\n                cg.CG_MESSAGE_FOR_STMT like '%*%*%'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.EquipExpDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.EquipExpDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:291^7*/
      resultSet = it.getResultSet();
      while( resultSet.next() )
      {
        extractEquipModelIds( resultSet.getDate("active_date"),
                            resultSet.getString("message"),
                            resultSet.getInt("num_items"),
                            resultSet.getDate("expiration_date") );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  public void reset( )
  {
    ReportRows.removeAllElements();
  }
  
  public void showData( java.io.PrintStream out )
  {
    EquipData       data = null;
    
    for ( int i = 0; i < ReportRows.size(); ++i )
    {
      data = (EquipData)ReportRows.elementAt(i);
      data.showData(out);
    }
  }
  
  public static void main( String[] args )
  {
    EquipExpDataBean        bean      = null;
    Calendar                cal       = Calendar.getInstance();
    java.sql.Date           sqlDate   = null;
    
    try
    {
      // turn on connection pool debug output
      com.mes.database.OracleConnectionPool.getInstance().setDebugOutput(true);
      
      bean = new EquipExpDataBean();
      
      // setup the default date
      cal.set(Calendar.MONTH,Calendar.SEPTEMBER);
      cal.set(Calendar.DAY_OF_MONTH,1);
      sqlDate = new java.sql.Date( cal.getTime().getTime() );
      
      // load the data
      bean.loadData( bean.hierarchyNodeToOrgId( Long.parseLong(args[0]) ), sqlDate, sqlDate );
      
      // dump to console
      bean.showData(System.out);
    }
    finally
    {
      bean.cleanUp();
      com.mes.database.OracleConnectionPool.getInstance().cleanUp();
    }
  }
}/*@lineinfo:generated-code*/