/*@lineinfo:filename=ReasonCodeDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ReasonCodeDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 10/13/04 10:49a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import sqlj.runtime.ResultSetIterator;


public class ReasonCodeDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public String         reasonCode        = null;
    public String         itemType          = null;
    public String         cardType          = null;
    public String         reasonDesc        = null;
    public String         reasonDescDetail  = null;
    public String         reasonRes         = null;    
    
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      reasonCode        = resultSet.getString("reason_code");
      itemType          = resultSet.getString("item_type");
      cardType          = resultSet.getString("card_type");
      reasonDesc        = resultSet.getString("reason_desc");
      reasonDescDetail  = resultSet.getString("reason_desc_detail");
      reasonRes         = resultSet.getString("reason_res");
    }
  }
    
  
  public void loadData()
  {
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:80^7*/

//  ************************************************************
//  #sql [Ctx] it = { select
//            reason_code,
//            item_type,
//            card_type,
//            reason_desc,
//            reason_desc_detail,
//            reason_res
//          from
//            chargeback_reason_desc
//          order by
//            reason_code asc,
//            card_type asc,
//            item_type asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n          reason_code,\n          item_type,\n          card_type,\n          reason_desc,\n          reason_desc_detail,\n          reason_res\n        from\n          chargeback_reason_desc\n        order by\n          reason_code asc,\n          card_type asc,\n          item_type asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ReasonCodeDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ReasonCodeDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:95^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "ReasonCodeDataBean:loadData", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/