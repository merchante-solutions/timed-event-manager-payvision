/*@lineinfo:filename=CSVolumeDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CSVolumeDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-06-01 15:36:27 -0700 (Fri, 01 Jun 2007) $
  Version            : $Revision: 13770 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class CSVolumeDataBean extends ReportSQLJBean
{
  public static final int        MONTH_COUNT         = 3;
  
  public class RowData
  {
    public long       HierarchyNode       = 0L;
    public long       OrgId               = 0L;
    public String     OrgName             = null;
    public int        MonthIndex          = 0;
    public double[]   MonthlySalesAmount  = new double[MONTH_COUNT];
    public int[]      MonthlySalesCount   = new int[MONTH_COUNT];
    
    public RowData( long           orgId,
                    long           node,
                    String         orgName )
    {
      MonthIndex          = 0;
      HierarchyNode       = node;
      OrgId               = orgId;
      OrgName             = orgName;
    }
    
    public double getAvgTicket( int index )
    {
      return( (MonthlySalesAmount[index]/((MonthlySalesCount[index]==0)?1:MonthlySalesCount[index])) );
    }
    
    public void setMonthlySales( double salesAmount, int salesCount )
    {
      try
      {
        MonthlySalesAmount[MonthIndex]  = salesAmount;
        MonthlySalesCount[MonthIndex]   = salesCount;
        MonthIndex++;
      }
      catch( ArrayIndexOutOfBoundsException e )
      {
        logEntry( "RowData.setMonthlySales()", e.toString() );
      }
    }
  }
  
  public class ReportTotals
  {
    public double[]   SalesAmountTotal    = new double[MONTH_COUNT];
    public int[]      SalesCountTotal     = new int[MONTH_COUNT];
  
    public ReportTotals()
    {
    }
    
    public void add( RowData rowData )
    {
      for( int i = 0; i < MONTH_COUNT; ++i )
      {
        SalesAmountTotal[i] += rowData.MonthlySalesAmount[i];
        SalesCountTotal[i]  += rowData.MonthlySalesCount[i];
      }
    }
    
    public double getAvgTicket( int index )
    {
      return( (SalesAmountTotal[index]/((SalesCountTotal[index]==0)?1:SalesCountTotal[index])) );
    }
  }
  
  private Date[]        ReportDates             = new Date[MONTH_COUNT];
  
  public CSVolumeDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    line.append("\"Sales " + DateTimeFormatter.getFormattedDate(ReportDates[0],"MMM yyyy")  + "\",");
    line.append("\"Sales " + DateTimeFormatter.getFormattedDate(ReportDates[1],"MMM yyyy")  + "\",");
    line.append("\"Sales " + DateTimeFormatter.getFormattedDate(ReportDates[2],"MMM yyyy")  + "\",");
    line.append("\"Avg Tkt " + DateTimeFormatter.getFormattedDate(ReportDates[0],"MMM yyyy")  + "\",");
    line.append("\"Avg Tkt " + DateTimeFormatter.getFormattedDate(ReportDates[1],"MMM yyyy")  + "\",");
    line.append("\"Avg Tkt " + DateTimeFormatter.getFormattedDate(ReportDates[2],"MMM yyyy")  + "\",");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData       record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.HierarchyNode );
    line.append( ",\"" );
    line.append( record.OrgName );
    line.append( "\"," );
    line.append( record.MonthlySalesAmount[0] );
    line.append( "," );
    line.append( record.MonthlySalesAmount[1] );
    line.append( "," );
    line.append( record.MonthlySalesAmount[2] );
    line.append( "," );
    line.append( record.getAvgTicket(0) );
    line.append( "," );
    line.append( record.getAvgTicket(1) );
    line.append( "," );
    line.append( record.getAvgTicket(2) );
  }
  
  protected RowData findRowData( long orgId )
  {
    RowData     retVal    = null;
    
    for( int i = 0; i < ReportRows.size(); ++i )
    {
      if ( ((RowData)ReportRows.elementAt(i)).OrgId == orgId )
      {
        retVal = (RowData)ReportRows.elementAt(i);
        break;
      }
    }
    return(retVal);
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMMyyyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_cs_qrtr_sales_");
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDates[2] ) );
    filename.append("_to_");
    filename.append( rptDate.format( ReportDates[0] ) );
    return ( filename.toString() );
  }
  
  public Date getReportMonth( int index )
  {
    return( ReportDates[index] );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    Calendar                      cal               = Calendar.getInstance();
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    Date                          tempDate          = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      // setup the calendar to the last month
      cal.setTime(beginDate);
      cal.set(Calendar.DAY_OF_MONTH,1);
      
      // setup the array of months
      for( int i = 0; i < MONTH_COUNT; i++ )
      {
        ReportDates[i] = new java.sql.Date( cal.getTime().getTime() );
        cal.add(Calendar.MONTH,-1);
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:228^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      ordered 
//                      use_nl(o gm ds) 
//                      index(ds pk_ddf_summary) 
//                   */
//                  o.org_num                   as org_num,
//                  o.org_group                 as hierarchy_node,
//                  o.org_name                  as org_name,
//                  trunc(ds.batch_date,'month')as vol_month,
//                  sum(ds.BANK_SALES_AMOUNT)   as sales_amount,
//                  sum(ds.BANK_SALES_COUNT)    as sales_count
//          from    parent_org                po,
//                  organization              o,
//                  group_merchant            gm,
//                  daily_detail_file_summary ds
//          where   po.parent_org_num = :orgId and
//                  o.org_num = po.org_num and
//                  gm.org_num = o.org_num and
//                  ds.merchant_number = gm.merchant_number and
//                  ds.batch_date between :ReportDates[2] and last_day(:ReportDates[0])
//          group by o.org_num, o.org_group, o.org_name, trunc(ds.batch_date,'month')
//          order by o.org_name, vol_month desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1111 = ReportDates[2];
 java.sql.Date __sJT_1112 = ReportDates[0];
  try {
   String theSqlTS = "select  /*+ \n                    ordered \n                    use_nl(o gm ds) \n                    index(ds pk_ddf_summary) \n                 */\n                o.org_num                   as org_num,\n                o.org_group                 as hierarchy_node,\n                o.org_name                  as org_name,\n                trunc(ds.batch_date,'month')as vol_month,\n                sum(ds.BANK_SALES_AMOUNT)   as sales_amount,\n                sum(ds.BANK_SALES_COUNT)    as sales_count\n        from    parent_org                po,\n                organization              o,\n                group_merchant            gm,\n                daily_detail_file_summary ds\n        where   po.parent_org_num =  :1  and\n                o.org_num = po.org_num and\n                gm.org_num = o.org_num and\n                ds.merchant_number = gm.merchant_number and\n                ds.batch_date between  :2  and last_day( :3 )\n        group by o.org_num, o.org_group, o.org_name, trunc(ds.batch_date,'month')\n        order by o.org_name, vol_month desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.CSVolumeDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,__sJT_1111);
   __sJT_st.setDate(3,__sJT_1112);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.CSVolumeDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:252^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        setMonthlySalesRowData(  resultSet.getLong("org_num"),
                                 resultSet.getLong("hierarchy_node"),
                                 resultSet.getString("org_name"),
                                 resultSet.getDouble("sales_amount"),
                                 resultSet.getInt("sales_count") );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void setMonthlySalesRowData( long orgId, long nodeId, String name,
                                          double salesAmount, int salesCount )
  {
    RowData       row    = findRowData( orgId );
    
    if ( row == null )
    {
      row = new RowData(orgId,nodeId,name);
      ReportRows.addElement(row);
    }
    row.setMonthlySales(salesAmount,salesCount);
  }                            

  public void setProperties(HttpServletRequest request)
  {
    Calendar    cal           = Calendar.getInstance();
  
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      cal.add( Calendar.MONTH, -1 );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/