/*@lineinfo:filename=AcctClosedBillingDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AcctClosedBillingDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/03/02 4:38p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class AcctClosedBillingDataBean extends ReportSQLJBean
{
  public static final int     CLOSED_ACCT_DCE_REF             = 38;
  public static final long    MES_DIRECT_PORTFOLIO            = 3941500001L;
  
  public class RowData
  {
    public Date       ActivationDate      = null;
    public double     AdjAmount           = 0.0;
    public Date       AdjDate             = null;
    public Date       ClosedDate          = null;
    public String     DbaName             = null;
    public int        DceRefNum           = 0;
    public long       MerchantId          = 0L;
    public Date       OpenedDate          = null;
    public String     PortfolioName       = null;
    public long       PortfolioNode       = 0L;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException 
    {
      ActivationDate  = resultSet.getDate   ("activation_date");
      AdjAmount       = resultSet.getDouble ("adj_amount");
      AdjDate         = resultSet.getDate   ("date_billed");
      ClosedDate      = resultSet.getDate   ("date_closed");
      DbaName         = processString(resultSet.getString ("dba_name"));
      DceRefNum       = resultSet.getInt    ("dce_ref_num");
      MerchantId      = resultSet.getLong   ("merchant_number");
      OpenedDate      = resultSet.getDate   ("date_opened");
      PortfolioNode   = resultSet.getLong   ("portfolio_node");
      PortfolioName   = processString(resultSet.getString ("portfolio_name"));
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Portfolio Node\",");
    line.append("\"Portfolio Name\",");
    line.append("\"Merchant Id\",");
    line.append("\"DBA Name\",");
    line.append("\"Date Opened\",");
    line.append("\"Date Closed\",");
    line.append("\"DCE Reference\",");
    line.append("\"DCE Date\",");
    line.append("\"DCE Amount\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData       record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( encodeHierarchyNode(record.PortfolioNode) );
    line.append( ",\"" );
    line.append( record.PortfolioName );
    line.append( "\"," );
    line.append( encodeHierarchyNode(record.MerchantId) );
    line.append( ",\"" );
    line.append( record.DbaName );
    line.append( "\"," );
    line.append( DateTimeFormatter.getFormattedDate(record.OpenedDate,"MM/dd/yyyy") );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.ClosedDate,"MM/dd/yyyy") );
    line.append( "," );
    line.append( record.DceRefNum );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.AdjDate,"MM/dd/yyyy") );
    line.append( "," );
    line.append( record.AdjAmount );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_closed_dce_detail_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin, "MMddyyyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd, "MMddyyyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:158^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  op.org_group                              as portfolio_node,
//                  op.org_name                               as portfolio_name,
//                  mf.merchant_number                        as merchant_number,
//                  mf.dba_name                               as dba_name,
//                  mf.activation_date                        as activation_date,
//                  to_date(decode(mf.date_opened,
//                                 '000000','010100',
//                                 mf.date_opened),'mmddyy')  as date_opened,
//                  mf.date_stat_chgd_to_dcb                  as date_closed,
//                  mf.date_billed_for_close                  as date_billed,
//                  :CLOSED_ACCT_DCE_REF                      as dce_ref_num,
//                  get_early_close_fee(mf.merchant_number)   as adj_amount
//          from    organization            o,
//                  group_merchant          gm,
//                  group_merchant          gm2,
//                  mif                     mf,
//                  groups                  g,
//                  organization            op
//          where   o.org_group = :MES_DIRECT_PORTFOLIO and
//                  gm.org_num  = o.org_num and
//                  gm2.org_num  = :orgId and
//                  gm2.merchant_number = gm.merchant_number and
//                  mf.merchant_number = gm2.merchant_number and
//                  mf.DATE_STAT_CHGD_TO_DCB between :beginDate and :endDate and
//                  ( sysdate - to_date(decode(mf.date_opened,
//                                       '000000','010100',
//                                        mf.date_opened),'mmddyy') ) < (365*3) and
//                  g.assoc_number = (mf.bank_number || mf.dmagent) and
//                  op.org_group = g.group_2                                      
//          order by op.org_group, mf.dba_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  op.org_group                              as portfolio_node,\n                op.org_name                               as portfolio_name,\n                mf.merchant_number                        as merchant_number,\n                mf.dba_name                               as dba_name,\n                mf.activation_date                        as activation_date,\n                to_date(decode(mf.date_opened,\n                               '000000','010100',\n                               mf.date_opened),'mmddyy')  as date_opened,\n                mf.date_stat_chgd_to_dcb                  as date_closed,\n                mf.date_billed_for_close                  as date_billed,\n                 :1                       as dce_ref_num,\n                get_early_close_fee(mf.merchant_number)   as adj_amount\n        from    organization            o,\n                group_merchant          gm,\n                group_merchant          gm2,\n                mif                     mf,\n                groups                  g,\n                organization            op\n        where   o.org_group =  :2  and\n                gm.org_num  = o.org_num and\n                gm2.org_num  =  :3  and\n                gm2.merchant_number = gm.merchant_number and\n                mf.merchant_number = gm2.merchant_number and\n                mf.DATE_STAT_CHGD_TO_DCB between  :4  and  :5  and\n                ( sysdate - to_date(decode(mf.date_opened,\n                                     '000000','010100',\n                                      mf.date_opened),'mmddyy') ) < (365*3) and\n                g.assoc_number = (mf.bank_number || mf.dmagent) and\n                op.org_group = g.group_2                                      \n        order by op.org_group, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AcctClosedBillingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,CLOSED_ACCT_DCE_REF);
   __sJT_st.setLong(2,MES_DIRECT_PORTFOLIO);
   __sJT_st.setLong(3,orgId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AcctClosedBillingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:190^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData(resultSet) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range to rolling previous week
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, -7 );
      
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/