/*@lineinfo:filename=TridentBatchDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/reports/TridentBatchDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-07-25 09:15:41 -0700 (Thu, 25 Jul 2013) $
  Version            : $Revision: 21336 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.api.TridentApiTranBase;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.MesUsers;
import com.mes.constants.mesConstants;
import com.mes.flatfile.FlatFileRecord;
import com.mes.forms.ButtonField;
import com.mes.forms.ComboDateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.Validation;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.StringUtilities;
import com.mes.support.TridentTools;
import com.mes.tools.DropDownTable;
import com.mes.tools.HierarchyTree;
import com.mes.user.UserBean;
import masthead.formats.visak.Batch;
import masthead.formats.visak.BatchPackager;
import masthead.formats.visak.DetailRecord;
import masthead.formats.visak.HeaderRecord;
import masthead.formats.visak.ParameterRecord;
import masthead.formats.visak.RejectedBatchUtil;
import masthead.formats.visak.ResponseRBRecord;
import masthead.formats.visak.TrailerRecord;
import sqlj.runtime.ResultSetIterator;

public class TridentBatchDataBean extends ReportSQLJBean
{
  public static final int  FT_ID_METHOD       = 1;
  public static final int  FT_DATA_SOURCE     = 2;
  public static final int  FT_INPUT_CAP       = 3;
  public static final int  FT_OP_ENV          = 4;
  public static final int  FT_MOTO_IND        = 5;
  public static final int  FT_CARD_PRESENT    = 6;
  public static final int  FT_INP_MODE        = 7;
  public static final int  FT_AUTH_METHOD     = 8;
  public static final int  FT_AUTH_ENTITY     = 9;
  public static final int  FT_DEBIT_NET_IND   = 10;
  
  public static final int  FDT_TRAN_CODE      = 0;
  public static final int  FDT_DATA_SOURCE    = 1;
  public static final int  FDT_CARD_ID_CODE   = 2;
  public static final int  FDT_REQ_ACI        = 3;
  public static final int  FDT_RET_ACI        = 4;
  public static final int  FDT_AUTH_SOURCE    = 5;
  public static final int  FDT_RESPONSE_CODE  = 6;
  public static final int  FDT_AVS_RESULT     = 7;
  public static final int  FDT_TRAN_STATUS    = 8;
  
  public class BatchData 
    implements Comparable
  {
    public    long              BatchId             = 0L;
    public    int               BatchNumber         = 0;
    public    String            BatchResponseCode   = null;
    public    String            BatchResponseDesc   = null;
    public    int               BatchSource         = mesConstants.MBS_BT_UNKNOWN;
    public    Timestamp         BatchTS             = null;
    public    String            DbaName             = null;
    public    String            DbaAddress          = null;
    public    String            DbaCity             = null;
    public    String            DbaState            = null;
    public    String            DbaZip              = null;
    public    String            LoadFilename        = null;
    public    long              MerchantId          = 0L;
    public    double            NetAmount           = 0.0;
    public    String            PortfolioName       = null;
    public    long              PortfolioNode       = 0L;
    public    int               RejectDetailIndex   = -1;     // -1 if hdr/tpr/trl; 1-n if detail #1-n
    public    String            RejectErrorType     = null;
    public    String            RejectField         = null;
    public    String            RejectMessage       = null;
    public    String            RejectRecordType    = null;
    public    String            RejectValue         = null;
    public    String            TerminalId          = null;
    public    int               TotalCount          = 0;
    public    String            TridentBatchId      = null;


    Batch                 Batch               = null;
    
  
    public BatchData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      BatchId             = resultSet.getLong("batch_id");
      BatchNumber         = resultSet.getInt("batch_number");
      BatchResponseCode   = processString(resultSet.getString("response_code"));
      BatchResponseDesc   = processString(resultSet.getString("response_desc"));
      BatchSource         = resultSet.getInt("batch_source");
      BatchTS             = resultSet.getTimestamp("batch_ts");
      DbaName             = processString(resultSet.getString("dba_name"));
      DbaAddress          = processString(resultSet.getString("addr"));
      DbaCity             = processString(resultSet.getString("city"));
      DbaState            = processString(resultSet.getString("state"));
      DbaZip              = processString(resultSet.getString("zip"));
      LoadFilename        = processString(resultSet.getString("load_filename"));
      MerchantId          = resultSet.getLong("merchant_number");
      NetAmount           = resultSet.getDouble("net_amount");
      PortfolioName       = processString(resultSet.getString("portfolio_name"));
      PortfolioNode       = resultSet.getLong("portfolio_node");
      TerminalId          = processString(resultSet.getString("terminal_id"));
      TotalCount          = resultSet.getInt("total_count");
      TridentBatchId      = processString(resultSet.getString("trident_batch_id"));
    }

    public int compareTo( Object obj )
    {
      int         retVal        = 0;
      BatchData   compareObj    = (BatchData)obj;
      
      if ( (retVal = (int)(BatchSource - compareObj.BatchSource)) == 0 )
      {
        if ( (retVal = BatchTS.compareTo(compareObj.BatchTS)) == 0 )
        {
          if ( (retVal = TridentBatchId.compareTo(compareObj.TridentBatchId)) == 0 )
          {
            retVal = (int)(BatchId - compareObj.BatchId);
          }
        }
      }
      return( retVal * -1 );  // descending order
    }
    
    public String getBatchSourceText()
    {
      switch( BatchSource )
      {
        case mesConstants.MBS_BT_VISAK  : return( "visa k"  );
        case mesConstants.MBS_BT_PG     : return( "pg"      );
        case mesConstants.MBS_BT_VITAL  : return( "vital"   );
        case mesConstants.MBS_BT_CIELO  : return( "cielo"   );
        default                         : return( "unknown" );
      }
    }
    
    public DetailRecord getDetailRecord( int recNum )
    {
      DetailRecord    retVal    = null;
      
      try
      {
        retVal = (DetailRecord)(Batch.getDetailRecords()).get(recNum);
      }
      catch( Exception e )
      {
        logEntry( "getDetailRecord(" + BatchId + "," + recNum + ")", e.toString() );
      }
      return( retVal );
    }
    
    public List getDetailRecords()
    {
      return(Batch.getDetailRecords());
    }      
    
    public HeaderRecord getHeader( )
    {
      return( Batch.getHeader() );
    }
    
    public ParameterRecord getParameter( ) 
    {
      return( Batch.getParameter() );
    }
    
    public TrailerRecord getTrailer( )
    {
      return( Batch.getTrailer() );
    }
    
    public void loadBatchDetail( )
      throws Exception
    {
      Blob                  blobData            = null;
      byte[]                buffer              = null;
      int                   chunkSize           = 0;
      int                   dataLen             = 0;
      int                   dataRead            = 0;
      InputStream           is                  = null;
      ResultSetIterator     it                  = null;
      ResultSet             resultSet           = null;
      
      /*@lineinfo:generated-code*//*@lineinfo:221^7*/

//  ************************************************************
//  #sql [Ctx] { select  raw_batch_data 
//          from    trident_capture tc
//          where   tc.rec_id = :BatchId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  raw_batch_data  \n        from    trident_capture tc\n        where   tc.rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,BatchId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   blobData = (java.sql.Blob)__sJT_rs.getBlob(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:226^7*/
      
      // use the blob ref to read the statement data into internal storage
      dataLen   = (int)blobData.length();
      chunkSize = 0;
      dataRead  = 0;
      is        = blobData.getBinaryStream();
      buffer    = new byte[dataLen];
      
      // extract the data from the blob object into 
      // a byte buffer in memory
      while (chunkSize != -1)
      {
        chunkSize = is.read(buffer,dataRead,(dataLen - dataRead));
        dataRead += chunkSize;
      }
      is.close();
      
      // now that we have the data in a byte array use
      // the trident library to extract the batch
      Batch = BatchPackager.parseBatch(buffer);
      
      // 0 == GB, 1 == QD, 2+ == RB..
      try
      {
        ResponseRBRecord  resp      = (ResponseRBRecord) Batch.getResponse();
        
        /*@lineinfo:generated-code*//*@lineinfo:253^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tet.error_desc
//            from    trident_capture_error_types tet
//            where   tet.error_type = :resp.getErrorType()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1306 = resp.getErrorType();
  try {
   String theSqlTS = "select  tet.error_desc\n          from    trident_capture_error_types tet\n          where   tet.error_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1306);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.TridentBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:258^9*/
        resultSet = it.getResultSet();
        if ( resultSet.next() )
        {
          RejectErrorType = processString(resultSet.getString("error_desc"));
        }
        else
        {
          RejectErrorType = processString(resp.getErrorType().trim());
        }
        resultSet.close();
        it.close();
        
        /*@lineinfo:generated-code*//*@lineinfo:271^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  trt.record_desc
//            from    trident_capture_record_types trt
//            where   trt.record_type = :resp.getErrorRecordType()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1307 = resp.getErrorRecordType();
  try {
   String theSqlTS = "select  trt.record_desc\n          from    trident_capture_record_types trt\n          where   trt.record_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1307);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.TridentBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:276^9*/
        resultSet = it.getResultSet();
        if ( resultSet.next() )
        {
          RejectRecordType = processString(resultSet.getString("record_desc"));
        }
        else
        {
          RejectRecordType = processString(resp.getErrorRecordType().trim());
        }
        resultSet.close();
        it.close();
        
        RejectField         = RejectedBatchUtil.getErrorFieldLabel(Batch);
        RejectValue         = RejectedBatchUtil.getErrorFieldValue(Batch);
        RejectMessage       = resp.getErrorData();
        
        try
        {
          int recNum  = Integer.parseInt(resp.getErrorRecordSequenceNumber().trim());

          // batch format: hdr, tpr, detail 1 .. detail n, trailer
          if ( recNum > 2 && recNum < (TotalCount + 3) ) 
          {
            RejectDetailIndex = recNum - 2;
          }
        }
        catch( NumberFormatException nfe )
        {
        }
      }
      catch( ClassCastException cce )
      {
        // ignore, not a RB message
      }
      finally
      {
        try{ it.close(); } catch(Exception sqe){}
      }
    }
  }
  
  public class BatchHoldLogEntry
  {
    public  Timestamp     ActionDate    = null;
    public  String        ActionDesc    = null;
    public  String        ActionUser    = null;
    public  String        DetailIndex   = null;
    
    public BatchHoldLogEntry( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ActionDate    = resultSet.getTimestamp("action_date");
      ActionDesc    = resultSet.getString("action_desc");
      ActionUser    = resultSet.getString("action_user");
      DetailIndex   = (resultSet.getString("detail_index") == null) ? "" : resultSet.getString("detail_index");
    }
  }
  
  public class TDLData
  {
    public  long          AcqRefNum     = 0L;
    public  long          AuthRecId     = 0L;
    public  String        RejectReason  = null;
    public  String        TridentTranId = null;
    
    public TDLData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AcqRefNum     = resultSet.getLong("acq_reference_number");
      AuthRecId     = resultSet.getLong("auth_rec_id");
      RejectReason  = resultSet.getString("reject_reason");
      TridentTranId = resultSet.getString("trident_transaction_id");
    }
  }
  
  public class SummaryData implements Comparable
  {
    public long         HierarchyNode = 0L;
    public long         OrgId         = 0L;
    public String       OrgName       = null;
    public double       TranAmount    = 0.0;  
    public int          TranCount     = 0;  
    
    public SummaryData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      OrgId           = resultSet.getLong("org_num");
      HierarchyNode   = resultSet.getLong("hierarchy_node");
      OrgName         = resultSet.getString("org_name");
      
      addData( resultSet );
    }
    
    public void addData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      TranAmount  += resultSet.getDouble("item_amount");
      TranCount   += resultSet.getInt("item_count");
    }
    
    public int compareTo( Object obj )
    {
      SummaryData   compareObj    = (SummaryData)obj;
      int           retVal        = 0;
      
      if ( ( retVal = OrgName.compareTo(compareObj.OrgName) ) == 0 )
      {
        if ( ( retVal = (int)(HierarchyNode - compareObj.HierarchyNode) ) == 0 )
        {
          retVal = (int)(OrgId - compareObj.OrgId);
        }
      }
      return( retVal );
    }
  }
  
  public class SettlementTypeTable extends DropDownTable
  {
    public SettlementTypeTable( )
    {
      addElement("0","All Batches");
      addElement("1","Held Batches");
      addElement("2","Cancelled Batches");
      addElement("3","Rejected Batches");
    }
  }
  
  public class SearchCriteriaValidation implements Validation
  {
    private   Field       BatchTypeField        = null;
    private   String      ErrorMessage          = null;
    
    public SearchCriteriaValidation( Field batchType )
    {
      BatchTypeField = batchType;
    }
    
    public String getErrorText()
    {
      return( ErrorMessage );
    }
    
    public boolean validate(String fdata)
    {
      String      batchType = BatchTypeField.getData();
      
      ErrorMessage = null;
      if ( batchType.equals("0") && (fdata == null || fdata.equals("")) )
      {
        ErrorMessage = "Must specify search criteria to search All Batches";
      }
      return( ErrorMessage == null );
    }
  }
  
  public class ApiDetail extends TridentApiTranBase
  {
    public String           CardNumberEnc       = null;
    public String           CardType            = null;
    public String           PosEntryModeDesc    = null;
    public String           ProductCode         = null;
    public String           ProductDesc         = null;
    public String           RejectReason        = null;
    public String           TranDesc            = null;
  
    public ApiDetail( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super();
      
      AuthCode              = processString(resultSet.getString("auth_code"));
      AuthResponseCode      = processString(resultSet.getString("auth_response_code"));
      AuthRefNum            = processString(resultSet.getString("auth_ref_num"));
      AuthTranId            = processString(resultSet.getString("auth_tran_id"));
      AuthValCode           = processString(resultSet.getString("auth_val_code"));
      AuthSourceCode        = processString(resultSet.getString("auth_source_code"));
      AuthAvsResponse       = processString(resultSet.getString("auth_avs_resp"));
      AuthAmount            = resultSet.getDouble("auth_amount");
      AuthDate              = resultSet.getDate("auth_date");
      AuthReturnedACI       = processString(resultSet.getString("auth_returned_aci"));
      CardType              = processString(resultSet.getString("card_type"));
      CountryCode           = processString(resultSet.getString("country_code"));
      CurrencyCode          = processString(resultSet.getString("currency_code"));
      CustServicePhone      = processString(resultSet.getString("merchant_phone"));
      DbaName               = processString(resultSet.getString("dba_name"));
      DbaCity               = processString(resultSet.getString("dba_city"));
      DbaState              = processString(resultSet.getString("dba_state"));
      DbaZip                = processString(resultSet.getString("dba_zip"));
      DebitCreditInd        = processString(resultSet.getString("debit_credit_ind"));
      MerchantId            = resultSet.getLong("merchant_number");
      PosEntryMode          = processString(resultSet.getString("pos_entry_mode"));
      PosEntryModeDesc      = processString(resultSet.getString("pos_entry_mode_desc"));
      PurchaseId            = processString(resultSet.getString("purchase_id"));
      ReferenceNumber       = processString(resultSet.getString("reference_number"));
      RecurringPaymentInd   = processString(resultSet.getString("recurring_pmt_ind"));
      RecurringPaymentCount = resultSet.getInt("recurring_pmt_count");
      RecurringPaymentNumber= resultSet.getInt("recurring_pmt_num");
      RejectReason          = processString(resultSet.getString("reject_reason"));
      Sic                   = processString(resultSet.getString("sic_code"));
      TaxAmount             = resultSet.getDouble("tax_amount");
      ProfileId             = processString(resultSet.getString("terminal_id"));
      TranAmount            = resultSet.getDouble("tran_amount");
      TranDate              = resultSet.getDate("tran_date");
      TranType              = processString(resultSet.getString("tran_type"));
      TranDesc              = processString(resultSet.getString("tran_desc"));
      RecId                 = resultSet.getLong("rec_id");

      try { DMContactInfo = processString(resultSet.getString("dm_contact_info")); } catch( java.sql.SQLException sqle ) { }
      try { ProductCode   = processString(resultSet.getString("prod_code"      )); } catch( java.sql.SQLException sqle ) { }
      try { ProductDesc   = processString(resultSet.getString("prod_desc"      )); } catch( java.sql.SQLException sqle ) { }

      setCardNumber( processString(resultSet.getString("card_number")) );
      CardNumberEnc = processString(resultSet.getString("card_number_enc"));
    }
  }
  
  public TridentBatchDataBean( )
  {
    super(true);   // enable field bean support
  }
  
  protected void addSummaryData( ResultSet resultSet )
    throws java.sql.SQLException 
  {
    long          nodeId  = resultSet.getLong("hierarchy_node");
    int           rowIdx  = 0;
    SummaryData   row     = null;
    
    for( rowIdx = 0; rowIdx < ReportRows.size(); ++rowIdx )
    {
      row = (SummaryData)ReportRows.elementAt(rowIdx);
      
      if ( row.HierarchyNode == nodeId )
      {
        row.addData(resultSet);
        break;
      }
    }
    if ( rowIdx >= ReportRows.size() )
    {
      ReportRows.add( new SummaryData(resultSet) );
    }
  }
  
  protected boolean autoSubmit( )
  {
    String        fname       = getAutoSubmitName();
    boolean       retVal      = true;

    if ( fname.equals("holdConfirm") )
    {
      retVal = setBatchFilename( getLong("batchId") , "hold" );
    }
    else if ( fname.equals("releaseConfirm") )
    {
      retVal = setBatchFilename( getLong("batchId") , "release" );
    }
    else if ( fname.equals("cancelConfirm") )
    {
      retVal = setBatchFilename( getLong("batchId") , "cancelled" );
    }
    return( retVal );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup      fgroup    = null;
    
    super.createFields(request);
    
    fgroup = (FieldGroup)getField("searchFields");
    fgroup.add(new DropDownField("batchType","Batch Type", new SettlementTypeTable(), true));
    
    fields.add(new Field("searchValue",50,0,true));
    
    // add validation for the search value
    getField("searchValue").addValidation( new SearchCriteriaValidation( getField("batchType") ) );
    
    // API detail report fields
    fields.add( new Field("batchId",null,32,32,true) );
    fields.add( new Field("batchRecId",null,10,10,true) );
    
    // Hold batch fields
    fields.add(new ButtonField("holdConfirm","Hold Batch") );
    fields.add(new ButtonField("releaseConfirm","Release Batch") );
    fields.add(new ButtonField("cancelConfirm","Cancel Batch") );
    fields.add(new ButtonField("holdCancel","Cancel") );
    fields.add( new Field("batchAction",null,32,32,true) );
    fields.add( new Field("batchSource",null,10,10,true) );
    
    // hidden fields
    fields.add(new HiddenField("hierarchySummary"));
    setData("hierarchySummary","0");
    
    // set html extra
    fields.setHtmlExtra("class=\"formText\"");
  }
  
  public int decodeCieloExtDefType( String blockType )
  {
    int           retVal      = -1;
    String[][]    defMap      = new String[][]
    {
    //   format    flat file def type
      { "0001"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_001    ) },
      { "0002"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_002    ) },
      { "0003"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_003    ) },
      { "0004"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_004    ) },
      { "0005"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_005    ) },
      { "0006"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_006    ) },
      { "0007"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_007    ) },
      { "1008"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_008A   ) },
      { "0009"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_009    ) },
      { "0010"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_010    ) },
      { "0011"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_011    ) },
      { "0012"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_012    ) },
      { "0013"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_013    ) },
      { "0014"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_014    ) },
      { "0015"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_015    ) },
      { "0016"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_016    ) },
      { "2008"  ,String.valueOf(MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_008B   ) },
    };
    
    for( int i = 0; i < defMap.length; ++i )
    {
      String bt      = defMap[i][0];
  
      if ( blockType.equals(bt) )
      {
        retVal = Integer.parseInt(defMap[i][1]);
        break;
      }
    }
    return( retVal );
  }
  
  protected int decodeCieloFlatFileDefType( String cardType, int blockType )
  {
    int           retVal      = -1;
    
    if ( "VS".equals(cardType) && blockType == 0 )
    {
      retVal = MesFlatFiles.DEF_TYPE_DDF_CIELO_VS;
    }
    else if ( "MC".equals(cardType) && blockType == 0 )
    {
      retVal = MesFlatFiles.DEF_TYPE_DDF_CIELO_MC;
    }
    else if ( "VS".equals(cardType) && blockType > 0 )
    {
      retVal = MesFlatFiles.DEF_TYPE_DDF_CIELO_VS_COMPL_BASE;
    }
    else if ( "MC".equals(cardType) && blockType > 0 )
    {
      retVal = MesFlatFiles.DEF_TYPE_DDF_CIELO_MC_COMPL_BASE;
    }
    return( retVal );
  }
  
  protected int decodeFlatFileDefType( String cardType, String formatInd )
  {
    int           retVal      = -1;
    String[][]    defMap      = new String[][]
    {
    //  card     format    flat file def type
      { null  , "AIRL1"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_PT1_REC)             },
      { null  , "AIRL2"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_PT2_REC)             },
      { null  , "CARNT"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_AUTO_RENT_REC)       },
      { null  , "OPTIN"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_AUTO_RENT_REC_OPT)   },
      { null  , "DIRCT"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_DIRECT_MARKET_REC)   },
      { null  , "FLEET"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_FLEET1_REC)          },
      { null  , "FLEE2"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_FLEET2_REC)          },
      { "VS"  , "LODGE"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_VISA_HOTEL_REC)      },
      { null  , "OPTIN"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_HOTEL_REC_OPT)       },
      { null  , "MERCH"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_MERCH_DESC_REC)      },
      { null  , "AMEXR"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_REST_REC)            },
      { null  , "RETAL"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_RETAIL_REC)          },
      { null  , "TEMP1"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_TEMP_HELP1_REC)      },
      { null  , "TEMP2"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_TEMP_HELP2_REC)      },
      { null  , "EXT**"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_GEN_REC)             },
      { "VS"  , "PURC1"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_VISA_PURC1_REC)      },
      { "MC"  , "PURC1"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_MC_PURC1_REC)        },
      { "VS"  , "PURC2"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_VISA_PURC2_REC)      },
      { "MC"  , "PURC2"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_MC_PURC2_REC)        },
      { "VS"  , "SHIP1"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_VISA_SHIP1_REC)      },
      { "VS"  , "SHIP2"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_VISA_SHIP2_REC)      },
      { "MC"  , "SHIP1"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_MC_SHIP1_REC)        },
      { "MC"  , "SHIP2"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_MC_SHIP2_REC)        },
      { "MC"  , "LODGE"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_MC_HOTEL_REC)        },
      { "AM"  , "LODGE"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_AMEX_HOTEL_REC)      },
      { null  , "EXTDD"  ,String.valueOf(MesFlatFiles.DEF_TYPE_CDF_DIRECT_DELIVERY_REC) },
    };
    
    for( int i = 0; i < defMap.length; ++i )
    {
      String ct      = defMap[i][0];
      String fmt     = defMap[i][1];
  
      if ( formatInd.equals(fmt) && (ct == null || ct.equals(cardType)) )
      {
        retVal = Integer.parseInt(defMap[i][2]);
        break;
      }
    }
    return( retVal );
  }
  
  public String decodePosEntryMode( String input )
  {
    String code       = input.trim();
    String entryMode  = code;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:691^7*/

//  ************************************************************
//  #sql [Ctx] { select  pos_entry_desc 
//          from    pos_entry_mode_desc
//          where   pos_entry_code = :code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pos_entry_desc  \n        from    pos_entry_mode_desc\n        where   pos_entry_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,code);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   entryMode = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:696^7*/
    }
    catch(Exception e)
    {
      logEntry("decodePosEntryMode(" + entryMode + ")", e.toString());
    }
    return( entryMode );
  }
  
  public String decodeVisakDesc( int fieldType, DetailRecord detailRec )
  {
    String      retVal      = "";
    
    try
    {
      switch( fieldType )
      {
        case FDT_TRAN_CODE:
        {        
          String tranCode = detailRec.getTransactionCode();
          
          /*@lineinfo:generated-code*//*@lineinfo:717^11*/

//  ************************************************************
//  #sql [Ctx] { select  tcd.transaction_desc 
//              from    visak_tran_code_desc tcd
//              where   tcd.transaction_code = :tranCode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  tcd.transaction_desc  \n            from    visak_tran_code_desc tcd\n            where   tcd.transaction_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tranCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:722^11*/
          break;
        }
        
        case FDT_DATA_SOURCE:
        {        
          String dataSource = detailRec.getAccountDataSource();
          
          /*@lineinfo:generated-code*//*@lineinfo:730^11*/

//  ************************************************************
//  #sql [Ctx] { select  adsd.auth_data_source_desc 
//              from    visak_auth_data_source_desc adsd
//              where   adsd.auth_data_source = :dataSource
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  adsd.auth_data_source_desc  \n            from    visak_auth_data_source_desc adsd\n            where   adsd.auth_data_source =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,dataSource);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:735^11*/
          break;
        }
        
        case FDT_CARD_ID_CODE:
        {
          String cardIdMethod = detailRec.getCardholderIdentificationCode(); 
          
          /*@lineinfo:generated-code*//*@lineinfo:743^11*/

//  ************************************************************
//  #sql [Ctx] { select  cimd.card_id_desc 
//              from    visak_card_id_method_desc cimd
//              where   cimd.card_id_method = :cardIdMethod
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cimd.card_id_desc  \n            from    visak_card_id_method_desc cimd\n            where   cimd.card_id_method =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,cardIdMethod);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:748^11*/
          break;
        }
        
        case FDT_REQ_ACI:
        {        
          String ct     = TridentTools.decodeVisakCardType(detailRec);
          String reqAci = detailRec.getRequestedACI();
          
          if ( ct.equals("VS") || ct.equals("MC") )
          {
            /*@lineinfo:generated-code*//*@lineinfo:759^13*/

//  ************************************************************
//  #sql [Ctx] { select  rad.requested_aci_desc 
//                from    tc33_auth_req_aci_desc  rad
//                where   rad.requested_aci = :reqAci and
//                        rad.card_type = :ct
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  rad.requested_aci_desc  \n              from    tc33_auth_req_aci_desc  rad\n              where   rad.requested_aci =  :1  and\n                      rad.card_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,reqAci);
   __sJT_st.setString(2,ct);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:765^13*/
          }            
          break;
        }          
        
        case FDT_RET_ACI:
        {        
          String retAci = detailRec.getReturnedACI();
          
          /*@lineinfo:generated-code*//*@lineinfo:774^11*/

//  ************************************************************
//  #sql [Ctx] { select  ad.aci_desc 
//              from    tc33_authorization_aci_desc  ad
//              where   ad.aci = :retAci
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ad.aci_desc  \n            from    tc33_authorization_aci_desc  ad\n            where   ad.aci =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,retAci);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:779^11*/
          break;
        }
        
        case FDT_AUTH_SOURCE:
        {        
          String authSource = detailRec.getAuthorizationSourceCode();
          
          /*@lineinfo:generated-code*//*@lineinfo:787^11*/

//  ************************************************************
//  #sql [Ctx] { select  sicd.stand_in_desc 
//              from    tc33_auth_stand_in_code_desc sicd
//              where   sicd.stand_in_code = :authSource
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sicd.stand_in_desc  \n            from    tc33_auth_stand_in_code_desc sicd\n            where   sicd.stand_in_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,authSource);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:792^11*/
          break;
        }          
        
        case FDT_RESPONSE_CODE:
        {        
          String respCode = detailRec.getResponseCode();
          
          /*@lineinfo:generated-code*//*@lineinfo:800^11*/

//  ************************************************************
//  #sql [Ctx] { select  rc.response_desc 
//              from    tc33_authorization_resp_code  rc
//              where   rc.response_code = :respCode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  rc.response_desc  \n            from    tc33_authorization_resp_code  rc\n            where   rc.response_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,respCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:805^11*/
          break;
        }
        
        case FDT_AVS_RESULT:
        {        
          String avsResult = detailRec.getAVSResultCode();
          
          /*@lineinfo:generated-code*//*@lineinfo:813^11*/

//  ************************************************************
//  #sql [Ctx] { select  ac.response_desc 
//              from    tc33_authorization_avs_code ac
//              where   ac.avs_result_code = :avsResult
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ac.response_desc  \n            from    tc33_authorization_avs_code ac\n            where   ac.avs_result_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,avsResult);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:818^11*/
          break;
        }          
        
        case FDT_TRAN_STATUS:
        {        
          String value = detailRec.getTransactionStatusCode();
          int    reversalTransmitted = Integer.parseInt(value.substring(0,1));
          int    protocolCompletionStatus = Integer.parseInt(value.substring(1,2));
          StringBuffer buffer = new StringBuffer();
          
          buffer.append( (reversalTransmitted == 0) ?
                          "No Auth Reversal Txd" :
                          "Auth Reversal Txd" );
                          
          buffer.append( "/" );
                          
          buffer.append( (reversalTransmitted == 0) ?
                          "Host ACK Recvd" :
                          "Host ACK Not Recvd" );
          
          retVal = buffer.toString();                          
          break;
        }          
      }
    }
    catch( Exception e )
    {
      logEntry("WARNING: decodeVisakDesc(" + fieldType + ")", e.toString());
    }
    return( retVal );
  }
  
  public String decodeVisakValue( int fieldType, String visakValue )
  {
    return( decodeVisakValue( "D256", fieldType, visakValue ) );
  }
  
  public String decodeVisakValue( String fmt, int fieldType, String visakValue )
  {
    return( decodeVisakValue(fmt, fieldType,visakValue,null) );
  }
  
  public String decodeVisakValue( String fmt, int fieldType, String visakValue, String defaultValue )
  {
    String      retVal      = defaultValue;
    
    try
    {
      if ( fmt.equals("D256") )
      {
        /*@lineinfo:generated-code*//*@lineinfo:869^9*/

//  ************************************************************
//  #sql [Ctx] { select  fm.d256_value 
//            from    visak_d256_field_map fm
//            where   fm.field_type = :fieldType and
//                    nvl(fm.visak_value,'_space_') = nvl(:visakValue,'_space_')
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  fm.d256_value  \n          from    visak_d256_field_map fm\n          where   fm.field_type =  :1  and\n                  nvl(fm.visak_value,'_space_') = nvl( :2 ,'_space_')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,fieldType);
   __sJT_st.setString(2,visakValue);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:875^9*/
      }
      else if ( fmt.equals("AMEX") )
      {
        /*@lineinfo:generated-code*//*@lineinfo:879^9*/

//  ************************************************************
//  #sql [Ctx] { select  fm.amex_value 
//            from    visak_d256_field_map fm
//            where   fm.field_type = :fieldType and
//                    nvl(fm.visak_value,'_space_') = nvl(:visakValue,'_space_')
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  fm.amex_value  \n          from    visak_d256_field_map fm\n          where   fm.field_type =  :1  and\n                  nvl(fm.visak_value,'_space_') = nvl( :2 ,'_space_')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,fieldType);
   __sJT_st.setString(2,visakValue);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:885^9*/
      }
    }
    catch( Exception e )
    {
      logEntry("decodeVisakValue(" +fmt + "," + fieldType + ",'" + visakValue + "')", e.toString());
    }
    return( retVal );
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    if ( getInt("hierarchySummary") == 1 ) 
    {
      line.setLength(0);
      line.append("\"Org ID\",");
      line.append("\"Org Name\",");
      line.append("\"Item Count\",");
      line.append("\"Net Amount\"");
    }
    else
    {
      line.setLength(0);
      line.append("\"Batch ID\",");
      line.append("\"Merchant ID\",");
      line.append("\"DBA Name\",");
      line.append("\"Portfolio Node\",");
      line.append("\"Portfolio Name\",");
      line.append("\"Batch TS\",");
      line.append("\"Batch Number\",");
      line.append("\"Source\",");
      line.append("\"Outbound Filename\",");
      line.append("\"Item Count\",");
      line.append("\"Net Amount\"");
    }        
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    if ( obj instanceof SummaryData )
    {
      SummaryData record = (SummaryData)obj;
      line.setLength(0);
      line.append(encodeHierarchyNode(record.HierarchyNode));
      line.append(",\"");
      line.append(record.OrgName);
      line.append("\",");
      line.append(record.TranCount);
      line.append(",");
      line.append(record.TranAmount);
    }
    else
    {
      BatchData         record    = (BatchData)obj;
    
      line.append("\"");
      line.append(record.TridentBatchId);
      line.append("\",");
      line.append(encodeHierarchyNode(record.MerchantId));
      line.append(",\"");
      line.append(record.DbaName);
      line.append("\",");
      line.append(encodeHierarchyNode(record.PortfolioNode));
      line.append(",\"");
      line.append(record.PortfolioName);
      line.append("\",\"");
      line.append(DateTimeFormatter.getFormattedDate(record.BatchTS,"MM/dd/yyyy HH:mm:ss"));
      line.append("\",");
      line.append(record.BatchNumber);
      line.append(",\"");
      line.append(record.getBatchSourceText());
      line.append("\",\"");
      line.append((record.BatchResponseCode.equals("0")) ? record.LoadFilename : record.BatchResponseDesc);
      line.append("\",");
      line.append(record.TotalCount);
      line.append(",");
      line.append(record.NetAmount);
    }      
  }
  //'
  public String getDownloadFilenameBase()
  {
    Date              beginDate = getReportDateBegin();
    Date              endDate   = getReportDateEnd();
    StringBuffer      filename  = new StringBuffer("");
    
    if ( getInt("hierarchySummary") == 1 ) 
    {
      filename.append("trident_summary_");
    }
    else
    {
      filename.append("trident_batches_");
    }
            
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(beginDate,"MMddyy") );
    if ( beginDate.equals(endDate) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(endDate,"MMddyy") );
    }      
    
    return ( filename.toString() );
  }
  
  public TDLData getTDLData( long batchId, long batchRecordId )
  {
    ResultSetIterator           it          = null;
    ResultSet                   resultSet   = null;
    TDLData                     tdlData     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1002^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  acq_reference_number,
//                  auth_rec_id,
//                  reject_reason,
//                  trident_transaction_id
//          from    trident_detail_lookup
//          where   batch_rec_id = :batchId
//              and detail_index = :batchRecordId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  acq_reference_number,\n                auth_rec_id,\n                reject_reason,\n                trident_transaction_id\n        from    trident_detail_lookup\n        where   batch_rec_id =  :1 \n            and detail_index =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,batchId);
   __sJT_st.setLong(2,batchRecordId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.TridentBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1011^7*/
      resultSet = it.getResultSet();
      if( resultSet.next() )
      {
        tdlData = new TDLData(resultSet);
      }
      resultSet.close();
    }
    catch(Exception e)
    {
      logEntry("getTDLData(" + batchId + ", " + batchRecordId + ")", e.toString());
    }
    finally
    {
      try{ it.close(); }catch( Exception e ){}
    }
    return( tdlData );
  }
  
  public String getVisakGroupName( int groupId )
  {
    String  groupName   = "Visa K Group " + groupId;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1036^7*/

//  ************************************************************
//  #sql [Ctx] { select  group_desc 
//          from    visak_group_desc
//          where   group_id = :groupId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  group_desc  \n        from    visak_group_desc\n        where   group_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,groupId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   groupName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1041^7*/
    }
    catch(Exception e)
    {
      logEntry("getVisakGroupName(" + groupId + ")", e.toString());
    }
    return( groupName );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public boolean isNonUSCredit( String cardType, String cardNumberEnc, String cardNumberFull )
  {
    boolean     retVal      = false;
    try
    {
      if ( cardType.equals("VS") || cardType.equals("MC") )
      {
        if ( cardNumberFull == null && cardNumberEnc != null )
        {
          cardNumberFull = decryptCardNumberFull( cardNumberEnc, null );
        }

        int     recCount    = 0;
        
        if ( "VS".equals(cardType) )
        {
          /*@lineinfo:generated-code*//*@lineinfo:1078^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//              
//              from    visa_ardef_table    ard
//              where   to_number(substr(:cardNumberFull,1,9)) between ard.range_low and ard.range_high
//                      and not ard.issuer_country_code in ( 'US','CA' )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n             \n            from    visa_ardef_table    ard\n            where   to_number(substr( :1 ,1,9)) between ard.range_low and ard.range_high\n                    and not ard.issuer_country_code in ( 'US','CA' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,cardNumberFull);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1085^11*/  
        }
        else if ( "MC".equals(cardType) )
        {
          /*@lineinfo:generated-code*//*@lineinfo:1089^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//              
//              from    mc_ardef_table      ard
//              where   to_number(rpad(:cardNumberFull,19,'0')) between ard.range_low and ard.range_high
//                      and not ard.country_code_alpha in ( 'USA','CAN' )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n             \n            from    mc_ardef_table      ard\n            where   to_number(rpad( :1 ,19,'0')) between ard.range_low and ard.range_high\n                    and not ard.country_code_alpha in ( 'USA','CAN' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,cardNumberFull);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1096^11*/  
        }
        retVal = (recCount != 0);
      }
    }
    catch( Exception e )
    {
      logEntry( "isNonUSCredit()",e.toString() );
    }
    return( retVal );
  }
  
  public Vector loadBatchHoldHistory( long recId )
  {
    ResultSetIterator     it        = null;
    ResultSet             resultSet = null;
    Vector                retVal    = new Vector();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1116^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  hl.action_type      as action_desc,
//                  hl.action_date      as action_date,
//                  hl.action_user      as action_user,
//                  hl.detail_index     as detail_index
//          from    trident_capture_hold_log    hl
//          where   hl.batch_rec_id = :recId
//          order by action_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  hl.action_type      as action_desc,\n                hl.action_date      as action_date,\n                hl.action_user      as action_user,\n                hl.detail_index     as detail_index\n        from    trident_capture_hold_log    hl\n        where   hl.batch_rec_id =  :1 \n        order by action_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.reports.TridentBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1125^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        retVal.add( new BatchHoldLogEntry(resultSet) );
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("loadBatchHoldHistory(" + recId + ")", e.toString());
    }
    finally
    {
      try{ it.close(); }catch( Exception e ){}
    }
    return( retVal );
  }
  
  public Vector loadCdfTransaction( long batchId, int batchRecordId )
  {
    Date                  batchDate           = null;
    Date                  beginDate           = getReportDateBegin();
    String                cardType            = null;
    int                   defType             = -1;
    Date                  endDate             = getReportDateEnd();
    FlatFileRecord        ffd                 = null;
    ResultSetIterator     it                  = null;
    String                rawData             = null;
    String                recType             = null;
    ResultSet             resultSet           = null;
    Vector                retVal              = new Vector();

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1161^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.trn_seq_num                        as trn_seq_num,
//                  dt.btc_seq_num                        as batch_id,
//                  dt.batch_num                          as batch_number,
//                  dt.batch_date                         as batch_date,
//                  dt.merchant_number                    as merchant_number,
//                  dt.fin_seq_num                        as batch_record_id,
//                  dt.card_type                          as card_type,
//                  dt.sequence_number                    as seq_num,
//                  dt.transaction_code                   as tran_code,
//                  dt.card_number                        as card_number,
//                  dt.card_number_expansion              as card_number_expansion,
//                  dt.tran_date                          as tran_date,
//                  substr(dt.tran_date_expansion,1,1)    as market_specific_data_indic,
//                  substr(dt.tran_date_expansion,2,1)    as tsys_card_type,
//                  dt.prepaid_card_indicator             as prepaid_card_ind,
//                  dt.tran_amount                        as tran_amount,
//                  dt.merch_dba_name                     as dba_name,
//                  dt.merch_city                         as city,
//                  dt.merch_state                        as state,
//                  dt.free_text_flag                     as ext_free_text_flag,
//                  dt.merch_country_code                 as country_code,
//                  dt.mcc                                as mcc,
//                  dt.merch_zip                          as zip,
//                  dt.auth_code                          as auth_code,
//                  dt.acquirer_ref_num_alpha             as acq_ref_num,
//                  dt.auth_source_code                   as auth_source_code,
//                  dt.cardholder_id_method               as cardholder_id_method,
//                  dt.cat_indicator                      as cardholder_act_term_ind,
//                  dt.reimbursement_attr_code            as reimburse_attr_code,
//                  dt.chip_condition_code                as v_chip_cond_code,
//                  dt.moto_ec_indicator                  as moto_ind,
//                  dt.pos_entry_mode                     as pos_entry_mode,
//                  dt.aci                                as aci,
//                  dt.tran_id                            as tran_id,
//                  dt.auth_currency_code                 as auth_currency_code,
//                  dt.auth_amount                        as auth_amount,
//                  dt.val_code                           as val_code,
//                  dt.auth_response_code                 as auth_response_code,
//                  dt.auth_date                          as auth_date,
//                  dt.auth_date_expansion                as auth_date_exp,
//                  dt.debit_net_id                       as debit_net_ind,
//                  dt.switch_settled_indicator           as switch_sel_ind,
//                  dt.extension_rec_id                   as ext_rec_ind,
//                  dt.req_pay_srvcs_indicator            as req_payment_service_ind,
//                  dt.prom_code_tax_amount               as ecomm_goods_tcq_amex_tax,
//                  dt.risk_id_indicator                  as risk_id_ind,
//                  dt.merch_tran_indicator               as merch_tran_ind,
//                  dt.card_acceptor_id                   as card_acceptor_id,
//                  dt.pos_terminal_id                    as pos_tid,
//                  dt.disc_amex_flag                     as disc_amex_flag,
//                  dt.ect_indicator                      as ecomm_tran_ind,
//                  dt.cashback_amount                    as cashback_amount,
//                  dt.store_type                         as store_type,
//                  dt.invoice_num                        as invoice_number,
//                  dt.policy_num                         as policy_number,
//                  dt.insurance_code                     as insurance_code,
//                  dt.service_dev_indicator              as service_dev_ind,
//                  dt.register_num                       as register_number,
//                  dt.original_esid_code                 as org_esid_code,
//                  dt.recur_pay_indicator                as recurring_payment_ind
//          from    daily_detail_file_d256_dt   dt
//          where   dt.btc_seq_num = :batchId
//                  and dt.batch_date between :beginDate and :endDate
//                  and dt.fin_seq_num = :batchRecordId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.trn_seq_num                        as trn_seq_num,\n                dt.btc_seq_num                        as batch_id,\n                dt.batch_num                          as batch_number,\n                dt.batch_date                         as batch_date,\n                dt.merchant_number                    as merchant_number,\n                dt.fin_seq_num                        as batch_record_id,\n                dt.card_type                          as card_type,\n                dt.sequence_number                    as seq_num,\n                dt.transaction_code                   as tran_code,\n                dt.card_number                        as card_number,\n                dt.card_number_expansion              as card_number_expansion,\n                dt.tran_date                          as tran_date,\n                substr(dt.tran_date_expansion,1,1)    as market_specific_data_indic,\n                substr(dt.tran_date_expansion,2,1)    as tsys_card_type,\n                dt.prepaid_card_indicator             as prepaid_card_ind,\n                dt.tran_amount                        as tran_amount,\n                dt.merch_dba_name                     as dba_name,\n                dt.merch_city                         as city,\n                dt.merch_state                        as state,\n                dt.free_text_flag                     as ext_free_text_flag,\n                dt.merch_country_code                 as country_code,\n                dt.mcc                                as mcc,\n                dt.merch_zip                          as zip,\n                dt.auth_code                          as auth_code,\n                dt.acquirer_ref_num_alpha             as acq_ref_num,\n                dt.auth_source_code                   as auth_source_code,\n                dt.cardholder_id_method               as cardholder_id_method,\n                dt.cat_indicator                      as cardholder_act_term_ind,\n                dt.reimbursement_attr_code            as reimburse_attr_code,\n                dt.chip_condition_code                as v_chip_cond_code,\n                dt.moto_ec_indicator                  as moto_ind,\n                dt.pos_entry_mode                     as pos_entry_mode,\n                dt.aci                                as aci,\n                dt.tran_id                            as tran_id,\n                dt.auth_currency_code                 as auth_currency_code,\n                dt.auth_amount                        as auth_amount,\n                dt.val_code                           as val_code,\n                dt.auth_response_code                 as auth_response_code,\n                dt.auth_date                          as auth_date,\n                dt.auth_date_expansion                as auth_date_exp,\n                dt.debit_net_id                       as debit_net_ind,\n                dt.switch_settled_indicator           as switch_sel_ind,\n                dt.extension_rec_id                   as ext_rec_ind,\n                dt.req_pay_srvcs_indicator            as req_payment_service_ind,\n                dt.prom_code_tax_amount               as ecomm_goods_tcq_amex_tax,\n                dt.risk_id_indicator                  as risk_id_ind,\n                dt.merch_tran_indicator               as merch_tran_ind,\n                dt.card_acceptor_id                   as card_acceptor_id,\n                dt.pos_terminal_id                    as pos_tid,\n                dt.disc_amex_flag                     as disc_amex_flag,\n                dt.ect_indicator                      as ecomm_tran_ind,\n                dt.cashback_amount                    as cashback_amount,\n                dt.store_type                         as store_type,\n                dt.invoice_num                        as invoice_number,\n                dt.policy_num                         as policy_number,\n                dt.insurance_code                     as insurance_code,\n                dt.service_dev_indicator              as service_dev_ind,\n                dt.register_num                       as register_number,\n                dt.original_esid_code                 as org_esid_code,\n                dt.recur_pay_indicator                as recurring_payment_ind\n        from    daily_detail_file_d256_dt   dt\n        where   dt.btc_seq_num =  :1 \n                and dt.batch_date between  :2  and  :3 \n                and dt.fin_seq_num =  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,batchId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setInt(4,batchRecordId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.reports.TridentBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1227^7*/
      resultSet = it.getResultSet();
      if ( resultSet.next() )
      {
        batchDate = resultSet.getDate("batch_date");
        cardType  = resultSet.getString("card_type");
        
        ffd = new FlatFileRecord( MesFlatFiles.DEF_TYPE_CDF_FINANCIAL_REC );
        ffd.setAllFieldData(resultSet);
        retVal.addElement(ffd);
      }
      resultSet.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:1241^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  er.*
//          from    daily_detail_file_d256_er   er
//          where   er.fin_seq_num = :batchRecordId
//                  and er.batch_date = :batchDate
//          order by er.sequence_number                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  er.*\n        from    daily_detail_file_d256_er   er\n        where   er.fin_seq_num =  :1 \n                and er.batch_date =  :2 \n        order by er.sequence_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,batchRecordId);
   __sJT_st.setDate(2,batchDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.reports.TridentBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1248^7*/        
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        rawData = StringUtilities.leftJustify("                " + resultSet.getString("data"),256,' ');
        recType = resultSet.getString("format_indicator");
        
        defType = decodeFlatFileDefType(cardType,recType);
        
        if ( defType > 0 )
        {
          ffd = new FlatFileRecord( defType );
          ffd.suck(rawData);
          retVal.addElement(ffd);
        }
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadCdfTransaction(" + batchId + "," + batchRecordId + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
    return( retVal );
  }
  
  public Vector loadCieloTransaction( long batchId, int batchRecordId )
  {
    Date                  beginDate           = getReportDateBegin();
    int                   blockType           = -1;
    String                cardType            = null;
    int                   defType             = -1;
    Date                  endDate             = getReportDateEnd();
    FlatFileRecord        ffd                 = null;
    ResultSetIterator     it                  = null;
    String                rawData             = null;
    ResultSet             resultSet           = null;
    Vector                retVal              = new Vector();

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1294^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(substr(rw.load_filename,6,1),
//                         'v','VS','m','MC',null)    as card_type,
//                  lpad(rw.block_type,4,'0')         as block_type,
//                  rw.raw_data                       as raw_data
//          from    daily_detail_file_cielo_dt    dt,
//                  daily_detail_file_cielo_raw   rw
//          where   dt.batch_date between :beginDate and :endDate
//                  and dt.batch_id = :batchId
//                  and dt.batch_record_id = :batchRecordId
//                  and rw.transaction_key = dt.transaction_key
//          order by rw.block_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(substr(rw.load_filename,6,1),\n                       'v','VS','m','MC',null)    as card_type,\n                lpad(rw.block_type,4,'0')         as block_type,\n                rw.raw_data                       as raw_data\n        from    daily_detail_file_cielo_dt    dt,\n                daily_detail_file_cielo_raw   rw\n        where   dt.batch_date between  :1  and  :2 \n                and dt.batch_id =  :3 \n                and dt.batch_record_id =  :4 \n                and rw.transaction_key = dt.transaction_key\n        order by rw.block_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setLong(3,batchId);
   __sJT_st.setInt(4,batchRecordId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.reports.TridentBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1307^7*/        
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        rawData   = resultSet.getString("raw_data");
        blockType = resultSet.getInt   ("block_type");
        cardType  = resultSet.getString("card_type");
        
        defType = decodeCieloFlatFileDefType(cardType,blockType);
        
        if ( defType > 0 )
        {
          ffd = new FlatFileRecord( defType );
          ffd.suck(rawData);
          retVal.addElement(ffd);
        }
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadCieloTransaction(" + batchId + "," + batchRecordId + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
    return( retVal );
  }
  
  public void loadData( )
  {
    loadData( mesConstants.MBS_BT_UNKNOWN, 0 );
  }

  public BatchData loadData( int batchSource, long batchId )
  {
    Date                beginDate           = getReportDateBegin();
    Date                endDate             = getReportDateEnd();
    long                nodeId              = getReportHierarchyNode();
    long                orgId               = getReportOrgId();
    long                reportNode          = getReportHierarchyNodeDefault();

    BatchData           batchSummary        = null;
    String              crlf                = "\r\n";
    ResultSetIterator   it                  = null;
    ResultSet           resultSet           = null;


    try
    {
      // empty the current contents
      ReportRows.clear();

      if ( beginDate == null )
      {
        Calendar cal = Calendar.getInstance();
        endDate = new java.sql.Date(cal.getTime().getTime());
        cal.add(Calendar.DAY_OF_MONTH,-1);
        beginDate = new java.sql.Date(cal.getTime().getTime());
      }

      boolean   searchMbsBt       = true;
      boolean   searchVital       = true;
      String    whereClauseMbsBt  = "";
      String    whereClauseVital  = "";

      long      longLookup      = -1L;
      int       recCount        = 0;


      if( batchSource == mesConstants.MBS_BT_UNKNOWN )  // all sources
      {
        int       batchType       = getInt("batchType");
        String    stringLookup    = getData("searchValue").toUpperCase().trim();


        // only execute the search if the user provided search criteria
        if ( !stringLookup.equals("") || batchType != 0 )
        {
          switch( batchType )
          {
            case 1:
              whereClauseMbsBt += " and   mb.load_filename like 'hold-%' "      + crlf;
              whereClauseVital += " and   dt.load_filename like 'hold-%' "      + crlf;
              break;
            case 2:
              whereClauseMbsBt += " and   mb.load_filename like 'cancelled-%' " + crlf;
              whereClauseVital += " and   dt.load_filename like 'cancelled-%' " + crlf;
              break;
            case 3:
              whereClauseMbsBt += " and   mb.response_code > 0 " + crlf;
              searchVital       = false;
              break;
          }
          if ( !stringLookup.equals("") )
          {
            long loadFileId = -1L;
            try
            {
              // if search value is a load filename, then set things up to run fast
              /*@lineinfo:generated-code*//*@lineinfo:1410^15*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(lower(:stringLookup))
//                  
//                  from    dual
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id(lower( :1 ))\n                 \n                from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,stringLookup);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1415^15*/
              
              whereClauseMbsBt += " and mb.load_file_id = " + loadFileId;
            }
            catch( Exception e )
            {
              loadFileId = -1L;
            }
            
            if ( loadFileId <= 0L )
            {
              try
              {
                longLookup = Long.parseLong(stringLookup);

                // if search value is a merchant number then set things up to run fast
                /*@lineinfo:generated-code*//*@lineinfo:1431^17*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//                    
//                    from    mif
//                    where   merchant_number = :longLookup
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n                   \n                  from    mif\n                  where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.reports.TridentBatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1437^17*/
              }
              catch( Exception e )
              {
                longLookup = -1L;
              }
            
              String  tempClause = null;
              if( recCount == 0 )   // if this is not a merchant number
              {
                stringLookup    = (" '%" + stringLookup + "%' ");

                whereClauseMbsBt += " and ( mb.trident_batch_id  like " + stringLookup + crlf +
                                    "    or mb.terminal_id       like " + stringLookup + crlf;
                whereClauseVital += " and ( dt.pos_terminal_id   like " + stringLookup + crlf;

                if( longLookup < 0 )  // if there is at least one non-digit in the search string
                  tempClause = " or upper(mf.dba_name) like " + stringLookup + " ) ";
                else
                  tempClause =                                                 " ) ";
              }
              else
              {
                tempClause = " and mf.merchant_number = " + longLookup;
              }

              whereClauseMbsBt += tempClause + crlf;
              whereClauseVital += tempClause + crlf;
            }
          }
        }
        else
        {
          searchMbsBt       = false;
          searchVital       = false;
        }
      }
      else
      {
        switch( batchSource )
        {
          case mesConstants.MBS_BT_VISAK:
          case mesConstants.MBS_BT_PG:
          case mesConstants.MBS_BT_CIELO:
            whereClauseMbsBt  = " and mb.batch_id = "     + batchId + crlf;
            searchVital       = false;
            break;

          case mesConstants.MBS_BT_VITAL:
            searchMbsBt       = false;
            whereClauseVital  = " and dt.btc_seq_num = "  + batchId + crlf;
            break;
        }
      }

      String    selectClauseCommon =        // must be "from mif mf, organization o" etc
                    " o.org_group                       as portfolio_node,      " + crlf +
                    " o.org_name                        as portfolio_name,      " + crlf +
                    " mf.merchant_number                as merchant_number,     " + crlf +
                    " mf.dba_name                       as dba_name,            " + crlf +
                    " mf.addr1_line_1                   as addr,                " + crlf +
                    " mf.dmcity                         as city,                " + crlf +
                    " mf.dmstate                        as state,               " + crlf +
                    " substr(mf.dmzip,1,5)              as zip,                 " + crlf;

      String    whereClauseCommon =         // must be "from mif mf, organization o" etc
                    " and o.org_group = ( mf.bank_number ||                     " + crlf +
                    "                       decode( mf.bank_number,             " + crlf +
                    "                             3941,mf.group_2_association,  " + crlf +
                    "                                  '00000') )               " + crlf +
                    " and exists  -- node has access                            " + crlf +
                    " (                                                         " + crlf +
                    "   select  gm.merchant_number                              " + crlf +
                    "   from    organization    oi,                             " + crlf +
                    "           group_merchant  gm                              " + crlf +
                    "   where   oi.org_group = " + reportNode                     + crlf +
                    "           and gm.org_num = oi.org_num                     " + crlf +
                    "           and gm.merchant_number = mf.merchant_number     " + crlf +
                    " )                                                         " + crlf;


      if( searchMbsBt )
      {
        // load mbs_batches batches.....
        /*@lineinfo:generated-code*//*@lineinfo:1521^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  :selectClauseCommon
//                    mb.terminal_id                    as terminal_id,
//                    mb.response_code                  as response_code,
//                    tcr.response_desc                 as response_desc,
//                    mb.trident_batch_id               as trident_batch_id,
//                    mb.batch_id                       as batch_id,
//                    mb.batch_number                   as batch_number,
//                    mb.mbs_batch_type                 as batch_source,
//                    mb.load_filename                  as load_filename,
//                    mb.mesdb_timestamp                as batch_ts,
//                    mb.total_count                    as total_count,
//                    mb.net_amount                     as net_amount
//            from    mbs_batches                   mb,
//                    trident_capture_responses     tcr,
//                    mif                           mf,
//                    organization                  o
//            where   mb.merchant_batch_date between :beginDate and :endDate
//                    :whereClauseMbsBt
//                    and tcr.response_code(+)  = mb.response_code
//                    and mf.merchant_number    = mb.merchant_number
//                    :whereClauseCommon
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select   ");
   __sjT_sb.append(selectClauseCommon);
   __sjT_sb.append(" \n                  mb.terminal_id                    as terminal_id,\n                  mb.response_code                  as response_code,\n                  tcr.response_desc                 as response_desc,\n                  mb.trident_batch_id               as trident_batch_id,\n                  mb.batch_id                       as batch_id,\n                  mb.batch_number                   as batch_number,\n                  mb.mbs_batch_type                 as batch_source,\n                  mb.load_filename                  as load_filename,\n                  mb.mesdb_timestamp                as batch_ts,\n                  mb.total_count                    as total_count,\n                  mb.net_amount                     as net_amount\n          from    mbs_batches                   mb,\n                  trident_capture_responses     tcr,\n                  mif                           mf,\n                  organization                  o\n          where   mb.merchant_batch_date between  ?  and  ? \n                   ");
   __sjT_sb.append(whereClauseMbsBt);
   __sjT_sb.append(" \n                  and tcr.response_code(+)  = mb.response_code\n                  and mf.merchant_number    = mb.merchant_number\n                   ");
   __sjT_sb.append(whereClauseCommon);
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "24com.mes.reports.TridentBatchDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1544^9*/
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          ReportRows.addElement( new BatchData( resultSet ) );
        }
        it.close();   // this will also close the resultSet
      }


      if( searchVital )
      {
        // load vital batches.....
        /*@lineinfo:generated-code*//*@lineinfo:1557^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ use_hash(o,dt,mf) */
//                    :selectClauseCommon
//                    'V' || dt.pos_terminal_id         as terminal_id,
//                    '0'                               as response_code,
//                    'GB'                              as response_desc,
//                    ( 'V' || dt.pos_terminal_id
//                      || '.' || to_char(dt.batch_date,'mmdd')
//                      || '.' || lpad(dt.batch_num,3,'0')
//                      || '-' || dt.btc_seq_num )      as trident_batch_id,
//                    dt.btc_seq_num                    as batch_id,
//                    dt.batch_num                      as batch_number,
//                    :mesConstants.MBS_BT_VITAL      as batch_source,
//                    dt.load_filename                  as load_filename,
//                    dt.batch_date                     as batch_ts,
//                    count(1)                          as total_count,
//                    sum(decode(dt.debit_credit_indicator,'C',-1,1)
//                        * dt.tran_amount)             as net_amount
//            from    daily_detail_file_d256_dt     dt,
//                    mif                           mf,
//                    organization                  o
//            where   dt.batch_date between :beginDate and :endDate
//                    :whereClauseVital
//                    and mf.merchant_number = dt.merchant_number
//                    :whereClauseCommon
//            group by o.org_group, o.org_name,
//                     dt.pos_terminal_id,
//                     mf.merchant_number,
//                     mf.dba_name,
//                     mf.addr1_line_1,
//                     mf.dmcity,
//                     mf.dmstate,
//                     mf.dmzip,
//                     mf.dmstate,
//                     dt.batch_date,
//                     dt.btc_seq_num,
//                     dt.batch_num,
//                     dt.load_filename
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  /*+ use_hash(o,dt,mf) */\n                   ");
   __sjT_sb.append(selectClauseCommon);
   __sjT_sb.append(" \n                  'V' || dt.pos_terminal_id         as terminal_id,\n                  '0'                               as response_code,\n                  'GB'                              as response_desc,\n                  ( 'V' || dt.pos_terminal_id\n                    || '.' || to_char(dt.batch_date,'mmdd')\n                    || '.' || lpad(dt.batch_num,3,'0')\n                    || '-' || dt.btc_seq_num )      as trident_batch_id,\n                  dt.btc_seq_num                    as batch_id,\n                  dt.batch_num                      as batch_number,\n                   ?       as batch_source,\n                  dt.load_filename                  as load_filename,\n                  dt.batch_date                     as batch_ts,\n                  count(1)                          as total_count,\n                  sum(decode(dt.debit_credit_indicator,'C',-1,1)\n                      * dt.tran_amount)             as net_amount\n          from    daily_detail_file_d256_dt     dt,\n                  mif                           mf,\n                  organization                  o\n          where   dt.batch_date between  ?  and  ? \n                   ");
   __sjT_sb.append(whereClauseVital);
   __sjT_sb.append(" \n                  and mf.merchant_number = dt.merchant_number\n                   ");
   __sjT_sb.append(whereClauseCommon);
   __sjT_sb.append(" \n          group by o.org_group, o.org_name,\n                   dt.pos_terminal_id,\n                   mf.merchant_number,\n                   mf.dba_name,\n                   mf.addr1_line_1,\n                   mf.dmcity,\n                   mf.dmstate,\n                   mf.dmzip,\n                   mf.dmstate,\n                   dt.batch_date,\n                   dt.btc_seq_num,\n                   dt.batch_num,\n                   dt.load_filename");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "25com.mes.reports.TridentBatchDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.MBS_BT_VITAL);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1596^9*/
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          ReportRows.addElement( new BatchData( resultSet ) );
        }
        it.close();   // this will also close the resultSet
      }

      if( ReportRows.size() == 1 )
      {
        batchSummary = (BatchData) ReportRows.elementAt(0);
        if( batchSummary != null && batchSource == mesConstants.MBS_BT_VISAK )
        {
          batchSummary.loadBatchDetail();  // load using this context
        }
      }
    }
    catch( Exception e )
    {
      logEntry( "loadData(" + batchSource + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
    return( batchSummary );
  }
  
  public void loadDataApiDetails( )
  {
    loadDataApiDetails( getReportHierarchyNode(), getReportDateBegin(), getReportDateEnd() );
  }
  
  protected void loadDataApiDetails( long nodeId, Date beginDate, Date endDate )
  {
    long                batchId       = getLong("batchId");
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;

    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:1641^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tapi.rec_id                   as rec_id,
//                  -- merchant data
//                  tapi.terminal_id              as terminal_id,
//                  tapi.merchant_number          as merchant_number, 
//                  tapi.dba_name                 as dba_name, 
//                  tapi.dba_city                 as dba_city, 
//                  tapi.dba_state                as dba_state, 
//                  tapi.dba_zip                  as dba_zip, 
//                  tapi.phone_number             as merchant_phone, 
//                  nvl(tapi.country_code,
//                      decode(tp.addr_state,
//                             'PR','PR',
//                             'US'))             as country_code,
//                  tapi.sic_code                 as sic_code, 
//                  -- transaction data
//                  tapi.card_number              as card_number, 
//                  tapi.card_number_enc          as card_number_enc,
//                  tapi.card_type                as card_type,
//                  tapi.currency_code            as currency_code,
//                  tapi.debit_credit_indicator   as debit_credit_ind, 
//                  tapi.transaction_date         as tran_date, 
//                  ( tapi.transaction_amount *
//                    decode(tapi.debit_credit_indicator,
//                           'C',-1,1) )          as tran_amount, 
//                  tapi.reference_number         as reference_number, 
//                  tapi.pos_entry_mode           as pos_entry_mode, 
//                  nvl(emd.pos_entry_desc,
//                      tapi.pos_entry_mode)      as pos_entry_mode_desc, 
//                  tapi.reject_reason            as reject_reason,
//                  -- auth data
//                  tapi.auth_response_code       as auth_response_code, 
//                  tapi.auth_ref_num             as auth_ref_num, 
//                  tapi.auth_tran_id             as auth_tran_id, 
//                  tapi.auth_val_code            as auth_val_code, 
//                  tapi.auth_code                as auth_code,
//                  tapi.auth_source_code         as auth_source_code,
//                  tapi.auth_avs_response        as auth_avs_resp,
//                  tapi.auth_amount              as auth_amount,
//                  tapi.auth_date                as auth_date,
//                  tapi.auth_returned_aci        as auth_returned_aci,
//                  -- level II data
//                  tapi.tax_amount               as tax_amount, 
//                  tapi.purchase_id              as purchase_id, 
//                  tapi.ship_to_zip              as ship_to_zip,
//                  tapi.shipping_amount          as shipping_amount,
//                  -- recurring transaction data
//                  tapi.recurring_payment_ind    as recurring_pmt_ind,
//                  tapi.recurring_payment_number as recurring_pmt_num,
//                  tapi.recurring_payment_count  as recurring_pmt_count,
//                  -- system data
//                  tapi.transaction_type         as tran_type,
//                  nvl(tt.tran_desc,
//                      tapi.transaction_type)    as tran_desc
//          from    trident_capture_api             tapi,
//                  trident_profile                 tp,
//                  trident_capture_api_tran_type   tt,
//                  pos_entry_mode_desc             emd
//          where   tapi.batch_id = :batchId and
//                  tapi.mesdb_timestamp between (:beginDate-90) and (:endDate+1) and
//                  tp.terminal_id = tapi.terminal_id and
//                  tt.tran_type(+) = tapi.transaction_type and
//                  tapi.transaction_type != 'V' and
//                  emd.pos_entry_code(+) = tapi.pos_entry_mode
//          order by tapi.rec_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tapi.rec_id                   as rec_id,\n                -- merchant data\n                tapi.terminal_id              as terminal_id,\n                tapi.merchant_number          as merchant_number, \n                tapi.dba_name                 as dba_name, \n                tapi.dba_city                 as dba_city, \n                tapi.dba_state                as dba_state, \n                tapi.dba_zip                  as dba_zip, \n                tapi.phone_number             as merchant_phone, \n                nvl(tapi.country_code,\n                    decode(tp.addr_state,\n                           'PR','PR',\n                           'US'))             as country_code,\n                tapi.sic_code                 as sic_code, \n                -- transaction data\n                tapi.card_number              as card_number, \n                tapi.card_number_enc          as card_number_enc,\n                tapi.card_type                as card_type,\n                tapi.currency_code            as currency_code,\n                tapi.debit_credit_indicator   as debit_credit_ind, \n                tapi.transaction_date         as tran_date, \n                ( tapi.transaction_amount *\n                  decode(tapi.debit_credit_indicator,\n                         'C',-1,1) )          as tran_amount, \n                tapi.reference_number         as reference_number, \n                tapi.pos_entry_mode           as pos_entry_mode, \n                nvl(emd.pos_entry_desc,\n                    tapi.pos_entry_mode)      as pos_entry_mode_desc, \n                tapi.reject_reason            as reject_reason,\n                -- auth data\n                tapi.auth_response_code       as auth_response_code, \n                tapi.auth_ref_num             as auth_ref_num, \n                tapi.auth_tran_id             as auth_tran_id, \n                tapi.auth_val_code            as auth_val_code, \n                tapi.auth_code                as auth_code,\n                tapi.auth_source_code         as auth_source_code,\n                tapi.auth_avs_response        as auth_avs_resp,\n                tapi.auth_amount              as auth_amount,\n                tapi.auth_date                as auth_date,\n                tapi.auth_returned_aci        as auth_returned_aci,\n                -- level II data\n                tapi.tax_amount               as tax_amount, \n                tapi.purchase_id              as purchase_id, \n                tapi.ship_to_zip              as ship_to_zip,\n                tapi.shipping_amount          as shipping_amount,\n                -- recurring transaction data\n                tapi.recurring_payment_ind    as recurring_pmt_ind,\n                tapi.recurring_payment_number as recurring_pmt_num,\n                tapi.recurring_payment_count  as recurring_pmt_count,\n                -- system data\n                tapi.transaction_type         as tran_type,\n                nvl(tt.tran_desc,\n                    tapi.transaction_type)    as tran_desc\n        from    trident_capture_api             tapi,\n                trident_profile                 tp,\n                trident_capture_api_tran_type   tt,\n                pos_entry_mode_desc             emd\n        where   tapi.batch_id =  :1  and\n                tapi.mesdb_timestamp between ( :2 -90) and ( :3 +1) and\n                tp.terminal_id = tapi.terminal_id and\n                tt.tran_type(+) = tapi.transaction_type and\n                tapi.transaction_type != 'V' and\n                emd.pos_entry_code(+) = tapi.pos_entry_mode\n        order by tapi.rec_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,batchId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"26com.mes.reports.TridentBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1707^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement( new ApiDetail( resultSet ) );
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadDataApiDetails",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
    }
  }
  
  public void loadDataCieloDetails( )
  {
    loadDataCieloDetails( getReportHierarchyNode(), getReportDateBegin(), getReportDateEnd() );
  }
  
  protected void loadDataCieloDetails( long nodeId, Date beginDate, Date endDate )
  {
    long                batchId       = getLong("batchId");
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;

    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:1741^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.batch_record_id            as rec_id,
//                  -- merchant data
//                  dt.catid                      as terminal_id,
//                  dt.merchant_number            as merchant_number, 
//                  dt.dba_name                   as dba_name, 
//                  dt.dba_city                   as dba_city, 
//                  null                          as dba_state, 
//                  dt.dba_zip                    as dba_zip, 
//                  null                          as merchant_phone, 
//                  dt.country_code               as country_code,
//                  dt.currency_code              as currency_code,
//                  dt.sic_code                   as sic_code, 
//                  -- transaction data
//                  dt.card_number                as card_number, 
//                  dt.card_number_enc            as card_number_enc,
//                  dt.cielo_product              as prod_code,
//                  pc.product_desc               as prod_desc,
//                  nvl(pc.card_type,dt.cielo_product)
//                                                as card_type,
//                  dt.debit_credit_indicator     as debit_credit_ind, 
//                  dt.transaction_date           as tran_date, 
//                  ( decode(dt.debit_credit_indicator,'C',-1,1)
//                    * dt.transaction_amount )   as tran_amount, 
//                  dt.acq_reference_number       as reference_number, 
//                  dt.pos_entry_mode             as pos_entry_mode, 
//                  nvl(emd.pos_entry_desc,
//                      dt.pos_entry_mode)        as pos_entry_mode_desc, 
//                  null                          as reject_reason,
//                  -- auth data
//                  dt.auth_response_code         as auth_response_code, 
//                  dt.auth_retrieval_ref_num     as auth_ref_num, 
//                  dt.auth_tran_id               as auth_tran_id, 
//                  dt.auth_val_code              as auth_val_code, 
//                  dt.auth_code                  as auth_code,
//                  dt.auth_source_code           as auth_source_code,
//                  null                          as auth_avs_resp,
//                  dt.auth_amount                as auth_amount,
//                  dt.auth_date                  as auth_date,
//                  dt.auth_returned_aci          as auth_returned_aci,
//                  -- level II data
//                  null                          as tax_amount, 
//                  dt.purchase_id                as purchase_id, 
//                  null                          as ship_to_zip,
//                  null                          as shipping_amount,
//                  -- recurring transaction data                        
//                  dt.recurring_indicator        as recurring_pmt_ind,
//                  null                          as recurring_pmt_num,
//                  null                          as recurring_pmt_count,
//                  -- system data
//                  dt.debit_credit_indicator     as tran_type,
//                  decode(dt.debit_credit_indicator,
//                         'C','Credit','Sale')   as tran_desc
//          from    daily_detail_file_cielo_dt      dt,
//                  pos_entry_mode_desc             emd,
//                  cielo_product_codes             pc
//          where   dt.batch_date between :beginDate and :endDate and
//                  dt.batch_id = :batchId and
//                  emd.pos_entry_code(+) = dt.pos_entry_mode
//                  and pc.cielo_product(+) = dt.cielo_product
//          order by dt.transaction_key
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.batch_record_id            as rec_id,\n                -- merchant data\n                dt.catid                      as terminal_id,\n                dt.merchant_number            as merchant_number, \n                dt.dba_name                   as dba_name, \n                dt.dba_city                   as dba_city, \n                null                          as dba_state, \n                dt.dba_zip                    as dba_zip, \n                null                          as merchant_phone, \n                dt.country_code               as country_code,\n                dt.currency_code              as currency_code,\n                dt.sic_code                   as sic_code, \n                -- transaction data\n                dt.card_number                as card_number, \n                dt.card_number_enc            as card_number_enc,\n                dt.cielo_product              as prod_code,\n                pc.product_desc               as prod_desc,\n                nvl(pc.card_type,dt.cielo_product)\n                                              as card_type,\n                dt.debit_credit_indicator     as debit_credit_ind, \n                dt.transaction_date           as tran_date, \n                ( decode(dt.debit_credit_indicator,'C',-1,1)\n                  * dt.transaction_amount )   as tran_amount, \n                dt.acq_reference_number       as reference_number, \n                dt.pos_entry_mode             as pos_entry_mode, \n                nvl(emd.pos_entry_desc,\n                    dt.pos_entry_mode)        as pos_entry_mode_desc, \n                null                          as reject_reason,\n                -- auth data\n                dt.auth_response_code         as auth_response_code, \n                dt.auth_retrieval_ref_num     as auth_ref_num, \n                dt.auth_tran_id               as auth_tran_id, \n                dt.auth_val_code              as auth_val_code, \n                dt.auth_code                  as auth_code,\n                dt.auth_source_code           as auth_source_code,\n                null                          as auth_avs_resp,\n                dt.auth_amount                as auth_amount,\n                dt.auth_date                  as auth_date,\n                dt.auth_returned_aci          as auth_returned_aci,\n                -- level II data\n                null                          as tax_amount, \n                dt.purchase_id                as purchase_id, \n                null                          as ship_to_zip,\n                null                          as shipping_amount,\n                -- recurring transaction data                        \n                dt.recurring_indicator        as recurring_pmt_ind,\n                null                          as recurring_pmt_num,\n                null                          as recurring_pmt_count,\n                -- system data\n                dt.debit_credit_indicator     as tran_type,\n                decode(dt.debit_credit_indicator,\n                       'C','Credit','Sale')   as tran_desc\n        from    daily_detail_file_cielo_dt      dt,\n                pos_entry_mode_desc             emd,\n                cielo_product_codes             pc\n        where   dt.batch_date between  :1  and  :2  and\n                dt.batch_id =  :3  and\n                emd.pos_entry_code(+) = dt.pos_entry_mode\n                and pc.cielo_product(+) = dt.cielo_product\n        order by dt.transaction_key";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setLong(3,batchId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"27com.mes.reports.TridentBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1803^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement( new ApiDetail( resultSet ) );
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadDataCieloDetails",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
    }
  }
  
  public void loadDataHierarchySummary( )
  {
    loadDataSummary( getReportOrgId(), getReportDateBegin(), getReportDateEnd() );
  }

  protected void loadDataSummary( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it            = null;
    long                nodeId        = orgIdToHierarchyNode(orgId);
    ResultSet           resultSet     = null;

    try
    {
      int batchType = getInt("batchType");
      /*@lineinfo:generated-code*//*@lineinfo:1835^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num                     as org_num,
//                  o.org_group                   as hierarchy_node,
//                  o.org_name                    as org_name,
//                  sum(mb.total_count)           as item_count,
//                  sum(mb.net_amount)            as item_amount
//          from    parent_org              po,
//                  group_merchant          gm,
//                  mbs_batches             mb,
//                  organization            o
//          where   po.parent_org_num = :orgId and 
//                  gm.org_num = o.org_num and
//                  mb.merchant_number = gm.merchant_number and
//                  mb.merchant_batch_date between :beginDate and :endDate and
//                  (
//                    :batchType = 0
//                    or ( :batchType = 1 and mb.load_filename like 'hold-%' )
//                    or ( :batchType = 2 and mb.load_filename like 'cancelled-%' )
//                    or ( :batchType = 3 and mb.response_code > 0 )  -- QDs & RBs
//                  ) and
//                  o.org_num = po.org_num 
//          group by o.org_num, o.org_group, o.org_name
//          order by o.org_name, o.org_group
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num                     as org_num,\n                o.org_group                   as hierarchy_node,\n                o.org_name                    as org_name,\n                sum(mb.total_count)           as item_count,\n                sum(mb.net_amount)            as item_amount\n        from    parent_org              po,\n                group_merchant          gm,\n                mbs_batches             mb,\n                organization            o\n        where   po.parent_org_num =  :1  and \n                gm.org_num = o.org_num and\n                mb.merchant_number = gm.merchant_number and\n                mb.merchant_batch_date between  :2  and  :3  and\n                (\n                   :4  = 0\n                  or (  :5  = 1 and mb.load_filename like 'hold-%' )\n                  or (  :6  = 2 and mb.load_filename like 'cancelled-%' )\n                  or (  :7  = 3 and mb.response_code > 0 )  -- QDs & RBs\n                ) and\n                o.org_num = po.org_num \n        group by o.org_num, o.org_group, o.org_name\n        order by o.org_name, o.org_group";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setInt(4,batchType);
   __sJT_st.setInt(5,batchType);
   __sJT_st.setInt(6,batchType);
   __sJT_st.setInt(7,batchType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"28com.mes.reports.TridentBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1859^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        addSummaryData(resultSet);
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadDataSummary",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  public void loadDataVitalDetails( )
  {
    loadDataVitalDetails( getReportHierarchyNode(), getReportDateBegin(), getReportDateEnd() );
  }
  
  protected void loadDataVitalDetails( long nodeId, Date beginDate, Date endDate )
  {
    long                batchId       = getLong("batchId");
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;

    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:1895^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.fin_seq_num                as rec_id,
//                  -- merchant data
//                  'V' || dt.pos_terminal_id     as terminal_id,
//                  dt.merchant_number            as merchant_number, 
//                  dt.merch_dba_name             as dba_name, 
//                  dt.merch_city                 as dba_city, 
//                  dt.merch_state                as dba_state, 
//                  dt.merch_zip                  as dba_zip, 
//                  null                          as merchant_phone, 
//                  dt.merch_country_code         as country_code,
//                  dt.auth_currency_code         as currency_code,
//                  dt.mcc                        as sic_code, 
//                  -- transaction data
//                  dt.card_number                as card_number, 
//                  dt.card_number_enc            as card_number_enc,
//                  dt.card_type                  as card_type,
//                  dt.debit_credit_indicator     as debit_credit_ind, 
//                  dt.tran_date                  as tran_date, 
//                  ( decode(dt.debit_credit_indicator,'C',-1,1)
//                    * dt.tran_amount )          as tran_amount, 
//                  dt.acquirer_ref_num           as reference_number, 
//                  dt.pos_entry_mode             as pos_entry_mode, 
//                  nvl(emd.pos_entry_desc,
//                      dt.pos_entry_mode)        as pos_entry_mode_desc, 
//                  null                          as reject_reason,
//                  -- auth data
//                  dt.auth_response_code         as auth_response_code, 
//                  null                          as auth_ref_num, 
//                  dt.tran_id                    as auth_tran_id, 
//                  dt.val_code                   as auth_val_code, 
//                  dt.auth_code                  as auth_code,
//                  dt.auth_source_code           as auth_source_code,
//                  null                          as auth_avs_resp,
//                  dt.auth_amount                as auth_amount,
//                  dt.auth_date                  as auth_date,
//                  dt.aci                        as auth_returned_aci,
//                  -- level II data
//                  null                          as tax_amount, 
//                  dt.invoice_num                as purchase_id, 
//                  null                          as ship_to_zip,
//                  null                          as shipping_amount,
//                  -- recurring transaction data                        
//                  null                          as recurring_pmt_ind,
//                  null                          as recurring_pmt_num,
//                  null                          as recurring_pmt_count,
//                  -- system data
//                  dt.debit_credit_indicator     as tran_type,
//                  decode(dt.debit_credit_indicator,
//                         'C','Credit','Sale')   as tran_desc
//          from    daily_detail_file_d256_dt       dt,
//                  pos_entry_mode_desc             emd
//          where   dt.btc_seq_num = :batchId and
//                  dt.batch_date between :beginDate and :endDate and
//                  emd.pos_entry_code(+) = dt.pos_entry_mode
//          order by dt.fin_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.fin_seq_num                as rec_id,\n                -- merchant data\n                'V' || dt.pos_terminal_id     as terminal_id,\n                dt.merchant_number            as merchant_number, \n                dt.merch_dba_name             as dba_name, \n                dt.merch_city                 as dba_city, \n                dt.merch_state                as dba_state, \n                dt.merch_zip                  as dba_zip, \n                null                          as merchant_phone, \n                dt.merch_country_code         as country_code,\n                dt.auth_currency_code         as currency_code,\n                dt.mcc                        as sic_code, \n                -- transaction data\n                dt.card_number                as card_number, \n                dt.card_number_enc            as card_number_enc,\n                dt.card_type                  as card_type,\n                dt.debit_credit_indicator     as debit_credit_ind, \n                dt.tran_date                  as tran_date, \n                ( decode(dt.debit_credit_indicator,'C',-1,1)\n                  * dt.tran_amount )          as tran_amount, \n                dt.acquirer_ref_num           as reference_number, \n                dt.pos_entry_mode             as pos_entry_mode, \n                nvl(emd.pos_entry_desc,\n                    dt.pos_entry_mode)        as pos_entry_mode_desc, \n                null                          as reject_reason,\n                -- auth data\n                dt.auth_response_code         as auth_response_code, \n                null                          as auth_ref_num, \n                dt.tran_id                    as auth_tran_id, \n                dt.val_code                   as auth_val_code, \n                dt.auth_code                  as auth_code,\n                dt.auth_source_code           as auth_source_code,\n                null                          as auth_avs_resp,\n                dt.auth_amount                as auth_amount,\n                dt.auth_date                  as auth_date,\n                dt.aci                        as auth_returned_aci,\n                -- level II data\n                null                          as tax_amount, \n                dt.invoice_num                as purchase_id, \n                null                          as ship_to_zip,\n                null                          as shipping_amount,\n                -- recurring transaction data                        \n                null                          as recurring_pmt_ind,\n                null                          as recurring_pmt_num,\n                null                          as recurring_pmt_count,\n                -- system data\n                dt.debit_credit_indicator     as tran_type,\n                decode(dt.debit_credit_indicator,\n                       'C','Credit','Sale')   as tran_desc\n        from    daily_detail_file_d256_dt       dt,\n                pos_entry_mode_desc             emd\n        where   dt.btc_seq_num =  :1  and\n                dt.batch_date between  :2  and  :3  and\n                emd.pos_entry_code(+) = dt.pos_entry_mode\n        order by dt.fin_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,batchId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"29com.mes.reports.TridentBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1952^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement( new ApiDetail( resultSet ) );
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadDataVitalDetails",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
    }
  }
  
  protected void postHandleRequest(HttpServletRequest request)
  {
    long  nodeId    = HttpHelper.getLong(request,"com.mes.HierarchyNode",0L);
    
    super.postHandleRequest( request );
    
    // this allows for the CIF screen generic report link building 
    // code to automatically set the search value to the merchant number
    if ( isNodeMerchant(nodeId) && getData("searchValue").equals("") )
    {
      // default the search value to the specified node
      String prefix = "";
      if ( getNodeBankId(nodeId) == mesConstants.BANK_ID_CBT )
      {
        prefix = "0";   // append a leading '0' for CB&T accounts
      }
      setData("searchValue",prefix + String.valueOf(nodeId));
    }
    
    if ( usingDefaultReportDates() )
    {
      Calendar  cal = Calendar.getInstance();
      
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      ((ComboDateField)getField("beginDate")).setUtilDate( cal.getTime() );
      
      if ( getInt("hierarchySummary") == 1 )
      {
        // set both dates to previous day
        ((ComboDateField)getField("endDate")).setUtilDate( cal.getTime() );
      }
    }      
  }
  
  public boolean setBatchFilename( long batchId, String action )
  {
    Date          beginDate     = getReportDateBegin();
    Date          endDate       = getReportDateEnd();
    boolean       retVal        = false;
    UserBean      user          = getUser();
    String        userName      = getUserLoginName();
    
    try
    {
      if ( batchId != 0L && user.hasRight(MesUsers.RIGHT_REPORT_TRIDENT_BATCH_HOLD) )
      {
        String  filenameNew = (action + "-" + (userName == null ? "system" : userName));
        String  filenameOld = ("hold".equals(action) ? null : "hold-%");
        
        /*@lineinfo:generated-code*//*@lineinfo:2019^9*/

//  ************************************************************
//  #sql [Ctx] { update  mbs_batches   mb
//            set     mb.load_filename = :filenameNew
//            where   mb.merchant_batch_date between :beginDate and :endDate
//                    and mb.batch_id = :batchId
//                    and nvl(mb.load_filename,'null') like nvl(:filenameOld,'null')
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mbs_batches   mb\n          set     mb.load_filename =  :1 \n          where   mb.merchant_batch_date between  :2  and  :3 \n                  and mb.batch_id =  :4 \n                  and nvl(mb.load_filename,'null') like nvl( :5 ,'null')";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"30com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,filenameNew);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setLong(4,batchId);
   __sJT_st.setString(5,filenameOld);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2026^9*/
        retVal = (Ctx.getExecutionContext().getUpdateCount() != 0);
      }
    }
    catch( Exception e )
    {
      logEntry("setBatchFilename(" + batchId + ")",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd( getReportDateBegin() );
    }
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
    }
  }
  
  public boolean setTranReject( String rejectReason, String terminalId, String tridentBatchId,
                                int batchSource, long batchId, long batchRecId )
  {
    boolean       retVal        = false;
    UserBean      user          = getUser();
    String        userName      = getUserLoginName();

    try
    {
      if ( batchId != 0L && user.hasRight(MesUsers.RIGHT_REPORT_TRIDENT_BATCH_HOLD) )
      {
        // special case: "----" means to clear already-set reject reason
        if( "----".equals(rejectReason) ) rejectReason = null;

        switch( batchSource )
        {
          case mesConstants.MBS_BT_VISAK:
            /*@lineinfo:generated-code*//*@lineinfo:2076^13*/

//  ************************************************************
//  #sql [Ctx] { update  trident_detail_lookup tdl
//                set     tdl.reject_reason = :rejectReason
//                where   tdl.batch_rec_id = :batchId
//                    and tdl.detail_index = :batchRecId
//                    and nvl(tdl.reject_reason,'null') != nvl(:rejectReason,'null')
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_detail_lookup tdl\n              set     tdl.reject_reason =  :1 \n              where   tdl.batch_rec_id =  :2 \n                  and tdl.detail_index =  :3 \n                  and nvl(tdl.reject_reason,'null') != nvl( :4 ,'null')";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"31com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,rejectReason);
   __sJT_st.setLong(2,batchId);
   __sJT_st.setLong(3,batchRecId);
   __sJT_st.setString(4,rejectReason);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2083^13*/
            retVal = (Ctx.getExecutionContext().getUpdateCount() != 0);
            break;

          case mesConstants.MBS_BT_PG:
            /*@lineinfo:generated-code*//*@lineinfo:2088^13*/

//  ************************************************************
//  #sql [Ctx] { update  trident_capture_api tapi
//                set     tapi.reject_reason = :rejectReason
//                where   tapi.rec_id = :batchRecId
//                    and nvl(tapi.reject_reason,'null') != nvl(:rejectReason,'null')
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_capture_api tapi\n              set     tapi.reject_reason =  :1 \n              where   tapi.rec_id =  :2 \n                  and nvl(tapi.reject_reason,'null') != nvl( :3 ,'null')";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"32com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,rejectReason);
   __sJT_st.setLong(2,batchRecId);
   __sJT_st.setString(3,rejectReason);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2094^13*/
            retVal = (Ctx.getExecutionContext().getUpdateCount() != 0);
            break;

          case mesConstants.MBS_BT_VITAL:
          case mesConstants.MBS_BT_CIELO:
            break;
        }

        if( retVal == true )
        {
          if( rejectReason == null )  rejectReason = "reject (clear)";
          else                        rejectReason = "reject (" + rejectReason.toLowerCase() + ")";

          /*@lineinfo:generated-code*//*@lineinfo:2108^11*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_capture_hold_log
//              (
//                terminal_id,
//                batch_id,
//                action_type,
//                action_date,
//                action_user,
//                batch_rec_id,
//                detail_index
//              )
//              values
//              (
//                :terminalId,
//                :tridentBatchId,
//                :rejectReason,
//                sysdate,
//                :userName,
//                :batchId,
//                :batchRecId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into trident_capture_hold_log\n            (\n              terminal_id,\n              batch_id,\n              action_type,\n              action_date,\n              action_user,\n              batch_rec_id,\n              detail_index\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n              sysdate,\n               :4 ,\n               :5 ,\n               :6 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"33com.mes.reports.TridentBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,terminalId);
   __sJT_st.setString(2,tridentBatchId);
   __sJT_st.setString(3,rejectReason);
   __sJT_st.setString(4,userName);
   __sJT_st.setLong(5,batchId);
   __sJT_st.setLong(6,batchRecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2130^11*/
          retVal = (Ctx.getExecutionContext().getUpdateCount() != 0);
        }
      }
    }
    catch( Exception e )
    {
      logEntry("setTranReject(" + batchId + ", "  + batchRecId + ")",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }

  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/